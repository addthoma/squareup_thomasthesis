.class public Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen$DialogBuilder;
.super Ljava/lang/Object;
.source "TicketGroupLimitPopupScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "DialogBuilder"
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field res:Lcom/squareup/util/Res;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field final synthetic this$0:Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen;Landroid/content/Context;)V
    .locals 0

    .line 64
    iput-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen$DialogBuilder;->this$0:Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    iput-object p2, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen$DialogBuilder;->context:Landroid/content/Context;

    .line 66
    const-class p1, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScope$Component;

    invoke-static {p2, p1}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScope$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScope$Component;->inject(Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen$DialogBuilder;)V

    return-void
.end method

.method private getMessage(Lcom/squareup/util/Res;Lcom/squareup/tickets/OpenTicketsSettings;)Ljava/lang/String;
    .locals 3

    .line 91
    sget-object v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen$1;->$SwitchMap$com$squareup$ui$settings$opentickets$ticketgroups$TicketGroupLimitPopupScreen$LimitType:[I

    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen$DialogBuilder;->this$0:Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen;

    invoke-static {v1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen;->access$000(Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen;)Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen$LimitType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen$LimitType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    const-string v2, "limit"

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 98
    sget v0, Lcom/squareup/settingsapplet/R$string;->predefined_tickets_ticket_template_limit_reached_message:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 99
    invoke-interface {p2}, Lcom/squareup/tickets/OpenTicketsSettings;->getMaxTicketTemplatesPerGroup()I

    move-result p2

    invoke-virtual {p1, v2, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 100
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 101
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 103
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Limit type not suppoted: "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen$DialogBuilder;->this$0:Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen;

    invoke-static {v0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen;->access$000(Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen;)Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen$LimitType;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 93
    :cond_1
    sget v0, Lcom/squareup/settingsapplet/R$string;->predefined_tickets_ticket_group_limit_reached_message:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 94
    invoke-interface {p2}, Lcom/squareup/tickets/OpenTicketsSettings;->getMaxTicketGroupCount()I

    move-result p2

    invoke-virtual {p1, v2, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 95
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 96
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private getTitle(Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 2

    .line 80
    sget-object v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen$1;->$SwitchMap$com$squareup$ui$settings$opentickets$ticketgroups$TicketGroupLimitPopupScreen$LimitType:[I

    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen$DialogBuilder;->this$0:Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen;

    invoke-static {v1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen;->access$000(Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen;)Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen$LimitType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen$LimitType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 84
    sget v0, Lcom/squareup/settingsapplet/R$string;->predefined_tickets_ticket_template_limit_reached:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 86
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Limit type not suppoted: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen$DialogBuilder;->this$0:Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen;

    invoke-static {v1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen;->access$000(Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen;)Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen$LimitType;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 82
    :cond_1
    sget v0, Lcom/squareup/settingsapplet/R$string;->predefined_tickets_ticket_group_limit_reached:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method static synthetic lambda$build$0(Landroid/content/DialogInterface;I)V
    .locals 0

    return-void
.end method


# virtual methods
.method public build()Landroid/app/Dialog;
    .locals 3

    .line 70
    new-instance v0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen$DialogBuilder;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen$DialogBuilder;->res:Lcom/squareup/util/Res;

    .line 71
    invoke-direct {p0, v1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen$DialogBuilder;->getTitle(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen$DialogBuilder;->res:Lcom/squareup/util/Res;

    iget-object v2, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen$DialogBuilder;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    .line 72
    invoke-direct {p0, v1, v2}, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen$DialogBuilder;->getMessage(Lcom/squareup/util/Res;Lcom/squareup/tickets/OpenTicketsSettings;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/common/strings/R$string;->ok:I

    sget-object v2, Lcom/squareup/ui/settings/opentickets/ticketgroups/-$$Lambda$TicketGroupLimitPopupScreen$DialogBuilder$3wcubEOwYTq7gF_M3DPZdiMfD1I;->INSTANCE:Lcom/squareup/ui/settings/opentickets/ticketgroups/-$$Lambda$TicketGroupLimitPopupScreen$DialogBuilder$3wcubEOwYTq7gF_M3DPZdiMfD1I;

    .line 73
    invoke-virtual {v0, v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    .line 75
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setCancelable(Z)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 76
    invoke-virtual {v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
