.class Lcom/squareup/ui/settings/opentickets/ticketgroups/DiscardChangePopupScreen;
.super Lcom/squareup/ui/settings/opentickets/ticketgroups/InEditTicketGroupScope;
.source "DiscardChangePopupScreen.java"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/settings/opentickets/ticketgroups/DiscardChangePopupScreen$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/opentickets/ticketgroups/DiscardChangePopupScreen$Factory;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/opentickets/ticketgroups/DiscardChangePopupScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/settings/opentickets/ticketgroups/DiscardChangePopupScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 16
    new-instance v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/DiscardChangePopupScreen;

    invoke-direct {v0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/DiscardChangePopupScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/DiscardChangePopupScreen;->INSTANCE:Lcom/squareup/ui/settings/opentickets/ticketgroups/DiscardChangePopupScreen;

    .line 41
    sget-object v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/DiscardChangePopupScreen;->INSTANCE:Lcom/squareup/ui/settings/opentickets/ticketgroups/DiscardChangePopupScreen;

    .line 42
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/DiscardChangePopupScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/InEditTicketGroupScope;-><init>()V

    return-void
.end method
