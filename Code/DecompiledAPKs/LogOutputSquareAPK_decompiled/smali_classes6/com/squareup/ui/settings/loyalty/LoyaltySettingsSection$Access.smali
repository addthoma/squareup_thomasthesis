.class public final Lcom/squareup/ui/settings/loyalty/LoyaltySettingsSection$Access;
.super Lcom/squareup/ui/settings/checkout/CheckoutSettingsSectionAccess;
.source "LoyaltySettingsSection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/loyalty/LoyaltySettingsSection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Access"
.end annotation


# instance fields
.field private final loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/Features;Lcom/squareup/loyalty/LoyaltySettings;)V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/squareup/permissions/Permission;

    .line 53
    sget-object v1, Lcom/squareup/permissions/Permission;->UPDATE_LOYALTY:Lcom/squareup/permissions/Permission;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/settings/checkout/CheckoutSettingsSectionAccess;-><init>(Lcom/squareup/settings/server/Features;[Lcom/squareup/permissions/Permission;)V

    .line 54
    iput-object p2, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsSection$Access;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    return-void
.end method


# virtual methods
.method public determineVisibility()Z
    .locals 1

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsSection$Access;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    invoke-interface {v0}, Lcom/squareup/loyalty/LoyaltySettings;->isLoyaltyProgramActive()Z

    move-result v0

    return v0
.end method
