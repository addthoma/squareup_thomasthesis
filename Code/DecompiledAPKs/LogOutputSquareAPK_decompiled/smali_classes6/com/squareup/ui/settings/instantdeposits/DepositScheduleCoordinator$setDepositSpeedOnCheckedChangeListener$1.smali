.class final Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator$setDepositSpeedOnCheckedChangeListener$1;
.super Ljava/lang/Object;
.source "DepositScheduleCoordinator.kt"

# interfaces
.implements Lcom/squareup/noho/NohoCheckableGroup$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->setDepositSpeedOnCheckedChangeListener(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "<anonymous parameter 0>",
        "Lcom/squareup/noho/NohoCheckableGroup;",
        "kotlin.jvm.PlatformType",
        "checkedId",
        "",
        "<anonymous parameter 2>",
        "onCheckedChanged"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $hasLinkedCard:Z

.field final synthetic this$0:Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;Z)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator$setDepositSpeedOnCheckedChangeListener$1;->this$0:Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;

    iput-boolean p2, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator$setDepositSpeedOnCheckedChangeListener$1;->$hasLinkedCard:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Lcom/squareup/noho/NohoCheckableGroup;II)V
    .locals 0

    .line 271
    sget p1, Lcom/squareup/settingsapplet/R$id;->deposit_speed_one_to_two_business_days:I

    const/4 p3, 0x0

    if-ne p2, p1, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator$setDepositSpeedOnCheckedChangeListener$1;->this$0:Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->access$getScopeRunner$p(Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;)Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;

    move-result-object p1

    invoke-virtual {p1, p3}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->setSameDayDeposit(Z)V

    goto :goto_0

    .line 272
    :cond_0
    sget p1, Lcom/squareup/settingsapplet/R$id;->deposit_speed_same_day:I

    if-ne p2, p1, :cond_2

    iget-boolean p1, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator$setDepositSpeedOnCheckedChangeListener$1;->$hasLinkedCard:Z

    if-eqz p1, :cond_1

    .line 275
    iget-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator$setDepositSpeedOnCheckedChangeListener$1;->this$0:Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->access$getScopeRunner$p(Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;)Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;

    move-result-object p1

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->setSameDayDeposit(Z)V

    goto :goto_0

    .line 278
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator$setDepositSpeedOnCheckedChangeListener$1;->this$0:Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;

    invoke-static {p1, p3}, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->access$setSameDayDeposit(Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;Z)V

    .line 279
    iget-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator$setDepositSpeedOnCheckedChangeListener$1;->this$0:Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->access$getScopeRunner$p(Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;)Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->goToLinkDebitCardEntryScreen()V

    :cond_2
    :goto_0
    return-void
.end method
