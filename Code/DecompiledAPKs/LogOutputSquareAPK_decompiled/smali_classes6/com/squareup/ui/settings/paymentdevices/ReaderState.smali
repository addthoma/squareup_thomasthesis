.class public Lcom/squareup/ui/settings/paymentdevices/ReaderState;
.super Ljava/lang/Object;
.source "ReaderState.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/paymentdevices/ReaderState$ForFwupProgress;,
        Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;,
        Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;
    }
.end annotation


# instance fields
.field public final cardReaderAddress:Ljava/lang/String;

.field public final cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

.field public final firmwareVersion:Ljava/lang/String;

.field public final hardwareSerialNumber:Ljava/lang/String;

.field public final lastConnectionSuccessUtcMillis:Ljava/lang/Long;

.field public final nickname:Ljava/lang/String;

.field public final serialNumberLast4:Ljava/lang/String;

.field public final type:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 141
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->nickname:Ljava/lang/String;

    .line 142
    iput-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->serialNumberLast4:Ljava/lang/String;

    .line 143
    iput-object p3, p0, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderAddress:Ljava/lang/String;

    .line 144
    iput-object p4, p0, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    .line 145
    iput-object p5, p0, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->type:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    .line 146
    iput-object p6, p0, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->lastConnectionSuccessUtcMillis:Ljava/lang/Long;

    .line 147
    iput-object p7, p0, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->firmwareVersion:Ljava/lang/String;

    .line 148
    iput-object p8, p0, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->hardwareSerialNumber:Ljava/lang/String;

    return-void
.end method

.method public static createReaderStateByMergingInfoFrom(Lcom/squareup/ui/settings/paymentdevices/ReaderState;Lcom/squareup/ui/settings/paymentdevices/ReaderState;)Lcom/squareup/ui/settings/paymentdevices/ReaderState;
    .locals 10

    .line 172
    instance-of v0, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$ForFwupProgress;

    if-eqz v0, :cond_0

    .line 173
    check-cast p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$ForFwupProgress;

    invoke-static {p0, p1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->createReaderStateByMergingInfoFromFwupProgress(Lcom/squareup/ui/settings/paymentdevices/ReaderState;Lcom/squareup/ui/settings/paymentdevices/ReaderState$ForFwupProgress;)Lcom/squareup/ui/settings/paymentdevices/ReaderState$ForFwupProgress;

    move-result-object p0

    return-object p0

    .line 177
    :cond_0
    new-instance v9, Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    iget-object v0, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->nickname:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->nickname:Ljava/lang/String;

    :cond_1
    move-object v1, v0

    iget-object v0, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->serialNumberLast4:Ljava/lang/String;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->serialNumberLast4:Ljava/lang/String;

    :cond_2
    move-object v2, v0

    iget-object v0, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderAddress:Ljava/lang/String;

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderAddress:Ljava/lang/String;

    :cond_3
    move-object v3, v0

    iget-object v0, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    :cond_4
    move-object v4, v0

    iget-object v0, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->type:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->type:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    :cond_5
    move-object v5, v0

    iget-object v0, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->lastConnectionSuccessUtcMillis:Ljava/lang/Long;

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->lastConnectionSuccessUtcMillis:Ljava/lang/Long;

    :cond_6
    move-object v6, v0

    iget-object v0, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->firmwareVersion:Ljava/lang/String;

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->firmwareVersion:Ljava/lang/String;

    :cond_7
    move-object v7, v0

    iget-object p1, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->hardwareSerialNumber:Ljava/lang/String;

    if-nez p1, :cond_8

    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->hardwareSerialNumber:Ljava/lang/String;

    move-object v8, p0

    goto :goto_0

    :cond_8
    move-object v8, p1

    :goto_0
    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/squareup/ui/settings/paymentdevices/ReaderState;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)V

    return-object v9
.end method

.method private static createReaderStateByMergingInfoFromFwupProgress(Lcom/squareup/ui/settings/paymentdevices/ReaderState;Lcom/squareup/ui/settings/paymentdevices/ReaderState$ForFwupProgress;)Lcom/squareup/ui/settings/paymentdevices/ReaderState$ForFwupProgress;
    .locals 12

    .line 203
    new-instance v11, Lcom/squareup/ui/settings/paymentdevices/ReaderState$ForFwupProgress;

    iget-object v0, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$ForFwupProgress;->nickname:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->nickname:Ljava/lang/String;

    goto :goto_0

    :cond_0
    iget-object v0, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$ForFwupProgress;->nickname:Ljava/lang/String;

    :goto_0
    move-object v1, v0

    iget-object v0, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$ForFwupProgress;->serialNumberLast4:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->serialNumberLast4:Ljava/lang/String;

    goto :goto_1

    :cond_1
    iget-object v0, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$ForFwupProgress;->serialNumberLast4:Ljava/lang/String;

    :goto_1
    move-object v2, v0

    iget-object v0, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$ForFwupProgress;->cardReaderAddress:Ljava/lang/String;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderAddress:Ljava/lang/String;

    goto :goto_2

    :cond_2
    iget-object v0, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$ForFwupProgress;->cardReaderAddress:Ljava/lang/String;

    :goto_2
    move-object v3, v0

    iget-object v0, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$ForFwupProgress;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    goto :goto_3

    :cond_3
    iget-object v0, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$ForFwupProgress;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    :goto_3
    move-object v4, v0

    iget-object v0, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$ForFwupProgress;->type:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->type:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    goto :goto_4

    :cond_4
    iget-object v0, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$ForFwupProgress;->type:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    :goto_4
    move-object v5, v0

    iget-object v0, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$ForFwupProgress;->lastConnectionSuccessUtcMillis:Ljava/lang/Long;

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->lastConnectionSuccessUtcMillis:Ljava/lang/Long;

    goto :goto_5

    :cond_5
    iget-object v0, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$ForFwupProgress;->lastConnectionSuccessUtcMillis:Ljava/lang/Long;

    :goto_5
    move-object v6, v0

    iget-object v0, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$ForFwupProgress;->firmwareVersion:Ljava/lang/String;

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->firmwareVersion:Ljava/lang/String;

    goto :goto_6

    :cond_6
    iget-object v0, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$ForFwupProgress;->firmwareVersion:Ljava/lang/String;

    :goto_6
    move-object v7, v0

    iget-object v0, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$ForFwupProgress;->hardwareSerialNumber:Ljava/lang/String;

    if-nez v0, :cond_7

    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->hardwareSerialNumber:Ljava/lang/String;

    goto :goto_7

    :cond_7
    iget-object p0, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$ForFwupProgress;->hardwareSerialNumber:Ljava/lang/String;

    :goto_7
    move-object v8, p0

    iget v9, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$ForFwupProgress;->currentPercentComplete:I

    iget v10, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$ForFwupProgress;->totalPercentComplete:I

    move-object v0, v11

    invoke-direct/range {v0 .. v10}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$ForFwupProgress;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;II)V

    return-object v11
.end method


# virtual methods
.method public getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;
    .locals 2

    .line 152
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    if-eqz v0, :cond_0

    .line 153
    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v0

    return-object v0

    .line 156
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->type:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_BLE_DISCONNECTED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->type:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_STILL_BLE_PAIRING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    if-ne v0, v1, :cond_1

    goto :goto_0

    .line 161
    :cond_1
    sget-object v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->UNKNOWN:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    return-object v0

    .line 158
    :cond_2
    :goto_0
    sget-object v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R12:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    return-object v0
.end method
