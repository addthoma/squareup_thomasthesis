.class public abstract Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScope$Module;
.super Ljava/lang/Object;
.source "GiftCardsSettingsScope.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Module"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract addDesignsSettingsScreenRunner(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;)Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract cropCustomDesignSettingsScreenRunner(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;)Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract giftCardsSettingsScreenRunner(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;)Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract uploadCustomDesignDialogScreenRunner(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;)Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract viewDesignsSettingsScreenRunner(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;)Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
