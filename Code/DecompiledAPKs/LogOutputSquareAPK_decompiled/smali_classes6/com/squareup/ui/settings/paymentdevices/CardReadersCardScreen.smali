.class public Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "CardReadersCardScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/spot/HasSpot;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardOverSheetScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Component;,
        Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$ParentComponent;,
        Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 49
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen;

    invoke-direct {v0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen;->INSTANCE:Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen;

    .line 215
    sget-object v0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen;->INSTANCE:Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 51
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 60
    sget-object p1, Lcom/squareup/container/spot/Spots;->GROW_OVER:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 56
    sget v0, Lcom/squareup/cardreader/ui/R$layout;->card_readers_card_view:I

    return v0
.end method
