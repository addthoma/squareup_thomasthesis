.class public Lcom/squareup/ui/settings/sharedsettings/SharedSettingsSection$ListEntry;
.super Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;
.source "SharedSettingsSection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/sharedsettings/SharedSettingsSection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ListEntry"
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/ui/settings/sharedsettings/SharedSettingsSection;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$SettingsAppletGrouping;)V
    .locals 6

    .line 39
    sget v3, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsSection;->TITLE_ID:I

    move-object v0, p0

    move-object v1, p1

    move-object v2, p4

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;-><init>(Lcom/squareup/applet/AppletSection;Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$SettingsAppletGrouping;ILcom/squareup/util/Res;Lcom/squareup/util/Device;)V

    return-void
.end method


# virtual methods
.method public getValueText()Ljava/lang/String;
    .locals 2

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsSection$ListEntry;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/settingsapplet/R$string;->shared_settings_synced:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
