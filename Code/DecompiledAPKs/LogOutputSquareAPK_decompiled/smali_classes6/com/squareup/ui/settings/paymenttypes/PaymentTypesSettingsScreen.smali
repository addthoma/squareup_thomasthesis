.class public Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen;
.super Lcom/squareup/ui/settings/InSettingsAppletScope;
.source "PaymentTypesSettingsScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/layer/InSection;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;,
        Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 46
    new-instance v0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen;

    invoke-direct {v0}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen;->INSTANCE:Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen;

    .line 47
    sget-object v0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen;->INSTANCE:Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen;

    .line 48
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 50
    invoke-direct {p0}, Lcom/squareup/ui/settings/InSettingsAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 54
    const-class v0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 58
    sget v0, Lcom/squareup/settingsapplet/R$layout;->payment_types_settings_view:I

    return v0
.end method
