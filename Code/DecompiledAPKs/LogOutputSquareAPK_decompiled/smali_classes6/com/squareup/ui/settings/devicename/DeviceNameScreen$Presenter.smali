.class Lcom/squareup/ui/settings/devicename/DeviceNameScreen$Presenter;
.super Lcom/squareup/ui/settings/SettingsSectionPresenter;
.source "DeviceNameScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/devicename/DeviceNameScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/settings/SettingsSectionPresenter<",
        "Lcom/squareup/ui/settings/devicename/DeviceNameView;",
        ">;"
    }
.end annotation


# instance fields
.field private final deviceNameSetting:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final flow:Lflow/Flow;

.field private final res:Lcom/squareup/util/Res;

.field private final sidebarRefresher:Lcom/squareup/ui/settings/SidebarRefresher;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;Lcom/squareup/settings/LocalSetting;Lcom/squareup/util/Res;Lcom/squareup/ui/settings/SidebarRefresher;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/ui/settings/SidebarRefresher;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 47
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter;-><init>(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;)V

    .line 48
    iput-object p2, p0, Lcom/squareup/ui/settings/devicename/DeviceNameScreen$Presenter;->deviceNameSetting:Lcom/squareup/settings/LocalSetting;

    .line 49
    iput-object p3, p0, Lcom/squareup/ui/settings/devicename/DeviceNameScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 50
    iput-object p4, p0, Lcom/squareup/ui/settings/devicename/DeviceNameScreen$Presenter;->sidebarRefresher:Lcom/squareup/ui/settings/SidebarRefresher;

    .line 51
    invoke-virtual {p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;->getFlow()Lflow/Flow;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/devicename/DeviceNameScreen$Presenter;->flow:Lflow/Flow;

    return-void
.end method


# virtual methods
.method public getActionbarText()Ljava/lang/String;
    .locals 2

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/settings/devicename/DeviceNameScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/settings/devicename/DeviceSection;->TITLE_ID:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method onBackPressed()Z
    .locals 1

    .line 87
    invoke-virtual {p0}, Lcom/squareup/ui/settings/devicename/DeviceNameScreen$Presenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    invoke-virtual {p0}, Lcom/squareup/ui/settings/devicename/DeviceNameScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method onDoneKeyPressed()V
    .locals 2

    .line 94
    invoke-virtual {p0}, Lcom/squareup/ui/settings/devicename/DeviceNameScreen$Presenter;->saveSettings()V

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/settings/devicename/DeviceNameScreen$Presenter;->sidebarRefresher:Lcom/squareup/ui/settings/SidebarRefresher;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/SidebarRefresher;->refresh()V

    .line 96
    invoke-virtual {p0}, Lcom/squareup/ui/settings/devicename/DeviceNameScreen$Presenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 97
    invoke-virtual {p0}, Lcom/squareup/ui/settings/devicename/DeviceNameScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 99
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/settings/devicename/DeviceNameScreen$Presenter;->getDevice()Lcom/squareup/util/Device;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 100
    iget-object v0, p0, Lcom/squareup/ui/settings/devicename/DeviceNameScreen$Presenter;->flow:Lflow/Flow;

    invoke-virtual {p0}, Lcom/squareup/ui/settings/devicename/DeviceNameScreen$Presenter;->screenForAssertion()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    :cond_1
    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 55
    invoke-super {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter;->onLoad(Landroid/os/Bundle;)V

    if-nez p1, :cond_0

    .line 58
    invoke-virtual {p0}, Lcom/squareup/ui/settings/devicename/DeviceNameScreen$Presenter;->getDevice()Lcom/squareup/util/Device;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 59
    invoke-virtual {p0}, Lcom/squareup/ui/settings/devicename/DeviceNameScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/devicename/DeviceNameView;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/devicename/DeviceNameView;->requestInitialFocus()V

    .line 62
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/settings/devicename/DeviceNameScreen$Presenter;->deviceNameSetting:Lcom/squareup/settings/LocalSetting;

    const-string v0, ""

    invoke-interface {p1, v0}, Lcom/squareup/settings/LocalSetting;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 63
    invoke-virtual {p0}, Lcom/squareup/ui/settings/devicename/DeviceNameScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/devicename/DeviceNameView;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/devicename/DeviceNameView;->setDeviceNameValue(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected onUpPressed()Z
    .locals 1

    .line 79
    invoke-virtual {p0}, Lcom/squareup/ui/settings/devicename/DeviceNameScreen$Presenter;->saveSettings()V

    .line 80
    invoke-virtual {p0}, Lcom/squareup/ui/settings/devicename/DeviceNameScreen$Presenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    invoke-virtual {p0}, Lcom/squareup/ui/settings/devicename/DeviceNameScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method protected saveSettings()V
    .locals 2

    .line 75
    iget-object v0, p0, Lcom/squareup/ui/settings/devicename/DeviceNameScreen$Presenter;->deviceNameSetting:Lcom/squareup/settings/LocalSetting;

    invoke-virtual {p0}, Lcom/squareup/ui/settings/devicename/DeviceNameScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/settings/devicename/DeviceNameView;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/devicename/DeviceNameView;->getDeviceNameValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public screenForAssertion()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/ui/main/RegisterTreeKey;",
            ">;"
        }
    .end annotation

    .line 67
    const-class v0, Lcom/squareup/ui/settings/devicename/DeviceNameScreen;

    return-object v0
.end method
