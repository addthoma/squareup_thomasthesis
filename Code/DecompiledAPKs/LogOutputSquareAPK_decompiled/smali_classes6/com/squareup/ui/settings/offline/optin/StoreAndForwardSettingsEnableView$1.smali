.class Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableView$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "StoreAndForwardSettingsEnableView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableView;->onAttachedToWindow()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableView;)V
    .locals 0

    .line 35
    iput-object p1, p0, Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableView$1;->this$0:Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableView;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 0

    .line 37
    iget-object p1, p0, Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableView$1;->this$0:Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableView;

    iget-object p1, p1, Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableView;->presenter:Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableScreen$Presenter;->enableOfflinePayments()V

    return-void
.end method
