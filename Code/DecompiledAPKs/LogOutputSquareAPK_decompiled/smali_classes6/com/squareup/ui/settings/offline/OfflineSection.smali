.class public Lcom/squareup/ui/settings/offline/OfflineSection;
.super Lcom/squareup/applet/AppletSection;
.source "OfflineSection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/offline/OfflineSection$Access;,
        Lcom/squareup/ui/settings/offline/OfflineSection$ListEntry;
    }
.end annotation


# static fields
.field public static TITLE_ID:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    sget v0, Lcom/squareup/registerlib/R$string;->offline_mode:I

    sput v0, Lcom/squareup/ui/settings/offline/OfflineSection;->TITLE_ID:I

    return-void
.end method

.method public constructor <init>(Lcom/squareup/settings/server/Features;Lcom/squareup/payment/offline/OfflineModeCanBeOffered;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 26
    new-instance v0, Lcom/squareup/ui/settings/offline/OfflineSection$Access;

    invoke-direct {v0, p1, p2}, Lcom/squareup/ui/settings/offline/OfflineSection$Access;-><init>(Lcom/squareup/settings/server/Features;Lcom/squareup/payment/offline/OfflineModeCanBeOffered;)V

    invoke-direct {p0, v0}, Lcom/squareup/applet/AppletSection;-><init>(Lcom/squareup/applet/SectionAccess;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic getInitialScreen()Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lcom/squareup/ui/settings/offline/OfflineSection;->getInitialScreen()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    return-object v0
.end method

.method public getInitialScreen()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 1

    .line 30
    sget-object v0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen;->INSTANCE:Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen;

    return-object v0
.end method
