.class Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder$2;
.super Lcom/squareup/debounce/DebouncedTextWatcher;
.source "EditTicketGroupRecyclerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;-><init>(Landroid/view/View;Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;

.field final synthetic val$presenter:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;)V
    .locals 0

    .line 97
    iput-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder$2;->this$0:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;

    iput-object p2, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder$2;->val$presenter:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public doAfterTextChanged(Landroid/text/Editable;)V
    .locals 3

    .line 99
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 100
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder$2;->this$0:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;

    iget-object v0, v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;->ticketTemplateBuilder:Lcom/squareup/api/items/TicketTemplate$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/TicketTemplate$Builder;->name(Ljava/lang/String;)Lcom/squareup/api/items/TicketTemplate$Builder;

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder$2;->this$0:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;

    iget-boolean v0, v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;->editingLastTemplate:Z

    if-eqz v0, :cond_1

    .line 103
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder$2;->val$presenter:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder$2;->this$0:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;

    iget-object v1, v1, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;->ticketTemplateBuilder:Lcom/squareup/api/items/TicketTemplate$Builder;

    iget-object v2, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder$2;->this$0:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;

    invoke-virtual {v2}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;->getAdapterPosition()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;->onLastTemplateNameChanged(Lcom/squareup/api/items/TicketTemplate$Builder;I)V

    .line 105
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 106
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder$2;->this$0:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;

    iget-object p1, p1, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;->ticketTemplateRow:Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;->hideControls()V

    goto :goto_0

    .line 108
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder$2;->this$0:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;

    iget-object p1, p1, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;->ticketTemplateRow:Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;->showControls()V

    :cond_1
    :goto_0
    return-void
.end method
