.class public final Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter_Factory;
.super Ljava/lang/Object;
.source "PairingPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final activityVisibilityPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final apiReaderSettingsControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiReaderSettingsController;",
            ">;"
        }
    .end annotation
.end field

.field private final bleEventLogFilterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleEventLogFilter;",
            ">;"
        }
    .end annotation
.end field

.field private final bluetoothStatusReceiverProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;",
            ">;"
        }
    .end annotation
.end field

.field private final bluetoothUtilsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderListenersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderMessagesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderOracleProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            ">;"
        }
    .end annotation
.end field

.field private final clockProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final readerTypeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/client/bills/CardData$ReaderType;",
            ">;"
        }
    .end annotation
.end field

.field private final readerUiEventSinkProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderUiEventSink;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/pairing/PairingScope$Runner;",
            ">;"
        }
    .end annotation
.end field

.field private final waitForAdditionalReadersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final wirelessSearcherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/WirelessSearcher;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/WirelessSearcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleEventLogFilter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/pairing/PairingScope$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderUiEventSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/client/bills/CardData$ReaderType;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiReaderSettingsController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 82
    iput-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter_Factory;->bluetoothUtilsProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 83
    iput-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter_Factory;->wirelessSearcherProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 84
    iput-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter_Factory;->bluetoothStatusReceiverProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 85
    iput-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter_Factory;->cardReaderFactoryProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 86
    iput-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter_Factory;->cardReaderListenersProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 87
    iput-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter_Factory;->cardReaderMessagesProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 88
    iput-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter_Factory;->cardReaderOracleProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 89
    iput-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter_Factory;->clockProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 90
    iput-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 91
    iput-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter_Factory;->bleEventLogFilterProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 92
    iput-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter_Factory;->runnerProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 93
    iput-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter_Factory;->readerUiEventSinkProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 94
    iput-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 95
    iput-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter_Factory;->waitForAdditionalReadersProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 96
    iput-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter_Factory;->readerTypeProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 97
    iput-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter_Factory;->activityVisibilityPresenterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 98
    iput-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter_Factory;->apiReaderSettingsControllerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p18

    .line 99
    iput-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter_Factory;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/WirelessSearcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleEventLogFilter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/pairing/PairingScope$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderUiEventSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/client/bills/CardData$ReaderType;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiReaderSettingsController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;)",
            "Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    .line 122
    new-instance v19, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter_Factory;

    move-object/from16 v0, v19

    invoke-direct/range {v0 .. v18}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v19
.end method

.method public static newInstance(Lcom/squareup/cardreader/BluetoothUtils;Lcom/squareup/cardreader/WirelessSearcher;Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;Lcom/squareup/cardreader/CardReaderFactory;Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/util/Clock;Lcom/squareup/util/Res;Lcom/squareup/cardreader/ble/BleEventLogFilter;Lcom/squareup/ui/settings/paymentdevices/pairing/PairingScope$Runner;Lcom/squareup/cardreader/dipper/ReaderUiEventSink;Lcom/squareup/thread/executor/MainThread;ZLcom/squareup/protos/client/bills/CardData$ReaderType;Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;Lcom/squareup/api/ApiReaderSettingsController;Lflow/Flow;)Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;
    .locals 20

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    .line 133
    new-instance v19, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;

    move-object/from16 v0, v19

    invoke-direct/range {v0 .. v18}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;-><init>(Lcom/squareup/cardreader/BluetoothUtils;Lcom/squareup/cardreader/WirelessSearcher;Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;Lcom/squareup/cardreader/CardReaderFactory;Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/util/Clock;Lcom/squareup/util/Res;Lcom/squareup/cardreader/ble/BleEventLogFilter;Lcom/squareup/ui/settings/paymentdevices/pairing/PairingScope$Runner;Lcom/squareup/cardreader/dipper/ReaderUiEventSink;Lcom/squareup/thread/executor/MainThread;ZLcom/squareup/protos/client/bills/CardData$ReaderType;Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;Lcom/squareup/api/ApiReaderSettingsController;Lflow/Flow;)V

    return-object v19
.end method


# virtual methods
.method public get()Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;
    .locals 20

    move-object/from16 v0, p0

    .line 104
    iget-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter_Factory;->bluetoothUtilsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/cardreader/BluetoothUtils;

    iget-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter_Factory;->wirelessSearcherProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/cardreader/WirelessSearcher;

    iget-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter_Factory;->bluetoothStatusReceiverProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;

    iget-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter_Factory;->cardReaderFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/cardreader/CardReaderFactory;

    iget-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter_Factory;->cardReaderListenersProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/cardreader/CardReaderListeners;

    iget-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter_Factory;->cardReaderMessagesProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;

    iget-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter_Factory;->cardReaderOracleProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    iget-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter_Factory;->clockProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/util/Clock;

    iget-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/util/Res;

    iget-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter_Factory;->bleEventLogFilterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/cardreader/ble/BleEventLogFilter;

    iget-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter_Factory;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingScope$Runner;

    iget-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter_Factory;->readerUiEventSinkProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;

    iget-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/thread/executor/MainThread;

    iget-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter_Factory;->waitForAdditionalReadersProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v15

    iget-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter_Factory;->readerTypeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/protos/client/bills/CardData$ReaderType;

    iget-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter_Factory;->activityVisibilityPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;

    iget-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter_Factory;->apiReaderSettingsControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lcom/squareup/api/ApiReaderSettingsController;

    iget-object v1, v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v19, v1

    check-cast v19, Lflow/Flow;

    invoke-static/range {v2 .. v19}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter_Factory;->newInstance(Lcom/squareup/cardreader/BluetoothUtils;Lcom/squareup/cardreader/WirelessSearcher;Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;Lcom/squareup/cardreader/CardReaderFactory;Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/util/Clock;Lcom/squareup/util/Res;Lcom/squareup/cardreader/ble/BleEventLogFilter;Lcom/squareup/ui/settings/paymentdevices/pairing/PairingScope$Runner;Lcom/squareup/cardreader/dipper/ReaderUiEventSink;Lcom/squareup/thread/executor/MainThread;ZLcom/squareup/protos/client/bills/CardData$ReaderType;Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;Lcom/squareup/api/ApiReaderSettingsController;Lflow/Flow;)Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter_Factory;->get()Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;

    move-result-object v0

    return-object v0
.end method
