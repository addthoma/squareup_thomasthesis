.class public Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen;
.super Lcom/squareup/ui/settings/printerstations/station/InPrinterStationScope;
.source "PrinterStationDetailScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/layer/InSection;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Component;,
        Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 667
    sget-object v0, Lcom/squareup/ui/settings/printerstations/station/-$$Lambda$PrinterStationDetailScreen$PfoqfRQZJ2Bs0qIKkc0l2QngcQ4;->INSTANCE:Lcom/squareup/ui/settings/printerstations/station/-$$Lambda$PrinterStationDetailScreen$PfoqfRQZJ2Bs0qIKkc0l2QngcQ4;

    .line 668
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;)V
    .locals 0

    .line 78
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/printerstations/station/InPrinterStationScope;-><init>(Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen;
    .locals 1

    .line 669
    const-class v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;

    .line 670
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;

    .line 671
    new-instance v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen;-><init>(Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 663
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/settings/printerstations/station/InPrinterStationScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 664
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen;->printerStationFlow:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 86
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_PRINTER_EDIT:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 82
    const-class v0, Lcom/squareup/ui/settings/printerstations/PrinterStationsSection;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 675
    sget v0, Lcom/squareup/settingsapplet/R$layout;->printer_station_detail_view:I

    return v0
.end method
