.class public Lcom/squareup/ui/settings/paymentdevices/pairing/PairingScope$Module;
.super Ljava/lang/Object;
.source "PairingScope.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/paymentdevices/pairing/PairingScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Module"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideScopeRunner()Lcom/squareup/ui/settings/paymentdevices/pairing/PairingScope$Runner;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 19
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingScope$Runner;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingScope$Runner;-><init>(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingScope$1;)V

    return-object v0
.end method
