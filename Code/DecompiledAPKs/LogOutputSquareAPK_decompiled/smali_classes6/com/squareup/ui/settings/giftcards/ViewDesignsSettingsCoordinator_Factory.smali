.class public final Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator_Factory;
.super Ljava/lang/Object;
.source "ViewDesignsSettingsCoordinator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;",
        ">;"
    }
.end annotation


# instance fields
.field private final picassoProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/picasso/Picasso;",
            ">;"
        }
    .end annotation
.end field

.field private final recyclerFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/recycler/RecyclerFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsScreen$Runner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsScreen$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/picasso/Picasso;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/recycler/RecyclerFactory;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator_Factory;->runnerProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator_Factory;->picassoProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator_Factory;->recyclerFactoryProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsScreen$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/picasso/Picasso;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/recycler/RecyclerFactory;",
            ">;)",
            "Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator_Factory;"
        }
    .end annotation

    .line 40
    new-instance v0, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsScreen$Runner;Lcom/squareup/picasso/Picasso;Lcom/squareup/recycler/RecyclerFactory;)Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;
    .locals 1

    .line 45
    new-instance v0, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;-><init>(Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsScreen$Runner;Lcom/squareup/picasso/Picasso;Lcom/squareup/recycler/RecyclerFactory;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;
    .locals 3

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator_Factory;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsScreen$Runner;

    iget-object v1, p0, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator_Factory;->picassoProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/picasso/Picasso;

    iget-object v2, p0, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator_Factory;->recyclerFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/recycler/RecyclerFactory;

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator_Factory;->newInstance(Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsScreen$Runner;Lcom/squareup/picasso/Picasso;Lcom/squareup/recycler/RecyclerFactory;)Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator_Factory;->get()Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;

    move-result-object v0

    return-object v0
.end method
