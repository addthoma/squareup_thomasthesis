.class public final Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen;
.super Lcom/squareup/ui/settings/InSettingsAppletScope;
.source "PrinterStationsListScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/layer/InSection;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Component;,
        Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;,
        Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$RowType;
    }
.end annotation


# static fields
.field static final BUTTON_ROW:I = 0x0

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen;",
            ">;"
        }
    .end annotation
.end field

.field static final HEADER_ROW:I = 0x1

.field public static final INSTANCE:Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen;

.field static final PRINTER_STATION_ROW:I = 0x2


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 42
    new-instance v0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen;

    invoke-direct {v0}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen;->INSTANCE:Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen;

    .line 321
    sget-object v0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen;->INSTANCE:Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen;

    .line 322
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 44
    invoke-direct {p0}, Lcom/squareup/ui/settings/InSettingsAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 48
    const-class v0, Lcom/squareup/ui/settings/printerstations/PrinterStationsSection;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 325
    sget v0, Lcom/squareup/settingsapplet/R$layout;->printer_stations_list_view:I

    return v0
.end method
