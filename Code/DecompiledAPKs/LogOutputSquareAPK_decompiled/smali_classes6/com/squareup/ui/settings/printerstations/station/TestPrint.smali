.class public Lcom/squareup/ui/settings/printerstations/station/TestPrint;
.super Ljava/lang/Object;
.source "TestPrint.java"


# static fields
.field static final PRINT_JOB_SOURCE:Ljava/lang/String; = "TestPrint"


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final hardwarePrinterTracker:Lcom/squareup/print/HardwarePrinterTracker;

.field private final printSpooler:Lcom/squareup/print/PrintSpooler;

.field private final res:Lcom/squareup/util/Res;

.field private final testReceiptPayloadFactory:Lcom/squareup/print/payload/TestReceiptPayloadFactory;

.field private final ticketPayloadFactory:Lcom/squareup/print/payload/TicketPayload$Factory;


# direct methods
.method public constructor <init>(Lcom/squareup/print/payload/TestReceiptPayloadFactory;Lcom/squareup/print/PrintSpooler;Lcom/squareup/print/payload/TicketPayload$Factory;Lcom/squareup/print/HardwarePrinterTracker;Lcom/squareup/settings/server/Features;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Lcom/squareup/protos/common/CurrencyCode;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/TestPrint;->testReceiptPayloadFactory:Lcom/squareup/print/payload/TestReceiptPayloadFactory;

    .line 55
    iput-object p2, p0, Lcom/squareup/ui/settings/printerstations/station/TestPrint;->printSpooler:Lcom/squareup/print/PrintSpooler;

    .line 56
    iput-object p3, p0, Lcom/squareup/ui/settings/printerstations/station/TestPrint;->ticketPayloadFactory:Lcom/squareup/print/payload/TicketPayload$Factory;

    .line 57
    iput-object p4, p0, Lcom/squareup/ui/settings/printerstations/station/TestPrint;->hardwarePrinterTracker:Lcom/squareup/print/HardwarePrinterTracker;

    .line 58
    iput-object p5, p0, Lcom/squareup/ui/settings/printerstations/station/TestPrint;->features:Lcom/squareup/settings/server/Features;

    .line 59
    iput-object p6, p0, Lcom/squareup/ui/settings/printerstations/station/TestPrint;->res:Lcom/squareup/util/Res;

    .line 60
    iput-object p7, p0, Lcom/squareup/ui/settings/printerstations/station/TestPrint;->analytics:Lcom/squareup/analytics/Analytics;

    .line 61
    iput-object p8, p0, Lcom/squareup/ui/settings/printerstations/station/TestPrint;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    return-void
.end method

.method private buildOrderForTestPrint()Lcom/squareup/payment/Order;
    .locals 6

    .line 114
    new-instance v0, Lcom/squareup/payment/Order$Builder;

    invoke-direct {v0}, Lcom/squareup/payment/Order$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/TestPrint;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    .line 115
    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->currencyCode(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/calc/constants/RoundingType;->BANKERS:Lcom/squareup/calc/constants/RoundingType;

    .line 116
    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->roundingType(Lcom/squareup/calc/constants/RoundingType;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/squareup/checkout/CartItem;

    new-instance v2, Lcom/squareup/checkout/CartItem$Builder;

    invoke-direct {v2}, Lcom/squareup/checkout/CartItem$Builder;-><init>()V

    iget-object v3, p0, Lcom/squareup/ui/settings/printerstations/station/TestPrint;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/settingsapplet/R$string;->printer_test_print_item_example:I

    .line 118
    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/checkout/CartItem$Builder;->itemName(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/settings/printerstations/station/TestPrint;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    const-wide/16 v4, 0x64

    .line 119
    invoke-static {v4, v5, v3}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/checkout/CartItem$Builder;->variablePrice(Lcom/squareup/protos/common/Money;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v2

    .line 120
    invoke-virtual {v2}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 117
    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->items([Lcom/squareup/checkout/CartItem;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 121
    invoke-virtual {v0}, Lcom/squareup/payment/Order$Builder;->build()Lcom/squareup/payment/Order;

    move-result-object v0

    .line 122
    invoke-virtual {v0}, Lcom/squareup/payment/Order;->snapshot()Lcom/squareup/payment/OrderSnapshot;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method performTestPrint(Ljava/lang/String;)V
    .locals 20

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    .line 65
    iget-object v2, v0, Lcom/squareup/ui/settings/printerstations/station/TestPrint;->hardwarePrinterTracker:Lcom/squareup/print/HardwarePrinterTracker;

    invoke-virtual {v2, v1}, Lcom/squareup/print/HardwarePrinterTracker;->getHardwarePrinter(Ljava/lang/String;)Lcom/squareup/print/HardwarePrinter;

    move-result-object v2

    .line 66
    iget-object v3, v0, Lcom/squareup/ui/settings/printerstations/station/TestPrint;->hardwarePrinterTracker:Lcom/squareup/print/HardwarePrinterTracker;

    invoke-virtual {v3, v1}, Lcom/squareup/print/HardwarePrinterTracker;->getCachedHardwareInfo(Ljava/lang/String;)Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    move-result-object v1

    .line 67
    invoke-virtual {v1}, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->getDisplayableModelName()Ljava/lang/String;

    move-result-object v3

    .line 68
    iget-object v4, v1, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->connectionType:Lcom/squareup/print/ConnectionType;

    sget-object v5, Lcom/squareup/print/ConnectionType;->INTERNAL:Lcom/squareup/print/ConnectionType;

    const/4 v6, 0x0

    if-eq v4, v5, :cond_0

    iget-object v4, v1, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->connectionType:Lcom/squareup/print/ConnectionType;

    sget-object v5, Lcom/squareup/print/ConnectionType;->FAKE_INTERNAL:Lcom/squareup/print/ConnectionType;

    if-ne v4, v5, :cond_1

    :cond_0
    move-object v3, v6

    .line 74
    :cond_1
    iget-boolean v4, v1, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->supportsRasterMode:Z

    const-string v5, "TestPrint"

    if-eqz v4, :cond_2

    .line 75
    iget-object v4, v0, Lcom/squareup/ui/settings/printerstations/station/TestPrint;->testReceiptPayloadFactory:Lcom/squareup/print/payload/TestReceiptPayloadFactory;

    .line 76
    invoke-direct/range {p0 .. p0}, Lcom/squareup/ui/settings/printerstations/station/TestPrint;->buildOrderForTestPrint()Lcom/squareup/payment/Order;

    move-result-object v6

    invoke-virtual {v4, v3, v6}, Lcom/squareup/print/payload/TestReceiptPayloadFactory;->createPayload(Ljava/lang/String;Lcom/squareup/payment/Order;)Lcom/squareup/print/payload/ReceiptPayload;

    move-result-object v3

    .line 77
    iget-object v4, v0, Lcom/squareup/ui/settings/printerstations/station/TestPrint;->printSpooler:Lcom/squareup/print/PrintSpooler;

    iget-object v6, v0, Lcom/squareup/ui/settings/printerstations/station/TestPrint;->res:Lcom/squareup/util/Res;

    sget v7, Lcom/squareup/settingsapplet/R$string;->printer_stations_test_print_job_name:I

    .line 78
    invoke-interface {v6, v7}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 77
    invoke-virtual {v4, v2, v3, v6, v5}, Lcom/squareup/print/PrintSpooler;->enqueueForTestPrint(Lcom/squareup/print/HardwarePrinter;Lcom/squareup/print/PrintablePayload;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 81
    :cond_2
    iget-object v3, v0, Lcom/squareup/ui/settings/printerstations/station/TestPrint;->features:Lcom/squareup/settings/server/Features;

    sget-object v4, Lcom/squareup/settings/server/Features$Feature;->DINING_OPTIONS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v3, v4}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 82
    iget-object v3, v0, Lcom/squareup/ui/settings/printerstations/station/TestPrint;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/settingsapplet/R$string;->printer_test_print_uppercase_dining_option_for_here:I

    .line 83
    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    .line 82
    invoke-static {v3, v4}, Lcom/squareup/checkout/DiningOption;->forTesting(Ljava/lang/String;I)Lcom/squareup/checkout/DiningOption;

    move-result-object v6

    :cond_3
    move-object/from16 v16, v6

    .line 86
    new-instance v3, Lcom/squareup/checkout/CartItem$Builder;

    invoke-direct {v3}, Lcom/squareup/checkout/CartItem$Builder;-><init>()V

    iget-object v4, v0, Lcom/squareup/ui/settings/printerstations/station/TestPrint;->res:Lcom/squareup/util/Res;

    sget v6, Lcom/squareup/settingsapplet/R$string;->printer_test_print_item_example:I

    .line 87
    invoke-interface {v4, v6}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/squareup/checkout/CartItem$Builder;->itemName(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v3

    sget-object v4, Lcom/squareup/util/BigDecimals;->TWO:Ljava/math/BigDecimal;

    .line 88
    invoke-virtual {v3, v4}, Lcom/squareup/checkout/CartItem$Builder;->quantity(Ljava/math/BigDecimal;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v3

    const-wide/16 v6, 0x64

    iget-object v4, v0, Lcom/squareup/ui/settings/printerstations/station/TestPrint;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    .line 89
    invoke-static {v6, v7, v4}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/squareup/checkout/CartItem$Builder;->variablePrice(Lcom/squareup/protos/common/Money;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v3

    .line 90
    invoke-virtual {v3}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object v3

    .line 86
    invoke-static {v3}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    .line 92
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    const-string v6, "2334"

    .line 93
    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 95
    iget-object v7, v0, Lcom/squareup/ui/settings/printerstations/station/TestPrint;->ticketPayloadFactory:Lcom/squareup/print/payload/TicketPayload$Factory;

    new-instance v8, Ljava/util/Date;

    invoke-direct {v8}, Ljava/util/Date;-><init>()V

    const/4 v10, 0x0

    iget-object v6, v0, Lcom/squareup/ui/settings/printerstations/station/TestPrint;->res:Lcom/squareup/util/Res;

    sget v9, Lcom/squareup/settingsapplet/R$string;->printer_stations_test_print_ticket_name:I

    .line 97
    invoke-interface {v6, v9}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 98
    invoke-static {v3}, Lcom/squareup/print/PrintablePaymentOrder;->convertCartItemsToItems(Ljava/util/List;)Ljava/util/List;

    move-result-object v13

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v19, 0x0

    const-string v9, ""

    const-string v11, ""

    const-string v17, "printerStationName"

    move-object/from16 v18, v4

    .line 96
    invoke-virtual/range {v7 .. v19}, Lcom/squareup/print/payload/TicketPayload$Factory;->forOrderTicket(Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZZLcom/squareup/checkout/DiningOption;Ljava/lang/String;Ljava/util/List;Lcom/squareup/print/PrintableRecipient;)Lcom/squareup/print/payload/TicketPayload;

    move-result-object v3

    .line 106
    iget-object v4, v0, Lcom/squareup/ui/settings/printerstations/station/TestPrint;->printSpooler:Lcom/squareup/print/PrintSpooler;

    iget-object v6, v0, Lcom/squareup/ui/settings/printerstations/station/TestPrint;->res:Lcom/squareup/util/Res;

    sget v7, Lcom/squareup/settingsapplet/R$string;->printer_stations_test_print_job_name:I

    .line 107
    invoke-interface {v6, v7}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 106
    invoke-virtual {v4, v2, v3, v6, v5}, Lcom/squareup/print/PrintSpooler;->enqueueForTestPrint(Lcom/squareup/print/HardwarePrinter;Lcom/squareup/print/PrintablePayload;Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    :goto_0
    iget-object v2, v0, Lcom/squareup/ui/settings/printerstations/station/TestPrint;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {v1}, Lcom/squareup/print/PrinterEventKt;->forPrintTest(Lcom/squareup/print/HardwarePrinter$HardwareInfo;)Lcom/squareup/print/PrinterEvent;

    move-result-object v1

    invoke-interface {v2, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method
