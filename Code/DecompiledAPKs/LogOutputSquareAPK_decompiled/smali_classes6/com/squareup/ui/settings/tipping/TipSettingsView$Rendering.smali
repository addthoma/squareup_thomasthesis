.class public final Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;
.super Ljava/lang/Object;
.source "TipSettingsView.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/tipping/TipSettingsView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Rendering"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u001c\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u0089\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0003\u0012\u0006\u0010\u0008\u001a\u00020\u0003\u0012\u0006\u0010\t\u001a\u00020\u0003\u0012\u0006\u0010\n\u001a\u00020\u0003\u0012\u0006\u0010\u000b\u001a\u00020\u0003\u0012!\u0010\u000c\u001a\u001d\u0012\u0013\u0012\u00110\u0000\u00a2\u0006\u000c\u0008\u000e\u0012\u0008\u0008\u000f\u0012\u0004\u0008\u0008(\u0010\u0012\u0004\u0012\u00020\u00110\r\u0012!\u0010\u0012\u001a\u001d\u0012\u0013\u0012\u00110\u0013\u00a2\u0006\u000c\u0008\u000e\u0012\u0008\u0008\u000f\u0012\u0004\u0008\u0008(\u0014\u0012\u0004\u0012\u00020\u00110\r\u00a2\u0006\u0002\u0010\u0015J\t\u0010\"\u001a\u00020\u0003H\u00c6\u0003J\u000f\u0010#\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\u00c6\u0003J\t\u0010$\u001a\u00020\u0003H\u00c6\u0003J\t\u0010%\u001a\u00020\u0003H\u00c6\u0003J\t\u0010&\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\'\u001a\u00020\u0003H\u00c6\u0003J\t\u0010(\u001a\u00020\u0003H\u00c6\u0003J$\u0010)\u001a\u001d\u0012\u0013\u0012\u00110\u0000\u00a2\u0006\u000c\u0008\u000e\u0012\u0008\u0008\u000f\u0012\u0004\u0008\u0008(\u0010\u0012\u0004\u0012\u00020\u00110\rH\u00c6\u0003J$\u0010*\u001a\u001d\u0012\u0013\u0012\u00110\u0013\u00a2\u0006\u000c\u0008\u000e\u0012\u0008\u0008\u000f\u0012\u0004\u0008\u0008(\u0014\u0012\u0004\u0012\u00020\u00110\rH\u00c6\u0003J\u009f\u0001\u0010+\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u000e\u0008\u0002\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00052\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u00032\u0008\u0008\u0002\u0010\t\u001a\u00020\u00032\u0008\u0008\u0002\u0010\n\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u00032#\u0008\u0002\u0010\u000c\u001a\u001d\u0012\u0013\u0012\u00110\u0000\u00a2\u0006\u000c\u0008\u000e\u0012\u0008\u0008\u000f\u0012\u0004\u0008\u0008(\u0010\u0012\u0004\u0012\u00020\u00110\r2#\u0008\u0002\u0010\u0012\u001a\u001d\u0012\u0013\u0012\u00110\u0013\u00a2\u0006\u000c\u0008\u000e\u0012\u0008\u0008\u000f\u0012\u0004\u0008\u0008(\u0014\u0012\u0004\u0012\u00020\u00110\rH\u00c6\u0001J\u0013\u0010,\u001a\u00020\u00032\u0008\u0010-\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010.\u001a\u00020\u0013H\u00d6\u0001J\t\u0010/\u001a\u000200H\u00d6\u0001R\u0011\u0010\u0008\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017R,\u0010\u000c\u001a\u001d\u0012\u0013\u0012\u00110\u0000\u00a2\u0006\u000c\u0008\u000e\u0012\u0008\u0008\u000f\u0012\u0004\u0008\u0008(\u0010\u0012\u0004\u0012\u00020\u00110\r\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019R,\u0010\u0012\u001a\u001d\u0012\u0013\u0012\u00110\u0013\u00a2\u0006\u000c\u0008\u000e\u0012\u0008\u0008\u000f\u0012\u0004\u0008\u0008(\u0014\u0012\u0004\u0012\u00020\u00110\r\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u0019R\u0017\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u001cR\u0011\u0010\t\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001d\u0010\u0017R\u0011\u0010\n\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001e\u0010\u0017R\u0011\u0010\u0007\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001f\u0010\u0017R\u0011\u0010\u000b\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008 \u0010\u0017R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008!\u0010\u0017\u00a8\u00061"
    }
    d2 = {
        "Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;",
        "",
        "tipsEnabled",
        "",
        "percentages",
        "",
        "Lcom/squareup/util/Percentage;",
        "smartTippingEnabled",
        "customAmounts",
        "separateTippingScreen",
        "separateTipsScreenOptionsAvailable",
        "tipPreTax",
        "onChanged",
        "Lkotlin/Function1;",
        "Lkotlin/ParameterName;",
        "name",
        "newRendering",
        "",
        "onCustomTipClicked",
        "",
        "index",
        "(ZLjava/util/List;ZZZZZLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V",
        "getCustomAmounts",
        "()Z",
        "getOnChanged",
        "()Lkotlin/jvm/functions/Function1;",
        "getOnCustomTipClicked",
        "getPercentages",
        "()Ljava/util/List;",
        "getSeparateTippingScreen",
        "getSeparateTipsScreenOptionsAvailable",
        "getSmartTippingEnabled",
        "getTipPreTax",
        "getTipsEnabled",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "component9",
        "copy",
        "equals",
        "other",
        "hashCode",
        "toString",
        "",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final customAmounts:Z

.field private final onChanged:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onCustomTipClicked:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final percentages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation
.end field

.field private final separateTippingScreen:Z

.field private final separateTipsScreenOptionsAvailable:Z

.field private final smartTippingEnabled:Z

.field private final tipPreTax:Z

.field private final tipsEnabled:Z


# direct methods
.method public constructor <init>(ZLjava/util/List;ZZZZZLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List<",
            "Lcom/squareup/util/Percentage;",
            ">;ZZZZZ",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "percentages"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onChanged"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onCustomTipClicked"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->tipsEnabled:Z

    iput-object p2, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->percentages:Ljava/util/List;

    iput-boolean p3, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->smartTippingEnabled:Z

    iput-boolean p4, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->customAmounts:Z

    iput-boolean p5, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->separateTippingScreen:Z

    iput-boolean p6, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->separateTipsScreenOptionsAvailable:Z

    iput-boolean p7, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->tipPreTax:Z

    iput-object p8, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->onChanged:Lkotlin/jvm/functions/Function1;

    iput-object p9, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->onCustomTipClicked:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;ZLjava/util/List;ZZZZZLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;
    .locals 10

    move-object v0, p0

    move/from16 v1, p10

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-boolean v2, v0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->tipsEnabled:Z

    goto :goto_0

    :cond_0
    move v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->percentages:Ljava/util/List;

    goto :goto_1

    :cond_1
    move-object v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-boolean v4, v0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->smartTippingEnabled:Z

    goto :goto_2

    :cond_2
    move v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-boolean v5, v0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->customAmounts:Z

    goto :goto_3

    :cond_3
    move v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-boolean v6, v0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->separateTippingScreen:Z

    goto :goto_4

    :cond_4
    move v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-boolean v7, v0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->separateTipsScreenOptionsAvailable:Z

    goto :goto_5

    :cond_5
    move/from16 v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-boolean v8, v0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->tipPreTax:Z

    goto :goto_6

    :cond_6
    move/from16 v8, p7

    :goto_6
    and-int/lit16 v9, v1, 0x80

    if-eqz v9, :cond_7

    iget-object v9, v0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->onChanged:Lkotlin/jvm/functions/Function1;

    goto :goto_7

    :cond_7
    move-object/from16 v9, p8

    :goto_7
    and-int/lit16 v1, v1, 0x100

    if-eqz v1, :cond_8

    iget-object v1, v0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->onCustomTipClicked:Lkotlin/jvm/functions/Function1;

    goto :goto_8

    :cond_8
    move-object/from16 v1, p9

    :goto_8
    move p1, v2

    move-object p2, v3

    move p3, v4

    move p4, v5

    move p5, v6

    move/from16 p6, v7

    move/from16 p7, v8

    move-object/from16 p8, v9

    move-object/from16 p9, v1

    invoke-virtual/range {p0 .. p9}, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->copy(ZLjava/util/List;ZZZZZLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->tipsEnabled:Z

    return v0
.end method

.method public final component2()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->percentages:Ljava/util/List;

    return-object v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->smartTippingEnabled:Z

    return v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->customAmounts:Z

    return v0
.end method

.method public final component5()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->separateTippingScreen:Z

    return v0
.end method

.method public final component6()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->separateTipsScreenOptionsAvailable:Z

    return v0
.end method

.method public final component7()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->tipPreTax:Z

    return v0
.end method

.method public final component8()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->onChanged:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final component9()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->onCustomTipClicked:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final copy(ZLjava/util/List;ZZZZZLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List<",
            "Lcom/squareup/util/Percentage;",
            ">;ZZZZZ",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;"
        }
    .end annotation

    const-string v0, "percentages"

    move-object v3, p2

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onChanged"

    move-object/from16 v9, p8

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onCustomTipClicked"

    move-object/from16 v10, p9

    invoke-static {v10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;

    move-object v1, v0

    move v2, p1

    move v4, p3

    move v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    invoke-direct/range {v1 .. v10}, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;-><init>(ZLjava/util/List;ZZZZZLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;

    iget-boolean v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->tipsEnabled:Z

    iget-boolean v1, p1, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->tipsEnabled:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->percentages:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->percentages:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->smartTippingEnabled:Z

    iget-boolean v1, p1, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->smartTippingEnabled:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->customAmounts:Z

    iget-boolean v1, p1, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->customAmounts:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->separateTippingScreen:Z

    iget-boolean v1, p1, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->separateTippingScreen:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->separateTipsScreenOptionsAvailable:Z

    iget-boolean v1, p1, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->separateTipsScreenOptionsAvailable:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->tipPreTax:Z

    iget-boolean v1, p1, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->tipPreTax:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->onChanged:Lkotlin/jvm/functions/Function1;

    iget-object v1, p1, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->onChanged:Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->onCustomTipClicked:Lkotlin/jvm/functions/Function1;

    iget-object p1, p1, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->onCustomTipClicked:Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCustomAmounts()Z
    .locals 1

    .line 86
    iget-boolean v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->customAmounts:Z

    return v0
.end method

.method public final getOnChanged()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 90
    iget-object v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->onChanged:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getOnCustomTipClicked()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 91
    iget-object v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->onCustomTipClicked:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getPercentages()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->percentages:Ljava/util/List;

    return-object v0
.end method

.method public final getSeparateTippingScreen()Z
    .locals 1

    .line 87
    iget-boolean v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->separateTippingScreen:Z

    return v0
.end method

.method public final getSeparateTipsScreenOptionsAvailable()Z
    .locals 1

    .line 88
    iget-boolean v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->separateTipsScreenOptionsAvailable:Z

    return v0
.end method

.method public final getSmartTippingEnabled()Z
    .locals 1

    .line 85
    iget-boolean v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->smartTippingEnabled:Z

    return v0
.end method

.method public final getTipPreTax()Z
    .locals 1

    .line 89
    iget-boolean v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->tipPreTax:Z

    return v0
.end method

.method public final getTipsEnabled()Z
    .locals 1

    .line 82
    iget-boolean v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->tipsEnabled:Z

    return v0
.end method

.method public hashCode()I
    .locals 4

    iget-boolean v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->tipsEnabled:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->percentages:Ljava/util/List;

    const/4 v3, 0x0

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->smartTippingEnabled:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->customAmounts:Z

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :cond_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->separateTippingScreen:Z

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :cond_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->separateTipsScreenOptionsAvailable:Z

    if-eqz v2, :cond_5

    const/4 v2, 0x1

    :cond_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->tipPreTax:Z

    if-eqz v2, :cond_6

    goto :goto_1

    :cond_6
    move v1, v2

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->onChanged:Lkotlin/jvm/functions/Function1;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_7
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->onCustomTipClicked:Lkotlin/jvm/functions/Function1;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v3

    :cond_8
    add-int/2addr v0, v3

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Rendering(tipsEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->tipsEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", percentages="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->percentages:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", smartTippingEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->smartTippingEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", customAmounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->customAmounts:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", separateTippingScreen="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->separateTippingScreen:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", separateTipsScreenOptionsAvailable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->separateTipsScreenOptionsAvailable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", tipPreTax="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->tipPreTax:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", onChanged="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->onChanged:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onCustomTipClicked="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->onCustomTipClicked:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
