.class public final Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$bindTestPrintButton$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "HardwarePrinterSelectView.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->bindTestPrintButton(Lcom/squareup/marketfont/MarketButton;Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$TestPrintButtonRow;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006"
    }
    d2 = {
        "com/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$bindTestPrintButton$1",
        "Lcom/squareup/debounce/DebouncedOnClickListener;",
        "doClick",
        "",
        "v",
        "Landroid/view/View;",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $item:Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$TestPrintButtonRow;

.field final synthetic this$0:Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$TestPrintButtonRow;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$TestPrintButtonRow;",
            ")V"
        }
    .end annotation

    .line 178
    iput-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$bindTestPrintButton$1;->this$0:Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;

    iput-object p2, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$bindTestPrintButton$1;->$item:Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$TestPrintButtonRow;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    const-string v0, "v"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 180
    iget-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$bindTestPrintButton$1;->$item:Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$TestPrintButtonRow;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$TestPrintButtonRow;->isEnabled()Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 183
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$bindTestPrintButton$1;->this$0:Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->getPresenter$settings_applet_release()Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->onTestPrintClicked$settings_applet_release()V

    return-void
.end method
