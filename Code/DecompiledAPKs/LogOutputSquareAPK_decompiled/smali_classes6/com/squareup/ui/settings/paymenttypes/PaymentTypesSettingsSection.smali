.class public Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection;
.super Lcom/squareup/applet/AppletSection;
.source "PaymentTypesSettingsSection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection$Access;,
        Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection$ListEntry;,
        Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection$PaymentTypesSettingsCheckoutListEntry;
    }
.end annotation


# static fields
.field public static TITLE_ID:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    sget v0, Lcom/squareup/settingsapplet/R$string;->payment_types_title:I

    sput v0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection;->TITLE_ID:I

    return-void
.end method

.method public constructor <init>(Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 27
    new-instance v0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection$Access;

    invoke-direct {v0, p1}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection$Access;-><init>(Lcom/squareup/settings/server/Features;)V

    invoke-direct {p0, v0}, Lcom/squareup/applet/AppletSection;-><init>(Lcom/squareup/applet/SectionAccess;)V

    return-void
.end method


# virtual methods
.method public getInitialScreen()Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    .line 31
    sget-object v0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen;->INSTANCE:Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen;

    return-object v0
.end method
