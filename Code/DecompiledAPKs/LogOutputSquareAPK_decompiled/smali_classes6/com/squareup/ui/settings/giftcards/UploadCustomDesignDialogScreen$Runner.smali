.class public interface abstract Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$Runner;
.super Ljava/lang/Object;
.source "UploadCustomDesignDialogScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u000e\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H&\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$Runner;",
        "",
        "uploadCustomDesignDialogScreenData",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$UploadCustomDesignDialogScreenData;",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract uploadCustomDesignDialogScreenData()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$UploadCustomDesignDialogScreenData;",
            ">;"
        }
    .end annotation
.end method
