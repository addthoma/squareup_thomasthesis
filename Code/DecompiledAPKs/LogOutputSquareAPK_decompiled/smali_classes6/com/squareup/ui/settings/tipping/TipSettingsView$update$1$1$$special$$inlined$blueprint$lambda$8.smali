.class final Lcom/squareup/ui/settings/tipping/TipSettingsView$update$1$1$$special$$inlined$blueprint$lambda$8;
.super Lkotlin/jvm/internal/Lambda;
.source "TipSettingsView.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/tipping/TipSettingsView$update$1$1;->invoke(Lcom/squareup/mosaic/components/VerticalScrollUiModel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Boolean;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0005\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004\u00a8\u0006\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "",
        "invoke",
        "com/squareup/ui/settings/tipping/TipSettingsView$update$1$1$1$2$4$1$1",
        "com/squareup/ui/settings/tipping/TipSettingsView$update$1$1$$special$$inlined$marinRow$lambda$8",
        "com/squareup/ui/settings/tipping/TipSettingsView$update$1$1$$special$$inlined$section$lambda$8",
        "com/squareup/ui/settings/tipping/TipSettingsView$update$1$1$$special$$inlined$vertical$lambda$8"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $this_blueprint$inlined:Lcom/squareup/blueprint/mosaic/BlueprintUiModel;

.field final synthetic this$0:Lcom/squareup/ui/settings/tipping/TipSettingsView$update$1$1;


# direct methods
.method constructor <init>(Lcom/squareup/blueprint/mosaic/BlueprintUiModel;Lcom/squareup/ui/settings/tipping/TipSettingsView$update$1$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$update$1$1$$special$$inlined$blueprint$lambda$8;->$this_blueprint$inlined:Lcom/squareup/blueprint/mosaic/BlueprintUiModel;

    iput-object p2, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$update$1$1$$special$$inlined$blueprint$lambda$8;->this$0:Lcom/squareup/ui/settings/tipping/TipSettingsView$update$1$1;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 76
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/tipping/TipSettingsView$update$1$1$$special$$inlined$blueprint$lambda$8;->invoke(Z)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Z)V
    .locals 13

    if-eqz p1, :cond_0

    .line 265
    iget-object p1, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$update$1$1$$special$$inlined$blueprint$lambda$8;->this$0:Lcom/squareup/ui/settings/tipping/TipSettingsView$update$1$1;

    iget-object p1, p1, Lcom/squareup/ui/settings/tipping/TipSettingsView$update$1$1;->this$0:Lcom/squareup/ui/settings/tipping/TipSettingsView$update$1;

    iget-object p1, p1, Lcom/squareup/ui/settings/tipping/TipSettingsView$update$1;->$rendering:Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->getOnChanged()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$update$1$1$$special$$inlined$blueprint$lambda$8;->this$0:Lcom/squareup/ui/settings/tipping/TipSettingsView$update$1$1;

    iget-object v0, v0, Lcom/squareup/ui/settings/tipping/TipSettingsView$update$1$1;->this$0:Lcom/squareup/ui/settings/tipping/TipSettingsView$update$1;

    iget-object v1, v0, Lcom/squareup/ui/settings/tipping/TipSettingsView$update$1;->$rendering:Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0x1bf

    const/4 v12, 0x0

    invoke-static/range {v1 .. v12}, Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;->copy$default(Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;ZLjava/util/List;ZZZZZLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;

    move-result-object v0

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method
