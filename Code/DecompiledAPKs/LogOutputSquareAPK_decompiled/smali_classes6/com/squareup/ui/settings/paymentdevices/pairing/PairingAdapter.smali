.class Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "PairingAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter$ItemViewHolder;,
        Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter$HeaderViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;",
        ">;"
    }
.end annotation


# static fields
.field private static final COMPACT_SIZE:F = 0.6666667f

.field private static final DELAY:I = 0x190

.field private static final DURATION:I = 0x320

.field private static final FOOTER_ROW:I = 0x2

.field private static final HEADER_ROW:I = 0x0

.field private static final ITEM_ROW:I = 0x1


# instance fields
.field private final cardReaderMessages:Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;

.field private final pairingRecyclerView:Landroid/view/View;

.field private final presenter:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;

.field private showMultipleReaders:Z


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;Landroid/view/View;)V
    .locals 0

    .line 40
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter;->presenter:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;

    .line 42
    iput-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter;->cardReaderMessages:Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;

    .line 43
    iput-object p3, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter;->pairingRecyclerView:Landroid/view/View;

    .line 44
    invoke-virtual {p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->shouldManuallySelectReader()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter;->showMultipleReaders:Z

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter;)Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter;->presenter:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;

    return-object p0
.end method

.method private createFooterViewHolder(Landroid/view/ViewGroup;)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 5

    .line 167
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/squareup/cardreader/ui/R$layout;->pairing_help_row_view:I

    const/4 v2, 0x0

    .line 168
    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 169
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 170
    sget v1, Lcom/squareup/cardreader/ui/R$id;->pairing_screen_help:I

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/widgets/MessageView;

    .line 171
    new-instance v2, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-direct {v2, v0}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    sget v3, Lcom/squareup/cardreader/ui/R$string;->pairing_screen_help_verbose:I

    const-string v4, "having_trouble"

    .line 172
    invoke-virtual {v2, v3, v4}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v2

    sget v3, Lcom/squareup/cardreader/ui/R$string;->pairing_screen_help:I

    .line 173
    invoke-virtual {v2, v3}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v2

    new-instance v3, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter$2;

    .line 174
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v4, Lcom/squareup/ui/LinkSpan;->DEFAULT_COLOR_ID:I

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-direct {v3, p0, v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter$2;-><init>(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter;I)V

    invoke-virtual {v2, v3}, Lcom/squareup/ui/LinkSpan$Builder;->clickableSpan(Lcom/squareup/ui/LinkSpan;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    .line 179
    invoke-virtual {v0}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object v0

    .line 180
    invoke-virtual {v1, v0}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 181
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter$3;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter$3;-><init>(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter;Landroid/view/View;)V

    return-object v0
.end method

.method private createHeaderViewHolder(Landroid/view/ViewGroup;)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 4

    .line 127
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 128
    sget v1, Lcom/squareup/cardreader/ui/R$layout;->pairing_header_view:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 129
    sget v1, Lcom/squareup/cardreader/ui/R$id;->pairing_screen_content_container:I

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 131
    sget-object v2, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter$4;->$SwitchMap$com$squareup$protos$client$bills$CardData$ReaderType:[I

    iget-object v3, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter;->presenter:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;

    invoke-virtual {v3}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/protos/client/bills/CardData$ReaderType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 133
    sget v2, Lcom/squareup/cardreader/ui/R$layout;->r12_pairing_header_content_view:I

    .line 138
    invoke-virtual {v0, v2, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 141
    sget v0, Lcom/squareup/cardreader/ui/R$id;->pairing_screen_content_help:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter$1;-><init>(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 149
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter$HeaderViewHolder;

    invoke-direct {v0, p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter$HeaderViewHolder;-><init>(Landroid/view/View;)V

    .line 150
    new-instance p1, Lcom/squareup/ui/settings/paymentdevices/pairing/-$$Lambda$PairingAdapter$KDi1IapvuyLyfTaSHkltTyQfk54;

    invoke-direct {p1, p0, v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/-$$Lambda$PairingAdapter$KDi1IapvuyLyfTaSHkltTyQfk54;-><init>(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter;Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter$HeaderViewHolder;)V

    .line 153
    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter;->pairingRecyclerView:Landroid/view/View;

    invoke-static {v1, p1}, Lcom/squareup/util/Views;->waitForHeightMeasure(Landroid/view/View;Lcom/squareup/util/OnMeasuredCallback;)V

    return-object v0

    .line 136
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown ReaderType: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter;->presenter:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private createItemViewHolder(Landroid/view/ViewGroup;)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 1

    .line 159
    invoke-static {p1}, Lcom/squareup/ui/account/view/SmartLineRow;->inflateForListView(Landroid/view/ViewGroup;)Lcom/squareup/ui/account/view/SmartLineRow;

    move-result-object p1

    .line 160
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/pairing/-$$Lambda$PairingAdapter$8ZSHNmT-8yHpT-fjHCl99KXzgfw;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/-$$Lambda$PairingAdapter$8ZSHNmT-8yHpT-fjHCl99KXzgfw;-><init>(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter;)V

    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 162
    invoke-static {p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter;->updatePadding(Landroid/view/View;)V

    .line 163
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter$ItemViewHolder;

    invoke-direct {v0, p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter$ItemViewHolder;-><init>(Lcom/squareup/ui/account/view/SmartLineRow;)V

    return-object v0
.end method

.method private getReaderCount()I
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter;->presenter:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->getWirelessConnections()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method private static updatePadding(Landroid/view/View;)V
    .locals 2

    .line 188
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/marin/R$dimen;->marin_gutter:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    const/4 v1, 0x0

    .line 189
    invoke-virtual {p0, v0, v1, v0, v1}, Landroid/view/View;->setPadding(IIII)V

    return-void
.end method


# virtual methods
.method public getItemCount()I
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter;->presenter:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->shouldManuallySelectReader()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51
    invoke-direct {p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter;->getReaderCount()I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0
.end method

.method public getItemViewType(I)I
    .locals 2

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return p1

    .line 101
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter;->getItemCount()I

    move-result v0

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    if-ne p1, v0, :cond_1

    const/4 p1, 0x2

    return p1

    :cond_1
    return v1
.end method

.method protected getWirelessConnectionForViewPosition(I)Lcom/squareup/cardreader/WirelessConnection;
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter;->presenter:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->getWirelessConnections()Ljava/util/List;

    move-result-object v0

    add-int/lit8 p1, p1, -0x1

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cardreader/WirelessConnection;

    return-object p1
.end method

.method public synthetic lambda$createHeaderViewHolder$1$PairingAdapter(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter$HeaderViewHolder;Landroid/view/View;II)V
    .locals 0

    .line 151
    iget-boolean p2, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter;->showMultipleReaders:Z

    if-eqz p2, :cond_0

    int-to-float p2, p4

    const p3, 0x3f2aaaab

    mul-float p2, p2, p3

    float-to-int p4, p2

    :cond_0
    invoke-virtual {p1, p4}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter$HeaderViewHolder;->setContentMinimumHeight(I)V

    return-void
.end method

.method public synthetic lambda$createItemViewHolder$2$PairingAdapter(Landroid/view/View;)V
    .locals 1

    .line 161
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter;->presenter:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cardreader/ble/BleScanResult;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->onWirelessConnectionSelected(Lcom/squareup/cardreader/WirelessConnection;)V

    return-void
.end method

.method public synthetic lambda$updateHeader$0$PairingAdapter(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter$HeaderViewHolder;Landroid/animation/ValueAnimator;)V
    .locals 1

    .line 69
    invoke-virtual {p2}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Float;

    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result p2

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter;->pairingRecyclerView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    int-to-float v0, v0

    mul-float p2, p2, v0

    float-to-int p2, p2

    .line 72
    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter$HeaderViewHolder;->setContentMinimumHeight(I)V

    return-void
.end method

.method public onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 2

    .line 108
    invoke-virtual {p0, p2}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter;->getItemViewType(I)I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 p1, 0x2

    if-ne v0, p1, :cond_0

    goto :goto_0

    .line 121
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown ViewType: "

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 114
    :cond_1
    check-cast p1, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter$ItemViewHolder;

    invoke-virtual {p0, p2}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter;->getWirelessConnectionForViewPosition(I)Lcom/squareup/cardreader/WirelessConnection;

    move-result-object p2

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter;->cardReaderMessages:Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;

    invoke-virtual {p1, p2, v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter$ItemViewHolder;->bind(Lcom/squareup/cardreader/WirelessConnection;Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;)V

    goto :goto_0

    .line 111
    :cond_2
    check-cast p1, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter$HeaderViewHolder;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter;->updateHeader(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter$HeaderViewHolder;)V

    :goto_0
    return-void
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 2

    if-eqz p2, :cond_2

    const/4 v0, 0x1

    if-eq p2, v0, :cond_1

    const/4 v0, 0x2

    if-ne p2, v0, :cond_0

    .line 91
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter;->createFooterViewHolder(Landroid/view/ViewGroup;)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    move-result-object p1

    return-object p1

    .line 93
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown ViewType: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 89
    :cond_1
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter;->createItemViewHolder(Landroid/view/ViewGroup;)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    move-result-object p1

    return-object p1

    .line 87
    :cond_2
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter;->createHeaderViewHolder(Landroid/view/ViewGroup;)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    move-result-object p1

    return-object p1
.end method

.method protected updateHeader(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter$HeaderViewHolder;)V
    .locals 3

    .line 61
    iget-boolean v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter;->showMultipleReaders:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter;->presenter:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->shouldManuallySelectReader()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 62
    iput-boolean v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter;->showMultipleReaders:Z

    const/4 v0, 0x2

    new-array v0, v0, [F

    .line 65
    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    const-wide/16 v1, 0x190

    .line 66
    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    const-wide/16 v1, 0x320

    .line 67
    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 68
    new-instance v1, Lcom/squareup/ui/settings/paymentdevices/pairing/-$$Lambda$PairingAdapter$Pufn_wF4pWZEZCR9UDSpKRR1tMo;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/-$$Lambda$PairingAdapter$Pufn_wF4pWZEZCR9UDSpKRR1tMo;-><init>(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter;Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter$HeaderViewHolder;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 74
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 77
    :cond_0
    iget-boolean v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter;->showMultipleReaders:Z

    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter$HeaderViewHolder;->setShowMultipleReaders(Z)V

    return-void

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3f2aaaab
    .end array-data
.end method
