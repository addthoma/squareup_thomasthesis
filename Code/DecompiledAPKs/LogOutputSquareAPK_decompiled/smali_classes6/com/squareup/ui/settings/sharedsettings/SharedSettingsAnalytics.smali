.class public Lcom/squareup/ui/settings/sharedsettings/SharedSettingsAnalytics;
.super Ljava/lang/Object;
.source "SharedSettingsAnalytics.java"


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/Analytics;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method


# virtual methods
.method public dashboardLinkClicked()V
    .locals 2

    .line 16
    iget-object v0, p0, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SHARED_SETTINGS_DASHBOARD_LINK:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    return-void
.end method
