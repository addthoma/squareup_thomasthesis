.class public final Lcom/squareup/ui/settings/bankaccount/LinkBankAccountBootstrapScreen;
.super Lcom/squareup/container/BootstrapTreeKey;
.source "LinkBankAccountBootstrapScreen.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\u0018\u00002\u00020\u0001B!\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0016J\u0008\u0010\r\u001a\u00020\u000eH\u0016R\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/ui/settings/bankaccount/LinkBankAccountBootstrapScreen;",
        "Lcom/squareup/container/BootstrapTreeKey;",
        "requiresPassword",
        "",
        "primaryInstitutionNumber",
        "",
        "verificationState",
        "Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;",
        "(ZLjava/lang/String;Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;)V",
        "doRegister",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "getParentKey",
        "",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final primaryInstitutionNumber:Ljava/lang/String;

.field private final requiresPassword:Z

.field private final verificationState:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;


# direct methods
.method public constructor <init>(ZLjava/lang/String;Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;)V
    .locals 0

    .line 14
    invoke-direct {p0}, Lcom/squareup/container/BootstrapTreeKey;-><init>()V

    iput-boolean p1, p0, Lcom/squareup/ui/settings/bankaccount/LinkBankAccountBootstrapScreen;->requiresPassword:Z

    iput-object p2, p0, Lcom/squareup/ui/settings/bankaccount/LinkBankAccountBootstrapScreen;->primaryInstitutionNumber:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/ui/settings/bankaccount/LinkBankAccountBootstrapScreen;->verificationState:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    return-void
.end method


# virtual methods
.method public doRegister(Lmortar/MortarScope;)V
    .locals 4

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    new-instance v0, Lcom/squareup/banklinking/LinkBankAccountProps$SkipFetchingBankAccount;

    iget-boolean v1, p0, Lcom/squareup/ui/settings/bankaccount/LinkBankAccountBootstrapScreen;->requiresPassword:Z

    iget-object v2, p0, Lcom/squareup/ui/settings/bankaccount/LinkBankAccountBootstrapScreen;->primaryInstitutionNumber:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/ui/settings/bankaccount/LinkBankAccountBootstrapScreen;->verificationState:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/banklinking/LinkBankAccountProps$SkipFetchingBankAccount;-><init>(ZLjava/lang/String;Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;)V

    .line 21
    sget-object v1, Lcom/squareup/banklinking/LinkBankAccountWorkflowRunner;->Companion:Lcom/squareup/banklinking/LinkBankAccountWorkflowRunner$Companion;

    check-cast v0, Lcom/squareup/banklinking/LinkBankAccountProps;

    invoke-virtual {v1, p1, v0}, Lcom/squareup/banklinking/LinkBankAccountWorkflowRunner$Companion;->startNewWorkflow(Lmortar/MortarScope;Lcom/squareup/banklinking/LinkBankAccountProps;)V

    return-void
.end method

.method public getParentKey()Ljava/lang/Object;
    .locals 2

    .line 16
    new-instance v0, Lcom/squareup/banklinking/LinkBankAccountScope;

    sget-object v1, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScope;->INSTANCE:Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScope;

    check-cast v1, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-direct {v0, v1}, Lcom/squareup/banklinking/LinkBankAccountScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-object v0
.end method
