.class final Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$updateMaxAmount$1;
.super Lkotlin/jvm/internal/Lambda;
.source "GiftCardsSettingsScopeRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->updateMaxAmount(Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;Lcom/squareup/protos/common/Money;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;",
        "Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u0006\u0010\u0003\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;",
        "kotlin.jvm.PlatformType",
        "builder",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $newMax:Lcom/squareup/protos/common/Money;


# direct methods
.method constructor <init>(Lcom/squareup/protos/common/Money;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$updateMaxAmount$1;->$newMax:Lcom/squareup/protos/common/Money;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;)Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;
    .locals 1

    const-string v0, "builder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 198
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$updateMaxAmount$1;->$newMax:Lcom/squareup/protos/common/Money;

    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;->max_load_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;

    move-result-object p1

    const-string v0, "builder.max_load_amount(newMax)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 47
    check-cast p1, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$updateMaxAmount$1;->invoke(Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;)Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration$Builder;

    move-result-object p1

    return-object p1
.end method
