.class public Lcom/squareup/ui/settings/opentickets/ticketgroups/DiscardChangePopupScreen$Factory;
.super Ljava/lang/Object;
.source "DiscardChangePopupScreen.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/opentickets/ticketgroups/DiscardChangePopupScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$create$0(Lflow/Flow;Landroid/content/DialogInterface;I)V
    .locals 1

    const/4 p1, 0x2

    new-array p1, p1, [Ljava/lang/Class;

    .line 31
    const-class p2, Lcom/squareup/ui/settings/opentickets/ticketgroups/DiscardChangePopupScreen;

    const/4 v0, 0x0

    aput-object p2, p1, v0

    const-class p2, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScreen;

    const/4 v0, 0x1

    aput-object p2, p1, v0

    invoke-static {p0, p1}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    return-void
.end method

.method static synthetic lambda$create$1(Landroid/content/DialogInterface;I)V
    .locals 0

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 24
    invoke-static {p1}, Lflow/Flow;->get(Landroid/content/Context;)Lflow/Flow;

    move-result-object v0

    .line 25
    new-instance v1, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v1, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget p1, Lcom/squareup/widgets/pos/R$string;->edit_item_confirm_discard_changes_dialog_title:I

    .line 26
    invoke-virtual {v1, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v1, Lcom/squareup/widgets/pos/R$string;->edit_item_confirm_discard_changes_dialog_message:I

    .line 28
    invoke-virtual {p1, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v1, Lcom/squareup/common/strings/R$string;->confirmation_popup_discard:I

    new-instance v2, Lcom/squareup/ui/settings/opentickets/ticketgroups/-$$Lambda$DiscardChangePopupScreen$Factory$AEfaqYZO-uQslwEWvDUv6hEKNTA;

    invoke-direct {v2, v0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/-$$Lambda$DiscardChangePopupScreen$Factory$AEfaqYZO-uQslwEWvDUv6hEKNTA;-><init>(Lflow/Flow;)V

    .line 30
    invoke-virtual {p1, v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/widgets/pos/R$string;->confirmation_popup_resume:I

    sget-object v1, Lcom/squareup/ui/settings/opentickets/ticketgroups/-$$Lambda$DiscardChangePopupScreen$Factory$o6lYna-T5jOhWjosKgl57TG-oBM;->INSTANCE:Lcom/squareup/ui/settings/opentickets/ticketgroups/-$$Lambda$DiscardChangePopupScreen$Factory$o6lYna-T5jOhWjosKgl57TG-oBM;

    .line 33
    invoke-virtual {p1, v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    const/4 v0, 0x1

    .line 36
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setCancelable(Z)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 37
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 25
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
