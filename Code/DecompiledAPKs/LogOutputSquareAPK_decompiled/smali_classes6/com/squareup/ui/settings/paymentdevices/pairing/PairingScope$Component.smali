.class public interface abstract Lcom/squareup/ui/settings/paymentdevices/pairing/PairingScope$Component;
.super Ljava/lang/Object;
.source "PairingScope.java"


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/ui/settings/paymentdevices/pairing/PairingScope$Module;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/paymentdevices/pairing/PairingScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract pairingHelp()Lcom/squareup/ui/settings/paymentdevices/pairing/PairingHelpScreen$Component;
.end method

.method public abstract r12PairingScreen()Lcom/squareup/ui/settings/paymentdevices/pairing/R12PairingScreen$Component;
.end method
