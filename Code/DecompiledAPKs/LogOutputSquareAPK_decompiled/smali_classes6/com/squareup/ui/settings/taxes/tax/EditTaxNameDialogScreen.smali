.class public Lcom/squareup/ui/settings/taxes/tax/EditTaxNameDialogScreen;
.super Lcom/squareup/ui/settings/taxes/tax/InTaxScope;
.source "EditTaxNameDialogScreen.java"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/settings/taxes/tax/EditTaxNameDialogScreen$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/taxes/tax/EditTaxNameDialogScreen$Factory;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/taxes/tax/EditTaxNameDialogScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final editTextDialogFactory:Lcom/squareup/register/widgets/EditTextDialogFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 39
    sget-object v0, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$EditTaxNameDialogScreen$TzNJQAzRjpIgU3D5SBACqknAOAc;->INSTANCE:Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$EditTaxNameDialogScreen$TzNJQAzRjpIgU3D5SBACqknAOAc;

    .line 40
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/taxes/tax/EditTaxNameDialogScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Lcom/squareup/register/widgets/EditTextDialogFactory;)V
    .locals 0

    .line 20
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/taxes/tax/InTaxScope;-><init>(Ljava/lang/String;)V

    .line 21
    iput-object p2, p0, Lcom/squareup/ui/settings/taxes/tax/EditTaxNameDialogScreen;->editTextDialogFactory:Lcom/squareup/register/widgets/EditTextDialogFactory;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/settings/taxes/tax/EditTaxNameDialogScreen;)Lcom/squareup/register/widgets/EditTextDialogFactory;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/squareup/ui/settings/taxes/tax/EditTaxNameDialogScreen;->editTextDialogFactory:Lcom/squareup/register/widgets/EditTextDialogFactory;

    return-object p0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/settings/taxes/tax/EditTaxNameDialogScreen;
    .locals 2

    .line 41
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 42
    const-class v1, Lcom/squareup/register/widgets/EditTextDialogFactory;

    .line 43
    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/register/widgets/EditTextDialogFactory;

    .line 44
    new-instance v1, Lcom/squareup/ui/settings/taxes/tax/EditTaxNameDialogScreen;

    invoke-direct {v1, v0, p0}, Lcom/squareup/ui/settings/taxes/tax/EditTaxNameDialogScreen;-><init>(Ljava/lang/String;Lcom/squareup/register/widgets/EditTextDialogFactory;)V

    return-object v1
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/EditTaxNameDialogScreen;->cogsTaxId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/EditTaxNameDialogScreen;->editTextDialogFactory:Lcom/squareup/register/widgets/EditTextDialogFactory;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
