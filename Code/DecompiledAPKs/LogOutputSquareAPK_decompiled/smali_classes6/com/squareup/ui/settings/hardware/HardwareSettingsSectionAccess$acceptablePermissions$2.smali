.class final Lcom/squareup/ui/settings/hardware/HardwareSettingsSectionAccess$acceptablePermissions$2;
.super Lkotlin/jvm/internal/Lambda;
.source "HardwareSettingsSectionAccess.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/hardware/HardwareSettingsSectionAccess;-><init>(Lcom/squareup/settings/server/Features;[Lcom/squareup/permissions/Permission;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Ljava/util/Set<",
        "+",
        "Lcom/squareup/permissions/Permission;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/permissions/Permission;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $alternatePermissions:[Lcom/squareup/permissions/Permission;

.field final synthetic $features:Lcom/squareup/settings/server/Features;


# direct methods
.method constructor <init>(Lcom/squareup/settings/server/Features;[Lcom/squareup/permissions/Permission;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/settings/hardware/HardwareSettingsSectionAccess$acceptablePermissions$2;->$features:Lcom/squareup/settings/server/Features;

    iput-object p2, p0, Lcom/squareup/ui/settings/hardware/HardwareSettingsSectionAccess$acceptablePermissions$2;->$alternatePermissions:[Lcom/squareup/permissions/Permission;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ui/settings/hardware/HardwareSettingsSectionAccess$acceptablePermissions$2;->invoke()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final invoke()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;"
        }
    .end annotation

    .line 22
    iget-object v0, p0, Lcom/squareup/ui/settings/hardware/HardwareSettingsSectionAccess$acceptablePermissions$2;->$features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_SETTINGS_GRANULAR_PERMISSIONS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 23
    sget-object v0, Lcom/squareup/permissions/Permission;->MANAGE_HARDWARE_SETTINGS:Lcom/squareup/permissions/Permission;

    invoke-static {v0}, Lkotlin/collections/SetsKt;->setOf(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    goto :goto_0

    .line 25
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/hardware/HardwareSettingsSectionAccess$acceptablePermissions$2;->$alternatePermissions:[Lcom/squareup/permissions/Permission;

    invoke-static {v0}, Lkotlin/collections/ArraysKt;->toSet([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    :goto_0
    return-object v0
.end method
