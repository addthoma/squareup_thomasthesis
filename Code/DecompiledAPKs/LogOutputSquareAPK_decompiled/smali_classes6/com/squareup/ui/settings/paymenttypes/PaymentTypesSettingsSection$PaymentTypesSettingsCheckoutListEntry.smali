.class public Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection$PaymentTypesSettingsCheckoutListEntry;
.super Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection$ListEntry;
.source "PaymentTypesSettingsSection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PaymentTypesSettingsCheckoutListEntry"
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/tenderpayment/TenderSettingsManager;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/tenderpayment/InvoiceTenderSetting;)V
    .locals 8
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 40
    sget-object v7, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;->CHECKOUT_OPTIONS:Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection$ListEntry;-><init>(Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/tenderpayment/TenderSettingsManager;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/tenderpayment/InvoiceTenderSetting;Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$SettingsAppletGrouping;)V

    return-void
.end method
