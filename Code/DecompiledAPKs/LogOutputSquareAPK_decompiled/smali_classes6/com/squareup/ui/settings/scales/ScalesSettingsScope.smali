.class public final Lcom/squareup/ui/settings/scales/ScalesSettingsScope;
.super Lcom/squareup/ui/settings/InSettingsAppletScope;
.source "ScalesSettingsScope.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/scales/ScalesSettingsScope$Creator;,
        Lcom/squareup/ui/settings/scales/ScalesSettingsScope$ParentComponent;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nScalesSettingsScope.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ScalesSettingsScope.kt\ncom/squareup/ui/settings/scales/ScalesSettingsScope\n+ 2 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,24:1\n35#2:25\n*E\n*S KotlinDebug\n*F\n+ 1 ScalesSettingsScope.kt\ncom/squareup/ui/settings/scales/ScalesSettingsScope\n*L\n13#1:25\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u00c7\u0002\u0018\u00002\u00020\u0001:\u0001\u000eB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\t\u0010\u0007\u001a\u00020\u0008H\u00d6\u0001J\u0019\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u0008H\u00d6\u0001\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/ui/settings/scales/ScalesSettingsScope;",
        "Lcom/squareup/ui/settings/InSettingsAppletScope;",
        "()V",
        "buildScope",
        "Lmortar/MortarScope$Builder;",
        "parentScope",
        "Lmortar/MortarScope;",
        "describeContents",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "ParentComponent",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field public static final INSTANCE:Lcom/squareup/ui/settings/scales/ScalesSettingsScope;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 9
    new-instance v0, Lcom/squareup/ui/settings/scales/ScalesSettingsScope;

    invoke-direct {v0}, Lcom/squareup/ui/settings/scales/ScalesSettingsScope;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/scales/ScalesSettingsScope;->INSTANCE:Lcom/squareup/ui/settings/scales/ScalesSettingsScope;

    new-instance v0, Lcom/squareup/ui/settings/scales/ScalesSettingsScope$Creator;

    invoke-direct {v0}, Lcom/squareup/ui/settings/scales/ScalesSettingsScope$Creator;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/scales/ScalesSettingsScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Lcom/squareup/ui/settings/InSettingsAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;
    .locals 2

    const-string v0, "parentScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-super {p0, p1}, Lcom/squareup/ui/settings/InSettingsAppletScope;->buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;

    move-result-object v0

    .line 25
    const-class v1, Lcom/squareup/ui/settings/scales/ScalesSettingsScope$ParentComponent;

    invoke-static {p1, v1}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/scales/ScalesSettingsScope$ParentComponent;

    .line 15
    invoke-interface {p1}, Lcom/squareup/ui/settings/scales/ScalesSettingsScope$ParentComponent;->scalesWorkflowRunner()Lcom/squareup/ui/settings/scales/ScalesWorkflowRunner;

    move-result-object p1

    const-string v1, "this"

    .line 16
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/scales/ScalesWorkflowRunner;->registerServices(Lmortar/MortarScope$Builder;)V

    const-string p1, "super.buildScope(parentS\u2026rServices(this)\n        }"

    .line 13
    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
