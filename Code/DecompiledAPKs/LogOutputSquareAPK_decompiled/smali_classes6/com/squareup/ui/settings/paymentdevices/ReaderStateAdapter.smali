.class public Lcom/squareup/ui/settings/paymentdevices/ReaderStateAdapter;
.super Landroid/widget/BaseAdapter;
.source "ReaderStateAdapter.java"


# instance fields
.field private final cardReaderMessages:Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;

.field private final readerStateList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/settings/paymentdevices/ReaderState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;)V
    .locals 0

    .line 20
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/ReaderStateAdapter;->cardReaderMessages:Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;

    .line 22
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/ReaderStateAdapter;->readerStateList:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/ReaderStateAdapter;->readerStateList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/ReaderStateAdapter;->readerStateList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .line 44
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    if-nez p2, :cond_0

    .line 47
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    sget v0, Lcom/squareup/cardreader/ui/R$layout;->card_reader_status_row:I

    const/4 v1, 0x0

    invoke-virtual {p2, v0, p3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 51
    :cond_0
    move-object p3, p2

    check-cast p3, Lcom/squareup/ui/widgets/CardReaderRow;

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/ReaderStateAdapter;->readerStateList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/ReaderStateAdapter;->cardReaderMessages:Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;->getCardReaderRowName(Lcom/squareup/ui/settings/paymentdevices/ReaderState;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p3, v0}, Lcom/squareup/ui/widgets/CardReaderRow;->setName(Ljava/lang/CharSequence;)V

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/ReaderStateAdapter;->cardReaderMessages:Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;

    iget-object v1, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->type:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;->getCardReaderStatusDescription(Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p3, v0}, Lcom/squareup/ui/widgets/CardReaderRow;->setDescription(Ljava/lang/CharSequence;)V

    .line 56
    sget-object v0, Lcom/squareup/ui/settings/paymentdevices/ReaderStateAdapter$1;->$SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Level:[I

    iget-object p1, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->type:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    iget-object p1, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->level:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_1

    const/4 p1, 0x0

    .line 64
    invoke-virtual {p3, p1}, Lcom/squareup/ui/widgets/CardReaderRow;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    goto :goto_0

    .line 60
    :cond_1
    sget-object p1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->WARNING_SMALL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p3, p1}, Lcom/squareup/ui/widgets/CardReaderRow;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 61
    sget p1, Lcom/squareup/marin/R$color;->marin_orange:I

    invoke-virtual {p3, p1}, Lcom/squareup/ui/widgets/CardReaderRow;->setGlyphColorRes(I)V

    :goto_0
    return-object p2
.end method

.method updateReaderList(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/settings/paymentdevices/ReaderState;",
            ">;)V"
        }
    .end annotation

    .line 26
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/ReaderStateAdapter;->readerStateList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 27
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/ReaderStateAdapter;->readerStateList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    return-void
.end method
