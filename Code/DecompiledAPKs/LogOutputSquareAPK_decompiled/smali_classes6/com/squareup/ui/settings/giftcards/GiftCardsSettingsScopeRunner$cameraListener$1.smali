.class public final Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$cameraListener$1;
.super Ljava/lang/Object;
.source "GiftCardsSettingsScopeRunner.kt"

# interfaces
.implements Lcom/squareup/camerahelper/CameraHelper$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;-><init>(Lflow/Flow;Lcom/squareup/giftcard/GiftCardServiceHelper;Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsHelper;Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;Lio/reactivex/Scheduler;Lcom/squareup/util/Device;Lcom/squareup/camerahelper/CameraHelper;Lcom/squareup/register/widgets/GlassSpinner;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0019\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H\u0016J\u0012\u0010\u0004\u001a\u00020\u00032\u0008\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0016\u00a8\u0006\u0007"
    }
    d2 = {
        "com/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$cameraListener$1",
        "Lcom/squareup/camerahelper/CameraHelper$Listener;",
        "onImageCanceled",
        "",
        "onImagePicked",
        "image",
        "Landroid/net/Uri;",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 76
    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$cameraListener$1;->this$0:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onImageCanceled()V
    .locals 0

    return-void
.end method

.method public onImagePicked(Landroid/net/Uri;)V
    .locals 1

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$cameraListener$1;->this$0:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->setCustomImageUri(Landroid/net/Uri;)V

    .line 79
    iget-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$cameraListener$1;->this$0:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->getCustomImageUri()Landroid/net/Uri;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 80
    iget-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$cameraListener$1;->this$0:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;

    invoke-static {p1}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->access$getFlow$p(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;)Lflow/Flow;

    move-result-object p1

    sget-object v0, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen;->Companion:Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen$Companion;->getINSTANCE()Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen;

    move-result-object v0

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method
