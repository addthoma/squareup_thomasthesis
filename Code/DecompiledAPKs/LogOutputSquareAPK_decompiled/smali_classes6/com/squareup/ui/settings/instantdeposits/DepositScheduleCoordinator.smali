.class public final Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "DepositScheduleCoordinator.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00a6\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\u0008\u0006\n\u0002\u0010\r\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020!H\u0016J\u0010\u0010\"\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020!H\u0002J\u0010\u0010#\u001a\u00020$2\u0006\u0010%\u001a\u00020$H\u0002J\u0010\u0010&\u001a\u00020$2\u0006\u0010%\u001a\u00020$H\u0002J\u0018\u0010\'\u001a\u00020(2\u0006\u0010%\u001a\u00020$2\u0006\u0010)\u001a\u00020$H\u0002J\u0010\u0010*\u001a\u00020\u001f2\u0006\u0010+\u001a\u00020,H\u0002J\u0008\u0010-\u001a\u00020\u001fH\u0002J\u0010\u0010.\u001a\u00020\u001f2\u0006\u0010/\u001a\u000200H\u0002J\u0010\u00101\u001a\u00020\u001f2\u0006\u0010/\u001a\u000200H\u0002J\u0018\u00102\u001a\u00020\u001f2\u0006\u00103\u001a\u0002042\u0006\u00105\u001a\u000206H\u0002J\u0010\u00107\u001a\u00020\u001f2\u0006\u00108\u001a\u00020(H\u0002J+\u00109\u001a\u00020\u001f2\u000c\u0010:\u001a\u0008\u0012\u0004\u0012\u00020<0;2\u0006\u0010%\u001a\u00020$2\u0006\u0010)\u001a\u00020$H\u0002\u00a2\u0006\u0002\u0010=J\u0010\u0010>\u001a\u00020\u001f2\u0006\u0010?\u001a\u00020(H\u0002J\u0018\u0010@\u001a\u00020\u001f2\u0006\u00103\u001a\u0002042\u0006\u00105\u001a\u000206H\u0002J\u0010\u0010A\u001a\u00020\u001f2\u0006\u0010B\u001a\u00020CH\u0002R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006D"
    }
    d2 = {
        "Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "scopeRunner",
        "Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;",
        "clock",
        "Lcom/squareup/util/Clock;",
        "(Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;Lcom/squareup/util/Clock;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "closeOfDay",
        "Lcom/squareup/noho/NohoRow;",
        "context",
        "Landroid/content/Context;",
        "depositScheduleHint",
        "Lcom/squareup/widgets/MessageView;",
        "depositSpeedCheckableGroup",
        "Lcom/squareup/noho/NohoCheckableGroup;",
        "depositSpeedOneToTwoBusDays",
        "Lcom/squareup/noho/NohoCheckableRow;",
        "depositSpeedSameDay",
        "depositSpeedTitle",
        "Lcom/squareup/marketfont/MarketTextView;",
        "errorMessage",
        "Lcom/squareup/noho/NohoMessageView;",
        "res",
        "Landroid/content/res/Resources;",
        "spinner",
        "Landroid/widget/ProgressBar;",
        "weekendBalanceButton",
        "weekendBalanceHint",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "dayToNextBusinessDay",
        "",
        "day",
        "dayToSecondBusinessDay",
        "hasSecondBusinessDayAsDepositType",
        "",
        "hour",
        "onFailed",
        "failureMessage",
        "Lcom/squareup/receiving/FailureMessage;",
        "onLoading",
        "onScreenData",
        "screenData",
        "Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;",
        "onSucceeded",
        "setCloseOfDay",
        "timeZone",
        "Ljava/util/TimeZone;",
        "cutoffTime",
        "Lcom/squareup/protos/common/time/DayTime;",
        "setDepositSpeedOnCheckedChangeListener",
        "hasLinkedCard",
        "setOneToTwoBusDaysSubtitleText",
        "days",
        "",
        "",
        "([Ljava/lang/String;II)V",
        "setSameDayDeposit",
        "sameDay",
        "setSameDaySubtitle",
        "setUpActionBar",
        "dayOfWeek",
        "",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final clock:Lcom/squareup/util/Clock;

.field private closeOfDay:Lcom/squareup/noho/NohoRow;

.field private context:Landroid/content/Context;

.field private depositScheduleHint:Lcom/squareup/widgets/MessageView;

.field private depositSpeedCheckableGroup:Lcom/squareup/noho/NohoCheckableGroup;

.field private depositSpeedOneToTwoBusDays:Lcom/squareup/noho/NohoCheckableRow;

.field private depositSpeedSameDay:Lcom/squareup/noho/NohoCheckableRow;

.field private depositSpeedTitle:Lcom/squareup/marketfont/MarketTextView;

.field private errorMessage:Lcom/squareup/noho/NohoMessageView;

.field private res:Landroid/content/res/Resources;

.field private final scopeRunner:Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;

.field private spinner:Landroid/widget/ProgressBar;

.field private weekendBalanceButton:Lcom/squareup/noho/NohoCheckableRow;

.field private weekendBalanceHint:Lcom/squareup/widgets/MessageView;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;Lcom/squareup/util/Clock;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "scopeRunner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->scopeRunner:Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;

    iput-object p2, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->clock:Lcom/squareup/util/Clock;

    return-void
.end method

.method public static final synthetic access$getScopeRunner$p(Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;)Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;
    .locals 0

    .line 52
    iget-object p0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->scopeRunner:Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;

    return-object p0
.end method

.method public static final synthetic access$onScreenData(Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;)V
    .locals 0

    .line 52
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->onScreenData(Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;)V

    return-void
.end method

.method public static final synthetic access$setSameDayDeposit(Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;Z)V
    .locals 0

    .line 52
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->setSameDayDeposit(Z)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 287
    sget-object v0, Lcom/squareup/noho/NohoActionBar;->Companion:Lcom/squareup/noho/NohoActionBar$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar$Companion;->findIn(Landroid/view/View;)Lcom/squareup/noho/NohoActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 288
    sget v0, Lcom/squareup/settingsapplet/R$id;->close_of_day_row:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->closeOfDay:Lcom/squareup/noho/NohoRow;

    .line 289
    sget v0, Lcom/squareup/settingsapplet/R$id;->deposit_schedule_hint:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->depositScheduleHint:Lcom/squareup/widgets/MessageView;

    .line 290
    sget v0, Lcom/squareup/settingsapplet/R$id;->deposit_speed_title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->depositSpeedTitle:Lcom/squareup/marketfont/MarketTextView;

    .line 291
    sget v0, Lcom/squareup/settingsapplet/R$id;->deposit_speed_checkable_group:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoCheckableGroup;

    iput-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->depositSpeedCheckableGroup:Lcom/squareup/noho/NohoCheckableGroup;

    .line 292
    sget v0, Lcom/squareup/settingsapplet/R$id;->deposit_speed_one_to_two_business_days:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoCheckableRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->depositSpeedOneToTwoBusDays:Lcom/squareup/noho/NohoCheckableRow;

    .line 293
    sget v0, Lcom/squareup/settingsapplet/R$id;->deposit_speed_same_day:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoCheckableRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->depositSpeedSameDay:Lcom/squareup/noho/NohoCheckableRow;

    .line 294
    sget v0, Lcom/squareup/settingsapplet/R$id;->weekend_balance_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoCheckableRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->weekendBalanceButton:Lcom/squareup/noho/NohoCheckableRow;

    .line 295
    sget v0, Lcom/squareup/settingsapplet/R$id;->weekend_balance_hint:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->weekendBalanceHint:Lcom/squareup/widgets/MessageView;

    .line 296
    sget v0, Lcom/squareup/settingsapplet/R$id;->spinner:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->spinner:Landroid/widget/ProgressBar;

    .line 297
    sget v0, Lcom/squareup/settingsapplet/R$id;->deposit_schedule_error_message:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoMessageView;

    iput-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->errorMessage:Lcom/squareup/noho/NohoMessageView;

    return-void
.end method

.method private final dayToNextBusinessDay(I)I
    .locals 1

    packed-switch p1, :pswitch_data_0

    .line 177
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Invalid day of week."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :pswitch_0
    const/4 p1, 0x0

    goto :goto_0

    :pswitch_1
    const/4 p1, 0x4

    goto :goto_0

    :pswitch_2
    const/4 p1, 0x3

    goto :goto_0

    :pswitch_3
    const/4 p1, 0x2

    goto :goto_0

    :pswitch_4
    const/4 p1, 0x1

    :goto_0
    return p1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private final dayToSecondBusinessDay(I)I
    .locals 1

    packed-switch p1, :pswitch_data_0

    .line 192
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Invalid day of week."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :pswitch_0
    const/4 p1, 0x1

    goto :goto_0

    :pswitch_1
    const/4 p1, 0x0

    goto :goto_0

    :pswitch_2
    const/4 p1, 0x4

    goto :goto_0

    :pswitch_3
    const/4 p1, 0x3

    goto :goto_0

    :pswitch_4
    const/4 p1, 0x2

    :goto_0
    return p1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private final hasSecondBusinessDayAsDepositType(II)Z
    .locals 1

    const/4 v0, 0x4

    if-eq p1, v0, :cond_1

    const/4 v0, 0x5

    if-eq p1, v0, :cond_1

    const/16 p1, 0x12

    if-ge p2, p1, :cond_0

    if-gt p2, v0, :cond_1

    :cond_0
    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private final onFailed(Lcom/squareup/receiving/FailureMessage;)V
    .locals 3

    .line 106
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->closeOfDay:Lcom/squareup/noho/NohoRow;

    if-nez v0, :cond_0

    const-string v1, "closeOfDay"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoRow;->setVisibility(I)V

    .line 107
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->depositScheduleHint:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_1

    const-string v2, "depositScheduleHint"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    .line 108
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->depositSpeedTitle:Lcom/squareup/marketfont/MarketTextView;

    if-nez v0, :cond_2

    const-string v2, "depositSpeedTitle"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 109
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->depositSpeedCheckableGroup:Lcom/squareup/noho/NohoCheckableGroup;

    if-nez v0, :cond_3

    const-string v2, "depositSpeedCheckableGroup"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoCheckableGroup;->setVisibility(I)V

    .line 110
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->weekendBalanceButton:Lcom/squareup/noho/NohoCheckableRow;

    if-nez v0, :cond_4

    const-string v2, "weekendBalanceButton"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoCheckableRow;->setVisibility(I)V

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->weekendBalanceHint:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_5

    const-string v2, "weekendBalanceHint"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->spinner:Landroid/widget/ProgressBar;

    if-nez v0, :cond_6

    const-string v2, "spinner"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 113
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->errorMessage:Lcom/squareup/noho/NohoMessageView;

    const-string v1, "errorMessage"

    if-nez v0, :cond_7

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getTitle()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoMessageView;->setTitle(Ljava/lang/CharSequence;)V

    .line 114
    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getRetryable()Z

    move-result p1

    if-eqz p1, :cond_9

    .line 115
    iget-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->errorMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez p1, :cond_8

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    invoke-virtual {p1}, Lcom/squareup/noho/NohoMessageView;->showSecondaryButton()V

    goto :goto_0

    .line 117
    :cond_9
    iget-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->errorMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez p1, :cond_a

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_a
    invoke-virtual {p1}, Lcom/squareup/noho/NohoMessageView;->hideSecondaryButton()V

    .line 119
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->errorMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez p1, :cond_b

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_b
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoMessageView;->setVisibility(I)V

    return-void
.end method

.method private final onLoading()V
    .locals 3

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->closeOfDay:Lcom/squareup/noho/NohoRow;

    if-nez v0, :cond_0

    const-string v1, "closeOfDay"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoRow;->setVisibility(I)V

    .line 96
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->depositScheduleHint:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_1

    const-string v2, "depositScheduleHint"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    .line 97
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->depositSpeedTitle:Lcom/squareup/marketfont/MarketTextView;

    if-nez v0, :cond_2

    const-string v2, "depositSpeedTitle"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 98
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->depositSpeedCheckableGroup:Lcom/squareup/noho/NohoCheckableGroup;

    if-nez v0, :cond_3

    const-string v2, "depositSpeedCheckableGroup"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoCheckableGroup;->setVisibility(I)V

    .line 99
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->weekendBalanceButton:Lcom/squareup/noho/NohoCheckableRow;

    if-nez v0, :cond_4

    const-string v2, "weekendBalanceButton"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoCheckableRow;->setVisibility(I)V

    .line 100
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->weekendBalanceHint:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_5

    const-string v2, "weekendBalanceHint"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    .line 101
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->errorMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez v0, :cond_6

    const-string v2, "errorMessage"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoMessageView;->setVisibility(I)V

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->spinner:Landroid/widget/ProgressBar;

    if-nez v0, :cond_7

    const-string v1, "spinner"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    return-void
.end method

.method private final onScreenData(Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;)V
    .locals 2

    .line 88
    invoke-virtual {p1}, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;->getDepositScheduleState()Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 91
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->onSucceeded(Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;)V

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 90
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;->getFailureMessage()Lcom/squareup/receiving/FailureMessage;

    move-result-object p1

    if-nez p1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->onFailed(Lcom/squareup/receiving/FailureMessage;)V

    goto :goto_0

    .line 89
    :cond_3
    invoke-direct {p0}, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->onLoading()V

    :goto_0
    return-void
.end method

.method private final onSucceeded(Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;)V
    .locals 8

    .line 123
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->res:Landroid/content/res/Resources;

    if-nez v0, :cond_0

    const-string v1, "res"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget v1, Lcom/squareup/settingsapplet/R$array;->day_of_week:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    const-string v1, "res.getStringArray(com.s\u2026plet.R.array.day_of_week)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 125
    invoke-virtual {p1}, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;->getDayOfWeek()I

    move-result v1

    aget-object v1, v0, v1

    const-string v2, "days[screenData.dayOfWeek]"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/CharSequence;

    invoke-direct {p0, v1}, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->setUpActionBar(Ljava/lang/CharSequence;)V

    .line 127
    invoke-virtual {p1}, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v1

    .line 128
    invoke-virtual {p1}, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;->getCutoffTime()Lcom/squareup/protos/common/time/DayTime;

    move-result-object v2

    .line 129
    invoke-direct {p0, v1, v2}, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->setCloseOfDay(Ljava/util/TimeZone;Lcom/squareup/protos/common/time/DayTime;)V

    .line 131
    iget-object v3, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->depositScheduleHint:Lcom/squareup/widgets/MessageView;

    const-string v4, "depositScheduleHint"

    if-nez v3, :cond_1

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1}, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;->getDepositScheduleHint()Lcom/squareup/resources/TextModel;

    move-result-object v5

    iget-object v6, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->context:Landroid/content/Context;

    const-string v7, "context"

    if-nez v6, :cond_2

    invoke-static {v7}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-interface {v5, v6}, Lcom/squareup/resources/TextModel;->evaluate(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    invoke-virtual {p1}, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;->getSameDaySettlementEnabled()Z

    move-result v3

    invoke-direct {p0, v3}, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->setSameDayDeposit(Z)V

    .line 135
    invoke-virtual {p1}, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;->getDayOfWeek()I

    move-result v3

    iget-object v5, v2, Lcom/squareup/protos/common/time/DayTime;->time_at:Lcom/squareup/protos/common/time/LocalTime;

    iget-object v5, v5, Lcom/squareup/protos/common/time/LocalTime;->hour_of_day:Ljava/lang/Integer;

    const-string v6, "cutoffTime.time_at.hour_of_day"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-direct {p0, v0, v3, v5}, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->setOneToTwoBusDaysSubtitleText([Ljava/lang/String;II)V

    .line 136
    invoke-direct {p0, v1, v2}, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->setSameDaySubtitle(Ljava/util/TimeZone;Lcom/squareup/protos/common/time/DayTime;)V

    .line 138
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->depositSpeedSameDay:Lcom/squareup/noho/NohoCheckableRow;

    if-nez v0, :cond_3

    const-string v1, "depositSpeedSameDay"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p1}, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;->getFee()Lcom/squareup/resources/TextModel;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->context:Landroid/content/Context;

    if-nez v2, :cond_4

    invoke-static {v7}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-interface {v1, v2}, Lcom/squareup/resources/TextModel;->evaluate(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoCheckableRow;->setValue(Ljava/lang/CharSequence;)V

    .line 140
    invoke-virtual {p1}, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;->getHasLinkedCard()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->setDepositSpeedOnCheckedChangeListener(Z)V

    .line 142
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->weekendBalanceButton:Lcom/squareup/noho/NohoCheckableRow;

    const-string v1, "weekendBalanceButton"

    if-nez v0, :cond_5

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {p1}, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;->getSettleIfOptional()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoCheckableRow;->setChecked(Z)V

    .line 143
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->weekendBalanceButton:Lcom/squareup/noho/NohoCheckableRow;

    if-nez v0, :cond_6

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    new-instance v2, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator$onSucceeded$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator$onSucceeded$1;-><init>(Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;)V

    check-cast v2, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoCheckableRow;->onCheckedChange(Lkotlin/jvm/functions/Function2;)V

    .line 147
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->spinner:Landroid/widget/ProgressBar;

    if-nez v0, :cond_7

    const-string v2, "spinner"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 148
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->errorMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez v0, :cond_8

    const-string v3, "errorMessage"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoMessageView;->setVisibility(I)V

    .line 149
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->closeOfDay:Lcom/squareup/noho/NohoRow;

    if-nez v0, :cond_9

    const-string v2, "closeOfDay"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoRow;->setVisibility(I)V

    .line 150
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->depositScheduleHint:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_a

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_a
    invoke-virtual {v0, v2}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    .line 151
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->depositSpeedTitle:Lcom/squareup/marketfont/MarketTextView;

    if-nez v0, :cond_b

    const-string v2, "depositSpeedTitle"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_b
    check-cast v0, Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;->getShouldShowDepositSpeed()Z

    move-result v2

    invoke-static {v0, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 152
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->depositSpeedCheckableGroup:Lcom/squareup/noho/NohoCheckableGroup;

    if-nez v0, :cond_c

    const-string v2, "depositSpeedCheckableGroup"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_c
    check-cast v0, Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;->getShouldShowDepositSpeed()Z

    move-result v2

    invoke-static {v0, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 153
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->weekendBalanceButton:Lcom/squareup/noho/NohoCheckableRow;

    if-nez v0, :cond_d

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_d
    check-cast v0, Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;->getShouldShowWeekendBalance()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 154
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->weekendBalanceHint:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_e

    const-string v1, "weekendBalanceHint"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_e
    check-cast v0, Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;->getShouldShowWeekendBalance()Z

    move-result p1

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method private final setCloseOfDay(Ljava/util/TimeZone;Lcom/squareup/protos/common/time/DayTime;)V
    .locals 7

    .line 210
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v1

    .line 212
    iget-object v3, p2, Lcom/squareup/protos/common/time/DayTime;->day_of_week:Ljava/lang/Integer;

    .line 213
    iget-object p2, p2, Lcom/squareup/protos/common/time/DayTime;->time_at:Lcom/squareup/protos/common/time/LocalTime;

    iget-object p2, p2, Lcom/squareup/protos/common/time/LocalTime;->hour_of_day:Ljava/lang/Integer;

    const-string v0, "cutoffTime.time_at.hour_of_day"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/4 v5, 0x0

    const-string v6, "EEEE, ha z"

    move-object v2, p1

    .line 209
    invoke-static/range {v1 .. v6}, Lcom/squareup/util/Times;->currentTimeZoneRepresentation(Ljava/util/TimeZone;Ljava/util/TimeZone;Ljava/lang/Integer;IILjava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 217
    iget-object p2, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->closeOfDay:Lcom/squareup/noho/NohoRow;

    if-nez p2, :cond_0

    const-string v0, "closeOfDay"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const-string v0, "text"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/CharSequence;

    .line 218
    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    .line 219
    iget-object v1, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->context:Landroid/content/Context;

    if-nez v1, :cond_1

    const-string v2, "context"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    sget v2, Lcom/squareup/noho/R$color;->noho_text_button_link_enabled:I

    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    .line 218
    invoke-direct {v0, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    check-cast v0, Landroid/text/style/CharacterStyle;

    .line 217
    invoke-static {p1, v0}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p2, p1}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final setDepositSpeedOnCheckedChangeListener(Z)V
    .locals 2

    .line 269
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->depositSpeedCheckableGroup:Lcom/squareup/noho/NohoCheckableGroup;

    if-nez v0, :cond_0

    const-string v1, "depositSpeedCheckableGroup"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    new-instance v1, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator$setDepositSpeedOnCheckedChangeListener$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator$setDepositSpeedOnCheckedChangeListener$1;-><init>(Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;Z)V

    check-cast v1, Lcom/squareup/noho/NohoCheckableGroup$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoCheckableGroup;->setOnCheckedChangeListener(Lcom/squareup/noho/NohoCheckableGroup$OnCheckedChangeListener;)V

    return-void
.end method

.method private final setOneToTwoBusDaysSubtitleText([Ljava/lang/String;II)V
    .locals 3

    .line 235
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->depositSpeedOneToTwoBusDays:Lcom/squareup/noho/NohoCheckableRow;

    if-nez v0, :cond_0

    const-string v1, "depositSpeedOneToTwoBusDays"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-direct {p0, p2, p3}, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->hasSecondBusinessDayAsDepositType(II)Z

    move-result p3

    const-string v1, "res"

    const-string v2, "day_and_time"

    if-eqz p3, :cond_2

    .line 237
    iget-object p3, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->res:Landroid/content/res/Resources;

    if-nez p3, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    sget v1, Lcom/squareup/settingsapplet/R$string;->deposit_schedule_arrival_day_and_time:I

    invoke-static {p3, v1}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object p3

    .line 238
    invoke-direct {p0, p2}, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->dayToSecondBusinessDay(I)I

    move-result p2

    aget-object p1, p1, p2

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p3, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 239
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_0

    .line 242
    :cond_2
    iget-object p3, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->res:Landroid/content/res/Resources;

    if-nez p3, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    sget v1, Lcom/squareup/settingsapplet/R$string;->deposit_schedule_arrival_day_and_time:I

    invoke-static {p3, v1}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object p3

    .line 243
    invoke-direct {p0, p2}, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->dayToNextBusinessDay(I)I

    move-result p2

    aget-object p1, p1, p2

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p3, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 244
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 235
    :goto_0
    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoCheckableRow;->setDescription(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final setSameDayDeposit(Z)V
    .locals 2

    .line 225
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->depositSpeedCheckableGroup:Lcom/squareup/noho/NohoCheckableGroup;

    if-nez v0, :cond_0

    const-string v1, "depositSpeedCheckableGroup"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    if-eqz p1, :cond_1

    .line 226
    sget p1, Lcom/squareup/settingsapplet/R$id;->deposit_speed_same_day:I

    goto :goto_0

    :cond_1
    sget p1, Lcom/squareup/settingsapplet/R$id;->deposit_speed_one_to_two_business_days:I

    .line 225
    :goto_0
    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoCheckableGroup;->check(I)V

    return-void
.end method

.method private final setSameDaySubtitle(Ljava/util/TimeZone;Lcom/squareup/protos/common/time/DayTime;)V
    .locals 7

    .line 254
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v1

    .line 256
    iget-object v3, p2, Lcom/squareup/protos/common/time/DayTime;->day_of_week:Ljava/lang/Integer;

    .line 257
    iget-object p2, p2, Lcom/squareup/protos/common/time/DayTime;->time_at:Lcom/squareup/protos/common/time/LocalTime;

    iget-object p2, p2, Lcom/squareup/protos/common/time/LocalTime;->hour_of_day:Ljava/lang/Integer;

    const-string v0, "cutoffTime.time_at.hour_of_day"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/16 v5, 0xf

    const-string v6, "EEEE, hh:mm a"

    move-object v2, p1

    .line 253
    invoke-static/range {v1 .. v6}, Lcom/squareup/util/Times;->currentTimeZoneRepresentation(Ljava/util/TimeZone;Ljava/util/TimeZone;Ljava/lang/Integer;IILjava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 264
    iget-object p2, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->depositSpeedSameDay:Lcom/squareup/noho/NohoCheckableRow;

    if-nez p2, :cond_0

    const-string v0, "depositSpeedSameDay"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 262
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->res:Landroid/content/res/Resources;

    if-nez v0, :cond_1

    const-string v1, "res"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    sget v1, Lcom/squareup/settingsapplet/R$string;->deposit_schedule_arrival_day_and_time:I

    invoke-static {v0, v1}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 263
    check-cast p1, Ljava/lang/CharSequence;

    const-string v1, "day_and_time"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 264
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/noho/NohoCheckableRow;->setDescription(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final setUpActionBar(Ljava/lang/CharSequence;)V
    .locals 4

    .line 196
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->res:Landroid/content/res/Resources;

    if-nez v0, :cond_0

    const-string v1, "res"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget v1, Lcom/squareup/settingsapplet/R$string;->deposit_schedule_title:I

    invoke-static {v0, v1}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v1, "day_of_week"

    .line 197
    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 198
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 202
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez v0, :cond_1

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 199
    :cond_1
    new-instance v1, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 200
    new-instance v2, Lcom/squareup/util/ViewString$TextString;

    const-string v3, "title"

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v2, p1}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v2, Lcom/squareup/resources/TextModel;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object p1

    .line 201
    sget-object v1, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    new-instance v2, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator$setUpActionBar$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator$setUpActionBar$1;-><init>(Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    invoke-virtual {p1, v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object p1

    .line 202
    invoke-virtual {p1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 3

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->bindViews(Landroid/view/View;)V

    .line 74
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "view.context"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->context:Landroid/content/Context;

    .line 75
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "view.resources"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->res:Landroid/content/res/Resources;

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->closeOfDay:Lcom/squareup/noho/NohoRow;

    if-nez v0, :cond_0

    const-string v1, "closeOfDay"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    new-instance v1, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator$attach$1;

    iget-object v2, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->scopeRunner:Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;

    invoke-direct {v1, v2}, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator$attach$1;-><init>(Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    new-instance v2, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinatorKt$sam$java_lang_Runnable$0;

    invoke-direct {v2, v1}, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinatorKt$sam$java_lang_Runnable$0;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-static {v2}, Lcom/squareup/debounce/Debouncers;->debounceRunnable(Ljava/lang/Runnable;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v1

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->errorMessage:Lcom/squareup/noho/NohoMessageView;

    const-string v1, "errorMessage"

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    sget-object v2, Lcom/squareup/noho/NohoButtonType;->SECONDARY:Lcom/squareup/noho/NohoButtonType;

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoMessageView;->setSecondaryButtonType(Lcom/squareup/noho/NohoButtonType;)V

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->errorMessage:Lcom/squareup/noho/NohoMessageView;

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 81
    :cond_2
    new-instance v1, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator$attach$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator$attach$2;-><init>(Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounceRunnable(Ljava/lang/Runnable;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v1

    const-string v2, "debounceRunnable { scope\u2026efreshDepositSchedule() }"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoMessageView;->setSecondaryButtonOnClickListener(Lcom/squareup/debounce/DebouncedOnClickListener;)V

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;->scopeRunner:Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->depositScheduleScreenData()Lio/reactivex/Observable;

    move-result-object v0

    .line 84
    new-instance v1, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator$attach$3;

    move-object v2, p0

    check-cast v2, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;

    invoke-direct {v1, v2}, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator$attach$3;-><init>(Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    new-instance v2, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinatorKt$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {v2, v1}, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinatorKt$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "scopeRunner.depositSched\u2026subscribe(::onScreenData)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method
