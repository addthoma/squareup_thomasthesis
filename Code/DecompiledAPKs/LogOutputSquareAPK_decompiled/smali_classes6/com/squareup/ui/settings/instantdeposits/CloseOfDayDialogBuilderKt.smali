.class public final Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilderKt;
.super Ljava/lang/Object;
.source "CloseOfDayDialogBuilder.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\t\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0005\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0006\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0007\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0008\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\t\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"
    }
    d2 = {
        "AM",
        "",
        "CURRENT_DAY",
        "CURRENT_DAY_MAX_HOUR",
        "DAYS_OF_WEEK_LENGTH",
        "MAX_HOUR",
        "MIN_HOUR",
        "NEXT_DAY",
        "NEXT_DAY_MAX_HOUR",
        "PM",
        "settings-applet_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final AM:I = 0x0

.field private static final CURRENT_DAY:I = 0x0

.field private static final CURRENT_DAY_MAX_HOUR:I = 0xb

.field private static final DAYS_OF_WEEK_LENGTH:I = 0x2

.field private static final MAX_HOUR:I = 0x17

.field private static final MIN_HOUR:I = 0x0

.field private static final NEXT_DAY:I = 0x1

.field private static final NEXT_DAY_MAX_HOUR:I = 0x5

.field private static final PM:I = 0x1
