.class public Lcom/squareup/ui/settings/sharedsettings/SharedSettingsSection$SharedSettingsEntry;
.super Lcom/squareup/ui/settings/sharedsettings/SharedSettingsSection$ListEntry;
.source "SharedSettingsSection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/sharedsettings/SharedSettingsSection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SharedSettingsEntry"
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/ui/settings/sharedsettings/SharedSettingsSection;Lcom/squareup/util/Res;Lcom/squareup/util/Device;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 31
    sget-object v0, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;->CHECKOUT_OPTIONS:Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsSection$ListEntry;-><init>(Lcom/squareup/ui/settings/sharedsettings/SharedSettingsSection;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$SettingsAppletGrouping;)V

    return-void
.end method
