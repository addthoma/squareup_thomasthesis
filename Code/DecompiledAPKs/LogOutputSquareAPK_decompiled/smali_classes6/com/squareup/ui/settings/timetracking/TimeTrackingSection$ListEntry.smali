.class public Lcom/squareup/ui/settings/timetracking/TimeTrackingSection$ListEntry;
.super Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;
.source "TimeTrackingSection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/timetracking/TimeTrackingSection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ListEntry"
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/ui/settings/timetracking/TimeTrackingSection;Lcom/squareup/util/Res;Lcom/squareup/util/Device;)V
    .locals 6
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 41
    sget-object v2, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;->TEAM_MANAGEMENT:Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;

    sget v3, Lcom/squareup/ui/settings/timetracking/TimeTrackingSection;->TITLE_ID:I

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;-><init>(Lcom/squareup/applet/AppletSection;Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$SettingsAppletGrouping;ILcom/squareup/util/Res;Lcom/squareup/util/Device;)V

    return-void
.end method


# virtual methods
.method public getShortValueText()Ljava/lang/String;
    .locals 1

    .line 45
    invoke-virtual {p0}, Lcom/squareup/ui/settings/timetracking/TimeTrackingSection$ListEntry;->getValueText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
