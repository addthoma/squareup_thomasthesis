.class public final Lcom/squareup/ui/settings/SettingsApplet_Factory;
.super Ljava/lang/Object;
.source "SettingsApplet_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/settings/SettingsApplet;",
        ">;"
    }
.end annotation


# instance fields
.field private final bankAccountSectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/bankaccount/BankAccountSection;",
            ">;"
        }
    .end annotation
.end field

.field private final containerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final depositsSectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/instantdeposits/DepositsSection;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsBadgeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SettingsBadge;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsEntryPointProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SettingsAppletEntryPoint;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SettingsAppletEntryPoint;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/bankaccount/BankAccountSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/instantdeposits/DepositsSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SettingsBadge;",
            ">;)V"
        }
    .end annotation

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/squareup/ui/settings/SettingsApplet_Factory;->containerProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p2, p0, Lcom/squareup/ui/settings/SettingsApplet_Factory;->settingsEntryPointProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p3, p0, Lcom/squareup/ui/settings/SettingsApplet_Factory;->bankAccountSectionProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p4, p0, Lcom/squareup/ui/settings/SettingsApplet_Factory;->depositsSectionProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p5, p0, Lcom/squareup/ui/settings/SettingsApplet_Factory;->settingsBadgeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/settings/SettingsApplet_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SettingsAppletEntryPoint;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/bankaccount/BankAccountSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/instantdeposits/DepositsSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SettingsBadge;",
            ">;)",
            "Lcom/squareup/ui/settings/SettingsApplet_Factory;"
        }
    .end annotation

    .line 53
    new-instance v6, Lcom/squareup/ui/settings/SettingsApplet_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/settings/SettingsApplet_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Ldagger/Lazy;Lcom/squareup/ui/settings/SettingsAppletEntryPoint;Lcom/squareup/ui/settings/bankaccount/BankAccountSection;Lcom/squareup/ui/settings/instantdeposits/DepositsSection;Lcom/squareup/ui/settings/SettingsBadge;)Lcom/squareup/ui/settings/SettingsApplet;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Lcom/squareup/ui/settings/SettingsAppletEntryPoint;",
            "Lcom/squareup/ui/settings/bankaccount/BankAccountSection;",
            "Lcom/squareup/ui/settings/instantdeposits/DepositsSection;",
            "Lcom/squareup/ui/settings/SettingsBadge;",
            ")",
            "Lcom/squareup/ui/settings/SettingsApplet;"
        }
    .end annotation

    .line 59
    new-instance v6, Lcom/squareup/ui/settings/SettingsApplet;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/settings/SettingsApplet;-><init>(Ldagger/Lazy;Lcom/squareup/ui/settings/SettingsAppletEntryPoint;Lcom/squareup/ui/settings/bankaccount/BankAccountSection;Lcom/squareup/ui/settings/instantdeposits/DepositsSection;Lcom/squareup/ui/settings/SettingsBadge;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/ui/settings/SettingsApplet;
    .locals 5

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsApplet_Factory;->containerProvider:Ljavax/inject/Provider;

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/settings/SettingsApplet_Factory;->settingsEntryPointProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/settings/SettingsAppletEntryPoint;

    iget-object v2, p0, Lcom/squareup/ui/settings/SettingsApplet_Factory;->bankAccountSectionProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/settings/bankaccount/BankAccountSection;

    iget-object v3, p0, Lcom/squareup/ui/settings/SettingsApplet_Factory;->depositsSectionProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/ui/settings/instantdeposits/DepositsSection;

    iget-object v4, p0, Lcom/squareup/ui/settings/SettingsApplet_Factory;->settingsBadgeProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/ui/settings/SettingsBadge;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/ui/settings/SettingsApplet_Factory;->newInstance(Ldagger/Lazy;Lcom/squareup/ui/settings/SettingsAppletEntryPoint;Lcom/squareup/ui/settings/bankaccount/BankAccountSection;Lcom/squareup/ui/settings/instantdeposits/DepositsSection;Lcom/squareup/ui/settings/SettingsBadge;)Lcom/squareup/ui/settings/SettingsApplet;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/ui/settings/SettingsApplet_Factory;->get()Lcom/squareup/ui/settings/SettingsApplet;

    move-result-object v0

    return-object v0
.end method
