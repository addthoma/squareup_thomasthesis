.class Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "PrinterStationDetailView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;)V
    .locals 0

    .line 130
    iput-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$1;->this$0:Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    .line 132
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$1;->this$0:Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;

    iget-object v0, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->presenter:Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    invoke-static {p1}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;->ofId(I)Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->onNamingOptionSelected(Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;)V

    return-void
.end method
