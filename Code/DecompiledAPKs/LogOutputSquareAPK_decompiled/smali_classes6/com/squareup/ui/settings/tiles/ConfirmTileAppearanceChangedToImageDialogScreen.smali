.class public Lcom/squareup/ui/settings/tiles/ConfirmTileAppearanceChangedToImageDialogScreen;
.super Lcom/squareup/ui/settings/InSettingsAppletScope;
.source "ConfirmTileAppearanceChangedToImageDialogScreen.java"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/settings/tiles/ConfirmTileAppearanceChangedToImageDialogScreen$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/tiles/ConfirmTileAppearanceChangedToImageDialogScreen$Factory;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/tiles/ConfirmTileAppearanceChangedToImageDialogScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 43
    sget-object v0, Lcom/squareup/ui/settings/tiles/-$$Lambda$ConfirmTileAppearanceChangedToImageDialogScreen$QShVNf90fSvQafC1LGCI9OzOrHs;->INSTANCE:Lcom/squareup/ui/settings/tiles/-$$Lambda$ConfirmTileAppearanceChangedToImageDialogScreen$QShVNf90fSvQafC1LGCI9OzOrHs;

    .line 44
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/tiles/ConfirmTileAppearanceChangedToImageDialogScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Lcom/squareup/ui/settings/InSettingsAppletScope;-><init>()V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/settings/tiles/ConfirmTileAppearanceChangedToImageDialogScreen;
    .locals 0

    .line 44
    new-instance p0, Lcom/squareup/ui/settings/tiles/ConfirmTileAppearanceChangedToImageDialogScreen;

    invoke-direct {p0}, Lcom/squareup/ui/settings/tiles/ConfirmTileAppearanceChangedToImageDialogScreen;-><init>()V

    return-object p0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    return-void
.end method
