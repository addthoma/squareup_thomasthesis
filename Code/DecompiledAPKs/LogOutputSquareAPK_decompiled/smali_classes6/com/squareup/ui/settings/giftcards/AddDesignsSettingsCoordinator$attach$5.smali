.class final Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator$attach$5;
.super Lkotlin/jvm/internal/Lambda;
.source "AddDesignsSettingsCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lkotlin/Pair<",
        "+",
        "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
        "+",
        "Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012&\u0010\u0002\u001a\"\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005 \u0006*\u0010\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
        "Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator$attach$5;->this$0:Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 32
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator$attach$5;->invoke(Lkotlin/Pair;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lkotlin/Pair;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
            "Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/giftcards/EGiftTheme;

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;

    .line 105
    invoke-virtual {p1}, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;->getThemesToBeAdded()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 106
    iget-object p1, p0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator$attach$5;->this$0:Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;->access$getRunner$p(Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;)Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$Runner;

    move-result-object p1

    invoke-interface {p1, v0}, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$Runner;->removeTheme(Lcom/squareup/protos/client/giftcards/EGiftTheme;)V

    goto :goto_0

    .line 108
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator$attach$5;->this$0:Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;->access$getRunner$p(Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsCoordinator;)Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$Runner;

    move-result-object p1

    invoke-interface {p1, v0}, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$Runner;->addTheme(Lcom/squareup/protos/client/giftcards/EGiftTheme;)V

    :goto_0
    return-void
.end method
