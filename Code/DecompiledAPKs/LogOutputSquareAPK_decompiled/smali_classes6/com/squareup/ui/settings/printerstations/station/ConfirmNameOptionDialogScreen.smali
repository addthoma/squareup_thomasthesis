.class public Lcom/squareup/ui/settings/printerstations/station/ConfirmNameOptionDialogScreen;
.super Lcom/squareup/ui/settings/printerstations/station/InPrinterStationScope;
.source "ConfirmNameOptionDialogScreen.java"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/settings/printerstations/station/ConfirmNameOptionDialogScreen$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/printerstations/station/ConfirmNameOptionDialogScreen$Factory;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/printerstations/station/ConfirmNameOptionDialogScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final confirmation:Lcom/squareup/register/widgets/Confirmation;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 55
    sget-object v0, Lcom/squareup/ui/settings/printerstations/station/-$$Lambda$ConfirmNameOptionDialogScreen$FlvgpEdAxudyudPEk09hh2eBGrs;->INSTANCE:Lcom/squareup/ui/settings/printerstations/station/-$$Lambda$ConfirmNameOptionDialogScreen$FlvgpEdAxudyudPEk09hh2eBGrs;

    .line 56
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/printerstations/station/ConfirmNameOptionDialogScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>(Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;Lcom/squareup/register/widgets/Confirmation;)V
    .locals 0

    .line 26
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/printerstations/station/InPrinterStationScope;-><init>(Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;)V

    .line 27
    iput-object p2, p0, Lcom/squareup/ui/settings/printerstations/station/ConfirmNameOptionDialogScreen;->confirmation:Lcom/squareup/register/widgets/Confirmation;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/settings/printerstations/station/ConfirmNameOptionDialogScreen;)Lcom/squareup/register/widgets/Confirmation;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/squareup/ui/settings/printerstations/station/ConfirmNameOptionDialogScreen;->confirmation:Lcom/squareup/register/widgets/Confirmation;

    return-object p0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/settings/printerstations/station/ConfirmNameOptionDialogScreen;
    .locals 2

    .line 57
    const-class v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;

    .line 58
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;

    .line 59
    const-class v1, Lcom/squareup/register/widgets/Confirmation;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/register/widgets/Confirmation;

    .line 60
    new-instance v1, Lcom/squareup/ui/settings/printerstations/station/ConfirmNameOptionDialogScreen;

    invoke-direct {v1, v0, p0}, Lcom/squareup/ui/settings/printerstations/station/ConfirmNameOptionDialogScreen;-><init>(Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;Lcom/squareup/register/widgets/Confirmation;)V

    return-object v1
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/ConfirmNameOptionDialogScreen;->printerStationFlow:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/ConfirmNameOptionDialogScreen;->confirmation:Lcom/squareup/register/widgets/Confirmation;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
