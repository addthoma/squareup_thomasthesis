.class public final Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;
.super Ljava/lang/Object;
.source "DepositScheduleScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ScreenData"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000J\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\r\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001BA\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\u0008\u0012\u0006\u0010\n\u001a\u00020\u0008\u0012\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0002\u0010\u000cR\u0011\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0011\u0010\u0011\u001a\u00020\u0012\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u0017\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016R\u0011\u0010\u0017\u001a\u00020\u0018\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u001aR\u0013\u0010\u001b\u001a\u0004\u0018\u00010\u001c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001d\u0010\u001eR\u0017\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001f\u0010\u0016R\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008 \u0010!R\u0011\u0010\"\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008#\u0010!R\u0011\u0010$\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008%\u0010!R\u0011\u0010\t\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008&\u0010!R\u0011\u0010\n\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\'\u0010!R\u0011\u0010(\u001a\u00020)\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008*\u0010+\u00a8\u0006,"
    }
    d2 = {
        "Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;",
        "",
        "state",
        "Lcom/squareup/depositschedule/DepositScheduleSettings$State;",
        "fee",
        "Lcom/squareup/resources/TextModel;",
        "",
        "hasLinkedCard",
        "",
        "shouldShowDepositSpeed",
        "shouldShowWeekendBalance",
        "depositScheduleHint",
        "(Lcom/squareup/depositschedule/DepositScheduleSettings$State;Lcom/squareup/resources/TextModel;ZZZLcom/squareup/resources/TextModel;)V",
        "cutoffTime",
        "Lcom/squareup/protos/common/time/DayTime;",
        "getCutoffTime",
        "()Lcom/squareup/protos/common/time/DayTime;",
        "dayOfWeek",
        "",
        "getDayOfWeek",
        "()I",
        "getDepositScheduleHint",
        "()Lcom/squareup/resources/TextModel;",
        "depositScheduleState",
        "Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;",
        "getDepositScheduleState",
        "()Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;",
        "failureMessage",
        "Lcom/squareup/receiving/FailureMessage;",
        "getFailureMessage",
        "()Lcom/squareup/receiving/FailureMessage;",
        "getFee",
        "getHasLinkedCard",
        "()Z",
        "sameDaySettlementEnabled",
        "getSameDaySettlementEnabled",
        "settleIfOptional",
        "getSettleIfOptional",
        "getShouldShowDepositSpeed",
        "getShouldShowWeekendBalance",
        "timeZone",
        "Ljava/util/TimeZone;",
        "getTimeZone",
        "()Ljava/util/TimeZone;",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final cutoffTime:Lcom/squareup/protos/common/time/DayTime;

.field private final dayOfWeek:I

.field private final depositScheduleHint:Lcom/squareup/resources/TextModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field private final depositScheduleState:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;

.field private final failureMessage:Lcom/squareup/receiving/FailureMessage;

.field private final fee:Lcom/squareup/resources/TextModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field private final hasLinkedCard:Z

.field private final sameDaySettlementEnabled:Z

.field private final settleIfOptional:Z

.field private final shouldShowDepositSpeed:Z

.field private final shouldShowWeekendBalance:Z

.field private final timeZone:Ljava/util/TimeZone;


# direct methods
.method public constructor <init>(Lcom/squareup/depositschedule/DepositScheduleSettings$State;Lcom/squareup/resources/TextModel;ZZZLcom/squareup/resources/TextModel;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/depositschedule/DepositScheduleSettings$State;",
            "Lcom/squareup/resources/TextModel<",
            "+",
            "Ljava/lang/CharSequence;",
            ">;ZZZ",
            "Lcom/squareup/resources/TextModel<",
            "+",
            "Ljava/lang/CharSequence;",
            ">;)V"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fee"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "depositScheduleHint"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;->fee:Lcom/squareup/resources/TextModel;

    iput-boolean p3, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;->hasLinkedCard:Z

    iput-boolean p4, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;->shouldShowDepositSpeed:Z

    iput-boolean p5, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;->shouldShowWeekendBalance:Z

    iput-object p6, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;->depositScheduleHint:Lcom/squareup/resources/TextModel;

    .line 40
    iget-object p2, p1, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->depositScheduleState:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;

    iput-object p2, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;->depositScheduleState:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;

    .line 41
    iget p2, p1, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->dayOfWeek:I

    iput p2, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;->dayOfWeek:I

    .line 42
    invoke-virtual {p1}, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->depositInterval()Lcom/squareup/protos/client/depositsettings/DepositInterval;

    move-result-object p2

    iget-object p2, p2, Lcom/squareup/protos/client/depositsettings/DepositInterval;->cutoff_at:Lcom/squareup/protos/common/time/DayTime;

    const-string p3, "state.depositInterval.cutoff_at"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;->cutoffTime:Lcom/squareup/protos/common/time/DayTime;

    .line 43
    invoke-virtual {p1}, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->timeZone()Ljava/util/TimeZone;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;->timeZone:Ljava/util/TimeZone;

    .line 44
    invoke-virtual {p1}, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->depositInterval()Lcom/squareup/protos/client/depositsettings/DepositInterval;

    move-result-object p2

    iget-object p2, p2, Lcom/squareup/protos/client/depositsettings/DepositInterval;->same_day_settlement_enabled:Ljava/lang/Boolean;

    const-string p3, "state.depositInterval.same_day_settlement_enabled"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    iput-boolean p2, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;->sameDaySettlementEnabled:Z

    .line 45
    invoke-virtual {p1}, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->settleIfOptional()Z

    move-result p2

    iput-boolean p2, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;->settleIfOptional:Z

    .line 46
    iget-object p1, p1, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->failureMessage:Lcom/squareup/receiving/FailureMessage;

    iput-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;->failureMessage:Lcom/squareup/receiving/FailureMessage;

    return-void
.end method


# virtual methods
.method public final getCutoffTime()Lcom/squareup/protos/common/time/DayTime;
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;->cutoffTime:Lcom/squareup/protos/common/time/DayTime;

    return-object v0
.end method

.method public final getDayOfWeek()I
    .locals 1

    .line 41
    iget v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;->dayOfWeek:I

    return v0
.end method

.method public final getDepositScheduleHint()Lcom/squareup/resources/TextModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    .line 38
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;->depositScheduleHint:Lcom/squareup/resources/TextModel;

    return-object v0
.end method

.method public final getDepositScheduleState()Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;->depositScheduleState:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;

    return-object v0
.end method

.method public final getFailureMessage()Lcom/squareup/receiving/FailureMessage;
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;->failureMessage:Lcom/squareup/receiving/FailureMessage;

    return-object v0
.end method

.method public final getFee()Lcom/squareup/resources/TextModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;->fee:Lcom/squareup/resources/TextModel;

    return-object v0
.end method

.method public final getHasLinkedCard()Z
    .locals 1

    .line 35
    iget-boolean v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;->hasLinkedCard:Z

    return v0
.end method

.method public final getSameDaySettlementEnabled()Z
    .locals 1

    .line 44
    iget-boolean v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;->sameDaySettlementEnabled:Z

    return v0
.end method

.method public final getSettleIfOptional()Z
    .locals 1

    .line 45
    iget-boolean v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;->settleIfOptional:Z

    return v0
.end method

.method public final getShouldShowDepositSpeed()Z
    .locals 1

    .line 36
    iget-boolean v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;->shouldShowDepositSpeed:Z

    return v0
.end method

.method public final getShouldShowWeekendBalance()Z
    .locals 1

    .line 37
    iget-boolean v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;->shouldShowWeekendBalance:Z

    return v0
.end method

.method public final getTimeZone()Ljava/util/TimeZone;
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;->timeZone:Ljava/util/TimeZone;

    return-object v0
.end method
