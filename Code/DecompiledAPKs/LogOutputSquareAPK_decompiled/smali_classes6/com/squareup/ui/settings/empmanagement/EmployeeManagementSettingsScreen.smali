.class public final Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen;
.super Lcom/squareup/ui/settings/InSettingsAppletScope;
.source "EmployeeManagementSettingsScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/layer/InSection;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Component;,
        Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 50
    new-instance v0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen;

    invoke-direct {v0}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen;->INSTANCE:Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen;

    .line 300
    sget-object v0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen;->INSTANCE:Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen;

    .line 301
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 53
    invoke-direct {p0}, Lcom/squareup/ui/settings/InSettingsAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 61
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_EMPLOYEE_MANAGEMENT:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 57
    const-class v0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSection;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 304
    sget v0, Lcom/squareup/settingsapplet/R$layout;->employee_management_settings_view:I

    return v0
.end method
