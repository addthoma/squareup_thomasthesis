.class final Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$bindPrinterRow$1;
.super Ljava/lang/Object;
.source "HardwarePrinterSelectView.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->bindPrinterRow(ILcom/squareup/ui/RadioButtonListRow;Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$PrinterRow;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Landroid/view/View;",
        "kotlin.jvm.PlatformType",
        "onClick"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $item:Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$PrinterRow;

.field final synthetic $pos:I

.field final synthetic $view:Lcom/squareup/ui/RadioButtonListRow;

.field final synthetic this$0:Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$PrinterRow;Lcom/squareup/ui/RadioButtonListRow;I)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$bindPrinterRow$1;->this$0:Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;

    iput-object p2, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$bindPrinterRow$1;->$item:Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$PrinterRow;

    iput-object p3, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$bindPrinterRow$1;->$view:Lcom/squareup/ui/RadioButtonListRow;

    iput p4, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$bindPrinterRow$1;->$pos:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 1

    .line 152
    iget-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$bindPrinterRow$1;->this$0:Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->getPresenter$settings_applet_release()Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$bindPrinterRow$1;->$item:Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$PrinterRow;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$PrinterRow;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->isPrinterAtPositionSelected$settings_applet_release(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    .line 156
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$bindPrinterRow$1;->$item:Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$PrinterRow;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$PrinterRow;->getId()Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_1

    .line 157
    iget-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$bindPrinterRow$1;->this$0:Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->getPresenter$settings_applet_release()Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->onNoPrinterSelectedClicked$settings_applet_release()V

    goto :goto_0

    .line 159
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$bindPrinterRow$1;->this$0:Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->getPresenter$settings_applet_release()Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$bindPrinterRow$1;->$item:Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$PrinterRow;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$PrinterRow;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->onHardwarePrinterClicked$settings_applet_release(Ljava/lang/String;)V

    .line 161
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$bindPrinterRow$1;->$view:Lcom/squareup/ui/RadioButtonListRow;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/squareup/ui/RadioButtonListRow;->setChecked(Z)V

    .line 162
    iget-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$bindPrinterRow$1;->this$0:Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;

    invoke-static {p1}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->access$getRecycler$p(Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;)Lcom/squareup/cycler/Recycler;

    move-result-object p1

    iget v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$bindPrinterRow$1;->$pos:I

    invoke-virtual {p1, v0}, Lcom/squareup/cycler/Recycler;->refresh(I)V

    .line 163
    iget-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$bindPrinterRow$1;->this$0:Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;

    invoke-static {p1}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->access$getCheckedRowPosition$p(Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;)I

    move-result p1

    const/4 v0, -0x1

    if-eq p1, v0, :cond_2

    .line 164
    iget-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$bindPrinterRow$1;->this$0:Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;

    invoke-static {p1}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->access$getRecycler$p(Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;)Lcom/squareup/cycler/Recycler;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$bindPrinterRow$1;->this$0:Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;

    invoke-static {v0}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->access$getCheckedRowPosition$p(Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/cycler/Recycler;->refresh(I)V

    .line 166
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$bindPrinterRow$1;->this$0:Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;

    iget v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView$bindPrinterRow$1;->$pos:I

    invoke-static {p1, v0}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->access$setCheckedRowPosition$p(Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;I)V

    return-void
.end method
