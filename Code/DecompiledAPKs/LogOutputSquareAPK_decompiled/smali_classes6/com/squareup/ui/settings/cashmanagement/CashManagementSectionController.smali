.class public Lcom/squareup/ui/settings/cashmanagement/CashManagementSectionController;
.super Ljava/lang/Object;
.source "CashManagementSectionController.java"

# interfaces
.implements Lmortar/Scoped;


# instance fields
.field private final cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManager;

.field private cashManagementEnabled:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final cashManagementSettings:Lcom/squareup/cashmanagement/CashManagementSettings;

.field private final flow:Lflow/Flow;

.field private final sidebarRefresher:Lcom/squareup/ui/settings/SidebarRefresher;


# direct methods
.method constructor <init>(Lflow/Flow;Lcom/squareup/cashmanagement/CashDrawerShiftManager;Lcom/squareup/cashmanagement/CashManagementSettings;Lcom/squareup/ui/settings/SidebarRefresher;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/cashmanagement/CashManagementSectionController;->cashManagementEnabled:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 37
    iput-object p1, p0, Lcom/squareup/ui/settings/cashmanagement/CashManagementSectionController;->flow:Lflow/Flow;

    .line 38
    iput-object p2, p0, Lcom/squareup/ui/settings/cashmanagement/CashManagementSectionController;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManager;

    .line 39
    iput-object p3, p0, Lcom/squareup/ui/settings/cashmanagement/CashManagementSectionController;->cashManagementSettings:Lcom/squareup/cashmanagement/CashManagementSettings;

    .line 40
    iput-object p4, p0, Lcom/squareup/ui/settings/cashmanagement/CashManagementSectionController;->sidebarRefresher:Lcom/squareup/ui/settings/SidebarRefresher;

    return-void
.end method

.method private updateCashManagerEnabled(Z)V
    .locals 1

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/settings/cashmanagement/CashManagementSectionController;->cashManagementSettings:Lcom/squareup/cashmanagement/CashManagementSettings;

    invoke-virtual {v0}, Lcom/squareup/cashmanagement/CashManagementSettings;->isCashManagementEnabled()Z

    move-result v0

    if-eq v0, p1, :cond_0

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/settings/cashmanagement/CashManagementSectionController;->cashManagementSettings:Lcom/squareup/cashmanagement/CashManagementSettings;

    invoke-virtual {v0, p1}, Lcom/squareup/cashmanagement/CashManagementSettings;->setCashManagementEnabled(Z)V

    .line 83
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/cashmanagement/CashManagementSectionController;->cashManagementEnabled:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public cashManagementEnabled()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/settings/cashmanagement/CashManagementSectionController;->cashManagementEnabled:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object v0
.end method

.method public closeConfirmationDialog(Z)V
    .locals 1

    if-eqz p1, :cond_0

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/settings/cashmanagement/CashManagementSectionController;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManager;

    invoke-interface {v0}, Lcom/squareup/cashmanagement/CashDrawerShiftManager;->endCurrentCashDrawerShift()V

    :cond_0
    xor-int/lit8 p1, p1, 0x1

    .line 75
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/cashmanagement/CashManagementSectionController;->updateCashManagerEnabled(Z)V

    return-void
.end method

.method public enableCashManagementToggled(Z)V
    .locals 2

    if-nez p1, :cond_0

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/settings/cashmanagement/CashManagementSectionController;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManager;

    invoke-interface {v0}, Lcom/squareup/cashmanagement/CashDrawerShiftManager;->getCurrentCashDrawerShiftId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 61
    iget-object p1, p0, Lcom/squareup/ui/settings/cashmanagement/CashManagementSectionController;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/ui/settings/cashmanagement/CloseDrawerConfirmationDialog;

    sget-object v1, Lcom/squareup/ui/settings/SettingsAppletScope;->INSTANCE:Lcom/squareup/ui/settings/SettingsAppletScope;

    invoke-direct {v0, v1}, Lcom/squareup/ui/settings/cashmanagement/CloseDrawerConfirmationDialog;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 63
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/cashmanagement/CashManagementSectionController;->updateCashManagerEnabled(Z)V

    :goto_0
    return-void
.end method

.method public synthetic lambda$onEnterScope$0$CashManagementSectionController(Ljava/lang/Boolean;)V
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/settings/cashmanagement/CashManagementSectionController;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManager;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-interface {v0, p1}, Lcom/squareup/cashmanagement/CashDrawerShiftManager;->enableCashManagement(Z)V

    .line 51
    iget-object p1, p0, Lcom/squareup/ui/settings/cashmanagement/CashManagementSectionController;->sidebarRefresher:Lcom/squareup/ui/settings/SidebarRefresher;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/SidebarRefresher;->refresh()V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/settings/cashmanagement/CashManagementSectionController;->cashManagementEnabled:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v1, p0, Lcom/squareup/ui/settings/cashmanagement/CashManagementSectionController;->cashManagementSettings:Lcom/squareup/cashmanagement/CashManagementSettings;

    invoke-virtual {v1}, Lcom/squareup/cashmanagement/CashManagementSettings;->isCashManagementEnabled()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/settings/cashmanagement/CashManagementSectionController;->cashManagementEnabled:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 47
    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/cashmanagement/-$$Lambda$CashManagementSectionController$SP-rRdwTYGz4W-oNYZh83TaBCQs;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/cashmanagement/-$$Lambda$CashManagementSectionController$SP-rRdwTYGz4W-oNYZh83TaBCQs;-><init>(Lcom/squareup/ui/settings/cashmanagement/CashManagementSectionController;)V

    .line 48
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 46
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method
