.class public Lcom/squareup/ui/settings/PairingHistoryFactory;
.super Ljava/lang/Object;
.source "PairingHistoryFactory.java"

# interfaces
.implements Lcom/squareup/ui/main/HistoryFactory;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createHistory(Lcom/squareup/ui/main/Home;Lflow/History;)Lflow/History;
    .locals 0

    .line 18
    invoke-static {p1, p2}, Lcom/squareup/ui/main/HomeKt;->buildUpon(Lcom/squareup/ui/main/Home;Lflow/History;)Lflow/History$Builder;

    move-result-object p1

    sget-object p2, Lcom/squareup/ui/settings/SettingsSectionsScreen;->INSTANCE:Lcom/squareup/ui/settings/SettingsSectionsScreen;

    .line 19
    invoke-virtual {p1, p2}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    move-result-object p1

    sget-object p2, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen;->INSTANCE:Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen;

    .line 20
    invoke-virtual {p1, p2}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    move-result-object p1

    sget-object p2, Lcom/squareup/ui/settings/paymentdevices/pairing/R12PairingScreen;->INSTANCE:Lcom/squareup/ui/settings/paymentdevices/pairing/R12PairingScreen;

    .line 21
    invoke-virtual {p1, p2}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    move-result-object p1

    .line 22
    invoke-virtual {p1}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p1

    return-object p1
.end method

.method public getPermissions()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method
