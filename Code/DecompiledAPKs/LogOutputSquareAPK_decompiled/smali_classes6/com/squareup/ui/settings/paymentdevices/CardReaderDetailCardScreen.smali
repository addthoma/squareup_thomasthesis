.class public Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "CardReaderDetailCardScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/MaybePersistent;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardOverSheetScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Component;,
        Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$ParentComponent;,
        Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;
    }
.end annotation


# static fields
.field private static final AUDIO_COMMS_LONG_CONNECTION_INTERVAL_SECONDS:J = 0xfL


# instance fields
.field private final initialReaderState:Lcom/squareup/ui/settings/paymentdevices/ReaderState;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/settings/paymentdevices/ReaderState;)V
    .locals 0

    .line 62
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    .line 63
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen;->initialReaderState:Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen;)Lcom/squareup/ui/settings/paymentdevices/ReaderState;
    .locals 0

    .line 55
    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen;->initialReaderState:Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    return-object p0
.end method


# virtual methods
.method public isPersistent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public screenLayout()I
    .locals 1

    .line 267
    sget v0, Lcom/squareup/cardreader/ui/R$layout;->card_reader_detail_card_view:I

    return v0
.end method
