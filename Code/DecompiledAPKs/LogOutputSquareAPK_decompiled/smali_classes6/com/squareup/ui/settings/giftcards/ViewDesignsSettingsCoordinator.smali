.class public final Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "ViewDesignsSettingsCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator$ImageItem;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nViewDesignsSettingsCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ViewDesignsSettingsCoordinator.kt\ncom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator\n+ 2 Views.kt\ncom/squareup/util/Views\n+ 3 RecyclerFactory.kt\ncom/squareup/recycler/RecyclerFactory\n+ 4 Recycler.kt\ncom/squareup/cycler/Recycler$Companion\n+ 5 Recycler.kt\ncom/squareup/cycler/Recycler$Config\n+ 6 StandardRowSpec.kt\ncom/squareup/cycler/StandardRowSpec\n+ 7 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,108:1\n1103#2,7:109\n49#3:116\n50#3,3:122\n53#3:137\n599#4,4:117\n601#4:121\n310#5,3:125\n313#5,3:134\n35#6,6:128\n1360#7:138\n1429#7,3:139\n*E\n*S KotlinDebug\n*F\n+ 1 ViewDesignsSettingsCoordinator.kt\ncom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator\n*L\n52#1,7:109\n54#1:116\n54#1,3:122\n54#1:137\n54#1,4:117\n54#1:121\n54#1,3:125\n54#1,3:134\n54#1,6:128\n90#1:138\n90#1,3:139\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001:\u0001#B\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\u001e\u001a\u00020\u001f2\u0006\u0010\u001c\u001a\u00020\u001dH\u0016J\u0010\u0010 \u001a\u00020\u001f2\u0006\u0010!\u001a\u00020\"H\u0002R\u001b\u0010\t\u001a\u00020\n8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\r\u0010\u000e\u001a\u0004\u0008\u000b\u0010\u000cR\u001b\u0010\u000f\u001a\u00020\u00108BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u0013\u0010\u000e\u001a\u0004\u0008\u0011\u0010\u0012R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00160\u0015X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u0017\u001a\u00020\u00188BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u001b\u0010\u000e\u001a\u0004\u0008\u0019\u0010\u001aR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u001dX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006$"
    }
    d2 = {
        "Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "runner",
        "Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsScreen$Runner;",
        "picasso",
        "Lcom/squareup/picasso/Picasso;",
        "recyclerFactory",
        "Lcom/squareup/recycler/RecyclerFactory;",
        "(Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsScreen$Runner;Lcom/squareup/picasso/Picasso;Lcom/squareup/recycler/RecyclerFactory;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "getActionBar",
        "()Lcom/squareup/noho/NohoActionBar;",
        "actionBar$delegate",
        "Lkotlin/Lazy;",
        "addDesigns",
        "Lcom/squareup/noho/NohoButton;",
        "getAddDesigns",
        "()Lcom/squareup/noho/NohoButton;",
        "addDesigns$delegate",
        "recycler",
        "Lcom/squareup/cycler/Recycler;",
        "Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator$ImageItem;",
        "recyclerView",
        "Landroidx/recyclerview/widget/RecyclerView;",
        "getRecyclerView",
        "()Landroidx/recyclerview/widget/RecyclerView;",
        "recyclerView$delegate",
        "view",
        "Landroid/view/View;",
        "attach",
        "",
        "onScreenData",
        "state",
        "Lcom/squareup/ui/settings/giftcards/ScreenData;",
        "ImageItem",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final actionBar$delegate:Lkotlin/Lazy;

.field private final addDesigns$delegate:Lkotlin/Lazy;

.field private final picasso:Lcom/squareup/picasso/Picasso;

.field private recycler:Lcom/squareup/cycler/Recycler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/Recycler<",
            "Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator$ImageItem;",
            ">;"
        }
    .end annotation
.end field

.field private final recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

.field private final recyclerView$delegate:Lkotlin/Lazy;

.field private final runner:Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsScreen$Runner;

.field private view:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsScreen$Runner;Lcom/squareup/picasso/Picasso;Lcom/squareup/recycler/RecyclerFactory;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "runner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "picasso"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "recyclerFactory"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;->runner:Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsScreen$Runner;

    iput-object p2, p0, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;->picasso:Lcom/squareup/picasso/Picasso;

    iput-object p3, p0, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    .line 36
    new-instance p1, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator$actionBar$2;

    invoke-direct {p1, p0}, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator$actionBar$2;-><init>(Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;->actionBar$delegate:Lkotlin/Lazy;

    .line 37
    new-instance p1, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator$addDesigns$2;

    invoke-direct {p1, p0}, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator$addDesigns$2;-><init>(Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;->addDesigns$delegate:Lkotlin/Lazy;

    .line 39
    new-instance p1, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator$recyclerView$2;

    invoke-direct {p1, p0}, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator$recyclerView$2;-><init>(Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;->recyclerView$delegate:Lkotlin/Lazy;

    return-void
.end method

.method public static final synthetic access$getPicasso$p(Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;)Lcom/squareup/picasso/Picasso;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;->picasso:Lcom/squareup/picasso/Picasso;

    return-object p0
.end method

.method public static final synthetic access$getRecyclerView$p(Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;)Landroidx/recyclerview/widget/RecyclerView;
    .locals 0

    .line 29
    invoke-direct {p0}, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;->getRecyclerView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getRunner$p(Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;)Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsScreen$Runner;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;->runner:Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsScreen$Runner;

    return-object p0
.end method

.method public static final synthetic access$getView$p(Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;)Landroid/view/View;
    .locals 1

    .line 29
    iget-object p0, p0, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;->view:Landroid/view/View;

    if-nez p0, :cond_0

    const-string v0, "view"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$onScreenData(Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;Lcom/squareup/ui/settings/giftcards/ScreenData;)V
    .locals 0

    .line 29
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;->onScreenData(Lcom/squareup/ui/settings/giftcards/ScreenData;)V

    return-void
.end method

.method public static final synthetic access$setView$p(Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;Landroid/view/View;)V
    .locals 0

    .line 29
    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;->view:Landroid/view/View;

    return-void
.end method

.method private final getActionBar()Lcom/squareup/noho/NohoActionBar;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;->actionBar$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoActionBar;

    return-object v0
.end method

.method private final getAddDesigns()Lcom/squareup/noho/NohoButton;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;->addDesigns$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoButton;

    return-object v0
.end method

.method private final getRecyclerView()Landroidx/recyclerview/widget/RecyclerView;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;->recyclerView$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    return-object v0
.end method

.method private final onScreenData(Lcom/squareup/ui/settings/giftcards/ScreenData;)V
    .locals 8

    .line 86
    instance-of v0, p1, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;

    if-eqz v0, :cond_3

    move-object v0, p1

    check-cast v0, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->getOrder_configuration()Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 88
    invoke-virtual {v0}, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->getOrder_configuration()Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->enabled_theme_tokens:Ljava/util/List;

    const-string v2, "state.order_configuration.enabled_theme_tokens"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    invoke-virtual {v0}, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->getAll_egift_themes()Ljava/util/List;

    move-result-object v2

    .line 87
    invoke-static {v1, v2}, Lcom/squareup/egiftcard/activation/EGiftCardConfigKt;->generateEGiftCardEnabledThemes(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 138
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v1, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 139
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 140
    check-cast v3, Lcom/squareup/protos/client/giftcards/EGiftTheme;

    .line 91
    new-instance v4, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator$ImageItem;

    .line 92
    iget-object v5, v3, Lcom/squareup/protos/client/giftcards/EGiftTheme;->image_url:Ljava/lang/String;

    const-string v6, "it.image_url"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    invoke-virtual {v0}, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->getOrder_configuration()Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    move-result-object v6

    iget-object v6, v6, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->enabled_theme_tokens:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    const/4 v7, 0x1

    if-le v6, v7, :cond_0

    goto :goto_1

    :cond_0
    const/4 v7, 0x0

    .line 94
    :goto_1
    new-instance v6, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator$onScreenData$$inlined$map$lambda$1;

    invoke-direct {v6, v3, p0, p1}, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator$onScreenData$$inlined$map$lambda$1;-><init>(Lcom/squareup/protos/client/giftcards/EGiftTheme;Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;Lcom/squareup/ui/settings/giftcards/ScreenData;)V

    check-cast v6, Lkotlin/jvm/functions/Function0;

    .line 91
    invoke-direct {v4, v5, v7, v6}, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator$ImageItem;-><init>(Ljava/lang/String;ZLkotlin/jvm/functions/Function0;)V

    .line 95
    invoke-interface {v2, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 141
    :cond_1
    check-cast v2, Ljava/util/List;

    .line 98
    iget-object p1, p0, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;->recycler:Lcom/squareup/cycler/Recycler;

    if-nez p1, :cond_2

    const-string v0, "recycler"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-static {v2}, Lcom/squareup/cycler/DataSourceKt;->toDataSource(Ljava/util/List;)Lcom/squareup/cycler/DataSource;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/cycler/Recycler;->setData(Lcom/squareup/cycler/DataSource;)V

    :cond_3
    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 5

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;->view:Landroid/view/View;

    .line 50
    invoke-direct {p0}, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;->getActionBar()Lcom/squareup/noho/NohoActionBar;

    move-result-object v0

    .line 47
    new-instance v1, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 48
    new-instance v2, Lcom/squareup/util/ViewString$ResourceString;

    sget v3, Lcom/squareup/settingsapplet/R$string;->giftcards_settings_egiftcard_designs:I

    invoke-direct {v2, v3}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v2, Lcom/squareup/resources/TextModel;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 49
    sget-object v2, Lcom/squareup/noho/UpIcon;->X:Lcom/squareup/noho/UpIcon;

    new-instance v3, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator$attach$1;

    invoke-direct {v3, p0}, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator$attach$1;-><init>(Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 50
    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 52
    invoke-direct {p0}, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;->getAddDesigns()Lcom/squareup/noho/NohoButton;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 109
    new-instance v1, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator$attach$$inlined$onClickDebounced$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator$attach$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    invoke-direct {p0}, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;->getRecyclerView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v1

    .line 116
    sget-object v2, Lcom/squareup/cycler/Recycler;->Companion:Lcom/squareup/cycler/Recycler$Companion;

    .line 117
    invoke-virtual {v1}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 118
    new-instance v2, Lcom/squareup/cycler/Recycler$Config;

    invoke-direct {v2}, Lcom/squareup/cycler/Recycler$Config;-><init>()V

    .line 122
    invoke-virtual {v0}, Lcom/squareup/recycler/RecyclerFactory;->getMainDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/cycler/Recycler$Config;->setMainDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 123
    invoke-virtual {v0}, Lcom/squareup/recycler/RecyclerFactory;->getBackgroundDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/squareup/cycler/Recycler$Config;->setBackgroundDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 126
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v3, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator$$special$$inlined$row$1;->INSTANCE:Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator$$special$$inlined$row$1;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v3}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 56
    sget v3, Lcom/squareup/settingsapplet/R$layout;->egiftcard_settings_design_image:I

    .line 128
    new-instance v4, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator$attach$$inlined$adopt$lambda$1;

    invoke-direct {v4, v3, p0}, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator$attach$$inlined$adopt$lambda$1;-><init>(ILcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;)V

    check-cast v4, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v4}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 126
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 125
    invoke-virtual {v2, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 120
    invoke-virtual {v2, v1}, Lcom/squareup/cycler/Recycler$Config;->setUp(Landroidx/recyclerview/widget/RecyclerView;)Lcom/squareup/cycler/Recycler;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;->recycler:Lcom/squareup/cycler/Recycler;

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;->runner:Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsScreen$Runner;->gridColumnCount()Lio/reactivex/Single;

    move-result-object v0

    .line 77
    new-instance v1, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator$attach$4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator$attach$4;-><init>(Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Single;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;->runner:Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsScreen$Runner;->screenData()Lio/reactivex/Observable;

    move-result-object v0

    .line 82
    new-instance v1, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator$attach$5;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator$attach$5;-><init>(Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void

    .line 117
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "RecyclerView needs a layoutManager assigned."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
