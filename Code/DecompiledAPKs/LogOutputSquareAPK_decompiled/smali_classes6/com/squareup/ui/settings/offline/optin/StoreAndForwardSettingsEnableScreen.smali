.class public final Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableScreen;
.super Lcom/squareup/ui/settings/InSettingsAppletScope;
.source "StoreAndForwardSettingsEnableScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/layer/InSection;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableScreen$Component;,
        Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 29
    new-instance v0, Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableScreen;

    invoke-direct {v0}, Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableScreen;->INSTANCE:Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableScreen;

    .line 76
    sget-object v0, Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableScreen;->INSTANCE:Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableScreen;

    .line 77
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 32
    invoke-direct {p0}, Lcom/squareup/ui/settings/InSettingsAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 36
    const-class v0, Lcom/squareup/ui/settings/offline/OfflineSection;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 80
    sget v0, Lcom/squareup/settingsapplet/R$layout;->store_and_forward_settings_enable_view:I

    return v0
.end method
