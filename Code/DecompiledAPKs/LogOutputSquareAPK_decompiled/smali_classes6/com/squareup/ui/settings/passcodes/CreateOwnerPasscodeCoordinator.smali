.class public Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "CreateOwnerPasscodeCoordinator.java"


# instance fields
.field private actionBar:Lcom/squareup/noho/NohoActionBar;

.field private createPasscodeContainer:Lcom/squareup/noho/NohoLinearLayout;

.field private description:Lcom/squareup/marketfont/MarketTextView;

.field private header:Lcom/squareup/marketfont/MarketTextView;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private pinPad:Lcom/squareup/padlock/Padlock;

.field private progressBar:Landroid/widget/LinearLayout;

.field private final res:Lcom/squareup/util/Res;

.field private final scopeRunner:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

.field private starGroup:Lcom/squareup/ui/StarGroup;

.field private view:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lio/reactivex/Scheduler;Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;)V
    .locals 0
    .param p2    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 43
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->res:Lcom/squareup/util/Res;

    .line 45
    iput-object p2, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->mainScheduler:Lio/reactivex/Scheduler;

    .line 46
    iput-object p3, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->scopeRunner:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;)Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->scopeRunner:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

    return-object p0
.end method

.method private bindViews(Landroid/view/View;)V
    .locals 1

    .line 185
    sget v0, Lcom/squareup/settingsapplet/R$id;->create_passcode_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoActionBar;

    iput-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 186
    sget v0, Lcom/squareup/settingsapplet/R$id;->create_passcode_progress_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->progressBar:Landroid/widget/LinearLayout;

    .line 187
    sget v0, Lcom/squareup/settingsapplet/R$id;->create_passcode_container:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoLinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->createPasscodeContainer:Lcom/squareup/noho/NohoLinearLayout;

    .line 188
    sget v0, Lcom/squareup/settingsapplet/R$id;->create_passcode_star_group:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/StarGroup;

    iput-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->starGroup:Lcom/squareup/ui/StarGroup;

    .line 189
    sget v0, Lcom/squareup/settingsapplet/R$id;->create_passcode_pin_pad:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock;

    iput-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->pinPad:Lcom/squareup/padlock/Padlock;

    .line 190
    sget v0, Lcom/squareup/settingsapplet/R$id;->create_passcode_header:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->header:Lcom/squareup/marketfont/MarketTextView;

    .line 191
    sget v0, Lcom/squareup/settingsapplet/R$id;->create_passcode_description:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketTextView;

    iput-object p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->description:Lcom/squareup/marketfont/MarketTextView;

    return-void
.end method

.method private getLoadingActionBarConfig()Lcom/squareup/noho/NohoActionBar$Config;
    .locals 3

    .line 195
    new-instance v0, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    new-instance v1, Lcom/squareup/util/ViewString$ResourceString;

    sget v2, Lcom/squareup/settingsapplet/R$string;->passcode_settings_create_owner_passcode_title:I

    invoke-direct {v1, v2}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    .line 196
    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 198
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v0

    return-object v0
.end method

.method private getSuccessOrFailedActionBarConfig()Lcom/squareup/noho/NohoActionBar$Config;
    .locals 4

    .line 202
    new-instance v0, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    sget-object v1, Lcom/squareup/noho/UpIcon;->X:Lcom/squareup/noho/UpIcon;

    iget-object v2, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->scopeRunner:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v3, Lcom/squareup/ui/settings/passcodes/-$$Lambda$PqfxcSC9k-tHZD-u6LjgG-kXNjY;

    invoke-direct {v3, v2}, Lcom/squareup/ui/settings/passcodes/-$$Lambda$PqfxcSC9k-tHZD-u6LjgG-kXNjY;-><init>(Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;)V

    .line 203
    invoke-virtual {v0, v1, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    new-instance v1, Lcom/squareup/util/ViewString$ResourceString;

    sget v2, Lcom/squareup/settingsapplet/R$string;->passcode_settings_create_owner_passcode_title:I

    invoke-direct {v1, v2}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    .line 204
    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 206
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic lambda$6FQX8fNFRx86odG15bvtAmzMA2w(Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeScreen$Data;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->showScreenData(Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeScreen$Data;)V

    return-void
.end method

.method static synthetic lambda$showScreenData$1()V
    .locals 0

    return-void
.end method

.method private showConfirmationEntryData(Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeScreen$Data;)V
    .locals 3

    .line 165
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->header:Lcom/squareup/marketfont/MarketTextView;

    iget-object v1, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/settingsapplet/R$string;->passcode_settings_confirm_passcode:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 166
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->header:Lcom/squareup/marketfont/MarketTextView;

    iget-object v1, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/settingsapplet/R$color;->create_passcode_body_text:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setTextColor(I)V

    .line 168
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->starGroup:Lcom/squareup/ui/StarGroup;

    iget v1, p1, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeScreen$Data;->expectedPasscodeLength:I

    invoke-virtual {v0, v1}, Lcom/squareup/ui/StarGroup;->setExpectedStarCount(I)V

    .line 169
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->starGroup:Lcom/squareup/ui/StarGroup;

    iget-object v1, p1, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeScreen$Data;->unsavedOwnerPasscodeConfirmation:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/StarGroup;->setStarCount(I)V

    .line 171
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->pinPad:Lcom/squareup/padlock/Padlock;

    iget-object p1, p1, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeScreen$Data;->unsavedOwnerPasscodeConfirmation:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    invoke-virtual {v0, p1}, Lcom/squareup/padlock/Padlock;->setBackspaceEnabled(Z)V

    .line 172
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->pinPad:Lcom/squareup/padlock/Padlock;

    new-instance v0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator$3;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator$3;-><init>(Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;)V

    invoke-virtual {p1, v0}, Lcom/squareup/padlock/Padlock;->setOnKeyPressListener(Lcom/squareup/padlock/Padlock$OnKeyPressListener;)V

    return-void
.end method

.method private showFailedScreenData(Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeScreen$Data;Ljava/lang/String;)V
    .locals 3

    .line 100
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->view:Landroid/view/View;

    iget-object v1, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->scopeRunner:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/settings/passcodes/-$$Lambda$nNmqDU_fDZw3mTMfmnlT8ATFNGw;

    invoke-direct {v2, v1}, Lcom/squareup/ui/settings/passcodes/-$$Lambda$nNmqDU_fDZw3mTMfmnlT8ATFNGw;-><init>(Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;)V

    invoke-static {v0, v2}, Lcom/squareup/workflow/ui/HandlesBack$Helper;->setBackHandler(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 101
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    invoke-direct {p0}, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->getSuccessOrFailedActionBarConfig()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 103
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->progressBar:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 104
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->createPasscodeContainer:Lcom/squareup/noho/NohoLinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoLinearLayout;->setVisibility(I)V

    .line 106
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->starGroup:Lcom/squareup/ui/StarGroup;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/StarGroup;->setVisibility(I)V

    .line 107
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->starGroup:Lcom/squareup/ui/StarGroup;

    iget v2, p1, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeScreen$Data;->expectedPasscodeLength:I

    invoke-virtual {v0, v2}, Lcom/squareup/ui/StarGroup;->setExpectedStarCount(I)V

    .line 108
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->starGroup:Lcom/squareup/ui/StarGroup;

    iget-object p1, p1, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeScreen$Data;->unsavedOwnerPasscode:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/StarGroup;->setStarCount(I)V

    .line 110
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->header:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p1, p2}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 111
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->header:Lcom/squareup/marketfont/MarketTextView;

    iget-object p2, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/settingsapplet/R$color;->create_passcode_error:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getColor(I)I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/marketfont/MarketTextView;->setTextColor(I)V

    .line 113
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->pinPad:Lcom/squareup/padlock/Padlock;

    invoke-virtual {p1, v1}, Lcom/squareup/padlock/Padlock;->setBackspaceEnabled(Z)V

    .line 114
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->pinPad:Lcom/squareup/padlock/Padlock;

    new-instance p2, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator$1;

    invoke-direct {p2, p0}, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator$1;-><init>(Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;)V

    invoke-virtual {p1, p2}, Lcom/squareup/padlock/Padlock;->setOnKeyPressListener(Lcom/squareup/padlock/Padlock$OnKeyPressListener;)V

    return-void
.end method

.method private showInitialEntryData(Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeScreen$Data;)V
    .locals 3

    .line 145
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->header:Lcom/squareup/marketfont/MarketTextView;

    iget-object v1, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/settingsapplet/R$string;->passcode_settings_enter_passcode:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 146
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->header:Lcom/squareup/marketfont/MarketTextView;

    iget-object v1, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/settingsapplet/R$color;->create_passcode_body_text:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setTextColor(I)V

    .line 148
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->starGroup:Lcom/squareup/ui/StarGroup;

    iget v1, p1, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeScreen$Data;->expectedPasscodeLength:I

    invoke-virtual {v0, v1}, Lcom/squareup/ui/StarGroup;->setExpectedStarCount(I)V

    .line 149
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->starGroup:Lcom/squareup/ui/StarGroup;

    iget-object v1, p1, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeScreen$Data;->unsavedOwnerPasscode:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/StarGroup;->setStarCount(I)V

    .line 151
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->pinPad:Lcom/squareup/padlock/Padlock;

    iget-object p1, p1, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeScreen$Data;->unsavedOwnerPasscode:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    invoke-virtual {v0, p1}, Lcom/squareup/padlock/Padlock;->setBackspaceEnabled(Z)V

    .line 152
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->pinPad:Lcom/squareup/padlock/Padlock;

    new-instance v0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator$2;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator$2;-><init>(Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;)V

    invoke-virtual {p1, v0}, Lcom/squareup/padlock/Padlock;->setOnKeyPressListener(Lcom/squareup/padlock/Padlock$OnKeyPressListener;)V

    return-void
.end method

.method private showScreenData(Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeScreen$Data;)V
    .locals 4

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar;->setVisibility(I)V

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->description:Lcom/squareup/marketfont/MarketTextView;

    iget-object v2, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/settingsapplet/R$string;->passcode_settings_create_owner_passcode_description:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->description:Lcom/squareup/marketfont/MarketTextView;

    iget-object v2, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/settingsapplet/R$color;->create_passcode_body_text:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/squareup/marketfont/MarketTextView;->setTextColor(I)V

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->pinPad:Lcom/squareup/padlock/Padlock;

    iget-object v2, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->view:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-static {v2, v3, v1, v1}, Lcom/squareup/marketfont/MarketTypeface;->getTypeface(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;ZZ)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/padlock/Padlock;->setTypeface(Landroid/graphics/Typeface;)V

    .line 69
    sget-object v0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator$4;->$SwitchMap$com$squareup$permissions$PasscodesSettings$RequestState:[I

    iget-object v2, p1, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeScreen$Data;->requestState:Lcom/squareup/permissions/PasscodesSettings$RequestState;

    invoke-virtual {v2}, Lcom/squareup/permissions/PasscodesSettings$RequestState;->ordinal()I

    move-result v2

    aget v0, v0, v2

    const/4 v2, 0x1

    if-eq v0, v2, :cond_4

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    goto :goto_0

    .line 95
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->showSuccessScreenData(Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeScreen$Data;)V

    goto :goto_0

    .line 89
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/settingsapplet/R$string;->passcode_settings_passcodes_do_not_match_error:I

    .line 90
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 89
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->showFailedScreenData(Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeScreen$Data;Ljava/lang/String;)V

    .line 91
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->starGroup:Lcom/squareup/ui/StarGroup;

    invoke-virtual {p1}, Lcom/squareup/ui/StarGroup;->wiggleClear()V

    goto :goto_0

    .line 85
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/settingsapplet/R$string;->passcode_settings_error:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->showFailedScreenData(Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeScreen$Data;Ljava/lang/String;)V

    goto :goto_0

    .line 81
    :cond_3
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/settingsapplet/R$string;->passcode_settings_passcode_in_use:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->showFailedScreenData(Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeScreen$Data;Ljava/lang/String;)V

    goto :goto_0

    .line 73
    :cond_4
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->view:Landroid/view/View;

    sget-object v0, Lcom/squareup/ui/settings/passcodes/-$$Lambda$CreateOwnerPasscodeCoordinator$fA8B_y6lVJW37ucTdsIqiv2Y674;->INSTANCE:Lcom/squareup/ui/settings/passcodes/-$$Lambda$CreateOwnerPasscodeCoordinator$fA8B_y6lVJW37ucTdsIqiv2Y674;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBack$Helper;->setBackHandler(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 74
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    invoke-direct {p0}, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->getLoadingActionBarConfig()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 76
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->progressBar:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 77
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->createPasscodeContainer:Lcom/squareup/noho/NohoLinearLayout;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoLinearLayout;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method private showSuccessScreenData(Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeScreen$Data;)V
    .locals 3

    .line 123
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->view:Landroid/view/View;

    iget-object v1, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->scopeRunner:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/settings/passcodes/-$$Lambda$nNmqDU_fDZw3mTMfmnlT8ATFNGw;

    invoke-direct {v2, v1}, Lcom/squareup/ui/settings/passcodes/-$$Lambda$nNmqDU_fDZw3mTMfmnlT8ATFNGw;-><init>(Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;)V

    invoke-static {v0, v2}, Lcom/squareup/workflow/ui/HandlesBack$Helper;->setBackHandler(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 124
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    invoke-direct {p0}, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->getSuccessOrFailedActionBarConfig()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 126
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->progressBar:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 127
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->createPasscodeContainer:Lcom/squareup/noho/NohoLinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoLinearLayout;->setVisibility(I)V

    .line 129
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->starGroup:Lcom/squareup/ui/StarGroup;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/StarGroup;->setVisibility(I)V

    .line 137
    iget-object v0, p1, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeScreen$Data;->unsavedOwnerPasscode:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x4

    if-ge v0, v1, :cond_0

    .line 138
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->showInitialEntryData(Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeScreen$Data;)V

    goto :goto_0

    .line 140
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->showConfirmationEntryData(Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeScreen$Data;)V

    :goto_0
    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 1

    .line 50
    iput-object p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->view:Landroid/view/View;

    .line 52
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->bindViews(Landroid/view/View;)V

    .line 55
    new-instance v0, Lcom/squareup/ui/settings/passcodes/-$$Lambda$CreateOwnerPasscodeCoordinator$j6RROnQIOieQwl3oG0GprdZq_Co;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/passcodes/-$$Lambda$CreateOwnerPasscodeCoordinator$j6RROnQIOieQwl3oG0GprdZq_Co;-><init>(Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public synthetic lambda$attach$0$CreateOwnerPasscodeCoordinator()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->scopeRunner:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->createOwnerPasscodeCoordinatorScreenData()Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;->mainScheduler:Lio/reactivex/Scheduler;

    .line 56
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/passcodes/-$$Lambda$CreateOwnerPasscodeCoordinator$6FQX8fNFRx86odG15bvtAmzMA2w;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/passcodes/-$$Lambda$CreateOwnerPasscodeCoordinator$6FQX8fNFRx86odG15bvtAmzMA2w;-><init>(Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;)V

    .line 57
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method
