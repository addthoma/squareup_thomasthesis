.class public final Lcom/squareup/ui/settings/bankaccount/BankAccountCancelVerificationDialog$Factory;
.super Ljava/lang/Object;
.source "BankAccountCancelVerificationDialog.kt"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/bankaccount/BankAccountCancelVerificationDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBankAccountCancelVerificationDialog.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BankAccountCancelVerificationDialog.kt\ncom/squareup/ui/settings/bankaccount/BankAccountCancelVerificationDialog$Factory\n+ 2 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,67:1\n52#2:68\n*E\n*S KotlinDebug\n*F\n+ 1 BankAccountCancelVerificationDialog.kt\ncom/squareup/ui/settings/bankaccount/BankAccountCancelVerificationDialog$Factory\n*L\n29#1:68\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u0007H\u0016J \u0010\u0008\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0002\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/ui/settings/bankaccount/BankAccountCancelVerificationDialog$Factory;",
        "Lcom/squareup/workflow/DialogFactory;",
        "()V",
        "create",
        "Lio/reactivex/Single;",
        "Landroid/app/Dialog;",
        "context",
        "Landroid/content/Context;",
        "createDialog",
        "scopeRunner",
        "Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;",
        "screenData",
        "Lcom/squareup/ui/settings/bankaccount/BankAccountCancelVerificationDialog$ScreenData;",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final synthetic access$createDialog(Lcom/squareup/ui/settings/bankaccount/BankAccountCancelVerificationDialog$Factory;Landroid/content/Context;Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;Lcom/squareup/ui/settings/bankaccount/BankAccountCancelVerificationDialog$ScreenData;)Landroid/app/Dialog;
    .locals 0

    .line 27
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/settings/bankaccount/BankAccountCancelVerificationDialog$Factory;->createDialog(Landroid/content/Context;Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;Lcom/squareup/ui/settings/bankaccount/BankAccountCancelVerificationDialog$ScreenData;)Landroid/app/Dialog;

    move-result-object p0

    return-object p0
.end method

.method private final createDialog(Landroid/content/Context;Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;Lcom/squareup/ui/settings/bankaccount/BankAccountCancelVerificationDialog$ScreenData;)Landroid/app/Dialog;
    .locals 2

    .line 42
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/settingsapplet/R$string;->cancel_verification_message:I

    invoke-static {v0, v1}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 43
    invoke-virtual {p3}, Lcom/squareup/ui/settings/bankaccount/BankAccountCancelVerificationDialog$ScreenData;->getSuffix()Ljava/lang/String;

    move-result-object p3

    check-cast p3, Ljava/lang/CharSequence;

    const-string v1, "suffix"

    invoke-virtual {v0, v1, p3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p3

    .line 44
    invoke-virtual {p3}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p3

    .line 46
    new-instance v0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v0, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 47
    sget p1, Lcom/squareup/settingsapplet/R$string;->cancel_verification_title:I

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 48
    invoke-virtual {p1, p3}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 49
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setVerticalButtonOrientation()Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 51
    sget p3, Lcom/squareup/noho/R$drawable;->noho_selector_destructive_button_background:I

    .line 50
    invoke-virtual {p1, p3}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButtonBackground(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 54
    sget p3, Lcom/squareup/noho/R$color;->noho_color_selector_destructive_button_text:I

    .line 53
    invoke-virtual {p1, p3}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButtonTextColor(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 56
    sget p3, Lcom/squareup/settingsapplet/R$string;->cancel_bank_verification:I

    new-instance v0, Lcom/squareup/ui/settings/bankaccount/BankAccountCancelVerificationDialog$Factory$createDialog$1;

    invoke-direct {v0, p2}, Lcom/squareup/ui/settings/bankaccount/BankAccountCancelVerificationDialog$Factory$createDialog$1;-><init>(Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;)V

    check-cast v0, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {p1, p3, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 59
    sget p3, Lcom/squareup/settingsapplet/R$string;->continue_verification:I

    new-instance v0, Lcom/squareup/ui/settings/bankaccount/BankAccountCancelVerificationDialog$Factory$createDialog$2;

    invoke-direct {v0, p2}, Lcom/squareup/ui/settings/bankaccount/BankAccountCancelVerificationDialog$Factory$createDialog$2;-><init>(Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;)V

    check-cast v0, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {p1, p3, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    const/4 p2, 0x0

    .line 62
    invoke-virtual {p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setCancelable(Z)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 63
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    const-string p2, "ThemedAlertDialog.Builde\u2026alse)\n          .create()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/app/Dialog;

    return-object p1
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    const-class v0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScope$Component;

    .line 30
    invoke-interface {v0}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScope$Component;->scopeRunner()Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;

    move-result-object v0

    .line 32
    invoke-virtual {v0}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;->bankAccountCancelVerificationScreenData()Lio/reactivex/Observable;

    move-result-object v1

    .line 33
    invoke-virtual {v1}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v1

    .line 34
    new-instance v2, Lcom/squareup/ui/settings/bankaccount/BankAccountCancelVerificationDialog$Factory$create$1;

    invoke-direct {v2, p0, p1, v0}, Lcom/squareup/ui/settings/bankaccount/BankAccountCancelVerificationDialog$Factory$create$1;-><init>(Lcom/squareup/ui/settings/bankaccount/BankAccountCancelVerificationDialog$Factory;Landroid/content/Context;Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;)V

    check-cast v2, Lio/reactivex/functions/Function;

    invoke-virtual {v1, v2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "scopeRunner.bankAccountC\u2026ntext, scopeRunner, it) }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
