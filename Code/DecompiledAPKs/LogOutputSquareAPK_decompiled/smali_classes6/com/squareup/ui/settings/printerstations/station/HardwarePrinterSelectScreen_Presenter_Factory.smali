.class public final Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen_Presenter_Factory;
.super Ljava/lang/Object;
.source "HardwarePrinterSelectScreen_Presenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final appNameFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AppNameFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final hardwarePrinterTrackerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/HardwarePrinterTracker;",
            ">;"
        }
    .end annotation
.end field

.field private final paperSignatureSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/PaperSignatureSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final printerStationStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;",
            ">;"
        }
    .end annotation
.end field

.field private final printerStationsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final scopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final testPrintProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/printerstations/station/TestPrint;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/HardwarePrinterTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/PaperSignatureSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/printerstations/station/TestPrint;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AppNameFormatter;",
            ">;)V"
        }
    .end annotation

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p2, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen_Presenter_Factory;->scopeRunnerProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p3, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen_Presenter_Factory;->hardwarePrinterTrackerProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p4, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen_Presenter_Factory;->printerStationsProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p5, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen_Presenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p6, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen_Presenter_Factory;->printerStationStateProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p7, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen_Presenter_Factory;->paperSignatureSettingsProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p8, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen_Presenter_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p9, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen_Presenter_Factory;->testPrintProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p10, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen_Presenter_Factory;->appNameFormatterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen_Presenter_Factory;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/HardwarePrinterTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/PaperSignatureSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/printerstations/station/TestPrint;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AppNameFormatter;",
            ">;)",
            "Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen_Presenter_Factory;"
        }
    .end annotation

    .line 74
    new-instance v11, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen_Presenter_Factory;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen_Presenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v11
.end method

.method public static newInstance(Lcom/squareup/util/Res;Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;Lcom/squareup/print/HardwarePrinterTracker;Lcom/squareup/print/PrinterStations;Lcom/squareup/analytics/Analytics;Ljava/lang/Object;Lcom/squareup/papersignature/PaperSignatureSettings;Lflow/Flow;Lcom/squareup/ui/settings/printerstations/station/TestPrint;Lcom/squareup/util/AppNameFormatter;)Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;
    .locals 12

    .line 82
    new-instance v11, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;

    move-object/from16 v6, p5

    check-cast v6, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;-><init>(Lcom/squareup/util/Res;Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;Lcom/squareup/print/HardwarePrinterTracker;Lcom/squareup/print/PrinterStations;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;Lcom/squareup/papersignature/PaperSignatureSettings;Lflow/Flow;Lcom/squareup/ui/settings/printerstations/station/TestPrint;Lcom/squareup/util/AppNameFormatter;)V

    return-object v11
.end method


# virtual methods
.method public get()Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;
    .locals 11

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen_Presenter_Factory;->scopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;

    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen_Presenter_Factory;->hardwarePrinterTrackerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/print/HardwarePrinterTracker;

    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen_Presenter_Factory;->printerStationsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/print/PrinterStations;

    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen_Presenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen_Presenter_Factory;->printerStationStateProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v6

    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen_Presenter_Factory;->paperSignatureSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/papersignature/PaperSignatureSettings;

    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen_Presenter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lflow/Flow;

    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen_Presenter_Factory;->testPrintProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/ui/settings/printerstations/station/TestPrint;

    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen_Presenter_Factory;->appNameFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/util/AppNameFormatter;

    invoke-static/range {v1 .. v10}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen_Presenter_Factory;->newInstance(Lcom/squareup/util/Res;Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;Lcom/squareup/print/HardwarePrinterTracker;Lcom/squareup/print/PrinterStations;Lcom/squareup/analytics/Analytics;Ljava/lang/Object;Lcom/squareup/papersignature/PaperSignatureSettings;Lflow/Flow;Lcom/squareup/ui/settings/printerstations/station/TestPrint;Lcom/squareup/util/AppNameFormatter;)Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen_Presenter_Factory;->get()Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;

    move-result-object v0

    return-object v0
.end method
