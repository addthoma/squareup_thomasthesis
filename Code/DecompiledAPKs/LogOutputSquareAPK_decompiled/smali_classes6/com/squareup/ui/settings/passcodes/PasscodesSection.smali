.class public Lcom/squareup/ui/settings/passcodes/PasscodesSection;
.super Lcom/squareup/applet/AppletSection;
.source "PasscodesSection.java"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/passcodes/PasscodesSection$Access;,
        Lcom/squareup/ui/settings/passcodes/PasscodesSection$ListEntry;
    }
.end annotation


# static fields
.field public static TITLE_ID:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 29
    sget v0, Lcom/squareup/settingsapplet/R$string;->passcode_settings_section_title:I

    sput v0, Lcom/squareup/ui/settings/passcodes/PasscodesSection;->TITLE_ID:I

    return-void
.end method

.method public constructor <init>(Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/permissions/EmployeeManagementModeDecider;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 34
    new-instance v0, Lcom/squareup/ui/settings/passcodes/PasscodesSection$Access;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/ui/settings/passcodes/PasscodesSection$Access;-><init>(Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/permissions/EmployeeManagementModeDecider;)V

    invoke-direct {p0, v0}, Lcom/squareup/applet/AppletSection;-><init>(Lcom/squareup/applet/SectionAccess;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic getInitialScreen()Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    .line 28
    invoke-virtual {p0}, Lcom/squareup/ui/settings/passcodes/PasscodesSection;->getInitialScreen()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    return-object v0
.end method

.method public getInitialScreen()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 1

    .line 39
    sget-object v0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScreen;->INSTANCE:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScreen;

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method
