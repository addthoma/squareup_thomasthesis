.class Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsAdapter;
.super Lcom/squareup/ui/account/HardwarePeripheralListAdapter;
.source "CashDrawerSettingsAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/account/HardwarePeripheralListAdapter<",
        "Lcom/squareup/cashdrawer/CashDrawer;",
        ">;"
    }
.end annotation


# static fields
.field private static final STRINGS:Lcom/squareup/ui/account/HardwarePeripheralListAdapter$StringHolder;


# instance fields
.field private final availableCashDrawers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/cashdrawer/CashDrawer;",
            ">;"
        }
    .end annotation
.end field

.field private final presenter:Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen$Presenter;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 27
    new-instance v0, Lcom/squareup/ui/account/HardwarePeripheralListAdapter$StringHolder;

    sget v1, Lcom/squareup/settingsapplet/R$string;->cash_drawers_uppercase_available:I

    sget v2, Lcom/squareup/settingsapplet/R$string;->cash_drawers_none_found:I

    sget v3, Lcom/squareup/settingsapplet/R$string;->cash_drawers_help_message:I

    const/4 v4, 0x0

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/squareup/ui/account/HardwarePeripheralListAdapter$StringHolder;-><init>(IIII)V

    sput-object v0, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsAdapter;->STRINGS:Lcom/squareup/ui/account/HardwarePeripheralListAdapter$StringHolder;

    return-void
.end method

.method constructor <init>(Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen$Presenter;)V
    .locals 0

    .line 35
    invoke-direct {p0}, Lcom/squareup/ui/account/HardwarePeripheralListAdapter;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsAdapter;->presenter:Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen$Presenter;

    .line 37
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsAdapter;->availableCashDrawers:Ljava/util/List;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsAdapter;)Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen$Presenter;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsAdapter;->presenter:Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen$Presenter;

    return-object p0
.end method


# virtual methods
.method protected buildAndBindHelpRow(Landroid/widget/LinearLayout;Landroid/view/ViewGroup;)Landroid/widget/LinearLayout;
    .locals 1

    if-nez p1, :cond_0

    .line 87
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/account/HardwarePeripheralListAdapter;->buildAndBindHelpRow(Landroid/widget/LinearLayout;Landroid/view/ViewGroup;)Landroid/widget/LinearLayout;

    move-result-object p1

    .line 88
    sget p2, Lcom/squareup/settingsapplet/R$id;->message_button:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/squareup/marketfont/MarketButton;

    const/4 v0, 0x0

    .line 89
    invoke-virtual {p2, v0}, Lcom/squareup/marketfont/MarketButton;->setVisibility(I)V

    .line 90
    sget v0, Lcom/squareup/settingsapplet/R$string;->cash_drawers_test_open:I

    invoke-virtual {p2, v0}, Lcom/squareup/marketfont/MarketButton;->setText(I)V

    .line 91
    new-instance v0, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsAdapter$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsAdapter$1;-><init>(Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsAdapter;)V

    invoke-virtual {p2, v0}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-object p1
.end method

.method protected buildAndBindItemRow(Lcom/squareup/ui/account/view/SmartLineRow;Lcom/squareup/cashdrawer/CashDrawer;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 0

    if-nez p1, :cond_0

    .line 72
    invoke-static {p3}, Lcom/squareup/ui/account/view/SmartLineRow;->inflateForListView(Landroid/view/ViewGroup;)Lcom/squareup/ui/account/view/SmartLineRow;

    move-result-object p1

    .line 75
    :cond_0
    invoke-interface {p2}, Lcom/squareup/cashdrawer/CashDrawer;->getName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleText(Ljava/lang/CharSequence;)V

    return-object p1
.end method

.method protected bridge synthetic buildAndBindItemRow(Lcom/squareup/ui/account/view/SmartLineRow;Ljava/lang/Object;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 0

    .line 25
    check-cast p2, Lcom/squareup/cashdrawer/CashDrawer;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsAdapter;->buildAndBindItemRow(Lcom/squareup/ui/account/view/SmartLineRow;Lcom/squareup/cashdrawer/CashDrawer;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method protected buildAndBindUnsupportedItemRow(Lcom/squareup/ui/account/view/SmartLineRow;Lcom/squareup/cashdrawer/CashDrawer;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 0

    .line 82
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Unable to show unsupported cash drawers."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method protected bridge synthetic buildAndBindUnsupportedItemRow(Lcom/squareup/ui/account/view/SmartLineRow;Ljava/lang/Object;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 0

    .line 25
    check-cast p2, Lcom/squareup/cashdrawer/CashDrawer;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsAdapter;->buildAndBindUnsupportedItemRow(Lcom/squareup/ui/account/view/SmartLineRow;Lcom/squareup/cashdrawer/CashDrawer;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public getAvailableCount()I
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsAdapter;->availableCashDrawers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/squareup/cashdrawer/CashDrawer;
    .locals 2

    .line 58
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsAdapter;->getItemViewType(I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsAdapter;->availableCashDrawers:Ljava/util/List;

    sub-int/2addr p1, v1

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cashdrawer/CashDrawer;

    return-object p1

    .line 61
    :cond_0
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Attempting to get a position that is not an ITEM_ROW."

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 0

    .line 25
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsAdapter;->getItem(I)Lcom/squareup/cashdrawer/CashDrawer;

    move-result-object p1

    return-object p1
.end method

.method protected getStrings()Lcom/squareup/ui/account/HardwarePeripheralListAdapter$StringHolder;
    .locals 1

    .line 102
    sget-object v0, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsAdapter;->STRINGS:Lcom/squareup/ui/account/HardwarePeripheralListAdapter$StringHolder;

    return-object v0
.end method

.method public getUnsupportedCount()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isEnabled(I)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method setCashDrawerList(Ljava/util/Collection;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/squareup/cashdrawer/CashDrawer;",
            ">;)V"
        }
    .end annotation

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsAdapter;->availableCashDrawers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 42
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cashdrawer/CashDrawer;

    .line 43
    iget-object v1, p0, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsAdapter;->availableCashDrawers:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 45
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsAdapter;->notifyDataSetChanged()V

    return-void
.end method
