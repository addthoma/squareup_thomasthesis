.class public Lcom/squareup/ui/settings/hardware/HardwareSettingsSectionAccess;
.super Lcom/squareup/applet/SectionAccess;
.source "HardwareSettingsSectionAccess.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nHardwareSettingsSectionAccess.kt\nKotlin\n*S Kotlin\n*F\n+ 1 HardwareSettingsSectionAccess.kt\ncom/squareup/ui/settings/hardware/HardwareSettingsSectionAccess\n*L\n1#1,31:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\"\n\u0002\u0008\u0006\u0008\u0016\u0018\u00002\u00020\u0001B!\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0012\u0010\u0004\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00060\u0005\"\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u000e\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00060\tH\u0016R!\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\u00060\t8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u000c\u0010\r\u001a\u0004\u0008\n\u0010\u000b\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/ui/settings/hardware/HardwareSettingsSectionAccess;",
        "Lcom/squareup/applet/SectionAccess;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "alternatePermissions",
        "",
        "Lcom/squareup/permissions/Permission;",
        "(Lcom/squareup/settings/server/Features;[Lcom/squareup/permissions/Permission;)V",
        "acceptablePermissions",
        "",
        "getAcceptablePermissions",
        "()Ljava/util/Set;",
        "acceptablePermissions$delegate",
        "Lkotlin/Lazy;",
        "getPermissions",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final acceptablePermissions$delegate:Lkotlin/Lazy;


# direct methods
.method public varargs constructor <init>(Lcom/squareup/settings/server/Features;[Lcom/squareup/permissions/Permission;)V
    .locals 1

    const-string v0, "features"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "alternatePermissions"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Lcom/squareup/applet/SectionAccess;-><init>()V

    .line 21
    new-instance v0, Lcom/squareup/ui/settings/hardware/HardwareSettingsSectionAccess$acceptablePermissions$2;

    invoke-direct {v0, p1, p2}, Lcom/squareup/ui/settings/hardware/HardwareSettingsSectionAccess$acceptablePermissions$2;-><init>(Lcom/squareup/settings/server/Features;[Lcom/squareup/permissions/Permission;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {v0}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/hardware/HardwareSettingsSectionAccess;->acceptablePermissions$delegate:Lkotlin/Lazy;

    return-void
.end method

.method private final getAcceptablePermissions()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/settings/hardware/HardwareSettingsSectionAccess;->acceptablePermissions$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method


# virtual methods
.method public getPermissions()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;"
        }
    .end annotation

    .line 29
    invoke-direct {p0}, Lcom/squareup/ui/settings/hardware/HardwareSettingsSectionAccess;->getAcceptablePermissions()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
