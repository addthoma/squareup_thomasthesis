.class public Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScopeRunner;
.super Ljava/lang/Object;
.source "LoyaltySettingsScopeRunner.java"

# interfaces
.implements Lmortar/Scoped;


# instance fields
.field private final flow:Lflow/Flow;

.field private final loyaltyFrontOfTransactionSetting:Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionSetting;

.field private final loyaltyLoggedInSettings:Lcom/squareup/loyalty/LoyaltySettings;

.field private screenData:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScreen$ScreenData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lflow/Flow;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionSetting;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScopeRunner;->screenData:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 26
    iput-object p1, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScopeRunner;->flow:Lflow/Flow;

    .line 27
    iput-object p2, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScopeRunner;->loyaltyLoggedInSettings:Lcom/squareup/loyalty/LoyaltySettings;

    .line 28
    iput-object p3, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScopeRunner;->loyaltyFrontOfTransactionSetting:Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionSetting;

    return-void
.end method

.method private updateScreenData()V
    .locals 13

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScopeRunner;->screenData:Lcom/jakewharton/rxrelay/BehaviorRelay;

    new-instance v9, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScreen$ScreenData;

    iget-object v1, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScopeRunner;->loyaltyLoggedInSettings:Lcom/squareup/loyalty/LoyaltySettings;

    .line 41
    invoke-interface {v1}, Lcom/squareup/loyalty/LoyaltySettings;->isLoyaltyScreensEnabled()Z

    move-result v2

    iget-object v1, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScopeRunner;->loyaltyLoggedInSettings:Lcom/squareup/loyalty/LoyaltySettings;

    .line 42
    invoke-interface {v1}, Lcom/squareup/loyalty/LoyaltySettings;->showNonQualifyingLoyaltyEvents()Z

    move-result v3

    iget-object v1, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScopeRunner;->loyaltyLoggedInSettings:Lcom/squareup/loyalty/LoyaltySettings;

    .line 43
    invoke-interface {v1}, Lcom/squareup/loyalty/LoyaltySettings;->getLoyaltyScreenTimeoutSeconds()J

    move-result-wide v4

    const/4 v1, 0x1

    const/4 v6, 0x0

    const-wide/16 v7, 0x1e

    cmp-long v10, v4, v7

    if-nez v10, :cond_0

    const/4 v4, 0x1

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    :goto_0
    iget-object v5, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScopeRunner;->loyaltyLoggedInSettings:Lcom/squareup/loyalty/LoyaltySettings;

    .line 44
    invoke-interface {v5}, Lcom/squareup/loyalty/LoyaltySettings;->getLoyaltyScreenTimeoutSeconds()J

    move-result-wide v7

    const-wide/16 v10, 0x3c

    cmp-long v5, v7, v10

    if-nez v5, :cond_1

    const/4 v5, 0x1

    goto :goto_1

    :cond_1
    const/4 v5, 0x0

    :goto_1
    iget-object v7, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScopeRunner;->loyaltyLoggedInSettings:Lcom/squareup/loyalty/LoyaltySettings;

    .line 45
    invoke-interface {v7}, Lcom/squareup/loyalty/LoyaltySettings;->getLoyaltyScreenTimeoutSeconds()J

    move-result-wide v7

    const-wide/16 v10, 0x5a

    cmp-long v12, v7, v10

    if-nez v12, :cond_2

    const/4 v6, 0x1

    :cond_2
    iget-object v1, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScopeRunner;->loyaltyFrontOfTransactionSetting:Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionSetting;

    .line 46
    invoke-interface {v1}, Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionSetting;->featureFlagOn()Z

    move-result v7

    iget-object v1, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScopeRunner;->loyaltyFrontOfTransactionSetting:Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionSetting;

    .line 47
    invoke-interface {v1}, Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionSetting;->getValue()Z

    move-result v8

    move-object v1, v9

    invoke-direct/range {v1 .. v8}, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScreen$ScreenData;-><init>(ZZZZZZZ)V

    .line 40
    invoke-virtual {v0, v9}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method exitScreen()V
    .locals 4

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScopeRunner;->flow:Lflow/Flow;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScreen;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    return-void
.end method

.method getScreenData()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScreen$ScreenData;",
            ">;"
        }
    .end annotation

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScopeRunner;->screenData:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    .line 32
    invoke-direct {p0}, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScopeRunner;->updateScreenData()V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method setFrontOfTransaction(Z)V
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScopeRunner;->loyaltyFrontOfTransactionSetting:Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionSetting;

    invoke-interface {v0, p1}, Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionSetting;->setValueLocallyInternal(Z)V

    .line 72
    invoke-direct {p0}, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScopeRunner;->updateScreenData()V

    return-void
.end method

.method setLoyaltyScreenEnabled(Z)V
    .locals 1

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScopeRunner;->loyaltyLoggedInSettings:Lcom/squareup/loyalty/LoyaltySettings;

    invoke-interface {v0, p1}, Lcom/squareup/loyalty/LoyaltySettings;->setLoyaltyScreensEnabled(Z)V

    .line 57
    invoke-direct {p0}, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScopeRunner;->updateScreenData()V

    return-void
.end method

.method setLoyaltyTimeout(J)V
    .locals 1

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScopeRunner;->loyaltyLoggedInSettings:Lcom/squareup/loyalty/LoyaltySettings;

    invoke-interface {v0, p1, p2}, Lcom/squareup/loyalty/LoyaltySettings;->setLoyaltyScreenTimeoutSeconds(J)V

    .line 67
    invoke-direct {p0}, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScopeRunner;->updateScreenData()V

    return-void
.end method

.method setShowNonQualifyingLoyaltyEvents(Z)V
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScopeRunner;->loyaltyLoggedInSettings:Lcom/squareup/loyalty/LoyaltySettings;

    invoke-interface {v0, p1}, Lcom/squareup/loyalty/LoyaltySettings;->setShowNonQualifyingLoyaltyEvents(Z)V

    .line 62
    invoke-direct {p0}, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScopeRunner;->updateScreenData()V

    return-void
.end method
