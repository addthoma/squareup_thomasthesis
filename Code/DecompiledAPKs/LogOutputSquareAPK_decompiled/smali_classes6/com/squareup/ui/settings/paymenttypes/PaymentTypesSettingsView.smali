.class public Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;
.super Landroid/widget/LinearLayout;
.source "PaymentTypesSettingsView.java"

# interfaces
.implements Lcom/squareup/ui/HasActionBar;


# instance fields
.field private customerCheckoutSettings:Landroid/view/View;

.field private instructions:Lcom/squareup/marketfont/MarketTextView;

.field presenter:Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private preview:Lcom/squareup/marketfont/MarketTextView;

.field private recyclerView:Landroidx/recyclerview/widget/RecyclerView;

.field private skipItemizedCart:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private skipPaymentTypeSelection:Lcom/squareup/widgets/list/ToggleButtonRow;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 36
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    const-class p2, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Component;->inject(Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;)Landroidx/recyclerview/widget/RecyclerView;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    return-object p0
.end method

.method private bindViews()V
    .locals 1

    .line 111
    sget v0, Lcom/squareup/settingsapplet/R$id;->payment_types_settings_instructions:I

    invoke-virtual {p0, v0}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;->instructions:Lcom/squareup/marketfont/MarketTextView;

    .line 112
    sget v0, Lcom/squareup/settingsapplet/R$id;->payment_types_settings_preview:I

    invoke-virtual {p0, v0}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;->preview:Lcom/squareup/marketfont/MarketTextView;

    .line 113
    sget v0, Lcom/squareup/settingsapplet/R$id;->payment_types_settings_recycler_view:I

    invoke-virtual {p0, v0}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 114
    sget v0, Lcom/squareup/settingsapplet/R$id;->skip_itemized_cart:I

    invoke-virtual {p0, v0}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;->skipItemizedCart:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 115
    sget v0, Lcom/squareup/settingsapplet/R$id;->skip_payment_type_selection:I

    invoke-virtual {p0, v0}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;->skipPaymentTypeSelection:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 116
    sget v0, Lcom/squareup/settingsapplet/R$id;->customer_checkout_settings_group:I

    invoke-virtual {p0, v0}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;->customerCheckoutSettings:Landroid/view/View;

    return-void
.end method


# virtual methods
.method public getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 107
    invoke-static {p0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onAttachedToWindow$0$PaymentTypesSettingsView(Landroid/view/View;)V
    .locals 0

    .line 46
    iget-object p1, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;->presenter:Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;->showPreview()V

    return-void
.end method

.method public synthetic lambda$onAttachedToWindow$1$PaymentTypesSettingsView(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 52
    iget-object p1, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;->presenter:Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;->setDefaultCustomerCheckoutEnabled(Z)V

    return-void
.end method

.method public synthetic lambda$onAttachedToWindow$2$PaymentTypesSettingsView(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 56
    iget-object p1, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;->presenter:Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;->setSkipCartScreenEnabled(Z)V

    return-void
.end method

.method public synthetic lambda$onAttachedToWindow$3$PaymentTypesSettingsView(Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;)Lrx/Subscription;
    .locals 2

    .line 97
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;->presenter:Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;->tenderUpdates()Lrx/Observable;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/settings/paymenttypes/-$$Lambda$lfst9xYZ1FhoF9224rwnjpkjk_4;

    invoke-direct {v1, p1}, Lcom/squareup/ui/settings/paymenttypes/-$$Lambda$lfst9xYZ1FhoF9224rwnjpkjk_4;-><init>(Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method protected onAttachedToWindow()V
    .locals 4

    .line 41
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 42
    invoke-direct {p0}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;->bindViews()V

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;->instructions:Lcom/squareup/marketfont/MarketTextView;

    sget v1, Lcom/squareup/settingsapplet/R$string;->payment_types_settings_instructions:I

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setText(I)V

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;->preview:Lcom/squareup/marketfont/MarketTextView;

    new-instance v1, Lcom/squareup/ui/settings/paymenttypes/-$$Lambda$PaymentTypesSettingsView$MbypDxpvDws-aI42A4j_2G1sOk4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/paymenttypes/-$$Lambda$PaymentTypesSettingsView$MbypDxpvDws-aI42A4j_2G1sOk4;-><init>(Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;->customerCheckoutSettings:Landroid/view/View;

    iget-object v1, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;->presenter:Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;->isCustomerCheckoutSettingsVisible()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;->skipPaymentTypeSelection:Lcom/squareup/widgets/list/ToggleButtonRow;

    iget-object v1, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;->presenter:Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;->isDefaultCustomerCheckoutEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;->skipPaymentTypeSelection:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v1, Lcom/squareup/ui/settings/paymenttypes/-$$Lambda$PaymentTypesSettingsView$HeM73xE1zLsr5UhHXJZl8R4kXYI;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/paymenttypes/-$$Lambda$PaymentTypesSettingsView$HeM73xE1zLsr5UhHXJZl8R4kXYI;-><init>(Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;->skipItemizedCart:Lcom/squareup/widgets/list/ToggleButtonRow;

    iget-object v1, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;->presenter:Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;->isSkipCartScreenEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;->skipItemizedCart:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v1, Lcom/squareup/ui/settings/paymenttypes/-$$Lambda$PaymentTypesSettingsView$-DIcrF6f_ABH01PnTxraFREh-Po;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/paymenttypes/-$$Lambda$PaymentTypesSettingsView$-DIcrF6f_ABH01PnTxraFREh-Po;-><init>(Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 58
    new-instance v0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;

    .line 59
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;->presenter:Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;

    invoke-virtual {v2}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;->getOtherTendersMap()Ljava/util/Map;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;->presenter:Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;

    .line 60
    invoke-virtual {v3}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;->atLeastOnePrimaryPaymentType()Z

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;-><init>(Landroid/content/Context;Ljava/util/Map;Z)V

    .line 62
    new-instance v1, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView$1;-><init>(Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->setListener(Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$Listener;)V

    .line 91
    iget-object v1, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    new-instance v2, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 92
    iget-object v1, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroidx/recyclerview/widget/RecyclerView;->setNestedScrollingEnabled(Z)V

    .line 93
    iget-object v1, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 94
    iget-object v1, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;->presenter:Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;->getTenderSettings()Lcom/squareup/protos/client/devicesettings/TenderSettings;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->setTenderSettings(Lcom/squareup/protos/client/devicesettings/TenderSettings;)V

    .line 95
    iget-object v1, v0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->itemTouchHelper:Landroidx/recyclerview/widget/ItemTouchHelper;

    iget-object v2, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v1, v2}, Landroidx/recyclerview/widget/ItemTouchHelper;->attachToRecyclerView(Landroidx/recyclerview/widget/RecyclerView;)V

    .line 97
    new-instance v1, Lcom/squareup/ui/settings/paymenttypes/-$$Lambda$PaymentTypesSettingsView$vq7ZydnoTZQTlup3vSgkuvIjYUk;

    invoke-direct {v1, p0, v0}, Lcom/squareup/ui/settings/paymenttypes/-$$Lambda$PaymentTypesSettingsView$vq7ZydnoTZQTlup3vSgkuvIjYUk;-><init>(Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;)V

    invoke-static {p0, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 98
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;->presenter:Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;->presenter:Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;->dropView(Landroid/view/ViewGroup;)V

    .line 103
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method
