.class public Lcom/squareup/ui/settings/SettingsAppletGateway$NoSettingsAppletGateway;
.super Ljava/lang/Object;
.source "SettingsAppletGateway.java"

# interfaces
.implements Lcom/squareup/ui/settings/SettingsAppletGateway;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/SettingsAppletGateway;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NoSettingsAppletGateway"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private noSettingsAppletAccess()V
    .locals 2

    .line 62
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Cannot launch the Settings Applet from device without a Settings Applet"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public activate()V
    .locals 0

    .line 43
    invoke-direct {p0}, Lcom/squareup/ui/settings/SettingsAppletGateway$NoSettingsAppletGateway;->noSettingsAppletAccess()V

    return-void
.end method

.method public activateBankAccountSettings()V
    .locals 0

    .line 50
    invoke-direct {p0}, Lcom/squareup/ui/settings/SettingsAppletGateway$NoSettingsAppletGateway;->noSettingsAppletAccess()V

    return-void
.end method

.method public activateDepositsSettings()V
    .locals 0

    .line 46
    invoke-direct {p0}, Lcom/squareup/ui/settings/SettingsAppletGateway$NoSettingsAppletGateway;->noSettingsAppletAccess()V

    return-void
.end method

.method public isBankAccountsVisible()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isInstantDepositsVisible()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
