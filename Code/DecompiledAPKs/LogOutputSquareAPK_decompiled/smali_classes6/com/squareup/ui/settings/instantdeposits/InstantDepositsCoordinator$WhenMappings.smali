.class public final synthetic Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I

.field public static final synthetic $EnumSwitchMapping$1:[I

.field public static final synthetic $EnumSwitchMapping$2:[I

.field public static final synthetic $EnumSwitchMapping$3:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 6

    invoke-static {}, Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;->values()[Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;->LOADING:Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;

    invoke-virtual {v1}, Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;->HAS_BANK_ACCOUNT:Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;

    invoke-virtual {v1}, Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;->ordinal()I

    move-result v1

    const/4 v3, 0x2

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;->NO_BANK_ACCOUNT:Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;

    invoke-virtual {v1}, Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;->ordinal()I

    move-result v1

    const/4 v4, 0x3

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;->COULD_NOT_LOAD:Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;

    invoke-virtual {v1}, Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;->ordinal()I

    move-result v1

    const/4 v5, 0x4

    aput v5, v0, v1

    invoke-static {}, Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;->values()[Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;->LOADING:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;

    invoke-virtual {v1}, Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;->FAILED:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;

    invoke-virtual {v1}, Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;->SUCCEEDED:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;

    invoke-virtual {v1}, Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;->ordinal()I

    move-result v1

    aput v4, v0, v1

    invoke-static {}, Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;->values()[Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;->NO_SELECTION:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;

    invoke-virtual {v1}, Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;->ONE_TO_TWO_BUSINESS_DAYS:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;

    invoke-virtual {v1}, Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;->SAME_DAY:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;

    invoke-virtual {v1}, Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;->ordinal()I

    move-result v1

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;->CUSTOM:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;

    invoke-virtual {v1}, Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;->ordinal()I

    move-result v1

    aput v5, v0, v1

    invoke-static {}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;->values()[Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v1, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;->VERIFIED:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v1, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;->VERIFICATION_EXPIRED:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v1, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;->VERIFICATION_PENDING:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;->ordinal()I

    move-result v1

    aput v4, v0, v1

    return-void
.end method
