.class Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "CardReadersCardScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;",
        ">;"
    }
.end annotation


# instance fields
.field private final apiReaderSettingsController:Lcom/squareup/api/ApiReaderSettingsController;

.field private final bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

.field private final cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

.field private final cardReaderPowerMonitor:Lcom/squareup/cardreader/CardReaderPowerMonitor;

.field private final features:Lcom/squareup/settings/server/Features;

.field private flow:Lflow/Flow;

.field private final readerStateList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/settings/paymentdevices/ReaderState;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method constructor <init>(Lcom/squareup/cardreader/BluetoothUtils;Lcom/squareup/cardreader/CardReaderPowerMonitor;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;Lflow/Flow;Lcom/squareup/api/ApiReaderSettingsController;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 78
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 79
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    .line 80
    iput-object p3, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;->cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    .line 81
    iput-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;->cardReaderPowerMonitor:Lcom/squareup/cardreader/CardReaderPowerMonitor;

    .line 82
    iput-object p4, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 83
    iput-object p5, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    .line 84
    iput-object p6, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;->flow:Lflow/Flow;

    .line 85
    iput-object p7, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;->apiReaderSettingsController:Lcom/squareup/api/ApiReaderSettingsController;

    .line 86
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;->readerStateList:Ljava/util/List;

    return-void
.end method

.method private buildDefaultConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 1

    .line 199
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 200
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hidePrimaryButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 201
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hideSecondaryButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    return-object v0
.end method

.method private getActionbarText()Ljava/lang/String;
    .locals 2

    .line 129
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/cardreader/ui/R$string;->square_readers:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private hasBluetoothPermission()Z
    .locals 2

    .line 195
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "android.permission.BLUETOOTH"

    invoke-virtual {v0, v1}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method static synthetic lambda$null$0(Lcom/squareup/ui/settings/paymentdevices/ReaderState;Lcom/squareup/ui/settings/paymentdevices/ReaderState;)I
    .locals 0

    .line 104
    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->serialNumberLast4:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->serialNumberLast4:Ljava/lang/String;

    invoke-static {p0, p1}, Lcom/squareup/util/Objects;->compare(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result p0

    return p0
.end method

.method public static synthetic lambda$qCGtkqzy8xacgfbi2uvsapUFRIs(Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;)V
    .locals 0

    invoke-direct {p0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;->onUpButtonClicked()V

    return-void
.end method

.method private onUpButtonClicked()V
    .locals 1

    .line 133
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;->apiReaderSettingsController:Lcom/squareup/api/ApiReaderSettingsController;

    invoke-virtual {v0}, Lcom/squareup/api/ApiReaderSettingsController;->handleReaderSettingsCanceled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 134
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    :cond_0
    return-void
.end method


# virtual methods
.method enableBleAndGo()V
    .locals 1

    .line 178
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    invoke-interface {v0}, Lcom/squareup/cardreader/BluetoothUtils;->enable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 179
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;->showReaderSdkR12PairingScreenOrThrowError()V

    :cond_0
    return-void
.end method

.method public synthetic lambda$null$1$CardReadersCardScreen$Presenter(Ljava/util/Collection;)V
    .locals 5

    .line 100
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;->readerStateList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 101
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;->readerStateList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 103
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;->readerStateList:Ljava/util/List;

    sget-object v0, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReadersCardScreen$Presenter$bHDPP_5gf416ngrQye5ka_HykZ4;->INSTANCE:Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReadersCardScreen$Presenter$bHDPP_5gf416ngrQye5ka_HykZ4;

    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 106
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;

    .line 107
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;->readerStateList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;->displayHeader(Z)V

    .line 108
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;->readerStateList:Ljava/util/List;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;->updateCardReaderList(Ljava/util/List;)V

    .line 109
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;->apiReaderSettingsController:Lcom/squareup/api/ApiReaderSettingsController;

    invoke-virtual {v0}, Lcom/squareup/api/ApiReaderSettingsController;->isApiReaderSettingsRequest()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 110
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->USE_R12:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    .line 111
    iget-object v2, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    invoke-interface {v2}, Lcom/squareup/cardreader/BluetoothUtils;->supportsBle()Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v0, :cond_0

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 113
    :goto_0
    invoke-direct {p0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;->hasBluetoothPermission()Z

    move-result v4

    if-eqz v4, :cond_1

    if-eqz v2, :cond_1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {p1, v1}, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;->setConnectReaderVisible(Z)V

    .line 114
    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;->displayFooter(Z)V

    :cond_2
    return-void
.end method

.method public synthetic lambda$onLoad$2$CardReadersCardScreen$Presenter()Lrx/Subscription;
    .locals 2

    .line 98
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;->cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->readerStates()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReadersCardScreen$Presenter$bSihyGcZViCkRt-e2NuncQvRGZ4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReadersCardScreen$Presenter$bSihyGcZViCkRt-e2NuncQvRGZ4;-><init>(Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;)V

    .line 99
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public onBackPressed()Z
    .locals 1

    .line 125
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;->apiReaderSettingsController:Lcom/squareup/api/ApiReaderSettingsController;

    invoke-virtual {v0}, Lcom/squareup/api/ApiReaderSettingsController;->handleReaderSettingsCanceled()Z

    move-result v0

    return v0
.end method

.method onConnectReaderClicked()V
    .locals 1

    .line 170
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    invoke-interface {v0}, Lcom/squareup/cardreader/BluetoothUtils;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 171
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;->showBluetoothRequiredDialog()V

    goto :goto_0

    .line 173
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;->showReaderSdkR12PairingScreenOrThrowError()V

    :goto_0
    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    .line 90
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 91
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;->cardReaderPowerMonitor:Lcom/squareup/cardreader/CardReaderPowerMonitor;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderPowerMonitor;->requestPowerStatusForAllReaders()V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 2

    .line 95
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 97
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReadersCardScreen$Presenter$8T5i0T59R4YUBPmB_JCv85Lw2oI;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReadersCardScreen$Presenter$8T5i0T59R4YUBPmB_JCv85Lw2oI;-><init>(Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 119
    invoke-direct {p0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;->buildDefaultConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-direct {p0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;->getActionbarText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReadersCardScreen$Presenter$qCGtkqzy8xacgfbi2uvsapUFRIs;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReadersCardScreen$Presenter$qCGtkqzy8xacgfbi2uvsapUFRIs;-><init>(Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;)V

    .line 120
    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 121
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method onReaderClicked(I)V
    .locals 3

    const/4 v0, 0x1

    sub-int/2addr p1, v0

    .line 140
    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;->readerStateList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    .line 142
    iget-object v1, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->type:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    sget-object v2, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_MIC_PERMISSION_MISSING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    if-ne v1, v2, :cond_0

    .line 143
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;->flow:Lflow/Flow;

    invoke-static {}, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen;->createAudioPermissionWithTitleText()Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen;

    move-result-object v0

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void

    .line 147
    :cond_0
    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$1;->$SwitchMap$com$squareup$protos$client$bills$CardData$ReaderType:[I

    invoke-virtual {p1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/protos/client/bills/CardData$ReaderType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 163
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 165
    invoke-virtual {p1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object p1

    aput-object p1, v0, v2

    const-string p1, "Unknown reader type %s, should we open a detail card?"

    .line 164
    invoke-static {p1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 160
    :pswitch_0
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen;

    invoke-direct {v1, p1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen;-><init>(Lcom/squareup/ui/settings/paymentdevices/ReaderState;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :pswitch_1
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method showReaderSdkR12PairingScreenOrThrowError()V
    .locals 2

    .line 184
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_R12:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 186
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/pairing/ReaderSdkR12PairingScreen;->INSTANCE:Lcom/squareup/ui/settings/paymentdevices/pairing/ReaderSdkR12PairingScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void

    .line 188
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Attempt to connect an unsupported reader. There should be no Connect Reader button visible - Reader SDK only supports R12 and the feature is disabled."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
