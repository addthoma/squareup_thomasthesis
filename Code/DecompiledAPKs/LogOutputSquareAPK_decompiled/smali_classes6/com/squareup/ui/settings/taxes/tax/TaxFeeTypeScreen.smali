.class public final Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeScreen;
.super Lcom/squareup/ui/settings/taxes/tax/InTaxScope;
.source "TaxFeeTypeScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/layer/InSection;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeScreen$Component;,
        Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 142
    sget-object v0, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$TaxFeeTypeScreen$Ejns9KsrmT12HSYO95UrQXBREcg;->INSTANCE:Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$TaxFeeTypeScreen$Ejns9KsrmT12HSYO95UrQXBREcg;

    .line 143
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 31
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/taxes/tax/InTaxScope;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeScreen;
    .locals 1

    .line 143
    new-instance v0, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeScreen;

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeScreen;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 138
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/settings/taxes/tax/InTaxScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 139
    iget-object p2, p0, Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeScreen;->cogsTaxId:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method

.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 35
    const-class v0, Lcom/squareup/ui/settings/taxes/TaxesSection;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 146
    sget v0, Lcom/squareup/settingsapplet/R$layout;->tax_fee_type_view:I

    return v0
.end method
