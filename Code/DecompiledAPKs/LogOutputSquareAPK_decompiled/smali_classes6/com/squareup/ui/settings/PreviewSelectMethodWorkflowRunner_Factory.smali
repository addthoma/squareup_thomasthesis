.class public final Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner_Factory;
.super Ljava/lang/Object;
.source "PreviewSelectMethodWorkflowRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final containerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderOptionListsFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/TenderOptionListsFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final viewFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/PaymentViewFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final workflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/PreviewSelectMethodScreenWorkflow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/PreviewSelectMethodScreenWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/PaymentViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/TenderOptionListsFactory;",
            ">;)V"
        }
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner_Factory;->workflowProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p2, p0, Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner_Factory;->viewFactoryProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p3, p0, Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner_Factory;->containerProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p4, p0, Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner_Factory;->tenderOptionListsFactoryProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/PreviewSelectMethodScreenWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/PaymentViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/TenderOptionListsFactory;",
            ">;)",
            "Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner_Factory;"
        }
    .end annotation

    .line 47
    new-instance v0, Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/tenderpayment/PreviewSelectMethodScreenWorkflow;Lcom/squareup/tenderpayment/PaymentViewFactory;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/checkoutflow/TenderOptionListsFactory;)Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner;
    .locals 1

    .line 53
    new-instance v0, Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner;-><init>(Lcom/squareup/tenderpayment/PreviewSelectMethodScreenWorkflow;Lcom/squareup/tenderpayment/PaymentViewFactory;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/checkoutflow/TenderOptionListsFactory;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner;
    .locals 4

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner_Factory;->workflowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tenderpayment/PreviewSelectMethodScreenWorkflow;

    iget-object v1, p0, Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner_Factory;->viewFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/tenderpayment/PaymentViewFactory;

    iget-object v2, p0, Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner_Factory;->containerProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/main/PosContainer;

    iget-object v3, p0, Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner_Factory;->tenderOptionListsFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/checkoutflow/TenderOptionListsFactory;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner_Factory;->newInstance(Lcom/squareup/tenderpayment/PreviewSelectMethodScreenWorkflow;Lcom/squareup/tenderpayment/PaymentViewFactory;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/checkoutflow/TenderOptionListsFactory;)Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner_Factory;->get()Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner;

    move-result-object v0

    return-object v0
.end method
