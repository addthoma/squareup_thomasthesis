.class public final Lcom/squareup/ui/settings/taxes/tax/TaxDetailView_MembersInjector;
.super Ljava/lang/Object;
.source "TaxDetailView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView_MembersInjector;->resProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView_MembersInjector;->analyticsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;",
            ">;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView_MembersInjector;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectAnalytics(Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;Lcom/squareup/analytics/Analytics;)V
    .locals 0

    .line 56
    iput-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;)V
    .locals 0

    .line 46
    iput-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->presenter:Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;

    return-void
.end method

.method public static injectRes(Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;Lcom/squareup/util/Res;)V
    .locals 0

    .line 51
    iput-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;->res:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;)V
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView_MembersInjector;->injectPresenter(Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;Lcom/squareup/ui/settings/taxes/tax/TaxDetailPresenter;)V

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView_MembersInjector;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    invoke-static {p1, v0}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView_MembersInjector;->injectRes(Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;Lcom/squareup/util/Res;)V

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView_MembersInjector;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/analytics/Analytics;

    invoke-static {p1, v0}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView_MembersInjector;->injectAnalytics(Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;Lcom/squareup/analytics/Analytics;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 10
    check-cast p1, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailView_MembersInjector;->injectMembers(Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;)V

    return-void
.end method
