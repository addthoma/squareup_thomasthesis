.class public Lcom/squareup/ui/tender/ChooseCardOnFileRowView;
.super Lcom/squareup/ui/tender/AbstractCardOnFileRowView;
.source "ChooseCardOnFileRowView.java"


# instance fields
.field presenter:Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 14
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/tender/AbstractCardOnFileRowView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 15
    const-class p2, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Component;->inject(Lcom/squareup/ui/tender/ChooseCardOnFileRowView;)V

    return-void
.end method


# virtual methods
.method protected onChargeCardOnFileClicked(ILjava/lang/String;)V
    .locals 0

    .line 20
    iget-object p2, p0, Lcom/squareup/ui/tender/ChooseCardOnFileRowView;->presenter:Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;

    invoke-virtual {p2, p1}, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->onCardOnFileChargeClicked(I)V

    return-void
.end method
