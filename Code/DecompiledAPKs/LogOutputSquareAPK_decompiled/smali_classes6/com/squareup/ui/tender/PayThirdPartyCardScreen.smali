.class public final Lcom/squareup/ui/tender/PayThirdPartyCardScreen;
.super Lcom/squareup/ui/tender/InTenderScope;
.source "PayThirdPartyCardScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/HasSoftInputModeForPhone;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/tender/PayThirdPartyCardScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/tender/PayThirdPartyCardScreen$Component;,
        Lcom/squareup/ui/tender/PayThirdPartyCardScreen$Module;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/tender/PayThirdPartyCardScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final note:Ljava/lang/String;

.field final type:Lcom/squareup/server/account/protos/OtherTenderType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 67
    sget-object v0, Lcom/squareup/ui/tender/-$$Lambda$PayThirdPartyCardScreen$wo-FsvSBLnRCOae9FelkUE3CJ48;->INSTANCE:Lcom/squareup/ui/tender/-$$Lambda$PayThirdPartyCardScreen$wo-FsvSBLnRCOae9FelkUE3CJ48;

    .line 68
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/tender/PayThirdPartyCardScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/server/account/protos/OtherTenderType;Ljava/lang/String;)V
    .locals 1

    .line 30
    sget-object v0, Lcom/squareup/ui/tender/TenderScope;->INSTANCE:Lcom/squareup/ui/tender/TenderScope;

    invoke-direct {p0, v0}, Lcom/squareup/ui/tender/InTenderScope;-><init>(Lcom/squareup/ui/tender/TenderScopeTreeKey;)V

    .line 31
    iput-object p1, p0, Lcom/squareup/ui/tender/PayThirdPartyCardScreen;->type:Lcom/squareup/server/account/protos/OtherTenderType;

    .line 32
    iput-object p2, p0, Lcom/squareup/ui/tender/PayThirdPartyCardScreen;->note:Ljava/lang/String;

    return-void
.end method

.method public static synthetic lambda$wo-FsvSBLnRCOae9FelkUE3CJ48(Landroid/os/Parcel;)Lcom/squareup/ui/tender/PayThirdPartyCardScreen;
    .locals 0

    invoke-static {p0}, Lcom/squareup/ui/tender/PayThirdPartyCardScreen;->readFromParcel(Landroid/os/Parcel;)Lcom/squareup/ui/tender/PayThirdPartyCardScreen;

    move-result-object p0

    return-object p0
.end method

.method private static readFromParcel(Landroid/os/Parcel;)Lcom/squareup/ui/tender/PayThirdPartyCardScreen;
    .locals 2

    .line 62
    new-instance v0, Lcom/squareup/ui/tender/PayThirdPartyCardScreen;

    const-class v1, Lcom/squareup/server/account/protos/OtherTenderType;

    .line 63
    invoke-static {p0, v1}, Lcom/squareup/util/Parcels;->readProtoMessage(Landroid/os/Parcel;Ljava/lang/Class;)Lcom/squareup/wire/Message;

    move-result-object v1

    check-cast v1, Lcom/squareup/server/account/protos/OtherTenderType;

    .line 64
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, v1, p0}, Lcom/squareup/ui/tender/PayThirdPartyCardScreen;-><init>(Lcom/squareup/server/account/protos/OtherTenderType;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 56
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/tender/InTenderScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 57
    iget-object p2, p0, Lcom/squareup/ui/tender/PayThirdPartyCardScreen;->type:Lcom/squareup/server/account/protos/OtherTenderType;

    invoke-static {p1, p2}, Lcom/squareup/util/Parcels;->writeProtoMessage(Landroid/os/Parcel;Lcom/squareup/wire/Message;)V

    .line 58
    iget-object p2, p0, Lcom/squareup/ui/tender/PayThirdPartyCardScreen;->note:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 36
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_THIRD_PARTY_CARD:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getSoftInputMode()Lcom/squareup/workflow/SoftInputMode;
    .locals 1

    .line 40
    sget-object v0, Lcom/squareup/workflow/SoftInputMode;->ALWAYS_SHOW_RESIZE:Lcom/squareup/workflow/SoftInputMode;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 71
    sget v0, Lcom/squareup/ui/tender/legacy/R$layout;->pay_third_party_card_view:I

    return v0
.end method
