.class public Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen;
.super Lcom/squareup/ui/tender/InTenderScope;
.source "ChooseCardOnFileConfirmationScreen.java"

# interfaces
.implements Lcom/squareup/container/MaybePersistent;


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen$Factory;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen$Component;,
        Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen$Presenter;,
        Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen$Factory;
    }
.end annotation


# instance fields
.field private final message:Ljava/lang/String;

.field private final selectedInstrument:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;Ljava/lang/String;)V
    .locals 1

    .line 36
    sget-object v0, Lcom/squareup/ui/tender/TenderScope;->INSTANCE:Lcom/squareup/ui/tender/TenderScope;

    invoke-direct {p0, v0}, Lcom/squareup/ui/tender/InTenderScope;-><init>(Lcom/squareup/ui/tender/TenderScopeTreeKey;)V

    .line 37
    iput-object p1, p0, Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen;->selectedInstrument:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;

    .line 38
    iput-object p2, p0, Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen;->message:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen;)Ljava/lang/String;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen;->message:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen;->selectedInstrument:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;

    return-object p0
.end method


# virtual methods
.method public isPersistent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
