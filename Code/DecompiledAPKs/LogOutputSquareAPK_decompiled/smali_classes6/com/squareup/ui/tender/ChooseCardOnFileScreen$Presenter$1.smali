.class Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter$1;
.super Lcom/squareup/permissions/PermissionGatekeeper$When;
.source "ChooseCardOnFileScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->onCardOnFileChargeClicked(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;

.field final synthetic val$message:Ljava/lang/String;

.field final synthetic val$selectedInstrument:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;


# direct methods
.method constructor <init>(Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;Ljava/lang/String;)V
    .locals 0

    .line 155
    iput-object p1, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter$1;->this$0:Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;

    iput-object p2, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter$1;->val$selectedInstrument:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;

    iput-object p3, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter$1;->val$message:Ljava/lang/String;

    invoke-direct {p0}, Lcom/squareup/permissions/PermissionGatekeeper$When;-><init>()V

    return-void
.end method


# virtual methods
.method public success()V
    .locals 4

    .line 157
    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter$1;->this$0:Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;

    invoke-static {v0}, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;->access$200(Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;)Lflow/Flow;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen;

    iget-object v2, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter$1;->val$selectedInstrument:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;

    iget-object v3, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter$1;->val$message:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen;-><init>(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method
