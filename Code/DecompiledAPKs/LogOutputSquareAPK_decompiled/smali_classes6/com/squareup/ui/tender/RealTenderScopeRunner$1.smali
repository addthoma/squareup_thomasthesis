.class Lcom/squareup/ui/tender/RealTenderScopeRunner$1;
.super Lcom/squareup/permissions/PermissionGatekeeper$When;
.source "RealTenderScopeRunner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/tender/RealTenderScopeRunner;->gatedCancelSplitTender()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/tender/RealTenderScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/tender/RealTenderScopeRunner;)V
    .locals 0

    .line 257
    iput-object p1, p0, Lcom/squareup/ui/tender/RealTenderScopeRunner$1;->this$0:Lcom/squareup/ui/tender/RealTenderScopeRunner;

    invoke-direct {p0}, Lcom/squareup/permissions/PermissionGatekeeper$When;-><init>()V

    return-void
.end method


# virtual methods
.method public success()V
    .locals 2

    .line 259
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderScopeRunner$1;->this$0:Lcom/squareup/ui/tender/RealTenderScopeRunner;

    sget-object v1, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_HUMAN_INITIATED:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/tender/RealTenderScopeRunner;->cancelPaymentAndExit(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    return-void
.end method
