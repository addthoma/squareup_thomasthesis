.class public abstract Lcom/squareup/ui/tender/ThirdPartyCardChargedScreen$Module;
.super Ljava/lang/Object;
.source "ThirdPartyCardChargedScreen.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/tender/ThirdPartyCardChargedScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Module"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract providePresenter(Lcom/squareup/ui/tender/ThirdPartyCardChargedPresenter;)Lcom/squareup/ui/tender/AbstractThirdPartyCardPresenter;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
