.class public Lcom/squareup/ui/tender/PayCardSwipeOnlyPresenter;
.super Lmortar/ViewPresenter;
.source "PayCardSwipeOnlyPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/tender/PayCardSwipeOnlyView;",
        ">;"
    }
.end annotation


# instance fields
.field private final bus:Lcom/squareup/badbus/BadBus;

.field private final emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

.field private final formatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final tenderScopeRunner:Lcom/squareup/tenderpayment/TenderScopeRunner;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/badbus/BadBus;Lcom/squareup/payment/Transaction;Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;Lcom/squareup/tenderpayment/TenderScopeRunner;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/badbus/BadBus;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;",
            "Lcom/squareup/tenderpayment/TenderScopeRunner;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 34
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/squareup/ui/tender/PayCardSwipeOnlyPresenter;->res:Lcom/squareup/util/Res;

    .line 36
    iput-object p2, p0, Lcom/squareup/ui/tender/PayCardSwipeOnlyPresenter;->formatter:Lcom/squareup/text/Formatter;

    .line 37
    iput-object p3, p0, Lcom/squareup/ui/tender/PayCardSwipeOnlyPresenter;->bus:Lcom/squareup/badbus/BadBus;

    .line 38
    iput-object p6, p0, Lcom/squareup/ui/tender/PayCardSwipeOnlyPresenter;->tenderScopeRunner:Lcom/squareup/tenderpayment/TenderScopeRunner;

    .line 39
    iput-object p4, p0, Lcom/squareup/ui/tender/PayCardSwipeOnlyPresenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 40
    iput-object p5, p0, Lcom/squareup/ui/tender/PayCardSwipeOnlyPresenter;->emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    return-void
.end method

.method public static synthetic lambda$ZcNX0EIsR9I0fcl5HNRcC4gEDlw(Lcom/squareup/ui/tender/PayCardSwipeOnlyPresenter;Lcom/squareup/payment/OfflineModeMonitor$OfflineModeChangeEvent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/tender/PayCardSwipeOnlyPresenter;->onAvailabilityChanged(Lcom/squareup/payment/OfflineModeMonitor$OfflineModeChangeEvent;)V

    return-void
.end method

.method private onAvailabilityChanged(Lcom/squareup/payment/OfflineModeMonitor$OfflineModeChangeEvent;)V
    .locals 0

    .line 66
    iget-boolean p1, p1, Lcom/squareup/payment/OfflineModeMonitor$OfflineModeChangeEvent;->isInOfflineMode:Z

    if-nez p1, :cond_0

    .line 67
    iget-object p1, p0, Lcom/squareup/ui/tender/PayCardSwipeOnlyPresenter;->tenderScopeRunner:Lcom/squareup/tenderpayment/TenderScopeRunner;

    invoke-interface {p1}, Lcom/squareup/tenderpayment/TenderScopeRunner;->availabilityOnline()V

    :cond_0
    return-void
.end method


# virtual methods
.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/tender/PayCardSwipeOnlyPresenter;->bus:Lcom/squareup/badbus/BadBus;

    const-class v1, Lcom/squareup/payment/OfflineModeMonitor$OfflineModeChangeEvent;

    .line 45
    invoke-virtual {v0, v1}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/tender/-$$Lambda$PayCardSwipeOnlyPresenter$ZcNX0EIsR9I0fcl5HNRcC4gEDlw;

    invoke-direct {v1, p0}, Lcom/squareup/ui/tender/-$$Lambda$PayCardSwipeOnlyPresenter$ZcNX0EIsR9I0fcl5HNRcC4gEDlw;-><init>(Lcom/squareup/ui/tender/PayCardSwipeOnlyPresenter;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 44
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/tender/PayCardSwipeOnlyPresenter;->emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;->registerDefaultEmvCardInsertRemoveProcessor(Lmortar/MortarScope;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 3

    .line 50
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 51
    iget-object p1, p0, Lcom/squareup/ui/tender/PayCardSwipeOnlyPresenter;->tenderScopeRunner:Lcom/squareup/tenderpayment/TenderScopeRunner;

    invoke-interface {p1}, Lcom/squareup/tenderpayment/TenderScopeRunner;->buildTenderActionBarConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/tender/PayCardSwipeOnlyPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getTenderAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 54
    iget-object v1, p0, Lcom/squareup/ui/tender/PayCardSwipeOnlyPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/ui/tender/legacy/R$string;->pay_card_swipe_title:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/tender/PayCardSwipeOnlyPresenter;->formatter:Lcom/squareup/text/Formatter;

    .line 55
    invoke-interface {v2, v0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    const-string v2, "amount"

    invoke-virtual {v1, v2, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 56
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 57
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 58
    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 59
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    .line 61
    invoke-virtual {p0}, Lcom/squareup/ui/tender/PayCardSwipeOnlyPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/tender/PayCardSwipeOnlyView;

    invoke-virtual {v0}, Lcom/squareup/ui/tender/PayCardSwipeOnlyView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method
