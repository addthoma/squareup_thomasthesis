.class public final Lcom/squareup/ui/tender/TenderPaymentLegacyModule_ProvidePayCashScreenIdentifierFactory;
.super Ljava/lang/Object;
.source "TenderPaymentLegacyModule_ProvidePayCashScreenIdentifierFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/tender/TenderPaymentLegacyModule_ProvidePayCashScreenIdentifierFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/tenderpayment/PayCashScreenIdentifier;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/ui/tender/TenderPaymentLegacyModule_ProvidePayCashScreenIdentifierFactory;
    .locals 1

    .line 23
    invoke-static {}, Lcom/squareup/ui/tender/TenderPaymentLegacyModule_ProvidePayCashScreenIdentifierFactory$InstanceHolder;->access$000()Lcom/squareup/ui/tender/TenderPaymentLegacyModule_ProvidePayCashScreenIdentifierFactory;

    move-result-object v0

    return-object v0
.end method

.method public static providePayCashScreenIdentifier()Lcom/squareup/tenderpayment/PayCashScreenIdentifier;
    .locals 2

    .line 27
    invoke-static {}, Lcom/squareup/ui/tender/TenderPaymentLegacyModule;->providePayCashScreenIdentifier()Lcom/squareup/tenderpayment/PayCashScreenIdentifier;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tenderpayment/PayCashScreenIdentifier;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/tenderpayment/PayCashScreenIdentifier;
    .locals 1

    .line 19
    invoke-static {}, Lcom/squareup/ui/tender/TenderPaymentLegacyModule_ProvidePayCashScreenIdentifierFactory;->providePayCashScreenIdentifier()Lcom/squareup/tenderpayment/PayCashScreenIdentifier;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ui/tender/TenderPaymentLegacyModule_ProvidePayCashScreenIdentifierFactory;->get()Lcom/squareup/tenderpayment/PayCashScreenIdentifier;

    move-result-object v0

    return-object v0
.end method
