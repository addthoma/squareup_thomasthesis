.class public final Lcom/squareup/ui/tender/ChooseCardOnFileScreen_Presenter_Factory;
.super Ljava/lang/Object;
.source "ChooseCardOnFileScreen_Presenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final cnpFeesMessageHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cnp/CnpFeesMessageHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final expirationHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/card/ExpirationHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final permissionGatekeeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderScopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/card/ExpirationHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cnp/CnpFeesMessageHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderScopeRunner;",
            ">;)V"
        }
    .end annotation

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p2, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen_Presenter_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p3, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen_Presenter_Factory;->transactionProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p4, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen_Presenter_Factory;->localeProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p5, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen_Presenter_Factory;->expirationHelperProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p6, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen_Presenter_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p7, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen_Presenter_Factory;->cnpFeesMessageHelperProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p8, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen_Presenter_Factory;->tenderScopeRunnerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/tender/ChooseCardOnFileScreen_Presenter_Factory;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/card/ExpirationHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cnp/CnpFeesMessageHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderScopeRunner;",
            ">;)",
            "Lcom/squareup/ui/tender/ChooseCardOnFileScreen_Presenter_Factory;"
        }
    .end annotation

    .line 67
    new-instance v9, Lcom/squareup/ui/tender/ChooseCardOnFileScreen_Presenter_Factory;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/ui/tender/ChooseCardOnFileScreen_Presenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v9
.end method

.method public static newInstance(Lcom/squareup/util/Res;Lflow/Flow;Lcom/squareup/payment/Transaction;Ljavax/inject/Provider;Lcom/squareup/card/ExpirationHelper;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/cnp/CnpFeesMessageHelper;Lcom/squareup/tenderpayment/TenderScopeRunner;)Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lflow/Flow;",
            "Lcom/squareup/payment/Transaction;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/card/ExpirationHelper;",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            "Lcom/squareup/cnp/CnpFeesMessageHelper;",
            "Lcom/squareup/tenderpayment/TenderScopeRunner;",
            ")",
            "Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;"
        }
    .end annotation

    .line 74
    new-instance v9, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;-><init>(Lcom/squareup/util/Res;Lflow/Flow;Lcom/squareup/payment/Transaction;Ljavax/inject/Provider;Lcom/squareup/card/ExpirationHelper;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/cnp/CnpFeesMessageHelper;Lcom/squareup/tenderpayment/TenderScopeRunner;)V

    return-object v9
.end method


# virtual methods
.method public get()Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;
    .locals 9

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen_Presenter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lflow/Flow;

    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen_Presenter_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/payment/Transaction;

    iget-object v4, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen_Presenter_Factory;->localeProvider:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen_Presenter_Factory;->expirationHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/card/ExpirationHelper;

    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen_Presenter_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/permissions/PermissionGatekeeper;

    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen_Presenter_Factory;->cnpFeesMessageHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/cnp/CnpFeesMessageHelper;

    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen_Presenter_Factory;->tenderScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/tenderpayment/TenderScopeRunner;

    invoke-static/range {v1 .. v8}, Lcom/squareup/ui/tender/ChooseCardOnFileScreen_Presenter_Factory;->newInstance(Lcom/squareup/util/Res;Lflow/Flow;Lcom/squareup/payment/Transaction;Ljavax/inject/Provider;Lcom/squareup/card/ExpirationHelper;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/cnp/CnpFeesMessageHelper;Lcom/squareup/tenderpayment/TenderScopeRunner;)Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/squareup/ui/tender/ChooseCardOnFileScreen_Presenter_Factory;->get()Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;

    move-result-object v0

    return-object v0
.end method
