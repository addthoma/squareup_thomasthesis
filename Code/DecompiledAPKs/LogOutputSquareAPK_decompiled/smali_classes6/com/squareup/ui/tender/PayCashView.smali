.class public Lcom/squareup/ui/tender/PayCashView;
.super Lcom/squareup/noho/NohoResponsiveLinearLayout;
.source "PayCashView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;
.implements Lcom/squareup/ui/tender/PayCashPresenter$View;


# instance fields
.field private actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

.field analytics:Lcom/squareup/analytics/Analytics;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private cashAmount:Lcom/squareup/widgets/SelectableEditText;

.field private didFirstUpdate:Z

.field moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field moneyLocaleHelper:Lcom/squareup/money/MoneyLocaleHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field presenter:Lcom/squareup/ui/tender/PayCashPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private tenderButton:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 47
    invoke-direct {p0, p1, p2}, Lcom/squareup/noho/NohoResponsiveLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    const-class p2, Lcom/squareup/ui/tender/PayCashScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/tender/PayCashScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/tender/PayCashScreen$Component;->inject(Lcom/squareup/ui/tender/PayCashView;)V

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 144
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/tender/PayCashView;->actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

    .line 145
    sget v0, Lcom/squareup/ui/tender/legacy/R$id;->pay_cash_amount:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SelectableEditText;

    iput-object v0, p0, Lcom/squareup/ui/tender/PayCashView;->cashAmount:Lcom/squareup/widgets/SelectableEditText;

    .line 146
    sget v0, Lcom/squareup/ui/tender/legacy/R$id;->pay_cash_tender_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/tender/PayCashView;->tenderButton:Landroid/view/View;

    return-void
.end method

.method private setMax(Lcom/squareup/protos/common/Money;)V
    .locals 4

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/tender/PayCashView;->moneyLocaleHelper:Lcom/squareup/money/MoneyLocaleHelper;

    iget-object v1, p0, Lcom/squareup/ui/tender/PayCashView;->cashAmount:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0, v1}, Lcom/squareup/money/MoneyLocaleHelper;->configure(Lcom/squareup/text/HasSelectableText;)Lcom/squareup/text/ScrubbingTextWatcher;

    move-result-object v0

    .line 113
    new-instance v1, Lcom/squareup/money/MaxMoneyScrubber;

    iget-object v2, p0, Lcom/squareup/ui/tender/PayCashView;->moneyLocaleHelper:Lcom/squareup/money/MoneyLocaleHelper;

    iget-object v3, p0, Lcom/squareup/ui/tender/PayCashView;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-direct {v1, v2, v3, p1}, Lcom/squareup/money/MaxMoneyScrubber;-><init>(Lcom/squareup/money/MoneyExtractor;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/Money;)V

    .line 115
    invoke-virtual {v0, v1}, Lcom/squareup/text/ScrubbingTextWatcher;->addScrubber(Lcom/squareup/text/Scrubber;)V

    return-void
.end method

.method private updateDefaultCustomAmount(Lcom/squareup/protos/common/Money;)V
    .locals 2

    .line 119
    iget-object v0, p0, Lcom/squareup/ui/tender/PayCashView;->cashAmount:Lcom/squareup/widgets/SelectableEditText;

    iget-object v1, p0, Lcom/squareup/ui/tender/PayCashView;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {v1, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/SelectableEditText;->setHint(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private updateTenderButton(Z)V
    .locals 1

    .line 123
    iget-object v0, p0, Lcom/squareup/ui/tender/PayCashView;->tenderButton:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    return-void
.end method


# virtual methods
.method public getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 140
    iget-object v0, p0, Lcom/squareup/ui/tender/PayCashView;->actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method public getTenderedAmount()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 127
    iget-object v0, p0, Lcom/squareup/ui/tender/PayCashView;->moneyLocaleHelper:Lcom/squareup/money/MoneyLocaleHelper;

    iget-object v1, p0, Lcom/squareup/ui/tender/PayCashView;->cashAmount:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v1}, Lcom/squareup/widgets/SelectableEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/money/MoneyLocaleHelper;->extractMoney(Ljava/lang/CharSequence;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public hideSoftKeyboard()V
    .locals 0

    .line 131
    invoke-static {p0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    return-void
.end method

.method public synthetic lambda$onAttachedToWindow$0$PayCashView(Landroid/view/View;)V
    .locals 0

    .line 79
    iget-object p1, p0, Lcom/squareup/ui/tender/PayCashView;->presenter:Lcom/squareup/ui/tender/PayCashPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/tender/PayCashPresenter;->onTenderButtonClicked()V

    return-void
.end method

.method public synthetic lambda$requestInitialFocus$1$PayCashView()V
    .locals 1

    .line 96
    iget-object v0, p0, Lcom/squareup/ui/tender/PayCashView;->cashAmount:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0}, Lcom/squareup/widgets/SelectableEditText;->requestFocus()Z

    const/4 v0, 0x1

    .line 97
    invoke-static {p0, v0}, Lcom/squareup/util/Views;->showSoftKeyboard(Landroid/view/View;Z)V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 3

    .line 52
    invoke-super {p0}, Lcom/squareup/noho/NohoResponsiveLinearLayout;->onAttachedToWindow()V

    .line 53
    invoke-direct {p0}, Lcom/squareup/ui/tender/PayCashView;->bindViews()V

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/tender/PayCashView;->cashAmount:Lcom/squareup/widgets/SelectableEditText;

    invoke-static {v0}, Lcom/squareup/util/Views;->showSoftKeyboard(Landroid/view/View;)V

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/tender/PayCashView;->cashAmount:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {p0}, Lcom/squareup/ui/tender/PayCashView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/ui/tender/legacy/R$string;->pay_cash_tender_button:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2}, Lcom/squareup/widgets/SelectableEditText;->setImeActionLabel(Ljava/lang/CharSequence;I)V

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/tender/PayCashView;->cashAmount:Lcom/squareup/widgets/SelectableEditText;

    new-instance v1, Lcom/squareup/ui/tender/PayCashView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/tender/PayCashView$1;-><init>(Lcom/squareup/ui/tender/PayCashView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/tender/PayCashView;->cashAmount:Lcom/squareup/widgets/SelectableEditText;

    new-instance v1, Lcom/squareup/ui/tender/PayCashView$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/tender/PayCashView$2;-><init>(Lcom/squareup/ui/tender/PayCashView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/tender/PayCashView;->tenderButton:Landroid/view/View;

    new-instance v1, Lcom/squareup/ui/tender/-$$Lambda$PayCashView$PZ4R9MRSu0inras_fk23BeLSfaM;

    invoke-direct {v1, p0}, Lcom/squareup/ui/tender/-$$Lambda$PayCashView$PZ4R9MRSu0inras_fk23BeLSfaM;-><init>(Lcom/squareup/ui/tender/PayCashView;)V

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/tender/PayCashView;->presenter:Lcom/squareup/ui/tender/PayCashPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/tender/PayCashPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 90
    invoke-virtual {p0}, Lcom/squareup/ui/tender/PayCashView;->hideSoftKeyboard()V

    const/4 v0, 0x0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/tender/PayCashView;->presenter:Lcom/squareup/ui/tender/PayCashPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/tender/PayCashPresenter;->dropView(Ljava/lang/Object;)V

    .line 86
    invoke-super {p0}, Lcom/squareup/noho/NohoResponsiveLinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1

    .line 135
    iget-object v0, p0, Lcom/squareup/ui/tender/PayCashView;->presenter:Lcom/squareup/ui/tender/PayCashPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/tender/PayCashPresenter;->onWindowFocusChanged(Z)V

    .line 136
    invoke-super {p0, p1}, Lcom/squareup/noho/NohoResponsiveLinearLayout;->onWindowFocusChanged(Z)V

    return-void
.end method

.method public requestInitialFocus()V
    .locals 2

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/tender/PayCashView;->cashAmount:Lcom/squareup/widgets/SelectableEditText;

    new-instance v1, Lcom/squareup/ui/tender/-$$Lambda$PayCashView$hLk7_2JgT6Cx0bYGLGIjelCOKpk;

    invoke-direct {v1, p0}, Lcom/squareup/ui/tender/-$$Lambda$PayCashView$hLk7_2JgT6Cx0bYGLGIjelCOKpk;-><init>(Lcom/squareup/ui/tender/PayCashView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public update(Lcom/squareup/ui/tender/PayCashPresenter$ScreenData;)V
    .locals 1

    .line 102
    iget-boolean v0, p0, Lcom/squareup/ui/tender/PayCashView;->didFirstUpdate:Z

    if-nez v0, :cond_0

    .line 103
    iget-object v0, p1, Lcom/squareup/ui/tender/PayCashPresenter$ScreenData;->maxTender:Lcom/squareup/protos/common/Money;

    invoke-direct {p0, v0}, Lcom/squareup/ui/tender/PayCashView;->setMax(Lcom/squareup/protos/common/Money;)V

    .line 104
    iget-object v0, p1, Lcom/squareup/ui/tender/PayCashPresenter$ScreenData;->amountDue:Lcom/squareup/protos/common/Money;

    invoke-direct {p0, v0}, Lcom/squareup/ui/tender/PayCashView;->updateDefaultCustomAmount(Lcom/squareup/protos/common/Money;)V

    .line 107
    :cond_0
    iget-boolean p1, p1, Lcom/squareup/ui/tender/PayCashPresenter$ScreenData;->enableTenderButton:Z

    invoke-direct {p0, p1}, Lcom/squareup/ui/tender/PayCashView;->updateTenderButton(Z)V

    const/4 p1, 0x1

    .line 108
    iput-boolean p1, p0, Lcom/squareup/ui/tender/PayCashView;->didFirstUpdate:Z

    return-void
.end method
