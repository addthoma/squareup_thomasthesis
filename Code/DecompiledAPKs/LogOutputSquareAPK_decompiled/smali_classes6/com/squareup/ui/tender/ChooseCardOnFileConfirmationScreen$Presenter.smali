.class public Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen$Presenter;
.super Ljava/lang/Object;
.source "ChooseCardOnFileConfirmationScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation


# instance fields
.field analytics:Lcom/squareup/analytics/Analytics;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field customerSettings:Lcom/squareup/crm/CustomerManagementSettings;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private screen:Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen;

.field settings:Lcom/squareup/settings/server/AccountStatusSettings;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field tenderCompleter:Lcom/squareup/tenderpayment/TenderCompleter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field tenderInEdit:Lcom/squareup/payment/TenderInEdit;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field tenderScopeRunner:Lcom/squareup/tenderpayment/TenderScopeRunner;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field transaction:Lcom/squareup/payment/Transaction;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen;

    iput-object p1, p0, Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen$Presenter;->screen:Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen;

    return-void
.end method


# virtual methods
.method public getMessage()Ljava/lang/String;
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen$Presenter;->screen:Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen;

    invoke-static {v0}, Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen;->access$000(Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onConfirm()V
    .locals 3

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen$Presenter;->tenderScopeRunner:Lcom/squareup/tenderpayment/TenderScopeRunner;

    iget-object v1, p0, Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen$Presenter;->tenderCompleter:Lcom/squareup/tenderpayment/TenderCompleter;

    iget-object v2, p0, Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen$Presenter;->screen:Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen;

    .line 84
    invoke-static {v2}, Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen;->access$100(Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/tenderpayment/TenderCompleter;->chargeCardOnFile(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    move-result-object v1

    .line 83
    invoke-interface {v0, v1}, Lcom/squareup/tenderpayment/TenderScopeRunner;->advanceToNextFlow(Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;)Z

    return-void
.end method
