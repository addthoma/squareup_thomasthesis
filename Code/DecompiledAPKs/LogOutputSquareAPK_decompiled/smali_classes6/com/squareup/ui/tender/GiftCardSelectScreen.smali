.class public final Lcom/squareup/ui/tender/GiftCardSelectScreen;
.super Lcom/squareup/ui/tender/InTenderScope;
.source "GiftCardSelectScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/tender/GiftCardSelectScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/tender/GiftCardSelectScreen$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/tender/GiftCardSelectScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/tender/GiftCardSelectScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 13
    new-instance v0, Lcom/squareup/ui/tender/GiftCardSelectScreen;

    invoke-direct {v0}, Lcom/squareup/ui/tender/GiftCardSelectScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/tender/GiftCardSelectScreen;->INSTANCE:Lcom/squareup/ui/tender/GiftCardSelectScreen;

    .line 25
    sget-object v0, Lcom/squareup/ui/tender/GiftCardSelectScreen;->INSTANCE:Lcom/squareup/ui/tender/GiftCardSelectScreen;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/tender/GiftCardSelectScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 16
    sget-object v0, Lcom/squareup/ui/tender/TenderScope;->INSTANCE:Lcom/squareup/ui/tender/TenderScope;

    invoke-direct {p0, v0}, Lcom/squareup/ui/tender/InTenderScope;-><init>(Lcom/squareup/ui/tender/TenderScopeTreeKey;)V

    return-void
.end method


# virtual methods
.method public screenLayout()I
    .locals 1

    .line 28
    sget v0, Lcom/squareup/ui/tender/legacy/R$layout;->gift_card_select_screen_view:I

    return v0
.end method
