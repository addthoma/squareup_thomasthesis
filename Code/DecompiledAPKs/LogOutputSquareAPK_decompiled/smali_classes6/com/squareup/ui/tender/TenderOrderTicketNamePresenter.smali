.class public Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;
.super Lmortar/ViewPresenter;
.source "TenderOrderTicketNamePresenter.java"

# interfaces
.implements Lcom/squareup/ui/seller/SwipedCardHandler;
.implements Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;
.implements Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter$CardholderNameListener;,
        Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter$NfcTenderDisplay;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/tender/TenderOrderTicketNameView;",
        ">;",
        "Lcom/squareup/ui/seller/SwipedCardHandler;",
        "Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;",
        "Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;"
    }
.end annotation


# instance fields
.field private final activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

.field private final buyerFlowStarter:Lcom/squareup/ui/buyer/BuyerFlowStarter;

.field private final cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

.field private cardReaderId:Lcom/squareup/cardreader/CardReaderId;

.field private cardholderName:Ljava/lang/String;

.field private final cardholderNameProcessor:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;

.field private final device:Lcom/squareup/util/Device;

.field private final emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

.field private final emvPaymentStarter:Lcom/squareup/ui/buyer/emv/EmvPaymentStarter;

.field private final hubScoper:Lcom/squareup/cardreader/dipper/CardReaderHubScoper;

.field private final hudToaster:Lcom/squareup/hudtoaster/HudToaster;

.field private final nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

.field private final nfcProcessor:Lcom/squareup/ui/NfcProcessor;

.field private final paymentCounter:Lcom/squareup/cardreader/PaymentCounter;

.field private final readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

.field private final res:Lcom/squareup/util/Res;

.field private final smartPaymentFlowStarter:Lcom/squareup/ui/main/SmartPaymentFlowStarter;

.field private final swipeHandler:Lcom/squareup/ui/payment/SwipeHandler;

.field private final tenderScopeRunner:Lcom/squareup/tenderpayment/TenderScopeRunner;

.field private final tipDeterminerFactory:Lcom/squareup/payment/TipDeterminerFactory;

.field private final transaction:Lcom/squareup/payment/Transaction;

.field private final x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;


# direct methods
.method constructor <init>(Lcom/squareup/util/Device;Lcom/squareup/util/Res;Lcom/squareup/payment/Transaction;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/ui/payment/SwipeHandler;Lcom/squareup/cardreader/PaymentCounter;Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;Lcom/squareup/cardreader/dipper/CardReaderHubScoper;Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;Lcom/squareup/ui/main/SmartPaymentFlowStarter;Lcom/squareup/ui/buyer/emv/EmvPaymentStarter;Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/payment/TipDeterminerFactory;Lcom/squareup/ui/NfcProcessor;Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/ui/buyer/BuyerFlowStarter;Lcom/squareup/tenderpayment/TenderScopeRunner;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    .line 104
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    move-object v1, p1

    .line 105
    iput-object v1, v0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->device:Lcom/squareup/util/Device;

    move-object v1, p2

    .line 106
    iput-object v1, v0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->res:Lcom/squareup/util/Res;

    move-object v1, p3

    .line 107
    iput-object v1, v0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->transaction:Lcom/squareup/payment/Transaction;

    move-object v1, p4

    .line 108
    iput-object v1, v0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    move-object v1, p5

    .line 109
    iput-object v1, v0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    move-object v1, p6

    .line 110
    iput-object v1, v0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->swipeHandler:Lcom/squareup/ui/payment/SwipeHandler;

    move-object v1, p7

    .line 111
    iput-object v1, v0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->paymentCounter:Lcom/squareup/cardreader/PaymentCounter;

    move-object v1, p8

    .line 112
    iput-object v1, v0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    move-object v1, p9

    .line 113
    iput-object v1, v0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->hubScoper:Lcom/squareup/cardreader/dipper/CardReaderHubScoper;

    move-object v1, p10

    .line 114
    iput-object v1, v0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->cardholderNameProcessor:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;

    move-object v1, p11

    .line 115
    iput-object v1, v0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->smartPaymentFlowStarter:Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    move-object v1, p12

    .line 116
    iput-object v1, v0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->emvPaymentStarter:Lcom/squareup/ui/buyer/emv/EmvPaymentStarter;

    move-object v1, p13

    .line 117
    iput-object v1, v0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    move-object/from16 v1, p14

    .line 118
    iput-object v1, v0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    move-object/from16 v1, p15

    .line 119
    iput-object v1, v0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->tipDeterminerFactory:Lcom/squareup/payment/TipDeterminerFactory;

    move-object/from16 v1, p16

    .line 120
    iput-object v1, v0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    move-object/from16 v1, p17

    .line 121
    iput-object v1, v0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

    move-object/from16 v1, p18

    .line 122
    iput-object v1, v0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    move-object/from16 v1, p19

    .line 123
    iput-object v1, v0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->buyerFlowStarter:Lcom/squareup/ui/buyer/BuyerFlowStarter;

    move-object/from16 v1, p20

    .line 124
    iput-object v1, v0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->tenderScopeRunner:Lcom/squareup/tenderpayment/TenderScopeRunner;

    return-void
.end method

.method static synthetic access$200(Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;)Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;
    .locals 0

    .line 67
    iget-object p0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    return-object p0
.end method

.method private handleX2OrderTicketNameResult(Lcom/squareup/x2/tender/X2OrderTicketNameResult;)V
    .locals 3

    .line 416
    sget-object v0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter$1;->$SwitchMap$com$squareup$x2$tender$X2OrderTicketNameResult$State:[I

    invoke-virtual {p1}, Lcom/squareup/x2/tender/X2OrderTicketNameResult;->getState()Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x0

    packed-switch v0, :pswitch_data_0

    .line 455
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Unknown X2 card state."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 450
    :pswitch_0
    invoke-virtual {p0}, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/tender/TenderOrderTicketNameView;

    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/cardreader/ui/R$string;->emv_card_removed_title:I

    .line 451
    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 450
    invoke-virtual {p1, v0}, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->setCardholderNameStatus(Ljava/lang/String;)V

    .line 452
    invoke-virtual {p0}, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/tender/TenderOrderTicketNameView;

    invoke-virtual {p1, v1}, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->setProgressVisible(Z)V

    goto/16 :goto_0

    .line 444
    :pswitch_1
    invoke-virtual {p0}, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/tender/TenderOrderTicketNameView;

    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/checkout/R$string;->kitchen_printing_cardholder_name_retrieving:I

    .line 445
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 444
    invoke-virtual {p1, v0}, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->setCardholderNameStatus(Ljava/lang/String;)V

    .line 447
    invoke-virtual {p0}, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/tender/TenderOrderTicketNameView;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->setProgressVisible(Z)V

    goto :goto_0

    .line 435
    :pswitch_2
    iget-object p1, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_CHIP_CARD_NOT_USABLE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v2, Lcom/squareup/opentickets/R$string;->open_tickets_unable_to_read:I

    invoke-interface {p1, v0, v2, v1}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/glyph/GlyphTypeface$Glyph;II)Z

    .line 438
    invoke-virtual {p0}, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/tender/TenderOrderTicketNameView;

    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/checkout/R$string;->kitchen_printing_cardholder_name_unavailable:I

    .line 439
    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 438
    invoke-virtual {p1, v0}, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->setCardholderNameStatus(Ljava/lang/String;)V

    .line 441
    invoke-virtual {p0}, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/tender/TenderOrderTicketNameView;

    invoke-virtual {p1, v1}, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->setProgressVisible(Z)V

    goto :goto_0

    .line 428
    :pswitch_3
    iget-object p1, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v0, Lcom/squareup/ui/cardreader/HudToasts;->SWIPE_FAILED_SWIPE_STRAIGHT:Lcom/squareup/ui/cardreader/HudToasts;

    invoke-interface {p1, v0}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/hudtoaster/HudToaster$ToastBundle;)Z

    .line 429
    invoke-virtual {p0}, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/tender/TenderOrderTicketNameView;

    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/checkout/R$string;->kitchen_printing_cardholder_name_unavailable:I

    .line 430
    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 429
    invoke-virtual {p1, v0}, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->setCardholderNameStatus(Ljava/lang/String;)V

    .line 432
    invoke-virtual {p0}, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/tender/TenderOrderTicketNameView;

    invoke-virtual {p1, v1}, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->setProgressVisible(Z)V

    goto :goto_0

    .line 421
    :pswitch_4
    iget-object p1, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v0, Lcom/squareup/ui/cardreader/HudToasts;->SWIPE_FAILED_TRY_AGAIN:Lcom/squareup/ui/cardreader/HudToasts;

    invoke-interface {p1, v0}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/hudtoaster/HudToaster$ToastBundle;)Z

    .line 422
    invoke-virtual {p0}, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/tender/TenderOrderTicketNameView;

    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/checkout/R$string;->kitchen_printing_cardholder_name_unavailable:I

    .line 423
    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 422
    invoke-virtual {p1, v0}, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->setCardholderNameStatus(Ljava/lang/String;)V

    .line 425
    invoke-virtual {p0}, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/tender/TenderOrderTicketNameView;

    invoke-virtual {p1, v1}, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->setProgressVisible(Z)V

    goto :goto_0

    .line 418
    :pswitch_5
    invoke-virtual {p1}, Lcom/squareup/x2/tender/X2OrderTicketNameResult;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->processNameReceived(Ljava/lang/String;)V

    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static synthetic lambda$OkrtEfcG2YYl3BgzPj1qPpU_L1g(Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;Lcom/squareup/x2/tender/X2OrderTicketNameResult;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->handleX2OrderTicketNameResult(Lcom/squareup/x2/tender/X2OrderTicketNameResult;)V

    return-void
.end method

.method private resetTransaction(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V
    .locals 2

    .line 360
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->hasActiveCardReader()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 363
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReader()Lcom/squareup/cardreader/CardReader;

    move-result-object v0

    .line 364
    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/cardreader/CardReaderInfo;->isInPayment()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 365
    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->cancelPayment()V

    .line 367
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasBillPayment()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    .line 368
    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->hasSmartCardTenderWithCaptureArgs()Z

    move-result v0

    if-nez v0, :cond_2

    .line 370
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->transaction:Lcom/squareup/payment/Transaction;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Lcom/squareup/payment/Transaction;->dropPaymentOrTender(ZLcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    :cond_2
    return-void
.end method

.method private setNameOnTextField(Ljava/lang/String;)V
    .locals 1

    .line 409
    invoke-virtual {p0}, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/tender/TenderOrderTicketNameView;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->setOrderTicketName(Ljava/lang/String;)V

    .line 410
    invoke-virtual {p0}, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/tender/TenderOrderTicketNameView;

    invoke-virtual {p1}, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->selectAllText()V

    .line 411
    invoke-virtual {p0}, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/tender/TenderOrderTicketNameView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->setCardholderClickable(Z)V

    .line 412
    invoke-virtual {p0}, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/tender/TenderOrderTicketNameView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->setCardholderNameStatus(Ljava/lang/String;)V

    return-void
.end method

.method private setUpView()V
    .locals 4

    .line 177
    invoke-virtual {p0}, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/tender/TenderOrderTicketNameView;

    .line 180
    iget-object v1, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getCustomerContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->res:Lcom/squareup/util/Res;

    invoke-static {v1, v2}, Lcom/squareup/crm/util/RolodexContactHelper;->getFullNameOrNull(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v1

    .line 182
    iget-object v2, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v2}, Lcom/squareup/payment/Transaction;->hasOrderTicketName()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 184
    iget-object v2, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v2}, Lcom/squareup/payment/Transaction;->getOrderTicketName()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_0
    if-eqz v1, :cond_1

    move-object v2, v1

    goto :goto_1

    .line 188
    :cond_1
    iget-object v2, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v2}, Lcom/squareup/payment/Transaction;->hasCard()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 191
    iget-object v2, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->res:Lcom/squareup/util/Res;

    iget-object v3, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->getCard()Lcom/squareup/Card;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/Card;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/squareup/text/Cards;->formattedCardOwnerName(Lcom/squareup/util/Res;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 192
    :cond_2
    invoke-direct {p0}, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->shouldDisplaySwipeOrInsertText()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 194
    iget-object v2, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

    invoke-virtual {v2}, Lcom/squareup/cardreader/CardReaderHubUtils;->isDipSupportedOnConnectedReader()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 195
    sget v2, Lcom/squareup/ui/tender/legacy/R$string;->kitchen_printing_cardholder_name_swipe_or_insert:I

    goto :goto_0

    .line 197
    :cond_3
    sget v2, Lcom/squareup/ui/tender/legacy/R$string;->kitchen_printing_cardholder_name_swipe:I

    :goto_0
    const/4 v3, 0x0

    .line 200
    invoke-virtual {v0, v3}, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->setCardholderClickable(Z)V

    .line 201
    iget-object v3, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->res:Lcom/squareup/util/Res;

    invoke-interface {v3, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->setCardholderNameStatus(Ljava/lang/String;)V

    :cond_4
    const/4 v2, 0x0

    :goto_1
    if-eqz v2, :cond_5

    .line 205
    invoke-virtual {v0, v2}, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->setOrderTicketName(Ljava/lang/String;)V

    if-eqz v1, :cond_5

    .line 207
    invoke-virtual {v0}, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->selectAllText()V

    :cond_5
    return-void
.end method

.method private shouldDisplaySwipeOrInsertText()Z
    .locals 1

    .line 393
    invoke-direct {p0}, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->supportsCardholderName()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->tipDeterminerFactory:Lcom/squareup/payment/TipDeterminerFactory;

    .line 394
    invoke-virtual {v0}, Lcom/squareup/payment/TipDeterminerFactory;->createForCurrentTender()Lcom/squareup/payment/TipDeterminer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/TipDeterminer;->showPreAuthTipScreen()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private shouldStartNfcMonitoring()Z
    .locals 1

    .line 387
    invoke-direct {p0}, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->shouldDisplaySwipeOrInsertText()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

    .line 388
    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderHubUtils;->isCardInsertedOnAnyContactlessReader()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

    .line 389
    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderHubUtils;->canStartNfcMonitoringInOrderTicket()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private shouldStartPaymentForCardholderName()Z
    .locals 2

    .line 398
    invoke-direct {p0}, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->supportsCardholderName()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->smartPaymentFlowStarter:Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    .line 399
    invoke-virtual {v0}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->checkCanStartSingleTenderEmvInBuyerFlow()Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;->SUCCESS:Lcom/squareup/ui/main/SmartPaymentFlowStarter$TransitionResult;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private supportsCardholderName()Z
    .locals 1

    .line 403
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasOrderTicketName()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 404
    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasCard()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    .line 405
    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->badIsHodorCheck()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method


# virtual methods
.method protected handleCardholderError()V
    .locals 4

    .line 276
    invoke-virtual {p0}, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->hasView()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 279
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/tender/TenderOrderTicketNameView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->setProgressVisible(Z)V

    .line 280
    invoke-virtual {p0}, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/tender/TenderOrderTicketNameView;

    iget-object v2, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/checkout/R$string;->kitchen_printing_cardholder_name_unavailable:I

    .line 281
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 280
    invoke-virtual {v0, v2}, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->setCardholderNameStatus(Ljava/lang/String;)V

    .line 282
    invoke-virtual {p0}, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/tender/TenderOrderTicketNameView;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->setCardholderClickable(Z)V

    return-void
.end method

.method public ignoreSwipeFor(Lcom/squareup/Card;)Z
    .locals 1

    .line 245
    iget-object p1, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->getResult()Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    move-result-object p1

    sget-object v0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->NONE:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->equals(Ljava/lang/Object;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    return p1
.end method

.method onBackPressed()Z
    .locals 2

    .line 230
    invoke-virtual {p0}, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/tender/TenderOrderTicketNameView;

    .line 232
    iget-object v1, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {v1}, Lcom/squareup/util/Device;->isPhone()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 233
    iget-object v1, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->getOrderTicketName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/payment/Transaction;->setOrderTicketName(Ljava/lang/String;)V

    .line 237
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->transaction:Lcom/squareup/payment/Transaction;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Transaction;->setCard(Lcom/squareup/Card;)V

    .line 238
    sget-object v0, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_HUMAN_INITIATED:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    invoke-direct {p0, v0}, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->resetTransaction(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    .line 239
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->reset()V

    .line 240
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->tenderScopeRunner:Lcom/squareup/tenderpayment/TenderScopeRunner;

    invoke-interface {v0}, Lcom/squareup/tenderpayment/TenderScopeRunner;->onBackPressedOnFirstTenderScreen()Z

    move-result v0

    return v0
.end method

.method public onCardReaderAdded(Lcom/squareup/cardreader/CardReader;)V
    .locals 0

    return-void
.end method

.method public onCardReaderRemoved(Lcom/squareup/cardreader/CardReader;)V
    .locals 3

    .line 331
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 333
    iget-object v1, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v1, v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->unsetActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    .line 335
    :cond_0
    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->isCardPresent()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 336
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->transaction:Lcom/squareup/payment/Transaction;

    const/4 v1, 0x0

    sget-object v2, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_READER_INITIATED:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/payment/Transaction;->dropPaymentOrTender(ZLcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    .line 337
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->getResult()Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->NONE:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 338
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->reset()V

    .line 339
    new-instance v0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;-><init>()V

    sget-object v1, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->GENERIC_ERROR:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    .line 341
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->warningType(Lcom/squareup/cardreader/ui/api/ReaderWarningType;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object v0

    .line 342
    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->isWireless()Z

    move-result p1

    if-eqz p1, :cond_1

    sget p1, Lcom/squareup/cardreader/R$string;->contactless_reader_disconnected_title:I

    goto :goto_0

    :cond_1
    sget p1, Lcom/squareup/checkout/R$string;->emv_reader_disconnected_title:I

    :goto_0
    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->titleId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/checkout/R$string;->emv_reader_disconnected_msg:I

    .line 345
    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->messageId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 346
    invoke-virtual {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->build()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object p1

    .line 347
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    new-instance v1, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowWarningScreen;

    invoke-direct {v1, p1}, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowWarningScreen;-><init>(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;)V

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;->requestReaderIssueScreen(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;)V

    :cond_2
    return-void
.end method

.method onCardholderStatusClicked()V
    .locals 2

    .line 353
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->cardholderName:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 356
    invoke-direct {p0, v0}, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->setNameOnTextField(Ljava/lang/String;)V

    return-void

    .line 354
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "cardholderName must not be null in order to click."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method onCommitOrderName()V
    .locals 3

    .line 213
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p0}, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/tender/TenderOrderTicketNameView;

    invoke-virtual {v1}, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->getOrderTicketName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/util/Strings;->trim(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Transaction;->setOrderTicketName(Ljava/lang/String;)V

    .line 216
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->transaction:Lcom/squareup/payment/Transaction;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Transaction;->setCanRenameCart(Z)V

    .line 217
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasCard()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    if-eqz v0, :cond_0

    .line 218
    iget-object v1, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->paymentCounter:Lcom/squareup/cardreader/PaymentCounter;

    invoke-virtual {v1, v0}, Lcom/squareup/cardreader/PaymentCounter;->onPaymentStarted(Lcom/squareup/cardreader/CardReaderId;)V

    .line 219
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->swipeHandler:Lcom/squareup/ui/payment/SwipeHandler;

    invoke-virtual {v0}, Lcom/squareup/ui/payment/SwipeHandler;->authorize()V

    goto :goto_0

    .line 220
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->getResult()Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    move-result-object v0

    sget-object v2, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->NFC_SUCCESS:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    invoke-virtual {v0, v2}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 221
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->buyerFlowStarter:Lcom/squareup/ui/buyer/BuyerFlowStarter;

    invoke-interface {v0}, Lcom/squareup/ui/buyer/BuyerFlowStarter;->startBuyerFlow()V

    goto :goto_0

    .line 222
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->getResult()Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    move-result-object v0

    sget-object v2, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->NONE:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    invoke-virtual {v0, v2}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasBillPayment()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 223
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->tenderScopeRunner:Lcom/squareup/tenderpayment/TenderScopeRunner;

    invoke-interface {v0, v1}, Lcom/squareup/tenderpayment/TenderScopeRunner;->completeTenderAndAdvance(Z)Z

    goto :goto_0

    .line 225
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->tenderScopeRunner:Lcom/squareup/tenderpayment/TenderScopeRunner;

    invoke-interface {v0}, Lcom/squareup/tenderpayment/TenderScopeRunner;->onOrderNameCommitted()V

    :goto_0
    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    .line 128
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 131
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    invoke-virtual {v0}, Lcom/squareup/ui/NfcProcessor;->continueMonitoring()V

    .line 132
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    iget-object v1, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->cardholderNameProcessor:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/ui/NfcProcessor;->registerNfcAuthDelegate(Lmortar/MortarScope;Lcom/squareup/ui/NfcProcessor$NfcAuthDelegate;)V

    .line 133
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    iget-object v1, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->cardholderNameProcessor:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/ui/NfcProcessor;->registerNfcListener(Lmortar/MortarScope;Lcom/squareup/ui/NfcProcessor$NfcListenerOverrider;)V

    .line 135
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->swipeHandler:Lcom/squareup/ui/payment/SwipeHandler;

    invoke-virtual {v0, p1, p0}, Lcom/squareup/ui/payment/SwipeHandler;->setSwipeHandler(Lmortar/MortarScope;Lcom/squareup/ui/seller/SwipedCardHandler;)V

    .line 136
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    invoke-interface {v0, p1, p0}, Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;->registerEmvCardInsertRemoveProcessor(Lmortar/MortarScope;Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;)V

    .line 137
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->hubScoper:Lcom/squareup/cardreader/dipper/CardReaderHubScoper;

    invoke-virtual {v0, p1, p0}, Lcom/squareup/cardreader/dipper/CardReaderHubScoper;->scopeAttachListener(Lmortar/MortarScope;Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;)V

    .line 138
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->cardholderNameProcessor:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 139
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->cardholderNameProcessor:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;

    new-instance v1, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter$CardholderNameListener;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter$CardholderNameListener;-><init>(Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter$1;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor;->setListener(Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Listener;)V

    .line 140
    invoke-direct {p0}, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->shouldStartNfcMonitoring()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    new-instance v1, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter$NfcTenderDisplay;

    invoke-direct {v1, v2}, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter$NfcTenderDisplay;-><init>(Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter$1;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/NfcProcessor;->startMonitoring(Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;)V

    .line 144
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    .line 145
    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->onOrderTicketNameResult()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/tender/-$$Lambda$TenderOrderTicketNamePresenter$OkrtEfcG2YYl3BgzPj1qPpU_L1g;

    invoke-direct {v1, p0}, Lcom/squareup/ui/tender/-$$Lambda$TenderOrderTicketNamePresenter$OkrtEfcG2YYl3BgzPj1qPpU_L1g;-><init>(Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 144
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 147
    iget-object p1, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {p1}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->enteringOrderTicketName()Z

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 4

    .line 151
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 153
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasTicket()Z

    move-result v0

    if-nez v0, :cond_2

    .line 157
    invoke-virtual {p0}, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/tender/TenderOrderTicketNameView;

    .line 158
    iget-object v1, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->tenderScopeRunner:Lcom/squareup/tenderpayment/TenderScopeRunner;

    iget-object v2, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {v2}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v2

    invoke-interface {v1, v2}, Lcom/squareup/tenderpayment/TenderScopeRunner;->buildTenderActionBarConfig(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    .line 160
    iget-object v2, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {v2}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v2

    if-nez v2, :cond_0

    .line 161
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    const/4 v2, 0x1

    .line 162
    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/strings/R$string;->next:I

    .line 163
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/tender/-$$Lambda$abRX_9HZTimPsazt0uwgrzU4RkA;

    invoke-direct {v2, p0}, Lcom/squareup/ui/tender/-$$Lambda$abRX_9HZTimPsazt0uwgrzU4RkA;-><init>(Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;)V

    .line 164
    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 165
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    .line 167
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 169
    invoke-direct {p0}, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->setUpView()V

    if-nez p1, :cond_1

    .line 172
    invoke-virtual {p0}, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/tender/TenderOrderTicketNameView;

    invoke-virtual {p1}, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->requestInitialFocus()V

    :cond_1
    return-void

    .line 154
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Should never see an order ticket name screen with an  open ticket active for a cart!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public processEmvCardInserted(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 3

    .line 286
    invoke-direct {p0}, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->shouldStartPaymentForCardholderName()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 287
    iget-object p1, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->emvPaymentStarter:Lcom/squareup/ui/buyer/emv/EmvPaymentStarter;

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Lcom/squareup/ui/buyer/emv/EmvPaymentStarter;->setUpEmvPayment(Z)V

    .line 288
    iget-object p1, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->tipDeterminerFactory:Lcom/squareup/payment/TipDeterminerFactory;

    .line 289
    invoke-virtual {p1}, Lcom/squareup/payment/TipDeterminerFactory;->createForCurrentTender()Lcom/squareup/payment/TipDeterminer;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/payment/TipDeterminer;->showPreAuthTipScreen()Z

    move-result p1

    const/4 v1, 0x1

    xor-int/2addr p1, v1

    if-nez p1, :cond_0

    .line 291
    iget-object p1, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    sget-object v1, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->ABORTED_PRE_AUTH:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    invoke-virtual {p1, v1}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->setResult(Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;)V

    .line 293
    invoke-virtual {p0}, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/tender/TenderOrderTicketNameView;

    iget-object v1, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/checkout/R$string;->kitchen_printing_cardholder_name_unavailable:I

    .line 294
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 293
    invoke-virtual {p1, v1}, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->setCardholderNameStatus(Ljava/lang/String;)V

    .line 296
    invoke-virtual {p0}, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/tender/TenderOrderTicketNameView;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->setProgressVisible(Z)V

    goto :goto_0

    .line 298
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->emvPaymentStarter:Lcom/squareup/ui/buyer/emv/EmvPaymentStarter;

    invoke-interface {p1}, Lcom/squareup/ui/buyer/emv/EmvPaymentStarter;->startPaymentOnActiveReader()V

    .line 299
    iget-object p1, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    sget-object v0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->DIP_FETCHING:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->setResult(Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;)V

    .line 300
    invoke-virtual {p0}, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/tender/TenderOrderTicketNameView;

    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/checkout/R$string;->kitchen_printing_cardholder_name_retrieving:I

    .line 301
    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 300
    invoke-virtual {p1, v0}, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->setCardholderNameStatus(Ljava/lang/String;)V

    .line 303
    invoke-virtual {p0}, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/tender/TenderOrderTicketNameView;

    invoke-virtual {p1, v1}, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->setProgressVisible(Z)V

    :cond_1
    :goto_0
    return-void
.end method

.method public processEmvCardRemoved(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 2

    .line 309
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->isCardPresenceRequired()Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 312
    :cond_0
    sget-object p1, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_HUMAN_INITIATED:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    invoke-direct {p0, p1}, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->resetTransaction(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    .line 313
    iget-object p1, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->reset()V

    .line 314
    new-instance p1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    invoke-direct {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;-><init>()V

    sget-object v0, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->GENERIC_ERROR:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    .line 316
    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->warningType(Lcom/squareup/cardreader/ui/api/ReaderWarningType;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/cardreader/ui/R$string;->emv_card_removed_title:I

    .line 317
    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->titleId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/cardreader/ui/R$string;->emv_card_removed_msg:I

    .line 318
    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->messageId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 319
    invoke-virtual {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->build()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object p1

    .line 320
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    new-instance v1, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowWarningScreen;

    invoke-direct {v1, p1}, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowWarningScreen;-><init>(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;)V

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;->requestReaderIssueScreen(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;)V

    return-void
.end method

.method protected processNameReceived(Ljava/lang/String;)V
    .locals 3

    .line 257
    invoke-virtual {p0}, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->hasView()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 260
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/tender/TenderOrderTicketNameView;

    const/4 v1, 0x0

    .line 261
    invoke-virtual {v0, v1}, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->setProgressVisible(Z)V

    .line 262
    iget-object v1, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->res:Lcom/squareup/util/Res;

    invoke-static {v1, p1}, Lcom/squareup/text/Cards;->formattedCardOwnerName(Lcom/squareup/util/Res;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->cardholderName:Ljava/lang/String;

    .line 263
    invoke-virtual {v0}, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->getOrderTicketName()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 264
    iget-object p1, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->cardholderName:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->setNameOnTextField(Ljava/lang/String;)V

    goto :goto_0

    .line 266
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/checkout/R$string;->kitchen_printing_cardholder_name_name_order:I

    .line 267
    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->cardholderName:Ljava/lang/String;

    const-string v2, "name"

    .line 268
    invoke-virtual {p1, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 269
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 270
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    .line 266
    invoke-virtual {v0, p1}, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->setCardholderNameStatus(Ljava/lang/String;)V

    const/4 p1, 0x1

    .line 271
    invoke-virtual {v0, p1}, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->setCardholderClickable(Z)V

    :goto_0
    return-void
.end method

.method public processSwipedCard(Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/Card;)V
    .locals 0

    .line 249
    iput-object p1, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    .line 250
    iget-object p1, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1, p2}, Lcom/squareup/payment/Transaction;->setCard(Lcom/squareup/Card;)V

    .line 251
    invoke-virtual {p0}, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->hasView()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 252
    invoke-direct {p0}, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->setUpView()V

    :cond_0
    return-void
.end method
