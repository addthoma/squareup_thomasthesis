.class Lcom/squareup/ui/systempermissions/AudioPermissionScreen$Presenter$1;
.super Ljava/lang/Object;
.source "AudioPermissionScreen.java"

# interfaces
.implements Lrx/Observer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/systempermissions/AudioPermissionScreen$Presenter;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/Observer<",
        "Lcom/squareup/ui/settings/paymentdevices/ReaderState;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/systempermissions/AudioPermissionScreen$Presenter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/systempermissions/AudioPermissionScreen$Presenter;)V
    .locals 0

    .line 84
    iput-object p1, p0, Lcom/squareup/ui/systempermissions/AudioPermissionScreen$Presenter$1;->this$0:Lcom/squareup/ui/systempermissions/AudioPermissionScreen$Presenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompleted()V
    .locals 1

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/AudioPermissionScreen$Presenter$1;->this$0:Lcom/squareup/ui/systempermissions/AudioPermissionScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/systempermissions/AudioPermissionScreen$Presenter;->closeCard()V

    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    .line 90
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onNext(Lcom/squareup/ui/settings/paymentdevices/ReaderState;)V
    .locals 0

    return-void
.end method

.method public bridge synthetic onNext(Ljava/lang/Object;)V
    .locals 0

    .line 84
    check-cast p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/systempermissions/AudioPermissionScreen$Presenter$1;->onNext(Lcom/squareup/ui/settings/paymentdevices/ReaderState;)V

    return-void
.end method
