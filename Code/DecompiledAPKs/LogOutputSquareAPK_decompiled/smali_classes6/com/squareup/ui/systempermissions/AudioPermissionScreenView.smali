.class public Lcom/squareup/ui/systempermissions/AudioPermissionScreenView;
.super Landroid/widget/LinearLayout;
.source "AudioPermissionScreenView.java"

# interfaces
.implements Lcom/squareup/ui/HasActionBar;


# instance fields
.field private actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

.field appNameFormatter:Lcom/squareup/util/AppNameFormatter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private permissionButton:Lcom/squareup/marketfont/MarketButton;

.field presenter:Lcom/squareup/ui/systempermissions/AudioPermissionScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 31
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    const-class p2, Lcom/squareup/ui/systempermissions/AudioPermissionScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/systempermissions/AudioPermissionScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/systempermissions/AudioPermissionScreen$Component;->inject(Lcom/squareup/ui/systempermissions/AudioPermissionScreenView;)V

    return-void
.end method


# virtual methods
.method public getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/AudioPermissionScreenView;->actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 36
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 37
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/systempermissions/AudioPermissionScreenView;->actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

    .line 38
    sget v0, Lcom/squareup/cardreader/ui/R$id;->permission_enable:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/ui/systempermissions/AudioPermissionScreenView;->permissionButton:Lcom/squareup/marketfont/MarketButton;

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/AudioPermissionScreenView;->presenter:Lcom/squareup/ui/systempermissions/AudioPermissionScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/systempermissions/AudioPermissionScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/AudioPermissionScreenView;->presenter:Lcom/squareup/ui/systempermissions/AudioPermissionScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/systempermissions/AudioPermissionScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 44
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method setButtonText(I)V
    .locals 1

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/AudioPermissionScreenView;->permissionButton:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketButton;->setText(I)V

    return-void
.end method

.method setupLayout(I)V
    .locals 2

    .line 52
    sget v0, Lcom/squareup/cardreader/ui/R$id;->permission_glyph:I

    .line 53
    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    .line 54
    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_MICROPHONE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    .line 55
    invoke-virtual {p0, p1}, Lcom/squareup/ui/systempermissions/AudioPermissionScreenView;->setButtonText(I)V

    .line 56
    iget-object p1, p0, Lcom/squareup/ui/systempermissions/AudioPermissionScreenView;->permissionButton:Lcom/squareup/marketfont/MarketButton;

    new-instance v0, Lcom/squareup/ui/systempermissions/AudioPermissionScreenView$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/systempermissions/AudioPermissionScreenView$1;-><init>(Lcom/squareup/ui/systempermissions/AudioPermissionScreenView;)V

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 61
    sget p1, Lcom/squareup/cardreader/ui/R$id;->permission_explanation:I

    .line 62
    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/MessageView;

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/AudioPermissionScreenView;->appNameFormatter:Lcom/squareup/util/AppNameFormatter;

    sget v1, Lcom/squareup/ui/systempermissions/R$string;->system_permission_mic_body:I

    .line 64
    invoke-interface {v0, v1}, Lcom/squareup/util/AppNameFormatter;->getStringWithAppName(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 63
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
