.class public Lcom/squareup/ui/systempermissions/AudioPermissionScreen$Presenter;
.super Lcom/squareup/ui/settings/SettingsCardPresenter;
.source "AudioPermissionScreen.java"

# interfaces
.implements Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$PermissionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/systempermissions/AudioPermissionScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/settings/SettingsCardPresenter<",
        "Lcom/squareup/ui/systempermissions/AudioPermissionScreenView;",
        ">;",
        "Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$PermissionListener;"
    }
.end annotation


# instance fields
.field private final cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

.field private final cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

.field private final res:Lcom/squareup/util/Res;

.field private final systemPermissionsPresenter:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Device;Lflow/Flow;Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/util/Res;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 55
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/settings/SettingsCardPresenter;-><init>(Lcom/squareup/util/Device;Lflow/Flow;)V

    .line 56
    iput-object p3, p0, Lcom/squareup/ui/systempermissions/AudioPermissionScreen$Presenter;->systemPermissionsPresenter:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;

    .line 57
    iput-object p5, p0, Lcom/squareup/ui/systempermissions/AudioPermissionScreen$Presenter;->cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    .line 58
    iput-object p6, p0, Lcom/squareup/ui/systempermissions/AudioPermissionScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 59
    iput-object p4, p0, Lcom/squareup/ui/systempermissions/AudioPermissionScreen$Presenter;->cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

    return-void
.end method

.method private getButtonText()I
    .locals 2

    .line 120
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/AudioPermissionScreen$Presenter;->systemPermissionsPresenter:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;

    sget-object v1, Lcom/squareup/systempermissions/SystemPermission;->MICROPHONE:Lcom/squareup/systempermissions/SystemPermission;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->canAskDirectly(Lcom/squareup/systempermissions/SystemPermission;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/systempermissions/SystemPermission;->MICROPHONE:Lcom/squareup/systempermissions/SystemPermission;

    .line 121
    invoke-static {v0}, Lcom/squareup/ui/systempermissions/PermissionMessages;->buttonText(Lcom/squareup/systempermissions/SystemPermission;)I

    move-result v0

    goto :goto_0

    :cond_0
    sget v0, Lcom/squareup/utilities/R$string;->system_permission_settings_button:I

    :goto_0
    return v0
.end method


# virtual methods
.method askForMicPermission()V
    .locals 2

    .line 116
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/AudioPermissionScreen$Presenter;->systemPermissionsPresenter:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;

    sget-object v1, Lcom/squareup/systempermissions/SystemPermission;->MICROPHONE:Lcom/squareup/systempermissions/SystemPermission;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->requestPermission(Lcom/squareup/systempermissions/SystemPermission;)V

    return-void
.end method

.method public getActionbarText()Ljava/lang/String;
    .locals 2

    .line 104
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/AudioPermissionScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/cardreader/ui/R$string;->square_readers:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onDenied(ILcom/squareup/systempermissions/SystemPermission;)V
    .locals 0

    .line 71
    invoke-virtual {p0}, Lcom/squareup/ui/systempermissions/AudioPermissionScreen$Presenter;->hasView()Z

    move-result p1

    if-eqz p1, :cond_0

    sget-object p1, Lcom/squareup/systempermissions/SystemPermission;->MICROPHONE:Lcom/squareup/systempermissions/SystemPermission;

    if-ne p2, p1, :cond_0

    .line 72
    invoke-virtual {p0}, Lcom/squareup/ui/systempermissions/AudioPermissionScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/systempermissions/AudioPermissionScreenView;

    invoke-direct {p0}, Lcom/squareup/ui/systempermissions/AudioPermissionScreen$Presenter;->getButtonText()I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/systempermissions/AudioPermissionScreenView;->setButtonText(I)V

    :cond_0
    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/AudioPermissionScreen$Presenter;->systemPermissionsPresenter:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->addPermissionListener(Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$PermissionListener;)V

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/AudioPermissionScreen$Presenter;->cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->readerStates()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/cardreader/CardReaderInfo;->AUDIO_READER_ADDRESS:Ljava/lang/String;

    .line 83
    invoke-static {v1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracleFilters;->filterByReaderAddress(Ljava/lang/String;)Lrx/Observable$Transformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/systempermissions/AudioPermissionScreen$Presenter$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/systempermissions/AudioPermissionScreen$Presenter$1;-><init>(Lcom/squareup/ui/systempermissions/AudioPermissionScreen$Presenter;)V

    .line 84
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/Observer;)Lrx/Subscription;

    move-result-object v0

    .line 82
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 100
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/AudioPermissionScreen$Presenter;->systemPermissionsPresenter:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->removePermissionListener(Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$PermissionListener;)V

    return-void
.end method

.method public onGranted(ILcom/squareup/systempermissions/SystemPermission;)V
    .locals 0

    .line 63
    iget-object p1, p0, Lcom/squareup/ui/systempermissions/AudioPermissionScreen$Presenter;->cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderHubUtils;->maybeGetAudioReader()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 65
    iget-object p2, p0, Lcom/squareup/ui/systempermissions/AudioPermissionScreen$Presenter;->cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    invoke-virtual {p2, p1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->stopEventOverrideForReader(Lcom/squareup/cardreader/CardReaderInfo;)V

    .line 67
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/systempermissions/AudioPermissionScreen$Presenter;->closeCard()V

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 2

    .line 108
    invoke-super {p0, p1}, Lcom/squareup/ui/settings/SettingsCardPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 109
    invoke-virtual {p0}, Lcom/squareup/ui/systempermissions/AudioPermissionScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/systempermissions/AudioPermissionScreenView;

    .line 110
    invoke-virtual {p0}, Lcom/squareup/ui/systempermissions/AudioPermissionScreen$Presenter;->buildActionbarConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 111
    invoke-virtual {p1}, Lcom/squareup/ui/systempermissions/AudioPermissionScreenView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v1

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 112
    invoke-direct {p0}, Lcom/squareup/ui/systempermissions/AudioPermissionScreen$Presenter;->getButtonText()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/systempermissions/AudioPermissionScreenView;->setupLayout(I)V

    return-void
.end method

.method public screenForAssertion()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/ui/main/RegisterTreeKey;",
            ">;"
        }
    .end annotation

    .line 77
    const-class v0, Lcom/squareup/ui/systempermissions/AudioPermissionScreen;

    return-object v0
.end method
