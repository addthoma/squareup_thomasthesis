.class public final Lcom/squareup/ui/systempermissions/EnableDeviceSettingsScreen;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "EnableDeviceSettingsScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/spot/ModalBodyScreen;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/systempermissions/EnableDeviceSettingsScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/systempermissions/EnableDeviceSettingsScreen$Component;,
        Lcom/squareup/ui/systempermissions/EnableDeviceSettingsScreen$ParentComponent;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/systempermissions/EnableDeviceSettingsScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/systempermissions/EnableDeviceSettingsScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    new-instance v0, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsScreen;

    invoke-direct {v0}, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsScreen;->INSTANCE:Lcom/squareup/ui/systempermissions/EnableDeviceSettingsScreen;

    .line 39
    sget-object v0, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsScreen;->INSTANCE:Lcom/squareup/ui/systempermissions/EnableDeviceSettingsScreen;

    .line 40
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/systempermissions/EnableDeviceSettingsScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 43
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->SYSTEM_PERMISSIONS_ENABLE_SETTINGS:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getHideMaster()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public screenLayout()I
    .locals 1

    .line 47
    sget v0, Lcom/squareup/common/bootstrap/R$layout;->enable_device_settings_view:I

    return v0
.end method
