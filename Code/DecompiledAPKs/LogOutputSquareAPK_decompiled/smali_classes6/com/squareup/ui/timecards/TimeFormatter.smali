.class public Lcom/squareup/ui/timecards/TimeFormatter;
.super Ljava/lang/Object;
.source "TimeFormatter.java"


# static fields
.field private static final DECIMAL_FORMAT_ROUND_TO_TWO_DECIMALS:Ljava/text/DecimalFormat;

.field private static final ONE_HOUR_SECONDS:I = 0xe10


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 12
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "0.00"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/ui/timecards/TimeFormatter;->DECIMAL_FORMAT_ROUND_TO_TWO_DECIMALS:Ljava/text/DecimalFormat;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static getCurrentWorkDay(Lorg/threeten/bp/ZoneId;Ljava/util/Locale;J)Ljava/lang/String;
    .locals 2

    .line 44
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "EEE, MMM d"

    invoke-direct {v0, v1, p1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 45
    invoke-virtual {p0}, Lorg/threeten/bp/ZoneId;->getId()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 46
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static getFormattedHoursFraction(Lcom/squareup/util/Res;J)Ljava/lang/String;
    .locals 2

    long-to-double p1, p1

    const-wide v0, 0x40ac200000000000L    # 3600.0

    div-double/2addr p1, v0

    .line 85
    sget v0, Lcom/squareup/ui/timecards/R$string;->employee_management_timecard_hrs_fraction:I

    invoke-interface {p0, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Lcom/squareup/ui/timecards/TimeFormatter;->DECIMAL_FORMAT_ROUND_TO_TWO_DECIMALS:Ljava/text/DecimalFormat;

    .line 86
    invoke-virtual {v1, p1, p2}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "num_hours_fraction"

    invoke-virtual {p0, p2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 87
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    .line 88
    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static getFormattedTimePeriod(Lcom/squareup/util/Res;Lcom/squareup/ui/timecards/HoursMinutes;)Ljava/lang/String;
    .locals 5

    .line 55
    iget v0, p1, Lcom/squareup/ui/timecards/HoursMinutes;->hours:I

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    iget v0, p1, Lcom/squareup/ui/timecards/HoursMinutes;->minutes:I

    if-ge v0, v1, :cond_0

    .line 56
    sget p1, Lcom/squareup/ui/timecards/R$string;->employee_management_timecard_hrs_mins_zero:I

    invoke-interface {p0, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 60
    :cond_0
    iget v0, p1, Lcom/squareup/ui/timecards/HoursMinutes;->hours:I

    const-string v2, ""

    if-ne v0, v1, :cond_1

    .line 61
    sget v0, Lcom/squareup/ui/timecards/R$string;->employee_management_timecard_hrs_singular:I

    invoke-interface {p0, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 62
    :cond_1
    iget v0, p1, Lcom/squareup/ui/timecards/HoursMinutes;->hours:I

    if-le v0, v1, :cond_2

    .line 63
    sget v0, Lcom/squareup/ui/timecards/R$string;->employee_management_timecard_hrs_plural:I

    invoke-interface {p0, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget v3, p1, Lcom/squareup/ui/timecards/HoursMinutes;->hours:I

    const-string v4, "num_hours"

    .line 64
    invoke-virtual {v0, v4, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 65
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    :cond_2
    move-object v0, v2

    .line 67
    :goto_0
    iget v3, p1, Lcom/squareup/ui/timecards/HoursMinutes;->minutes:I

    if-ne v3, v1, :cond_3

    .line 68
    sget p1, Lcom/squareup/ui/timecards/R$string;->employee_management_timecard_mins_singular:I

    invoke-interface {p0, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_1

    .line 70
    :cond_3
    sget v1, Lcom/squareup/ui/timecards/R$string;->employee_management_timecard_mins_plural:I

    invoke-interface {p0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    iget p1, p1, Lcom/squareup/ui/timecards/HoursMinutes;->minutes:I

    const-string v1, "num_minutes"

    .line 71
    invoke-virtual {p0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 72
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    .line 74
    :goto_1
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_4

    goto :goto_2

    :cond_4
    const-string v2, " "

    :goto_2
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static getSentenceTime(Lorg/threeten/bp/ZoneId;J)Ljava/lang/String;
    .locals 0

    .line 34
    invoke-static {p0, p1, p2}, Lcom/squareup/ui/timecards/TimeFormatter;->getTitleTime(Lorg/threeten/bp/ZoneId;J)Ljava/lang/String;

    move-result-object p0

    const-string p1, "AM"

    const-string p2, "a.m."

    .line 35
    invoke-virtual {p0, p1, p2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    const-string p1, "PM"

    const-string p2, "p.m."

    .line 36
    invoke-virtual {p0, p1, p2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static getTitleTime(Lorg/threeten/bp/ZoneId;J)Ljava/lang/String;
    .locals 3

    .line 22
    new-instance v0, Ljava/text/SimpleDateFormat;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    const-string v2, "h:mm a"

    invoke-direct {v0, v2, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 23
    invoke-virtual {p0}, Lorg/threeten/bp/ZoneId;->getId()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 24
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method
