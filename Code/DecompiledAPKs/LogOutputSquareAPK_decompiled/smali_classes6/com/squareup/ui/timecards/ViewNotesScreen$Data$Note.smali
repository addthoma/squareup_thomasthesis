.class public final Lcom/squareup/ui/timecards/ViewNotesScreen$Data$Note;
.super Ljava/lang/Object;
.source "ViewNotesScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/timecards/ViewNotesScreen$Data;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Note"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\t\u0018\u00002\u00020\u0001B\u0017\u0012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0005R\u001c\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007\"\u0004\u0008\u0008\u0010\tR\u001a\u0010\u0004\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\n\u0010\u0007\"\u0004\u0008\u000b\u0010\t\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/ui/timecards/ViewNotesScreen$Data$Note;",
        "",
        "jobTitle",
        "",
        "note",
        "(Ljava/lang/String;Ljava/lang/String;)V",
        "getJobTitle",
        "()Ljava/lang/String;",
        "setJobTitle",
        "(Ljava/lang/String;)V",
        "getNote",
        "setNote",
        "timecards_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private jobTitle:Ljava/lang/String;

.field private note:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string v0, "note"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/timecards/ViewNotesScreen$Data$Note;->jobTitle:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/ui/timecards/ViewNotesScreen$Data$Note;->note:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final getJobTitle()Ljava/lang/String;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/ui/timecards/ViewNotesScreen$Data$Note;->jobTitle:Ljava/lang/String;

    return-object v0
.end method

.method public final getNote()Ljava/lang/String;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/ui/timecards/ViewNotesScreen$Data$Note;->note:Ljava/lang/String;

    return-object v0
.end method

.method public final setJobTitle(Ljava/lang/String;)V
    .locals 0

    .line 24
    iput-object p1, p0, Lcom/squareup/ui/timecards/ViewNotesScreen$Data$Note;->jobTitle:Ljava/lang/String;

    return-void
.end method

.method public final setNote(Ljava/lang/String;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    iput-object p1, p0, Lcom/squareup/ui/timecards/ViewNotesScreen$Data$Note;->note:Ljava/lang/String;

    return-void
.end method
