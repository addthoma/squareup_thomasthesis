.class public Lcom/squareup/ui/timecards/StartBreakCoordinator;
.super Lcom/squareup/ui/timecards/TimecardsCoordinator;
.source "StartBreakCoordinator.java"


# instance fields
.field private successContainer:Landroid/view/View;

.field private successMessage:Lcom/squareup/marketfont/MarketTextView;

.field private successTitle:Lcom/squareup/marketfont/MarketTextView;

.field private successView:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/ui/timecards/TimecardsScopeRunner;Lcom/squareup/time/CurrentTime;Lrx/Scheduler;Lcom/squareup/analytics/Analytics;)V
    .locals 0
    .param p4    # Lrx/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 26
    invoke-direct/range {p0 .. p5}, Lcom/squareup/ui/timecards/TimecardsCoordinator;-><init>(Lcom/squareup/util/Res;Lcom/squareup/ui/timecards/TimecardsScopeRunner;Lcom/squareup/time/CurrentTime;Lrx/Scheduler;Lcom/squareup/analytics/Analytics;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 1

    .line 30
    invoke-super {p0, p1}, Lcom/squareup/ui/timecards/TimecardsCoordinator;->attach(Landroid/view/View;)V

    .line 31
    new-instance v0, Lcom/squareup/ui/timecards/-$$Lambda$StartBreakCoordinator$cX2dw8kbVJ6NlpjktChFWlG__QM;

    invoke-direct {v0, p0}, Lcom/squareup/ui/timecards/-$$Lambda$StartBreakCoordinator$cX2dw8kbVJ6NlpjktChFWlG__QM;-><init>(Lcom/squareup/ui/timecards/StartBreakCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method protected bindViews(Landroid/view/View;)V
    .locals 2

    .line 77
    invoke-super {p0, p1}, Lcom/squareup/ui/timecards/TimecardsCoordinator;->bindViews(Landroid/view/View;)V

    .line 78
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_success:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/timecards/StartBreakCoordinator;->successView:Landroid/view/View;

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/timecards/StartBreakCoordinator;->successView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 80
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_success_container:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/timecards/StartBreakCoordinator;->successContainer:Landroid/view/View;

    .line 81
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_success_title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/timecards/StartBreakCoordinator;->successTitle:Lcom/squareup/marketfont/MarketTextView;

    .line 82
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_success_message:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketTextView;

    iput-object p1, p0, Lcom/squareup/ui/timecards/StartBreakCoordinator;->successMessage:Lcom/squareup/marketfont/MarketTextView;

    return-void
.end method

.method protected getErrorMessage()Ljava/lang/String;
    .locals 2

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/timecards/StartBreakCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_start_error_message:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getErrorTitle()Ljava/lang/String;
    .locals 2

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/timecards/StartBreakCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_start_error_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getProgressBarTitle()Ljava/lang/String;
    .locals 2

    .line 38
    iget-object v0, p0, Lcom/squareup/ui/timecards/StartBreakCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_start_progress_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getSuccessViewEvent()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 42
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_SUCCESS_START_BREAK:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public synthetic lambda$attach$0$StartBreakCoordinator()Lrx/Subscription;
    .locals 3

    .line 31
    iget-object v0, p0, Lcom/squareup/ui/timecards/StartBreakCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->startBreakScreenData()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/timecards/StartBreakCoordinator;->mainScheduler:Lrx/Scheduler;

    .line 32
    invoke-virtual {v0, v1}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$QcMFgD9daUdgqNTVC20zV9rVOpE;

    invoke-direct {v1, p0}, Lcom/squareup/ui/timecards/-$$Lambda$QcMFgD9daUdgqNTVC20zV9rVOpE;-><init>(Lcom/squareup/ui/timecards/StartBreakCoordinator;)V

    .line 33
    invoke-virtual {v0, v1}, Lrx/Observable;->doOnSubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$kyLL2CiSijPChgfR3NpMvagqESA;

    invoke-direct {v1, p0}, Lcom/squareup/ui/timecards/-$$Lambda$kyLL2CiSijPChgfR3NpMvagqESA;-><init>(Lcom/squareup/ui/timecards/StartBreakCoordinator;)V

    new-instance v2, Lcom/squareup/ui/timecards/-$$Lambda$dmcvpTSGY2lM4v1YKFFFPvTjV2E;

    invoke-direct {v2, p0}, Lcom/squareup/ui/timecards/-$$Lambda$dmcvpTSGY2lM4v1YKFFFPvTjV2E;-><init>(Lcom/squareup/ui/timecards/StartBreakCoordinator;)V

    .line 34
    invoke-virtual {v0, v1, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method protected showSuccess(Lcom/squareup/ui/timecards/TimecardsScreenData;)V
    .locals 4

    .line 54
    move-object v0, p1

    check-cast v0, Lcom/squareup/ui/timecards/StartBreakScreen$Data;

    .line 55
    invoke-super {p0, p1}, Lcom/squareup/ui/timecards/TimecardsCoordinator;->showSuccess(Lcom/squareup/ui/timecards/TimecardsScreenData;)V

    .line 56
    iget-object v1, p0, Lcom/squareup/ui/timecards/StartBreakCoordinator;->timecardsActionBar:Lcom/squareup/ui/timecards/TimecardsActionBarView;

    iget-object v2, p0, Lcom/squareup/ui/timecards/StartBreakCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v3, Lcom/squareup/ui/timecards/-$$Lambda$7XwxV_VzV14tlOleK0NWbjzouFM;

    invoke-direct {v3, v2}, Lcom/squareup/ui/timecards/-$$Lambda$7XwxV_VzV14tlOleK0NWbjzouFM;-><init>(Lcom/squareup/ui/timecards/TimecardsScopeRunner;)V

    invoke-virtual {v1, v3}, Lcom/squareup/ui/timecards/TimecardsActionBarView;->updateConfigForSuccessScreen(Ljava/lang/Runnable;)V

    .line 58
    iget-object v1, p0, Lcom/squareup/ui/timecards/StartBreakCoordinator;->currentTime:Lcom/squareup/time/CurrentTime;

    invoke-interface {v1}, Lcom/squareup/time/CurrentTime;->zonedDateTime()Lorg/threeten/bp/ZonedDateTime;

    move-result-object v1

    invoke-virtual {v1}, Lorg/threeten/bp/ZonedDateTime;->getZone()Lorg/threeten/bp/ZoneId;

    move-result-object v1

    iget-object v2, v0, Lcom/squareup/ui/timecards/StartBreakScreen$Data;->breakEndTimeMillis:Ljava/lang/Long;

    .line 59
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 58
    invoke-static {v1, v2, v3}, Lcom/squareup/ui/timecards/TimeFormatter;->getSentenceTime(Lorg/threeten/bp/ZoneId;J)Ljava/lang/String;

    move-result-object v1

    .line 60
    iget-object v2, p0, Lcom/squareup/ui/timecards/StartBreakCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_start_success_message:I

    .line 61
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    const-string v3, "break_end_time"

    .line 62
    invoke-virtual {v2, v3, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 63
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 64
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 65
    iget-object v2, p0, Lcom/squareup/ui/timecards/StartBreakCoordinator;->successTitle:Lcom/squareup/marketfont/MarketTextView;

    sget v3, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_start_success_title:I

    invoke-virtual {v2, v3}, Lcom/squareup/marketfont/MarketTextView;->setText(I)V

    .line 66
    iget-object v2, p0, Lcom/squareup/ui/timecards/StartBreakCoordinator;->successMessage:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v2, v1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 67
    iget-object v1, p0, Lcom/squareup/ui/timecards/StartBreakCoordinator;->successMessage:Lcom/squareup/marketfont/MarketTextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 69
    sget v1, Lcom/squareup/ui/timecards/R$id;->success_notes_button:I

    invoke-virtual {p0, p1, v1}, Lcom/squareup/ui/timecards/StartBreakCoordinator;->showOrHideNotesButton(Lcom/squareup/ui/timecards/TimecardsScreenData;I)V

    .line 71
    iget-object p1, v0, Lcom/squareup/ui/timecards/StartBreakScreen$Data;->device:Lcom/squareup/util/Device;

    invoke-interface {p1}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 72
    iget-object p1, p0, Lcom/squareup/ui/timecards/StartBreakCoordinator;->successContainer:Landroid/view/View;

    sget-object v0, Lcom/squareup/ui/timecards/StartBreakCoordinator;->MOBILE_LAYOUT_PARAMS:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    return-void
.end method
