.class Lcom/squareup/ui/timecards/Timecards$EndTimecardBreakEvent;
.super Lcom/squareup/ui/timecards/Timecards$Event;
.source "Timecards.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/timecards/Timecards;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EndTimecardBreakEvent"
.end annotation


# instance fields
.field public final authorization:Lcom/squareup/protos/client/timecards/Authorization;

.field final synthetic this$0:Lcom/squareup/ui/timecards/Timecards;


# direct methods
.method constructor <init>(Lcom/squareup/ui/timecards/Timecards;)V
    .locals 0

    .line 670
    iput-object p1, p0, Lcom/squareup/ui/timecards/Timecards$EndTimecardBreakEvent;->this$0:Lcom/squareup/ui/timecards/Timecards;

    invoke-direct {p0}, Lcom/squareup/ui/timecards/Timecards$Event;-><init>()V

    const/4 p1, 0x0

    .line 671
    iput-object p1, p0, Lcom/squareup/ui/timecards/Timecards$EndTimecardBreakEvent;->authorization:Lcom/squareup/protos/client/timecards/Authorization;

    return-void
.end method

.method constructor <init>(Lcom/squareup/ui/timecards/Timecards;Lcom/squareup/protos/client/timecards/Authorization;)V
    .locals 0

    .line 674
    iput-object p1, p0, Lcom/squareup/ui/timecards/Timecards$EndTimecardBreakEvent;->this$0:Lcom/squareup/ui/timecards/Timecards;

    invoke-direct {p0}, Lcom/squareup/ui/timecards/Timecards$Event;-><init>()V

    .line 675
    iput-object p2, p0, Lcom/squareup/ui/timecards/Timecards$EndTimecardBreakEvent;->authorization:Lcom/squareup/protos/client/timecards/Authorization;

    return-void
.end method
