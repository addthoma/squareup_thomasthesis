.class public final Lcom/squareup/ui/timecards/ClockInModalCoordinator_Factory;
.super Ljava/lang/Object;
.source "ClockInModalCoordinator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/timecards/ClockInModalCoordinator;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final appNameFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AppNameFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final currentTimeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTime;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final scopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/timecards/TimecardsScopeRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/timecards/TimecardsScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTime;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AppNameFormatter;",
            ">;)V"
        }
    .end annotation

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/squareup/ui/timecards/ClockInModalCoordinator_Factory;->resProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p2, p0, Lcom/squareup/ui/timecards/ClockInModalCoordinator_Factory;->scopeRunnerProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p3, p0, Lcom/squareup/ui/timecards/ClockInModalCoordinator_Factory;->currentTimeProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p4, p0, Lcom/squareup/ui/timecards/ClockInModalCoordinator_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p5, p0, Lcom/squareup/ui/timecards/ClockInModalCoordinator_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p6, p0, Lcom/squareup/ui/timecards/ClockInModalCoordinator_Factory;->appNameFormatterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/timecards/ClockInModalCoordinator_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/timecards/TimecardsScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTime;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AppNameFormatter;",
            ">;)",
            "Lcom/squareup/ui/timecards/ClockInModalCoordinator_Factory;"
        }
    .end annotation

    .line 54
    new-instance v7, Lcom/squareup/ui/timecards/ClockInModalCoordinator_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/timecards/ClockInModalCoordinator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/util/Res;Lcom/squareup/ui/timecards/TimecardsScopeRunner;Lcom/squareup/time/CurrentTime;Lrx/Scheduler;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/AppNameFormatter;)Lcom/squareup/ui/timecards/ClockInModalCoordinator;
    .locals 8

    .line 60
    new-instance v7, Lcom/squareup/ui/timecards/ClockInModalCoordinator;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/timecards/ClockInModalCoordinator;-><init>(Lcom/squareup/util/Res;Lcom/squareup/ui/timecards/TimecardsScopeRunner;Lcom/squareup/time/CurrentTime;Lrx/Scheduler;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/AppNameFormatter;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/ui/timecards/ClockInModalCoordinator;
    .locals 7

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInModalCoordinator_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInModalCoordinator_Factory;->scopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInModalCoordinator_Factory;->currentTimeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/time/CurrentTime;

    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInModalCoordinator_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lrx/Scheduler;

    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInModalCoordinator_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInModalCoordinator_Factory;->appNameFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/util/AppNameFormatter;

    invoke-static/range {v1 .. v6}, Lcom/squareup/ui/timecards/ClockInModalCoordinator_Factory;->newInstance(Lcom/squareup/util/Res;Lcom/squareup/ui/timecards/TimecardsScopeRunner;Lcom/squareup/time/CurrentTime;Lrx/Scheduler;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/AppNameFormatter;)Lcom/squareup/ui/timecards/ClockInModalCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/ui/timecards/ClockInModalCoordinator_Factory;->get()Lcom/squareup/ui/timecards/ClockInModalCoordinator;

    move-result-object v0

    return-object v0
.end method
