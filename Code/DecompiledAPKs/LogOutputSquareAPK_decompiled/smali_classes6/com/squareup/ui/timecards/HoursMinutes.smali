.class public Lcom/squareup/ui/timecards/HoursMinutes;
.super Ljava/lang/Object;
.source "HoursMinutes.java"


# instance fields
.field public final hours:I

.field public final minutes:I


# direct methods
.method public constructor <init>(II)V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput p1, p0, Lcom/squareup/ui/timecards/HoursMinutes;->hours:I

    .line 17
    iput p2, p0, Lcom/squareup/ui/timecards/HoursMinutes;->minutes:I

    return-void
.end method

.method public constructor <init>(J)V
    .locals 2

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, p1, p2}, Ljava/util/concurrent/TimeUnit;->toHours(J)J

    move-result-wide v0

    long-to-int v1, v0

    iput v1, p0, Lcom/squareup/ui/timecards/HoursMinutes;->hours:I

    .line 22
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, p1, p2}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide p1

    long-to-int p2, p1

    rem-int/lit8 p2, p2, 0x3c

    iput p2, p0, Lcom/squareup/ui/timecards/HoursMinutes;->minutes:I

    return-void
.end method

.method public static getDiff(Ljava/util/Date;Ljava/util/Date;)Lcom/squareup/ui/timecards/HoursMinutes;
    .locals 2

    if-eqz p0, :cond_1

    if-eqz p1, :cond_0

    .line 37
    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-virtual {p0}, Ljava/util/Date;->getTime()J

    move-result-wide p0

    sub-long/2addr v0, p0

    .line 39
    sget-object p0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p0, v0, v1}, Ljava/util/concurrent/TimeUnit;->toHours(J)J

    move-result-wide p0

    long-to-int p1, p0

    .line 40
    sget-object p0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p0, v0, v1}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v0

    long-to-int p0, v0

    rem-int/lit8 p0, p0, 0x3c

    .line 42
    new-instance v0, Lcom/squareup/ui/timecards/HoursMinutes;

    invoke-direct {v0, p1, p0}, Lcom/squareup/ui/timecards/HoursMinutes;-><init>(II)V

    return-object v0

    .line 35
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "end input is null"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 32
    :cond_1
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "start input is null"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method


# virtual methods
.method public isEmpty()Z
    .locals 1

    .line 49
    iget v0, p0, Lcom/squareup/ui/timecards/HoursMinutes;->hours:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/squareup/ui/timecards/HoursMinutes;->minutes:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
