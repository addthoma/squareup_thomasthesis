.class public final Lcom/squareup/ui/payment/SwipeHandler_Factory;
.super Ljava/lang/Object;
.source "SwipeHandler_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/payment/SwipeHandler;",
        ">;"
    }
.end annotation


# instance fields
.field private final apiTransactionStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionState;",
            ">;"
        }
    .end annotation
.end field

.field private final applicationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final badKeyboardHiderIsBadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/BadKeyboardHider;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final giftCardsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/GiftCards;",
            ">;"
        }
    .end annotation
.end field

.field private final glassConfirmControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/widgets/glass/GlassConfirmController;",
            ">;"
        }
    .end annotation
.end field

.field private final nfcProcessorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/NfcProcessor;",
            ">;"
        }
    .end annotation
.end field

.field private final onboardingDiverterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingDiverter;",
            ">;"
        }
    .end annotation
.end field

.field private final openTicketsSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final orderEntryScreenStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;"
        }
    .end annotation
.end field

.field private final passcodeEmployeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final pauseAndResumeRegistrarProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pauses/PauseAndResumeRegistrar;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentCounterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/PaymentCounter;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentHudToasterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/PaymentHudToaster;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final swipeValidatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/swipe/SwipeValidator;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderCompleterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderCompleter;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderInEditProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field

.field private final workflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/CardSellerWorkflow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingDiverter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/PaymentCounter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pauses/PauseAndResumeRegistrar;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/PaymentHudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/BadKeyboardHider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/GiftCards;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/widgets/glass/GlassConfirmController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/NfcProcessor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/CardSellerWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderCompleter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/swipe/SwipeValidator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 99
    iput-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler_Factory;->applicationProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 100
    iput-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler_Factory;->onboardingDiverterProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 101
    iput-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler_Factory;->flowProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 102
    iput-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler_Factory;->paymentCounterProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 103
    iput-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler_Factory;->transactionProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 104
    iput-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler_Factory;->pauseAndResumeRegistrarProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 105
    iput-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler_Factory;->paymentHudToasterProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 106
    iput-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler_Factory;->settingsProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 107
    iput-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler_Factory;->badKeyboardHiderIsBadProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 108
    iput-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler_Factory;->orderEntryScreenStateProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 109
    iput-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler_Factory;->deviceProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 110
    iput-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler_Factory;->giftCardsProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 111
    iput-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler_Factory;->glassConfirmControllerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 112
    iput-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler_Factory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 113
    iput-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler_Factory;->passcodeEmployeeManagementProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 114
    iput-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler_Factory;->nfcProcessorProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 115
    iput-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler_Factory;->workflowProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p18

    .line 116
    iput-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler_Factory;->apiTransactionStateProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p19

    .line 117
    iput-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler_Factory;->tenderCompleterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p20

    .line 118
    iput-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler_Factory;->tenderInEditProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p21

    .line 119
    iput-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler_Factory;->swipeValidatorProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p22

    .line 120
    iput-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler_Factory;->tenderStarterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/payment/SwipeHandler_Factory;
    .locals 24
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingDiverter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/PaymentCounter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pauses/PauseAndResumeRegistrar;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/PaymentHudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/BadKeyboardHider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/GiftCards;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/widgets/glass/GlassConfirmController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/NfcProcessor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/CardSellerWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderCompleter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/swipe/SwipeValidator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;)",
            "Lcom/squareup/ui/payment/SwipeHandler_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    .line 145
    new-instance v23, Lcom/squareup/ui/payment/SwipeHandler_Factory;

    move-object/from16 v0, v23

    invoke-direct/range {v0 .. v22}, Lcom/squareup/ui/payment/SwipeHandler_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v23
.end method

.method public static newInstance(Landroid/app/Application;Lcom/squareup/onboarding/OnboardingDiverter;Lflow/Flow;Lcom/squareup/cardreader/PaymentCounter;Lcom/squareup/payment/Transaction;Lcom/squareup/pauses/PauseAndResumeRegistrar;Lcom/squareup/payment/PaymentHudToaster;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/main/BadKeyboardHider;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/util/Device;Lcom/squareup/giftcard/GiftCards;Lcom/squareup/widgets/glass/GlassConfirmController;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/ui/NfcProcessor;Lcom/squareup/tenderpayment/CardSellerWorkflow;Lcom/squareup/api/ApiTransactionState;Lcom/squareup/tenderpayment/TenderCompleter;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/swipe/SwipeValidator;Lcom/squareup/ui/tender/TenderStarter;)Lcom/squareup/ui/payment/SwipeHandler;
    .locals 24

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    .line 159
    new-instance v23, Lcom/squareup/ui/payment/SwipeHandler;

    move-object/from16 v0, v23

    invoke-direct/range {v0 .. v22}, Lcom/squareup/ui/payment/SwipeHandler;-><init>(Landroid/app/Application;Lcom/squareup/onboarding/OnboardingDiverter;Lflow/Flow;Lcom/squareup/cardreader/PaymentCounter;Lcom/squareup/payment/Transaction;Lcom/squareup/pauses/PauseAndResumeRegistrar;Lcom/squareup/payment/PaymentHudToaster;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/main/BadKeyboardHider;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/util/Device;Lcom/squareup/giftcard/GiftCards;Lcom/squareup/widgets/glass/GlassConfirmController;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/ui/NfcProcessor;Lcom/squareup/tenderpayment/CardSellerWorkflow;Lcom/squareup/api/ApiTransactionState;Lcom/squareup/tenderpayment/TenderCompleter;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/swipe/SwipeValidator;Lcom/squareup/ui/tender/TenderStarter;)V

    return-object v23
.end method


# virtual methods
.method public get()Lcom/squareup/ui/payment/SwipeHandler;
    .locals 24

    move-object/from16 v0, p0

    .line 125
    iget-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler_Factory;->applicationProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Landroid/app/Application;

    iget-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler_Factory;->onboardingDiverterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/onboarding/OnboardingDiverter;

    iget-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lflow/Flow;

    iget-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler_Factory;->paymentCounterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/cardreader/PaymentCounter;

    iget-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/payment/Transaction;

    iget-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler_Factory;->pauseAndResumeRegistrarProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/pauses/PauseAndResumeRegistrar;

    iget-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler_Factory;->paymentHudToasterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/payment/PaymentHudToaster;

    iget-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler_Factory;->badKeyboardHiderIsBadProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/ui/main/BadKeyboardHider;

    iget-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler_Factory;->orderEntryScreenStateProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/orderentry/OrderEntryScreenState;

    iget-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/util/Device;

    iget-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler_Factory;->giftCardsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/giftcard/GiftCards;

    iget-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler_Factory;->glassConfirmControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/widgets/glass/GlassConfirmController;

    iget-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler_Factory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/tickets/OpenTicketsSettings;

    iget-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler_Factory;->passcodeEmployeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/permissions/PasscodeEmployeeManagement;

    iget-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler_Factory;->nfcProcessorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/ui/NfcProcessor;

    iget-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler_Factory;->workflowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lcom/squareup/tenderpayment/CardSellerWorkflow;

    iget-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler_Factory;->apiTransactionStateProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v19, v1

    check-cast v19, Lcom/squareup/api/ApiTransactionState;

    iget-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler_Factory;->tenderCompleterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v20, v1

    check-cast v20, Lcom/squareup/tenderpayment/TenderCompleter;

    iget-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler_Factory;->tenderInEditProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v21, v1

    check-cast v21, Lcom/squareup/payment/TenderInEdit;

    iget-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler_Factory;->swipeValidatorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v22, v1

    check-cast v22, Lcom/squareup/swipe/SwipeValidator;

    iget-object v1, v0, Lcom/squareup/ui/payment/SwipeHandler_Factory;->tenderStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v23, v1

    check-cast v23, Lcom/squareup/ui/tender/TenderStarter;

    invoke-static/range {v2 .. v23}, Lcom/squareup/ui/payment/SwipeHandler_Factory;->newInstance(Landroid/app/Application;Lcom/squareup/onboarding/OnboardingDiverter;Lflow/Flow;Lcom/squareup/cardreader/PaymentCounter;Lcom/squareup/payment/Transaction;Lcom/squareup/pauses/PauseAndResumeRegistrar;Lcom/squareup/payment/PaymentHudToaster;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/main/BadKeyboardHider;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/util/Device;Lcom/squareup/giftcard/GiftCards;Lcom/squareup/widgets/glass/GlassConfirmController;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/ui/NfcProcessor;Lcom/squareup/tenderpayment/CardSellerWorkflow;Lcom/squareup/api/ApiTransactionState;Lcom/squareup/tenderpayment/TenderCompleter;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/swipe/SwipeValidator;Lcom/squareup/ui/tender/TenderStarter;)Lcom/squareup/ui/payment/SwipeHandler;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 29
    invoke-virtual {p0}, Lcom/squareup/ui/payment/SwipeHandler_Factory;->get()Lcom/squareup/ui/payment/SwipeHandler;

    move-result-object v0

    return-object v0
.end method
