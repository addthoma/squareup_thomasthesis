.class final Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$attach$1$1;
.super Ljava/lang/Object;
.source "DeviceCodeLoginCoordinator.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$attach$1;->invoke(Lcom/squareup/workflow/legacy/Screen;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "run"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

.field final synthetic this$0:Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$attach$1;


# direct methods
.method constructor <init>(Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$attach$1;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$attach$1$1;->this$0:Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$attach$1;

    iput-object p2, p0, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$attach$1$1;->$workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$attach$1$1;->$workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    new-instance v1, Lcom/squareup/ui/login/AuthenticatorEvent$LoginWithDeviceCode;

    iget-object v2, p0, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$attach$1$1;->this$0:Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$attach$1;

    iget-object v2, v2, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$attach$1;->this$0:Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;

    invoke-static {v2}, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;->access$getDeviceCode$p(Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/ui/login/AuthenticatorEvent$LoginWithDeviceCode;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method
