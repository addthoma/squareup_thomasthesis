.class final Lcom/squareup/ui/login/RealAuthenticator$emailLoginCallback$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealAuthenticator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/RealAuthenticator;->emailLoginCallback$impl_release(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/protos/register/api/LoginRequest;Lcom/squareup/ui/login/AuthenticatorInput;)Lkotlin/jvm/functions/Function1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ui/login/AuthenticationCallResult<",
        "+",
        "Lcom/squareup/protos/register/api/LoginResponse;",
        ">;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/ui/login/AuthenticatorState;",
        "+",
        "Lcom/squareup/ui/login/AuthenticatorOutput;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0012\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001j\u0002`\u00042\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<no name provided>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/login/AuthenticatorState;",
        "Lcom/squareup/ui/login/AuthenticatorOutput;",
        "Lcom/squareup/ui/login/AuthUpdate;",
        "response",
        "Lcom/squareup/ui/login/AuthenticationCallResult;",
        "Lcom/squareup/protos/register/api/LoginResponse;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $input:Lcom/squareup/ui/login/AuthenticatorInput;

.field final synthetic $request:Lcom/squareup/protos/register/api/LoginRequest;

.field final synthetic $this_emailLoginCallback:Lcom/squareup/ui/login/AuthenticatorState;

.field final synthetic this$0:Lcom/squareup/ui/login/RealAuthenticator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/login/RealAuthenticator;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/protos/register/api/LoginRequest;Lcom/squareup/ui/login/AuthenticatorInput;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/login/RealAuthenticator$emailLoginCallback$1;->this$0:Lcom/squareup/ui/login/RealAuthenticator;

    iput-object p2, p0, Lcom/squareup/ui/login/RealAuthenticator$emailLoginCallback$1;->$this_emailLoginCallback:Lcom/squareup/ui/login/AuthenticatorState;

    iput-object p3, p0, Lcom/squareup/ui/login/RealAuthenticator$emailLoginCallback$1;->$request:Lcom/squareup/protos/register/api/LoginRequest;

    iput-object p4, p0, Lcom/squareup/ui/login/RealAuthenticator$emailLoginCallback$1;->$input:Lcom/squareup/ui/login/AuthenticatorInput;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/ui/login/AuthenticationCallResult;)Lcom/squareup/workflow/WorkflowAction;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/login/AuthenticationCallResult<",
            "Lcom/squareup/protos/register/api/LoginResponse;",
            ">;)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/login/AuthenticatorState;",
            "Lcom/squareup/ui/login/AuthenticatorOutput;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const-string v2, "response"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 811
    instance-of v2, v1, Lcom/squareup/ui/login/AuthenticationCallResult$Success;

    const/4 v3, 0x4

    const/4 v4, 0x0

    if-eqz v2, :cond_5

    .line 812
    iget-object v5, v0, Lcom/squareup/ui/login/RealAuthenticator$emailLoginCallback$1;->$this_emailLoginCallback:Lcom/squareup/ui/login/AuthenticatorState;

    .line 814
    check-cast v1, Lcom/squareup/ui/login/AuthenticationCallResult$Success;

    invoke-virtual {v1}, Lcom/squareup/ui/login/AuthenticationCallResult$Success;->getResponse()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/register/api/LoginResponse;

    iget-object v6, v2, Lcom/squareup/protos/register/api/LoginResponse;->session_token:Ljava/lang/String;

    .line 816
    new-instance v7, Lcom/squareup/ui/login/UnitsAndMerchants;

    invoke-virtual {v1}, Lcom/squareup/ui/login/AuthenticationCallResult$Success;->getResponse()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/register/api/LoginResponse;

    iget-object v2, v2, Lcom/squareup/protos/register/api/LoginResponse;->unit:Ljava/util/List;

    const-string v11, "response.response.unit"

    invoke-static {v2, v11}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v7, v2}, Lcom/squareup/ui/login/UnitsAndMerchants;-><init>(Ljava/util/List;)V

    const/4 v8, 0x0

    const/4 v9, 0x4

    const/4 v10, 0x0

    .line 812
    invoke-static/range {v5 .. v10}, Lcom/squareup/ui/login/RealAuthenticatorKt;->withTemporarySessionData$default(Lcom/squareup/ui/login/AuthenticatorState;Ljava/lang/String;Lcom/squareup/ui/login/UnitsAndMerchants;Ljava/lang/Boolean;ILjava/lang/Object;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object v13

    .line 819
    invoke-virtual {v1}, Lcom/squareup/ui/login/AuthenticationCallResult$Success;->getResponse()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/register/api/LoginResponse;

    iget-object v2, v2, Lcom/squareup/protos/register/api/LoginResponse;->captcha_required:Ljava/lang/Boolean;

    if-eqz v2, :cond_0

    goto :goto_0

    .line 820
    :cond_0
    sget-object v2, Lcom/squareup/protos/register/api/LoginResponse;->DEFAULT_CAPTCHA_REQUIRED:Ljava/lang/Boolean;

    :goto_0
    const-string v5, "requiresCaptcha"

    .line 822
    invoke-static {v2, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 825
    iget-object v2, v0, Lcom/squareup/ui/login/RealAuthenticator$emailLoginCallback$1;->this$0:Lcom/squareup/ui/login/RealAuthenticator;

    iget-object v5, v0, Lcom/squareup/ui/login/RealAuthenticator$emailLoginCallback$1;->$request:Lcom/squareup/protos/register/api/LoginRequest;

    invoke-virtual {v1}, Lcom/squareup/ui/login/AuthenticationCallResult$Success;->getResponse()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/register/api/LoginResponse;

    iget-object v1, v1, Lcom/squareup/protos/register/api/LoginResponse;->captcha_api_key:Ljava/lang/String;

    const-string v6, "response.response.captcha_api_key"

    invoke-static {v1, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2, v13, v5, v1}, Lcom/squareup/ui/login/RealAuthenticator;->verifyLoginByCaptcha$impl_release(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/protos/register/api/LoginRequest;Ljava/lang/String;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object v1

    const-string v2, "verifyLoginByCaptcha"

    .line 823
    invoke-static {v2, v1, v4, v3, v4}, Lcom/squareup/ui/login/RealAuthenticatorKt;->enterState$default(Ljava/lang/String;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorOutput;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    return-object v1

    .line 829
    :cond_1
    invoke-virtual {v1}, Lcom/squareup/ui/login/AuthenticationCallResult$Success;->getResponse()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/register/api/LoginResponse;

    iget-object v2, v2, Lcom/squareup/protos/register/api/LoginResponse;->requires_two_factor:Ljava/lang/Boolean;

    if-eqz v2, :cond_2

    goto :goto_1

    .line 830
    :cond_2
    sget-object v2, Lcom/squareup/protos/register/api/LoginResponse;->DEFAULT_REQUIRES_TWO_FACTOR:Ljava/lang/Boolean;

    :goto_1
    const-string v5, "requiresTwoFactor"

    .line 832
    invoke-static {v2, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 833
    invoke-virtual {v1}, Lcom/squareup/ui/login/AuthenticationCallResult$Success;->getResponse()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/register/api/LoginResponse;

    iget-object v2, v2, Lcom/squareup/protos/register/api/LoginResponse;->can_skip_two_factor_enroll:Ljava/lang/Boolean;

    if-eqz v2, :cond_3

    goto :goto_2

    .line 834
    :cond_3
    sget-object v2, Lcom/squareup/protos/register/api/LoginResponse;->DEFAULT_CAN_SKIP_TWO_FACTOR_ENROLL:Ljava/lang/Boolean;

    .line 837
    :goto_2
    iget-object v12, v0, Lcom/squareup/ui/login/RealAuthenticator$emailLoginCallback$1;->this$0:Lcom/squareup/ui/login/RealAuthenticator;

    .line 838
    invoke-virtual {v1}, Lcom/squareup/ui/login/AuthenticationCallResult$Success;->getResponse()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/protos/register/api/LoginResponse;

    iget-object v14, v5, Lcom/squareup/protos/register/api/LoginResponse;->two_factor_details:Ljava/util/List;

    const-string v5, "response.response.two_factor_details"

    invoke-static {v14, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 839
    invoke-virtual {v1}, Lcom/squareup/ui/login/AuthenticationCallResult$Success;->getResponse()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/protos/register/api/LoginResponse;

    iget-object v5, v5, Lcom/squareup/protos/register/api/LoginResponse;->can_use_google_authenticator:Ljava/lang/Boolean;

    const-string v6, "response.response.can_use_google_authenticator"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v15

    .line 840
    invoke-virtual {v1}, Lcom/squareup/ui/login/AuthenticationCallResult$Success;->getResponse()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/register/api/LoginResponse;

    iget-object v1, v1, Lcom/squareup/protos/register/api/LoginResponse;->can_use_sms:Ljava/lang/Boolean;

    const-string v5, "response.response.can_use_sms"

    invoke-static {v1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v16

    const-string v1, "canSkipEnroll"

    .line 841
    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v17

    .line 837
    invoke-static/range {v12 .. v17}, Lcom/squareup/ui/login/RealAuthenticator;->access$startTwoFactorFlow(Lcom/squareup/ui/login/RealAuthenticator;Lcom/squareup/ui/login/AuthenticatorState;Ljava/util/List;ZZZ)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object v1

    const-string v2, "startTwoFactorFlow"

    .line 835
    invoke-static {v2, v1, v4, v3, v4}, Lcom/squareup/ui/login/RealAuthenticatorKt;->enterState$default(Ljava/lang/String;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorOutput;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto :goto_3

    .line 845
    :cond_4
    iget-object v12, v0, Lcom/squareup/ui/login/RealAuthenticator$emailLoginCallback$1;->this$0:Lcom/squareup/ui/login/RealAuthenticator;

    iget-object v14, v0, Lcom/squareup/ui/login/RealAuthenticator$emailLoginCallback$1;->$input:Lcom/squareup/ui/login/AuthenticatorInput;

    invoke-virtual {v1}, Lcom/squareup/ui/login/AuthenticationCallResult$Success;->getResponse()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/register/api/LoginResponse;

    iget-object v15, v1, Lcom/squareup/protos/register/api/LoginResponse;->unit:Ljava/util/List;

    invoke-static {v15, v11}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v16, 0x0

    const/16 v17, 0x4

    const/16 v18, 0x0

    invoke-static/range {v12 .. v18}, Lcom/squareup/ui/login/RealAuthenticator;->startEmployeeLoginFlow$impl_release$default(Lcom/squareup/ui/login/RealAuthenticator;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorInput;Ljava/util/List;IILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    :goto_3
    return-object v1

    .line 848
    :cond_5
    instance-of v2, v1, Lcom/squareup/ui/login/AuthenticationCallResult$Failure;

    if-eqz v2, :cond_6

    iget-object v2, v0, Lcom/squareup/ui/login/RealAuthenticator$emailLoginCallback$1;->$this_emailLoginCallback:Lcom/squareup/ui/login/AuthenticatorState;

    check-cast v1, Lcom/squareup/ui/login/AuthenticationCallResult$Failure;

    invoke-static {v2, v1}, Lcom/squareup/ui/login/RealAuthenticatorKt;->access$showFailure(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticationCallResult$Failure;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object v1

    const-string v2, "show emailLogin failure"

    invoke-static {v2, v1, v4, v3, v4}, Lcom/squareup/ui/login/RealAuthenticatorKt;->enterState$default(Ljava/lang/String;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorOutput;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    return-object v1

    :cond_6
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 157
    check-cast p1, Lcom/squareup/ui/login/AuthenticationCallResult;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/login/RealAuthenticator$emailLoginCallback$1;->invoke(Lcom/squareup/ui/login/AuthenticationCallResult;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
