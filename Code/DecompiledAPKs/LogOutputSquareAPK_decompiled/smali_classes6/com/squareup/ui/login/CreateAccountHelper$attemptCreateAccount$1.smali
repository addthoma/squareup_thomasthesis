.class final Lcom/squareup/ui/login/CreateAccountHelper$attemptCreateAccount$1;
.super Ljava/lang/Object;
.source "CreateAccountHelper.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/CreateAccountHelper;->attemptCreateAccount(Lcom/squareup/server/account/CreateBody;)Lio/reactivex/disposables/Disposable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/SingleSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0012\u0012\u000e\u0008\u0001\u0012\n \u0003*\u0004\u0018\u00010\u00020\u00020\u00012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lcom/squareup/ui/login/CreateAccountHelper$NextStepResponse;",
        "kotlin.jvm.PlatformType",
        "successOrFailure",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/server/account/CreateResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $createBody:Lcom/squareup/server/account/CreateBody;

.field final synthetic this$0:Lcom/squareup/ui/login/CreateAccountHelper;


# direct methods
.method constructor <init>(Lcom/squareup/ui/login/CreateAccountHelper;Lcom/squareup/server/account/CreateBody;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/login/CreateAccountHelper$attemptCreateAccount$1;->this$0:Lcom/squareup/ui/login/CreateAccountHelper;

    iput-object p2, p0, Lcom/squareup/ui/login/CreateAccountHelper$attemptCreateAccount$1;->$createBody:Lcom/squareup/server/account/CreateBody;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "+",
            "Lcom/squareup/server/account/CreateResponse;",
            ">;)",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/ui/login/CreateAccountHelper$NextStepResponse;",
            ">;"
        }
    .end annotation

    const-string v0, "successOrFailure"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountHelper$attemptCreateAccount$1;->this$0:Lcom/squareup/ui/login/CreateAccountHelper;

    .line 84
    iget-object v1, p0, Lcom/squareup/ui/login/CreateAccountHelper$attemptCreateAccount$1;->$createBody:Lcom/squareup/server/account/CreateBody;

    iget-object v1, v1, Lcom/squareup/server/account/CreateBody;->email:Ljava/lang/String;

    const-string v2, "createBody.email"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    iget-object v2, p0, Lcom/squareup/ui/login/CreateAccountHelper$attemptCreateAccount$1;->$createBody:Lcom/squareup/server/account/CreateBody;

    iget-object v2, v2, Lcom/squareup/server/account/CreateBody;->password:Ljava/lang/String;

    const-string v3, "createBody.password"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/server/account/CreateResponse;

    iget-boolean p1, p1, Lcom/squareup/server/account/CreateResponse;->existing_merchant:Z

    .line 83
    invoke-static {v0, v1, v2, p1}, Lcom/squareup/ui/login/CreateAccountHelper;->access$postCreateLogin(Lcom/squareup/ui/login/CreateAccountHelper;Ljava/lang/String;Ljava/lang/String;Z)Lio/reactivex/Single;

    move-result-object p1

    goto :goto_0

    .line 88
    :cond_0
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz v0, :cond_1

    .line 89
    new-instance v0, Lcom/squareup/ui/login/CreateAccountHelper$NextStepResponse$FailureResponse;

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    invoke-direct {v0, p1}, Lcom/squareup/ui/login/CreateAccountHelper$NextStepResponse$FailureResponse;-><init>(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V

    .line 88
    invoke-static {v0}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Single.just(\n           \u2026sOrFailure)\n            )"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 49
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/login/CreateAccountHelper$attemptCreateAccount$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
