.class final Lcom/squareup/ui/login/SmsPickerCoordinator$TwoFactorDetailsAdapter;
.super Landroid/widget/BaseAdapter;
.source "SmsPickerCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/login/SmsPickerCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "TwoFactorDetailsAdapter"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0082\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H\u0016J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0004H\u0016J\u0010\u0010\u0008\u001a\u00020\t2\u0006\u0010\u0007\u001a\u00020\u0004H\u0016J\"\u0010\n\u001a\u00020\u000b2\u0006\u0010\u0007\u001a\u00020\u00042\u0008\u0010\u000c\u001a\u0004\u0018\u00010\u000b2\u0006\u0010\r\u001a\u00020\u000eH\u0016\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/ui/login/SmsPickerCoordinator$TwoFactorDetailsAdapter;",
        "Landroid/widget/BaseAdapter;",
        "(Lcom/squareup/ui/login/SmsPickerCoordinator;)V",
        "getCount",
        "",
        "getItem",
        "Lcom/squareup/protos/multipass/common/TwoFactorDetails;",
        "position",
        "getItemId",
        "",
        "getView",
        "Landroid/view/View;",
        "convertView",
        "parent",
        "Landroid/view/ViewGroup;",
        "authenticator-views_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/login/SmsPickerCoordinator;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/login/SmsPickerCoordinator;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 72
    iput-object p1, p0, Lcom/squareup/ui/login/SmsPickerCoordinator$TwoFactorDetailsAdapter;->this$0:Lcom/squareup/ui/login/SmsPickerCoordinator;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/login/SmsPickerCoordinator$TwoFactorDetailsAdapter;->this$0:Lcom/squareup/ui/login/SmsPickerCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/login/SmsPickerCoordinator;->access$getSmsTwoFactorDetails$p(Lcom/squareup/ui/login/SmsPickerCoordinator;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/squareup/protos/multipass/common/TwoFactorDetails;
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/login/SmsPickerCoordinator$TwoFactorDetailsAdapter;->this$0:Lcom/squareup/ui/login/SmsPickerCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/login/SmsPickerCoordinator;->access$getSmsTwoFactorDetails$p(Lcom/squareup/ui/login/SmsPickerCoordinator;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    return-object p1
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 0

    .line 72
    invoke-virtual {p0, p1}, Lcom/squareup/ui/login/SmsPickerCoordinator$TwoFactorDetailsAdapter;->getItem(I)Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    move-result-object p1

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    const-string v0, "parent"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    instance-of v0, p2, Lcom/squareup/ui/account/view/LineRow;

    if-nez v0, :cond_0

    const/4 p2, 0x0

    :cond_0
    check-cast p2, Lcom/squareup/ui/account/view/LineRow;

    if-eqz p2, :cond_1

    goto :goto_0

    :cond_1
    new-instance p2, Lcom/squareup/ui/account/view/LineRow$Builder;

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p3

    invoke-direct {p2, p3}, Lcom/squareup/ui/account/view/LineRow$Builder;-><init>(Landroid/content/Context;)V

    .line 83
    sget-object p3, Lcom/squareup/marin/widgets/ChevronVisibility;->VISIBLE:Lcom/squareup/marin/widgets/ChevronVisibility;

    invoke-virtual {p2, p3}, Lcom/squareup/ui/account/view/LineRow$Builder;->setChevronVisibility(Lcom/squareup/marin/widgets/ChevronVisibility;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object p2

    .line 84
    invoke-virtual {p2}, Lcom/squareup/ui/account/view/LineRow$Builder;->build()Lcom/squareup/ui/account/view/LineRow;

    move-result-object p2

    .line 86
    :goto_0
    invoke-virtual {p0, p1}, Lcom/squareup/ui/login/SmsPickerCoordinator$TwoFactorDetailsAdapter;->getItem(I)Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->description:Ljava/lang/String;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p2, p1}, Lcom/squareup/ui/account/view/LineRow;->setTitle(Ljava/lang/CharSequence;)V

    const-string p1, "lineRow"

    .line 87
    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Landroid/view/View;

    return-object p2
.end method
