.class public final Lcom/squareup/ui/login/CreateAccountCanceledListener_NoCreateAccountCanceledListenerModule_Companion_ProvideCreateAccountCanceledListenerFactory;
.super Ljava/lang/Object;
.source "CreateAccountCanceledListener_NoCreateAccountCanceledListenerModule_Companion_ProvideCreateAccountCanceledListenerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/login/CreateAccountCanceledListener_NoCreateAccountCanceledListenerModule_Companion_ProvideCreateAccountCanceledListenerFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/login/CreateAccountCanceledListener;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/ui/login/CreateAccountCanceledListener_NoCreateAccountCanceledListenerModule_Companion_ProvideCreateAccountCanceledListenerFactory;
    .locals 1

    .line 23
    invoke-static {}, Lcom/squareup/ui/login/CreateAccountCanceledListener_NoCreateAccountCanceledListenerModule_Companion_ProvideCreateAccountCanceledListenerFactory$InstanceHolder;->access$000()Lcom/squareup/ui/login/CreateAccountCanceledListener_NoCreateAccountCanceledListenerModule_Companion_ProvideCreateAccountCanceledListenerFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideCreateAccountCanceledListener()Lcom/squareup/ui/login/CreateAccountCanceledListener;
    .locals 2

    .line 27
    sget-object v0, Lcom/squareup/ui/login/CreateAccountCanceledListener$NoCreateAccountCanceledListenerModule;->Companion:Lcom/squareup/ui/login/CreateAccountCanceledListener$NoCreateAccountCanceledListenerModule$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/login/CreateAccountCanceledListener$NoCreateAccountCanceledListenerModule$Companion;->provideCreateAccountCanceledListener()Lcom/squareup/ui/login/CreateAccountCanceledListener;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/login/CreateAccountCanceledListener;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/login/CreateAccountCanceledListener;
    .locals 1

    .line 18
    invoke-static {}, Lcom/squareup/ui/login/CreateAccountCanceledListener_NoCreateAccountCanceledListenerModule_Companion_ProvideCreateAccountCanceledListenerFactory;->provideCreateAccountCanceledListener()Lcom/squareup/ui/login/CreateAccountCanceledListener;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/ui/login/CreateAccountCanceledListener_NoCreateAccountCanceledListenerModule_Companion_ProvideCreateAccountCanceledListenerFactory;->get()Lcom/squareup/ui/login/CreateAccountCanceledListener;

    move-result-object v0

    return-object v0
.end method
