.class final Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator$attach$1$$special$$inlined$apply$lambda$1;
.super Ljava/lang/Object;
.source "TwoFactorDetailsPickerCoordinator.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator$attach$1;->invoke(Lcom/squareup/workflow/legacy/Screen;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nTwoFactorDetailsPickerCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 TwoFactorDetailsPickerCoordinator.kt\ncom/squareup/ui/login/TwoFactorDetailsPickerCoordinator$attach$1$4$1\n+ 2 AuthenticatorEvent.kt\ncom/squareup/ui/login/AuthenticatorEventKt\n*L\n1#1,94:1\n200#2:95\n*E\n*S KotlinDebug\n*F\n+ 1 TwoFactorDetailsPickerCoordinator.kt\ncom/squareup/ui/login/TwoFactorDetailsPickerCoordinator$attach$1$4$1\n*L\n61#1:95\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "<anonymous>",
        "",
        "run",
        "com/squareup/ui/login/TwoFactorDetailsPickerCoordinator$attach$1$4$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $screen$inlined:Lcom/squareup/ui/login/AuthenticatorScreen$PickTwoFactorMethod;

.field final synthetic $workflow$inlined:Lcom/squareup/workflow/legacy/WorkflowInput;

.field final synthetic this$0:Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator$attach$1;


# direct methods
.method constructor <init>(Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator$attach$1;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/ui/login/AuthenticatorScreen$PickTwoFactorMethod;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator$attach$1$$special$$inlined$apply$lambda$1;->this$0:Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator$attach$1;

    iput-object p2, p0, Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator$attach$1$$special$$inlined$apply$lambda$1;->$workflow$inlined:Lcom/squareup/workflow/legacy/WorkflowInput;

    iput-object p3, p0, Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator$attach$1$$special$$inlined$apply$lambda$1;->$screen$inlined:Lcom/squareup/ui/login/AuthenticatorScreen$PickTwoFactorMethod;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator$attach$1$$special$$inlined$apply$lambda$1;->$workflow$inlined:Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 95
    new-instance v1, Lcom/squareup/ui/login/AuthenticatorEvent$GoBackFrom;

    const-class v2, Lcom/squareup/ui/login/AuthenticatorScreen$PickTwoFactorMethod;

    invoke-direct {v1, v2}, Lcom/squareup/ui/login/AuthenticatorEvent$GoBackFrom;-><init>(Ljava/lang/Class;)V

    .line 61
    invoke-interface {v0, v1}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method
