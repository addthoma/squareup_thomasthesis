.class public final Lcom/squareup/ui/login/LocalSettingTrustedDeviceDetailsStore_Factory;
.super Ljava/lang/Object;
.source "LocalSettingTrustedDeviceDetailsStore_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/login/LocalSettingTrustedDeviceDetailsStore;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;",
            ">;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;",
            ">;>;>;)V"
        }
    .end annotation

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/squareup/ui/login/LocalSettingTrustedDeviceDetailsStore_Factory;->arg0Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/ui/login/LocalSettingTrustedDeviceDetailsStore_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;",
            ">;>;>;)",
            "Lcom/squareup/ui/login/LocalSettingTrustedDeviceDetailsStore_Factory;"
        }
    .end annotation

    .line 29
    new-instance v0, Lcom/squareup/ui/login/LocalSettingTrustedDeviceDetailsStore_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/ui/login/LocalSettingTrustedDeviceDetailsStore_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/settings/LocalSetting;)Lcom/squareup/ui/login/LocalSettingTrustedDeviceDetailsStore;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;",
            ">;>;)",
            "Lcom/squareup/ui/login/LocalSettingTrustedDeviceDetailsStore;"
        }
    .end annotation

    .line 34
    new-instance v0, Lcom/squareup/ui/login/LocalSettingTrustedDeviceDetailsStore;

    invoke-direct {v0, p0}, Lcom/squareup/ui/login/LocalSettingTrustedDeviceDetailsStore;-><init>(Lcom/squareup/settings/LocalSetting;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/login/LocalSettingTrustedDeviceDetailsStore;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/ui/login/LocalSettingTrustedDeviceDetailsStore_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/LocalSetting;

    invoke-static {v0}, Lcom/squareup/ui/login/LocalSettingTrustedDeviceDetailsStore_Factory;->newInstance(Lcom/squareup/settings/LocalSetting;)Lcom/squareup/ui/login/LocalSettingTrustedDeviceDetailsStore;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/ui/login/LocalSettingTrustedDeviceDetailsStore_Factory;->get()Lcom/squareup/ui/login/LocalSettingTrustedDeviceDetailsStore;

    move-result-object v0

    return-object v0
.end method
