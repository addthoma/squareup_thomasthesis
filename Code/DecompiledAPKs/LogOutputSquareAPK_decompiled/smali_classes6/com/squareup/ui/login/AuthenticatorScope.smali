.class public final Lcom/squareup/ui/login/AuthenticatorScope;
.super Lcom/squareup/ui/loggedout/InLoggedOutActivityScope;
.source "AuthenticatorScope.kt"

# interfaces
.implements Lcom/squareup/container/RegistersInScope;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/login/AuthenticatorScope$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/login/AuthenticatorScope$ParentComponent;,
        Lcom/squareup/ui/login/AuthenticatorScope$Component;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAuthenticatorScope.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AuthenticatorScope.kt\ncom/squareup/ui/login/AuthenticatorScope\n+ 2 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,74:1\n35#2:75\n35#2:76\n*E\n*S KotlinDebug\n*F\n+ 1 AuthenticatorScope.kt\ncom/squareup/ui/login/AuthenticatorScope\n*L\n39#1:75\n45#1:76\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\u0008\u00c7\u0002\u0018\u00002\u00020\u00012\u00020\u0002:\u0002\u000e\u000fB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0016J\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\nH\u0016R\u001e\u0010\u0004\u001a\u0010\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00000\u00000\u00058\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/ui/login/AuthenticatorScope;",
        "Lcom/squareup/ui/loggedout/InLoggedOutActivityScope;",
        "Lcom/squareup/container/RegistersInScope;",
        "()V",
        "CREATOR",
        "Lcom/squareup/container/ContainerTreeKey$PathCreator;",
        "kotlin.jvm.PlatformType",
        "buildScope",
        "Lmortar/MortarScope$Builder;",
        "parentScope",
        "Lmortar/MortarScope;",
        "register",
        "",
        "scope",
        "Component",
        "ParentComponent",
        "loggedout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Lcom/squareup/container/ContainerTreeKey$PathCreator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/container/ContainerTreeKey$PathCreator<",
            "Lcom/squareup/ui/login/AuthenticatorScope;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/login/AuthenticatorScope;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 21
    new-instance v0, Lcom/squareup/ui/login/AuthenticatorScope;

    invoke-direct {v0}, Lcom/squareup/ui/login/AuthenticatorScope;-><init>()V

    sput-object v0, Lcom/squareup/ui/login/AuthenticatorScope;->INSTANCE:Lcom/squareup/ui/login/AuthenticatorScope;

    .line 49
    sget-object v0, Lcom/squareup/ui/login/AuthenticatorScope;->INSTANCE:Lcom/squareup/ui/login/AuthenticatorScope;

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    const-string v1, "forSingleton(AuthenticatorScope)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/ui/login/AuthenticatorScope;->CREATOR:Lcom/squareup/container/ContainerTreeKey$PathCreator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 21
    invoke-direct {p0}, Lcom/squareup/ui/loggedout/InLoggedOutActivityScope;-><init>()V

    return-void
.end method


# virtual methods
.method public buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;
    .locals 2

    const-string v0, "parentScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-super {p0, p1}, Lcom/squareup/ui/loggedout/InLoggedOutActivityScope;->buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;

    move-result-object v0

    .line 75
    const-class v1, Lcom/squareup/ui/login/AuthenticatorScope$ParentComponent;

    invoke-static {p1, v1}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/login/AuthenticatorScope$ParentComponent;

    .line 40
    invoke-interface {p1}, Lcom/squareup/ui/login/AuthenticatorScope$ParentComponent;->authenticatorWorkflowRunner()Lcom/squareup/ui/login/AuthenticatorWorkflowRunner;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/squareup/ui/login/AuthenticatorWorkflowRunner;->registerServices(Lmortar/MortarScope$Builder;)V

    const-string p1, "super.buildScope(parentS\u2026egisterServices\n        )"

    .line 38
    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public register(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    const-class v0, Lcom/squareup/ui/login/AuthenticatorScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    .line 45
    check-cast v0, Lcom/squareup/ui/login/AuthenticatorScope$Component;

    .line 46
    invoke-interface {v0}, Lcom/squareup/ui/login/AuthenticatorScope$Component;->authenticatorScopeRunner()Lcom/squareup/ui/login/AuthenticatorScopeRunner;

    move-result-object v0

    check-cast v0, Lmortar/Scoped;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method
