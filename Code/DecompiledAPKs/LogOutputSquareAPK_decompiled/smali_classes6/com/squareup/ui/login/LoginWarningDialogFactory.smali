.class public final Lcom/squareup/ui/login/LoginWarningDialogFactory;
.super Ljava/lang/Object;
.source "LoginWarningDialogFactory.kt"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/login/LoginWarningDialogFactory$Factory;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001:\u0001\u0014B1\u0008\u0002\u0012\u0018\u0010\u0002\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00040\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\u0018\u0010\u000c\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u000e0\r2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u000c\u0010\u0011\u001a\u00020\u0012*\u00020\u0013H\u0002R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R \u0010\u0002\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/ui/login/LoginWarningDialogFactory;",
        "Lcom/squareup/workflow/DialogFactory;",
        "screenData",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning;",
        "Lcom/squareup/ui/login/AuthenticatorEvent;",
        "appNameFormatter",
        "Lcom/squareup/util/AppNameFormatter;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lio/reactivex/Observable;Lcom/squareup/util/AppNameFormatter;Lcom/squareup/util/Res;)V",
        "create",
        "Lio/reactivex/Single;",
        "Landroid/app/Dialog;",
        "context",
        "Landroid/content/Context;",
        "asWarning",
        "Lcom/squareup/widgets/warning/Warning;",
        "Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText;",
        "Factory",
        "authenticator-views_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final appNameFormatter:Lcom/squareup/util/AppNameFormatter;

.field private final res:Lcom/squareup/util/Res;

.field private final screenData:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning;",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lio/reactivex/Observable;Lcom/squareup/util/AppNameFormatter;Lcom/squareup/util/Res;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning;",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;>;",
            "Lcom/squareup/util/AppNameFormatter;",
            "Lcom/squareup/util/Res;",
            ")V"
        }
    .end annotation

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/login/LoginWarningDialogFactory;->screenData:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/ui/login/LoginWarningDialogFactory;->appNameFormatter:Lcom/squareup/util/AppNameFormatter;

    iput-object p3, p0, Lcom/squareup/ui/login/LoginWarningDialogFactory;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method public synthetic constructor <init>(Lio/reactivex/Observable;Lcom/squareup/util/AppNameFormatter;Lcom/squareup/util/Res;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 34
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/login/LoginWarningDialogFactory;-><init>(Lio/reactivex/Observable;Lcom/squareup/util/AppNameFormatter;Lcom/squareup/util/Res;)V

    return-void
.end method

.method public static final synthetic access$asWarning(Lcom/squareup/ui/login/LoginWarningDialogFactory;Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText;)Lcom/squareup/widgets/warning/Warning;
    .locals 0

    .line 34
    invoke-direct {p0, p1}, Lcom/squareup/ui/login/LoginWarningDialogFactory;->asWarning(Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText;)Lcom/squareup/widgets/warning/Warning;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getScreenData$p(Lcom/squareup/ui/login/LoginWarningDialogFactory;)Lio/reactivex/Observable;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/squareup/ui/login/LoginWarningDialogFactory;->screenData:Lio/reactivex/Observable;

    return-object p0
.end method

.method private final asWarning(Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText;)Lcom/squareup/widgets/warning/Warning;
    .locals 3

    .line 74
    sget-object v0, Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$AccountStatusErrorText;->INSTANCE:Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$AccountStatusErrorText;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance p1, Lcom/squareup/widgets/warning/WarningIds;

    .line 75
    sget v0, Lcom/squareup/common/authenticatorviews/R$string;->account_status_error_title:I

    .line 76
    sget v1, Lcom/squareup/common/authenticatorviews/R$string;->account_status_error_message:I

    .line 74
    invoke-direct {p1, v0, v1}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    check-cast p1, Lcom/squareup/widgets/warning/Warning;

    goto/16 :goto_0

    .line 78
    :cond_0
    sget-object v0, Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$EmailPasswordLoginFailedText;->INSTANCE:Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$EmailPasswordLoginFailedText;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance p1, Lcom/squareup/widgets/warning/WarningIds;

    .line 79
    sget v0, Lcom/squareup/common/authenticatorviews/R$string;->login_failed_title:I

    .line 80
    sget v1, Lcom/squareup/common/authenticatorviews/R$string;->login_failed_message:I

    .line 78
    invoke-direct {p1, v0, v1}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    check-cast p1, Lcom/squareup/widgets/warning/Warning;

    goto/16 :goto_0

    .line 82
    :cond_1
    sget-object v0, Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$DeviceCodeLoginFailedText;->INSTANCE:Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$DeviceCodeLoginFailedText;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance p1, Lcom/squareup/widgets/warning/WarningIds;

    .line 83
    sget v0, Lcom/squareup/common/authenticatorviews/R$string;->login_failed_title:I

    .line 84
    sget v1, Lcom/squareup/common/authenticatorviews/R$string;->device_code_login_failed_message:I

    .line 82
    invoke-direct {p1, v0, v1}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    check-cast p1, Lcom/squareup/widgets/warning/Warning;

    goto/16 :goto_0

    .line 86
    :cond_2
    sget-object v0, Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$ProvidedDeviceCodeAuthFailedText;->INSTANCE:Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$ProvidedDeviceCodeAuthFailedText;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance p1, Lcom/squareup/widgets/warning/WarningIds;

    .line 87
    sget v0, Lcom/squareup/common/authenticatorviews/R$string;->login_failed_title:I

    .line 88
    sget v1, Lcom/squareup/common/authenticatorviews/R$string;->device_code_auth_failed_message:I

    .line 86
    invoke-direct {p1, v0, v1}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    check-cast p1, Lcom/squareup/widgets/warning/Warning;

    goto :goto_0

    .line 90
    :cond_3
    sget-object v0, Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$NetworkErrorText;->INSTANCE:Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$NetworkErrorText;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance p1, Lcom/squareup/widgets/warning/WarningIds;

    .line 91
    sget v0, Lcom/squareup/common/strings/R$string;->network_error_title:I

    .line 92
    sget v1, Lcom/squareup/common/strings/R$string;->network_error_message:I

    .line 90
    invoke-direct {p1, v0, v1}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    check-cast p1, Lcom/squareup/widgets/warning/Warning;

    goto :goto_0

    .line 94
    :cond_4
    sget-object v0, Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$ServerErrorText;->INSTANCE:Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$ServerErrorText;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance p1, Lcom/squareup/widgets/warning/WarningIds;

    .line 95
    sget v0, Lcom/squareup/services/hairball/R$string;->square_server_error_title:I

    .line 96
    sget v1, Lcom/squareup/services/hairball/R$string;->square_server_error_message:I

    .line 94
    invoke-direct {p1, v0, v1}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    check-cast p1, Lcom/squareup/widgets/warning/Warning;

    goto :goto_0

    .line 98
    :cond_5
    sget-object v0, Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$SessionExpired;->INSTANCE:Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$SessionExpired;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    new-instance p1, Lcom/squareup/widgets/warning/WarningStrings;

    .line 99
    iget-object v0, p0, Lcom/squareup/ui/login/LoginWarningDialogFactory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/common/strings/R$string;->session_expired_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 100
    iget-object v1, p0, Lcom/squareup/ui/login/LoginWarningDialogFactory;->appNameFormatter:Lcom/squareup/util/AppNameFormatter;

    .line 101
    sget v2, Lcom/squareup/common/strings/R$string;->session_expired_message:I

    .line 100
    invoke-interface {v1, v2}, Lcom/squareup/util/AppNameFormatter;->getStringWithAppName(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 102
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 98
    invoke-direct {p1, v0, v1}, Lcom/squareup/widgets/warning/WarningStrings;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/widgets/warning/Warning;

    goto :goto_0

    .line 104
    :cond_6
    instance-of v0, p1, Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$CustomWarningText;

    if-eqz v0, :cond_7

    new-instance v0, Lcom/squareup/widgets/warning/WarningStrings;

    check-cast p1, Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$CustomWarningText;

    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$CustomWarningText;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$CustomWarningText;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/squareup/widgets/warning/WarningStrings;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/widgets/warning/Warning;

    :goto_0
    return-object p1

    :cond_7
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/login/LoginWarningDialogFactory;->screenData:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/ui/login/LoginWarningDialogFactory$create$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/login/LoginWarningDialogFactory$create$1;-><init>(Lcom/squareup/ui/login/LoginWarningDialogFactory;Landroid/content/Context;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    .line 70
    invoke-virtual {p1}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "screenData.map { (screen\u2026}\n        .firstOrError()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
