.class final Lcom/squareup/ui/login/UnitPickerCoordinator$attach$1$$special$$inlined$also$lambda$1;
.super Ljava/lang/Object;
.source "UnitPickerCoordinator.kt"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/UnitPickerCoordinator$attach$1;->invoke(Lcom/squareup/workflow/legacy/Screen;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0016\u0010\u0002\u001a\u0012\u0012\u0002\u0008\u0003 \u0004*\u0008\u0012\u0002\u0008\u0003\u0018\u00010\u00030\u00032\u000e\u0010\u0005\u001a\n \u0004*\u0004\u0018\u00010\u00060\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\n\u00a2\u0006\u0002\u0008\u000b\u00a8\u0006\u000c"
    }
    d2 = {
        "<anonymous>",
        "",
        "<anonymous parameter 0>",
        "Landroid/widget/AdapterView;",
        "kotlin.jvm.PlatformType",
        "<anonymous parameter 1>",
        "Landroid/view/View;",
        "position",
        "",
        "<anonymous parameter 3>",
        "",
        "onItemClick",
        "com/squareup/ui/login/UnitPickerCoordinator$attach$1$1$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $workflow$inlined:Lcom/squareup/workflow/legacy/WorkflowInput;

.field final synthetic this$0:Lcom/squareup/ui/login/UnitPickerCoordinator$attach$1;


# direct methods
.method constructor <init>(Lcom/squareup/ui/login/UnitPickerCoordinator$attach$1;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/login/UnitPickerCoordinator$attach$1$$special$$inlined$also$lambda$1;->this$0:Lcom/squareup/ui/login/UnitPickerCoordinator$attach$1;

    iput-object p2, p0, Lcom/squareup/ui/login/UnitPickerCoordinator$attach$1$$special$$inlined$also$lambda$1;->$workflow$inlined:Lcom/squareup/workflow/legacy/WorkflowInput;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .line 44
    iget-object p1, p0, Lcom/squareup/ui/login/UnitPickerCoordinator$attach$1$$special$$inlined$also$lambda$1;->$workflow$inlined:Lcom/squareup/workflow/legacy/WorkflowInput;

    new-instance p2, Lcom/squareup/ui/login/AuthenticatorEvent$SelectUnit;

    iget-object p4, p0, Lcom/squareup/ui/login/UnitPickerCoordinator$attach$1$$special$$inlined$also$lambda$1;->this$0:Lcom/squareup/ui/login/UnitPickerCoordinator$attach$1;

    iget-object p4, p4, Lcom/squareup/ui/login/UnitPickerCoordinator$attach$1;->this$0:Lcom/squareup/ui/login/UnitPickerCoordinator;

    invoke-virtual {p4}, Lcom/squareup/ui/login/UnitPickerCoordinator;->getAdapter$authenticator_views_release()Lcom/squareup/ui/login/UnitPickerCoordinator$UnitAdapter;

    move-result-object p4

    invoke-virtual {p4}, Lcom/squareup/ui/login/UnitPickerCoordinator$UnitAdapter;->getUnits()Ljava/util/List;

    move-result-object p4

    invoke-interface {p4, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/squareup/protos/register/api/Unit;

    iget-object p3, p3, Lcom/squareup/protos/register/api/Unit;->unit_token:Ljava/lang/String;

    const-string p4, "adapter.units[position].unit_token"

    invoke-static {p3, p4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p2, p3}, Lcom/squareup/ui/login/AuthenticatorEvent$SelectUnit;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, p2}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method
