.class public final Lcom/squareup/ui/login/AuthenticatorStateSerializationKt;
.super Ljava/lang/Object;
.source "AuthenticatorStateSerialization.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAuthenticatorStateSerialization.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AuthenticatorStateSerialization.kt\ncom/squareup/ui/login/AuthenticatorStateSerializationKt\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 Snapshot.kt\ncom/squareup/workflow/SnapshotKt\n+ 4 BuffersProtos.kt\ncom/squareup/workflow/BuffersProtos\n*L\n1#1,180:1\n1412#2,9:181\n1642#2,2:190\n1421#2:192\n1642#2,2:193\n1591#2,2:199\n180#3:195\n158#3,3:196\n161#3:201\n165#3:203\n148#3:204\n148#3:208\n148#3:210\n56#4:202\n99#4:205\n56#4:206\n56#4:207\n56#4:209\n56#4:211\n99#4:212\n*E\n*S KotlinDebug\n*F\n+ 1 AuthenticatorStateSerialization.kt\ncom/squareup/ui/login/AuthenticatorStateSerializationKt\n*L\n66#1,9:181\n66#1,2:190\n66#1:192\n68#1,2:193\n85#1,2:199\n74#1:195\n85#1,3:196\n85#1:201\n157#1:203\n157#1:204\n166#1:208\n171#1:210\n155#1:202\n161#1:205\n163#1:206\n164#1:207\n167#1:209\n172#1:211\n173#1:212\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u001a\u0012\u0010\u0000\u001a\u0004\u0018\u00010\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\u0002\u001a\u000c\u0010\u0004\u001a\u00020\u0003*\u00020\u0005H\u0002\u001a\u000c\u0010\u0006\u001a\u00020\u0007*\u00020\u0005H\u0002\u001a\n\u0010\u0008\u001a\u00020\t*\u00020\u0005\u001a\u000c\u0010\n\u001a\u00020\u000b*\u00020\u0005H\u0002\u001a\u0014\u0010\u000c\u001a\u00020\r*\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0007H\u0002\u001a\u0012\u0010\u0010\u001a\u00020\r*\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\t\u001a\u0014\u0010\u0012\u001a\u00020\r*\u00020\u000e2\u0006\u0010\u0013\u001a\u00020\u000bH\u0002\u00a8\u0006\u0014"
    }
    d2 = {
        "writeAuthenticatorScreen",
        "Lokio/ByteString;",
        "result",
        "Lcom/squareup/ui/login/AuthenticatorScreen;",
        "readAuthenticatorScreen",
        "Lokio/BufferedSource;",
        "readAuthenticatorStack",
        "Lcom/squareup/ui/login/AuthenticatorStack;",
        "readAuthenticatorState",
        "Lcom/squareup/ui/login/AuthenticatorState;",
        "readUnitsAndMerchants",
        "Lcom/squareup/ui/login/UnitsAndMerchants;",
        "writeAuthenticatorStack",
        "",
        "Lokio/BufferedSink;",
        "stack",
        "writeAuthenticatorState",
        "state",
        "writeUnitsAndMerchants",
        "unitsAndMerchants",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private static final readAuthenticatorScreen(Lokio/BufferedSource;)Lcom/squareup/ui/login/AuthenticatorScreen;
    .locals 11

    .line 136
    invoke-static {p0}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v0

    .line 139
    const-class v1, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    .line 140
    invoke-static {p0}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result v4

    .line 141
    invoke-static {p0}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result v5

    .line 142
    invoke-static {p0}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object p0

    move-object v0, p0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v6, p0

    goto :goto_0

    :cond_0
    move-object v6, v2

    :goto_0
    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x18

    const/4 v10, 0x0

    .line 139
    new-instance p0, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;

    move-object v3, p0

    invoke-direct/range {v3 .. v10}, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;-><init>(ZZLjava/lang/String;ZLcom/squareup/account/SecretString;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreen;

    goto/16 :goto_3

    .line 144
    :cond_1
    const-class v1, Lcom/squareup/ui/login/AuthenticatorScreen$DeviceCode;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz v1, :cond_2

    new-instance p0, Lcom/squareup/ui/login/AuthenticatorScreen$DeviceCode;

    invoke-direct {p0, v4, v3, v2}, Lcom/squareup/ui/login/AuthenticatorScreen$DeviceCode;-><init>(IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreen;

    goto/16 :goto_3

    .line 145
    :cond_2
    const-class v1, Lcom/squareup/ui/login/AuthenticatorScreen$ProvidedDeviceCodeAuth;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object p0, Lcom/squareup/ui/login/AuthenticatorScreen$ProvidedDeviceCodeAuth;->INSTANCE:Lcom/squareup/ui/login/AuthenticatorScreen$ProvidedDeviceCodeAuth;

    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreen;

    goto/16 :goto_3

    .line 146
    :cond_3
    const-class v1, Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPassword;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    const/4 v5, 0x2

    if-eqz v1, :cond_5

    .line 147
    invoke-static {p0}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object p0

    move-object v0, p0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    goto :goto_1

    :cond_4
    move-object p0, v2

    .line 146
    :goto_1
    new-instance v0, Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPassword;

    invoke-direct {v0, p0, v4, v5, v2}, Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPassword;-><init>(Ljava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    move-object p0, v0

    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreen;

    goto/16 :goto_3

    .line 149
    :cond_5
    const-class v1, Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPasswordFailed;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    new-instance v0, Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPasswordFailed;

    .line 150
    invoke-static {p0}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v1

    .line 151
    invoke-static {p0}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object p0

    .line 149
    invoke-direct {v0, v1, p0}, Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPasswordFailed;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object p0, v0

    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreen;

    goto/16 :goto_3

    .line 153
    :cond_6
    const-class v1, Lcom/squareup/ui/login/AuthenticatorScreen$PickUnit;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    new-instance v0, Lcom/squareup/ui/login/AuthenticatorScreen$PickUnit;

    invoke-static {p0}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0, v2, v5, v2}, Lcom/squareup/ui/login/AuthenticatorScreen$PickUnit;-><init>(Ljava/lang/String;Lcom/squareup/ui/login/UnitsAndMerchants;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    move-object p0, v0

    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreen;

    goto/16 :goto_3

    .line 154
    :cond_7
    const-class v1, Lcom/squareup/ui/login/AuthenticatorScreen$PickMerchant;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    new-instance p0, Lcom/squareup/ui/login/AuthenticatorScreen$PickMerchant;

    invoke-direct {p0, v2, v3, v2}, Lcom/squareup/ui/login/AuthenticatorScreen$PickMerchant;-><init>(Lcom/squareup/ui/login/UnitsAndMerchants;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreen;

    goto/16 :goto_3

    .line 155
    :cond_8
    const-class v1, Lcom/squareup/ui/login/AuthenticatorScreen$ShowLoginAlert;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 202
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const-class v1, Lcom/squareup/protos/multipass/mobile/Alert;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/squareup/workflow/BuffersProtos;->readProtoWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/Message;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/multipass/mobile/Alert;

    .line 155
    new-instance v0, Lcom/squareup/ui/login/AuthenticatorScreen$ShowLoginAlert;

    invoke-direct {v0, p0}, Lcom/squareup/ui/login/AuthenticatorScreen$ShowLoginAlert;-><init>(Lcom/squareup/protos/multipass/mobile/Alert;)V

    move-object p0, v0

    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreen;

    goto/16 :goto_3

    .line 156
    :cond_9
    const-class v1, Lcom/squareup/ui/login/AuthenticatorScreen$PickTwoFactorMethod;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    const-string v2, "T::class.java.enumConstants[readInt()]"

    if-eqz v1, :cond_b

    .line 203
    invoke-interface {p0}, Lokio/BufferedSource;->readInt()I

    move-result v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    :goto_2
    if-ge v4, v0, :cond_a

    .line 204
    const-class v3, Lcom/squareup/ui/login/TwoFactorDetailsMethod;

    invoke-virtual {v3}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/Enum;

    invoke-interface {p0}, Lokio/BufferedSource;->readInt()I

    move-result v5

    aget-object v3, v3, v5

    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Lcom/squareup/ui/login/TwoFactorDetailsMethod;

    .line 203
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_a
    check-cast v1, Ljava/util/List;

    .line 158
    invoke-static {p0}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result p0

    .line 159
    new-instance v0, Lcom/squareup/ui/login/AuthenticatorScreen$PickTwoFactorMethod;

    invoke-direct {v0, v1, p0}, Lcom/squareup/ui/login/AuthenticatorScreen$PickTwoFactorMethod;-><init>(Ljava/util/List;Z)V

    move-object p0, v0

    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreen;

    goto/16 :goto_3

    .line 161
    :cond_b
    const-class v1, Lcom/squareup/ui/login/AuthenticatorScreen$PickSms;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 205
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const-class v1, Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/squareup/workflow/BuffersProtos;->readProtosWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Ljava/util/List;

    move-result-object p0

    .line 161
    new-instance v0, Lcom/squareup/ui/login/AuthenticatorScreen$PickSms;

    invoke-direct {v0, p0}, Lcom/squareup/ui/login/AuthenticatorScreen$PickSms;-><init>(Ljava/util/List;)V

    move-object p0, v0

    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreen;

    goto/16 :goto_3

    .line 162
    :cond_c
    const-class v1, Lcom/squareup/ui/login/AuthenticatorScreen$EnrollSms;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    new-instance v0, Lcom/squareup/ui/login/AuthenticatorScreen$EnrollSms;

    invoke-static {p0}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result p0

    invoke-direct {v0, p0}, Lcom/squareup/ui/login/AuthenticatorScreen$EnrollSms;-><init>(Z)V

    move-object p0, v0

    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreen;

    goto/16 :goto_3

    .line 163
    :cond_d
    const-class v1, Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthQr;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 206
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const-class v1, Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/squareup/workflow/BuffersProtos;->readProtoWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/Message;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    .line 163
    new-instance v0, Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthQr;

    invoke-direct {v0, p0}, Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthQr;-><init>(Lcom/squareup/protos/multipass/common/TwoFactorDetails;)V

    move-object p0, v0

    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreen;

    goto/16 :goto_3

    .line 164
    :cond_e
    const-class v1, Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthCode;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 207
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const-class v1, Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/squareup/workflow/BuffersProtos;->readProtoWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/Message;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    .line 164
    new-instance v0, Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthCode;

    invoke-direct {v0, p0}, Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthCode;-><init>(Lcom/squareup/protos/multipass/common/TwoFactorDetails;)V

    move-object p0, v0

    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreen;

    goto/16 :goto_3

    .line 165
    :cond_f
    const-class v1, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 208
    const-class v0, Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Enum;

    invoke-interface {p0}, Lokio/BufferedSource;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v5, v0

    check-cast v5, Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;

    .line 209
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const-class v1, Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/squareup/workflow/BuffersProtos;->readProtoWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/Message;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    .line 168
    invoke-static {p0}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x18

    const/4 v10, 0x0

    .line 165
    new-instance p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;

    move-object v3, p0

    invoke-direct/range {v3 .. v10}, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;-><init>(Lcom/squareup/protos/multipass/common/TwoFactorDetails;Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;ZILcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms$RequestedVerificationCode;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreen;

    goto :goto_3

    .line 170
    :cond_10
    const-class v1, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 210
    const-class v0, Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Enum;

    invoke-interface {p0}, Lokio/BufferedSource;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;

    .line 211
    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const-class v2, Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    invoke-virtual {v1, v2}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/squareup/workflow/BuffersProtos;->readProtoWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/Message;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    .line 212
    sget-object v2, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const-class v3, Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    invoke-virtual {v2, v3}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/squareup/workflow/BuffersProtos;->readProtosWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Ljava/util/List;

    move-result-object p0

    .line 170
    new-instance v2, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;

    invoke-direct {v2, p0, v1, v0}, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;-><init>(Ljava/util/List;Lcom/squareup/protos/multipass/common/TwoFactorDetails;Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;)V

    move-object p0, v2

    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreen;

    :goto_3
    return-object p0

    .line 175
    :cond_11
    new-instance p0, Ljava/lang/IllegalArgumentException;

    .line 176
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected a valid AuthenticatorScreen class name, got "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 175
    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0
.end method

.method private static final readAuthenticatorStack(Lokio/BufferedSource;)Lcom/squareup/ui/login/AuthenticatorStack;
    .locals 5

    .line 72
    invoke-interface {p0}, Lokio/BufferedSource;->readInt()I

    move-result v0

    .line 74
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    .line 77
    invoke-static {p0}, Lcom/squareup/workflow/SnapshotKt;->readByteStringWithLength(Lokio/BufferedSource;)Lokio/ByteString;

    move-result-object v3

    .line 195
    new-instance v4, Lokio/Buffer;

    invoke-direct {v4}, Lokio/Buffer;-><init>()V

    invoke-virtual {v4, v3}, Lokio/Buffer;->write(Lokio/ByteString;)Lokio/Buffer;

    move-result-object v3

    check-cast v3, Lokio/BufferedSource;

    .line 77
    invoke-static {v3}, Lcom/squareup/ui/login/AuthenticatorStateSerializationKt;->readAuthenticatorScreen(Lokio/BufferedSource;)Lcom/squareup/ui/login/AuthenticatorScreen;

    move-result-object v3

    .line 74
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    check-cast v1, Ljava/util/List;

    .line 73
    new-instance p0, Lcom/squareup/ui/login/AuthenticatorStack;

    invoke-direct {p0, v1}, Lcom/squareup/ui/login/AuthenticatorStack;-><init>(Ljava/util/List;)V

    return-object p0
.end method

.method public static final readAuthenticatorState(Lokio/BufferedSource;)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 9

    const-string v0, "$this$readAuthenticatorState"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    new-instance v0, Lcom/squareup/ui/login/AuthenticatorState;

    .line 50
    invoke-static {p0}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result v2

    .line 51
    new-instance v3, Lcom/squareup/account/SecretString;

    invoke-static {p0}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1}, Lcom/squareup/account/SecretString;-><init>(Ljava/lang/String;)V

    .line 52
    invoke-static {p0}, Lcom/squareup/ui/login/AuthenticatorStateSerializationKt;->readUnitsAndMerchants(Lokio/BufferedSource;)Lcom/squareup/ui/login/UnitsAndMerchants;

    move-result-object v4

    .line 53
    invoke-static {p0}, Lcom/squareup/ui/login/AuthenticatorStateSerializationKt;->readAuthenticatorStack(Lokio/BufferedSource;)Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object v5

    const/4 v6, 0x0

    const/16 v7, 0x10

    const/4 v8, 0x0

    move-object v1, v0

    .line 49
    invoke-direct/range {v1 .. v8}, Lcom/squareup/ui/login/AuthenticatorState;-><init>(ZLcom/squareup/account/SecretString;Lcom/squareup/ui/login/UnitsAndMerchants;Lcom/squareup/ui/login/AuthenticatorStack;Lcom/squareup/ui/login/Operation;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method

.method private static final readUnitsAndMerchants(Lokio/BufferedSource;)Lcom/squareup/ui/login/UnitsAndMerchants;
    .locals 3

    .line 62
    new-instance v0, Lcom/squareup/ui/login/UnitsAndMerchants;

    sget-object v1, Lcom/squareup/protos/register/api/Unit;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-string v2, "SqUnit.ADAPTER"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0, v1}, Lcom/squareup/workflow/BuffersProtos;->readProtosWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Ljava/util/List;

    move-result-object p0

    invoke-direct {v0, p0}, Lcom/squareup/ui/login/UnitsAndMerchants;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method private static final writeAuthenticatorScreen(Lcom/squareup/ui/login/AuthenticatorScreen;)Lokio/ByteString;
    .locals 4

    .line 85
    new-instance v0, Lokio/Buffer;

    invoke-direct {v0}, Lokio/Buffer;-><init>()V

    .line 86
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "result.javaClass.name"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    move-object v2, v0

    check-cast v2, Lokio/BufferedSink;

    invoke-static {v2, v1}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 90
    instance-of v1, p0, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;

    const-string v3, ""

    if-eqz v1, :cond_1

    .line 91
    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;

    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->getAsLandingScreen()Z

    move-result v1

    invoke-static {v2, v1}, Lcom/squareup/workflow/SnapshotKt;->writeBooleanAsInt(Lokio/BufferedSink;Z)Lokio/BufferedSink;

    .line 92
    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->getSupportsDeviceCode()Z

    move-result v1

    invoke-static {v2, v1}, Lcom/squareup/workflow/SnapshotKt;->writeBooleanAsInt(Lokio/BufferedSink;Z)Lokio/BufferedSink;

    .line 94
    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->getEmailForPrefill()Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    move-object p0, v3

    :goto_0
    invoke-static {v2, p0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    goto/16 :goto_3

    .line 98
    :cond_1
    instance-of v1, p0, Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPassword;

    if-eqz v1, :cond_3

    .line 99
    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPassword;

    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPassword;->getEmailForPrefill()Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_2

    goto :goto_1

    :cond_2
    move-object p0, v3

    :goto_1
    invoke-static {v2, p0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    goto/16 :goto_3

    .line 101
    :cond_3
    instance-of v1, p0, Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPasswordFailed;

    if-eqz v1, :cond_4

    .line 102
    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPasswordFailed;

    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPasswordFailed;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 103
    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPasswordFailed;->getBody()Ljava/lang/String;

    move-result-object p0

    invoke-static {v2, p0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    goto/16 :goto_3

    .line 105
    :cond_4
    instance-of v1, p0, Lcom/squareup/ui/login/AuthenticatorScreen$PickUnit;

    if-eqz v1, :cond_5

    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreen$PickUnit;

    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorScreen$PickUnit;->getMerchantToken()Ljava/lang/String;

    move-result-object p0

    invoke-static {v2, p0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    goto/16 :goto_3

    .line 107
    :cond_5
    instance-of v1, p0, Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning;

    if-eqz v1, :cond_6

    const/4 p0, 0x0

    return-object p0

    .line 111
    :cond_6
    instance-of v1, p0, Lcom/squareup/ui/login/AuthenticatorScreen$ShowLoginAlert;

    if-eqz v1, :cond_7

    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreen$ShowLoginAlert;

    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorScreen$ShowLoginAlert;->getAlert()Lcom/squareup/protos/multipass/mobile/Alert;

    move-result-object p0

    check-cast p0, Lcom/squareup/wire/Message;

    invoke-static {v2, p0}, Lcom/squareup/workflow/BuffersProtos;->writeProtoWithLength(Lokio/BufferedSink;Lcom/squareup/wire/Message;)Lokio/BufferedSink;

    goto/16 :goto_3

    .line 112
    :cond_7
    instance-of v1, p0, Lcom/squareup/ui/login/AuthenticatorScreen$PickTwoFactorMethod;

    if-eqz v1, :cond_9

    .line 113
    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreen$PickTwoFactorMethod;

    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorScreen$PickTwoFactorMethod;->getMethods()Ljava/util/List;

    move-result-object v1

    .line 197
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    invoke-interface {v2, v3}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    .line 198
    check-cast v1, Ljava/lang/Iterable;

    .line 199
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 198
    check-cast v3, Lcom/squareup/ui/login/TwoFactorDetailsMethod;

    .line 113
    check-cast v3, Ljava/lang/Enum;

    invoke-static {v2, v3}, Lcom/squareup/workflow/SnapshotKt;->writeEnumByOrdinal(Lokio/BufferedSink;Ljava/lang/Enum;)Lokio/BufferedSink;

    goto :goto_2

    .line 114
    :cond_8
    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorScreen$PickTwoFactorMethod;->getCanSkipEnroll()Z

    move-result p0

    invoke-static {v2, p0}, Lcom/squareup/workflow/SnapshotKt;->writeBooleanAsInt(Lokio/BufferedSink;Z)Lokio/BufferedSink;

    goto/16 :goto_3

    .line 116
    :cond_9
    instance-of v1, p0, Lcom/squareup/ui/login/AuthenticatorScreen$PickSms;

    if-eqz v1, :cond_a

    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreen$PickSms;

    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorScreen$PickSms;->getSmsTwoFactorDetails()Ljava/util/List;

    move-result-object p0

    invoke-static {v2, p0}, Lcom/squareup/workflow/BuffersProtos;->writeProtosWithLength(Lokio/BufferedSink;Ljava/util/List;)Lokio/BufferedSink;

    goto :goto_3

    .line 117
    :cond_a
    instance-of v1, p0, Lcom/squareup/ui/login/AuthenticatorScreen$EnrollSms;

    if-eqz v1, :cond_b

    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreen$EnrollSms;

    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorScreen$EnrollSms;->getCanSkipEnroll()Z

    move-result p0

    invoke-static {v2, p0}, Lcom/squareup/workflow/SnapshotKt;->writeBooleanAsInt(Lokio/BufferedSink;Z)Lokio/BufferedSink;

    goto :goto_3

    .line 118
    :cond_b
    instance-of v1, p0, Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthQr;

    if-eqz v1, :cond_c

    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthQr;

    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthQr;->getTwoFactorDetails()Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    move-result-object p0

    check-cast p0, Lcom/squareup/wire/Message;

    invoke-static {v2, p0}, Lcom/squareup/workflow/BuffersProtos;->writeProtoWithLength(Lokio/BufferedSink;Lcom/squareup/wire/Message;)Lokio/BufferedSink;

    goto :goto_3

    .line 119
    :cond_c
    instance-of v1, p0, Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthCode;

    if-eqz v1, :cond_d

    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthCode;

    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthCode;->getTwoFactorDetails()Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    move-result-object p0

    check-cast p0, Lcom/squareup/wire/Message;

    invoke-static {v2, p0}, Lcom/squareup/workflow/BuffersProtos;->writeProtoWithLength(Lokio/BufferedSink;Lcom/squareup/wire/Message;)Lokio/BufferedSink;

    goto :goto_3

    .line 120
    :cond_d
    instance-of v1, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;

    if-eqz v1, :cond_e

    .line 121
    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;

    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->getLogin()Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;

    move-result-object v1

    check-cast v1, Ljava/lang/Enum;

    invoke-static {v2, v1}, Lcom/squareup/workflow/SnapshotKt;->writeEnumByOrdinal(Lokio/BufferedSink;Ljava/lang/Enum;)Lokio/BufferedSink;

    .line 122
    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->getSmsTwoFactorDetails()Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    move-result-object v1

    check-cast v1, Lcom/squareup/wire/Message;

    invoke-static {v2, v1}, Lcom/squareup/workflow/BuffersProtos;->writeProtoWithLength(Lokio/BufferedSink;Lcom/squareup/wire/Message;)Lokio/BufferedSink;

    .line 123
    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->getResendCodeLinkEnabled()Z

    move-result p0

    invoke-static {v2, p0}, Lcom/squareup/workflow/SnapshotKt;->writeBooleanAsInt(Lokio/BufferedSink;Z)Lokio/BufferedSink;

    goto :goto_3

    .line 125
    :cond_e
    instance-of v1, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;

    if-eqz v1, :cond_f

    .line 126
    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;

    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;->getLogin()Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;

    move-result-object v1

    check-cast v1, Ljava/lang/Enum;

    invoke-static {v2, v1}, Lcom/squareup/workflow/SnapshotKt;->writeEnumByOrdinal(Lokio/BufferedSink;Ljava/lang/Enum;)Lokio/BufferedSink;

    .line 127
    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;->getGoogleAuthTwoFactorDetails()Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    move-result-object v1

    check-cast v1, Lcom/squareup/wire/Message;

    invoke-static {v2, v1}, Lcom/squareup/workflow/BuffersProtos;->writeProtoWithLength(Lokio/BufferedSink;Lcom/squareup/wire/Message;)Lokio/BufferedSink;

    .line 128
    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;->getAllTwoFactorDetails()Ljava/util/List;

    move-result-object p0

    invoke-static {v2, p0}, Lcom/squareup/workflow/BuffersProtos;->writeProtosWithLength(Lokio/BufferedSink;Ljava/util/List;)Lokio/BufferedSink;

    .line 132
    :cond_f
    :goto_3
    invoke-virtual {v0}, Lokio/Buffer;->readByteString()Lokio/ByteString;

    move-result-object p0

    return-object p0
.end method

.method private static final writeAuthenticatorStack(Lokio/BufferedSink;Lcom/squareup/ui/login/AuthenticatorStack;)V
    .locals 2

    .line 66
    check-cast p1, Ljava/lang/Iterable;

    .line 181
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 190
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 189
    check-cast v1, Lcom/squareup/ui/login/AuthenticatorScreen;

    .line 66
    invoke-static {v1}, Lcom/squareup/ui/login/AuthenticatorStateSerializationKt;->writeAuthenticatorScreen(Lcom/squareup/ui/login/AuthenticatorScreen;)Lokio/ByteString;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 189
    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 192
    :cond_1
    check-cast v0, Ljava/util/List;

    .line 67
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p1

    invoke-interface {p0, p1}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    .line 68
    check-cast v0, Ljava/lang/Iterable;

    .line 193
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lokio/ByteString;

    .line 68
    invoke-static {p0, v0}, Lcom/squareup/workflow/SnapshotKt;->writeByteStringWithLength(Lokio/BufferedSink;Lokio/ByteString;)Lokio/BufferedSink;

    goto :goto_1

    :cond_2
    return-void
.end method

.method public static final writeAuthenticatorState(Lokio/BufferedSink;Lcom/squareup/ui/login/AuthenticatorState;)V
    .locals 1

    const-string v0, "$this$writeAuthenticatorState"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticatorState;->isWorldEnabled()Z

    move-result v0

    invoke-static {p0, v0}, Lcom/squareup/workflow/SnapshotKt;->writeBooleanAsInt(Lokio/BufferedSink;Z)Lokio/BufferedSink;

    .line 43
    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticatorState;->getTemporarySessionToken()Lcom/squareup/account/SecretString;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/account/SecretString;->getSecretValue()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 44
    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticatorState;->getUnitsAndMerchants()Lcom/squareup/ui/login/UnitsAndMerchants;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/squareup/ui/login/AuthenticatorStateSerializationKt;->writeUnitsAndMerchants(Lokio/BufferedSink;Lcom/squareup/ui/login/UnitsAndMerchants;)V

    .line 45
    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticatorState;->getScreens()Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/squareup/ui/login/AuthenticatorStateSerializationKt;->writeAuthenticatorStack(Lokio/BufferedSink;Lcom/squareup/ui/login/AuthenticatorStack;)V

    return-void
.end method

.method private static final writeUnitsAndMerchants(Lokio/BufferedSink;Lcom/squareup/ui/login/UnitsAndMerchants;)V
    .locals 0

    .line 58
    invoke-virtual {p1}, Lcom/squareup/ui/login/UnitsAndMerchants;->getUnits()Ljava/util/List;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/squareup/workflow/BuffersProtos;->writeProtosWithLength(Lokio/BufferedSink;Ljava/util/List;)Lokio/BufferedSink;

    return-void
.end method
