.class final Lcom/squareup/ui/login/RealAuthenticatorKt$modifyState$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealAuthenticator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/RealAuthenticatorKt;->modifyState(Lkotlin/jvm/functions/Function0;Lcom/squareup/ui/login/AuthenticatorOutput;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/WorkflowAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ui/login/AuthenticatorState;",
        "Lcom/squareup/ui/login/AuthenticatorState;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/ui/login/AuthenticatorState;",
        "oldState",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $block:Lkotlin/jvm/functions/Function1;


# direct methods
.method constructor <init>(Lkotlin/jvm/functions/Function1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/login/RealAuthenticatorKt$modifyState$1;->$block:Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/ui/login/AuthenticatorState;)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 8

    const-string v0, "oldState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1525
    iget-object v0, p0, Lcom/squareup/ui/login/RealAuthenticatorKt$modifyState$1;->$block:Lkotlin/jvm/functions/Function1;

    invoke-interface {v0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    move-object v0, p1

    check-cast v0, Lcom/squareup/ui/login/AuthenticatorState;

    .line 1526
    invoke-virtual {v0}, Lcom/squareup/ui/login/AuthenticatorState;->getScreens()Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/ui/login/RealAuthenticatorKt;->access$scrubPasswordWhenLeavingScreen(Lcom/squareup/ui/login/AuthenticatorStack;)Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object v4

    .line 1527
    invoke-virtual {v0}, Lcom/squareup/ui/login/AuthenticatorState;->getScreens()Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object p1

    if-eq v4, p1, :cond_0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x17

    const/4 v7, 0x0

    .line 1528
    invoke-static/range {v0 .. v7}, Lcom/squareup/ui/login/AuthenticatorState;->copy$default(Lcom/squareup/ui/login/AuthenticatorState;ZLcom/squareup/account/SecretString;Lcom/squareup/ui/login/UnitsAndMerchants;Lcom/squareup/ui/login/AuthenticatorStack;Lcom/squareup/ui/login/Operation;ILjava/lang/Object;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/squareup/ui/login/AuthenticatorState;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/login/RealAuthenticatorKt$modifyState$1;->invoke(Lcom/squareup/ui/login/AuthenticatorState;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    return-object p1
.end method
