.class public final Lcom/squareup/ui/login/AuthenticatorViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "AuthenticatorViewFactory.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/login/AuthenticatorViewFactory$DeviceCodeViewBinding;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001:\u0001\u0011B?\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\u0002\u0010\u0010\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/ui/login/AuthenticatorViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "emailPasswordFactory",
        "Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$Factory;",
        "loginAlertFactory",
        "Lcom/squareup/ui/login/LoginAlertDialogFactory$Factory;",
        "loginWarningFactory",
        "Lcom/squareup/ui/login/LoginWarningDialogFactory$Factory;",
        "enrollSmsFactory",
        "Lcom/squareup/ui/login/EnrollSmsCoordinator$Factory;",
        "enrollGoogleAuthCodeFactory",
        "Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator$Factory;",
        "verifySmsCodeFactory",
        "Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$Factory;",
        "deviceCodeViewBinding",
        "Lcom/squareup/ui/login/AuthenticatorViewFactory$DeviceCodeViewBinding;",
        "(Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$Factory;Lcom/squareup/ui/login/LoginAlertDialogFactory$Factory;Lcom/squareup/ui/login/LoginWarningDialogFactory$Factory;Lcom/squareup/ui/login/EnrollSmsCoordinator$Factory;Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator$Factory;Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$Factory;Lcom/squareup/ui/login/AuthenticatorViewFactory$DeviceCodeViewBinding;)V",
        "DeviceCodeViewBinding",
        "authenticator-views_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$Factory;Lcom/squareup/ui/login/LoginAlertDialogFactory$Factory;Lcom/squareup/ui/login/LoginWarningDialogFactory$Factory;Lcom/squareup/ui/login/EnrollSmsCoordinator$Factory;Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator$Factory;Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$Factory;Lcom/squareup/ui/login/AuthenticatorViewFactory$DeviceCodeViewBinding;)V
    .locals 25
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    move-object/from16 v5, p6

    const-string v6, "emailPasswordFactory"

    invoke-static {v0, v6}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v6, "loginAlertFactory"

    invoke-static {v1, v6}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v6, "loginWarningFactory"

    invoke-static {v2, v6}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v6, "enrollSmsFactory"

    invoke-static {v3, v6}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v6, "enrollGoogleAuthCodeFactory"

    invoke-static {v4, v6}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v6, "verifySmsCodeFactory"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v6, "deviceCodeViewBinding"

    move-object/from16 v7, p7

    invoke-static {v7, v6}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v6, 0x11

    new-array v6, v6, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 44
    sget-object v8, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 45
    sget-object v9, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EmailPasswordRendering;->Companion:Lcom/squareup/ui/login/AuthenticatorScreenRendering$EmailPasswordRendering$Companion;

    invoke-virtual {v9}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EmailPasswordRendering$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v9

    .line 46
    sget v10, Lcom/squareup/common/authenticatorviews/R$layout;->email_password_login_view:I

    .line 47
    new-instance v23, Lcom/squareup/workflow/ScreenHint;

    .line 48
    sget-object v17, Lcom/squareup/container/spot/Spots;->RIGHT:Lcom/squareup/container/spot/Spot;

    .line 49
    sget-object v11, Lcom/squareup/analytics/RegisterViewName;->WELCOME_SIGN_IN:Lcom/squareup/analytics/RegisterViewName;

    iget-object v15, v11, Lcom/squareup/analytics/RegisterViewName;->value:Ljava/lang/String;

    const-string v11, "WELCOME_SIGN_IN.value"

    invoke-static {v15, v11}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v16, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0xdf

    const/16 v22, 0x0

    move-object/from16 v11, v23

    move-object/from16 v24, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v18

    move/from16 v18, v19

    move/from16 v19, v20

    move-object/from16 v20, v24

    .line 47
    invoke-direct/range {v11 .. v22}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 51
    new-instance v11, Lcom/squareup/ui/login/AuthenticatorViewFactory$1;

    invoke-direct {v11, v0}, Lcom/squareup/ui/login/AuthenticatorViewFactory$1;-><init>(Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$Factory;)V

    move-object v13, v11

    check-cast v13, Lkotlin/jvm/functions/Function1;

    const/16 v14, 0x8

    const/4 v15, 0x0

    move-object/from16 v11, v23

    .line 44
    invoke-static/range {v8 .. v15}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v8, 0x0

    aput-object v0, v6, v8

    .line 57
    sget-object v9, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 58
    sget-object v0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ForgotPasswordRendering;->Companion:Lcom/squareup/ui/login/AuthenticatorScreenRendering$ForgotPasswordRendering$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ForgotPasswordRendering$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v10

    .line 59
    sget v11, Lcom/squareup/common/authenticatorviews/R$layout;->forgot_password_view:I

    .line 60
    new-instance v0, Lcom/squareup/workflow/ScreenHint;

    .line 61
    sget-object v18, Lcom/squareup/container/spot/Spots;->RIGHT_STABLE_ACTION_BAR:Lcom/squareup/container/spot/Spot;

    .line 62
    sget-object v8, Lcom/squareup/analytics/RegisterViewName;->WELCOME_RESET:Lcom/squareup/analytics/RegisterViewName;

    iget-object v8, v8, Lcom/squareup/analytics/RegisterViewName;->value:Ljava/lang/String;

    const-string v12, "WELCOME_RESET.value"

    invoke-static {v8, v12}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v22, 0xdf

    const/16 v23, 0x0

    move-object v12, v0

    move-object/from16 v21, v8

    .line 60
    invoke-direct/range {v12 .. v23}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 64
    sget-object v8, Lcom/squareup/ui/login/AuthenticatorViewFactory$2;->INSTANCE:Lcom/squareup/ui/login/AuthenticatorViewFactory$2;

    move-object v14, v8

    check-cast v14, Lkotlin/jvm/functions/Function1;

    const/16 v15, 0x8

    .line 57
    invoke-static/range {v9 .. v16}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v8, 0x1

    aput-object v0, v6, v8

    .line 67
    sget-object v0, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 68
    sget-object v8, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ForgotPasswordFailedRendering;->Companion:Lcom/squareup/ui/login/AuthenticatorScreenRendering$ForgotPasswordFailedRendering$Companion;

    invoke-virtual {v8}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ForgotPasswordFailedRendering$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v8

    .line 69
    sget-object v9, Lcom/squareup/ui/login/AuthenticatorViewFactory$3;->INSTANCE:Lcom/squareup/ui/login/AuthenticatorViewFactory$3;

    check-cast v9, Lkotlin/jvm/functions/Function1;

    .line 67
    invoke-virtual {v0, v8, v9}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lcom/squareup/workflow/legacy/Screen$Key;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v8, 0x2

    aput-object v0, v6, v8

    .line 72
    sget-object v9, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 73
    sget-object v0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$DeviceCodeRendering;->Companion:Lcom/squareup/ui/login/AuthenticatorScreenRendering$DeviceCodeRendering$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$DeviceCodeRendering$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v10

    .line 74
    invoke-interface/range {p7 .. p7}, Lcom/squareup/ui/login/AuthenticatorViewFactory$DeviceCodeViewBinding;->getLayoutId()I

    move-result v11

    .line 75
    new-instance v0, Lcom/squareup/workflow/ScreenHint;

    .line 76
    sget-object v18, Lcom/squareup/container/spot/Spots;->RIGHT_STABLE_ACTION_BAR:Lcom/squareup/container/spot/Spot;

    .line 77
    sget-object v8, Lcom/squareup/analytics/RegisterViewName;->WELCOME_DEVICE_CODE_SIGN_IN:Lcom/squareup/analytics/RegisterViewName;

    iget-object v8, v8, Lcom/squareup/analytics/RegisterViewName;->value:Ljava/lang/String;

    const-string v12, "WELCOME_DEVICE_CODE_SIGN_IN.value"

    invoke-static {v8, v12}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object v12, v0

    move-object/from16 v21, v8

    .line 75
    invoke-direct/range {v12 .. v23}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 79
    invoke-interface/range {p7 .. p7}, Lcom/squareup/ui/login/AuthenticatorViewFactory$DeviceCodeViewBinding;->getCoordinatorFactory()Lkotlin/jvm/functions/Function1;

    move-result-object v14

    const/16 v15, 0x8

    .line 72
    invoke-static/range {v9 .. v16}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v7, 0x3

    aput-object v0, v6, v7

    .line 82
    sget-object v8, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 83
    sget-object v0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ProvidedDeviceCodeAuthRendering;->Companion:Lcom/squareup/ui/login/AuthenticatorScreenRendering$ProvidedDeviceCodeAuthRendering$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ProvidedDeviceCodeAuthRendering$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v9

    .line 84
    sget v10, Lcom/squareup/common/authenticatorviews/R$layout;->device_code_auth_view:I

    .line 85
    new-instance v0, Lcom/squareup/workflow/ScreenHint;

    sget-object v17, Lcom/squareup/container/spot/Spots;->RIGHT_STABLE_ACTION_BAR:Lcom/squareup/container/spot/Spot;

    const/4 v12, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v18, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x1df

    const/16 v22, 0x0

    move-object v11, v0

    invoke-direct/range {v11 .. v22}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 86
    sget-object v7, Lcom/squareup/ui/login/AuthenticatorViewFactory$4;->INSTANCE:Lcom/squareup/ui/login/AuthenticatorViewFactory$4;

    move-object v13, v7

    check-cast v13, Lkotlin/jvm/functions/Function1;

    const/16 v14, 0x8

    .line 82
    invoke-static/range {v8 .. v15}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v7, 0x4

    aput-object v0, v6, v7

    .line 89
    sget-object v8, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 90
    sget-object v0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$PickUnitRendering;->Companion:Lcom/squareup/ui/login/AuthenticatorScreenRendering$PickUnitRendering$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$PickUnitRendering$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v9

    .line 91
    sget v10, Lcom/squareup/common/authenticatorviews/R$layout;->unit_picker_view:I

    .line 92
    new-instance v0, Lcom/squareup/workflow/ScreenHint;

    sget-object v17, Lcom/squareup/container/spot/Spots;->RIGHT_STABLE_ACTION_BAR:Lcom/squareup/container/spot/Spot;

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object v11, v0

    invoke-direct/range {v11 .. v22}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 93
    sget-object v7, Lcom/squareup/ui/login/AuthenticatorViewFactory$5;->INSTANCE:Lcom/squareup/ui/login/AuthenticatorViewFactory$5;

    move-object v13, v7

    check-cast v13, Lkotlin/jvm/functions/Function1;

    const/16 v14, 0x8

    .line 89
    invoke-static/range {v8 .. v15}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v7, 0x5

    aput-object v0, v6, v7

    .line 96
    sget-object v8, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 97
    sget-object v0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$PickMerchantRendering;->Companion:Lcom/squareup/ui/login/AuthenticatorScreenRendering$PickMerchantRendering$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$PickMerchantRendering$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v9

    .line 98
    sget v10, Lcom/squareup/common/authenticatorviews/R$layout;->merchant_picker_view:I

    .line 99
    new-instance v0, Lcom/squareup/workflow/ScreenHint;

    sget-object v17, Lcom/squareup/container/spot/Spots;->RIGHT_STABLE_ACTION_BAR:Lcom/squareup/container/spot/Spot;

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object v11, v0

    invoke-direct/range {v11 .. v22}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 100
    sget-object v7, Lcom/squareup/ui/login/AuthenticatorViewFactory$6;->INSTANCE:Lcom/squareup/ui/login/AuthenticatorViewFactory$6;

    move-object v13, v7

    check-cast v13, Lkotlin/jvm/functions/Function1;

    const/16 v14, 0x8

    .line 96
    invoke-static/range {v8 .. v15}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v7, 0x6

    aput-object v0, v6, v7

    .line 103
    sget-object v0, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 104
    sget-object v7, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ShowWarningRendering;->Companion:Lcom/squareup/ui/login/AuthenticatorScreenRendering$ShowWarningRendering$Companion;

    invoke-virtual {v7}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ShowWarningRendering$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v7

    .line 105
    new-instance v8, Lcom/squareup/ui/login/AuthenticatorViewFactory$7;

    invoke-direct {v8, v2}, Lcom/squareup/ui/login/AuthenticatorViewFactory$7;-><init>(Lcom/squareup/ui/login/LoginWarningDialogFactory$Factory;)V

    check-cast v8, Lkotlin/jvm/functions/Function1;

    .line 103
    invoke-virtual {v0, v7, v8}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lcom/squareup/workflow/legacy/Screen$Key;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v2, 0x7

    aput-object v0, v6, v2

    .line 108
    sget-object v0, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 109
    sget-object v2, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ShowLoginAlertRendering;->Companion:Lcom/squareup/ui/login/AuthenticatorScreenRendering$ShowLoginAlertRendering$Companion;

    invoke-virtual {v2}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ShowLoginAlertRendering$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 110
    new-instance v7, Lcom/squareup/ui/login/AuthenticatorViewFactory$8;

    invoke-direct {v7, v1}, Lcom/squareup/ui/login/AuthenticatorViewFactory$8;-><init>(Lcom/squareup/ui/login/LoginAlertDialogFactory$Factory;)V

    check-cast v7, Lkotlin/jvm/functions/Function1;

    .line 108
    invoke-virtual {v0, v2, v7}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lcom/squareup/workflow/legacy/Screen$Key;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/16 v1, 0x8

    aput-object v0, v6, v1

    .line 113
    sget-object v0, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 114
    sget-object v1, Lcom/squareup/ui/login/LoginGlassSpinner;->INSTANCE:Lcom/squareup/ui/login/LoginGlassSpinner;

    invoke-virtual {v1}, Lcom/squareup/ui/login/LoginGlassSpinner;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    .line 115
    sget-object v2, Lcom/squareup/ui/login/AuthenticatorViewFactory$9;->INSTANCE:Lcom/squareup/ui/login/AuthenticatorViewFactory$9;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 113
    invoke-virtual {v0, v1, v2}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lcom/squareup/workflow/legacy/Screen$Key;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/16 v1, 0x9

    aput-object v0, v6, v1

    .line 118
    sget-object v7, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 119
    sget-object v0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$PickTwoFactorMethodRendering;->Companion:Lcom/squareup/ui/login/AuthenticatorScreenRendering$PickTwoFactorMethodRendering$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$PickTwoFactorMethodRendering$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v8

    .line 120
    sget v9, Lcom/squareup/common/authenticatorviews/R$layout;->two_factor_details_picker_view:I

    .line 121
    new-instance v0, Lcom/squareup/workflow/ScreenHint;

    sget-object v16, Lcom/squareup/container/spot/Spots;->RIGHT_STABLE_ACTION_BAR:Lcom/squareup/container/spot/Spot;

    const/4 v11, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v17, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x1df

    const/16 v21, 0x0

    move-object v10, v0

    invoke-direct/range {v10 .. v21}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 122
    sget-object v1, Lcom/squareup/ui/login/AuthenticatorViewFactory$10;->INSTANCE:Lcom/squareup/ui/login/AuthenticatorViewFactory$10;

    move-object v12, v1

    check-cast v12, Lkotlin/jvm/functions/Function1;

    const/16 v13, 0x8

    .line 118
    invoke-static/range {v7 .. v14}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/16 v1, 0xa

    aput-object v0, v6, v1

    .line 125
    sget-object v7, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 126
    sget-object v0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$PickSmsRendering;->Companion:Lcom/squareup/ui/login/AuthenticatorScreenRendering$PickSmsRendering$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$PickSmsRendering$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v8

    .line 127
    sget v9, Lcom/squareup/common/authenticatorviews/R$layout;->sms_picker_view:I

    .line 128
    new-instance v0, Lcom/squareup/workflow/ScreenHint;

    sget-object v16, Lcom/squareup/container/spot/Spots;->RIGHT_STABLE_ACTION_BAR:Lcom/squareup/container/spot/Spot;

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v10, v0

    invoke-direct/range {v10 .. v21}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 129
    sget-object v1, Lcom/squareup/ui/login/AuthenticatorViewFactory$11;->INSTANCE:Lcom/squareup/ui/login/AuthenticatorViewFactory$11;

    move-object v12, v1

    check-cast v12, Lkotlin/jvm/functions/Function1;

    const/16 v13, 0x8

    .line 125
    invoke-static/range {v7 .. v14}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/16 v1, 0xb

    aput-object v0, v6, v1

    .line 132
    sget-object v7, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 133
    sget-object v0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollSmsRendering;->Companion:Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollSmsRendering$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollSmsRendering$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v8

    .line 134
    sget v9, Lcom/squareup/common/authenticatorviews/R$layout;->enroll_sms_view:I

    .line 135
    new-instance v0, Lcom/squareup/workflow/ScreenHint;

    sget-object v16, Lcom/squareup/container/spot/Spots;->RIGHT_STABLE_ACTION_BAR:Lcom/squareup/container/spot/Spot;

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v10, v0

    invoke-direct/range {v10 .. v21}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 136
    new-instance v1, Lcom/squareup/ui/login/AuthenticatorViewFactory$12;

    invoke-direct {v1, v3}, Lcom/squareup/ui/login/AuthenticatorViewFactory$12;-><init>(Lcom/squareup/ui/login/EnrollSmsCoordinator$Factory;)V

    move-object v12, v1

    check-cast v12, Lkotlin/jvm/functions/Function1;

    const/16 v13, 0x8

    .line 132
    invoke-static/range {v7 .. v14}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/16 v1, 0xc

    aput-object v0, v6, v1

    .line 139
    sget-object v7, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 140
    sget-object v0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthQrRendering;->Companion:Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthQrRendering$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthQrRendering$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v8

    .line 141
    sget v9, Lcom/squareup/common/authenticatorviews/R$layout;->enroll_google_auth_qr_view:I

    .line 142
    new-instance v0, Lcom/squareup/workflow/ScreenHint;

    sget-object v16, Lcom/squareup/container/spot/Spots;->RIGHT_STABLE_ACTION_BAR:Lcom/squareup/container/spot/Spot;

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v10, v0

    invoke-direct/range {v10 .. v21}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 143
    sget-object v1, Lcom/squareup/ui/login/AuthenticatorViewFactory$13;->INSTANCE:Lcom/squareup/ui/login/AuthenticatorViewFactory$13;

    move-object v12, v1

    check-cast v12, Lkotlin/jvm/functions/Function1;

    const/16 v13, 0x8

    .line 139
    invoke-static/range {v7 .. v14}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/16 v1, 0xd

    aput-object v0, v6, v1

    .line 146
    sget-object v7, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 147
    sget-object v0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthCodeRendering;->Companion:Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthCodeRendering$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthCodeRendering$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v8

    .line 148
    sget v9, Lcom/squareup/common/authenticatorviews/R$layout;->enroll_google_auth_code_view:I

    .line 149
    new-instance v0, Lcom/squareup/workflow/ScreenHint;

    sget-object v16, Lcom/squareup/container/spot/Spots;->RIGHT_STABLE_ACTION_BAR:Lcom/squareup/container/spot/Spot;

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v10, v0

    invoke-direct/range {v10 .. v21}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 150
    new-instance v1, Lcom/squareup/ui/login/AuthenticatorViewFactory$14;

    invoke-direct {v1, v4}, Lcom/squareup/ui/login/AuthenticatorViewFactory$14;-><init>(Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator$Factory;)V

    move-object v12, v1

    check-cast v12, Lkotlin/jvm/functions/Function1;

    const/16 v13, 0x8

    .line 146
    invoke-static/range {v7 .. v14}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/16 v1, 0xe

    aput-object v0, v6, v1

    .line 153
    sget-object v7, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 154
    sget-object v0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$VerifyCodeSmsRendering;->Companion:Lcom/squareup/ui/login/AuthenticatorScreenRendering$VerifyCodeSmsRendering$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$VerifyCodeSmsRendering$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v8

    .line 155
    sget v9, Lcom/squareup/common/authenticatorviews/R$layout;->verification_code_sms_view:I

    .line 156
    new-instance v0, Lcom/squareup/workflow/ScreenHint;

    sget-object v16, Lcom/squareup/container/spot/Spots;->RIGHT_STABLE_ACTION_BAR:Lcom/squareup/container/spot/Spot;

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v10, v0

    invoke-direct/range {v10 .. v21}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 157
    new-instance v1, Lcom/squareup/ui/login/AuthenticatorViewFactory$15;

    invoke-direct {v1, v5}, Lcom/squareup/ui/login/AuthenticatorViewFactory$15;-><init>(Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$Factory;)V

    move-object v12, v1

    check-cast v12, Lkotlin/jvm/functions/Function1;

    const/16 v13, 0x8

    .line 153
    invoke-static/range {v7 .. v14}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/16 v1, 0xf

    aput-object v0, v6, v1

    .line 160
    sget-object v7, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 161
    sget-object v0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$VerifyCodeGoogleAuthRendering;->Companion:Lcom/squareup/ui/login/AuthenticatorScreenRendering$VerifyCodeGoogleAuthRendering$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$VerifyCodeGoogleAuthRendering$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v8

    .line 162
    sget v9, Lcom/squareup/common/authenticatorviews/R$layout;->verification_code_google_auth_view:I

    .line 163
    new-instance v0, Lcom/squareup/workflow/ScreenHint;

    sget-object v16, Lcom/squareup/container/spot/Spots;->RIGHT_STABLE_ACTION_BAR:Lcom/squareup/container/spot/Spot;

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v10, v0

    invoke-direct/range {v10 .. v21}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 164
    sget-object v1, Lcom/squareup/ui/login/AuthenticatorViewFactory$16;->INSTANCE:Lcom/squareup/ui/login/AuthenticatorViewFactory$16;

    move-object v12, v1

    check-cast v12, Lkotlin/jvm/functions/Function1;

    const/16 v13, 0x8

    .line 160
    invoke-static/range {v7 .. v14}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/16 v1, 0x10

    aput-object v0, v6, v1

    move-object/from16 v0, p0

    .line 42
    invoke-direct {v0, v6}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
