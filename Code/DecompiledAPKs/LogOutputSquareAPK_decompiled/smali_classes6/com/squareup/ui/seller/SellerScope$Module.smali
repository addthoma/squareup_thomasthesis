.class public abstract Lcom/squareup/ui/seller/SellerScope$Module;
.super Ljava/lang/Object;
.source "SellerScope.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/ui/crm/flow/UpdateCustomerModule;,
        Lcom/squareup/ui/crm/edit/EditCustomerModule;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/seller/SellerScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Module"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideHost(Lcom/squareup/util/Device;Lcom/squareup/payment/Transaction;Lcom/squareup/payment/TransactionComps;Lcom/squareup/log/cart/TransactionInteractionsLogger;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/ui/main/DiningOptionCache;)Lcom/squareup/configure/item/ConfigureItemHost;
    .locals 8
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 78
    new-instance v7, Lcom/squareup/ui/configure/SellerScopeConfigureItemHost;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/configure/SellerScopeConfigureItemHost;-><init>(Lcom/squareup/util/Device;Lcom/squareup/payment/Transaction;Lcom/squareup/payment/TransactionComps;Lcom/squareup/log/cart/TransactionInteractionsLogger;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/ui/main/DiningOptionCache;)V

    return-object v7
.end method
