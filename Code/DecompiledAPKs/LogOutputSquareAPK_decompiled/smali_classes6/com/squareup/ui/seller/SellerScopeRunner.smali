.class public Lcom/squareup/ui/seller/SellerScopeRunner;
.super Ljava/lang/Object;
.source "SellerScopeRunner.java"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/seller/SellerScopeRunner$ClearOptions;
    }
.end annotation


# instance fields
.field private final addEligibleItemForCouponLegacyOutputs:Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponLegacyOutputs;

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final badBus:Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;

.field private final buyerFlowStarter:Lcom/squareup/ui/buyer/BuyerFlowStarter;

.field private final changeHudToaster:Lcom/squareup/tenderpayment/ChangeHudToaster;

.field private final chooseCustomerFlow:Lcom/squareup/ui/crm/ChooseCustomerFlow;

.field private final cogs:Lcom/squareup/cogs/Cogs;

.field private final drawerPresenter:Lcom/squareup/applet/AppletsDrawerPresenter;

.field private final duplicateSkuResultController:Lcom/squareup/sku/DuplicateSkuResultController;

.field private final entryHandler:Lcom/squareup/ui/main/CheckoutEntryHandler;

.field private final favoritePageTileCreator:Lcom/squareup/orderentry/FavoritePageTileCreator;

.field private final favoritesTileItemSelectionEvents:Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final flow:Lflow/Flow;

.field private final hudToaster:Lcom/squareup/cardreader/dipper/ReaderHudManager;

.field private final mainContainer:Lcom/squareup/ui/main/PosContainer;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final maybeX2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

.field private final openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

.field private final posApplet:Lcom/squareup/orderentry/OrderEntryAppletGateway;

.field private final res:Lcom/squareup/util/Res;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final swipeHandler:Lcom/squareup/ui/payment/SwipeHandler;

.field private final tenderCompleter:Lcom/squareup/tenderpayment/TenderCompleter;

.field private final ticketAutoIdentifiers:Lcom/squareup/print/TicketAutoIdentifiers;

.field private final transaction:Lcom/squareup/payment/Transaction;

.field private final transactionDiscountAdapter:Lcom/squareup/payment/TransactionDiscountAdapter;

.field private final transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;

.field private final variationSelectionDisposables:Lio/reactivex/disposables/CompositeDisposable;

.field private final workflowRunner:Lcom/squareup/ui/seller/SellerScopeWorkflowRunner;


# direct methods
.method public constructor <init>(Lcom/squareup/payment/Transaction;Lcom/squareup/payment/TransactionDiscountAdapter;Lcom/squareup/cardreader/dipper/ReaderHudManager;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/log/cart/TransactionInteractionsLogger;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/print/TicketAutoIdentifiers;Lcom/squareup/ui/payment/SwipeHandler;Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;Lflow/Flow;Lcom/squareup/tenderpayment/ChangeHudToaster;Lcom/squareup/util/Res;Lcom/squareup/tenderpayment/TenderCompleter;Lcom/squareup/ui/buyer/BuyerFlowStarter;Lcom/squareup/sku/DuplicateSkuResultController;Lcom/squareup/ui/main/CheckoutEntryHandler;Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;Lcom/squareup/orderentry/FavoritePageTileCreator;Lio/reactivex/Scheduler;Lcom/squareup/ui/crm/ChooseCustomerFlow;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/analytics/Analytics;Lcom/squareup/cogs/Cogs;Lcom/squareup/settings/server/Features;Lcom/squareup/applet/AppletsDrawerPresenter;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/ui/seller/SellerScopeWorkflowRunner;Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponLegacyOutputs;)V
    .locals 2
    .param p20    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    .line 129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    new-instance v1, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v1}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner;->variationSelectionDisposables:Lio/reactivex/disposables/CompositeDisposable;

    move-object v1, p1

    .line 130
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    move-object v1, p2

    .line 131
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner;->transactionDiscountAdapter:Lcom/squareup/payment/TransactionDiscountAdapter;

    move-object v1, p3

    .line 132
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner;->hudToaster:Lcom/squareup/cardreader/dipper/ReaderHudManager;

    move-object v1, p4

    .line 133
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    move-object v1, p5

    .line 134
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner;->transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;

    move-object v1, p6

    .line 135
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner;->posApplet:Lcom/squareup/orderentry/OrderEntryAppletGateway;

    move-object v1, p7

    .line 136
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    move-object v1, p8

    .line 137
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner;->ticketAutoIdentifiers:Lcom/squareup/print/TicketAutoIdentifiers;

    move-object v1, p9

    .line 138
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner;->swipeHandler:Lcom/squareup/ui/payment/SwipeHandler;

    move-object v1, p10

    .line 139
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner;->badBus:Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;

    move-object v1, p11

    .line 140
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner;->flow:Lflow/Flow;

    move-object v1, p12

    .line 141
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner;->changeHudToaster:Lcom/squareup/tenderpayment/ChangeHudToaster;

    move-object v1, p13

    .line 142
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner;->res:Lcom/squareup/util/Res;

    move-object/from16 v1, p14

    .line 143
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner;->tenderCompleter:Lcom/squareup/tenderpayment/TenderCompleter;

    move-object/from16 v1, p15

    .line 144
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner;->buyerFlowStarter:Lcom/squareup/ui/buyer/BuyerFlowStarter;

    move-object/from16 v1, p16

    .line 145
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner;->duplicateSkuResultController:Lcom/squareup/sku/DuplicateSkuResultController;

    move-object/from16 v1, p17

    .line 146
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner;->entryHandler:Lcom/squareup/ui/main/CheckoutEntryHandler;

    move-object/from16 v1, p18

    .line 147
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner;->favoritesTileItemSelectionEvents:Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;

    move-object/from16 v1, p19

    .line 148
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner;->favoritePageTileCreator:Lcom/squareup/orderentry/FavoritePageTileCreator;

    move-object/from16 v1, p20

    .line 149
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner;->mainScheduler:Lio/reactivex/Scheduler;

    move-object/from16 v1, p21

    .line 150
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner;->chooseCustomerFlow:Lcom/squareup/ui/crm/ChooseCustomerFlow;

    move-object/from16 v1, p22

    .line 151
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner;->maybeX2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    move-object/from16 v1, p23

    .line 152
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    move-object/from16 v1, p24

    .line 153
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner;->cogs:Lcom/squareup/cogs/Cogs;

    move-object/from16 v1, p25

    .line 154
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner;->features:Lcom/squareup/settings/server/Features;

    move-object/from16 v1, p26

    .line 155
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner;->drawerPresenter:Lcom/squareup/applet/AppletsDrawerPresenter;

    move-object/from16 v1, p27

    .line 156
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    move-object/from16 v1, p28

    .line 157
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner;->workflowRunner:Lcom/squareup/ui/seller/SellerScopeWorkflowRunner;

    move-object/from16 v1, p29

    .line 158
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner;->addEligibleItemForCouponLegacyOutputs:Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponLegacyOutputs;

    return-void
.end method

.method static synthetic lambda$null$3(Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$TilePosition;Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$ItemSelectionResult;)Landroid/util/Pair;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 219
    invoke-static {p0, p1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$onEnterScope$0(Lcom/squareup/container/ContainerTreeKey;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 181
    instance-of p0, p0, Lcom/squareup/ui/main/RegisterTreeKey;

    return p0
.end method

.method static synthetic lambda$onEnterScope$1(Lcom/squareup/container/ContainerTreeKey;)Ljava/lang/Boolean;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 193
    instance-of p0, p0, Lcom/squareup/ui/seller/EnablesCardSwipes;

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$onEnterScope$7(Lcom/squareup/ui/crm/ChooseCustomerFlow$Result;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 258
    invoke-virtual {p0}, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result;->getChooseCustomerResultKey()Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;

    move-result-object p0

    sget-object v0, Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;->ORDER_ENTRY:Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private onMultipleVariationsMatchingBarcode(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;",
            ">;)V"
        }
    .end annotation

    .line 390
    iget-object v0, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->variationSelectionDisposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->duplicateSkuResultController:Lcom/squareup/sku/DuplicateSkuResultController;

    invoke-interface {v1}, Lcom/squareup/sku/DuplicateSkuResultController;->variationSelection()Lio/reactivex/Observable;

    move-result-object v1

    .line 391
    invoke-virtual {v1}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/seller/-$$Lambda$hM66tjlM5LNZSSfzkGAk9VOo9kE;->INSTANCE:Lcom/squareup/ui/seller/-$$Lambda$hM66tjlM5LNZSSfzkGAk9VOo9kE;

    .line 392
    invoke-virtual {v1, v2}, Lio/reactivex/Single;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Maybe;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/seller/-$$Lambda$0aZYHUwyUbxJxGAHU0p3XUhjTVk;->INSTANCE:Lcom/squareup/ui/seller/-$$Lambda$0aZYHUwyUbxJxGAHU0p3XUhjTVk;

    .line 393
    invoke-virtual {v1, v2}, Lio/reactivex/Maybe;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Maybe;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/seller/-$$Lambda$SellerScopeRunner$9E65Jd7U4PKwxdpxgXarkhzMSBM;

    invoke-direct {v2, p0}, Lcom/squareup/ui/seller/-$$Lambda$SellerScopeRunner$9E65Jd7U4PKwxdpxgXarkhzMSBM;-><init>(Lcom/squareup/ui/seller/SellerScopeRunner;)V

    .line 394
    invoke-virtual {v1, v2}, Lio/reactivex/Maybe;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 390
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 397
    iget-object v0, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->duplicateSkuResultController:Lcom/squareup/sku/DuplicateSkuResultController;

    invoke-interface {v0, p1}, Lcom/squareup/sku/DuplicateSkuResultController;->showDuplicateSkuResult(Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public confirmClearCart()V
    .locals 1

    .line 325
    iget-object v0, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->reset()V

    return-void
.end method

.method public confirmRemoveNonLockedItems()V
    .locals 1

    .line 338
    iget-object v0, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->removeUnlockedItems()V

    return-void
.end method

.method public goToScreenAfterSaveOpenTicket()V
    .locals 2

    .line 308
    iget-object v0, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    .line 309
    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsAsHomeScreenEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 310
    invoke-static {}, Lcom/squareup/ui/ticket/MasterDetailTicketScreen;->forLoadTicket()Lcom/squareup/ui/ticket/MasterDetailTicketScreen;

    move-result-object v0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/orderentry/OrderEntryScreen;->LAST_SELECTED:Lcom/squareup/orderentry/OrderEntryScreen;

    .line 312
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->flow:Lflow/Flow;

    invoke-virtual {v1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$maybeProcessBarcodeScanned$11$SellerScopeRunner(Ljava/lang/String;Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/util/List;
    .locals 5

    .line 348
    const-class v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader;

    .line 349
    invoke-interface {p2, v0}, Lcom/squareup/shared/catalog/Catalog$Local;->getSyntheticTableReader(Ljava/lang/Class;)Lcom/squareup/shared/catalog/synthetictables/SyntheticTableReader;

    move-result-object p2

    check-cast p2, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader;

    .line 350
    iget-object v0, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    const/4 v1, 0x1

    new-array v2, v1, [Lcom/squareup/api/items/Item$Type;

    sget-object v3, Lcom/squareup/api/items/Item$Type;->GIFT_CARD:Lcom/squareup/api/items/Item$Type;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    .line 351
    invoke-virtual {v0, v2}, Lcom/squareup/settings/server/AccountStatusSettings;->supportedCatalogItemTypesExcluding([Lcom/squareup/api/items/Item$Type;)Ljava/util/List;

    move-result-object v0

    .line 350
    invoke-virtual {p2, p1, v0}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader;->getInfoForSku(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 356
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 357
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 358
    iget-object p1, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    new-array v1, v1, [Lcom/squareup/api/items/Item$Type;

    sget-object v3, Lcom/squareup/api/items/Item$Type;->GIFT_CARD:Lcom/squareup/api/items/Item$Type;

    aput-object v3, v1, v4

    .line 360
    invoke-virtual {p1, v1}, Lcom/squareup/settings/server/AccountStatusSettings;->supportedCatalogItemTypesExcluding([Lcom/squareup/api/items/Item$Type;)Ljava/util/List;

    move-result-object p1

    .line 359
    invoke-virtual {p2, v2, p1}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader;->getInfoForSku(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    .line 361
    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_0
    return-object v0
.end method

.method public synthetic lambda$maybeProcessBarcodeScanned$12$SellerScopeRunner(Ljava/lang/String;Lcom/squareup/shared/catalog/CatalogResult;)V
    .locals 0

    .line 366
    invoke-interface {p2}, Lcom/squareup/shared/catalog/CatalogResult;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/util/List;

    .line 367
    invoke-virtual {p0, p2, p1}, Lcom/squareup/ui/seller/SellerScopeRunner;->onResultOfVariationsMatchingBarcode(Ljava/util/List;Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$null$8$SellerScopeRunner(Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;Lflow/History$Builder;)Lflow/History$Builder;
    .locals 3

    .line 277
    invoke-virtual {p1}, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->getParentKeyForChild()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->getContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    iget-object v2, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->transactionDiscountAdapter:Lcom/squareup/payment/TransactionDiscountAdapter;

    .line 276
    invoke-static {v0, p1, v1, v2}, Lcom/squareup/ui/crm/flow/CrmScope;->newViewInTransactionCrmScreen(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/payment/Transaction;Lcom/squareup/checkout/HoldsCoupons;)Lcom/squareup/ui/crm/flow/InCrmScope;

    move-result-object p1

    invoke-virtual {p2, p1}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    return-object p2
.end method

.method public synthetic lambda$onEnterScope$10$SellerScopeRunner(Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponOutput$MatchingItemAdded;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 296
    iget-object v0, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->entryHandler:Lcom/squareup/ui/main/CheckoutEntryHandler;

    invoke-virtual {p1}, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponOutput$MatchingItemAdded;->getItem()Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object p1

    const/4 v1, 0x0

    const-string v2, ""

    invoke-virtual {v0, v1, p1, v2}, Lcom/squareup/ui/main/CheckoutEntryHandler;->itemClicked(Landroid/widget/ImageView;Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$onEnterScope$2$SellerScopeRunner(Ljava/lang/Boolean;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 195
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 196
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->buyerFlowStarter:Lcom/squareup/ui/buyer/BuyerFlowStarter;

    .line 197
    invoke-interface {p1}, Lcom/squareup/ui/buyer/BuyerFlowStarter;->isInTransitionToBuyerFlow()Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 195
    :goto_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onEnterScope$4$SellerScopeRunner(Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$TilePosition;)Lio/reactivex/ObservableSource;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 217
    iget-object v0, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->favoritesTileItemSelectionEvents:Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;

    .line 218
    invoke-virtual {v0}, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;->onItemSelectionFinished()Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->first()Lrx/Observable;

    move-result-object v0

    .line 217
    invoke-static {v0}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/seller/-$$Lambda$SellerScopeRunner$wFK0jBAZs57Aklmbv8ZV9V1B4DQ;

    invoke-direct {v1, p1}, Lcom/squareup/ui/seller/-$$Lambda$SellerScopeRunner$wFK0jBAZs57Aklmbv8ZV9V1B4DQ;-><init>(Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$TilePosition;)V

    .line 219
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onEnterScope$5$SellerScopeRunner(Landroid/util/Pair;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 221
    iget-object v0, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$TilePosition;

    .line 222
    iget-object p1, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast p1, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$ItemSelectionResult;

    .line 223
    instance-of v1, p1, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$CatalogItemSelected;

    if-eqz v1, :cond_0

    .line 224
    check-cast p1, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$CatalogItemSelected;

    .line 226
    iget-object v1, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->favoritePageTileCreator:Lcom/squareup/orderentry/FavoritePageTileCreator;

    .line 227
    invoke-virtual {p1}, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$CatalogItemSelected;->getCatalogObjectType()Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object v2

    .line 228
    invoke-virtual {p1}, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$CatalogItemSelected;->getCatalogObjectId()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$TilePosition;->pageId:Ljava/lang/String;

    iget-object v5, v0, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$TilePosition;->position:Landroid/graphics/Point;

    iget v6, v0, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$TilePosition;->columnCount:I

    .line 226
    invoke-virtual/range {v1 .. v6}, Lcom/squareup/orderentry/FavoritePageTileCreator;->createTile(Lcom/squareup/shared/catalog/models/CatalogObjectType;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Point;I)V

    goto :goto_0

    .line 232
    :cond_0
    instance-of v1, p1, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$CatalogPlaceholderSelected;

    if-eqz v1, :cond_1

    .line 233
    check-cast p1, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$CatalogPlaceholderSelected;

    .line 235
    iget-object v1, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->favoritePageTileCreator:Lcom/squareup/orderentry/FavoritePageTileCreator;

    .line 236
    invoke-virtual {p1}, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$CatalogPlaceholderSelected;->getPlaceholderType()Lcom/squareup/api/items/Placeholder$PlaceholderType;

    move-result-object v2

    .line 237
    invoke-virtual {p1}, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$CatalogPlaceholderSelected;->getPlaceholderName()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$TilePosition;->pageId:Ljava/lang/String;

    iget-object v5, v0, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$TilePosition;->position:Landroid/graphics/Point;

    iget v6, v0, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$TilePosition;->columnCount:I

    .line 235
    invoke-virtual/range {v1 .. v6}, Lcom/squareup/orderentry/FavoritePageTileCreator;->createTile(Lcom/squareup/api/items/Placeholder$PlaceholderType;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Point;I)V

    :cond_1
    :goto_0
    return-void
.end method

.method public synthetic lambda$onEnterScope$6$SellerScopeRunner(Lcom/squareup/ui/seller/SellerScopeWorkflowOutput$SeparatedPrintoutsFinished;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 249
    invoke-virtual {p0}, Lcom/squareup/ui/seller/SellerScopeRunner;->goToScreenAfterSaveOpenTicket()V

    return-void
.end method

.method public synthetic lambda$onEnterScope$9$SellerScopeRunner(Lcom/squareup/ui/crm/ChooseCustomerFlow$Result;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 260
    instance-of v0, p1, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;

    if-eqz v0, :cond_1

    .line 261
    check-cast p1, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;

    .line 263
    invoke-virtual {p1}, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->getContactChosenFromRecent()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "IN_CART_FROM_RECENT"

    goto :goto_0

    :cond_0
    const-string v0, "IN_CART_FROM_SEARCH"

    .line 266
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v2, Lcom/squareup/analytics/event/v1/DetailEvent;

    sget-object v3, Lcom/squareup/analytics/RegisterActionName;->CRM_ADD_CUSTOMER_TO_SALE:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {v2, v3, v0}, Lcom/squareup/analytics/event/v1/DetailEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 267
    iget-object v1, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v2, Lcom/squareup/crm/events/CrmAddCustomerToSaleEvent;

    invoke-direct {v2, v0}, Lcom/squareup/crm/events/CrmAddCustomerToSaleEvent;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    .line 272
    iget-object v0, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->flow:Lflow/Flow;

    sget-object v1, Lflow/Direction;->FORWARD:Lflow/Direction;

    const/4 v2, 0x2

    new-array v2, v2, [Lkotlin/jvm/functions/Function1;

    const/4 v3, 0x0

    .line 274
    invoke-virtual {p1}, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->getHistoryFuncForConfirmation()Lkotlin/jvm/functions/Function1;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    new-instance v4, Lcom/squareup/ui/seller/-$$Lambda$SellerScopeRunner$5xUpdlmsq-qWLtMZFmKRJbDQyPY;

    invoke-direct {v4, p0, p1}, Lcom/squareup/ui/seller/-$$Lambda$SellerScopeRunner$5xUpdlmsq-qWLtMZFmKRJbDQyPY;-><init>(Lcom/squareup/ui/seller/SellerScopeRunner;Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;)V

    aput-object v4, v2, v3

    .line 272
    invoke-static {v0, v1, v2}, Lcom/squareup/container/Flows;->editHistory(Lflow/Flow;Lflow/Direction;[Lkotlin/jvm/functions/Function1;)V

    goto :goto_1

    .line 284
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->maybeX2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {p1}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->dismissSaveCustomer()Z

    :goto_1
    return-void
.end method

.method public synthetic lambda$onMultipleVariationsMatchingBarcode$13$SellerScopeRunner(Lcom/squareup/sku/DuplicateSkuResultController$VariationSelection;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 395
    iget-object v0, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->entryHandler:Lcom/squareup/ui/main/CheckoutEntryHandler;

    invoke-virtual {p1}, Lcom/squareup/sku/DuplicateSkuResultController$VariationSelection;->getItemId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/sku/DuplicateSkuResultController$VariationSelection;->getVariationId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lcom/squareup/ui/main/CheckoutEntryHandler;->itemScanned(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public maybeClearCart()V
    .locals 6

    .line 316
    new-instance v0, Lcom/squareup/register/widgets/ConfirmationStrings;

    iget-object v1, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/orderentry/R$string;->clear_sale:I

    .line 317
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/orderentry/R$string;->are_you_sure:I

    .line 318
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/orderentry/R$string;->clear:I

    .line 319
    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->res:Lcom/squareup/util/Res;

    sget v5, Lcom/squareup/widgets/R$string;->cancel:I

    .line 320
    invoke-interface {v4, v5}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/register/widgets/ConfirmationStrings;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    iget-object v1, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->flow:Lflow/Flow;

    new-instance v2, Lcom/squareup/orderentry/ClearKeypadPanelDialogScreen;

    sget-object v3, Lcom/squareup/ui/seller/SellerScopeRunner$ClearOptions;->CLEAR_CART:Lcom/squareup/ui/seller/SellerScopeRunner$ClearOptions;

    invoke-direct {v2, v0, v3}, Lcom/squareup/orderentry/ClearKeypadPanelDialogScreen;-><init>(Lcom/squareup/register/widgets/Confirmation;Lcom/squareup/ui/seller/SellerScopeRunner$ClearOptions;)V

    invoke-virtual {v1, v2}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public maybeProcessBarcodeScanned(Ljava/lang/String;)V
    .locals 3

    .line 346
    iget-object v0, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BARCODE_SCANNERS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->drawerPresenter:Lcom/squareup/applet/AppletsDrawerPresenter;

    invoke-virtual {v0}, Lcom/squareup/applet/AppletsDrawerPresenter;->isDrawerOpen()Z

    move-result v0

    if-nez v0, :cond_0

    .line 347
    iget-object v0, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->cogs:Lcom/squareup/cogs/Cogs;

    new-instance v1, Lcom/squareup/ui/seller/-$$Lambda$SellerScopeRunner$tWhbQTAqcKIAfPXbRviGoBN-T2o;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/seller/-$$Lambda$SellerScopeRunner$tWhbQTAqcKIAfPXbRviGoBN-T2o;-><init>(Lcom/squareup/ui/seller/SellerScopeRunner;Ljava/lang/String;)V

    new-instance v2, Lcom/squareup/ui/seller/-$$Lambda$SellerScopeRunner$8PJZjinaECsO9BwaRkRl6cCNyEE;

    invoke-direct {v2, p0, p1}, Lcom/squareup/ui/seller/-$$Lambda$SellerScopeRunner$8PJZjinaECsO9BwaRkRl6cCNyEE;-><init>(Lcom/squareup/ui/seller/SellerScopeRunner;Ljava/lang/String;)V

    invoke-interface {v0, v1, v2}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    goto :goto_0

    :cond_0
    const/4 p1, 0x2

    new-array p1, p1, [Ljava/lang/Object;

    const/4 v0, 0x0

    .line 370
    iget-object v1, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->BARCODE_SCANNERS:Lcom/squareup/settings/server/Features$Feature;

    .line 371
    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, p1, v0

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->drawerPresenter:Lcom/squareup/applet/AppletsDrawerPresenter;

    invoke-virtual {v1}, Lcom/squareup/applet/AppletsDrawerPresenter;->isDrawerOpen()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, p1, v0

    const-string v0, "Ignoring barcode scanned. Feature enabled: %b, applets drawer open: %b"

    .line 370
    invoke-static {v0, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public maybeRemoveNonLockedItems()V
    .locals 6

    .line 329
    new-instance v0, Lcom/squareup/register/widgets/ConfirmationStrings;

    iget-object v1, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/orderentry/R$string;->open_tickets_clear_new_items:I

    .line 330
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/orderentry/R$string;->are_you_sure:I

    .line 331
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/orderentry/R$string;->clear:I

    .line 332
    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->res:Lcom/squareup/util/Res;

    sget v5, Lcom/squareup/widgets/R$string;->cancel:I

    .line 333
    invoke-interface {v4, v5}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/register/widgets/ConfirmationStrings;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    iget-object v1, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->flow:Lflow/Flow;

    new-instance v2, Lcom/squareup/orderentry/ClearKeypadPanelDialogScreen;

    sget-object v3, Lcom/squareup/ui/seller/SellerScopeRunner$ClearOptions;->CLEAR_NON_LOCKED_ITEMS:Lcom/squareup/ui/seller/SellerScopeRunner$ClearOptions;

    invoke-direct {v2, v0, v3}, Lcom/squareup/orderentry/ClearKeypadPanelDialogScreen;-><init>(Lcom/squareup/register/widgets/Confirmation;Lcom/squareup/ui/seller/SellerScopeRunner$ClearOptions;)V

    invoke-virtual {v1, v2}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 4

    .line 162
    iget-object v0, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->tenderCompleter:Lcom/squareup/tenderpayment/TenderCompleter;

    .line 163
    invoke-interface {v0}, Lcom/squareup/tenderpayment/TenderCompleter;->onPrepareChangeHud()Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->changeHudToaster:Lcom/squareup/tenderpayment/ChangeHudToaster;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/seller/-$$Lambda$Fd1YngF1n8ToDgc0eXJLlzzsYmw;

    invoke-direct {v2, v1}, Lcom/squareup/ui/seller/-$$Lambda$Fd1YngF1n8ToDgc0eXJLlzzsYmw;-><init>(Lcom/squareup/tenderpayment/ChangeHudToaster;)V

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 162
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 165
    iget-object v0, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->hudToaster:Lcom/squareup/cardreader/dipper/ReaderHudManager;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ReaderHudManager;->toastIfAnyReaderBatteryLow()V

    .line 170
    iget-object v0, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->isTakingPaymentFromInvoice()Z

    move-result v0

    if-nez v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->posApplet:Lcom/squareup/orderentry/OrderEntryAppletGateway;

    invoke-interface {v0}, Lcom/squareup/orderentry/OrderEntryAppletGateway;->selectApplet()V

    .line 174
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 175
    iget-object v0, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->ticketAutoIdentifiers:Lcom/squareup/print/TicketAutoIdentifiers;

    invoke-interface {v0}, Lcom/squareup/print/TicketAutoIdentifiers;->requestTicketIdentifierIfNecessary()V

    .line 179
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    .line 180
    invoke-static {v0}, Lcom/squareup/ui/main/PosContainers;->nextScreen(Lcom/squareup/ui/main/PosContainer;)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/seller/-$$Lambda$SellerScopeRunner$qfSSCukk2bgnyXPP-XmM6FbcecU;->INSTANCE:Lcom/squareup/ui/seller/-$$Lambda$SellerScopeRunner$qfSSCukk2bgnyXPP-XmM6FbcecU;

    .line 181
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    const-class v1, Lcom/squareup/ui/main/RegisterTreeKey;

    .line 182
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->cast(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/seller/-$$Lambda$JBWqocgx4XmYNXGsCyGGs8m9DgY;->INSTANCE:Lcom/squareup/ui/seller/-$$Lambda$JBWqocgx4XmYNXGsCyGGs8m9DgY;

    .line 183
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/seller/-$$Lambda$YW4ReCsGjZbYQwkjJ2JRLutSdL4;

    invoke-direct {v2, v1}, Lcom/squareup/ui/seller/-$$Lambda$YW4ReCsGjZbYQwkjJ2JRLutSdL4;-><init>(Lcom/squareup/log/cart/TransactionInteractionsLogger;)V

    .line 184
    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 179
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 190
    iget-object v0, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    .line 191
    invoke-interface {v0}, Lcom/squareup/ui/main/PosContainer;->hasViewNow()Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    .line 192
    invoke-static {v1}, Lcom/squareup/ui/main/PosContainers;->nextScreen(Lcom/squareup/ui/main/PosContainer;)Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/seller/-$$Lambda$SellerScopeRunner$S_ZwYV21LcUgqkS8i6kR_mg0nyg;->INSTANCE:Lcom/squareup/ui/seller/-$$Lambda$SellerScopeRunner$S_ZwYV21LcUgqkS8i6kR_mg0nyg;

    .line 193
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/seller/-$$Lambda$SellerScopeRunner$xU2kTa8QP7IWWwYco51Adrhq9ZM;

    invoke-direct {v2, p0}, Lcom/squareup/ui/seller/-$$Lambda$SellerScopeRunner$xU2kTa8QP7IWWwYco51Adrhq9ZM;-><init>(Lcom/squareup/ui/seller/SellerScopeRunner;)V

    .line 190
    invoke-static {v0, v1, v2}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    .line 198
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    .line 202
    iget-object v1, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->badBus:Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;

    invoke-virtual {v1}, Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;->successfulSwipes()Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {v0}, Lcom/squareup/util/rx2/Rx2TransformersKt;->gateBy(Lio/reactivex/Observable;)Lio/reactivex/ObservableTransformer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->swipeHandler:Lcom/squareup/ui/payment/SwipeHandler;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v3, Lcom/squareup/ui/seller/-$$Lambda$GfeD-xZnkveMJKA9-eyJ5Jwczv0;

    invoke-direct {v3, v2}, Lcom/squareup/ui/seller/-$$Lambda$GfeD-xZnkveMJKA9-eyJ5Jwczv0;-><init>(Lcom/squareup/ui/payment/SwipeHandler;)V

    .line 203
    invoke-virtual {v1, v3}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 202
    invoke-static {p1, v1}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 204
    iget-object v1, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->badBus:Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;

    invoke-virtual {v1}, Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;->failedSwipes()Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {v0}, Lcom/squareup/util/rx2/Rx2TransformersKt;->gateBy(Lio/reactivex/Observable;)Lio/reactivex/ObservableTransformer;

    move-result-object v0

    invoke-virtual {v1, v0}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->swipeHandler:Lcom/squareup/ui/payment/SwipeHandler;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/seller/-$$Lambda$ZUCDrb8p48WT7DU7uI1yP0Z7FYg;

    invoke-direct {v2, v1}, Lcom/squareup/ui/seller/-$$Lambda$ZUCDrb8p48WT7DU7uI1yP0Z7FYg;-><init>(Lcom/squareup/ui/payment/SwipeHandler;)V

    .line 205
    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 204
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 213
    iget-object v0, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->favoritesTileItemSelectionEvents:Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;

    .line 214
    invoke-virtual {v0}, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;->onItemSelectionStarted()Lrx/Observable;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/seller/-$$Lambda$SellerScopeRunner$l2AMdK_-ixUEg6jGbYz64XFSjf8;

    invoke-direct {v1, p0}, Lcom/squareup/ui/seller/-$$Lambda$SellerScopeRunner$l2AMdK_-ixUEg6jGbYz64XFSjf8;-><init>(Lcom/squareup/ui/seller/SellerScopeRunner;)V

    .line 215
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/seller/-$$Lambda$SellerScopeRunner$mayp8_UdVQB24cEutj7yrKmjglM;

    invoke-direct {v1, p0}, Lcom/squareup/ui/seller/-$$Lambda$SellerScopeRunner$mayp8_UdVQB24cEutj7yrKmjglM;-><init>(Lcom/squareup/ui/seller/SellerScopeRunner;)V

    .line 220
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 213
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 245
    iget-object v0, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->workflowRunner:Lcom/squareup/ui/seller/SellerScopeWorkflowRunner;

    .line 246
    invoke-virtual {v0}, Lcom/squareup/ui/seller/SellerScopeWorkflowRunner;->onResult()Lio/reactivex/Observable;

    move-result-object v0

    const-class v1, Lcom/squareup/ui/seller/SellerScopeWorkflowOutput$SeparatedPrintoutsFinished;

    .line 247
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->mainScheduler:Lio/reactivex/Scheduler;

    .line 248
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/seller/-$$Lambda$SellerScopeRunner$Dc9agwws8iStQzRdE2laDah8lwU;

    invoke-direct {v1, p0}, Lcom/squareup/ui/seller/-$$Lambda$SellerScopeRunner$Dc9agwws8iStQzRdE2laDah8lwU;-><init>(Lcom/squareup/ui/seller/SellerScopeRunner;)V

    .line 249
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 245
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 256
    iget-object v0, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->chooseCustomerFlow:Lcom/squareup/ui/crm/ChooseCustomerFlow;

    .line 257
    invoke-virtual {v0}, Lcom/squareup/ui/crm/ChooseCustomerFlow;->getResults()Lrx/Observable;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/seller/-$$Lambda$SellerScopeRunner$aTBR5Ixrx9CIF_upbPxwFrWjBG8;->INSTANCE:Lcom/squareup/ui/seller/-$$Lambda$SellerScopeRunner$aTBR5Ixrx9CIF_upbPxwFrWjBG8;

    .line 258
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/seller/-$$Lambda$SellerScopeRunner$c9dUfNzFUqpVU96UkdbOjo0R2l0;

    invoke-direct {v1, p0}, Lcom/squareup/ui/seller/-$$Lambda$SellerScopeRunner$c9dUfNzFUqpVU96UkdbOjo0R2l0;-><init>(Lcom/squareup/ui/seller/SellerScopeRunner;)V

    .line 259
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 256
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 293
    iget-object v0, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->addEligibleItemForCouponLegacyOutputs:Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponLegacyOutputs;

    .line 294
    invoke-virtual {v0}, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponLegacyOutputs;->getOutput()Lio/reactivex/Observable;

    move-result-object v0

    const-class v1, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponOutput$MatchingItemAdded;

    .line 295
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/seller/-$$Lambda$SellerScopeRunner$v4ZvQSK_7z-EzZZylHD9BdMDUfY;

    invoke-direct {v1, p0}, Lcom/squareup/ui/seller/-$$Lambda$SellerScopeRunner$v4ZvQSK_7z-EzZZylHD9BdMDUfY;-><init>(Lcom/squareup/ui/seller/SellerScopeRunner;)V

    .line 296
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 293
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 300
    iget-object v0, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->variationSelectionDisposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    return-void
.end method

.method public onResultOfVariationsMatchingBarcode(Ljava/util/List;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 377
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 378
    iget-object p1, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->entryHandler:Lcom/squareup/ui/main/CheckoutEntryHandler;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/main/CheckoutEntryHandler;->itemNotFound(Ljava/lang/String;)V

    return-void

    .line 381
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p2

    const/4 v0, 0x1

    if-le p2, v0, :cond_1

    .line 382
    invoke-direct {p0, p1}, Lcom/squareup/ui/seller/SellerScopeRunner;->onMultipleVariationsMatchingBarcode(Ljava/util/List;)V

    goto :goto_0

    :cond_1
    const/4 p2, 0x0

    .line 384
    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;

    .line 385
    iget-object p2, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->entryHandler:Lcom/squareup/ui/main/CheckoutEntryHandler;

    iget-object v0, p1, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;->itemId:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;->variationId:Ljava/lang/String;

    invoke-virtual {p2, v0, p1}, Lcom/squareup/ui/main/CheckoutEntryHandler;->itemScanned(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public startSeparatedPrintouts(Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;)V
    .locals 2

    .line 304
    iget-object v0, p0, Lcom/squareup/ui/seller/SellerScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/seller/SeparatedPrintoutsOrderTicketBootstrapScreen;

    invoke-direct {v1, p1}, Lcom/squareup/ui/seller/SeparatedPrintoutsOrderTicketBootstrapScreen;-><init>(Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method
