.class public final Lcom/squareup/ui/seller/SellerScopeRunner_Factory;
.super Ljava/lang/Object;
.source "SellerScopeRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/seller/SellerScopeRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final addEligibleItemForCouponLegacyOutputsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponLegacyOutputs;",
            ">;"
        }
    .end annotation
.end field

.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final appletsDrawerPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletsDrawerPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final badBusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;",
            ">;"
        }
    .end annotation
.end field

.field private final buyerFlowStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerFlowStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final changeHudToasterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/ChangeHudToaster;",
            ">;"
        }
    .end annotation
.end field

.field private final chooseCustomerFlowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/ChooseCustomerFlow;",
            ">;"
        }
    .end annotation
.end field

.field private final cogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field private final duplicateSkuResultControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/sku/DuplicateSkuResultController;",
            ">;"
        }
    .end annotation
.end field

.field private final entryHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/CheckoutEntryHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final favoritePageTileCreatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/FavoritePageTileCreator;",
            ">;"
        }
    .end annotation
.end field

.field private final favoritesTileItemSelectionEventsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final hudToasterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderHudManager;",
            ">;"
        }
    .end annotation
.end field

.field private final mainContainerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final maybeX2SellerScreenRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final openTicketsSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final posAppletProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final swipeHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/payment/SwipeHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderCompleterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderCompleter;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketAutoIdentifiersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/TicketAutoIdentifiers;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionDiscountAdapterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TransactionDiscountAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionInteractionsLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/cart/TransactionInteractionsLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field

.field private final workflowRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/seller/SellerScopeWorkflowRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TransactionDiscountAdapter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderHudManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/cart/TransactionInteractionsLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/TicketAutoIdentifiers;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/payment/SwipeHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/ChangeHudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderCompleter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerFlowStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/sku/DuplicateSkuResultController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/CheckoutEntryHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/FavoritePageTileCreator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/ChooseCustomerFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletsDrawerPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/seller/SellerScopeWorkflowRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponLegacyOutputs;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 127
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->transactionProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 128
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->transactionDiscountAdapterProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 129
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->hudToasterProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 130
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->settingsProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 131
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->transactionInteractionsLoggerProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 132
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->posAppletProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 133
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->mainContainerProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 134
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->ticketAutoIdentifiersProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 135
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->swipeHandlerProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 136
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->badBusProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 137
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 138
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->changeHudToasterProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 139
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->resProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 140
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->tenderCompleterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 141
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->buyerFlowStarterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 142
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->duplicateSkuResultControllerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 143
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->entryHandlerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p18

    .line 144
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->favoritesTileItemSelectionEventsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p19

    .line 145
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->favoritePageTileCreatorProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p20

    .line 146
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p21

    .line 147
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->chooseCustomerFlowProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p22

    .line 148
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->maybeX2SellerScreenRunnerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p23

    .line 149
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->analyticsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p24

    .line 150
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->cogsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p25

    .line 151
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->featuresProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p26

    .line 152
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->appletsDrawerPresenterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p27

    .line 153
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p28

    .line 154
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->workflowRunnerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p29

    .line 155
    iput-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->addEligibleItemForCouponLegacyOutputsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/seller/SellerScopeRunner_Factory;
    .locals 31
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TransactionDiscountAdapter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderHudManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/cart/TransactionInteractionsLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/TicketAutoIdentifiers;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/payment/SwipeHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/ChangeHudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderCompleter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerFlowStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/sku/DuplicateSkuResultController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/CheckoutEntryHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/FavoritePageTileCreator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/ChooseCustomerFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletsDrawerPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/seller/SellerScopeWorkflowRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponLegacyOutputs;",
            ">;)",
            "Lcom/squareup/ui/seller/SellerScopeRunner_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    move-object/from16 v25, p24

    move-object/from16 v26, p25

    move-object/from16 v27, p26

    move-object/from16 v28, p27

    move-object/from16 v29, p28

    .line 188
    new-instance v30, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;

    move-object/from16 v0, v30

    invoke-direct/range {v0 .. v29}, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v30
.end method

.method public static newInstance(Lcom/squareup/payment/Transaction;Lcom/squareup/payment/TransactionDiscountAdapter;Lcom/squareup/cardreader/dipper/ReaderHudManager;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/log/cart/TransactionInteractionsLogger;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/print/TicketAutoIdentifiers;Lcom/squareup/ui/payment/SwipeHandler;Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;Lflow/Flow;Lcom/squareup/tenderpayment/ChangeHudToaster;Lcom/squareup/util/Res;Lcom/squareup/tenderpayment/TenderCompleter;Lcom/squareup/ui/buyer/BuyerFlowStarter;Lcom/squareup/sku/DuplicateSkuResultController;Lcom/squareup/ui/main/CheckoutEntryHandler;Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;Lcom/squareup/orderentry/FavoritePageTileCreator;Lio/reactivex/Scheduler;Lcom/squareup/ui/crm/ChooseCustomerFlow;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/analytics/Analytics;Lcom/squareup/cogs/Cogs;Lcom/squareup/settings/server/Features;Lcom/squareup/applet/AppletsDrawerPresenter;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/ui/seller/SellerScopeWorkflowRunner;Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponLegacyOutputs;)Lcom/squareup/ui/seller/SellerScopeRunner;
    .locals 31

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    move-object/from16 v25, p24

    move-object/from16 v26, p25

    move-object/from16 v27, p26

    move-object/from16 v28, p27

    move-object/from16 v29, p28

    .line 206
    new-instance v30, Lcom/squareup/ui/seller/SellerScopeRunner;

    move-object/from16 v0, v30

    invoke-direct/range {v0 .. v29}, Lcom/squareup/ui/seller/SellerScopeRunner;-><init>(Lcom/squareup/payment/Transaction;Lcom/squareup/payment/TransactionDiscountAdapter;Lcom/squareup/cardreader/dipper/ReaderHudManager;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/log/cart/TransactionInteractionsLogger;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/print/TicketAutoIdentifiers;Lcom/squareup/ui/payment/SwipeHandler;Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;Lflow/Flow;Lcom/squareup/tenderpayment/ChangeHudToaster;Lcom/squareup/util/Res;Lcom/squareup/tenderpayment/TenderCompleter;Lcom/squareup/ui/buyer/BuyerFlowStarter;Lcom/squareup/sku/DuplicateSkuResultController;Lcom/squareup/ui/main/CheckoutEntryHandler;Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;Lcom/squareup/orderentry/FavoritePageTileCreator;Lio/reactivex/Scheduler;Lcom/squareup/ui/crm/ChooseCustomerFlow;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/analytics/Analytics;Lcom/squareup/cogs/Cogs;Lcom/squareup/settings/server/Features;Lcom/squareup/applet/AppletsDrawerPresenter;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/ui/seller/SellerScopeWorkflowRunner;Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponLegacyOutputs;)V

    return-object v30
.end method


# virtual methods
.method public get()Lcom/squareup/ui/seller/SellerScopeRunner;
    .locals 31

    move-object/from16 v0, p0

    .line 160
    iget-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/payment/Transaction;

    iget-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->transactionDiscountAdapterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/payment/TransactionDiscountAdapter;

    iget-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->hudToasterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/cardreader/dipper/ReaderHudManager;

    iget-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->transactionInteractionsLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/log/cart/TransactionInteractionsLogger;

    iget-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->posAppletProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/orderentry/OrderEntryAppletGateway;

    iget-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->mainContainerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/ui/main/PosContainer;

    iget-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->ticketAutoIdentifiersProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/print/TicketAutoIdentifiers;

    iget-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->swipeHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/ui/payment/SwipeHandler;

    iget-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->badBusProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;

    iget-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lflow/Flow;

    iget-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->changeHudToasterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/tenderpayment/ChangeHudToaster;

    iget-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/util/Res;

    iget-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->tenderCompleterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/tenderpayment/TenderCompleter;

    iget-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->buyerFlowStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/ui/buyer/BuyerFlowStarter;

    iget-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->duplicateSkuResultControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/sku/DuplicateSkuResultController;

    iget-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->entryHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lcom/squareup/ui/main/CheckoutEntryHandler;

    iget-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->favoritesTileItemSelectionEventsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v19, v1

    check-cast v19, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;

    iget-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->favoritePageTileCreatorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v20, v1

    check-cast v20, Lcom/squareup/orderentry/FavoritePageTileCreator;

    iget-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v21, v1

    check-cast v21, Lio/reactivex/Scheduler;

    iget-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->chooseCustomerFlowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v22, v1

    check-cast v22, Lcom/squareup/ui/crm/ChooseCustomerFlow;

    iget-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->maybeX2SellerScreenRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v23, v1

    check-cast v23, Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    iget-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v24, v1

    check-cast v24, Lcom/squareup/analytics/Analytics;

    iget-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->cogsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v25, v1

    check-cast v25, Lcom/squareup/cogs/Cogs;

    iget-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v26, v1

    check-cast v26, Lcom/squareup/settings/server/Features;

    iget-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->appletsDrawerPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v27, v1

    check-cast v27, Lcom/squareup/applet/AppletsDrawerPresenter;

    iget-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v28, v1

    check-cast v28, Lcom/squareup/tickets/OpenTicketsSettings;

    iget-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->workflowRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v29, v1

    check-cast v29, Lcom/squareup/ui/seller/SellerScopeWorkflowRunner;

    iget-object v1, v0, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->addEligibleItemForCouponLegacyOutputsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v30, v1

    check-cast v30, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponLegacyOutputs;

    invoke-static/range {v2 .. v30}, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->newInstance(Lcom/squareup/payment/Transaction;Lcom/squareup/payment/TransactionDiscountAdapter;Lcom/squareup/cardreader/dipper/ReaderHudManager;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/log/cart/TransactionInteractionsLogger;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/print/TicketAutoIdentifiers;Lcom/squareup/ui/payment/SwipeHandler;Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;Lflow/Flow;Lcom/squareup/tenderpayment/ChangeHudToaster;Lcom/squareup/util/Res;Lcom/squareup/tenderpayment/TenderCompleter;Lcom/squareup/ui/buyer/BuyerFlowStarter;Lcom/squareup/sku/DuplicateSkuResultController;Lcom/squareup/ui/main/CheckoutEntryHandler;Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;Lcom/squareup/orderentry/FavoritePageTileCreator;Lio/reactivex/Scheduler;Lcom/squareup/ui/crm/ChooseCustomerFlow;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/analytics/Analytics;Lcom/squareup/cogs/Cogs;Lcom/squareup/settings/server/Features;Lcom/squareup/applet/AppletsDrawerPresenter;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/ui/seller/SellerScopeWorkflowRunner;Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponLegacyOutputs;)Lcom/squareup/ui/seller/SellerScopeRunner;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 35
    invoke-virtual {p0}, Lcom/squareup/ui/seller/SellerScopeRunner_Factory;->get()Lcom/squareup/ui/seller/SellerScopeRunner;

    move-result-object v0

    return-object v0
.end method
