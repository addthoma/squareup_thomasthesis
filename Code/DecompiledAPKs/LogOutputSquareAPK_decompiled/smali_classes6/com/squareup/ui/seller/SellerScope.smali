.class public final Lcom/squareup/ui/seller/SellerScope;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "SellerScope.java"

# interfaces
.implements Lcom/squareup/container/RegistersInScope;
.implements Lcom/squareup/register/tutorial/R6VideoLauncherAllowed;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent$Responsive;
    phone = Lcom/squareup/ui/seller/SellerScope$PhoneComponent;
    tablet = Lcom/squareup/ui/seller/SellerScope$TabletComponent;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/seller/SellerScope$TabletComponent;,
        Lcom/squareup/ui/seller/SellerScope$PhoneComponent;,
        Lcom/squareup/ui/seller/SellerScope$BaseComponent;,
        Lcom/squareup/ui/seller/SellerScope$ParentComponent;,
        Lcom/squareup/ui/seller/SellerScope$Module;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/seller/SellerScope;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/seller/SellerScope;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 52
    new-instance v0, Lcom/squareup/ui/seller/SellerScope;

    invoke-direct {v0}, Lcom/squareup/ui/seller/SellerScope;-><init>()V

    sput-object v0, Lcom/squareup/ui/seller/SellerScope;->INSTANCE:Lcom/squareup/ui/seller/SellerScope;

    .line 145
    sget-object v0, Lcom/squareup/ui/seller/SellerScope;->INSTANCE:Lcom/squareup/ui/seller/SellerScope;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/seller/SellerScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 54
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    return-void
.end method


# virtual methods
.method public buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;
    .locals 2

    .line 58
    invoke-super {p0, p1}, Lcom/squareup/ui/main/InMainActivityScope;->buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;

    move-result-object v0

    .line 60
    const-class v1, Lcom/squareup/ui/seller/SellerScope$ParentComponent;

    invoke-static {p1, v1}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/seller/SellerScope$ParentComponent;

    .line 61
    invoke-interface {p1}, Lcom/squareup/ui/seller/SellerScope$ParentComponent;->sellerWorkflowRunner()Lcom/squareup/ui/seller/SellerScopeWorkflowRunner;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/squareup/ui/seller/SellerScopeWorkflowRunner;->registerServices(Lmortar/MortarScope$Builder;)V

    return-object v0
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 148
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public register(Lmortar/MortarScope;)V
    .locals 1

    .line 67
    const-class v0, Lcom/squareup/ui/seller/SellerScope$BaseComponent;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/seller/SellerScope$BaseComponent;

    .line 68
    invoke-interface {v0}, Lcom/squareup/ui/seller/SellerScope$BaseComponent;->scopeRunner()Lcom/squareup/ui/seller/SellerScopeRunner;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method
