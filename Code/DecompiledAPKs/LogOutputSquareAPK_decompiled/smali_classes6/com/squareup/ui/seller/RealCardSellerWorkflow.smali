.class public Lcom/squareup/ui/seller/RealCardSellerWorkflow;
.super Lcom/squareup/ui/seller/SellerWorkflow;
.source "RealCardSellerWorkflow.java"

# interfaces
.implements Lcom/squareup/tenderpayment/CardSellerWorkflow;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/seller/SellerWorkflow<",
        "Lcom/squareup/Card;",
        ">;",
        "Lcom/squareup/tenderpayment/CardSellerWorkflow;"
    }
.end annotation


# instance fields
.field private final apiTransactionState:Lcom/squareup/api/ApiTransactionState;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final giftCards:Lcom/squareup/giftcard/GiftCards;

.field private final offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

.field private final onboardingDiverter:Lcom/squareup/onboarding/OnboardingDiverter;

.field private final swipeInputTypeTracker:Lcom/squareup/ui/reader_deprecation/SwipeInputTypeTracker;


# direct methods
.method constructor <init>(Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/payment/Transaction;Lcom/squareup/payment/PaymentHudToaster;Lcom/squareup/analytics/StoreAndForwardAnalytics;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/giftcard/GiftCards;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/ui/reader_deprecation/SwipeInputTypeTracker;Lcom/squareup/payment/TipDeterminerFactory;Lcom/squareup/onboarding/OnboardingDiverter;Lcom/squareup/api/ApiTransactionState;Lcom/squareup/settings/server/Features;)V
    .locals 10
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v9, p0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    move-object/from16 v8, p10

    .line 49
    invoke-direct/range {v0 .. v8}, Lcom/squareup/ui/seller/SellerWorkflow;-><init>(Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/payment/Transaction;Lcom/squareup/payment/PaymentHudToaster;Lcom/squareup/analytics/StoreAndForwardAnalytics;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/payment/TipDeterminerFactory;)V

    move-object/from16 v0, p9

    .line 51
    iput-object v0, v9, Lcom/squareup/ui/seller/RealCardSellerWorkflow;->swipeInputTypeTracker:Lcom/squareup/ui/reader_deprecation/SwipeInputTypeTracker;

    move-object/from16 v0, p6

    .line 52
    iput-object v0, v9, Lcom/squareup/ui/seller/RealCardSellerWorkflow;->giftCards:Lcom/squareup/giftcard/GiftCards;

    move-object/from16 v0, p11

    .line 53
    iput-object v0, v9, Lcom/squareup/ui/seller/RealCardSellerWorkflow;->onboardingDiverter:Lcom/squareup/onboarding/OnboardingDiverter;

    move-object/from16 v0, p12

    .line 54
    iput-object v0, v9, Lcom/squareup/ui/seller/RealCardSellerWorkflow;->apiTransactionState:Lcom/squareup/api/ApiTransactionState;

    move-object/from16 v0, p7

    .line 55
    iput-object v0, v9, Lcom/squareup/ui/seller/RealCardSellerWorkflow;->offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

    move-object/from16 v0, p13

    .line 56
    iput-object v0, v9, Lcom/squareup/ui/seller/RealCardSellerWorkflow;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method


# virtual methods
.method public bridge synthetic authorize(Lcom/squareup/Card;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;
    .locals 0

    .line 33
    invoke-super {p0, p1}, Lcom/squareup/ui/seller/SellerWorkflow;->authorize(Ljava/lang/Object;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    move-result-object p1

    return-object p1
.end method

.method protected canProcessInstrumentOffline(Lcom/squareup/Card;)Z
    .locals 0

    .line 78
    invoke-virtual {p1}, Lcom/squareup/Card;->isManual()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    return p1
.end method

.method protected bridge synthetic canProcessInstrumentOffline(Ljava/lang/Object;)Z
    .locals 0

    .line 33
    check-cast p1, Lcom/squareup/Card;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/seller/RealCardSellerWorkflow;->canProcessInstrumentOffline(Lcom/squareup/Card;)Z

    move-result p1

    return p1
.end method

.method protected createTender(Lcom/squareup/Card;)Lcom/squareup/payment/tender/BaseTender$Builder;
    .locals 2

    .line 114
    instance-of v0, p1, Lcom/squareup/ui/library/giftcard/GiftCardWithClientId;

    if-eqz v0, :cond_0

    .line 115
    move-object v0, p1

    check-cast v0, Lcom/squareup/ui/library/giftcard/GiftCardWithClientId;

    invoke-virtual {v0}, Lcom/squareup/ui/library/giftcard/GiftCardWithClientId;->getClientId()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 117
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/seller/RealCardSellerWorkflow;->giftCards:Lcom/squareup/giftcard/GiftCards;

    invoke-virtual {v1, p1}, Lcom/squareup/giftcard/GiftCards;->isPossiblyGiftCard(Lcom/squareup/Card;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/squareup/ui/seller/RealCardSellerWorkflow;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    .line 118
    invoke-virtual {v1, v0}, Lcom/squareup/payment/tender/TenderFactory;->createGiftCard(Ljava/lang/String;)Lcom/squareup/payment/tender/MagStripeTenderBuilder$GiftCardTenderBuilder;

    move-result-object v0

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/seller/RealCardSellerWorkflow;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    .line 119
    invoke-virtual {v0}, Lcom/squareup/payment/tender/TenderFactory;->createCreditCard()Lcom/squareup/payment/tender/MagStripeTenderBuilder$CreditCardTenderBuilder;

    move-result-object v0

    .line 121
    :goto_1
    invoke-virtual {v0, p1}, Lcom/squareup/payment/tender/MagStripeTenderBuilder;->setCard(Lcom/squareup/Card;)Lcom/squareup/payment/tender/MagStripeTenderBuilder;

    return-object v0
.end method

.method protected bridge synthetic createTender(Ljava/lang/Object;)Lcom/squareup/payment/tender/BaseTender$Builder;
    .locals 0

    .line 33
    check-cast p1, Lcom/squareup/Card;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/seller/RealCardSellerWorkflow;->createTender(Lcom/squareup/Card;)Lcom/squareup/payment/tender/BaseTender$Builder;

    move-result-object p1

    return-object p1
.end method

.method protected isInAuthRange(Lcom/squareup/payment/Transaction;Lcom/squareup/Card;)Z
    .locals 3

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/seller/RealCardSellerWorkflow;->giftCards:Lcom/squareup/giftcard/GiftCards;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->getCard()Lcom/squareup/Card;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/giftcard/GiftCards;->isPossiblyGiftCard(Lcom/squareup/Card;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 64
    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide p1

    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-lez v2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1

    .line 66
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/seller/SellerWorkflow;->isInAuthRange(Lcom/squareup/payment/Transaction;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method protected bridge synthetic isInAuthRange(Lcom/squareup/payment/Transaction;Ljava/lang/Object;)Z
    .locals 0

    .line 33
    check-cast p2, Lcom/squareup/Card;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/seller/RealCardSellerWorkflow;->isInAuthRange(Lcom/squareup/payment/Transaction;Lcom/squareup/Card;)Z

    move-result p1

    return p1
.end method

.method protected isInstrumentReadyToAuthorize(Lcom/squareup/Card;)Z
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/seller/RealCardSellerWorkflow;->giftCards:Lcom/squareup/giftcard/GiftCards;

    invoke-virtual {v0, p1}, Lcom/squareup/giftcard/GiftCards;->isPossiblyGiftCard(Lcom/squareup/Card;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    return p1

    .line 74
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/seller/RealCardSellerWorkflow;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {p1}, Lcom/squareup/settings/server/AccountStatusSettings;->getOnboardingSettings()Lcom/squareup/settings/server/OnboardingSettings;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/settings/server/OnboardingSettings;->acceptsCards()Z

    move-result p1

    return p1
.end method

.method protected bridge synthetic isInstrumentReadyToAuthorize(Ljava/lang/Object;)Z
    .locals 0

    .line 33
    check-cast p1, Lcom/squareup/Card;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/seller/RealCardSellerWorkflow;->isInstrumentReadyToAuthorize(Lcom/squareup/Card;)Z

    move-result p1

    return p1
.end method

.method protected onOfflineLimitExceeded()V
    .locals 2

    .line 99
    iget-object v0, p0, Lcom/squareup/ui/seller/RealCardSellerWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Transaction;->setCard(Lcom/squareup/Card;)V

    return-void
.end method

.method protected onWillTryToAuth(Lcom/squareup/Card;)V
    .locals 1

    .line 126
    iget-object v0, p0, Lcom/squareup/ui/seller/RealCardSellerWorkflow;->swipeInputTypeTracker:Lcom/squareup/ui/reader_deprecation/SwipeInputTypeTracker;

    invoke-virtual {p1}, Lcom/squareup/Card;->getInputType()Lcom/squareup/Card$InputType;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/reader_deprecation/SwipeInputTypeTracker;->noteInputType(Lcom/squareup/Card$InputType;)V

    return-void
.end method

.method protected bridge synthetic onWillTryToAuth(Ljava/lang/Object;)V
    .locals 0

    .line 33
    check-cast p1, Lcom/squareup/Card;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/seller/RealCardSellerWorkflow;->onWillTryToAuth(Lcom/squareup/Card;)V

    return-void
.end method

.method public bridge synthetic readyToAuthorize(Lcom/squareup/Card;)Z
    .locals 0

    .line 33
    invoke-super {p0, p1}, Lcom/squareup/ui/seller/SellerWorkflow;->readyToAuthorize(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public startCardNotPresent()Lcom/squareup/tenderpayment/CardSellerWorkflow$Result;
    .locals 2

    .line 130
    iget-object v0, p0, Lcom/squareup/ui/seller/RealCardSellerWorkflow;->onboardingDiverter:Lcom/squareup/onboarding/OnboardingDiverter;

    invoke-interface {v0}, Lcom/squareup/onboarding/OnboardingDiverter;->shouldDivertToOnboarding()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/squareup/ui/seller/RealCardSellerWorkflow;->onboardingDiverter:Lcom/squareup/onboarding/OnboardingDiverter;

    sget-object v1, Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;->PROMPT:Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;

    invoke-interface {v0, v1}, Lcom/squareup/onboarding/OnboardingDiverter;->divertToOnboarding(Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;)V

    .line 132
    sget-object v0, Lcom/squareup/tenderpayment/CardSellerWorkflow$Result;->DivertedToOnboarding:Lcom/squareup/tenderpayment/CardSellerWorkflow$Result;

    return-object v0

    .line 135
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/seller/RealCardSellerWorkflow;->offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

    invoke-interface {v0}, Lcom/squareup/payment/OfflineModeMonitor;->isInOfflineMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 136
    sget-object v0, Lcom/squareup/tenderpayment/CardSellerWorkflow$Result;->PayCardSwipeOnly:Lcom/squareup/tenderpayment/CardSellerWorkflow$Result;

    return-object v0

    .line 137
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/seller/RealCardSellerWorkflow;->apiTransactionState:Lcom/squareup/api/ApiTransactionState;

    invoke-virtual {v0}, Lcom/squareup/api/ApiTransactionState;->cardNotPresentSupported()Z

    move-result v0

    if-nez v0, :cond_2

    .line 138
    sget-object v0, Lcom/squareup/tenderpayment/CardSellerWorkflow$Result;->PayCardCnpDisabled:Lcom/squareup/tenderpayment/CardSellerWorkflow$Result;

    return-object v0

    .line 140
    :cond_2
    sget-object v0, Lcom/squareup/tenderpayment/CardSellerWorkflow$Result;->PayCreditCard:Lcom/squareup/tenderpayment/CardSellerWorkflow$Result;

    return-object v0
.end method

.method public startGiftCard()Lcom/squareup/tenderpayment/CardSellerWorkflow$Result;
    .locals 2

    .line 145
    iget-object v0, p0, Lcom/squareup/ui/seller/RealCardSellerWorkflow;->onboardingDiverter:Lcom/squareup/onboarding/OnboardingDiverter;

    invoke-interface {v0}, Lcom/squareup/onboarding/OnboardingDiverter;->shouldDivertToOnboarding()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/squareup/ui/seller/RealCardSellerWorkflow;->onboardingDiverter:Lcom/squareup/onboarding/OnboardingDiverter;

    sget-object v1, Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;->PROMPT:Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;

    invoke-interface {v0, v1}, Lcom/squareup/onboarding/OnboardingDiverter;->divertToOnboarding(Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;)V

    .line 147
    sget-object v0, Lcom/squareup/tenderpayment/CardSellerWorkflow$Result;->DivertedToOnboarding:Lcom/squareup/tenderpayment/CardSellerWorkflow$Result;

    return-object v0

    .line 150
    :cond_0
    sget-object v0, Lcom/squareup/tenderpayment/CardSellerWorkflow$Result;->PayGiftCard:Lcom/squareup/tenderpayment/CardSellerWorkflow$Result;

    return-object v0
.end method

.method protected toastOutOfRange(Lcom/squareup/Card;)Z
    .locals 4

    .line 103
    iget-object v0, p0, Lcom/squareup/ui/seller/RealCardSellerWorkflow;->giftCards:Lcom/squareup/giftcard/GiftCards;

    invoke-virtual {v0, p1}, Lcom/squareup/giftcard/GiftCards;->isPossiblyGiftCard(Lcom/squareup/Card;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104
    iget-object p1, p0, Lcom/squareup/ui/seller/RealCardSellerWorkflow;->hudToaster:Lcom/squareup/payment/PaymentHudToaster;

    sget v0, Lcom/squareup/cardreader/ui/R$string;->payment_failed_gift_card_not_charged:I

    sget v1, Lcom/squareup/cardreader/ui/R$string;->payment_failed_below_minimum:I

    iget-object v2, p0, Lcom/squareup/ui/seller/RealCardSellerWorkflow;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 107
    invoke-virtual {v2}, Lcom/squareup/settings/server/AccountStatusSettings;->getGiftCardSettings()Lcom/squareup/settings/server/GiftCardSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/settings/server/GiftCardSettings;->getGiftCardTransactionMinimum()J

    move-result-wide v2

    .line 104
    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/squareup/payment/PaymentHudToaster;->toastOutOfRangeHud(IIJ)Z

    move-result p1

    return p1

    .line 109
    :cond_0
    invoke-super {p0, p1}, Lcom/squareup/ui/seller/SellerWorkflow;->toastOutOfRange(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method protected bridge synthetic toastOutOfRange(Ljava/lang/Object;)Z
    .locals 0

    .line 33
    check-cast p1, Lcom/squareup/Card;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/seller/RealCardSellerWorkflow;->toastOutOfRange(Lcom/squareup/Card;)Z

    move-result p1

    return p1
.end method

.method protected validateInstrument(Lcom/squareup/Card;)V
    .locals 2

    .line 87
    iget-object v0, p0, Lcom/squareup/ui/seller/RealCardSellerWorkflow;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->GIFT_CARDS_THIRD_PARTY_REFACTOR:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/seller/RealCardSellerWorkflow;->giftCards:Lcom/squareup/giftcard/GiftCards;

    .line 88
    invoke-virtual {v0, p1}, Lcom/squareup/giftcard/GiftCards;->isPossiblyGiftCard(Lcom/squareup/Card;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/seller/RealCardSellerWorkflow;->giftCards:Lcom/squareup/giftcard/GiftCards;

    .line 89
    invoke-virtual {p1}, Lcom/squareup/Card;->getPan()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/giftcard/GiftCards;->isValidThirdPartyGiftCardPan(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 93
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/Card;->isValid()Z

    move-result p1

    if-eqz p1, :cond_1

    return-void

    .line 94
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Attempted to authorize invalid card."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method protected bridge synthetic validateInstrument(Ljava/lang/Object;)V
    .locals 0

    .line 33
    check-cast p1, Lcom/squareup/Card;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/seller/RealCardSellerWorkflow;->validateInstrument(Lcom/squareup/Card;)V

    return-void
.end method
