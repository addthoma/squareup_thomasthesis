.class public final Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView_MembersInjector;
.super Ljava/lang/Object;
.source "ConfirmMagstripeAddressView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;",
        ">;"
    }
.end annotation


# instance fields
.field private final phoneNumberScrubberProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/InsertingScrubber;",
            ">;"
        }
    .end annotation
.end field

.field private final postalValidatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/postalvalidation/PostalValidator;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/InsertingScrubber;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/postalvalidation/PostalValidator;",
            ">;)V"
        }
    .end annotation

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p2, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView_MembersInjector;->phoneNumberScrubberProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p3, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView_MembersInjector;->postalValidatorProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/InsertingScrubber;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/postalvalidation/PostalValidator;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;",
            ">;"
        }
    .end annotation

    .line 38
    new-instance v0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView_MembersInjector;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectPhoneNumberScrubber(Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;Lcom/squareup/text/InsertingScrubber;)V
    .locals 0

    .line 56
    iput-object p1, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;->phoneNumberScrubber:Lcom/squareup/text/InsertingScrubber;

    return-void
.end method

.method public static injectPostalValidator(Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;Lcom/squareup/ui/onboarding/postalvalidation/PostalValidator;)V
    .locals 0

    .line 62
    iput-object p1, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;->postalValidator:Lcom/squareup/ui/onboarding/postalvalidation/PostalValidator;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;)V
    .locals 0

    .line 50
    iput-object p1, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;->presenter:Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;)V
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView_MembersInjector;->injectPresenter(Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;)V

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView_MembersInjector;->phoneNumberScrubberProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/text/InsertingScrubber;

    invoke-static {p1, v0}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView_MembersInjector;->injectPhoneNumberScrubber(Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;Lcom/squareup/text/InsertingScrubber;)V

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView_MembersInjector;->postalValidatorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/postalvalidation/PostalValidator;

    invoke-static {p1, v0}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView_MembersInjector;->injectPostalValidator(Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;Lcom/squareup/ui/onboarding/postalvalidation/PostalValidator;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 10
    check-cast p1, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView_MembersInjector;->injectMembers(Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;)V

    return-void
.end method
