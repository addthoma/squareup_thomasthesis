.class public final Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;
.super Ljava/lang/Object;
.source "DepositLinkingResultScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/onboarding/bank/DepositLinkingResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ScreenData"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000e\u0018\u00002\u00020\u0001B?\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0007\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0008\u001a\u00020\u0003\u0012\u0008\u0008\u0003\u0010\t\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\nR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0008\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000cR\u0011\u0010\t\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000cR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0011\u0010\u0007\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u000cR\u0011\u0010\u0006\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u000c\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;",
        "",
        "actionBarTitle",
        "",
        "glyph",
        "Lcom/squareup/glyph/GlyphTypeface$Glyph;",
        "title",
        "message",
        "buttonText",
        "footerText",
        "(ILcom/squareup/glyph/GlyphTypeface$Glyph;IIII)V",
        "getActionBarTitle",
        "()I",
        "getButtonText",
        "getFooterText",
        "getGlyph",
        "()Lcom/squareup/glyph/GlyphTypeface$Glyph;",
        "getMessage",
        "getTitle",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final actionBarTitle:I

.field private final buttonText:I

.field private final footerText:I

.field private final glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field private final message:I

.field private final title:I


# direct methods
.method public constructor <init>(ILcom/squareup/glyph/GlyphTypeface$Glyph;IIII)V
    .locals 1

    const-string v0, "glyph"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;->actionBarTitle:I

    iput-object p2, p0, Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iput p3, p0, Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;->title:I

    iput p4, p0, Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;->message:I

    iput p5, p0, Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;->buttonText:I

    iput p6, p0, Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;->footerText:I

    return-void
.end method

.method public synthetic constructor <init>(ILcom/squareup/glyph/GlyphTypeface$Glyph;IIIIILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 7

    and-int/lit8 p7, p7, 0x20

    if-eqz p7, :cond_0

    const/4 p6, -0x1

    const/4 v6, -0x1

    goto :goto_0

    :cond_0
    move v6, p6

    :goto_0
    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    .line 30
    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;-><init>(ILcom/squareup/glyph/GlyphTypeface$Glyph;IIII)V

    return-void
.end method


# virtual methods
.method public final getActionBarTitle()I
    .locals 1

    .line 25
    iget v0, p0, Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;->actionBarTitle:I

    return v0
.end method

.method public final getButtonText()I
    .locals 1

    .line 29
    iget v0, p0, Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;->buttonText:I

    return v0
.end method

.method public final getFooterText()I
    .locals 1

    .line 30
    iget v0, p0, Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;->footerText:I

    return v0
.end method

.method public final getGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object v0
.end method

.method public final getMessage()I
    .locals 1

    .line 28
    iget v0, p0, Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;->message:I

    return v0
.end method

.method public final getTitle()I
    .locals 1

    .line 27
    iget v0, p0, Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;->title:I

    return v0
.end method
