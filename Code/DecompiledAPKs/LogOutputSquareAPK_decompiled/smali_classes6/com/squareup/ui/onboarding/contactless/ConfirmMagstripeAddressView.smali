.class public Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;
.super Lcom/squareup/widgets/ResponsiveScrollView;
.source "ConfirmMagstripeAddressView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;


# instance fields
.field private addressLayout:Lcom/squareup/address/AddressLayout;

.field private final confirmHaveReaderPopup:Lcom/squareup/flowlegacy/ConfirmationPopup;

.field private final createAddressProgressView:Lcom/squareup/caller/ProgressAndFailurePresenter$View;

.field private phone:Lcom/squareup/widgets/SelectableEditText;

.field phoneNumberScrubber:Lcom/squareup/text/InsertingScrubber;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field postalValidator:Lcom/squareup/ui/onboarding/postalvalidation/PostalValidator;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field presenter:Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private shippingName:Landroid/widget/EditText;

.field private final warningPopup:Lcom/squareup/flowlegacy/WarningPopup;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 48
    invoke-direct {p0, p1, p2}, Lcom/squareup/widgets/ResponsiveScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 50
    const-class p2, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Component;

    invoke-interface {p2, p0}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Component;->inject(Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;)V

    .line 52
    new-instance p2, Lcom/squareup/flowlegacy/WarningPopup;

    invoke-direct {p2, p1}, Lcom/squareup/flowlegacy/WarningPopup;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;->warningPopup:Lcom/squareup/flowlegacy/WarningPopup;

    .line 53
    new-instance p2, Lcom/squareup/flowlegacy/ConfirmationPopup;

    invoke-direct {p2, p1}, Lcom/squareup/flowlegacy/ConfirmationPopup;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;->confirmHaveReaderPopup:Lcom/squareup/flowlegacy/ConfirmationPopup;

    .line 54
    new-instance p2, Lcom/squareup/caller/ProgressAndFailureView;

    invoke-direct {p2, p1}, Lcom/squareup/caller/ProgressAndFailureView;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;->createAddressProgressView:Lcom/squareup/caller/ProgressAndFailurePresenter$View;

    return-void
.end method


# virtual methods
.method focusView(I)V
    .locals 0

    .line 101
    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;->findViewById(I)Landroid/view/View;

    move-result-object p1

    .line 102
    invoke-virtual {p1}, Landroid/view/View;->requestFocus()Z

    return-void
.end method

.method getAddress()Lcom/squareup/address/Address;
    .locals 1

    .line 143
    iget-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;->addressLayout:Lcom/squareup/address/AddressLayout;

    invoke-virtual {v0}, Lcom/squareup/address/AddressLayout;->getAddress()Lcom/squareup/address/Address;

    move-result-object v0

    return-object v0
.end method

.method getPhone()Ljava/lang/String;
    .locals 1

    .line 110
    iget-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;->phone:Lcom/squareup/widgets/SelectableEditText;

    invoke-static {v0}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getShippingName()Ljava/lang/String;
    .locals 1

    .line 106
    iget-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;->shippingName:Landroid/widget/EditText;

    invoke-static {v0}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method hideKeyboard()V
    .locals 0

    .line 122
    invoke-static {p0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    return-void
.end method

.method indicateCorrection()V
    .locals 2

    .line 151
    sget v0, Lcom/squareup/onboarding/flow/R$id;->modified_warning:I

    invoke-virtual {p0, v0}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 152
    sget v0, Lcom/squareup/onboarding/flow/R$id;->uncorrectable_warning:I

    invoke-virtual {p0, v0}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method indicateUncorrectable()V
    .locals 2

    .line 156
    sget v0, Lcom/squareup/onboarding/flow/R$id;->uncorrectable_warning:I

    invoke-virtual {p0, v0}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 157
    sget v0, Lcom/squareup/onboarding/flow/R$id;->modified_warning:I

    invoke-virtual {p0, v0}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public synthetic lambda$onFinishInflate$0$ConfirmMagstripeAddressView(Landroid/view/View;)V
    .locals 0

    .line 76
    iget-object p1, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;->presenter:Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->onMailFreeReader()V

    return-void
.end method

.method public synthetic lambda$onFinishInflate$1$ConfirmMagstripeAddressView(Landroid/view/View;)V
    .locals 0

    .line 78
    iget-object p1, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;->presenter:Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->onHasReader()V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .line 87
    iget-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;->presenter:Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;

    iget-object v0, v0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->createAddressProgressPresenter:Lcom/squareup/caller/ProgressAndFailurePresenter;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;->createAddressProgressView:Lcom/squareup/caller/ProgressAndFailurePresenter$View;

    invoke-virtual {v0, v1}, Lcom/squareup/caller/ProgressAndFailurePresenter;->dropView(Lcom/squareup/caller/ProgressAndFailurePresenter$View;)V

    .line 88
    iget-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;->presenter:Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;

    iget-object v0, v0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->confirmHaveReaderPopupPresenter:Lcom/squareup/mortar/PopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;->confirmHaveReaderPopup:Lcom/squareup/flowlegacy/ConfirmationPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/mortar/PopupPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    .line 89
    iget-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;->presenter:Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;

    iget-object v0, v0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;->warningPopup:Lcom/squareup/flowlegacy/WarningPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    .line 90
    iget-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;->presenter:Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 92
    invoke-super {p0}, Lcom/squareup/widgets/ResponsiveScrollView;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .line 58
    invoke-super {p0}, Lcom/squareup/widgets/ResponsiveScrollView;->onFinishInflate()V

    .line 60
    sget v0, Lcom/squareup/onboarding/flow/R$id;->shipping_address:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/address/AddressLayout;

    iput-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;->addressLayout:Lcom/squareup/address/AddressLayout;

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;->addressLayout:Lcom/squareup/address/AddressLayout;

    invoke-virtual {v0}, Lcom/squareup/address/AddressLayout;->getPostal()Landroid/widget/EditText;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView$1;-><init>(Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 71
    sget v0, Lcom/squareup/onboarding/flow/R$id;->shipping_name:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;->shippingName:Landroid/widget/EditText;

    .line 72
    sget v0, Lcom/squareup/onboarding/flow/R$id;->phone_number:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SelectableEditText;

    iput-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;->phone:Lcom/squareup/widgets/SelectableEditText;

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;->phone:Lcom/squareup/widgets/SelectableEditText;

    new-instance v1, Lcom/squareup/text/ScrubbingTextWatcher;

    iget-object v2, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;->phoneNumberScrubber:Lcom/squareup/text/InsertingScrubber;

    invoke-direct {v1, v2, v0}, Lcom/squareup/text/ScrubbingTextWatcher;-><init>(Lcom/squareup/text/InsertingScrubber;Lcom/squareup/text/HasSelectableText;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 75
    sget v0, Lcom/squareup/onboarding/flow/R$id;->first_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    .line 76
    new-instance v1, Lcom/squareup/ui/onboarding/contactless/-$$Lambda$ConfirmMagstripeAddressView$XZBEUE9t0TZoUsAena-ioX3K3OI;

    invoke-direct {v1, p0}, Lcom/squareup/ui/onboarding/contactless/-$$Lambda$ConfirmMagstripeAddressView$XZBEUE9t0TZoUsAena-ioX3K3OI;-><init>(Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 77
    sget v0, Lcom/squareup/onboarding/flow/R$id;->second_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    .line 78
    new-instance v1, Lcom/squareup/ui/onboarding/contactless/-$$Lambda$ConfirmMagstripeAddressView$KJ9qAWS2IL1rRl3KyNe8KutB11Y;

    invoke-direct {v1, p0}, Lcom/squareup/ui/onboarding/contactless/-$$Lambda$ConfirmMagstripeAddressView$KJ9qAWS2IL1rRl3KyNe8KutB11Y;-><init>(Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;->presenter:Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->takeView(Ljava/lang/Object;)V

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;->presenter:Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;

    iget-object v0, v0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;->warningPopup:Lcom/squareup/flowlegacy/WarningPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->takeView(Ljava/lang/Object;)V

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;->presenter:Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;

    iget-object v0, v0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->confirmHaveReaderPopupPresenter:Lcom/squareup/mortar/PopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;->confirmHaveReaderPopup:Lcom/squareup/flowlegacy/ConfirmationPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/mortar/PopupPresenter;->takeView(Ljava/lang/Object;)V

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;->presenter:Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;

    iget-object v0, v0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->createAddressProgressPresenter:Lcom/squareup/caller/ProgressAndFailurePresenter;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;->createAddressProgressView:Lcom/squareup/caller/ProgressAndFailurePresenter$View;

    invoke-virtual {v0, v1}, Lcom/squareup/caller/ProgressAndFailurePresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method setAddress(Lcom/squareup/address/Address;)V
    .locals 2

    .line 147
    iget-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;->addressLayout:Lcom/squareup/address/AddressLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/squareup/address/AddressLayout;->setAddress(Lcom/squareup/address/Address;Z)V

    return-void
.end method

.method setName(Ljava/lang/String;)V
    .locals 1

    .line 118
    iget-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;->shippingName:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method setPhone(Ljava/lang/String;)V
    .locals 1

    .line 114
    iget-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;->phone:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/SelectableEditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method validateAddress()Lcom/squareup/ui/onboarding/ValidationError;
    .locals 4

    .line 126
    iget-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;->addressLayout:Lcom/squareup/address/AddressLayout;

    invoke-virtual {v0}, Lcom/squareup/address/AddressLayout;->getEmptyField()Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 129
    new-instance v1, Lcom/squareup/ui/onboarding/ValidationError;

    sget v2, Lcom/squareup/banklinking/R$string;->missing_required_field:I

    sget v3, Lcom/squareup/onboarding/flow/R$string;->missing_address_fields:I

    .line 131
    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    invoke-direct {v1, v2, v3, v0}, Lcom/squareup/ui/onboarding/ValidationError;-><init>(III)V

    return-object v1

    .line 134
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;->addressLayout:Lcom/squareup/address/AddressLayout;

    invoke-virtual {v0}, Lcom/squareup/address/AddressLayout;->getPostal()Landroid/widget/EditText;

    move-result-object v0

    .line 135
    iget-object v1, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;->postalValidator:Lcom/squareup/ui/onboarding/postalvalidation/PostalValidator;

    if-eqz v1, :cond_1

    .line 136
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Landroid/widget/TextView;->getId()I

    move-result v0

    invoke-interface {v1, v2, v0}, Lcom/squareup/ui/onboarding/postalvalidation/PostalValidator;->precheck(Ljava/lang/String;I)Lcom/squareup/ui/onboarding/ValidationError;

    move-result-object v0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method
