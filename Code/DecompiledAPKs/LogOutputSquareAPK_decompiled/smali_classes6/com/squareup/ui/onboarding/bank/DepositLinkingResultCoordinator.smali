.class public final Lcom/squareup/ui/onboarding/bank/DepositLinkingResultCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "DepositLinkingResultCoordinator.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0008\n\u0002\u0008\u0005\u0018\u00002\u00020\u0001B#\u0012\u001c\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016H\u0016J\u0010\u0010\u0017\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016H\u0002J \u0010\u0018\u001a\u00020\u00142\u0016\u0010\u0019\u001a\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u0007H\u0002J\u0012\u0010\u001a\u001a\u00020\u00142\u0008\u0008\u0001\u0010\u001b\u001a\u00020\u001cH\u0002J \u0010\u001d\u001a\u00020\u00142\u0016\u0010\u0019\u001a\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u0007H\u0002J\u0012\u0010\u001e\u001a\u00020\u00142\u0008\u0008\u0001\u0010\u001f\u001a\u00020\u001cH\u0002J \u0010 \u001a\u00020\u00142\u0016\u0010\u0019\u001a\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u0007H\u0002R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R$\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/ui/onboarding/bank/DepositLinkingResultCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;",
        "Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ContinueOrSkip;",
        "Lcom/squareup/ui/onboarding/bank/DepositLinkingResultScreen;",
        "(Lio/reactivex/Observable;)V",
        "button",
        "Lcom/squareup/marketfont/MarketButton;",
        "footerText",
        "Lcom/squareup/widgets/MessageView;",
        "glyphMessage",
        "Lcom/squareup/marin/widgets/MarinGlyphMessage;",
        "res",
        "Landroid/content/res/Resources;",
        "subtitle",
        "title",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "onScreen",
        "screen",
        "setUpActionBar",
        "titleText",
        "",
        "setUpButton",
        "setUpFooterText",
        "footerTextId",
        "setUpGlyphMessage",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private button:Lcom/squareup/marketfont/MarketButton;

.field private footerText:Lcom/squareup/widgets/MessageView;

.field private glyphMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

.field private res:Landroid/content/res/Resources;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;",
            "Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ContinueOrSkip;",
            ">;>;"
        }
    .end annotation
.end field

.field private subtitle:Lcom/squareup/widgets/MessageView;

.field private title:Lcom/squareup/widgets/MessageView;


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;",
            "Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ContinueOrSkip;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositLinkingResultCoordinator;->screens:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$onScreen(Lcom/squareup/ui/onboarding/bank/DepositLinkingResultCoordinator;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 0

    .line 20
    invoke-direct {p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositLinkingResultCoordinator;->onScreen(Lcom/squareup/workflow/legacy/Screen;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 79
    sget v0, Lcom/squareup/onboarding/flow/R$id;->title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositLinkingResultCoordinator;->title:Lcom/squareup/widgets/MessageView;

    .line 80
    sget v0, Lcom/squareup/onboarding/flow/R$id;->subtitle:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositLinkingResultCoordinator;->subtitle:Lcom/squareup/widgets/MessageView;

    .line 81
    sget v0, Lcom/squareup/onboarding/flow/R$id;->glyph_message:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinGlyphMessage;

    iput-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositLinkingResultCoordinator;->glyphMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    .line 82
    sget v0, Lcom/squareup/onboarding/flow/R$id;->button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositLinkingResultCoordinator;->button:Lcom/squareup/marketfont/MarketButton;

    .line 83
    sget v0, Lcom/squareup/onboarding/flow/R$id;->footer:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/MessageView;

    iput-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositLinkingResultCoordinator;->footerText:Lcom/squareup/widgets/MessageView;

    return-void
.end method

.method private final onScreen(Lcom/squareup/workflow/legacy/Screen;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;",
            "Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ContinueOrSkip;",
            ">;)V"
        }
    .end annotation

    .line 43
    iget-object v0, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;->getActionBarTitle()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/squareup/ui/onboarding/bank/DepositLinkingResultCoordinator;->setUpActionBar(I)V

    .line 44
    invoke-direct {p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositLinkingResultCoordinator;->setUpGlyphMessage(Lcom/squareup/workflow/legacy/Screen;)V

    .line 45
    invoke-direct {p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositLinkingResultCoordinator;->setUpButton(Lcom/squareup/workflow/legacy/Screen;)V

    .line 46
    iget-object p1, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;->getFooterText()I

    move-result p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositLinkingResultCoordinator;->setUpFooterText(I)V

    return-void
.end method

.method private final setUpActionBar(I)V
    .locals 3

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositLinkingResultCoordinator;->title:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_0

    const-string v1, "title"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/onboarding/bank/DepositLinkingResultCoordinator;->res:Landroid/content/res/Resources;

    if-nez v1, :cond_1

    const-string v2, "res"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 51
    iget-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositLinkingResultCoordinator;->subtitle:Lcom/squareup/widgets/MessageView;

    if-nez p1, :cond_2

    const-string v0, "subtitle"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    return-void
.end method

.method private final setUpButton(Lcom/squareup/workflow/legacy/Screen;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;",
            "Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ContinueOrSkip;",
            ">;)V"
        }
    .end annotation

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositLinkingResultCoordinator;->button:Lcom/squareup/marketfont/MarketButton;

    const-string v1, "button"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object v2, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v2, Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;

    invoke-virtual {v2}, Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;->getButtonText()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/squareup/marketfont/MarketButton;->setText(I)V

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositLinkingResultCoordinator;->button:Lcom/squareup/marketfont/MarketButton;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    new-instance v1, Lcom/squareup/ui/onboarding/bank/DepositLinkingResultCoordinator$setUpButton$1;

    invoke-direct {v1, p1}, Lcom/squareup/ui/onboarding/bank/DepositLinkingResultCoordinator$setUpButton$1;-><init>(Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounceRunnable(Ljava/lang/Runnable;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p1

    check-cast p1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private final setUpFooterText(I)V
    .locals 2

    const-string v0, "footerText"

    const/4 v1, -0x1

    if-eq p1, v1, :cond_2

    .line 71
    iget-object v1, p0, Lcom/squareup/ui/onboarding/bank/DepositLinkingResultCoordinator;->footerText:Lcom/squareup/widgets/MessageView;

    if-nez v1, :cond_0

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v1, p1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 72
    iget-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositLinkingResultCoordinator;->footerText:Lcom/squareup/widgets/MessageView;

    if-nez p1, :cond_1

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    goto :goto_0

    .line 74
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositLinkingResultCoordinator;->footerText:Lcom/squareup/widgets/MessageView;

    if-nez p1, :cond_3

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method private final setUpGlyphMessage(Lcom/squareup/workflow/legacy/Screen;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;",
            "Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ContinueOrSkip;",
            ">;)V"
        }
    .end annotation

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositLinkingResultCoordinator;->glyphMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    const-string v1, "glyphMessage"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object v2, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v2, Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;

    invoke-virtual {v2}, Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;->getGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositLinkingResultCoordinator;->glyphMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    iget-object v2, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v2, Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;

    invoke-virtual {v2}, Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;->getTitle()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setTitle(I)V

    .line 57
    iget-object v0, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;->getMessage()I

    move-result v0

    const/4 v2, -0x1

    if-eq v0, v2, :cond_3

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositLinkingResultCoordinator;->glyphMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    iget-object p1, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;->getMessage()I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setMessage(I)V

    goto :goto_0

    .line 60
    :cond_3
    iget-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositLinkingResultCoordinator;->glyphMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    if-nez p1, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->hideMessage()V

    :goto_0
    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 3

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    sget-object v0, Lcom/squareup/ui/onboarding/bank/DepositLinkingResultCoordinator$attach$1;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositLinkingResultCoordinator$attach$1;

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 33
    invoke-direct {p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositLinkingResultCoordinator;->bindViews(Landroid/view/View;)V

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositLinkingResultCoordinator;->glyphMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    if-nez v0, :cond_0

    const-string v1, "glyphMessage"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->hideButton()V

    .line 37
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "view.resources"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositLinkingResultCoordinator;->res:Landroid/content/res/Resources;

    .line 38
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositLinkingResultCoordinator;->screens:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/ui/onboarding/bank/DepositLinkingResultCoordinator$attach$2;

    move-object v2, p0

    check-cast v2, Lcom/squareup/ui/onboarding/bank/DepositLinkingResultCoordinator;

    invoke-direct {v1, v2}, Lcom/squareup/ui/onboarding/bank/DepositLinkingResultCoordinator$attach$2;-><init>(Lcom/squareup/ui/onboarding/bank/DepositLinkingResultCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    new-instance v2, Lcom/squareup/ui/onboarding/bank/DepositLinkingResultCoordinator$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {v2, v1}, Lcom/squareup/ui/onboarding/bank/DepositLinkingResultCoordinator$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "screens.subscribe(::onScreen)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method
