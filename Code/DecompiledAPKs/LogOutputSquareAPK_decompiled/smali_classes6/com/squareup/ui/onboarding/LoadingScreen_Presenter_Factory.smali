.class public final Lcom/squareup/ui/onboarding/LoadingScreen_Presenter_Factory;
.super Ljava/lang/Object;
.source "LoadingScreen_Presenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/onboarding/LoadingScreen$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final serverCallPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/LoadingStatusPresenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/LoadingStatusPresenter;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/ui/onboarding/LoadingScreen_Presenter_Factory;->serverCallPresenterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/ui/onboarding/LoadingScreen_Presenter_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/LoadingStatusPresenter;",
            ">;)",
            "Lcom/squareup/ui/onboarding/LoadingScreen_Presenter_Factory;"
        }
    .end annotation

    .line 30
    new-instance v0, Lcom/squareup/ui/onboarding/LoadingScreen_Presenter_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/ui/onboarding/LoadingScreen_Presenter_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Ljava/lang/Object;)Lcom/squareup/ui/onboarding/LoadingScreen$Presenter;
    .locals 1

    .line 34
    new-instance v0, Lcom/squareup/ui/onboarding/LoadingScreen$Presenter;

    check-cast p0, Lcom/squareup/ui/onboarding/LoadingStatusPresenter;

    invoke-direct {v0, p0}, Lcom/squareup/ui/onboarding/LoadingScreen$Presenter;-><init>(Lcom/squareup/ui/onboarding/LoadingStatusPresenter;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/onboarding/LoadingScreen$Presenter;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/ui/onboarding/LoadingScreen_Presenter_Factory;->serverCallPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/ui/onboarding/LoadingScreen_Presenter_Factory;->newInstance(Ljava/lang/Object;)Lcom/squareup/ui/onboarding/LoadingScreen$Presenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/LoadingScreen_Presenter_Factory;->get()Lcom/squareup/ui/onboarding/LoadingScreen$Presenter;

    move-result-object v0

    return-object v0
.end method
