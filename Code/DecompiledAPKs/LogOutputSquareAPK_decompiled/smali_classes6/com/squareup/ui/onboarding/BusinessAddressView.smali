.class public Lcom/squareup/ui/onboarding/BusinessAddressView;
.super Landroid/widget/LinearLayout;
.source "BusinessAddressView.java"


# instance fields
.field private addressTypeGroup:Lcom/squareup/widgets/CheckableGroup;

.field private businessAddress:Lcom/squareup/address/AddressLayout;

.field private businessAddressDifferentAsHomeCheckbox:Lcom/squareup/marketfont/MarketCheckedTextView;

.field private businessAddressLabel:Landroid/view/View;

.field private businessAddressMobileCheckbox:Lcom/squareup/marketfont/MarketCheckedTextView;

.field private businessAddressMobileLabel:Landroid/view/View;

.field private businessAddressSameAsHomeCheckbox:Lcom/squareup/marketfont/MarketCheckedTextView;

.field presenter:Lcom/squareup/ui/onboarding/BusinessAddressPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final warningPopup:Lcom/squareup/flowlegacy/WarningPopup;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 42
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    const-class p2, Lcom/squareup/ui/onboarding/BusinessAddressScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/onboarding/BusinessAddressScreen$Component;

    invoke-interface {p2, p0}, Lcom/squareup/ui/onboarding/BusinessAddressScreen$Component;->inject(Lcom/squareup/ui/onboarding/BusinessAddressView;)V

    .line 45
    new-instance p2, Lcom/squareup/flowlegacy/WarningPopup;

    invoke-direct {p2, p1}, Lcom/squareup/flowlegacy/WarningPopup;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/squareup/ui/onboarding/BusinessAddressView;->warningPopup:Lcom/squareup/flowlegacy/WarningPopup;

    return-void
.end method

.method private bindAdvanceOnDone(Landroid/widget/EditText;)V
    .locals 1

    const/4 v0, 0x2

    .line 120
    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 121
    new-instance v0, Lcom/squareup/ui/onboarding/BusinessAddressView$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/onboarding/BusinessAddressView$1;-><init>(Lcom/squareup/ui/onboarding/BusinessAddressView;Landroid/widget/EditText;)V

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    return-void
.end method

.method private clearAddressForm()V
    .locals 3

    .line 148
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/BusinessAddressView;->showAddressForm()V

    .line 150
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessAddressView;->businessAddress:Lcom/squareup/address/AddressLayout;

    invoke-virtual {v0}, Lcom/squareup/address/AddressLayout;->getCountry()Lcom/squareup/CountryCode;

    move-result-object v0

    .line 151
    iget-object v1, p0, Lcom/squareup/ui/onboarding/BusinessAddressView;->businessAddress:Lcom/squareup/address/AddressLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/squareup/address/AddressLayout;->setAddress(Lcom/squareup/address/Address;)V

    .line 152
    sget-object v1, Lcom/squareup/CountryCode;->US:Lcom/squareup/CountryCode;

    if-ne v0, v1, :cond_0

    .line 155
    iget-object v1, p0, Lcom/squareup/ui/onboarding/BusinessAddressView;->businessAddress:Lcom/squareup/address/AddressLayout;

    invoke-virtual {v1}, Lcom/squareup/address/AddressLayout;->hideManualCityEntry()V

    .line 157
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/onboarding/BusinessAddressView;->businessAddress:Lcom/squareup/address/AddressLayout;

    invoke-virtual {v1, v0}, Lcom/squareup/address/AddressLayout;->setCountry(Lcom/squareup/CountryCode;)V

    .line 158
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessAddressView;->businessAddress:Lcom/squareup/address/AddressLayout;

    invoke-virtual {v0}, Lcom/squareup/address/AddressLayout;->enableAddress()V

    .line 159
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessAddressView;->businessAddress:Lcom/squareup/address/AddressLayout;

    invoke-virtual {v0}, Lcom/squareup/address/AddressLayout;->requestFocus()Z

    return-void
.end method

.method private hideAddressForm()V
    .locals 2

    .line 168
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessAddressView;->businessAddressLabel:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 169
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessAddressView;->businessAddress:Lcom/squareup/address/AddressLayout;

    invoke-virtual {v0, v1}, Lcom/squareup/address/AddressLayout;->setVisibility(I)V

    return-void
.end method

.method private prefillAddressForm()V
    .locals 2

    .line 142
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/BusinessAddressView;->hideAddressForm()V

    .line 143
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessAddressView;->businessAddress:Lcom/squareup/address/AddressLayout;

    invoke-virtual {v0}, Lcom/squareup/address/AddressLayout;->disableAddress()V

    .line 144
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessAddressView;->businessAddress:Lcom/squareup/address/AddressLayout;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/BusinessAddressView;->presenter:Lcom/squareup/ui/onboarding/BusinessAddressPresenter;

    invoke-virtual {v1}, Lcom/squareup/ui/onboarding/BusinessAddressPresenter;->getPersonalAddress()Lcom/squareup/address/Address;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/address/AddressLayout;->setAddress(Lcom/squareup/address/Address;)V

    return-void
.end method

.method private showAddressForm()V
    .locals 2

    .line 163
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessAddressView;->businessAddressLabel:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 164
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessAddressView;->businessAddress:Lcom/squareup/address/AddressLayout;

    invoke-virtual {v0, v1}, Lcom/squareup/address/AddressLayout;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method public focusBusinessAddress()V
    .locals 1

    .line 104
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessAddressView;->businessAddress:Lcom/squareup/address/AddressLayout;

    invoke-virtual {v0}, Lcom/squareup/address/AddressLayout;->requestFocus()Z

    return-void
.end method

.method public getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 134
    invoke-static {p0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method public getBusinessAddress()Lcom/squareup/address/Address;
    .locals 1

    .line 138
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessAddressView;->businessAddress:Lcom/squareup/address/AddressLayout;

    invoke-virtual {v0}, Lcom/squareup/address/AddressLayout;->getAddress()Lcom/squareup/address/Address;

    move-result-object v0

    return-object v0
.end method

.method public isBusinessAddressSet()Z
    .locals 1

    .line 116
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessAddressView;->businessAddress:Lcom/squareup/address/AddressLayout;

    invoke-virtual {v0}, Lcom/squareup/address/AddressLayout;->isSet()Z

    move-result v0

    return v0
.end method

.method isMobileBusiness()Z
    .locals 2

    .line 108
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessAddressView;->addressTypeGroup:Lcom/squareup/widgets/CheckableGroup;

    invoke-virtual {v0}, Lcom/squareup/widgets/CheckableGroup;->getCheckedId()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/ui/onboarding/BusinessAddressView;->businessAddressMobileCheckbox:Lcom/squareup/marketfont/MarketCheckedTextView;

    invoke-virtual {v1}, Lcom/squareup/marketfont/MarketCheckedTextView;->getId()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method isSameAsPersonal()Z
    .locals 2

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessAddressView;->addressTypeGroup:Lcom/squareup/widgets/CheckableGroup;

    invoke-virtual {v0}, Lcom/squareup/widgets/CheckableGroup;->getCheckedId()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/ui/onboarding/BusinessAddressView;->businessAddressSameAsHomeCheckbox:Lcom/squareup/marketfont/MarketCheckedTextView;

    invoke-virtual {v1}, Lcom/squareup/marketfont/MarketCheckedTextView;->getId()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public synthetic lambda$onFinishInflate$0$BusinessAddressView(Lcom/squareup/widgets/CheckableGroup;II)V
    .locals 0

    .line 65
    iget-object p1, p0, Lcom/squareup/ui/onboarding/BusinessAddressView;->businessAddressMobileLabel:Landroid/view/View;

    const/16 p3, 0x8

    invoke-virtual {p1, p3}, Landroid/view/View;->setVisibility(I)V

    .line 66
    iget-object p1, p0, Lcom/squareup/ui/onboarding/BusinessAddressView;->businessAddressSameAsHomeCheckbox:Lcom/squareup/marketfont/MarketCheckedTextView;

    invoke-virtual {p1}, Lcom/squareup/marketfont/MarketCheckedTextView;->getId()I

    move-result p1

    if-ne p2, p1, :cond_0

    .line 67
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/BusinessAddressView;->prefillAddressForm()V

    goto :goto_0

    .line 68
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/onboarding/BusinessAddressView;->businessAddressMobileCheckbox:Lcom/squareup/marketfont/MarketCheckedTextView;

    invoke-virtual {p1}, Lcom/squareup/marketfont/MarketCheckedTextView;->getId()I

    move-result p1

    if-ne p2, p1, :cond_1

    .line 69
    iget-object p1, p0, Lcom/squareup/ui/onboarding/BusinessAddressView;->businessAddressMobileLabel:Landroid/view/View;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    .line 70
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/BusinessAddressView;->hideAddressForm()V

    goto :goto_0

    .line 71
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/onboarding/BusinessAddressView;->businessAddressDifferentAsHomeCheckbox:Lcom/squareup/marketfont/MarketCheckedTextView;

    invoke-virtual {p1}, Lcom/squareup/marketfont/MarketCheckedTextView;->getId()I

    move-result p1

    if-ne p2, p1, :cond_2

    .line 72
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/BusinessAddressView;->clearAddressForm()V

    :cond_2
    :goto_0
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .line 83
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessAddressView;->addressTypeGroup:Lcom/squareup/widgets/CheckableGroup;

    invoke-virtual {v0}, Lcom/squareup/widgets/CheckableGroup;->getCheckedId()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/ui/onboarding/BusinessAddressView;->businessAddressDifferentAsHomeCheckbox:Lcom/squareup/marketfont/MarketCheckedTextView;

    invoke-virtual {v1}, Lcom/squareup/marketfont/MarketCheckedTextView;->getId()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 87
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/BusinessAddressView;->showAddressForm()V

    goto :goto_0

    .line 89
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/BusinessAddressView;->hideAddressForm()V

    .line 92
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessAddressView;->presenter:Lcom/squareup/ui/onboarding/BusinessAddressPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/onboarding/BusinessAddressPresenter;->takeView(Ljava/lang/Object;)V

    .line 93
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessAddressView;->presenter:Lcom/squareup/ui/onboarding/BusinessAddressPresenter;

    iget-object v0, v0, Lcom/squareup/ui/onboarding/BusinessAddressPresenter;->warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/BusinessAddressView;->warningPopup:Lcom/squareup/flowlegacy/WarningPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .line 97
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessAddressView;->presenter:Lcom/squareup/ui/onboarding/BusinessAddressPresenter;

    iget-object v0, v0, Lcom/squareup/ui/onboarding/BusinessAddressPresenter;->warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/BusinessAddressView;->warningPopup:Lcom/squareup/flowlegacy/WarningPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    .line 98
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessAddressView;->presenter:Lcom/squareup/ui/onboarding/BusinessAddressPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/onboarding/BusinessAddressPresenter;->dropView(Lcom/squareup/ui/onboarding/BusinessAddressView;)V

    .line 100
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 49
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 51
    sget v0, Lcom/squareup/onboarding/flow/R$id;->title:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    .line 52
    sget v1, Lcom/squareup/onboarding/flow/R$string;->business_address_heading:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 53
    sget v0, Lcom/squareup/onboarding/flow/R$id;->subtitle:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    .line 54
    sget v1, Lcom/squareup/onboarding/flow/R$string;->business_address_subheading:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 56
    sget v0, Lcom/squareup/onboarding/flow/R$id;->business_address_same_as_home_group:I

    .line 57
    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/CheckableGroup;

    iput-object v0, p0, Lcom/squareup/ui/onboarding/BusinessAddressView;->addressTypeGroup:Lcom/squareup/widgets/CheckableGroup;

    .line 58
    sget v0, Lcom/squareup/onboarding/flow/R$id;->business_address_same_as_home:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketCheckedTextView;

    iput-object v0, p0, Lcom/squareup/ui/onboarding/BusinessAddressView;->businessAddressSameAsHomeCheckbox:Lcom/squareup/marketfont/MarketCheckedTextView;

    .line 59
    sget v0, Lcom/squareup/onboarding/flow/R$id;->business_address_mobile_business:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketCheckedTextView;

    iput-object v0, p0, Lcom/squareup/ui/onboarding/BusinessAddressView;->businessAddressMobileCheckbox:Lcom/squareup/marketfont/MarketCheckedTextView;

    .line 60
    sget v0, Lcom/squareup/onboarding/flow/R$id;->business_address_different_as_home:I

    .line 61
    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketCheckedTextView;

    iput-object v0, p0, Lcom/squareup/ui/onboarding/BusinessAddressView;->businessAddressDifferentAsHomeCheckbox:Lcom/squareup/marketfont/MarketCheckedTextView;

    .line 62
    sget v0, Lcom/squareup/onboarding/flow/R$id;->business_address_mobile_business_label:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/onboarding/BusinessAddressView;->businessAddressMobileLabel:Landroid/view/View;

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessAddressView;->addressTypeGroup:Lcom/squareup/widgets/CheckableGroup;

    new-instance v1, Lcom/squareup/ui/onboarding/-$$Lambda$BusinessAddressView$7UOsiLClK_Z2UJCUa5wPEqXA1BQ;

    invoke-direct {v1, p0}, Lcom/squareup/ui/onboarding/-$$Lambda$BusinessAddressView$7UOsiLClK_Z2UJCUa5wPEqXA1BQ;-><init>(Lcom/squareup/ui/onboarding/BusinessAddressView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/CheckableGroup;->setOnCheckedChangeListener(Lcom/squareup/widgets/CheckableGroup$OnCheckedChangeListener;)V

    .line 76
    sget v0, Lcom/squareup/onboarding/flow/R$id;->business_address_label:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/onboarding/BusinessAddressView;->businessAddressLabel:Landroid/view/View;

    .line 77
    sget v0, Lcom/squareup/onboarding/flow/R$id;->business_info_business_address:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/address/AddressLayout;

    iput-object v0, p0, Lcom/squareup/ui/onboarding/BusinessAddressView;->businessAddress:Lcom/squareup/address/AddressLayout;

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessAddressView;->businessAddress:Lcom/squareup/address/AddressLayout;

    invoke-virtual {v0}, Lcom/squareup/address/AddressLayout;->getPostal()Landroid/widget/EditText;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/ui/onboarding/BusinessAddressView;->bindAdvanceOnDone(Landroid/widget/EditText;)V

    return-void
.end method
