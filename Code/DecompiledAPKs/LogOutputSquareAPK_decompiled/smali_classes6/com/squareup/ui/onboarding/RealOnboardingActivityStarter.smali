.class public final Lcom/squareup/ui/onboarding/RealOnboardingActivityStarter;
.super Ljava/lang/Object;
.source "RealOnboardingActivityStarter.kt"

# interfaces
.implements Lcom/squareup/onboarding/OnboardingActivityStarter;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/ui/onboarding/RealOnboardingActivityStarter;",
        "Lcom/squareup/onboarding/OnboardingActivityStarter;",
        "application",
        "Landroid/app/Application;",
        "(Landroid/app/Application;)V",
        "startOnboardingActivity",
        "",
        "params",
        "Lcom/squareup/onboarding/OnboardingStarter$OnboardingParams;",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final application:Landroid/app/Application;


# direct methods
.method public constructor <init>(Landroid/app/Application;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "application"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/onboarding/RealOnboardingActivityStarter;->application:Landroid/app/Application;

    return-void
.end method


# virtual methods
.method public startOnboardingActivity(Lcom/squareup/onboarding/OnboardingStarter$OnboardingParams;)V
    .locals 4

    const-string v0, "params"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    iget-object v0, p0, Lcom/squareup/ui/onboarding/RealOnboardingActivityStarter;->application:Landroid/app/Application;

    .line 20
    new-instance v1, Landroid/content/Intent;

    move-object v2, v0

    check-cast v2, Landroid/content/Context;

    const-class v3, Lcom/squareup/ui/onboarding/OnboardingActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v2, 0x14000000

    .line 21
    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v1

    .line 22
    invoke-virtual {p1}, Lcom/squareup/onboarding/OnboardingStarter$OnboardingParams;->getActivationLaunchMode()Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;

    move-result-object v2

    check-cast v2, Ljava/io/Serializable;

    const-string v3, "ACTIVATION_MODE_KEY"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v1

    .line 23
    invoke-virtual {p1}, Lcom/squareup/onboarding/OnboardingStarter$OnboardingParams;->getDestinationAfterOnboarding()Lcom/squareup/onboarding/OnboardingStarter$DestinationAfterOnboarding;

    move-result-object p1

    check-cast p1, Ljava/io/Serializable;

    const-string v2, "DESTINATION_AFTER_KEY"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object p1

    .line 19
    invoke-virtual {v0, p1}, Landroid/app/Application;->startActivity(Landroid/content/Intent;)V

    return-void
.end method
