.class public Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "MerchantSubcategoryScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/onboarding/MerchantSubcategoryView;",
        ">;"
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final container:Lcom/squareup/ui/onboarding/OnboardingContainer;

.field private final model:Lcom/squareup/ui/onboarding/OnboardingModel;

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

.field final warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/flowlegacy/NoResultPopupPresenter<",
            "Lcom/squareup/widgets/warning/Warning;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/ui/onboarding/OnboardingModel;Lcom/squareup/ui/onboarding/OnboardingContainer;Lcom/squareup/ui/onboarding/OnboardingActivityRunner;Lcom/squareup/analytics/Analytics;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 49
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 46
    new-instance v0, Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    invoke-direct {v0}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen$Presenter;->warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    .line 50
    iput-object p1, p0, Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 51
    iput-object p2, p0, Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen$Presenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    .line 52
    iput-object p3, p0, Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen$Presenter;->container:Lcom/squareup/ui/onboarding/OnboardingContainer;

    .line 53
    iput-object p4, p0, Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen$Presenter;->runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    .line 54
    iput-object p5, p0, Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method

.method public static synthetic lambda$69GrDDSLIjEDzHsOvifMDM-Xhgo(Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen$Presenter;)V
    .locals 0

    invoke-direct {p0}, Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen$Presenter;->onContinueButtonClicked()V

    return-void
.end method

.method private onContinueButtonClicked()V
    .locals 3

    .line 75
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/MerchantSubcategoryView;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/MerchantSubcategoryView;->getSelected()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/activation/ActivationResources$BusinessSubcategory;

    if-eqz v0, :cond_0

    .line 77
    iget-object v1, p0, Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen$Presenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    invoke-virtual {v1, v0}, Lcom/squareup/ui/onboarding/OnboardingModel;->setBusinessSubcategory(Lcom/squareup/server/activation/ActivationResources$BusinessSubcategory;)V

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen$Presenter;->runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->onBusinessSubcategorySelected()V

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_SUB_MCC_CONTINUE:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    goto :goto_0

    .line 81
    :cond_0
    new-instance v0, Lcom/squareup/widgets/warning/WarningIds;

    sget v1, Lcom/squareup/banklinking/R$string;->missing_required_field:I

    sget v2, Lcom/squareup/onboarding/flow/R$string;->choose_a_category:I

    invoke-direct {v0, v1, v2}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    .line 84
    iget-object v1, p0, Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen$Presenter;->warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    invoke-virtual {v1, v0}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->show(Landroid/os/Parcelable;)V

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_SUB_MCC_CONTINUE_NON_SELECTED:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    :goto_0
    return-void
.end method


# virtual methods
.method public synthetic lambda$onLoad$0$MerchantSubcategoryScreen$Presenter(Lcom/squareup/widgets/CheckableGroup;II)V
    .locals 0

    .line 70
    iget-object p1, p0, Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object p2, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_SUB_MCC_RADIO:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {p1, p2}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 3

    .line 58
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 60
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/onboarding/MerchantSubcategoryView;

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/MerchantSubcategoryView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object p1

    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen$Presenter;->container:Lcom/squareup/ui/onboarding/OnboardingContainer;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/onboarding/-$$Lambda$5ZQ0I9OVAgq3phbZJSSrfxeiYdE;

    invoke-direct {v2, v1}, Lcom/squareup/ui/onboarding/-$$Lambda$5ZQ0I9OVAgq3phbZJSSrfxeiYdE;-><init>(Lcom/squareup/ui/onboarding/OnboardingContainer;)V

    .line 61
    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 62
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->updateUpButtonGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/onboarding/flow/R$string;->onboarding_actionbar_continue:I

    .line 63
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/onboarding/-$$Lambda$MerchantSubcategoryScreen$Presenter$69GrDDSLIjEDzHsOvifMDM-Xhgo;

    invoke-direct {v1, p0}, Lcom/squareup/ui/onboarding/-$$Lambda$MerchantSubcategoryScreen$Presenter$69GrDDSLIjEDzHsOvifMDM-Xhgo;-><init>(Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen$Presenter;)V

    .line 64
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 65
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    .line 60
    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 67
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/onboarding/MerchantSubcategoryView;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen$Presenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/OnboardingModel;->getBusinessCategory()Lcom/squareup/server/activation/ActivationResources$BusinessCategory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/server/activation/ActivationResources$BusinessCategory;->getSubCategories()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/onboarding/MerchantSubcategoryView;->setCategories(Ljava/util/List;)V

    .line 68
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/onboarding/MerchantSubcategoryView;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen$Presenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/OnboardingModel;->getBusinessCategory()Lcom/squareup/server/activation/ActivationResources$BusinessCategory;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/activation/ActivationResources$BusinessCategory;->sub_categories_title:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/onboarding/MerchantSubcategoryView;->setTitle(Ljava/lang/String;)V

    .line 69
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/onboarding/MerchantSubcategoryView;

    new-instance v0, Lcom/squareup/ui/onboarding/-$$Lambda$MerchantSubcategoryScreen$Presenter$RXkicySv0CnAB7hqylEycAJVhzI;

    invoke-direct {v0, p0}, Lcom/squareup/ui/onboarding/-$$Lambda$MerchantSubcategoryScreen$Presenter$RXkicySv0CnAB7hqylEycAJVhzI;-><init>(Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen$Presenter;)V

    invoke-virtual {p1, v0}, Lcom/squareup/ui/onboarding/MerchantSubcategoryView;->setCategorySelectedListener(Lcom/squareup/widgets/CheckableGroup$OnCheckedChangeListener;)V

    return-void
.end method
