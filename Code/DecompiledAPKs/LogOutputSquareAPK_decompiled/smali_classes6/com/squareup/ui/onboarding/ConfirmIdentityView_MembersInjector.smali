.class public final Lcom/squareup/ui/onboarding/ConfirmIdentityView_MembersInjector;
.super Ljava/lang/Object;
.source "ConfirmIdentityView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/onboarding/ConfirmIdentityView;",
        ">;"
    }
.end annotation


# instance fields
.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityView_MembersInjector;->localeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/onboarding/ConfirmIdentityView;",
            ">;"
        }
    .end annotation

    .line 30
    new-instance v0, Lcom/squareup/ui/onboarding/ConfirmIdentityView_MembersInjector;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/onboarding/ConfirmIdentityView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectLocaleProvider(Lcom/squareup/ui/onboarding/ConfirmIdentityView;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/onboarding/ConfirmIdentityView;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)V"
        }
    .end annotation

    .line 47
    iput-object p1, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityView;->localeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/ui/onboarding/ConfirmIdentityView;Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;)V
    .locals 0

    .line 41
    iput-object p1, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityView;->presenter:Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/onboarding/ConfirmIdentityView;)V
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/onboarding/ConfirmIdentityView_MembersInjector;->injectPresenter(Lcom/squareup/ui/onboarding/ConfirmIdentityView;Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;)V

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityView_MembersInjector;->localeProvider:Ljavax/inject/Provider;

    invoke-static {p1, v0}, Lcom/squareup/ui/onboarding/ConfirmIdentityView_MembersInjector;->injectLocaleProvider(Lcom/squareup/ui/onboarding/ConfirmIdentityView;Ljavax/inject/Provider;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 9
    check-cast p1, Lcom/squareup/ui/onboarding/ConfirmIdentityView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/ConfirmIdentityView_MembersInjector;->injectMembers(Lcom/squareup/ui/onboarding/ConfirmIdentityView;)V

    return-void
.end method
