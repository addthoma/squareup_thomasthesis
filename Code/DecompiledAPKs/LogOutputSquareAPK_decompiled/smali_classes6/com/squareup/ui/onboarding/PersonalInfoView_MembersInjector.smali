.class public final Lcom/squareup/ui/onboarding/PersonalInfoView_MembersInjector;
.super Ljava/lang/Object;
.source "PersonalInfoView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/onboarding/PersonalInfoView;",
        ">;"
    }
.end annotation


# instance fields
.field private final phoneNumberScrubberProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/InsertingScrubber;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/PersonalInfoPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final ssnStringsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/SSNStrings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/PersonalInfoPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/InsertingScrubber;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/SSNStrings;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/ui/onboarding/PersonalInfoView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/ui/onboarding/PersonalInfoView_MembersInjector;->phoneNumberScrubberProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/ui/onboarding/PersonalInfoView_MembersInjector;->ssnStringsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/PersonalInfoPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/InsertingScrubber;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/SSNStrings;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/onboarding/PersonalInfoView;",
            ">;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/ui/onboarding/PersonalInfoView_MembersInjector;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/onboarding/PersonalInfoView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectPhoneNumberScrubber(Lcom/squareup/ui/onboarding/PersonalInfoView;Lcom/squareup/text/InsertingScrubber;)V
    .locals 0

    .line 53
    iput-object p1, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->phoneNumberScrubber:Lcom/squareup/text/InsertingScrubber;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/ui/onboarding/PersonalInfoView;Lcom/squareup/ui/onboarding/PersonalInfoPresenter;)V
    .locals 0

    .line 47
    iput-object p1, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->presenter:Lcom/squareup/ui/onboarding/PersonalInfoPresenter;

    return-void
.end method

.method public static injectSsnStrings(Lcom/squareup/ui/onboarding/PersonalInfoView;Lcom/squareup/ui/onboarding/SSNStrings;)V
    .locals 0

    .line 58
    iput-object p1, p0, Lcom/squareup/ui/onboarding/PersonalInfoView;->ssnStrings:Lcom/squareup/ui/onboarding/SSNStrings;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/onboarding/PersonalInfoView;)V
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/onboarding/PersonalInfoView_MembersInjector;->injectPresenter(Lcom/squareup/ui/onboarding/PersonalInfoView;Lcom/squareup/ui/onboarding/PersonalInfoPresenter;)V

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView_MembersInjector;->phoneNumberScrubberProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/text/InsertingScrubber;

    invoke-static {p1, v0}, Lcom/squareup/ui/onboarding/PersonalInfoView_MembersInjector;->injectPhoneNumberScrubber(Lcom/squareup/ui/onboarding/PersonalInfoView;Lcom/squareup/text/InsertingScrubber;)V

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoView_MembersInjector;->ssnStringsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/SSNStrings;

    invoke-static {p1, v0}, Lcom/squareup/ui/onboarding/PersonalInfoView_MembersInjector;->injectSsnStrings(Lcom/squareup/ui/onboarding/PersonalInfoView;Lcom/squareup/ui/onboarding/SSNStrings;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 9
    check-cast p1, Lcom/squareup/ui/onboarding/PersonalInfoView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/PersonalInfoView_MembersInjector;->injectMembers(Lcom/squareup/ui/onboarding/PersonalInfoView;)V

    return-void
.end method
