.class public Lcom/squareup/ui/onboarding/AdditionalInfoScreen;
.super Lcom/squareup/ui/onboarding/InLoggedInOnboardingScope;
.source "AdditionalInfoScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Component;,
        Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/onboarding/AdditionalInfoScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/onboarding/AdditionalInfoScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 28
    new-instance v0, Lcom/squareup/ui/onboarding/AdditionalInfoScreen;

    invoke-direct {v0}, Lcom/squareup/ui/onboarding/AdditionalInfoScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/onboarding/AdditionalInfoScreen;->INSTANCE:Lcom/squareup/ui/onboarding/AdditionalInfoScreen;

    .line 33
    sget-object v0, Lcom/squareup/ui/onboarding/AdditionalInfoScreen;->INSTANCE:Lcom/squareup/ui/onboarding/AdditionalInfoScreen;

    .line 34
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/onboarding/AdditionalInfoScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 30
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/InLoggedInOnboardingScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 37
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_VERIFY:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 131
    sget v0, Lcom/squareup/onboarding/flow/R$layout;->additional_info_view:I

    return v0
.end method
