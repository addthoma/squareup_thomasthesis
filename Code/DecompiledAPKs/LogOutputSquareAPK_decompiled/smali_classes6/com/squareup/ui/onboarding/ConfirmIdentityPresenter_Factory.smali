.class public final Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter_Factory;
.super Ljava/lang/Object;
.source "ConfirmIdentityPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final modelProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingModel;",
            ">;"
        }
    .end annotation
.end field

.field private final resourcesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final updateCallPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/ActivationStatusPresenter<",
            "Lcom/squareup/server/activation/AnswersBody;",
            ">;>;"
        }
    .end annotation
.end field

.field private final updateReceivedResponseProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/ShareableReceivedResponse<",
            "Lcom/squareup/server/activation/AnswersBody;",
            "Lcom/squareup/server/SimpleResponse;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingModel;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/ActivationStatusPresenter<",
            "Lcom/squareup/server/activation/AnswersBody;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/ShareableReceivedResponse<",
            "Lcom/squareup/server/activation/AnswersBody;",
            "Lcom/squareup/server/SimpleResponse;",
            ">;>;)V"
        }
    .end annotation

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter_Factory;->resourcesProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p2, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter_Factory;->modelProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p3, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter_Factory;->updateCallPresenterProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p4, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p5, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter_Factory;->updateReceivedResponseProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingModel;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/ActivationStatusPresenter<",
            "Lcom/squareup/server/activation/AnswersBody;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/ShareableReceivedResponse<",
            "Lcom/squareup/server/activation/AnswersBody;",
            "Lcom/squareup/server/SimpleResponse;",
            ">;>;)",
            "Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter_Factory;"
        }
    .end annotation

    .line 53
    new-instance v6, Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/util/Res;Lcom/squareup/ui/onboarding/OnboardingModel;Ljava/lang/Object;Lcom/squareup/analytics/Analytics;Lcom/squareup/onboarding/ShareableReceivedResponse;)Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/ui/onboarding/OnboardingModel;",
            "Ljava/lang/Object;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/onboarding/ShareableReceivedResponse<",
            "Lcom/squareup/server/activation/AnswersBody;",
            "Lcom/squareup/server/SimpleResponse;",
            ">;)",
            "Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;"
        }
    .end annotation

    .line 59
    new-instance v6, Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;

    move-object v3, p2

    check-cast v3, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;-><init>(Lcom/squareup/util/Res;Lcom/squareup/ui/onboarding/OnboardingModel;Lcom/squareup/ui/onboarding/ActivationStatusPresenter;Lcom/squareup/analytics/Analytics;Lcom/squareup/onboarding/ShareableReceivedResponse;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;
    .locals 5

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter_Factory;->resourcesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter_Factory;->modelProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/onboarding/OnboardingModel;

    iget-object v2, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter_Factory;->updateCallPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/analytics/Analytics;

    iget-object v4, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter_Factory;->updateReceivedResponseProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/onboarding/ShareableReceivedResponse;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter_Factory;->newInstance(Lcom/squareup/util/Res;Lcom/squareup/ui/onboarding/OnboardingModel;Ljava/lang/Object;Lcom/squareup/analytics/Analytics;Lcom/squareup/onboarding/ShareableReceivedResponse;)Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter_Factory;->get()Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;

    move-result-object v0

    return-object v0
.end method
