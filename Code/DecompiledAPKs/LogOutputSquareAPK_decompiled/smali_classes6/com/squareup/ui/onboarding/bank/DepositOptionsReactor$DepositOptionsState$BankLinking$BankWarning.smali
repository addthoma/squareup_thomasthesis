.class public final Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$BankWarning;
.super Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking;
.source "DepositOptionsReactor.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BankWarning"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\r\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010\u0010\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0007H\u00c6\u0003J)\u0010\u0012\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007H\u00c6\u0001J\u0013\u0010\u0013\u001a\u00020\u00032\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u00d6\u0003J\t\u0010\u0016\u001a\u00020\u0017H\u00d6\u0001J\t\u0010\u0018\u001a\u00020\u0019H\u00d6\u0001R\u0016\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$BankWarning;",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking;",
        "sameDayDepositSelected",
        "",
        "bankAccountType",
        "Lcom/squareup/protos/client/bankaccount/BankAccountType;",
        "failureMessage",
        "Lcom/squareup/receiving/FailureMessage;",
        "(ZLcom/squareup/protos/client/bankaccount/BankAccountType;Lcom/squareup/receiving/FailureMessage;)V",
        "getBankAccountType",
        "()Lcom/squareup/protos/client/bankaccount/BankAccountType;",
        "getFailureMessage",
        "()Lcom/squareup/receiving/FailureMessage;",
        "getSameDayDepositSelected",
        "()Z",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final bankAccountType:Lcom/squareup/protos/client/bankaccount/BankAccountType;

.field private final failureMessage:Lcom/squareup/receiving/FailureMessage;

.field private final sameDayDepositSelected:Z


# direct methods
.method public constructor <init>(ZLcom/squareup/protos/client/bankaccount/BankAccountType;Lcom/squareup/receiving/FailureMessage;)V
    .locals 1

    const-string v0, "failureMessage"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 146
    invoke-direct {p0, v0}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-boolean p1, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$BankWarning;->sameDayDepositSelected:Z

    iput-object p2, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$BankWarning;->bankAccountType:Lcom/squareup/protos/client/bankaccount/BankAccountType;

    iput-object p3, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$BankWarning;->failureMessage:Lcom/squareup/receiving/FailureMessage;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$BankWarning;ZLcom/squareup/protos/client/bankaccount/BankAccountType;Lcom/squareup/receiving/FailureMessage;ILjava/lang/Object;)Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$BankWarning;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$BankWarning;->getSameDayDepositSelected()Z

    move-result p1

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$BankWarning;->getBankAccountType()Lcom/squareup/protos/client/bankaccount/BankAccountType;

    move-result-object p2

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$BankWarning;->failureMessage:Lcom/squareup/receiving/FailureMessage;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$BankWarning;->copy(ZLcom/squareup/protos/client/bankaccount/BankAccountType;Lcom/squareup/receiving/FailureMessage;)Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$BankWarning;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$BankWarning;->getSameDayDepositSelected()Z

    move-result v0

    return v0
.end method

.method public final component2()Lcom/squareup/protos/client/bankaccount/BankAccountType;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$BankWarning;->getBankAccountType()Lcom/squareup/protos/client/bankaccount/BankAccountType;

    move-result-object v0

    return-object v0
.end method

.method public final component3()Lcom/squareup/receiving/FailureMessage;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$BankWarning;->failureMessage:Lcom/squareup/receiving/FailureMessage;

    return-object v0
.end method

.method public final copy(ZLcom/squareup/protos/client/bankaccount/BankAccountType;Lcom/squareup/receiving/FailureMessage;)Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$BankWarning;
    .locals 1

    const-string v0, "failureMessage"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$BankWarning;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$BankWarning;-><init>(ZLcom/squareup/protos/client/bankaccount/BankAccountType;Lcom/squareup/receiving/FailureMessage;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$BankWarning;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$BankWarning;

    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$BankWarning;->getSameDayDepositSelected()Z

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$BankWarning;->getSameDayDepositSelected()Z

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$BankWarning;->getBankAccountType()Lcom/squareup/protos/client/bankaccount/BankAccountType;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$BankWarning;->getBankAccountType()Lcom/squareup/protos/client/bankaccount/BankAccountType;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$BankWarning;->failureMessage:Lcom/squareup/receiving/FailureMessage;

    iget-object p1, p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$BankWarning;->failureMessage:Lcom/squareup/receiving/FailureMessage;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getBankAccountType()Lcom/squareup/protos/client/bankaccount/BankAccountType;
    .locals 1

    .line 144
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$BankWarning;->bankAccountType:Lcom/squareup/protos/client/bankaccount/BankAccountType;

    return-object v0
.end method

.method public final getFailureMessage()Lcom/squareup/receiving/FailureMessage;
    .locals 1

    .line 145
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$BankWarning;->failureMessage:Lcom/squareup/receiving/FailureMessage;

    return-object v0
.end method

.method public getSameDayDepositSelected()Z
    .locals 1

    .line 143
    iget-boolean v0, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$BankWarning;->sameDayDepositSelected:Z

    return v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$BankWarning;->getSameDayDepositSelected()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$BankWarning;->getBankAccountType()Lcom/squareup/protos/client/bankaccount/BankAccountType;

    move-result-object v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$BankWarning;->failureMessage:Lcom/squareup/receiving/FailureMessage;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BankWarning(sameDayDepositSelected="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$BankWarning;->getSameDayDepositSelected()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", bankAccountType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$BankWarning;->getBankAccountType()Lcom/squareup/protos/client/bankaccount/BankAccountType;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", failureMessage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$BankWarning;->failureMessage:Lcom/squareup/receiving/FailureMessage;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
