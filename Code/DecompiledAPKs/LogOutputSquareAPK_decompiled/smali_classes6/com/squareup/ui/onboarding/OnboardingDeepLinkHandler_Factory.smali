.class public final Lcom/squareup/ui/onboarding/OnboardingDeepLinkHandler_Factory;
.super Ljava/lang/Object;
.source "OnboardingDeepLinkHandler_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/onboarding/OnboardingDeepLinkHandler;",
        ">;"
    }
.end annotation


# instance fields
.field private final onboardingDiverterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingDiverter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingDiverter;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingDeepLinkHandler_Factory;->onboardingDiverterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/ui/onboarding/OnboardingDeepLinkHandler_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingDiverter;",
            ">;)",
            "Lcom/squareup/ui/onboarding/OnboardingDeepLinkHandler_Factory;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/ui/onboarding/OnboardingDeepLinkHandler_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/ui/onboarding/OnboardingDeepLinkHandler_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/onboarding/OnboardingDiverter;)Lcom/squareup/ui/onboarding/OnboardingDeepLinkHandler;
    .locals 1

    .line 35
    new-instance v0, Lcom/squareup/ui/onboarding/OnboardingDeepLinkHandler;

    invoke-direct {v0, p0}, Lcom/squareup/ui/onboarding/OnboardingDeepLinkHandler;-><init>(Lcom/squareup/onboarding/OnboardingDiverter;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/onboarding/OnboardingDeepLinkHandler;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingDeepLinkHandler_Factory;->onboardingDiverterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/onboarding/OnboardingDiverter;

    invoke-static {v0}, Lcom/squareup/ui/onboarding/OnboardingDeepLinkHandler_Factory;->newInstance(Lcom/squareup/onboarding/OnboardingDiverter;)Lcom/squareup/ui/onboarding/OnboardingDeepLinkHandler;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/OnboardingDeepLinkHandler_Factory;->get()Lcom/squareup/ui/onboarding/OnboardingDeepLinkHandler;

    move-result-object v0

    return-object v0
.end method
