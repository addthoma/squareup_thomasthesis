.class public final Lcom/squareup/ui/onboarding/intent/IntentHowScreen;
.super Lcom/squareup/ui/onboarding/InLoggedInOnboardingScope;
.source "IntentHowScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/LocksOrientation;
.implements Lcom/squareup/coordinators/CoordinatorProvider;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/onboarding/intent/IntentHowScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/onboarding/intent/IntentHowScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/squareup/ui/onboarding/intent/IntentHowScreen;

    invoke-direct {v0}, Lcom/squareup/ui/onboarding/intent/IntentHowScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/onboarding/intent/IntentHowScreen;->INSTANCE:Lcom/squareup/ui/onboarding/intent/IntentHowScreen;

    .line 47
    sget-object v0, Lcom/squareup/ui/onboarding/intent/IntentHowScreen;->INSTANCE:Lcom/squareup/ui/onboarding/intent/IntentHowScreen;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/onboarding/intent/IntentHowScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 24
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/InLoggedInOnboardingScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 40
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->ACTIVATION_INTENT_HOW:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getOrientationForPhone()Lcom/squareup/workflow/WorkflowViewFactory$Orientation;
    .locals 1

    .line 32
    sget-object v0, Lcom/squareup/workflow/WorkflowViewFactory$Orientation;->SENSOR_PORTRAIT:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    return-object v0
.end method

.method public getOrientationForTablet()Lcom/squareup/workflow/WorkflowViewFactory$Orientation;
    .locals 1

    .line 36
    sget-object v0, Lcom/squareup/workflow/WorkflowViewFactory$Orientation;->UNLOCKED:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    return-object v0
.end method

.method public bridge synthetic provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 0

    .line 20
    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/intent/IntentHowScreen;->provideCoordinator(Landroid/view/View;)Lcom/squareup/ui/onboarding/intent/IntentHowCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/ui/onboarding/intent/IntentHowCoordinator;
    .locals 1

    .line 44
    const-class v0, Lcom/squareup/ui/onboarding/CommonOnboardingActivityComponent;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/onboarding/CommonOnboardingActivityComponent;

    invoke-interface {p1}, Lcom/squareup/ui/onboarding/CommonOnboardingActivityComponent;->intentHowCoordinator()Lcom/squareup/ui/onboarding/intent/IntentHowCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 28
    sget v0, Lcom/squareup/onboarding/flow/R$layout;->intent_how_view:I

    return v0
.end method
