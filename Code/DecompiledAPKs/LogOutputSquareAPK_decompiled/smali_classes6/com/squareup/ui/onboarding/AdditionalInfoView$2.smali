.class Lcom/squareup/ui/onboarding/AdditionalInfoView$2;
.super Lcom/squareup/debounce/DebouncedOnEditorActionListener;
.source "AdditionalInfoView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/onboarding/AdditionalInfoView;->setBirthDateViewVisible()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/onboarding/AdditionalInfoView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/onboarding/AdditionalInfoView;)V
    .locals 0

    .line 131
    iput-object p1, p0, Lcom/squareup/ui/onboarding/AdditionalInfoView$2;->this$0:Lcom/squareup/ui/onboarding/AdditionalInfoView;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnEditorActionListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doOnEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 0

    const/4 p1, 0x5

    if-ne p2, p1, :cond_0

    .line 134
    iget-object p1, p0, Lcom/squareup/ui/onboarding/AdditionalInfoView$2;->this$0:Lcom/squareup/ui/onboarding/AdditionalInfoView;

    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 135
    iget-object p1, p0, Lcom/squareup/ui/onboarding/AdditionalInfoView$2;->this$0:Lcom/squareup/ui/onboarding/AdditionalInfoView;

    invoke-static {p1}, Lcom/squareup/ui/onboarding/AdditionalInfoView;->access$000(Lcom/squareup/ui/onboarding/AdditionalInfoView;)Landroid/widget/TextView;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/TextView;->requestFocus()Z

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method
