.class final Lcom/squareup/ui/onboarding/OnboardingContainer$Module$1;
.super Ljava/lang/Object;
.source "OnboardingContainer.java"

# interfaces
.implements Lcom/squareup/container/ContainerBackgroundsProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/onboarding/OnboardingContainer$Module;->provideContainerActivityDelegate(Lcom/squareup/util/Device;)Lcom/squareup/container/ContainerActivityDelegate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 298
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCardScreenBackground(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;
    .locals 1

    .line 304
    sget-object v0, Lcom/squareup/ui/main/PosContainer;->Companion:Lcom/squareup/ui/main/PosContainer$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/main/PosContainer$Companion;->getThemeCardBackgroundDrawable(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    return-object p1
.end method

.method public getCardScreenScrimColor(Landroid/content/Context;)I
    .locals 0

    .line 308
    sget p1, Lcom/squareup/marin/R$color;->marin_screen_scrim:I

    return p1
.end method

.method public getWindowBackground(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;
    .locals 1

    .line 300
    sget-object v0, Lcom/squareup/ui/main/PosContainer;->Companion:Lcom/squareup/ui/main/PosContainer$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/main/PosContainer$Companion;->getThemeWindowBackgroundDrawable(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    return-object p1
.end method
