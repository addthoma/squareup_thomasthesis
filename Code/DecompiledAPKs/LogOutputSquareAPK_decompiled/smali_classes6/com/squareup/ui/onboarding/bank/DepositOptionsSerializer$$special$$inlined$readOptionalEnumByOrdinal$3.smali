.class public final Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer$$special$$inlined$readOptionalEnumByOrdinal$3;
.super Lkotlin/jvm/internal/Lambda;
.source "Snapshot.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer;->deserializeState(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lokio/BufferedSource;",
        "Lcom/squareup/protos/client/bankaccount/BankAccountType;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSnapshot.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Snapshot.kt\ncom/squareup/workflow/SnapshotKt$readOptionalEnumByOrdinal$1\n+ 2 Snapshot.kt\ncom/squareup/workflow/SnapshotKt\n*L\n1#1,181:1\n148#2:182\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0010\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u0002H\u0001\"\u0010\u0008\u0000\u0010\u0001\u0018\u0001*\u0008\u0012\u0004\u0012\u0002H\u00010\u0002*\u00020\u0003H\n\u00a2\u0006\u0004\u0008\u0004\u0010\u0005\u00a8\u0006\u0006"
    }
    d2 = {
        "<anonymous>",
        "T",
        "",
        "Lokio/BufferedSource;",
        "invoke",
        "(Lokio/BufferedSource;)Ljava/lang/Enum;",
        "com/squareup/workflow/SnapshotKt$readOptionalEnumByOrdinal$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer$$special$$inlined$readOptionalEnumByOrdinal$3;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer$$special$$inlined$readOptionalEnumByOrdinal$3;

    invoke-direct {v0}, Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer$$special$$inlined$readOptionalEnumByOrdinal$3;-><init>()V

    sput-object v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer$$special$$inlined$readOptionalEnumByOrdinal$3;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer$$special$$inlined$readOptionalEnumByOrdinal$3;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lokio/BufferedSource;)Ljava/lang/Enum;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lokio/BufferedSource;",
            ")",
            "Lcom/squareup/protos/client/bankaccount/BankAccountType;"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 182
    const-class v0, Lcom/squareup/protos/client/bankaccount/BankAccountType;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Enum;

    invoke-interface {p1}, Lokio/BufferedSource;->readInt()I

    move-result p1

    aget-object p1, v0, p1

    const-string v0, "T::class.java.enumConstants[readInt()]"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lokio/BufferedSource;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer$$special$$inlined$readOptionalEnumByOrdinal$3;->invoke(Lokio/BufferedSource;)Ljava/lang/Enum;

    move-result-object p1

    return-object p1
.end method
