.class Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "CardProcessingNotActivatedScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/onboarding/CardProcessingNotActivatedView;",
        ">;"
    }
.end annotation


# instance fields
.field private final actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private final flow:Lflow/Flow;

.field private final formatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method constructor <init>(Lflow/Flow;Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/payment/Transaction;Lcom/squareup/text/Formatter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflow/Flow;",
            "Lcom/squareup/marin/widgets/MarinActionBar;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 41
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen$Presenter;->flow:Lflow/Flow;

    .line 43
    iput-object p2, p0, Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 44
    iput-object p3, p0, Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 45
    iput-object p4, p0, Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen$Presenter;->formatter:Lcom/squareup/text/Formatter;

    return-void
.end method

.method private cardProcessingNotActivatedCanceled()V
    .locals 2

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen$Presenter;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public static synthetic lambda$mxWDiZA3CAmiNzSssJcsPfR4JTw(Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen$Presenter;)V
    .locals 0

    invoke-direct {p0}, Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen$Presenter;->cardProcessingNotActivatedCanceled()V

    return-void
.end method


# virtual methods
.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 49
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 51
    iget-object p1, p0, Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen$Presenter;->formatter:Lcom/squareup/text/Formatter;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getGrandTotal()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 52
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 53
    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 54
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hidePrimaryButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 55
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hideSecondaryButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/onboarding/-$$Lambda$CardProcessingNotActivatedScreen$Presenter$mxWDiZA3CAmiNzSssJcsPfR4JTw;

    invoke-direct {v0, p0}, Lcom/squareup/ui/onboarding/-$$Lambda$CardProcessingNotActivatedScreen$Presenter$mxWDiZA3CAmiNzSssJcsPfR4JTw;-><init>(Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen$Presenter;)V

    .line 56
    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 57
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method
