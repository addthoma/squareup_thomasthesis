.class public final Lcom/squareup/ui/onboarding/MerchantSubcategoryView_MembersInjector;
.super Ljava/lang/Object;
.source "MerchantSubcategoryView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/onboarding/MerchantSubcategoryView;",
        ">;"
    }
.end annotation


# instance fields
.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen$Presenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen$Presenter;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/ui/onboarding/MerchantSubcategoryView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen$Presenter;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/onboarding/MerchantSubcategoryView;",
            ">;"
        }
    .end annotation

    .line 26
    new-instance v0, Lcom/squareup/ui/onboarding/MerchantSubcategoryView_MembersInjector;

    invoke-direct {v0, p0}, Lcom/squareup/ui/onboarding/MerchantSubcategoryView_MembersInjector;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectPresenter(Lcom/squareup/ui/onboarding/MerchantSubcategoryView;Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen$Presenter;)V
    .locals 0

    .line 36
    iput-object p1, p0, Lcom/squareup/ui/onboarding/MerchantSubcategoryView;->presenter:Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen$Presenter;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/onboarding/MerchantSubcategoryView;)V
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/ui/onboarding/MerchantSubcategoryView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen$Presenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/onboarding/MerchantSubcategoryView_MembersInjector;->injectPresenter(Lcom/squareup/ui/onboarding/MerchantSubcategoryView;Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen$Presenter;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 8
    check-cast p1, Lcom/squareup/ui/onboarding/MerchantSubcategoryView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/MerchantSubcategoryView_MembersInjector;->injectMembers(Lcom/squareup/ui/onboarding/MerchantSubcategoryView;)V

    return-void
.end method
