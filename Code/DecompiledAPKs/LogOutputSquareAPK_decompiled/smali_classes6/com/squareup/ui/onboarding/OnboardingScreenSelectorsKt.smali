.class public final Lcom/squareup/ui/onboarding/OnboardingScreenSelectorsKt;
.super Ljava/lang/Object;
.source "OnboardingScreenSelectors.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0000\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a$\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0008\u00a8\u0006\t"
    }
    d2 = {
        "shouldShowCustomFirstScreen",
        "",
        "Lcom/squareup/onboarding/OnboardingScreenSelector;",
        "modelHolder",
        "Lcom/squareup/ui/onboarding/ModelHolder;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "feature",
        "Lcom/squareup/settings/server/Features$Feature;",
        "onboarding_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final shouldShowCustomFirstScreen(Lcom/squareup/onboarding/OnboardingScreenSelector;Lcom/squareup/ui/onboarding/ModelHolder;Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/Features$Feature;)Z
    .locals 1

    const-string v0, "$this$shouldShowCustomFirstScreen"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p0, "modelHolder"

    invoke-static {p1, p0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p0, "features"

    invoke-static {p2, p0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p0, 0x1

    if-eqz p3, :cond_0

    .line 21
    invoke-interface {p2, p3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p2

    goto :goto_0

    :cond_0
    const/4 p2, 0x1

    :goto_0
    if-eqz p2, :cond_1

    .line 26
    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/ModelHolder;->getModel()Lcom/squareup/ui/onboarding/OnboardingModel;

    move-result-object p1

    const-string p2, "modelHolder.model"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/OnboardingModel;->isRestart()Z

    move-result p1

    if-nez p1, :cond_1

    goto :goto_1

    :cond_1
    const/4 p0, 0x0

    :goto_1
    return p0
.end method
