.class public final Lcom/squareup/ui/onboarding/bank/DepositOptionsWarningDialogScreenKt;
.super Ljava/lang/Object;
.source "DepositOptionsWarningDialogScreen.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\u001a,\u0010\u0000\u001a\u0012\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001j\u0002`\u00042\u0006\u0010\u0005\u001a\u00020\u00022\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u0007\u001a\u001a\u0010\u0008\u001a\u00020\t*\u0012\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001j\u0002`\u0004*\"\u0010\u0000\"\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001\u00a8\u0006\n"
    }
    d2 = {
        "DepositOptionsWarningDialogScreen",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/widgets/warning/Warning;",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsWarningDialog$DismissDialog;",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsWarningDialogScreen;",
        "screenData",
        "workflow",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "dismiss",
        "",
        "onboarding_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final DepositOptionsWarningDialogScreen(Lcom/squareup/widgets/warning/Warning;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/widgets/warning/Warning;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/ui/onboarding/bank/DepositOptionsWarningDialog$DismissDialog;",
            ">;)",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/widgets/warning/Warning;",
            "Lcom/squareup/ui/onboarding/bank/DepositOptionsWarningDialog$DismissDialog;",
            ">;"
        }
    .end annotation

    const-string v0, "screenData"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "workflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    new-instance v0, Lcom/squareup/workflow/legacy/Screen;

    sget-object v1, Lcom/squareup/ui/onboarding/bank/DepositOptionsWarningDialog;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsWarningDialog;

    invoke-virtual {v1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsWarningDialog;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    invoke-direct {v0, v1, p0, p1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object v0
.end method

.method public static final dismiss(Lcom/squareup/workflow/legacy/Screen;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/widgets/warning/Warning;",
            "Lcom/squareup/ui/onboarding/bank/DepositOptionsWarningDialog$DismissDialog;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$dismiss"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    iget-object p0, p0, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    sget-object v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsWarningDialog$DismissDialog;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsWarningDialog$DismissDialog;

    invoke-interface {p0, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method
