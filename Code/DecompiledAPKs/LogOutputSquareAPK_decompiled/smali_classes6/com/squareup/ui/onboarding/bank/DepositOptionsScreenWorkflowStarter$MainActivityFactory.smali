.class public final Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$MainActivityFactory;
.super Ljava/lang/Object;
.source "DepositOptionsScreenWorkflow.kt"

# interfaces
.implements Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MainActivityFactory"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDepositOptionsScreenWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DepositOptionsScreenWorkflow.kt\ncom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$MainActivityFactory\n*L\n1#1,423:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\'\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0008\u0010\u000b\u001a\u00020\u000cH\u0016R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$MainActivityFactory;",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$Factory;",
        "reactor",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "res",
        "Lcom/squareup/util/Res;",
        "rateFormatter",
        "Lcom/squareup/text/RateFormatter;",
        "(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Res;Lcom/squareup/text/RateFormatter;)V",
        "create",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final rateFormatter:Lcom/squareup/text/RateFormatter;

.field private final reactor:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;

.field private final res:Lcom/squareup/util/Res;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Res;Lcom/squareup/text/RateFormatter;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "reactor"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "settings"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rateFormatter"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$MainActivityFactory;->reactor:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;

    iput-object p2, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$MainActivityFactory;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object p3, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$MainActivityFactory;->res:Lcom/squareup/util/Res;

    iput-object p4, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$MainActivityFactory;->rateFormatter:Lcom/squareup/text/RateFormatter;

    return-void
.end method


# virtual methods
.method public create()Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;
    .locals 8

    .line 131
    iget-object v1, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$MainActivityFactory;->reactor:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$MainActivityFactory;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v0

    const-string v2, "settings.userSettings"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    move-object v2, v0

    iget-object v3, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$MainActivityFactory;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v4, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$MainActivityFactory;->res:Lcom/squareup/util/Res;

    iget-object v5, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$MainActivityFactory;->rateFormatter:Lcom/squareup/text/RateFormatter;

    .line 132
    new-instance v6, Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowConfig;

    const/4 v0, 0x1

    const/4 v7, 0x0

    invoke-direct {v6, v0, v7}, Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowConfig;-><init>(ZZ)V

    .line 130
    new-instance v7, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;-><init>(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;Ljava/lang/String;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Res;Lcom/squareup/text/RateFormatter;Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowConfig;)V

    return-object v7
.end method
