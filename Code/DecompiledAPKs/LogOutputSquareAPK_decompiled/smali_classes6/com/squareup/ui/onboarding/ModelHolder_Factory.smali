.class public final Lcom/squareup/ui/onboarding/ModelHolder_Factory;
.super Ljava/lang/Object;
.source "ModelHolder_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/onboarding/ModelHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private final onboardingModelBundleKeyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/ui/onboarding/OnboardingModel;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/ui/onboarding/OnboardingModel;",
            ">;>;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/ui/onboarding/ModelHolder_Factory;->onboardingModelBundleKeyProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/ui/onboarding/ModelHolder_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/ui/onboarding/OnboardingModel;",
            ">;>;)",
            "Lcom/squareup/ui/onboarding/ModelHolder_Factory;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/ui/onboarding/ModelHolder_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/ui/onboarding/ModelHolder_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/BundleKey;)Lcom/squareup/ui/onboarding/ModelHolder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/ui/onboarding/OnboardingModel;",
            ">;)",
            "Lcom/squareup/ui/onboarding/ModelHolder;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/ui/onboarding/ModelHolder;

    invoke-direct {v0, p0}, Lcom/squareup/ui/onboarding/ModelHolder;-><init>(Lcom/squareup/BundleKey;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/onboarding/ModelHolder;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/ui/onboarding/ModelHolder_Factory;->onboardingModelBundleKeyProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/BundleKey;

    invoke-static {v0}, Lcom/squareup/ui/onboarding/ModelHolder_Factory;->newInstance(Lcom/squareup/BundleKey;)Lcom/squareup/ui/onboarding/ModelHolder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/ModelHolder_Factory;->get()Lcom/squareup/ui/onboarding/ModelHolder;

    move-result-object v0

    return-object v0
.end method
