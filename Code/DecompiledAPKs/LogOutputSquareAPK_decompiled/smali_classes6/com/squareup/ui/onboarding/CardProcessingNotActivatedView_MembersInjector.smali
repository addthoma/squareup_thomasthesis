.class public final Lcom/squareup/ui/onboarding/CardProcessingNotActivatedView_MembersInjector;
.super Ljava/lang/Object;
.source "CardProcessingNotActivatedView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/onboarding/CardProcessingNotActivatedView;",
        ">;"
    }
.end annotation


# instance fields
.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen$Presenter;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen$Presenter;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/ui/onboarding/CardProcessingNotActivatedView_MembersInjector;->settingsProvider:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/squareup/ui/onboarding/CardProcessingNotActivatedView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen$Presenter;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/onboarding/CardProcessingNotActivatedView;",
            ">;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/ui/onboarding/CardProcessingNotActivatedView_MembersInjector;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/onboarding/CardProcessingNotActivatedView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectPresenter(Lcom/squareup/ui/onboarding/CardProcessingNotActivatedView;Ljava/lang/Object;)V
    .locals 0

    .line 48
    check-cast p1, Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen$Presenter;

    iput-object p1, p0, Lcom/squareup/ui/onboarding/CardProcessingNotActivatedView;->presenter:Lcom/squareup/ui/onboarding/CardProcessingNotActivatedScreen$Presenter;

    return-void
.end method

.method public static injectSettings(Lcom/squareup/ui/onboarding/CardProcessingNotActivatedView;Lcom/squareup/settings/server/AccountStatusSettings;)V
    .locals 0

    .line 43
    iput-object p1, p0, Lcom/squareup/ui/onboarding/CardProcessingNotActivatedView;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/onboarding/CardProcessingNotActivatedView;)V
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/onboarding/CardProcessingNotActivatedView_MembersInjector;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-static {p1, v0}, Lcom/squareup/ui/onboarding/CardProcessingNotActivatedView_MembersInjector;->injectSettings(Lcom/squareup/ui/onboarding/CardProcessingNotActivatedView;Lcom/squareup/settings/server/AccountStatusSettings;)V

    .line 37
    iget-object v0, p0, Lcom/squareup/ui/onboarding/CardProcessingNotActivatedView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/onboarding/CardProcessingNotActivatedView_MembersInjector;->injectPresenter(Lcom/squareup/ui/onboarding/CardProcessingNotActivatedView;Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 9
    check-cast p1, Lcom/squareup/ui/onboarding/CardProcessingNotActivatedView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/CardProcessingNotActivatedView_MembersInjector;->injectMembers(Lcom/squareup/ui/onboarding/CardProcessingNotActivatedView;)V

    return-void
.end method
