.class public Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "OnboardingErrorScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/onboarding/OnboardingErrorScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/onboarding/OnboardingErrorView;",
        ">;"
    }
.end annotation


# instance fields
.field private final model:Lcom/squareup/ui/onboarding/OnboardingModel;

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

.field private final settingsProvider:Lcom/squareup/settings/server/AccountStatusSettings;

.field private state:Lcom/squareup/server/activation/ActivationStatus$State;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/onboarding/OnboardingModel;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/onboarding/OnboardingActivityRunner;Lcom/squareup/util/Res;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 46
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Presenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    .line 48
    iput-object p2, p0, Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Presenter;->settingsProvider:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 49
    iput-object p3, p0, Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Presenter;->runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    .line 50
    iput-object p4, p0, Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Presenter;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Presenter;)Lcom/squareup/ui/onboarding/OnboardingActivityRunner;
    .locals 0

    .line 36
    iget-object p0, p0, Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Presenter;->runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Presenter;)Lcom/squareup/settings/server/AccountStatusSettings;
    .locals 0

    .line 36
    iget-object p0, p0, Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Presenter;->settingsProvider:Lcom/squareup/settings/server/AccountStatusSettings;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Presenter;)Ljava/lang/Object;
    .locals 0

    .line 36
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method protected onLoad(Landroid/os/Bundle;)V
    .locals 6

    .line 54
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 56
    iget-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Presenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/OnboardingModel;->getActivationErrorState()Lcom/squareup/server/activation/ActivationStatus$State;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Presenter;->state:Lcom/squareup/server/activation/ActivationStatus$State;

    .line 57
    iget-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Presenter;->state:Lcom/squareup/server/activation/ActivationStatus$State;

    if-eqz p1, :cond_3

    .line 61
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;

    .line 62
    new-instance v0, Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Presenter$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Presenter$1;-><init>(Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Presenter;)V

    invoke-virtual {p1, v0}, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->setTopButtonOnClicked(Landroid/view/View$OnClickListener;)V

    .line 68
    sget-object v0, Lcom/squareup/ui/onboarding/OnboardingErrorScreen$1;->$SwitchMap$com$squareup$server$activation$ActivationStatus$State:[I

    iget-object v1, p0, Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Presenter;->state:Lcom/squareup/server/activation/ActivationStatus$State;

    invoke-virtual {v1}, Lcom/squareup/server/activation/ActivationStatus$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    const/4 v2, -0x1

    const/4 v3, 0x0

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 96
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/onboarding/flow/R$string;->activation_declined_explanation_contact_support:I

    .line 97
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/text/style/URLSpan;

    iget-object v4, p0, Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v5, Lcom/squareup/onboarding/flow/R$string;->help_url:I

    .line 98
    invoke-interface {v4, v5}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Landroid/text/style/URLSpan;-><init>(Ljava/lang/String;)V

    .line 97
    invoke-static {v0, v1}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object v0

    .line 100
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/onboarding/OnboardingErrorView;

    invoke-virtual {v1}, Lcom/squareup/ui/onboarding/OnboardingErrorView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v4, Lcom/squareup/onboarding/flow/R$string;->activation_declined_explanation:I

    invoke-static {v1, v4}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    const-string v4, "activation_declined_explanation_contact_support"

    .line 101
    invoke-virtual {v1, v4, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 102
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 103
    sget v1, Lcom/squareup/onboarding/flow/R$string;->activation_declined_title:I

    invoke-virtual {p1, v1}, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->setTitleText(I)V

    .line 104
    invoke-virtual {p1, v0}, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->setMessageText(Ljava/lang/CharSequence;)V

    .line 105
    sget v0, Lcom/squareup/common/strings/R$string;->continue_label:I

    invoke-virtual {p1, v0}, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->setTopButton(I)V

    .line 106
    invoke-virtual {p1, v2}, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->setBottomButton(I)V

    .line 107
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p1, v0, v3}, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;F)V

    goto :goto_0

    .line 110
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown activation state: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Presenter;->state:Lcom/squareup/server/activation/ActivationStatus$State;

    invoke-virtual {v1}, Lcom/squareup/server/activation/ActivationStatus$State;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 77
    :cond_1
    sget v0, Lcom/squareup/onboarding/flow/R$string;->onboarding_quiz_timeout_title:I

    invoke-virtual {p1, v0}, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->setTitleText(I)V

    .line 78
    sget v0, Lcom/squareup/onboarding/flow/R$string;->onboarding_quiz_timeout_explanation:I

    invoke-virtual {p1, v0}, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->setMessageText(I)V

    .line 79
    sget v0, Lcom/squareup/onboarding/flow/R$string;->contact_support_reapply:I

    invoke-virtual {p1, v0}, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->setTopButton(I)V

    .line 80
    sget v0, Lcom/squareup/onboarding/flow/R$string;->later:I

    invoke-virtual {p1, v0}, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->setBottomButton(I)V

    .line 81
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p1, v0, v3}, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;F)V

    .line 82
    new-instance v0, Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Presenter$2;

    invoke-direct {v0, p0}, Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Presenter$2;-><init>(Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Presenter;)V

    invoke-virtual {p1, v0}, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->setTopButtonOnClicked(Landroid/view/View$OnClickListener;)V

    .line 88
    new-instance v0, Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Presenter$3;

    invoke-direct {v0, p0}, Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Presenter$3;-><init>(Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Presenter;)V

    invoke-virtual {p1, v0}, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->setBottomButtonOnClicked(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 70
    :cond_2
    sget v0, Lcom/squareup/onboarding/flow/R$string;->underwriting_error_title:I

    invoke-virtual {p1, v0}, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->setTitleText(I)V

    .line 71
    sget v0, Lcom/squareup/onboarding/flow/R$string;->underwriting_error_explanation:I

    invoke-virtual {p1, v0}, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->setMessageText(I)V

    .line 72
    sget v0, Lcom/squareup/common/strings/R$string;->done:I

    invoke-virtual {p1, v0}, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->setTopButton(I)V

    .line 73
    invoke-virtual {p1, v2}, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->setBottomButton(I)V

    .line 74
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p1, v0, v3}, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;F)V

    :goto_0
    return-void

    .line 58
    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "OnboardingErrorScreen has no activation state"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
