.class public final Lcom/squareup/ui/orderhub/notificationcenter/UnknownOrdersNotificationsSource;
.super Ljava/lang/Object;
.source "UnknownOrdersNotificationsSource.kt"

# interfaces
.implements Lcom/squareup/notificationcenterdata/NotificationsSource;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nUnknownOrdersNotificationsSource.kt\nKotlin\n*S Kotlin\n*F\n+ 1 UnknownOrdersNotificationsSource.kt\ncom/squareup/ui/orderhub/notificationcenter/UnknownOrdersNotificationsSource\n*L\n1#1,132:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\\\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B/\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0018\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0002J\u0010\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u000f\u001a\u00020\u0010H\u0002J\u000e\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00170\u0016H\u0016J4\u0010\u0018\u001a&\u0012\u000c\u0012\n \u0019*\u0004\u0018\u00010\u00120\u0012 \u0019*\u0012\u0012\u000c\u0012\n \u0019*\u0004\u0018\u00010\u00120\u0012\u0018\u00010\u00160\u00162\u0006\u0010\u001a\u001a\u00020\u0014H\u0002J\u0010\u0010\u001b\u001a\u00020\u00142\u0006\u0010\u000f\u001a\u00020\u0010H\u0002J\u0010\u0010\u001c\u001a\u00020\u00142\u0006\u0010\u000f\u001a\u00020\u0010H\u0002J\u001e\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\u00170\u0016*\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020 0\u001f0\u001eH\u0002R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/notificationcenter/UnknownOrdersNotificationsSource;",
        "Lcom/squareup/notificationcenterdata/NotificationsSource;",
        "orderRepository",
        "Lcom/squareup/ordermanagerdata/OrderRepository;",
        "resources",
        "Landroid/content/res/Resources;",
        "currentTime",
        "Lcom/squareup/time/CurrentTime;",
        "localNotificationAnalytics",
        "Lcom/squareup/notificationcenterdata/logging/LocalNotificationAnalytics;",
        "notificationStateStore",
        "Lcom/squareup/notificationcenterdata/NotificationStateStore;",
        "(Lcom/squareup/ordermanagerdata/OrderRepository;Landroid/content/res/Resources;Lcom/squareup/time/CurrentTime;Lcom/squareup/notificationcenterdata/logging/LocalNotificationAnalytics;Lcom/squareup/notificationcenterdata/NotificationStateStore;)V",
        "createUnknownOrdersNotification",
        "Lcom/squareup/notificationcenterdata/Notification;",
        "count",
        "",
        "state",
        "Lcom/squareup/notificationcenterdata/Notification$State;",
        "id",
        "",
        "notifications",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/notificationcenterdata/NotificationsSource$Result;",
        "orderState",
        "kotlin.jvm.PlatformType",
        "notificationId",
        "unknownOrdersNotificationContent",
        "unknownOrdersNotificationTitle",
        "toResult",
        "Lcom/squareup/ordermanagerdata/ResultState;",
        "",
        "Lcom/squareup/orders/model/Order;",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final currentTime:Lcom/squareup/time/CurrentTime;

.field private final localNotificationAnalytics:Lcom/squareup/notificationcenterdata/logging/LocalNotificationAnalytics;

.field private final notificationStateStore:Lcom/squareup/notificationcenterdata/NotificationStateStore;

.field private final orderRepository:Lcom/squareup/ordermanagerdata/OrderRepository;

.field private final resources:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Lcom/squareup/ordermanagerdata/OrderRepository;Landroid/content/res/Resources;Lcom/squareup/time/CurrentTime;Lcom/squareup/notificationcenterdata/logging/LocalNotificationAnalytics;Lcom/squareup/notificationcenterdata/NotificationStateStore;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "orderRepository"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "resources"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currentTime"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "localNotificationAnalytics"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "notificationStateStore"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/notificationcenter/UnknownOrdersNotificationsSource;->orderRepository:Lcom/squareup/ordermanagerdata/OrderRepository;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/notificationcenter/UnknownOrdersNotificationsSource;->resources:Landroid/content/res/Resources;

    iput-object p3, p0, Lcom/squareup/ui/orderhub/notificationcenter/UnknownOrdersNotificationsSource;->currentTime:Lcom/squareup/time/CurrentTime;

    iput-object p4, p0, Lcom/squareup/ui/orderhub/notificationcenter/UnknownOrdersNotificationsSource;->localNotificationAnalytics:Lcom/squareup/notificationcenterdata/logging/LocalNotificationAnalytics;

    iput-object p5, p0, Lcom/squareup/ui/orderhub/notificationcenter/UnknownOrdersNotificationsSource;->notificationStateStore:Lcom/squareup/notificationcenterdata/NotificationStateStore;

    return-void
.end method

.method public static final synthetic access$createUnknownOrdersNotification(Lcom/squareup/ui/orderhub/notificationcenter/UnknownOrdersNotificationsSource;ILcom/squareup/notificationcenterdata/Notification$State;)Lcom/squareup/notificationcenterdata/Notification;
    .locals 0

    .line 34
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/orderhub/notificationcenter/UnknownOrdersNotificationsSource;->createUnknownOrdersNotification(ILcom/squareup/notificationcenterdata/Notification$State;)Lcom/squareup/notificationcenterdata/Notification;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$toResult(Lcom/squareup/ui/orderhub/notificationcenter/UnknownOrdersNotificationsSource;Lcom/squareup/ordermanagerdata/ResultState;)Lio/reactivex/Observable;
    .locals 0

    .line 34
    invoke-direct {p0, p1}, Lcom/squareup/ui/orderhub/notificationcenter/UnknownOrdersNotificationsSource;->toResult(Lcom/squareup/ordermanagerdata/ResultState;)Lio/reactivex/Observable;

    move-result-object p0

    return-object p0
.end method

.method private final createUnknownOrdersNotification(ILcom/squareup/notificationcenterdata/Notification$State;)Lcom/squareup/notificationcenterdata/Notification;
    .locals 13

    .line 87
    new-instance v12, Lcom/squareup/notificationcenterdata/Notification;

    .line 88
    invoke-direct {p0, p1}, Lcom/squareup/ui/orderhub/notificationcenter/UnknownOrdersNotificationsSource;->id(I)Ljava/lang/String;

    move-result-object v1

    .line 90
    invoke-direct {p0, p1}, Lcom/squareup/ui/orderhub/notificationcenter/UnknownOrdersNotificationsSource;->unknownOrdersNotificationTitle(I)Ljava/lang/String;

    move-result-object v3

    .line 91
    invoke-direct {p0, p1}, Lcom/squareup/ui/orderhub/notificationcenter/UnknownOrdersNotificationsSource;->unknownOrdersNotificationContent(I)Ljava/lang/String;

    move-result-object v4

    .line 93
    sget-object p1, Lcom/squareup/notificationcenterdata/Notification$Priority$Important;->INSTANCE:Lcom/squareup/notificationcenterdata/Notification$Priority$Important;

    move-object v6, p1

    check-cast v6, Lcom/squareup/notificationcenterdata/Notification$Priority;

    .line 94
    sget-object p1, Lcom/squareup/notificationcenterdata/Notification$DisplayType$Normal;->INSTANCE:Lcom/squareup/notificationcenterdata/Notification$DisplayType$Normal;

    move-object v7, p1

    check-cast v7, Lcom/squareup/notificationcenterdata/Notification$DisplayType;

    .line 95
    sget-object v9, Lcom/squareup/notificationcenterdata/Notification$Source;->LOCAL:Lcom/squareup/notificationcenterdata/Notification$Source;

    .line 96
    new-instance p1, Lcom/squareup/notificationcenterdata/Notification$Destination$SupportedClientAction;

    .line 97
    new-instance v0, Lcom/squareup/protos/client/ClientAction$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/ClientAction$Builder;-><init>()V

    .line 98
    new-instance v2, Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet$Builder;-><init>()V

    invoke-virtual {v2}, Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet$Builder;->build()Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/protos/client/ClientAction$Builder;->view_orders_applet(Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;)Lcom/squareup/protos/client/ClientAction$Builder;

    move-result-object v0

    .line 99
    sget-object v2, Lcom/squareup/protos/client/ClientAction$FallbackBehavior;->NONE:Lcom/squareup/protos/client/ClientAction$FallbackBehavior;

    invoke-virtual {v0, v2}, Lcom/squareup/protos/client/ClientAction$Builder;->fallback_behavior(Lcom/squareup/protos/client/ClientAction$FallbackBehavior;)Lcom/squareup/protos/client/ClientAction$Builder;

    move-result-object v0

    .line 100
    invoke-virtual {v0}, Lcom/squareup/protos/client/ClientAction$Builder;->build()Lcom/squareup/protos/client/ClientAction;

    move-result-object v0

    const-string v2, "ClientAction.Builder()\n \u2026\n                .build()"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 96
    invoke-direct {p1, v0}, Lcom/squareup/notificationcenterdata/Notification$Destination$SupportedClientAction;-><init>(Lcom/squareup/protos/client/ClientAction;)V

    move-object v8, p1

    check-cast v8, Lcom/squareup/notificationcenterdata/Notification$Destination;

    .line 102
    iget-object p1, p0, Lcom/squareup/ui/orderhub/notificationcenter/UnknownOrdersNotificationsSource;->currentTime:Lcom/squareup/time/CurrentTime;

    invoke-interface {p1}, Lcom/squareup/time/CurrentTime;->instant()Lorg/threeten/bp/Instant;

    move-result-object v10

    .line 103
    sget-object p1, Lcom/squareup/communications/Message$Type$AlertToCompleteOrders;->INSTANCE:Lcom/squareup/communications/Message$Type$AlertToCompleteOrders;

    move-object v11, p1

    check-cast v11, Lcom/squareup/communications/Message$Type;

    const-string v2, ""

    move-object v0, v12

    move-object v5, p2

    .line 87
    invoke-direct/range {v0 .. v11}, Lcom/squareup/notificationcenterdata/Notification;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/notificationcenterdata/Notification$State;Lcom/squareup/notificationcenterdata/Notification$Priority;Lcom/squareup/notificationcenterdata/Notification$DisplayType;Lcom/squareup/notificationcenterdata/Notification$Destination;Lcom/squareup/notificationcenterdata/Notification$Source;Lorg/threeten/bp/Instant;Lcom/squareup/communications/Message$Type;)V

    .line 106
    iget-object p1, p0, Lcom/squareup/ui/orderhub/notificationcenter/UnknownOrdersNotificationsSource;->localNotificationAnalytics:Lcom/squareup/notificationcenterdata/logging/LocalNotificationAnalytics;

    invoke-interface {p1, v12}, Lcom/squareup/notificationcenterdata/logging/LocalNotificationAnalytics;->logLocalNotificationCreated(Lcom/squareup/notificationcenterdata/Notification;)V

    return-object v12
.end method

.method private final id(I)Ljava/lang/String;
    .locals 2

    .line 42
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "unknown-orders-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private final orderState(Ljava/lang/String;)Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/notificationcenterdata/Notification$State;",
            ">;"
        }
    .end annotation

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/orderhub/notificationcenter/UnknownOrdersNotificationsSource;->notificationStateStore:Lcom/squareup/notificationcenterdata/NotificationStateStore;

    .line 45
    sget-object v1, Lcom/squareup/notificationcenterdata/Notification$Source;->LOCAL:Lcom/squareup/notificationcenterdata/Notification$Source;

    invoke-interface {v0, p1, v1}, Lcom/squareup/notificationcenterdata/NotificationStateStore;->getNotificationState(Ljava/lang/String;Lcom/squareup/notificationcenterdata/Notification$Source;)Lio/reactivex/Observable;

    move-result-object p1

    .line 46
    sget-object v0, Lcom/squareup/notificationcenterdata/Notification$State;->UNREAD:Lcom/squareup/notificationcenterdata/Notification$State;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method private final toResult(Lcom/squareup/ordermanagerdata/ResultState;)Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ordermanagerdata/ResultState<",
            "+",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;>;)",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/notificationcenterdata/NotificationsSource$Result;",
            ">;"
        }
    .end annotation

    .line 64
    instance-of v0, p1, Lcom/squareup/ordermanagerdata/ResultState$Success;

    if-eqz v0, :cond_0

    .line 65
    check-cast p1, Lcom/squareup/ordermanagerdata/ResultState$Success;

    invoke-virtual {p1}, Lcom/squareup/ordermanagerdata/ResultState$Success;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    .line 66
    invoke-direct {p0, p1}, Lcom/squareup/ui/orderhub/notificationcenter/UnknownOrdersNotificationsSource;->id(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/ui/orderhub/notificationcenter/UnknownOrdersNotificationsSource;->orderState(Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object v0

    .line 67
    new-instance v1, Lcom/squareup/ui/orderhub/notificationcenter/UnknownOrdersNotificationsSource$toResult$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/orderhub/notificationcenter/UnknownOrdersNotificationsSource$toResult$1;-><init>(Lcom/squareup/ui/orderhub/notificationcenter/UnknownOrdersNotificationsSource;I)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    const-string v0, "orderState(notificationI\u2026          )\n            }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 77
    :cond_0
    instance-of p1, p1, Lcom/squareup/ordermanagerdata/ResultState$Failure;

    if-eqz p1, :cond_1

    sget-object p1, Lcom/squareup/notificationcenterdata/NotificationsSource$Result$Failure;->INSTANCE:Lcom/squareup/notificationcenterdata/NotificationsSource$Result$Failure;

    invoke-static {p1}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object p1

    const-string v0, "Observable.just(Failure)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final unknownOrdersNotificationContent(I)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x1

    if-lt p1, v0, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_2

    if-le p1, v0, :cond_1

    .line 116
    iget-object p1, p0, Lcom/squareup/ui/orderhub/notificationcenter/UnknownOrdersNotificationsSource;->resources:Landroid/content/res/Resources;

    sget v0, Lcom/squareup/orderhub/applet/R$string;->orderhub_new_orders_notification_content_plural:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v0, "resources.getString(R.st\u2026ification_content_plural)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 117
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/orderhub/notificationcenter/UnknownOrdersNotificationsSource;->resources:Landroid/content/res/Resources;

    sget v0, Lcom/squareup/orderhub/applet/R$string;->orderhub_new_orders_notification_content_singular:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v0, "resources.getString(R.st\u2026ication_content_singular)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_1
    return-object p1

    .line 114
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Failed requirement."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final unknownOrdersNotificationTitle(I)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x1

    if-lt p1, v0, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_2

    if-le p1, v0, :cond_1

    .line 127
    iget-object p1, p0, Lcom/squareup/ui/orderhub/notificationcenter/UnknownOrdersNotificationsSource;->resources:Landroid/content/res/Resources;

    sget v0, Lcom/squareup/orderhub/applet/R$string;->orderhub_new_orders_notification_title_plural:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v0, "resources.getString(R.st\u2026otification_title_plural)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 128
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/orderhub/notificationcenter/UnknownOrdersNotificationsSource;->resources:Landroid/content/res/Resources;

    sget v0, Lcom/squareup/orderhub/applet/R$string;->orderhub_new_orders_notification_title_singular:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v0, "resources.getString(R.st\u2026ification_title_singular)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_1
    return-object p1

    .line 125
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Failed requirement."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method


# virtual methods
.method public notifications()Lio/reactivex/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/notificationcenterdata/NotificationsSource$Result;",
            ">;"
        }
    .end annotation

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/orderhub/notificationcenter/UnknownOrdersNotificationsSource;->orderRepository:Lcom/squareup/ordermanagerdata/OrderRepository;

    .line 55
    invoke-interface {v0}, Lcom/squareup/ordermanagerdata/OrderRepository;->unknownOrders()Lio/reactivex/Observable;

    move-result-object v0

    .line 56
    new-instance v1, Lcom/squareup/ui/orderhub/notificationcenter/UnknownOrdersNotificationsSource$notifications$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/orderhub/notificationcenter/UnknownOrdersNotificationsSource$notifications$1;-><init>(Lcom/squareup/ui/orderhub/notificationcenter/UnknownOrdersNotificationsSource;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 57
    new-instance v1, Lcom/squareup/notificationcenterdata/NotificationsSource$Result$Success;

    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/notificationcenterdata/NotificationsSource$Result$Success;-><init>(Ljava/util/List;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "orderRepository\n        \u2026ith(Success(emptyList()))"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
