.class public final Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "BillHistoryDetailScreen.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailView;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B%\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008\u00a2\u0006\u0002\u0010\nJ\u0018\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00022\u0006\u0010\u0012\u001a\u00020\u0013H\u0002J\r\u0010\u0014\u001a\u00020\u0015H\u0000\u00a2\u0006\u0002\u0008\u0016J\u0012\u0010\u0017\u001a\u00020\u00102\u0008\u0010\u0018\u001a\u0004\u0018\u00010\u0019H\u0014J\u0012\u0010\u001a\u001a\u00020\u00102\u0008\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0014R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;",
        "Lmortar/ViewPresenter;",
        "Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailView;",
        "res",
        "Lcom/squareup/util/Res;",
        "controller",
        "Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Controller;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "(Lcom/squareup/util/Res;Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Controller;Lcom/squareup/text/Formatter;)V",
        "billToken",
        "",
        "screen",
        "Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen;",
        "handleBillLoadedSuccess",
        "",
        "view",
        "loadedBillState",
        "Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState$Loaded;",
        "onBackPressed",
        "",
        "onBackPressed$orderhub_applet_release",
        "onEnterScope",
        "mortarScope",
        "Lmortar/MortarScope;",
        "onLoad",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private billToken:Ljava/lang/String;

.field private final controller:Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Controller;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private screen:Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Controller;Lcom/squareup/text/Formatter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Controller;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "controller"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;->controller:Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Controller;

    iput-object p3, p0, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    return-void
.end method

.method public static final synthetic access$getBillToken$p(Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;)Ljava/lang/String;
    .locals 1

    .line 88
    iget-object p0, p0, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;->billToken:Ljava/lang/String;

    if-nez p0, :cond_0

    const-string v0, "billToken"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getController$p(Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;)Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Controller;
    .locals 0

    .line 88
    iget-object p0, p0, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;->controller:Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Controller;

    return-object p0
.end method

.method public static final synthetic access$getView(Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;)Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailView;
    .locals 0

    .line 88
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailView;

    return-object p0
.end method

.method public static final synthetic access$handleBillLoadedSuccess(Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailView;Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState$Loaded;)V
    .locals 0

    .line 88
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;->handleBillLoadedSuccess(Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailView;Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState$Loaded;)V

    return-void
.end method

.method public static final synthetic access$setBillToken$p(Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;Ljava/lang/String;)V
    .locals 0

    .line 88
    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;->billToken:Ljava/lang/String;

    return-void
.end method

.method private final handleBillLoadedSuccess(Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailView;Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState$Loaded;)V
    .locals 3

    .line 166
    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState$Loaded;->getBill()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object p2

    .line 167
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;->res:Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-static {p2, v0, v1}, Lcom/squareup/billhistory/Bills;->formatTitleOf(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 168
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 170
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailView;->getActionBar$orderhub_applet_release()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v1

    .line 171
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v2

    .line 172
    invoke-virtual {v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v2

    .line 173
    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v2, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 174
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 176
    invoke-virtual {p1, p2}, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailView;->show$orderhub_applet_release(Lcom/squareup/billhistory/model/BillHistory;)V

    return-void
.end method


# virtual methods
.method public final onBackPressed$orderhub_applet_release()Z
    .locals 1

    .line 158
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;->controller:Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Controller;

    invoke-interface {v0}, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Controller;->closeBillHistory()V

    const/4 v0, 0x1

    return v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 98
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 99
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    const-string v0, "RegisterTreeKey.get(mortarScope)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen;

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;->screen:Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen;

    .line 100
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;->screen:Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen;

    if-nez p1, :cond_0

    const-string v0, "screen"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {p1}, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen;->access$getBillToken$p(Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;->billToken:Ljava/lang/String;

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 3

    .line 104
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 106
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailView;

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailView;->getActionBar$orderhub_applet_release()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object p1

    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 107
    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphNoText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 108
    new-instance v1, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter$onLoad$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter$onLoad$1;-><init>(Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 109
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 112
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailView;

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const-string v1, "TRUE"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailView;->showProgress$orderhub_applet_release(Z)V

    .line 117
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailView;

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/view/View;

    new-instance v1, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter$onLoad$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter$onLoad$2;-><init>(Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 129
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailView;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/view/View;

    new-instance v1, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter$onLoad$3;

    invoke-direct {v1, p0}, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter$onLoad$3;-><init>(Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 134
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailView;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/view/View;

    new-instance v1, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter$onLoad$4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter$onLoad$4;-><init>(Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 140
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailView;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/view/View;

    new-instance v1, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter$onLoad$5;

    invoke-direct {v1, p0}, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter$onLoad$5;-><init>(Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 148
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailView;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/view/View;

    new-instance v0, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter$onLoad$6;

    invoke-direct {v0, p0}, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter$onLoad$6;-><init>(Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
