.class final Lcom/squareup/ui/orderhub/master/Filter$Companion$DEFAULT$2;
.super Lkotlin/jvm/internal/Lambda;
.source "Filter.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/orderhub/master/Filter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lcom/squareup/ui/orderhub/master/Filter$Status;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/ui/orderhub/master/Filter$Status;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/orderhub/master/Filter$Companion$DEFAULT$2;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/orderhub/master/Filter$Companion$DEFAULT$2;

    invoke-direct {v0}, Lcom/squareup/ui/orderhub/master/Filter$Companion$DEFAULT$2;-><init>()V

    sput-object v0, Lcom/squareup/ui/orderhub/master/Filter$Companion$DEFAULT$2;->INSTANCE:Lcom/squareup/ui/orderhub/master/Filter$Companion$DEFAULT$2;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()Lcom/squareup/ui/orderhub/master/Filter$Status;
    .locals 2

    .line 121
    new-instance v0, Lcom/squareup/ui/orderhub/master/Filter$Status;

    sget-object v1, Lcom/squareup/protos/client/orders/OrderGroup;->ACTIVE:Lcom/squareup/protos/client/orders/OrderGroup;

    invoke-direct {v0, v1}, Lcom/squareup/ui/orderhub/master/Filter$Status;-><init>(Lcom/squareup/protos/client/orders/OrderGroup;)V

    return-object v0
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 116
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/master/Filter$Companion$DEFAULT$2;->invoke()Lcom/squareup/ui/orderhub/master/Filter$Status;

    move-result-object v0

    return-object v0
.end method
