.class public final Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledStateKt;
.super Ljava/lang/Object;
.source "OrderMarkCanceledState.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderMarkCanceledState.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderMarkCanceledState.kt\ncom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledStateKt\n*L\n1#1,78:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\"\u0018\u0010\u0000\u001a\u00020\u0001*\u00020\u00028@X\u0080\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0004\"<\u0010\u0005\u001a&\u0012\u0004\u0012\u00020\u0007\u0012\u0018\u0012\u0016\u0012\u0008\u0012\u00060\u0007j\u0002`\u0008\u0012\u0004\u0012\u00020\t0\u0006j\u0002`\n0\u0006j\u0002`\u000b*\u00020\u00028@X\u0080\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\r\u00a8\u0006\u000e"
    }
    d2 = {
        "nonEmptyCancellationReason",
        "Lcom/squareup/ordermanagerdata/CancellationReason;",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;",
        "getNonEmptyCancellationReason",
        "(Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;)Lcom/squareup/ordermanagerdata/CancellationReason;",
        "nonEmptySelectedLineItems",
        "",
        "",
        "Lcom/squareup/ui/orderhub/order/itemselection/LineItemRowIdentifier;",
        "Lcom/squareup/orders/model/Order$LineItem;",
        "Lcom/squareup/ui/orderhub/order/itemselection/LineItemWithQuantityByIdentifier;",
        "Lcom/squareup/ui/orderhub/order/itemselection/LineItemSelectionsByUid;",
        "getNonEmptySelectedLineItems",
        "(Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;)Ljava/util/Map;",
        "orderhub-applet_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final getNonEmptyCancellationReason(Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;)Lcom/squareup/ordermanagerdata/CancellationReason;
    .locals 1

    const-string v0, "$this$nonEmptyCancellationReason"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->getCancellationReason()Lcom/squareup/ordermanagerdata/CancellationReason;

    move-result-object p0

    if-eqz p0, :cond_0

    return-object p0

    .line 45
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "At this point the Order should have selected a Cancellation Reason"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0
.end method

.method public static final getNonEmptySelectedLineItems(Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;>;"
        }
    .end annotation

    const-string v0, "$this$nonEmptySelectedLineItems"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->getSelectedLineItems()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelectionsKt;->getAvailableLineItemsToRowIdentifiers(Lcom/squareup/orders/model/Order;)Ljava/util/Map;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    return-object v0

    :cond_1
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "Required value was null."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0
.end method
