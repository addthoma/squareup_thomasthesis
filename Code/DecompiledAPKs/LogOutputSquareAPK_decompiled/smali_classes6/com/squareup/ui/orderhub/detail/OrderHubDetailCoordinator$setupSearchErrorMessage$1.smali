.class public final Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$setupSearchErrorMessage$1;
.super Lcom/squareup/ui/LinkSpan;
.source "OrderHubDetailCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->setupSearchErrorMessage()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006"
    }
    d2 = {
        "com/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$setupSearchErrorMessage$1",
        "Lcom/squareup/ui/LinkSpan;",
        "onClick",
        "",
        "widget",
        "Landroid/view/View;",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 581
    iput-object p1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$setupSearchErrorMessage$1;->this$0:Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;

    invoke-direct {p0, p2}, Lcom/squareup/ui/LinkSpan;-><init>(I)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1

    const-string v0, "widget"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 583
    iget-object p1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$setupSearchErrorMessage$1;->this$0:Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->access$getScreen$p(Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;)Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen;->getSearchRendering()Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->getOnClickedRetrySearch()Lkotlin/jvm/functions/Function0;

    move-result-object p1

    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    return-void
.end method
