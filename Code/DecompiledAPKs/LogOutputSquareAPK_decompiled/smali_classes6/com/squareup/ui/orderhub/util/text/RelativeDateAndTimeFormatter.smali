.class public final Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;
.super Ljava/lang/Object;
.source "RelativeDateAndTimeFormatter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u000b\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 -2\u00020\u0001:\u0001-B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0016\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cJ\u0018\u0010\r\u001a\u00020\u00082\u0008\u0010\u000e\u001a\u0004\u0018\u00010\u00082\u0006\u0010\u000b\u001a\u00020\u000cJ \u0010\u000f\u001a\u00020\u00082\u0006\u0010\u000e\u001a\u00020\u00082\u0006\u0010\u0010\u001a\u00020\u00082\u0006\u0010\u0011\u001a\u00020\u0012H\u0002J\u0018\u0010\u0013\u001a\u00020\u00082\u0006\u0010\u0014\u001a\u00020\u000c2\u0006\u0010\u0015\u001a\u00020\u000cH\u0002J \u0010\u0016\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\u0011\u001a\u00020\u0012H\u0002J\u001d\u0010\u0017\u001a\u00020\u00082\u0006\u0010\u0018\u001a\u00020\u000c2\u0006\u0010\u000b\u001a\u00020\u000cH\u0000\u00a2\u0006\u0002\u0008\u0019J\u0018\u0010\u001a\u001a\u00020\u00082\u0006\u0010\u0018\u001a\u00020\u000c2\u0006\u0010\u000b\u001a\u00020\u000cH\u0002J \u0010\u001b\u001a\u00020\u00082\u0006\u0010\u001c\u001a\u00020\u000c2\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\u001d\u001a\u00020\u001eH\u0002J\u0018\u0010\u001f\u001a\u00020\u00082\u0006\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020#H\u0002J\u0018\u0010$\u001a\u00020\u00082\u0006\u0010\u0014\u001a\u00020\u000c2\u0006\u0010\u0015\u001a\u00020\u000cH\u0002J\u001a\u0010%\u001a\u0004\u0018\u00010\u00082\u0006\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020#H\u0002J \u0010&\u001a\u00020\u00082\u0006\u0010\u0014\u001a\u00020\u000c2\u0006\u0010\u0015\u001a\u00020\u000c2\u0006\u0010\'\u001a\u00020!H\u0002J\u0018\u0010(\u001a\u00020\u00082\u0006\u0010)\u001a\u00020!2\u0006\u0010*\u001a\u00020!H\u0002J\u0014\u0010+\u001a\u00020\u001e*\u00020,2\u0006\u0010\u000b\u001a\u00020,H\u0002R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006."
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "dateTimeFormatter",
        "Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;",
        "(Lcom/squareup/util/Res;Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;)V",
        "formatSubtitle",
        "",
        "order",
        "Lcom/squareup/orders/model/Order;",
        "now",
        "Lorg/threeten/bp/ZonedDateTime;",
        "formatTitle",
        "dateTime",
        "getCanceledCompletedAtLabel",
        "actionWord",
        "formatter",
        "Lorg/threeten/bp/format/DateTimeFormatter;",
        "getDaysLabel",
        "startDateTime",
        "endDateTime",
        "getFulfillmentPlacedAtLabel",
        "getHoursAndMinutesLabel",
        "zonedDateTime",
        "getHoursAndMinutesLabel$orderhub_applet_release",
        "getLabelOutsideOneDay",
        "getLabelWithinOneDay",
        "zoneDateTime",
        "getHoursMinsLabel",
        "",
        "getLocalizedUnitOfTimeOutsideOneDay",
        "numUnit",
        "",
        "unit",
        "Lorg/threeten/bp/temporal/ChronoUnit;",
        "getNumberOfUnitsLabel",
        "getOneOfNextUnitUpLabel",
        "getWeeksLabel",
        "weeksBetween",
        "getYearsLabel",
        "yearsBetween",
        "months",
        "isWithinOneDay",
        "Lorg/threeten/bp/LocalDate;",
        "Companion",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter$Companion;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final DAYS_TO_ONE_WEEK:I = 0x7

.field public static final MONTHS_TO_ONE_YEAR:I = 0xc

.field public static final WEEKS_TO_ONE_MONTH:I = 0x4

.field public static final WEEKS_TO_ONE_YEAR:I = 0x34


# instance fields
.field private final dateTimeFormatter:Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;->Companion:Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dateTimeFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;->dateTimeFormatter:Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;

    return-void
.end method

.method private final getCanceledCompletedAtLabel(Ljava/lang/String;Ljava/lang/String;Lorg/threeten/bp/format/DateTimeFormatter;)Ljava/lang/String;
    .locals 1

    .line 110
    iget-object v0, p0, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;->dateTimeFormatter:Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;->parse(Ljava/lang/String;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object p1

    invoke-virtual {p1, p3}, Lorg/threeten/bp/ZonedDateTime;->format(Lorg/threeten/bp/format/DateTimeFormatter;)Ljava/lang/String;

    move-result-object p1

    .line 111
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 p2, 0x20

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private final getDaysLabel(Lorg/threeten/bp/ZonedDateTime;Lorg/threeten/bp/ZonedDateTime;)Ljava/lang/String;
    .locals 1

    .line 304
    invoke-virtual {p1}, Lorg/threeten/bp/ZonedDateTime;->toLocalDate()Lorg/threeten/bp/LocalDate;

    move-result-object p1

    .line 305
    invoke-virtual {p2}, Lorg/threeten/bp/ZonedDateTime;->toLocalDate()Lorg/threeten/bp/LocalDate;

    move-result-object p2

    check-cast p2, Lorg/threeten/bp/temporal/Temporal;

    sget-object v0, Lorg/threeten/bp/temporal/ChronoUnit;->DAYS:Lorg/threeten/bp/temporal/ChronoUnit;

    check-cast v0, Lorg/threeten/bp/temporal/TemporalUnit;

    invoke-virtual {p1, p2, v0}, Lorg/threeten/bp/LocalDate;->until(Lorg/threeten/bp/temporal/Temporal;Lorg/threeten/bp/temporal/TemporalUnit;)J

    move-result-wide p1

    .line 306
    sget-object v0, Lorg/threeten/bp/temporal/ChronoUnit;->DAYS:Lorg/threeten/bp/temporal/ChronoUnit;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;->getOneOfNextUnitUpLabel(JLorg/threeten/bp/temporal/ChronoUnit;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lorg/threeten/bp/temporal/ChronoUnit;->DAYS:Lorg/threeten/bp/temporal/ChronoUnit;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;->getLocalizedUnitOfTimeOutsideOneDay(JLorg/threeten/bp/temporal/ChronoUnit;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method private final getFulfillmentPlacedAtLabel(Lcom/squareup/orders/model/Order;Lorg/threeten/bp/ZonedDateTime;Lorg/threeten/bp/format/DateTimeFormatter;)Ljava/lang/String;
    .locals 4

    .line 126
    iget-object v0, p0, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;->dateTimeFormatter:Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;

    invoke-static {p1}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getPlacedAtDate(Lcom/squareup/orders/model/Order;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;->parse(Ljava/lang/String;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    .line 128
    invoke-virtual {v0}, Lorg/threeten/bp/ZonedDateTime;->toLocalDate()Lorg/threeten/bp/LocalDate;

    move-result-object v1

    const-string v2, "zoneDateTime.toLocalDate()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Lorg/threeten/bp/ZonedDateTime;->toLocalDate()Lorg/threeten/bp/LocalDate;

    move-result-object v2

    const-string v3, "now.toLocalDate()"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v1, v2}, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;->isWithinOneDay(Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;)Z

    move-result v1

    const-string v2, "date_time"

    if-eqz v1, :cond_0

    const/4 p3, 0x0

    .line 130
    invoke-direct {p0, v0, p2, p3}, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;->getLabelWithinOneDay(Lorg/threeten/bp/ZonedDateTime;Lorg/threeten/bp/ZonedDateTime;Z)Ljava/lang/String;

    move-result-object p2

    .line 131
    iget-object p3, p0, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;->res:Lcom/squareup/util/Res;

    invoke-static {p1}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getDisplayState(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/OrderDisplayStateData;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/ui/orderhub/util/proto/OrderDisplayStateDatasKt;->getUpdatedAtPatternRes(Lcom/squareup/protos/client/orders/OrderDisplayStateData;)I

    move-result p1

    invoke-interface {p3, p1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 132
    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p1, v2, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 133
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 134
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 136
    :cond_0
    iget-object p2, p0, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;->res:Lcom/squareup/util/Res;

    invoke-static {p1}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getDisplayState(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/OrderDisplayStateData;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/ui/orderhub/util/proto/OrderDisplayStateDatasKt;->getUpdatedAtPatternRes(Lcom/squareup/protos/client/orders/OrderDisplayStateData;)I

    move-result p1

    invoke-interface {p2, p1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 137
    invoke-virtual {v0, p3}, Lorg/threeten/bp/ZonedDateTime;->format(Lorg/threeten/bp/format/DateTimeFormatter;)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p1, v2, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 138
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 139
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private final getLabelOutsideOneDay(Lorg/threeten/bp/ZonedDateTime;Lorg/threeten/bp/ZonedDateTime;)Ljava/lang/String;
    .locals 1

    .line 221
    move-object v0, p2

    check-cast v0, Lorg/threeten/bp/chrono/ChronoZonedDateTime;

    invoke-virtual {p1, v0}, Lorg/threeten/bp/ZonedDateTime;->isBefore(Lorg/threeten/bp/chrono/ChronoZonedDateTime;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 222
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;->getNumberOfUnitsLabel(Lorg/threeten/bp/ZonedDateTime;Lorg/threeten/bp/ZonedDateTime;)Ljava/lang/String;

    move-result-object p1

    .line 223
    iget-object p2, p0, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_relative_date_ago:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 224
    check-cast p1, Ljava/lang/CharSequence;

    const-string v0, "date_time"

    invoke-virtual {p2, v0, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 225
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 226
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 228
    :cond_0
    invoke-direct {p0, p2, p1}, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;->getNumberOfUnitsLabel(Lorg/threeten/bp/ZonedDateTime;Lorg/threeten/bp/ZonedDateTime;)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private final getLabelWithinOneDay(Lorg/threeten/bp/ZonedDateTime;Lorg/threeten/bp/ZonedDateTime;Z)Ljava/lang/String;
    .locals 4

    .line 162
    invoke-virtual {p2}, Lorg/threeten/bp/ZonedDateTime;->toLocalDate()Lorg/threeten/bp/LocalDate;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 163
    invoke-virtual {v0, v1, v2}, Lorg/threeten/bp/LocalDate;->plusDays(J)Lorg/threeten/bp/LocalDate;

    move-result-object v3

    .line 164
    invoke-virtual {v0, v1, v2}, Lorg/threeten/bp/LocalDate;->minusDays(J)Lorg/threeten/bp/LocalDate;

    move-result-object v1

    .line 166
    invoke-virtual {p1}, Lorg/threeten/bp/ZonedDateTime;->toLocalDate()Lorg/threeten/bp/LocalDate;

    move-result-object v2

    .line 167
    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_relative_date_yesterday:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 168
    :cond_0
    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz p3, :cond_1

    .line 170
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;->getHoursAndMinutesLabel$orderhub_applet_release(Lorg/threeten/bp/ZonedDateTime;Lorg/threeten/bp/ZonedDateTime;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 172
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_relative_date_today:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 175
    :cond_2
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_3

    iget-object p1, p0, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_relative_date_tomorrow:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1

    .line 176
    :cond_3
    new-instance p3, Ljava/lang/IllegalStateException;

    .line 177
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Date "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, " was expected to be within one day of "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, ", but wasn\'t."

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 176
    invoke-direct {p3, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p3, Ljava/lang/Throwable;

    throw p3
.end method

.method private final getLocalizedUnitOfTimeOutsideOneDay(JLorg/threeten/bp/temporal/ChronoUnit;)Ljava/lang/String;
    .locals 4

    .line 355
    sget-object v0, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter$WhenMappings;->$EnumSwitchMapping$2:[I

    invoke-virtual {p3}, Lorg/threeten/bp/temporal/ChronoUnit;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    const-wide/16 v2, 0x1

    if-eq v0, v1, :cond_4

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    cmp-long v0, p1, v2

    if-eqz v0, :cond_0

    .line 383
    iget-object p3, p0, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_relative_date_days:I

    invoke-interface {p3, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p3

    .line 384
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    const-string p2, "num_days"

    invoke-virtual {p3, p2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 385
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 386
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 378
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 379
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expecting "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/16 p1, 0x20

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, " to be outside of 1 day range"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "from now, but was not."

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 378
    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 389
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Unit of time must be either Years, Months, Weeks, or Days, but was not."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :cond_2
    cmp-long p3, p1, v2

    if-nez p3, :cond_3

    .line 368
    iget-object p1, p0, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_relative_date_one_week:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 370
    :cond_3
    iget-object p3, p0, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_relative_date_weeks:I

    invoke-interface {p3, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p3

    .line 371
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    const-string p2, "num_weeks"

    invoke-virtual {p3, p2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 372
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 373
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_4
    cmp-long p3, p1, v2

    if-nez p3, :cond_5

    .line 358
    iget-object p1, p0, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_relative_date_one_year:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 360
    :cond_5
    iget-object p3, p0, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_relative_date_years:I

    invoke-interface {p3, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p3

    .line 361
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    const-string p2, "num_years"

    invoke-virtual {p3, p2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 362
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 363
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_1
    return-object p1
.end method

.method private final getNumberOfUnitsLabel(Lorg/threeten/bp/ZonedDateTime;Lorg/threeten/bp/ZonedDateTime;)Ljava/lang/String;
    .locals 8

    .line 254
    move-object v0, p2

    check-cast v0, Lorg/threeten/bp/temporal/Temporal;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoUnit;->MONTHS:Lorg/threeten/bp/temporal/ChronoUnit;

    check-cast v1, Lorg/threeten/bp/temporal/TemporalUnit;

    invoke-virtual {p1, v0, v1}, Lorg/threeten/bp/ZonedDateTime;->until(Lorg/threeten/bp/temporal/Temporal;Lorg/threeten/bp/temporal/TemporalUnit;)J

    move-result-wide v1

    .line 255
    sget-object v3, Lorg/threeten/bp/temporal/ChronoUnit;->YEARS:Lorg/threeten/bp/temporal/ChronoUnit;

    check-cast v3, Lorg/threeten/bp/temporal/TemporalUnit;

    invoke-virtual {p1, v0, v3}, Lorg/threeten/bp/ZonedDateTime;->until(Lorg/threeten/bp/temporal/Temporal;Lorg/threeten/bp/temporal/TemporalUnit;)J

    move-result-wide v3

    const-wide/16 v5, 0x1

    cmp-long v7, v3, v5

    if-ltz v7, :cond_0

    .line 257
    invoke-direct {p0, v3, v4, v1, v2}, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;->getYearsLabel(JJ)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 259
    :cond_0
    sget-object v1, Lorg/threeten/bp/temporal/ChronoUnit;->WEEKS:Lorg/threeten/bp/temporal/ChronoUnit;

    check-cast v1, Lorg/threeten/bp/temporal/TemporalUnit;

    invoke-virtual {p1, v0, v1}, Lorg/threeten/bp/ZonedDateTime;->until(Lorg/threeten/bp/temporal/Temporal;Lorg/threeten/bp/temporal/TemporalUnit;)J

    move-result-wide v0

    cmp-long v2, v0, v5

    if-ltz v2, :cond_1

    .line 261
    invoke-direct {p0, p1, p2, v0, v1}, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;->getWeeksLabel(Lorg/threeten/bp/ZonedDateTime;Lorg/threeten/bp/ZonedDateTime;J)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 263
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;->getDaysLabel(Lorg/threeten/bp/ZonedDateTime;Lorg/threeten/bp/ZonedDateTime;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private final getOneOfNextUnitUpLabel(JLorg/threeten/bp/temporal/ChronoUnit;)Ljava/lang/String;
    .locals 6

    .line 322
    sget-object v0, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p3}, Lorg/threeten/bp/temporal/ChronoUnit;->ordinal()I

    move-result p3

    aget p3, v0, p3

    const/4 v0, 0x1

    const-wide/16 v1, 0x1

    const/4 v3, 0x0

    if-eq p3, v0, :cond_2

    const/4 v0, 0x2

    if-ne p3, v0, :cond_1

    const/4 p3, 0x7

    int-to-long v4, p3

    cmp-long p3, p1, v4

    if-nez p3, :cond_0

    .line 331
    sget-object p1, Lorg/threeten/bp/temporal/ChronoUnit;->WEEKS:Lorg/threeten/bp/temporal/ChronoUnit;

    invoke-direct {p0, v1, v2, p1}, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;->getLocalizedUnitOfTimeOutsideOneDay(JLorg/threeten/bp/temporal/ChronoUnit;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    return-object v3

    .line 335
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "unit needs to be of type MONTHS, WEEKS, or DAYS"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :cond_2
    const/16 p3, 0x34

    int-to-long v4, p3

    cmp-long p3, p1, v4

    if-nez p3, :cond_3

    .line 325
    sget-object p1, Lorg/threeten/bp/temporal/ChronoUnit;->YEARS:Lorg/threeten/bp/temporal/ChronoUnit;

    invoke-direct {p0, v1, v2, p1}, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;->getLocalizedUnitOfTimeOutsideOneDay(JLorg/threeten/bp/temporal/ChronoUnit;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_3
    return-object v3
.end method

.method private final getWeeksLabel(Lorg/threeten/bp/ZonedDateTime;Lorg/threeten/bp/ZonedDateTime;J)Ljava/lang/String;
    .locals 3

    .line 286
    check-cast p2, Lorg/threeten/bp/temporal/Temporal;

    sget-object v0, Lorg/threeten/bp/temporal/ChronoUnit;->DAYS:Lorg/threeten/bp/temporal/ChronoUnit;

    check-cast v0, Lorg/threeten/bp/temporal/TemporalUnit;

    invoke-virtual {p1, p2, v0}, Lorg/threeten/bp/ZonedDateTime;->until(Lorg/threeten/bp/temporal/Temporal;Lorg/threeten/bp/temporal/TemporalUnit;)J

    move-result-wide p1

    long-to-double p1, p1

    const/4 v0, 0x7

    int-to-double v0, v0

    div-double/2addr p1, v0

    double-to-int v0, p1

    int-to-double v0, v0

    sub-double/2addr p1, v0

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    cmpl-double v2, p1, v0

    if-ltz v2, :cond_0

    const-wide/16 p1, 0x1

    add-long/2addr p3, p1

    .line 296
    :cond_0
    sget-object p1, Lorg/threeten/bp/temporal/ChronoUnit;->WEEKS:Lorg/threeten/bp/temporal/ChronoUnit;

    invoke-direct {p0, p3, p4, p1}, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;->getOneOfNextUnitUpLabel(JLorg/threeten/bp/temporal/ChronoUnit;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_1

    goto :goto_0

    .line 297
    :cond_1
    sget-object p1, Lorg/threeten/bp/temporal/ChronoUnit;->WEEKS:Lorg/threeten/bp/temporal/ChronoUnit;

    invoke-direct {p0, p3, p4, p1}, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;->getLocalizedUnitOfTimeOutsideOneDay(JLorg/threeten/bp/temporal/ChronoUnit;)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private final getYearsLabel(JJ)Ljava/lang/String;
    .locals 3

    long-to-double p3, p3

    const/16 v0, 0xc

    int-to-double v0, v0

    div-double/2addr p3, v0

    double-to-int v0, p3

    int-to-double v0, v0

    sub-double/2addr p3, v0

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    cmpl-double v2, p3, v0

    if-ltz v2, :cond_0

    const-wide/16 p3, 0x1

    add-long/2addr p1, p3

    .line 278
    :cond_0
    sget-object p3, Lorg/threeten/bp/temporal/ChronoUnit;->YEARS:Lorg/threeten/bp/temporal/ChronoUnit;

    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;->getLocalizedUnitOfTimeOutsideOneDay(JLorg/threeten/bp/temporal/ChronoUnit;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private final isWithinOneDay(Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;)Z
    .locals 3

    const-wide/16 v0, 0x2

    .line 410
    invoke-virtual {p2, v0, v1}, Lorg/threeten/bp/LocalDate;->minusDays(J)Lorg/threeten/bp/LocalDate;

    move-result-object v2

    check-cast v2, Lorg/threeten/bp/chrono/ChronoLocalDate;

    invoke-virtual {p1, v2}, Lorg/threeten/bp/LocalDate;->isAfter(Lorg/threeten/bp/chrono/ChronoLocalDate;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p2, v0, v1}, Lorg/threeten/bp/LocalDate;->plusDays(J)Lorg/threeten/bp/LocalDate;

    move-result-object p2

    check-cast p2, Lorg/threeten/bp/chrono/ChronoLocalDate;

    invoke-virtual {p1, p2}, Lorg/threeten/bp/LocalDate;->isBefore(Lorg/threeten/bp/chrono/ChronoLocalDate;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method


# virtual methods
.method public final formatSubtitle(Lcom/squareup/orders/model/Order;Lorg/threeten/bp/ZonedDateTime;)Ljava/lang/String;
    .locals 4

    const-string v0, "order"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "now"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_relative_date_format:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/threeten/bp/format/DateTimeFormatter;->ofPattern(Ljava/lang/String;)Lorg/threeten/bp/format/DateTimeFormatter;

    move-result-object v0

    .line 82
    iget-object v1, p1, Lcom/squareup/orders/model/Order;->state:Lcom/squareup/orders/model/Order$State;

    if-eqz v1, :cond_3

    sget-object v2, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$State;->ordinal()I

    move-result v1

    aget v1, v2, v1

    const/4 v2, 0x1

    const-string v3, "formatter"

    if-eq v1, v2, :cond_2

    const/4 p2, 0x2

    const-string v2, "order.closed_at"

    if-eq v1, p2, :cond_1

    const/4 p2, 0x3

    if-eq v1, p2, :cond_0

    const/4 p1, 0x4

    if-eq v1, p1, :cond_3

    .line 90
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 88
    :cond_0
    iget-object p1, p1, Lcom/squareup/orders/model/Order;->closed_at:Ljava/lang/String;

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_display_state_canceled:I

    invoke-interface {p2, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;->getCanceledCompletedAtLabel(Ljava/lang/String;Ljava/lang/String;Lorg/threeten/bp/format/DateTimeFormatter;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 85
    :cond_1
    iget-object p1, p1, Lcom/squareup/orders/model/Order;->closed_at:Ljava/lang/String;

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_display_state_completed:I

    invoke-interface {p2, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;->getCanceledCompletedAtLabel(Ljava/lang/String;Ljava/lang/String;Lorg/threeten/bp/format/DateTimeFormatter;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 83
    :cond_2
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;->getFulfillmentPlacedAtLabel(Lcom/squareup/orders/model/Order;Lorg/threeten/bp/ZonedDateTime;Lorg/threeten/bp/format/DateTimeFormatter;)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1

    .line 90
    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Invalid order state"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public final formatTitle(Ljava/lang/String;Lorg/threeten/bp/ZonedDateTime;)Ljava/lang/String;
    .locals 3

    const-string v0, "now"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-nez p1, :cond_0

    const-string p1, ""

    return-object p1

    .line 51
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;->dateTimeFormatter:Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;->parse(Ljava/lang/String;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object p1

    .line 52
    invoke-virtual {p1}, Lorg/threeten/bp/ZonedDateTime;->toLocalDate()Lorg/threeten/bp/LocalDate;

    move-result-object v0

    const-string v1, "zonedDateTime.toLocalDate()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Lorg/threeten/bp/ZonedDateTime;->toLocalDate()Lorg/threeten/bp/LocalDate;

    move-result-object v1

    const-string v2, "now.toLocalDate()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;->isWithinOneDay(Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 53
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;->getLabelWithinOneDay(Lorg/threeten/bp/ZonedDateTime;Lorg/threeten/bp/ZonedDateTime;Z)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 55
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;->getLabelOutsideOneDay(Lorg/threeten/bp/ZonedDateTime;Lorg/threeten/bp/ZonedDateTime;)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public final getHoursAndMinutesLabel$orderhub_applet_release(Lorg/threeten/bp/ZonedDateTime;Lorg/threeten/bp/ZonedDateTime;)Ljava/lang/String;
    .locals 10

    const-string v0, "zonedDateTime"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "now"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 195
    move-object v0, p2

    check-cast v0, Lorg/threeten/bp/chrono/ChronoZonedDateTime;

    invoke-virtual {p1, v0}, Lorg/threeten/bp/ZonedDateTime;->isBefore(Lorg/threeten/bp/chrono/ChronoZonedDateTime;)Z

    move-result v0

    const-string v1, "num_min"

    const-string v2, "num_hours"

    const/16 v3, 0x3c

    if-eqz v0, :cond_0

    .line 197
    check-cast p2, Lorg/threeten/bp/temporal/Temporal;

    sget-object v0, Lorg/threeten/bp/temporal/ChronoUnit;->HOURS:Lorg/threeten/bp/temporal/ChronoUnit;

    check-cast v0, Lorg/threeten/bp/temporal/TemporalUnit;

    invoke-virtual {p1, p2, v0}, Lorg/threeten/bp/ZonedDateTime;->until(Lorg/threeten/bp/temporal/Temporal;Lorg/threeten/bp/temporal/TemporalUnit;)J

    move-result-wide v4

    .line 198
    sget-object v0, Lorg/threeten/bp/temporal/ChronoUnit;->MINUTES:Lorg/threeten/bp/temporal/ChronoUnit;

    check-cast v0, Lorg/threeten/bp/temporal/TemporalUnit;

    invoke-virtual {p1, p2, v0}, Lorg/threeten/bp/ZonedDateTime;->until(Lorg/threeten/bp/temporal/Temporal;Lorg/threeten/bp/temporal/TemporalUnit;)J

    move-result-wide p1

    long-to-double p1, p1

    long-to-double v6, v4

    int-to-double v8, v3

    mul-double v6, v6, v8

    sub-double/2addr p1, v6

    .line 200
    iget-object v0, p0, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_relative_date_hours_minutes_ago:I

    invoke-interface {v0, v3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 201
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    double-to-int p1, p1

    .line 202
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 203
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 204
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 206
    :cond_0
    check-cast p1, Lorg/threeten/bp/temporal/Temporal;

    sget-object v0, Lorg/threeten/bp/temporal/ChronoUnit;->HOURS:Lorg/threeten/bp/temporal/ChronoUnit;

    check-cast v0, Lorg/threeten/bp/temporal/TemporalUnit;

    invoke-virtual {p2, p1, v0}, Lorg/threeten/bp/ZonedDateTime;->until(Lorg/threeten/bp/temporal/Temporal;Lorg/threeten/bp/temporal/TemporalUnit;)J

    move-result-wide v4

    .line 207
    sget-object v0, Lorg/threeten/bp/temporal/ChronoUnit;->MINUTES:Lorg/threeten/bp/temporal/ChronoUnit;

    check-cast v0, Lorg/threeten/bp/temporal/TemporalUnit;

    invoke-virtual {p2, p1, v0}, Lorg/threeten/bp/ZonedDateTime;->until(Lorg/threeten/bp/temporal/Temporal;Lorg/threeten/bp/temporal/TemporalUnit;)J

    move-result-wide p1

    long-to-double p1, p1

    long-to-double v6, v4

    int-to-double v8, v3

    mul-double v6, v6, v8

    sub-double/2addr p1, v6

    .line 209
    iget-object v0, p0, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_relative_date_hours_minutes:I

    invoke-interface {v0, v3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 210
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    double-to-int p1, p1

    .line 211
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 212
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 213
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method
