.class public final Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter$initiateRefundFlow$1;
.super Lcom/squareup/permissions/PermissionGatekeeper$When;
.source "OrderHubRefundFlowStarter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter;->initiateRefundFlow$orderhub_applet_release(Lcom/squareup/billhistory/model/BillHistory;Ljava/util/Map;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0013\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H\u0016J\u0008\u0010\u0004\u001a\u00020\u0003H\u0016\u00a8\u0006\u0005"
    }
    d2 = {
        "com/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter$initiateRefundFlow$1",
        "Lcom/squareup/permissions/PermissionGatekeeper$When;",
        "failure",
        "",
        "success",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $bill:Lcom/squareup/billhistory/model/BillHistory;

.field final synthetic $selectedLineItems:Ljava/util/Map;

.field final synthetic $skipRestock:Z

.field final synthetic this$0:Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter;Lcom/squareup/billhistory/model/BillHistory;ZLjava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/billhistory/model/BillHistory;",
            "Z",
            "Ljava/util/Map;",
            ")V"
        }
    .end annotation

    .line 30
    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter$initiateRefundFlow$1;->this$0:Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter$initiateRefundFlow$1;->$bill:Lcom/squareup/billhistory/model/BillHistory;

    iput-boolean p3, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter$initiateRefundFlow$1;->$skipRestock:Z

    iput-object p4, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter$initiateRefundFlow$1;->$selectedLineItems:Ljava/util/Map;

    invoke-direct {p0}, Lcom/squareup/permissions/PermissionGatekeeper$When;-><init>()V

    return-void
.end method


# virtual methods
.method public failure()V
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter$initiateRefundFlow$1;->this$0:Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter;

    invoke-static {v0}, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter;->access$getOrderHubRefundFlowState$p(Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter;)Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;->cancelRefundFlow()V

    return-void
.end method

.method public success()V
    .locals 7

    .line 33
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter$initiateRefundFlow$1;->$bill:Lcom/squareup/billhistory/model/BillHistory;

    .line 34
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter$initiateRefundFlow$1;->getAuthorizedEmployeeToken()Ljava/lang/String;

    move-result-object v1

    .line 35
    sget-object v2, Lcom/squareup/ui/orderhub/OrderHubAppletScope;->INSTANCE:Lcom/squareup/ui/orderhub/OrderHubAppletScope;

    check-cast v2, Lcom/squareup/ui/main/RegisterTreeKey;

    .line 36
    iget-object v3, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter$initiateRefundFlow$1;->this$0:Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter;

    invoke-static {v3}, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter;->access$getFlow$p(Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter;)Lflow/Flow;

    move-result-object v3

    .line 38
    iget-boolean v5, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter$initiateRefundFlow$1;->$skipRestock:Z

    .line 39
    iget-object v4, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter$initiateRefundFlow$1;->$selectedLineItems:Ljava/util/Map;

    if-eqz v4, :cond_0

    invoke-static {v4}, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelectionsKt;->asLineItemQuantitiesByUid(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v4

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    :goto_0
    move-object v6, v4

    const/4 v4, 0x0

    .line 32
    invoke-static/range {v0 .. v6}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->startIssueRefundFlow(Lcom/squareup/billhistory/model/BillHistory;Ljava/lang/String;Lcom/squareup/ui/main/RegisterTreeKey;Lflow/Flow;ZZLjava/util/Map;)V

    return-void
.end method
