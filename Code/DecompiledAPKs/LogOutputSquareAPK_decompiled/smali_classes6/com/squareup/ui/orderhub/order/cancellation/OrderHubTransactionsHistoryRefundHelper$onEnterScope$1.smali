.class final Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper$onEnterScope$1;
.super Lkotlin/jvm/internal/Lambda;
.source "OrderHubTransactionsHistoryRefundHelper.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper$onEnterScope$1;->this$0:Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 27
    check-cast p1, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper$onEnterScope$1;->invoke(Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started;)V
    .locals 7

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    instance-of v0, p1, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForCancellationFlow;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper$onEnterScope$1;->this$0:Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper;

    .line 46
    check-cast p1, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForCancellationFlow;

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForCancellationFlow;->getBill$orderhub_applet_release()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForCancellationFlow;->getSelectedLineItems$orderhub_applet_release()Ljava/util/Map;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    .line 45
    invoke-static/range {v1 .. v6}, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper;->initiateRefundFlow$default(Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper;Lcom/squareup/billhistory/model/BillHistory;Ljava/util/Map;ZILjava/lang/Object;)V

    goto :goto_0

    .line 48
    :cond_0
    instance-of v0, p1, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForBillHistoryFlow;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper$onEnterScope$1;->this$0:Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper;

    check-cast p1, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForBillHistoryFlow;

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForBillHistoryFlow;->getBill$orderhub_applet_release()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper;->initiateRefundFlow$default(Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper;Lcom/squareup/billhistory/model/BillHistory;Ljava/util/Map;ZILjava/lang/Object;)V

    :cond_1
    :goto_0
    return-void
.end method
