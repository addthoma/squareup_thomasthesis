.class public final Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;
.super Lcom/squareup/ui/orderhub/order/OrderDetailsState;
.source "OrderDetailsState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/orderhub/order/OrderDetailsState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RetrievingBillState"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0019\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001B{\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\t\u00120\u0008\u0002\u0010\n\u001a*\u0012\u0004\u0012\u00020\u000c\u0012\u0018\u0012\u0016\u0012\u0008\u0012\u00060\u000cj\u0002`\r\u0012\u0004\u0012\u00020\u000e0\u000bj\u0002`\u000f\u0018\u00010\u000bj\u0004\u0018\u0001`\u0010\u0012\n\u0008\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u0012\u0012\u0008\u0008\u0002\u0010\u0013\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0014J\t\u0010!\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\"\u001a\u00020\u0003H\u00c6\u0003J\t\u0010#\u001a\u00020\u0006H\u00c6\u0003J\t\u0010$\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010%\u001a\u0004\u0018\u00010\tH\u00c6\u0003J1\u0010&\u001a*\u0012\u0004\u0012\u00020\u000c\u0012\u0018\u0012\u0016\u0012\u0008\u0012\u00060\u000cj\u0002`\r\u0012\u0004\u0012\u00020\u000e0\u000bj\u0002`\u000f\u0018\u00010\u000bj\u0004\u0018\u0001`\u0010H\u00c6\u0003J\u000b\u0010\'\u001a\u0004\u0018\u00010\u0012H\u00c6\u0003J\t\u0010(\u001a\u00020\u0003H\u00c6\u0003J\u0085\u0001\u0010)\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00032\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\t20\u0008\u0002\u0010\n\u001a*\u0012\u0004\u0012\u00020\u000c\u0012\u0018\u0012\u0016\u0012\u0008\u0012\u00060\u000cj\u0002`\r\u0012\u0004\u0012\u00020\u000e0\u000bj\u0002`\u000f\u0018\u00010\u000bj\u0004\u0018\u0001`\u00102\n\u0008\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u00122\u0008\u0008\u0002\u0010\u0013\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010*\u001a\u00020\u00032\u0008\u0010+\u001a\u0004\u0018\u00010,H\u00d6\u0003J\t\u0010-\u001a\u00020.H\u00d6\u0001J\t\u0010/\u001a\u00020\u000cH\u00d6\u0001R\u0013\u0010\u0008\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0002\u0010\u0017R\u0014\u0010\u0005\u001a\u00020\u0006X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019R\u0013\u0010\u0011\u001a\u0004\u0018\u00010\u0012\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u001bR\u0011\u0010\u0013\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001c\u0010\u0017R9\u0010\n\u001a*\u0012\u0004\u0012\u00020\u000c\u0012\u0018\u0012\u0016\u0012\u0008\u0012\u00060\u000cj\u0002`\r\u0012\u0004\u0012\u00020\u000e0\u000bj\u0002`\u000f\u0018\u00010\u000bj\u0004\u0018\u0001`\u0010\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001d\u0010\u001eR\u0014\u0010\u0004\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001f\u0010\u0017R\u0014\u0010\u0007\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008 \u0010\u0017\u00a8\u00060"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsState;",
        "isReadOnly",
        "",
        "showOrderIdInActionBar",
        "order",
        "Lcom/squareup/orders/model/Order;",
        "showSpinner",
        "cancelReason",
        "Lcom/squareup/ordermanagerdata/CancellationReason;",
        "selectedLineItems",
        "",
        "",
        "Lcom/squareup/ui/orderhub/order/itemselection/LineItemRowIdentifier;",
        "Lcom/squareup/orders/model/Order$LineItem;",
        "Lcom/squareup/ui/orderhub/order/itemselection/LineItemWithQuantityByIdentifier;",
        "Lcom/squareup/ui/orderhub/order/itemselection/LineItemSelectionsByUid;",
        "orderUpdateFailureState",
        "Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;",
        "retryCancellation",
        "(ZZLcom/squareup/orders/model/Order;ZLcom/squareup/ordermanagerdata/CancellationReason;Ljava/util/Map;Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Z)V",
        "getCancelReason",
        "()Lcom/squareup/ordermanagerdata/CancellationReason;",
        "()Z",
        "getOrder",
        "()Lcom/squareup/orders/model/Order;",
        "getOrderUpdateFailureState",
        "()Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;",
        "getRetryCancellation",
        "getSelectedLineItems",
        "()Ljava/util/Map;",
        "getShowOrderIdInActionBar",
        "getShowSpinner",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final cancelReason:Lcom/squareup/ordermanagerdata/CancellationReason;

.field private final isReadOnly:Z

.field private final order:Lcom/squareup/orders/model/Order;

.field private final orderUpdateFailureState:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

.field private final retryCancellation:Z

.field private final selectedLineItems:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;>;"
        }
    .end annotation
.end field

.field private final showOrderIdInActionBar:Z

.field private final showSpinner:Z


# direct methods
.method public constructor <init>(ZZLcom/squareup/orders/model/Order;ZLcom/squareup/ordermanagerdata/CancellationReason;Ljava/util/Map;Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Z)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ",
            "Lcom/squareup/orders/model/Order;",
            "Z",
            "Lcom/squareup/ordermanagerdata/CancellationReason;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;>;",
            "Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;",
            "Z)V"
        }
    .end annotation

    const-string v0, "order"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v6, 0x0

    move-object v1, p0

    move v2, p1

    move v3, p2

    move-object v4, p3

    move v5, p4

    .line 96
    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;-><init>(ZZLcom/squareup/orders/model/Order;ZLkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-boolean p1, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->isReadOnly:Z

    iput-boolean p2, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->showOrderIdInActionBar:Z

    iput-object p3, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->order:Lcom/squareup/orders/model/Order;

    iput-boolean p4, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->showSpinner:Z

    iput-object p5, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->cancelReason:Lcom/squareup/ordermanagerdata/CancellationReason;

    iput-object p6, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->selectedLineItems:Ljava/util/Map;

    iput-object p7, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->orderUpdateFailureState:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    iput-boolean p8, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->retryCancellation:Z

    return-void
.end method

.method public synthetic constructor <init>(ZZLcom/squareup/orders/model/Order;ZLcom/squareup/ordermanagerdata/CancellationReason;Ljava/util/Map;Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 11

    move/from16 v0, p9

    and-int/lit8 v1, v0, 0x8

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    const/4 v6, 0x1

    goto :goto_0

    :cond_0
    move v6, p4

    :goto_0
    and-int/lit8 v1, v0, 0x10

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    .line 92
    move-object v1, v2

    check-cast v1, Lcom/squareup/ordermanagerdata/CancellationReason;

    move-object v7, v1

    goto :goto_1

    :cond_1
    move-object/from16 v7, p5

    :goto_1
    and-int/lit8 v1, v0, 0x20

    if-eqz v1, :cond_2

    .line 93
    move-object v1, v2

    check-cast v1, Ljava/util/Map;

    move-object v8, v1

    goto :goto_2

    :cond_2
    move-object/from16 v8, p6

    :goto_2
    and-int/lit8 v1, v0, 0x40

    if-eqz v1, :cond_3

    .line 94
    move-object v1, v2

    check-cast v1, Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    move-object v9, v1

    goto :goto_3

    :cond_3
    move-object/from16 v9, p7

    :goto_3
    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_4

    const/4 v0, 0x0

    const/4 v10, 0x0

    goto :goto_4

    :cond_4
    move/from16 v10, p8

    :goto_4
    move-object v2, p0

    move v3, p1

    move v4, p2

    move-object v5, p3

    .line 95
    invoke-direct/range {v2 .. v10}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;-><init>(ZZLcom/squareup/orders/model/Order;ZLcom/squareup/ordermanagerdata/CancellationReason;Ljava/util/Map;Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Z)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;ZZLcom/squareup/orders/model/Order;ZLcom/squareup/ordermanagerdata/CancellationReason;Ljava/util/Map;Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;ZILjava/lang/Object;)Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;
    .locals 9

    move-object v0, p0

    move/from16 v1, p9

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->isReadOnly()Z

    move-result v2

    goto :goto_0

    :cond_0
    move v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->getShowOrderIdInActionBar()Z

    move-result v3

    goto :goto_1

    :cond_1
    move v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v4

    goto :goto_2

    :cond_2
    move-object v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->getShowSpinner()Z

    move-result v5

    goto :goto_3

    :cond_3
    move v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-object v6, v0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->cancelReason:Lcom/squareup/ordermanagerdata/CancellationReason;

    goto :goto_4

    :cond_4
    move-object v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-object v7, v0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->selectedLineItems:Ljava/util/Map;

    goto :goto_5

    :cond_5
    move-object v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-object v8, v0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->orderUpdateFailureState:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    goto :goto_6

    :cond_6
    move-object/from16 v8, p7

    :goto_6
    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_7

    iget-boolean v1, v0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->retryCancellation:Z

    goto :goto_7

    :cond_7
    move/from16 v1, p8

    :goto_7
    move p1, v2

    move p2, v3

    move-object p3, v4

    move p4, v5

    move-object p5, v6

    move-object p6, v7

    move-object/from16 p7, v8

    move/from16 p8, v1

    invoke-virtual/range {p0 .. p8}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->copy(ZZLcom/squareup/orders/model/Order;ZLcom/squareup/ordermanagerdata/CancellationReason;Ljava/util/Map;Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Z)Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->isReadOnly()Z

    move-result v0

    return v0
.end method

.method public final component2()Z
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->getShowOrderIdInActionBar()Z

    move-result v0

    return v0
.end method

.method public final component3()Lcom/squareup/orders/model/Order;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v0

    return-object v0
.end method

.method public final component4()Z
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->getShowSpinner()Z

    move-result v0

    return v0
.end method

.method public final component5()Lcom/squareup/ordermanagerdata/CancellationReason;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->cancelReason:Lcom/squareup/ordermanagerdata/CancellationReason;

    return-object v0
.end method

.method public final component6()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->selectedLineItems:Ljava/util/Map;

    return-object v0
.end method

.method public final component7()Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->orderUpdateFailureState:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    return-object v0
.end method

.method public final component8()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->retryCancellation:Z

    return v0
.end method

.method public final copy(ZZLcom/squareup/orders/model/Order;ZLcom/squareup/ordermanagerdata/CancellationReason;Ljava/util/Map;Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Z)Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ",
            "Lcom/squareup/orders/model/Order;",
            "Z",
            "Lcom/squareup/ordermanagerdata/CancellationReason;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;>;",
            "Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;",
            "Z)",
            "Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;"
        }
    .end annotation

    const-string v0, "order"

    move-object v4, p3

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;

    move-object v1, v0

    move v2, p1

    move v3, p2

    move v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move/from16 v9, p8

    invoke-direct/range {v1 .. v9}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;-><init>(ZZLcom/squareup/orders/model/Order;ZLcom/squareup/ordermanagerdata/CancellationReason;Ljava/util/Map;Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Z)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->isReadOnly()Z

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->isReadOnly()Z

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->getShowOrderIdInActionBar()Z

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->getShowOrderIdInActionBar()Z

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->getShowSpinner()Z

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->getShowSpinner()Z

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->cancelReason:Lcom/squareup/ordermanagerdata/CancellationReason;

    iget-object v1, p1, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->cancelReason:Lcom/squareup/ordermanagerdata/CancellationReason;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->selectedLineItems:Ljava/util/Map;

    iget-object v1, p1, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->selectedLineItems:Ljava/util/Map;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->orderUpdateFailureState:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    iget-object v1, p1, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->orderUpdateFailureState:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->retryCancellation:Z

    iget-boolean p1, p1, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->retryCancellation:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCancelReason()Lcom/squareup/ordermanagerdata/CancellationReason;
    .locals 1

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->cancelReason:Lcom/squareup/ordermanagerdata/CancellationReason;

    return-object v0
.end method

.method public getOrder()Lcom/squareup/orders/model/Order;
    .locals 1

    .line 90
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->order:Lcom/squareup/orders/model/Order;

    return-object v0
.end method

.method public final getOrderUpdateFailureState()Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;
    .locals 1

    .line 94
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->orderUpdateFailureState:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    return-object v0
.end method

.method public final getRetryCancellation()Z
    .locals 1

    .line 95
    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->retryCancellation:Z

    return v0
.end method

.method public final getSelectedLineItems()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;>;"
        }
    .end annotation

    .line 93
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->selectedLineItems:Ljava/util/Map;

    return-object v0
.end method

.method public getShowOrderIdInActionBar()Z
    .locals 1

    .line 89
    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->showOrderIdInActionBar:Z

    return v0
.end method

.method public getShowSpinner()Z
    .locals 1

    .line 91
    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->showSpinner:Z

    return v0
.end method

.method public hashCode()I
    .locals 4

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->isReadOnly()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->getShowOrderIdInActionBar()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v2

    const/4 v3, 0x0

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->getShowSpinner()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :cond_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->cancelReason:Lcom/squareup/ordermanagerdata/CancellationReason;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_4
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->selectedLineItems:Ljava/util/Map;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_5
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->orderUpdateFailureState:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v3

    :cond_6
    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->retryCancellation:Z

    if-eqz v2, :cond_7

    goto :goto_3

    :cond_7
    move v1, v2

    :goto_3
    add-int/2addr v0, v1

    return v0
.end method

.method public isReadOnly()Z
    .locals 1

    .line 88
    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->isReadOnly:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RetrievingBillState(isReadOnly="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->isReadOnly()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", showOrderIdInActionBar="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->getShowOrderIdInActionBar()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", order="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", showSpinner="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->getShowSpinner()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", cancelReason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->cancelReason:Lcom/squareup/ordermanagerdata/CancellationReason;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", selectedLineItems="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->selectedLineItems:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", orderUpdateFailureState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->orderUpdateFailureState:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", retryCancellation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->retryCancellation:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
