.class public final Lcom/squareup/ui/orderhub/OrderHubState;
.super Ljava/lang/Object;
.source "OrderHubState.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0016\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001BC\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u000fJ\t\u0010\u0019\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001a\u001a\u00020\u0003H\u00c6\u0003J\u000f\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006H\u00c6\u0003J\t\u0010\u001c\u001a\u00020\tH\u00c6\u0003J\t\u0010\u001d\u001a\u00020\u000bH\u00c6\u0003J\t\u0010\u001e\u001a\u00020\rH\u00c6\u0003J\t\u0010\u001f\u001a\u00020\u0003H\u00c6\u0003JU\u0010 \u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u000e\u0008\u0002\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00062\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t2\u0008\u0008\u0002\u0010\n\u001a\u00020\u000b2\u0008\u0008\u0002\u0010\u000c\u001a\u00020\r2\u0008\u0008\u0002\u0010\u000e\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010!\u001a\u00020\u00032\u0008\u0010\"\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010#\u001a\u00020$H\u00d6\u0001J\t\u0010%\u001a\u00020&H\u00d6\u0001R\u0011\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0004\u0010\u0012R\u0011\u0010\u000e\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u0012R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0002\u0010\u0012R\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u0011\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016R\u0017\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0018\u00a8\u0006\'"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/OrderHubState;",
        "",
        "isReadOnly",
        "",
        "isMasterDetail",
        "syncedOrders",
        "",
        "Lcom/squareup/orders/model/Order;",
        "masterState",
        "Lcom/squareup/ui/orderhub/MasterState;",
        "detailState",
        "Lcom/squareup/ui/orderhub/DetailState;",
        "nextWorkflowAction",
        "Lcom/squareup/ui/orderhub/OrderWorkflowAction;",
        "isQuickActionsEnabled",
        "(ZZLjava/util/List;Lcom/squareup/ui/orderhub/MasterState;Lcom/squareup/ui/orderhub/DetailState;Lcom/squareup/ui/orderhub/OrderWorkflowAction;Z)V",
        "getDetailState",
        "()Lcom/squareup/ui/orderhub/DetailState;",
        "()Z",
        "getMasterState",
        "()Lcom/squareup/ui/orderhub/MasterState;",
        "getNextWorkflowAction",
        "()Lcom/squareup/ui/orderhub/OrderWorkflowAction;",
        "getSyncedOrders",
        "()Ljava/util/List;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final detailState:Lcom/squareup/ui/orderhub/DetailState;

.field private final isMasterDetail:Z

.field private final isQuickActionsEnabled:Z

.field private final isReadOnly:Z

.field private final masterState:Lcom/squareup/ui/orderhub/MasterState;

.field private final nextWorkflowAction:Lcom/squareup/ui/orderhub/OrderWorkflowAction;

.field private final syncedOrders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(ZZLjava/util/List;Lcom/squareup/ui/orderhub/MasterState;Lcom/squareup/ui/orderhub/DetailState;Lcom/squareup/ui/orderhub/OrderWorkflowAction;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;",
            "Lcom/squareup/ui/orderhub/MasterState;",
            "Lcom/squareup/ui/orderhub/DetailState;",
            "Lcom/squareup/ui/orderhub/OrderWorkflowAction;",
            "Z)V"
        }
    .end annotation

    const-string v0, "syncedOrders"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "masterState"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "detailState"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nextWorkflowAction"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/squareup/ui/orderhub/OrderHubState;->isReadOnly:Z

    iput-boolean p2, p0, Lcom/squareup/ui/orderhub/OrderHubState;->isMasterDetail:Z

    iput-object p3, p0, Lcom/squareup/ui/orderhub/OrderHubState;->syncedOrders:Ljava/util/List;

    iput-object p4, p0, Lcom/squareup/ui/orderhub/OrderHubState;->masterState:Lcom/squareup/ui/orderhub/MasterState;

    iput-object p5, p0, Lcom/squareup/ui/orderhub/OrderHubState;->detailState:Lcom/squareup/ui/orderhub/DetailState;

    iput-object p6, p0, Lcom/squareup/ui/orderhub/OrderHubState;->nextWorkflowAction:Lcom/squareup/ui/orderhub/OrderWorkflowAction;

    iput-boolean p7, p0, Lcom/squareup/ui/orderhub/OrderHubState;->isQuickActionsEnabled:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/orderhub/OrderHubState;ZZLjava/util/List;Lcom/squareup/ui/orderhub/MasterState;Lcom/squareup/ui/orderhub/DetailState;Lcom/squareup/ui/orderhub/OrderWorkflowAction;ZILjava/lang/Object;)Lcom/squareup/ui/orderhub/OrderHubState;
    .locals 5

    and-int/lit8 p9, p8, 0x1

    if-eqz p9, :cond_0

    iget-boolean p1, p0, Lcom/squareup/ui/orderhub/OrderHubState;->isReadOnly:Z

    :cond_0
    and-int/lit8 p9, p8, 0x2

    if-eqz p9, :cond_1

    iget-boolean p2, p0, Lcom/squareup/ui/orderhub/OrderHubState;->isMasterDetail:Z

    :cond_1
    move p9, p2

    and-int/lit8 p2, p8, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/ui/orderhub/OrderHubState;->syncedOrders:Ljava/util/List;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p8, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/ui/orderhub/OrderHubState;->masterState:Lcom/squareup/ui/orderhub/MasterState;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p8, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/ui/orderhub/OrderHubState;->detailState:Lcom/squareup/ui/orderhub/DetailState;

    :cond_4
    move-object v2, p5

    and-int/lit8 p2, p8, 0x20

    if-eqz p2, :cond_5

    iget-object p6, p0, Lcom/squareup/ui/orderhub/OrderHubState;->nextWorkflowAction:Lcom/squareup/ui/orderhub/OrderWorkflowAction;

    :cond_5
    move-object v3, p6

    and-int/lit8 p2, p8, 0x40

    if-eqz p2, :cond_6

    iget-boolean p7, p0, Lcom/squareup/ui/orderhub/OrderHubState;->isQuickActionsEnabled:Z

    :cond_6
    move v4, p7

    move-object p2, p0

    move p3, p1

    move p4, p9

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    move-object p8, v3

    move p9, v4

    invoke-virtual/range {p2 .. p9}, Lcom/squareup/ui/orderhub/OrderHubState;->copy(ZZLjava/util/List;Lcom/squareup/ui/orderhub/MasterState;Lcom/squareup/ui/orderhub/DetailState;Lcom/squareup/ui/orderhub/OrderWorkflowAction;Z)Lcom/squareup/ui/orderhub/OrderHubState;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/OrderHubState;->isReadOnly:Z

    return v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/OrderHubState;->isMasterDetail:Z

    return v0
.end method

.method public final component3()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/orderhub/OrderHubState;->syncedOrders:Ljava/util/List;

    return-object v0
.end method

.method public final component4()Lcom/squareup/ui/orderhub/MasterState;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/orderhub/OrderHubState;->masterState:Lcom/squareup/ui/orderhub/MasterState;

    return-object v0
.end method

.method public final component5()Lcom/squareup/ui/orderhub/DetailState;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/orderhub/OrderHubState;->detailState:Lcom/squareup/ui/orderhub/DetailState;

    return-object v0
.end method

.method public final component6()Lcom/squareup/ui/orderhub/OrderWorkflowAction;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/orderhub/OrderHubState;->nextWorkflowAction:Lcom/squareup/ui/orderhub/OrderWorkflowAction;

    return-object v0
.end method

.method public final component7()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/OrderHubState;->isQuickActionsEnabled:Z

    return v0
.end method

.method public final copy(ZZLjava/util/List;Lcom/squareup/ui/orderhub/MasterState;Lcom/squareup/ui/orderhub/DetailState;Lcom/squareup/ui/orderhub/OrderWorkflowAction;Z)Lcom/squareup/ui/orderhub/OrderHubState;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;",
            "Lcom/squareup/ui/orderhub/MasterState;",
            "Lcom/squareup/ui/orderhub/DetailState;",
            "Lcom/squareup/ui/orderhub/OrderWorkflowAction;",
            "Z)",
            "Lcom/squareup/ui/orderhub/OrderHubState;"
        }
    .end annotation

    const-string v0, "syncedOrders"

    move-object v4, p3

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "masterState"

    move-object v5, p4

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "detailState"

    move-object v6, p5

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nextWorkflowAction"

    move-object v7, p6

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/orderhub/OrderHubState;

    move-object v1, v0

    move v2, p1

    move v3, p2

    move/from16 v8, p7

    invoke-direct/range {v1 .. v8}, Lcom/squareup/ui/orderhub/OrderHubState;-><init>(ZZLjava/util/List;Lcom/squareup/ui/orderhub/MasterState;Lcom/squareup/ui/orderhub/DetailState;Lcom/squareup/ui/orderhub/OrderWorkflowAction;Z)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/orderhub/OrderHubState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/orderhub/OrderHubState;

    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/OrderHubState;->isReadOnly:Z

    iget-boolean v1, p1, Lcom/squareup/ui/orderhub/OrderHubState;->isReadOnly:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/OrderHubState;->isMasterDetail:Z

    iget-boolean v1, p1, Lcom/squareup/ui/orderhub/OrderHubState;->isMasterDetail:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/orderhub/OrderHubState;->syncedOrders:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/ui/orderhub/OrderHubState;->syncedOrders:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/orderhub/OrderHubState;->masterState:Lcom/squareup/ui/orderhub/MasterState;

    iget-object v1, p1, Lcom/squareup/ui/orderhub/OrderHubState;->masterState:Lcom/squareup/ui/orderhub/MasterState;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/orderhub/OrderHubState;->detailState:Lcom/squareup/ui/orderhub/DetailState;

    iget-object v1, p1, Lcom/squareup/ui/orderhub/OrderHubState;->detailState:Lcom/squareup/ui/orderhub/DetailState;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/orderhub/OrderHubState;->nextWorkflowAction:Lcom/squareup/ui/orderhub/OrderWorkflowAction;

    iget-object v1, p1, Lcom/squareup/ui/orderhub/OrderHubState;->nextWorkflowAction:Lcom/squareup/ui/orderhub/OrderWorkflowAction;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/OrderHubState;->isQuickActionsEnabled:Z

    iget-boolean p1, p1, Lcom/squareup/ui/orderhub/OrderHubState;->isQuickActionsEnabled:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getDetailState()Lcom/squareup/ui/orderhub/DetailState;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/ui/orderhub/OrderHubState;->detailState:Lcom/squareup/ui/orderhub/DetailState;

    return-object v0
.end method

.method public final getMasterState()Lcom/squareup/ui/orderhub/MasterState;
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/squareup/ui/orderhub/OrderHubState;->masterState:Lcom/squareup/ui/orderhub/MasterState;

    return-object v0
.end method

.method public final getNextWorkflowAction()Lcom/squareup/ui/orderhub/OrderWorkflowAction;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/squareup/ui/orderhub/OrderHubState;->nextWorkflowAction:Lcom/squareup/ui/orderhub/OrderWorkflowAction;

    return-object v0
.end method

.method public final getSyncedOrders()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;"
        }
    .end annotation

    .line 14
    iget-object v0, p0, Lcom/squareup/ui/orderhub/OrderHubState;->syncedOrders:Ljava/util/List;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/OrderHubState;->isReadOnly:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/ui/orderhub/OrderHubState;->isMasterDetail:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/orderhub/OrderHubState;->syncedOrders:Ljava/util/List;

    const/4 v3, 0x0

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/orderhub/OrderHubState;->masterState:Lcom/squareup/ui/orderhub/MasterState;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/orderhub/OrderHubState;->detailState:Lcom/squareup/ui/orderhub/DetailState;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_4
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/orderhub/OrderHubState;->nextWorkflowAction:Lcom/squareup/ui/orderhub/OrderWorkflowAction;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v3

    :cond_5
    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/ui/orderhub/OrderHubState;->isQuickActionsEnabled:Z

    if-eqz v2, :cond_6

    goto :goto_3

    :cond_6
    move v1, v2

    :goto_3
    add-int/2addr v0, v1

    return v0
.end method

.method public final isMasterDetail()Z
    .locals 1

    .line 13
    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/OrderHubState;->isMasterDetail:Z

    return v0
.end method

.method public final isQuickActionsEnabled()Z
    .locals 1

    .line 18
    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/OrderHubState;->isQuickActionsEnabled:Z

    return v0
.end method

.method public final isReadOnly()Z
    .locals 1

    .line 12
    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/OrderHubState;->isReadOnly:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OrderHubState(isReadOnly="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/orderhub/OrderHubState;->isReadOnly:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isMasterDetail="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/orderhub/OrderHubState;->isMasterDetail:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", syncedOrders="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/OrderHubState;->syncedOrders:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", masterState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/OrderHubState;->masterState:Lcom/squareup/ui/orderhub/MasterState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", detailState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/OrderHubState;->detailState:Lcom/squareup/ui/orderhub/DetailState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", nextWorkflowAction="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/OrderHubState;->nextWorkflowAction:Lcom/squareup/ui/orderhub/OrderWorkflowAction;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isQuickActionsEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/orderhub/OrderHubState;->isQuickActionsEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
