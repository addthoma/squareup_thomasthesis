.class public final Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;
.super Lcom/squareup/ui/orderhub/OrderHubWorkflow;
.source "RealOrderHubWorkflow.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/ui/orderhub/OrderHubAppletScope;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealOrderHubWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealOrderHubWorkflow.kt\ncom/squareup/ui/orderhub/RealOrderHubWorkflow\n+ 2 Maps.kt\nkotlin/collections/MapsKt__MapsKt\n+ 3 _Maps.kt\nkotlin/collections/MapsKt___MapsKt\n+ 4 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 5 Worker.kt\ncom/squareup/workflow/Worker$Companion\n+ 6 Worker.kt\ncom/squareup/workflow/WorkerKt\n+ 7 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n+ 8 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,713:1\n501#2:714\n486#2,6:715\n428#2:789\n378#2:790\n151#3:721\n152#3:728\n85#4:722\n85#4:725\n41#4:729\n56#4,2:730\n41#4:733\n56#4,2:734\n85#4:737\n85#4:740\n41#4:743\n56#4,2:744\n41#4:747\n56#4,2:748\n85#4:751\n240#5:723\n240#5:726\n240#5:738\n240#5:741\n240#5:752\n276#6:724\n276#6:727\n276#6:732\n276#6:736\n276#6:739\n276#6:742\n276#6:746\n276#6:750\n276#6:753\n149#7,5:754\n149#7,5:759\n149#7,5:764\n149#7,5:769\n149#7,5:774\n149#7,5:779\n149#7,5:784\n1143#8,2:791\n1577#8,4:793\n1146#8:797\n1360#8:798\n1429#8,3:799\n*E\n*S KotlinDebug\n*F\n+ 1 RealOrderHubWorkflow.kt\ncom/squareup/ui/orderhub/RealOrderHubWorkflow\n*L\n158#1:714\n158#1,6:715\n609#1:789\n609#1:790\n158#1:721\n158#1:728\n158#1:722\n158#1:725\n202#1:729\n202#1,2:730\n219#1:733\n219#1,2:734\n225#1:737\n231#1:740\n236#1:743\n236#1,2:744\n242#1:747\n242#1,2:748\n257#1:751\n158#1:723\n158#1:726\n225#1:738\n231#1:741\n257#1:752\n158#1:724\n158#1:727\n202#1:732\n219#1:736\n225#1:739\n231#1:742\n236#1:746\n242#1:750\n257#1:753\n382#1,5:754\n385#1,5:759\n395#1,5:764\n398#1,5:769\n407#1,5:774\n410#1,5:779\n433#1,5:784\n609#1,2:791\n609#1,4:793\n609#1:797\n648#1:798\n648#1,3:799\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00c0\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0008\u000b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0007\u0008\u0007\u0018\u0000 Q2\u00020\u0001:\u0001QBo\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0015\u0012\u0006\u0010\u0016\u001a\u00020\u0017\u0012\u000e\u0008\u0001\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u001a0\u0019\u00a2\u0006\u0002\u0010\u001bJ\u0010\u0010\u001c\u001a\u00020\u001a2\u0006\u0010\u001d\u001a\u00020\u001eH\u0002J\u0008\u0010\u001f\u001a\u00020 H\u0002JF\u0010!\u001a$\u0012\u0004\u0012\u00020#\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030$j\u0002`%0\"j\u0008\u0012\u0004\u0012\u00020#`&2\u0012\u0010\'\u001a\u000e\u0012\u0004\u0012\u00020 \u0012\u0004\u0012\u00020)0(2\u0006\u0010*\u001a\u00020 H\u0002JJ\u0010+\u001a(\u0012\u0004\u0012\u00020#\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030$j\u0002`%\u0018\u00010\"j\n\u0012\u0004\u0012\u00020#\u0018\u0001`&2\u0006\u0010*\u001a\u00020 2\u0012\u0010\'\u001a\u000e\u0012\u0004\u0012\u00020 \u0012\u0004\u0012\u00020)0(H\u0002J\u0080\u0001\u0010,\u001a\u00020 2\u0006\u0010-\u001a\u00020\u001e2\u000c\u0010.\u001a\u0008\u0012\u0004\u0012\u0002000/2\u0006\u00101\u001a\u00020\u001a2\u0006\u00102\u001a\u00020\u001a2\u0006\u00103\u001a\u0002042\n\u0008\u0002\u00105\u001a\u0004\u0018\u0001062\u0008\u0008\u0002\u00107\u001a\u00020\u001a2\u0008\u0008\u0002\u00108\u001a\u00020\u001a2\n\u0008\u0002\u00109\u001a\u0004\u0018\u00010:2\u0014\u0008\u0002\u0010;\u001a\u000e\u0012\u0004\u0012\u00020<\u0012\u0004\u0012\u00020=0\"2\u0006\u0010>\u001a\u00020\u001aH\u0002J\u0018\u0010?\u001a\u00020 2\u0006\u0010@\u001a\u00020 2\u0006\u0010-\u001a\u00020\u001eH\u0002J\u0018\u0010A\u001a\u00020 2\u0006\u0010@\u001a\u00020 2\u0006\u0010B\u001a\u000200H\u0002J\u001c\u0010C\u001a\u00020 2\u0006\u0010@\u001a\u00020 2\n\u0008\u0002\u00105\u001a\u0004\u0018\u000106H\u0002J\u0010\u0010D\u001a\u00020 2\u0006\u0010@\u001a\u00020 H\u0002J\u001e\u0010E\u001a\u00020 2\u0006\u0010@\u001a\u00020 2\u000c\u0010F\u001a\u0008\u0012\u0004\u0012\u0002000/H\u0002J\u001a\u0010G\u001a\u00020 2\u0006\u0010H\u001a\u00020I2\u0008\u0010J\u001a\u0004\u0018\u00010KH\u0016J \u0010L\u001a\u00020 2\u0006\u0010M\u001a\u00020I2\u0006\u0010N\u001a\u00020I2\u0006\u0010*\u001a\u00020 H\u0016JN\u0010O\u001a$\u0012\u0004\u0012\u00020#\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030$j\u0002`%0\"j\u0008\u0012\u0004\u0012\u00020#`&2\u0006\u0010H\u001a\u00020I2\u0006\u0010*\u001a\u00020 2\u0012\u0010\'\u001a\u000e\u0012\u0004\u0012\u00020 \u0012\u0004\u0012\u00020)0(H\u0016J\u0010\u0010P\u001a\u00020K2\u0006\u0010*\u001a\u00020 H\u0016R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u001a0\u0019X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006R"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;",
        "Lcom/squareup/ui/orderhub/OrderHubWorkflow;",
        "orderDetailsWorkflow",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsWorkflow;",
        "orderMarkShippedWorkflow",
        "Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedWorkflow;",
        "searchWorkflow",
        "Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow;",
        "res",
        "Lcom/squareup/util/Res;",
        "device",
        "Lcom/squareup/util/Device;",
        "connectivityMonitor",
        "Lcom/squareup/connectivity/ConnectivityMonitor;",
        "orderRepository",
        "Lcom/squareup/ordermanagerdata/OrderRepository;",
        "orderHubAnalytics",
        "Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;",
        "uuidGenerator",
        "Lcom/squareup/log/UUIDGenerator;",
        "currentTime",
        "Lcom/squareup/time/CurrentTime;",
        "accountStatusSettings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "orderQuickActionsEnabledPreference",
        "Lcom/f2prateek/rx/preferences2/Preference;",
        "",
        "(Lcom/squareup/ui/orderhub/order/OrderDetailsWorkflow;Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedWorkflow;Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/ordermanagerdata/OrderRepository;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;Lcom/squareup/log/UUIDGenerator;Lcom/squareup/time/CurrentTime;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/f2prateek/rx/preferences2/Preference;)V",
        "canLoadMoreOrders",
        "filter",
        "Lcom/squareup/ui/orderhub/master/Filter;",
        "getInitialState",
        "Lcom/squareup/ui/orderhub/OrderHubState;",
        "getMarkShippedChildRendering",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "Lcom/squareup/ui/orderhub/OrderHubResult;",
        "state",
        "getOrderDetailChildRendering",
        "getOrderHubStateFromValues",
        "selectedFilter",
        "syncedOrders",
        "",
        "Lcom/squareup/orders/model/Order;",
        "isReadOnly",
        "isMasterDetail",
        "nextWorkflowAction",
        "Lcom/squareup/ui/orderhub/OrderWorkflowAction;",
        "syncingError",
        "Lcom/squareup/ordermanagerdata/ResultState$Failure;",
        "loadingMoreOrders",
        "updatingSyncDueToState",
        "lastSyncDate",
        "Lorg/threeten/bp/ZonedDateTime;",
        "orderQuickActionsMap",
        "",
        "Lcom/squareup/ui/orderhub/OrderQuickActionStatus;",
        "isQuickActionsEnabled",
        "getStateAfterFilterSelected",
        "currentState",
        "getStateAfterOrderViewFinished",
        "changedOrder",
        "getStateAfterPaginationCompleted",
        "getStateAfterPaginationRequested",
        "getStateWhenOrdersUpdated",
        "orders",
        "initialState",
        "props",
        "Lcom/squareup/ui/orderhub/OrderHubWorkflowInput;",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "onPropsChanged",
        "old",
        "new",
        "render",
        "snapshotState",
        "Companion",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final ALL_ORDERS_WORKER_KEY:Ljava/lang/String; = "all-orders"

.field public static final Companion:Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$Companion;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final LOADING_MORE_ORDERS_WORKER_KEY:Ljava/lang/String; = "loading-more-orders"

.field public static final MARK_ORDERS_KNOWN_WORKER_KEY:Ljava/lang/String; = "mark-orders-known"

.field public static final REFRESH_ALL_ORDERS_WORKER_KEY:Ljava/lang/String; = "refresh-all-orders"

.field public static final UNKNOWN_ORDERS_WORKER_KEY:Ljava/lang/String; = "unknown-orders"


# instance fields
.field private final accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

.field private final currentTime:Lcom/squareup/time/CurrentTime;

.field private final device:Lcom/squareup/util/Device;

.field private final orderDetailsWorkflow:Lcom/squareup/ui/orderhub/order/OrderDetailsWorkflow;

.field private final orderHubAnalytics:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;

.field private final orderMarkShippedWorkflow:Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedWorkflow;

.field private final orderQuickActionsEnabledPreference:Lcom/f2prateek/rx/preferences2/Preference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final orderRepository:Lcom/squareup/ordermanagerdata/OrderRepository;

.field private final res:Lcom/squareup/util/Res;

.field private final searchWorkflow:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow;

.field private final uuidGenerator:Lcom/squareup/log/UUIDGenerator;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->Companion:Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/orderhub/order/OrderDetailsWorkflow;Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedWorkflow;Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/ordermanagerdata/OrderRepository;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;Lcom/squareup/log/UUIDGenerator;Lcom/squareup/time/CurrentTime;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/f2prateek/rx/preferences2/Preference;)V
    .locals 1
    .param p12    # Lcom/f2prateek/rx/preferences2/Preference;
        .annotation runtime Lcom/squareup/orderhub/settings/OrderHubQuickActionsEnabledPreference;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/orderhub/order/OrderDetailsWorkflow;",
            "Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedWorkflow;",
            "Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            "Lcom/squareup/ordermanagerdata/OrderRepository;",
            "Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;",
            "Lcom/squareup/log/UUIDGenerator;",
            "Lcom/squareup/time/CurrentTime;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "orderDetailsWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderMarkShippedWorkflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "searchWorkflow"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "device"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "connectivityMonitor"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderRepository"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderHubAnalytics"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "uuidGenerator"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currentTime"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountStatusSettings"

    invoke-static {p11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderQuickActionsEnabledPreference"

    invoke-static {p12, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    invoke-direct {p0}, Lcom/squareup/ui/orderhub/OrderHubWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->orderDetailsWorkflow:Lcom/squareup/ui/orderhub/order/OrderDetailsWorkflow;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->orderMarkShippedWorkflow:Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedWorkflow;

    iput-object p3, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->searchWorkflow:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow;

    iput-object p4, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->res:Lcom/squareup/util/Res;

    iput-object p5, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->device:Lcom/squareup/util/Device;

    iput-object p6, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    iput-object p7, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->orderRepository:Lcom/squareup/ordermanagerdata/OrderRepository;

    iput-object p8, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->orderHubAnalytics:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;

    iput-object p9, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->uuidGenerator:Lcom/squareup/log/UUIDGenerator;

    iput-object p10, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->currentTime:Lcom/squareup/time/CurrentTime;

    iput-object p11, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object p12, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->orderQuickActionsEnabledPreference:Lcom/f2prateek/rx/preferences2/Preference;

    return-void
.end method

.method public static final synthetic access$getOrderHubAnalytics$p(Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;)Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;
    .locals 0

    .line 77
    iget-object p0, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->orderHubAnalytics:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;

    return-object p0
.end method

.method public static final synthetic access$getOrderQuickActionsEnabledPreference$p(Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 0

    .line 77
    iget-object p0, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->orderQuickActionsEnabledPreference:Lcom/f2prateek/rx/preferences2/Preference;

    return-object p0
.end method

.method public static final synthetic access$getOrderRepository$p(Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;)Lcom/squareup/ordermanagerdata/OrderRepository;
    .locals 0

    .line 77
    iget-object p0, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->orderRepository:Lcom/squareup/ordermanagerdata/OrderRepository;

    return-object p0
.end method

.method public static final synthetic access$getStateAfterFilterSelected(Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;Lcom/squareup/ui/orderhub/OrderHubState;Lcom/squareup/ui/orderhub/master/Filter;)Lcom/squareup/ui/orderhub/OrderHubState;
    .locals 0

    .line 77
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->getStateAfterFilterSelected(Lcom/squareup/ui/orderhub/OrderHubState;Lcom/squareup/ui/orderhub/master/Filter;)Lcom/squareup/ui/orderhub/OrderHubState;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getStateAfterOrderViewFinished(Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;Lcom/squareup/ui/orderhub/OrderHubState;Lcom/squareup/orders/model/Order;)Lcom/squareup/ui/orderhub/OrderHubState;
    .locals 0

    .line 77
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->getStateAfterOrderViewFinished(Lcom/squareup/ui/orderhub/OrderHubState;Lcom/squareup/orders/model/Order;)Lcom/squareup/ui/orderhub/OrderHubState;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getStateAfterPaginationCompleted(Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;Lcom/squareup/ui/orderhub/OrderHubState;Lcom/squareup/ordermanagerdata/ResultState$Failure;)Lcom/squareup/ui/orderhub/OrderHubState;
    .locals 0

    .line 77
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->getStateAfterPaginationCompleted(Lcom/squareup/ui/orderhub/OrderHubState;Lcom/squareup/ordermanagerdata/ResultState$Failure;)Lcom/squareup/ui/orderhub/OrderHubState;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getStateAfterPaginationRequested(Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;Lcom/squareup/ui/orderhub/OrderHubState;)Lcom/squareup/ui/orderhub/OrderHubState;
    .locals 0

    .line 77
    invoke-direct {p0, p1}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->getStateAfterPaginationRequested(Lcom/squareup/ui/orderhub/OrderHubState;)Lcom/squareup/ui/orderhub/OrderHubState;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getStateWhenOrdersUpdated(Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;Lcom/squareup/ui/orderhub/OrderHubState;Ljava/util/List;)Lcom/squareup/ui/orderhub/OrderHubState;
    .locals 0

    .line 77
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->getStateWhenOrdersUpdated(Lcom/squareup/ui/orderhub/OrderHubState;Ljava/util/List;)Lcom/squareup/ui/orderhub/OrderHubState;

    move-result-object p0

    return-object p0
.end method

.method private final canLoadMoreOrders(Lcom/squareup/ui/orderhub/master/Filter;)Z
    .locals 2

    .line 701
    iget-object v0, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->orderRepository:Lcom/squareup/ordermanagerdata/OrderRepository;

    invoke-interface {v0}, Lcom/squareup/ordermanagerdata/OrderRepository;->canFetchMoreCompletedOrders()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/ui/orderhub/master/Filter$Status;

    .line 702
    sget-object v1, Lcom/squareup/protos/client/orders/OrderGroup;->COMPLETED:Lcom/squareup/protos/client/orders/OrderGroup;

    invoke-direct {v0, v1}, Lcom/squareup/ui/orderhub/master/Filter$Status;-><init>(Lcom/squareup/protos/client/orders/OrderGroup;)V

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private final getInitialState()Lcom/squareup/ui/orderhub/OrderHubState;
    .locals 15

    .line 443
    sget-object v0, Lcom/squareup/ui/orderhub/master/Filter;->Companion:Lcom/squareup/ui/orderhub/master/Filter$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/master/Filter$Companion;->getDEFAULT()Lcom/squareup/ui/orderhub/master/Filter;

    move-result-object v2

    .line 444
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v3

    .line 449
    sget-object v0, Lcom/squareup/ui/orderhub/OrderWorkflowAction$OrderNoWorkflowAction;->INSTANCE:Lcom/squareup/ui/orderhub/OrderWorkflowAction$OrderNoWorkflowAction;

    move-object v6, v0

    check-cast v6, Lcom/squareup/ui/orderhub/OrderWorkflowAction;

    .line 450
    iget-object v0, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->orderQuickActionsEnabledPreference:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v0}, Lcom/f2prateek/rx/preferences2/Preference;->get()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "orderQuickActionsEnabledPreference.get()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v12

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/16 v13, 0x3e0

    const/4 v14, 0x0

    move-object v1, p0

    .line 442
    invoke-static/range {v1 .. v14}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->getOrderHubStateFromValues$default(Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;Lcom/squareup/ui/orderhub/master/Filter;Ljava/util/List;ZZLcom/squareup/ui/orderhub/OrderWorkflowAction;Lcom/squareup/ordermanagerdata/ResultState$Failure;ZZLorg/threeten/bp/ZonedDateTime;Ljava/util/Map;ZILjava/lang/Object;)Lcom/squareup/ui/orderhub/OrderHubState;

    move-result-object v0

    return-object v0
.end method

.method private final getMarkShippedChildRendering(Lcom/squareup/workflow/RenderContext;Lcom/squareup/ui/orderhub/OrderHubState;)Ljava/util/Map;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/ui/orderhub/OrderHubState;",
            "-",
            "Lcom/squareup/ui/orderhub/OrderHubResult;",
            ">;",
            "Lcom/squareup/ui/orderhub/OrderHubState;",
            ")",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 458
    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/OrderHubState;->getNextWorkflowAction()Lcom/squareup/ui/orderhub/OrderWorkflowAction;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/ui/orderhub/OrderWorkflowAction$OrderMarkShippedWorkflowAction;

    if-eqz v0, :cond_1

    .line 459
    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/OrderHubState;->getNextWorkflowAction()Lcom/squareup/ui/orderhub/OrderWorkflowAction;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/orderhub/OrderWorkflowAction$OrderMarkShippedWorkflowAction;

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/OrderWorkflowAction$OrderMarkShippedWorkflowAction;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v0

    .line 462
    invoke-static {v0}, Lcom/squareup/ui/orderhub/util/proto/OrdersKt;->getPrimaryAction(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/Action;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v2, 0x0

    .line 460
    new-instance v5, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedInput;

    invoke-direct {v5, v0, v1, v2}, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedInput;-><init>(Lcom/squareup/orders/model/Order;Lcom/squareup/protos/client/orders/Action;Z)V

    .line 466
    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/OrderHubState;->getDetailState()Lcom/squareup/ui/orderhub/DetailState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/DetailState;->getOrderQuickActionStatusMap()Ljava/util/Map;

    move-result-object v1

    invoke-static {v1}, Lkotlin/collections/MapsKt;->toMutableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    .line 469
    iget-object v2, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->orderMarkShippedWorkflow:Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedWorkflow;

    move-object v4, v2

    check-cast v4, Lcom/squareup/workflow/Workflow;

    const/4 v6, 0x0

    .line 471
    new-instance v2, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$getMarkShippedChildRendering$1;

    invoke-direct {v2, v1, v0, p2}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$getMarkShippedChildRendering$1;-><init>(Ljava/util/Map;Lcom/squareup/orders/model/Order;Lcom/squareup/ui/orderhub/OrderHubState;)V

    move-object v7, v2

    check-cast v7, Lkotlin/jvm/functions/Function1;

    const/4 v8, 0x4

    const/4 v9, 0x0

    move-object v3, p1

    .line 468
    invoke-static/range {v3 .. v9}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    return-object p1

    .line 462
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Required value was null."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 458
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Failed requirement."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final getOrderDetailChildRendering(Lcom/squareup/ui/orderhub/OrderHubState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/orderhub/OrderHubState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/ui/orderhub/OrderHubState;",
            "-",
            "Lcom/squareup/ui/orderhub/OrderHubResult;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 506
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/OrderHubState;->getNextWorkflowAction()Lcom/squareup/ui/orderhub/OrderWorkflowAction;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/ui/orderhub/OrderWorkflowAction$OrderDetailWorkflowAction;

    if-eqz v0, :cond_2

    .line 507
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/OrderHubState;->getNextWorkflowAction()Lcom/squareup/ui/orderhub/OrderWorkflowAction;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/orderhub/OrderWorkflowAction$OrderDetailWorkflowAction;

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/OrderWorkflowAction$OrderDetailWorkflowAction;->getViewedOrderInput()Lcom/squareup/ui/orderhub/ViewedOrderInput;

    move-result-object v0

    .line 508
    instance-of v1, v0, Lcom/squareup/ui/orderhub/ViewedOrderInput$ViewedOrder;

    if-eqz v1, :cond_0

    .line 509
    new-instance v0, Lcom/squareup/ui/orderhub/order/OrderDetailsInput$ViewedOrder;

    .line 510
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/OrderHubState;->getNextWorkflowAction()Lcom/squareup/ui/orderhub/OrderWorkflowAction;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/orderhub/OrderWorkflowAction$OrderDetailWorkflowAction;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/OrderWorkflowAction$OrderDetailWorkflowAction;->getViewedOrderInput()Lcom/squareup/ui/orderhub/ViewedOrderInput;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/orderhub/ViewedOrderInput$ViewedOrder;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/ViewedOrderInput$ViewedOrder;->getCurrentlyViewedOrder()Lcom/squareup/orders/model/Order;

    move-result-object v1

    .line 509
    invoke-direct {v0, v1}, Lcom/squareup/ui/orderhub/order/OrderDetailsInput$ViewedOrder;-><init>(Lcom/squareup/orders/model/Order;)V

    check-cast v0, Lcom/squareup/ui/orderhub/order/OrderDetailsInput;

    :goto_0
    move-object v3, v0

    goto :goto_1

    .line 513
    :cond_0
    instance-of v0, v0, Lcom/squareup/ui/orderhub/ViewedOrderInput$ViewedOrderAfterCancellation;

    if-eqz v0, :cond_1

    .line 514
    new-instance v0, Lcom/squareup/ui/orderhub/order/OrderDetailsInput$RestartingAfterCancellation;

    .line 515
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/OrderHubState;->getNextWorkflowAction()Lcom/squareup/ui/orderhub/OrderWorkflowAction;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/orderhub/OrderWorkflowAction$OrderDetailWorkflowAction;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/OrderWorkflowAction$OrderDetailWorkflowAction;->getViewedOrderInput()Lcom/squareup/ui/orderhub/ViewedOrderInput;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/orderhub/ViewedOrderInput$ViewedOrderAfterCancellation;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/ViewedOrderInput$ViewedOrderAfterCancellation;->getCurrentlyViewedOrder()Lcom/squareup/orders/model/Order;

    move-result-object v2

    .line 516
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/OrderHubState;->getNextWorkflowAction()Lcom/squareup/ui/orderhub/OrderWorkflowAction;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/orderhub/OrderWorkflowAction$OrderDetailWorkflowAction;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/OrderWorkflowAction$OrderDetailWorkflowAction;->getViewedOrderInput()Lcom/squareup/ui/orderhub/ViewedOrderInput;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/orderhub/ViewedOrderInput$ViewedOrderAfterCancellation;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/ViewedOrderInput$ViewedOrderAfterCancellation;->getBill()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v3

    .line 517
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/OrderHubState;->getNextWorkflowAction()Lcom/squareup/ui/orderhub/OrderWorkflowAction;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/orderhub/OrderWorkflowAction$OrderDetailWorkflowAction;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/OrderWorkflowAction$OrderDetailWorkflowAction;->getViewedOrderInput()Lcom/squareup/ui/orderhub/ViewedOrderInput;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/orderhub/ViewedOrderInput$ViewedOrderAfterCancellation;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/ViewedOrderInput$ViewedOrderAfterCancellation;->getCancelReason()Lcom/squareup/ordermanagerdata/CancellationReason;

    move-result-object v4

    .line 518
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/OrderHubState;->getNextWorkflowAction()Lcom/squareup/ui/orderhub/OrderWorkflowAction;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/orderhub/OrderWorkflowAction$OrderDetailWorkflowAction;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/OrderWorkflowAction$OrderDetailWorkflowAction;->getViewedOrderInput()Lcom/squareup/ui/orderhub/ViewedOrderInput;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/orderhub/ViewedOrderInput$ViewedOrderAfterCancellation;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/ViewedOrderInput$ViewedOrderAfterCancellation;->getSelectedLineItems()Ljava/util/Map;

    move-result-object v5

    .line 519
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/OrderHubState;->getNextWorkflowAction()Lcom/squareup/ui/orderhub/OrderWorkflowAction;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/orderhub/OrderWorkflowAction$OrderDetailWorkflowAction;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/OrderWorkflowAction$OrderDetailWorkflowAction;->getViewedOrderInput()Lcom/squareup/ui/orderhub/ViewedOrderInput;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/orderhub/ViewedOrderInput$ViewedOrderAfterCancellation;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/ViewedOrderInput$ViewedOrderAfterCancellation;->getIdempotenceKey()Ljava/lang/String;

    move-result-object v6

    move-object v1, v0

    .line 514
    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/orderhub/order/OrderDetailsInput$RestartingAfterCancellation;-><init>(Lcom/squareup/orders/model/Order;Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/ordermanagerdata/CancellationReason;Ljava/util/Map;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/ui/orderhub/order/OrderDetailsInput;

    goto :goto_0

    .line 525
    :goto_1
    iget-object v0, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->orderDetailsWorkflow:Lcom/squareup/ui/orderhub/order/OrderDetailsWorkflow;

    move-object v2, v0

    check-cast v2, Lcom/squareup/workflow/Workflow;

    const/4 v4, 0x0

    .line 527
    new-instance v0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$getOrderDetailChildRendering$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$getOrderDetailChildRendering$1;-><init>(Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;Lcom/squareup/ui/orderhub/OrderHubState;)V

    move-object v5, v0

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v1, p2

    .line 524
    invoke-static/range {v1 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    return-object p1

    .line 514
    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 506
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Failed requirement."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final getOrderHubStateFromValues(Lcom/squareup/ui/orderhub/master/Filter;Ljava/util/List;ZZLcom/squareup/ui/orderhub/OrderWorkflowAction;Lcom/squareup/ordermanagerdata/ResultState$Failure;ZZLorg/threeten/bp/ZonedDateTime;Ljava/util/Map;Z)Lcom/squareup/ui/orderhub/OrderHubState;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/orderhub/master/Filter;",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;ZZ",
            "Lcom/squareup/ui/orderhub/OrderWorkflowAction;",
            "Lcom/squareup/ordermanagerdata/ResultState$Failure;",
            "ZZ",
            "Lorg/threeten/bp/ZonedDateTime;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/ui/orderhub/OrderQuickActionStatus;",
            ">;Z)",
            "Lcom/squareup/ui/orderhub/OrderHubState;"
        }
    .end annotation

    move-object/from16 v0, p0

    .line 596
    sget-object v1, Lcom/squareup/ui/orderhub/master/Filter;->Companion:Lcom/squareup/ui/orderhub/master/Filter$Companion;

    .line 598
    iget-object v2, v0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->res:Lcom/squareup/util/Res;

    .line 599
    iget-object v3, v0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v3}, Lcom/squareup/settings/server/AccountStatusSettings;->getOrderHubSettings()Lcom/squareup/settings/server/OrderHubSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/settings/server/OrderHubSettings;->getCanSupportUpcomingOrders()Z

    move-result v3

    move-object/from16 v7, p2

    .line 596
    invoke-virtual {v1, v7, v2, v3}, Lcom/squareup/ui/orderhub/master/Filter$Companion;->getGroupedFilters(Ljava/util/List;Lcom/squareup/util/Res;Z)Ljava/util/Map;

    move-result-object v1

    move-object/from16 v2, p1

    .line 601
    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 602
    sget-object v2, Lcom/squareup/ui/orderhub/master/Filter;->Companion:Lcom/squareup/ui/orderhub/master/Filter$Companion;

    invoke-virtual {v2}, Lcom/squareup/ui/orderhub/master/Filter$Companion;->getDEFAULT()Lcom/squareup/ui/orderhub/master/Filter;

    move-result-object v2

    :cond_0
    move-object v9, v2

    .line 606
    invoke-static {v1, v9}, Lkotlin/collections/MapsKt;->getValue(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v10, v2

    check-cast v10, Ljava/util/List;

    .line 789
    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v3

    invoke-static {v3}, Lkotlin/collections/MapsKt;->mapCapacity(I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/LinkedHashMap;-><init>(I)V

    check-cast v2, Ljava/util/Map;

    .line 790
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 791
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 792
    check-cast v3, Ljava/util/Map$Entry;

    .line 790
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 610
    check-cast v3, Ljava/lang/Iterable;

    .line 793
    instance-of v5, v3, Ljava/util/Collection;

    const/4 v6, 0x0

    if-eqz v5, :cond_1

    move-object v5, v3

    check-cast v5, Ljava/util/Collection;

    invoke-interface {v5}, Ljava/util/Collection;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_1

    const/4 v5, 0x0

    goto :goto_3

    .line 795
    :cond_1
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    const/4 v5, 0x0

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/squareup/orders/model/Order;

    .line 610
    invoke-static {v8}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getGroup(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/OrderGroup;

    move-result-object v8

    sget-object v11, Lcom/squareup/protos/client/orders/OrderGroup;->COMPLETED:Lcom/squareup/protos/client/orders/OrderGroup;

    if-eq v8, v11, :cond_3

    const/4 v8, 0x1

    goto :goto_2

    :cond_3
    const/4 v8, 0x0

    :goto_2
    if-eqz v8, :cond_2

    add-int/lit8 v5, v5, 0x1

    if-gez v5, :cond_2

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwCountOverflow()V

    goto :goto_1

    :cond_4
    :goto_3
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 608
    :cond_5
    new-instance v1, Lcom/squareup/ui/orderhub/MasterState;

    invoke-direct {v1, v2, v9}, Lcom/squareup/ui/orderhub/MasterState;-><init>(Ljava/util/Map;Lcom/squareup/ui/orderhub/master/Filter;)V

    if-eqz p8, :cond_6

    if-nez p6, :cond_6

    .line 617
    iget-object v2, v0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->currentTime:Lcom/squareup/time/CurrentTime;

    invoke-interface {v2}, Lcom/squareup/time/CurrentTime;->zonedDateTime()Lorg/threeten/bp/ZonedDateTime;

    move-result-object v2

    move-object v14, v2

    goto :goto_4

    :cond_6
    move-object/from16 v14, p9

    .line 622
    :goto_4
    new-instance v2, Lcom/squareup/ui/orderhub/DetailState;

    .line 626
    invoke-direct {v0, v9}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->canLoadMoreOrders(Lcom/squareup/ui/orderhub/master/Filter;)Z

    move-result v12

    move-object v8, v2

    move/from16 v11, p7

    move-object/from16 v13, p6

    move-object/from16 v15, p10

    .line 622
    invoke-direct/range {v8 .. v15}, Lcom/squareup/ui/orderhub/DetailState;-><init>(Lcom/squareup/ui/orderhub/master/Filter;Ljava/util/List;ZZLcom/squareup/ordermanagerdata/ResultState$Failure;Lorg/threeten/bp/ZonedDateTime;Ljava/util/Map;)V

    .line 632
    new-instance v3, Lcom/squareup/ui/orderhub/OrderHubState;

    move-object v4, v3

    move/from16 v5, p3

    move/from16 v6, p4

    move-object/from16 v7, p2

    move-object v8, v1

    move-object v9, v2

    move-object/from16 v10, p5

    move/from16 v11, p11

    invoke-direct/range {v4 .. v11}, Lcom/squareup/ui/orderhub/OrderHubState;-><init>(ZZLjava/util/List;Lcom/squareup/ui/orderhub/MasterState;Lcom/squareup/ui/orderhub/DetailState;Lcom/squareup/ui/orderhub/OrderWorkflowAction;Z)V

    return-object v3
.end method

.method static synthetic getOrderHubStateFromValues$default(Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;Lcom/squareup/ui/orderhub/master/Filter;Ljava/util/List;ZZLcom/squareup/ui/orderhub/OrderWorkflowAction;Lcom/squareup/ordermanagerdata/ResultState$Failure;ZZLorg/threeten/bp/ZonedDateTime;Ljava/util/Map;ZILjava/lang/Object;)Lcom/squareup/ui/orderhub/OrderHubState;
    .locals 15

    move/from16 v0, p12

    and-int/lit8 v1, v0, 0x20

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 589
    move-object v1, v2

    check-cast v1, Lcom/squareup/ordermanagerdata/ResultState$Failure;

    move-object v9, v1

    goto :goto_0

    :cond_0
    move-object/from16 v9, p6

    :goto_0
    and-int/lit8 v1, v0, 0x40

    const/4 v3, 0x0

    if-eqz v1, :cond_1

    const/4 v10, 0x0

    goto :goto_1

    :cond_1
    move/from16 v10, p7

    :goto_1
    and-int/lit16 v1, v0, 0x80

    if-eqz v1, :cond_2

    const/4 v11, 0x0

    goto :goto_2

    :cond_2
    move/from16 v11, p8

    :goto_2
    and-int/lit16 v1, v0, 0x100

    if-eqz v1, :cond_3

    .line 592
    move-object v1, v2

    check-cast v1, Lorg/threeten/bp/ZonedDateTime;

    move-object v12, v1

    goto :goto_3

    :cond_3
    move-object/from16 v12, p9

    :goto_3
    and-int/lit16 v0, v0, 0x200

    if-eqz v0, :cond_4

    .line 593
    invoke-static {}, Lkotlin/collections/MapsKt;->emptyMap()Ljava/util/Map;

    move-result-object v0

    move-object v13, v0

    goto :goto_4

    :cond_4
    move-object/from16 v13, p10

    :goto_4
    move-object v3, p0

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move/from16 v6, p3

    move/from16 v7, p4

    move-object/from16 v8, p5

    move/from16 v14, p11

    invoke-direct/range {v3 .. v14}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->getOrderHubStateFromValues(Lcom/squareup/ui/orderhub/master/Filter;Ljava/util/List;ZZLcom/squareup/ui/orderhub/OrderWorkflowAction;Lcom/squareup/ordermanagerdata/ResultState$Failure;ZZLorg/threeten/bp/ZonedDateTime;Ljava/util/Map;Z)Lcom/squareup/ui/orderhub/OrderHubState;

    move-result-object v0

    return-object v0
.end method

.method private final getStateAfterFilterSelected(Lcom/squareup/ui/orderhub/OrderHubState;Lcom/squareup/ui/orderhub/master/Filter;)Lcom/squareup/ui/orderhub/OrderHubState;
    .locals 14

    .line 555
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/OrderHubState;->getSyncedOrders()Ljava/util/List;

    move-result-object v2

    .line 556
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/OrderHubState;->isReadOnly()Z

    move-result v3

    .line 557
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/OrderHubState;->isMasterDetail()Z

    move-result v4

    .line 558
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/OrderHubState;->getDetailState()Lcom/squareup/ui/orderhub/DetailState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/DetailState;->getSyncingError()Lcom/squareup/ordermanagerdata/ResultState$Failure;

    move-result-object v6

    .line 559
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/OrderHubState;->getDetailState()Lcom/squareup/ui/orderhub/DetailState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/DetailState;->getLastSyncDate()Lorg/threeten/bp/ZonedDateTime;

    move-result-object v9

    .line 560
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/OrderHubState;->getNextWorkflowAction()Lcom/squareup/ui/orderhub/OrderWorkflowAction;

    move-result-object v5

    .line 561
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/OrderHubState;->isQuickActionsEnabled()Z

    move-result v11

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v10, 0x0

    const/16 v12, 0x2c0

    const/4 v13, 0x0

    move-object v0, p0

    move-object/from16 v1, p2

    .line 553
    invoke-static/range {v0 .. v13}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->getOrderHubStateFromValues$default(Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;Lcom/squareup/ui/orderhub/master/Filter;Ljava/util/List;ZZLcom/squareup/ui/orderhub/OrderWorkflowAction;Lcom/squareup/ordermanagerdata/ResultState$Failure;ZZLorg/threeten/bp/ZonedDateTime;Ljava/util/Map;ZILjava/lang/Object;)Lcom/squareup/ui/orderhub/OrderHubState;

    move-result-object v0

    return-object v0
.end method

.method private final getStateAfterOrderViewFinished(Lcom/squareup/ui/orderhub/OrderHubState;Lcom/squareup/orders/model/Order;)Lcom/squareup/ui/orderhub/OrderHubState;
    .locals 10

    .line 648
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/OrderHubState;->getSyncedOrders()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 798
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 799
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 800
    check-cast v3, Lcom/squareup/orders/model/Order;

    .line 649
    iget-object v4, p2, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    iget-object v5, v3, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v2, 0x1

    move-object v3, p2

    .line 654
    :cond_0
    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 801
    :cond_1
    check-cast v1, Ljava/util/List;

    if-eqz v2, :cond_2

    .line 657
    invoke-direct {p0, p1, v1}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->getStateWhenOrdersUpdated(Lcom/squareup/ui/orderhub/OrderHubState;Ljava/util/List;)Lcom/squareup/ui/orderhub/OrderHubState;

    move-result-object p1

    :cond_2
    move-object v0, p1

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 664
    sget-object p1, Lcom/squareup/ui/orderhub/OrderWorkflowAction$OrderNoWorkflowAction;->INSTANCE:Lcom/squareup/ui/orderhub/OrderWorkflowAction$OrderNoWorkflowAction;

    move-object v6, p1

    check-cast v6, Lcom/squareup/ui/orderhub/OrderWorkflowAction;

    const/4 v7, 0x0

    const/16 v8, 0x5f

    const/4 v9, 0x0

    invoke-static/range {v0 .. v9}, Lcom/squareup/ui/orderhub/OrderHubState;->copy$default(Lcom/squareup/ui/orderhub/OrderHubState;ZZLjava/util/List;Lcom/squareup/ui/orderhub/MasterState;Lcom/squareup/ui/orderhub/DetailState;Lcom/squareup/ui/orderhub/OrderWorkflowAction;ZILjava/lang/Object;)Lcom/squareup/ui/orderhub/OrderHubState;

    move-result-object p1

    return-object p1
.end method

.method private final getStateAfterPaginationCompleted(Lcom/squareup/ui/orderhub/OrderHubState;Lcom/squareup/ordermanagerdata/ResultState$Failure;)Lcom/squareup/ui/orderhub/OrderHubState;
    .locals 15

    .line 688
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/ui/orderhub/OrderHubState;->getMasterState()Lcom/squareup/ui/orderhub/MasterState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/MasterState;->getSelectedFilter()Lcom/squareup/ui/orderhub/master/Filter;

    move-result-object v2

    .line 689
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/ui/orderhub/OrderHubState;->getSyncedOrders()Ljava/util/List;

    move-result-object v3

    .line 690
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/ui/orderhub/OrderHubState;->isReadOnly()Z

    move-result v4

    .line 691
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/ui/orderhub/OrderHubState;->isMasterDetail()Z

    move-result v5

    .line 694
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/ui/orderhub/OrderHubState;->getDetailState()Lcom/squareup/ui/orderhub/DetailState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/DetailState;->getLastSyncDate()Lorg/threeten/bp/ZonedDateTime;

    move-result-object v10

    .line 695
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/ui/orderhub/OrderHubState;->getNextWorkflowAction()Lcom/squareup/ui/orderhub/OrderWorkflowAction;

    move-result-object v6

    .line 696
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/ui/orderhub/OrderHubState;->isQuickActionsEnabled()Z

    move-result v12

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v11, 0x0

    const/16 v13, 0x240

    const/4 v14, 0x0

    move-object v1, p0

    move-object/from16 v7, p2

    .line 687
    invoke-static/range {v1 .. v14}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->getOrderHubStateFromValues$default(Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;Lcom/squareup/ui/orderhub/master/Filter;Ljava/util/List;ZZLcom/squareup/ui/orderhub/OrderWorkflowAction;Lcom/squareup/ordermanagerdata/ResultState$Failure;ZZLorg/threeten/bp/ZonedDateTime;Ljava/util/Map;ZILjava/lang/Object;)Lcom/squareup/ui/orderhub/OrderHubState;

    move-result-object v0

    return-object v0
.end method

.method static synthetic getStateAfterPaginationCompleted$default(Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;Lcom/squareup/ui/orderhub/OrderHubState;Lcom/squareup/ordermanagerdata/ResultState$Failure;ILjava/lang/Object;)Lcom/squareup/ui/orderhub/OrderHubState;
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 685
    check-cast p2, Lcom/squareup/ordermanagerdata/ResultState$Failure;

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->getStateAfterPaginationCompleted(Lcom/squareup/ui/orderhub/OrderHubState;Lcom/squareup/ordermanagerdata/ResultState$Failure;)Lcom/squareup/ui/orderhub/OrderHubState;

    move-result-object p0

    return-object p0
.end method

.method private final getStateAfterPaginationRequested(Lcom/squareup/ui/orderhub/OrderHubState;)Lcom/squareup/ui/orderhub/OrderHubState;
    .locals 15

    .line 671
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/ui/orderhub/OrderHubState;->getMasterState()Lcom/squareup/ui/orderhub/MasterState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/MasterState;->getSelectedFilter()Lcom/squareup/ui/orderhub/master/Filter;

    move-result-object v2

    .line 672
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/ui/orderhub/OrderHubState;->getSyncedOrders()Ljava/util/List;

    move-result-object v3

    .line 673
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/ui/orderhub/OrderHubState;->isReadOnly()Z

    move-result v4

    .line 674
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/ui/orderhub/OrderHubState;->isMasterDetail()Z

    move-result v5

    .line 675
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/ui/orderhub/OrderHubState;->getDetailState()Lcom/squareup/ui/orderhub/DetailState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/DetailState;->getSyncingError()Lcom/squareup/ordermanagerdata/ResultState$Failure;

    move-result-object v7

    .line 677
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/ui/orderhub/OrderHubState;->getDetailState()Lcom/squareup/ui/orderhub/DetailState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/DetailState;->getLastSyncDate()Lorg/threeten/bp/ZonedDateTime;

    move-result-object v10

    .line 678
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/ui/orderhub/OrderHubState;->getNextWorkflowAction()Lcom/squareup/ui/orderhub/OrderWorkflowAction;

    move-result-object v6

    .line 679
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/ui/orderhub/OrderHubState;->isQuickActionsEnabled()Z

    move-result v12

    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v11, 0x0

    const/16 v13, 0x280

    const/4 v14, 0x0

    move-object v1, p0

    .line 670
    invoke-static/range {v1 .. v14}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->getOrderHubStateFromValues$default(Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;Lcom/squareup/ui/orderhub/master/Filter;Ljava/util/List;ZZLcom/squareup/ui/orderhub/OrderWorkflowAction;Lcom/squareup/ordermanagerdata/ResultState$Failure;ZZLorg/threeten/bp/ZonedDateTime;Ljava/util/Map;ZILjava/lang/Object;)Lcom/squareup/ui/orderhub/OrderHubState;

    move-result-object v0

    return-object v0
.end method

.method private final getStateWhenOrdersUpdated(Lcom/squareup/ui/orderhub/OrderHubState;Ljava/util/List;)Lcom/squareup/ui/orderhub/OrderHubState;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/orderhub/OrderHubState;",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;)",
            "Lcom/squareup/ui/orderhub/OrderHubState;"
        }
    .end annotation

    .line 570
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/ui/orderhub/OrderHubState;->getMasterState()Lcom/squareup/ui/orderhub/MasterState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/MasterState;->getSelectedFilter()Lcom/squareup/ui/orderhub/master/Filter;

    move-result-object v2

    .line 572
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/ui/orderhub/OrderHubState;->isReadOnly()Z

    move-result v4

    .line 573
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/ui/orderhub/OrderHubState;->isMasterDetail()Z

    move-result v5

    .line 574
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/ui/orderhub/OrderHubState;->getDetailState()Lcom/squareup/ui/orderhub/DetailState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/DetailState;->getSyncingError()Lcom/squareup/ordermanagerdata/ResultState$Failure;

    move-result-object v7

    .line 576
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/ui/orderhub/OrderHubState;->getDetailState()Lcom/squareup/ui/orderhub/DetailState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/DetailState;->getLastSyncDate()Lorg/threeten/bp/ZonedDateTime;

    move-result-object v10

    .line 577
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/ui/orderhub/OrderHubState;->getDetailState()Lcom/squareup/ui/orderhub/DetailState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/DetailState;->getOrderQuickActionStatusMap()Ljava/util/Map;

    move-result-object v11

    .line 578
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/ui/orderhub/OrderHubState;->getNextWorkflowAction()Lcom/squareup/ui/orderhub/OrderWorkflowAction;

    move-result-object v6

    .line 579
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/ui/orderhub/OrderHubState;->isQuickActionsEnabled()Z

    move-result v12

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/16 v13, 0x40

    const/4 v14, 0x0

    move-object v1, p0

    move-object/from16 v3, p2

    .line 569
    invoke-static/range {v1 .. v14}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->getOrderHubStateFromValues$default(Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;Lcom/squareup/ui/orderhub/master/Filter;Ljava/util/List;ZZLcom/squareup/ui/orderhub/OrderWorkflowAction;Lcom/squareup/ordermanagerdata/ResultState$Failure;ZZLorg/threeten/bp/ZonedDateTime;Ljava/util/Map;ZILjava/lang/Object;)Lcom/squareup/ui/orderhub/OrderHubState;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public initialState(Lcom/squareup/ui/orderhub/OrderHubWorkflowInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/ui/orderhub/OrderHubState;
    .locals 13

    const-string p2, "props"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    sget-object p2, Lcom/squareup/ui/orderhub/OrderHubWorkflowInput$NoInput;->INSTANCE:Lcom/squareup/ui/orderhub/OrderHubWorkflowInput$NoInput;

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_0

    .line 101
    invoke-direct {p0}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->getInitialState()Lcom/squareup/ui/orderhub/OrderHubState;

    move-result-object p1

    goto :goto_0

    .line 103
    :cond_0
    instance-of p2, p1, Lcom/squareup/ui/orderhub/OrderHubWorkflowInput$RestartingAfterCancellation;

    if-eqz p2, :cond_1

    .line 104
    invoke-direct {p0}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->getInitialState()Lcom/squareup/ui/orderhub/OrderHubState;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 105
    new-instance p2, Lcom/squareup/ui/orderhub/OrderWorkflowAction$OrderDetailWorkflowAction;

    .line 106
    new-instance v12, Lcom/squareup/ui/orderhub/ViewedOrderInput$ViewedOrderAfterCancellation;

    .line 107
    check-cast p1, Lcom/squareup/ui/orderhub/OrderHubWorkflowInput$RestartingAfterCancellation;

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/OrderHubWorkflowInput$RestartingAfterCancellation;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v7

    .line 108
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/OrderHubWorkflowInput$RestartingAfterCancellation;->getBill()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v8

    .line 109
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/OrderHubWorkflowInput$RestartingAfterCancellation;->getCancelReason()Lcom/squareup/ordermanagerdata/CancellationReason;

    move-result-object v10

    .line 110
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/OrderHubWorkflowInput$RestartingAfterCancellation;->getSelectedLineItems()Ljava/util/Map;

    move-result-object v9

    .line 111
    iget-object p1, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->uuidGenerator:Lcom/squareup/log/UUIDGenerator;

    invoke-interface {p1}, Lcom/squareup/log/UUIDGenerator;->randomUUID()Ljava/lang/String;

    move-result-object v11

    const-string p1, "uuidGenerator.randomUUID()"

    invoke-static {v11, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v6, v12

    .line 106
    invoke-direct/range {v6 .. v11}, Lcom/squareup/ui/orderhub/ViewedOrderInput$ViewedOrderAfterCancellation;-><init>(Lcom/squareup/orders/model/Order;Lcom/squareup/billhistory/model/BillHistory;Ljava/util/Map;Lcom/squareup/ordermanagerdata/CancellationReason;Ljava/lang/String;)V

    check-cast v12, Lcom/squareup/ui/orderhub/ViewedOrderInput;

    .line 105
    invoke-direct {p2, v12}, Lcom/squareup/ui/orderhub/OrderWorkflowAction$OrderDetailWorkflowAction;-><init>(Lcom/squareup/ui/orderhub/ViewedOrderInput;)V

    move-object v6, p2

    check-cast v6, Lcom/squareup/ui/orderhub/OrderWorkflowAction;

    const/4 v7, 0x0

    const/16 v8, 0x5f

    const/4 v9, 0x0

    .line 104
    invoke-static/range {v0 .. v9}, Lcom/squareup/ui/orderhub/OrderHubState;->copy$default(Lcom/squareup/ui/orderhub/OrderHubState;ZZLjava/util/List;Lcom/squareup/ui/orderhub/MasterState;Lcom/squareup/ui/orderhub/DetailState;Lcom/squareup/ui/orderhub/OrderWorkflowAction;ZILjava/lang/Object;)Lcom/squareup/ui/orderhub/OrderHubState;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 77
    check-cast p1, Lcom/squareup/ui/orderhub/OrderHubWorkflowInput;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->initialState(Lcom/squareup/ui/orderhub/OrderHubWorkflowInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/ui/orderhub/OrderHubState;

    move-result-object p1

    return-object p1
.end method

.method public onPropsChanged(Lcom/squareup/ui/orderhub/OrderHubWorkflowInput;Lcom/squareup/ui/orderhub/OrderHubWorkflowInput;Lcom/squareup/ui/orderhub/OrderHubState;)Lcom/squareup/ui/orderhub/OrderHubState;
    .locals 12

    const-string v0, "old"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "new"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 124
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    return-object p3

    .line 129
    :cond_0
    sget-object p1, Lcom/squareup/ui/orderhub/OrderHubWorkflowInput$NoInput;->INSTANCE:Lcom/squareup/ui/orderhub/OrderHubWorkflowInput$NoInput;

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    sget-object p1, Lcom/squareup/ui/orderhub/OrderWorkflowAction$OrderNoWorkflowAction;->INSTANCE:Lcom/squareup/ui/orderhub/OrderWorkflowAction$OrderNoWorkflowAction;

    move-object v6, p1

    check-cast v6, Lcom/squareup/ui/orderhub/OrderWorkflowAction;

    const/4 v7, 0x0

    const/16 v8, 0x5f

    const/4 v9, 0x0

    move-object v0, p3

    invoke-static/range {v0 .. v9}, Lcom/squareup/ui/orderhub/OrderHubState;->copy$default(Lcom/squareup/ui/orderhub/OrderHubState;ZZLjava/util/List;Lcom/squareup/ui/orderhub/MasterState;Lcom/squareup/ui/orderhub/DetailState;Lcom/squareup/ui/orderhub/OrderWorkflowAction;ZILjava/lang/Object;)Lcom/squareup/ui/orderhub/OrderHubState;

    move-result-object p1

    goto :goto_0

    .line 130
    :cond_1
    instance-of p1, p2, Lcom/squareup/ui/orderhub/OrderHubWorkflowInput$RestartingAfterCancellation;

    if-eqz p1, :cond_2

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 132
    new-instance p1, Lcom/squareup/ui/orderhub/OrderWorkflowAction$OrderDetailWorkflowAction;

    .line 133
    new-instance v0, Lcom/squareup/ui/orderhub/ViewedOrderInput$ViewedOrderAfterCancellation;

    .line 134
    check-cast p2, Lcom/squareup/ui/orderhub/OrderHubWorkflowInput$RestartingAfterCancellation;

    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/OrderHubWorkflowInput$RestartingAfterCancellation;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v7

    .line 135
    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/OrderHubWorkflowInput$RestartingAfterCancellation;->getBill()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v8

    .line 136
    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/OrderHubWorkflowInput$RestartingAfterCancellation;->getCancelReason()Lcom/squareup/ordermanagerdata/CancellationReason;

    move-result-object v10

    .line 137
    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/OrderHubWorkflowInput$RestartingAfterCancellation;->getSelectedLineItems()Ljava/util/Map;

    move-result-object v9

    .line 138
    iget-object p2, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->uuidGenerator:Lcom/squareup/log/UUIDGenerator;

    invoke-interface {p2}, Lcom/squareup/log/UUIDGenerator;->randomUUID()Ljava/lang/String;

    move-result-object v11

    const-string p2, "uuidGenerator.randomUUID()"

    invoke-static {v11, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v6, v0

    .line 133
    invoke-direct/range {v6 .. v11}, Lcom/squareup/ui/orderhub/ViewedOrderInput$ViewedOrderAfterCancellation;-><init>(Lcom/squareup/orders/model/Order;Lcom/squareup/billhistory/model/BillHistory;Ljava/util/Map;Lcom/squareup/ordermanagerdata/CancellationReason;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/ui/orderhub/ViewedOrderInput;

    .line 132
    invoke-direct {p1, v0}, Lcom/squareup/ui/orderhub/OrderWorkflowAction$OrderDetailWorkflowAction;-><init>(Lcom/squareup/ui/orderhub/ViewedOrderInput;)V

    move-object v6, p1

    check-cast v6, Lcom/squareup/ui/orderhub/OrderWorkflowAction;

    const/4 v7, 0x0

    const/16 v8, 0x5f

    const/4 v9, 0x0

    move-object v0, p3

    .line 131
    invoke-static/range {v0 .. v9}, Lcom/squareup/ui/orderhub/OrderHubState;->copy$default(Lcom/squareup/ui/orderhub/OrderHubState;ZZLjava/util/List;Lcom/squareup/ui/orderhub/MasterState;Lcom/squareup/ui/orderhub/DetailState;Lcom/squareup/ui/orderhub/OrderWorkflowAction;ZILjava/lang/Object;)Lcom/squareup/ui/orderhub/OrderHubState;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic onPropsChanged(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 77
    check-cast p1, Lcom/squareup/ui/orderhub/OrderHubWorkflowInput;

    check-cast p2, Lcom/squareup/ui/orderhub/OrderHubWorkflowInput;

    check-cast p3, Lcom/squareup/ui/orderhub/OrderHubState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->onPropsChanged(Lcom/squareup/ui/orderhub/OrderHubWorkflowInput;Lcom/squareup/ui/orderhub/OrderHubWorkflowInput;Lcom/squareup/ui/orderhub/OrderHubState;)Lcom/squareup/ui/orderhub/OrderHubState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 77
    check-cast p1, Lcom/squareup/ui/orderhub/OrderHubWorkflowInput;

    check-cast p2, Lcom/squareup/ui/orderhub/OrderHubState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->render(Lcom/squareup/ui/orderhub/OrderHubWorkflowInput;Lcom/squareup/ui/orderhub/OrderHubState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/ui/orderhub/OrderHubWorkflowInput;Lcom/squareup/ui/orderhub/OrderHubState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/orderhub/OrderHubWorkflowInput;",
            "Lcom/squareup/ui/orderhub/OrderHubState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/ui/orderhub/OrderHubState;",
            "-",
            "Lcom/squareup/ui/orderhub/OrderHubResult;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v9, p3

    const-string v2, "props"

    move-object/from16 v3, p1

    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "state"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "context"

    invoke-static {v9, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 150
    iget-object v2, v0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->orderQuickActionsEnabledPreference:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v2}, Lcom/f2prateek/rx/preferences2/Preference;->asObservable()Lio/reactivex/Observable;

    move-result-object v2

    .line 151
    new-instance v3, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$1;

    invoke-direct {v3, v0, v1}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$1;-><init>(Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;Lcom/squareup/ui/orderhub/OrderHubState;)V

    check-cast v3, Lio/reactivex/functions/Consumer;

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    .line 157
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/ui/orderhub/OrderHubState;->getNextWorkflowAction()Lcom/squareup/ui/orderhub/OrderWorkflowAction;

    move-result-object v2

    instance-of v2, v2, Lcom/squareup/ui/orderhub/OrderWorkflowAction$OrderMarkShippedWorkflowAction;

    const/4 v10, 0x0

    const/4 v8, 0x0

    if-nez v2, :cond_7

    .line 158
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/ui/orderhub/OrderHubState;->getDetailState()Lcom/squareup/ui/orderhub/DetailState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/ui/orderhub/DetailState;->getOrderQuickActionStatusMap()Ljava/util/Map;

    move-result-object v2

    .line 714
    new-instance v3, Ljava/util/LinkedHashMap;

    invoke-direct {v3}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v3, Ljava/util/Map;

    .line 715
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 161
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/ui/orderhub/OrderQuickActionStatus;

    invoke-virtual {v5}, Lcom/squareup/ui/orderhub/OrderQuickActionStatus;->isLoading()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/ui/orderhub/OrderQuickActionStatus;

    invoke-virtual {v5}, Lcom/squareup/ui/orderhub/OrderQuickActionStatus;->getHasError()Z

    move-result v5

    if-nez v5, :cond_1

    const/4 v5, 0x1

    goto :goto_1

    :cond_1
    const/4 v5, 0x0

    :goto_1
    if-eqz v5, :cond_0

    .line 717
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 721
    :cond_2
    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 164
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/ui/orderhub/OrderQuickActionStatus;

    invoke-virtual {v4}, Lcom/squareup/ui/orderhub/OrderQuickActionStatus;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v4

    invoke-static {v4}, Lcom/squareup/ui/orderhub/util/proto/OrdersKt;->getPrimaryAction(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/Action;

    move-result-object v4

    const-string v5, "Required value was null."

    if-eqz v4, :cond_5

    iget-object v4, v4, Lcom/squareup/protos/client/orders/Action;->type:Lcom/squareup/protos/client/orders/Action$Type;

    sget-object v6, Lcom/squareup/protos/client/orders/Action$Type;->MARK_IN_PROGRESS:Lcom/squareup/protos/client/orders/Action$Type;

    if-ne v4, v6, :cond_3

    .line 165
    iget-object v4, v0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->orderRepository:Lcom/squareup/ordermanagerdata/OrderRepository;

    .line 166
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/ui/orderhub/OrderQuickActionStatus;

    invoke-virtual {v5}, Lcom/squareup/ui/orderhub/OrderQuickActionStatus;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v5

    invoke-interface {v4, v5, v8}, Lcom/squareup/ordermanagerdata/OrderRepository;->markOrderInProgress(Lcom/squareup/orders/model/Order;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v4

    .line 722
    sget-object v5, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v5, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$$special$$inlined$asWorker$1;

    invoke-direct {v5, v4, v8}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$$special$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v5, Lkotlin/jvm/functions/Function1;

    .line 723
    invoke-static {v5}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v4

    .line 724
    const-class v5, Lcom/squareup/ordermanagerdata/ResultState;

    sget-object v6, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class v7, Lcom/squareup/orders/model/Order;

    invoke-static {v7}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v7

    invoke-virtual {v6, v7}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object v6

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;Lkotlin/reflect/KTypeProjection;)Lkotlin/reflect/KType;

    move-result-object v5

    new-instance v6, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v6, v5, v4}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v6, Lcom/squareup/workflow/Worker;

    goto :goto_3

    .line 169
    :cond_3
    iget-object v4, v0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->orderRepository:Lcom/squareup/ordermanagerdata/OrderRepository;

    .line 171
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/ui/orderhub/OrderQuickActionStatus;

    invoke-virtual {v6}, Lcom/squareup/ui/orderhub/OrderQuickActionStatus;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v6

    .line 172
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/squareup/ui/orderhub/OrderQuickActionStatus;

    invoke-virtual {v7}, Lcom/squareup/ui/orderhub/OrderQuickActionStatus;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v7

    invoke-static {v7}, Lcom/squareup/ui/orderhub/util/proto/OrdersKt;->getPrimaryAction(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/Action;

    move-result-object v7

    if-eqz v7, :cond_4

    iget-object v5, v7, Lcom/squareup/protos/client/orders/Action;->type:Lcom/squareup/protos/client/orders/Action$Type;

    const-string v7, "requireNotNull(it.value.order.primaryAction).type"

    invoke-static {v5, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 170
    invoke-interface {v4, v6, v5}, Lcom/squareup/ordermanagerdata/OrderRepository;->transitionOrder(Lcom/squareup/orders/model/Order;Lcom/squareup/protos/client/orders/Action$Type;)Lio/reactivex/Single;

    move-result-object v4

    .line 725
    sget-object v5, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v5, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$$special$$inlined$asWorker$2;

    invoke-direct {v5, v4, v8}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$$special$$inlined$asWorker$2;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v5, Lkotlin/jvm/functions/Function1;

    .line 726
    invoke-static {v5}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v4

    .line 727
    const-class v5, Lcom/squareup/ordermanagerdata/ResultState;

    sget-object v6, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class v7, Lcom/squareup/orders/model/Order;

    invoke-static {v7}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v7

    invoke-virtual {v6, v7}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object v6

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;Lkotlin/reflect/KTypeProjection;)Lkotlin/reflect/KType;

    move-result-object v5

    new-instance v6, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v6, v5, v4}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v6, Lcom/squareup/workflow/Worker;

    .line 179
    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v5, ".key  "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v5, ".value.order.version"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 180
    new-instance v5, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$$inlined$let$lambda$1;

    invoke-direct {v5, v3, v0, v9, v1}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$$inlined$let$lambda$1;-><init>(Ljava/util/Map$Entry;Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;Lcom/squareup/workflow/RenderContext;Lcom/squareup/ui/orderhub/OrderHubState;)V

    check-cast v5, Lkotlin/jvm/functions/Function1;

    invoke-interface {v9, v6, v4, v5}, Lcom/squareup/workflow/RenderContext;->runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    goto/16 :goto_2

    .line 172
    :cond_4
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    .line 164
    :cond_5
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    .line 198
    :cond_6
    sget-object v2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    .line 202
    :cond_7
    iget-object v2, v0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->orderRepository:Lcom/squareup/ordermanagerdata/OrderRepository;

    invoke-interface {v2}, Lcom/squareup/ordermanagerdata/OrderRepository;->syncedOrders()Lio/reactivex/Observable;

    move-result-object v2

    .line 729
    sget-object v3, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object v2

    const-string v11, "this.toFlowable(BUFFER)"

    invoke-static {v2, v11}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lorg/reactivestreams/Publisher;

    const-string v12, "null cannot be cast to non-null type org.reactivestreams.Publisher<T>"

    if-eqz v2, :cond_16

    .line 731
    invoke-static {v2}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v2

    .line 732
    const-class v3, Lcom/squareup/ordermanagerdata/ResultState;

    sget-object v4, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class v5, Ljava/util/List;

    sget-object v6, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class v7, Lcom/squareup/orders/model/Order;

    invoke-static {v7}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v7

    invoke-virtual {v6, v7}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object v6

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;Lkotlin/reflect/KTypeProjection;)Lkotlin/reflect/KType;

    move-result-object v5

    invoke-virtual {v4, v5}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object v4

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;Lkotlin/reflect/KTypeProjection;)Lkotlin/reflect/KType;

    move-result-object v3

    new-instance v4, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v4, v3, v2}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v4, Lcom/squareup/workflow/Worker;

    .line 204
    new-instance v2, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$3;

    invoke-direct {v2, v0, v1}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$3;-><init>(Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;Lcom/squareup/ui/orderhub/OrderHubState;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    const-string v3, "all-orders"

    .line 201
    invoke-interface {v9, v4, v3, v2}, Lcom/squareup/workflow/RenderContext;->runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    .line 216
    iget-object v2, v0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->orderRepository:Lcom/squareup/ordermanagerdata/OrderRepository;

    invoke-interface {v2}, Lcom/squareup/ordermanagerdata/OrderRepository;->unknownOrders()Lio/reactivex/Observable;

    move-result-object v2

    .line 217
    sget-object v3, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$4;->INSTANCE:Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$4;

    check-cast v3, Lio/reactivex/functions/Predicate;

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v2

    .line 218
    new-instance v3, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$5;

    invoke-direct {v3, v0}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$5;-><init>(Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;)V

    check-cast v3, Lio/reactivex/functions/Consumer;

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v2

    const-string v3, "orderRepository.unknownO\u2026.markAllOrdersAsKnown() }"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 733
    sget-object v3, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object v2

    invoke-static {v2, v11}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lorg/reactivestreams/Publisher;

    if-eqz v2, :cond_15

    .line 735
    invoke-static {v2}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v2

    .line 736
    const-class v3, Lcom/squareup/ordermanagerdata/ResultState;

    sget-object v4, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class v5, Ljava/util/List;

    sget-object v6, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class v7, Lcom/squareup/orders/model/Order;

    invoke-static {v7}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v7

    invoke-virtual {v6, v7}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object v6

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;Lkotlin/reflect/KTypeProjection;)Lkotlin/reflect/KType;

    move-result-object v5

    invoke-virtual {v4, v5}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object v4

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;Lkotlin/reflect/KTypeProjection;)Lkotlin/reflect/KType;

    move-result-object v3

    new-instance v4, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v4, v3, v2}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v4, Lcom/squareup/workflow/Worker;

    .line 221
    sget-object v2, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$6;->INSTANCE:Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$6;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    const-string v3, "unknown-orders"

    .line 215
    invoke-interface {v9, v4, v3, v2}, Lcom/squareup/workflow/RenderContext;->runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    .line 224
    iget-object v2, v0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->orderRepository:Lcom/squareup/ordermanagerdata/OrderRepository;

    invoke-interface {v2}, Lcom/squareup/ordermanagerdata/OrderRepository;->markAllOrdersAsKnown()Lio/reactivex/Single;

    move-result-object v2

    .line 737
    sget-object v3, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v3, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$$inlined$asWorker$1;

    invoke-direct {v3, v2, v8}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    .line 738
    invoke-static {v3}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v2

    .line 739
    const-class v3, Lcom/squareup/ordermanagerdata/ResultState;

    sget-object v4, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class v5, Lkotlin/Unit;

    invoke-static {v5}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v5

    invoke-virtual {v4, v5}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object v4

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;Lkotlin/reflect/KTypeProjection;)Lkotlin/reflect/KType;

    move-result-object v3

    new-instance v4, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v4, v3, v2}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v4, Lcom/squareup/workflow/Worker;

    .line 227
    sget-object v2, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$7;->INSTANCE:Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$7;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    const-string v3, "mark-orders-known"

    .line 223
    invoke-interface {v9, v4, v3, v2}, Lcom/squareup/workflow/RenderContext;->runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    .line 230
    iget-object v2, v0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->orderRepository:Lcom/squareup/ordermanagerdata/OrderRepository;

    invoke-interface {v2}, Lcom/squareup/ordermanagerdata/OrderRepository;->refreshSyncedOrders()Lio/reactivex/Single;

    move-result-object v2

    .line 740
    sget-object v3, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v3, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$$inlined$asWorker$2;

    invoke-direct {v3, v2, v8}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$$inlined$asWorker$2;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    .line 741
    invoke-static {v3}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v2

    .line 742
    const-class v3, Lcom/squareup/ordermanagerdata/ResultState;

    sget-object v4, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class v5, Lkotlin/Unit;

    invoke-static {v5}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v5

    invoke-virtual {v4, v5}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object v4

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;Lkotlin/reflect/KTypeProjection;)Lkotlin/reflect/KType;

    move-result-object v3

    new-instance v4, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v4, v3, v2}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v4, Lcom/squareup/workflow/Worker;

    .line 233
    sget-object v2, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$8;->INSTANCE:Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$8;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    const-string v3, "refresh-all-orders"

    .line 229
    invoke-interface {v9, v4, v3, v2}, Lcom/squareup/workflow/RenderContext;->runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    .line 236
    iget-object v2, v0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->device:Lcom/squareup/util/Device;

    invoke-interface {v2}, Lcom/squareup/util/Device;->getScreenSize()Lio/reactivex/Observable;

    move-result-object v2

    .line 743
    sget-object v3, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object v2

    invoke-static {v2, v11}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lorg/reactivestreams/Publisher;

    if-eqz v2, :cond_14

    .line 745
    invoke-static {v2}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v2

    .line 746
    const-class v3, Lcom/squareup/util/DeviceScreenSizeInfo;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v3

    new-instance v4, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v4, v3, v2}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v3, v4

    check-cast v3, Lcom/squareup/workflow/Worker;

    const/4 v4, 0x0

    .line 237
    new-instance v2, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$9;

    invoke-direct {v2, v1}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$9;-><init>(Lcom/squareup/ui/orderhub/OrderHubState;)V

    move-object v5, v2

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x2

    const/4 v7, 0x0

    move-object/from16 v2, p3

    .line 235
    invoke-static/range {v2 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 242
    iget-object v2, v0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    invoke-interface {v2}, Lcom/squareup/connectivity/ConnectivityMonitor;->internetState()Lio/reactivex/Observable;

    move-result-object v2

    const-string v3, "connectivityMonitor.internetState()"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 747
    sget-object v3, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object v2

    invoke-static {v2, v11}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lorg/reactivestreams/Publisher;

    if-eqz v2, :cond_13

    .line 749
    invoke-static {v2}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v2

    .line 750
    const-class v3, Lcom/squareup/connectivity/InternetState;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v3

    new-instance v4, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v4, v3, v2}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v3, v4

    check-cast v3, Lcom/squareup/workflow/Worker;

    const/4 v4, 0x0

    .line 243
    new-instance v2, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$10;

    invoke-direct {v2, v1}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$10;-><init>(Lcom/squareup/ui/orderhub/OrderHubState;)V

    move-object v5, v2

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x2

    const/4 v7, 0x0

    move-object/from16 v2, p3

    .line 241
    invoke-static/range {v2 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 255
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/ui/orderhub/OrderHubState;->getDetailState()Lcom/squareup/ui/orderhub/DetailState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/ui/orderhub/DetailState;->getLoadingMoreOrders()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 257
    iget-object v2, v0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->orderRepository:Lcom/squareup/ordermanagerdata/OrderRepository;

    invoke-interface {v2}, Lcom/squareup/ordermanagerdata/OrderRepository;->loadNextPageOfCompletedOrders()Lio/reactivex/Single;

    move-result-object v2

    .line 751
    sget-object v3, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v3, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$$inlined$asWorker$3;

    invoke-direct {v3, v2, v8}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$$inlined$asWorker$3;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    .line 752
    invoke-static {v3}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v2

    .line 753
    const-class v3, Lcom/squareup/ordermanagerdata/ResultState;

    sget-object v4, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class v5, Lkotlin/Unit;

    invoke-static {v5}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v5

    invoke-virtual {v4, v5}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object v4

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;Lkotlin/reflect/KTypeProjection;)Lkotlin/reflect/KType;

    move-result-object v3

    new-instance v4, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v4, v3, v2}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v4, Lcom/squareup/workflow/Worker;

    .line 259
    new-instance v2, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$11;

    invoke-direct {v2, v0, v1}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$11;-><init>(Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;Lcom/squareup/ui/orderhub/OrderHubState;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    const-string v3, "loading-more-orders"

    .line 256
    invoke-interface {v9, v4, v3, v2}, Lcom/squareup/workflow/RenderContext;->runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    .line 278
    :cond_8
    new-instance v2, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$masterEventHandler$1;

    invoke-direct {v2, v0, v1}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$masterEventHandler$1;-><init>(Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;Lcom/squareup/ui/orderhub/OrderHubState;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-interface {v9, v2}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v11

    .line 290
    new-instance v2, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$detailEventHandler$1;

    invoke-direct {v2, v0, v1}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$detailEventHandler$1;-><init>(Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;Lcom/squareup/ui/orderhub/OrderHubState;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-interface {v9, v2}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v12

    .line 363
    iget-object v2, v0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v2}, Lcom/squareup/settings/server/AccountStatusSettings;->getOrderHubSettings()Lcom/squareup/settings/server/OrderHubSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/settings/server/OrderHubSettings;->getAllowOrdersSearch()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 365
    iget-object v2, v0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->searchWorkflow:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow;

    move-object v3, v2

    check-cast v3, Lcom/squareup/workflow/Workflow;

    new-instance v4, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$Props;

    invoke-virtual/range {p2 .. p2}, Lcom/squareup/ui/orderhub/OrderHubState;->getDetailState()Lcom/squareup/ui/orderhub/DetailState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/ui/orderhub/DetailState;->getSelectedFilter()Lcom/squareup/ui/orderhub/master/Filter;

    move-result-object v2

    invoke-direct {v4, v2}, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$Props;-><init>(Lcom/squareup/ui/orderhub/master/Filter;)V

    const/4 v5, 0x0

    .line 366
    sget-object v2, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$searchRendering$1;->INSTANCE:Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$searchRendering$1;

    move-object v6, v2

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v7, 0x4

    const/4 v8, 0x0

    move-object/from16 v2, p3

    .line 364
    invoke-static/range {v2 .. v8}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;

    goto :goto_4

    .line 370
    :cond_9
    sget-object v2, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->Companion:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$Companion;

    invoke-virtual {v2}, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$Companion;->getSEARCH_NOT_ALLOWED()Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;

    move-result-object v2

    .line 373
    :goto_4
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/ui/orderhub/OrderHubState;->isMasterDetail()Z

    move-result v3

    const-string v4, ""

    if-eqz v3, :cond_e

    .line 374
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/ui/orderhub/OrderHubState;->getNextWorkflowAction()Lcom/squareup/ui/orderhub/OrderWorkflowAction;

    move-result-object v3

    .line 375
    instance-of v5, v3, Lcom/squareup/ui/orderhub/OrderWorkflowAction$OrderMarkShippedWorkflowAction;

    if-eqz v5, :cond_a

    .line 376
    invoke-direct {v0, v9, v1}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->getMarkShippedChildRendering(Lcom/squareup/workflow/RenderContext;Lcom/squareup/ui/orderhub/OrderHubState;)Ljava/util/Map;

    move-result-object v3

    .line 377
    sget-object v5, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {v3, v5}, Lkotlin/collections/MapsKt;->getValue(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    move-object/from16 v16, v5

    check-cast v16, Lcom/squareup/workflow/legacy/Screen;

    .line 378
    sget-object v5, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    invoke-interface {v3, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object/from16 v18, v3

    check-cast v18, Lcom/squareup/workflow/legacy/Screen;

    .line 379
    sget-object v13, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 380
    new-instance v3, Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen;

    invoke-direct {v3, v1, v2, v12}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen;-><init>(Lcom/squareup/ui/orderhub/OrderHubState;Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;Lkotlin/jvm/functions/Function1;)V

    check-cast v3, Lcom/squareup/workflow/legacy/V2Screen;

    .line 755
    new-instance v14, Lcom/squareup/workflow/legacy/Screen;

    .line 756
    const-class v2, Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v2, v4}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 757
    sget-object v5, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v5}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v5

    .line 755
    invoke-direct {v14, v2, v3, v5}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 383
    new-instance v2, Lcom/squareup/ui/orderhub/master/OrderHubMasterScreen;

    .line 384
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/ui/orderhub/OrderHubState;->getMasterState()Lcom/squareup/ui/orderhub/MasterState;

    move-result-object v1

    .line 383
    invoke-direct {v2, v1, v11}, Lcom/squareup/ui/orderhub/master/OrderHubMasterScreen;-><init>(Lcom/squareup/ui/orderhub/MasterState;Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lcom/squareup/workflow/legacy/V2Screen;

    .line 760
    new-instance v15, Lcom/squareup/workflow/legacy/Screen;

    .line 761
    const-class v1, Lcom/squareup/ui/orderhub/master/OrderHubMasterScreen;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-static {v1, v4}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    .line 762
    sget-object v3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v3

    .line 760
    invoke-direct {v15, v1, v2, v3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    const/16 v17, 0x0

    const/16 v19, 0x8

    const/16 v20, 0x0

    .line 379
    invoke-static/range {v13 .. v20}, Lcom/squareup/container/PosLayering$Companion;->fullStack$default(Lcom/squareup/container/PosLayering$Companion;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;ILjava/lang/Object;)Ljava/util/Map;

    move-result-object v1

    goto/16 :goto_5

    .line 390
    :cond_a
    instance-of v5, v3, Lcom/squareup/ui/orderhub/OrderWorkflowAction$OrderDetailWorkflowAction;

    if-eqz v5, :cond_c

    .line 391
    invoke-direct {v0, v1, v9}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->getOrderDetailChildRendering(Lcom/squareup/ui/orderhub/OrderHubState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object v3

    if-eqz v3, :cond_b

    .line 392
    sget-object v13, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 393
    new-instance v5, Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen;

    invoke-direct {v5, v1, v2, v12}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen;-><init>(Lcom/squareup/ui/orderhub/OrderHubState;Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;Lkotlin/jvm/functions/Function1;)V

    check-cast v5, Lcom/squareup/workflow/legacy/V2Screen;

    .line 765
    new-instance v14, Lcom/squareup/workflow/legacy/Screen;

    .line 766
    const-class v2, Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v2, v4}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 767
    sget-object v6, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v6}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v6

    .line 765
    invoke-direct {v14, v2, v5, v6}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 396
    new-instance v2, Lcom/squareup/ui/orderhub/master/OrderHubMasterScreen;

    .line 397
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/ui/orderhub/OrderHubState;->getMasterState()Lcom/squareup/ui/orderhub/MasterState;

    move-result-object v1

    .line 396
    invoke-direct {v2, v1, v11}, Lcom/squareup/ui/orderhub/master/OrderHubMasterScreen;-><init>(Lcom/squareup/ui/orderhub/MasterState;Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lcom/squareup/workflow/legacy/V2Screen;

    .line 770
    new-instance v15, Lcom/squareup/workflow/legacy/Screen;

    .line 771
    const-class v1, Lcom/squareup/ui/orderhub/master/OrderHubMasterScreen;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-static {v1, v4}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    .line 772
    sget-object v4, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v4}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v4

    .line 770
    invoke-direct {v15, v1, v2, v4}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 399
    sget-object v1, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {v3, v1}, Lkotlin/collections/MapsKt;->getValue(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/workflow/legacy/Screen;

    const/16 v17, 0x0

    .line 400
    sget-object v1, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lcom/squareup/workflow/legacy/Screen;

    const/16 v19, 0x8

    const/16 v20, 0x0

    .line 392
    invoke-static/range {v13 .. v20}, Lcom/squareup/container/PosLayering$Companion;->fullStack$default(Lcom/squareup/container/PosLayering$Companion;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;ILjava/lang/Object;)Ljava/util/Map;

    move-result-object v1

    goto/16 :goto_5

    .line 391
    :cond_b
    invoke-static {}, Lkotlin/collections/MapsKt;->emptyMap()Ljava/util/Map;

    move-result-object v1

    return-object v1

    .line 403
    :cond_c
    instance-of v3, v3, Lcom/squareup/ui/orderhub/OrderWorkflowAction$OrderNoWorkflowAction;

    if-eqz v3, :cond_d

    .line 404
    sget-object v3, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 405
    new-instance v5, Lcom/squareup/ui/orderhub/master/OrderHubMasterScreen;

    .line 406
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/ui/orderhub/OrderHubState;->getMasterState()Lcom/squareup/ui/orderhub/MasterState;

    move-result-object v6

    .line 405
    invoke-direct {v5, v6, v11}, Lcom/squareup/ui/orderhub/master/OrderHubMasterScreen;-><init>(Lcom/squareup/ui/orderhub/MasterState;Lkotlin/jvm/functions/Function1;)V

    check-cast v5, Lcom/squareup/workflow/legacy/V2Screen;

    .line 775
    new-instance v6, Lcom/squareup/workflow/legacy/Screen;

    .line 776
    const-class v7, Lcom/squareup/ui/orderhub/master/OrderHubMasterScreen;

    invoke-static {v7}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v7

    invoke-static {v7, v4}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v7

    .line 777
    sget-object v8, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v8}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v8

    .line 775
    invoke-direct {v6, v7, v5, v8}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 408
    new-instance v5, Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen;

    invoke-direct {v5, v1, v2, v12}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen;-><init>(Lcom/squareup/ui/orderhub/OrderHubState;Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;Lkotlin/jvm/functions/Function1;)V

    check-cast v5, Lcom/squareup/workflow/legacy/V2Screen;

    .line 780
    new-instance v1, Lcom/squareup/workflow/legacy/Screen;

    .line 781
    const-class v2, Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v2, v4}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 782
    sget-object v4, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v4}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v4

    .line 780
    invoke-direct {v1, v2, v5, v4}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 404
    invoke-virtual {v3, v6, v1}, Lcom/squareup/container/PosLayering$Companion;->masterDetailScreen(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object v1

    goto/16 :goto_5

    :cond_d
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1

    .line 414
    :cond_e
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/ui/orderhub/OrderHubState;->getNextWorkflowAction()Lcom/squareup/ui/orderhub/OrderWorkflowAction;

    move-result-object v3

    .line 415
    instance-of v5, v3, Lcom/squareup/ui/orderhub/OrderWorkflowAction$OrderMarkShippedWorkflowAction;

    if-eqz v5, :cond_f

    .line 416
    invoke-direct {v0, v9, v1}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->getMarkShippedChildRendering(Lcom/squareup/workflow/RenderContext;Lcom/squareup/ui/orderhub/OrderHubState;)Ljava/util/Map;

    move-result-object v1

    .line 417
    sget-object v2, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 418
    sget-object v3, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {v1, v3}, Lkotlin/collections/MapsKt;->getValue(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/workflow/legacy/Screen;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 419
    sget-object v7, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    invoke-interface {v1, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/workflow/legacy/Screen;

    const/16 v8, 0xe

    const/4 v9, 0x0

    .line 417
    invoke-static/range {v2 .. v9}, Lcom/squareup/container/PosLayering$Companion;->fullStack$default(Lcom/squareup/container/PosLayering$Companion;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;ILjava/lang/Object;)Ljava/util/Map;

    move-result-object v1

    goto :goto_5

    .line 422
    :cond_f
    instance-of v5, v3, Lcom/squareup/ui/orderhub/OrderWorkflowAction$OrderDetailWorkflowAction;

    if-eqz v5, :cond_11

    .line 423
    invoke-direct {v0, v1, v9}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->getOrderDetailChildRendering(Lcom/squareup/ui/orderhub/OrderHubState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object v1

    if-eqz v1, :cond_10

    .line 424
    sget-object v2, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 425
    sget-object v3, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {v1, v3}, Lkotlin/collections/MapsKt;->getValue(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/workflow/legacy/Screen;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 426
    sget-object v7, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    invoke-interface {v1, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/workflow/legacy/Screen;

    const/16 v8, 0xe

    const/4 v9, 0x0

    .line 424
    invoke-static/range {v2 .. v9}, Lcom/squareup/container/PosLayering$Companion;->fullStack$default(Lcom/squareup/container/PosLayering$Companion;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;ILjava/lang/Object;)Ljava/util/Map;

    move-result-object v1

    goto :goto_5

    .line 423
    :cond_10
    invoke-static {}, Lkotlin/collections/MapsKt;->emptyMap()Ljava/util/Map;

    move-result-object v1

    return-object v1

    .line 429
    :cond_11
    instance-of v3, v3, Lcom/squareup/ui/orderhub/OrderWorkflowAction$OrderNoWorkflowAction;

    if-eqz v3, :cond_12

    .line 430
    sget-object v3, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 431
    new-instance v5, Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen;

    invoke-direct {v5, v1, v2, v12}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen;-><init>(Lcom/squareup/ui/orderhub/OrderHubState;Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;Lkotlin/jvm/functions/Function1;)V

    check-cast v5, Lcom/squareup/workflow/legacy/V2Screen;

    .line 785
    new-instance v1, Lcom/squareup/workflow/legacy/Screen;

    .line 786
    const-class v2, Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v2, v4}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 787
    sget-object v4, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v4}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v4

    .line 785
    invoke-direct {v1, v2, v5, v4}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 430
    invoke-virtual {v3, v1}, Lcom/squareup/container/PosLayering$Companion;->bodyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object v1

    .line 438
    :goto_5
    invoke-static {v1, v10}, Lcom/squareup/workflow/LayeredScreenKt;->withPersistence(Ljava/util/Map;Z)Ljava/util/Map;

    move-result-object v1

    return-object v1

    .line 430
    :cond_12
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1

    .line 749
    :cond_13
    new-instance v1, Lkotlin/TypeCastException;

    invoke-direct {v1, v12}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 745
    :cond_14
    new-instance v1, Lkotlin/TypeCastException;

    invoke-direct {v1, v12}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 735
    :cond_15
    new-instance v1, Lkotlin/TypeCastException;

    invoke-direct {v1, v12}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 731
    :cond_16
    new-instance v1, Lkotlin/TypeCastException;

    invoke-direct {v1, v12}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public snapshotState(Lcom/squareup/ui/orderhub/OrderHubState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    sget-object p1, Lcom/squareup/workflow/Snapshot;->EMPTY:Lcom/squareup/workflow/Snapshot;

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 77
    check-cast p1, Lcom/squareup/ui/orderhub/OrderHubState;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->snapshotState(Lcom/squareup/ui/orderhub/OrderHubState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
