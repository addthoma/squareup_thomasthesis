.class public final Lcom/squareup/ui/orderhub/OrderQuickActionStatus;
.super Ljava/lang/Object;
.source "OrderHubState.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\r\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0007J\t\u0010\u000c\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\r\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u000e\u001a\u00020\u0005H\u00c6\u0003J\'\u0010\u000f\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u0010\u001a\u00020\u00052\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001J\t\u0010\u0014\u001a\u00020\u0015H\u00d6\u0001R\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0004\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000b\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/OrderQuickActionStatus;",
        "",
        "order",
        "Lcom/squareup/orders/model/Order;",
        "isLoading",
        "",
        "hasError",
        "(Lcom/squareup/orders/model/Order;ZZ)V",
        "getHasError",
        "()Z",
        "getOrder",
        "()Lcom/squareup/orders/model/Order;",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final hasError:Z

.field private final isLoading:Z

.field private final order:Lcom/squareup/orders/model/Order;


# direct methods
.method public constructor <init>(Lcom/squareup/orders/model/Order;ZZ)V
    .locals 1

    const-string v0, "order"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/OrderQuickActionStatus;->order:Lcom/squareup/orders/model/Order;

    iput-boolean p2, p0, Lcom/squareup/ui/orderhub/OrderQuickActionStatus;->isLoading:Z

    iput-boolean p3, p0, Lcom/squareup/ui/orderhub/OrderQuickActionStatus;->hasError:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/orderhub/OrderQuickActionStatus;Lcom/squareup/orders/model/Order;ZZILjava/lang/Object;)Lcom/squareup/ui/orderhub/OrderQuickActionStatus;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/orderhub/OrderQuickActionStatus;->order:Lcom/squareup/orders/model/Order;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-boolean p2, p0, Lcom/squareup/ui/orderhub/OrderQuickActionStatus;->isLoading:Z

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-boolean p3, p0, Lcom/squareup/ui/orderhub/OrderQuickActionStatus;->hasError:Z

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/orderhub/OrderQuickActionStatus;->copy(Lcom/squareup/orders/model/Order;ZZ)Lcom/squareup/ui/orderhub/OrderQuickActionStatus;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/orders/model/Order;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/orderhub/OrderQuickActionStatus;->order:Lcom/squareup/orders/model/Order;

    return-object v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/OrderQuickActionStatus;->isLoading:Z

    return v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/OrderQuickActionStatus;->hasError:Z

    return v0
.end method

.method public final copy(Lcom/squareup/orders/model/Order;ZZ)Lcom/squareup/ui/orderhub/OrderQuickActionStatus;
    .locals 1

    const-string v0, "order"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/orderhub/OrderQuickActionStatus;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/ui/orderhub/OrderQuickActionStatus;-><init>(Lcom/squareup/orders/model/Order;ZZ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/orderhub/OrderQuickActionStatus;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/orderhub/OrderQuickActionStatus;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/OrderQuickActionStatus;->order:Lcom/squareup/orders/model/Order;

    iget-object v1, p1, Lcom/squareup/ui/orderhub/OrderQuickActionStatus;->order:Lcom/squareup/orders/model/Order;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/OrderQuickActionStatus;->isLoading:Z

    iget-boolean v1, p1, Lcom/squareup/ui/orderhub/OrderQuickActionStatus;->isLoading:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/OrderQuickActionStatus;->hasError:Z

    iget-boolean p1, p1, Lcom/squareup/ui/orderhub/OrderQuickActionStatus;->hasError:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getHasError()Z
    .locals 1

    .line 64
    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/OrderQuickActionStatus;->hasError:Z

    return v0
.end method

.method public final getOrder()Lcom/squareup/orders/model/Order;
    .locals 1

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/orderhub/OrderQuickActionStatus;->order:Lcom/squareup/orders/model/Order;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/ui/orderhub/OrderQuickActionStatus;->order:Lcom/squareup/orders/model/Order;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/ui/orderhub/OrderQuickActionStatus;->isLoading:Z

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/ui/orderhub/OrderQuickActionStatus;->hasError:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public final isLoading()Z
    .locals 1

    .line 63
    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/OrderQuickActionStatus;->isLoading:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OrderQuickActionStatus(order="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/OrderQuickActionStatus;->order:Lcom/squareup/orders/model/Order;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isLoading="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/orderhub/OrderQuickActionStatus;->isLoading:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", hasError="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/orderhub/OrderQuickActionStatus;->hasError:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
