.class public final Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealOrderDetailsWorkflow.kt"

# interfaces
.implements Lcom/squareup/ui/orderhub/order/OrderDetailsWorkflow;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/ui/orderhub/OrderHubAppletScope;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsInput;",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsState;",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsResult;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealOrderDetailsWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealOrderDetailsWorkflow.kt\ncom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow\n+ 2 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 3 Worker.kt\ncom/squareup/workflow/WorkerKt\n+ 4 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n+ 5 Worker.kt\ncom/squareup/workflow/Worker$Companion\n*L\n1#1,885:1\n41#2:886\n56#2,2:887\n41#2:890\n56#2,2:891\n85#2:909\n85#2:912\n85#2:915\n85#2:938\n85#2:946\n276#3:889\n276#3:893\n276#3:911\n276#3:914\n276#3:917\n276#3:940\n276#3:948\n149#4,5:894\n149#4,5:899\n149#4,5:904\n149#4,5:918\n149#4,5:923\n149#4,5:928\n149#4,5:933\n149#4,5:941\n149#4,5:949\n149#4,5:954\n149#4,5:959\n240#5:910\n240#5:913\n240#5:916\n240#5:939\n240#5:947\n*E\n*S KotlinDebug\n*F\n+ 1 RealOrderDetailsWorkflow.kt\ncom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow\n*L\n156#1:886\n156#1,2:887\n164#1:890\n164#1,2:891\n258#1:909\n298#1:912\n305#1:915\n475#1:938\n510#1:946\n156#1:889\n164#1:893\n258#1:911\n298#1:914\n305#1:917\n475#1:940\n510#1:948\n205#1,5:894\n213#1,5:899\n238#1,5:904\n406#1,5:918\n411#1,5:923\n467#1,5:928\n470#1,5:933\n503#1,5:941\n557#1,5:949\n564#1,5:954\n601#1,5:959\n258#1:910\n298#1:913\n305#1:916\n475#1:939\n510#1:947\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u009e\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\u0008\u0007\u0018\u00002\u00020\u00012<\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012&\u0012$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n0\u0002BO\u0008\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u0012\u0006\u0010\u0015\u001a\u00020\u0016\u0012\u0006\u0010\u0017\u001a\u00020\u0018\u0012\u0006\u0010\u0019\u001a\u00020\u001a\u0012\u0006\u0010\u001b\u001a\u00020\u001c\u00a2\u0006\u0002\u0010\u001dJF\u0010\u001e\u001a$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n2\u0006\u0010\u001f\u001a\u00020 2\u0012\u0010!\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\"H\u0002JF\u0010#\u001a$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n2\u0006\u0010\u001f\u001a\u00020$2\u0012\u0010!\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\"H\u0002JF\u0010%\u001a$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n2\u0006\u0010\u001f\u001a\u00020&2\u0012\u0010!\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\"H\u0002J\u001a\u0010\'\u001a\u00020\u00042\u0006\u0010(\u001a\u00020\u00032\u0008\u0010)\u001a\u0004\u0018\u00010*H\u0016J \u0010+\u001a\u00020\u00042\u0006\u0010,\u001a\u00020\u00032\u0006\u0010-\u001a\u00020\u00032\u0006\u0010\u001f\u001a\u00020\u0004H\u0016JN\u0010.\u001a$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n2\u0006\u0010(\u001a\u00020\u00032\u0006\u0010\u001f\u001a\u00020\u00042\u0012\u0010!\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\"H\u0016J\u0010\u0010/\u001a\u00020*2\u0006\u0010\u001f\u001a\u00020\u0004H\u0016J\u000c\u00100\u001a\u000201*\u00020 H\u0002J\u000c\u00102\u001a\u00020\u0004*\u00020\u0003H\u0002J \u00103\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u000504*\u00020\u00042\u0006\u00105\u001a\u000206H\u0002J \u00107\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u000504*\u00020\u00042\u0006\u00108\u001a\u000206H\u0002R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u001aX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u001cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00069"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsInput;",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsState;",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsResult;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "orderRepository",
        "Lcom/squareup/ordermanagerdata/OrderRepository;",
        "device",
        "Lcom/squareup/util/Device;",
        "connectivityMonitor",
        "Lcom/squareup/connectivity/ConnectivityMonitor;",
        "flow",
        "Lflow/Flow;",
        "orderHubBillLoader",
        "Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;",
        "orderHubAnalytics",
        "Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;",
        "orderMarkShippedWorkflow",
        "Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedWorkflow;",
        "orderEditTrackingWorkflow",
        "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingWorkflow;",
        "orderMarkCanceledWorkflow",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledWorkflow;",
        "(Lcom/squareup/ordermanagerdata/OrderRepository;Lcom/squareup/util/Device;Lcom/squareup/connectivity/ConnectivityMonitor;Lflow/Flow;Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedWorkflow;Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingWorkflow;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledWorkflow;)V",
        "getEditTrackingChildRendering",
        "state",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "getMarkCanceledChildRendering",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;",
        "getMarkShippedChildRendering",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;",
        "initialState",
        "input",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "onPropsChanged",
        "old",
        "new",
        "render",
        "snapshotState",
        "logShipmentTrackingInfoEvent",
        "",
        "nextState",
        "updateIsReadOnly",
        "Lcom/squareup/workflow/WorkflowAction;",
        "updatedIsReadOnly",
        "",
        "updateShowIdInActionBar",
        "updatedShowOrderIdInActionBar",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

.field private final device:Lcom/squareup/util/Device;

.field private final flow:Lflow/Flow;

.field private final orderEditTrackingWorkflow:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingWorkflow;

.field private final orderHubAnalytics:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;

.field private final orderHubBillLoader:Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;

.field private final orderMarkCanceledWorkflow:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledWorkflow;

.field private final orderMarkShippedWorkflow:Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedWorkflow;

.field private final orderRepository:Lcom/squareup/ordermanagerdata/OrderRepository;


# direct methods
.method public constructor <init>(Lcom/squareup/ordermanagerdata/OrderRepository;Lcom/squareup/util/Device;Lcom/squareup/connectivity/ConnectivityMonitor;Lflow/Flow;Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedWorkflow;Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingWorkflow;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledWorkflow;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "orderRepository"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "device"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "connectivityMonitor"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "flow"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderHubBillLoader"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderHubAnalytics"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderMarkShippedWorkflow"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderEditTrackingWorkflow"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderMarkCanceledWorkflow"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 123
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;->orderRepository:Lcom/squareup/ordermanagerdata/OrderRepository;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;->device:Lcom/squareup/util/Device;

    iput-object p3, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    iput-object p4, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;->flow:Lflow/Flow;

    iput-object p5, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;->orderHubBillLoader:Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;

    iput-object p6, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;->orderHubAnalytics:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;

    iput-object p7, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;->orderMarkShippedWorkflow:Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedWorkflow;

    iput-object p8, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;->orderEditTrackingWorkflow:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingWorkflow;

    iput-object p9, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;->orderMarkCanceledWorkflow:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledWorkflow;

    return-void
.end method

.method public static final synthetic access$getFlow$p(Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;)Lflow/Flow;
    .locals 0

    .line 112
    iget-object p0, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;->flow:Lflow/Flow;

    return-object p0
.end method

.method public static final synthetic access$getOrderHubAnalytics$p(Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;)Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;
    .locals 0

    .line 112
    iget-object p0, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;->orderHubAnalytics:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;

    return-object p0
.end method

.method public static final synthetic access$updateIsReadOnly(Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;Lcom/squareup/ui/orderhub/order/OrderDetailsState;Z)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 112
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;->updateIsReadOnly(Lcom/squareup/ui/orderhub/order/OrderDetailsState;Z)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$updateShowIdInActionBar(Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;Lcom/squareup/ui/orderhub/order/OrderDetailsState;Z)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 112
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;->updateShowIdInActionBar(Lcom/squareup/ui/orderhub/order/OrderDetailsState;Z)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method private final getEditTrackingChildRendering(Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/ui/orderhub/order/OrderDetailsState;",
            "-",
            "Lcom/squareup/ui/orderhub/order/OrderDetailsResult;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 693
    new-instance v2, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingInput;

    .line 694
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v0

    .line 695
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;->getShouldShowRemoveTracking()Z

    move-result v1

    .line 696
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;->getOrderUpdateFailureState()Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    move-result-object v3

    .line 697
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;->getTracking()Lcom/squareup/ordermanagerdata/TrackingInfo;

    move-result-object v4

    .line 693
    invoke-direct {v2, v0, v1, v3, v4}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingInput;-><init>(Lcom/squareup/orders/model/Order;ZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/ordermanagerdata/TrackingInfo;)V

    .line 700
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;->orderEditTrackingWorkflow:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingWorkflow;

    move-object v1, v0

    check-cast v1, Lcom/squareup/workflow/Workflow;

    .line 702
    new-instance v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$getEditTrackingChildRendering$1;

    invoke-direct {v0, p1}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$getEditTrackingChildRendering$1;-><init>(Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;)V

    move-object v4, v0

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v3, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v0, p2

    .line 699
    invoke-static/range {v0 .. v6}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    return-object p1
.end method

.method private final getMarkCanceledChildRendering(Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/ui/orderhub/order/OrderDetailsState;",
            "-",
            "Lcom/squareup/ui/orderhub/order/OrderDetailsResult;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 612
    new-instance v2, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput;

    .line 613
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v0

    .line 614
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;->getCancelAction()Lcom/squareup/protos/client/orders/Action;

    move-result-object v1

    .line 615
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;->getRetryInventoryAdjustment()Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$InventoryAdjustmentRetry;

    move-result-object v3

    .line 616
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;->getBillRetrievalRetry()Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$BillRetrievalRetry;

    move-result-object v4

    .line 612
    invoke-direct {v2, v0, v1, v3, v4}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput;-><init>(Lcom/squareup/orders/model/Order;Lcom/squareup/protos/client/orders/Action;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$InventoryAdjustmentRetry;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$BillRetrievalRetry;)V

    .line 619
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;->orderMarkCanceledWorkflow:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledWorkflow;

    move-object v1, v0

    check-cast v1, Lcom/squareup/workflow/Workflow;

    .line 621
    new-instance v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$getMarkCanceledChildRendering$1;

    invoke-direct {v0, p1}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$getMarkCanceledChildRendering$1;-><init>(Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;)V

    move-object v4, v0

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v3, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v0, p2

    .line 618
    invoke-static/range {v0 .. v6}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    return-object p1
.end method

.method private final getMarkShippedChildRendering(Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/ui/orderhub/order/OrderDetailsState;",
            "-",
            "Lcom/squareup/ui/orderhub/order/OrderDetailsResult;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 731
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v0

    .line 732
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->getShipAction()Lcom/squareup/protos/client/orders/Action;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v2, 0x1

    .line 730
    new-instance v5, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedInput;

    invoke-direct {v5, v0, v1, v2}, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedInput;-><init>(Lcom/squareup/orders/model/Order;Lcom/squareup/protos/client/orders/Action;Z)V

    .line 736
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;->orderMarkShippedWorkflow:Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedWorkflow;

    move-object v4, v0

    check-cast v4, Lcom/squareup/workflow/Workflow;

    const/4 v6, 0x0

    .line 738
    new-instance v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$getMarkShippedChildRendering$1;

    invoke-direct {v0, p1}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$getMarkShippedChildRendering$1;-><init>(Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;)V

    move-object v7, v0

    check-cast v7, Lkotlin/jvm/functions/Function1;

    const/4 v8, 0x4

    const/4 v9, 0x0

    move-object v3, p2

    .line 735
    invoke-static/range {v3 .. v9}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    return-object p1

    .line 732
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Required value was null."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final logShipmentTrackingInfoEvent(Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;)V
    .locals 3

    .line 783
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getPrimaryFulfillment(Lcom/squareup/orders/model/Order;)Lcom/squareup/orders/model/Order$Fulfillment;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/ui/orderhub/util/proto/FulfillmentsKt;->getTrackingInfo(Lcom/squareup/orders/model/Order$Fulfillment;)Lcom/squareup/ordermanagerdata/TrackingInfo;

    move-result-object v0

    .line 784
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;->getTracking()Lcom/squareup/ordermanagerdata/TrackingInfo;

    move-result-object v1

    const-string v2, "order.id"

    if-eqz v1, :cond_1

    if-nez v0, :cond_0

    .line 786
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;->orderHubAnalytics:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;->ORDER_HUB_ADD_TRACKING_INFORMATION:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;->logEvent$orderhub_applet_release(Ljava/lang/String;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;)V

    goto :goto_0

    .line 787
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;->getTracking()Lcom/squareup/ordermanagerdata/TrackingInfo;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_3

    .line 788
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;->orderHubAnalytics:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;->ORDER_HUB_EDIT_TRACKING_INFORMATION:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;->logEvent$orderhub_applet_release(Ljava/lang/String;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;)V

    goto :goto_0

    :cond_1
    if-nez v0, :cond_2

    .line 791
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;->orderHubAnalytics:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;->ORDER_HUB_SKIP_TRACKING_INFORMATION:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;->logEvent$orderhub_applet_release(Ljava/lang/String;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;)V

    goto :goto_0

    .line 793
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;->orderHubAnalytics:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v1, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;->ORDER_HUB_DELETE_TRACKING_INFORMATION:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;->logEvent$orderhub_applet_release(Ljava/lang/String;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;)V

    :cond_3
    :goto_0
    return-void
.end method

.method private final nextState(Lcom/squareup/ui/orderhub/order/OrderDetailsInput;)Lcom/squareup/ui/orderhub/order/OrderDetailsState;
    .locals 16

    move-object/from16 v0, p1

    .line 839
    instance-of v1, v0, Lcom/squareup/ui/orderhub/order/OrderDetailsInput$ViewedOrder;

    if-eqz v1, :cond_0

    .line 840
    new-instance v1, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;

    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 843
    check-cast v0, Lcom/squareup/ui/orderhub/order/OrderDetailsInput$ViewedOrder;

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/OrderDetailsInput$ViewedOrder;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x78

    const/4 v11, 0x0

    move-object v2, v1

    .line 840
    invoke-direct/range {v2 .. v11}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;-><init>(ZZLcom/squareup/orders/model/Order;ZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/protos/client/orders/Action;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v1, Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    goto :goto_0

    .line 846
    :cond_0
    instance-of v1, v0, Lcom/squareup/ui/orderhub/order/OrderDetailsInput$RestartingAfterCancellation;

    if-eqz v1, :cond_1

    .line 847
    new-instance v1, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;

    const/4 v6, 0x1

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 851
    check-cast v0, Lcom/squareup/ui/orderhub/order/OrderDetailsInput$RestartingAfterCancellation;

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/OrderDetailsInput$RestartingAfterCancellation;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v5

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 853
    new-instance v15, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$InventoryAdjustmentRetry;

    const/4 v10, 0x0

    .line 855
    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/OrderDetailsInput$RestartingAfterCancellation;->getBill()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v11

    .line 856
    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/OrderDetailsInput$RestartingAfterCancellation;->getCancelReason()Lcom/squareup/ordermanagerdata/CancellationReason;

    move-result-object v12

    .line 857
    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/OrderDetailsInput$RestartingAfterCancellation;->getSelectedLineItems()Ljava/util/Map;

    move-result-object v13

    .line 858
    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/OrderDetailsInput$RestartingAfterCancellation;->getIdempotencyKey()Ljava/lang/String;

    move-result-object v14

    move-object v9, v15

    .line 853
    invoke-direct/range {v9 .. v14}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$InventoryAdjustmentRetry;-><init>(Lcom/squareup/protos/client/orders/Action;Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/ordermanagerdata/CancellationReason;Ljava/util/Map;Ljava/lang/String;)V

    const/16 v11, 0xa0

    const/4 v12, 0x0

    move-object v2, v1

    .line 847
    invoke-direct/range {v2 .. v12}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;-><init>(ZZLcom/squareup/orders/model/Order;ZLcom/squareup/protos/client/orders/Action;Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$InventoryAdjustmentRetry;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$BillRetrievalRetry;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v1, Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    :goto_0
    return-object v1

    :cond_1
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method

.method private final updateIsReadOnly(Lcom/squareup/ui/orderhub/order/OrderDetailsState;Z)Lcom/squareup/workflow/WorkflowAction;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/orderhub/order/OrderDetailsState;",
            "Z)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/orderhub/order/OrderDetailsState;",
            "Lcom/squareup/ui/orderhub/order/OrderDetailsResult;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p1

    .line 827
    instance-of v1, v0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;

    const/4 v2, 0x2

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    move-object v4, v0

    check-cast v4, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/16 v12, 0x7e

    const/4 v13, 0x0

    move/from16 v5, p2

    invoke-static/range {v4 .. v13}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;->copy$default(Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;ZZLcom/squareup/orders/model/Order;ZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/protos/client/orders/Action;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;

    move-result-object v0

    invoke-static {v1, v0, v3, v2, v3}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v0

    goto/16 :goto_0

    .line 828
    :cond_0
    instance-of v1, v0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$AdjustingPickupTimeState;

    if-eqz v1, :cond_1

    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/WorkflowAction$Companion;->noAction()Lcom/squareup/workflow/WorkflowAction;

    move-result-object v0

    goto/16 :goto_0

    .line 829
    :cond_1
    instance-of v1, v0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;

    if-eqz v1, :cond_2

    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    move-object v4, v0

    check-cast v4, Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/16 v13, 0xfe

    const/4 v14, 0x0

    move/from16 v5, p2

    invoke-static/range {v4 .. v14}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;->copy$default(Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;ZZLcom/squareup/orders/model/Order;ZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/orders/model/Order$Fulfillment;ZLcom/squareup/ordermanagerdata/TrackingInfo;ILjava/lang/Object;)Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;

    move-result-object v0

    invoke-static {v1, v0, v3, v2, v3}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v0

    goto :goto_0

    .line 830
    :cond_2
    instance-of v1, v0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RefreshingOrderState;

    if-eqz v1, :cond_3

    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    move-object v4, v0

    check-cast v4, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RefreshingOrderState;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x6

    const/4 v9, 0x0

    move/from16 v5, p2

    invoke-static/range {v4 .. v9}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RefreshingOrderState;->copy$default(Lcom/squareup/ui/orderhub/order/OrderDetailsState$RefreshingOrderState;ZZLcom/squareup/orders/model/Order;ILjava/lang/Object;)Lcom/squareup/ui/orderhub/order/OrderDetailsState$RefreshingOrderState;

    move-result-object v0

    invoke-static {v1, v0, v3, v2, v3}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v0

    goto :goto_0

    .line 831
    :cond_3
    instance-of v1, v0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;

    if-eqz v1, :cond_4

    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    move-object v4, v0

    check-cast v4, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/16 v13, 0xfe

    const/4 v14, 0x0

    move/from16 v5, p2

    invoke-static/range {v4 .. v14}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->copy$default(Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;ZZLcom/squareup/orders/model/Order;ZLcom/squareup/ordermanagerdata/CancellationReason;Ljava/util/Map;Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;ZILjava/lang/Object;)Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;

    move-result-object v0

    invoke-static {v1, v0, v3, v2, v3}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v0

    goto :goto_0

    .line 832
    :cond_4
    instance-of v1, v0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;

    if-eqz v1, :cond_5

    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    move-object v4, v0

    check-cast v4, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/16 v13, 0xfe

    const/4 v14, 0x0

    move/from16 v5, p2

    invoke-static/range {v4 .. v14}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;->copy$default(Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;ZZLcom/squareup/orders/model/Order;ZLcom/squareup/protos/client/orders/Action;Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$InventoryAdjustmentRetry;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$BillRetrievalRetry;ILjava/lang/Object;)Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;

    move-result-object v0

    invoke-static {v1, v0, v3, v2, v3}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v0

    goto :goto_0

    .line 833
    :cond_5
    instance-of v0, v0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;

    if-eqz v0, :cond_6

    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/WorkflowAction$Companion;->noAction()Lcom/squareup/workflow/WorkflowAction;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_6
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method

.method private final updateShowIdInActionBar(Lcom/squareup/ui/orderhub/order/OrderDetailsState;Z)Lcom/squareup/workflow/WorkflowAction;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/orderhub/order/OrderDetailsState;",
            "Z)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/orderhub/order/OrderDetailsState;",
            "Lcom/squareup/ui/orderhub/order/OrderDetailsResult;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p1

    .line 800
    instance-of v1, v0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;

    const/4 v2, 0x2

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 801
    move-object v4, v0

    check-cast v4, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/16 v12, 0x7d

    const/4 v13, 0x0

    move/from16 v6, p2

    invoke-static/range {v4 .. v13}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;->copy$default(Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;ZZLcom/squareup/orders/model/Order;ZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/protos/client/orders/Action;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;

    move-result-object v0

    .line 800
    invoke-static {v1, v0, v3, v2, v3}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v0

    goto/16 :goto_0

    .line 803
    :cond_0
    instance-of v1, v0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$AdjustingPickupTimeState;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 804
    move-object v4, v0

    check-cast v4, Lcom/squareup/ui/orderhub/order/OrderDetailsState$AdjustingPickupTimeState;

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x5

    const/4 v9, 0x0

    move/from16 v6, p2

    invoke-static/range {v4 .. v9}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$AdjustingPickupTimeState;->copy$default(Lcom/squareup/ui/orderhub/order/OrderDetailsState$AdjustingPickupTimeState;Lcom/squareup/orders/model/Order;ZLjava/lang/String;ILjava/lang/Object;)Lcom/squareup/ui/orderhub/order/OrderDetailsState$AdjustingPickupTimeState;

    move-result-object v0

    .line 803
    invoke-static {v1, v0, v3, v2, v3}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v0

    goto/16 :goto_0

    .line 806
    :cond_1
    instance-of v1, v0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;

    if-eqz v1, :cond_2

    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 807
    move-object v4, v0

    check-cast v4, Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/16 v13, 0xfd

    const/4 v14, 0x0

    move/from16 v6, p2

    invoke-static/range {v4 .. v14}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;->copy$default(Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;ZZLcom/squareup/orders/model/Order;ZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/orders/model/Order$Fulfillment;ZLcom/squareup/ordermanagerdata/TrackingInfo;ILjava/lang/Object;)Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;

    move-result-object v0

    .line 806
    invoke-static {v1, v0, v3, v2, v3}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v0

    goto/16 :goto_0

    .line 809
    :cond_2
    instance-of v1, v0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RefreshingOrderState;

    if-eqz v1, :cond_3

    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 810
    move-object v4, v0

    check-cast v4, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RefreshingOrderState;

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x5

    const/4 v9, 0x0

    move/from16 v6, p2

    invoke-static/range {v4 .. v9}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RefreshingOrderState;->copy$default(Lcom/squareup/ui/orderhub/order/OrderDetailsState$RefreshingOrderState;ZZLcom/squareup/orders/model/Order;ILjava/lang/Object;)Lcom/squareup/ui/orderhub/order/OrderDetailsState$RefreshingOrderState;

    move-result-object v0

    .line 809
    invoke-static {v1, v0, v3, v2, v3}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v0

    goto :goto_0

    .line 812
    :cond_3
    instance-of v1, v0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;

    if-eqz v1, :cond_4

    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 813
    move-object v4, v0

    check-cast v4, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/16 v13, 0xfd

    const/4 v14, 0x0

    move/from16 v6, p2

    invoke-static/range {v4 .. v14}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->copy$default(Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;ZZLcom/squareup/orders/model/Order;ZLcom/squareup/ordermanagerdata/CancellationReason;Ljava/util/Map;Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;ZILjava/lang/Object;)Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;

    move-result-object v0

    .line 812
    invoke-static {v1, v0, v3, v2, v3}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v0

    goto :goto_0

    .line 815
    :cond_4
    instance-of v1, v0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;

    if-eqz v1, :cond_5

    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 816
    move-object v4, v0

    check-cast v4, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/16 v13, 0xfd

    const/4 v14, 0x0

    move/from16 v6, p2

    invoke-static/range {v4 .. v14}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;->copy$default(Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;ZZLcom/squareup/orders/model/Order;ZLcom/squareup/protos/client/orders/Action;Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$InventoryAdjustmentRetry;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$BillRetrievalRetry;ILjava/lang/Object;)Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;

    move-result-object v0

    .line 815
    invoke-static {v1, v0, v3, v2, v3}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v0

    goto :goto_0

    .line 818
    :cond_5
    instance-of v1, v0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;

    if-eqz v1, :cond_6

    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 819
    move-object v4, v0

    check-cast v4, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0x3d

    const/4 v12, 0x0

    move/from16 v6, p2

    invoke-static/range {v4 .. v12}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->copy$default(Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;ZZLcom/squareup/orders/model/Order;ZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/protos/client/orders/Action;ILjava/lang/Object;)Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;

    move-result-object v0

    .line 818
    invoke-static {v1, v0, v3, v2, v3}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_6
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method


# virtual methods
.method public initialState(Lcom/squareup/ui/orderhub/order/OrderDetailsInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/ui/orderhub/order/OrderDetailsState;
    .locals 0

    const-string p2, "input"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 134
    invoke-direct {p0, p1}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;->nextState(Lcom/squareup/ui/orderhub/order/OrderDetailsInput;)Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 112
    check-cast p1, Lcom/squareup/ui/orderhub/order/OrderDetailsInput;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;->initialState(Lcom/squareup/ui/orderhub/order/OrderDetailsInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    move-result-object p1

    return-object p1
.end method

.method public onPropsChanged(Lcom/squareup/ui/orderhub/order/OrderDetailsInput;Lcom/squareup/ui/orderhub/order/OrderDetailsInput;Lcom/squareup/ui/orderhub/order/OrderDetailsState;)Lcom/squareup/ui/orderhub/order/OrderDetailsState;
    .locals 1

    const-string v0, "old"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "new"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 142
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    return-object p3

    .line 146
    :cond_0
    invoke-direct {p0, p2}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;->nextState(Lcom/squareup/ui/orderhub/order/OrderDetailsInput;)Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic onPropsChanged(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 112
    check-cast p1, Lcom/squareup/ui/orderhub/order/OrderDetailsInput;

    check-cast p2, Lcom/squareup/ui/orderhub/order/OrderDetailsInput;

    check-cast p3, Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;->onPropsChanged(Lcom/squareup/ui/orderhub/order/OrderDetailsInput;Lcom/squareup/ui/orderhub/order/OrderDetailsInput;Lcom/squareup/ui/orderhub/order/OrderDetailsState;)Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 112
    check-cast p1, Lcom/squareup/ui/orderhub/order/OrderDetailsInput;

    check-cast p2, Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;->render(Lcom/squareup/ui/orderhub/order/OrderDetailsInput;Lcom/squareup/ui/orderhub/order/OrderDetailsState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/ui/orderhub/order/OrderDetailsInput;Lcom/squareup/ui/orderhub/order/OrderDetailsState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/orderhub/order/OrderDetailsInput;",
            "Lcom/squareup/ui/orderhub/order/OrderDetailsState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/ui/orderhub/order/OrderDetailsState;",
            "-",
            "Lcom/squareup/ui/orderhub/order/OrderDetailsResult;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "state"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "context"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 155
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;->device:Lcom/squareup/util/Device;

    invoke-interface {p1}, Lcom/squareup/util/Device;->getScreenSize()Lio/reactivex/Observable;

    move-result-object p1

    .line 886
    sget-object v0, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object p1

    const-string v0, "this.toFlowable(BUFFER)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lorg/reactivestreams/Publisher;

    const-string v1, "null cannot be cast to non-null type org.reactivestreams.Publisher<T>"

    if-eqz p1, :cond_12

    .line 888
    invoke-static {p1}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 889
    const-class v2, Lcom/squareup/util/DeviceScreenSizeInfo;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v2

    new-instance v3, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v3, v2, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v5, v3

    check-cast v5, Lcom/squareup/workflow/Worker;

    const/4 v6, 0x0

    .line 157
    new-instance p1, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$1;

    invoke-direct {p1, p0, p2}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$1;-><init>(Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;Lcom/squareup/ui/orderhub/order/OrderDetailsState;)V

    move-object v7, p1

    check-cast v7, Lkotlin/jvm/functions/Function1;

    const/4 v8, 0x2

    const/4 v9, 0x0

    move-object v4, p3

    .line 154
    invoke-static/range {v4 .. v9}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 163
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    invoke-interface {p1}, Lcom/squareup/connectivity/ConnectivityMonitor;->internetState()Lio/reactivex/Observable;

    move-result-object p1

    const-string v2, "connectivityMonitor.internetState()"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 890
    sget-object v2, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {p1, v2}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lorg/reactivestreams/Publisher;

    if-eqz p1, :cond_11

    .line 892
    invoke-static {p1}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 893
    const-class v0, Lcom/squareup/connectivity/InternetState;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v0

    new-instance v1, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v1, v0, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v3, v1

    check-cast v3, Lcom/squareup/workflow/Worker;

    const/4 v4, 0x0

    .line 165
    new-instance p1, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$2;

    invoke-direct {p1, p0, p2}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$2;-><init>(Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;Lcom/squareup/ui/orderhub/order/OrderDetailsState;)V

    move-object v5, p1

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x2

    const/4 v7, 0x0

    move-object v2, p3

    .line 162
    invoke-static/range {v2 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 171
    instance-of p1, p2, Lcom/squareup/ui/orderhub/order/OrderDetailsState$AdjustingPickupTimeState;

    const-string v0, ""

    if-eqz p1, :cond_1

    .line 172
    sget-object p1, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 175
    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getPrimaryFulfillment(Lcom/squareup/orders/model/Order;)Lcom/squareup/orders/model/Order$Fulfillment;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/ui/orderhub/util/proto/FulfillmentsKt;->getPickupAt(Lcom/squareup/orders/model/Order$Fulfillment;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 178
    move-object v2, p2

    check-cast v2, Lcom/squareup/ui/orderhub/order/OrderDetailsState$AdjustingPickupTimeState;

    invoke-virtual {v2}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$AdjustingPickupTimeState;->getSelectedPickupTime()Ljava/lang/String;

    move-result-object v2

    .line 174
    new-instance v3, Lcom/squareup/ui/orderhub/order/adjusttime/AdjustPickupTimeScreenData;

    invoke-direct {v3, v1, v2}, Lcom/squareup/ui/orderhub/order/adjusttime/AdjustPickupTimeScreenData;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    new-instance v1, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$4;

    invoke-direct {v1, p0, p2}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$4;-><init>(Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;Lcom/squareup/ui/orderhub/order/OrderDetailsState;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v1}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p2

    .line 173
    new-instance p3, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeScreen;

    invoke-direct {p3, v3, p2}, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeScreen;-><init>(Lcom/squareup/ui/orderhub/order/adjusttime/AdjustPickupTimeScreenData;Lkotlin/jvm/functions/Function1;)V

    check-cast p3, Lcom/squareup/workflow/legacy/V2Screen;

    .line 895
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 896
    const-class v1, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeScreen;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    .line 897
    sget-object v1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    .line 895
    invoke-direct {p2, v0, p3, v1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 172
    invoke-virtual {p1, p2}, Lcom/squareup/container/PosLayering$Companion;->bodyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_1

    .line 175
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Order should have a pickup time in order to adjust the pickup time."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 208
    :cond_1
    instance-of p1, p2, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;

    if-eqz p1, :cond_3

    .line 209
    move-object p1, p2

    check-cast p1, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;->getRetryInventoryAdjustment()Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$InventoryAdjustmentRetry;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;->getOrderUpdateFailureState()Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 210
    sget-object v2, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 211
    new-instance v1, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;

    .line 212
    new-instance v3, Lcom/squareup/ui/orderhub/order/OrderDetailsScreenData$ShowingOrderDetailsScreen;

    invoke-direct {v3, p2}, Lcom/squareup/ui/orderhub/order/OrderDetailsScreenData$ShowingOrderDetailsScreen;-><init>(Lcom/squareup/ui/orderhub/order/OrderDetailsState;)V

    check-cast v3, Lcom/squareup/ui/orderhub/order/OrderDetailsScreenData;

    .line 213
    sget-object v4, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$5;->INSTANCE:Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$5;

    check-cast v4, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v4}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v4

    .line 211
    invoke-direct {v1, v3, v4}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;-><init>(Lcom/squareup/ui/orderhub/order/OrderDetailsScreenData;Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 900
    new-instance v3, Lcom/squareup/workflow/legacy/Screen;

    .line 901
    const-class v4, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;

    invoke-static {v4}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v4

    invoke-static {v4, v0}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v4

    .line 902
    sget-object v5, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v5}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v5

    .line 900
    invoke-direct {v3, v4, v1, v5}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 214
    new-instance v1, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen;

    .line 215
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;->getOrderUpdateFailureState()Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    move-result-object p1

    new-instance v6, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$6;

    invoke-direct {v6, p2}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$6;-><init>(Lcom/squareup/ui/orderhub/order/OrderDetailsState;)V

    check-cast v6, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v6}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p2

    .line 214
    invoke-direct {v1, p1, p2}, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen;-><init>(Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 905
    new-instance v6, Lcom/squareup/workflow/legacy/Screen;

    .line 906
    const-class p1, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen;

    invoke-static {p1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p1

    invoke-static {p1, v0}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p1

    .line 907
    sget-object p2, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {p2}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p2

    .line 905
    invoke-direct {v6, p1, v1, p2}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    const/4 v7, 0x6

    const/4 v8, 0x0

    .line 210
    invoke-static/range {v2 .. v8}, Lcom/squareup/container/PosLayering$Companion;->dialogStack$default(Lcom/squareup/container/PosLayering$Companion;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;ILjava/lang/Object;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_1

    .line 241
    :cond_2
    invoke-direct {p0, p1, p3}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;->getMarkCanceledChildRendering(Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    .line 242
    sget-object p2, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lkotlin/collections/MapsKt;->getValue(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    move-object v1, p2

    check-cast v1, Lcom/squareup/workflow/legacy/Screen;

    .line 243
    sget-object p2, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    invoke-interface {p1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    move-object v5, p1

    check-cast v5, Lcom/squareup/workflow/legacy/Screen;

    .line 244
    sget-object v0, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v6, 0xe

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/squareup/container/PosLayering$Companion;->fullStack$default(Lcom/squareup/container/PosLayering$Companion;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;ILjava/lang/Object;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_1

    .line 247
    :cond_3
    instance-of p1, p2, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;

    if-eqz p1, :cond_4

    .line 248
    check-cast p2, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;

    invoke-direct {p0, p2, p3}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;->getMarkShippedChildRendering(Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    .line 249
    sget-object p2, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lkotlin/collections/MapsKt;->getValue(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    move-object v1, p2

    check-cast v1, Lcom/squareup/workflow/legacy/Screen;

    .line 250
    sget-object p2, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    invoke-interface {p1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    move-object v5, p1

    check-cast v5, Lcom/squareup/workflow/legacy/Screen;

    .line 251
    sget-object v0, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v6, 0xe

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/squareup/container/PosLayering$Companion;->fullStack$default(Lcom/squareup/container/PosLayering$Companion;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;ILjava/lang/Object;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_1

    .line 253
    :cond_4
    instance-of p1, p2, Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;

    const/4 v1, 0x0

    if-eqz p1, :cond_6

    .line 254
    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getShowSpinner()Z

    move-result p1

    if-eqz p1, :cond_5

    .line 255
    move-object p1, p2

    check-cast p1, Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;

    invoke-direct {p0, p1}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;->logShipmentTrackingInfoEvent(Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;)V

    .line 257
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;->orderRepository:Lcom/squareup/ordermanagerdata/OrderRepository;

    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;->getFulfillment()Lcom/squareup/orders/model/Order$Fulfillment;

    move-result-object v3

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;->getTracking()Lcom/squareup/ordermanagerdata/TrackingInfo;

    move-result-object p1

    invoke-interface {v0, v2, v3, p1}, Lcom/squareup/ordermanagerdata/OrderRepository;->editTracking(Lcom/squareup/orders/model/Order;Lcom/squareup/orders/model/Order$Fulfillment;Lcom/squareup/ordermanagerdata/TrackingInfo;)Lio/reactivex/Single;

    move-result-object p1

    .line 909
    sget-object v0, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$$inlined$asWorker$1;

    invoke-direct {v0, p1, v1}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 910
    invoke-static {v0}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 911
    const-class v0, Lcom/squareup/ordermanagerdata/ResultState;

    sget-object v1, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class v2, Lcom/squareup/orders/model/Order;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v2

    invoke-virtual {v1, v2}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;Lkotlin/reflect/KTypeProjection;)Lkotlin/reflect/KType;

    move-result-object v0

    new-instance v1, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v1, v0, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v3, v1

    check-cast v3, Lcom/squareup/workflow/Worker;

    const/4 v4, 0x0

    .line 259
    new-instance p1, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$7;

    invoke-direct {p1, p2}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$7;-><init>(Lcom/squareup/ui/orderhub/order/OrderDetailsState;)V

    move-object v5, p1

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x2

    const/4 v7, 0x0

    move-object v2, p3

    invoke-static/range {v2 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 283
    :cond_5
    check-cast p2, Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;

    invoke-direct {p0, p2, p3}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;->getEditTrackingChildRendering(Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    .line 284
    sget-object p2, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lkotlin/collections/MapsKt;->getValue(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    move-object v1, p2

    check-cast v1, Lcom/squareup/workflow/legacy/Screen;

    .line 285
    sget-object p2, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    invoke-interface {p1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    move-object v5, p1

    check-cast v5, Lcom/squareup/workflow/legacy/Screen;

    .line 286
    sget-object v0, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v6, 0xe

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/squareup/container/PosLayering$Companion;->fullStack$default(Lcom/squareup/container/PosLayering$Companion;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;ILjava/lang/Object;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_1

    .line 289
    :cond_6
    instance-of p1, p2, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;

    const-string v2, "state.order.id"

    if-eqz p1, :cond_b

    .line 290
    move-object p1, p2

    check-cast p1, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;->getAction()Lcom/squareup/protos/client/orders/Action;

    move-result-object v3

    if-eqz v3, :cond_8

    .line 291
    iget-object v3, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;->orderHubAnalytics:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;

    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v4

    iget-object v4, v4, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    invoke-static {v4, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;->getAction()Lcom/squareup/protos/client/orders/Action;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/protos/client/orders/Action;->type:Lcom/squareup/protos/client/orders/Action$Type;

    const-string v5, "state.action.type"

    invoke-static {v2, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3, v4, v2}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;->logAction$orderhub_applet_release(Ljava/lang/String;Lcom/squareup/protos/client/orders/Action$Type;)V

    .line 292
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;->getAction()Lcom/squareup/protos/client/orders/Action;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/protos/client/orders/Action;->type:Lcom/squareup/protos/client/orders/Action$Type;

    sget-object v3, Lcom/squareup/protos/client/orders/Action$Type;->MARK_IN_PROGRESS:Lcom/squareup/protos/client/orders/Action$Type;

    if-ne v2, v3, :cond_7

    .line 293
    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;->orderRepository:Lcom/squareup/ordermanagerdata/OrderRepository;

    .line 295
    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v3

    .line 296
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;->getPickupTimeOverride()Ljava/lang/String;

    move-result-object v4

    .line 294
    invoke-interface {v2, v3, v4}, Lcom/squareup/ordermanagerdata/OrderRepository;->markOrderInProgress(Lcom/squareup/orders/model/Order;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v2

    .line 912
    sget-object v3, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v3, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$$inlined$asWorker$2;

    invoke-direct {v3, v2, v1}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$$inlined$asWorker$2;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    .line 913
    invoke-static {v3}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v1

    .line 914
    const-class v2, Lcom/squareup/ordermanagerdata/ResultState;

    sget-object v3, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class v4, Lcom/squareup/orders/model/Order;

    invoke-static {v4}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v4

    invoke-virtual {v3, v4}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;Lkotlin/reflect/KTypeProjection;)Lkotlin/reflect/KType;

    move-result-object v2

    new-instance v3, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v3, v2, v1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v3, Lcom/squareup/workflow/Worker;

    goto :goto_0

    .line 300
    :cond_7
    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;->orderRepository:Lcom/squareup/ordermanagerdata/OrderRepository;

    .line 302
    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v3

    .line 303
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;->getAction()Lcom/squareup/protos/client/orders/Action;

    move-result-object v4

    iget-object v4, v4, Lcom/squareup/protos/client/orders/Action;->type:Lcom/squareup/protos/client/orders/Action$Type;

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 301
    invoke-interface {v2, v3, v4}, Lcom/squareup/ordermanagerdata/OrderRepository;->transitionOrder(Lcom/squareup/orders/model/Order;Lcom/squareup/protos/client/orders/Action$Type;)Lio/reactivex/Single;

    move-result-object v2

    .line 915
    sget-object v3, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v3, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$$inlined$asWorker$3;

    invoke-direct {v3, v2, v1}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$$inlined$asWorker$3;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    .line 916
    invoke-static {v3}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v1

    .line 917
    const-class v2, Lcom/squareup/ordermanagerdata/ResultState;

    sget-object v3, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class v4, Lcom/squareup/orders/model/Order;

    invoke-static {v4}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v4

    invoke-virtual {v3, v4}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;Lkotlin/reflect/KTypeProjection;)Lkotlin/reflect/KType;

    move-result-object v2

    new-instance v3, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v3, v2, v1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v3, Lcom/squareup/workflow/Worker;

    :goto_0
    move-object v5, v3

    const/4 v6, 0x0

    .line 308
    new-instance v1, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$8;

    invoke-direct {v1, p2}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$8;-><init>(Lcom/squareup/ui/orderhub/order/OrderDetailsState;)V

    move-object v7, v1

    check-cast v7, Lkotlin/jvm/functions/Function1;

    const/4 v8, 0x2

    const/4 v9, 0x0

    move-object v4, p3

    invoke-static/range {v4 .. v9}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 335
    :cond_8
    new-instance v1, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$onEvent$1;

    invoke-direct {v1, p0, p2}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$onEvent$1;-><init>(Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;Lcom/squareup/ui/orderhub/order/OrderDetailsState;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v1}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v1

    .line 404
    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getShowSpinner()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 405
    sget-object p1, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 406
    new-instance p2, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;

    sget-object p3, Lcom/squareup/ui/orderhub/order/OrderDetailsScreenData$ShowingOrderDetailsSpinner;->INSTANCE:Lcom/squareup/ui/orderhub/order/OrderDetailsScreenData$ShowingOrderDetailsSpinner;

    check-cast p3, Lcom/squareup/ui/orderhub/order/OrderDetailsScreenData;

    invoke-direct {p2, p3, v1}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;-><init>(Lcom/squareup/ui/orderhub/order/OrderDetailsScreenData;Lkotlin/jvm/functions/Function1;)V

    check-cast p2, Lcom/squareup/workflow/legacy/V2Screen;

    .line 919
    new-instance p3, Lcom/squareup/workflow/legacy/Screen;

    .line 920
    const-class v1, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    .line 921
    sget-object v1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    .line 919
    invoke-direct {p3, v0, p2, v1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 405
    invoke-virtual {p1, p3}, Lcom/squareup/container/PosLayering$Companion;->bodyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_1

    .line 408
    :cond_9
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;->getOrderUpdateFailureState()Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    move-result-object v2

    if-eqz v2, :cond_a

    sget-object v3, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 409
    new-instance v2, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;

    .line 410
    new-instance v4, Lcom/squareup/ui/orderhub/order/OrderDetailsScreenData$ShowingOrderDetailsScreen;

    invoke-direct {v4, p2}, Lcom/squareup/ui/orderhub/order/OrderDetailsScreenData$ShowingOrderDetailsScreen;-><init>(Lcom/squareup/ui/orderhub/order/OrderDetailsState;)V

    check-cast v4, Lcom/squareup/ui/orderhub/order/OrderDetailsScreenData;

    .line 409
    invoke-direct {v2, v4, v1}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;-><init>(Lcom/squareup/ui/orderhub/order/OrderDetailsScreenData;Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lcom/squareup/workflow/legacy/V2Screen;

    .line 924
    new-instance v4, Lcom/squareup/workflow/legacy/Screen;

    .line 925
    const-class v1, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    .line 926
    sget-object v5, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v5}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v5

    .line 924
    invoke-direct {v4, v1, v2, v5}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 412
    new-instance v1, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen;

    .line 413
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;->getOrderUpdateFailureState()Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    move-result-object p1

    new-instance v2, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$9;

    invoke-direct {v2, p2}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$9;-><init>(Lcom/squareup/ui/orderhub/order/OrderDetailsState;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v2}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p2

    .line 412
    invoke-direct {v1, p1, p2}, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen;-><init>(Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 929
    new-instance v7, Lcom/squareup/workflow/legacy/Screen;

    .line 930
    const-class p1, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen;

    invoke-static {p1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p1

    invoke-static {p1, v0}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p1

    .line 931
    sget-object p2, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {p2}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p2

    .line 929
    invoke-direct {v7, p1, v1, p2}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    const/4 v8, 0x6

    const/4 v9, 0x0

    .line 408
    invoke-static/range {v3 .. v9}, Lcom/squareup/container/PosLayering$Companion;->dialogStack$default(Lcom/squareup/container/PosLayering$Companion;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;ILjava/lang/Object;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_1

    .line 469
    :cond_a
    sget-object p1, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 470
    new-instance p3, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;

    new-instance v2, Lcom/squareup/ui/orderhub/order/OrderDetailsScreenData$ShowingOrderDetailsScreen;

    invoke-direct {v2, p2}, Lcom/squareup/ui/orderhub/order/OrderDetailsScreenData$ShowingOrderDetailsScreen;-><init>(Lcom/squareup/ui/orderhub/order/OrderDetailsState;)V

    check-cast v2, Lcom/squareup/ui/orderhub/order/OrderDetailsScreenData;

    invoke-direct {p3, v2, v1}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;-><init>(Lcom/squareup/ui/orderhub/order/OrderDetailsScreenData;Lkotlin/jvm/functions/Function1;)V

    check-cast p3, Lcom/squareup/workflow/legacy/V2Screen;

    .line 934
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 935
    const-class v1, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    .line 936
    sget-object v1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    .line 934
    invoke-direct {p2, v0, p3, v1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 469
    invoke-virtual {p1, p2}, Lcom/squareup/container/PosLayering$Companion;->bodyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_1

    .line 474
    :cond_b
    instance-of p1, p2, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RefreshingOrderState;

    if-eqz p1, :cond_c

    .line 475
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;->orderRepository:Lcom/squareup/ordermanagerdata/OrderRepository;

    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v3

    iget-object v3, v3, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, v3}, Lcom/squareup/ordermanagerdata/OrderRepository;->getOrder(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    .line 938
    sget-object v2, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v2, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$$inlined$asWorker$4;

    invoke-direct {v2, p1, v1}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$$inlined$asWorker$4;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 939
    invoke-static {v2}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 940
    const-class v1, Lcom/squareup/ordermanagerdata/ResultState;

    sget-object v2, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class v3, Lcom/squareup/orders/model/Order;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v3

    invoke-virtual {v2, v3}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;Lkotlin/reflect/KTypeProjection;)Lkotlin/reflect/KType;

    move-result-object v1

    new-instance v2, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v2, v1, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v4, v2

    check-cast v4, Lcom/squareup/workflow/Worker;

    const/4 v5, 0x0

    .line 475
    new-instance p1, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$10;

    invoke-direct {p1, p2}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$10;-><init>(Lcom/squareup/ui/orderhub/order/OrderDetailsState;)V

    move-object v6, p1

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v7, 0x2

    const/4 v8, 0x0

    move-object v3, p3

    invoke-static/range {v3 .. v8}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 501
    sget-object p1, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 502
    new-instance p2, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;

    .line 503
    sget-object v1, Lcom/squareup/ui/orderhub/order/OrderDetailsScreenData$ShowingOrderDetailsSpinner;->INSTANCE:Lcom/squareup/ui/orderhub/order/OrderDetailsScreenData$ShowingOrderDetailsSpinner;

    check-cast v1, Lcom/squareup/ui/orderhub/order/OrderDetailsScreenData;

    sget-object v2, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$11;->INSTANCE:Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$11;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v2}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p3

    .line 502
    invoke-direct {p2, v1, p3}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;-><init>(Lcom/squareup/ui/orderhub/order/OrderDetailsScreenData;Lkotlin/jvm/functions/Function1;)V

    check-cast p2, Lcom/squareup/workflow/legacy/V2Screen;

    .line 942
    new-instance p3, Lcom/squareup/workflow/legacy/Screen;

    .line 943
    const-class v1, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    .line 944
    sget-object v1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    .line 942
    invoke-direct {p3, v0, p2, v1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 501
    invoke-virtual {p1, p3}, Lcom/squareup/container/PosLayering$Companion;->bodyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_1

    .line 506
    :cond_c
    instance-of p1, p2, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;

    if-eqz p1, :cond_10

    .line 507
    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getShowSpinner()Z

    move-result p1

    if-eqz p1, :cond_d

    .line 508
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;->orderHubBillLoader:Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;->clear$orderhub_applet_release()V

    .line 510
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;->orderHubBillLoader:Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;

    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/ui/orderhub/util/proto/OrdersKt;->getBillServerToken(Lcom/squareup/orders/model/Order;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;->loadFromToken$orderhub_applet_release(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    .line 946
    sget-object v2, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v2, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$$inlined$asWorker$5;

    invoke-direct {v2, p1, v1}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$$inlined$asWorker$5;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 947
    invoke-static {v2}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 948
    const-class v1, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v1

    new-instance v2, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v2, v1, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v4, v2

    check-cast v4, Lcom/squareup/workflow/Worker;

    const/4 v5, 0x0

    .line 511
    new-instance p1, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$12;

    invoke-direct {p1, p0, p2}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$12;-><init>(Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;Lcom/squareup/ui/orderhub/order/OrderDetailsState;)V

    move-object v6, p1

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v7, 0x2

    const/4 v8, 0x0

    move-object v3, p3

    .line 509
    invoke-static/range {v3 .. v8}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 554
    :cond_d
    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getShowSpinner()Z

    move-result p1

    if-eqz p1, :cond_e

    .line 555
    sget-object p1, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 556
    new-instance p2, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;

    .line 557
    sget-object v1, Lcom/squareup/ui/orderhub/order/OrderDetailsScreenData$ShowingOrderDetailsSpinner;->INSTANCE:Lcom/squareup/ui/orderhub/order/OrderDetailsScreenData$ShowingOrderDetailsSpinner;

    check-cast v1, Lcom/squareup/ui/orderhub/order/OrderDetailsScreenData;

    sget-object v2, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$13;->INSTANCE:Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$13;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v2}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p3

    .line 556
    invoke-direct {p2, v1, p3}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;-><init>(Lcom/squareup/ui/orderhub/order/OrderDetailsScreenData;Lkotlin/jvm/functions/Function1;)V

    check-cast p2, Lcom/squareup/workflow/legacy/V2Screen;

    .line 950
    new-instance p3, Lcom/squareup/workflow/legacy/Screen;

    .line 951
    const-class v1, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    .line 952
    sget-object v1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    .line 950
    invoke-direct {p3, v0, p2, v1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 555
    invoke-virtual {p1, p3}, Lcom/squareup/container/PosLayering$Companion;->bodyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    goto :goto_1

    .line 560
    :cond_e
    move-object p1, p2

    check-cast p1, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->getOrderUpdateFailureState()Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    move-result-object v1

    if-eqz v1, :cond_f

    .line 561
    sget-object v2, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 562
    new-instance v1, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;

    .line 563
    new-instance v3, Lcom/squareup/ui/orderhub/order/OrderDetailsScreenData$ShowingOrderDetailsScreen;

    invoke-direct {v3, p2}, Lcom/squareup/ui/orderhub/order/OrderDetailsScreenData$ShowingOrderDetailsScreen;-><init>(Lcom/squareup/ui/orderhub/order/OrderDetailsState;)V

    check-cast v3, Lcom/squareup/ui/orderhub/order/OrderDetailsScreenData;

    .line 564
    sget-object v4, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$14;->INSTANCE:Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$14;

    check-cast v4, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v4}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v4

    .line 562
    invoke-direct {v1, v3, v4}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;-><init>(Lcom/squareup/ui/orderhub/order/OrderDetailsScreenData;Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 955
    new-instance v3, Lcom/squareup/workflow/legacy/Screen;

    .line 956
    const-class v4, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;

    invoke-static {v4}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v4

    invoke-static {v4, v0}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v4

    .line 957
    sget-object v5, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v5}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v5

    .line 955
    invoke-direct {v3, v4, v1, v5}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 565
    new-instance v1, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen;

    .line 566
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->getOrderUpdateFailureState()Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    move-result-object p1

    new-instance v6, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$15;

    invoke-direct {v6, p2}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$15;-><init>(Lcom/squareup/ui/orderhub/order/OrderDetailsState;)V

    check-cast v6, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v6}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p2

    .line 565
    invoke-direct {v1, p1, p2}, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen;-><init>(Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 960
    new-instance v6, Lcom/squareup/workflow/legacy/Screen;

    .line 961
    const-class p1, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen;

    invoke-static {p1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p1

    invoke-static {p1, v0}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p1

    .line 962
    sget-object p2, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {p2}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p2

    .line 960
    invoke-direct {v6, p1, v1, p2}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    const/4 v7, 0x6

    const/4 v8, 0x0

    .line 561
    invoke-static/range {v2 .. v8}, Lcom/squareup/container/PosLayering$Companion;->dialogStack$default(Lcom/squareup/container/PosLayering$Companion;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;ILjava/lang/Object;)Ljava/util/Map;

    move-result-object p1

    :goto_1
    return-object p1

    .line 560
    :cond_f
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Required value was null."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 554
    :cond_10
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 892
    :cond_11
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 888
    :cond_12
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/ui/orderhub/order/OrderDetailsState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 128
    sget-object p1, Lcom/squareup/workflow/Snapshot;->EMPTY:Lcom/squareup/workflow/Snapshot;

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 112
    check-cast p1, Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;->snapshotState(Lcom/squareup/ui/orderhub/order/OrderDetailsState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
