.class public final Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionResult$GoBackFromItemSelection;
.super Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionResult;
.source "OrderItemSelectionState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GoBackFromItemSelection"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionResult$GoBackFromItemSelection;",
        "Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionResult;",
        "()V",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionResult$GoBackFromItemSelection;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    new-instance v0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionResult$GoBackFromItemSelection;

    invoke-direct {v0}, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionResult$GoBackFromItemSelection;-><init>()V

    sput-object v0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionResult$GoBackFromItemSelection;->INSTANCE:Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionResult$GoBackFromItemSelection;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 20
    invoke-direct {p0, v0}, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionResult;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method
