.class public final Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter;
.super Ljava/lang/Object;
.source "OrderHubRefundFlowStarter.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u0001B\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008JO\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000c20\u0008\u0002\u0010\r\u001a*\u0012\u0004\u0012\u00020\u000f\u0012\u0018\u0012\u0016\u0012\u0008\u0012\u00060\u000fj\u0002`\u0010\u0012\u0004\u0012\u00020\u00110\u000ej\u0002`\u0012\u0018\u00010\u000ej\u0004\u0018\u0001`\u00132\u0006\u0010\u0014\u001a\u00020\u0015H\u0000\u00a2\u0006\u0002\u0008\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter;",
        "",
        "flow",
        "Lflow/Flow;",
        "permissionGatekeeper",
        "Lcom/squareup/permissions/PermissionGatekeeper;",
        "orderHubRefundFlowState",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;",
        "(Lflow/Flow;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;)V",
        "initiateRefundFlow",
        "",
        "bill",
        "Lcom/squareup/billhistory/model/BillHistory;",
        "selectedLineItems",
        "",
        "",
        "Lcom/squareup/ui/orderhub/order/itemselection/LineItemRowIdentifier;",
        "Lcom/squareup/orders/model/Order$LineItem;",
        "Lcom/squareup/ui/orderhub/order/itemselection/LineItemWithQuantityByIdentifier;",
        "Lcom/squareup/ui/orderhub/order/itemselection/LineItemSelectionsByUid;",
        "skipRestock",
        "",
        "initiateRefundFlow$orderhub_applet_release",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final flow:Lflow/Flow;

.field private final orderHubRefundFlowState:Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;

.field private final permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;


# direct methods
.method public constructor <init>(Lflow/Flow;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "flow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "permissionGatekeeper"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderHubRefundFlowState"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter;->flow:Lflow/Flow;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    iput-object p3, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter;->orderHubRefundFlowState:Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;

    return-void
.end method

.method public static final synthetic access$getFlow$p(Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter;)Lflow/Flow;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter;->flow:Lflow/Flow;

    return-object p0
.end method

.method public static final synthetic access$getOrderHubRefundFlowState$p(Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter;)Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter;->orderHubRefundFlowState:Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;

    return-object p0
.end method

.method public static synthetic initiateRefundFlow$orderhub_applet_release$default(Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter;Lcom/squareup/billhistory/model/BillHistory;Ljava/util/Map;ZILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p4, p4, 0x2

    if-eqz p4, :cond_0

    const/4 p2, 0x0

    .line 26
    check-cast p2, Ljava/util/Map;

    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter;->initiateRefundFlow$orderhub_applet_release(Lcom/squareup/billhistory/model/BillHistory;Ljava/util/Map;Z)V

    return-void
.end method


# virtual methods
.method public final initiateRefundFlow$orderhub_applet_release(Lcom/squareup/billhistory/model/BillHistory;Ljava/util/Map;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/billhistory/model/BillHistory;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;>;Z)V"
        }
    .end annotation

    const-string v0, "bill"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    .line 30
    sget-object v1, Lcom/squareup/permissions/Permission;->ISSUE_REFUNDS:Lcom/squareup/permissions/Permission;

    new-instance v2, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter$initiateRefundFlow$1;

    invoke-direct {v2, p0, p1, p3, p2}, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter$initiateRefundFlow$1;-><init>(Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter;Lcom/squareup/billhistory/model/BillHistory;ZLjava/util/Map;)V

    check-cast v2, Lcom/squareup/permissions/PermissionGatekeeper$When;

    .line 29
    invoke-virtual {v0, v1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    return-void
.end method
