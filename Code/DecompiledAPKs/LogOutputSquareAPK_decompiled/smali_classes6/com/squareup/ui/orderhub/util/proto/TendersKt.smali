.class public final Lcom/squareup/ui/orderhub/util/proto/TendersKt;
.super Ljava/lang/Object;
.source "Tenders.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a\u0012\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0004\u001a\u0014\u0010\u0005\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0004H\u0002\u00a8\u0006\u0006"
    }
    d2 = {
        "getTenderDetails",
        "",
        "Lcom/squareup/protos/connect/v2/resources/Tender;",
        "res",
        "Lcom/squareup/util/Res;",
        "getTenderType",
        "orderhub-applet_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final getTenderDetails(Lcom/squareup/protos/connect/v2/resources/Tender;Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 3

    const-string v0, "$this$getTenderDetails"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-static {p0, p1}, Lcom/squareup/ui/orderhub/util/proto/TendersKt;->getTenderType(Lcom/squareup/protos/connect/v2/resources/Tender;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v0

    .line 26
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->type:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    sget-object v2, Lcom/squareup/protos/connect/v2/resources/Tender$Type;->CARD:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    if-eq v1, v2, :cond_0

    return-object v0

    .line 30
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->card_details:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->card_details:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;

    iget-object v1, v1, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;->card:Lcom/squareup/protos/connect/v2/resources/Card;

    if-eqz v1, :cond_2

    .line 31
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->card_details:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;

    iget-object v1, v1, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;->card:Lcom/squareup/protos/connect/v2/resources/Card;

    iget-object v1, v1, Lcom/squareup/protos/connect/v2/resources/Card;->card_brand:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    if-eqz v1, :cond_1

    .line 34
    iget-object v0, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->card_details:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;->card:Lcom/squareup/protos/connect/v2/resources/Card;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/resources/Card;->card_brand:Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    invoke-static {v0}, Lcom/squareup/card/ConnectCardBrandConverter;->toCardBrand(Lcom/squareup/protos/connect/v2/resources/Card$Brand;)Lcom/squareup/Card$Brand;

    move-result-object v0

    .line 33
    invoke-static {v0}, Lcom/squareup/text/CardBrandResources;->forBrand(Lcom/squareup/Card$Brand;)Lcom/squareup/text/CardBrandResources;

    move-result-object v0

    iget v0, v0, Lcom/squareup/text/CardBrandResources;->brandNameId:I

    .line 32
    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 38
    :cond_1
    iget-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->card_details:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;->card:Lcom/squareup/protos/connect/v2/resources/Card;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/resources/Card;->last_4:Ljava/lang/String;

    if-eqz p1, :cond_2

    .line 39
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 40
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " "

    .line 41
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 42
    iget-object p0, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->card_details:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;

    iget-object p0, p0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;->card:Lcom/squareup/protos/connect/v2/resources/Card;

    iget-object p0, p0, Lcom/squareup/protos/connect/v2/resources/Card;->last_4:Ljava/lang/String;

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 43
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string p0, "StringBuilder()\n        \u2026_4)\n          .toString()"

    invoke-static {v0, p0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_2
    return-object v0
.end method

.method private static final getTenderType(Lcom/squareup/protos/connect/v2/resources/Tender;Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 2

    .line 50
    iget-object p0, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->type:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    const-string v0, ""

    if-nez p0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/squareup/ui/orderhub/util/proto/TendersKt$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/resources/Tender$Type;->ordinal()I

    move-result p0

    aget p0, v1, p0

    packed-switch p0, :pswitch_data_0

    .line 60
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    .line 59
    :pswitch_0
    sget p0, Lcom/squareup/billhistory/R$string;->payment_type_other:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 58
    :pswitch_1
    sget p0, Lcom/squareup/common/card/R$string;->gift_card:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 57
    :pswitch_2
    sget p0, Lcom/squareup/billhistory/R$string;->payment_type_other:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 55
    :pswitch_3
    sget p0, Lcom/squareup/billhistory/R$string;->payment_type_other:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 54
    :pswitch_4
    sget p0, Lcom/squareup/common/card/R$string;->gift_card:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 53
    :pswitch_5
    sget p0, Lcom/squareup/common/card/R$string;->card_brand_unknown:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 52
    :pswitch_6
    sget p0, Lcom/squareup/billhistory/R$string;->cash:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 51
    :pswitch_7
    sget p0, Lcom/squareup/common/card/R$string;->card_brand_unknown:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    :pswitch_8
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_8
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
