.class public final Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;
.super Ljava/lang/Object;
.source "OrderMarkShippedState.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u001a\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u0000 12\u00020\u0001:\u00011Be\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012*\u0010\u0008\u001a&\u0012\u0004\u0012\u00020\n\u0012\u0018\u0012\u0016\u0012\u0008\u0012\u00060\nj\u0002`\u000b\u0012\u0004\u0012\u00020\u000c0\tj\u0002`\r0\tj\u0002`\u000e\u0012\u0008\u0010\u000f\u001a\u0004\u0018\u00010\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u00a2\u0006\u0002\u0010\u0015J\t\u0010$\u001a\u00020\u0003H\u00c6\u0003J\t\u0010%\u001a\u00020\u0005H\u00c6\u0003J\u000b\u0010&\u001a\u0004\u0018\u00010\u0007H\u00c6\u0003J-\u0010\'\u001a&\u0012\u0004\u0012\u00020\n\u0012\u0018\u0012\u0016\u0012\u0008\u0012\u00060\nj\u0002`\u000b\u0012\u0004\u0012\u00020\u000c0\tj\u0002`\r0\tj\u0002`\u000eH\u00c6\u0003J\u000b\u0010(\u001a\u0004\u0018\u00010\u0010H\u00c6\u0003J\t\u0010)\u001a\u00020\u0012H\u00c6\u0003J\t\u0010*\u001a\u00020\u0014H\u00c6\u0003Jw\u0010+\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00072,\u0008\u0002\u0010\u0008\u001a&\u0012\u0004\u0012\u00020\n\u0012\u0018\u0012\u0016\u0012\u0008\u0012\u00060\nj\u0002`\u000b\u0012\u0004\u0012\u00020\u000c0\tj\u0002`\r0\tj\u0002`\u000e2\n\u0008\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u00102\u0008\u0008\u0002\u0010\u0011\u001a\u00020\u00122\u0008\u0008\u0002\u0010\u0013\u001a\u00020\u0014H\u00c6\u0001J\u0013\u0010,\u001a\u00020\u00052\u0008\u0010-\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010.\u001a\u00020/H\u00d6\u0001J\t\u00100\u001a\u00020\nH\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017R\u0011\u0010\u0013\u001a\u00020\u0014\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019R\u0011\u0010\u0011\u001a\u00020\u0012\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u001bR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001c\u0010\u001dR\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001e\u0010\u001fR5\u0010\u0008\u001a&\u0012\u0004\u0012\u00020\n\u0012\u0018\u0012\u0016\u0012\u0008\u0012\u00060\nj\u0002`\u000b\u0012\u0004\u0012\u00020\u000c0\tj\u0002`\r0\tj\u0002`\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008 \u0010!R\u0013\u0010\u000f\u001a\u0004\u0018\u00010\u0010\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\"\u0010#\u00a8\u00062"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;",
        "",
        "order",
        "Lcom/squareup/orders/model/Order;",
        "canShowErrorDialog",
        "",
        "orderUpdateFailureState",
        "Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;",
        "selectedLineItems",
        "",
        "",
        "Lcom/squareup/ui/orderhub/order/itemselection/LineItemRowIdentifier;",
        "Lcom/squareup/orders/model/Order$LineItem;",
        "Lcom/squareup/ui/orderhub/order/itemselection/LineItemWithQuantityByIdentifier;",
        "Lcom/squareup/ui/orderhub/order/itemselection/LineItemSelectionsByUid;",
        "trackingInfo",
        "Lcom/squareup/ordermanagerdata/TrackingInfo;",
        "nextShipmentAction",
        "Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState$Companion$ShipmentAction;",
        "markShippedAction",
        "Lcom/squareup/protos/client/orders/Action;",
        "(Lcom/squareup/orders/model/Order;ZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Ljava/util/Map;Lcom/squareup/ordermanagerdata/TrackingInfo;Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState$Companion$ShipmentAction;Lcom/squareup/protos/client/orders/Action;)V",
        "getCanShowErrorDialog",
        "()Z",
        "getMarkShippedAction",
        "()Lcom/squareup/protos/client/orders/Action;",
        "getNextShipmentAction",
        "()Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState$Companion$ShipmentAction;",
        "getOrder",
        "()Lcom/squareup/orders/model/Order;",
        "getOrderUpdateFailureState",
        "()Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;",
        "getSelectedLineItems",
        "()Ljava/util/Map;",
        "getTrackingInfo",
        "()Lcom/squareup/ordermanagerdata/TrackingInfo;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "Companion",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState$Companion;


# instance fields
.field private final canShowErrorDialog:Z

.field private final markShippedAction:Lcom/squareup/protos/client/orders/Action;

.field private final nextShipmentAction:Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState$Companion$ShipmentAction;

.field private final order:Lcom/squareup/orders/model/Order;

.field private final orderUpdateFailureState:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

.field private final selectedLineItems:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;>;"
        }
    .end annotation
.end field

.field private final trackingInfo:Lcom/squareup/ordermanagerdata/TrackingInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->Companion:Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/orders/model/Order;ZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Ljava/util/Map;Lcom/squareup/ordermanagerdata/TrackingInfo;Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState$Companion$ShipmentAction;Lcom/squareup/protos/client/orders/Action;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/orders/model/Order;",
            "Z",
            "Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;>;",
            "Lcom/squareup/ordermanagerdata/TrackingInfo;",
            "Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState$Companion$ShipmentAction;",
            "Lcom/squareup/protos/client/orders/Action;",
            ")V"
        }
    .end annotation

    const-string v0, "order"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedLineItems"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nextShipmentAction"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "markShippedAction"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->order:Lcom/squareup/orders/model/Order;

    iput-boolean p2, p0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->canShowErrorDialog:Z

    iput-object p3, p0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->orderUpdateFailureState:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    iput-object p4, p0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->selectedLineItems:Ljava/util/Map;

    iput-object p5, p0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->trackingInfo:Lcom/squareup/ordermanagerdata/TrackingInfo;

    iput-object p6, p0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->nextShipmentAction:Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState$Companion$ShipmentAction;

    iput-object p7, p0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->markShippedAction:Lcom/squareup/protos/client/orders/Action;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;Lcom/squareup/orders/model/Order;ZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Ljava/util/Map;Lcom/squareup/ordermanagerdata/TrackingInfo;Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState$Companion$ShipmentAction;Lcom/squareup/protos/client/orders/Action;ILjava/lang/Object;)Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;
    .locals 5

    and-int/lit8 p9, p8, 0x1

    if-eqz p9, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->order:Lcom/squareup/orders/model/Order;

    :cond_0
    and-int/lit8 p9, p8, 0x2

    if-eqz p9, :cond_1

    iget-boolean p2, p0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->canShowErrorDialog:Z

    :cond_1
    move p9, p2

    and-int/lit8 p2, p8, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->orderUpdateFailureState:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p8, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->selectedLineItems:Ljava/util/Map;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p8, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->trackingInfo:Lcom/squareup/ordermanagerdata/TrackingInfo;

    :cond_4
    move-object v2, p5

    and-int/lit8 p2, p8, 0x20

    if-eqz p2, :cond_5

    iget-object p6, p0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->nextShipmentAction:Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState$Companion$ShipmentAction;

    :cond_5
    move-object v3, p6

    and-int/lit8 p2, p8, 0x40

    if-eqz p2, :cond_6

    iget-object p7, p0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->markShippedAction:Lcom/squareup/protos/client/orders/Action;

    :cond_6
    move-object v4, p7

    move-object p2, p0

    move-object p3, p1

    move p4, p9

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    move-object p8, v3

    move-object p9, v4

    invoke-virtual/range {p2 .. p9}, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->copy(Lcom/squareup/orders/model/Order;ZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Ljava/util/Map;Lcom/squareup/ordermanagerdata/TrackingInfo;Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState$Companion$ShipmentAction;Lcom/squareup/protos/client/orders/Action;)Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/orders/model/Order;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->order:Lcom/squareup/orders/model/Order;

    return-object v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->canShowErrorDialog:Z

    return v0
.end method

.method public final component3()Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->orderUpdateFailureState:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    return-object v0
.end method

.method public final component4()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->selectedLineItems:Ljava/util/Map;

    return-object v0
.end method

.method public final component5()Lcom/squareup/ordermanagerdata/TrackingInfo;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->trackingInfo:Lcom/squareup/ordermanagerdata/TrackingInfo;

    return-object v0
.end method

.method public final component6()Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState$Companion$ShipmentAction;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->nextShipmentAction:Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState$Companion$ShipmentAction;

    return-object v0
.end method

.method public final component7()Lcom/squareup/protos/client/orders/Action;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->markShippedAction:Lcom/squareup/protos/client/orders/Action;

    return-object v0
.end method

.method public final copy(Lcom/squareup/orders/model/Order;ZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Ljava/util/Map;Lcom/squareup/ordermanagerdata/TrackingInfo;Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState$Companion$ShipmentAction;Lcom/squareup/protos/client/orders/Action;)Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/orders/model/Order;",
            "Z",
            "Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;>;",
            "Lcom/squareup/ordermanagerdata/TrackingInfo;",
            "Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState$Companion$ShipmentAction;",
            "Lcom/squareup/protos/client/orders/Action;",
            ")",
            "Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;"
        }
    .end annotation

    const-string v0, "order"

    move-object v2, p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedLineItems"

    move-object v5, p4

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nextShipmentAction"

    move-object v7, p6

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "markShippedAction"

    move-object/from16 v8, p7

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;

    move-object v1, v0

    move v3, p2

    move-object v4, p3

    move-object v6, p5

    invoke-direct/range {v1 .. v8}, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;-><init>(Lcom/squareup/orders/model/Order;ZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Ljava/util/Map;Lcom/squareup/ordermanagerdata/TrackingInfo;Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState$Companion$ShipmentAction;Lcom/squareup/protos/client/orders/Action;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->order:Lcom/squareup/orders/model/Order;

    iget-object v1, p1, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->order:Lcom/squareup/orders/model/Order;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->canShowErrorDialog:Z

    iget-boolean v1, p1, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->canShowErrorDialog:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->orderUpdateFailureState:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    iget-object v1, p1, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->orderUpdateFailureState:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->selectedLineItems:Ljava/util/Map;

    iget-object v1, p1, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->selectedLineItems:Ljava/util/Map;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->trackingInfo:Lcom/squareup/ordermanagerdata/TrackingInfo;

    iget-object v1, p1, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->trackingInfo:Lcom/squareup/ordermanagerdata/TrackingInfo;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->nextShipmentAction:Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState$Companion$ShipmentAction;

    iget-object v1, p1, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->nextShipmentAction:Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState$Companion$ShipmentAction;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->markShippedAction:Lcom/squareup/protos/client/orders/Action;

    iget-object p1, p1, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->markShippedAction:Lcom/squareup/protos/client/orders/Action;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCanShowErrorDialog()Z
    .locals 1

    .line 12
    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->canShowErrorDialog:Z

    return v0
.end method

.method public final getMarkShippedAction()Lcom/squareup/protos/client/orders/Action;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->markShippedAction:Lcom/squareup/protos/client/orders/Action;

    return-object v0
.end method

.method public final getNextShipmentAction()Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState$Companion$ShipmentAction;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->nextShipmentAction:Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState$Companion$ShipmentAction;

    return-object v0
.end method

.method public final getOrder()Lcom/squareup/orders/model/Order;
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->order:Lcom/squareup/orders/model/Order;

    return-object v0
.end method

.method public final getOrderUpdateFailureState()Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;
    .locals 1

    .line 13
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->orderUpdateFailureState:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    return-object v0
.end method

.method public final getSelectedLineItems()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;>;"
        }
    .end annotation

    .line 14
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->selectedLineItems:Ljava/util/Map;

    return-object v0
.end method

.method public final getTrackingInfo()Lcom/squareup/ordermanagerdata/TrackingInfo;
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->trackingInfo:Lcom/squareup/ordermanagerdata/TrackingInfo;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->order:Lcom/squareup/orders/model/Order;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->canShowErrorDialog:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->orderUpdateFailureState:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->selectedLineItems:Ljava/util/Map;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->trackingInfo:Lcom/squareup/ordermanagerdata/TrackingInfo;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_4
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->nextShipmentAction:Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState$Companion$ShipmentAction;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_5
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->markShippedAction:Lcom/squareup/protos/client/orders/Action;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_6
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OrderMarkShippedState(order="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->order:Lcom/squareup/orders/model/Order;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", canShowErrorDialog="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->canShowErrorDialog:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", orderUpdateFailureState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->orderUpdateFailureState:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", selectedLineItems="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->selectedLineItems:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", trackingInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->trackingInfo:Lcom/squareup/ordermanagerdata/TrackingInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", nextShipmentAction="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->nextShipmentAction:Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState$Companion$ShipmentAction;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", markShippedAction="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->markShippedAction:Lcom/squareup/protos/client/orders/Action;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
