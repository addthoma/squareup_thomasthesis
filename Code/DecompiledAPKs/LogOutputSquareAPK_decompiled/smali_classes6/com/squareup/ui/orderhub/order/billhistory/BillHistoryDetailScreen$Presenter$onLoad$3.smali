.class final Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter$onLoad$3;
.super Lkotlin/jvm/internal/Lambda;
.source "BillHistoryDetailScreen.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;->onLoad(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lrx/Subscription;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "Lrx/Subscription;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter$onLoad$3;->this$0:Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 88
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter$onLoad$3;->invoke()Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public final invoke()Lrx/Subscription;
    .locals 2

    .line 130
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter$onLoad$3;->this$0:Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;

    invoke-static {v0}, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;->access$getView(Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;)Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailView;->onRefundButtonClicked$orderhub_applet_release()Lrx/Observable;

    move-result-object v0

    .line 131
    new-instance v1, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter$onLoad$3$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter$onLoad$3$1;-><init>(Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter$onLoad$3;)V

    check-cast v1, Lrx/functions/Action1;

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    const-string v1, "view.onRefundButtonClick\u2026showIssueRefundScreen() }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
