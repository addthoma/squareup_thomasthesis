.class public final Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonInputKt;
.super Ljava/lang/Object;
.source "OrderCancellationReasonInput.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\"\u0018\u0010\u0000\u001a\u00020\u0001*\u00020\u00028@X\u0080\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "initialState",
        "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationSelectReasonState;",
        "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonInput;",
        "getInitialState",
        "(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonInput;)Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationSelectReasonState;",
        "orderhub-applet_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final getInitialState(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonInput;)Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationSelectReasonState;
    .locals 2

    const-string v0, "$this$initialState"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    new-instance v0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationSelectReasonState;

    .line 14
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonInput;->getCancelAction()Lcom/squareup/protos/client/orders/Action;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/ui/orderhub/util/proto/ActionsKt;->getCanApplyToLineItems(Lcom/squareup/protos/client/orders/Action;)Z

    move-result p0

    const/4 v1, 0x0

    .line 12
    invoke-direct {v0, v1, p0}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationSelectReasonState;-><init>(ZZ)V

    return-object v0
.end method
