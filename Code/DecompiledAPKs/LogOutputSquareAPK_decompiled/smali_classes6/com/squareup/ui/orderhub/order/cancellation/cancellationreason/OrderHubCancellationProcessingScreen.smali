.class public final Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen;
.super Ljava/lang/Object;
.source "OrderHubCancellationProcessingScreen.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/V2Screen;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen$Event;,
        Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u0000 \u00182\u00020\u0001:\u0002\u0018\u0019B!\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005\u00a2\u0006\u0002\u0010\u0008J\t\u0010\r\u001a\u00020\u0003H\u00c6\u0003J\u0015\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005H\u00c6\u0003J)\u0010\u000f\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0014\u0008\u0002\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005H\u00c6\u0001J\u0013\u0010\u0010\u001a\u00020\u00112\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u00d6\u0003J\t\u0010\u0014\u001a\u00020\u0015H\u00d6\u0001J\t\u0010\u0016\u001a\u00020\u0017H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u001d\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000c\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen;",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        "data",
        "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/CancellationProcessingScreenData;",
        "onEvent",
        "Lkotlin/Function1;",
        "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen$Event;",
        "",
        "(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/CancellationProcessingScreenData;Lkotlin/jvm/functions/Function1;)V",
        "getData",
        "()Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/CancellationProcessingScreenData;",
        "getOnEvent",
        "()Lkotlin/jvm/functions/Function1;",
        "component1",
        "component2",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "Companion",
        "Event",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen$Companion;

.field private static final KEY:Lcom/squareup/workflow/legacy/Screen$Key;


# instance fields
.field private final data:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/CancellationProcessingScreenData;

.field private final onEvent:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen$Event;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen;->Companion:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen$Companion;

    .line 18
    const-class v0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    const/4 v2, 0x1

    invoke-static {v0, v1, v2, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey$default(Lkotlin/reflect/KClass;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/CancellationProcessingScreenData;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/CancellationProcessingScreenData;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen$Event;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "data"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onEvent"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen;->data:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/CancellationProcessingScreenData;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen;->onEvent:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public static final synthetic access$getKEY$cp()Lcom/squareup/workflow/legacy/Screen$Key;
    .locals 1

    .line 6
    sget-object v0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-object v0
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen;Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/CancellationProcessingScreenData;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen;->data:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/CancellationProcessingScreenData;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen;->onEvent:Lkotlin/jvm/functions/Function1;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen;->copy(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/CancellationProcessingScreenData;Lkotlin/jvm/functions/Function1;)Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/CancellationProcessingScreenData;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen;->data:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/CancellationProcessingScreenData;

    return-object v0
.end method

.method public final component2()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen$Event;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen;->onEvent:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final copy(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/CancellationProcessingScreenData;Lkotlin/jvm/functions/Function1;)Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/CancellationProcessingScreenData;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen$Event;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen;"
        }
    .end annotation

    const-string v0, "data"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onEvent"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen;

    invoke-direct {v0, p1, p2}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen;-><init>(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/CancellationProcessingScreenData;Lkotlin/jvm/functions/Function1;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen;->data:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/CancellationProcessingScreenData;

    iget-object v1, p1, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen;->data:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/CancellationProcessingScreenData;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen;->onEvent:Lkotlin/jvm/functions/Function1;

    iget-object p1, p1, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen;->onEvent:Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getData()Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/CancellationProcessingScreenData;
    .locals 1

    .line 7
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen;->data:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/CancellationProcessingScreenData;

    return-object v0
.end method

.method public final getOnEvent()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen$Event;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 8
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen;->onEvent:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen;->data:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/CancellationProcessingScreenData;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen;->onEvent:Lkotlin/jvm/functions/Function1;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OrderHubCancellationProcessingScreen(data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen;->data:Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/CancellationProcessingScreenData;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onEvent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderHubCancellationProcessingScreen;->onEvent:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
