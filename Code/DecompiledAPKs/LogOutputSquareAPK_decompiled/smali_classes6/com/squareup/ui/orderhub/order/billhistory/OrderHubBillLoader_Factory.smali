.class public final Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader_Factory;
.super Ljava/lang/Object;
.source "OrderHubBillLoader_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;",
        ">;"
    }
.end annotation


# instance fields
.field private final billServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/bills/BillListServiceHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final orderHubBillHistoryCreatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryCreator;",
            ">;"
        }
    .end annotation
.end field

.field private final userTokenProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/bills/BillListServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryCreator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader_Factory;->billServiceProvider:Ljavax/inject/Provider;

    .line 27
    iput-object p2, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader_Factory;->orderHubBillHistoryCreatorProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p3, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader_Factory;->userTokenProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/bills/BillListServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryCreator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader_Factory;"
        }
    .end annotation

    .line 40
    new-instance v0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/server/bills/BillListServiceHelper;Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryCreator;Ljava/lang/String;)Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;
    .locals 1

    .line 45
    new-instance v0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;-><init>(Lcom/squareup/server/bills/BillListServiceHelper;Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryCreator;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;
    .locals 3

    .line 33
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader_Factory;->billServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/bills/BillListServiceHelper;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader_Factory;->orderHubBillHistoryCreatorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryCreator;

    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader_Factory;->userTokenProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader_Factory;->newInstance(Lcom/squareup/server/bills/BillListServiceHelper;Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryCreator;Ljava/lang/String;)Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader_Factory;->get()Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;

    move-result-object v0

    return-object v0
.end method
