.class public final synthetic Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 3

    invoke-static {}, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;->values()[Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;->NOT_SEARCHING:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;->SHOW_INFO_TEXT:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;->TYPING:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;->LOADING:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;->ERROR:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;->SHOW_RESULTS:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;->NO_SEARCH_RESULTS:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1

    return-void
.end method
