.class final Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService$issueInventoryAdjustmentRequest$2;
.super Ljava/lang/Object;
.source "OrderHubInventoryService.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;->issueInventoryAdjustmentRequest(Lcom/squareup/orders/model/Order;Lcom/squareup/billhistory/model/BillHistory;Ljava/util/Map;ZLjava/lang/String;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/SingleSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderHubInventoryService.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderHubInventoryService.kt\ncom/squareup/ui/orderhub/inventory/OrderHubInventoryService$issueInventoryAdjustmentRequest$2\n+ 2 Maps.kt\nkotlin/collections/MapsKt__MapsKt\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,231:1\n428#2:232\n378#2:233\n1143#3,4:234\n*E\n*S KotlinDebug\n*F\n+ 1 OrderHubInventoryService.kt\ncom/squareup/ui/orderhub/inventory/OrderHubInventoryService$issueInventoryAdjustmentRequest$2\n*L\n80#1:232\n80#1:233\n80#1,4:234\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010%\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0000\u0010\u0000\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00030\u00020\u00012D\u0010\u0004\u001a@\u0012\u000c\u0012\n \u0007*\u0004\u0018\u00010\u00060\u0006\u0012\u000c\u0012\n \u0007*\u0004\u0018\u00010\u00080\u0008 \u0007*\u001e\u0012\u000c\u0012\n \u0007*\u0004\u0018\u00010\u00060\u0006\u0012\u000c\u0012\n \u0007*\u0004\u0018\u00010\u00080\u00080\t0\u0005H\n\u00a2\u0006\u0002\u0008\n"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/BatchAdjustVariationInventoryResponse;",
        "catalogVariationsById",
        "",
        "",
        "kotlin.jvm.PlatformType",
        "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
        "",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $idempotencyKey:Ljava/lang/String;

.field final synthetic $isRestock:Z

.field final synthetic $order:Lcom/squareup/orders/model/Order;

.field final synthetic $returnBill:Lcom/squareup/billhistory/model/BillHistory;

.field final synthetic $selectedItemQuantities:Ljava/util/Map;

.field final synthetic this$0:Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;Lcom/squareup/orders/model/Order;Lcom/squareup/billhistory/model/BillHistory;Ljava/util/Map;ZLjava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService$issueInventoryAdjustmentRequest$2;->this$0:Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService$issueInventoryAdjustmentRequest$2;->$order:Lcom/squareup/orders/model/Order;

    iput-object p3, p0, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService$issueInventoryAdjustmentRequest$2;->$returnBill:Lcom/squareup/billhistory/model/BillHistory;

    iput-object p4, p0, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService$issueInventoryAdjustmentRequest$2;->$selectedItemQuantities:Ljava/util/Map;

    iput-boolean p5, p0, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService$issueInventoryAdjustmentRequest$2;->$isRestock:Z

    iput-object p6, p0, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService$issueInventoryAdjustmentRequest$2;->$idempotencyKey:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/util/Map;)Lio/reactivex/Single;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
            ">;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/BatchAdjustVariationInventoryResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "catalogVariationsById"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 232
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-static {v1}, Lkotlin/collections/MapsKt;->mapCapacity(I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/LinkedHashMap;-><init>(I)V

    move-object v6, v0

    check-cast v6, Ljava/util/Map;

    .line 233
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 234
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 235
    check-cast v0, Ljava/util/Map$Entry;

    .line 233
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    .line 80
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    const-string v2, "it.value"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->isInventoryTracked()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {v6, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 81
    :cond_0
    iget-object v2, p0, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService$issueInventoryAdjustmentRequest$2;->this$0:Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;

    .line 82
    iget-object v3, p0, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService$issueInventoryAdjustmentRequest$2;->$order:Lcom/squareup/orders/model/Order;

    .line 83
    iget-object v4, p0, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService$issueInventoryAdjustmentRequest$2;->$returnBill:Lcom/squareup/billhistory/model/BillHistory;

    .line 84
    iget-object v5, p0, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService$issueInventoryAdjustmentRequest$2;->$selectedItemQuantities:Ljava/util/Map;

    .line 86
    iget-boolean v7, p0, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService$issueInventoryAdjustmentRequest$2;->$isRestock:Z

    .line 87
    iget-object v8, p0, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService$issueInventoryAdjustmentRequest$2;->$idempotencyKey:Ljava/lang/String;

    .line 81
    invoke-static/range {v2 .. v8}, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;->access$issueInventoryAdjustmentRequest(Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;Lcom/squareup/orders/model/Order;Lcom/squareup/billhistory/model/BillHistory;Ljava/util/Map;Ljava/util/Map;ZLjava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 35
    check-cast p1, Ljava/util/Map;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService$issueInventoryAdjustmentRequest$2;->apply(Ljava/util/Map;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
