.class final Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$render$2;
.super Lkotlin/jvm/internal/Lambda;
.source "RealOrderMarkCanceledWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow;->render(Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
        "+",
        "Lcom/squareup/protos/client/BatchAdjustVariationInventoryResponse;",
        ">;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;",
        "+",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult;",
        ">;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealOrderMarkCanceledWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealOrderMarkCanceledWorkflow.kt\ncom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$render$2\n*L\n1#1,390:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult;",
        "result",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/BatchAdjustVariationInventoryResponse;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$render$2;->$state:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/workflow/WorkflowAction;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/BatchAdjustVariationInventoryResponse;",
            ">;)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;",
            "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult;",
            ">;"
        }
    .end annotation

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 167
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    .line 168
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    new-instance v0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult$MarkCanceledSkipRefundComplete;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$render$2;->$state:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult$MarkCanceledSkipRefundComplete;-><init>(Lcom/squareup/orders/model/Order;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 169
    :cond_0
    instance-of p1, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz p1, :cond_2

    .line 170
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 172
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$render$2;->$state:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v0

    .line 174
    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$render$2;->$state:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->getCancelAction()Lcom/squareup/protos/client/orders/Action;

    move-result-object v3

    .line 175
    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$render$2;->$state:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->getBillHistory()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v4

    .line 176
    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$render$2;->$state:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;

    invoke-static {v1}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledStateKt;->getNonEmptyCancellationReason(Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;)Lcom/squareup/ordermanagerdata/CancellationReason;

    move-result-object v5

    .line 177
    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$render$2;->$state:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;

    invoke-static {v1}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledStateKt;->getNonEmptySelectedLineItems(Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;)Ljava/util/Map;

    move-result-object v6

    .line 178
    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$render$2;->$state:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->getIdempotencyKey()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_1

    .line 173
    new-instance v1, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$InventoryAdjustmentRetry;

    move-object v2, v1

    invoke-direct/range {v2 .. v7}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$InventoryAdjustmentRetry;-><init>(Lcom/squareup/protos/client/orders/Action;Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/ordermanagerdata/CancellationReason;Ljava/util/Map;Ljava/lang/String;)V

    .line 180
    new-instance v2, Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    .line 181
    sget-object v3, Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure$InventoryAdjustmentFailedError;->INSTANCE:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure$InventoryAdjustmentFailedError;

    check-cast v3, Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure;

    .line 182
    sget-object v4, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event$RetryInventoryAdjustment;->INSTANCE:Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event$RetryInventoryAdjustment;

    check-cast v4, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;

    .line 183
    sget-object v5, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event$CancelInventoryAdjustment;->INSTANCE:Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event$CancelInventoryAdjustment;

    check-cast v5, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;

    .line 180
    invoke-direct {v2, v3, v4, v5}, Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;-><init>(Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure;Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;)V

    .line 171
    new-instance v3, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult$MarkCanceledInventoryAdjustmentFailed;

    invoke-direct {v3, v0, v1, v2}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult$MarkCanceledInventoryAdjustmentFailed;-><init>(Lcom/squareup/orders/model/Order;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$InventoryAdjustmentRetry;Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;)V

    .line 170
    invoke-virtual {p1, v3}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    .line 178
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Required value was null."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 170
    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 83
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$render$2;->invoke(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
