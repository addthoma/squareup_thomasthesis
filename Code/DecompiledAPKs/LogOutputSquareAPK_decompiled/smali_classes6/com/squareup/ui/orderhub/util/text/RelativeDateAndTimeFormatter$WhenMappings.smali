.class public final synthetic Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I

.field public static final synthetic $EnumSwitchMapping$1:[I

.field public static final synthetic $EnumSwitchMapping$2:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 6

    invoke-static {}, Lcom/squareup/orders/model/Order$State;->values()[Lcom/squareup/orders/model/Order$State;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/orders/model/Order$State;->OPEN:Lcom/squareup/orders/model/Order$State;

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$State;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/orders/model/Order$State;->COMPLETED:Lcom/squareup/orders/model/Order$State;

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$State;->ordinal()I

    move-result v1

    const/4 v3, 0x2

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/orders/model/Order$State;->CANCELED:Lcom/squareup/orders/model/Order$State;

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$State;->ordinal()I

    move-result v1

    const/4 v4, 0x3

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/orders/model/Order$State;->DO_NOT_USE:Lcom/squareup/orders/model/Order$State;

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$State;->ordinal()I

    move-result v1

    const/4 v5, 0x4

    aput v5, v0, v1

    invoke-static {}, Lorg/threeten/bp/temporal/ChronoUnit;->values()[Lorg/threeten/bp/temporal/ChronoUnit;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v0, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lorg/threeten/bp/temporal/ChronoUnit;->WEEKS:Lorg/threeten/bp/temporal/ChronoUnit;

    invoke-virtual {v1}, Lorg/threeten/bp/temporal/ChronoUnit;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lorg/threeten/bp/temporal/ChronoUnit;->DAYS:Lorg/threeten/bp/temporal/ChronoUnit;

    invoke-virtual {v1}, Lorg/threeten/bp/temporal/ChronoUnit;->ordinal()I

    move-result v1

    aput v3, v0, v1

    invoke-static {}, Lorg/threeten/bp/temporal/ChronoUnit;->values()[Lorg/threeten/bp/temporal/ChronoUnit;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v0, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lorg/threeten/bp/temporal/ChronoUnit;->YEARS:Lorg/threeten/bp/temporal/ChronoUnit;

    invoke-virtual {v1}, Lorg/threeten/bp/temporal/ChronoUnit;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lorg/threeten/bp/temporal/ChronoUnit;->WEEKS:Lorg/threeten/bp/temporal/ChronoUnit;

    invoke-virtual {v1}, Lorg/threeten/bp/temporal/ChronoUnit;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lorg/threeten/bp/temporal/ChronoUnit;->DAYS:Lorg/threeten/bp/temporal/ChronoUnit;

    invoke-virtual {v1}, Lorg/threeten/bp/temporal/ChronoUnit;->ordinal()I

    move-result v1

    aput v4, v0, v1

    return-void
.end method
