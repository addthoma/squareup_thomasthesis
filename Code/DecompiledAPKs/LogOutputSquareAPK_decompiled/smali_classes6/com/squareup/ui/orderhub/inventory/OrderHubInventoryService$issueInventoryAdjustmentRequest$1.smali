.class final Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService$issueInventoryAdjustmentRequest$1;
.super Ljava/lang/Object;
.source "OrderHubInventoryService.kt"

# interfaces
.implements Lcom/squareup/shared/catalog/CatalogTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;->issueInventoryAdjustmentRequest(Lcom/squareup/orders/model/Order;Lcom/squareup/billhistory/model/BillHistory;Ljava/util/Map;ZLjava/lang/String;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/shared/catalog/CatalogTask<",
        "Ljava/util/Map<",
        "Ljava/lang/String;",
        "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0010%\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001aB\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00040\u0004 \u0003* \u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00040\u0004\u0018\u00010\u00050\u00012\u000e\u0010\u0006\u001a\n \u0003*\u0004\u0018\u00010\u00070\u0007H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "",
        "kotlin.jvm.PlatformType",
        "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
        "",
        "it",
        "Lcom/squareup/shared/catalog/Catalog$Local;",
        "perform"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $order:Lcom/squareup/orders/model/Order;

.field final synthetic this$0:Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;Lcom/squareup/orders/model/Order;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService$issueInventoryAdjustmentRequest$1;->this$0:Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService$issueInventoryAdjustmentRequest$1;->$order:Lcom/squareup/orders/model/Order;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic perform(Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/lang/Object;
    .locals 0

    .line 35
    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService$issueInventoryAdjustmentRequest$1;->perform(Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public final perform(Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/Catalog$Local;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
            ">;"
        }
    .end annotation

    .line 74
    const-class v0, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService$issueInventoryAdjustmentRequest$1;->this$0:Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;

    iget-object v2, p0, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService$issueInventoryAdjustmentRequest$1;->$order:Lcom/squareup/orders/model/Order;

    invoke-static {v1, v2}, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;->access$catalogVariationIds(Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;Lcom/squareup/orders/model/Order;)Ljava/util/Set;

    move-result-object v1

    .line 73
    invoke-interface {p1, v0, v1}, Lcom/squareup/shared/catalog/Catalog$Local;->findObjectsByTokens(Ljava/lang/Class;Ljava/util/Set;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method
