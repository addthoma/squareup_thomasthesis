.class public final Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;
.super Ljava/lang/Object;
.source "OrderItemSelectionInput.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001BC\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012,\u0008\u0002\u0010\u0006\u001a&\u0012\u0004\u0012\u00020\u0008\u0012\u0018\u0012\u0016\u0012\u0008\u0012\u00060\u0008j\u0002`\t\u0012\u0004\u0012\u00020\n0\u0007j\u0002`\u000b0\u0007j\u0002`\u000c\u00a2\u0006\u0002\u0010\rJ\t\u0010\u0014\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0005H\u00c6\u0003J-\u0010\u0016\u001a&\u0012\u0004\u0012\u00020\u0008\u0012\u0018\u0012\u0016\u0012\u0008\u0012\u00060\u0008j\u0002`\t\u0012\u0004\u0012\u00020\n0\u0007j\u0002`\u000b0\u0007j\u0002`\u000cH\u00c6\u0003JK\u0010\u0017\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052,\u0008\u0002\u0010\u0006\u001a&\u0012\u0004\u0012\u00020\u0008\u0012\u0018\u0012\u0016\u0012\u0008\u0012\u00060\u0008j\u0002`\t\u0012\u0004\u0012\u00020\n0\u0007j\u0002`\u000b0\u0007j\u0002`\u000cH\u00c6\u0001J\u0013\u0010\u0018\u001a\u00020\u00192\u0008\u0010\u001a\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u001b\u001a\u00020\u001cH\u00d6\u0001J\t\u0010\u001d\u001a\u00020\u0008H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R5\u0010\u0006\u001a&\u0012\u0004\u0012\u00020\u0008\u0012\u0018\u0012\u0016\u0012\u0008\u0012\u00060\u0008j\u0002`\t\u0012\u0004\u0012\u00020\n0\u0007j\u0002`\u000b0\u0007j\u0002`\u000c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;",
        "",
        "order",
        "Lcom/squareup/orders/model/Order;",
        "action",
        "Lcom/squareup/protos/client/orders/Action;",
        "preSelectedLineItems",
        "",
        "",
        "Lcom/squareup/ui/orderhub/order/itemselection/LineItemRowIdentifier;",
        "Lcom/squareup/orders/model/Order$LineItem;",
        "Lcom/squareup/ui/orderhub/order/itemselection/LineItemWithQuantityByIdentifier;",
        "Lcom/squareup/ui/orderhub/order/itemselection/LineItemSelectionsByUid;",
        "(Lcom/squareup/orders/model/Order;Lcom/squareup/protos/client/orders/Action;Ljava/util/Map;)V",
        "getAction",
        "()Lcom/squareup/protos/client/orders/Action;",
        "getOrder",
        "()Lcom/squareup/orders/model/Order;",
        "getPreSelectedLineItems",
        "()Ljava/util/Map;",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final action:Lcom/squareup/protos/client/orders/Action;

.field private final order:Lcom/squareup/orders/model/Order;

.field private final preSelectedLineItems:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/orders/model/Order;Lcom/squareup/protos/client/orders/Action;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/orders/model/Order;",
            "Lcom/squareup/protos/client/orders/Action;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "order"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "action"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "preSelectedLineItems"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;->order:Lcom/squareup/orders/model/Order;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;->action:Lcom/squareup/protos/client/orders/Action;

    iput-object p3, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;->preSelectedLineItems:Ljava/util/Map;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/orders/model/Order;Lcom/squareup/protos/client/orders/Action;Ljava/util/Map;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    .line 9
    invoke-static {p1}, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelectionsKt;->getAvailableLineItemsToRowIdentifiers(Lcom/squareup/orders/model/Order;)Ljava/util/Map;

    move-result-object p3

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;-><init>(Lcom/squareup/orders/model/Order;Lcom/squareup/protos/client/orders/Action;Ljava/util/Map;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;Lcom/squareup/orders/model/Order;Lcom/squareup/protos/client/orders/Action;Ljava/util/Map;ILjava/lang/Object;)Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;->order:Lcom/squareup/orders/model/Order;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;->action:Lcom/squareup/protos/client/orders/Action;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;->preSelectedLineItems:Ljava/util/Map;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;->copy(Lcom/squareup/orders/model/Order;Lcom/squareup/protos/client/orders/Action;Ljava/util/Map;)Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/orders/model/Order;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;->order:Lcom/squareup/orders/model/Order;

    return-object v0
.end method

.method public final component2()Lcom/squareup/protos/client/orders/Action;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;->action:Lcom/squareup/protos/client/orders/Action;

    return-object v0
.end method

.method public final component3()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;->preSelectedLineItems:Ljava/util/Map;

    return-object v0
.end method

.method public final copy(Lcom/squareup/orders/model/Order;Lcom/squareup/protos/client/orders/Action;Ljava/util/Map;)Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/orders/model/Order;",
            "Lcom/squareup/protos/client/orders/Action;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;>;)",
            "Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;"
        }
    .end annotation

    const-string v0, "order"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "action"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "preSelectedLineItems"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;-><init>(Lcom/squareup/orders/model/Order;Lcom/squareup/protos/client/orders/Action;Ljava/util/Map;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;->order:Lcom/squareup/orders/model/Order;

    iget-object v1, p1, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;->order:Lcom/squareup/orders/model/Order;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;->action:Lcom/squareup/protos/client/orders/Action;

    iget-object v1, p1, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;->action:Lcom/squareup/protos/client/orders/Action;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;->preSelectedLineItems:Ljava/util/Map;

    iget-object p1, p1, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;->preSelectedLineItems:Ljava/util/Map;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAction()Lcom/squareup/protos/client/orders/Action;
    .locals 1

    .line 8
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;->action:Lcom/squareup/protos/client/orders/Action;

    return-object v0
.end method

.method public final getOrder()Lcom/squareup/orders/model/Order;
    .locals 1

    .line 7
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;->order:Lcom/squareup/orders/model/Order;

    return-object v0
.end method

.method public final getPreSelectedLineItems()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;>;"
        }
    .end annotation

    .line 9
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;->preSelectedLineItems:Ljava/util/Map;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;->order:Lcom/squareup/orders/model/Order;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;->action:Lcom/squareup/protos/client/orders/Action;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;->preSelectedLineItems:Ljava/util/Map;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OrderItemSelectionInput(order="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;->order:Lcom/squareup/orders/model/Order;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", action="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;->action:Lcom/squareup/protos/client/orders/Action;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", preSelectedLineItems="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;->preSelectedLineItems:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
