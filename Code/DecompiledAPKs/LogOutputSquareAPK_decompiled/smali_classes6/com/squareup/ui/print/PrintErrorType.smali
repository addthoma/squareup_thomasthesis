.class abstract enum Lcom/squareup/ui/print/PrintErrorType;
.super Ljava/lang/Enum;
.source "PrintErrorType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/print/PrintErrorType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/print/PrintErrorType;

.field public static final enum COVER_OPEN:Lcom/squareup/ui/print/PrintErrorType;

.field public static final enum CUTTER:Lcom/squareup/ui/print/PrintErrorType;

.field public static final enum ISSUE_NETWORK:Lcom/squareup/ui/print/PrintErrorType;

.field public static final enum ISSUE_REBOOT:Lcom/squareup/ui/print/PrintErrorType;

.field public static final enum JAM:Lcom/squareup/ui/print/PrintErrorType;

.field public static final enum MECHANICAL:Lcom/squareup/ui/print/PrintErrorType;

.field public static final enum OUT_OF_PAPER:Lcom/squareup/ui/print/PrintErrorType;

.field public static final enum UNAVAILABLE:Lcom/squareup/ui/print/PrintErrorType;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .line 13
    new-instance v0, Lcom/squareup/ui/print/PrintErrorType$1;

    const/4 v1, 0x0

    const-string v2, "COVER_OPEN"

    invoke-direct {v0, v2, v1}, Lcom/squareup/ui/print/PrintErrorType$1;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/print/PrintErrorType;->COVER_OPEN:Lcom/squareup/ui/print/PrintErrorType;

    .line 22
    new-instance v0, Lcom/squareup/ui/print/PrintErrorType$2;

    const/4 v2, 0x1

    const-string v3, "CUTTER"

    invoke-direct {v0, v3, v2}, Lcom/squareup/ui/print/PrintErrorType$2;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/print/PrintErrorType;->CUTTER:Lcom/squareup/ui/print/PrintErrorType;

    .line 31
    new-instance v0, Lcom/squareup/ui/print/PrintErrorType$3;

    const/4 v3, 0x2

    const-string v4, "JAM"

    invoke-direct {v0, v4, v3}, Lcom/squareup/ui/print/PrintErrorType$3;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/print/PrintErrorType;->JAM:Lcom/squareup/ui/print/PrintErrorType;

    .line 40
    new-instance v0, Lcom/squareup/ui/print/PrintErrorType$4;

    const/4 v4, 0x3

    const-string v5, "MECHANICAL"

    invoke-direct {v0, v5, v4}, Lcom/squareup/ui/print/PrintErrorType$4;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/print/PrintErrorType;->MECHANICAL:Lcom/squareup/ui/print/PrintErrorType;

    .line 49
    new-instance v0, Lcom/squareup/ui/print/PrintErrorType$5;

    const/4 v5, 0x4

    const-string v6, "OUT_OF_PAPER"

    invoke-direct {v0, v6, v5}, Lcom/squareup/ui/print/PrintErrorType$5;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/print/PrintErrorType;->OUT_OF_PAPER:Lcom/squareup/ui/print/PrintErrorType;

    .line 58
    new-instance v0, Lcom/squareup/ui/print/PrintErrorType$6;

    const/4 v6, 0x5

    const-string v7, "ISSUE_REBOOT"

    invoke-direct {v0, v7, v6}, Lcom/squareup/ui/print/PrintErrorType$6;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/print/PrintErrorType;->ISSUE_REBOOT:Lcom/squareup/ui/print/PrintErrorType;

    .line 67
    new-instance v0, Lcom/squareup/ui/print/PrintErrorType$7;

    const/4 v7, 0x6

    const-string v8, "ISSUE_NETWORK"

    invoke-direct {v0, v8, v7}, Lcom/squareup/ui/print/PrintErrorType$7;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/print/PrintErrorType;->ISSUE_NETWORK:Lcom/squareup/ui/print/PrintErrorType;

    .line 76
    new-instance v0, Lcom/squareup/ui/print/PrintErrorType$8;

    const/4 v8, 0x7

    const-string v9, "UNAVAILABLE"

    invoke-direct {v0, v9, v8}, Lcom/squareup/ui/print/PrintErrorType$8;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/print/PrintErrorType;->UNAVAILABLE:Lcom/squareup/ui/print/PrintErrorType;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/squareup/ui/print/PrintErrorType;

    .line 11
    sget-object v9, Lcom/squareup/ui/print/PrintErrorType;->COVER_OPEN:Lcom/squareup/ui/print/PrintErrorType;

    aput-object v9, v0, v1

    sget-object v1, Lcom/squareup/ui/print/PrintErrorType;->CUTTER:Lcom/squareup/ui/print/PrintErrorType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/print/PrintErrorType;->JAM:Lcom/squareup/ui/print/PrintErrorType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/ui/print/PrintErrorType;->MECHANICAL:Lcom/squareup/ui/print/PrintErrorType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/ui/print/PrintErrorType;->OUT_OF_PAPER:Lcom/squareup/ui/print/PrintErrorType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/ui/print/PrintErrorType;->ISSUE_REBOOT:Lcom/squareup/ui/print/PrintErrorType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/ui/print/PrintErrorType;->ISSUE_NETWORK:Lcom/squareup/ui/print/PrintErrorType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/ui/print/PrintErrorType;->UNAVAILABLE:Lcom/squareup/ui/print/PrintErrorType;

    aput-object v1, v0, v8

    sput-object v0, Lcom/squareup/ui/print/PrintErrorType;->$VALUES:[Lcom/squareup/ui/print/PrintErrorType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 11
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/squareup/ui/print/PrintErrorType$1;)V
    .locals 0

    .line 11
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/print/PrintErrorType;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method static fromResult(Lcom/squareup/print/PrintJob$PrintAttempt$Result;)Lcom/squareup/ui/print/PrintErrorType;
    .locals 2

    .line 124
    sget-object v0, Lcom/squareup/ui/print/PrintErrorType$9;->$SwitchMap$com$squareup$print$PrintJob$PrintAttempt$Result:[I

    invoke-virtual {p0}, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 159
    sget-object p0, Lcom/squareup/ui/print/PrintErrorType;->ISSUE_NETWORK:Lcom/squareup/ui/print/PrintErrorType;

    return-object p0

    .line 155
    :pswitch_0
    sget-object p0, Lcom/squareup/ui/print/PrintErrorType;->JAM:Lcom/squareup/ui/print/PrintErrorType;

    return-object p0

    .line 152
    :pswitch_1
    sget-object p0, Lcom/squareup/ui/print/PrintErrorType;->MECHANICAL:Lcom/squareup/ui/print/PrintErrorType;

    return-object p0

    .line 149
    :pswitch_2
    sget-object p0, Lcom/squareup/ui/print/PrintErrorType;->CUTTER:Lcom/squareup/ui/print/PrintErrorType;

    return-object p0

    .line 146
    :pswitch_3
    sget-object p0, Lcom/squareup/ui/print/PrintErrorType;->OUT_OF_PAPER:Lcom/squareup/ui/print/PrintErrorType;

    return-object p0

    .line 143
    :pswitch_4
    sget-object p0, Lcom/squareup/ui/print/PrintErrorType;->COVER_OPEN:Lcom/squareup/ui/print/PrintErrorType;

    return-object p0

    .line 140
    :pswitch_5
    sget-object p0, Lcom/squareup/ui/print/PrintErrorType;->ISSUE_REBOOT:Lcom/squareup/ui/print/PrintErrorType;

    return-object p0

    .line 135
    :pswitch_6
    sget-object p0, Lcom/squareup/ui/print/PrintErrorType;->UNAVAILABLE:Lcom/squareup/ui/print/PrintErrorType;

    return-object p0

    .line 131
    :pswitch_7
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->name()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, " should be a silent error!"

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 127
    :pswitch_8
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->name()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, " is not an error type result!"

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_8
        :pswitch_8
        :pswitch_7
        :pswitch_7
        :pswitch_6
        :pswitch_6
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method static isInternalPrinter(Lcom/squareup/print/HardwarePrinter;)Ljava/lang/Boolean;
    .locals 2

    if-eqz p0, :cond_1

    .line 115
    invoke-virtual {p0}, Lcom/squareup/print/HardwarePrinter;->getHardwareInfo()Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->connectionType:Lcom/squareup/print/ConnectionType;

    sget-object v1, Lcom/squareup/print/ConnectionType;->INTERNAL:Lcom/squareup/print/ConnectionType;

    if-eq v0, v1, :cond_0

    .line 116
    invoke-virtual {p0}, Lcom/squareup/print/HardwarePrinter;->getHardwareInfo()Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->connectionType:Lcom/squareup/print/ConnectionType;

    sget-object v0, Lcom/squareup/print/ConnectionType;->FAKE_INTERNAL:Lcom/squareup/print/ConnectionType;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 p0, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    .line 114
    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static shouldDisplayError(Lkotlin/Pair;Lcom/squareup/print/PrintJob$PrintAttempt$Result;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "Lcom/squareup/print/PrintTargetRouter$RouteResult;",
            "Lcom/squareup/print/HardwarePrinter;",
            ">;",
            "Lcom/squareup/print/PrintJob$PrintAttempt$Result;",
            ")Z"
        }
    .end annotation

    .line 104
    invoke-virtual {p0}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/print/PrintTargetRouter$RouteResult;

    invoke-static {v0}, Lcom/squareup/ui/print/PrintErrorType;->shouldDisplayToMerchant(Lcom/squareup/print/PrintTargetRouter$RouteResult;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    invoke-static {p1}, Lcom/squareup/ui/print/PrintErrorType;->shouldDisplayToMerchant(Lcom/squareup/print/PrintJob$PrintAttempt$Result;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 106
    invoke-virtual {p0}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/print/HardwarePrinter;

    invoke-static {p0}, Lcom/squareup/ui/print/PrintErrorType;->isInternalPrinter(Lcom/squareup/print/HardwarePrinter;)Ljava/lang/Boolean;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private static shouldDisplayToMerchant(Lcom/squareup/print/PrintJob$PrintAttempt$Result;)Z
    .locals 3

    .line 180
    sget-object v0, Lcom/squareup/ui/print/PrintErrorType$9;->$SwitchMap$com$squareup$print$PrintJob$PrintAttempt$Result:[I

    invoke-virtual {p0}, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 202
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t understand Result "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const/4 p0, 0x1

    return p0

    :pswitch_1
    const/4 p0, 0x0

    return p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private static shouldDisplayToMerchant(Lcom/squareup/print/PrintTargetRouter$RouteResult;)Z
    .locals 3

    .line 164
    sget-object v0, Lcom/squareup/ui/print/PrintErrorType$9;->$SwitchMap$com$squareup$print$PrintTargetRouter$RouteResult:[I

    invoke-virtual {p0}, Lcom/squareup/print/PrintTargetRouter$RouteResult;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v2, 0x2

    if-eq v0, v2, :cond_2

    const/4 v2, 0x3

    if-eq v0, v2, :cond_1

    const/4 v2, 0x4

    if-ne v0, v2, :cond_0

    goto :goto_0

    .line 175
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t understand RouteResult "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    return v1

    :cond_2
    const/4 p0, 0x0

    return p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/print/PrintErrorType;
    .locals 1

    .line 11
    const-class v0, Lcom/squareup/ui/print/PrintErrorType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/print/PrintErrorType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/print/PrintErrorType;
    .locals 1

    .line 11
    sget-object v0, Lcom/squareup/ui/print/PrintErrorType;->$VALUES:[Lcom/squareup/ui/print/PrintErrorType;

    invoke-virtual {v0}, [Lcom/squareup/ui/print/PrintErrorType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/print/PrintErrorType;

    return-object v0
.end method


# virtual methods
.method canShowSpinner()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method abstract getMessage()I
.end method

.method abstract getTitle()I
.end method
