.class public Lcom/squareup/ui/print/PrintErrorPopupView;
.super Lcom/squareup/ui/DialogCardView;
.source "PrintErrorPopupView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/print/PrintErrorPopupView$PrinterErrorHolder;,
        Lcom/squareup/ui/print/PrintErrorPopupView$FooterRowHolder;,
        Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;,
        Lcom/squareup/ui/print/PrintErrorPopupView$PrintErrorAdapter;,
        Lcom/squareup/ui/print/PrintErrorPopupView$ParentComponent;
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field device:Lcom/squareup/util/Device;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private discardPrintJobsPopup:Lcom/squareup/flowlegacy/ConfirmationPopup;

.field private errorAdapter:Landroidx/recyclerview/widget/RecyclerView$Adapter;

.field presenter:Lcom/squareup/ui/print/PrintErrorPopupPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private recyclerView:Landroidx/recyclerview/widget/RecyclerView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 45
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/DialogCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 46
    const-class p2, Lcom/squareup/ui/print/PrintErrorPopupView$ParentComponent;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/print/PrintErrorPopupView$ParentComponent;

    invoke-interface {p1, p0}, Lcom/squareup/ui/print/PrintErrorPopupView$ParentComponent;->inject(Lcom/squareup/ui/print/PrintErrorPopupView;)V

    .line 48
    new-instance p1, Lcom/squareup/flowlegacy/ConfirmationPopup;

    invoke-virtual {p0}, Lcom/squareup/ui/print/PrintErrorPopupView;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/squareup/flowlegacy/ConfirmationPopup;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/squareup/ui/print/PrintErrorPopupView;->discardPrintJobsPopup:Lcom/squareup/flowlegacy/ConfirmationPopup;

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 283
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 284
    sget v0, Lcom/squareup/print/popup/error/impl/R$id;->print_error_recycler:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    return-void
.end method


# virtual methods
.method public synthetic lambda$onFinishInflate$0$PrintErrorPopupView()V
    .locals 1

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupView;->presenter:Lcom/squareup/ui/print/PrintErrorPopupPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->onCloseButtonClicked()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .line 83
    invoke-super {p0}, Lcom/squareup/ui/DialogCardView;->onDetachedFromWindow()V

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    .line 88
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupView;->presenter:Lcom/squareup/ui/print/PrintErrorPopupPresenter;

    iget-object v0, v0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->discardPrintJobsPresenter:Lcom/squareup/mortar/PopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/print/PrintErrorPopupView;->discardPrintJobsPopup:Lcom/squareup/flowlegacy/ConfirmationPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/mortar/PopupPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 4

    .line 52
    invoke-super {p0}, Lcom/squareup/ui/DialogCardView;->onFinishInflate()V

    .line 53
    invoke-direct {p0}, Lcom/squareup/ui/print/PrintErrorPopupView;->bindViews()V

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 57
    invoke-virtual {p0}, Lcom/squareup/ui/print/PrintErrorPopupView;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/squareup/print/popup/error/impl/R$string;->kitchen_printing_printer_errors:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    .line 56
    invoke-virtual {v0, v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/print/-$$Lambda$PrintErrorPopupView$_1iYp-hEcNWGiNiiwdCj66nixYs;

    invoke-direct {v1, p0}, Lcom/squareup/ui/print/-$$Lambda$PrintErrorPopupView$_1iYp-hEcNWGiNiiwdCj66nixYs;-><init>(Lcom/squareup/ui/print/PrintErrorPopupView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    new-instance v1, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lcom/squareup/ui/print/PrintErrorPopupView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setHasFixedSize(Z)V

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    new-instance v1, Landroidx/recyclerview/widget/DefaultItemAnimator;

    invoke-direct {v1}, Landroidx/recyclerview/widget/DefaultItemAnimator;-><init>()V

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setItemAnimator(Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;)V

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupView;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    invoke-virtual {p0}, Lcom/squareup/ui/print/PrintErrorPopupView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/marin/R$dimen;->marin_gap_large:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 68
    new-instance v1, Lcom/squareup/ui/DividerDecoration;

    invoke-direct {v1, v0}, Lcom/squareup/ui/DividerDecoration;-><init>(I)V

    goto :goto_0

    .line 71
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/print/PrintErrorPopupView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/marin/R$dimen;->marin_gutter:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 73
    invoke-virtual {p0}, Lcom/squareup/ui/print/PrintErrorPopupView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/marin/R$color;->marin_divider_gray:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 74
    new-instance v2, Lcom/squareup/ui/DividerDecoration;

    mul-int/lit8 v0, v0, 0x2

    const/4 v3, 0x1

    invoke-direct {v2, v0, v3, v1}, Lcom/squareup/ui/DividerDecoration;-><init>(III)V

    .line 75
    invoke-virtual {v2, v3}, Lcom/squareup/ui/DividerDecoration;->ignorePadding(Z)V

    move-object v1, v2

    .line 77
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupView;->presenter:Lcom/squareup/ui/print/PrintErrorPopupPresenter;

    iget-object v0, v0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->discardPrintJobsPresenter:Lcom/squareup/mortar/PopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/print/PrintErrorPopupView;->discardPrintJobsPopup:Lcom/squareup/flowlegacy/ConfirmationPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/mortar/PopupPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method update()V
    .locals 2

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupView;->errorAdapter:Landroidx/recyclerview/widget/RecyclerView$Adapter;

    if-nez v0, :cond_0

    .line 93
    new-instance v0, Lcom/squareup/ui/print/PrintErrorPopupView$PrintErrorAdapter;

    invoke-direct {v0, p0}, Lcom/squareup/ui/print/PrintErrorPopupView$PrintErrorAdapter;-><init>(Lcom/squareup/ui/print/PrintErrorPopupView;)V

    iput-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupView;->errorAdapter:Landroidx/recyclerview/widget/RecyclerView$Adapter;

    .line 94
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v1, p0, Lcom/squareup/ui/print/PrintErrorPopupView;->errorAdapter:Landroidx/recyclerview/widget/RecyclerView$Adapter;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    goto :goto_0

    .line 96
    :cond_0
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    :goto_0
    return-void
.end method
