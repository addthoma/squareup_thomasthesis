.class Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorRow;
.super Ljava/lang/Object;
.source "PrintErrorPopupPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/print/PrintErrorPopupPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ErrorRow"
.end annotation


# instance fields
.field final bucket:Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorBucket;

.field final printJobs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/print/PrintJob;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorBucket;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorBucket;",
            "Ljava/util/List<",
            "Lcom/squareup/print/PrintJob;",
            ">;)V"
        }
    .end annotation

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput-object p1, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorRow;->bucket:Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorBucket;

    .line 70
    iput-object p2, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorRow;->printJobs:Ljava/util/List;

    return-void
.end method
