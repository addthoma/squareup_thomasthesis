.class Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "PrintErrorPopupView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;->bindRow(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;

.field final synthetic val$adjustedPosition:I


# direct methods
.method constructor <init>(Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;I)V
    .locals 0

    .line 214
    iput-object p1, p0, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder$1;->this$1:Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;

    iput p2, p0, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder$1;->val$adjustedPosition:I

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    .line 217
    iget-object p1, p0, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder$1;->this$1:Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;

    invoke-static {p1}, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;->access$000(Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;)Landroid/widget/TextView;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 218
    iget-object p1, p0, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder$1;->this$1:Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;

    iget-object p1, p1, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder;->this$0:Lcom/squareup/ui/print/PrintErrorPopupView;

    iget-object p1, p1, Lcom/squareup/ui/print/PrintErrorPopupView;->presenter:Lcom/squareup/ui/print/PrintErrorPopupPresenter;

    iget v0, p0, Lcom/squareup/ui/print/PrintErrorPopupView$ErrorRowHolder$1;->val$adjustedPosition:I

    invoke-virtual {p1, v0}, Lcom/squareup/ui/print/PrintErrorPopupPresenter;->onReprintClicked(I)V

    return-void
.end method
