.class public Lcom/squareup/ui/print/PrintErrorPopupEvent;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "PrintErrorPopupEvent.java"


# instance fields
.field public final error_messages:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 16
    invoke-direct {p0, p1, p2}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    .line 17
    iput-object p3, p0, Lcom/squareup/ui/print/PrintErrorPopupEvent;->error_messages:Ljava/lang/String;

    return-void
.end method

.method public static forViewPrintErrorPopup([Ljava/lang/String;)Lcom/squareup/eventstream/v1/EventStreamEvent;
    .locals 3

    .line 21
    sget-object v0, Ljava/lang/String;->CASE_INSENSITIVE_ORDER:Ljava/util/Comparator;

    invoke-static {p0, v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    const-string v0, "|"

    .line 22
    invoke-static {p0, v0}, Lcom/squareup/util/Strings;->join([Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 23
    new-instance v0, Lcom/squareup/ui/print/PrintErrorPopupEvent;

    sget-object v1, Lcom/squareup/eventstream/v1/EventStream$Name;->VIEW:Lcom/squareup/eventstream/v1/EventStream$Name;

    sget-object v2, Lcom/squareup/analytics/RegisterViewName;->PRINT_ERROR_POPUP:Lcom/squareup/analytics/RegisterViewName;

    iget-object v2, v2, Lcom/squareup/analytics/RegisterViewName;->value:Ljava/lang/String;

    invoke-direct {v0, v1, v2, p0}, Lcom/squareup/ui/print/PrintErrorPopupEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method
