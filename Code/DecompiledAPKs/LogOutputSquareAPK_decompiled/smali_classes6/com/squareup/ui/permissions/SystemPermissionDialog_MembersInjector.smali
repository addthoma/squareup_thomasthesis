.class public final Lcom/squareup/ui/permissions/SystemPermissionDialog_MembersInjector;
.super Ljava/lang/Object;
.source "SystemPermissionDialog_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/permissions/SystemPermissionDialog;",
        ">;"
    }
.end annotation


# instance fields
.field private final appNameFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AppNameFormatter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AppNameFormatter;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/ui/permissions/SystemPermissionDialog_MembersInjector;->appNameFormatterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AppNameFormatter;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/permissions/SystemPermissionDialog;",
            ">;"
        }
    .end annotation

    .line 27
    new-instance v0, Lcom/squareup/ui/permissions/SystemPermissionDialog_MembersInjector;

    invoke-direct {v0, p0}, Lcom/squareup/ui/permissions/SystemPermissionDialog_MembersInjector;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectAppNameFormatter(Lcom/squareup/ui/permissions/SystemPermissionDialog;Lcom/squareup/util/AppNameFormatter;)V
    .locals 0

    .line 37
    iput-object p1, p0, Lcom/squareup/ui/permissions/SystemPermissionDialog;->appNameFormatter:Lcom/squareup/util/AppNameFormatter;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/permissions/SystemPermissionDialog;)V
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/squareup/ui/permissions/SystemPermissionDialog_MembersInjector;->appNameFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/AppNameFormatter;

    invoke-static {p1, v0}, Lcom/squareup/ui/permissions/SystemPermissionDialog_MembersInjector;->injectAppNameFormatter(Lcom/squareup/ui/permissions/SystemPermissionDialog;Lcom/squareup/util/AppNameFormatter;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 9
    check-cast p1, Lcom/squareup/ui/permissions/SystemPermissionDialog;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/permissions/SystemPermissionDialog_MembersInjector;->injectMembers(Lcom/squareup/ui/permissions/SystemPermissionDialog;)V

    return-void
.end method
