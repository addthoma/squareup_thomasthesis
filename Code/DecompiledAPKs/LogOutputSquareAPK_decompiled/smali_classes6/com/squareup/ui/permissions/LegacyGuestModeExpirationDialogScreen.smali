.class public Lcom/squareup/ui/permissions/LegacyGuestModeExpirationDialogScreen;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "LegacyGuestModeExpirationDialogScreen.java"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/permissions/LegacyGuestModeExpirationDialogScreen$Factory;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/permissions/LegacyGuestModeExpirationDialogScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/permissions/LegacyGuestModeExpirationDialogScreen$Factory;,
        Lcom/squareup/ui/permissions/LegacyGuestModeExpirationDialogScreen$Component;,
        Lcom/squareup/ui/permissions/LegacyGuestModeExpirationDialogScreen$ParentComponent;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/permissions/LegacyGuestModeExpirationDialogScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final daysUntilExpiration:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 51
    sget-object v0, Lcom/squareup/ui/permissions/-$$Lambda$LegacyGuestModeExpirationDialogScreen$U9YT6SbDPWnoZci58wt-DxI2rHE;->INSTANCE:Lcom/squareup/ui/permissions/-$$Lambda$LegacyGuestModeExpirationDialogScreen$U9YT6SbDPWnoZci58wt-DxI2rHE;

    .line 52
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/permissions/LegacyGuestModeExpirationDialogScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(J)V
    .locals 0

    .line 55
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    .line 56
    iput-wide p1, p0, Lcom/squareup/ui/permissions/LegacyGuestModeExpirationDialogScreen;->daysUntilExpiration:J

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/permissions/LegacyGuestModeExpirationDialogScreen;)J
    .locals 2

    .line 20
    iget-wide v0, p0, Lcom/squareup/ui/permissions/LegacyGuestModeExpirationDialogScreen;->daysUntilExpiration:J

    return-wide v0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/permissions/LegacyGuestModeExpirationDialogScreen;
    .locals 3

    .line 53
    new-instance v0, Lcom/squareup/ui/permissions/LegacyGuestModeExpirationDialogScreen;

    invoke-virtual {p0}, Landroid/os/Parcel;->readLong()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lcom/squareup/ui/permissions/LegacyGuestModeExpirationDialogScreen;-><init>(J)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .line 60
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/main/InMainActivityScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 61
    iget-wide v0, p0, Lcom/squareup/ui/permissions/LegacyGuestModeExpirationDialogScreen;->daysUntilExpiration:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    return-void
.end method
