.class final Lcom/squareup/ui/photo/ItemPhoto$Loader;
.super Ljava/lang/Object;
.source "ItemPhoto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/photo/ItemPhoto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "Loader"
.end annotation


# instance fields
.field private final picasso:Lcom/squareup/picasso/Picasso;

.field private final thumbor:Lcom/squareup/pollexor/Thumbor;


# direct methods
.method private constructor <init>(Lcom/squareup/picasso/Picasso;Lcom/squareup/pollexor/Thumbor;)V
    .locals 0

    .line 135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 136
    iput-object p1, p0, Lcom/squareup/ui/photo/ItemPhoto$Loader;->picasso:Lcom/squareup/picasso/Picasso;

    .line 137
    iput-object p2, p0, Lcom/squareup/ui/photo/ItemPhoto$Loader;->thumbor:Lcom/squareup/pollexor/Thumbor;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/picasso/Picasso;Lcom/squareup/pollexor/Thumbor;Lcom/squareup/ui/photo/ItemPhoto$1;)V
    .locals 0

    .line 131
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/photo/ItemPhoto$Loader;-><init>(Lcom/squareup/picasso/Picasso;Lcom/squareup/pollexor/Thumbor;)V

    return-void
.end method

.method static synthetic access$400(Lcom/squareup/ui/photo/ItemPhoto$Loader;)Lcom/squareup/picasso/Picasso;
    .locals 0

    .line 131
    iget-object p0, p0, Lcom/squareup/ui/photo/ItemPhoto$Loader;->picasso:Lcom/squareup/picasso/Picasso;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/ui/photo/ItemPhoto$Loader;Landroid/net/Uri;ZILandroid/graphics/drawable/Drawable;)Lcom/squareup/picasso/RequestCreator;
    .locals 0

    .line 131
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/ui/photo/ItemPhoto$Loader;->load(Landroid/net/Uri;ZILandroid/graphics/drawable/Drawable;)Lcom/squareup/picasso/RequestCreator;

    move-result-object p0

    return-object p0
.end method

.method private load(Landroid/net/Uri;ZILandroid/graphics/drawable/Drawable;)Lcom/squareup/picasso/RequestCreator;
    .locals 0

    if-eqz p2, :cond_0

    .line 160
    iget-object p2, p0, Lcom/squareup/ui/photo/ItemPhoto$Loader;->thumbor:Lcom/squareup/pollexor/Thumbor;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/pollexor/Thumbor;->buildImage(Ljava/lang/String;)Lcom/squareup/pollexor/ThumborUrlBuilder;

    move-result-object p1

    invoke-virtual {p1, p3, p3}, Lcom/squareup/pollexor/ThumborUrlBuilder;->resize(II)Lcom/squareup/pollexor/ThumborUrlBuilder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/pollexor/ThumborUrlBuilder;->toUrl()Ljava/lang/String;

    move-result-object p1

    .line 161
    iget-object p2, p0, Lcom/squareup/ui/photo/ItemPhoto$Loader;->picasso:Lcom/squareup/picasso/Picasso;

    invoke-virtual {p2, p1}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object p1

    invoke-virtual {p1, p4}, Lcom/squareup/picasso/RequestCreator;->placeholder(Landroid/graphics/drawable/Drawable;)Lcom/squareup/picasso/RequestCreator;

    move-result-object p1

    goto :goto_0

    .line 164
    :cond_0
    iget-object p2, p0, Lcom/squareup/ui/photo/ItemPhoto$Loader;->picasso:Lcom/squareup/picasso/Picasso;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object p1

    invoke-virtual {p1, p3, p3}, Lcom/squareup/picasso/RequestCreator;->resize(II)Lcom/squareup/picasso/RequestCreator;

    move-result-object p1

    invoke-virtual {p1, p4}, Lcom/squareup/picasso/RequestCreator;->placeholder(Landroid/graphics/drawable/Drawable;)Lcom/squareup/picasso/RequestCreator;

    move-result-object p1

    .line 166
    invoke-virtual {p1}, Lcom/squareup/picasso/RequestCreator;->centerInside()Lcom/squareup/picasso/RequestCreator;

    move-result-object p1

    :goto_0
    return-object p1
.end method


# virtual methods
.method load(Landroid/net/Uri;ZILandroid/widget/ImageView;Landroid/graphics/drawable/Drawable;)V
    .locals 0

    if-nez p1, :cond_0

    .line 142
    iget-object p1, p0, Lcom/squareup/ui/photo/ItemPhoto$Loader;->picasso:Lcom/squareup/picasso/Picasso;

    invoke-virtual {p1, p4}, Lcom/squareup/picasso/Picasso;->cancelRequest(Landroid/widget/ImageView;)V

    .line 143
    invoke-virtual {p4, p5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 145
    :cond_0
    invoke-direct {p0, p1, p2, p3, p5}, Lcom/squareup/ui/photo/ItemPhoto$Loader;->load(Landroid/net/Uri;ZILandroid/graphics/drawable/Drawable;)Lcom/squareup/picasso/RequestCreator;

    move-result-object p1

    invoke-virtual {p1, p4}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    :goto_0
    return-void
.end method

.method load(Landroid/net/Uri;ZILcom/squareup/picasso/Target;)V
    .locals 1

    if-nez p1, :cond_0

    .line 151
    iget-object p1, p0, Lcom/squareup/ui/photo/ItemPhoto$Loader;->picasso:Lcom/squareup/picasso/Picasso;

    invoke-virtual {p1, p4}, Lcom/squareup/picasso/Picasso;->cancelRequest(Lcom/squareup/picasso/Target;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 153
    check-cast v0, Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/ui/photo/ItemPhoto$Loader;->load(Landroid/net/Uri;ZILandroid/graphics/drawable/Drawable;)Lcom/squareup/picasso/RequestCreator;

    move-result-object p1

    invoke-virtual {p1, p4}, Lcom/squareup/picasso/RequestCreator;->into(Lcom/squareup/picasso/Target;)V

    :goto_0
    return-void
.end method
