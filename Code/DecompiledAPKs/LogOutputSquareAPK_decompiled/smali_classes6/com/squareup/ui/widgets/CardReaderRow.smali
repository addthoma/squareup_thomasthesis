.class public Lcom/squareup/ui/widgets/CardReaderRow;
.super Landroid/widget/LinearLayout;
.source "CardReaderRow.java"


# instance fields
.field private description:Landroid/widget/TextView;

.field private glyph:Lcom/squareup/glyph/SquareGlyphView;

.field private name:Lcom/squareup/widgets/ShorteningTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .line 27
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    sget-object v0, Lcom/squareup/widgets/pos/R$styleable;->GlyphRadioRow:[I

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p2

    .line 29
    sget v0, Lcom/squareup/widgets/pos/R$styleable;->GlyphRadioRow_android_text:I

    invoke-virtual {p2, v0}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 30
    sget v1, Lcom/squareup/widgets/pos/R$styleable;->GlyphRadioRow_shortText:I

    invoke-virtual {p2, v1}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 32
    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    .line 34
    sget p2, Lcom/squareup/widgets/pos/R$layout;->reader_status_row:I

    invoke-static {p1, p2, p0}, Lcom/squareup/ui/widgets/CardReaderRow;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 35
    invoke-direct {p0}, Lcom/squareup/ui/widgets/CardReaderRow;->bindViews()V

    .line 37
    iget-object p1, p0, Lcom/squareup/ui/widgets/CardReaderRow;->name:Lcom/squareup/widgets/ShorteningTextView;

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/ShorteningTextView;->setText(Ljava/lang/CharSequence;)V

    .line 38
    iget-object p1, p0, Lcom/squareup/ui/widgets/CardReaderRow;->name:Lcom/squareup/widgets/ShorteningTextView;

    invoke-virtual {p1, v1}, Lcom/squareup/widgets/ShorteningTextView;->setShortText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 62
    sget v0, Lcom/squareup/widgets/pos/R$id;->description:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/widgets/CardReaderRow;->description:Landroid/widget/TextView;

    .line 63
    sget v0, Lcom/squareup/widgets/pos/R$id;->glyph:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/ui/widgets/CardReaderRow;->glyph:Lcom/squareup/glyph/SquareGlyphView;

    .line 64
    sget v0, Lcom/squareup/widgets/pos/R$id;->name:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/ShorteningTextView;

    iput-object v0, p0, Lcom/squareup/ui/widgets/CardReaderRow;->name:Lcom/squareup/widgets/ShorteningTextView;

    return-void
.end method


# virtual methods
.method public setDescription(Ljava/lang/CharSequence;)V
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/widgets/CardReaderRow;->description:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/widgets/CardReaderRow;->description:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/widgets/CardReaderRow;->glyph:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    .line 54
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/widgets/CardReaderRow;->glyph:Lcom/squareup/glyph/SquareGlyphView;

    if-eqz p1, :cond_1

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public setGlyphColorRes(I)V
    .locals 1

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/widgets/CardReaderRow;->glyph:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyphColorRes(I)V

    return-void
.end method

.method public setName(Ljava/lang/CharSequence;)V
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/widgets/CardReaderRow;->name:Lcom/squareup/widgets/ShorteningTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/ShorteningTextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
