.class Lcom/squareup/ui/tour/LearnMoreTourPopupView$3;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "LearnMoreTourPopupView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/tour/LearnMoreTourPopupView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/tour/LearnMoreTourPopupView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/tour/LearnMoreTourPopupView;)V
    .locals 0

    .line 103
    iput-object p1, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView$3;->this$0:Lcom/squareup/ui/tour/LearnMoreTourPopupView;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 2

    .line 105
    iget-object p1, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView$3;->this$0:Lcom/squareup/ui/tour/LearnMoreTourPopupView;

    invoke-static {p1}, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->access$300(Lcom/squareup/ui/tour/LearnMoreTourPopupView;)Landroidx/viewpager/widget/ViewPager;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView$3;->this$0:Lcom/squareup/ui/tour/LearnMoreTourPopupView;

    invoke-static {v0}, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->access$000(Lcom/squareup/ui/tour/LearnMoreTourPopupView;)Lcom/squareup/tour/TourAdapter;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/tour/TourAdapter;->pages:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iget-object v1, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupView$3;->this$0:Lcom/squareup/ui/tour/LearnMoreTourPopupView;

    invoke-static {v1}, Lcom/squareup/ui/tour/LearnMoreTourPopupView;->access$300(Lcom/squareup/ui/tour/LearnMoreTourPopupView;)Landroidx/viewpager/widget/ViewPager;

    move-result-object v1

    invoke-virtual {v1}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-virtual {p1, v0}, Landroidx/viewpager/widget/ViewPager;->setCurrentItem(I)V

    return-void
.end method
