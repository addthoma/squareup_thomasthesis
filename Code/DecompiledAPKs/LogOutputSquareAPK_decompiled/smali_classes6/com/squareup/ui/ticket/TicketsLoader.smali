.class public Lcom/squareup/ui/ticket/TicketsLoader;
.super Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;
.source "TicketsLoader.java"

# interfaces
.implements Lmortar/Scoped;


# direct methods
.method public constructor <init>(Lcom/squareup/tickets/Tickets;Lcom/squareup/opentickets/TicketsListScheduler;Lcom/squareup/log/tickets/OpenTicketsLogger;Lcom/squareup/connectivity/ConnectivityMonitor;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 18
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;-><init>(Lcom/squareup/tickets/Tickets;Lcom/squareup/opentickets/TicketsListScheduler;Lcom/squareup/log/tickets/OpenTicketsLogger;Lcom/squareup/connectivity/ConnectivityMonitor;)V

    return-void
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    .line 22
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketsLoader;->start()V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    .line 26
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketsLoader;->stop()V

    return-void
.end method
