.class public Lcom/squareup/ui/ticket/TicketCompScreen$Presenter;
.super Lcom/squareup/configure/item/VoidCompPresenter;
.source "TicketCompScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/TicketCompScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/configure/item/VoidCompPresenter<",
        "Lcom/squareup/shared/catalog/models/CatalogDiscount;",
        ">;"
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

.field private final flow:Lflow/Flow;

.field private screen:Lcom/squareup/ui/ticket/TicketCompScreen;

.field private final transaction:Lcom/squareup/payment/Transaction;

.field private final transactionComps:Lcom/squareup/payment/TransactionComps;

.field private final zeroAmount:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Res;Lcom/squareup/tickets/voidcomp/CompDiscountsCache;Lflow/Flow;Lcom/squareup/payment/Transaction;Lcom/squareup/payment/TransactionComps;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/tickets/voidcomp/CompDiscountsCache;",
            "Lflow/Flow;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/payment/TransactionComps;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 76
    invoke-direct {p0, p2, p3}, Lcom/squareup/configure/item/VoidCompPresenter;-><init>(Lcom/squareup/util/Res;Lcom/squareup/tickets/voidcomp/CogsCache;)V

    .line 77
    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketCompScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 78
    iput-object p4, p0, Lcom/squareup/ui/ticket/TicketCompScreen$Presenter;->flow:Lflow/Flow;

    .line 79
    iput-object p5, p0, Lcom/squareup/ui/ticket/TicketCompScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 80
    iput-object p6, p0, Lcom/squareup/ui/ticket/TicketCompScreen$Presenter;->transactionComps:Lcom/squareup/payment/TransactionComps;

    .line 81
    iput-object p7, p0, Lcom/squareup/ui/ticket/TicketCompScreen$Presenter;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    .line 82
    new-instance p1, Lcom/squareup/protos/common/Money;

    const-wide/16 p2, 0x0

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-direct {p1, p2, p9}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    invoke-interface {p8, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketCompScreen$Presenter;->zeroAmount:Ljava/lang/CharSequence;

    return-void
.end method


# virtual methods
.method protected getCancelButtonTextResourceId()I
    .locals 1

    .line 113
    sget v0, Lcom/squareup/orderentry/R$string;->comp_ticket:I

    return v0
.end method

.method protected getHelpText()Ljava/lang/String;
    .locals 3

    .line 125
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketCompScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderentry/R$string;->comp_ticket_help_text:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketCompScreen$Presenter;->zeroAmount:Ljava/lang/CharSequence;

    const-string v2, "zero_amount"

    .line 126
    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 127
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getPrimaryButtonTextResourceId()I
    .locals 1

    .line 117
    sget v0, Lcom/squareup/configure/item/R$string;->comp_initial:I

    return v0
.end method

.method protected getReasonHeaderTextResourceId()I
    .locals 1

    .line 121
    sget v0, Lcom/squareup/configure/item/R$string;->comp_uppercase_reason:I

    return v0
.end method

.method public isCompingTicket()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public synthetic lambda$null$0$TicketCompScreen$Presenter(Lcom/squareup/util/Optional;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 101
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketCompScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketCompScreen$Presenter;->screen:Lcom/squareup/ui/ticket/TicketCompScreen;

    invoke-static {v1}, Lcom/squareup/ui/ticket/TicketCompScreen;->access$100(Lcom/squareup/ui/ticket/TicketCompScreen;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/ticket/TicketCompScreen$Presenter;->screen:Lcom/squareup/ui/ticket/TicketCompScreen;

    invoke-static {v2}, Lcom/squareup/ui/ticket/TicketCompScreen;->access$200(Lcom/squareup/ui/ticket/TicketCompScreen;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/payment/Transaction;->setOpenTicketNameAndNote(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketCompScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketCompScreen$Presenter;->screen:Lcom/squareup/ui/ticket/TicketCompScreen;

    invoke-static {v1}, Lcom/squareup/ui/ticket/TicketCompScreen;->access$300(Lcom/squareup/ui/ticket/TicketCompScreen;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Transaction;->setPredefinedTicket(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;)V

    .line 103
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketCompScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->COMP_CART_COMPED:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 104
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketCompScreen$Presenter;->transactionComps:Lcom/squareup/payment/TransactionComps;

    .line 105
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketCompScreen$Presenter;->getCheckedReason()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/models/CatalogDiscount;

    .line 106
    invoke-virtual {p1}, Lcom/squareup/util/Optional;->getValueOrNull()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/Employee;

    .line 104
    invoke-virtual {v0, v1, p1}, Lcom/squareup/payment/TransactionComps;->compAllItems(Lcom/squareup/shared/catalog/models/CatalogDiscount;Lcom/squareup/protos/client/Employee;)V

    .line 108
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketCompScreen$Presenter;->flow:Lflow/Flow;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Class;

    const-class v1, Lcom/squareup/ui/ticket/TicketScreen;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {p1, v0}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    return-void
.end method

.method public synthetic lambda$onPrimaryClicked$1$TicketCompScreen$Presenter()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 97
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketCompScreen$Presenter;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketCompScreen$Presenter;->screen:Lcom/squareup/ui/ticket/TicketCompScreen;

    invoke-static {v1}, Lcom/squareup/ui/ticket/TicketCompScreen;->access$000(Lcom/squareup/ui/ticket/TicketCompScreen;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/permissions/EmployeeManagement;->oneEmployeeProtoByToken(Ljava/lang/String;)Lio/reactivex/Maybe;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/ticket/-$$Lambda$Dy0PHuYjqCY7wofLHWfV9mlLNt0;->INSTANCE:Lcom/squareup/ui/ticket/-$$Lambda$Dy0PHuYjqCY7wofLHWfV9mlLNt0;

    .line 98
    invoke-virtual {v0, v1}, Lio/reactivex/Maybe;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Maybe;

    move-result-object v0

    .line 99
    invoke-static {}, Lcom/squareup/util/Optional;->empty()Lcom/squareup/util/Optional;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Maybe;->toSingle(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$TicketCompScreen$Presenter$TI_pw-05dcYLZPnceUKi-Uabe6M;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/-$$Lambda$TicketCompScreen$Presenter$TI_pw-05dcYLZPnceUKi-Uabe6M;-><init>(Lcom/squareup/ui/ticket/TicketCompScreen$Presenter;)V

    .line 100
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public onCancelClicked()V
    .locals 2

    .line 91
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketCompScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->COMP_CART_CANCELED:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketCompScreen$Presenter;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    .line 86
    invoke-super {p0, p1}, Lcom/squareup/configure/item/VoidCompPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 87
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ticket/TicketCompScreen;

    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketCompScreen$Presenter;->screen:Lcom/squareup/ui/ticket/TicketCompScreen;

    return-void
.end method

.method public onPrimaryClicked()V
    .locals 2

    .line 96
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketCompScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$TicketCompScreen$Presenter$CWH1aKKRiouIEcQbn3hFuY2MZZ4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/-$$Lambda$TicketCompScreen$Presenter$CWH1aKKRiouIEcQbn3hFuY2MZZ4;-><init>(Lcom/squareup/ui/ticket/TicketCompScreen$Presenter;)V

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
