.class public final enum Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;
.super Ljava/lang/Enum;
.source "MergeCanceledDialogScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/MergeCanceledDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Action"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;

.field public static final enum BULK_MERGE:Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;

.field public static final enum MERGE_TRANSACTION_TICKET:Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;

.field public static final enum MOVE:Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 31
    new-instance v0, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;

    const/4 v1, 0x0

    const-string v2, "BULK_MERGE"

    invoke-direct {v0, v2, v1}, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;->BULK_MERGE:Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;

    .line 32
    new-instance v0, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;

    const/4 v2, 0x1

    const-string v3, "MERGE_TRANSACTION_TICKET"

    invoke-direct {v0, v3, v2}, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;->MERGE_TRANSACTION_TICKET:Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;

    .line 33
    new-instance v0, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;

    const/4 v3, 0x2

    const-string v4, "MOVE"

    invoke-direct {v0, v4, v3}, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;->MOVE:Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;

    .line 30
    sget-object v4, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;->BULK_MERGE:Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;->MERGE_TRANSACTION_TICKET:Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;->MOVE:Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;->$VALUES:[Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 30
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;
    .locals 1

    .line 30
    const-class v0, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;
    .locals 1

    .line 30
    sget-object v0, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;->$VALUES:[Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;

    invoke-virtual {v0}, [Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/ticket/MergeCanceledDialogScreen$Action;

    return-object v0
.end method
