.class Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$ConvertToCustomTicketButtonRowHolder;
.super Lcom/squareup/ui/RangerRecyclerAdapter$RangerHolder;
.source "EditTicketView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ConvertToCustomTicketButtonRowHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/RangerRecyclerAdapter<",
        "Lcom/squareup/ui/ticket/EditTicketView$RowType;",
        "Lcom/squareup/ui/ticket/EditTicketView$HolderType;",
        ">.RangerHolder;"
    }
.end annotation


# instance fields
.field private final convertButton:Lcom/squareup/marketfont/MarketButton;

.field final synthetic this$1:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;Landroid/view/ViewGroup;)V
    .locals 1

    .line 176
    iput-object p1, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$ConvertToCustomTicketButtonRowHolder;->this$1:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;

    .line 177
    sget v0, Lcom/squareup/orderentry/R$layout;->ticket_detail_convert_to_custom_ticket_button:I

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/ui/RangerRecyclerAdapter$RangerHolder;-><init>(Lcom/squareup/ui/RangerRecyclerAdapter;Landroid/view/ViewGroup;I)V

    .line 178
    iget-object p2, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$ConvertToCustomTicketButtonRowHolder;->itemView:Landroid/view/View;

    sget v0, Lcom/squareup/orderentry/R$id;->ticket_convert_to_custom_ticket:I

    invoke-static {p2, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/squareup/marketfont/MarketButton;

    iput-object p2, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$ConvertToCustomTicketButtonRowHolder;->convertButton:Lcom/squareup/marketfont/MarketButton;

    .line 179
    iget-object p2, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$ConvertToCustomTicketButtonRowHolder;->convertButton:Lcom/squareup/marketfont/MarketButton;

    new-instance v0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$ConvertToCustomTicketButtonRowHolder$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$ConvertToCustomTicketButtonRowHolder$1;-><init>(Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$ConvertToCustomTicketButtonRowHolder;Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;)V

    invoke-virtual {p2, v0}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
