.class public Lcom/squareup/ui/ticket/TicketSortGroup;
.super Lcom/squareup/widgets/CheckableGroup;
.source "TicketSortGroup.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/ticket/TicketSortGroup$OnSortChangeListener;
    }
.end annotation


# instance fields
.field private final employeeSortOption:Landroid/view/View;

.field private listener:Lcom/squareup/ui/ticket/TicketSortGroup$OnSortChangeListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 35
    invoke-direct {p0, p1, p2}, Lcom/squareup/widgets/CheckableGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    sget p2, Lcom/squareup/orderentry/R$layout;->ticket_sort_group:I

    invoke-static {p1, p2, p0}, Lcom/squareup/ui/ticket/TicketSortGroup;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 37
    sget p1, Lcom/squareup/orderentry/R$id;->ticket_sort_employee:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketSortGroup;->employeeSortOption:Landroid/view/View;

    .line 38
    new-instance p1, Lcom/squareup/ui/ticket/-$$Lambda$TicketSortGroup$r_D4vfsPwinc8-MkdStdEX6hLNs;

    invoke-direct {p1, p0}, Lcom/squareup/ui/ticket/-$$Lambda$TicketSortGroup$r_D4vfsPwinc8-MkdStdEX6hLNs;-><init>(Lcom/squareup/ui/ticket/TicketSortGroup;)V

    invoke-virtual {p0, p1}, Lcom/squareup/ui/ticket/TicketSortGroup;->setOnCheckedChangeListener(Lcom/squareup/widgets/CheckableGroup$OnCheckedChangeListener;)V

    return-void
.end method


# virtual methods
.method public synthetic lambda$new$0$TicketSortGroup(Lcom/squareup/widgets/CheckableGroup;II)V
    .locals 0

    .line 39
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketSortGroup;->listener:Lcom/squareup/ui/ticket/TicketSortGroup$OnSortChangeListener;

    if-eqz p1, :cond_5

    if-ne p2, p3, :cond_0

    return-void

    .line 42
    :cond_0
    sget p1, Lcom/squareup/orderentry/R$id;->ticket_sort_name:I

    if-ne p2, p1, :cond_1

    .line 43
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketSortGroup;->listener:Lcom/squareup/ui/ticket/TicketSortGroup$OnSortChangeListener;

    sget-object p2, Lcom/squareup/tickets/TicketSort;->NAME:Lcom/squareup/tickets/TicketSort;

    invoke-interface {p1, p2}, Lcom/squareup/ui/ticket/TicketSortGroup$OnSortChangeListener;->onSortChange(Lcom/squareup/tickets/TicketSort;)V

    goto :goto_0

    .line 44
    :cond_1
    sget p1, Lcom/squareup/orderentry/R$id;->ticket_sort_amount:I

    if-ne p2, p1, :cond_2

    .line 45
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketSortGroup;->listener:Lcom/squareup/ui/ticket/TicketSortGroup$OnSortChangeListener;

    sget-object p2, Lcom/squareup/tickets/TicketSort;->AMOUNT:Lcom/squareup/tickets/TicketSort;

    invoke-interface {p1, p2}, Lcom/squareup/ui/ticket/TicketSortGroup$OnSortChangeListener;->onSortChange(Lcom/squareup/tickets/TicketSort;)V

    goto :goto_0

    .line 46
    :cond_2
    sget p1, Lcom/squareup/orderentry/R$id;->ticket_sort_recent:I

    if-ne p2, p1, :cond_3

    .line 47
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketSortGroup;->listener:Lcom/squareup/ui/ticket/TicketSortGroup$OnSortChangeListener;

    sget-object p2, Lcom/squareup/tickets/TicketSort;->RECENT:Lcom/squareup/tickets/TicketSort;

    invoke-interface {p1, p2}, Lcom/squareup/ui/ticket/TicketSortGroup$OnSortChangeListener;->onSortChange(Lcom/squareup/tickets/TicketSort;)V

    goto :goto_0

    .line 48
    :cond_3
    sget p1, Lcom/squareup/orderentry/R$id;->ticket_sort_employee:I

    if-ne p2, p1, :cond_4

    .line 49
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketSortGroup;->listener:Lcom/squareup/ui/ticket/TicketSortGroup$OnSortChangeListener;

    sget-object p2, Lcom/squareup/tickets/TicketSort;->EMPLOYEE_NAME:Lcom/squareup/tickets/TicketSort;

    invoke-interface {p1, p2}, Lcom/squareup/ui/ticket/TicketSortGroup$OnSortChangeListener;->onSortChange(Lcom/squareup/tickets/TicketSort;)V

    goto :goto_0

    .line 51
    :cond_4
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketSortGroup;->listener:Lcom/squareup/ui/ticket/TicketSortGroup$OnSortChangeListener;

    sget-object p2, Lcom/squareup/tickets/TicketSort;->NAME:Lcom/squareup/tickets/TicketSort;

    invoke-interface {p1, p2}, Lcom/squareup/ui/ticket/TicketSortGroup$OnSortChangeListener;->onSortChange(Lcom/squareup/tickets/TicketSort;)V

    :cond_5
    :goto_0
    return-void
.end method

.method public setEmployeeSortVisible(Z)V
    .locals 1

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketSortGroup;->employeeSortOption:Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public setOnSortChangeListener(Lcom/squareup/ui/ticket/TicketSortGroup$OnSortChangeListener;)V
    .locals 0

    .line 90
    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketSortGroup;->listener:Lcom/squareup/ui/ticket/TicketSortGroup$OnSortChangeListener;

    return-void
.end method

.method public setSortType(Lcom/squareup/tickets/TicketSort;)V
    .locals 1

    .line 62
    sget-object v0, Lcom/squareup/ui/ticket/TicketSortGroup$1;->$SwitchMap$com$squareup$tickets$TicketSort:[I

    invoke-virtual {p1}, Lcom/squareup/tickets/TicketSort;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_3

    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    const/4 v0, 0x3

    if-eq p1, v0, :cond_1

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    .line 80
    sget p1, Lcom/squareup/orderentry/R$id;->ticket_sort_name:I

    invoke-virtual {p0, p1}, Lcom/squareup/ui/ticket/TicketSortGroup;->check(I)V

    goto :goto_0

    .line 76
    :cond_0
    sget p1, Lcom/squareup/orderentry/R$id;->ticket_sort_employee:I

    invoke-virtual {p0, p1}, Lcom/squareup/ui/ticket/TicketSortGroup;->check(I)V

    goto :goto_0

    .line 72
    :cond_1
    sget p1, Lcom/squareup/orderentry/R$id;->ticket_sort_recent:I

    invoke-virtual {p0, p1}, Lcom/squareup/ui/ticket/TicketSortGroup;->check(I)V

    goto :goto_0

    .line 68
    :cond_2
    sget p1, Lcom/squareup/orderentry/R$id;->ticket_sort_amount:I

    invoke-virtual {p0, p1}, Lcom/squareup/ui/ticket/TicketSortGroup;->check(I)V

    goto :goto_0

    .line 64
    :cond_3
    sget p1, Lcom/squareup/orderentry/R$id;->ticket_sort_name:I

    invoke-virtual {p0, p1}, Lcom/squareup/ui/ticket/TicketSortGroup;->check(I)V

    :goto_0
    return-void
.end method
