.class Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter$1;
.super Lio/reactivex/observers/DisposableObserver;
.source "TicketTransferEmployeesScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->lambda$onLoad$1()Lio/reactivex/disposables/Disposable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lio/reactivex/observers/DisposableObserver<",
        "Ljava/util/ArrayList<",
        "Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$SearchableEmployeeInfo;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;)V
    .locals 0

    .line 183
    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter$1;->this$0:Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;

    invoke-direct {p0}, Lio/reactivex/observers/DisposableObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onComplete()V
    .locals 2

    .line 185
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Shouldn\'t ever complete an activeEmployee\'s sub!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    .line 190
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method public bridge synthetic onNext(Ljava/lang/Object;)V
    .locals 0

    .line 183
    check-cast p1, Ljava/util/ArrayList;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter$1;->onNext(Ljava/util/ArrayList;)V

    return-void
.end method

.method public onNext(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$SearchableEmployeeInfo;",
            ">;)V"
        }
    .end annotation

    .line 195
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter$1;->this$0:Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;

    invoke-static {v0, p1}, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->access$102(Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;Ljava/util/List;)Ljava/util/List;

    .line 196
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter$1;->this$0:Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;

    invoke-static {v0}, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->access$200(Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/TicketTransferEmployeesView;

    .line 197
    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketTransferEmployeesView;->showList()V

    .line 198
    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter$1;->this$0:Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;

    invoke-static {v1}, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->access$300(Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$Presenter;->filterEmployees(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/TicketTransferEmployeesView;->updateEmployees(Ljava/util/List;)V

    return-void
.end method
