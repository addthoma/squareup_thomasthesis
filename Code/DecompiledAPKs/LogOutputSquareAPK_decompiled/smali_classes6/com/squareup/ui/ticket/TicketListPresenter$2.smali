.class Lcom/squareup/ui/ticket/TicketListPresenter$2;
.super Lcom/squareup/ui/LinkSpan;
.source "TicketListPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/ticket/TicketListPresenter;->getErrorMessage()Ljava/lang/CharSequence;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/ticket/TicketListPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/TicketListPresenter;I)V
    .locals 0

    .line 394
    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketListPresenter$2;->this$0:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-direct {p0, p2}, Lcom/squareup/ui/LinkSpan;-><init>(I)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .line 402
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    .line 403
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 404
    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketListPresenter$2;->this$0:Lcom/squareup/ui/ticket/TicketListPresenter;

    .line 405
    invoke-static {v1}, Lcom/squareup/ui/ticket/TicketListPresenter;->access$100(Lcom/squareup/ui/ticket/TicketListPresenter;)Lcom/squareup/util/Res;

    move-result-object v1

    sget v2, Lcom/squareup/cardreader/ui/R$string;->play_store_intent_uri:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 404
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 406
    invoke-static {v0, p1}, Lcom/squareup/util/Intents;->isIntentAvailable(Landroid/content/Intent;Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 407
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method
