.class public Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;
.super Ljava/lang/Object;
.source "MoveTicketPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/MoveTicketPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MoveTicketListener"
.end annotation


# instance fields
.field private final moveAboutToComplete:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final moveCompleted:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 384
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 386
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;->moveAboutToComplete:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 387
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;->moveCompleted:Lcom/jakewharton/rxrelay/PublishRelay;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;Z)V
    .locals 0

    .line 384
    invoke-direct {p0, p1}, Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;->publishMoveTicketsAboutToComplete(Z)V

    return-void
.end method

.method private publishMoveTicketsAboutToComplete(Z)V
    .locals 1

    .line 390
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;->moveAboutToComplete:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public onMoveTicketsAboutToComplete()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 398
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;->moveAboutToComplete:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method public onMoveTicketsCompleted()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 402
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;->moveCompleted:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method public publishMoveTicketsCompleted(Z)V
    .locals 1

    .line 394
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;->moveCompleted:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method
