.class public final Lcom/squareup/ui/ticket/MergeTicketScreen;
.super Lcom/squareup/ui/ticket/InTicketActionScope;
.source "MergeTicketScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/spot/HasSpot;
.implements Lcom/squareup/ui/ticket/TicketScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/ticket/MergeTicketScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/ticket/MergeTicketScreen$Component;,
        Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketListener;,
        Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/ticket/MergeTicketScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/ticket/MergeTicketScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 59
    new-instance v0, Lcom/squareup/ui/ticket/MergeTicketScreen;

    invoke-direct {v0}, Lcom/squareup/ui/ticket/MergeTicketScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/ticket/MergeTicketScreen;->INSTANCE:Lcom/squareup/ui/ticket/MergeTicketScreen;

    .line 385
    sget-object v0, Lcom/squareup/ui/ticket/MergeTicketScreen;->INSTANCE:Lcom/squareup/ui/ticket/MergeTicketScreen;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/ticket/MergeTicketScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 69
    invoke-direct {p0}, Lcom/squareup/ui/ticket/InTicketActionScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 66
    sget-object p1, Lcom/squareup/container/spot/Spots;->RIGHT:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 62
    sget v0, Lcom/squareup/orderentry/R$layout;->merge_ticket_view:I

    return v0
.end method
