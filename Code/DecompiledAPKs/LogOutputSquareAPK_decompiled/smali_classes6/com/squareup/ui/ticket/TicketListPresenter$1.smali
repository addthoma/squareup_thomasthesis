.class Lcom/squareup/ui/ticket/TicketListPresenter$1;
.super Lcom/squareup/permissions/PermissionGatekeeper$When;
.source "TicketListPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/ticket/TicketListPresenter;->onViewAllTicketsButtonClicked()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/ticket/TicketListPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/TicketListPresenter;)V
    .locals 0

    .line 252
    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketListPresenter$1;->this$0:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-direct {p0}, Lcom/squareup/permissions/PermissionGatekeeper$When;-><init>()V

    return-void
.end method


# virtual methods
.method public success()V
    .locals 2

    .line 254
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter$1;->this$0:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-static {v0}, Lcom/squareup/ui/ticket/TicketListPresenter;->access$000(Lcom/squareup/ui/ticket/TicketListPresenter;)Lcom/squareup/ui/ticket/TicketScopeRunner;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ticket/TicketScopeRunner;->setPermittedToViewAllTickets(Z)V

    return-void
.end method
