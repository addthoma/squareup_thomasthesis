.class abstract Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketPresenter;
.super Lmortar/ViewPresenter;
.source "EditTicketScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/EditTicketScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x408
    name = "EditTicketPresenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ">",
        "Lmortar/ViewPresenter<",
        "TV;>;"
    }
.end annotation


# instance fields
.field afterAction:Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;

.field final editTicketController:Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;

.field protected final flow:Lflow/Flow;

.field ticketAction:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

.field protected final tickets:Lcom/squareup/tickets/Tickets;

.field protected final ticketsLogger:Lcom/squareup/log/tickets/OpenTicketsLogger;

.field protected final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;Lflow/Flow;)V
    .locals 1

    .line 397
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 398
    iput-object p1, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketPresenter;->editTicketController:Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;

    .line 399
    iget-object v0, p1, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->ticketsLogger:Lcom/squareup/log/tickets/OpenTicketsLogger;

    iput-object v0, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketPresenter;->ticketsLogger:Lcom/squareup/log/tickets/OpenTicketsLogger;

    .line 400
    iget-object v0, p1, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->transaction:Lcom/squareup/payment/Transaction;

    iput-object v0, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketPresenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 401
    iget-object p1, p1, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->tickets:Lcom/squareup/tickets/Tickets;

    iput-object p1, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketPresenter;->tickets:Lcom/squareup/tickets/Tickets;

    .line 402
    iput-object p2, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketPresenter;->flow:Lflow/Flow;

    return-void
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 406
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 407
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ticket/EditTicketScreen;

    .line 408
    invoke-static {p1}, Lcom/squareup/ui/ticket/EditTicketScreen;->access$100(Lcom/squareup/ui/ticket/EditTicketScreen;)Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketPresenter;->ticketAction:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    .line 409
    invoke-static {p1}, Lcom/squareup/ui/ticket/EditTicketScreen;->access$200(Lcom/squareup/ui/ticket/EditTicketScreen;)Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketPresenter;->afterAction:Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;

    return-void
.end method

.method saveTicket(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/TicketGroup;Lcom/squareup/api/items/TicketTemplate;)V
    .locals 14

    move-object v0, p0

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/Object;

    .line 414
    iget-object v3, v0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketPresenter;->ticketAction:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-string v3, "saveTicket: %s"

    invoke-static {v3, v2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 415
    sget-object v2, Lcom/squareup/ui/ticket/EditTicketScreen$1;->$SwitchMap$com$squareup$ui$ticket$EditTicketScreen$TicketAction:[I

    iget-object v3, v0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketPresenter;->ticketAction:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    invoke-virtual {v3}, Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;->ordinal()I

    move-result v3

    aget v2, v2, v3

    const-string v3, "Must use EXIT for ticketAction %s!"

    if-eq v2, v1, :cond_5

    const/4 v5, 0x2

    if-eq v2, v5, :cond_3

    const/4 v3, 0x3

    if-ne v2, v3, :cond_2

    .line 429
    iget-object v2, v0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketPresenter;->afterAction:Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;

    sget-object v3, Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;->SPLIT_TICKET:Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;

    if-ne v2, v3, :cond_0

    .line 430
    iget-object v5, v0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketPresenter;->editTicketController:Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;

    iget-object v6, v0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketPresenter;->flow:Lflow/Flow;

    move-object v7, p1

    move-object/from16 v8, p2

    move-object/from16 v9, p3

    move-object/from16 v10, p4

    invoke-virtual/range {v5 .. v10}, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->saveForSplitTicket(Lflow/Flow;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/TicketGroup;Lcom/squareup/api/items/TicketTemplate;)V

    goto :goto_1

    .line 433
    :cond_0
    iget-object v7, v0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketPresenter;->editTicketController:Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;

    iget-object v8, v0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketPresenter;->flow:Lflow/Flow;

    iget-object v2, v0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketPresenter;->afterAction:Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;

    sget-object v3, Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;->PRINT_BILL:Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;

    if-ne v2, v3, :cond_1

    const/4 v13, 0x1

    goto :goto_0

    :cond_1
    const/4 v13, 0x0

    :goto_0
    move-object v9, p1

    move-object/from16 v10, p2

    move-object/from16 v11, p3

    move-object/from16 v12, p4

    invoke-virtual/range {v7 .. v13}, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->saveTransactionToNewTicket(Lflow/Flow;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/TicketGroup;Lcom/squareup/api/items/TicketTemplate;Z)V

    :goto_1
    return-void

    .line 439
    :cond_2
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "No handling setup for ticketAction "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, v0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketPresenter;->ticketAction:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 423
    :cond_3
    iget-object v2, v0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketPresenter;->afterAction:Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;

    sget-object v5, Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;->EXIT:Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;

    if-ne v2, v5, :cond_4

    const/4 v2, 0x1

    goto :goto_2

    :cond_4
    const/4 v2, 0x0

    :goto_2
    new-array v1, v1, [Ljava/lang/Object;

    iget-object v5, v0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketPresenter;->ticketAction:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    aput-object v5, v1, v4

    invoke-static {v2, v3, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 424
    iget-object v6, v0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketPresenter;->editTicketController:Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;

    iget-object v7, v0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketPresenter;->flow:Lflow/Flow;

    move-object v8, p1

    move-object/from16 v9, p2

    move-object/from16 v10, p3

    move-object/from16 v11, p4

    invoke-virtual/range {v6 .. v11}, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->editTransactionTicketAndExit(Lflow/Flow;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/TicketGroup;Lcom/squareup/api/items/TicketTemplate;)V

    return-void

    .line 417
    :cond_5
    iget-object v2, v0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketPresenter;->afterAction:Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;

    sget-object v5, Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;->EXIT:Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;

    if-ne v2, v5, :cond_6

    const/4 v2, 0x1

    goto :goto_3

    :cond_6
    const/4 v2, 0x0

    :goto_3
    new-array v1, v1, [Ljava/lang/Object;

    iget-object v5, v0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketPresenter;->ticketAction:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    aput-object v5, v1, v4

    invoke-static {v2, v3, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 418
    iget-object v6, v0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketPresenter;->editTicketController:Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;

    iget-object v7, v0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketPresenter;->flow:Lflow/Flow;

    move-object v8, p1

    move-object/from16 v9, p2

    move-object/from16 v10, p3

    move-object/from16 v11, p4

    invoke-virtual/range {v6 .. v11}, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->createNewEmptyTicketAndExit(Lflow/Flow;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/TicketGroup;Lcom/squareup/api/items/TicketTemplate;)V

    return-void
.end method

.method saveTicket(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/TicketGroup;Lcom/squareup/api/items/TicketTemplate;)V
    .locals 10

    .line 445
    iget-object v0, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketPresenter;->ticketAction:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    sget-object v1, Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;->EDIT_EXISTING_TICKET:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    .line 446
    iget-object v0, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketPresenter;->afterAction:Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;

    sget-object v1, Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;->EXIT:Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    new-array v1, v2, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketPresenter;->ticketAction:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    aput-object v2, v1, v3

    const-string v2, "Must use EXIT for ticketAction %s!"

    invoke-static {v0, v2, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 447
    iget-object v3, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketPresenter;->editTicketController:Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;

    iget-object v4, p0, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketPresenter;->flow:Lflow/Flow;

    move-object v5, p1

    move-object v6, p2

    move-object v7, p3

    move-object v8, p4

    move-object v9, p5

    invoke-virtual/range {v3 .. v9}, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;->editExistingTicketAndExit(Lflow/Flow;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/TicketGroup;Lcom/squareup/api/items/TicketTemplate;)V

    return-void
.end method
