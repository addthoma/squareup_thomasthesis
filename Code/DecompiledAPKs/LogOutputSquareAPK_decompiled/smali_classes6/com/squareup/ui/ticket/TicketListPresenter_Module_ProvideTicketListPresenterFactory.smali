.class public final Lcom/squareup/ui/ticket/TicketListPresenter_Module_ProvideTicketListPresenterFactory;
.super Ljava/lang/Object;
.source "TicketListPresenter_Module_ProvideTicketListPresenterFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/ticket/TicketListPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final cogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field private final configProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;",
            ">;"
        }
    .end annotation
.end field

.field private final currencyCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final openTicketsSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final passcodeGatekeeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;"
        }
    .end annotation
.end field

.field private final predefinedTicketsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/opentickets/PredefinedTickets;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketListListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketScopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketSelectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketSelection;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketSortSettingProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/tickets/TicketSort;",
            ">;>;"
        }
    .end annotation
.end field

.field private final ticketsLoaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketsLoader;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/opentickets/PredefinedTickets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketsLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketSelection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/tickets/TicketSort;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)V"
        }
    .end annotation

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketListPresenter_Module_ProvideTicketListPresenterFactory;->configProvider:Ljavax/inject/Provider;

    .line 69
    iput-object p2, p0, Lcom/squareup/ui/ticket/TicketListPresenter_Module_ProvideTicketListPresenterFactory;->predefinedTicketsProvider:Ljavax/inject/Provider;

    .line 70
    iput-object p3, p0, Lcom/squareup/ui/ticket/TicketListPresenter_Module_ProvideTicketListPresenterFactory;->ticketsLoaderProvider:Ljavax/inject/Provider;

    .line 71
    iput-object p4, p0, Lcom/squareup/ui/ticket/TicketListPresenter_Module_ProvideTicketListPresenterFactory;->ticketSelectionProvider:Ljavax/inject/Provider;

    .line 72
    iput-object p5, p0, Lcom/squareup/ui/ticket/TicketListPresenter_Module_ProvideTicketListPresenterFactory;->ticketScopeRunnerProvider:Ljavax/inject/Provider;

    .line 73
    iput-object p6, p0, Lcom/squareup/ui/ticket/TicketListPresenter_Module_ProvideTicketListPresenterFactory;->ticketSortSettingProvider:Ljavax/inject/Provider;

    .line 74
    iput-object p7, p0, Lcom/squareup/ui/ticket/TicketListPresenter_Module_ProvideTicketListPresenterFactory;->ticketListListenerProvider:Ljavax/inject/Provider;

    .line 75
    iput-object p8, p0, Lcom/squareup/ui/ticket/TicketListPresenter_Module_ProvideTicketListPresenterFactory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    .line 76
    iput-object p9, p0, Lcom/squareup/ui/ticket/TicketListPresenter_Module_ProvideTicketListPresenterFactory;->passcodeGatekeeperProvider:Ljavax/inject/Provider;

    .line 77
    iput-object p10, p0, Lcom/squareup/ui/ticket/TicketListPresenter_Module_ProvideTicketListPresenterFactory;->cogsProvider:Ljavax/inject/Provider;

    .line 78
    iput-object p11, p0, Lcom/squareup/ui/ticket/TicketListPresenter_Module_ProvideTicketListPresenterFactory;->deviceProvider:Ljavax/inject/Provider;

    .line 79
    iput-object p12, p0, Lcom/squareup/ui/ticket/TicketListPresenter_Module_ProvideTicketListPresenterFactory;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 80
    iput-object p13, p0, Lcom/squareup/ui/ticket/TicketListPresenter_Module_ProvideTicketListPresenterFactory;->currencyCodeProvider:Ljavax/inject/Provider;

    .line 81
    iput-object p14, p0, Lcom/squareup/ui/ticket/TicketListPresenter_Module_ProvideTicketListPresenterFactory;->resProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/ticket/TicketListPresenter_Module_ProvideTicketListPresenterFactory;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/opentickets/PredefinedTickets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketsLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketSelection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/tickets/TicketSort;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)",
            "Lcom/squareup/ui/ticket/TicketListPresenter_Module_ProvideTicketListPresenterFactory;"
        }
    .end annotation

    .line 101
    new-instance v15, Lcom/squareup/ui/ticket/TicketListPresenter_Module_ProvideTicketListPresenterFactory;

    move-object v0, v15

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    invoke-direct/range {v0 .. v14}, Lcom/squareup/ui/ticket/TicketListPresenter_Module_ProvideTicketListPresenterFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v15
.end method

.method public static provideTicketListPresenter(Ljava/lang/Object;Lcom/squareup/opentickets/PredefinedTickets;Lcom/squareup/ui/ticket/TicketsLoader;Lcom/squareup/ui/ticket/TicketSelection;Lcom/squareup/ui/ticket/TicketScopeRunner;Lcom/squareup/settings/LocalSetting;Ljava/lang/Object;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/cogs/Cogs;Lcom/squareup/util/Device;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/util/Res;)Lcom/squareup/ui/ticket/TicketListPresenter;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lcom/squareup/opentickets/PredefinedTickets;",
            "Lcom/squareup/ui/ticket/TicketsLoader;",
            "Lcom/squareup/ui/ticket/TicketSelection;",
            "Lcom/squareup/ui/ticket/TicketScopeRunner;",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/tickets/TicketSort;",
            ">;",
            "Ljava/lang/Object;",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            "Lcom/squareup/cogs/Cogs;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/util/Res;",
            ")",
            "Lcom/squareup/ui/ticket/TicketListPresenter;"
        }
    .end annotation

    .line 110
    move-object v0, p0

    check-cast v0, Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;

    move-object/from16 v6, p6

    check-cast v6, Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;

    move-object v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    invoke-static/range {v0 .. v13}, Lcom/squareup/ui/ticket/TicketListPresenter$Module;->provideTicketListPresenter(Lcom/squareup/ui/ticket/TicketListPresenter$Configuration;Lcom/squareup/opentickets/PredefinedTickets;Lcom/squareup/ui/ticket/TicketsLoader;Lcom/squareup/ui/ticket/TicketSelection;Lcom/squareup/ui/ticket/TicketScopeRunner;Lcom/squareup/settings/LocalSetting;Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/cogs/Cogs;Lcom/squareup/util/Device;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/util/Res;)Lcom/squareup/ui/ticket/TicketListPresenter;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/TicketListPresenter;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/ticket/TicketListPresenter;
    .locals 15

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter_Module_ProvideTicketListPresenterFactory;->configProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter_Module_ProvideTicketListPresenterFactory;->predefinedTicketsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/opentickets/PredefinedTickets;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter_Module_ProvideTicketListPresenterFactory;->ticketsLoaderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/ui/ticket/TicketsLoader;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter_Module_ProvideTicketListPresenterFactory;->ticketSelectionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/ui/ticket/TicketSelection;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter_Module_ProvideTicketListPresenterFactory;->ticketScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/ui/ticket/TicketScopeRunner;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter_Module_ProvideTicketListPresenterFactory;->ticketSortSettingProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/settings/LocalSetting;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter_Module_ProvideTicketListPresenterFactory;->ticketListListenerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v7

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter_Module_ProvideTicketListPresenterFactory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/tickets/OpenTicketsSettings;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter_Module_ProvideTicketListPresenterFactory;->passcodeGatekeeperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/permissions/PermissionGatekeeper;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter_Module_ProvideTicketListPresenterFactory;->cogsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/cogs/Cogs;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter_Module_ProvideTicketListPresenterFactory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/util/Device;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter_Module_ProvideTicketListPresenterFactory;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Lcom/squareup/text/Formatter;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter_Module_ProvideTicketListPresenterFactory;->currencyCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v13, v0

    check-cast v13, Lcom/squareup/protos/common/CurrencyCode;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter_Module_ProvideTicketListPresenterFactory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v14, v0

    check-cast v14, Lcom/squareup/util/Res;

    invoke-static/range {v1 .. v14}, Lcom/squareup/ui/ticket/TicketListPresenter_Module_ProvideTicketListPresenterFactory;->provideTicketListPresenter(Ljava/lang/Object;Lcom/squareup/opentickets/PredefinedTickets;Lcom/squareup/ui/ticket/TicketsLoader;Lcom/squareup/ui/ticket/TicketSelection;Lcom/squareup/ui/ticket/TicketScopeRunner;Lcom/squareup/settings/LocalSetting;Ljava/lang/Object;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/cogs/Cogs;Lcom/squareup/util/Device;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/util/Res;)Lcom/squareup/ui/ticket/TicketListPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketListPresenter_Module_ProvideTicketListPresenterFactory;->get()Lcom/squareup/ui/ticket/TicketListPresenter;

    move-result-object v0

    return-object v0
.end method
