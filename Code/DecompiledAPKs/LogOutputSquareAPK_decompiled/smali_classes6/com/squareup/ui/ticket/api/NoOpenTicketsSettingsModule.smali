.class public Lcom/squareup/ui/ticket/api/NoOpenTicketsSettingsModule;
.super Ljava/lang/Object;
.source "NoOpenTicketsSettingsModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static bindOpenTicketsSettings()Lcom/squareup/tickets/OpenTicketsSettings;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 11
    sget-object v0, Lcom/squareup/tickets/NoOpenTicketsSettings;->INSTANCE:Lcom/squareup/tickets/NoOpenTicketsSettings;

    return-object v0
.end method
