.class Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$GroupRowHolder;
.super Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$ClickableRowHolder;
.source "GroupListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GroupRowHolder"
.end annotation


# instance fields
.field private groupId:Ljava/lang/String;

.field private final presenter:Lcom/squareup/ui/ticket/GroupListPresenter;

.field final synthetic this$0:Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;Landroid/view/ViewGroup;Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;Lcom/squareup/ui/ticket/GroupListPresenter;)V
    .locals 0

    .line 188
    iput-object p1, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$GroupRowHolder;->this$0:Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;

    .line 189
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$ClickableRowHolder;-><init>(Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;Landroid/view/ViewGroup;Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;)V

    .line 190
    iput-object p4, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$GroupRowHolder;->presenter:Lcom/squareup/ui/ticket/GroupListPresenter;

    return-void
.end method


# virtual methods
.method protected bindRow(Lcom/squareup/ui/ticket/GroupListView$GroupRow;II)V
    .locals 0

    .line 194
    invoke-super {p0, p1, p2, p3}, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$ClickableRowHolder;->bindRow(Lcom/squareup/ui/ticket/GroupListView$GroupRow;II)V

    .line 195
    iget-object p1, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$GroupRowHolder;->this$0:Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;

    invoke-static {p1}, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;->access$200(Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;)Ljava/util/List;

    move-result-object p1

    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    .line 196
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getObjectId()Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$GroupRowHolder;->groupId:Ljava/lang/String;

    .line 197
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getName()Ljava/lang/String;

    move-result-object p2

    .line 198
    iget-object p3, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$GroupRowHolder;->presenter:Lcom/squareup/ui/ticket/GroupListPresenter;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getObjectId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p3, p1}, Lcom/squareup/ui/ticket/GroupListPresenter;->getTicketCountForGroup(Ljava/lang/String;)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p1

    .line 199
    iget-object p3, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$GroupRowHolder;->row:Lcom/squareup/ui/account/view/SmartLineRow;

    invoke-virtual {p3, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleText(Ljava/lang/CharSequence;)V

    .line 200
    iget-object p2, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$GroupRowHolder;->row:Lcom/squareup/ui/account/view/SmartLineRow;

    invoke-virtual {p2, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueText(Ljava/lang/CharSequence;)V

    .line 201
    iget-object p1, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$GroupRowHolder;->row:Lcom/squareup/ui/account/view/SmartLineRow;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueVisible(Z)V

    return-void
.end method

.method protected bridge synthetic bindRow(Ljava/lang/Enum;II)V
    .locals 0

    .line 181
    check-cast p1, Lcom/squareup/ui/ticket/GroupListView$GroupRow;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$GroupRowHolder;->bindRow(Lcom/squareup/ui/ticket/GroupListView$GroupRow;II)V

    return-void
.end method

.method public onRowClicked(Landroid/view/View;)V
    .locals 1

    .line 205
    iget-object p1, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$GroupRowHolder;->presenter:Lcom/squareup/ui/ticket/GroupListPresenter;

    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$GroupRowHolder;->groupId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/ticket/GroupListPresenter;->selectGroupSection(Ljava/lang/String;)V

    return-void
.end method
