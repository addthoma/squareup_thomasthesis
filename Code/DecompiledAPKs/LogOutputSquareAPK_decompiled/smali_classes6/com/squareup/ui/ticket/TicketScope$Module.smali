.class public Lcom/squareup/ui/ticket/TicketScope$Module;
.super Ljava/lang/Object;
.source "TicketScope.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/TicketScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Module"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideMoveTicketListener()Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 60
    new-instance v0, Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;

    invoke-direct {v0}, Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;-><init>()V

    return-object v0
.end method

.method static providePermittedToViewAllTicketsKey(Lcom/google/gson/Gson;)Lcom/squareup/BundleKey;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/squareup/BundleKey<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 65
    const-class v0, Ljava/lang/Boolean;

    const-string v1, "home-screen-permitted-to-view-all-tickets"

    invoke-static {p0, v1, v0}, Lcom/squareup/BundleKey;->json(Lcom/google/gson/Gson;Ljava/lang/String;Ljava/lang/Class;)Lcom/squareup/BundleKey;

    move-result-object p0

    return-object p0
.end method
