.class public Lcom/squareup/ui/ticket/MoveTicketScreen$ComponentFactory;
.super Ljava/lang/Object;
.source "MoveTicketScreen.java"

# interfaces
.implements Lcom/squareup/ui/WithComponent$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/MoveTicketScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ComponentFactory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 185
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Lmortar/MortarScope;Lcom/squareup/container/ContainerTreeKey;)Ljava/lang/Object;
    .locals 1

    .line 187
    const-class v0, Lcom/squareup/ui/ticket/TicketScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ticket/TicketScope$Component;

    .line 188
    check-cast p2, Lcom/squareup/ui/ticket/MoveTicketScreen;

    .line 189
    new-instance v0, Lcom/squareup/ui/ticket/MoveTicketScreen$Module;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v0, p2}, Lcom/squareup/ui/ticket/MoveTicketScreen$Module;-><init>(Lcom/squareup/ui/ticket/MoveTicketScreen;)V

    .line 190
    invoke-interface {p1, v0}, Lcom/squareup/ui/ticket/TicketScope$Component;->moveTicket(Lcom/squareup/ui/ticket/MoveTicketScreen$Module;)Lcom/squareup/ui/ticket/MoveTicketScreen$Component;

    move-result-object p1

    return-object p1
.end method
