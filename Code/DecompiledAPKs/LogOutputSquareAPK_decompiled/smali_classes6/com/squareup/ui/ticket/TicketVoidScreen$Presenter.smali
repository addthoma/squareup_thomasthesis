.class public Lcom/squareup/ui/ticket/TicketVoidScreen$Presenter;
.super Lcom/squareup/configure/item/VoidCompPresenter;
.source "TicketVoidScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/TicketVoidScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/configure/item/VoidCompPresenter<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

.field private final flow:Lflow/Flow;

.field private final openTicketsRunner:Lcom/squareup/ui/ticket/OpenTicketsRunner;

.field private screen:Lcom/squareup/ui/ticket/TicketVoidScreen;

.field private final transaction:Lcom/squareup/payment/Transaction;

.field private final voidController:Lcom/squareup/compvoidcontroller/VoidController;


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Res;Lflow/Flow;Lcom/squareup/payment/Transaction;Lcom/squareup/tickets/voidcomp/VoidReasonsCache;Lcom/squareup/compvoidcontroller/VoidController;Lcom/squareup/ui/ticket/OpenTicketsRunner;Lcom/squareup/permissions/EmployeeManagement;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 67
    invoke-direct {p0, p2, p5}, Lcom/squareup/configure/item/VoidCompPresenter;-><init>(Lcom/squareup/util/Res;Lcom/squareup/tickets/voidcomp/CogsCache;)V

    .line 68
    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketVoidScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 69
    iput-object p3, p0, Lcom/squareup/ui/ticket/TicketVoidScreen$Presenter;->flow:Lflow/Flow;

    .line 70
    iput-object p4, p0, Lcom/squareup/ui/ticket/TicketVoidScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 71
    iput-object p6, p0, Lcom/squareup/ui/ticket/TicketVoidScreen$Presenter;->voidController:Lcom/squareup/compvoidcontroller/VoidController;

    .line 72
    iput-object p7, p0, Lcom/squareup/ui/ticket/TicketVoidScreen$Presenter;->openTicketsRunner:Lcom/squareup/ui/ticket/OpenTicketsRunner;

    .line 73
    iput-object p8, p0, Lcom/squareup/ui/ticket/TicketVoidScreen$Presenter;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    return-void
.end method


# virtual methods
.method protected getCancelButtonTextResourceId()I
    .locals 1

    .line 101
    sget v0, Lcom/squareup/orderentry/R$string;->void_ticket:I

    return v0
.end method

.method protected getPrimaryButtonTextResourceId()I
    .locals 1

    .line 105
    sget v0, Lcom/squareup/configure/item/R$string;->void_initial:I

    return v0
.end method

.method protected getReasonHeaderTextResourceId()I
    .locals 1

    .line 109
    sget v0, Lcom/squareup/configure/item/R$string;->void_uppercase_reason:I

    return v0
.end method

.method public isVoidingTicket()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public synthetic lambda$null$0$TicketVoidScreen$Presenter(Lcom/squareup/util/Optional;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketVoidScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketVoidScreen$Presenter;->screen:Lcom/squareup/ui/ticket/TicketVoidScreen;

    invoke-static {v1}, Lcom/squareup/ui/ticket/TicketVoidScreen;->access$100(Lcom/squareup/ui/ticket/TicketVoidScreen;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/ticket/TicketVoidScreen$Presenter;->screen:Lcom/squareup/ui/ticket/TicketVoidScreen;

    invoke-static {v2}, Lcom/squareup/ui/ticket/TicketVoidScreen;->access$200(Lcom/squareup/ui/ticket/TicketVoidScreen;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/payment/Transaction;->setOpenTicketNameAndNote(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketVoidScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketVoidScreen$Presenter;->screen:Lcom/squareup/ui/ticket/TicketVoidScreen;

    invoke-static {v1}, Lcom/squareup/ui/ticket/TicketVoidScreen;->access$300(Lcom/squareup/ui/ticket/TicketVoidScreen;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Transaction;->setPredefinedTicket(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;)V

    .line 94
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketVoidScreen$Presenter;->voidController:Lcom/squareup/compvoidcontroller/VoidController;

    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketVoidScreen$Presenter;->getCheckedReason()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 95
    invoke-virtual {p1}, Lcom/squareup/util/Optional;->getValueOrNull()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/Employee;

    .line 94
    invoke-interface {v0, v1, p1}, Lcom/squareup/compvoidcontroller/VoidController;->voidTransactionTicket(Ljava/lang/String;Lcom/squareup/protos/client/Employee;)V

    .line 96
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketVoidScreen$Presenter;->openTicketsRunner:Lcom/squareup/ui/ticket/OpenTicketsRunner;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketVoidScreen$Presenter;->flow:Lflow/Flow;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/ticket/OpenTicketsRunner;->finishVoidTicketFromCartMenu(Lflow/Flow;)V

    return-void
.end method

.method public synthetic lambda$onPrimaryClicked$1$TicketVoidScreen$Presenter()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 88
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketVoidScreen$Presenter;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketVoidScreen$Presenter;->screen:Lcom/squareup/ui/ticket/TicketVoidScreen;

    invoke-static {v1}, Lcom/squareup/ui/ticket/TicketVoidScreen;->access$000(Lcom/squareup/ui/ticket/TicketVoidScreen;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/permissions/EmployeeManagement;->oneEmployeeProtoByToken(Ljava/lang/String;)Lio/reactivex/Maybe;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/ticket/-$$Lambda$Dy0PHuYjqCY7wofLHWfV9mlLNt0;->INSTANCE:Lcom/squareup/ui/ticket/-$$Lambda$Dy0PHuYjqCY7wofLHWfV9mlLNt0;

    .line 89
    invoke-virtual {v0, v1}, Lio/reactivex/Maybe;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Maybe;

    move-result-object v0

    .line 90
    invoke-static {}, Lcom/squareup/util/Optional;->empty()Lcom/squareup/util/Optional;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Maybe;->toSingle(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$TicketVoidScreen$Presenter$FcyQYer7Rq1u18Pf-Ia0_RCJQyw;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/-$$Lambda$TicketVoidScreen$Presenter$FcyQYer7Rq1u18Pf-Ia0_RCJQyw;-><init>(Lcom/squareup/ui/ticket/TicketVoidScreen$Presenter;)V

    .line 91
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public onCancelClicked()V
    .locals 2

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketVoidScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->VOID_CART_CANCELED:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketVoidScreen$Presenter;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    .line 77
    invoke-super {p0, p1}, Lcom/squareup/configure/item/VoidCompPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 78
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ticket/TicketVoidScreen;

    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketVoidScreen$Presenter;->screen:Lcom/squareup/ui/ticket/TicketVoidScreen;

    return-void
.end method

.method public onPrimaryClicked()V
    .locals 2

    .line 87
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketVoidScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$TicketVoidScreen$Presenter$IchANMVsgTFXtLMJLk1LmJaAUvI;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/-$$Lambda$TicketVoidScreen$Presenter$IchANMVsgTFXtLMJLk1LmJaAUvI;-><init>(Lcom/squareup/ui/ticket/TicketVoidScreen$Presenter;)V

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
