.class public Lcom/squareup/ui/ticket/MasterDetailTicketView;
.super Landroid/widget/LinearLayout;
.source "MasterDetailTicketView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;
.implements Lcom/squareup/ui/HasActionBar;


# instance fields
.field private actionBar:Landroid/view/View;

.field private actionBarTitle:Landroid/widget/TextView;

.field private bulkDeleteButton:Landroid/widget/TextView;

.field private bulkMergeButton:Landroid/widget/TextView;

.field private bulkMoveButton:Landroid/widget/TextView;

.field private bulkReprintTicketButton:Landroid/widget/TextView;

.field private bulkTransferButton:Landroid/widget/TextView;

.field private bulkVoidButton:Landroid/widget/TextView;

.field private cancelButton:Landroid/view/View;

.field device:Lcom/squareup/util/Device;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private dismissEditButton:Landroid/view/View;

.field private editBar:Landroid/view/View;

.field private editBarTitle:Landroid/view/View;

.field private editButton:Landroid/view/View;

.field private final fadeDuration:I

.field private final fadeDurationLong:I

.field private final fadeDurationShort:I

.field private fadeOutInProgress:Z

.field private groupListView:Lcom/squareup/ui/ticket/GroupListView;

.field private mergeTransactionTicketButton:Landroid/view/View;

.field private newTicketButton:Landroid/view/View;

.field private phoneSortMenuContainer:Lcom/squareup/ui/DropDownContainer;

.field private phoneSortMenuDropDown:Lcom/squareup/ui/ticket/TicketSortGroup;

.field presenter:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private ticketListView:Lcom/squareup/ui/ticket/TicketListView;

.field private upButton:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .line 90
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 92
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const/high16 v0, 0x10e0000

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p2

    iput p2, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->fadeDuration:I

    .line 93
    iget p2, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->fadeDuration:I

    int-to-double v0, p2

    const-wide v2, 0x3ff199999999999aL    # 1.1

    div-double/2addr v0, v2

    double-to-int v0, v0

    iput v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->fadeDurationShort:I

    mul-int/lit8 p2, p2, 0x2

    .line 94
    iput p2, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->fadeDurationLong:I

    .line 95
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->isInEditMode()Z

    move-result p2

    if-eqz p2, :cond_0

    return-void

    .line 96
    :cond_0
    const-class p2, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$Component;->inject(Lcom/squareup/ui/ticket/MasterDetailTicketView;)V

    return-void
.end method

.method private bindPhoneViews()V
    .locals 1

    .line 478
    sget v0, Lcom/squareup/orderentry/R$id;->master_detail_ticket_view_sort_type_menu_container:I

    .line 479
    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/DropDownContainer;

    iput-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->phoneSortMenuContainer:Lcom/squareup/ui/DropDownContainer;

    .line 480
    sget v0, Lcom/squareup/orderentry/R$id;->master_detail_ticket_view_sort_type_menu_drop_down:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/TicketSortGroup;

    iput-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->phoneSortMenuDropDown:Lcom/squareup/ui/ticket/TicketSortGroup;

    .line 481
    sget v0, Lcom/squareup/orderentry/R$id;->ticket_list_view:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/TicketListView;

    iput-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->ticketListView:Lcom/squareup/ui/ticket/TicketListView;

    return-void
.end method

.method private bindTabletViews()V
    .locals 1

    .line 485
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->actionBar:Landroid/view/View;

    .line 486
    sget v0, Lcom/squareup/orderentry/R$id;->master_detail_ticket_view_action_bar_title:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->actionBarTitle:Landroid/widget/TextView;

    .line 487
    sget v0, Lcom/squareup/orderentry/R$id;->master_detail_ticket_view_action_bar_up_button_glyph:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->upButton:Landroid/view/View;

    .line 488
    sget v0, Lcom/squareup/orderentry/R$id;->master_detail_ticket_view_action_bar_edit_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->editButton:Landroid/view/View;

    .line 489
    sget v0, Lcom/squareup/orderentry/R$id;->master_detail_ticket_view_action_bar_new_ticket_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->newTicketButton:Landroid/view/View;

    .line 490
    sget v0, Lcom/squareup/orderentry/R$id;->master_detail_ticket_view_action_bar_merge_transaction_ticket_button:I

    .line 491
    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->mergeTransactionTicketButton:Landroid/view/View;

    .line 493
    sget v0, Lcom/squareup/orderentry/R$id;->master_detail_ticket_view_edit_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->editBar:Landroid/view/View;

    .line 494
    sget v0, Lcom/squareup/orderentry/R$id;->master_detail_ticket_view_edit_bar_title:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->editBarTitle:Landroid/view/View;

    .line 495
    sget v0, Lcom/squareup/orderentry/R$id;->master_detail_ticket_view_edit_bar_bulk_delete_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->bulkDeleteButton:Landroid/widget/TextView;

    .line 496
    sget v0, Lcom/squareup/orderentry/R$id;->master_detail_ticket_view_edit_bar_bulk_reprint_ticket_button:I

    .line 497
    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->bulkReprintTicketButton:Landroid/widget/TextView;

    .line 498
    sget v0, Lcom/squareup/orderentry/R$id;->master_detail_ticket_view_edit_bar_bulk_void_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->bulkVoidButton:Landroid/widget/TextView;

    .line 499
    sget v0, Lcom/squareup/orderentry/R$id;->master_detail_ticket_view_edit_bar_bulk_merge_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->bulkMergeButton:Landroid/widget/TextView;

    .line 500
    sget v0, Lcom/squareup/orderentry/R$id;->master_detail_ticket_view_edit_bar_bulk_move_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->bulkMoveButton:Landroid/widget/TextView;

    .line 501
    sget v0, Lcom/squareup/orderentry/R$id;->master_detail_ticket_view_edit_bar_bulk_transfer_button:I

    .line 502
    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->bulkTransferButton:Landroid/widget/TextView;

    .line 503
    sget v0, Lcom/squareup/orderentry/R$id;->master_detail_ticket_view_edit_bar_cancel_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->cancelButton:Landroid/view/View;

    .line 504
    sget v0, Lcom/squareup/orderentry/R$id;->master_detail_ticket_view_edit_bar_dismiss_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->dismissEditButton:Landroid/view/View;

    .line 506
    sget v0, Lcom/squareup/orderentry/R$id;->master_group_list_view:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/GroupListView;

    iput-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->groupListView:Lcom/squareup/ui/ticket/GroupListView;

    .line 507
    sget v0, Lcom/squareup/orderentry/R$id;->ticket_list_view:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/TicketListView;

    iput-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->ticketListView:Lcom/squareup/ui/ticket/TicketListView;

    return-void
.end method

.method private buildPhoneActionBar()V
    .locals 5

    .line 529
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    .line 530
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v3, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->presenter:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;

    .line 531
    invoke-virtual {v3}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->getActionBarTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketView$mJZQjVIQCIibcPFVDU01F3wX81w;

    invoke-direct {v2, p0}, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketView$mJZQjVIQCIibcPFVDU01F3wX81w;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketView;)V

    .line 532
    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SORT:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 534
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/squareup/crmviewcustomer/R$string;->open_menu:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 533
    invoke-virtual {v1, v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setSecondaryButtonGlyphNoText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/ticket/-$$Lambda$f81xAjYJ8jqe6CuD6HZREvtGMbk;

    invoke-direct {v2, p0}, Lcom/squareup/ui/ticket/-$$Lambda$f81xAjYJ8jqe6CuD6HZREvtGMbk;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketView;)V

    .line 535
    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showSecondaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 536
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method private buildTabletActionBar(Z)V
    .locals 2

    if-eqz p1, :cond_0

    .line 541
    iget-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->actionBarTitle:Landroid/widget/TextView;

    iget v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->fadeDuration:I

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketView$zE9TSsjlA11xDf7YnguDvnqlsQM;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketView$zE9TSsjlA11xDf7YnguDvnqlsQM;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketView;)V

    invoke-static {p1, v0, v1}, Lcom/squareup/util/Views;->fadeOut(Landroid/view/View;ILkotlin/jvm/functions/Function1;)V

    goto :goto_0

    .line 548
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->presenter:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->getActionBarTitle()Ljava/lang/String;

    move-result-object p1

    .line 549
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->actionBarTitle:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void
.end method

.method private restoreOpacity(Landroid/view/View;)V
    .locals 2

    .line 523
    invoke-virtual {p1}, Landroid/view/View;->getAlpha()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    .line 524
    invoke-virtual {p1, v0}, Landroid/view/View;->setAlpha(F)V

    :cond_0
    return-void
.end method

.method private setViewAndChildrenClickable(Landroid/view/View;Z)V
    .locals 2

    .line 511
    invoke-virtual {p1, p2}, Landroid/view/View;->setClickable(Z)V

    .line 512
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 513
    check-cast p1, Landroid/view/ViewGroup;

    const/4 v0, 0x0

    .line 515
    :goto_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 516
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 517
    invoke-direct {p0, v1, p2}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->setViewAndChildrenClickable(Landroid/view/View;Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method clearSearchBar()V
    .locals 1

    .line 470
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->ticketListView:Lcom/squareup/ui/ticket/TicketListView;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketListView;->clearSearchBar()V

    return-void
.end method

.method fadeInLogOutButton()V
    .locals 1

    .line 404
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->groupListView:Lcom/squareup/ui/ticket/GroupListView;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/GroupListView;->fadeInLogOutButton()V

    return-void
.end method

.method public getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 198
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhone()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 199
    invoke-static {p0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method getSelectedGroup()Lcom/squareup/api/items/TicketGroup;
    .locals 1

    .line 395
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->presenter:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->isTabletAndPredefinedTicketsEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->groupListView:Lcom/squareup/ui/ticket/GroupListView;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/GroupListView;->getSelectedGroup()Lcom/squareup/api/items/TicketGroup;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method getSelectedSection()Ljava/lang/String;
    .locals 1

    .line 390
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->presenter:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->isTabletAndPredefinedTicketsEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->groupListView:Lcom/squareup/ui/ticket/GroupListView;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/GroupListView;->getSelectedSection()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const-string v0, "group-list-presenter-selected-section-all-tickets"

    :goto_0
    return-object v0
.end method

.method hideGroupListContainer()V
    .locals 2

    .line 386
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->groupListView:Lcom/squareup/ui/ticket/GroupListView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public synthetic lambda$buildPhoneActionBar$2$MasterDetailTicketView()V
    .locals 1

    .line 532
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->presenter:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->exit()V

    return-void
.end method

.method public synthetic lambda$buildTabletActionBar$3$MasterDetailTicketView(Landroid/view/View;)Lkotlin/Unit;
    .locals 1

    .line 542
    iget-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->presenter:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->getActionBarTitle()Ljava/lang/String;

    move-result-object p1

    .line 543
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->actionBarTitle:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 544
    iget-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->actionBarTitle:Landroid/widget/TextView;

    iget v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->fadeDuration:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    .line 545
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public synthetic lambda$onAttachedToWindow$0$MasterDetailTicketView(Lcom/squareup/tickets/TicketSort;)V
    .locals 1

    .line 114
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->presenter:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->onSortChange(Lcom/squareup/tickets/TicketSort;)V

    .line 115
    iget-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->phoneSortMenuContainer:Lcom/squareup/ui/DropDownContainer;

    invoke-virtual {p1}, Lcom/squareup/ui/DropDownContainer;->closeDropDown()V

    return-void
.end method

.method public synthetic lambda$setEditButtonVisible$1$MasterDetailTicketView(Landroid/view/View;)Lkotlin/Unit;
    .locals 1

    .line 259
    iget-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->editButton:Landroid/view/View;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 260
    iget-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->editButton:Landroid/view/View;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->setEnabled(Z)V

    const/4 p1, 0x0

    .line 261
    iput-boolean p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->fadeOutInProgress:Z

    .line 262
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .line 100
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 102
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 104
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhone()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 105
    invoke-direct {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->bindPhoneViews()V

    .line 107
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->presenter:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->takeView(Ljava/lang/Object;)V

    .line 110
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->presenter:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->getSanitizedSortType()Lcom/squareup/tickets/TicketSort;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->setPhoneDropDownSortType(Lcom/squareup/tickets/TicketSort;)V

    .line 113
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->phoneSortMenuDropDown:Lcom/squareup/ui/ticket/TicketSortGroup;

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketView$PM8E25pkpVx8_rE9VWr3Ii-YZdQ;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketView$PM8E25pkpVx8_rE9VWr3Ii-YZdQ;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ticket/TicketSortGroup;->setOnSortChangeListener(Lcom/squareup/ui/ticket/TicketSortGroup$OnSortChangeListener;)V

    goto/16 :goto_0

    .line 118
    :cond_1
    invoke-direct {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->bindTabletViews()V

    .line 120
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->upButton:Landroid/view/View;

    new-instance v1, Lcom/squareup/ui/ticket/MasterDetailTicketView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/MasterDetailTicketView$1;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 125
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->editButton:Landroid/view/View;

    new-instance v1, Lcom/squareup/ui/ticket/MasterDetailTicketView$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/MasterDetailTicketView$2;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 130
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->newTicketButton:Landroid/view/View;

    new-instance v1, Lcom/squareup/ui/ticket/MasterDetailTicketView$3;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/MasterDetailTicketView$3;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 135
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->mergeTransactionTicketButton:Landroid/view/View;

    new-instance v1, Lcom/squareup/ui/ticket/MasterDetailTicketView$4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/MasterDetailTicketView$4;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 140
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->bulkDeleteButton:Landroid/widget/TextView;

    new-instance v1, Lcom/squareup/ui/ticket/MasterDetailTicketView$5;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/MasterDetailTicketView$5;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 145
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->bulkReprintTicketButton:Landroid/widget/TextView;

    new-instance v1, Lcom/squareup/ui/ticket/MasterDetailTicketView$6;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/MasterDetailTicketView$6;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 150
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->bulkVoidButton:Landroid/widget/TextView;

    new-instance v1, Lcom/squareup/ui/ticket/MasterDetailTicketView$7;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/MasterDetailTicketView$7;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 155
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->bulkMergeButton:Landroid/widget/TextView;

    new-instance v1, Lcom/squareup/ui/ticket/MasterDetailTicketView$8;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/MasterDetailTicketView$8;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 160
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->bulkMoveButton:Landroid/widget/TextView;

    new-instance v1, Lcom/squareup/ui/ticket/MasterDetailTicketView$9;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/MasterDetailTicketView$9;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 165
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->bulkTransferButton:Landroid/widget/TextView;

    new-instance v1, Lcom/squareup/ui/ticket/MasterDetailTicketView$10;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/MasterDetailTicketView$10;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 170
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->cancelButton:Landroid/view/View;

    new-instance v1, Lcom/squareup/ui/ticket/MasterDetailTicketView$11;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/MasterDetailTicketView$11;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 175
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->dismissEditButton:Landroid/view/View;

    new-instance v1, Lcom/squareup/ui/ticket/MasterDetailTicketView$12;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/MasterDetailTicketView$12;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 181
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->presenter:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->takeView(Ljava/lang/Object;)V

    :goto_0
    const/4 v0, 0x0

    .line 185
    invoke-virtual {p0, v0}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->updateActionBarTitle(Z)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 189
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->presenter:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 193
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->presenter:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->dropView(Ljava/lang/Object;)V

    .line 194
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method setActionBarVisible(Z)V
    .locals 1

    if-eqz p1, :cond_0

    .line 218
    iget-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->actionBar:Landroid/view/View;

    iget v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->fadeDurationLong:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    goto :goto_0

    .line 220
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->actionBar:Landroid/view/View;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    :goto_0
    return-void
.end method

.method setBulkDeleteButtonEnabled(Z)V
    .locals 1

    .line 336
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->bulkDeleteButton:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void
.end method

.method setBulkDeleteButtonVisible(Z)V
    .locals 2

    if-eqz p1, :cond_1

    .line 330
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->bulkVoidButton:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    const-string v1, "Cannot show Delete Ticket and Void Ticket buttons at the same time!"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 332
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->bulkDeleteButton:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method setBulkMergeButtonEnabled(Z)V
    .locals 1

    .line 364
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->bulkMergeButton:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void
.end method

.method setBulkMergeButtonVisible(Z)V
    .locals 2

    if-eqz p1, :cond_1

    .line 358
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->bulkMoveButton:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    const-string v1, "Cannot show Merge Ticket and Move Ticket buttons at the same time!"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 360
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->bulkMergeButton:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method setBulkMoveButtonEnabled(Z)V
    .locals 1

    .line 374
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->bulkMoveButton:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void
.end method

.method setBulkMoveButtonVisible(Z)V
    .locals 2

    if-eqz p1, :cond_1

    .line 368
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->bulkMergeButton:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    const-string v1, "Cannot show Move Ticket and Merge Ticket buttons at the same time!"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 370
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->bulkMoveButton:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method setBulkReprintTicketButtonEnabled(Z)V
    .locals 1

    .line 344
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->bulkReprintTicketButton:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void
.end method

.method setBulkReprintTicketButtonVisible(Z)V
    .locals 1

    .line 340
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->bulkReprintTicketButton:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method setBulkTransferButtonEnabled(Z)V
    .locals 1

    .line 382
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->bulkTransferButton:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void
.end method

.method setBulkTransferButtonVisible(Z)V
    .locals 1

    .line 378
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->bulkTransferButton:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method setBulkVoidButtonEnabled(Z)V
    .locals 1

    .line 354
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->bulkVoidButton:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void
.end method

.method setBulkVoidButtonVisible(Z)V
    .locals 2

    if-eqz p1, :cond_1

    .line 348
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->bulkDeleteButton:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    const-string v1, "Cannot show Delete Ticket and Void Ticket buttons at the same time!"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 350
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->bulkVoidButton:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method setEditBarAlwaysVisible(Z)V
    .locals 2

    .line 290
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->actionBar:Landroid/view/View;

    xor-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 291
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->editBar:Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 292
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->editBarTitle:Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 293
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->dismissEditButton:Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 294
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->cancelButton:Landroid/view/View;

    xor-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    if-eqz p1, :cond_0

    .line 296
    sget v0, Lcom/squareup/marin/R$drawable;->marin_white_border_bottom_light_gray_1px:I

    goto :goto_0

    :cond_0
    sget v0, Lcom/squareup/marin/R$drawable;->marin_blue:I

    .line 299
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->editBar:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    if-eqz p1, :cond_1

    .line 301
    sget v0, Lcom/squareup/marin/R$color;->marin_text_selector_blue_disabled_light_gray:I

    goto :goto_1

    :cond_1
    sget v0, Lcom/squareup/marin/R$color;->marin_text_selector_white_disabled_white_translucent:I

    .line 304
    :goto_1
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Landroidx/core/content/ContextCompat;->getColorStateList(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v0

    if-eqz p1, :cond_2

    .line 306
    sget p1, Lcom/squareup/marin/R$drawable;->marin_selector_ultra_light_gray_pressed:I

    goto :goto_2

    :cond_2
    sget p1, Lcom/squareup/marin/R$drawable;->marin_selector_blue_pressed:I

    .line 310
    :goto_2
    iget-object v1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->bulkDeleteButton:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 311
    iget-object v1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->bulkDeleteButton:Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 313
    iget-object v1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->bulkReprintTicketButton:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 314
    iget-object v1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->bulkReprintTicketButton:Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 316
    iget-object v1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->bulkVoidButton:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 317
    iget-object v1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->bulkVoidButton:Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 319
    iget-object v1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->bulkMergeButton:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 320
    iget-object v1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->bulkMergeButton:Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 322
    iget-object v1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->bulkMoveButton:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 323
    iget-object v1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->bulkMoveButton:Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 325
    iget-object v1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->bulkTransferButton:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 326
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->bulkTransferButton:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    return-void
.end method

.method setEditBarVisible(Z)V
    .locals 1

    if-eqz p1, :cond_0

    .line 283
    iget-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->editBar:Landroid/view/View;

    iget v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->fadeDurationShort:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    goto :goto_0

    .line 285
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->editBar:Landroid/view/View;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    :goto_0
    return-void
.end method

.method setEditButtonVisible(Z)V
    .locals 1

    const/4 v0, 0x0

    .line 245
    invoke-virtual {p0, p1, v0}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->setEditButtonVisible(ZZ)V

    return-void
.end method

.method setEditButtonVisible(ZZ)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-eqz p2, :cond_1

    if-eqz p1, :cond_0

    .line 252
    iget-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->editButton:Landroid/view/View;

    iget p2, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->fadeDuration:I

    invoke-static {p1, p2, p2}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;II)V

    return-void

    .line 256
    :cond_0
    iput-boolean v1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->fadeOutInProgress:Z

    .line 257
    iget-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->editButton:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 258
    iget-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->editButton:Landroid/view/View;

    iget p2, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->fadeDuration:I

    iget v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->fadeDurationLong:I

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketView$zcVhxzIoOM3_WX0oTejl29pPfYU;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/-$$Lambda$MasterDetailTicketView$zcVhxzIoOM3_WX0oTejl29pPfYU;-><init>(Lcom/squareup/ui/ticket/MasterDetailTicketView;)V

    invoke-static {p1, p2, v0, v1}, Lcom/squareup/util/Views;->fadeOut(Landroid/view/View;IILkotlin/jvm/functions/Function1;)V

    return-void

    :cond_1
    if-eqz p1, :cond_2

    .line 270
    iget-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->editButton:Landroid/view/View;

    invoke-direct {p0, p1}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->restoreOpacity(Landroid/view/View;)V

    .line 271
    iget-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->editButton:Landroid/view/View;

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void

    .line 275
    :cond_2
    iget-boolean p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->fadeOutInProgress:Z

    if-eqz p1, :cond_3

    return-void

    .line 278
    :cond_3
    iget-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->editButton:Landroid/view/View;

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method setLogOutButtonVisible(Z)V
    .locals 1

    .line 400
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->groupListView:Lcom/squareup/ui/ticket/GroupListView;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/GroupListView;->setLogOutButtonVisible(Z)V

    return-void
.end method

.method setMergeTransactionTicketButtonEnabled(Z)V
    .locals 1

    .line 241
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->mergeTransactionTicketButton:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    return-void
.end method

.method setMergeTransactionTicketButtonVisible(Z)V
    .locals 1

    .line 237
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->mergeTransactionTicketButton:Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method setNewTicketButtonVisible(Z)V
    .locals 1

    .line 233
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->newTicketButton:Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method setPhoneDropDownSortType(Lcom/squareup/tickets/TicketSort;)V
    .locals 1

    .line 209
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->phoneSortMenuDropDown:Lcom/squareup/ui/ticket/TicketSortGroup;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/TicketSortGroup;->setSortType(Lcom/squareup/tickets/TicketSort;)V

    return-void
.end method

.method setPhoneEmployeesFilterVisible(Z)V
    .locals 1

    .line 213
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->phoneSortMenuDropDown:Lcom/squareup/ui/ticket/TicketSortGroup;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/TicketSortGroup;->setEmployeeSortVisible(Z)V

    return-void
.end method

.method setTicketListClickable(Z)V
    .locals 1

    .line 408
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->ticketListView:Lcom/squareup/ui/ticket/TicketListView;

    invoke-direct {p0, v0, p1}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->setViewAndChildrenClickable(Landroid/view/View;Z)V

    return-void
.end method

.method showDetailOnlyTicketList(Z)V
    .locals 2

    .line 415
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/orderentry/R$string;->open_tickets_search_tickets:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 416
    iget-object v1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->ticketListView:Lcom/squareup/ui/ticket/TicketListView;

    invoke-virtual {v1, v0}, Lcom/squareup/ui/ticket/TicketListView;->setSearchBarHint(Ljava/lang/String;)V

    if-eqz p1, :cond_0

    .line 418
    iget-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->ticketListView:Lcom/squareup/ui/ticket/TicketListView;

    invoke-virtual {p1}, Lcom/squareup/ui/ticket/TicketListView;->showSearchBarWithFade()V

    .line 419
    iget-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->ticketListView:Lcom/squareup/ui/ticket/TicketListView;

    invoke-virtual {p1}, Lcom/squareup/ui/ticket/TicketListView;->showListWithFade()V

    goto :goto_0

    .line 421
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->ticketListView:Lcom/squareup/ui/ticket/TicketListView;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/squareup/ui/ticket/TicketListView;->setSearchBarVisible(Z)V

    .line 422
    iget-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->ticketListView:Lcom/squareup/ui/ticket/TicketListView;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/ticket/TicketListView;->setListVisible(Z)V

    :goto_0
    return-void
.end method

.method showMasterDetailSearchView(Z)V
    .locals 1

    const/4 v0, 0x0

    .line 446
    invoke-virtual {p0, p1, v0}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->showMasterDetailSearchView(ZZ)V

    return-void
.end method

.method showMasterDetailSearchView(ZZ)V
    .locals 2

    .line 450
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/orderentry/R$string;->predefined_tickets_search_all_tickets:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 451
    iget-object v1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->ticketListView:Lcom/squareup/ui/ticket/TicketListView;

    invoke-virtual {v1, v0}, Lcom/squareup/ui/ticket/TicketListView;->setSearchBarHint(Ljava/lang/String;)V

    if-eqz p2, :cond_1

    .line 455
    iget-object p2, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->ticketListView:Lcom/squareup/ui/ticket/TicketListView;

    invoke-virtual {p2}, Lcom/squareup/ui/ticket/TicketListView;->showSearchBarWithFade()V

    if-eqz p1, :cond_0

    .line 457
    iget-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->ticketListView:Lcom/squareup/ui/ticket/TicketListView;

    invoke-virtual {p1}, Lcom/squareup/ui/ticket/TicketListView;->showListWithFade()V

    goto :goto_0

    .line 459
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->ticketListView:Lcom/squareup/ui/ticket/TicketListView;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lcom/squareup/ui/ticket/TicketListView;->setListVisible(Z)V

    :goto_0
    return-void

    .line 465
    :cond_1
    iget-object p2, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->ticketListView:Lcom/squareup/ui/ticket/TicketListView;

    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lcom/squareup/ui/ticket/TicketListView;->setSearchBarVisible(Z)V

    .line 466
    iget-object p2, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->ticketListView:Lcom/squareup/ui/ticket/TicketListView;

    invoke-virtual {p2, p1}, Lcom/squareup/ui/ticket/TicketListView;->setListVisible(Z)V

    return-void
.end method

.method showMasterDetailTicketList(Z)V
    .locals 2

    .line 433
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->ticketListView:Lcom/squareup/ui/ticket/TicketListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ticket/TicketListView;->setSearchBarVisible(Z)V

    if-eqz p1, :cond_0

    .line 435
    iget-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->ticketListView:Lcom/squareup/ui/ticket/TicketListView;

    invoke-virtual {p1}, Lcom/squareup/ui/ticket/TicketListView;->showListWithFade()V

    goto :goto_0

    .line 437
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->ticketListView:Lcom/squareup/ui/ticket/TicketListView;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/squareup/ui/ticket/TicketListView;->setListVisible(Z)V

    :goto_0
    return-void
.end method

.method showNoTicketsMessage(Ljava/lang/String;ZZ)V
    .locals 1

    .line 474
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->ticketListView:Lcom/squareup/ui/ticket/TicketListView;

    invoke-virtual {v0, p1, p2, p3}, Lcom/squareup/ui/ticket/TicketListView;->showNoTicketsMessage(Ljava/lang/String;ZZ)V

    return-void
.end method

.method togglePhoneDropDown()V
    .locals 1

    .line 205
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->phoneSortMenuContainer:Lcom/squareup/ui/DropDownContainer;

    invoke-virtual {v0}, Lcom/squareup/ui/DropDownContainer;->toggleDropDown()V

    return-void
.end method

.method updateActionBarTitle(Z)V
    .locals 1

    .line 225
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketView;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhone()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 226
    invoke-direct {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->buildPhoneActionBar()V

    goto :goto_0

    .line 228
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/ui/ticket/MasterDetailTicketView;->buildTabletActionBar(Z)V

    :goto_0
    return-void
.end method
