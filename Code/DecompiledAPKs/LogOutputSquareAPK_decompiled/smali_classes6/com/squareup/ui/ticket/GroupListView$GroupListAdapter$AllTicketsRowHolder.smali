.class Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$AllTicketsRowHolder;
.super Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$ClickableRowHolder;
.source "GroupListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AllTicketsRowHolder"
.end annotation


# instance fields
.field private final presenter:Lcom/squareup/ui/ticket/GroupListPresenter;

.field private final res:Landroid/content/res/Resources;

.field final synthetic this$0:Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;Landroid/view/ViewGroup;Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;Lcom/squareup/ui/ticket/GroupListPresenter;)V
    .locals 0

    .line 135
    iput-object p1, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$AllTicketsRowHolder;->this$0:Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;

    .line 136
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$ClickableRowHolder;-><init>(Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;Landroid/view/ViewGroup;Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;)V

    .line 137
    iput-object p4, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$AllTicketsRowHolder;->presenter:Lcom/squareup/ui/ticket/GroupListPresenter;

    .line 138
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$AllTicketsRowHolder;->res:Landroid/content/res/Resources;

    return-void
.end method


# virtual methods
.method protected bindRow(Lcom/squareup/ui/ticket/GroupListView$GroupRow;II)V
    .locals 0

    .line 142
    invoke-super {p0, p1, p2, p3}, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$ClickableRowHolder;->bindRow(Lcom/squareup/ui/ticket/GroupListView$GroupRow;II)V

    .line 143
    iget-object p1, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$AllTicketsRowHolder;->res:Landroid/content/res/Resources;

    sget p2, Lcom/squareup/orderentry/R$string;->predefined_tickets_all_tickets:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 144
    iget-object p2, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$AllTicketsRowHolder;->presenter:Lcom/squareup/ui/ticket/GroupListPresenter;

    invoke-virtual {p2}, Lcom/squareup/ui/ticket/GroupListPresenter;->getAllTicketsCount()I

    move-result p2

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p2

    .line 145
    iget-object p3, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$AllTicketsRowHolder;->row:Lcom/squareup/ui/account/view/SmartLineRow;

    invoke-virtual {p3, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleText(Ljava/lang/CharSequence;)V

    .line 146
    iget-object p1, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$AllTicketsRowHolder;->row:Lcom/squareup/ui/account/view/SmartLineRow;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueText(Ljava/lang/CharSequence;)V

    .line 147
    iget-object p1, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$AllTicketsRowHolder;->row:Lcom/squareup/ui/account/view/SmartLineRow;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueVisible(Z)V

    return-void
.end method

.method protected bridge synthetic bindRow(Ljava/lang/Enum;II)V
    .locals 0

    .line 130
    check-cast p1, Lcom/squareup/ui/ticket/GroupListView$GroupRow;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$AllTicketsRowHolder;->bindRow(Lcom/squareup/ui/ticket/GroupListView$GroupRow;II)V

    return-void
.end method

.method public onRowClicked(Landroid/view/View;)V
    .locals 0

    .line 151
    iget-object p1, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$AllTicketsRowHolder;->presenter:Lcom/squareup/ui/ticket/GroupListPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/ticket/GroupListPresenter;->selectAllTicketsSection()V

    return-void
.end method
