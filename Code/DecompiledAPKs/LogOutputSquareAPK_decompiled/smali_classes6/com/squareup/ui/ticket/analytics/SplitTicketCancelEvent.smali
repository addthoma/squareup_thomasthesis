.class public Lcom/squareup/ui/ticket/analytics/SplitTicketCancelEvent;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "SplitTicketCancelEvent.java"


# instance fields
.field public final not_saved:I

.field public final saved:I


# direct methods
.method public constructor <init>(II)V
    .locals 1

    .line 16
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->SPLIT_TICKET_CANCEL:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    .line 17
    iput p1, p0, Lcom/squareup/ui/ticket/analytics/SplitTicketCancelEvent;->saved:I

    .line 18
    iput p2, p0, Lcom/squareup/ui/ticket/analytics/SplitTicketCancelEvent;->not_saved:I

    return-void
.end method
