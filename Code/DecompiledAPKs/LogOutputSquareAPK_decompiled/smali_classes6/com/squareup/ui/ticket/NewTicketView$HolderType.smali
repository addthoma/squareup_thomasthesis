.class final enum Lcom/squareup/ui/ticket/NewTicketView$HolderType;
.super Ljava/lang/Enum;
.source "NewTicketView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/NewTicketView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "HolderType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/ticket/NewTicketView$HolderType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/ticket/NewTicketView$HolderType;

.field public static final enum CUSTOM_TICKET_BUTTON_ROW_HOLDER:Lcom/squareup/ui/ticket/NewTicketView$HolderType;

.field public static final enum NO_RESULTS_ROW_HOLDER:Lcom/squareup/ui/ticket/NewTicketView$HolderType;

.field public static final enum NO_TICKETS_ROW_HOLDER:Lcom/squareup/ui/ticket/NewTicketView$HolderType;

.field public static final enum SELECT_TICKET_GROUP_HEADER_ROW_HOLDER:Lcom/squareup/ui/ticket/NewTicketView$HolderType;

.field public static final enum TICKET_GROUP_ROW_HOLDER:Lcom/squareup/ui/ticket/NewTicketView$HolderType;

.field public static final enum TICKET_TEMPLATE_ROW_HOLDER:Lcom/squareup/ui/ticket/NewTicketView$HolderType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 84
    new-instance v0, Lcom/squareup/ui/ticket/NewTicketView$HolderType;

    const/4 v1, 0x0

    const-string v2, "CUSTOM_TICKET_BUTTON_ROW_HOLDER"

    invoke-direct {v0, v2, v1}, Lcom/squareup/ui/ticket/NewTicketView$HolderType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/ticket/NewTicketView$HolderType;->CUSTOM_TICKET_BUTTON_ROW_HOLDER:Lcom/squareup/ui/ticket/NewTicketView$HolderType;

    .line 85
    new-instance v0, Lcom/squareup/ui/ticket/NewTicketView$HolderType;

    const/4 v2, 0x1

    const-string v3, "SELECT_TICKET_GROUP_HEADER_ROW_HOLDER"

    invoke-direct {v0, v3, v2}, Lcom/squareup/ui/ticket/NewTicketView$HolderType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/ticket/NewTicketView$HolderType;->SELECT_TICKET_GROUP_HEADER_ROW_HOLDER:Lcom/squareup/ui/ticket/NewTicketView$HolderType;

    .line 86
    new-instance v0, Lcom/squareup/ui/ticket/NewTicketView$HolderType;

    const/4 v3, 0x2

    const-string v4, "NO_RESULTS_ROW_HOLDER"

    invoke-direct {v0, v4, v3}, Lcom/squareup/ui/ticket/NewTicketView$HolderType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/ticket/NewTicketView$HolderType;->NO_RESULTS_ROW_HOLDER:Lcom/squareup/ui/ticket/NewTicketView$HolderType;

    .line 87
    new-instance v0, Lcom/squareup/ui/ticket/NewTicketView$HolderType;

    const/4 v4, 0x3

    const-string v5, "NO_TICKETS_ROW_HOLDER"

    invoke-direct {v0, v5, v4}, Lcom/squareup/ui/ticket/NewTicketView$HolderType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/ticket/NewTicketView$HolderType;->NO_TICKETS_ROW_HOLDER:Lcom/squareup/ui/ticket/NewTicketView$HolderType;

    .line 88
    new-instance v0, Lcom/squareup/ui/ticket/NewTicketView$HolderType;

    const/4 v5, 0x4

    const-string v6, "TICKET_GROUP_ROW_HOLDER"

    invoke-direct {v0, v6, v5}, Lcom/squareup/ui/ticket/NewTicketView$HolderType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/ticket/NewTicketView$HolderType;->TICKET_GROUP_ROW_HOLDER:Lcom/squareup/ui/ticket/NewTicketView$HolderType;

    .line 89
    new-instance v0, Lcom/squareup/ui/ticket/NewTicketView$HolderType;

    const/4 v6, 0x5

    const-string v7, "TICKET_TEMPLATE_ROW_HOLDER"

    invoke-direct {v0, v7, v6}, Lcom/squareup/ui/ticket/NewTicketView$HolderType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/ticket/NewTicketView$HolderType;->TICKET_TEMPLATE_ROW_HOLDER:Lcom/squareup/ui/ticket/NewTicketView$HolderType;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/ui/ticket/NewTicketView$HolderType;

    .line 83
    sget-object v7, Lcom/squareup/ui/ticket/NewTicketView$HolderType;->CUSTOM_TICKET_BUTTON_ROW_HOLDER:Lcom/squareup/ui/ticket/NewTicketView$HolderType;

    aput-object v7, v0, v1

    sget-object v1, Lcom/squareup/ui/ticket/NewTicketView$HolderType;->SELECT_TICKET_GROUP_HEADER_ROW_HOLDER:Lcom/squareup/ui/ticket/NewTicketView$HolderType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/ticket/NewTicketView$HolderType;->NO_RESULTS_ROW_HOLDER:Lcom/squareup/ui/ticket/NewTicketView$HolderType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/ui/ticket/NewTicketView$HolderType;->NO_TICKETS_ROW_HOLDER:Lcom/squareup/ui/ticket/NewTicketView$HolderType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/ui/ticket/NewTicketView$HolderType;->TICKET_GROUP_ROW_HOLDER:Lcom/squareup/ui/ticket/NewTicketView$HolderType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/ui/ticket/NewTicketView$HolderType;->TICKET_TEMPLATE_ROW_HOLDER:Lcom/squareup/ui/ticket/NewTicketView$HolderType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/squareup/ui/ticket/NewTicketView$HolderType;->$VALUES:[Lcom/squareup/ui/ticket/NewTicketView$HolderType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 83
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/ticket/NewTicketView$HolderType;
    .locals 1

    .line 83
    const-class v0, Lcom/squareup/ui/ticket/NewTicketView$HolderType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/ticket/NewTicketView$HolderType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/ticket/NewTicketView$HolderType;
    .locals 1

    .line 83
    sget-object v0, Lcom/squareup/ui/ticket/NewTicketView$HolderType;->$VALUES:[Lcom/squareup/ui/ticket/NewTicketView$HolderType;

    invoke-virtual {v0}, [Lcom/squareup/ui/ticket/NewTicketView$HolderType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/ticket/NewTicketView$HolderType;

    return-object v0
.end method
