.class public final Lcom/squareup/ui/ticket/EditTicketScreen$TicketCreatedListener;
.super Ljava/lang/Object;
.source "EditTicketScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/EditTicketScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TicketCreatedListener"
.end annotation


# instance fields
.field private final newTicketCreated:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 217
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 220
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/ticket/EditTicketScreen$TicketCreatedListener;->newTicketCreated:Lcom/jakewharton/rxrelay/PublishRelay;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/ticket/EditTicketScreen$TicketCreatedListener;)V
    .locals 0

    .line 215
    invoke-direct {p0}, Lcom/squareup/ui/ticket/EditTicketScreen$TicketCreatedListener;->publishNewTicketCreated()V

    return-void
.end method

.method private publishNewTicketCreated()V
    .locals 2

    .line 227
    iget-object v0, p0, Lcom/squareup/ui/ticket/EditTicketScreen$TicketCreatedListener;->newTicketCreated:Lcom/jakewharton/rxrelay/PublishRelay;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method onNewTicketCreated()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 223
    iget-object v0, p0, Lcom/squareup/ui/ticket/EditTicketScreen$TicketCreatedListener;->newTicketCreated:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method
