.class public interface abstract Lcom/squareup/ui/ticket/SplitTicketScreen$Component;
.super Ljava/lang/Object;
.source "SplitTicketScreen.java"

# interfaces
.implements Lcom/squareup/marin/widgets/MarinActionBarView$Component;
.implements Lcom/squareup/ui/crm/flow/CrmScope$ParentOrderEntryComponent;


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/marin/widgets/MarinActionBarModule;,
        Lcom/squareup/splitticket/TicketSplitterModule;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/SplitTicketScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract inject(Lcom/squareup/ui/ticket/SplitTicketView;)V
.end method

.method public abstract inject(Lcom/squareup/ui/ticket/TicketView;)V
.end method

.method public abstract splitTicketDetail()Lcom/squareup/ui/ticket/EditSplitTicketScreen$Component;
.end method
