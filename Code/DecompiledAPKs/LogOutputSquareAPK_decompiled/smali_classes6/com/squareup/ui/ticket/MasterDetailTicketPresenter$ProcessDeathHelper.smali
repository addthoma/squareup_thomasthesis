.class Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$ProcessDeathHelper;
.super Ljava/lang/Object;
.source "MasterDetailTicketPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ProcessDeathHelper"
.end annotation


# instance fields
.field private reapplySearchFilter:Z

.field final synthetic this$0:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;)V
    .locals 2

    .line 173
    iput-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$ProcessDeathHelper;->this$0:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 174
    invoke-static {p1}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->access$000(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 175
    invoke-static {p1}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->access$100(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;)Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;->DETAIL_ONLY:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;

    if-eq v0, v1, :cond_0

    invoke-static {p1}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->access$200(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "group-list-presenter-selected-section-search"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    :cond_0
    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    iput-boolean p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$ProcessDeathHelper;->reapplySearchFilter:Z

    return-void
.end method


# virtual methods
.method shouldReapplySearchFilter()Z
    .locals 2

    .line 179
    iget-boolean v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$ProcessDeathHelper;->reapplySearchFilter:Z

    const/4 v1, 0x0

    .line 180
    iput-boolean v1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$ProcessDeathHelper;->reapplySearchFilter:Z

    return v0
.end method
