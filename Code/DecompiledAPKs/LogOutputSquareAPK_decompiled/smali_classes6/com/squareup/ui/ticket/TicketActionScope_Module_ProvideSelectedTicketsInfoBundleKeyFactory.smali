.class public final Lcom/squareup/ui/ticket/TicketActionScope_Module_ProvideSelectedTicketsInfoBundleKeyFactory;
.super Ljava/lang/Object;
.source "TicketActionScope_Module_ProvideSelectedTicketsInfoBundleKeyFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/BundleKey<",
        "Ljava/util/List<",
        "Lcom/squareup/ui/ticket/TicketInfo;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field private final gsonProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketActionScope_Module_ProvideSelectedTicketsInfoBundleKeyFactory;->gsonProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/ui/ticket/TicketActionScope_Module_ProvideSelectedTicketsInfoBundleKeyFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;)",
            "Lcom/squareup/ui/ticket/TicketActionScope_Module_ProvideSelectedTicketsInfoBundleKeyFactory;"
        }
    .end annotation

    .line 34
    new-instance v0, Lcom/squareup/ui/ticket/TicketActionScope_Module_ProvideSelectedTicketsInfoBundleKeyFactory;

    invoke-direct {v0, p0}, Lcom/squareup/ui/ticket/TicketActionScope_Module_ProvideSelectedTicketsInfoBundleKeyFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideSelectedTicketsInfoBundleKey(Lcom/google/gson/Gson;)Lcom/squareup/BundleKey;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/squareup/BundleKey<",
            "Ljava/util/List<",
            "Lcom/squareup/ui/ticket/TicketInfo;",
            ">;>;"
        }
    .end annotation

    .line 38
    invoke-static {p0}, Lcom/squareup/ui/ticket/TicketActionScope$Module;->provideSelectedTicketsInfoBundleKey(Lcom/google/gson/Gson;)Lcom/squareup/BundleKey;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/BundleKey;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/BundleKey;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/BundleKey<",
            "Ljava/util/List<",
            "Lcom/squareup/ui/ticket/TicketInfo;",
            ">;>;"
        }
    .end annotation

    .line 29
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketActionScope_Module_ProvideSelectedTicketsInfoBundleKeyFactory;->gsonProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/gson/Gson;

    invoke-static {v0}, Lcom/squareup/ui/ticket/TicketActionScope_Module_ProvideSelectedTicketsInfoBundleKeyFactory;->provideSelectedTicketsInfoBundleKey(Lcom/google/gson/Gson;)Lcom/squareup/BundleKey;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketActionScope_Module_ProvideSelectedTicketsInfoBundleKeyFactory;->get()Lcom/squareup/BundleKey;

    move-result-object v0

    return-object v0
.end method
