.class Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;
.super Lcom/squareup/ui/RangerRecyclerAdapter;
.source "EditTicketView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/EditTicketView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EditTicketRecylcerAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$ButtonGroupRowHolder;,
        Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$TicketGroupRowHolder;,
        Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$NoTicketGroupRowHolder;,
        Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$TicketGroupHeaderRowHolder;,
        Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$ConvertToCustomTicketButtonRowHolder;,
        Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/RangerRecyclerAdapter<",
        "Lcom/squareup/ui/ticket/EditTicketView$RowType;",
        "Lcom/squareup/ui/ticket/EditTicketView$HolderType;",
        ">;"
    }
.end annotation


# instance fields
.field private buttonsEnabled:Z

.field private selectedItemIndex:I

.field final synthetic this$0:Lcom/squareup/ui/ticket/EditTicketView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/EditTicketView;)V
    .locals 0

    .line 324
    iput-object p1, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->this$0:Lcom/squareup/ui/ticket/EditTicketView;

    .line 325
    const-class p1, Lcom/squareup/ui/ticket/EditTicketView$HolderType;

    invoke-direct {p0, p1}, Lcom/squareup/ui/RangerRecyclerAdapter;-><init>(Ljava/lang/Class;)V

    .line 326
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->resetRanger()V

    return-void
.end method

.method static synthetic access$200(Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;)I
    .locals 0

    .line 93
    iget p0, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->selectedItemIndex:I

    return p0
.end method

.method static synthetic access$202(Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;I)I
    .locals 0

    .line 93
    iput p1, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->selectedItemIndex:I

    return p1
.end method

.method static synthetic access$400(Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;)Z
    .locals 0

    .line 93
    iget-boolean p0, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->buttonsEnabled:Z

    return p0
.end method


# virtual methods
.method public onCreateViewHolder(Landroid/view/ViewGroup;Lcom/squareup/ui/ticket/EditTicketView$HolderType;)Lcom/squareup/ui/RangerRecyclerAdapter$RangerHolder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Lcom/squareup/ui/ticket/EditTicketView$HolderType;",
            ")",
            "Lcom/squareup/ui/RangerRecyclerAdapter<",
            "Lcom/squareup/ui/ticket/EditTicketView$RowType;",
            "Lcom/squareup/ui/ticket/EditTicketView$HolderType;",
            ">.RangerHolder;"
        }
    .end annotation

    .line 303
    sget-object v0, Lcom/squareup/ui/ticket/EditTicketView$1;->$SwitchMap$com$squareup$ui$ticket$EditTicketView$HolderType:[I

    invoke-virtual {p2}, Lcom/squareup/ui/ticket/EditTicketView$HolderType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 317
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Invalid holder type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 315
    :pswitch_0
    new-instance p2, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$ButtonGroupRowHolder;

    invoke-direct {p2, p0, p1}, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$ButtonGroupRowHolder;-><init>(Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;Landroid/view/ViewGroup;)V

    return-object p2

    .line 313
    :pswitch_1
    new-instance p2, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$TicketGroupRowHolder;

    invoke-direct {p2, p0, p1}, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$TicketGroupRowHolder;-><init>(Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;Landroid/view/ViewGroup;)V

    return-object p2

    .line 311
    :pswitch_2
    new-instance p2, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$NoTicketGroupRowHolder;

    invoke-direct {p2, p0, p1}, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$NoTicketGroupRowHolder;-><init>(Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;Landroid/view/ViewGroup;)V

    return-object p2

    .line 309
    :pswitch_3
    new-instance p2, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$TicketGroupHeaderRowHolder;

    invoke-direct {p2, p0, p1}, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$TicketGroupHeaderRowHolder;-><init>(Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;Landroid/view/ViewGroup;)V

    return-object p2

    .line 307
    :pswitch_4
    new-instance p2, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$ConvertToCustomTicketButtonRowHolder;

    invoke-direct {p2, p0, p1}, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$ConvertToCustomTicketButtonRowHolder;-><init>(Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;Landroid/view/ViewGroup;)V

    return-object p2

    .line 305
    :pswitch_5
    new-instance p2, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;

    invoke-direct {p2, p0, p1}, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;-><init>(Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;Landroid/view/ViewGroup;)V

    return-object p2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;Ljava/lang/Enum;)Lcom/squareup/ui/RangerRecyclerAdapter$RangerHolder;
    .locals 0

    .line 93
    check-cast p2, Lcom/squareup/ui/ticket/EditTicketView$HolderType;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;Lcom/squareup/ui/ticket/EditTicketView$HolderType;)Lcom/squareup/ui/RangerRecyclerAdapter$RangerHolder;

    move-result-object p1

    return-object p1
.end method

.method resetRanger()V
    .locals 3

    .line 330
    new-instance v0, Lcom/squareup/ui/Ranger$Builder;

    invoke-direct {v0}, Lcom/squareup/ui/Ranger$Builder;-><init>()V

    .line 331
    iget-object v1, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->this$0:Lcom/squareup/ui/ticket/EditTicketView;

    invoke-static {v1}, Lcom/squareup/ui/ticket/EditTicketView;->access$100(Lcom/squareup/ui/ticket/EditTicketView;)Lcom/squareup/ui/ticket/EditTicketState;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 332
    sget-object v1, Lcom/squareup/ui/ticket/EditTicketView$RowType;->HEADER_ROW:Lcom/squareup/ui/ticket/EditTicketView$RowType;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/Ranger$Builder;->addOne(Ljava/lang/Enum;)Lcom/squareup/ui/Ranger$Builder;

    .line 333
    iget-object v1, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->this$0:Lcom/squareup/ui/ticket/EditTicketView;

    invoke-static {v1}, Lcom/squareup/ui/ticket/EditTicketView;->access$100(Lcom/squareup/ui/ticket/EditTicketView;)Lcom/squareup/ui/ticket/EditTicketState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/ticket/EditTicketState;->isConvertToCustomTicketButtonVisible()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 334
    sget-object v1, Lcom/squareup/ui/ticket/EditTicketView$RowType;->CONVERT_TO_CUSTOM_TICKET_BUTTON_ROW:Lcom/squareup/ui/ticket/EditTicketView$RowType;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/Ranger$Builder;->addOne(Ljava/lang/Enum;)Lcom/squareup/ui/Ranger$Builder;

    .line 336
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->this$0:Lcom/squareup/ui/ticket/EditTicketView;

    invoke-static {v1}, Lcom/squareup/ui/ticket/EditTicketView;->access$100(Lcom/squareup/ui/ticket/EditTicketView;)Lcom/squareup/ui/ticket/EditTicketState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/ticket/EditTicketState;->areTicketGroupsVisible()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->this$0:Lcom/squareup/ui/ticket/EditTicketView;

    invoke-static {v1}, Lcom/squareup/ui/ticket/EditTicketView;->access$300(Lcom/squareup/ui/ticket/EditTicketView;)Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->this$0:Lcom/squareup/ui/ticket/EditTicketView;

    invoke-static {v1}, Lcom/squareup/ui/ticket/EditTicketView;->access$300(Lcom/squareup/ui/ticket/EditTicketView;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 337
    sget-object v1, Lcom/squareup/ui/ticket/EditTicketView$RowType;->TICKET_GROUP_HEADER_ROW:Lcom/squareup/ui/ticket/EditTicketView$RowType;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/Ranger$Builder;->addOne(Ljava/lang/Enum;)Lcom/squareup/ui/Ranger$Builder;

    .line 338
    sget-object v1, Lcom/squareup/ui/ticket/EditTicketView$RowType;->NO_TICKET_GROUP_ROW:Lcom/squareup/ui/ticket/EditTicketView$RowType;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/Ranger$Builder;->addOne(Ljava/lang/Enum;)Lcom/squareup/ui/Ranger$Builder;

    .line 339
    sget-object v1, Lcom/squareup/ui/ticket/EditTicketView$RowType;->TICKET_GROUP_ROW:Lcom/squareup/ui/ticket/EditTicketView$RowType;

    iget-object v2, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->this$0:Lcom/squareup/ui/ticket/EditTicketView;

    invoke-static {v2}, Lcom/squareup/ui/ticket/EditTicketView;->access$300(Lcom/squareup/ui/ticket/EditTicketView;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/Ranger$Builder;->addRange(Ljava/lang/Enum;I)Lcom/squareup/ui/Ranger$Builder;

    .line 341
    :cond_1
    iget-object v1, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->this$0:Lcom/squareup/ui/ticket/EditTicketView;

    invoke-static {v1}, Lcom/squareup/ui/ticket/EditTicketView;->access$100(Lcom/squareup/ui/ticket/EditTicketView;)Lcom/squareup/ui/ticket/EditTicketState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/ticket/EditTicketState;->isDeleteVoidOrCompButtonVisible()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 342
    sget-object v1, Lcom/squareup/ui/ticket/EditTicketView$RowType;->BUTTON_GROUP_ROW:Lcom/squareup/ui/ticket/EditTicketView$RowType;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/Ranger$Builder;->addOne(Ljava/lang/Enum;)Lcom/squareup/ui/Ranger$Builder;

    .line 345
    :cond_2
    invoke-virtual {v0}, Lcom/squareup/ui/Ranger$Builder;->build()Lcom/squareup/ui/Ranger;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->setRanger(Lcom/squareup/ui/Ranger;)V

    return-void
.end method

.method setButtonsEnabled(Z)V
    .locals 2

    .line 349
    iget-boolean v0, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->buttonsEnabled:Z

    if-eq v0, p1, :cond_0

    .line 350
    iput-boolean p1, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->buttonsEnabled:Z

    .line 351
    iget-object p1, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->ranger:Lcom/squareup/ui/Ranger;

    sget-object v0, Lcom/squareup/ui/ticket/EditTicketView$RowType;->BUTTON_GROUP_ROW:Lcom/squareup/ui/ticket/EditTicketView$RowType;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lcom/squareup/ui/Ranger;->indexOfRowType(Ljava/lang/Enum;I)I

    move-result p1

    if-ltz p1, :cond_0

    .line 352
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->getItemCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 353
    invoke-virtual {p0, p1}, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->notifyItemChanged(I)V

    :cond_0
    return-void
.end method
