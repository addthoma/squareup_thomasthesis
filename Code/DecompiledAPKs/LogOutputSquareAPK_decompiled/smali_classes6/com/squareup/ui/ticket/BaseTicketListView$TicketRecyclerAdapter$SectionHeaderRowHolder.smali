.class Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$SectionHeaderRowHolder;
.super Lcom/squareup/ui/RangerRecyclerAdapter$RangerHolder;
.source "BaseTicketListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SectionHeaderRowHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/RangerRecyclerAdapter<",
        "Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;",
        "Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;",
        ">.RangerHolder;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;Landroid/view/ViewGroup;)V
    .locals 1

    .line 386
    iput-object p1, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$SectionHeaderRowHolder;->this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

    .line 387
    sget v0, Lcom/squareup/orderentry/R$layout;->ticket_list_section_header_row:I

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/ui/RangerRecyclerAdapter$RangerHolder;-><init>(Lcom/squareup/ui/RangerRecyclerAdapter;Landroid/view/ViewGroup;I)V

    return-void
.end method


# virtual methods
.method protected bindRow(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;II)V
    .locals 3

    .line 391
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$SectionHeaderRowHolder;->itemView()Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/squareup/noho/NohoLinearLayout;

    .line 393
    sget v0, Lcom/squareup/orderentry/R$id;->section_header_row:I

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/squareup/noho/NohoRow;

    .line 395
    iget-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$SectionHeaderRowHolder;->this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

    sget-object v1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->SORT:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    invoke-static {v0, p3, v1}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->access$500(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;ILcom/squareup/ui/ticket/BaseTicketListView$TicketRow;)Z

    move-result p3

    if-eqz p3, :cond_0

    const/4 p3, 0x0

    goto :goto_0

    :cond_0
    iget-object p3, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$SectionHeaderRowHolder;->this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

    iget-object p3, p3, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->this$0:Lcom/squareup/ui/ticket/BaseTicketListView;

    invoke-static {p3}, Lcom/squareup/ui/ticket/BaseTicketListView;->access$600(Lcom/squareup/ui/ticket/BaseTicketListView;)I

    move-result p3

    .line 396
    :goto_0
    invoke-virtual {p2}, Lcom/squareup/noho/NohoRow;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p2}, Lcom/squareup/noho/NohoRow;->getPaddingRight()I

    move-result v1

    .line 397
    invoke-virtual {p2}, Lcom/squareup/noho/NohoRow;->getPaddingBottom()I

    move-result v2

    .line 396
    invoke-virtual {p2, v0, p3, v1, v2}, Lcom/squareup/noho/NohoRow;->setPadding(IIII)V

    .line 399
    sget-object p3, Lcom/squareup/ui/ticket/BaseTicketListView$2;->$SwitchMap$com$squareup$ui$ticket$BaseTicketListView$TicketRow:[I

    invoke-virtual {p1}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->ordinal()I

    move-result v0

    aget p3, p3, v0

    const/4 v0, 0x3

    if-eq p3, v0, :cond_6

    const/4 v0, 0x4

    const-string v1, ""

    if-eq p3, v0, :cond_4

    const/4 v0, 0x5

    if-eq p3, v0, :cond_3

    const/4 v0, 0x6

    if-ne p3, v0, :cond_2

    .line 415
    sget p1, Lcom/squareup/orderentry/R$string;->open_tickets_other_tickets:I

    invoke-virtual {p2, p1}, Lcom/squareup/noho/NohoRow;->setLabelId(I)V

    .line 416
    iget-object p1, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$SectionHeaderRowHolder;->this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

    invoke-static {p1}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->access$1000(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;)Ljava/util/Map;

    move-result-object p1

    sget-object p3, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->OTHER_TICKETS_HEADER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    invoke-interface {p1, p3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 417
    iget-object p1, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$SectionHeaderRowHolder;->this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

    invoke-static {p1}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->access$1000(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;)Ljava/util/Map;

    move-result-object p1

    sget-object p3, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->OTHER_TICKETS_HEADER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    invoke-interface {p1, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p2, p1}, Lcom/squareup/noho/NohoRow;->setDescription(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 419
    :cond_1
    invoke-virtual {p2, v1}, Lcom/squareup/noho/NohoRow;->setDescription(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 423
    :cond_2
    new-instance p2, Ljava/lang/IllegalStateException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Unrecognized rowType: "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p2

    .line 412
    :cond_3
    sget p1, Lcom/squareup/orderentry/R$string;->open_tickets_available_tickets:I

    invoke-virtual {p2, p1}, Lcom/squareup/noho/NohoRow;->setLabelId(I)V

    goto :goto_1

    .line 404
    :cond_4
    sget p1, Lcom/squareup/orderentry/R$string;->open_tickets_your_tickets:I

    invoke-virtual {p2, p1}, Lcom/squareup/noho/NohoRow;->setLabelId(I)V

    .line 405
    iget-object p1, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$SectionHeaderRowHolder;->this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

    invoke-static {p1}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->access$1000(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;)Ljava/util/Map;

    move-result-object p1

    sget-object p3, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->YOUR_TICKETS_HEADER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    invoke-interface {p1, p3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_5

    .line 406
    iget-object p1, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$SectionHeaderRowHolder;->this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

    invoke-static {p1}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->access$1000(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;)Ljava/util/Map;

    move-result-object p1

    sget-object p3, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->YOUR_TICKETS_HEADER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    invoke-interface {p1, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p2, p1}, Lcom/squareup/noho/NohoRow;->setDescription(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 408
    :cond_5
    invoke-virtual {p2, v1}, Lcom/squareup/noho/NohoRow;->setDescription(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 401
    :cond_6
    sget p1, Lcom/squareup/orderentry/R$string;->open_tickets_open_tickets:I

    invoke-virtual {p2, p1}, Lcom/squareup/noho/NohoRow;->setLabelId(I)V

    :goto_1
    return-void
.end method

.method protected bridge synthetic bindRow(Ljava/lang/Enum;II)V
    .locals 0

    .line 384
    check-cast p1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$SectionHeaderRowHolder;->bindRow(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;II)V

    return-void
.end method
