.class public final Lcom/squareup/ui/ticket/EditSplitTicketScreen_EditSplitTicketPresenter_Factory;
.super Ljava/lang/Object;
.source "EditSplitTicketScreen_EditSplitTicketPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final cardNameHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/TicketCardNameHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final openTicketsSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final predefinedTicketsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/opentickets/PredefinedTickets;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final splitTicketPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/SplitTicketPresenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/SplitTicketPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/TicketCardNameHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/opentickets/PredefinedTickets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;)V"
        }
    .end annotation

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen_EditSplitTicketPresenter_Factory;->splitTicketPresenterProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p2, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen_EditSplitTicketPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p3, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen_EditSplitTicketPresenter_Factory;->cardNameHandlerProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p4, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen_EditSplitTicketPresenter_Factory;->predefinedTicketsProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p5, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen_EditSplitTicketPresenter_Factory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p6, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen_EditSplitTicketPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/ticket/EditSplitTicketScreen_EditSplitTicketPresenter_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/SplitTicketPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/TicketCardNameHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/opentickets/PredefinedTickets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;)",
            "Lcom/squareup/ui/ticket/EditSplitTicketScreen_EditSplitTicketPresenter_Factory;"
        }
    .end annotation

    .line 56
    new-instance v7, Lcom/squareup/ui/ticket/EditSplitTicketScreen_EditSplitTicketPresenter_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/ticket/EditSplitTicketScreen_EditSplitTicketPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Ljava/lang/Object;Lcom/squareup/util/Res;Lcom/squareup/tickets/TicketCardNameHandler;Lcom/squareup/opentickets/PredefinedTickets;Lcom/squareup/tickets/OpenTicketsSettings;Lflow/Flow;)Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;
    .locals 8

    .line 62
    new-instance v7, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;

    move-object v1, p0

    check-cast v1, Lcom/squareup/ui/ticket/SplitTicketPresenter;

    move-object v0, v7

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;-><init>(Lcom/squareup/ui/ticket/SplitTicketPresenter;Lcom/squareup/util/Res;Lcom/squareup/tickets/TicketCardNameHandler;Lcom/squareup/opentickets/PredefinedTickets;Lcom/squareup/tickets/OpenTicketsSettings;Lflow/Flow;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;
    .locals 7

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen_EditSplitTicketPresenter_Factory;->splitTicketPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    iget-object v0, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen_EditSplitTicketPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen_EditSplitTicketPresenter_Factory;->cardNameHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/tickets/TicketCardNameHandler;

    iget-object v0, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen_EditSplitTicketPresenter_Factory;->predefinedTicketsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/opentickets/PredefinedTickets;

    iget-object v0, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen_EditSplitTicketPresenter_Factory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/tickets/OpenTicketsSettings;

    iget-object v0, p0, Lcom/squareup/ui/ticket/EditSplitTicketScreen_EditSplitTicketPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lflow/Flow;

    invoke-static/range {v1 .. v6}, Lcom/squareup/ui/ticket/EditSplitTicketScreen_EditSplitTicketPresenter_Factory;->newInstance(Ljava/lang/Object;Lcom/squareup/util/Res;Lcom/squareup/tickets/TicketCardNameHandler;Lcom/squareup/opentickets/PredefinedTickets;Lcom/squareup/tickets/OpenTicketsSettings;Lflow/Flow;)Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/EditSplitTicketScreen_EditSplitTicketPresenter_Factory;->get()Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;

    move-result-object v0

    return-object v0
.end method
