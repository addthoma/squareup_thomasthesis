.class public final Lcom/squareup/ui/loggedout/NoFinalLogInCheck;
.super Ljava/lang/Object;
.source "FinalLogInCheck.kt"

# interfaces
.implements Lcom/squareup/ui/loggedout/FinalLogInCheck;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nFinalLogInCheck.kt\nKotlin\n*S Kotlin\n*F\n+ 1 FinalLogInCheck.kt\ncom/squareup/ui/loggedout/NoFinalLogInCheck\n+ 2 Objects.kt\ncom/squareup/util/Objects\n*L\n1#1,74:1\n152#2:75\n*E\n*S KotlinDebug\n*F\n+ 1 FinalLogInCheck.kt\ncom/squareup/ui/loggedout/NoFinalLogInCheck\n*L\n62#1:75\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u0011\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0096\u0001J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0016\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/ui/loggedout/NoFinalLogInCheck;",
        "Lcom/squareup/ui/loggedout/FinalLogInCheck;",
        "()V",
        "handleLoginCheckFailed",
        "",
        "sessionToken",
        "",
        "isLoginSuccessful",
        "",
        "response",
        "Lcom/squareup/server/account/protos/AccountStatusResponse;",
        "loggedout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final synthetic $$delegate_0:Lcom/squareup/ui/loggedout/FinalLogInCheck;


# direct methods
.method public constructor <init>()V
    .locals 5
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    const-class v0, Lcom/squareup/ui/loggedout/FinalLogInCheck;

    const-string v1, "FinalLogInCheck is successful, should not be called"

    const/4 v2, 0x0

    const/4 v3, 0x4

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/util/Objects;->allMethodsThrowUnsupportedOperation$default(Ljava/lang/Class;Ljava/lang/String;ZILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/loggedout/FinalLogInCheck;

    iput-object v0, p0, Lcom/squareup/ui/loggedout/NoFinalLogInCheck;->$$delegate_0:Lcom/squareup/ui/loggedout/FinalLogInCheck;

    return-void
.end method


# virtual methods
.method public handleLoginCheckFailed(Ljava/lang/String;)V
    .locals 1

    const-string v0, "sessionToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/ui/loggedout/NoFinalLogInCheck;->$$delegate_0:Lcom/squareup/ui/loggedout/FinalLogInCheck;

    invoke-interface {v0, p1}, Lcom/squareup/ui/loggedout/FinalLogInCheck;->handleLoginCheckFailed(Ljava/lang/String;)V

    return-void
.end method

.method public isLoginSuccessful(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z
    .locals 1

    const-string v0, "response"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x1

    return p1
.end method
