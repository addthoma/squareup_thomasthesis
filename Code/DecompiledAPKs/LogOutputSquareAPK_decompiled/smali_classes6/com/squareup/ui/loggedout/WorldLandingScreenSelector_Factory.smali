.class public final Lcom/squareup/ui/loggedout/WorldLandingScreenSelector_Factory;
.super Ljava/lang/Object;
.source "WorldLandingScreenSelector_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/loggedout/WorldLandingScreenSelector;",
        ">;"
    }
.end annotation


# instance fields
.field private final countryGuesserProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/location/CountryCodeGuesser;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final postInstallEncryptedEmailSettingProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/ui/login/PostInstallEncryptedEmailFromReferral;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/ui/login/PostInstallEncryptedEmailFromReferral;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/location/CountryCodeGuesser;",
            ">;)V"
        }
    .end annotation

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/squareup/ui/loggedout/WorldLandingScreenSelector_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p2, p0, Lcom/squareup/ui/loggedout/WorldLandingScreenSelector_Factory;->postInstallEncryptedEmailSettingProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p3, p0, Lcom/squareup/ui/loggedout/WorldLandingScreenSelector_Factory;->countryGuesserProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/loggedout/WorldLandingScreenSelector_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/ui/login/PostInstallEncryptedEmailFromReferral;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/location/CountryCodeGuesser;",
            ">;)",
            "Lcom/squareup/ui/loggedout/WorldLandingScreenSelector_Factory;"
        }
    .end annotation

    .line 42
    new-instance v0, Lcom/squareup/ui/loggedout/WorldLandingScreenSelector_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/loggedout/WorldLandingScreenSelector_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/settings/server/Features;Lcom/squareup/settings/LocalSetting;Lcom/squareup/location/CountryCodeGuesser;)Lcom/squareup/ui/loggedout/WorldLandingScreenSelector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/ui/login/PostInstallEncryptedEmailFromReferral;",
            ">;",
            "Lcom/squareup/location/CountryCodeGuesser;",
            ")",
            "Lcom/squareup/ui/loggedout/WorldLandingScreenSelector;"
        }
    .end annotation

    .line 48
    new-instance v0, Lcom/squareup/ui/loggedout/WorldLandingScreenSelector;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/loggedout/WorldLandingScreenSelector;-><init>(Lcom/squareup/settings/server/Features;Lcom/squareup/settings/LocalSetting;Lcom/squareup/location/CountryCodeGuesser;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/loggedout/WorldLandingScreenSelector;
    .locals 3

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/loggedout/WorldLandingScreenSelector_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/Features;

    iget-object v1, p0, Lcom/squareup/ui/loggedout/WorldLandingScreenSelector_Factory;->postInstallEncryptedEmailSettingProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/settings/LocalSetting;

    iget-object v2, p0, Lcom/squareup/ui/loggedout/WorldLandingScreenSelector_Factory;->countryGuesserProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/location/CountryCodeGuesser;

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/loggedout/WorldLandingScreenSelector_Factory;->newInstance(Lcom/squareup/settings/server/Features;Lcom/squareup/settings/LocalSetting;Lcom/squareup/location/CountryCodeGuesser;)Lcom/squareup/ui/loggedout/WorldLandingScreenSelector;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ui/loggedout/WorldLandingScreenSelector_Factory;->get()Lcom/squareup/ui/loggedout/WorldLandingScreenSelector;

    move-result-object v0

    return-object v0
.end method
