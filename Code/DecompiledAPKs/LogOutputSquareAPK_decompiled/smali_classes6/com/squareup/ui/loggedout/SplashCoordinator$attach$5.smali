.class final Lcom/squareup/ui/loggedout/SplashCoordinator$attach$5;
.super Lkotlin/jvm/internal/Lambda;
.source "SplashCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/loggedout/SplashCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lrx/Subscription;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "Lrx/Subscription;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/loggedout/SplashCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/loggedout/SplashCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/loggedout/SplashCoordinator$attach$5;->this$0:Lcom/squareup/ui/loggedout/SplashCoordinator;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/ui/loggedout/SplashCoordinator$attach$5;->invoke()Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public final invoke()Lrx/Subscription;
    .locals 3

    .line 97
    iget-object v0, p0, Lcom/squareup/ui/loggedout/SplashCoordinator$attach$5;->this$0:Lcom/squareup/ui/loggedout/SplashCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/loggedout/SplashCoordinator;->access$getEncryptedEmailsFromReferrals$p(Lcom/squareup/ui/loggedout/SplashCoordinator;)Lcom/squareup/analytics/EncryptedEmailsFromReferrals;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/analytics/EncryptedEmailsFromReferrals;->encryptedEmails()Lrx/Observable;

    move-result-object v0

    .line 98
    new-instance v1, Lcom/squareup/ui/loggedout/SplashCoordinator$attach$5$1;

    iget-object v2, p0, Lcom/squareup/ui/loggedout/SplashCoordinator$attach$5;->this$0:Lcom/squareup/ui/loggedout/SplashCoordinator;

    invoke-static {v2}, Lcom/squareup/ui/loggedout/SplashCoordinator;->access$getScopeRunner$p(Lcom/squareup/ui/loggedout/SplashCoordinator;)Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/ui/loggedout/SplashCoordinator$attach$5$1;-><init>(Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    new-instance v2, Lcom/squareup/ui/loggedout/SplashCoordinator$sam$rx_functions_Action1$0;

    invoke-direct {v2, v1}, Lcom/squareup/ui/loggedout/SplashCoordinator$sam$rx_functions_Action1$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lrx/functions/Action1;

    invoke-virtual {v0, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    const-string v1, "encryptedEmailsFromRefer\u2026onReceivedEncryptedEmail)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
