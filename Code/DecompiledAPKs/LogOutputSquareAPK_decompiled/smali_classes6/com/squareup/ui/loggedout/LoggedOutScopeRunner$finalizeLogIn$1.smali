.class final Lcom/squareup/ui/loggedout/LoggedOutScopeRunner$finalizeLogIn$1;
.super Ljava/lang/Object;
.source "LoggedOutScopeRunner.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->finalizeLogIn(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
        "+",
        "Lcom/squareup/server/account/protos/AccountStatusResponse;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012*\u0010\u0002\u001a&\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004 \u0005*\u0012\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "successOrFailure",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/server/account/protos/AccountStatusResponse;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $sessionToken:Ljava/lang/String;

.field final synthetic this$0:Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner$finalizeLogIn$1;->this$0:Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    iput-object p2, p0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner$finalizeLogIn$1;->$sessionToken:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;)V"
        }
    .end annotation

    .line 343
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_6

    .line 344
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/server/account/protos/AccountStatusResponse;

    .line 350
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner$finalizeLogIn$1;->this$0:Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    invoke-static {v0}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->access$getFinalLogInCheck$p(Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;)Lcom/squareup/ui/loggedout/FinalLogInCheck;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/ui/loggedout/FinalLogInCheck;->isLoginSuccessful(Lcom/squareup/server/account/protos/AccountStatusResponse;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 351
    iget-object p1, p0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner$finalizeLogIn$1;->this$0:Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    invoke-static {p1}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->access$getFinalLogInCheck$p(Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;)Lcom/squareup/ui/loggedout/FinalLogInCheck;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner$finalizeLogIn$1;->$sessionToken:Ljava/lang/String;

    invoke-interface {p1, v0}, Lcom/squareup/ui/loggedout/FinalLogInCheck;->handleLoginCheckFailed(Ljava/lang/String;)V

    goto :goto_0

    .line 353
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner$finalizeLogIn$1;->this$0:Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    invoke-static {v0}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->access$getLegacyAuthenticator$p(Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;)Lcom/squareup/account/LegacyAuthenticator;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner$finalizeLogIn$1;->$sessionToken:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Lcom/squareup/account/LegacyAuthenticator;->loggedIn(Ljava/lang/String;Lcom/squareup/server/account/protos/AccountStatusResponse;)V

    .line 354
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner$finalizeLogIn$1;->this$0:Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    invoke-static {v0}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->access$getAnalytics$p(Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;)Lcom/squareup/analytics/Analytics;

    move-result-object v0

    new-instance v1, Lcom/squareup/analytics/event/ClickEvent;

    const-string v2, "Merchant Signed in"

    invoke-direct {v1, v2}, Lcom/squareup/analytics/event/ClickEvent;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    .line 356
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    if-nez v0, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    iget-object v0, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_onboard:Ljava/lang/Boolean;

    if-nez v0, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string v1, "response.features!!.can_onboard!!"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 357
    iget-object p1, p0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner$finalizeLogIn$1;->this$0:Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->goToActivation()V

    goto :goto_0

    .line 359
    :cond_3
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner$finalizeLogIn$1;->this$0:Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    invoke-static {v0}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->access$getAdAnalytics$p(Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;)Lcom/squareup/adanalytics/AdAnalytics;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->user:Lcom/squareup/server/account/protos/User;

    if-nez p1, :cond_4

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_4
    iget-object p1, p1, Lcom/squareup/server/account/protos/User;->merchant_token:Ljava/lang/String;

    if-nez p1, :cond_5

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_5
    invoke-interface {v0, p1}, Lcom/squareup/adanalytics/AdAnalytics;->recordSignIn(Ljava/lang/String;)V

    goto :goto_0

    .line 363
    :cond_6
    instance-of p1, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz p1, :cond_7

    iget-object p1, p0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner$finalizeLogIn$1;->this$0:Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner$finalizeLogIn$1;->$sessionToken:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->access$showFinalizeLoginFailure(Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;Ljava/lang/String;)V

    :cond_7
    :goto_0
    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 76
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner$finalizeLogIn$1;->accept(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V

    return-void
.end method
