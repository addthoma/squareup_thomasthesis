.class public final Lcom/squareup/ui/loggedout/SplashCoordinator_Factory;
.super Ljava/lang/Object;
.source "SplashCoordinator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/loggedout/SplashCoordinator;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final encryptedEmailsFromReferralsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/EncryptedEmailsFromReferrals;",
            ">;"
        }
    .end annotation
.end field

.field private final pagesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/squareup/ui/loggedout/SplashCoordinator$SplashPage;",
            ">;>;"
        }
    .end annotation
.end field

.field private final scopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/squareup/ui/loggedout/SplashCoordinator$SplashPage;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/EncryptedEmailsFromReferrals;",
            ">;)V"
        }
    .end annotation

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/squareup/ui/loggedout/SplashCoordinator_Factory;->scopeRunnerProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p2, p0, Lcom/squareup/ui/loggedout/SplashCoordinator_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p3, p0, Lcom/squareup/ui/loggedout/SplashCoordinator_Factory;->pagesProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p4, p0, Lcom/squareup/ui/loggedout/SplashCoordinator_Factory;->encryptedEmailsFromReferralsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/loggedout/SplashCoordinator_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/squareup/ui/loggedout/SplashCoordinator$SplashPage;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/EncryptedEmailsFromReferrals;",
            ">;)",
            "Lcom/squareup/ui/loggedout/SplashCoordinator_Factory;"
        }
    .end annotation

    .line 46
    new-instance v0, Lcom/squareup/ui/loggedout/SplashCoordinator_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/loggedout/SplashCoordinator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;Lcom/squareup/analytics/Analytics;Ljava/util/List;Lcom/squareup/analytics/EncryptedEmailsFromReferrals;)Lcom/squareup/ui/loggedout/SplashCoordinator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;",
            "Lcom/squareup/analytics/Analytics;",
            "Ljava/util/List<",
            "Lcom/squareup/ui/loggedout/SplashCoordinator$SplashPage;",
            ">;",
            "Lcom/squareup/analytics/EncryptedEmailsFromReferrals;",
            ")",
            "Lcom/squareup/ui/loggedout/SplashCoordinator;"
        }
    .end annotation

    .line 52
    new-instance v0, Lcom/squareup/ui/loggedout/SplashCoordinator;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/loggedout/SplashCoordinator;-><init>(Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;Lcom/squareup/analytics/Analytics;Ljava/util/List;Lcom/squareup/analytics/EncryptedEmailsFromReferrals;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/loggedout/SplashCoordinator;
    .locals 4

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/loggedout/SplashCoordinator_Factory;->scopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    iget-object v1, p0, Lcom/squareup/ui/loggedout/SplashCoordinator_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/analytics/Analytics;

    iget-object v2, p0, Lcom/squareup/ui/loggedout/SplashCoordinator_Factory;->pagesProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/ui/loggedout/SplashCoordinator_Factory;->encryptedEmailsFromReferralsProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/analytics/EncryptedEmailsFromReferrals;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/ui/loggedout/SplashCoordinator_Factory;->newInstance(Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;Lcom/squareup/analytics/Analytics;Ljava/util/List;Lcom/squareup/analytics/EncryptedEmailsFromReferrals;)Lcom/squareup/ui/loggedout/SplashCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/ui/loggedout/SplashCoordinator_Factory;->get()Lcom/squareup/ui/loggedout/SplashCoordinator;

    move-result-object v0

    return-object v0
.end method
