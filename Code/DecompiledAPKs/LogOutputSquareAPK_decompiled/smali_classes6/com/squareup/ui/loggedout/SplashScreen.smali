.class public final Lcom/squareup/ui/loggedout/SplashScreen;
.super Lcom/squareup/ui/loggedout/InLoggedOutActivityScope;
.source "SplashScreen.kt"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/LocksOrientation;
.implements Lcom/squareup/coordinators/CoordinatorProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/loggedout/SplashScreen$ParentComponent;,
        Lcom/squareup/ui/loggedout/SplashScreen$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSplashScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SplashScreen.kt\ncom/squareup/ui/loggedout/SplashScreen\n+ 2 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,44:1\n52#2:45\n*E\n*S KotlinDebug\n*F\n+ 1 SplashScreen.kt\ncom/squareup/ui/loggedout/SplashScreen\n*L\n36#1:45\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0018\u0000 \u00142\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u0004:\u0002\u0014\u0015B\u0005\u00a2\u0006\u0002\u0010\u0005J\u0008\u0010\u000c\u001a\u00020\rH\u0016J\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0016J\u0008\u0010\u0012\u001a\u00020\u0013H\u0016R\u0014\u0010\u0006\u001a\u00020\u0007X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0014\u0010\n\u001a\u00020\u0007X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\t\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/ui/loggedout/SplashScreen;",
        "Lcom/squareup/ui/loggedout/InLoggedOutActivityScope;",
        "Lcom/squareup/container/LayoutScreen;",
        "Lcom/squareup/container/LocksOrientation;",
        "Lcom/squareup/coordinators/CoordinatorProvider;",
        "()V",
        "orientationForPhone",
        "Lcom/squareup/workflow/WorkflowViewFactory$Orientation;",
        "getOrientationForPhone",
        "()Lcom/squareup/workflow/WorkflowViewFactory$Orientation;",
        "orientationForTablet",
        "getOrientationForTablet",
        "getAnalyticsName",
        "Lcom/squareup/analytics/RegisterViewName;",
        "provideCoordinator",
        "Lcom/squareup/coordinators/Coordinator;",
        "view",
        "Landroid/view/View;",
        "screenLayout",
        "",
        "Companion",
        "ParentComponent",
        "loggedout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Lcom/squareup/container/ContainerTreeKey$PathCreator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/container/ContainerTreeKey$PathCreator<",
            "Lcom/squareup/ui/loggedout/SplashScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/ui/loggedout/SplashScreen$Companion;

.field private static final INSTANCE:Lcom/squareup/ui/loggedout/SplashScreen;


# instance fields
.field private final orientationForPhone:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

.field private final orientationForTablet:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/loggedout/SplashScreen$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/loggedout/SplashScreen$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/loggedout/SplashScreen;->Companion:Lcom/squareup/ui/loggedout/SplashScreen$Companion;

    .line 24
    new-instance v0, Lcom/squareup/ui/loggedout/SplashScreen;

    invoke-direct {v0}, Lcom/squareup/ui/loggedout/SplashScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/loggedout/SplashScreen;->INSTANCE:Lcom/squareup/ui/loggedout/SplashScreen;

    .line 26
    sget-object v0, Lcom/squareup/ui/loggedout/SplashScreen;->INSTANCE:Lcom/squareup/ui/loggedout/SplashScreen;

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    const-string v1, "PathCreator.forSingleton(INSTANCE)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/ui/loggedout/SplashScreen;->CREATOR:Lcom/squareup/container/ContainerTreeKey$PathCreator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 22
    invoke-direct {p0}, Lcom/squareup/ui/loggedout/InLoggedOutActivityScope;-><init>()V

    .line 29
    sget-object v0, Lcom/squareup/workflow/WorkflowViewFactory$Orientation;->SENSOR_PORTRAIT:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    iput-object v0, p0, Lcom/squareup/ui/loggedout/SplashScreen;->orientationForPhone:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    .line 31
    sget-object v0, Lcom/squareup/workflow/WorkflowViewFactory$Orientation;->UNLOCKED:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    iput-object v0, p0, Lcom/squareup/ui/loggedout/SplashScreen;->orientationForTablet:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    return-void
.end method

.method public static final synthetic access$getINSTANCE$cp()Lcom/squareup/ui/loggedout/SplashScreen;
    .locals 1

    .line 22
    sget-object v0, Lcom/squareup/ui/loggedout/SplashScreen;->INSTANCE:Lcom/squareup/ui/loggedout/SplashScreen;

    return-object v0
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 38
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->WELCOME_SPLASH:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getOrientationForPhone()Lcom/squareup/workflow/WorkflowViewFactory$Orientation;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/ui/loggedout/SplashScreen;->orientationForPhone:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    return-object v0
.end method

.method public getOrientationForTablet()Lcom/squareup/workflow/WorkflowViewFactory$Orientation;
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/squareup/ui/loggedout/SplashScreen;->orientationForTablet:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    return-object v0
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string v0, "view.context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    const-class v0, Lcom/squareup/ui/loggedout/SplashScreen$ParentComponent;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/loggedout/SplashScreen$ParentComponent;

    .line 36
    invoke-interface {p1}, Lcom/squareup/ui/loggedout/SplashScreen$ParentComponent;->splashCoordinator()Lcom/squareup/ui/loggedout/SplashCoordinator;

    move-result-object p1

    check-cast p1, Lcom/squareup/coordinators/Coordinator;

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 33
    sget v0, Lcom/squareup/loggedout/R$layout;->splash_page_container:I

    return v0
.end method
