.class final Lcom/squareup/util/TaxBreakdown$TaxInformation;
.super Ljava/lang/Object;
.source "TaxBreakdown.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/util/TaxBreakdown;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "TaxInformation"
.end annotation


# instance fields
.field final amounts:Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;

.field final inclusionType:Lcom/squareup/api/items/Fee$InclusionType;

.field final name:Ljava/lang/String;

.field final percentage:Lcom/squareup/util/Percentage;

.field final taxId:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/squareup/protos/client/bills/FeeLineItem;)V
    .locals 2

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iget-object v0, p1, Lcom/squareup/protos/client/bills/FeeLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;

    const-string v1, ""

    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p1, Lcom/squareup/protos/client/bills/FeeLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->cogs_object_id:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/util/TaxBreakdown$TaxInformation;->taxId:Ljava/lang/String;

    .line 76
    iget-object v0, p1, Lcom/squareup/protos/client/bills/FeeLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->name:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/squareup/util/Strings;->valueOrDefault(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/util/TaxBreakdown$TaxInformation;->name:Ljava/lang/String;

    .line 77
    iget-object v0, p1, Lcom/squareup/protos/client/bills/FeeLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->percentage:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Percentage;->fromString(Ljava/lang/String;)Lcom/squareup/util/Percentage;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/util/TaxBreakdown$TaxInformation;->percentage:Lcom/squareup/util/Percentage;

    .line 78
    iget-object v0, p1, Lcom/squareup/protos/client/bills/FeeLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;

    iput-object v0, p0, Lcom/squareup/util/TaxBreakdown$TaxInformation;->inclusionType:Lcom/squareup/api/items/Fee$InclusionType;

    goto :goto_0

    .line 79
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bills/FeeLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;

    if-eqz v0, :cond_1

    .line 80
    iget-object v0, p1, Lcom/squareup/protos/client/bills/FeeLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;->fee:Lcom/squareup/api/items/Fee;

    iget-object v0, v0, Lcom/squareup/api/items/Fee;->id:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/util/TaxBreakdown$TaxInformation;->taxId:Ljava/lang/String;

    .line 81
    iget-object v0, p1, Lcom/squareup/protos/client/bills/FeeLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;->fee:Lcom/squareup/api/items/Fee;

    iget-object v0, v0, Lcom/squareup/api/items/Fee;->name:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/squareup/util/Strings;->valueOrDefault(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/util/TaxBreakdown$TaxInformation;->name:Ljava/lang/String;

    .line 82
    iget-object v0, p1, Lcom/squareup/protos/client/bills/FeeLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;->fee:Lcom/squareup/api/items/Fee;

    iget-object v0, v0, Lcom/squareup/api/items/Fee;->percentage:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Percentage;->fromString(Ljava/lang/String;)Lcom/squareup/util/Percentage;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/util/TaxBreakdown$TaxInformation;->percentage:Lcom/squareup/util/Percentage;

    .line 83
    iget-object v0, p1, Lcom/squareup/protos/client/bills/FeeLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;->fee:Lcom/squareup/api/items/Fee;

    iget-object v0, v0, Lcom/squareup/api/items/Fee;->inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;

    iput-object v0, p0, Lcom/squareup/util/TaxBreakdown$TaxInformation;->inclusionType:Lcom/squareup/api/items/Fee$InclusionType;

    .line 88
    :goto_0
    iget-object p1, p1, Lcom/squareup/protos/client/bills/FeeLineItem;->amounts:Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;

    iput-object p1, p0, Lcom/squareup/util/TaxBreakdown$TaxInformation;->amounts:Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;

    .line 90
    iget-object p1, p0, Lcom/squareup/util/TaxBreakdown$TaxInformation;->taxId:Ljava/lang/String;

    const-string v0, "taxId"

    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonBlank(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    .line 91
    iget-object p1, p0, Lcom/squareup/util/TaxBreakdown$TaxInformation;->percentage:Lcom/squareup/util/Percentage;

    const-string v0, "percentage"

    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 92
    iget-object p1, p0, Lcom/squareup/util/TaxBreakdown$TaxInformation;->amounts:Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;

    const-string v0, "amounts"

    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 93
    iget-object p1, p0, Lcom/squareup/util/TaxBreakdown$TaxInformation;->inclusionType:Lcom/squareup/api/items/Fee$InclusionType;

    const-string v0, "inclusionType"

    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    return-void

    .line 85
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Unable to read tax information from FeeLineItem."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method synthetic constructor <init>(Lcom/squareup/protos/client/bills/FeeLineItem;Lcom/squareup/util/TaxBreakdown$1;)V
    .locals 0

    .line 66
    invoke-direct {p0, p1}, Lcom/squareup/util/TaxBreakdown$TaxInformation;-><init>(Lcom/squareup/protos/client/bills/FeeLineItem;)V

    return-void
.end method
