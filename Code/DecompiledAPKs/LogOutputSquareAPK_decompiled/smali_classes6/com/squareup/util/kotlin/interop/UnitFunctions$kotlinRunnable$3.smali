.class final Lcom/squareup/util/kotlin/interop/UnitFunctions$kotlinRunnable$3;
.super Lkotlin/jvm/internal/Lambda;
.source "UnitFunctions.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/util/kotlin/interop/UnitFunctions;->kotlinRunnable(Lcom/squareup/util/kotlin/interop/KotlinRunnable2;)Lkotlin/jvm/functions/Function2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "TT1;TT2;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0006\u0010\u0000\u001a\u00020\u0001\"\u0004\u0008\u0000\u0010\u0002\"\u0004\u0008\u0001\u0010\u00032\u0006\u0010\u0004\u001a\u0002H\u00022\u0006\u0010\u0005\u001a\u0002H\u0003H\n\u00a2\u0006\u0004\u0008\u0006\u0010\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "T1",
        "T2",
        "arg1",
        "arg2",
        "invoke",
        "(Ljava/lang/Object;Ljava/lang/Object;)V"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $runnable:Lcom/squareup/util/kotlin/interop/KotlinRunnable2;


# direct methods
.method constructor <init>(Lcom/squareup/util/kotlin/interop/KotlinRunnable2;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/util/kotlin/interop/UnitFunctions$kotlinRunnable$3;->$runnable:Lcom/squareup/util/kotlin/interop/KotlinRunnable2;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    invoke-virtual {p0, p1, p2}, Lcom/squareup/util/kotlin/interop/UnitFunctions$kotlinRunnable$3;->invoke(Ljava/lang/Object;Ljava/lang/Object;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT1;TT2;)V"
        }
    .end annotation

    .line 58
    iget-object v0, p0, Lcom/squareup/util/kotlin/interop/UnitFunctions$kotlinRunnable$3;->$runnable:Lcom/squareup/util/kotlin/interop/KotlinRunnable2;

    invoke-interface {v0, p1, p2}, Lcom/squareup/util/kotlin/interop/KotlinRunnable2;->run(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method
