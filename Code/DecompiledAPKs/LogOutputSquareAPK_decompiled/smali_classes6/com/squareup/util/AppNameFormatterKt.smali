.class public final Lcom/squareup/util/AppNameFormatterKt;
.super Ljava/lang/Object;
.source "AppNameFormatter.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\r\n\u0000\u001a\u0012\u0010\u0000\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003\u001a\u0012\u0010\u0000\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u0004\u001a\u00020\u0005\u001a\u001c\u0010\u0000\u001a\n \u0006*\u0004\u0018\u00010\u00010\u0001*\u00020\u00012\u0006\u0010\u0007\u001a\u00020\u0008H\u0002\u00a8\u0006\t"
    }
    d2 = {
        "putAppName",
        "Lcom/squareup/phrase/Phrase;",
        "resources",
        "Landroid/content/res/Resources;",
        "res",
        "Lcom/squareup/util/Res;",
        "kotlin.jvm.PlatformType",
        "appName",
        "",
        "android-hairball_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final putAppName(Lcom/squareup/phrase/Phrase;Landroid/content/res/Resources;)Lcom/squareup/phrase/Phrase;
    .locals 1

    const-string v0, "$this$putAppName"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    sget v0, Lcom/squareup/utilities/R$string;->app_name_displayed_in_app:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v0, "resources.getString(R.st\u2026pp_name_displayed_in_app)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {p0, p1}, Lcom/squareup/util/AppNameFormatterKt;->putAppName(Lcom/squareup/phrase/Phrase;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    const-string p1, "putAppName(resources.get\u2026p_name_displayed_in_app))"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final putAppName(Lcom/squareup/phrase/Phrase;Lcom/squareup/util/Res;)Lcom/squareup/phrase/Phrase;
    .locals 1

    const-string v0, "$this$putAppName"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    sget v0, Lcom/squareup/utilities/R$string;->app_name_displayed_in_app:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {p0, p1}, Lcom/squareup/util/AppNameFormatterKt;->putAppName(Lcom/squareup/phrase/Phrase;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    const-string p1, "putAppName(res.getString\u2026p_name_displayed_in_app))"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method private static final putAppName(Lcom/squareup/phrase/Phrase;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;
    .locals 1

    const-string v0, "app_name"

    .line 37
    invoke-virtual {p0, v0, p1}, Lcom/squareup/phrase/Phrase;->putOptional(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    return-object p0
.end method
