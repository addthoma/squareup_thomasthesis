.class public final Lcom/squareup/util/Parcels;
.super Ljava/lang/Object;
.source "Parcels.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nParcels.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Parcels.kt\ncom/squareup/util/Parcels\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,70:1\n23#1:71\n23#1:72\n1642#2,2:73\n*E\n*S KotlinDebug\n*F\n+ 1 Parcels.kt\ncom/squareup/util/Parcels\n*L\n15#1:71\n19#1:72\n53#1,2:73\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0000\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0008\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u001a)\u0010\u0003\u001a\n\u0012\u0006\u0012\u0004\u0018\u0001H\u00050\u0004\"\u0012\u0008\u0000\u0010\u0005\u0018\u0001*\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006*\u00020\u0002H\u0086\u0008\u001a(\u0010\u0007\u001a\u0004\u0018\u0001H\u0005\"\u0012\u0008\u0000\u0010\u0005\u0018\u0001*\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006*\u00020\u0002H\u0086\u0008\u00a2\u0006\u0002\u0010\u0008\u001a1\u0010\u0007\u001a\u0004\u0018\u0001H\u0005\"\u0010\u0008\u0000\u0010\u0005*\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006*\u00020\u00022\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u0002H\u00050\n\u00a2\u0006\u0002\u0010\u000b\u001a\'\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u0002H\u00050\u0004\"\u0012\u0008\u0000\u0010\u0005\u0018\u0001*\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006*\u00020\u0002H\u0086\u0008\u001a\u0012\u0010\r\u001a\u00020\u000e*\u00020\u00022\u0006\u0010\u000f\u001a\u00020\u0001\u001a,\u0010\u0010\u001a\u00020\u000e\"\u0010\u0008\u0000\u0010\u0005*\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006*\u00020\u00022\u000e\u0010\u0011\u001a\n\u0012\u0006\u0012\u0004\u0018\u0001H\u00050\u0004\u001a+\u0010\u0012\u001a\u00020\u000e\"\u0010\u0008\u0000\u0010\u0005*\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006*\u00020\u00022\u0008\u0010\u0013\u001a\u0004\u0018\u0001H\u0005\u00a2\u0006\u0002\u0010\u0014\u001a*\u0010\u0015\u001a\u00020\u000e\"\u0010\u0008\u0000\u0010\u0005*\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006*\u00020\u00022\u000c\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u0002H\u00050\u0004\u00a8\u0006\u0016"
    }
    d2 = {
        "readBooleanFromInt",
        "",
        "Landroid/os/Parcel;",
        "readNullableProtoMessages",
        "",
        "T",
        "Lcom/squareup/wire/Message;",
        "readProtoMessage",
        "(Landroid/os/Parcel;)Lcom/squareup/wire/Message;",
        "protoClass",
        "Ljava/lang/Class;",
        "(Landroid/os/Parcel;Ljava/lang/Class;)Lcom/squareup/wire/Message;",
        "readProtoMessages",
        "writeBooleanAsInt",
        "",
        "bool",
        "writeNullableProtoMessages",
        "messages",
        "writeProtoMessage",
        "message",
        "(Landroid/os/Parcel;Lcom/squareup/wire/Message;)V",
        "writeProtoMessages",
        "proto-utilities_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final readBooleanFromInt(Landroid/os/Parcel;)Z
    .locals 1

    const-string v0, "$this$readBooleanFromInt"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result p0

    const/4 v0, 0x1

    if-ne v0, p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static final synthetic readNullableProtoMessages(Landroid/os/Parcel;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/wire/Message<",
            "**>;>(",
            "Landroid/os/Parcel;",
            ")",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "$this$readNullableProtoMessages"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    const/4 v3, 0x4

    const-string v4, "T"

    .line 72
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v3, Lcom/squareup/wire/Message;

    invoke-static {p0, v3}, Lcom/squareup/util/Parcels;->readProtoMessage(Landroid/os/Parcel;Ljava/lang/Class;)Lcom/squareup/wire/Message;

    move-result-object v3

    .line 19
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    check-cast v1, Ljava/util/List;

    return-object v1
.end method

.method public static final synthetic readProtoMessage(Landroid/os/Parcel;)Lcom/squareup/wire/Message;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/wire/Message<",
            "**>;>(",
            "Landroid/os/Parcel;",
            ")TT;"
        }
    .end annotation

    const-string v0, "$this$readProtoMessage"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x4

    const-string v1, "T"

    .line 23
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v0, Lcom/squareup/wire/Message;

    invoke-static {p0, v0}, Lcom/squareup/util/Parcels;->readProtoMessage(Landroid/os/Parcel;Ljava/lang/Class;)Lcom/squareup/wire/Message;

    move-result-object p0

    return-object p0
.end method

.method public static final readProtoMessage(Landroid/os/Parcel;Ljava/lang/Class;)Lcom/squareup/wire/Message;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/wire/Message<",
            "**>;>(",
            "Landroid/os/Parcel;",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    const-string v0, "$this$readProtoMessage"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "protoClass"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 32
    :cond_0
    new-array v0, v0, [B

    .line 33
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readByteArray([B)V

    .line 35
    :try_start_0
    sget-object p0, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    invoke-virtual {p0, p1}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object p0

    .line 36
    invoke-virtual {p0, v0}, Lcom/squareup/wire/ProtoAdapter;->decode([B)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/wire/Message;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    .line 38
    check-cast p0, Ljava/lang/Throwable;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "Failed to read message: %s"

    invoke-static {p0, p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 39
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public static final synthetic readProtoMessages(Landroid/os/Parcel;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/wire/Message<",
            "**>;>(",
            "Landroid/os/Parcel;",
            ")",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "$this$readProtoMessages"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_1

    const/4 v3, 0x4

    const-string v4, "T"

    .line 71
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v3, Lcom/squareup/wire/Message;

    invoke-static {p0, v3}, Lcom/squareup/util/Parcels;->readProtoMessage(Landroid/os/Parcel;Ljava/lang/Class;)Lcom/squareup/wire/Message;

    move-result-object v3

    if-nez v3, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 15
    :cond_0
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    check-cast v1, Ljava/util/List;

    return-object v1
.end method

.method public static final writeBooleanAsInt(Landroid/os/Parcel;Z)V
    .locals 1

    const-string v0, "$this$writeBooleanAsInt"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    invoke-virtual {p0, p1}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method

.method public static final writeNullableProtoMessages(Landroid/os/Parcel;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/wire/Message<",
            "**>;>(",
            "Landroid/os/Parcel;",
            "Ljava/util/List<",
            "+TT;>;)V"
        }
    .end annotation

    const-string v0, "$this$writeNullableProtoMessages"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "messages"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 53
    check-cast p1, Ljava/lang/Iterable;

    .line 73
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    .line 53
    invoke-static {p0, v0}, Lcom/squareup/util/Parcels;->writeProtoMessage(Landroid/os/Parcel;Lcom/squareup/wire/Message;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public static final writeProtoMessage(Landroid/os/Parcel;Lcom/squareup/wire/Message;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/wire/Message<",
            "**>;>(",
            "Landroid/os/Parcel;",
            "TT;)V"
        }
    .end annotation

    const-string v0, "$this$writeProtoMessage"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 58
    invoke-virtual {p0, p1}, Landroid/os/Parcel;->writeInt(I)V

    return-void

    .line 61
    :cond_0
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    .line 62
    invoke-virtual {v0, p1}, Lcom/squareup/wire/ProtoAdapter;->encode(Ljava/lang/Object;)[B

    move-result-object p1

    .line 63
    array-length v0, p1

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 64
    invoke-virtual {p0, p1}, Landroid/os/Parcel;->writeByteArray([B)V

    return-void
.end method

.method public static final writeProtoMessages(Landroid/os/Parcel;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/wire/Message<",
            "**>;>(",
            "Landroid/os/Parcel;",
            "Ljava/util/List<",
            "+TT;>;)V"
        }
    .end annotation

    const-string v0, "$this$writeProtoMessages"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "messages"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-static {p0, p1}, Lcom/squareup/util/Parcels;->writeNullableProtoMessages(Landroid/os/Parcel;Ljava/util/List;)V

    return-void
.end method
