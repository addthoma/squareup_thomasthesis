.class public final Lcom/squareup/util/Percentage$Companion;
.super Ljava/lang/Object;
.source "Percentage.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/util/Percentage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0010\n\u0002\u0010\u0006\n\u0002\u0008\u0006\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0014\u001a\u00020\u00072\u0006\u0010\u0015\u001a\u00020\u0004H\u0007J\u0010\u0010\u0016\u001a\u00020\u00072\u0006\u0010\u0017\u001a\u00020\u0018H\u0007J\u0010\u0010\u0019\u001a\u00020\u00072\u0006\u0010\u0017\u001a\u00020\u0004H\u0007J\u0019\u0010\u001a\u001a\u0004\u0018\u00010\u00072\u0008\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u0007\u00a2\u0006\u0002\u0010\u001bJ\u0010\u0010\u001c\u001a\u00020\u00072\u0006\u0010\u001d\u001a\u00020\u0018H\u0007J\u0010\u0010\u001e\u001a\u00020\u00072\u0006\u0010\u0017\u001a\u00020\u001fH\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u00020\u00078\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0008\u001a\u00020\u00078\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u00020\u00078\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u00020\u00078\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0010\u001a\u00020\u00078\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0011\u001a\u00020\u00078\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0012\u001a\u00020\u00078\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0013\u001a\u00020\u00078\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/util/Percentage$Companion;",
        "",
        "()V",
        "BASIS_POINT_TO_VALUE_ROUNDING",
        "",
        "BASIS_POINT_TO_VALUE_SCALE",
        "FIFTEEN",
        "Lcom/squareup/util/Percentage;",
        "NEGATIVE_ONE",
        "ONE",
        "ONE_HUNDRED",
        "PERCENT_OF_ROUNDING",
        "PERCENT_OF_SCALE",
        "PERCENT_OF_SHIFT",
        "SCALE",
        "SHIFT",
        "TEN",
        "TWENTY",
        "TWENTY_FIVE",
        "ZERO",
        "fromBasisPoints",
        "basisPoints",
        "fromDouble",
        "percentage",
        "",
        "fromInt",
        "fromNullableDouble",
        "(Ljava/lang/Double;)Lcom/squareup/util/Percentage;",
        "fromRate",
        "rate",
        "fromString",
        "",
        "public"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 23
    invoke-direct {p0}, Lcom/squareup/util/Percentage$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final fromBasisPoints(I)Lcom/squareup/util/Percentage;
    .locals 2
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 76
    new-instance v0, Lcom/squareup/util/Percentage;

    mul-int/lit16 p1, p1, 0x3e8

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/squareup/util/Percentage;-><init>(ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method

.method public final fromDouble(D)Lcom/squareup/util/Percentage;
    .locals 3
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 57
    new-instance v0, Lcom/squareup/util/Percentage;

    const v1, 0x186a0

    int-to-double v1, v1

    mul-double p1, p1, v1

    invoke-static {p1, p2}, Ljava/lang/Math;->round(D)J

    move-result-wide p1

    long-to-int p2, p1

    const/4 p1, 0x0

    invoke-direct {v0, p2, p1}, Lcom/squareup/util/Percentage;-><init>(ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method

.method public final fromInt(I)Lcom/squareup/util/Percentage;
    .locals 2
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 53
    new-instance v0, Lcom/squareup/util/Percentage;

    const v1, 0x186a0

    mul-int p1, p1, v1

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/squareup/util/Percentage;-><init>(ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method

.method public final fromNullableDouble(Ljava/lang/Double;)Lcom/squareup/util/Percentage;
    .locals 3
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    if-eqz p1, :cond_0

    .line 62
    move-object v0, p0

    check-cast v0, Lcom/squareup/util/Percentage$Companion;

    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/squareup/util/Percentage$Companion;->fromDouble(D)Lcom/squareup/util/Percentage;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public final fromRate(D)Lcom/squareup/util/Percentage;
    .locals 3
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 89
    move-object v0, p0

    check-cast v0, Lcom/squareup/util/Percentage$Companion;

    const/16 v1, 0x64

    int-to-double v1, v1

    mul-double p1, p1, v1

    invoke-virtual {v0, p1, p2}, Lcom/squareup/util/Percentage$Companion;->fromDouble(D)Lcom/squareup/util/Percentage;

    move-result-object p1

    return-object p1
.end method

.method public final fromString(Ljava/lang/String;)Lcom/squareup/util/Percentage;
    .locals 3
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "percentage"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    move-object v0, p0

    check-cast v0, Lcom/squareup/util/Percentage$Companion;

    invoke-static {p1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/squareup/util/Percentage$Companion;->fromDouble(D)Lcom/squareup/util/Percentage;

    move-result-object p1

    return-object p1
.end method
