.class public final Lcom/squareup/util/Dialogs;
.super Ljava/lang/Object;
.source "Dialogs.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static ignoresSearchKeyListener()Landroid/content/DialogInterface$OnKeyListener;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/util/-$$Lambda$Dialogs$7SDrZ5lA7b3J7tsTPJJzIO_Ztt8;->INSTANCE:Lcom/squareup/util/-$$Lambda$Dialogs$7SDrZ5lA7b3J7tsTPJJzIO_Ztt8;

    return-object v0
.end method

.method static synthetic lambda$ignoresSearchKeyListener$0(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 0

    const/16 p0, 0x54

    if-ne p1, p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method
