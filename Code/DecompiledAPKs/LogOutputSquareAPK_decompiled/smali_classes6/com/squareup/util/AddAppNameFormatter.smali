.class public final Lcom/squareup/util/AddAppNameFormatter;
.super Ljava/lang/Object;
.source "AppNameFormatter.kt"

# interfaces
.implements Lcom/squareup/util/AppNameFormatter;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\r\n\u0000\n\u0002\u0010\u0008\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/util/AddAppNameFormatter;",
        "Lcom/squareup/util/AppNameFormatter;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/util/Res;)V",
        "getStringWithAppName",
        "",
        "messageId",
        "",
        "android-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/util/AddAppNameFormatter;->res:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public getStringWithAppName(I)Ljava/lang/CharSequence;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/util/AddAppNameFormatter;->res:Lcom/squareup/util/Res;

    invoke-interface {v0, p1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 24
    iget-object v0, p0, Lcom/squareup/util/AddAppNameFormatter;->res:Lcom/squareup/util/Res;

    invoke-static {p1, v0}, Lcom/squareup/util/AppNameFormatterKt;->putAppName(Lcom/squareup/phrase/Phrase;Lcom/squareup/util/Res;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 25
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    const-string v0, "res.phrase(messageId)\n  \u2026me(res)\n        .format()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
