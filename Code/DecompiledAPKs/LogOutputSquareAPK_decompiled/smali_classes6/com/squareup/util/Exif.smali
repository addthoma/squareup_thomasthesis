.class public Lcom/squareup/util/Exif;
.super Ljava/lang/Object;
.source "Exif.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/util/Exif$Orientation;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static orientation(Ljava/io/File;)Lcom/squareup/util/Exif$Orientation;
    .locals 3

    .line 33
    :try_start_0
    new-instance v0, Landroid/media/ExifInterface;

    .line 34
    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    const-string v1, "Orientation"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_1

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    .line 45
    sget-object p0, Lcom/squareup/util/Exif$Orientation;->NORMAL:Lcom/squareup/util/Exif$Orientation;

    return-object p0

    .line 43
    :cond_0
    sget-object p0, Lcom/squareup/util/Exif$Orientation;->ROTATE_270:Lcom/squareup/util/Exif$Orientation;

    return-object p0

    .line 39
    :cond_1
    sget-object p0, Lcom/squareup/util/Exif$Orientation;->ROTATE_90:Lcom/squareup/util/Exif$Orientation;

    return-object p0

    .line 41
    :cond_2
    sget-object p0, Lcom/squareup/util/Exif$Orientation;->ROTATE_180:Lcom/squareup/util/Exif$Orientation;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception v0

    .line 48
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to create ExifInterface for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 49
    sget-object p0, Lcom/squareup/util/Exif$Orientation;->NORMAL:Lcom/squareup/util/Exif$Orientation;

    return-object p0
.end method
