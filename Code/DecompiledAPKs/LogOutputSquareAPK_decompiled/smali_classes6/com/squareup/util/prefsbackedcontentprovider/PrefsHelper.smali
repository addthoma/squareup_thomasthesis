.class public interface abstract Lcom/squareup/util/prefsbackedcontentprovider/PrefsHelper;
.super Ljava/lang/Object;
.source "PrefsHelper.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0008`\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H&J\u0018\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\tH&J\u0008\u0010\n\u001a\u00020\u000bH&\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/util/prefsbackedcontentprovider/PrefsHelper;",
        "",
        "doQuery",
        "Landroid/database/Cursor;",
        "doUpdate",
        "",
        "uri",
        "Landroid/net/Uri;",
        "values",
        "Landroid/content/ContentValues;",
        "hasPref",
        "",
        "android-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract doQuery()Landroid/database/Cursor;
.end method

.method public abstract doUpdate(Landroid/net/Uri;Landroid/content/ContentValues;)V
.end method

.method public abstract hasPref()Z
.end method
