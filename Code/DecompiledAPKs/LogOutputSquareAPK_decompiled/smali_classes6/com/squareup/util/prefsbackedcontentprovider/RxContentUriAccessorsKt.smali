.class public final Lcom/squareup/util/prefsbackedcontentprovider/RxContentUriAccessorsKt;
.super Ljava/lang/Object;
.source "RxContentUriAccessors.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRxContentUriAccessors.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RxContentUriAccessors.kt\ncom/squareup/util/prefsbackedcontentprovider/RxContentUriAccessorsKt\n+ 2 RxContentUriSingleItemAccessor.kt\ncom/squareup/util/RxContentUriSingleItemAccessorKt\n*L\n1#1,18:1\n17#2,5:19\n*E\n*S KotlinDebug\n*F\n+ 1 RxContentUriAccessors.kt\ncom/squareup/util/prefsbackedcontentprovider/RxContentUriAccessorsKt\n*L\n16#1,5:19\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a9\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u0002H\u00020\u0001\"\n\u0008\u0000\u0010\u0002\u0018\u0001*\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u0002H\u00020\tH\u0086\u0008\u00a8\u0006\n"
    }
    d2 = {
        "getRxContentUriSingleItemAccessor",
        "Lcom/squareup/util/RxContentUriSingleItemAccessor;",
        "T",
        "",
        "context",
        "Landroid/content/Context;",
        "contract",
        "Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProviderContract;",
        "contractItem",
        "Lcom/squareup/util/prefsbackedcontentprovider/ContractItem;",
        "android-hairball_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic getRxContentUriSingleItemAccessor(Landroid/content/Context;Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProviderContract;Lcom/squareup/util/prefsbackedcontentprovider/ContractItem;)Lcom/squareup/util/RxContentUriSingleItemAccessor;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/content/Context;",
            "Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProviderContract;",
            "Lcom/squareup/util/prefsbackedcontentprovider/ContractItem<",
            "TT;>;)",
            "Lcom/squareup/util/RxContentUriSingleItemAccessor<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "contract"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "contractItem"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-interface {p1}, Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProviderContract;->getItems()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 15
    invoke-interface {p1}, Lcom/squareup/util/prefsbackedcontentprovider/PrefsBackedContentProviderContract;->getAuthority()Ljava/lang/String;

    move-result-object p1

    invoke-interface {p2}, Lcom/squareup/util/prefsbackedcontentprovider/ContractItem;->getPath()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/squareup/util/Urls;->getContentUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    const-string p2, "contentUri"

    .line 16
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p2, 0x4

    const-string v0, "T"

    .line 19
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class p2, Ljava/lang/Object;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    .line 20
    sget-object v0, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_1

    sget-object p2, Lcom/squareup/util/AccessorBoolStrategy;->INSTANCE:Lcom/squareup/util/AccessorBoolStrategy;

    if-eqz p2, :cond_0

    .line 21
    check-cast p2, Lcom/squareup/util/AccessorStrategy;

    .line 23
    new-instance v0, Lcom/squareup/util/RxContentUriSingleItemAccessor;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/util/RxContentUriSingleItemAccessor;-><init>(Landroid/content/Context;Landroid/net/Uri;Lcom/squareup/util/AccessorStrategy;)V

    return-object v0

    .line 21
    :cond_0
    new-instance p0, Lkotlin/TypeCastException;

    const-string p1, "null cannot be cast to non-null type com.squareup.util.AccessorStrategy<T>"

    invoke-direct {p0, p1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_1
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "Unsupported type"

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0

    .line 14
    :cond_2
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string p1, "contract must contain "

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
