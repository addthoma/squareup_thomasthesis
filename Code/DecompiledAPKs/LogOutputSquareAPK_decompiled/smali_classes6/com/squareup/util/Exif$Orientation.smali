.class public final enum Lcom/squareup/util/Exif$Orientation;
.super Ljava/lang/Enum;
.source "Exif.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/util/Exif;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Orientation"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/util/Exif$Orientation;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/util/Exif$Orientation;

.field public static final enum NORMAL:Lcom/squareup/util/Exif$Orientation;

.field public static final enum ROTATE_180:Lcom/squareup/util/Exif$Orientation;

.field public static final enum ROTATE_270:Lcom/squareup/util/Exif$Orientation;

.field public static final enum ROTATE_90:Lcom/squareup/util/Exif$Orientation;


# instance fields
.field private final rotation:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 15
    new-instance v0, Lcom/squareup/util/Exif$Orientation;

    const/4 v1, 0x0

    const-string v2, "NORMAL"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/util/Exif$Orientation;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/util/Exif$Orientation;->NORMAL:Lcom/squareup/util/Exif$Orientation;

    new-instance v0, Lcom/squareup/util/Exif$Orientation;

    const/4 v2, 0x1

    const-string v3, "ROTATE_90"

    const/16 v4, 0x5a

    invoke-direct {v0, v3, v2, v4}, Lcom/squareup/util/Exif$Orientation;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/util/Exif$Orientation;->ROTATE_90:Lcom/squareup/util/Exif$Orientation;

    new-instance v0, Lcom/squareup/util/Exif$Orientation;

    const/4 v3, 0x2

    const-string v4, "ROTATE_180"

    const/16 v5, 0xb4

    invoke-direct {v0, v4, v3, v5}, Lcom/squareup/util/Exif$Orientation;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/util/Exif$Orientation;->ROTATE_180:Lcom/squareup/util/Exif$Orientation;

    new-instance v0, Lcom/squareup/util/Exif$Orientation;

    const/4 v4, 0x3

    const-string v5, "ROTATE_270"

    const/16 v6, 0x10e

    invoke-direct {v0, v5, v4, v6}, Lcom/squareup/util/Exif$Orientation;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/util/Exif$Orientation;->ROTATE_270:Lcom/squareup/util/Exif$Orientation;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/util/Exif$Orientation;

    .line 14
    sget-object v5, Lcom/squareup/util/Exif$Orientation;->NORMAL:Lcom/squareup/util/Exif$Orientation;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/util/Exif$Orientation;->ROTATE_90:Lcom/squareup/util/Exif$Orientation;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/util/Exif$Orientation;->ROTATE_180:Lcom/squareup/util/Exif$Orientation;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/util/Exif$Orientation;->ROTATE_270:Lcom/squareup/util/Exif$Orientation;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/util/Exif$Orientation;->$VALUES:[Lcom/squareup/util/Exif$Orientation;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 20
    iput p3, p0, Lcom/squareup/util/Exif$Orientation;->rotation:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/util/Exif$Orientation;
    .locals 1

    .line 14
    const-class v0, Lcom/squareup/util/Exif$Orientation;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/util/Exif$Orientation;

    return-object p0
.end method

.method public static values()[Lcom/squareup/util/Exif$Orientation;
    .locals 1

    .line 14
    sget-object v0, Lcom/squareup/util/Exif$Orientation;->$VALUES:[Lcom/squareup/util/Exif$Orientation;

    invoke-virtual {v0}, [Lcom/squareup/util/Exif$Orientation;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/util/Exif$Orientation;

    return-object v0
.end method


# virtual methods
.method public getRotation()I
    .locals 1

    .line 26
    iget v0, p0, Lcom/squareup/util/Exif$Orientation;->rotation:I

    return v0
.end method
