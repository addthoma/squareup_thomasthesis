.class final Lcom/squareup/util/OptionalExtensionsRx1Kt$mapToOptionalSingle$1;
.super Ljava/lang/Object;
.source "OptionalExtensionsRx1.kt"

# interfaces
.implements Lrx/Single$Transformer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/util/OptionalExtensionsRx1Kt;->mapToOptionalSingle()Lrx/Single$Transformer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/Single$Transformer<",
        "TT;",
        "Lcom/squareup/util/Optional<",
        "+TT;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\u0010\u0000\u001a\u0016\u0012\u0012\u0012\u0010\u0012\u000c\u0012\n \u0004*\u0004\u0018\u0001H\u0003H\u00030\u00020\u0001\"\u0008\u0008\u0000\u0010\u0003*\u00020\u00052*\u0010\u0006\u001a&\u0012\u000c\u0012\n \u0004*\u0004\u0018\u0001H\u0003H\u0003 \u0004*\u0012\u0012\u000c\u0012\n \u0004*\u0004\u0018\u0001H\u0003H\u0003\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lrx/Single;",
        "Lcom/squareup/util/Optional;",
        "T",
        "kotlin.jvm.PlatformType",
        "",
        "it",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/util/OptionalExtensionsRx1Kt$mapToOptionalSingle$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/util/OptionalExtensionsRx1Kt$mapToOptionalSingle$1;

    invoke-direct {v0}, Lcom/squareup/util/OptionalExtensionsRx1Kt$mapToOptionalSingle$1;-><init>()V

    sput-object v0, Lcom/squareup/util/OptionalExtensionsRx1Kt$mapToOptionalSingle$1;->INSTANCE:Lcom/squareup/util/OptionalExtensionsRx1Kt$mapToOptionalSingle$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 49
    check-cast p1, Lrx/Single;

    invoke-virtual {p0, p1}, Lcom/squareup/util/OptionalExtensionsRx1Kt$mapToOptionalSingle$1;->call(Lrx/Single;)Lrx/Single;

    move-result-object p1

    return-object p1
.end method

.method public final call(Lrx/Single;)Lrx/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Single<",
            "TT;>;)",
            "Lrx/Single<",
            "Lcom/squareup/util/Optional<",
            "TT;>;>;"
        }
    .end annotation

    const-string v0, "it"

    .line 49
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/util/OptionalExtensionsRx1Kt;->mapToOptional(Lrx/Single;)Lrx/Single;

    move-result-object p1

    return-object p1
.end method
