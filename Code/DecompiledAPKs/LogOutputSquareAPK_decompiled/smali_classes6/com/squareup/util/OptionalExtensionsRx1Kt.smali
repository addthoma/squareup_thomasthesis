.class public final Lcom/squareup/util/OptionalExtensionsRx1Kt;
.super Ljava/lang/Object;
.source "OptionalExtensionsRx1.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOptionalExtensionsRx1.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OptionalExtensionsRx1.kt\ncom/squareup/util/OptionalExtensionsRx1Kt\n+ 2 Rx.kt\ncom/squareup/util/rx/RxKt\n*L\n1#1,58:1\n18#2:59\n*E\n*S KotlinDebug\n*F\n+ 1 OptionalExtensionsRx1.kt\ncom/squareup/util/OptionalExtensionsRx1Kt\n*L\n11#1:59\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\"\u0010\u0000\u001a\u0014\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u00030\u0002\u0012\u0004\u0012\u0002H\u00030\u0001\"\u0008\u0008\u0000\u0010\u0003*\u00020\u0004\u001a\"\u0010\u0005\u001a\u0014\u0012\u0004\u0012\u0002H\u0003\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u00030\u00020\u0001\"\u0008\u0008\u0000\u0010\u0003*\u00020\u0004\u001a\"\u0010\u0006\u001a\u0014\u0012\u0004\u0012\u0002H\u0003\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u00030\u00020\u0007\"\u0008\u0008\u0000\u0010\u0003*\u00020\u0004\u001a&\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u0002H\u00030\u0008\"\u0008\u0008\u0000\u0010\u0003*\u00020\u0004*\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u00030\u00020\u0008\u001a&\u0010\u0005\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u00030\u00020\u0008\"\u0008\u0008\u0000\u0010\u0003*\u00020\u0004*\u0008\u0012\u0004\u0012\u0002H\u00030\u0008\u001a&\u0010\u0005\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u00030\u00020\t\"\u0008\u0008\u0000\u0010\u0003*\u00020\u0004*\u0008\u0012\u0004\u0012\u0002H\u00030\t\u001a&\u0010\n\u001a\u0008\u0012\u0004\u0012\u0002H\u00030\u000b\"\u0008\u0008\u0000\u0010\u0003*\u00020\u0004*\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u00030\u00020\t\u00a8\u0006\u000c"
    }
    d2 = {
        "mapIfPresent",
        "Lrx/Observable$Transformer;",
        "Lcom/squareup/util/Optional;",
        "T",
        "",
        "mapToOptional",
        "mapToOptionalSingle",
        "Lrx/Single$Transformer;",
        "Lrx/Observable;",
        "Lrx/Single;",
        "takeIfPresent",
        "Lio/reactivex/Maybe;",
        "pure"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final mapIfPresent()Lrx/Observable$Transformer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lrx/Observable$Transformer<",
            "Lcom/squareup/util/Optional<",
            "TT;>;TT;>;"
        }
    .end annotation

    .line 19
    sget-object v0, Lcom/squareup/util/OptionalExtensionsRx1Kt$mapIfPresent$2;->INSTANCE:Lcom/squareup/util/OptionalExtensionsRx1Kt$mapIfPresent$2;

    check-cast v0, Lrx/Observable$Transformer;

    return-object v0
.end method

.method public static final mapIfPresent(Lrx/Observable;)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/Observable<",
            "Lcom/squareup/util/Optional<",
            "TT;>;>;)",
            "Lrx/Observable<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "$this$mapIfPresent"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    const-class v0, Lcom/squareup/util/Optional$Present;

    invoke-virtual {p0, v0}, Lrx/Observable;->ofType(Ljava/lang/Class;)Lrx/Observable;

    move-result-object p0

    const-string v0, "ofType(T::class.java)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    sget-object v0, Lcom/squareup/util/OptionalExtensionsRx1Kt$mapIfPresent$1;->INSTANCE:Lcom/squareup/util/OptionalExtensionsRx1Kt$mapIfPresent$1;

    check-cast v0, Lrx/functions/Func1;

    invoke-virtual {p0, v0}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p0

    const-string v0, "ofType<Present<T>>().map { it.value }"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final mapToOptional()Lrx/Observable$Transformer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lrx/Observable$Transformer<",
            "TT;",
            "Lcom/squareup/util/Optional<",
            "TT;>;>;"
        }
    .end annotation

    .line 34
    sget-object v0, Lcom/squareup/util/OptionalExtensionsRx1Kt$mapToOptional$2;->INSTANCE:Lcom/squareup/util/OptionalExtensionsRx1Kt$mapToOptional$2;

    check-cast v0, Lrx/Observable$Transformer;

    return-object v0
.end method

.method public static final mapToOptional(Lrx/Observable;)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/Observable<",
            "TT;>;)",
            "Lrx/Observable<",
            "Lcom/squareup/util/Optional<",
            "TT;>;>;"
        }
    .end annotation

    const-string v0, "$this$mapToOptional"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    sget-object v0, Lcom/squareup/util/OptionalExtensionsRx1Kt$mapToOptional$1;->INSTANCE:Lcom/squareup/util/OptionalExtensionsRx1Kt$mapToOptional$1;

    check-cast v0, Lrx/functions/Func1;

    invoke-virtual {p0, v0}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p0

    const-string v0, "map { Optional.ofNullable(it) }"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final mapToOptional(Lrx/Single;)Lrx/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/Single<",
            "TT;>;)",
            "Lrx/Single<",
            "Lcom/squareup/util/Optional<",
            "TT;>;>;"
        }
    .end annotation

    const-string v0, "$this$mapToOptional"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    sget-object v0, Lcom/squareup/util/OptionalExtensionsRx1Kt$mapToOptional$3;->INSTANCE:Lcom/squareup/util/OptionalExtensionsRx1Kt$mapToOptional$3;

    check-cast v0, Lrx/functions/Func1;

    invoke-virtual {p0, v0}, Lrx/Single;->map(Lrx/functions/Func1;)Lrx/Single;

    move-result-object p0

    const-string v0, "map { Optional.ofNullable(it) }"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final mapToOptionalSingle()Lrx/Single$Transformer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lrx/Single$Transformer<",
            "TT;",
            "Lcom/squareup/util/Optional<",
            "TT;>;>;"
        }
    .end annotation

    .line 49
    sget-object v0, Lcom/squareup/util/OptionalExtensionsRx1Kt$mapToOptionalSingle$1;->INSTANCE:Lcom/squareup/util/OptionalExtensionsRx1Kt$mapToOptionalSingle$1;

    check-cast v0, Lrx/Single$Transformer;

    return-object v0
.end method

.method public static final takeIfPresent(Lrx/Single;)Lio/reactivex/Maybe;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/Single<",
            "Lcom/squareup/util/Optional<",
            "TT;>;>;)",
            "Lio/reactivex/Maybe<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "$this$takeIfPresent"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    invoke-static {p0}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Single(Lrx/Single;)Lio/reactivex/Single;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/util/OptionalExtensionsKt;->takeIfPresent(Lio/reactivex/Single;)Lio/reactivex/Maybe;

    move-result-object p0

    return-object p0
.end method
