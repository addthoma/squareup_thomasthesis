.class public Lcom/squareup/util/DisplayRotator;
.super Ljava/lang/Object;
.source "DisplayRotator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/util/DisplayRotator$DisplayRotation;,
        Lcom/squareup/util/DisplayRotator$Orientation;,
        Lcom/squareup/util/DisplayRotator$NaturalOrientation;,
        Lcom/squareup/util/DisplayRotator$DefaultDisplay;
    }
.end annotation


# instance fields
.field private final display:Lcom/squareup/util/DisplayRotator$DefaultDisplay;


# direct methods
.method public constructor <init>(Landroid/view/WindowManager;Lcom/squareup/util/SystemSettings;)V
    .locals 1

    .line 183
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 184
    new-instance v0, Lcom/squareup/util/DisplayRotator$DefaultDisplay;

    invoke-direct {v0, p1, p2}, Lcom/squareup/util/DisplayRotator$DefaultDisplay;-><init>(Landroid/view/WindowManager;Lcom/squareup/util/SystemSettings;)V

    iput-object v0, p0, Lcom/squareup/util/DisplayRotator;->display:Lcom/squareup/util/DisplayRotator$DefaultDisplay;

    return-void
.end method

.method constructor <init>(Lcom/squareup/util/DisplayRotator$DefaultDisplay;)V
    .locals 0

    .line 187
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 188
    iput-object p1, p0, Lcom/squareup/util/DisplayRotator;->display:Lcom/squareup/util/DisplayRotator$DefaultDisplay;

    return-void
.end method

.method private apply90DegreeCounterClockwiseRotations(I)V
    .locals 5

    .line 295
    invoke-direct {p0}, Lcom/squareup/util/DisplayRotator;->getCurrentDisplayRotation()Lcom/squareup/util/DisplayRotator$DisplayRotation;

    move-result-object v0

    .line 297
    invoke-static {v0, p1}, Lcom/squareup/util/DisplayRotator$DisplayRotation;->access$300(Lcom/squareup/util/DisplayRotator$DisplayRotation;I)Lcom/squareup/util/DisplayRotator$DisplayRotation;

    move-result-object p1

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "user_rotation"

    aput-object v3, v1, v2

    const/4 v3, 0x1

    aput-object v0, v1, v3

    const/4 v0, 0x2

    aput-object p1, v1, v0

    const-string v4, "Setting %s: %s -> %s"

    .line 298
    invoke-static {v4, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 299
    iget-object v1, p0, Lcom/squareup/util/DisplayRotator;->display:Lcom/squareup/util/DisplayRotator$DefaultDisplay;

    invoke-static {p1}, Lcom/squareup/util/DisplayRotator$DisplayRotation;->access$400(Lcom/squareup/util/DisplayRotator$DisplayRotation;)I

    move-result p1

    invoke-virtual {v1, p1}, Lcom/squareup/util/DisplayRotator$DefaultDisplay;->setUserRotation(I)V

    .line 304
    iget-object p1, p0, Lcom/squareup/util/DisplayRotator;->display:Lcom/squareup/util/DisplayRotator$DefaultDisplay;

    invoke-virtual {p1}, Lcom/squareup/util/DisplayRotator$DefaultDisplay;->getAccelerometerRotationEnabled()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 305
    iget-object p1, p0, Lcom/squareup/util/DisplayRotator;->display:Lcom/squareup/util/DisplayRotator$DefaultDisplay;

    invoke-virtual {p1}, Lcom/squareup/util/DisplayRotator$DefaultDisplay;->getAccelerometerRotationEnabled()Z

    move-result p1

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "accelerometer_rotation"

    aput-object v1, v0, v2

    .line 306
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    aput-object p1, v0, v3

    const-string p1, "Setting %s: %s -> false"

    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 307
    iget-object p1, p0, Lcom/squareup/util/DisplayRotator;->display:Lcom/squareup/util/DisplayRotator$DefaultDisplay;

    invoke-virtual {p1, v2}, Lcom/squareup/util/DisplayRotator$DefaultDisplay;->setAccelerometerRotationEnabled(Z)V

    :cond_0
    return-void
.end method

.method private getCurrentDisplayRotation()Lcom/squareup/util/DisplayRotator$DisplayRotation;
    .locals 4

    .line 279
    iget-object v0, p0, Lcom/squareup/util/DisplayRotator;->display:Lcom/squareup/util/DisplayRotator$DefaultDisplay;

    invoke-virtual {v0}, Lcom/squareup/util/DisplayRotator$DefaultDisplay;->getRotation()I

    move-result v0

    if-eqz v0, :cond_3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 288
    sget-object v0, Lcom/squareup/util/DisplayRotator$DisplayRotation;->ROTATION_270:Lcom/squareup/util/DisplayRotator$DisplayRotation;

    return-object v0

    .line 290
    :cond_0
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid rotation: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 286
    :cond_1
    sget-object v0, Lcom/squareup/util/DisplayRotator$DisplayRotation;->ROTATION_180:Lcom/squareup/util/DisplayRotator$DisplayRotation;

    return-object v0

    .line 284
    :cond_2
    sget-object v0, Lcom/squareup/util/DisplayRotator$DisplayRotation;->ROTATION_90:Lcom/squareup/util/DisplayRotator$DisplayRotation;

    return-object v0

    .line 282
    :cond_3
    sget-object v0, Lcom/squareup/util/DisplayRotator$DisplayRotation;->ROTATION_0:Lcom/squareup/util/DisplayRotator$DisplayRotation;

    return-object v0
.end method


# virtual methods
.method public getNaturalOrientation()Lcom/squareup/util/DisplayRotator$NaturalOrientation;
    .locals 5

    .line 197
    iget-object v0, p0, Lcom/squareup/util/DisplayRotator;->display:Lcom/squareup/util/DisplayRotator$DefaultDisplay;

    invoke-virtual {v0}, Lcom/squareup/util/DisplayRotator$DefaultDisplay;->getHeight()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/util/DisplayRotator;->display:Lcom/squareup/util/DisplayRotator$DefaultDisplay;

    invoke-virtual {v1}, Lcom/squareup/util/DisplayRotator$DefaultDisplay;->getWidth()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 198
    sget-object v0, Lcom/squareup/util/DisplayRotator$NaturalOrientation;->PORTRAIT:Lcom/squareup/util/DisplayRotator$NaturalOrientation;

    return-object v0

    .line 200
    :cond_0
    iget-object v0, p0, Lcom/squareup/util/DisplayRotator;->display:Lcom/squareup/util/DisplayRotator$DefaultDisplay;

    invoke-virtual {v0}, Lcom/squareup/util/DisplayRotator$DefaultDisplay;->getHeight()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/util/DisplayRotator;->display:Lcom/squareup/util/DisplayRotator$DefaultDisplay;

    invoke-virtual {v1}, Lcom/squareup/util/DisplayRotator$DefaultDisplay;->getWidth()I

    move-result v1

    const/4 v2, 0x1

    if-le v0, v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 201
    :goto_0
    invoke-direct {p0}, Lcom/squareup/util/DisplayRotator;->getCurrentDisplayRotation()Lcom/squareup/util/DisplayRotator$DisplayRotation;

    move-result-object v1

    .line 202
    sget-object v3, Lcom/squareup/util/DisplayRotator$1;->$SwitchMap$com$squareup$util$DisplayRotator$DisplayRotation:[I

    invoke-virtual {v1}, Lcom/squareup/util/DisplayRotator$DisplayRotation;->ordinal()I

    move-result v4

    aget v3, v3, v4

    if-eq v3, v2, :cond_5

    const/4 v2, 0x2

    if-eq v3, v2, :cond_5

    const/4 v2, 0x3

    if-eq v3, v2, :cond_3

    const/4 v2, 0x4

    if-ne v3, v2, :cond_2

    goto :goto_1

    .line 210
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid rotation: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    :goto_1
    if-eqz v0, :cond_4

    .line 208
    sget-object v0, Lcom/squareup/util/DisplayRotator$NaturalOrientation;->LANDSCAPE:Lcom/squareup/util/DisplayRotator$NaturalOrientation;

    goto :goto_2

    :cond_4
    sget-object v0, Lcom/squareup/util/DisplayRotator$NaturalOrientation;->PORTRAIT:Lcom/squareup/util/DisplayRotator$NaturalOrientation;

    :goto_2
    return-object v0

    :cond_5
    if-eqz v0, :cond_6

    .line 205
    sget-object v0, Lcom/squareup/util/DisplayRotator$NaturalOrientation;->PORTRAIT:Lcom/squareup/util/DisplayRotator$NaturalOrientation;

    goto :goto_3

    :cond_6
    sget-object v0, Lcom/squareup/util/DisplayRotator$NaturalOrientation;->LANDSCAPE:Lcom/squareup/util/DisplayRotator$NaturalOrientation;

    :goto_3
    return-object v0
.end method

.method public getOrientation()Lcom/squareup/util/DisplayRotator$Orientation;
    .locals 6

    .line 217
    iget-object v0, p0, Lcom/squareup/util/DisplayRotator;->display:Lcom/squareup/util/DisplayRotator$DefaultDisplay;

    invoke-virtual {v0}, Lcom/squareup/util/DisplayRotator$DefaultDisplay;->getHeight()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/util/DisplayRotator;->display:Lcom/squareup/util/DisplayRotator$DefaultDisplay;

    invoke-virtual {v1}, Lcom/squareup/util/DisplayRotator$DefaultDisplay;->getWidth()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 218
    :goto_0
    iget-object v1, p0, Lcom/squareup/util/DisplayRotator;->display:Lcom/squareup/util/DisplayRotator$DefaultDisplay;

    invoke-virtual {v1}, Lcom/squareup/util/DisplayRotator$DefaultDisplay;->getHeight()I

    move-result v1

    iget-object v4, p0, Lcom/squareup/util/DisplayRotator;->display:Lcom/squareup/util/DisplayRotator$DefaultDisplay;

    invoke-virtual {v4}, Lcom/squareup/util/DisplayRotator$DefaultDisplay;->getWidth()I

    move-result v4

    if-le v1, v4, :cond_1

    const/4 v2, 0x1

    .line 219
    :cond_1
    invoke-direct {p0}, Lcom/squareup/util/DisplayRotator;->getCurrentDisplayRotation()Lcom/squareup/util/DisplayRotator$DisplayRotation;

    move-result-object v1

    .line 220
    sget-object v4, Lcom/squareup/util/DisplayRotator$1;->$SwitchMap$com$squareup$util$DisplayRotator$DisplayRotation:[I

    invoke-virtual {v1}, Lcom/squareup/util/DisplayRotator$DisplayRotation;->ordinal()I

    move-result v5

    aget v4, v4, v5

    if-eq v4, v3, :cond_b

    const/4 v3, 0x2

    if-eq v4, v3, :cond_8

    const/4 v3, 0x3

    if-eq v4, v3, :cond_5

    const/4 v3, 0x4

    if-ne v4, v3, :cond_4

    if-eqz v0, :cond_2

    .line 231
    sget-object v0, Lcom/squareup/util/DisplayRotator$Orientation;->REVERSE_LANDSCAPE:Lcom/squareup/util/DisplayRotator$Orientation;

    return-object v0

    :cond_2
    if-eqz v2, :cond_3

    .line 232
    sget-object v0, Lcom/squareup/util/DisplayRotator$Orientation;->REVERSE_PORTRAIT:Lcom/squareup/util/DisplayRotator$Orientation;

    goto :goto_1

    :cond_3
    sget-object v0, Lcom/squareup/util/DisplayRotator$Orientation;->REVERSE_LANDSCAPE:Lcom/squareup/util/DisplayRotator$Orientation;

    :goto_1
    return-object v0

    .line 234
    :cond_4
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid rotation: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    if-eqz v0, :cond_6

    .line 225
    sget-object v0, Lcom/squareup/util/DisplayRotator$Orientation;->LANDSCAPE:Lcom/squareup/util/DisplayRotator$Orientation;

    return-object v0

    :cond_6
    if-eqz v2, :cond_7

    .line 226
    sget-object v0, Lcom/squareup/util/DisplayRotator$Orientation;->PORTRAIT:Lcom/squareup/util/DisplayRotator$Orientation;

    goto :goto_2

    :cond_7
    sget-object v0, Lcom/squareup/util/DisplayRotator$Orientation;->LANDSCAPE:Lcom/squareup/util/DisplayRotator$Orientation;

    :goto_2
    return-object v0

    :cond_8
    if-eqz v0, :cond_9

    .line 228
    sget-object v0, Lcom/squareup/util/DisplayRotator$Orientation;->REVERSE_PORTRAIT:Lcom/squareup/util/DisplayRotator$Orientation;

    return-object v0

    :cond_9
    if-eqz v2, :cond_a

    .line 229
    sget-object v0, Lcom/squareup/util/DisplayRotator$Orientation;->REVERSE_PORTRAIT:Lcom/squareup/util/DisplayRotator$Orientation;

    goto :goto_3

    :cond_a
    sget-object v0, Lcom/squareup/util/DisplayRotator$Orientation;->REVERSE_LANDSCAPE:Lcom/squareup/util/DisplayRotator$Orientation;

    :goto_3
    return-object v0

    :cond_b
    if-eqz v0, :cond_c

    .line 222
    sget-object v0, Lcom/squareup/util/DisplayRotator$Orientation;->PORTRAIT:Lcom/squareup/util/DisplayRotator$Orientation;

    return-object v0

    :cond_c
    if-eqz v2, :cond_d

    .line 223
    sget-object v0, Lcom/squareup/util/DisplayRotator$Orientation;->PORTRAIT:Lcom/squareup/util/DisplayRotator$Orientation;

    goto :goto_4

    :cond_d
    sget-object v0, Lcom/squareup/util/DisplayRotator$Orientation;->LANDSCAPE:Lcom/squareup/util/DisplayRotator$Orientation;

    :goto_4
    return-object v0
.end method

.method public rotate180DegreesCounterClockwise()V
    .locals 1

    const/4 v0, 0x2

    .line 266
    invoke-direct {p0, v0}, Lcom/squareup/util/DisplayRotator;->apply90DegreeCounterClockwiseRotations(I)V

    return-void
.end method

.method public rotate270DegreesCounterClockwise()V
    .locals 1

    const/4 v0, 0x3

    .line 275
    invoke-direct {p0, v0}, Lcom/squareup/util/DisplayRotator;->apply90DegreeCounterClockwiseRotations(I)V

    return-void
.end method

.method public rotate90DegreesCounterClockwise()V
    .locals 1

    const/4 v0, 0x1

    .line 257
    invoke-direct {p0, v0}, Lcom/squareup/util/DisplayRotator;->apply90DegreeCounterClockwiseRotations(I)V

    return-void
.end method

.method public setOrientation(Lcom/squareup/util/DisplayRotator$Orientation;)V
    .locals 2

    .line 244
    invoke-virtual {p0}, Lcom/squareup/util/DisplayRotator;->getNaturalOrientation()Lcom/squareup/util/DisplayRotator$NaturalOrientation;

    move-result-object v0

    .line 245
    invoke-virtual {p0}, Lcom/squareup/util/DisplayRotator;->getOrientation()Lcom/squareup/util/DisplayRotator$Orientation;

    move-result-object v1

    .line 247
    invoke-static {v0, v1, p1}, Lcom/squareup/util/DisplayRotator$NaturalOrientation;->access$200(Lcom/squareup/util/DisplayRotator$NaturalOrientation;Lcom/squareup/util/DisplayRotator$Orientation;Lcom/squareup/util/DisplayRotator$Orientation;)I

    move-result p1

    .line 248
    invoke-direct {p0, p1}, Lcom/squareup/util/DisplayRotator;->apply90DegreeCounterClockwiseRotations(I)V

    return-void
.end method
