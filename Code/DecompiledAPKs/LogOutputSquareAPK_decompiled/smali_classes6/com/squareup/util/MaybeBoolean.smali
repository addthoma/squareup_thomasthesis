.class public final enum Lcom/squareup/util/MaybeBoolean;
.super Ljava/lang/Enum;
.source "MaybeBoolean.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/util/MaybeBoolean;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/util/MaybeBoolean;

.field public static final enum FALSE:Lcom/squareup/util/MaybeBoolean;

.field public static final enum TRUE:Lcom/squareup/util/MaybeBoolean;

.field public static final enum UNKNOWN:Lcom/squareup/util/MaybeBoolean;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 8
    new-instance v0, Lcom/squareup/util/MaybeBoolean;

    const/4 v1, 0x0

    const-string v2, "TRUE"

    invoke-direct {v0, v2, v1}, Lcom/squareup/util/MaybeBoolean;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/util/MaybeBoolean;->TRUE:Lcom/squareup/util/MaybeBoolean;

    new-instance v0, Lcom/squareup/util/MaybeBoolean;

    const/4 v2, 0x1

    const-string v3, "FALSE"

    invoke-direct {v0, v3, v2}, Lcom/squareup/util/MaybeBoolean;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/util/MaybeBoolean;->FALSE:Lcom/squareup/util/MaybeBoolean;

    new-instance v0, Lcom/squareup/util/MaybeBoolean;

    const/4 v3, 0x2

    const-string v4, "UNKNOWN"

    invoke-direct {v0, v4, v3}, Lcom/squareup/util/MaybeBoolean;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/util/MaybeBoolean;->UNKNOWN:Lcom/squareup/util/MaybeBoolean;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/util/MaybeBoolean;

    .line 6
    sget-object v4, Lcom/squareup/util/MaybeBoolean;->TRUE:Lcom/squareup/util/MaybeBoolean;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/util/MaybeBoolean;->FALSE:Lcom/squareup/util/MaybeBoolean;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/util/MaybeBoolean;->UNKNOWN:Lcom/squareup/util/MaybeBoolean;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/util/MaybeBoolean;->$VALUES:[Lcom/squareup/util/MaybeBoolean;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/util/MaybeBoolean;
    .locals 1

    .line 6
    const-class v0, Lcom/squareup/util/MaybeBoolean;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/util/MaybeBoolean;

    return-object p0
.end method

.method public static values()[Lcom/squareup/util/MaybeBoolean;
    .locals 1

    .line 6
    sget-object v0, Lcom/squareup/util/MaybeBoolean;->$VALUES:[Lcom/squareup/util/MaybeBoolean;

    invoke-virtual {v0}, [Lcom/squareup/util/MaybeBoolean;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/util/MaybeBoolean;

    return-object v0
.end method


# virtual methods
.method public booleanValue()Z
    .locals 2

    .line 23
    invoke-virtual {p0}, Lcom/squareup/util/MaybeBoolean;->isKnown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 26
    invoke-virtual {p0}, Lcom/squareup/util/MaybeBoolean;->isTrue()Z

    move-result v0

    return v0

    .line 24
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Please call isKnown() first"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public isFalse()Z
    .locals 1

    .line 15
    sget-object v0, Lcom/squareup/util/MaybeBoolean;->FALSE:Lcom/squareup/util/MaybeBoolean;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isKnown()Z
    .locals 1

    .line 19
    sget-object v0, Lcom/squareup/util/MaybeBoolean;->UNKNOWN:Lcom/squareup/util/MaybeBoolean;

    if-eq p0, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isTrue()Z
    .locals 1

    .line 11
    sget-object v0, Lcom/squareup/util/MaybeBoolean;->TRUE:Lcom/squareup/util/MaybeBoolean;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
