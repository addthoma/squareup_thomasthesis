.class public Lcom/squareup/util/RingBuffer;
.super Ljava/util/AbstractList;
.source "RingBuffer.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/AbstractList<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private final items:[Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[TT;"
        }
    .end annotation
.end field

.field private size:I

.field private tail:I


# direct methods
.method public constructor <init>(Ljava/lang/Class;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "TT;>;I)V"
        }
    .end annotation

    .line 14
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    const/4 v0, 0x0

    .line 11
    iput v0, p0, Lcom/squareup/util/RingBuffer;->tail:I

    .line 15
    invoke-static {p1, p2}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Object;

    iput-object p1, p0, Lcom/squareup/util/RingBuffer;->items:[Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public add(Ljava/lang/Object;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .line 23
    iget-object v0, p0, Lcom/squareup/util/RingBuffer;->items:[Ljava/lang/Object;

    iget v1, p0, Lcom/squareup/util/RingBuffer;->tail:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/squareup/util/RingBuffer;->tail:I

    aput-object p1, v0, v1

    .line 24
    iget p1, p0, Lcom/squareup/util/RingBuffer;->tail:I

    array-length v0, v0

    if-ne p1, v0, :cond_0

    const/4 p1, 0x0

    .line 25
    iput p1, p0, Lcom/squareup/util/RingBuffer;->tail:I

    .line 27
    :cond_0
    iget p1, p0, Lcom/squareup/util/RingBuffer;->size:I

    iget-object v0, p0, Lcom/squareup/util/RingBuffer;->items:[Ljava/lang/Object;

    array-length v0, v0

    const/4 v1, 0x1

    if-ge p1, v0, :cond_1

    add-int/2addr p1, v1

    iput p1, p0, Lcom/squareup/util/RingBuffer;->size:I

    :cond_1
    return v1
.end method

.method public addAndReturnDeleted(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)TT;"
        }
    .end annotation

    .line 36
    iget v0, p0, Lcom/squareup/util/RingBuffer;->size:I

    iget-object v1, p0, Lcom/squareup/util/RingBuffer;->items:[Ljava/lang/Object;

    array-length v2, v1

    if-ne v0, v2, :cond_0

    .line 37
    iget v0, p0, Lcom/squareup/util/RingBuffer;->tail:I

    aget-object v0, v1, v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 41
    :goto_0
    invoke-virtual {p0, p1}, Lcom/squareup/util/RingBuffer;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method public get(I)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .line 50
    iget v0, p0, Lcom/squareup/util/RingBuffer;->size:I

    iget-object v1, p0, Lcom/squareup/util/RingBuffer;->items:[Ljava/lang/Object;

    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 51
    aget-object p1, v1, p1

    return-object p1

    .line 53
    :cond_0
    iget v2, p0, Lcom/squareup/util/RingBuffer;->tail:I

    add-int/2addr v2, p1

    rem-int/2addr v2, v0

    .line 54
    aget-object p1, v1, v2

    return-object p1
.end method

.method public size()I
    .locals 1

    .line 46
    iget v0, p0, Lcom/squareup/util/RingBuffer;->size:I

    return v0
.end method
