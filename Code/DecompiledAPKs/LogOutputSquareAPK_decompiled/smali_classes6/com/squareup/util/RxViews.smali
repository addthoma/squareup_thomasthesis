.class public final Lcom/squareup/util/RxViews;
.super Ljava/lang/Object;
.source "RxViews.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000J\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a\u001a\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0001*\u00020\u00082\u0008\u0008\u0001\u0010\u0010\u001a\u00020\r\u001a\u0018\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0001*\u00020\u00032\u0006\u0010\u0012\u001a\u00020\r\u001a\u000c\u0010\u0013\u001a\u00020\u0014*\u00020\u0008H\u0002\u001a\u0018\u0010\u0015\u001a\u00020\u0002*\u00020\u00082\u000c\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00180\u0017\u001a\n\u0010\u0019\u001a\u00020\u0002*\u00020\u0008\u001a\u001c\u0010\u001a\u001a\u00020\u0002*\u0006\u0012\u0002\u0008\u00030\u001b2\u000c\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0017\"$\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0001*\u00020\u00038GX\u0087\u0004\u00a2\u0006\u000c\u0012\u0004\u0008\u0004\u0010\u0005\u001a\u0004\u0008\u0000\u0010\u0006\"\u001b\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0001*\u00020\u00088G\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\t\"\u001b\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u0001*\u00020\u00038G\u00a2\u0006\u0006\u001a\u0004\u0008\n\u0010\u0006\"\u001b\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\r0\u0001*\u00020\u00038G\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\u0006\"\u001b\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u0001*\u00020\u00038G\u00a2\u0006\u0006\u001a\u0004\u0008\u000e\u0010\u0006\u00a8\u0006\u001d"
    }
    d2 = {
        "debouncedOnChanged",
        "Lrx/Observable;",
        "",
        "Landroid/widget/TextView;",
        "debouncedOnChanged$annotations",
        "(Landroid/widget/TextView;)V",
        "(Landroid/widget/TextView;)Lrx/Observable;",
        "debouncedOnClicked",
        "Landroid/view/View;",
        "(Landroid/view/View;)Lrx/Observable;",
        "debouncedShortText",
        "",
        "debouncedTextLength",
        "",
        "isBlank",
        "",
        "id",
        "debouncedOnEditorAction",
        "imeMaskAction",
        "ensureAttachedSubscriptions",
        "Lcom/squareup/util/AttachedSubscriptions;",
        "unsubscribeOnDetach",
        "subscriptionFactory",
        "Lkotlin/Function0;",
        "Lrx/Subscription;",
        "unsubscribeOnDetachNow",
        "unsubscribeOnMainThread",
        "Lrx/Emitter;",
        "dispose",
        "android-hairball_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final debouncedOnChanged(Landroid/widget/TextView;)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/TextView;",
            ")",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$debouncedOnChanged"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 163
    new-instance v0, Lcom/squareup/util/RxViews$debouncedOnChanged$1;

    invoke-direct {v0, p0}, Lcom/squareup/util/RxViews$debouncedOnChanged$1;-><init>(Landroid/widget/TextView;)V

    check-cast v0, Lrx/functions/Action1;

    .line 171
    sget-object p0, Lrx/Emitter$BackpressureMode;->NONE:Lrx/Emitter$BackpressureMode;

    .line 162
    invoke-static {v0, p0}, Lrx/Observable;->create(Lrx/functions/Action1;Lrx/Emitter$BackpressureMode;)Lrx/Observable;

    move-result-object p0

    const-string v0, "Observable.create(\n     \u2026ckpressureMode.NONE\n    )"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static synthetic debouncedOnChanged$annotations(Landroid/widget/TextView;)V
    .locals 0

    return-void
.end method

.method public static final debouncedOnClicked(Landroid/view/View;)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$debouncedOnClicked"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    new-instance v0, Lcom/squareup/util/RxViews$debouncedOnClicked$1;

    invoke-direct {v0, p0}, Lcom/squareup/util/RxViews$debouncedOnClicked$1;-><init>(Landroid/view/View;)V

    check-cast v0, Lrx/functions/Action1;

    .line 56
    sget-object p0, Lrx/Emitter$BackpressureMode;->NONE:Lrx/Emitter$BackpressureMode;

    .line 43
    invoke-static {v0, p0}, Lrx/Observable;->create(Lrx/functions/Action1;Lrx/Emitter$BackpressureMode;)Lrx/Observable;

    move-result-object p0

    const-string v0, "Observable.create(\n     \u2026ckpressureMode.NONE\n    )"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final debouncedOnClicked(Landroid/view/View;I)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "I)",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$debouncedOnClicked"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method public static final debouncedOnEditorAction(Landroid/widget/TextView;I)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/TextView;",
            "I)",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$debouncedOnEditorAction"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    new-instance v0, Lcom/squareup/util/RxViews$debouncedOnEditorAction$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/util/RxViews$debouncedOnEditorAction$1;-><init>(Landroid/widget/TextView;I)V

    check-cast v0, Lrx/functions/Action1;

    .line 103
    sget-object p0, Lrx/Emitter$BackpressureMode;->NONE:Lrx/Emitter$BackpressureMode;

    .line 78
    invoke-static {v0, p0}, Lrx/Observable;->create(Lrx/functions/Action1;Lrx/Emitter$BackpressureMode;)Lrx/Observable;

    move-result-object p0

    const-string p1, "Observable.create(\n     \u2026BackpressureMode.NONE\n  )"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final debouncedShortText(Landroid/widget/TextView;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/TextView;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$debouncedShortText"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 119
    invoke-static {p0}, Lcom/squareup/util/RxViews;->debouncedOnChanged(Landroid/widget/TextView;)Lrx/Observable;

    move-result-object v0

    .line 120
    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lrx/Observable;->startWith(Ljava/lang/Object;)Lrx/Observable;

    move-result-object v0

    .line 121
    new-instance v1, Lcom/squareup/util/RxViews$debouncedShortText$1;

    invoke-direct {v1, p0}, Lcom/squareup/util/RxViews$debouncedShortText$1;-><init>(Landroid/widget/TextView;)V

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p0

    .line 122
    invoke-virtual {p0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object p0

    const-string v0, "debouncedOnChanged\n     \u2026  .distinctUntilChanged()"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final debouncedTextLength(Landroid/widget/TextView;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/TextView;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$debouncedTextLength"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 132
    invoke-static {p0}, Lcom/squareup/util/RxViews;->debouncedOnChanged(Landroid/widget/TextView;)Lrx/Observable;

    move-result-object v0

    .line 133
    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lrx/Observable;->startWith(Ljava/lang/Object;)Lrx/Observable;

    move-result-object v0

    .line 134
    new-instance v1, Lcom/squareup/util/RxViews$debouncedTextLength$1;

    invoke-direct {v1, p0}, Lcom/squareup/util/RxViews$debouncedTextLength$1;-><init>(Landroid/widget/TextView;)V

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p0

    .line 135
    invoke-virtual {p0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object p0

    const-string v0, "debouncedOnChanged\n     \u2026  .distinctUntilChanged()"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method private static final ensureAttachedSubscriptions(Landroid/view/View;)Lcom/squareup/util/AttachedSubscriptions;
    .locals 2

    .line 225
    sget v0, Lcom/squareup/utilities/R$id;->tag_attached_subscriptions:I

    invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/AttachedSubscriptions;

    if-nez v0, :cond_0

    .line 228
    new-instance v0, Lcom/squareup/util/AttachedSubscriptions;

    invoke-direct {v0}, Lcom/squareup/util/AttachedSubscriptions;-><init>()V

    .line 229
    sget v1, Lcom/squareup/utilities/R$id;->tag_attached_subscriptions:I

    invoke-virtual {p0, v1, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 230
    move-object v1, v0

    check-cast v1, Landroid/view/View$OnAttachStateChangeListener;

    invoke-virtual {p0, v1}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    :cond_0
    return-object v0
.end method

.method public static final isBlank(Landroid/widget/TextView;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/TextView;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$isBlank"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 145
    invoke-static {p0}, Lcom/squareup/util/RxViews;->debouncedOnChanged(Landroid/widget/TextView;)Lrx/Observable;

    move-result-object v0

    .line 146
    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lrx/Observable;->startWith(Ljava/lang/Object;)Lrx/Observable;

    move-result-object v0

    .line 147
    new-instance v1, Lcom/squareup/util/RxViews$isBlank$1;

    invoke-direct {v1, p0}, Lcom/squareup/util/RxViews$isBlank$1;-><init>(Landroid/widget/TextView;)V

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p0

    .line 148
    invoke-virtual {p0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object p0

    const-string v0, "debouncedOnChanged\n     \u2026  .distinctUntilChanged()"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lkotlin/jvm/functions/Function0<",
            "+",
            "Lrx/Subscription;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$unsubscribeOnDetach"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "subscriptionFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 188
    invoke-static {p0}, Lcom/squareup/util/RxViews;->ensureAttachedSubscriptions(Landroid/view/View;)Lcom/squareup/util/AttachedSubscriptions;

    move-result-object v0

    .line 189
    invoke-static {p0}, Lcom/squareup/util/Views;->isAttachedToWindowCompat(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 191
    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lrx/Subscription;

    .line 192
    invoke-static {p0}, Lcom/squareup/util/Views;->isAttachedToWindowCompat(Landroid/view/View;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 194
    invoke-virtual {v0}, Lcom/squareup/util/AttachedSubscriptions;->getSubs()Lrx/subscriptions/CompositeSubscription;

    move-result-object p0

    invoke-static {p0, p1}, Lcom/squareup/util/rx/RxKt;->plusAssign(Lrx/subscriptions/CompositeSubscription;Lrx/Subscription;)V

    goto :goto_0

    .line 198
    :cond_0
    invoke-interface {p1}, Lrx/Subscription;->unsubscribe()V

    goto :goto_0

    .line 202
    :cond_1
    invoke-virtual {v0, p1}, Lcom/squareup/util/AttachedSubscriptions;->plusAssign(Lkotlin/jvm/functions/Function0;)V

    :goto_0
    return-void
.end method

.method public static final unsubscribeOnDetachNow(Landroid/view/View;)V
    .locals 1

    const-string v0, "$this$unsubscribeOnDetachNow"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 210
    invoke-static {p0}, Lcom/squareup/util/RxViews;->ensureAttachedSubscriptions(Landroid/view/View;)Lcom/squareup/util/AttachedSubscriptions;

    move-result-object p0

    .line 211
    invoke-virtual {p0}, Lcom/squareup/util/AttachedSubscriptions;->clearAllSubscriptions()V

    return-void
.end method

.method public static final unsubscribeOnMainThread(Lrx/Emitter;Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Emitter<",
            "*>;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$unsubscribeOnMainThread"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dispose"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 219
    new-instance v0, Lcom/squareup/util/RxViews$unsubscribeOnMainThread$1;

    invoke-direct {v0, p1}, Lcom/squareup/util/RxViews$unsubscribeOnMainThread$1;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v0, Lrx/Subscription;

    invoke-interface {p0, v0}, Lrx/Emitter;->setSubscription(Lrx/Subscription;)V

    return-void
.end method
