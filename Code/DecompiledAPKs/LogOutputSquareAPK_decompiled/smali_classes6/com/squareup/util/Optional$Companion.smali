.class public final Lcom/squareup/util/Optional$Companion;
.super Ljava/lang/Object;
.source "Optional.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/util/Optional;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u0002H\u00050\u0004\"\u0008\u0008\u0001\u0010\u0005*\u00020\u0001H\u0007J%\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u0002H\u00050\u0004\"\u0008\u0008\u0001\u0010\u0005*\u00020\u00012\u0006\u0010\u0007\u001a\u0002H\u0005H\u0007\u00a2\u0006\u0002\u0010\u0008J\'\u0010\t\u001a\u0008\u0012\u0004\u0012\u0002H\u00050\u0004\"\u0008\u0008\u0001\u0010\u0005*\u00020\u00012\u0008\u0010\u0007\u001a\u0004\u0018\u0001H\u0005H\u0007\u00a2\u0006\u0002\u0010\u0008\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/util/Optional$Companion;",
        "",
        "()V",
        "empty",
        "Lcom/squareup/util/Optional;",
        "T",
        "of",
        "value",
        "(Ljava/lang/Object;)Lcom/squareup/util/Optional;",
        "ofNullable",
        "public"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 204
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 204
    invoke-direct {p0}, Lcom/squareup/util/Optional$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final empty()Lcom/squareup/util/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/squareup/util/Optional<",
            "TT;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 210
    sget-object v0, Lcom/squareup/util/Optional$Empty;->INSTANCE:Lcom/squareup/util/Optional$Empty;

    check-cast v0, Lcom/squareup/util/Optional;

    return-object v0
.end method

.method public final of(Ljava/lang/Object;)Lcom/squareup/util/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)",
            "Lcom/squareup/util/Optional<",
            "TT;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 218
    new-instance v0, Lcom/squareup/util/Optional$Present;

    invoke-direct {v0, p1}, Lcom/squareup/util/Optional$Present;-><init>(Ljava/lang/Object;)V

    check-cast v0, Lcom/squareup/util/Optional;

    return-object v0
.end method

.method public final ofNullable(Ljava/lang/Object;)Lcom/squareup/util/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)",
            "Lcom/squareup/util/Optional<",
            "TT;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    if-nez p1, :cond_0

    .line 227
    move-object p1, p0

    check-cast p1, Lcom/squareup/util/Optional$Companion;

    invoke-virtual {p1}, Lcom/squareup/util/Optional$Companion;->empty()Lcom/squareup/util/Optional;

    move-result-object p1

    goto :goto_0

    :cond_0
    move-object v0, p0

    check-cast v0, Lcom/squareup/util/Optional$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/util/Optional$Companion;->of(Ljava/lang/Object;)Lcom/squareup/util/Optional;

    move-result-object p1

    :goto_0
    return-object p1
.end method
