.class final Lcom/squareup/util/RxContentUriSingleItemAccessor$get$1;
.super Ljava/lang/Object;
.source "RxContentUriSingleItemAccessor.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/util/RxContentUriSingleItemAccessor;->get()Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRxContentUriSingleItemAccessor.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RxContentUriSingleItemAccessor.kt\ncom/squareup/util/RxContentUriSingleItemAccessor$get$1\n*L\n1#1,110:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u0002H\u0001\"\u0004\u0008\u0000\u0010\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0004\u0008\u0004\u0010\u0005"
    }
    d2 = {
        "<anonymous>",
        "T",
        "it",
        "",
        "apply",
        "(Lkotlin/Unit;)Ljava/lang/Object;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/util/RxContentUriSingleItemAccessor;


# direct methods
.method constructor <init>(Lcom/squareup/util/RxContentUriSingleItemAccessor;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/util/RxContentUriSingleItemAccessor$get$1;->this$0:Lcom/squareup/util/RxContentUriSingleItemAccessor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 33
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1}, Lcom/squareup/util/RxContentUriSingleItemAccessor$get$1;->apply(Lkotlin/Unit;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final apply(Lkotlin/Unit;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Unit;",
            ")TT;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    iget-object p1, p0, Lcom/squareup/util/RxContentUriSingleItemAccessor$get$1;->this$0:Lcom/squareup/util/RxContentUriSingleItemAccessor;

    invoke-static {p1}, Lcom/squareup/util/RxContentUriSingleItemAccessor;->access$requireCursor(Lcom/squareup/util/RxContentUriSingleItemAccessor;)Landroid/database/Cursor;

    move-result-object p1

    check-cast p1, Ljava/io/Closeable;

    const/4 v0, 0x0

    check-cast v0, Ljava/lang/Throwable;

    :try_start_0
    move-object v1, p1

    check-cast v1, Landroid/database/Cursor;

    .line 45
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 48
    iget-object v2, p0, Lcom/squareup/util/RxContentUriSingleItemAccessor$get$1;->this$0:Lcom/squareup/util/RxContentUriSingleItemAccessor;

    invoke-static {v2}, Lcom/squareup/util/RxContentUriSingleItemAccessor;->access$getStrategy$p(Lcom/squareup/util/RxContentUriSingleItemAccessor;)Lcom/squareup/util/AccessorStrategy;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v1, v3}, Lcom/squareup/util/AccessorStrategy;->getValueFromCursor(Landroid/database/Cursor;I)Ljava/lang/Object;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 44
    invoke-static {p1, v0}, Lkotlin/io/CloseableKt;->closeFinally(Ljava/io/Closeable;Ljava/lang/Throwable;)V

    return-object v1

    .line 46
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/squareup/util/RxContentUriSingleItemAccessor$get$1;->this$0:Lcom/squareup/util/RxContentUriSingleItemAccessor;

    const-string v1, "received cursor with 0 rows"

    invoke-static {v0, v1}, Lcom/squareup/util/RxContentUriSingleItemAccessor;->access$getErrorMsg(Lcom/squareup/util/RxContentUriSingleItemAccessor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 45
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    .line 44
    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    move-exception v1

    invoke-static {p1, v0}, Lkotlin/io/CloseableKt;->closeFinally(Ljava/io/Closeable;Ljava/lang/Throwable;)V

    throw v1
.end method
