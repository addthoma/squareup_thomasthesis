.class public final Lcom/squareup/util/RxBroadcastReceiver$registerForIntents$1$receiver$1;
.super Landroid/content/BroadcastReceiver;
.source "RxBroadcastReceiver.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/util/RxBroadcastReceiver$registerForIntents$1;->call(Lrx/Emitter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRxBroadcastReceiver.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RxBroadcastReceiver.kt\ncom/squareup/util/RxBroadcastReceiver$registerForIntents$1$receiver$1\n*L\n1#1,60:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001d\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\u0016\u00a8\u0006\u0008"
    }
    d2 = {
        "com/squareup/util/RxBroadcastReceiver$registerForIntents$1$receiver$1",
        "Landroid/content/BroadcastReceiver;",
        "onReceive",
        "",
        "context",
        "Landroid/content/Context;",
        "intent",
        "Landroid/content/Intent;",
        "android-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $emitter:Lrx/Emitter;

.field final synthetic this$0:Lcom/squareup/util/RxBroadcastReceiver$registerForIntents$1;


# direct methods
.method constructor <init>(Lcom/squareup/util/RxBroadcastReceiver$registerForIntents$1;Lrx/Emitter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Emitter;",
            ")V"
        }
    .end annotation

    .line 43
    iput-object p1, p0, Lcom/squareup/util/RxBroadcastReceiver$registerForIntents$1$receiver$1;->this$0:Lcom/squareup/util/RxBroadcastReceiver$registerForIntents$1;

    iput-object p2, p0, Lcom/squareup/util/RxBroadcastReceiver$registerForIntents$1$receiver$1;->$emitter:Lrx/Emitter;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "intent"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    iget-object p1, p0, Lcom/squareup/util/RxBroadcastReceiver$registerForIntents$1$receiver$1;->this$0:Lcom/squareup/util/RxBroadcastReceiver$registerForIntents$1;

    iget-object p1, p1, Lcom/squareup/util/RxBroadcastReceiver$registerForIntents$1;->$actions:[Ljava/lang/String;

    array-length v0, p1

    invoke-static {p1, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Ljava/lang/String;

    invoke-static {p1}, Lcom/squareup/util/SquareCollections;->asSet([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p1

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 51
    iget-object p1, p0, Lcom/squareup/util/RxBroadcastReceiver$registerForIntents$1$receiver$1;->$emitter:Lrx/Emitter;

    invoke-interface {p1, p2}, Lrx/Emitter;->onNext(Ljava/lang/Object;)V

    return-void

    .line 49
    :cond_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Received unexpected intent: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 48
    new-instance p2, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2
.end method
