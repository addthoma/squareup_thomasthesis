.class final Lcom/squareup/util/rx/RxNullValueHook$observableHook$1$1;
.super Ljava/lang/Object;
.source "RxNullValueHook.kt"

# interfaces
.implements Lrx/Observable$OnSubscribe;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/util/rx/RxNullValueHook$observableHook$1;->call(Lrx/Observable;Lrx/Observable$OnSubscribe;)Lrx/Observable$OnSubscribe;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/Observable$OnSubscribe<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\u0010\u0000\u001a\u00020\u00012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "subscriber",
        "Lrx/Subscriber;",
        "",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $onSubscribe:Lrx/Observable$OnSubscribe;


# direct methods
.method constructor <init>(Lrx/Observable$OnSubscribe;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/util/rx/RxNullValueHook$observableHook$1$1;->$onSubscribe:Lrx/Observable$OnSubscribe;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 54
    check-cast p1, Lrx/Subscriber;

    invoke-virtual {p0, p1}, Lcom/squareup/util/rx/RxNullValueHook$observableHook$1$1;->call(Lrx/Subscriber;)V

    return-void
.end method

.method public final call(Lrx/Subscriber;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Subscriber<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const-string v0, "subscriber"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    new-instance v0, Lcom/squareup/util/rx/RxNullValueHook$observableHook$1$1$proxy$1;

    invoke-direct {v0, p1, p1}, Lcom/squareup/util/rx/RxNullValueHook$observableHook$1$1$proxy$1;-><init>(Lrx/Subscriber;Lrx/Subscriber;)V

    .line 74
    iget-object p1, p0, Lcom/squareup/util/rx/RxNullValueHook$observableHook$1$1;->$onSubscribe:Lrx/Observable$OnSubscribe;

    invoke-interface {p1, v0}, Lrx/Observable$OnSubscribe;->call(Ljava/lang/Object;)V

    return-void
.end method
