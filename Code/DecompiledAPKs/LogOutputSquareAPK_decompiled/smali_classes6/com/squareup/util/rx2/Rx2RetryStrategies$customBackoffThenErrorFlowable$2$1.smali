.class final Lcom/squareup/util/rx2/Rx2RetryStrategies$customBackoffThenErrorFlowable$2$1;
.super Ljava/lang/Object;
.source "Rx2RetryStrategies.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/util/rx2/Rx2RetryStrategies$customBackoffThenErrorFlowable$2;->apply(Lio/reactivex/Flowable;)Lio/reactivex/Flowable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lorg/reactivestreams/Publisher<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0010\u0000\u001a\u0010\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u00020\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0004\u0008\u0006\u0010\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Flowable;",
        "",
        "kotlin.jvm.PlatformType",
        "retryCount",
        "",
        "apply",
        "(Ljava/lang/Integer;)Lio/reactivex/Flowable;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/util/rx2/Rx2RetryStrategies$customBackoffThenErrorFlowable$2;


# direct methods
.method constructor <init>(Lcom/squareup/util/rx2/Rx2RetryStrategies$customBackoffThenErrorFlowable$2;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/util/rx2/Rx2RetryStrategies$customBackoffThenErrorFlowable$2$1;->this$0:Lcom/squareup/util/rx2/Rx2RetryStrategies$customBackoffThenErrorFlowable$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Integer;)Lio/reactivex/Flowable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Integer;",
            ")",
            "Lio/reactivex/Flowable<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    const-string v0, "retryCount"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 161
    iget-object v0, p0, Lcom/squareup/util/rx2/Rx2RetryStrategies$customBackoffThenErrorFlowable$2$1;->this$0:Lcom/squareup/util/rx2/Rx2RetryStrategies$customBackoffThenErrorFlowable$2;

    iget-object v0, v0, Lcom/squareup/util/rx2/Rx2RetryStrategies$customBackoffThenErrorFlowable$2;->$delayFunction:Lkotlin/jvm/functions/Function1;

    invoke-interface {v0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    iget-object p1, p0, Lcom/squareup/util/rx2/Rx2RetryStrategies$customBackoffThenErrorFlowable$2$1;->this$0:Lcom/squareup/util/rx2/Rx2RetryStrategies$customBackoffThenErrorFlowable$2;

    iget-object p1, p1, Lcom/squareup/util/rx2/Rx2RetryStrategies$customBackoffThenErrorFlowable$2;->$unit:Ljava/util/concurrent/TimeUnit;

    iget-object v2, p0, Lcom/squareup/util/rx2/Rx2RetryStrategies$customBackoffThenErrorFlowable$2$1;->this$0:Lcom/squareup/util/rx2/Rx2RetryStrategies$customBackoffThenErrorFlowable$2;

    iget-object v2, v2, Lcom/squareup/util/rx2/Rx2RetryStrategies$customBackoffThenErrorFlowable$2;->$scheduler:Lio/reactivex/Scheduler;

    invoke-static {v0, v1, p1, v2}, Lio/reactivex/Flowable;->timer(JLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/Flowable;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 11
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/squareup/util/rx2/Rx2RetryStrategies$customBackoffThenErrorFlowable$2$1;->apply(Ljava/lang/Integer;)Lio/reactivex/Flowable;

    move-result-object p1

    return-object p1
.end method
