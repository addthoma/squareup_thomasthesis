.class final Lcom/squareup/util/rx2/Rx2TransformersKt$adaptiveSample$2$2;
.super Ljava/lang/Object;
.source "Rx2Transformers.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/util/rx2/Rx2TransformersKt$adaptiveSample$2;->apply(Lio/reactivex/Observable;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/ObservableSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u0010\u0012\u000c\u0012\n \u0003*\u0004\u0018\u0001H\u0002H\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\n\u00a2\u0006\u0004\u0008\u0007\u0010\u0008"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Observable;",
        "T",
        "kotlin.jvm.PlatformType",
        "",
        "it",
        "",
        "apply",
        "(Lkotlin/Unit;)Lio/reactivex/Observable;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $shared:Lio/reactivex/Observable;

.field final synthetic this$0:Lcom/squareup/util/rx2/Rx2TransformersKt$adaptiveSample$2;


# direct methods
.method constructor <init>(Lcom/squareup/util/rx2/Rx2TransformersKt$adaptiveSample$2;Lio/reactivex/Observable;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/util/rx2/Rx2TransformersKt$adaptiveSample$2$2;->this$0:Lcom/squareup/util/rx2/Rx2TransformersKt$adaptiveSample$2;

    iput-object p2, p0, Lcom/squareup/util/rx2/Rx2TransformersKt$adaptiveSample$2$2;->$shared:Lio/reactivex/Observable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lkotlin/Unit;)Lio/reactivex/Observable;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Unit;",
            ")",
            "Lio/reactivex/Observable<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x2

    new-array p1, p1, [Lio/reactivex/Observable;

    .line 101
    iget-object v0, p0, Lcom/squareup/util/rx2/Rx2TransformersKt$adaptiveSample$2$2;->$shared:Lio/reactivex/Observable;

    const-wide/16 v1, 0x1

    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->take(J)Lio/reactivex/Observable;

    move-result-object v0

    const/4 v1, 0x0

    aput-object v0, p1, v1

    iget-object v0, p0, Lcom/squareup/util/rx2/Rx2TransformersKt$adaptiveSample$2$2;->$shared:Lio/reactivex/Observable;

    iget-object v1, p0, Lcom/squareup/util/rx2/Rx2TransformersKt$adaptiveSample$2$2;->this$0:Lcom/squareup/util/rx2/Rx2TransformersKt$adaptiveSample$2;

    iget-wide v1, v1, Lcom/squareup/util/rx2/Rx2TransformersKt$adaptiveSample$2;->$time:J

    iget-object v3, p0, Lcom/squareup/util/rx2/Rx2TransformersKt$adaptiveSample$2$2;->this$0:Lcom/squareup/util/rx2/Rx2TransformersKt$adaptiveSample$2;

    iget-object v3, v3, Lcom/squareup/util/rx2/Rx2TransformersKt$adaptiveSample$2;->$unit:Ljava/util/concurrent/TimeUnit;

    iget-object v4, p0, Lcom/squareup/util/rx2/Rx2TransformersKt$adaptiveSample$2$2;->this$0:Lcom/squareup/util/rx2/Rx2TransformersKt$adaptiveSample$2;

    iget-object v4, v4, Lcom/squareup/util/rx2/Rx2TransformersKt$adaptiveSample$2;->$scheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1, v2, v3, v4}, Lio/reactivex/Observable;->sample(JLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    const/4 v1, 0x1

    aput-object v0, p1, v1

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    invoke-static {p1}, Lio/reactivex/Observable;->concatDelayError(Ljava/lang/Iterable;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1}, Lcom/squareup/util/rx2/Rx2TransformersKt$adaptiveSample$2$2;->apply(Lkotlin/Unit;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method
