.class public final Lcom/squareup/util/rx2/Observables;
.super Ljava/lang/Object;
.source "RxKotlin.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0080\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002JJ\u0010\u0003\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u00070\u00050\u0004\"\u0008\u0008\u0000\u0010\u0006*\u00020\u0001\"\u0008\u0008\u0001\u0010\u0007*\u00020\u00012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u0002H\u00060\u00042\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\u0004H\u0007Je\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u0002H\n0\u0004\"\u0008\u0008\u0000\u0010\u0006*\u00020\u0001\"\u0008\u0008\u0001\u0010\u0007*\u00020\u0001\"\u0008\u0008\u0002\u0010\n*\u00020\u00012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u0002H\u00060\u00042\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\u00042\u001a\u0008\u0004\u0010\u000b\u001a\u0014\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u0002H\n0\u000cH\u0087\u0008Jh\u0010\u0003\u001a\u001a\u0012\u0016\u0012\u0014\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u0002H\u000e0\r0\u0004\"\u0008\u0008\u0000\u0010\u0006*\u00020\u0001\"\u0008\u0008\u0001\u0010\u0007*\u00020\u0001\"\u0008\u0008\u0002\u0010\u000e*\u00020\u00012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u0002H\u00060\u00042\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\u00042\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u0002H\u000e0\u0004H\u0007J\u0083\u0001\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u0002H\n0\u0004\"\u0008\u0008\u0000\u0010\u0006*\u00020\u0001\"\u0008\u0008\u0001\u0010\u0007*\u00020\u0001\"\u0008\u0008\u0002\u0010\u000e*\u00020\u0001\"\u0008\u0008\u0003\u0010\n*\u00020\u00012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u0002H\u00060\u00042\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\u00042\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u0002H\u000e0\u00042 \u0008\u0004\u0010\u000b\u001a\u001a\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u0002H\u000e\u0012\u0004\u0012\u0002H\n0\u0010H\u0087\u0008J\u0086\u0001\u0010\u0003\u001a \u0012\u001c\u0012\u001a\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u0002H\u000e\u0012\u0004\u0012\u0002H\u00120\u00110\u0004\"\u0008\u0008\u0000\u0010\u0006*\u00020\u0001\"\u0008\u0008\u0001\u0010\u0007*\u00020\u0001\"\u0008\u0008\u0002\u0010\u000e*\u00020\u0001\"\u0008\u0008\u0003\u0010\u0012*\u00020\u00012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u0002H\u00060\u00042\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\u00042\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u0002H\u000e0\u00042\u000c\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u0002H\u00120\u0004H\u0007J\u00a1\u0001\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u0002H\n0\u0004\"\u0008\u0008\u0000\u0010\u0006*\u00020\u0001\"\u0008\u0008\u0001\u0010\u0007*\u00020\u0001\"\u0008\u0008\u0002\u0010\u000e*\u00020\u0001\"\u0008\u0008\u0003\u0010\u0012*\u00020\u0001\"\u0008\u0008\u0004\u0010\n*\u00020\u00012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u0002H\u00060\u00042\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\u00042\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u0002H\u000e0\u00042\u000c\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u0002H\u00120\u00042&\u0008\u0004\u0010\u000b\u001a \u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u0002H\u000e\u0012\u0004\u0012\u0002H\u0012\u0012\u0004\u0012\u0002H\n0\u0014H\u0087\u0008J\u00a4\u0001\u0010\u0003\u001a&\u0012\"\u0012 \u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u0002H\u000e\u0012\u0004\u0012\u0002H\u0012\u0012\u0004\u0012\u0002H\u00160\u00150\u0004\"\u0008\u0008\u0000\u0010\u0006*\u00020\u0001\"\u0008\u0008\u0001\u0010\u0007*\u00020\u0001\"\u0008\u0008\u0002\u0010\u000e*\u00020\u0001\"\u0008\u0008\u0003\u0010\u0012*\u00020\u0001\"\u0008\u0008\u0004\u0010\u0016*\u00020\u00012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u0002H\u00060\u00042\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\u00042\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u0002H\u000e0\u00042\u000c\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u0002H\u00120\u00042\u000c\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u0002H\u00160\u0004H\u0007J\u00bf\u0001\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u0002H\n0\u0004\"\u0008\u0008\u0000\u0010\u0006*\u00020\u0001\"\u0008\u0008\u0001\u0010\u0007*\u00020\u0001\"\u0008\u0008\u0002\u0010\u000e*\u00020\u0001\"\u0008\u0008\u0003\u0010\u0012*\u00020\u0001\"\u0008\u0008\u0004\u0010\u0016*\u00020\u0001\"\u0008\u0008\u0005\u0010\n*\u00020\u00012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u0002H\u00060\u00042\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\u00042\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u0002H\u000e0\u00042\u000c\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u0002H\u00120\u00042\u000c\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u0002H\u00160\u00042,\u0008\u0004\u0010\u000b\u001a&\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u0002H\u000e\u0012\u0004\u0012\u0002H\u0012\u0012\u0004\u0012\u0002H\u0016\u0012\u0004\u0012\u0002H\n0\u0018H\u0087\u0008J\u00c2\u0001\u0010\u0003\u001a,\u0012(\u0012&\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u0002H\u000e\u0012\u0004\u0012\u0002H\u0012\u0012\u0004\u0012\u0002H\u0016\u0012\u0004\u0012\u0002H\u001a0\u00190\u0004\"\u0008\u0008\u0000\u0010\u0006*\u00020\u0001\"\u0008\u0008\u0001\u0010\u0007*\u00020\u0001\"\u0008\u0008\u0002\u0010\u000e*\u00020\u0001\"\u0008\u0008\u0003\u0010\u0012*\u00020\u0001\"\u0008\u0008\u0004\u0010\u0016*\u00020\u0001\"\u0008\u0008\u0005\u0010\u001a*\u00020\u00012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u0002H\u00060\u00042\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\u00042\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u0002H\u000e0\u00042\u000c\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u0002H\u00120\u00042\u000c\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u0002H\u00160\u00042\u000c\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u0002H\u001a0\u0004H\u0007J\u00dd\u0001\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u0002H\n0\u0004\"\u0008\u0008\u0000\u0010\u0006*\u00020\u0001\"\u0008\u0008\u0001\u0010\u0007*\u00020\u0001\"\u0008\u0008\u0002\u0010\u000e*\u00020\u0001\"\u0008\u0008\u0003\u0010\u0012*\u00020\u0001\"\u0008\u0008\u0004\u0010\u0016*\u00020\u0001\"\u0008\u0008\u0005\u0010\u001a*\u00020\u0001\"\u0008\u0008\u0006\u0010\n*\u00020\u00012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u0002H\u00060\u00042\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\u00042\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u0002H\u000e0\u00042\u000c\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u0002H\u00120\u00042\u000c\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u0002H\u00160\u00042\u000c\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u0002H\u001a0\u000422\u0008\u0004\u0010\u000b\u001a,\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u0002H\u000e\u0012\u0004\u0012\u0002H\u0012\u0012\u0004\u0012\u0002H\u0016\u0012\u0004\u0012\u0002H\u001a\u0012\u0004\u0012\u0002H\n0\u001cH\u0087\u0008J\u00e0\u0001\u0010\u0003\u001a2\u0012.\u0012,\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u0002H\u000e\u0012\u0004\u0012\u0002H\u0012\u0012\u0004\u0012\u0002H\u0016\u0012\u0004\u0012\u0002H\u001a\u0012\u0004\u0012\u0002H\u001e0\u001d0\u0004\"\u0008\u0008\u0000\u0010\u0006*\u00020\u0001\"\u0008\u0008\u0001\u0010\u0007*\u00020\u0001\"\u0008\u0008\u0002\u0010\u000e*\u00020\u0001\"\u0008\u0008\u0003\u0010\u0012*\u00020\u0001\"\u0008\u0008\u0004\u0010\u0016*\u00020\u0001\"\u0008\u0008\u0005\u0010\u001a*\u00020\u0001\"\u0008\u0008\u0006\u0010\u001e*\u00020\u00012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u0002H\u00060\u00042\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\u00042\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u0002H\u000e0\u00042\u000c\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u0002H\u00120\u00042\u000c\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u0002H\u00160\u00042\u000c\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u0002H\u001a0\u00042\u000c\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u0002H\u001e0\u0004H\u0007J\u00fb\u0001\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u0002H\n0\u0004\"\u0008\u0008\u0000\u0010\u0006*\u00020\u0001\"\u0008\u0008\u0001\u0010\u0007*\u00020\u0001\"\u0008\u0008\u0002\u0010\u000e*\u00020\u0001\"\u0008\u0008\u0003\u0010\u0012*\u00020\u0001\"\u0008\u0008\u0004\u0010\u0016*\u00020\u0001\"\u0008\u0008\u0005\u0010\u001a*\u00020\u0001\"\u0008\u0008\u0006\u0010\u001e*\u00020\u0001\"\u0008\u0008\u0007\u0010\n*\u00020\u00012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u0002H\u00060\u00042\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\u00042\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u0002H\u000e0\u00042\u000c\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u0002H\u00120\u00042\u000c\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u0002H\u00160\u00042\u000c\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u0002H\u001a0\u00042\u000c\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u0002H\u001e0\u000428\u0008\u0004\u0010\u000b\u001a2\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u0002H\u000e\u0012\u0004\u0012\u0002H\u0012\u0012\u0004\u0012\u0002H\u0016\u0012\u0004\u0012\u0002H\u001a\u0012\u0004\u0012\u0002H\u001e\u0012\u0004\u0012\u0002H\n0 H\u0087\u0008J\u00fe\u0001\u0010\u0003\u001a8\u00124\u00122\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u0002H\u000e\u0012\u0004\u0012\u0002H\u0012\u0012\u0004\u0012\u0002H\u0016\u0012\u0004\u0012\u0002H\u001a\u0012\u0004\u0012\u0002H\u001e\u0012\u0004\u0012\u0002H\"0!0\u0004\"\u0008\u0008\u0000\u0010\u0006*\u00020\u0001\"\u0008\u0008\u0001\u0010\u0007*\u00020\u0001\"\u0008\u0008\u0002\u0010\u000e*\u00020\u0001\"\u0008\u0008\u0003\u0010\u0012*\u00020\u0001\"\u0008\u0008\u0004\u0010\u0016*\u00020\u0001\"\u0008\u0008\u0005\u0010\u001a*\u00020\u0001\"\u0008\u0008\u0006\u0010\u001e*\u00020\u0001\"\u0008\u0008\u0007\u0010\"*\u00020\u00012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u0002H\u00060\u00042\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\u00042\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u0002H\u000e0\u00042\u000c\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u0002H\u00120\u00042\u000c\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u0002H\u00160\u00042\u000c\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u0002H\u001a0\u00042\u000c\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u0002H\u001e0\u00042\u000c\u0010#\u001a\u0008\u0012\u0004\u0012\u0002H\"0\u0004H\u0007J\u0099\u0002\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u0002H\n0\u0004\"\u0008\u0008\u0000\u0010\u0006*\u00020\u0001\"\u0008\u0008\u0001\u0010\u0007*\u00020\u0001\"\u0008\u0008\u0002\u0010\u000e*\u00020\u0001\"\u0008\u0008\u0003\u0010\u0012*\u00020\u0001\"\u0008\u0008\u0004\u0010\u0016*\u00020\u0001\"\u0008\u0008\u0005\u0010\u001a*\u00020\u0001\"\u0008\u0008\u0006\u0010\u001e*\u00020\u0001\"\u0008\u0008\u0007\u0010\"*\u00020\u0001\"\u0008\u0008\u0008\u0010\n*\u00020\u00012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u0002H\u00060\u00042\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\u00042\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u0002H\u000e0\u00042\u000c\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u0002H\u00120\u00042\u000c\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u0002H\u00160\u00042\u000c\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u0002H\u001a0\u00042\u000c\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u0002H\u001e0\u00042\u000c\u0010#\u001a\u0008\u0012\u0004\u0012\u0002H\"0\u00042>\u0008\u0004\u0010\u000b\u001a8\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u0002H\u000e\u0012\u0004\u0012\u0002H\u0012\u0012\u0004\u0012\u0002H\u0016\u0012\u0004\u0012\u0002H\u001a\u0012\u0004\u0012\u0002H\u001e\u0012\u0004\u0012\u0002H\"\u0012\u0004\u0012\u0002H\n0$H\u0087\u0008J\u00b7\u0002\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u0002H\n0\u0004\"\u0008\u0008\u0000\u0010\u0006*\u00020\u0001\"\u0008\u0008\u0001\u0010\u0007*\u00020\u0001\"\u0008\u0008\u0002\u0010\u000e*\u00020\u0001\"\u0008\u0008\u0003\u0010\u0012*\u00020\u0001\"\u0008\u0008\u0004\u0010\u0016*\u00020\u0001\"\u0008\u0008\u0005\u0010\u001a*\u00020\u0001\"\u0008\u0008\u0006\u0010\u001e*\u00020\u0001\"\u0008\u0008\u0007\u0010\"*\u00020\u0001\"\u0008\u0008\u0008\u0010%*\u00020\u0001\"\u0008\u0008\t\u0010\n*\u00020\u00012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u0002H\u00060\u00042\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\u00042\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u0002H\u000e0\u00042\u000c\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u0002H\u00120\u00042\u000c\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u0002H\u00160\u00042\u000c\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u0002H\u001a0\u00042\u000c\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u0002H\u001e0\u00042\u000c\u0010#\u001a\u0008\u0012\u0004\u0012\u0002H\"0\u00042\u000c\u0010&\u001a\u0008\u0012\u0004\u0012\u0002H%0\u00042D\u0008\u0004\u0010\u000b\u001a>\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u0002H\u000e\u0012\u0004\u0012\u0002H\u0012\u0012\u0004\u0012\u0002H\u0016\u0012\u0004\u0012\u0002H\u001a\u0012\u0004\u0012\u0002H\u001e\u0012\u0004\u0012\u0002H\"\u0012\u0004\u0012\u0002H%\u0012\u0004\u0012\u0002H\n0\'H\u0087\u0008Jb\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u0002H\n0\u0004\"\u0008\u0008\u0000\u0010(*\u00020\u0001\"\u0008\u0008\u0001\u0010\n*\u00020\u00012\u0012\u0010)\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H(0\u00040*2)\u0008\u0004\u0010\u000b\u001a#\u0012\u0019\u0012\u0017\u0012\u0004\u0012\u0002H(0*\u00a2\u0006\u000c\u0008,\u0012\u0008\u0008-\u0012\u0004\u0008\u0008(.\u0012\u0004\u0012\u0002H\n0+H\u0087\u0008JJ\u0010/\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u00070\u00050\u0004\"\u0008\u0008\u0000\u0010\u0006*\u00020\u0001\"\u0008\u0008\u0001\u0010\u0007*\u00020\u00012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u0002H\u00060\u00042\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\u0004H\u0007Je\u0010/\u001a\u0008\u0012\u0004\u0012\u0002H\n0\u0004\"\u0008\u0008\u0000\u0010\u0006*\u00020\u0001\"\u0008\u0008\u0001\u0010\u0007*\u00020\u0001\"\u0008\u0008\u0002\u0010\n*\u00020\u00012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u0002H\u00060\u00042\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\u00042\u001a\u0008\u0004\u0010\u000b\u001a\u0014\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u0002H\n0\u000cH\u0087\u0008Jh\u0010/\u001a\u001a\u0012\u0016\u0012\u0014\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u0002H\u000e0\r0\u0004\"\u0008\u0008\u0000\u0010\u0006*\u00020\u0001\"\u0008\u0008\u0001\u0010\u0007*\u00020\u0001\"\u0008\u0008\u0002\u0010\u000e*\u00020\u00012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u0002H\u00060\u00042\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\u00042\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u0002H\u000e0\u0004H\u0007J\u0083\u0001\u0010/\u001a\u0008\u0012\u0004\u0012\u0002H\n0\u0004\"\u0008\u0008\u0000\u0010\u0006*\u00020\u0001\"\u0008\u0008\u0001\u0010\u0007*\u00020\u0001\"\u0008\u0008\u0002\u0010\u000e*\u00020\u0001\"\u0008\u0008\u0003\u0010\n*\u00020\u00012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u0002H\u00060\u00042\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\u00042\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u0002H\u000e0\u00042 \u0008\u0004\u0010\u000b\u001a\u001a\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u0002H\u000e\u0012\u0004\u0012\u0002H\n0\u0010H\u0087\u0008J\u0086\u0001\u0010/\u001a \u0012\u001c\u0012\u001a\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u0002H\u000e\u0012\u0004\u0012\u0002H\u00120\u00110\u0004\"\u0008\u0008\u0000\u0010\u0006*\u00020\u0001\"\u0008\u0008\u0001\u0010\u0007*\u00020\u0001\"\u0008\u0008\u0002\u0010\u000e*\u00020\u0001\"\u0008\u0008\u0003\u0010\u0012*\u00020\u00012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u0002H\u00060\u00042\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\u00042\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u0002H\u000e0\u00042\u000c\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u0002H\u00120\u0004H\u0007J\u00a1\u0001\u0010/\u001a\u0008\u0012\u0004\u0012\u0002H\n0\u0004\"\u0008\u0008\u0000\u0010\u0006*\u00020\u0001\"\u0008\u0008\u0001\u0010\u0007*\u00020\u0001\"\u0008\u0008\u0002\u0010\u000e*\u00020\u0001\"\u0008\u0008\u0003\u0010\u0012*\u00020\u0001\"\u0008\u0008\u0004\u0010\n*\u00020\u00012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u0002H\u00060\u00042\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\u00042\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u0002H\u000e0\u00042\u000c\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u0002H\u00120\u00042&\u0008\u0004\u0010\u000b\u001a \u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u0002H\u000e\u0012\u0004\u0012\u0002H\u0012\u0012\u0004\u0012\u0002H\n0\u0014H\u0087\u0008J\u00a4\u0001\u0010/\u001a&\u0012\"\u0012 \u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u0002H\u000e\u0012\u0004\u0012\u0002H\u0012\u0012\u0004\u0012\u0002H\u00160\u00150\u0004\"\u0008\u0008\u0000\u0010\u0006*\u00020\u0001\"\u0008\u0008\u0001\u0010\u0007*\u00020\u0001\"\u0008\u0008\u0002\u0010\u000e*\u00020\u0001\"\u0008\u0008\u0003\u0010\u0012*\u00020\u0001\"\u0008\u0008\u0004\u0010\u0016*\u00020\u00012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u0002H\u00060\u00042\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\u00042\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u0002H\u000e0\u00042\u000c\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u0002H\u00120\u00042\u000c\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u0002H\u00160\u0004H\u0007J\u00bf\u0001\u0010/\u001a\u0008\u0012\u0004\u0012\u0002H\n0\u0004\"\u0008\u0008\u0000\u0010\u0006*\u00020\u0001\"\u0008\u0008\u0001\u0010\u0007*\u00020\u0001\"\u0008\u0008\u0002\u0010\u000e*\u00020\u0001\"\u0008\u0008\u0003\u0010\u0012*\u00020\u0001\"\u0008\u0008\u0004\u0010\u0016*\u00020\u0001\"\u0008\u0008\u0005\u0010\n*\u00020\u00012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u0002H\u00060\u00042\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\u00042\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u0002H\u000e0\u00042\u000c\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u0002H\u00120\u00042\u000c\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u0002H\u00160\u00042,\u0008\u0004\u0010\u000b\u001a&\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u0002H\u000e\u0012\u0004\u0012\u0002H\u0012\u0012\u0004\u0012\u0002H\u0016\u0012\u0004\u0012\u0002H\n0\u0018H\u0087\u0008J\u00c2\u0001\u0010/\u001a,\u0012(\u0012&\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u0002H\u000e\u0012\u0004\u0012\u0002H\u0012\u0012\u0004\u0012\u0002H\u0016\u0012\u0004\u0012\u0002H\u001a0\u00190\u0004\"\u0008\u0008\u0000\u0010\u0006*\u00020\u0001\"\u0008\u0008\u0001\u0010\u0007*\u00020\u0001\"\u0008\u0008\u0002\u0010\u000e*\u00020\u0001\"\u0008\u0008\u0003\u0010\u0012*\u00020\u0001\"\u0008\u0008\u0004\u0010\u0016*\u00020\u0001\"\u0008\u0008\u0005\u0010\u001a*\u00020\u00012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u0002H\u00060\u00042\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\u00042\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u0002H\u000e0\u00042\u000c\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u0002H\u00120\u00042\u000c\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u0002H\u00160\u00042\u000c\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u0002H\u001a0\u0004H\u0007J\u00dd\u0001\u0010/\u001a\u0008\u0012\u0004\u0012\u0002H\n0\u0004\"\u0008\u0008\u0000\u0010\u0006*\u00020\u0001\"\u0008\u0008\u0001\u0010\u0007*\u00020\u0001\"\u0008\u0008\u0002\u0010\u000e*\u00020\u0001\"\u0008\u0008\u0003\u0010\u0012*\u00020\u0001\"\u0008\u0008\u0004\u0010\u0016*\u00020\u0001\"\u0008\u0008\u0005\u0010\u001a*\u00020\u0001\"\u0008\u0008\u0006\u0010\n*\u00020\u00012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u0002H\u00060\u00042\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\u00042\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u0002H\u000e0\u00042\u000c\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u0002H\u00120\u00042\u000c\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u0002H\u00160\u00042\u000c\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u0002H\u001a0\u000422\u0008\u0004\u0010\u000b\u001a,\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u0002H\u000e\u0012\u0004\u0012\u0002H\u0012\u0012\u0004\u0012\u0002H\u0016\u0012\u0004\u0012\u0002H\u001a\u0012\u0004\u0012\u0002H\n0\u001cH\u0087\u0008J\u00fb\u0001\u0010/\u001a\u0008\u0012\u0004\u0012\u0002H\n0\u0004\"\u0008\u0008\u0000\u0010\u0006*\u00020\u0001\"\u0008\u0008\u0001\u0010\u0007*\u00020\u0001\"\u0008\u0008\u0002\u0010\u000e*\u00020\u0001\"\u0008\u0008\u0003\u0010\u0012*\u00020\u0001\"\u0008\u0008\u0004\u0010\u0016*\u00020\u0001\"\u0008\u0008\u0005\u0010\u001a*\u00020\u0001\"\u0008\u0008\u0006\u0010\u001e*\u00020\u0001\"\u0008\u0008\u0007\u0010\n*\u00020\u00012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u0002H\u00060\u00042\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\u00042\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u0002H\u000e0\u00042\u000c\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u0002H\u00120\u00042\u000c\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u0002H\u00160\u00042\u000c\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u0002H\u001a0\u00042\u000c\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u0002H\u001e0\u000428\u0008\u0004\u0010\u000b\u001a2\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u0002H\u000e\u0012\u0004\u0012\u0002H\u0012\u0012\u0004\u0012\u0002H\u0016\u0012\u0004\u0012\u0002H\u001a\u0012\u0004\u0012\u0002H\u001e\u0012\u0004\u0012\u0002H\n0 H\u0087\u0008J\u0099\u0002\u0010/\u001a\u0008\u0012\u0004\u0012\u0002H\n0\u0004\"\u0008\u0008\u0000\u0010\u0006*\u00020\u0001\"\u0008\u0008\u0001\u0010\u0007*\u00020\u0001\"\u0008\u0008\u0002\u0010\u000e*\u00020\u0001\"\u0008\u0008\u0003\u0010\u0012*\u00020\u0001\"\u0008\u0008\u0004\u0010\u0016*\u00020\u0001\"\u0008\u0008\u0005\u0010\u001a*\u00020\u0001\"\u0008\u0008\u0006\u0010\u001e*\u00020\u0001\"\u0008\u0008\u0007\u0010\"*\u00020\u0001\"\u0008\u0008\u0008\u0010\n*\u00020\u00012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u0002H\u00060\u00042\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\u00042\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u0002H\u000e0\u00042\u000c\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u0002H\u00120\u00042\u000c\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u0002H\u00160\u00042\u000c\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u0002H\u001a0\u00042\u000c\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u0002H\u001e0\u00042\u000c\u0010#\u001a\u0008\u0012\u0004\u0012\u0002H\"0\u00042>\u0008\u0004\u0010\u000b\u001a8\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u0002H\u000e\u0012\u0004\u0012\u0002H\u0012\u0012\u0004\u0012\u0002H\u0016\u0012\u0004\u0012\u0002H\u001a\u0012\u0004\u0012\u0002H\u001e\u0012\u0004\u0012\u0002H\"\u0012\u0004\u0012\u0002H\n0$H\u0087\u0008J\u00b7\u0002\u0010/\u001a\u0008\u0012\u0004\u0012\u0002H\n0\u0004\"\u0008\u0008\u0000\u0010\u0006*\u00020\u0001\"\u0008\u0008\u0001\u0010\u0007*\u00020\u0001\"\u0008\u0008\u0002\u0010\u000e*\u00020\u0001\"\u0008\u0008\u0003\u0010\u0012*\u00020\u0001\"\u0008\u0008\u0004\u0010\u0016*\u00020\u0001\"\u0008\u0008\u0005\u0010\u001a*\u00020\u0001\"\u0008\u0008\u0006\u0010\u001e*\u00020\u0001\"\u0008\u0008\u0007\u0010\"*\u00020\u0001\"\u0008\u0008\u0008\u0010%*\u00020\u0001\"\u0008\u0008\t\u0010\n*\u00020\u00012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u0002H\u00060\u00042\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\u00042\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u0002H\u000e0\u00042\u000c\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u0002H\u00120\u00042\u000c\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u0002H\u00160\u00042\u000c\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u0002H\u001a0\u00042\u000c\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u0002H\u001e0\u00042\u000c\u0010#\u001a\u0008\u0012\u0004\u0012\u0002H\"0\u00042\u000c\u0010&\u001a\u0008\u0012\u0004\u0012\u0002H%0\u00042D\u0008\u0004\u0010\u000b\u001a>\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u0002H\u000e\u0012\u0004\u0012\u0002H\u0012\u0012\u0004\u0012\u0002H\u0016\u0012\u0004\u0012\u0002H\u001a\u0012\u0004\u0012\u0002H\u001e\u0012\u0004\u0012\u0002H\"\u0012\u0004\u0012\u0002H%\u0012\u0004\u0012\u0002H\n0\'H\u0087\u0008\u00a8\u00060"
    }
    d2 = {
        "Lcom/squareup/util/rx2/Observables;",
        "",
        "()V",
        "combineLatest",
        "Lio/reactivex/Observable;",
        "Lkotlin/Pair;",
        "T1",
        "T2",
        "source1",
        "source2",
        "R",
        "combineFunction",
        "Lkotlin/Function2;",
        "Lkotlin/Triple;",
        "T3",
        "source3",
        "Lkotlin/Function3;",
        "Lcom/squareup/util/tuple/Quartet;",
        "T4",
        "source4",
        "Lkotlin/Function4;",
        "Lcom/squareup/util/tuple/Quintuple;",
        "T5",
        "source5",
        "Lkotlin/Function5;",
        "Lcom/squareup/util/tuple/Sextuple;",
        "T6",
        "source6",
        "Lkotlin/Function6;",
        "Lcom/squareup/util/tuple/Septuple;",
        "T7",
        "source7",
        "Lkotlin/Function7;",
        "Lcom/squareup/util/tuple/Octuple;",
        "T8",
        "source8",
        "Lkotlin/Function8;",
        "T9",
        "source9",
        "Lkotlin/Function9;",
        "T",
        "sources",
        "",
        "Lkotlin/Function1;",
        "Lkotlin/ParameterName;",
        "name",
        "args",
        "zip",
        "public"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/util/rx2/Observables;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 49
    new-instance v0, Lcom/squareup/util/rx2/Observables;

    invoke-direct {v0}, Lcom/squareup/util/rx2/Observables;-><init>()V

    sput-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Observable<",
            "TT1;>;",
            "Lio/reactivex/Observable<",
            "TT2;>;)",
            "Lio/reactivex/Observable<",
            "Lkotlin/Pair<",
            "TT1;TT2;>;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "source1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source2"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    check-cast p1, Lio/reactivex/ObservableSource;

    check-cast p2, Lio/reactivex/ObservableSource;

    sget-object v0, Lcom/squareup/util/rx2/Observables$combineLatest$3;->INSTANCE:Lcom/squareup/util/rx2/Observables$combineLatest$3;

    check-cast v0, Lkotlin/jvm/functions/Function2;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/squareup/util/rx2/RxKotlinKt$sam$io_reactivex_functions_BiFunction$0;

    invoke-direct {v1, v0}, Lcom/squareup/util/rx2/RxKotlinKt$sam$io_reactivex_functions_BiFunction$0;-><init>(Lkotlin/jvm/functions/Function2;)V

    move-object v0, v1

    :cond_0
    check-cast v0, Lio/reactivex/functions/BiFunction;

    invoke-static {p1, p2, v0}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "Observable.combineLatest\u2026rce2, BiFunction(::Pair))"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            "T3:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Observable<",
            "TT1;>;",
            "Lio/reactivex/Observable<",
            "TT2;>;",
            "Lio/reactivex/Observable<",
            "TT3;>;)",
            "Lio/reactivex/Observable<",
            "Lkotlin/Triple<",
            "TT1;TT2;TT3;>;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "source1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source2"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source3"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    check-cast p1, Lio/reactivex/ObservableSource;

    check-cast p2, Lio/reactivex/ObservableSource;

    check-cast p3, Lio/reactivex/ObservableSource;

    .line 108
    sget-object v0, Lcom/squareup/util/rx2/Observables$combineLatest$5;->INSTANCE:Lcom/squareup/util/rx2/Observables$combineLatest$5;

    check-cast v0, Lkotlin/jvm/functions/Function3;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/squareup/util/rx2/RxKotlinKt$sam$io_reactivex_functions_Function3$0;

    invoke-direct {v1, v0}, Lcom/squareup/util/rx2/RxKotlinKt$sam$io_reactivex_functions_Function3$0;-><init>(Lkotlin/jvm/functions/Function3;)V

    move-object v0, v1

    :cond_0
    check-cast v0, Lio/reactivex/functions/Function3;

    .line 106
    invoke-static {p1, p2, p3, v0}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function3;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "Observable.combineLatest\u2026  Function3(::Triple)\n  )"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            "T3:",
            "Ljava/lang/Object;",
            "T4:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Observable<",
            "TT1;>;",
            "Lio/reactivex/Observable<",
            "TT2;>;",
            "Lio/reactivex/Observable<",
            "TT3;>;",
            "Lio/reactivex/Observable<",
            "TT4;>;)",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/util/tuple/Quartet<",
            "TT1;TT2;TT3;TT4;>;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "source1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source2"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source3"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source4"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 135
    check-cast p1, Lio/reactivex/ObservableSource;

    check-cast p2, Lio/reactivex/ObservableSource;

    check-cast p3, Lio/reactivex/ObservableSource;

    check-cast p4, Lio/reactivex/ObservableSource;

    .line 136
    sget-object v0, Lcom/squareup/util/rx2/Observables$combineLatest$7;->INSTANCE:Lcom/squareup/util/rx2/Observables$combineLatest$7;

    check-cast v0, Lkotlin/jvm/functions/Function4;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/squareup/util/rx2/RxKotlinKt$sam$io_reactivex_functions_Function4$0;

    invoke-direct {v1, v0}, Lcom/squareup/util/rx2/RxKotlinKt$sam$io_reactivex_functions_Function4$0;-><init>(Lkotlin/jvm/functions/Function4;)V

    move-object v0, v1

    :cond_0
    check-cast v0, Lio/reactivex/functions/Function4;

    .line 134
    invoke-static {p1, p2, p3, p4, v0}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function4;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "Observable.combineLatest\u2026 Function4(::Quartet)\n  )"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            "T3:",
            "Ljava/lang/Object;",
            "T4:",
            "Ljava/lang/Object;",
            "T5:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Observable<",
            "TT1;>;",
            "Lio/reactivex/Observable<",
            "TT2;>;",
            "Lio/reactivex/Observable<",
            "TT3;>;",
            "Lio/reactivex/Observable<",
            "TT4;>;",
            "Lio/reactivex/Observable<",
            "TT5;>;)",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/util/tuple/Quintuple<",
            "TT1;TT2;TT3;TT4;TT5;>;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "source1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source2"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source3"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source4"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source5"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 165
    move-object v1, p1

    check-cast v1, Lio/reactivex/ObservableSource;

    move-object v2, p2

    check-cast v2, Lio/reactivex/ObservableSource;

    move-object v3, p3

    check-cast v3, Lio/reactivex/ObservableSource;

    move-object v4, p4

    check-cast v4, Lio/reactivex/ObservableSource;

    move-object v5, p5

    check-cast v5, Lio/reactivex/ObservableSource;

    .line 166
    sget-object p1, Lcom/squareup/util/rx2/Observables$combineLatest$9;->INSTANCE:Lcom/squareup/util/rx2/Observables$combineLatest$9;

    check-cast p1, Lkotlin/jvm/functions/Function5;

    if-eqz p1, :cond_0

    new-instance p2, Lcom/squareup/util/rx2/RxKotlinKt$sam$io_reactivex_functions_Function5$0;

    invoke-direct {p2, p1}, Lcom/squareup/util/rx2/RxKotlinKt$sam$io_reactivex_functions_Function5$0;-><init>(Lkotlin/jvm/functions/Function5;)V

    move-object p1, p2

    :cond_0
    move-object v6, p1

    check-cast v6, Lio/reactivex/functions/Function5;

    .line 164
    invoke-static/range {v1 .. v6}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function5;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "Observable.combineLatest\u2026unction5(::Quintuple)\n  )"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            "T3:",
            "Ljava/lang/Object;",
            "T4:",
            "Ljava/lang/Object;",
            "T5:",
            "Ljava/lang/Object;",
            "T6:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Observable<",
            "TT1;>;",
            "Lio/reactivex/Observable<",
            "TT2;>;",
            "Lio/reactivex/Observable<",
            "TT3;>;",
            "Lio/reactivex/Observable<",
            "TT4;>;",
            "Lio/reactivex/Observable<",
            "TT5;>;",
            "Lio/reactivex/Observable<",
            "TT6;>;)",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/util/tuple/Sextuple<",
            "TT1;TT2;TT3;TT4;TT5;TT6;>;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "source1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source2"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source3"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source4"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source5"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source6"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 199
    move-object v1, p1

    check-cast v1, Lio/reactivex/ObservableSource;

    move-object v2, p2

    check-cast v2, Lio/reactivex/ObservableSource;

    move-object v3, p3

    check-cast v3, Lio/reactivex/ObservableSource;

    move-object v4, p4

    check-cast v4, Lio/reactivex/ObservableSource;

    move-object v5, p5

    check-cast v5, Lio/reactivex/ObservableSource;

    move-object v6, p6

    check-cast v6, Lio/reactivex/ObservableSource;

    .line 200
    sget-object p1, Lcom/squareup/util/rx2/Observables$combineLatest$11;->INSTANCE:Lcom/squareup/util/rx2/Observables$combineLatest$11;

    check-cast p1, Lkotlin/jvm/functions/Function6;

    if-eqz p1, :cond_0

    new-instance p2, Lcom/squareup/util/rx2/RxKotlinKt$sam$io_reactivex_functions_Function6$0;

    invoke-direct {p2, p1}, Lcom/squareup/util/rx2/RxKotlinKt$sam$io_reactivex_functions_Function6$0;-><init>(Lkotlin/jvm/functions/Function6;)V

    move-object p1, p2

    :cond_0
    move-object v7, p1

    check-cast v7, Lio/reactivex/functions/Function6;

    .line 198
    invoke-static/range {v1 .. v7}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function6;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "Observable.combineLatest\u2026Function6(::Sextuple)\n  )"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            "T3:",
            "Ljava/lang/Object;",
            "T4:",
            "Ljava/lang/Object;",
            "T5:",
            "Ljava/lang/Object;",
            "T6:",
            "Ljava/lang/Object;",
            "T7:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Observable<",
            "TT1;>;",
            "Lio/reactivex/Observable<",
            "TT2;>;",
            "Lio/reactivex/Observable<",
            "TT3;>;",
            "Lio/reactivex/Observable<",
            "TT4;>;",
            "Lio/reactivex/Observable<",
            "TT5;>;",
            "Lio/reactivex/Observable<",
            "TT6;>;",
            "Lio/reactivex/Observable<",
            "TT7;>;)",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/util/tuple/Septuple<",
            "TT1;TT2;TT3;TT4;TT5;TT6;TT7;>;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    move-object/from16 v5, p6

    move-object/from16 v6, p7

    const-string v7, "source1"

    invoke-static {v0, v7}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v7, "source2"

    invoke-static {v1, v7}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v7, "source3"

    invoke-static {v2, v7}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v7, "source4"

    invoke-static {v3, v7}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v7, "source5"

    invoke-static {v4, v7}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v7, "source6"

    invoke-static {v5, v7}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v7, "source7"

    invoke-static {v6, v7}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 236
    move-object v8, v0

    check-cast v8, Lio/reactivex/ObservableSource;

    move-object v9, v1

    check-cast v9, Lio/reactivex/ObservableSource;

    move-object v10, v2

    check-cast v10, Lio/reactivex/ObservableSource;

    move-object v11, v3

    check-cast v11, Lio/reactivex/ObservableSource;

    move-object v12, v4

    check-cast v12, Lio/reactivex/ObservableSource;

    move-object v13, v5

    check-cast v13, Lio/reactivex/ObservableSource;

    move-object v14, v6

    check-cast v14, Lio/reactivex/ObservableSource;

    .line 237
    sget-object v0, Lcom/squareup/util/rx2/Observables$combineLatest$13;->INSTANCE:Lcom/squareup/util/rx2/Observables$combineLatest$13;

    check-cast v0, Lkotlin/jvm/functions/Function7;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/squareup/util/rx2/RxKotlinKt$sam$io_reactivex_functions_Function7$0;

    invoke-direct {v1, v0}, Lcom/squareup/util/rx2/RxKotlinKt$sam$io_reactivex_functions_Function7$0;-><init>(Lkotlin/jvm/functions/Function7;)V

    move-object v0, v1

    :cond_0
    move-object v15, v0

    check-cast v15, Lio/reactivex/functions/Function7;

    .line 235
    invoke-static/range {v8 .. v15}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function7;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observable.combineLatest\u2026Function7(::Septuple)\n  )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            "T3:",
            "Ljava/lang/Object;",
            "T4:",
            "Ljava/lang/Object;",
            "T5:",
            "Ljava/lang/Object;",
            "T6:",
            "Ljava/lang/Object;",
            "T7:",
            "Ljava/lang/Object;",
            "T8:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Observable<",
            "TT1;>;",
            "Lio/reactivex/Observable<",
            "TT2;>;",
            "Lio/reactivex/Observable<",
            "TT3;>;",
            "Lio/reactivex/Observable<",
            "TT4;>;",
            "Lio/reactivex/Observable<",
            "TT5;>;",
            "Lio/reactivex/Observable<",
            "TT6;>;",
            "Lio/reactivex/Observable<",
            "TT7;>;",
            "Lio/reactivex/Observable<",
            "TT8;>;)",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/util/tuple/Octuple<",
            "TT1;TT2;TT3;TT4;TT5;TT6;TT7;TT8;>;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    move-object/from16 v5, p6

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    const-string v8, "source1"

    invoke-static {v0, v8}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v8, "source2"

    invoke-static {v1, v8}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v8, "source3"

    invoke-static {v2, v8}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v8, "source4"

    invoke-static {v3, v8}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v8, "source5"

    invoke-static {v4, v8}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v8, "source6"

    invoke-static {v5, v8}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v8, "source7"

    invoke-static {v6, v8}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v8, "source8"

    invoke-static {v7, v8}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 275
    move-object v9, v0

    check-cast v9, Lio/reactivex/ObservableSource;

    move-object v10, v1

    check-cast v10, Lio/reactivex/ObservableSource;

    move-object v11, v2

    check-cast v11, Lio/reactivex/ObservableSource;

    move-object v12, v3

    check-cast v12, Lio/reactivex/ObservableSource;

    move-object v13, v4

    check-cast v13, Lio/reactivex/ObservableSource;

    move-object v14, v5

    check-cast v14, Lio/reactivex/ObservableSource;

    move-object v15, v6

    check-cast v15, Lio/reactivex/ObservableSource;

    move-object/from16 v16, v7

    check-cast v16, Lio/reactivex/ObservableSource;

    .line 276
    sget-object v0, Lcom/squareup/util/rx2/Observables$combineLatest$15;->INSTANCE:Lcom/squareup/util/rx2/Observables$combineLatest$15;

    check-cast v0, Lkotlin/jvm/functions/Function8;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/squareup/util/rx2/RxKotlinKt$sam$io_reactivex_functions_Function8$0;

    invoke-direct {v1, v0}, Lcom/squareup/util/rx2/RxKotlinKt$sam$io_reactivex_functions_Function8$0;-><init>(Lkotlin/jvm/functions/Function8;)V

    move-object v0, v1

    :cond_0
    move-object/from16 v17, v0

    check-cast v17, Lio/reactivex/functions/Function8;

    .line 274
    invoke-static/range {v9 .. v17}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function8;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observable.combineLatest\u2026 Function8(::Octuple)\n  )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lkotlin/jvm/functions/Function9;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            "T3:",
            "Ljava/lang/Object;",
            "T4:",
            "Ljava/lang/Object;",
            "T5:",
            "Ljava/lang/Object;",
            "T6:",
            "Ljava/lang/Object;",
            "T7:",
            "Ljava/lang/Object;",
            "T8:",
            "Ljava/lang/Object;",
            "T9:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Observable<",
            "TT1;>;",
            "Lio/reactivex/Observable<",
            "TT2;>;",
            "Lio/reactivex/Observable<",
            "TT3;>;",
            "Lio/reactivex/Observable<",
            "TT4;>;",
            "Lio/reactivex/Observable<",
            "TT5;>;",
            "Lio/reactivex/Observable<",
            "TT6;>;",
            "Lio/reactivex/Observable<",
            "TT7;>;",
            "Lio/reactivex/Observable<",
            "TT8;>;",
            "Lio/reactivex/Observable<",
            "TT9;>;",
            "Lkotlin/jvm/functions/Function9<",
            "-TT1;-TT2;-TT3;-TT4;-TT5;-TT6;-TT7;-TT8;-TT9;+TR;>;)",
            "Lio/reactivex/Observable<",
            "TR;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "source1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source2"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source3"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source4"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source5"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source6"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source7"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source8"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source9"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "combineFunction"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 294
    check-cast p1, Lio/reactivex/ObservableSource;

    check-cast p2, Lio/reactivex/ObservableSource;

    check-cast p3, Lio/reactivex/ObservableSource;

    check-cast p4, Lio/reactivex/ObservableSource;

    check-cast p5, Lio/reactivex/ObservableSource;

    check-cast p6, Lio/reactivex/ObservableSource;

    check-cast p7, Lio/reactivex/ObservableSource;

    check-cast p8, Lio/reactivex/ObservableSource;

    check-cast p9, Lio/reactivex/ObservableSource;

    .line 295
    new-instance v0, Lcom/squareup/util/rx2/Observables$combineLatest$16;

    invoke-direct {v0, p10}, Lcom/squareup/util/rx2/Observables$combineLatest$16;-><init>(Lkotlin/jvm/functions/Function9;)V

    move-object p10, v0

    check-cast p10, Lio/reactivex/functions/Function9;

    .line 293
    invoke-static/range {p1 .. p10}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function9;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "Observable.combineLatest\u2026, t7, t8, t9)\n      }\n  )"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lkotlin/jvm/functions/Function8;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            "T3:",
            "Ljava/lang/Object;",
            "T4:",
            "Ljava/lang/Object;",
            "T5:",
            "Ljava/lang/Object;",
            "T6:",
            "Ljava/lang/Object;",
            "T7:",
            "Ljava/lang/Object;",
            "T8:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Observable<",
            "TT1;>;",
            "Lio/reactivex/Observable<",
            "TT2;>;",
            "Lio/reactivex/Observable<",
            "TT3;>;",
            "Lio/reactivex/Observable<",
            "TT4;>;",
            "Lio/reactivex/Observable<",
            "TT5;>;",
            "Lio/reactivex/Observable<",
            "TT6;>;",
            "Lio/reactivex/Observable<",
            "TT7;>;",
            "Lio/reactivex/Observable<",
            "TT8;>;",
            "Lkotlin/jvm/functions/Function8<",
            "-TT1;-TT2;-TT3;-TT4;-TT5;-TT6;-TT7;-TT8;+TR;>;)",
            "Lio/reactivex/Observable<",
            "TR;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "source1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source2"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source3"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source4"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source5"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source6"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source7"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source8"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "combineFunction"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 254
    check-cast p1, Lio/reactivex/ObservableSource;

    check-cast p2, Lio/reactivex/ObservableSource;

    check-cast p3, Lio/reactivex/ObservableSource;

    check-cast p4, Lio/reactivex/ObservableSource;

    check-cast p5, Lio/reactivex/ObservableSource;

    check-cast p6, Lio/reactivex/ObservableSource;

    check-cast p7, Lio/reactivex/ObservableSource;

    check-cast p8, Lio/reactivex/ObservableSource;

    .line 255
    new-instance v0, Lcom/squareup/util/rx2/Observables$combineLatest$14;

    invoke-direct {v0, p9}, Lcom/squareup/util/rx2/Observables$combineLatest$14;-><init>(Lkotlin/jvm/functions/Function8;)V

    move-object p9, v0

    check-cast p9, Lio/reactivex/functions/Function8;

    .line 253
    invoke-static/range {p1 .. p9}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function8;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "Observable.combineLatest\u2026, t6, t7, t8)\n      }\n  )"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lkotlin/jvm/functions/Function7;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            "T3:",
            "Ljava/lang/Object;",
            "T4:",
            "Ljava/lang/Object;",
            "T5:",
            "Ljava/lang/Object;",
            "T6:",
            "Ljava/lang/Object;",
            "T7:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Observable<",
            "TT1;>;",
            "Lio/reactivex/Observable<",
            "TT2;>;",
            "Lio/reactivex/Observable<",
            "TT3;>;",
            "Lio/reactivex/Observable<",
            "TT4;>;",
            "Lio/reactivex/Observable<",
            "TT5;>;",
            "Lio/reactivex/Observable<",
            "TT6;>;",
            "Lio/reactivex/Observable<",
            "TT7;>;",
            "Lkotlin/jvm/functions/Function7<",
            "-TT1;-TT2;-TT3;-TT4;-TT5;-TT6;-TT7;+TR;>;)",
            "Lio/reactivex/Observable<",
            "TR;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "source1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source2"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source3"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source4"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source5"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source6"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source7"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "combineFunction"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 216
    check-cast p1, Lio/reactivex/ObservableSource;

    check-cast p2, Lio/reactivex/ObservableSource;

    check-cast p3, Lio/reactivex/ObservableSource;

    check-cast p4, Lio/reactivex/ObservableSource;

    check-cast p5, Lio/reactivex/ObservableSource;

    check-cast p6, Lio/reactivex/ObservableSource;

    check-cast p7, Lio/reactivex/ObservableSource;

    .line 217
    new-instance v0, Lcom/squareup/util/rx2/Observables$combineLatest$12;

    invoke-direct {v0, p8}, Lcom/squareup/util/rx2/Observables$combineLatest$12;-><init>(Lkotlin/jvm/functions/Function7;)V

    move-object p8, v0

    check-cast p8, Lio/reactivex/functions/Function7;

    .line 215
    invoke-static/range {p1 .. p8}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function7;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "Observable.combineLatest\u2026, t5, t6, t7)\n      }\n  )"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lkotlin/jvm/functions/Function6;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            "T3:",
            "Ljava/lang/Object;",
            "T4:",
            "Ljava/lang/Object;",
            "T5:",
            "Ljava/lang/Object;",
            "T6:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Observable<",
            "TT1;>;",
            "Lio/reactivex/Observable<",
            "TT2;>;",
            "Lio/reactivex/Observable<",
            "TT3;>;",
            "Lio/reactivex/Observable<",
            "TT4;>;",
            "Lio/reactivex/Observable<",
            "TT5;>;",
            "Lio/reactivex/Observable<",
            "TT6;>;",
            "Lkotlin/jvm/functions/Function6<",
            "-TT1;-TT2;-TT3;-TT4;-TT5;-TT6;+TR;>;)",
            "Lio/reactivex/Observable<",
            "TR;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "source1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source2"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source3"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source4"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source5"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source6"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "combineFunction"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 180
    check-cast p1, Lio/reactivex/ObservableSource;

    check-cast p2, Lio/reactivex/ObservableSource;

    check-cast p3, Lio/reactivex/ObservableSource;

    check-cast p4, Lio/reactivex/ObservableSource;

    check-cast p5, Lio/reactivex/ObservableSource;

    check-cast p6, Lio/reactivex/ObservableSource;

    .line 181
    new-instance v0, Lcom/squareup/util/rx2/Observables$combineLatest$10;

    invoke-direct {v0, p7}, Lcom/squareup/util/rx2/Observables$combineLatest$10;-><init>(Lkotlin/jvm/functions/Function6;)V

    move-object p7, v0

    check-cast p7, Lio/reactivex/functions/Function6;

    .line 179
    invoke-static/range {p1 .. p7}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function6;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "Observable.combineLatest\u2026, t4, t5, t6)\n      }\n  )"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lkotlin/jvm/functions/Function5;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            "T3:",
            "Ljava/lang/Object;",
            "T4:",
            "Ljava/lang/Object;",
            "T5:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Observable<",
            "TT1;>;",
            "Lio/reactivex/Observable<",
            "TT2;>;",
            "Lio/reactivex/Observable<",
            "TT3;>;",
            "Lio/reactivex/Observable<",
            "TT4;>;",
            "Lio/reactivex/Observable<",
            "TT5;>;",
            "Lkotlin/jvm/functions/Function5<",
            "-TT1;-TT2;-TT3;-TT4;-TT5;+TR;>;)",
            "Lio/reactivex/Observable<",
            "TR;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "source1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source2"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source3"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source4"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source5"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "combineFunction"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 149
    check-cast p1, Lio/reactivex/ObservableSource;

    check-cast p2, Lio/reactivex/ObservableSource;

    check-cast p3, Lio/reactivex/ObservableSource;

    check-cast p4, Lio/reactivex/ObservableSource;

    check-cast p5, Lio/reactivex/ObservableSource;

    .line 150
    new-instance v0, Lcom/squareup/util/rx2/Observables$combineLatest$8;

    invoke-direct {v0, p6}, Lcom/squareup/util/rx2/Observables$combineLatest$8;-><init>(Lkotlin/jvm/functions/Function5;)V

    move-object p6, v0

    check-cast p6, Lio/reactivex/functions/Function5;

    .line 148
    invoke-static/range {p1 .. p6}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function5;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "Observable.combineLatest\u2026t1, t2, t3, t4, t5) }\n  )"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lkotlin/jvm/functions/Function4;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            "T3:",
            "Ljava/lang/Object;",
            "T4:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Observable<",
            "TT1;>;",
            "Lio/reactivex/Observable<",
            "TT2;>;",
            "Lio/reactivex/Observable<",
            "TT3;>;",
            "Lio/reactivex/Observable<",
            "TT4;>;",
            "Lkotlin/jvm/functions/Function4<",
            "-TT1;-TT2;-TT3;-TT4;+TR;>;)",
            "Lio/reactivex/Observable<",
            "TR;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "source1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source2"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source3"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source4"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "combineFunction"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 120
    check-cast p1, Lio/reactivex/ObservableSource;

    check-cast p2, Lio/reactivex/ObservableSource;

    check-cast p3, Lio/reactivex/ObservableSource;

    check-cast p4, Lio/reactivex/ObservableSource;

    .line 121
    new-instance v0, Lcom/squareup/util/rx2/Observables$combineLatest$6;

    invoke-direct {v0, p5}, Lcom/squareup/util/rx2/Observables$combineLatest$6;-><init>(Lkotlin/jvm/functions/Function4;)V

    check-cast v0, Lio/reactivex/functions/Function4;

    .line 119
    invoke-static {p1, p2, p3, p4, v0}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function4;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "Observable.combineLatest\u2026ion(t1, t2, t3, t4) }\n  )"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lkotlin/jvm/functions/Function3;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            "T3:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Observable<",
            "TT1;>;",
            "Lio/reactivex/Observable<",
            "TT2;>;",
            "Lio/reactivex/Observable<",
            "TT3;>;",
            "Lkotlin/jvm/functions/Function3<",
            "-TT1;-TT2;-TT3;+TR;>;)",
            "Lio/reactivex/Observable<",
            "TR;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "source1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source2"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source3"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "combineFunction"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    check-cast p1, Lio/reactivex/ObservableSource;

    check-cast p2, Lio/reactivex/ObservableSource;

    check-cast p3, Lio/reactivex/ObservableSource;

    .line 94
    new-instance v0, Lcom/squareup/util/rx2/Observables$combineLatest$4;

    invoke-direct {v0, p4}, Lcom/squareup/util/rx2/Observables$combineLatest$4;-><init>(Lkotlin/jvm/functions/Function3;)V

    check-cast v0, Lio/reactivex/functions/Function3;

    .line 92
    invoke-static {p1, p2, p3, v0}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function3;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "Observable.combineLatest\u2026unction(t1, t2, t3) }\n  )"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;Lkotlin/jvm/functions/Function2;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Observable<",
            "TT1;>;",
            "Lio/reactivex/Observable<",
            "TT2;>;",
            "Lkotlin/jvm/functions/Function2<",
            "-TT1;-TT2;+TR;>;)",
            "Lio/reactivex/Observable<",
            "TR;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "source1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source2"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "combineFunction"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    check-cast p1, Lio/reactivex/ObservableSource;

    check-cast p2, Lio/reactivex/ObservableSource;

    .line 59
    new-instance v0, Lcom/squareup/util/rx2/Observables$combineLatest$1;

    invoke-direct {v0, p3}, Lcom/squareup/util/rx2/Observables$combineLatest$1;-><init>(Lkotlin/jvm/functions/Function2;)V

    check-cast v0, Lio/reactivex/functions/BiFunction;

    .line 57
    invoke-static {p1, p2, v0}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "Observable.combineLatest\u2026ineFunction(t1, t2) }\n  )"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final combineLatest(Ljava/lang/Iterable;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Iterable<",
            "+",
            "Lio/reactivex/Observable<",
            "TT;>;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Iterable<",
            "+TT;>;+TR;>;)",
            "Lio/reactivex/Observable<",
            "TR;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "sources"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "combineFunction"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    new-instance v0, Lcom/squareup/util/rx2/Observables$combineLatest$2;

    invoke-direct {v0, p2}, Lcom/squareup/util/rx2/Observables$combineLatest$2;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-static {p1, v0}, Lio/reactivex/Observable;->combineLatest(Ljava/lang/Iterable;Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "Observable.combineLatest\u2026ements.map { it as T }) }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final zip(Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Observable<",
            "TT1;>;",
            "Lio/reactivex/Observable<",
            "TT2;>;)",
            "Lio/reactivex/Observable<",
            "Lkotlin/Pair<",
            "TT1;TT2;>;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "source1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source2"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 318
    check-cast p1, Lio/reactivex/ObservableSource;

    check-cast p2, Lio/reactivex/ObservableSource;

    sget-object v0, Lcom/squareup/util/rx2/Observables$zip$2;->INSTANCE:Lcom/squareup/util/rx2/Observables$zip$2;

    check-cast v0, Lkotlin/jvm/functions/Function2;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/squareup/util/rx2/RxKotlinKt$sam$io_reactivex_functions_BiFunction$0;

    invoke-direct {v1, v0}, Lcom/squareup/util/rx2/RxKotlinKt$sam$io_reactivex_functions_BiFunction$0;-><init>(Lkotlin/jvm/functions/Function2;)V

    move-object v0, v1

    :cond_0
    check-cast v0, Lio/reactivex/functions/BiFunction;

    invoke-static {p1, p2, v0}, Lio/reactivex/Observable;->zip(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "Observable.zip(source1, \u2026rce2, BiFunction(::Pair))"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final zip(Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            "T3:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Observable<",
            "TT1;>;",
            "Lio/reactivex/Observable<",
            "TT2;>;",
            "Lio/reactivex/Observable<",
            "TT3;>;)",
            "Lio/reactivex/Observable<",
            "Lkotlin/Triple<",
            "TT1;TT2;TT3;>;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "source1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source2"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source3"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 341
    check-cast p1, Lio/reactivex/ObservableSource;

    check-cast p2, Lio/reactivex/ObservableSource;

    check-cast p3, Lio/reactivex/ObservableSource;

    sget-object v0, Lcom/squareup/util/rx2/Observables$zip$4;->INSTANCE:Lcom/squareup/util/rx2/Observables$zip$4;

    check-cast v0, Lkotlin/jvm/functions/Function3;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/squareup/util/rx2/RxKotlinKt$sam$io_reactivex_functions_Function3$0;

    invoke-direct {v1, v0}, Lcom/squareup/util/rx2/RxKotlinKt$sam$io_reactivex_functions_Function3$0;-><init>(Lkotlin/jvm/functions/Function3;)V

    move-object v0, v1

    :cond_0
    check-cast v0, Lio/reactivex/functions/Function3;

    invoke-static {p1, p2, p3, v0}, Lio/reactivex/Observable;->zip(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function3;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "Observable.zip(source1, \u2026ce3, Function3(::Triple))"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final zip(Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            "T3:",
            "Ljava/lang/Object;",
            "T4:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Observable<",
            "TT1;>;",
            "Lio/reactivex/Observable<",
            "TT2;>;",
            "Lio/reactivex/Observable<",
            "TT3;>;",
            "Lio/reactivex/Observable<",
            "TT4;>;)",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/util/tuple/Quartet<",
            "TT1;TT2;TT3;TT4;>;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "source1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source2"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source3"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source4"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 367
    check-cast p1, Lio/reactivex/ObservableSource;

    check-cast p2, Lio/reactivex/ObservableSource;

    check-cast p3, Lio/reactivex/ObservableSource;

    check-cast p4, Lio/reactivex/ObservableSource;

    .line 368
    sget-object v0, Lcom/squareup/util/rx2/Observables$zip$6;->INSTANCE:Lcom/squareup/util/rx2/Observables$zip$6;

    check-cast v0, Lkotlin/jvm/functions/Function4;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/squareup/util/rx2/RxKotlinKt$sam$io_reactivex_functions_Function4$0;

    invoke-direct {v1, v0}, Lcom/squareup/util/rx2/RxKotlinKt$sam$io_reactivex_functions_Function4$0;-><init>(Lkotlin/jvm/functions/Function4;)V

    move-object v0, v1

    :cond_0
    check-cast v0, Lio/reactivex/functions/Function4;

    .line 366
    invoke-static {p1, p2, p3, p4, v0}, Lio/reactivex/Observable;->zip(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function4;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "Observable.zip(\n      so\u2026 Function4(::Quartet)\n  )"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final zip(Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            "T3:",
            "Ljava/lang/Object;",
            "T4:",
            "Ljava/lang/Object;",
            "T5:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Observable<",
            "TT1;>;",
            "Lio/reactivex/Observable<",
            "TT2;>;",
            "Lio/reactivex/Observable<",
            "TT3;>;",
            "Lio/reactivex/Observable<",
            "TT4;>;",
            "Lio/reactivex/Observable<",
            "TT5;>;)",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/util/tuple/Quintuple<",
            "TT1;TT2;TT3;TT4;TT5;>;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "source1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source2"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source3"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source4"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source5"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 397
    move-object v1, p1

    check-cast v1, Lio/reactivex/ObservableSource;

    move-object v2, p2

    check-cast v2, Lio/reactivex/ObservableSource;

    move-object v3, p3

    check-cast v3, Lio/reactivex/ObservableSource;

    move-object v4, p4

    check-cast v4, Lio/reactivex/ObservableSource;

    move-object v5, p5

    check-cast v5, Lio/reactivex/ObservableSource;

    .line 398
    sget-object p1, Lcom/squareup/util/rx2/Observables$zip$8;->INSTANCE:Lcom/squareup/util/rx2/Observables$zip$8;

    check-cast p1, Lkotlin/jvm/functions/Function5;

    if-eqz p1, :cond_0

    new-instance p2, Lcom/squareup/util/rx2/RxKotlinKt$sam$io_reactivex_functions_Function5$0;

    invoke-direct {p2, p1}, Lcom/squareup/util/rx2/RxKotlinKt$sam$io_reactivex_functions_Function5$0;-><init>(Lkotlin/jvm/functions/Function5;)V

    move-object p1, p2

    :cond_0
    move-object v6, p1

    check-cast v6, Lio/reactivex/functions/Function5;

    .line 396
    invoke-static/range {v1 .. v6}, Lio/reactivex/Observable;->zip(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function5;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "Observable.zip(\n      so\u2026unction5(::Quintuple)\n  )"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final zip(Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            "T3:",
            "Ljava/lang/Object;",
            "T4:",
            "Ljava/lang/Object;",
            "T5:",
            "Ljava/lang/Object;",
            "T6:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Observable<",
            "TT1;>;",
            "Lio/reactivex/Observable<",
            "TT2;>;",
            "Lio/reactivex/Observable<",
            "TT3;>;",
            "Lio/reactivex/Observable<",
            "TT4;>;",
            "Lio/reactivex/Observable<",
            "TT5;>;",
            "Lio/reactivex/Observable<",
            "TT6;>;)",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/util/tuple/Sextuple<",
            "TT1;TT2;TT3;TT4;TT5;TT6;>;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "source1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source2"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source3"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source4"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source5"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source6"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 433
    move-object v1, p1

    check-cast v1, Lio/reactivex/ObservableSource;

    move-object v2, p2

    check-cast v2, Lio/reactivex/ObservableSource;

    move-object v3, p3

    check-cast v3, Lio/reactivex/ObservableSource;

    move-object v4, p4

    check-cast v4, Lio/reactivex/ObservableSource;

    move-object v5, p5

    check-cast v5, Lio/reactivex/ObservableSource;

    move-object v6, p6

    check-cast v6, Lio/reactivex/ObservableSource;

    .line 434
    sget-object p1, Lcom/squareup/util/rx2/Observables$zip$10;->INSTANCE:Lcom/squareup/util/rx2/Observables$zip$10;

    check-cast p1, Lkotlin/jvm/functions/Function6;

    if-eqz p1, :cond_0

    new-instance p2, Lcom/squareup/util/rx2/RxKotlinKt$sam$io_reactivex_functions_Function6$0;

    invoke-direct {p2, p1}, Lcom/squareup/util/rx2/RxKotlinKt$sam$io_reactivex_functions_Function6$0;-><init>(Lkotlin/jvm/functions/Function6;)V

    move-object p1, p2

    :cond_0
    move-object v7, p1

    check-cast v7, Lio/reactivex/functions/Function6;

    .line 432
    invoke-static/range {v1 .. v7}, Lio/reactivex/Observable;->zip(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function6;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "Observable.zip(\n      so\u2026Function6(::Sextuple)\n  )"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final zip(Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lkotlin/jvm/functions/Function9;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            "T3:",
            "Ljava/lang/Object;",
            "T4:",
            "Ljava/lang/Object;",
            "T5:",
            "Ljava/lang/Object;",
            "T6:",
            "Ljava/lang/Object;",
            "T7:",
            "Ljava/lang/Object;",
            "T8:",
            "Ljava/lang/Object;",
            "T9:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Observable<",
            "TT1;>;",
            "Lio/reactivex/Observable<",
            "TT2;>;",
            "Lio/reactivex/Observable<",
            "TT3;>;",
            "Lio/reactivex/Observable<",
            "TT4;>;",
            "Lio/reactivex/Observable<",
            "TT5;>;",
            "Lio/reactivex/Observable<",
            "TT6;>;",
            "Lio/reactivex/Observable<",
            "TT7;>;",
            "Lio/reactivex/Observable<",
            "TT8;>;",
            "Lio/reactivex/Observable<",
            "TT9;>;",
            "Lkotlin/jvm/functions/Function9<",
            "-TT1;-TT2;-TT3;-TT4;-TT5;-TT6;-TT7;-TT8;-TT9;+TR;>;)",
            "Lio/reactivex/Observable<",
            "TR;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "source1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source2"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source3"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source4"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source5"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source6"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source7"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source8"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source9"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "combineFunction"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 490
    check-cast p1, Lio/reactivex/ObservableSource;

    check-cast p2, Lio/reactivex/ObservableSource;

    check-cast p3, Lio/reactivex/ObservableSource;

    check-cast p4, Lio/reactivex/ObservableSource;

    check-cast p5, Lio/reactivex/ObservableSource;

    check-cast p6, Lio/reactivex/ObservableSource;

    check-cast p7, Lio/reactivex/ObservableSource;

    check-cast p8, Lio/reactivex/ObservableSource;

    check-cast p9, Lio/reactivex/ObservableSource;

    .line 491
    new-instance v0, Lcom/squareup/util/rx2/Observables$zip$13;

    invoke-direct {v0, p10}, Lcom/squareup/util/rx2/Observables$zip$13;-><init>(Lkotlin/jvm/functions/Function9;)V

    move-object p10, v0

    check-cast p10, Lio/reactivex/functions/Function9;

    .line 489
    invoke-static/range {p1 .. p10}, Lio/reactivex/Observable;->zip(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function9;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "Observable.zip(\n      so\u2026, t7, t8, t9)\n      }\n  )"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final zip(Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lkotlin/jvm/functions/Function8;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            "T3:",
            "Ljava/lang/Object;",
            "T4:",
            "Ljava/lang/Object;",
            "T5:",
            "Ljava/lang/Object;",
            "T6:",
            "Ljava/lang/Object;",
            "T7:",
            "Ljava/lang/Object;",
            "T8:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Observable<",
            "TT1;>;",
            "Lio/reactivex/Observable<",
            "TT2;>;",
            "Lio/reactivex/Observable<",
            "TT3;>;",
            "Lio/reactivex/Observable<",
            "TT4;>;",
            "Lio/reactivex/Observable<",
            "TT5;>;",
            "Lio/reactivex/Observable<",
            "TT6;>;",
            "Lio/reactivex/Observable<",
            "TT7;>;",
            "Lio/reactivex/Observable<",
            "TT8;>;",
            "Lkotlin/jvm/functions/Function8<",
            "-TT1;-TT2;-TT3;-TT4;-TT5;-TT6;-TT7;-TT8;+TR;>;)",
            "Lio/reactivex/Observable<",
            "TR;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "source1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source2"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source3"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source4"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source5"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source6"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source7"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source8"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "combineFunction"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 469
    check-cast p1, Lio/reactivex/ObservableSource;

    check-cast p2, Lio/reactivex/ObservableSource;

    check-cast p3, Lio/reactivex/ObservableSource;

    check-cast p4, Lio/reactivex/ObservableSource;

    check-cast p5, Lio/reactivex/ObservableSource;

    check-cast p6, Lio/reactivex/ObservableSource;

    check-cast p7, Lio/reactivex/ObservableSource;

    check-cast p8, Lio/reactivex/ObservableSource;

    .line 470
    new-instance v0, Lcom/squareup/util/rx2/Observables$zip$12;

    invoke-direct {v0, p9}, Lcom/squareup/util/rx2/Observables$zip$12;-><init>(Lkotlin/jvm/functions/Function8;)V

    move-object p9, v0

    check-cast p9, Lio/reactivex/functions/Function8;

    .line 468
    invoke-static/range {p1 .. p9}, Lio/reactivex/Observable;->zip(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function8;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "Observable.zip(\n      so\u2026, t6, t7, t8)\n      }\n  )"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final zip(Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lkotlin/jvm/functions/Function7;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            "T3:",
            "Ljava/lang/Object;",
            "T4:",
            "Ljava/lang/Object;",
            "T5:",
            "Ljava/lang/Object;",
            "T6:",
            "Ljava/lang/Object;",
            "T7:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Observable<",
            "TT1;>;",
            "Lio/reactivex/Observable<",
            "TT2;>;",
            "Lio/reactivex/Observable<",
            "TT3;>;",
            "Lio/reactivex/Observable<",
            "TT4;>;",
            "Lio/reactivex/Observable<",
            "TT5;>;",
            "Lio/reactivex/Observable<",
            "TT6;>;",
            "Lio/reactivex/Observable<",
            "TT7;>;",
            "Lkotlin/jvm/functions/Function7<",
            "-TT1;-TT2;-TT3;-TT4;-TT5;-TT6;-TT7;+TR;>;)",
            "Lio/reactivex/Observable<",
            "TR;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "source1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source2"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source3"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source4"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source5"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source6"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source7"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "combineFunction"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 449
    check-cast p1, Lio/reactivex/ObservableSource;

    check-cast p2, Lio/reactivex/ObservableSource;

    check-cast p3, Lio/reactivex/ObservableSource;

    check-cast p4, Lio/reactivex/ObservableSource;

    check-cast p5, Lio/reactivex/ObservableSource;

    check-cast p6, Lio/reactivex/ObservableSource;

    check-cast p7, Lio/reactivex/ObservableSource;

    .line 450
    new-instance v0, Lcom/squareup/util/rx2/Observables$zip$11;

    invoke-direct {v0, p8}, Lcom/squareup/util/rx2/Observables$zip$11;-><init>(Lkotlin/jvm/functions/Function7;)V

    move-object p8, v0

    check-cast p8, Lio/reactivex/functions/Function7;

    .line 448
    invoke-static/range {p1 .. p8}, Lio/reactivex/Observable;->zip(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function7;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "Observable.zip(\n      so\u2026, t5, t6, t7)\n      }\n  )"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final zip(Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lkotlin/jvm/functions/Function6;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            "T3:",
            "Ljava/lang/Object;",
            "T4:",
            "Ljava/lang/Object;",
            "T5:",
            "Ljava/lang/Object;",
            "T6:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Observable<",
            "TT1;>;",
            "Lio/reactivex/Observable<",
            "TT2;>;",
            "Lio/reactivex/Observable<",
            "TT3;>;",
            "Lio/reactivex/Observable<",
            "TT4;>;",
            "Lio/reactivex/Observable<",
            "TT5;>;",
            "Lio/reactivex/Observable<",
            "TT6;>;",
            "Lkotlin/jvm/functions/Function6<",
            "-TT1;-TT2;-TT3;-TT4;-TT5;-TT6;+TR;>;)",
            "Lio/reactivex/Observable<",
            "TR;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "source1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source2"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source3"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source4"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source5"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source6"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "combineFunction"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 412
    check-cast p1, Lio/reactivex/ObservableSource;

    check-cast p2, Lio/reactivex/ObservableSource;

    check-cast p3, Lio/reactivex/ObservableSource;

    check-cast p4, Lio/reactivex/ObservableSource;

    check-cast p5, Lio/reactivex/ObservableSource;

    check-cast p6, Lio/reactivex/ObservableSource;

    .line 413
    new-instance v0, Lcom/squareup/util/rx2/Observables$zip$9;

    invoke-direct {v0, p7}, Lcom/squareup/util/rx2/Observables$zip$9;-><init>(Lkotlin/jvm/functions/Function6;)V

    move-object p7, v0

    check-cast p7, Lio/reactivex/functions/Function6;

    .line 411
    invoke-static/range {p1 .. p7}, Lio/reactivex/Observable;->zip(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function6;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "Observable.zip(\n      so\u2026 t6\n        )\n      }\n  )"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final zip(Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lkotlin/jvm/functions/Function5;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            "T3:",
            "Ljava/lang/Object;",
            "T4:",
            "Ljava/lang/Object;",
            "T5:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Observable<",
            "TT1;>;",
            "Lio/reactivex/Observable<",
            "TT2;>;",
            "Lio/reactivex/Observable<",
            "TT3;>;",
            "Lio/reactivex/Observable<",
            "TT4;>;",
            "Lio/reactivex/Observable<",
            "TT5;>;",
            "Lkotlin/jvm/functions/Function5<",
            "-TT1;-TT2;-TT3;-TT4;-TT5;+TR;>;)",
            "Lio/reactivex/Observable<",
            "TR;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "source1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source2"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source3"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source4"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source5"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "combineFunction"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 381
    check-cast p1, Lio/reactivex/ObservableSource;

    check-cast p2, Lio/reactivex/ObservableSource;

    check-cast p3, Lio/reactivex/ObservableSource;

    check-cast p4, Lio/reactivex/ObservableSource;

    check-cast p5, Lio/reactivex/ObservableSource;

    .line 382
    new-instance v0, Lcom/squareup/util/rx2/Observables$zip$7;

    invoke-direct {v0, p6}, Lcom/squareup/util/rx2/Observables$zip$7;-><init>(Lkotlin/jvm/functions/Function5;)V

    move-object p6, v0

    check-cast p6, Lio/reactivex/functions/Function5;

    .line 380
    invoke-static/range {p1 .. p6}, Lio/reactivex/Observable;->zip(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function5;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "Observable.zip(\n      so\u2026t1, t2, t3, t4, t5) }\n  )"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final zip(Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lkotlin/jvm/functions/Function4;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            "T3:",
            "Ljava/lang/Object;",
            "T4:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Observable<",
            "TT1;>;",
            "Lio/reactivex/Observable<",
            "TT2;>;",
            "Lio/reactivex/Observable<",
            "TT3;>;",
            "Lio/reactivex/Observable<",
            "TT4;>;",
            "Lkotlin/jvm/functions/Function4<",
            "-TT1;-TT2;-TT3;-TT4;+TR;>;)",
            "Lio/reactivex/Observable<",
            "TR;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "source1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source2"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source3"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source4"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "combineFunction"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 352
    check-cast p1, Lio/reactivex/ObservableSource;

    check-cast p2, Lio/reactivex/ObservableSource;

    check-cast p3, Lio/reactivex/ObservableSource;

    check-cast p4, Lio/reactivex/ObservableSource;

    .line 353
    new-instance v0, Lcom/squareup/util/rx2/Observables$zip$5;

    invoke-direct {v0, p5}, Lcom/squareup/util/rx2/Observables$zip$5;-><init>(Lkotlin/jvm/functions/Function4;)V

    check-cast v0, Lio/reactivex/functions/Function4;

    .line 351
    invoke-static {p1, p2, p3, p4, v0}, Lio/reactivex/Observable;->zip(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function4;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "Observable.zip(\n      so\u2026ion(t1, t2, t3, t4) }\n  )"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final zip(Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;Lkotlin/jvm/functions/Function3;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            "T3:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Observable<",
            "TT1;>;",
            "Lio/reactivex/Observable<",
            "TT2;>;",
            "Lio/reactivex/Observable<",
            "TT3;>;",
            "Lkotlin/jvm/functions/Function3<",
            "-TT1;-TT2;-TT3;+TR;>;)",
            "Lio/reactivex/Observable<",
            "TR;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "source1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source2"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source3"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "combineFunction"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 328
    check-cast p1, Lio/reactivex/ObservableSource;

    check-cast p2, Lio/reactivex/ObservableSource;

    check-cast p3, Lio/reactivex/ObservableSource;

    .line 329
    new-instance v0, Lcom/squareup/util/rx2/Observables$zip$3;

    invoke-direct {v0, p4}, Lcom/squareup/util/rx2/Observables$zip$3;-><init>(Lkotlin/jvm/functions/Function3;)V

    check-cast v0, Lio/reactivex/functions/Function3;

    .line 327
    invoke-static {p1, p2, p3, v0}, Lio/reactivex/Observable;->zip(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function3;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "Observable.zip(\n      so\u2026unction(t1, t2, t3) }\n  )"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final zip(Lio/reactivex/Observable;Lio/reactivex/Observable;Lkotlin/jvm/functions/Function2;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Observable<",
            "TT1;>;",
            "Lio/reactivex/Observable<",
            "TT2;>;",
            "Lkotlin/jvm/functions/Function2<",
            "-TT1;-TT2;+TR;>;)",
            "Lio/reactivex/Observable<",
            "TR;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/CheckReturnValue;
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "source1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source2"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "combineFunction"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 306
    check-cast p1, Lio/reactivex/ObservableSource;

    check-cast p2, Lio/reactivex/ObservableSource;

    .line 307
    new-instance v0, Lcom/squareup/util/rx2/Observables$zip$1;

    invoke-direct {v0, p3}, Lcom/squareup/util/rx2/Observables$zip$1;-><init>(Lkotlin/jvm/functions/Function2;)V

    check-cast v0, Lio/reactivex/functions/BiFunction;

    .line 306
    invoke-static {p1, p2, v0}, Lio/reactivex/Observable;->zip(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "Observable.zip(source1, \u2026ineFunction(t1, t2) }\n  )"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
