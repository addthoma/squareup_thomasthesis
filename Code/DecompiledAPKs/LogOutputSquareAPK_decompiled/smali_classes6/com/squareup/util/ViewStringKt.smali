.class public final Lcom/squareup/util/ViewStringKt;
.super Ljava/lang/Object;
.source "ViewString.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\u0010\u0008\n\u0002\u0008\u0002\"\u0019\u0010\u0000\u001a\u00020\u0001*\u00020\u00028\u00c2\u0002X\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0000\u0010\u0003\u00a8\u0006\u0004"
    }
    d2 = {
        "isResourceValid",
        "",
        "",
        "(I)Z",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$isResourceValid$p(I)Z
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/util/ViewStringKt;->isResourceValid(I)Z

    move-result p0

    return p0
.end method

.method private static final isResourceValid(I)Z
    .locals 0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method
