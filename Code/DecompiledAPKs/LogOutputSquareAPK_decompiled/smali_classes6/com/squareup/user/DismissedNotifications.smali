.class public Lcom/squareup/user/DismissedNotifications;
.super Ljava/lang/Object;
.source "DismissedNotifications.java"


# static fields
.field private static final MAX_SIZE:I = 0x14


# instance fields
.field private final dismissedNotifications:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Lcom/squareup/user/DismissedNotifications$1;

    invoke-direct {v0, p0}, Lcom/squareup/user/DismissedNotifications$1;-><init>(Lcom/squareup/user/DismissedNotifications;)V

    iput-object v0, p0, Lcom/squareup/user/DismissedNotifications;->dismissedNotifications:Ljava/util/LinkedHashMap;

    return-void
.end method


# virtual methods
.method public dismiss(Lcom/squareup/server/account/protos/Notification;)V
    .locals 1

    .line 29
    iget-object p1, p1, Lcom/squareup/server/account/protos/Notification;->delivery_id:Ljava/lang/String;

    .line 31
    iget-object v0, p0, Lcom/squareup/user/DismissedNotifications;->dismissedNotifications:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 32
    iget-object v0, p0, Lcom/squareup/user/DismissedNotifications;->dismissedNotifications:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public getDismissedNotifications()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 41
    iget-object v0, p0, Lcom/squareup/user/DismissedNotifications;->dismissedNotifications:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public isDismissed(Lcom/squareup/server/account/protos/Notification;)Z
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/squareup/user/DismissedNotifications;->dismissedNotifications:Ljava/util/LinkedHashMap;

    iget-object p1, p1, Lcom/squareup/server/account/protos/Notification;->delivery_id:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method
