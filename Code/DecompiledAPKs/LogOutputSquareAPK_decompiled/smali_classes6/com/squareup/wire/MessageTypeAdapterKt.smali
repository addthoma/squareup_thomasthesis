.class public final Lcom/squareup/wire/MessageTypeAdapterKt;
.super Ljava/lang/Object;
.source "MessageTypeAdapter.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0002"
    }
    d2 = {
        "POWER_64",
        "Ljava/math/BigInteger;",
        "wire-gson-support"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# static fields
.field private static final POWER_64:Ljava/math/BigInteger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 32
    new-instance v0, Ljava/math/BigInteger;

    const-string v1, "18446744073709551616"

    invoke-direct {v0, v1}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/wire/MessageTypeAdapterKt;->POWER_64:Ljava/math/BigInteger;

    return-void
.end method

.method public static final synthetic access$getPOWER_64$p()Ljava/math/BigInteger;
    .locals 1

    .line 1
    sget-object v0, Lcom/squareup/wire/MessageTypeAdapterKt;->POWER_64:Ljava/math/BigInteger;

    return-object v0
.end method
