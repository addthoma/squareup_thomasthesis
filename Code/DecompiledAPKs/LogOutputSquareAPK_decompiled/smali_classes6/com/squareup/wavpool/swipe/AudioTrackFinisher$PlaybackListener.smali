.class Lcom/squareup/wavpool/swipe/AudioTrackFinisher$PlaybackListener;
.super Ljava/lang/Object;
.source "AudioTrackFinisher.java"

# interfaces
.implements Landroid/media/AudioTrack$OnPlaybackPositionUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/wavpool/swipe/AudioTrackFinisher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PlaybackListener"
.end annotation


# instance fields
.field private final listener:Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$PlaybackCompleteListener;

.field private final playbackTimer:Lcom/squareup/wavpool/swipe/AudioTrackFinisher$PlaybackTimer;

.field final synthetic this$0:Lcom/squareup/wavpool/swipe/AudioTrackFinisher;


# direct methods
.method constructor <init>(Lcom/squareup/wavpool/swipe/AudioTrackFinisher;Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$PlaybackCompleteListener;Lcom/squareup/wavpool/swipe/AudioTrackFinisher$PlaybackTimer;)V
    .locals 0

    .line 55
    iput-object p1, p0, Lcom/squareup/wavpool/swipe/AudioTrackFinisher$PlaybackListener;->this$0:Lcom/squareup/wavpool/swipe/AudioTrackFinisher;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p2, p0, Lcom/squareup/wavpool/swipe/AudioTrackFinisher$PlaybackListener;->listener:Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$PlaybackCompleteListener;

    .line 57
    iput-object p3, p0, Lcom/squareup/wavpool/swipe/AudioTrackFinisher$PlaybackListener;->playbackTimer:Lcom/squareup/wavpool/swipe/AudioTrackFinisher$PlaybackTimer;

    return-void
.end method


# virtual methods
.method public onMarkerReached(Landroid/media/AudioTrack;)V
    .locals 2

    .line 61
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioTrackFinisher$PlaybackListener;->this$0:Lcom/squareup/wavpool/swipe/AudioTrackFinisher;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/wavpool/swipe/AudioTrackFinisher;->access$002(Lcom/squareup/wavpool/swipe/AudioTrackFinisher;I)I

    .line 62
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioTrackFinisher$PlaybackListener;->this$0:Lcom/squareup/wavpool/swipe/AudioTrackFinisher;

    invoke-static {v0}, Lcom/squareup/wavpool/swipe/AudioTrackFinisher;->access$100(Lcom/squareup/wavpool/swipe/AudioTrackFinisher;)Lcom/squareup/thread/executor/MainThread;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/wavpool/swipe/AudioTrackFinisher$PlaybackListener;->playbackTimer:Lcom/squareup/wavpool/swipe/AudioTrackFinisher$PlaybackTimer;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->cancel(Ljava/lang/Runnable;)V

    .line 63
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioTrackFinisher$PlaybackListener;->listener:Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$PlaybackCompleteListener;

    invoke-virtual {v0, p1}, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$PlaybackCompleteListener;->onMarkerReached(Landroid/media/AudioTrack;)V

    return-void
.end method

.method public onPeriodicNotification(Landroid/media/AudioTrack;)V
    .locals 0

    return-void
.end method
