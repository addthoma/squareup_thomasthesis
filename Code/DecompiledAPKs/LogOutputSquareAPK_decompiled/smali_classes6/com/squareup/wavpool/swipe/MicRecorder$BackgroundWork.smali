.class Lcom/squareup/wavpool/swipe/MicRecorder$BackgroundWork;
.super Ljava/lang/Object;
.source "MicRecorder.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/wavpool/swipe/MicRecorder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BackgroundWork"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/wavpool/swipe/MicRecorder;


# direct methods
.method private constructor <init>(Lcom/squareup/wavpool/swipe/MicRecorder;)V
    .locals 0

    .line 148
    iput-object p1, p0, Lcom/squareup/wavpool/swipe/MicRecorder$BackgroundWork;->this$0:Lcom/squareup/wavpool/swipe/MicRecorder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/wavpool/swipe/MicRecorder;Lcom/squareup/wavpool/swipe/MicRecorder$1;)V
    .locals 0

    .line 148
    invoke-direct {p0, p1}, Lcom/squareup/wavpool/swipe/MicRecorder$BackgroundWork;-><init>(Lcom/squareup/wavpool/swipe/MicRecorder;)V

    return-void
.end method

.method private audioSource()I
    .locals 1

    .line 292
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/MicRecorder$BackgroundWork;->this$0:Lcom/squareup/wavpool/swipe/MicRecorder;

    invoke-static {v0}, Lcom/squareup/wavpool/swipe/MicRecorder;->access$700(Lcom/squareup/wavpool/swipe/MicRecorder;)Lcom/squareup/wavpool/swipe/AndroidDeviceParams;

    move-result-object v0

    iget-boolean v0, v0, Lcom/squareup/wavpool/swipe/AndroidDeviceParams;->useVoiceRecognition:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0
.end method

.method private backgroundRecordingEvent(Ljava/lang/String;)V
    .locals 7

    .line 302
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 303
    iget-object v2, p0, Lcom/squareup/wavpool/swipe/MicRecorder$BackgroundWork;->this$0:Lcom/squareup/wavpool/swipe/MicRecorder;

    invoke-static {v2}, Lcom/squareup/wavpool/swipe/MicRecorder;->access$800(Lcom/squareup/wavpool/swipe/MicRecorder;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v6, v2, v4

    if-ltz v6, :cond_0

    iget-object v2, p0, Lcom/squareup/wavpool/swipe/MicRecorder$BackgroundWork;->this$0:Lcom/squareup/wavpool/swipe/MicRecorder;

    .line 304
    invoke-static {v2}, Lcom/squareup/wavpool/swipe/MicRecorder;->access$800(Lcom/squareup/wavpool/swipe/MicRecorder;)J

    move-result-wide v2

    const-wide/32 v4, 0xdbba0

    add-long/2addr v2, v4

    cmp-long v4, v2, v0

    if-gez v4, :cond_1

    .line 305
    :cond_0
    iget-object v2, p0, Lcom/squareup/wavpool/swipe/MicRecorder$BackgroundWork;->this$0:Lcom/squareup/wavpool/swipe/MicRecorder;

    invoke-static {v2, v0, v1}, Lcom/squareup/wavpool/swipe/MicRecorder;->access$802(Lcom/squareup/wavpool/swipe/MicRecorder;J)J

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "Another application (%s) is recording in the background."

    .line 306
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 307
    invoke-direct {p0, p1}, Lcom/squareup/wavpool/swipe/MicRecorder$BackgroundWork;->reportRecordingEvent(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private record(Z)Z
    .locals 17

    move-object/from16 v1, p0

    const-string v2, "Ignoring exception from AudioRecord.release()"

    .line 170
    iget-object v0, v1, Lcom/squareup/wavpool/swipe/MicRecorder$BackgroundWork;->this$0:Lcom/squareup/wavpool/swipe/MicRecorder;

    invoke-static {v0}, Lcom/squareup/wavpool/swipe/MicRecorder;->access$200(Lcom/squareup/wavpool/swipe/MicRecorder;)Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v0

    const/4 v3, 0x0

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, v1, Lcom/squareup/wavpool/swipe/MicRecorder$BackgroundWork;->this$0:Lcom/squareup/wavpool/swipe/MicRecorder;

    sget-object v2, Lcom/squareup/wavpool/swipe/Recorder$State;->BLOCKED:Lcom/squareup/wavpool/swipe/Recorder$State;

    invoke-static {v0, v2}, Lcom/squareup/wavpool/swipe/MicRecorder;->access$300(Lcom/squareup/wavpool/swipe/MicRecorder;Lcom/squareup/wavpool/swipe/Recorder$State;)V

    return v3

    :cond_0
    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 179
    :try_start_0
    iget-object v0, v1, Lcom/squareup/wavpool/swipe/MicRecorder$BackgroundWork;->this$0:Lcom/squareup/wavpool/swipe/MicRecorder;

    invoke-static {v0}, Lcom/squareup/wavpool/swipe/MicRecorder;->access$400(Lcom/squareup/wavpool/swipe/MicRecorder;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v12

    const/4 v0, -0x1

    if-ne v0, v12, :cond_1

    .line 181
    iget-object v0, v1, Lcom/squareup/wavpool/swipe/MicRecorder$BackgroundWork;->this$0:Lcom/squareup/wavpool/swipe/MicRecorder;

    sget-object v5, Lcom/squareup/wavpool/swipe/Recorder$State;->SAMPLE_RATE_UNSET:Lcom/squareup/wavpool/swipe/Recorder$State;

    invoke-static {v0, v5}, Lcom/squareup/wavpool/swipe/MicRecorder;->access$300(Lcom/squareup/wavpool/swipe/MicRecorder;Lcom/squareup/wavpool/swipe/Recorder$State;)V

    return v3

    :cond_1
    const-string v0, "Recording at %dHz"

    new-array v6, v5, [Ljava/lang/Object;

    .line 184
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v3

    invoke-static {v0, v6}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_5

    move-object v13, v4

    const/4 v14, 0x0

    .line 190
    :goto_0
    :try_start_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v15
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_4

    .line 193
    :try_start_2
    new-instance v0, Landroid/media/AudioRecord;

    .line 194
    invoke-direct/range {p0 .. p0}, Lcom/squareup/wavpool/swipe/MicRecorder$BackgroundWork;->audioSource()I

    move-result v7

    const/16 v9, 0x10

    const/4 v10, 0x2

    const v11, 0xea60

    move-object v6, v0

    move v8, v12

    invoke-direct/range {v6 .. v11}, Landroid/media/AudioRecord;-><init>(IIIII)V
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_4

    move-object v13, v0

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_3
    const-string v6, "sampleRate: %d"

    new-array v7, v5, [Ljava/lang/Object;

    .line 197
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v3

    invoke-static {v0, v6, v7}, Ltimber/log/Timber;->i(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_1
    if-eqz v13, :cond_d

    .line 201
    invoke-virtual {v13}, Landroid/media/AudioRecord;->getState()I

    move-result v0

    const/4 v6, 0x4

    if-ne v0, v5, :cond_9

    .line 222
    iget-object v0, v1, Lcom/squareup/wavpool/swipe/MicRecorder$BackgroundWork;->this$0:Lcom/squareup/wavpool/swipe/MicRecorder;

    invoke-static {v0}, Lcom/squareup/wavpool/swipe/MicRecorder;->access$600(Lcom/squareup/wavpool/swipe/MicRecorder;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/squarewave/AudioFilter;

    invoke-interface {v0, v12}, Lcom/squareup/squarewave/AudioFilter;->start(I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_4

    .line 224
    :try_start_4
    invoke-virtual {v13}, Landroid/media/AudioRecord;->startRecording()V

    const-string v0, "Recording started."

    new-array v7, v3, [Ljava/lang/Object;

    .line 225
    invoke-static {v0, v7}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 231
    :cond_2
    :goto_2
    iget-object v0, v1, Lcom/squareup/wavpool/swipe/MicRecorder$BackgroundWork;->this$0:Lcom/squareup/wavpool/swipe/MicRecorder;

    invoke-static {v0}, Lcom/squareup/wavpool/swipe/MicRecorder;->access$500(Lcom/squareup/wavpool/swipe/MicRecorder;)Z

    move-result v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    if-eqz v0, :cond_7

    const/16 v0, 0xc8

    .line 234
    :try_start_5
    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v9
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 235
    :try_start_6
    sget-object v0, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v9, v0}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 236
    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    invoke-virtual {v13, v9, v0}, Landroid/media/AudioRecord;->read(Ljava/nio/ByteBuffer;I)I

    move-result v0
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    goto :goto_4

    :catch_1
    move-exception v0

    goto :goto_3

    :catch_2
    move-exception v0

    move-object v9, v4

    :goto_3
    :try_start_7
    const-string v10, "Unable to allocateDirect while recording"

    .line 238
    invoke-static {v0, v10}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_4
    if-lez v0, :cond_4

    .line 244
    iget-object v7, v1, Lcom/squareup/wavpool/swipe/MicRecorder$BackgroundWork;->this$0:Lcom/squareup/wavpool/swipe/MicRecorder;

    invoke-static {v7}, Lcom/squareup/wavpool/swipe/MicRecorder;->access$600(Lcom/squareup/wavpool/swipe/MicRecorder;)Ljavax/inject/Provider;

    move-result-object v7

    invoke-interface {v7}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/squareup/squarewave/AudioFilter;

    invoke-interface {v7, v9, v0}, Lcom/squareup/squarewave/AudioFilter;->process(Ljava/nio/ByteBuffer;I)V

    if-nez v8, :cond_3

    .line 249
    iget-object v0, v1, Lcom/squareup/wavpool/swipe/MicRecorder$BackgroundWork;->this$0:Lcom/squareup/wavpool/swipe/MicRecorder;

    iget-object v7, v1, Lcom/squareup/wavpool/swipe/MicRecorder$BackgroundWork;->this$0:Lcom/squareup/wavpool/swipe/MicRecorder;

    invoke-virtual {v7}, Lcom/squareup/wavpool/swipe/MicRecorder;->getRecordingState()Lcom/squareup/wavpool/swipe/Recorder$State;

    move-result-object v7

    invoke-static {v0, v7}, Lcom/squareup/wavpool/swipe/MicRecorder;->access$300(Lcom/squareup/wavpool/swipe/MicRecorder;Lcom/squareup/wavpool/swipe/Recorder$State;)V

    const/4 v7, 0x0

    const/4 v8, 0x1

    goto :goto_2

    :cond_3
    const/4 v7, 0x0

    goto :goto_2

    :cond_4
    add-int/2addr v7, v5

    const-wide/16 v9, 0xfa

    .line 253
    invoke-static {v9, v10}, Ljava/lang/Thread;->sleep(J)V

    if-le v7, v6, :cond_2

    if-eqz p1, :cond_5

    const-string v0, "evt_background_recording"

    .line 260
    invoke-direct {v1, v0}, Lcom/squareup/wavpool/swipe/MicRecorder$BackgroundWork;->backgroundRecordingEvent(Ljava/lang/String;)V

    .line 261
    iget-object v0, v1, Lcom/squareup/wavpool/swipe/MicRecorder$BackgroundWork;->this$0:Lcom/squareup/wavpool/swipe/MicRecorder;

    sget-object v3, Lcom/squareup/wavpool/swipe/Recorder$State;->BLOCKED:Lcom/squareup/wavpool/swipe/Recorder$State;

    invoke-static {v0, v3}, Lcom/squareup/wavpool/swipe/MicRecorder;->access$300(Lcom/squareup/wavpool/swipe/MicRecorder;Lcom/squareup/wavpool/swipe/Recorder$State;)V

    .line 263
    :cond_5
    invoke-virtual {v13}, Landroid/media/AudioRecord;->release()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    if-eqz v13, :cond_6

    .line 277
    :try_start_8
    invoke-virtual {v13}, Landroid/media/AudioRecord;->release()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_5

    :catchall_0
    move-exception v0

    move-object v3, v0

    .line 279
    invoke-static {v3, v2}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 284
    :cond_6
    :goto_5
    iget-object v0, v1, Lcom/squareup/wavpool/swipe/MicRecorder$BackgroundWork;->this$0:Lcom/squareup/wavpool/swipe/MicRecorder;

    invoke-static {v0}, Lcom/squareup/wavpool/swipe/MicRecorder;->access$600(Lcom/squareup/wavpool/swipe/MicRecorder;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/squarewave/AudioFilter;

    invoke-interface {v0}, Lcom/squareup/squarewave/AudioFilter;->finish()V

    return v5

    :cond_7
    if-eqz v13, :cond_8

    .line 277
    :try_start_9
    invoke-virtual {v13}, Landroid/media/AudioRecord;->release()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    goto :goto_6

    :catchall_1
    move-exception v0

    move-object v4, v0

    .line 279
    invoke-static {v4, v2}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 284
    :cond_8
    :goto_6
    iget-object v0, v1, Lcom/squareup/wavpool/swipe/MicRecorder$BackgroundWork;->this$0:Lcom/squareup/wavpool/swipe/MicRecorder;

    invoke-static {v0}, Lcom/squareup/wavpool/swipe/MicRecorder;->access$600(Lcom/squareup/wavpool/swipe/MicRecorder;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/squarewave/AudioFilter;

    invoke-interface {v0}, Lcom/squareup/squarewave/AudioFilter;->finish()V

    return v3

    :catchall_2
    move-exception v0

    const/4 v3, 0x1

    goto :goto_8

    .line 205
    :cond_9
    :try_start_a
    iget-object v0, v1, Lcom/squareup/wavpool/swipe/MicRecorder$BackgroundWork;->this$0:Lcom/squareup/wavpool/swipe/MicRecorder;

    invoke-static {v0}, Lcom/squareup/wavpool/swipe/MicRecorder;->access$500(Lcom/squareup/wavpool/swipe/MicRecorder;)Z

    move-result v0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    if-nez v0, :cond_b

    if-eqz v13, :cond_a

    .line 277
    :try_start_b
    invoke-virtual {v13}, Landroid/media/AudioRecord;->release()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    goto :goto_7

    :catchall_3
    move-exception v0

    move-object v4, v0

    .line 279
    invoke-static {v4, v2}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    :cond_a
    :goto_7
    return v3

    .line 206
    :cond_b
    :try_start_c
    invoke-virtual {v13}, Landroid/media/AudioRecord;->release()V

    if-gt v14, v6, :cond_c

    .line 209
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, v15

    const-wide/16 v8, 0x3e8

    cmp-long v0, v6, v8

    if-lez v0, :cond_d

    :cond_c
    const-string v0, "evt_background_recording_init"

    .line 210
    invoke-direct {v1, v0}, Lcom/squareup/wavpool/swipe/MicRecorder$BackgroundWork;->backgroundRecordingEvent(Ljava/lang/String;)V

    .line 211
    iget-object v0, v1, Lcom/squareup/wavpool/swipe/MicRecorder$BackgroundWork;->this$0:Lcom/squareup/wavpool/swipe/MicRecorder;

    sget-object v6, Lcom/squareup/wavpool/swipe/Recorder$State;->BLOCKED:Lcom/squareup/wavpool/swipe/Recorder$State;

    invoke-static {v0, v6}, Lcom/squareup/wavpool/swipe/MicRecorder;->access$300(Lcom/squareup/wavpool/swipe/MicRecorder;Lcom/squareup/wavpool/swipe/Recorder$State;)V

    :cond_d
    add-int/lit8 v14, v14, 0x1

    const-wide/16 v6, 0x64

    .line 217
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_4

    goto/16 :goto_0

    :catchall_4
    move-exception v0

    goto :goto_8

    :catchall_5
    move-exception v0

    move-object v13, v4

    .line 269
    :goto_8
    :try_start_d
    instance-of v4, v0, Ljava/lang/RuntimeException;

    if-eqz v4, :cond_e

    .line 270
    check-cast v0, Ljava/lang/RuntimeException;

    throw v0

    .line 272
    :cond_e
    new-instance v4, Ljava/lang/RuntimeException;

    invoke-direct {v4, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v4
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_6

    :catchall_6
    move-exception v0

    move-object v4, v0

    if-eqz v13, :cond_f

    .line 277
    :try_start_e
    invoke-virtual {v13}, Landroid/media/AudioRecord;->release()V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_7

    goto :goto_9

    :catchall_7
    move-exception v0

    move-object v5, v0

    .line 279
    invoke-static {v5, v2}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    :cond_f
    :goto_9
    if-eqz v3, :cond_10

    .line 284
    iget-object v0, v1, Lcom/squareup/wavpool/swipe/MicRecorder$BackgroundWork;->this$0:Lcom/squareup/wavpool/swipe/MicRecorder;

    invoke-static {v0}, Lcom/squareup/wavpool/swipe/MicRecorder;->access$600(Lcom/squareup/wavpool/swipe/MicRecorder;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/squarewave/AudioFilter;

    invoke-interface {v0}, Lcom/squareup/squarewave/AudioFilter;->finish()V

    .line 286
    :cond_10
    throw v4
.end method

.method private reportRecordingEvent(Ljava/lang/String;)V
    .locals 2

    .line 312
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/MicRecorder$BackgroundWork;->this$0:Lcom/squareup/wavpool/swipe/MicRecorder;

    invoke-static {v0}, Lcom/squareup/wavpool/swipe/MicRecorder;->access$900(Lcom/squareup/wavpool/swipe/MicRecorder;)Lcom/squareup/thread/executor/MainThread;

    move-result-object v0

    new-instance v1, Lcom/squareup/wavpool/swipe/-$$Lambda$MicRecorder$BackgroundWork$_BomQuASlFaOJeZD_-I0lP8Zlh0;

    invoke-direct {v1, p0, p1}, Lcom/squareup/wavpool/swipe/-$$Lambda$MicRecorder$BackgroundWork$_BomQuASlFaOJeZD_-I0lP8Zlh0;-><init>(Lcom/squareup/wavpool/swipe/MicRecorder$BackgroundWork;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method public synthetic lambda$reportRecordingEvent$0$MicRecorder$BackgroundWork(Ljava/lang/String;)V
    .locals 2

    .line 312
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/MicRecorder$BackgroundWork;->this$0:Lcom/squareup/wavpool/swipe/MicRecorder;

    invoke-static {v0}, Lcom/squareup/wavpool/swipe/MicRecorder;->access$1000(Lcom/squareup/wavpool/swipe/MicRecorder;)Lcom/squareup/badbus/BadEventSink;

    move-result-object v0

    new-instance v1, Lcom/squareup/wavpool/swipe/SwipeEvents$RecordingEvent;

    invoke-direct {v1, p1}, Lcom/squareup/wavpool/swipe/SwipeEvents$RecordingEvent;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/badbus/BadEventSink;->post(Ljava/lang/Object;)V

    return-void
.end method

.method public run()V
    .locals 3

    const-string v0, "Record thread terminated."

    .line 150
    iget-object v1, p0, Lcom/squareup/wavpool/swipe/MicRecorder$BackgroundWork;->this$0:Lcom/squareup/wavpool/swipe/MicRecorder;

    invoke-static {v1}, Lcom/squareup/wavpool/swipe/MicRecorder;->access$100(Lcom/squareup/wavpool/swipe/MicRecorder;)Lcom/squareup/crashnado/Crashnado;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/crashnado/Crashnado;->prepareStack()V

    const-string v1, "started_recording_mic"

    .line 151
    invoke-direct {p0, v1}, Lcom/squareup/wavpool/swipe/MicRecorder$BackgroundWork;->reportRecordingEvent(Ljava/lang/String;)V

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 155
    :cond_0
    :try_start_0
    invoke-direct {p0, v2}, Lcom/squareup/wavpool/swipe/MicRecorder$BackgroundWork;->record(Z)Z

    move-result v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_0

    new-array v1, v1, [Ljava/lang/Object;

    .line 158
    invoke-static {v0, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    const-string v0, "done_recording_mic"

    .line 160
    invoke-direct {p0, v0}, Lcom/squareup/wavpool/swipe/MicRecorder$BackgroundWork;->reportRecordingEvent(Ljava/lang/String;)V

    return-void

    :catchall_0
    move-exception v2

    new-array v1, v1, [Ljava/lang/Object;

    .line 158
    invoke-static {v0, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 159
    throw v2
.end method
