.class public abstract Lcom/squareup/wavpool/swipe/AndroidAudioModule;
.super Ljava/lang/Object;
.source "AndroidAudioModule.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/wavpool/swipe/AndroidDeviceParamsModule;,
        Lcom/squareup/wavpool/swipe/AsyncDecoderModule;,
        Lcom/squareup/wavpool/swipe/AudioModule;,
        Lcom/squareup/wavpool/swipe/AudioModule$Real;,
        Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule;,
        Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule$Real;,
        Lcom/squareup/wavpool/swipe/AndroidAudioRecordingModule;,
        Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideCardReaderAddress()Ljava/lang/String;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method static provideCardReaderName()Ljava/lang/String;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method static provideConnectionType()Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 28
    sget-object v0, Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;->AUDIO:Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

    return-object v0
.end method
