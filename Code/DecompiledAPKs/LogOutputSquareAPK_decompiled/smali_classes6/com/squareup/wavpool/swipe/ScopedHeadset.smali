.class public Lcom/squareup/wavpool/swipe/ScopedHeadset;
.super Ljava/lang/Object;
.source "ScopedHeadset.java"

# interfaces
.implements Lmortar/Scoped;


# instance fields
.field private final headset:Lcom/squareup/wavpool/swipe/Headset;


# direct methods
.method public constructor <init>(Lcom/squareup/wavpool/swipe/Headset;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-object p1, p0, Lcom/squareup/wavpool/swipe/ScopedHeadset;->headset:Lcom/squareup/wavpool/swipe/Headset;

    return-void
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    .line 15
    iget-object p1, p0, Lcom/squareup/wavpool/swipe/ScopedHeadset;->headset:Lcom/squareup/wavpool/swipe/Headset;

    invoke-virtual {p1}, Lcom/squareup/wavpool/swipe/Headset;->initialize()V

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/ScopedHeadset;->headset:Lcom/squareup/wavpool/swipe/Headset;

    invoke-virtual {v0}, Lcom/squareup/wavpool/swipe/Headset;->destroy()V

    return-void
.end method
