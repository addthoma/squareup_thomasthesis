.class public final enum Lcom/squareup/wavpool/swipe/Recorder$State;
.super Ljava/lang/Enum;
.source "Recorder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/wavpool/swipe/Recorder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/wavpool/swipe/Recorder$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/wavpool/swipe/Recorder$State;

.field public static final enum BLOCKED:Lcom/squareup/wavpool/swipe/Recorder$State;

.field public static final enum BLOCKED_NO_READER:Lcom/squareup/wavpool/swipe/Recorder$State;

.field public static final enum MISROUTED_AUDIO:Lcom/squareup/wavpool/swipe/Recorder$State;

.field public static final enum READY:Lcom/squareup/wavpool/swipe/Recorder$State;

.field public static final enum SAMPLE_RATE_UNSET:Lcom/squareup/wavpool/swipe/Recorder$State;

.field public static final enum STOPPED:Lcom/squareup/wavpool/swipe/Recorder$State;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 18
    new-instance v0, Lcom/squareup/wavpool/swipe/Recorder$State;

    const/4 v1, 0x0

    const-string v2, "STOPPED"

    invoke-direct {v0, v2, v1}, Lcom/squareup/wavpool/swipe/Recorder$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/wavpool/swipe/Recorder$State;->STOPPED:Lcom/squareup/wavpool/swipe/Recorder$State;

    .line 21
    new-instance v0, Lcom/squareup/wavpool/swipe/Recorder$State;

    const/4 v2, 0x1

    const-string v3, "READY"

    invoke-direct {v0, v3, v2}, Lcom/squareup/wavpool/swipe/Recorder$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/wavpool/swipe/Recorder$State;->READY:Lcom/squareup/wavpool/swipe/Recorder$State;

    .line 24
    new-instance v0, Lcom/squareup/wavpool/swipe/Recorder$State;

    const/4 v3, 0x2

    const-string v4, "SAMPLE_RATE_UNSET"

    invoke-direct {v0, v4, v3}, Lcom/squareup/wavpool/swipe/Recorder$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/wavpool/swipe/Recorder$State;->SAMPLE_RATE_UNSET:Lcom/squareup/wavpool/swipe/Recorder$State;

    .line 27
    new-instance v0, Lcom/squareup/wavpool/swipe/Recorder$State;

    const/4 v4, 0x3

    const-string v5, "BLOCKED"

    invoke-direct {v0, v5, v4}, Lcom/squareup/wavpool/swipe/Recorder$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/wavpool/swipe/Recorder$State;->BLOCKED:Lcom/squareup/wavpool/swipe/Recorder$State;

    .line 30
    new-instance v0, Lcom/squareup/wavpool/swipe/Recorder$State;

    const/4 v5, 0x4

    const-string v6, "BLOCKED_NO_READER"

    invoke-direct {v0, v6, v5}, Lcom/squareup/wavpool/swipe/Recorder$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/wavpool/swipe/Recorder$State;->BLOCKED_NO_READER:Lcom/squareup/wavpool/swipe/Recorder$State;

    .line 36
    new-instance v0, Lcom/squareup/wavpool/swipe/Recorder$State;

    const/4 v6, 0x5

    const-string v7, "MISROUTED_AUDIO"

    invoke-direct {v0, v7, v6}, Lcom/squareup/wavpool/swipe/Recorder$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/wavpool/swipe/Recorder$State;->MISROUTED_AUDIO:Lcom/squareup/wavpool/swipe/Recorder$State;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/wavpool/swipe/Recorder$State;

    .line 16
    sget-object v7, Lcom/squareup/wavpool/swipe/Recorder$State;->STOPPED:Lcom/squareup/wavpool/swipe/Recorder$State;

    aput-object v7, v0, v1

    sget-object v1, Lcom/squareup/wavpool/swipe/Recorder$State;->READY:Lcom/squareup/wavpool/swipe/Recorder$State;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/wavpool/swipe/Recorder$State;->SAMPLE_RATE_UNSET:Lcom/squareup/wavpool/swipe/Recorder$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/wavpool/swipe/Recorder$State;->BLOCKED:Lcom/squareup/wavpool/swipe/Recorder$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/wavpool/swipe/Recorder$State;->BLOCKED_NO_READER:Lcom/squareup/wavpool/swipe/Recorder$State;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/wavpool/swipe/Recorder$State;->MISROUTED_AUDIO:Lcom/squareup/wavpool/swipe/Recorder$State;

    aput-object v1, v0, v6

    sput-object v0, Lcom/squareup/wavpool/swipe/Recorder$State;->$VALUES:[Lcom/squareup/wavpool/swipe/Recorder$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 16
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/wavpool/swipe/Recorder$State;
    .locals 1

    .line 16
    const-class v0, Lcom/squareup/wavpool/swipe/Recorder$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/wavpool/swipe/Recorder$State;

    return-object p0
.end method

.method public static values()[Lcom/squareup/wavpool/swipe/Recorder$State;
    .locals 1

    .line 16
    sget-object v0, Lcom/squareup/wavpool/swipe/Recorder$State;->$VALUES:[Lcom/squareup/wavpool/swipe/Recorder$State;

    invoke-virtual {v0}, [Lcom/squareup/wavpool/swipe/Recorder$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/wavpool/swipe/Recorder$State;

    return-object v0
.end method
