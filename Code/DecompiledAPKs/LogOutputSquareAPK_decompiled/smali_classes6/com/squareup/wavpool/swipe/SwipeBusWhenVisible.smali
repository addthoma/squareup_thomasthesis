.class public final Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;
.super Ljava/lang/Object;
.source "SwipeBusWhenVisible.java"


# instance fields
.field private final failedSwipes:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;",
            ">;"
        }
    .end annotation
.end field

.field private final successfulSwipes:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/wavpool/swipe/SwipeBus;Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Lcom/squareup/wavpool/swipe/-$$Lambda$SwipeBusWhenVisible$C0PfLHKxKGInddDARIzNem4BZpE;

    invoke-direct {v0, p2}, Lcom/squareup/wavpool/swipe/-$$Lambda$SwipeBusWhenVisible$C0PfLHKxKGInddDARIzNem4BZpE;-><init>(Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;)V

    .line 20
    invoke-virtual {p1}, Lcom/squareup/wavpool/swipe/SwipeBus;->DANGERdoNotUseDirectly()Lcom/squareup/wavpool/swipe/SwipeBus$Danger;

    move-result-object p2

    invoke-interface {p2}, Lcom/squareup/wavpool/swipe/SwipeBus$Danger;->successfulSwipes()Lio/reactivex/Observable;

    move-result-object p2

    invoke-virtual {p2, v0}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;->successfulSwipes:Lio/reactivex/Observable;

    .line 21
    invoke-virtual {p1}, Lcom/squareup/wavpool/swipe/SwipeBus;->DANGERdoNotUseDirectly()Lcom/squareup/wavpool/swipe/SwipeBus$Danger;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/wavpool/swipe/SwipeBus$Danger;->failedSwipes()Lio/reactivex/Observable;

    move-result-object p1

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;->failedSwipes:Lio/reactivex/Observable;

    return-void
.end method

.method static synthetic lambda$new$0(Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;Ljava/lang/Object;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 19
    invoke-virtual {p0}, Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;->isActivityVisible()Z

    move-result p0

    return p0
.end method


# virtual methods
.method public failedSwipes()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;",
            ">;"
        }
    .end annotation

    .line 29
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;->failedSwipes:Lio/reactivex/Observable;

    return-object v0
.end method

.method public successfulSwipes()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;",
            ">;"
        }
    .end annotation

    .line 25
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;->successfulSwipes:Lio/reactivex/Observable;

    return-object v0
.end method
