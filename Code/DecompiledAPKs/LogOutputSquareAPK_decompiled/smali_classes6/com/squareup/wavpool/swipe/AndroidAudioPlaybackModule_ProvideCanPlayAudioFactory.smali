.class public final Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideCanPlayAudioFactory;
.super Ljava/lang/Object;
.source "AndroidAudioPlaybackModule_ProvideCanPlayAudioFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final accessibilityManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/view/accessibility/AccessibilityManager;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderinfoProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final headsetProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/HeadsetConnectionState;",
            ">;"
        }
    .end annotation
.end field

.field private final isRunningProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/HeadsetConnectionState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/view/accessibility/AccessibilityManager;",
            ">;)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideCanPlayAudioFactory;->headsetProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p2, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideCanPlayAudioFactory;->cardReaderinfoProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p3, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideCanPlayAudioFactory;->isRunningProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p4, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideCanPlayAudioFactory;->accessibilityManagerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideCanPlayAudioFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/HeadsetConnectionState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/view/accessibility/AccessibilityManager;",
            ">;)",
            "Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideCanPlayAudioFactory;"
        }
    .end annotation

    .line 45
    new-instance v0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideCanPlayAudioFactory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideCanPlayAudioFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideCanPlayAudio(Ljavax/inject/Provider;Lcom/squareup/cardreader/CardReaderInfo;Ljavax/inject/Provider;Landroid/view/accessibility/AccessibilityManager;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/HeadsetConnectionState;",
            ">;",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Landroid/view/accessibility/AccessibilityManager;",
            ")Z"
        }
    .end annotation

    .line 51
    invoke-static {p0, p1, p2, p3}, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule;->provideCanPlayAudio(Ljavax/inject/Provider;Lcom/squareup/cardreader/CardReaderInfo;Ljavax/inject/Provider;Landroid/view/accessibility/AccessibilityManager;)Z

    move-result p0

    return p0
.end method


# virtual methods
.method public get()Ljava/lang/Boolean;
    .locals 4

    .line 38
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideCanPlayAudioFactory;->headsetProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideCanPlayAudioFactory;->cardReaderinfoProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/CardReaderInfo;

    iget-object v2, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideCanPlayAudioFactory;->isRunningProvider:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideCanPlayAudioFactory;->accessibilityManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/accessibility/AccessibilityManager;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideCanPlayAudioFactory;->provideCanPlayAudio(Ljavax/inject/Provider;Lcom/squareup/cardreader/CardReaderInfo;Ljavax/inject/Provider;Landroid/view/accessibility/AccessibilityManager;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideCanPlayAudioFactory;->get()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
