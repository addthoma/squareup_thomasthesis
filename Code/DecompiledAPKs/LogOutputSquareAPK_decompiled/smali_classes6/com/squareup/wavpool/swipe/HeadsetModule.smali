.class public abstract Lcom/squareup/wavpool/swipe/HeadsetModule;
.super Ljava/lang/Object;
.source "HeadsetModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static headsetConnection(Lcom/squareup/wavpool/swipe/Headset;)Lcom/squareup/wavpool/swipe/HeadsetConnection;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 9
    invoke-virtual {p0}, Lcom/squareup/wavpool/swipe/Headset;->currentState()Lcom/squareup/wavpool/swipe/HeadsetConnectionState;

    move-result-object p0

    return-object p0
.end method
