.class public abstract Lcom/squareup/wavpool/swipe/DecoderModule$Prod;
.super Ljava/lang/Object;
.source "DecoderModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/wavpool/swipe/DecoderModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Prod"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideAudioFilter(Lcom/squareup/wavpool/swipe/SquarewaveDecoder;)Lcom/squareup/squarewave/AudioFilter;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
