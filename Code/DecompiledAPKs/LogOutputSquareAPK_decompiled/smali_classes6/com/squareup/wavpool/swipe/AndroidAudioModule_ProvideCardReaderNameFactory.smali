.class public final Lcom/squareup/wavpool/swipe/AndroidAudioModule_ProvideCardReaderNameFactory;
.super Ljava/lang/Object;
.source "AndroidAudioModule_ProvideCardReaderNameFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/wavpool/swipe/AndroidAudioModule_ProvideCardReaderNameFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/wavpool/swipe/AndroidAudioModule_ProvideCardReaderNameFactory;
    .locals 1

    .line 23
    invoke-static {}, Lcom/squareup/wavpool/swipe/AndroidAudioModule_ProvideCardReaderNameFactory$InstanceHolder;->access$000()Lcom/squareup/wavpool/swipe/AndroidAudioModule_ProvideCardReaderNameFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideCardReaderName()Ljava/lang/String;
    .locals 1

    .line 28
    invoke-static {}, Lcom/squareup/wavpool/swipe/AndroidAudioModule;->provideCardReaderName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/wavpool/swipe/AndroidAudioModule_ProvideCardReaderNameFactory;->get()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public get()Ljava/lang/String;
    .locals 1

    .line 19
    invoke-static {}, Lcom/squareup/wavpool/swipe/AndroidAudioModule_ProvideCardReaderNameFactory;->provideCardReaderName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
