.class public interface abstract Lcom/squareup/wavpool/swipe/Recorder;
.super Ljava/lang/Object;
.source "Recorder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/wavpool/swipe/Recorder$State;
    }
.end annotation


# virtual methods
.method public abstract getRecordingState()Lcom/squareup/wavpool/swipe/Recorder$State;
.end method

.method public abstract start()V
.end method

.method public abstract stop()V
.end method
