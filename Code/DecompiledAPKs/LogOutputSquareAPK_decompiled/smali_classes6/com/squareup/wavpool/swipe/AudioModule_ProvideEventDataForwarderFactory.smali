.class public final Lcom/squareup/wavpool/swipe/AudioModule_ProvideEventDataForwarderFactory;
.super Ljava/lang/Object;
.source "AudioModule_ProvideEventDataForwarderFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/squarewave/EventDataForwarder;",
        ">;"
    }
.end annotation


# instance fields
.field private final loggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/logging/SwipeEventLogger;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/logging/SwipeEventLogger;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/wavpool/swipe/AudioModule_ProvideEventDataForwarderFactory;->loggerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/AudioModule_ProvideEventDataForwarderFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/logging/SwipeEventLogger;",
            ">;)",
            "Lcom/squareup/wavpool/swipe/AudioModule_ProvideEventDataForwarderFactory;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/wavpool/swipe/AudioModule_ProvideEventDataForwarderFactory;

    invoke-direct {v0, p0}, Lcom/squareup/wavpool/swipe/AudioModule_ProvideEventDataForwarderFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideEventDataForwarder(Lcom/squareup/logging/SwipeEventLogger;)Lcom/squareup/squarewave/EventDataForwarder;
    .locals 1

    .line 36
    invoke-static {p0}, Lcom/squareup/wavpool/swipe/AudioModule;->provideEventDataForwarder(Lcom/squareup/logging/SwipeEventLogger;)Lcom/squareup/squarewave/EventDataForwarder;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/squarewave/EventDataForwarder;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/squarewave/EventDataForwarder;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioModule_ProvideEventDataForwarderFactory;->loggerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/logging/SwipeEventLogger;

    invoke-static {v0}, Lcom/squareup/wavpool/swipe/AudioModule_ProvideEventDataForwarderFactory;->provideEventDataForwarder(Lcom/squareup/logging/SwipeEventLogger;)Lcom/squareup/squarewave/EventDataForwarder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/wavpool/swipe/AudioModule_ProvideEventDataForwarderFactory;->get()Lcom/squareup/squarewave/EventDataForwarder;

    move-result-object v0

    return-object v0
.end method
