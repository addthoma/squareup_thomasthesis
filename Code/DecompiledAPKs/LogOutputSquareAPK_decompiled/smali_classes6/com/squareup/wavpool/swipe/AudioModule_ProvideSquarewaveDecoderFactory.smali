.class public final Lcom/squareup/wavpool/swipe/AudioModule_ProvideSquarewaveDecoderFactory;
.super Ljava/lang/Object;
.source "AudioModule_ProvideSquarewaveDecoderFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/wavpool/swipe/SquarewaveDecoder;",
        ">;"
    }
.end annotation


# instance fields
.field private final busProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderIdProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderId;",
            ">;"
        }
    .end annotation
.end field

.field private final crashnadoProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crashnado/Crashnado;",
            ">;"
        }
    .end annotation
.end field

.field private final decoderExecutorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field private final decoderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/SignalDecoder;",
            ">;"
        }
    .end annotation
.end field

.field private final eventDataForwarderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/EventDataForwarder;",
            ">;"
        }
    .end annotation
.end field

.field private final headsetListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;",
            ">;"
        }
    .end annotation
.end field

.field private final loggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/logging/SwipeEventLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final readerTypeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/ReaderTypeProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final sampleFeederProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/SampleFeeder;",
            ">;"
        }
    .end annotation
.end field

.field private final sampleProcessorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/gum/SampleProcessor;",
            ">;"
        }
    .end annotation
.end field

.field private final swipeBusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/SwipeBus;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crashnado/Crashnado;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/SwipeBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/SampleFeeder;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/SignalDecoder;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/gum/SampleProcessor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/EventDataForwarder;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/logging/SwipeEventLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderId;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/ReaderTypeProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;",
            ">;)V"
        }
    .end annotation

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object p1, p0, Lcom/squareup/wavpool/swipe/AudioModule_ProvideSquarewaveDecoderFactory;->crashnadoProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p2, p0, Lcom/squareup/wavpool/swipe/AudioModule_ProvideSquarewaveDecoderFactory;->busProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p3, p0, Lcom/squareup/wavpool/swipe/AudioModule_ProvideSquarewaveDecoderFactory;->swipeBusProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p4, p0, Lcom/squareup/wavpool/swipe/AudioModule_ProvideSquarewaveDecoderFactory;->mainThreadProvider:Ljavax/inject/Provider;

    .line 66
    iput-object p5, p0, Lcom/squareup/wavpool/swipe/AudioModule_ProvideSquarewaveDecoderFactory;->sampleFeederProvider:Ljavax/inject/Provider;

    .line 67
    iput-object p6, p0, Lcom/squareup/wavpool/swipe/AudioModule_ProvideSquarewaveDecoderFactory;->decoderProvider:Ljavax/inject/Provider;

    .line 68
    iput-object p7, p0, Lcom/squareup/wavpool/swipe/AudioModule_ProvideSquarewaveDecoderFactory;->decoderExecutorProvider:Ljavax/inject/Provider;

    .line 69
    iput-object p8, p0, Lcom/squareup/wavpool/swipe/AudioModule_ProvideSquarewaveDecoderFactory;->sampleProcessorProvider:Ljavax/inject/Provider;

    .line 70
    iput-object p9, p0, Lcom/squareup/wavpool/swipe/AudioModule_ProvideSquarewaveDecoderFactory;->eventDataForwarderProvider:Ljavax/inject/Provider;

    .line 71
    iput-object p10, p0, Lcom/squareup/wavpool/swipe/AudioModule_ProvideSquarewaveDecoderFactory;->loggerProvider:Ljavax/inject/Provider;

    .line 72
    iput-object p11, p0, Lcom/squareup/wavpool/swipe/AudioModule_ProvideSquarewaveDecoderFactory;->cardReaderIdProvider:Ljavax/inject/Provider;

    .line 73
    iput-object p12, p0, Lcom/squareup/wavpool/swipe/AudioModule_ProvideSquarewaveDecoderFactory;->readerTypeProvider:Ljavax/inject/Provider;

    .line 74
    iput-object p13, p0, Lcom/squareup/wavpool/swipe/AudioModule_ProvideSquarewaveDecoderFactory;->headsetListenerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/AudioModule_ProvideSquarewaveDecoderFactory;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crashnado/Crashnado;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/SwipeBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/SampleFeeder;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/SignalDecoder;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/gum/SampleProcessor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/EventDataForwarder;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/logging/SwipeEventLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderId;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/ReaderTypeProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;",
            ">;)",
            "Lcom/squareup/wavpool/swipe/AudioModule_ProvideSquarewaveDecoderFactory;"
        }
    .end annotation

    .line 92
    new-instance v14, Lcom/squareup/wavpool/swipe/AudioModule_ProvideSquarewaveDecoderFactory;

    move-object v0, v14

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    invoke-direct/range {v0 .. v13}, Lcom/squareup/wavpool/swipe/AudioModule_ProvideSquarewaveDecoderFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v14
.end method

.method public static provideSquarewaveDecoder(Lcom/squareup/crashnado/Crashnado;Lcom/squareup/badbus/BadEventSink;Lcom/squareup/wavpool/swipe/SwipeBus;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/squarewave/SampleFeeder;Lcom/squareup/squarewave/SignalDecoder;Ljava/util/concurrent/ExecutorService;Lcom/squareup/squarewave/gum/SampleProcessor;Lcom/squareup/squarewave/EventDataForwarder;Lcom/squareup/logging/SwipeEventLogger;Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/wavpool/swipe/ReaderTypeProvider;Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;)Lcom/squareup/wavpool/swipe/SquarewaveDecoder;
    .locals 0

    .line 100
    invoke-static/range {p0 .. p12}, Lcom/squareup/wavpool/swipe/AudioModule;->provideSquarewaveDecoder(Lcom/squareup/crashnado/Crashnado;Lcom/squareup/badbus/BadEventSink;Lcom/squareup/wavpool/swipe/SwipeBus;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/squarewave/SampleFeeder;Lcom/squareup/squarewave/SignalDecoder;Ljava/util/concurrent/ExecutorService;Lcom/squareup/squarewave/gum/SampleProcessor;Lcom/squareup/squarewave/EventDataForwarder;Lcom/squareup/logging/SwipeEventLogger;Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/wavpool/swipe/ReaderTypeProvider;Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;)Lcom/squareup/wavpool/swipe/SquarewaveDecoder;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/wavpool/swipe/SquarewaveDecoder;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/wavpool/swipe/SquarewaveDecoder;
    .locals 14

    .line 79
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioModule_ProvideSquarewaveDecoderFactory;->crashnadoProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/crashnado/Crashnado;

    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioModule_ProvideSquarewaveDecoderFactory;->busProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/badbus/BadEventSink;

    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioModule_ProvideSquarewaveDecoderFactory;->swipeBusProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/wavpool/swipe/SwipeBus;

    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioModule_ProvideSquarewaveDecoderFactory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/thread/executor/MainThread;

    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioModule_ProvideSquarewaveDecoderFactory;->sampleFeederProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/squarewave/SampleFeeder;

    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioModule_ProvideSquarewaveDecoderFactory;->decoderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/squarewave/SignalDecoder;

    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioModule_ProvideSquarewaveDecoderFactory;->decoderExecutorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Ljava/util/concurrent/ExecutorService;

    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioModule_ProvideSquarewaveDecoderFactory;->sampleProcessorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/squarewave/gum/SampleProcessor;

    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioModule_ProvideSquarewaveDecoderFactory;->eventDataForwarderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/squarewave/EventDataForwarder;

    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioModule_ProvideSquarewaveDecoderFactory;->loggerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/logging/SwipeEventLogger;

    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioModule_ProvideSquarewaveDecoderFactory;->cardReaderIdProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/cardreader/CardReaderId;

    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioModule_ProvideSquarewaveDecoderFactory;->readerTypeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Lcom/squareup/wavpool/swipe/ReaderTypeProvider;

    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioModule_ProvideSquarewaveDecoderFactory;->headsetListenerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v13, v0

    check-cast v13, Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;

    invoke-static/range {v1 .. v13}, Lcom/squareup/wavpool/swipe/AudioModule_ProvideSquarewaveDecoderFactory;->provideSquarewaveDecoder(Lcom/squareup/crashnado/Crashnado;Lcom/squareup/badbus/BadEventSink;Lcom/squareup/wavpool/swipe/SwipeBus;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/squarewave/SampleFeeder;Lcom/squareup/squarewave/SignalDecoder;Ljava/util/concurrent/ExecutorService;Lcom/squareup/squarewave/gum/SampleProcessor;Lcom/squareup/squarewave/EventDataForwarder;Lcom/squareup/logging/SwipeEventLogger;Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/wavpool/swipe/ReaderTypeProvider;Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;)Lcom/squareup/wavpool/swipe/SquarewaveDecoder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 18
    invoke-virtual {p0}, Lcom/squareup/wavpool/swipe/AudioModule_ProvideSquarewaveDecoderFactory;->get()Lcom/squareup/wavpool/swipe/SquarewaveDecoder;

    move-result-object v0

    return-object v0
.end method
