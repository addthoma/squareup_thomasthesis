.class public final Lcom/squareup/workflow/LaunchWorkflowKt;
.super Ljava/lang/Object;
.source "LaunchWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLaunchWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LaunchWorkflow.kt\ncom/squareup/workflow/LaunchWorkflowKt\n*L\n1#1,184:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000V\n\u0002\u0008\u0005\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u001a\u00db\u0001\u0010\u0000\u001a\u0002H\u0001\"\u0004\u0008\u0000\u0010\u0002\"\u0004\u0008\u0001\u0010\u0003\"\u0008\u0008\u0002\u0010\u0004*\u00020\u0005\"\u0004\u0008\u0003\u0010\u0006\"\u0004\u0008\u0004\u0010\u00012\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u001e\u0010\u000b\u001a\u001a\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00060\u000c2\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u0002H\u00020\u000e2\u0008\u0010\u000f\u001a\u0004\u0018\u00010\u00102\u0008\u0010\u0011\u001a\u0004\u0018\u0001H\u00032\u0008\u0008\u0002\u0010\u0012\u001a\u00020\u00132N\u0010\u0014\u001aJ\u0012\u0004\u0012\u00020\u0008\u0012\u001f\u0012\u001d\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00060\u0016\u00a2\u0006\u000c\u0008\u0017\u0012\u0008\u0008\u0018\u0012\u0004\u0008\u0008(\u0019\u0012\u0004\u0012\u0002H\u00010\u0015j\u0014\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u0001`\u001a\u00a2\u0006\u0002\u0008\u001bH\u0000\u00a2\u0006\u0002\u0010\u001c\u001a\u009d\u0001\u0010\u001d\u001a\u0002H\u0001\"\u0004\u0008\u0000\u0010\u0002\"\u0008\u0008\u0001\u0010\u0004*\u00020\u0005\"\u0004\u0008\u0002\u0010\u0006\"\u0004\u0008\u0003\u0010\u00012\u0006\u0010\u0007\u001a\u00020\u00082\u0018\u0010\u000b\u001a\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00060\u001e2\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u0002H\u00020\u000e2\n\u0008\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u001028\u0010\u0014\u001a4\u0012\u0004\u0012\u00020\u0008\u0012\u001f\u0012\u001d\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00060\u0016\u00a2\u0006\u000c\u0008\u0017\u0012\u0008\u0008\u0018\u0012\u0004\u0008\u0008(\u0019\u0012\u0004\u0012\u0002H\u00010\u0015\u00a2\u0006\u0002\u0008\u001b\u00a2\u0006\u0002\u0010\u001f*\u0082\u0001\u0008\u0000\u0010 \u001a\u0004\u0008\u0000\u0010!\u001a\u0004\u0008\u0001\u0010\"\u001a\u0004\u0008\u0002\u0010#\"4\u0012\u0004\u0012\u00020\u0008\u0012\u001f\u0012\u001d\u0012\u0004\u0012\u0002H!\u0012\u0004\u0012\u0002H\"0\u0016\u00a2\u0006\u000c\u0008\u0017\u0012\u0008\u0008\u0018\u0012\u0004\u0008\u0008(\u0019\u0012\u0004\u0012\u0002H#0\u0015\u00a2\u0006\u0002\u0008\u001b24\u0012\u0004\u0012\u00020\u0008\u0012\u001f\u0012\u001d\u0012\u0004\u0012\u0002H!\u0012\u0004\u0012\u0002H\"0\u0016\u00a2\u0006\u000c\u0008\u0017\u0012\u0008\u0008\u0018\u0012\u0004\u0008\u0008(\u0019\u0012\u0004\u0012\u0002H#0\u0015\u00a2\u0006\u0002\u0008\u001b\u00a8\u0006$"
    }
    d2 = {
        "launchWorkflowImpl",
        "RunnerT",
        "PropsT",
        "StateT",
        "OutputT",
        "",
        "RenderingT",
        "scope",
        "Lkotlinx/coroutines/CoroutineScope;",
        "workflowLoop",
        "Lcom/squareup/workflow/internal/WorkflowLoop;",
        "workflow",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "props",
        "Lkotlinx/coroutines/flow/Flow;",
        "initialSnapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "initialState",
        "workerContext",
        "Lkotlin/coroutines/CoroutineContext;",
        "beforeStart",
        "Lkotlin/Function2;",
        "Lcom/squareup/workflow/WorkflowSession;",
        "Lkotlin/ParameterName;",
        "name",
        "session",
        "Lcom/squareup/workflow/Configurator;",
        "Lkotlin/ExtensionFunctionType;",
        "(Lkotlinx/coroutines/CoroutineScope;Lcom/squareup/workflow/internal/WorkflowLoop;Lcom/squareup/workflow/StatefulWorkflow;Lkotlinx/coroutines/flow/Flow;Lcom/squareup/workflow/Snapshot;Ljava/lang/Object;Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;)Ljava/lang/Object;",
        "launchWorkflowIn",
        "Lcom/squareup/workflow/Workflow;",
        "(Lkotlinx/coroutines/CoroutineScope;Lcom/squareup/workflow/Workflow;Lkotlinx/coroutines/flow/Flow;Lcom/squareup/workflow/Snapshot;Lkotlin/jvm/functions/Function2;)Ljava/lang/Object;",
        "Configurator",
        "O",
        "R",
        "T",
        "workflow-runtime"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final launchWorkflowImpl(Lkotlinx/coroutines/CoroutineScope;Lcom/squareup/workflow/internal/WorkflowLoop;Lcom/squareup/workflow/StatefulWorkflow;Lkotlinx/coroutines/flow/Flow;Lcom/squareup/workflow/Snapshot;Ljava/lang/Object;Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;)Ljava/lang/Object;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<PropsT:",
            "Ljava/lang/Object;",
            "StateT:",
            "Ljava/lang/Object;",
            "OutputT:",
            "Ljava/lang/Object;",
            "RenderingT:",
            "Ljava/lang/Object;",
            "RunnerT:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlinx/coroutines/CoroutineScope;",
            "Lcom/squareup/workflow/internal/WorkflowLoop;",
            "Lcom/squareup/workflow/StatefulWorkflow<",
            "-TPropsT;TStateT;+TOutputT;+TRenderingT;>;",
            "Lkotlinx/coroutines/flow/Flow<",
            "+TPropsT;>;",
            "Lcom/squareup/workflow/Snapshot;",
            "TStateT;",
            "Lkotlin/coroutines/CoroutineContext;",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Lkotlinx/coroutines/CoroutineScope;",
            "-",
            "Lcom/squareup/workflow/WorkflowSession<",
            "+TOutputT;+TRenderingT;>;+TRunnerT;>;)TRunnerT;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v7, p6

    move-object/from16 v1, p7

    const-string v2, "scope"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "workflowLoop"

    move-object/from16 v3, p1

    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "workflow"

    move-object/from16 v4, p2

    invoke-static {v4, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "props"

    move-object/from16 v5, p3

    invoke-static {v5, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "workerContext"

    invoke-static {v7, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "beforeStart"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 131
    new-instance v11, Lkotlinx/coroutines/channels/ConflatedBroadcastChannel;

    invoke-direct {v11}, Lkotlinx/coroutines/channels/ConflatedBroadcastChannel;-><init>()V

    const/4 v2, 0x1

    .line 132
    invoke-static {v2}, Lkotlinx/coroutines/channels/BroadcastChannelKt;->BroadcastChannel(I)Lkotlinx/coroutines/channels/BroadcastChannel;

    move-result-object v12

    .line 133
    invoke-interface/range {p0 .. p0}, Lkotlinx/coroutines/CoroutineScope;->getCoroutineContext()Lkotlin/coroutines/CoroutineContext;

    move-result-object v6

    sget-object v8, Lkotlinx/coroutines/Job;->Key:Lkotlinx/coroutines/Job$Key;

    check-cast v8, Lkotlin/coroutines/CoroutineContext$Key;

    invoke-interface {v6, v8}, Lkotlin/coroutines/CoroutineContext;->get(Lkotlin/coroutines/CoroutineContext$Key;)Lkotlin/coroutines/CoroutineContext$Element;

    move-result-object v6

    check-cast v6, Lkotlinx/coroutines/Job;

    invoke-static {v6}, Lkotlinx/coroutines/JobKt;->Job(Lkotlinx/coroutines/Job;)Lkotlinx/coroutines/CompletableJob;

    move-result-object v6

    check-cast v6, Lkotlin/coroutines/CoroutineContext;

    invoke-static {v0, v6}, Lkotlinx/coroutines/CoroutineScopeKt;->plus(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;)Lkotlinx/coroutines/CoroutineScope;

    move-result-object v13

    .line 134
    sget-object v0, Lkotlinx/coroutines/Job;->Key:Lkotlinx/coroutines/Job$Key;

    check-cast v0, Lkotlin/coroutines/CoroutineContext$Key;

    invoke-interface {v7, v0}, Lkotlin/coroutines/CoroutineContext;->get(Lkotlin/coroutines/CoroutineContext$Key;)Lkotlin/coroutines/CoroutineContext$Element;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_1

    .line 137
    new-instance v0, Lcom/squareup/workflow/WorkflowSession;

    move-object v2, v11

    check-cast v2, Lkotlinx/coroutines/channels/BroadcastChannel;

    invoke-static {v2}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlinx/coroutines/channels/BroadcastChannel;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v15

    invoke-static {v12}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlinx/coroutines/channels/BroadcastChannel;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v16

    const/16 v17, 0x0

    const/16 v18, 0x4

    const/16 v19, 0x0

    move-object v14, v0

    invoke-direct/range {v14 .. v19}, Lcom/squareup/workflow/WorkflowSession;-><init>(Lkotlinx/coroutines/flow/Flow;Lkotlinx/coroutines/flow/Flow;Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 138
    invoke-interface {v1, v13, v0}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    .line 139
    invoke-virtual {v0}, Lcom/squareup/workflow/WorkflowSession;->getDiagnosticListener()Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    move-result-object v1

    const/4 v15, 0x0

    const/16 v16, 0x0

    .line 141
    new-instance v17, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$workflowJob$1;

    const/4 v10, 0x0

    move-object/from16 v0, v17

    move-object/from16 v2, p2

    move-object/from16 v3, p1

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object v8, v11

    move-object v9, v12

    invoke-direct/range {v0 .. v10}, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$workflowJob$1;-><init>(Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;Lcom/squareup/workflow/StatefulWorkflow;Lcom/squareup/workflow/internal/WorkflowLoop;Lkotlinx/coroutines/flow/Flow;Lcom/squareup/workflow/Snapshot;Ljava/lang/Object;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/channels/ConflatedBroadcastChannel;Lkotlinx/coroutines/channels/BroadcastChannel;Lkotlin/coroutines/Continuation;)V

    move-object/from16 v0, v17

    check-cast v0, Lkotlin/jvm/functions/Function2;

    const/4 v1, 0x3

    const/4 v2, 0x0

    move-object/from16 p0, v13

    move-object/from16 p1, v15

    move-object/from16 p2, v16

    move-object/from16 p3, v0

    move/from16 p4, v1

    move-object/from16 p5, v2

    invoke-static/range {p0 .. p5}, Lkotlinx/coroutines/BuildersKt;->launch$default(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    move-result-object v0

    .line 163
    new-instance v1, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$2;

    invoke-direct {v1, v11, v12, v13}, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$2;-><init>(Lkotlinx/coroutines/channels/ConflatedBroadcastChannel;Lkotlinx/coroutines/channels/BroadcastChannel;Lkotlinx/coroutines/CoroutineScope;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-interface {v0, v1}, Lkotlinx/coroutines/Job;->invokeOnCompletion(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/DisposableHandle;

    return-object v14

    .line 134
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Expected workerContext not to have a Job."

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public static synthetic launchWorkflowImpl$default(Lkotlinx/coroutines/CoroutineScope;Lcom/squareup/workflow/internal/WorkflowLoop;Lcom/squareup/workflow/StatefulWorkflow;Lkotlinx/coroutines/flow/Flow;Lcom/squareup/workflow/Snapshot;Ljava/lang/Object;Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Ljava/lang/Object;
    .locals 9

    and-int/lit8 v0, p8, 0x40

    if-eqz v0, :cond_0

    .line 128
    sget-object v0, Lkotlin/coroutines/EmptyCoroutineContext;->INSTANCE:Lkotlin/coroutines/EmptyCoroutineContext;

    check-cast v0, Lkotlin/coroutines/CoroutineContext;

    move-object v7, v0

    goto :goto_0

    :cond_0
    move-object v7, p6

    :goto_0
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v8, p7

    invoke-static/range {v1 .. v8}, Lcom/squareup/workflow/LaunchWorkflowKt;->launchWorkflowImpl(Lkotlinx/coroutines/CoroutineScope;Lcom/squareup/workflow/internal/WorkflowLoop;Lcom/squareup/workflow/StatefulWorkflow;Lkotlinx/coroutines/flow/Flow;Lcom/squareup/workflow/Snapshot;Ljava/lang/Object;Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static final launchWorkflowIn(Lkotlinx/coroutines/CoroutineScope;Lcom/squareup/workflow/Workflow;Lkotlinx/coroutines/flow/Flow;Lcom/squareup/workflow/Snapshot;Lkotlin/jvm/functions/Function2;)Ljava/lang/Object;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<PropsT:",
            "Ljava/lang/Object;",
            "OutputT:",
            "Ljava/lang/Object;",
            "RenderingT:",
            "Ljava/lang/Object;",
            "RunnerT:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlinx/coroutines/CoroutineScope;",
            "Lcom/squareup/workflow/Workflow<",
            "-TPropsT;+TOutputT;+TRenderingT;>;",
            "Lkotlinx/coroutines/flow/Flow<",
            "+TPropsT;>;",
            "Lcom/squareup/workflow/Snapshot;",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Lkotlinx/coroutines/CoroutineScope;",
            "-",
            "Lcom/squareup/workflow/WorkflowSession<",
            "+TOutputT;+TRenderingT;>;+TRunnerT;>;)TRunnerT;"
        }
    .end annotation

    const-string v0, "scope"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "workflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "props"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "beforeStart"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    new-instance v0, Lcom/squareup/workflow/internal/RealWorkflowLoop;

    invoke-direct {v0}, Lcom/squareup/workflow/internal/RealWorkflowLoop;-><init>()V

    move-object v2, v0

    check-cast v2, Lcom/squareup/workflow/internal/WorkflowLoop;

    .line 108
    invoke-interface {p1}, Lcom/squareup/workflow/Workflow;->asStatefulWorkflow()Lcom/squareup/workflow/StatefulWorkflow;

    move-result-object v3

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v9, 0x40

    const/4 v10, 0x0

    move-object v1, p0

    move-object v4, p2

    move-object v5, p3

    move-object v8, p4

    .line 105
    invoke-static/range {v1 .. v10}, Lcom/squareup/workflow/LaunchWorkflowKt;->launchWorkflowImpl$default(Lkotlinx/coroutines/CoroutineScope;Lcom/squareup/workflow/internal/WorkflowLoop;Lcom/squareup/workflow/StatefulWorkflow;Lkotlinx/coroutines/flow/Flow;Lcom/squareup/workflow/Snapshot;Ljava/lang/Object;Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic launchWorkflowIn$default(Lkotlinx/coroutines/CoroutineScope;Lcom/squareup/workflow/Workflow;Lkotlinx/coroutines/flow/Flow;Lcom/squareup/workflow/Snapshot;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Ljava/lang/Object;
    .locals 0

    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_0

    const/4 p3, 0x0

    .line 103
    check-cast p3, Lcom/squareup/workflow/Snapshot;

    :cond_0
    invoke-static {p0, p1, p2, p3, p4}, Lcom/squareup/workflow/LaunchWorkflowKt;->launchWorkflowIn(Lkotlinx/coroutines/CoroutineScope;Lcom/squareup/workflow/Workflow;Lkotlinx/coroutines/flow/Flow;Lcom/squareup/workflow/Snapshot;Lkotlin/jvm/functions/Function2;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method
