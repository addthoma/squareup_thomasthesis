.class public final Lcom/squareup/workflow/ui/WorkflowRunner$Companion;
.super Ljava/lang/Object;
.source "WorkflowRunner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/workflow/ui/WorkflowRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002JL\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H\u00060\u0004\"\u0008\u0008\u0001\u0010\u0006*\u00020\u00012\u0018\u0010\u0007\u001a\u0014\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u00020\u00010\u00082\u0008\u0008\u0002\u0010\t\u001a\u00020\n2\n\u0008\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u000cJ>\u0010\r\u001a\u0008\u0012\u0004\u0012\u0002H\u00060\u000e\"\u0004\u0008\u0001\u0010\u000f\"\u0008\u0008\u0002\u0010\u0006*\u00020\u00012\u0006\u0010\u0010\u001a\u00020\u00112\u0018\u0010\u0012\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u000f\u0012\u0004\u0012\u0002H\u00060\u00040\u0013J>\u0010\r\u001a\u0008\u0012\u0004\u0012\u0002H\u00060\u000e\"\u0004\u0008\u0001\u0010\u000f\"\u0008\u0008\u0002\u0010\u0006*\u00020\u00012\u0006\u0010\u0014\u001a\u00020\u00152\u0018\u0010\u0012\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u000f\u0012\u0004\u0012\u0002H\u00060\u00040\u0013\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/workflow/ui/WorkflowRunner$Companion;",
        "",
        "()V",
        "Config",
        "Lcom/squareup/workflow/ui/WorkflowRunner$Config;",
        "",
        "OutputT",
        "workflow",
        "Lcom/squareup/workflow/Workflow;",
        "dispatcher",
        "Lkotlinx/coroutines/CoroutineDispatcher;",
        "diagnosticListener",
        "Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;",
        "startWorkflow",
        "Lcom/squareup/workflow/ui/WorkflowRunner;",
        "PropsT",
        "fragment",
        "Landroidx/fragment/app/Fragment;",
        "configure",
        "Lkotlin/Function0;",
        "activity",
        "Landroidx/fragment/app/FragmentActivity;",
        "core-android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$INSTANCE:Lcom/squareup/workflow/ui/WorkflowRunner$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 80
    new-instance v0, Lcom/squareup/workflow/ui/WorkflowRunner$Companion;

    invoke-direct {v0}, Lcom/squareup/workflow/ui/WorkflowRunner$Companion;-><init>()V

    sput-object v0, Lcom/squareup/workflow/ui/WorkflowRunner$Companion;->$$INSTANCE:Lcom/squareup/workflow/ui/WorkflowRunner$Companion;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static synthetic Config$default(Lcom/squareup/workflow/ui/WorkflowRunner$Companion;Lcom/squareup/workflow/Workflow;Lkotlinx/coroutines/CoroutineDispatcher;Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;ILjava/lang/Object;)Lcom/squareup/workflow/ui/WorkflowRunner$Config;
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    .line 88
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->getMain()Lkotlinx/coroutines/MainCoroutineDispatcher;

    move-result-object p2

    invoke-virtual {p2}, Lkotlinx/coroutines/MainCoroutineDispatcher;->getImmediate()Lkotlinx/coroutines/MainCoroutineDispatcher;

    move-result-object p2

    check-cast p2, Lkotlinx/coroutines/CoroutineDispatcher;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 89
    check-cast p3, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    :cond_1
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/workflow/ui/WorkflowRunner$Companion;->Config(Lcom/squareup/workflow/Workflow;Lkotlinx/coroutines/CoroutineDispatcher;Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;)Lcom/squareup/workflow/ui/WorkflowRunner$Config;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final Config(Lcom/squareup/workflow/Workflow;Lkotlinx/coroutines/CoroutineDispatcher;Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;)Lcom/squareup/workflow/ui/WorkflowRunner$Config;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<OutputT:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/Workflow<",
            "-",
            "Lkotlin/Unit;",
            "+TOutputT;+",
            "Ljava/lang/Object;",
            ">;",
            "Lkotlinx/coroutines/CoroutineDispatcher;",
            "Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;",
            ")",
            "Lcom/squareup/workflow/ui/WorkflowRunner$Config<",
            "Lkotlin/Unit;",
            "TOutputT;>;"
        }
    .end annotation

    const-string v0, "workflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dispatcher"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    new-instance v0, Lcom/squareup/workflow/ui/WorkflowRunner$Config;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-direct {v0, p1, v1, p2, p3}, Lcom/squareup/workflow/ui/WorkflowRunner$Config;-><init>(Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Lkotlinx/coroutines/CoroutineDispatcher;Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;)V

    return-object v0
.end method

.method public final startWorkflow(Landroidx/fragment/app/Fragment;Lkotlin/jvm/functions/Function0;)Lcom/squareup/workflow/ui/WorkflowRunner;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<PropsT:",
            "Ljava/lang/Object;",
            "OutputT:",
            "Ljava/lang/Object;",
            ">(",
            "Landroidx/fragment/app/Fragment;",
            "Lkotlin/jvm/functions/Function0<",
            "Lcom/squareup/workflow/ui/WorkflowRunner$Config<",
            "TPropsT;TOutputT;>;>;)",
            "Lcom/squareup/workflow/ui/WorkflowRunner<",
            "TOutputT;>;"
        }
    .end annotation

    const-string v0, "fragment"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "configure"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 129
    new-instance v0, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel$Factory;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getSavedStateRegistry()Landroidx/savedstate/SavedStateRegistry;

    move-result-object v1

    const-string v2, "fragment.savedStateRegistry"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v1, p2}, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel$Factory;-><init>(Landroidx/savedstate/SavedStateRegistry;Lkotlin/jvm/functions/Function0;)V

    .line 132
    new-instance p2, Landroidx/lifecycle/ViewModelProvider;

    check-cast p1, Landroidx/lifecycle/ViewModelStoreOwner;

    check-cast v0, Landroidx/lifecycle/ViewModelProvider$Factory;

    invoke-direct {p2, p1, v0}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    const-class p1, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel;

    invoke-virtual {p2, p1}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object p1

    if-eqz p1, :cond_0

    check-cast p1, Lcom/squareup/workflow/ui/WorkflowRunner;

    return-object p1

    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type com.squareup.workflow.ui.WorkflowRunner<OutputT>"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final startWorkflow(Landroidx/fragment/app/FragmentActivity;Lkotlin/jvm/functions/Function0;)Lcom/squareup/workflow/ui/WorkflowRunner;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<PropsT:",
            "Ljava/lang/Object;",
            "OutputT:",
            "Ljava/lang/Object;",
            ">(",
            "Landroidx/fragment/app/FragmentActivity;",
            "Lkotlin/jvm/functions/Function0<",
            "Lcom/squareup/workflow/ui/WorkflowRunner$Config<",
            "TPropsT;TOutputT;>;>;)",
            "Lcom/squareup/workflow/ui/WorkflowRunner<",
            "TOutputT;>;"
        }
    .end annotation

    const-string v0, "activity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "configure"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 108
    new-instance v0, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel$Factory;

    invoke-virtual {p1}, Landroidx/fragment/app/FragmentActivity;->getSavedStateRegistry()Landroidx/savedstate/SavedStateRegistry;

    move-result-object v1

    const-string v2, "activity.savedStateRegistry"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v1, p2}, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel$Factory;-><init>(Landroidx/savedstate/SavedStateRegistry;Lkotlin/jvm/functions/Function0;)V

    .line 111
    new-instance p2, Landroidx/lifecycle/ViewModelProvider;

    check-cast p1, Landroidx/lifecycle/ViewModelStoreOwner;

    check-cast v0, Landroidx/lifecycle/ViewModelProvider$Factory;

    invoke-direct {p2, p1, v0}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    const-class p1, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel;

    invoke-virtual {p2, p1}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object p1

    if-eqz p1, :cond_0

    check-cast p1, Lcom/squareup/workflow/ui/WorkflowRunner;

    return-object p1

    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type com.squareup.workflow.ui.WorkflowRunner<OutputT>"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
