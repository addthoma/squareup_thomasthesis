.class public interface abstract Lcom/squareup/workflow/ui/WorkflowRunner;
.super Ljava/lang/Object;
.source "WorkflowRunner.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/workflow/ui/WorkflowRunner$Config;,
        Lcom/squareup/workflow/ui/WorkflowRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<OutputT:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008f\u0018\u0000 \u000b*\n\u0008\u0000\u0010\u0001 \u0001*\u00020\u00022\u00020\u0002:\u0002\u000b\u000cR\u001a\u0010\u0003\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00020\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006R\u001a\u0010\u0007\u001a\n\u0012\u0006\u0008\u0001\u0012\u00028\u00000\u0008X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/workflow/ui/WorkflowRunner;",
        "OutputT",
        "",
        "renderings",
        "Lio/reactivex/Observable;",
        "getRenderings",
        "()Lio/reactivex/Observable;",
        "result",
        "Lio/reactivex/Maybe;",
        "getResult",
        "()Lio/reactivex/Maybe;",
        "Companion",
        "Config",
        "core-android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/workflow/ui/WorkflowRunner$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/squareup/workflow/ui/WorkflowRunner$Companion;->$$INSTANCE:Lcom/squareup/workflow/ui/WorkflowRunner$Companion;

    sput-object v0, Lcom/squareup/workflow/ui/WorkflowRunner;->Companion:Lcom/squareup/workflow/ui/WorkflowRunner$Companion;

    return-void
.end method


# virtual methods
.method public abstract getRenderings()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "+",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getResult()Lio/reactivex/Maybe;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Maybe<",
            "+TOutputT;>;"
        }
    .end annotation
.end method
