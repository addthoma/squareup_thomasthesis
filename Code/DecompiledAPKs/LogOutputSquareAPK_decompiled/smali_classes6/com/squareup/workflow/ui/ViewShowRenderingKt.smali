.class public final Lcom/squareup/workflow/ui/ViewShowRenderingKt;
.super Ljava/lang/Object;
.source "ViewShowRendering.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nViewShowRendering.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ViewShowRendering.kt\ncom/squareup/workflow/ui/ViewShowRenderingKt\n*L\n1#1,139:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\n\u001aM\u0010\u000b\u001a\u00020\u000c\"\u0008\u0008\u0000\u0010\r*\u00020\u000e*\u00020\u00022\u0006\u0010\u000f\u001a\u0002H\r2\u0006\u0010\u0010\u001a\u00020\u00012\"\u0010\u0011\u001a\u001e\u0012\u0004\u0012\u0002H\r\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u000c0\u0012j\u0008\u0012\u0004\u0012\u0002H\r`\u0013\u00a2\u0006\u0002\u0010\u0014\u001a\u0012\u0010\u0015\u001a\u00020\u0016*\u00020\u00022\u0006\u0010\u0017\u001a\u00020\u000e\u001a\u001b\u0010\u0018\u001a\u0004\u0018\u0001H\r\"\u0008\u0008\u0000\u0010\r*\u00020\u000e*\u00020\u0002\u00a2\u0006\u0002\u0010\u0019\u001a4\u0010\u001a\u001a\"\u0012\u0004\u0012\u0002H\r\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u000c\u0018\u00010\u0012j\n\u0012\u0004\u0012\u0002H\r\u0018\u0001`\u0013\"\u0008\u0008\u0000\u0010\r*\u00020\u000e*\u00020\u0002\u001a\u0014\u0010\u001b\u001a\u00020\u0016*\u00020\u000e2\u0006\u0010\u001c\u001a\u00020\u000eH\u0002\u001a)\u0010\u0011\u001a\u00020\u000c\"\u0008\u0008\u0000\u0010\r*\u00020\u000e*\u00020\u00022\u0006\u0010\u0017\u001a\u0002H\r2\u0006\u0010\u001d\u001a\u00020\u0001\u00a2\u0006\u0002\u0010\u001e\"\u0017\u0010\u0000\u001a\u0004\u0018\u00010\u0001*\u00020\u00028F\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0004\"$\u0010\u0005\u001a\u0008\u0012\u0002\u0008\u0003\u0018\u00010\u0006*\u00020\u00028@X\u0081\u0004\u00a2\u0006\u000c\u0012\u0004\u0008\u0007\u0010\u0008\u001a\u0004\u0008\t\u0010\n*4\u0010\u001f\u001a\u0004\u0008\u0000\u0010\r\"\u0014\u0012\u0004\u0012\u0002H\r\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u000c0\u00122\u0014\u0012\u0004\u0012\u0002H\r\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u000c0\u0012\u00a8\u0006 "
    }
    d2 = {
        "hints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "Landroid/view/View;",
        "getHints",
        "(Landroid/view/View;)Lcom/squareup/workflow/ui/ContainerHints;",
        "showRenderingTag",
        "Lcom/squareup/workflow/ui/ShowRenderingTag;",
        "showRenderingTag$annotations",
        "(Landroid/view/View;)V",
        "getShowRenderingTag",
        "(Landroid/view/View;)Lcom/squareup/workflow/ui/ShowRenderingTag;",
        "bindShowRendering",
        "",
        "RenderingT",
        "",
        "initialRendering",
        "initialContainerHints",
        "showRendering",
        "Lkotlin/Function2;",
        "Lcom/squareup/workflow/ui/ViewShowRendering;",
        "(Landroid/view/View;Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;Lkotlin/jvm/functions/Function2;)V",
        "canShowRendering",
        "",
        "rendering",
        "getRendering",
        "(Landroid/view/View;)Ljava/lang/Object;",
        "getShowRendering",
        "matches",
        "other",
        "containerHints",
        "(Landroid/view/View;Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V",
        "ViewShowRendering",
        "core-android_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final bindShowRendering(Landroid/view/View;Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;Lkotlin/jvm/functions/Function2;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<RenderingT:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/view/View;",
            "TRenderingT;",
            "Lcom/squareup/workflow/ui/ContainerHints;",
            "Lkotlin/jvm/functions/Function2<",
            "-TRenderingT;-",
            "Lcom/squareup/workflow/ui/ContainerHints;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$bindShowRendering"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "initialRendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "initialContainerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "showRendering"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    sget v0, Lcom/squareup/workflow/ui/R$id;->view_show_rendering_function:I

    .line 55
    new-instance v1, Lcom/squareup/workflow/ui/ShowRenderingTag;

    invoke-direct {v1, p1, p2, p3}, Lcom/squareup/workflow/ui/ShowRenderingTag;-><init>(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;Lkotlin/jvm/functions/Function2;)V

    .line 53
    invoke-virtual {p0, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 57
    invoke-interface {p3, p1, p2}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public static final canShowRendering(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    const-string v0, "$this$canShowRendering"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    invoke-static {p0}, Lcom/squareup/workflow/ui/ViewShowRenderingKt;->getRendering(Landroid/view/View;)Ljava/lang/Object;

    move-result-object p0

    const/4 v0, 0x1

    if-eqz p0, :cond_0

    invoke-static {p0, p1}, Lcom/squareup/workflow/ui/ViewShowRenderingKt;->matches(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static final getHints(Landroid/view/View;)Lcom/squareup/workflow/ui/ContainerHints;
    .locals 1

    const-string v0, "$this$hints"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 120
    invoke-static {p0}, Lcom/squareup/workflow/ui/ViewShowRenderingKt;->getShowRenderingTag(Landroid/view/View;)Lcom/squareup/workflow/ui/ShowRenderingTag;

    move-result-object p0

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/workflow/ui/ShowRenderingTag;->getHints()Lcom/squareup/workflow/ui/ContainerHints;

    move-result-object p0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method public static final getRendering(Landroid/view/View;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<RenderingT:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/view/View;",
            ")TRenderingT;"
        }
    .end annotation

    const-string v0, "$this$getRendering"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 110
    invoke-static {p0}, Lcom/squareup/workflow/ui/ViewShowRenderingKt;->getShowRenderingTag(Landroid/view/View;)Lcom/squareup/workflow/ui/ShowRenderingTag;

    move-result-object p0

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/workflow/ui/ShowRenderingTag;->getShowing()Ljava/lang/Object;

    move-result-object p0

    goto :goto_0

    :cond_0
    move-object p0, v0

    :goto_0
    if-nez p0, :cond_1

    move-object p0, v0

    :cond_1
    return-object p0
.end method

.method public static final getShowRendering(Landroid/view/View;)Lkotlin/jvm/functions/Function2;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<RenderingT:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/view/View;",
            ")",
            "Lkotlin/jvm/functions/Function2<",
            "TRenderingT;",
            "Lcom/squareup/workflow/ui/ContainerHints;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$getShowRendering"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 127
    invoke-static {p0}, Lcom/squareup/workflow/ui/ViewShowRenderingKt;->getShowRenderingTag(Landroid/view/View;)Lcom/squareup/workflow/ui/ShowRenderingTag;

    move-result-object p0

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/workflow/ui/ShowRenderingTag;->getShowRendering()Lkotlin/jvm/functions/Function2;

    move-result-object p0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method public static final getShowRenderingTag(Landroid/view/View;)Lcom/squareup/workflow/ui/ShowRenderingTag;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")",
            "Lcom/squareup/workflow/ui/ShowRenderingTag<",
            "*>;"
        }
    .end annotation

    const-string v0, "$this$showRenderingTag"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 136
    sget v0, Lcom/squareup/workflow/ui/R$id;->view_show_rendering_function:I

    invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object p0

    instance-of v0, p0, Lcom/squareup/workflow/ui/ShowRenderingTag;

    if-nez v0, :cond_0

    const/4 p0, 0x0

    :cond_0
    check-cast p0, Lcom/squareup/workflow/ui/ShowRenderingTag;

    return-object p0
.end method

.method private static final matches(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 0

    .line 138
    invoke-static {p0, p1}, Lcom/squareup/workflow/ui/CompatibleKt;->compatible(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    return p0
.end method

.method public static final showRendering(Landroid/view/View;Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<RenderingT:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/view/View;",
            "TRenderingT;",
            "Lcom/squareup/workflow/ui/ContainerHints;",
            ")V"
        }
    .end annotation

    const-string v0, "$this$showRendering"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    invoke-static {p0}, Lcom/squareup/workflow/ui/ViewShowRenderingKt;->getShowRenderingTag(Landroid/view/View;)Lcom/squareup/workflow/ui/ShowRenderingTag;

    move-result-object v0

    const-string v1, ". "

    const-string v2, "Expected "

    if-eqz v0, :cond_1

    .line 87
    invoke-virtual {v0}, Lcom/squareup/workflow/ui/ShowRenderingTag;->getShowing()Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3, p1}, Lcom/squareup/workflow/ui/ViewShowRenderingKt;->matches(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 93
    invoke-virtual {v0}, Lcom/squareup/workflow/ui/ShowRenderingTag;->getShowRendering()Lkotlin/jvm/functions/Function2;

    move-result-object v0

    invoke-static {p0, p1, p2, v0}, Lcom/squareup/workflow/ui/ViewShowRenderingKt;->bindShowRendering(Landroid/view/View;Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;Lkotlin/jvm/functions/Function2;)V

    return-void

    .line 88
    :cond_0
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p0, " to be able to show rendering "

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p0, ", but that did not match "

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "previous rendering "

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    invoke-virtual {v0}, Lcom/squareup/workflow/ui/ShowRenderingTag;->getShowing()Ljava/lang/Object;

    move-result-object p0

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "Consider using "

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 90
    const-class p0, Lcom/squareup/workflow/ui/WorkflowViewStub;

    invoke-virtual {p0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, " to display arbitrary types."

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 87
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 96
    :cond_1
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p0, " to have a showRendering function to show "

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "Perhaps it was not built by a "

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 97
    const-class p0, Lcom/squareup/workflow/ui/ViewRegistry;

    invoke-virtual {p0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, ", "

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "or perhaps its "

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98
    const-class p0, Lcom/squareup/workflow/ui/ViewBinding;

    invoke-virtual {p0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, " did not call"

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "View.bindShowRendering."

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 95
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public static synthetic showRenderingTag$annotations(Landroid/view/View;)V
    .locals 0

    return-void
.end method
