.class final Lcom/squareup/workflow/WorkflowViewFactoryViewRegistry$keys$2;
.super Lkotlin/jvm/internal/Lambda;
.source "WorkflowViewFactoryViewRegistry.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/WorkflowViewFactoryViewRegistry;-><init>(Lcom/squareup/workflow/WorkflowViewFactory;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Ljava/util/Set<",
        "+",
        "Lkotlin/reflect/KClass<",
        "*>;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nWorkflowViewFactoryViewRegistry.kt\nKotlin\n*S Kotlin\n*F\n+ 1 WorkflowViewFactoryViewRegistry.kt\ncom/squareup/workflow/WorkflowViewFactoryViewRegistry$keys$2\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,77:1\n1360#2:78\n1429#2,3:79\n1412#2,9:82\n1642#2,2:91\n1421#2:93\n*E\n*S KotlinDebug\n*F\n+ 1 WorkflowViewFactoryViewRegistry.kt\ncom/squareup/workflow/WorkflowViewFactoryViewRegistry$keys$2\n*L\n42#1:78\n42#1,3:79\n43#1,9:82\n43#1,2:91\n43#1:93\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000c\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lkotlin/reflect/KClass;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/workflow/WorkflowViewFactoryViewRegistry;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/WorkflowViewFactoryViewRegistry;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/workflow/WorkflowViewFactoryViewRegistry$keys$2;->this$0:Lcom/squareup/workflow/WorkflowViewFactoryViewRegistry;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 36
    invoke-virtual {p0}, Lcom/squareup/workflow/WorkflowViewFactoryViewRegistry$keys$2;->invoke()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final invoke()Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lkotlin/reflect/KClass<",
            "*>;>;"
        }
    .end annotation

    .line 41
    iget-object v0, p0, Lcom/squareup/workflow/WorkflowViewFactoryViewRegistry$keys$2;->this$0:Lcom/squareup/workflow/WorkflowViewFactoryViewRegistry;

    invoke-static {v0}, Lcom/squareup/workflow/WorkflowViewFactoryViewRegistry;->access$getViewFactory$p(Lcom/squareup/workflow/WorkflowViewFactoryViewRegistry;)Lcom/squareup/workflow/WorkflowViewFactory;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/workflow/WorkflowViewFactory;->getKeys()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 78
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 79
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 80
    check-cast v2, Lcom/squareup/workflow/legacy/Screen$Key;

    .line 42
    iget-object v2, v2, Lcom/squareup/workflow/legacy/Screen$Key;->typeName:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 81
    :cond_0
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 82
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 91
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 90
    check-cast v2, Ljava/lang/String;

    .line 43
    iget-object v3, p0, Lcom/squareup/workflow/WorkflowViewFactoryViewRegistry$keys$2;->this$0:Lcom/squareup/workflow/WorkflowViewFactoryViewRegistry;

    invoke-static {v3, v2}, Lcom/squareup/workflow/WorkflowViewFactoryViewRegistry;->access$kclassOrNull(Lcom/squareup/workflow/WorkflowViewFactoryViewRegistry;Ljava/lang/String;)Lkotlin/reflect/KClass;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 90
    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 93
    :cond_2
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 44
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->toSet(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
