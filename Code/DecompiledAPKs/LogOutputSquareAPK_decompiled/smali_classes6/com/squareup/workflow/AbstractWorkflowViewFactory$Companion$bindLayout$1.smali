.class final Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion$bindLayout$1;
.super Lkotlin/jvm/internal/Lambda;
.source "AbstractWorkflowViewFactory.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout(Lkotlin/reflect/KClass;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Lio/reactivex/Observable<",
        "Lcom/squareup/workflow/legacy/Screen;",
        ">;",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "Lcom/squareup/workflow/LayoutRunnerCoordinator<",
        "TRenderingT;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u00032\u0018\u0010\u0004\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u00020\u00070\u00060\u00052\u0006\u0010\u0008\u001a\u00020\tH\n\u00a2\u0006\u0002\u0008\n"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/LayoutRunnerCoordinator;",
        "RenderingT",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "",
        "hints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $runnerConstructor:Lkotlin/jvm/functions/Function1;


# direct methods
.method constructor <init>(Lkotlin/jvm/functions/Function1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion$bindLayout$1;->$runnerConstructor:Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lio/reactivex/Observable;Lcom/squareup/workflow/ui/ContainerHints;)Lcom/squareup/workflow/LayoutRunnerCoordinator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;",
            "Lcom/squareup/workflow/ui/ContainerHints;",
            ")",
            "Lcom/squareup/workflow/LayoutRunnerCoordinator<",
            "TRenderingT;>;"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "hints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 263
    new-instance v0, Lcom/squareup/workflow/LayoutRunnerCoordinator;

    iget-object v1, p0, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion$bindLayout$1;->$runnerConstructor:Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, p1, v1, p2}, Lcom/squareup/workflow/LayoutRunnerCoordinator;-><init>(Lio/reactivex/Observable;Lkotlin/jvm/functions/Function1;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 241
    check-cast p1, Lio/reactivex/Observable;

    check-cast p2, Lcom/squareup/workflow/ui/ContainerHints;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion$bindLayout$1;->invoke(Lio/reactivex/Observable;Lcom/squareup/workflow/ui/ContainerHints;)Lcom/squareup/workflow/LayoutRunnerCoordinator;

    move-result-object p1

    return-object p1
.end method
