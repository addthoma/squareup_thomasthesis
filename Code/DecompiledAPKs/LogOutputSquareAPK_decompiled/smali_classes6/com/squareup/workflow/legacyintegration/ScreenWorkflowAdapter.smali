.class public final Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "ScreenWorkflowAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<InputT:",
        "Ljava/lang/Object;",
        "OutputT:",
        "Ljava/lang/Object;",
        "RenderingT:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "TInputT;",
        "Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState<",
        "TOutputT;TRenderingT;>;TOutputT;",
        "Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyRendering;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nScreenWorkflowAdapter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ScreenWorkflowAdapter.kt\ncom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter\n*L\n1#1,113:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u0000*\u0004\u0008\u0000\u0010\u0001*\u0008\u0008\u0001\u0010\u0002*\u00020\u0003*\u0008\u0008\u0002\u0010\u0004*\u00020\u000322\u0012\u0004\u0012\u0002H\u0001\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00040\u0006\u0012\u0004\u0012\u0002H\u0002\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u0002H\u00040\u00070\u0005:\u0001\u001aB]\u0012*\u0010\t\u001a&\u0012\u0004\u0012\u00028\u0000\u0012\u001c\u0012\u001a\u0012\n\u0012\u0008\u0012\u0004\u0012\u00028\u00020\u000c\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00028\u00010\u000b0\n\u0012*\u0010\r\u001a&\u0012\u0004\u0012\u00020\u000e\u0012\u001c\u0012\u001a\u0012\n\u0012\u0008\u0012\u0004\u0012\u00028\u00020\u000c\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00028\u00010\u000b0\n\u00a2\u0006\u0002\u0010\u000fJ+\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u00062\u0006\u0010\u0011\u001a\u00028\u00002\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u000eH\u0016\u00a2\u0006\u0002\u0010\u0013JU\u0010\u0014\u001a\u000e\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00028\u00020\u00072\u0006\u0010\u0011\u001a\u00028\u00002\u0012\u0010\u0015\u001a\u000e\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u00062\u001e\u0010\u0016\u001a\u001a\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u0006\u0012\u0004\u0012\u00028\u00010\u0017H\u0016\u00a2\u0006\u0002\u0010\u0018J\u001c\u0010\u0019\u001a\u00020\u000e2\u0012\u0010\u0015\u001a\u000e\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u0006H\u0016R2\u0010\r\u001a&\u0012\u0004\u0012\u00020\u000e\u0012\u001c\u0012\u001a\u0012\n\u0012\u0008\u0012\u0004\u0012\u00028\u00020\u000c\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00028\u00010\u000b0\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R2\u0010\t\u001a&\u0012\u0004\u0012\u00028\u0000\u0012\u001c\u0012\u001a\u0012\n\u0012\u0008\u0012\u0004\u0012\u00028\u00020\u000c\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00028\u00010\u000b0\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;",
        "InputT",
        "OutputT",
        "",
        "RenderingT",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;",
        "Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyRendering;",
        "",
        "start",
        "Lkotlin/Function1;",
        "Lcom/squareup/workflow/rx1/Workflow;",
        "Lcom/squareup/workflow/ScreenState;",
        "restore",
        "Lcom/squareup/workflow/Snapshot;",
        "(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V",
        "initialState",
        "input",
        "snapshot",
        "(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "(Ljava/lang/Object;Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyRendering;",
        "snapshotState",
        "ScreenWorkflowAdapterState",
        "v2-legacy-integration"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final restore:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/workflow/Snapshot;",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "Lcom/squareup/workflow/ScreenState<",
            "TRenderingT;>;*TOutputT;>;>;"
        }
    .end annotation
.end field

.field private final start:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "TInputT;",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "Lcom/squareup/workflow/ScreenState<",
            "TRenderingT;>;*TOutputT;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-TInputT;+",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "+",
            "Lcom/squareup/workflow/ScreenState<",
            "+TRenderingT;>;*+TOutputT;>;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/workflow/Snapshot;",
            "+",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "+",
            "Lcom/squareup/workflow/ScreenState<",
            "+TRenderingT;>;*+TOutputT;>;>;)V"
        }
    .end annotation

    const-string v0, "start"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "restore"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;->start:Lkotlin/jvm/functions/Function1;

    iput-object p2, p0, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;->restore:Lkotlin/jvm/functions/Function1;

    return-void
.end method


# virtual methods
.method public initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TInputT;",
            "Lcom/squareup/workflow/Snapshot;",
            ")",
            "Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState<",
            "TOutputT;TRenderingT;>;"
        }
    .end annotation

    const/4 v0, 0x0

    if-eqz p2, :cond_2

    .line 80
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v1}, Lokio/ByteString;->size()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-nez v1, :cond_1

    goto :goto_1

    :cond_1
    move-object p2, v0

    :goto_1
    if-eqz p2, :cond_2

    .line 81
    iget-object v1, p0, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;->restore:Lkotlin/jvm/functions/Function1;

    invoke-interface {v1, p2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/workflow/rx1/Workflow;

    if-eqz p2, :cond_2

    goto :goto_2

    .line 82
    :cond_2
    iget-object p2, p0, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;->start:Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    move-object p2, p1

    check-cast p2, Lcom/squareup/workflow/rx1/Workflow;

    .line 84
    :goto_2
    new-instance p1, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;

    invoke-direct {p1, p2, v0}, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;-><init>(Lcom/squareup/workflow/rx1/Workflow;Lcom/squareup/workflow/ScreenState;)V

    return-object p1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 42
    invoke-virtual {p0, p1, p2}, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;->initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;

    move-result-object p1

    return-object p1
.end method

.method public render(Ljava/lang/Object;Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyRendering;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TInputT;",
            "Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState<",
            "TOutputT;TRenderingT;>;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState<",
            "TOutputT;TRenderingT;>;-TOutputT;>;)",
            "Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyRendering;"
        }
    .end annotation

    const-string p1, "state"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "context"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 96
    invoke-virtual {p2}, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;->getCancellationWorker()Lcom/squareup/workflow/LifecycleWorker;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/Worker;

    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-static {p3, p1, v0, v1, v0}, Lcom/squareup/workflow/RenderContextKt;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;ILjava/lang/Object;)V

    .line 97
    invoke-virtual {p2}, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;->getChildStateWorker()Lcom/squareup/workflow/Worker;

    move-result-object v3

    new-instance p1, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$render$1;

    invoke-direct {p1, p2}, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$render$1;-><init>(Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;)V

    move-object v5, p1

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x0

    const/4 v6, 0x2

    const/4 v7, 0x0

    move-object v2, p3

    invoke-static/range {v2 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 98
    invoke-virtual {p2}, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;->getResultWorker()Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState$ResultWorker;

    move-result-object p1

    move-object v2, p1

    check-cast v2, Lcom/squareup/workflow/Worker;

    sget-object p1, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$render$2;->INSTANCE:Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$render$2;

    move-object v4, p1

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v3, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x0

    move-object v1, p3

    invoke-static/range {v1 .. v6}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 101
    invoke-virtual {p2}, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;->getLastLegacyState()Lcom/squareup/workflow/ScreenState;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/squareup/workflow/ScreenState;->screen:Ljava/lang/Object;

    .line 102
    :cond_0
    invoke-virtual {p2}, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;->getRunningWorkflow()Lcom/squareup/workflow/rx1/Workflow;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 103
    invoke-virtual {p2}, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;->getLastLegacyState()Lcom/squareup/workflow/ScreenState;

    move-result-object p2

    const/4 p3, 0x1

    if-eqz p2, :cond_2

    iget-object p2, p2, Lcom/squareup/workflow/ScreenState;->snapshot:Lcom/squareup/workflow/Snapshot;

    if-eqz p2, :cond_2

    sget-object v1, Lcom/squareup/workflow/Snapshot;->EMPTY:Lcom/squareup/workflow/Snapshot;

    if-ne p2, v1, :cond_1

    goto :goto_0

    :cond_1
    const/4 p2, 0x0

    const/4 p3, 0x0

    .line 100
    :cond_2
    :goto_0
    new-instance p2, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyRendering;

    invoke-direct {p2, v0, p1, p3}, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyRendering;-><init>(Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;Z)V

    return-object p2
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 42
    check-cast p2, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;->render(Ljava/lang/Object;Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyRendering;

    move-result-object p1

    return-object p1
.end method

.method public snapshotState(Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;)Lcom/squareup/workflow/Snapshot;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState<",
            "TOutputT;TRenderingT;>;)",
            "Lcom/squareup/workflow/Snapshot;"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 110
    invoke-virtual {p1}, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;->getLastLegacyState()Lcom/squareup/workflow/ScreenState;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p1, p1, Lcom/squareup/workflow/ScreenState;->snapshot:Lcom/squareup/workflow/Snapshot;

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/workflow/Snapshot;->EMPTY:Lcom/squareup/workflow/Snapshot;

    :goto_0
    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 42
    check-cast p1, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;

    invoke-virtual {p0, p1}, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;->snapshotState(Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
