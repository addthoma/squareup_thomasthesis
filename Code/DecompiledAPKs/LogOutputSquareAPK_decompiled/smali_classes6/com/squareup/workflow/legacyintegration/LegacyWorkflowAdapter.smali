.class public abstract Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "LegacyWorkflowAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;,
        Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyRendering;,
        Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$StateWorker;,
        Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$WorkflowCompletionWorker;,
        Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<InputT:",
        "Ljava/lang/Object;",
        "StateT:",
        "Ljava/lang/Object;",
        "EventT:",
        "Ljava/lang/Object;",
        "OutputT:",
        "Ljava/lang/Object;",
        "RenderingT:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "TInputT;",
        "Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState<",
        "TStateT;TEventT;TOutputT;>;TOutputT;",
        "Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyRendering<",
        "TEventT;TRenderingT;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLegacyWorkflowAdapter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LegacyWorkflowAdapter.kt\ncom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter\n*L\n1#1,194:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u000c\u0008&\u0018\u0000 \"*\u0008\u0008\u0000\u0010\u0001*\u00020\u0002*\u0008\u0008\u0001\u0010\u0003*\u00020\u0002*\u0008\u0008\u0002\u0010\u0004*\u00020\u0002*\u0008\u0008\u0003\u0010\u0005*\u00020\u0002*\u0008\u0008\u0004\u0010\u0006*\u00020\u000228\u0012\u0004\u0012\u0002H\u0001\u0012\u0016\u0012\u0014\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0008\u0012\u0004\u0012\u0002H\u0005\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00060\t0\u0007:\u0005\"#$%&B\u0005\u00a2\u0006\u0002\u0010\nJ<\u0010\u000b\u001a\u0014\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00028\u00030\u00082\u0006\u0010\u000c\u001a\u00020\r2\u0018\u0010\u000e\u001a\u0014\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00028\u00030\u000fH\u0002J/\u0010\u0010\u001a\u0014\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00028\u00030\u00082\u0006\u0010\u0011\u001a\u00028\u00002\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u0013\u00a2\u0006\u0002\u0010\u0014J_\u0010\u0015\u001a\u000e\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00028\u00040\t2\u0006\u0010\u0011\u001a\u00028\u00002\u0018\u0010\u0016\u001a\u0014\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00028\u00030\u00082$\u0010\u0017\u001a \u0012\u0016\u0012\u0014\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00028\u00030\u0008\u0012\u0004\u0012\u00028\u00030\u0018\u00a2\u0006\u0002\u0010\u0019J+\u0010\u0015\u001a\u00028\u00042\u0006\u0010\u0016\u001a\u00028\u00012\u000c\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00028\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\rH&\u00a2\u0006\u0002\u0010\u001dJ\u0015\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0016\u001a\u00028\u0001H&\u00a2\u0006\u0002\u0010\u001eJ \u0010\u001f\u001a\u00020\u00132\u0018\u0010\u0016\u001a\u0014\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00028\u00030\u0008J9\u0010 \u001a\u0014\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00028\u00030\u000f2\u0006\u0010\u0011\u001a\u00028\u00002\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u00132\u0006\u0010\u001c\u001a\u00020\rH&\u00a2\u0006\u0002\u0010!\u00a8\u0006\'"
    }
    d2 = {
        "Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;",
        "InputT",
        "",
        "StateT",
        "EventT",
        "OutputT",
        "RenderingT",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;",
        "Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyRendering;",
        "()V",
        "createInitialState",
        "pool",
        "Lcom/squareup/workflow/legacy/WorkflowPool;",
        "legacyWorkflow",
        "Lcom/squareup/workflow/legacy/Workflow;",
        "initialState",
        "input",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "(Ljava/lang/Object;Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyRendering;",
        "workflow",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "workflows",
        "(Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/lang/Object;",
        "(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;",
        "snapshotState",
        "startWorkflow",
        "(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;",
        "Companion",
        "LegacyRendering",
        "LegacyWorkflowState",
        "StateWorker",
        "WorkflowCompletionWorker",
        "v2-legacy-integration"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$Companion;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final RESULT_KEY:Ljava/lang/String; = "result"

.field public static final STATE_KEY:Ljava/lang/String; = "state"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;->Companion:Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 67
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    return-void
.end method

.method private final createInitialState(Lcom/squareup/workflow/legacy/WorkflowPool;Lcom/squareup/workflow/legacy/Workflow;)Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            "Lcom/squareup/workflow/legacy/Workflow<",
            "+TStateT;-TEventT;+TOutputT;>;)",
            "Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState<",
            "TStateT;TEventT;TOutputT;>;"
        }
    .end annotation

    .line 180
    invoke-interface {p2}, Lcom/squareup/workflow/legacy/Workflow;->openSubscriptionToState()Lkotlinx/coroutines/channels/ReceiveChannel;

    move-result-object v0

    .line 181
    new-instance v1, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;

    const/4 v2, 0x0

    invoke-direct {v1, p2, v0, v2, p1}, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;-><init>(Lcom/squareup/workflow/legacy/Workflow;Lkotlinx/coroutines/channels/ReceiveChannel;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;)V

    return-object v1
.end method


# virtual methods
.method public final initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TInputT;",
            "Lcom/squareup/workflow/Snapshot;",
            ")",
            "Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState<",
            "TStateT;TEventT;TOutputT;>;"
        }
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 141
    new-instance v0, Lcom/squareup/workflow/legacy/WorkflowPool;

    invoke-direct {v0}, Lcom/squareup/workflow/legacy/WorkflowPool;-><init>()V

    .line 142
    invoke-virtual {p0, p1, p2, v0}, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;->startWorkflow(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    .line 143
    invoke-direct {p0, v0, p1}, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;->createInitialState(Lcom/squareup/workflow/legacy/WorkflowPool;Lcom/squareup/workflow/legacy/Workflow;)Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 61
    invoke-virtual {p0, p1, p2}, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;->initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;

    move-result-object p1

    return-object p1
.end method

.method public final render(Ljava/lang/Object;Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyRendering;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TInputT;",
            "Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState<",
            "TStateT;TEventT;TOutputT;>;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState<",
            "TStateT;TEventT;TOutputT;>;-TOutputT;>;)",
            "Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyRendering<",
            "TEventT;TRenderingT;>;"
        }
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "state"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 153
    new-instance v0, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$StateWorker;

    invoke-virtual {p2}, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;->getStateSubscription()Lkotlinx/coroutines/channels/ReceiveChannel;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$StateWorker;-><init>(Lkotlinx/coroutines/channels/ReceiveChannel;)V

    check-cast v0, Lcom/squareup/workflow/Worker;

    sget-object v1, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$render$1;->INSTANCE:Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$render$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v0, p1, v1}, Lcom/squareup/workflow/RenderContext;->runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    .line 160
    new-instance p1, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$WorkflowCompletionWorker;

    invoke-virtual {p2}, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;->getRunningWorkflow()Lcom/squareup/workflow/legacy/Workflow;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$WorkflowCompletionWorker;-><init>(Lcom/squareup/workflow/legacy/Workflow;)V

    check-cast p1, Lcom/squareup/workflow/Worker;

    .line 161
    sget-object v0, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$render$2;->INSTANCE:Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$render$2;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const-string v1, "result"

    .line 159
    invoke-interface {p3, p1, v1, v0}, Lcom/squareup/workflow/RenderContext;->runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    .line 165
    invoke-virtual {p2}, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;->getLastLegacyState()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 166
    invoke-virtual {p2}, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;->getLastLegacyState()Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {p2}, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;->getRunningWorkflow()Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p3

    check-cast p3, Lcom/squareup/workflow/legacy/WorkflowInput;

    invoke-virtual {p2}, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;->getWorkflowPool()Lcom/squareup/workflow/legacy/WorkflowPool;

    move-result-object v0

    invoke-virtual {p0, p1, p3, v0}, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;->render(Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/lang/Object;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 169
    :goto_0
    invoke-virtual {p0, p2}, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;->snapshotState(Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p3

    sget-object v0, Lcom/squareup/workflow/Snapshot;->EMPTY:Lcom/squareup/workflow/Snapshot;

    if-ne p3, v0, :cond_1

    const/4 p3, 0x1

    goto :goto_1

    :cond_1
    const/4 p3, 0x0

    .line 170
    :goto_1
    new-instance v0, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyRendering;

    invoke-virtual {p2}, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;->getRunningWorkflow()Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p2

    check-cast p2, Lcom/squareup/workflow/legacy/WorkflowInput;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyRendering;-><init>(Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;Z)V

    return-object v0
.end method

.method public abstract render(Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TStateT;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-TEventT;>;",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            ")TRenderingT;"
        }
    .end annotation
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 61
    check-cast p2, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;->render(Ljava/lang/Object;Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyRendering;

    move-result-object p1

    return-object p1
.end method

.method public abstract snapshot(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TStateT;)",
            "Lcom/squareup/workflow/Snapshot;"
        }
    .end annotation
.end method

.method public final snapshotState(Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;)Lcom/squareup/workflow/Snapshot;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState<",
            "TStateT;TEventT;TOutputT;>;)",
            "Lcom/squareup/workflow/Snapshot;"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 174
    invoke-virtual {p1}, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;->getLastLegacyState()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_0

    move-object v0, p0

    check-cast v0, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;->snapshot(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/workflow/Snapshot;->EMPTY:Lcom/squareup/workflow/Snapshot;

    :goto_0
    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 61
    check-cast p1, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;

    invoke-virtual {p0, p1}, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;->snapshotState(Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyWorkflowState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public abstract startWorkflow(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TInputT;",
            "Lcom/squareup/workflow/Snapshot;",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            ")",
            "Lcom/squareup/workflow/legacy/Workflow<",
            "TStateT;TEventT;TOutputT;>;"
        }
    .end annotation
.end method
