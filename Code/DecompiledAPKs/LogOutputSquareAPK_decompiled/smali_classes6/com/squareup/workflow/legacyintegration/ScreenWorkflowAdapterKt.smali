.class public final Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapterKt;
.super Ljava/lang/Object;
.source "ScreenWorkflowAdapter.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000*b\u0010\u0000\u001a\u0004\u0008\u0000\u0010\u0001\u001a\u0004\u0008\u0001\u0010\u0002\"\u001a\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u0002\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u0005`\u00040\u000326\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u0002\u0012&\u0012$\u0012\u0004\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0006j\u0008\u0012\u0004\u0012\u00020\u0005`\u00040\u0003\u00a8\u0006\t"
    }
    d2 = {
        "PosScreenWorkflowAdapter",
        "InputT",
        "OutputT",
        "Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "Lcom/squareup/container/PosLayering;",
        "",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "v2-legacy-integration"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation
