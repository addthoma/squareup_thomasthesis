.class public final Lcom/squareup/workflow/pos/LegacyWorkflowAdaptersKt$asV2Workflow$1;
.super Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;
.source "LegacyWorkflowAdapters.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/pos/LegacyWorkflowAdaptersKt;->asV2Workflow(Lcom/squareup/container/PosWorkflowStarter;)Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter<",
        "Lkotlin/Unit;",
        "Lcom/squareup/workflow/ScreenState<",
        "+",
        "Ljava/util/Map<",
        "+",
        "Lcom/squareup/container/ContainerLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;TEventT;TOutputT;",
        "Lcom/squareup/workflow/ScreenState<",
        "+",
        "Ljava/util/Map<",
        "+",
        "Lcom/squareup/container/ContainerLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000C\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003*\u0001\u0000\u0008\n\u0018\u00002x\u0012\u0004\u0012\u00020\u0002\u00120\u0012.\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0005`\u00080\u0003\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u00120\u0012.\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0005`\u00080\u00030\u0001J~\u0010\t\u001a.\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0005`\u00080\u000322\u0010\n\u001a.\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0005`\u00080\u00032\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u000c2\u0006\u0010\r\u001a\u00020\u000eH\u0016J<\u0010\u000f\u001a\u00020\u001022\u0010\n\u001a.\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0005`\u00080\u0003H\u0016Je\u0010\u0011\u001a@\u00120\u0012.\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0005`\u00080\u0003\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00122\u0006\u0010\u0013\u001a\u00020\u00022\u0008\u0010\u000f\u001a\u0004\u0018\u00010\u00102\u0006\u0010\r\u001a\u00020\u000eH\u0016\u00a2\u0006\u0002\u0010\u0014\u00a8\u0006\u0015"
    }
    d2 = {
        "com/squareup/workflow/pos/LegacyWorkflowAdaptersKt$asV2Workflow$1",
        "Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;",
        "",
        "Lcom/squareup/workflow/ScreenState;",
        "",
        "Lcom/squareup/container/ContainerLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "render",
        "state",
        "workflow",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "workflows",
        "Lcom/squareup/workflow/legacy/WorkflowPool;",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "startWorkflow",
        "Lcom/squareup/workflow/legacy/Workflow;",
        "input",
        "(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;",
        "v2-pos-integration"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $this_asV2Workflow:Lcom/squareup/container/PosWorkflowStarter;


# direct methods
.method constructor <init>(Lcom/squareup/container/PosWorkflowStarter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/container/PosWorkflowStarter<",
            "TEventT;+TOutputT;>;)V"
        }
    .end annotation

    .line 20
    iput-object p1, p0, Lcom/squareup/workflow/pos/LegacyWorkflowAdaptersKt$asV2Workflow$1;->$this_asV2Workflow:Lcom/squareup/container/PosWorkflowStarter;

    invoke-direct {p0}, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public render(Lcom/squareup/workflow/ScreenState;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/ScreenState;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/ScreenState<",
            "+",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "+",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-TEventT;>;",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            ")",
            "Lcom/squareup/workflow/ScreenState<",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "workflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "workflows"

    invoke-static {p3, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/lang/Object;
    .locals 0

    .line 20
    check-cast p1, Lcom/squareup/workflow/ScreenState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/workflow/pos/LegacyWorkflowAdaptersKt$asV2Workflow$1;->render(Lcom/squareup/workflow/ScreenState;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/ScreenState;

    move-result-object p1

    return-object p1
.end method

.method public snapshot(Lcom/squareup/workflow/ScreenState;)Lcom/squareup/workflow/Snapshot;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/ScreenState<",
            "+",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "+",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;)",
            "Lcom/squareup/workflow/Snapshot;"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    iget-object p1, p1, Lcom/squareup/workflow/ScreenState;->snapshot:Lcom/squareup/workflow/Snapshot;

    return-object p1
.end method

.method public bridge synthetic snapshot(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 20
    check-cast p1, Lcom/squareup/workflow/ScreenState;

    invoke-virtual {p0, p1}, Lcom/squareup/workflow/pos/LegacyWorkflowAdaptersKt$asV2Workflow$1;->snapshot(Lcom/squareup/workflow/ScreenState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic startWorkflow(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;
    .locals 0

    .line 20
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/workflow/pos/LegacyWorkflowAdaptersKt$asV2Workflow$1;->startWorkflow(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    return-object p1
.end method

.method public startWorkflow(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Unit;",
            "Lcom/squareup/workflow/Snapshot;",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            ")",
            "Lcom/squareup/workflow/legacy/Workflow<",
            "Lcom/squareup/workflow/ScreenState<",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;TEventT;TOutputT;>;"
        }
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "workflows"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    iget-object p1, p0, Lcom/squareup/workflow/pos/LegacyWorkflowAdaptersKt$asV2Workflow$1;->$this_asV2Workflow:Lcom/squareup/container/PosWorkflowStarter;

    invoke-interface {p1, p2}, Lcom/squareup/container/PosWorkflowStarter;->start(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    return-object p1
.end method
