.class final synthetic Lcom/squareup/workflow/internal/WorkflowNode$subtreeManager$1;
.super Lkotlin/jvm/internal/FunctionReference;
.source "WorkflowNode.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/internal/WorkflowNode;-><init>(Lcom/squareup/workflow/internal/WorkflowId;Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/Object;Lokio/ByteString;Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function1;Ljava/lang/Long;Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;Lcom/squareup/workflow/diagnostic/IdCounter;Ljava/lang/Object;Lkotlin/coroutines/CoroutineContext;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/FunctionReference;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/WorkflowAction<",
        "TStateT;+TOutputT;>;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u0004\u0018\u00010\u0001\"\u0004\u0008\u0000\u0010\u0002\"\u0004\u0008\u0001\u0010\u0003\"\u0008\u0008\u0002\u0010\u0004*\u00020\u0001\"\u0004\u0008\u0003\u0010\u00052!\u0010\u0006\u001a\u001d\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u0007\u00a2\u0006\u000c\u0008\u0008\u0012\u0008\u0008\t\u0012\u0004\u0008\u0008(\n\u00a2\u0006\u0002\u0008\u000b"
    }
    d2 = {
        "<anonymous>",
        "",
        "PropsT",
        "StateT",
        "OutputT",
        "RenderingT",
        "p1",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lkotlin/ParameterName;",
        "name",
        "action",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/squareup/workflow/internal/WorkflowNode;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lkotlin/jvm/internal/FunctionReference;-><init>(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    const-string v0, "applyAction"

    return-object v0
.end method

.method public final getOwner()Lkotlin/reflect/KDeclarationContainer;
    .locals 1

    const-class v0, Lcom/squareup/workflow/internal/WorkflowNode;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    return-object v0
.end method

.method public final getSignature()Ljava/lang/String;
    .locals 1

    const-string v0, "applyAction(Lcom/squareup/workflow/WorkflowAction;)Ljava/lang/Object;"

    return-object v0
.end method

.method public final invoke(Lcom/squareup/workflow/WorkflowAction;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction<",
            "TStateT;+TOutputT;>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/workflow/internal/WorkflowNode$subtreeManager$1;->receiver:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/workflow/internal/WorkflowNode;

    .line 81
    invoke-static {v0, p1}, Lcom/squareup/workflow/internal/WorkflowNode;->access$applyAction(Lcom/squareup/workflow/internal/WorkflowNode;Lcom/squareup/workflow/WorkflowAction;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 53
    check-cast p1, Lcom/squareup/workflow/WorkflowAction;

    invoke-virtual {p0, p1}, Lcom/squareup/workflow/internal/WorkflowNode$subtreeManager$1;->invoke(Lcom/squareup/workflow/WorkflowAction;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
