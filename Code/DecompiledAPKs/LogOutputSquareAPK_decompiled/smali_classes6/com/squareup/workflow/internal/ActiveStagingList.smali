.class public final Lcom/squareup/workflow/internal/ActiveStagingList;
.super Ljava/lang/Object;
.source "ActiveStagingList.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode<",
        "TT;>;>",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nActiveStagingList.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ActiveStagingList.kt\ncom/squareup/workflow/internal/ActiveStagingList\n+ 2 InlineLinkedList.kt\ncom/squareup/workflow/internal/InlineLinkedList\n*L\n1#1,92:1\n76#2,24:93\n106#2,6:117\n106#2,6:123\n106#2,6:129\n*E\n*S KotlinDebug\n*F\n+ 1 ActiveStagingList.kt\ncom/squareup/workflow/internal/ActiveStagingList\n*L\n61#1,24:93\n73#1,6:117\n85#1,6:123\n90#1,6:129\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0000\u0018\u0000*\u000e\u0008\u0000\u0010\u0001*\u0008\u0012\u0004\u0012\u0002H\u00010\u00022\u00020\u0003B\u0005\u00a2\u0006\u0002\u0010\u0004J\u001d\u0010\u0008\u001a\u00020\t2\u0012\u0010\n\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\t0\u000bH\u0086\u0008J\u001d\u0010\u000c\u001a\u00020\t2\u0012\u0010\r\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\t0\u000bH\u0086\u0008J\u001d\u0010\u000e\u001a\u00020\t2\u0012\u0010\r\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\t0\u000bH\u0086\u0008J0\u0010\u000f\u001a\u00028\u00002\u0012\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u00110\u000b2\u000c\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0013H\u0086\u0008\u00a2\u0006\u0002\u0010\u0014R\u0014\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/workflow/internal/ActiveStagingList;",
        "T",
        "Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;",
        "",
        "()V",
        "active",
        "Lcom/squareup/workflow/internal/InlineLinkedList;",
        "staging",
        "commitStaging",
        "",
        "onRemove",
        "Lkotlin/Function1;",
        "forEachActive",
        "block",
        "forEachStaging",
        "retainOrCreate",
        "predicate",
        "",
        "create",
        "Lkotlin/Function0;",
        "(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;",
        "workflow-runtime"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private active:Lcom/squareup/workflow/internal/InlineLinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/internal/InlineLinkedList<",
            "TT;>;"
        }
    .end annotation
.end field

.field private staging:Lcom/squareup/workflow/internal/InlineLinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/internal/InlineLinkedList<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    new-instance v0, Lcom/squareup/workflow/internal/InlineLinkedList;

    invoke-direct {v0}, Lcom/squareup/workflow/internal/InlineLinkedList;-><init>()V

    iput-object v0, p0, Lcom/squareup/workflow/internal/ActiveStagingList;->active:Lcom/squareup/workflow/internal/InlineLinkedList;

    .line 51
    new-instance v0, Lcom/squareup/workflow/internal/InlineLinkedList;

    invoke-direct {v0}, Lcom/squareup/workflow/internal/InlineLinkedList;-><init>()V

    iput-object v0, p0, Lcom/squareup/workflow/internal/ActiveStagingList;->staging:Lcom/squareup/workflow/internal/InlineLinkedList;

    return-void
.end method

.method public static final synthetic access$getActive$p(Lcom/squareup/workflow/internal/ActiveStagingList;)Lcom/squareup/workflow/internal/InlineLinkedList;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/squareup/workflow/internal/ActiveStagingList;->active:Lcom/squareup/workflow/internal/InlineLinkedList;

    return-object p0
.end method

.method public static final synthetic access$getStaging$p(Lcom/squareup/workflow/internal/ActiveStagingList;)Lcom/squareup/workflow/internal/InlineLinkedList;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/squareup/workflow/internal/ActiveStagingList;->staging:Lcom/squareup/workflow/internal/InlineLinkedList;

    return-object p0
.end method

.method public static final synthetic access$setActive$p(Lcom/squareup/workflow/internal/ActiveStagingList;Lcom/squareup/workflow/internal/InlineLinkedList;)V
    .locals 0

    .line 32
    iput-object p1, p0, Lcom/squareup/workflow/internal/ActiveStagingList;->active:Lcom/squareup/workflow/internal/InlineLinkedList;

    return-void
.end method

.method public static final synthetic access$setStaging$p(Lcom/squareup/workflow/internal/ActiveStagingList;Lcom/squareup/workflow/internal/InlineLinkedList;)V
    .locals 0

    .line 32
    iput-object p1, p0, Lcom/squareup/workflow/internal/ActiveStagingList;->staging:Lcom/squareup/workflow/internal/InlineLinkedList;

    return-void
.end method


# virtual methods
.method public final commitStaging(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "onRemove"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    invoke-static {p0}, Lcom/squareup/workflow/internal/ActiveStagingList;->access$getActive$p(Lcom/squareup/workflow/internal/ActiveStagingList;)Lcom/squareup/workflow/internal/InlineLinkedList;

    move-result-object v0

    .line 117
    invoke-virtual {v0}, Lcom/squareup/workflow/internal/InlineLinkedList;->getHead()Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_0

    .line 119
    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    invoke-interface {v0}, Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;->getNextListNode()Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;

    move-result-object v0

    goto :goto_0

    .line 76
    :cond_0
    invoke-static {p0}, Lcom/squareup/workflow/internal/ActiveStagingList;->access$getActive$p(Lcom/squareup/workflow/internal/ActiveStagingList;)Lcom/squareup/workflow/internal/InlineLinkedList;

    move-result-object p1

    .line 77
    invoke-static {p0}, Lcom/squareup/workflow/internal/ActiveStagingList;->access$getStaging$p(Lcom/squareup/workflow/internal/ActiveStagingList;)Lcom/squareup/workflow/internal/InlineLinkedList;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/squareup/workflow/internal/ActiveStagingList;->access$setActive$p(Lcom/squareup/workflow/internal/ActiveStagingList;Lcom/squareup/workflow/internal/InlineLinkedList;)V

    .line 78
    invoke-static {p0, p1}, Lcom/squareup/workflow/internal/ActiveStagingList;->access$setStaging$p(Lcom/squareup/workflow/internal/ActiveStagingList;Lcom/squareup/workflow/internal/InlineLinkedList;)V

    .line 79
    invoke-static {p0}, Lcom/squareup/workflow/internal/ActiveStagingList;->access$getStaging$p(Lcom/squareup/workflow/internal/ActiveStagingList;)Lcom/squareup/workflow/internal/InlineLinkedList;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/workflow/internal/InlineLinkedList;->clear()V

    return-void
.end method

.method public final forEachActive(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    invoke-static {p0}, Lcom/squareup/workflow/internal/ActiveStagingList;->access$getActive$p(Lcom/squareup/workflow/internal/ActiveStagingList;)Lcom/squareup/workflow/internal/InlineLinkedList;

    move-result-object v0

    .line 123
    invoke-virtual {v0}, Lcom/squareup/workflow/internal/InlineLinkedList;->getHead()Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_0

    .line 125
    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    invoke-interface {v0}, Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;->getNextListNode()Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;

    move-result-object v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final forEachStaging(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    invoke-static {p0}, Lcom/squareup/workflow/internal/ActiveStagingList;->access$getStaging$p(Lcom/squareup/workflow/internal/ActiveStagingList;)Lcom/squareup/workflow/internal/InlineLinkedList;

    move-result-object v0

    .line 129
    invoke-virtual {v0}, Lcom/squareup/workflow/internal/InlineLinkedList;->getHead()Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_0

    .line 131
    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    invoke-interface {v0}, Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;->getNextListNode()Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;

    move-result-object v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final retainOrCreate(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;",
            "Ljava/lang/Boolean;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "+TT;>;)TT;"
        }
    .end annotation

    const-string v0, "predicate"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "create"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    invoke-static {p0}, Lcom/squareup/workflow/internal/ActiveStagingList;->access$getActive$p(Lcom/squareup/workflow/internal/ActiveStagingList;)Lcom/squareup/workflow/internal/InlineLinkedList;

    move-result-object v0

    .line 93
    invoke-virtual {v0}, Lcom/squareup/workflow/internal/InlineLinkedList;->getHead()Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;

    move-result-object v1

    const/4 v2, 0x0

    .line 94
    move-object v3, v2

    check-cast v3, Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;

    move-object v4, v3

    :goto_0
    if-eqz v1, :cond_3

    .line 97
    invoke-interface {p1, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_2

    if-nez v4, :cond_0

    .line 101
    invoke-interface {v1}, Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;->getNextListNode()Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/workflow/internal/InlineLinkedList;->setHead(Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;)V

    goto :goto_1

    .line 103
    :cond_0
    invoke-interface {v1}, Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;->getNextListNode()Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;

    move-result-object p1

    invoke-interface {v4, p1}, Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;->setNextListNode(Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;)V

    .line 105
    :goto_1
    invoke-virtual {v0}, Lcom/squareup/workflow/internal/InlineLinkedList;->getTail()Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;

    move-result-object p1

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 106
    invoke-virtual {v0, v4}, Lcom/squareup/workflow/internal/InlineLinkedList;->setTail(Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;)V

    .line 108
    :cond_1
    invoke-interface {v1, v3}, Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;->setNextListNode(Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;)V

    goto :goto_2

    .line 113
    :cond_2
    invoke-interface {v1}, Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;->getNextListNode()Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;

    move-result-object v4

    move-object v6, v4

    move-object v4, v1

    move-object v1, v6

    goto :goto_0

    :cond_3
    move-object v1, v2

    :goto_2
    if-eqz v1, :cond_4

    goto :goto_3

    .line 61
    :cond_4
    invoke-interface {p2}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object p1

    move-object v1, p1

    check-cast v1, Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;

    .line 62
    :goto_3
    invoke-static {p0}, Lcom/squareup/workflow/internal/ActiveStagingList;->access$getStaging$p(Lcom/squareup/workflow/internal/ActiveStagingList;)Lcom/squareup/workflow/internal/InlineLinkedList;

    move-result-object p1

    invoke-virtual {p1, v1}, Lcom/squareup/workflow/internal/InlineLinkedList;->plusAssign(Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;)V

    return-object v1
.end method
