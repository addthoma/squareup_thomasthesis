.class public final Lcom/squareup/workflow/internal/WorkersKt$wireUpDebugger$1$invokeSuspend$$inlined$collect$1;
.super Ljava/lang/Object;
.source "Collect.kt"

# interfaces
.implements Lkotlinx/coroutines/flow/FlowCollector;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/internal/WorkersKt$wireUpDebugger$1;->invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lkotlinx/coroutines/flow/FlowCollector<",
        "TT;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCollect.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Collect.kt\nkotlinx/coroutines/flow/FlowKt__CollectKt$collect$3\n+ 2 Workers.kt\ncom/squareup/workflow/internal/WorkersKt$wireUpDebugger$1\n*L\n1#1,136:1\n93#2,3:137\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003*\u0001\u0000\u0008\n\u0018\u00002\u0008\u0012\u0004\u0012\u00028\u00000\u0001J\u0019\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00028\u0000H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0005\u0082\u0002\u0004\n\u0002\u0008\u0019\u00a8\u0006\u0006\u00b8\u0006\u0000"
    }
    d2 = {
        "kotlinx/coroutines/flow/FlowKt__CollectKt$collect$3",
        "Lkotlinx/coroutines/flow/FlowCollector;",
        "emit",
        "",
        "value",
        "(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;",
        "kotlinx-coroutines-core"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $this_flow$inlined:Lkotlinx/coroutines/flow/FlowCollector;

.field final synthetic this$0:Lcom/squareup/workflow/internal/WorkersKt$wireUpDebugger$1;


# direct methods
.method public constructor <init>(Lcom/squareup/workflow/internal/WorkersKt$wireUpDebugger$1;Lkotlinx/coroutines/flow/FlowCollector;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/workflow/internal/WorkersKt$wireUpDebugger$1$invokeSuspend$$inlined$collect$1;->this$0:Lcom/squareup/workflow/internal/WorkersKt$wireUpDebugger$1;

    iput-object p2, p0, Lcom/squareup/workflow/internal/WorkersKt$wireUpDebugger$1$invokeSuspend$$inlined$collect$1;->$this_flow$inlined:Lkotlinx/coroutines/flow/FlowCollector;

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public emit(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 7

    .line 137
    iget-object v0, p0, Lcom/squareup/workflow/internal/WorkersKt$wireUpDebugger$1$invokeSuspend$$inlined$collect$1;->this$0:Lcom/squareup/workflow/internal/WorkersKt$wireUpDebugger$1;

    iget-object v1, v0, Lcom/squareup/workflow/internal/WorkersKt$wireUpDebugger$1;->$diagnosticListener:Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    iget-object v0, p0, Lcom/squareup/workflow/internal/WorkersKt$wireUpDebugger$1$invokeSuspend$$inlined$collect$1;->this$0:Lcom/squareup/workflow/internal/WorkersKt$wireUpDebugger$1;

    iget-wide v2, v0, Lcom/squareup/workflow/internal/WorkersKt$wireUpDebugger$1;->$workerDiagnosticId:J

    iget-object v0, p0, Lcom/squareup/workflow/internal/WorkersKt$wireUpDebugger$1$invokeSuspend$$inlined$collect$1;->this$0:Lcom/squareup/workflow/internal/WorkersKt$wireUpDebugger$1;

    iget-wide v4, v0, Lcom/squareup/workflow/internal/WorkersKt$wireUpDebugger$1;->$workflowDiagnosticId:J

    if-nez p1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    move-object v6, p1

    invoke-interface/range {v1 .. v6}, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;->onWorkerOutput(JJLjava/lang/Object;)V

    .line 138
    iget-object v0, p0, Lcom/squareup/workflow/internal/WorkersKt$wireUpDebugger$1$invokeSuspend$$inlined$collect$1;->$this_flow$inlined:Lkotlinx/coroutines/flow/FlowCollector;

    invoke-interface {v0, p1, p2}, Lkotlinx/coroutines/flow/FlowCollector;->emit(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object p1

    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    move-result-object p2

    if-ne p1, p2, :cond_1

    return-object p1

    .line 139
    :cond_1
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method
