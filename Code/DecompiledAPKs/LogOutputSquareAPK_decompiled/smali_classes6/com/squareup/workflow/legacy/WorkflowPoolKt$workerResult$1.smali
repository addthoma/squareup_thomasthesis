.class final Lcom/squareup/workflow/legacy/WorkflowPoolKt$workerResult$1;
.super Lkotlin/coroutines/jvm/internal/SuspendLambda;
.source "WorkflowPool.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/legacy/WorkflowPoolKt;->workerResult(Lcom/squareup/workflow/legacy/WorkflowPool;Lcom/squareup/workflow/legacy/Worker;Ljava/lang/Object;Ljava/lang/String;Lcom/squareup/workflow/legacy/WorkflowPool$Type;)Lkotlinx/coroutines/Deferred;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/coroutines/jvm/internal/SuspendLambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Lkotlinx/coroutines/CoroutineScope;",
        "Lkotlin/coroutines/Continuation<",
        "-TO;>;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0002\u0008\u0003\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u0002H\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0001*\u00020\u0003*\u00020\u0004H\u008a@\u00a2\u0006\u0004\u0008\u0005\u0010\u0006"
    }
    d2 = {
        "<anonymous>",
        "O",
        "I",
        "",
        "Lkotlinx/coroutines/CoroutineScope;",
        "invoke",
        "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation

.annotation runtime Lkotlin/coroutines/jvm/internal/DebugMetadata;
    c = "com.squareup.workflow.legacy.WorkflowPoolKt$workerResult$1"
    f = "WorkflowPool.kt"
    i = {
        0x0
    }
    l = {
        0x189
    }
    m = "invokeSuspend"
    n = {
        "$this$async"
    }
    s = {
        "L$0"
    }
.end annotation


# instance fields
.field final synthetic $input:Ljava/lang/Object;

.field final synthetic $name:Ljava/lang/String;

.field final synthetic $this_workerResult:Lcom/squareup/workflow/legacy/WorkflowPool;

.field final synthetic $type:Lcom/squareup/workflow/legacy/WorkflowPool$Type;

.field final synthetic $worker:Lcom/squareup/workflow/legacy/Worker;

.field L$0:Ljava/lang/Object;

.field label:I

.field private p$:Lkotlinx/coroutines/CoroutineScope;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/legacy/WorkflowPool;Lcom/squareup/workflow/legacy/Worker;Ljava/lang/Object;Ljava/lang/String;Lcom/squareup/workflow/legacy/WorkflowPool$Type;Lkotlin/coroutines/Continuation;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/workflow/legacy/WorkflowPoolKt$workerResult$1;->$this_workerResult:Lcom/squareup/workflow/legacy/WorkflowPool;

    iput-object p2, p0, Lcom/squareup/workflow/legacy/WorkflowPoolKt$workerResult$1;->$worker:Lcom/squareup/workflow/legacy/Worker;

    iput-object p3, p0, Lcom/squareup/workflow/legacy/WorkflowPoolKt$workerResult$1;->$input:Ljava/lang/Object;

    iput-object p4, p0, Lcom/squareup/workflow/legacy/WorkflowPoolKt$workerResult$1;->$name:Ljava/lang/String;

    iput-object p5, p0, Lcom/squareup/workflow/legacy/WorkflowPoolKt$workerResult$1;->$type:Lcom/squareup/workflow/legacy/WorkflowPool$Type;

    const/4 p1, 0x2

    invoke-direct {p0, p1, p6}, Lkotlin/coroutines/jvm/internal/SuspendLambda;-><init>(ILkotlin/coroutines/Continuation;)V

    return-void
.end method


# virtual methods
.method public final create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lkotlin/coroutines/Continuation<",
            "*>;)",
            "Lkotlin/coroutines/Continuation<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "completion"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/workflow/legacy/WorkflowPoolKt$workerResult$1;

    iget-object v2, p0, Lcom/squareup/workflow/legacy/WorkflowPoolKt$workerResult$1;->$this_workerResult:Lcom/squareup/workflow/legacy/WorkflowPool;

    iget-object v3, p0, Lcom/squareup/workflow/legacy/WorkflowPoolKt$workerResult$1;->$worker:Lcom/squareup/workflow/legacy/Worker;

    iget-object v4, p0, Lcom/squareup/workflow/legacy/WorkflowPoolKt$workerResult$1;->$input:Ljava/lang/Object;

    iget-object v5, p0, Lcom/squareup/workflow/legacy/WorkflowPoolKt$workerResult$1;->$name:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/workflow/legacy/WorkflowPoolKt$workerResult$1;->$type:Lcom/squareup/workflow/legacy/WorkflowPool$Type;

    move-object v1, v0

    move-object v7, p2

    invoke-direct/range {v1 .. v7}, Lcom/squareup/workflow/legacy/WorkflowPoolKt$workerResult$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool;Lcom/squareup/workflow/legacy/Worker;Ljava/lang/Object;Ljava/lang/String;Lcom/squareup/workflow/legacy/WorkflowPool$Type;Lkotlin/coroutines/Continuation;)V

    check-cast p1, Lkotlinx/coroutines/CoroutineScope;

    iput-object p1, v0, Lcom/squareup/workflow/legacy/WorkflowPoolKt$workerResult$1;->p$:Lkotlinx/coroutines/CoroutineScope;

    return-object v0
.end method

.method public final invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p2, Lkotlin/coroutines/Continuation;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/workflow/legacy/WorkflowPoolKt$workerResult$1;->create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/WorkflowPoolKt$workerResult$1;

    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, p2}, Lcom/squareup/workflow/legacy/WorkflowPoolKt$workerResult$1;->invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 9

    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    move-result-object v0

    .line 393
    iget v1, p0, Lcom/squareup/workflow/legacy/WorkflowPoolKt$workerResult$1;->label:I

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    if-ne v1, v2, :cond_0

    iget-object v0, p0, Lcom/squareup/workflow/legacy/WorkflowPoolKt$workerResult$1;->L$0:Ljava/lang/Object;

    check-cast v0, Lkotlinx/coroutines/CoroutineScope;

    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    iget-object p1, p0, Lcom/squareup/workflow/legacy/WorkflowPoolKt$workerResult$1;->p$:Lkotlinx/coroutines/CoroutineScope;

    iget-object v3, p0, Lcom/squareup/workflow/legacy/WorkflowPoolKt$workerResult$1;->$this_workerResult:Lcom/squareup/workflow/legacy/WorkflowPool;

    iget-object v4, p0, Lcom/squareup/workflow/legacy/WorkflowPoolKt$workerResult$1;->$worker:Lcom/squareup/workflow/legacy/Worker;

    iget-object v5, p0, Lcom/squareup/workflow/legacy/WorkflowPoolKt$workerResult$1;->$input:Ljava/lang/Object;

    iget-object v6, p0, Lcom/squareup/workflow/legacy/WorkflowPoolKt$workerResult$1;->$name:Ljava/lang/String;

    iget-object v7, p0, Lcom/squareup/workflow/legacy/WorkflowPoolKt$workerResult$1;->$type:Lcom/squareup/workflow/legacy/WorkflowPool$Type;

    iput-object p1, p0, Lcom/squareup/workflow/legacy/WorkflowPoolKt$workerResult$1;->L$0:Ljava/lang/Object;

    iput v2, p0, Lcom/squareup/workflow/legacy/WorkflowPoolKt$workerResult$1;->label:I

    move-object v8, p0

    invoke-virtual/range {v3 .. v8}, Lcom/squareup/workflow/legacy/WorkflowPool;->awaitWorkerResult(Lcom/squareup/workflow/legacy/Worker;Ljava/lang/Object;Ljava/lang/String;Lcom/squareup/workflow/legacy/WorkflowPool$Type;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object p1

    if-ne p1, v0, :cond_2

    return-object v0

    :cond_2
    :goto_0
    return-object p1
.end method
