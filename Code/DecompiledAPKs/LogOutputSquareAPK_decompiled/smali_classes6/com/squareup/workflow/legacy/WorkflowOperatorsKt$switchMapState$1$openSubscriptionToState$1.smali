.class final Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1;
.super Lkotlin/coroutines/jvm/internal/SuspendLambda;
.source "WorkflowOperators.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1;->openSubscriptionToState()Lkotlinx/coroutines/channels/ReceiveChannel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/coroutines/jvm/internal/SuspendLambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Lkotlinx/coroutines/channels/ProducerScope<",
        "-TS2;>;",
        "Lkotlin/coroutines/Continuation<",
        "-",
        "Lkotlin/Unit;",
        ">;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nWorkflowOperators.kt\nKotlin\n*S Kotlin\n*F\n+ 1 WorkflowOperators.kt\ncom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1\n+ 2 Channels.common.kt\nkotlinx/coroutines/channels/ChannelsKt__Channels_commonKt\n*L\n1#1,152:1\n179#2:153\n158#2,3:154\n180#2,2:157\n165#2:159\n161#2,3:160\n*E\n*S KotlinDebug\n*F\n+ 1 WorkflowOperators.kt\ncom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1\n*L\n91#1:153\n91#1,3:154\n91#1,2:157\n91#1:159\n91#1,3:160\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0004*\u00020\u0003\"\u0008\u0008\u0002\u0010\u0005*\u00020\u0003\"\u0008\u0008\u0003\u0010\u0006*\u00020\u0003*\u0008\u0012\u0004\u0012\u0002H\u00040\u0007H\u008a@\u00a2\u0006\u0004\u0008\u0008\u0010\t"
    }
    d2 = {
        "<anonymous>",
        "",
        "S1",
        "",
        "S2",
        "E",
        "O",
        "Lkotlinx/coroutines/channels/ProducerScope;",
        "invoke",
        "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation

.annotation runtime Lkotlin/coroutines/jvm/internal/DebugMetadata;
    c = "com.squareup.workflow.legacy.WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1"
    f = "WorkflowOperators.kt"
    i = {
        0x0,
        0x0,
        0x0,
        0x0,
        0x0,
        0x0,
        0x0,
        0x0
    }
    l = {
        0x9d
    }
    m = "invokeSuspend"
    n = {
        "$this$produce",
        "upstreamChannel",
        "downstreamChannel",
        "transformerJob",
        "$this$consumeEach$iv",
        "$this$consume$iv$iv",
        "cause$iv$iv",
        "$this$consume$iv"
    }
    s = {
        "L$0",
        "L$1",
        "L$2",
        "L$3",
        "L$4",
        "L$6",
        "L$7",
        "L$8"
    }
.end annotation


# instance fields
.field L$0:Ljava/lang/Object;

.field L$1:Ljava/lang/Object;

.field L$2:Ljava/lang/Object;

.field L$3:Ljava/lang/Object;

.field L$4:Ljava/lang/Object;

.field L$5:Ljava/lang/Object;

.field L$6:Ljava/lang/Object;

.field L$7:Ljava/lang/Object;

.field L$8:Ljava/lang/Object;

.field L$9:Ljava/lang/Object;

.field label:I

.field private p$:Lkotlinx/coroutines/channels/ProducerScope;

.field final synthetic this$0:Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1;Lkotlin/coroutines/Continuation;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1;->this$0:Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1;

    const/4 p1, 0x2

    invoke-direct {p0, p1, p2}, Lkotlin/coroutines/jvm/internal/SuspendLambda;-><init>(ILkotlin/coroutines/Continuation;)V

    return-void
.end method


# virtual methods
.method public final create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lkotlin/coroutines/Continuation<",
            "*>;)",
            "Lkotlin/coroutines/Continuation<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "completion"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1;

    iget-object v1, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1;->this$0:Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1;

    invoke-direct {v0, v1, p2}, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1;Lkotlin/coroutines/Continuation;)V

    check-cast p1, Lkotlinx/coroutines/channels/ProducerScope;

    iput-object p1, v0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1;->p$:Lkotlinx/coroutines/channels/ProducerScope;

    return-object v0
.end method

.method public final invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p2, Lkotlin/coroutines/Continuation;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1;->create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1;

    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, p2}, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1;->invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 26

    move-object/from16 v1, p0

    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    move-result-object v0

    .line 86
    iget v2, v1, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1;->label:I

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz v2, :cond_1

    if-ne v2, v3, :cond_0

    iget-object v2, v1, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1;->L$9:Ljava/lang/Object;

    check-cast v2, Lkotlinx/coroutines/channels/ChannelIterator;

    iget-object v5, v1, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1;->L$8:Ljava/lang/Object;

    check-cast v5, Lkotlinx/coroutines/channels/ReceiveChannel;

    iget-object v6, v1, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1;->L$7:Ljava/lang/Object;

    check-cast v6, Ljava/lang/Throwable;

    iget-object v7, v1, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1;->L$6:Ljava/lang/Object;

    check-cast v7, Lkotlinx/coroutines/channels/ReceiveChannel;

    iget-object v8, v1, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1;->L$5:Ljava/lang/Object;

    check-cast v8, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1;

    iget-object v9, v1, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1;->L$4:Ljava/lang/Object;

    check-cast v9, Lkotlinx/coroutines/channels/ReceiveChannel;

    iget-object v10, v1, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1;->L$3:Ljava/lang/Object;

    check-cast v10, Lkotlin/jvm/internal/Ref$ObjectRef;

    iget-object v11, v1, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1;->L$2:Ljava/lang/Object;

    check-cast v11, Lkotlinx/coroutines/channels/SendChannel;

    iget-object v12, v1, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1;->L$1:Ljava/lang/Object;

    check-cast v12, Lkotlinx/coroutines/channels/ReceiveChannel;

    iget-object v13, v1, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1;->L$0:Ljava/lang/Object;

    check-cast v13, Lkotlinx/coroutines/channels/ProducerScope;

    :try_start_0
    invoke-static/range {p1 .. p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object/from16 v15, p1

    move-object/from16 v19, v0

    move-object/from16 v16, v1

    move-object/from16 v22, v5

    move-object/from16 v21, v8

    move-object/from16 v20, v9

    move-object v0, v10

    move-object/from16 v17, v11

    move-object/from16 v18, v12

    move-object v5, v13

    goto/16 :goto_1

    .line 116
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 86
    :cond_1
    invoke-static/range {p1 .. p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    iget-object v2, v1, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1;->p$:Lkotlinx/coroutines/channels/ProducerScope;

    .line 87
    iget-object v5, v1, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1;->this$0:Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1;

    iget-object v5, v5, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1;->$this_switchMapState:Lcom/squareup/workflow/legacy/Workflow;

    invoke-interface {v5}, Lcom/squareup/workflow/legacy/Workflow;->openSubscriptionToState()Lkotlinx/coroutines/channels/ReceiveChannel;

    move-result-object v7

    .line 88
    invoke-interface {v2}, Lkotlinx/coroutines/channels/ProducerScope;->getChannel()Lkotlinx/coroutines/channels/SendChannel;

    move-result-object v5

    .line 89
    new-instance v6, Lkotlin/jvm/internal/Ref$ObjectRef;

    invoke-direct {v6}, Lkotlin/jvm/internal/Ref$ObjectRef;-><init>()V

    move-object v8, v4

    check-cast v8, Lkotlinx/coroutines/Job;

    iput-object v8, v6, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 154
    move-object v8, v4

    check-cast v8, Ljava/lang/Throwable;

    .line 157
    :try_start_1
    invoke-interface {v7}, Lkotlinx/coroutines/channels/ReceiveChannel;->iterator()Lkotlinx/coroutines/channels/ChannelIterator;

    move-result-object v9

    move-object v14, v0

    move-object v0, v1

    move-object v13, v2

    move-object v11, v5

    move-object v10, v6

    move-object v12, v7

    move-object v6, v8

    move-object v2, v9

    move-object v5, v0

    move-object v8, v12

    move-object v9, v8

    :goto_0
    iput-object v13, v5, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1;->L$0:Ljava/lang/Object;

    iput-object v12, v5, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1;->L$1:Ljava/lang/Object;

    iput-object v11, v5, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1;->L$2:Ljava/lang/Object;

    iput-object v10, v5, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1;->L$3:Ljava/lang/Object;

    iput-object v9, v5, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1;->L$4:Ljava/lang/Object;

    iput-object v0, v5, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1;->L$5:Ljava/lang/Object;

    iput-object v7, v5, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1;->L$6:Ljava/lang/Object;

    iput-object v6, v5, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1;->L$7:Ljava/lang/Object;

    iput-object v8, v5, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1;->L$8:Ljava/lang/Object;

    iput-object v2, v5, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1;->L$9:Ljava/lang/Object;

    iput v3, v5, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1;->label:I

    invoke-interface {v2, v0}, Lkotlinx/coroutines/channels/ChannelIterator;->hasNext(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object v15

    if-ne v15, v14, :cond_2

    return-object v14

    :cond_2
    move-object/from16 v21, v0

    move-object/from16 v16, v5

    move-object/from16 v22, v8

    move-object/from16 v20, v9

    move-object v0, v10

    move-object/from16 v17, v11

    move-object/from16 v18, v12

    move-object v5, v13

    move-object/from16 v19, v14

    .line 86
    :goto_1
    check-cast v15, Ljava/lang/Boolean;

    invoke-virtual {v15}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-interface {v2}, Lkotlinx/coroutines/channels/ChannelIterator;->next()Ljava/lang/Object;

    move-result-object v9

    .line 94
    iget-object v8, v0, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    check-cast v8, Lkotlinx/coroutines/Job;

    if-eqz v8, :cond_3

    invoke-static {v8, v4, v3, v4}, Lkotlinx/coroutines/Job$DefaultImpls;->cancel$default(Lkotlinx/coroutines/Job;Ljava/util/concurrent/CancellationException;ILjava/lang/Object;)V

    .line 99
    :cond_3
    move-object v15, v5

    check-cast v15, Lkotlinx/coroutines/CoroutineScope;

    const/16 v23, 0x0

    const/16 v24, 0x0

    new-instance v25, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1$invokeSuspend$$inlined$consumeEach$lambda$1;

    const/4 v10, 0x0

    move-object/from16 v8, v25

    move-object/from16 v11, v16

    move-object v12, v5

    move-object v13, v0

    move-object/from16 v14, v17

    invoke-direct/range {v8 .. v14}, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1$invokeSuspend$$inlined$consumeEach$lambda$1;-><init>(Ljava/lang/Object;Lkotlin/coroutines/Continuation;Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1;Lkotlinx/coroutines/channels/ProducerScope;Lkotlin/jvm/internal/Ref$ObjectRef;Lkotlinx/coroutines/channels/SendChannel;)V

    move-object/from16 v13, v25

    check-cast v13, Lkotlin/jvm/functions/Function2;

    const/4 v14, 0x3

    const/4 v8, 0x0

    move-object v10, v15

    move-object/from16 v11, v23

    move-object/from16 v12, v24

    move-object v15, v8

    invoke-static/range {v10 .. v15}, Lkotlinx/coroutines/BuildersKt;->launch$default(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    move-result-object v8

    iput-object v8, v0, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    move-object v10, v0

    move-object v13, v5

    move-object/from16 v5, v16

    move-object/from16 v11, v17

    move-object/from16 v12, v18

    move-object/from16 v14, v19

    move-object/from16 v9, v20

    move-object/from16 v0, v21

    move-object/from16 v8, v22

    goto :goto_0

    .line 158
    :cond_4
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 159
    invoke-static {v7, v6}, Lkotlinx/coroutines/channels/ChannelsKt;->cancelConsumed(Lkotlinx/coroutines/channels/ReceiveChannel;Ljava/lang/Throwable;)V

    .line 116
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0

    :catchall_0
    move-exception v0

    move-object v2, v0

    .line 162
    :try_start_2
    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    move-exception v0

    move-object v3, v0

    .line 159
    invoke-static {v7, v2}, Lkotlinx/coroutines/channels/ChannelsKt;->cancelConsumed(Lkotlinx/coroutines/channels/ReceiveChannel;Ljava/lang/Throwable;)V

    throw v3
.end method
