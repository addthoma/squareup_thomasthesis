.class public interface abstract Lcom/squareup/workflow/legacy/Workflow;
.super Ljava/lang/Object;
.source "Workflow.kt"

# interfaces
.implements Lkotlinx/coroutines/Deferred;
.implements Lcom/squareup/workflow/legacy/WorkflowInput;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/workflow/legacy/Workflow$DefaultImpls;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<S:",
        "Ljava/lang/Object;",
        "E:",
        "Ljava/lang/Object;",
        "O:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lkotlinx/coroutines/Deferred<",
        "TO;>;",
        "Lcom/squareup/workflow/legacy/WorkflowInput<",
        "TE;>;"
    }
.end annotation

.annotation runtime Lkotlin/Deprecated;
    message = "Use com.squareup.workflow.Workflow"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008g\u0018\u0000*\n\u0008\u0000\u0010\u0001 \u0001*\u00020\u0002*\n\u0008\u0001\u0010\u0003 \u0000*\u00020\u0002*\n\u0008\u0002\u0010\u0004 \u0001*\u00020\u00022\u0008\u0012\u0004\u0012\u0002H\u00040\u00052\u0008\u0012\u0004\u0012\u0002H\u00030\u0006J\u000e\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0008H&\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/workflow/legacy/Workflow;",
        "S",
        "",
        "E",
        "O",
        "Lkotlinx/coroutines/Deferred;",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "openSubscriptionToState",
        "Lkotlinx/coroutines/channels/ReceiveChannel;",
        "legacy-workflow-core"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract openSubscriptionToState()Lkotlinx/coroutines/channels/ReceiveChannel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlinx/coroutines/channels/ReceiveChannel<",
            "TS;>;"
        }
    .end annotation
.end method
