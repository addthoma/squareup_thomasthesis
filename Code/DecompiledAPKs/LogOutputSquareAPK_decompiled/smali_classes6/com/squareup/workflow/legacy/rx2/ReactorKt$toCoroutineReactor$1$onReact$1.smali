.class final Lcom/squareup/workflow/legacy/rx2/ReactorKt$toCoroutineReactor$1$onReact$1;
.super Lkotlin/coroutines/jvm/internal/ContinuationImpl;
.source "Reactor.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/legacy/rx2/ReactorKt$toCoroutineReactor$1;->onReact(Ljava/lang/Object;Lkotlinx/coroutines/channels/ReceiveChannel;Lcom/squareup/workflow/legacy/WorkflowPool;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\u0010\u0000\u001a\u0004\u0018\u00010\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0001\"\u0008\u0008\u0001\u0010\u0003*\u00020\u0001\"\u0008\u0008\u0002\u0010\u0004*\u00020\u00012\u0006\u0010\u0005\u001a\u0002H\u00022\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u0002H\u00030\u00072\u0006\u0010\u0008\u001a\u00020\t2\u0018\u0010\n\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00040\u000c0\u000bH\u0096@"
    }
    d2 = {
        "onReact",
        "",
        "S",
        "E",
        "O",
        "state",
        "events",
        "Lkotlinx/coroutines/channels/ReceiveChannel;",
        "workflows",
        "Lcom/squareup/workflow/legacy/WorkflowPool;",
        "continuation",
        "Lkotlin/coroutines/Continuation;",
        "Lcom/squareup/workflow/legacy/Reaction;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation

.annotation runtime Lkotlin/coroutines/jvm/internal/DebugMetadata;
    c = "com.squareup.workflow.legacy.rx2.ReactorKt$toCoroutineReactor$1"
    f = "Reactor.kt"
    i = {
        0x0,
        0x0,
        0x0,
        0x0
    }
    l = {
        0x101
    }
    m = "onReact"
    n = {
        "this",
        "state",
        "events",
        "workflows"
    }
    s = {
        "L$0",
        "L$1",
        "L$2",
        "L$3"
    }
.end annotation


# instance fields
.field L$0:Ljava/lang/Object;

.field L$1:Ljava/lang/Object;

.field L$2:Ljava/lang/Object;

.field L$3:Ljava/lang/Object;

.field label:I

.field synthetic result:Ljava/lang/Object;

.field final synthetic this$0:Lcom/squareup/workflow/legacy/rx2/ReactorKt$toCoroutineReactor$1;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/legacy/rx2/ReactorKt$toCoroutineReactor$1;Lkotlin/coroutines/Continuation;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/workflow/legacy/rx2/ReactorKt$toCoroutineReactor$1$onReact$1;->this$0:Lcom/squareup/workflow/legacy/rx2/ReactorKt$toCoroutineReactor$1;

    invoke-direct {p0, p2}, Lkotlin/coroutines/jvm/internal/ContinuationImpl;-><init>(Lkotlin/coroutines/Continuation;)V

    return-void
.end method


# virtual methods
.method public final invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    iput-object p1, p0, Lcom/squareup/workflow/legacy/rx2/ReactorKt$toCoroutineReactor$1$onReact$1;->result:Ljava/lang/Object;

    iget p1, p0, Lcom/squareup/workflow/legacy/rx2/ReactorKt$toCoroutineReactor$1$onReact$1;->label:I

    const/high16 v0, -0x80000000

    or-int/2addr p1, v0

    iput p1, p0, Lcom/squareup/workflow/legacy/rx2/ReactorKt$toCoroutineReactor$1$onReact$1;->label:I

    iget-object p1, p0, Lcom/squareup/workflow/legacy/rx2/ReactorKt$toCoroutineReactor$1$onReact$1;->this$0:Lcom/squareup/workflow/legacy/rx2/ReactorKt$toCoroutineReactor$1;

    const/4 v0, 0x0

    invoke-virtual {p1, v0, v0, v0, p0}, Lcom/squareup/workflow/legacy/rx2/ReactorKt$toCoroutineReactor$1;->onReact(Ljava/lang/Object;Lkotlinx/coroutines/channels/ReceiveChannel;Lcom/squareup/workflow/legacy/WorkflowPool;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
