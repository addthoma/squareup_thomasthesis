.class public interface abstract Lcom/squareup/workflow/legacy/WorkflowInput;
.super Ljava/lang/Object;
.source "WorkflowInput.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/workflow/legacy/WorkflowInput$ReadOnly;,
        Lcom/squareup/workflow/legacy/WorkflowInput$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Lkotlin/Deprecated;
    message = "Use com.squareup.workflow.Workflow"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0005\u0008g\u0018\u0000 \u0007*\u0006\u0008\u0000\u0010\u0001 \u00002\u00020\u0002:\u0002\u0007\u0008J\u0015\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00028\u0000H&\u00a2\u0006\u0002\u0010\u0006\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "E",
        "",
        "sendEvent",
        "",
        "event",
        "(Ljava/lang/Object;)V",
        "Companion",
        "ReadOnly",
        "legacy-workflow-core"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->$$INSTANCE:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    sput-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    return-void
.end method


# virtual methods
.method public abstract sendEvent(Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation
.end method
