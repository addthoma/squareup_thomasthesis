.class final Lcom/squareup/workflow/LayoutRunnerCoordinator$attach$2;
.super Lkotlin/jvm/internal/Lambda;
.source "LayoutRunnerCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/LayoutRunnerCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "TD;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u00032\u000e\u0010\u0004\u001a\n \u0005*\u0004\u0018\u0001H\u0002H\u0002H\n\u00a2\u0006\u0004\u0008\u0006\u0010\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "D",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        "screen",
        "kotlin.jvm.PlatformType",
        "invoke",
        "(Lcom/squareup/workflow/legacy/V2Screen;)V"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $view:Landroid/view/View;

.field final synthetic this$0:Lcom/squareup/workflow/LayoutRunnerCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/LayoutRunnerCoordinator;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/workflow/LayoutRunnerCoordinator$attach$2;->this$0:Lcom/squareup/workflow/LayoutRunnerCoordinator;

    iput-object p2, p0, Lcom/squareup/workflow/LayoutRunnerCoordinator$attach$2;->$view:Landroid/view/View;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 16
    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    invoke-virtual {p0, p1}, Lcom/squareup/workflow/LayoutRunnerCoordinator$attach$2;->invoke(Lcom/squareup/workflow/legacy/V2Screen;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/legacy/V2Screen;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TD;)V"
        }
    .end annotation

    .line 30
    iget-object v0, p0, Lcom/squareup/workflow/LayoutRunnerCoordinator$attach$2;->$view:Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/workflow/LayoutRunnerCoordinatorKt;->access$getRunner(Landroid/view/View;)Lcom/squareup/workflow/ui/LayoutRunner;

    move-result-object v0

    if-nez v0, :cond_0

    .line 31
    iget-object v0, p0, Lcom/squareup/workflow/LayoutRunnerCoordinator$attach$2;->$view:Landroid/view/View;

    iget-object v1, p0, Lcom/squareup/workflow/LayoutRunnerCoordinator$attach$2;->this$0:Lcom/squareup/workflow/LayoutRunnerCoordinator;

    invoke-static {v1}, Lcom/squareup/workflow/LayoutRunnerCoordinator;->access$getRunnerConstructor$p(Lcom/squareup/workflow/LayoutRunnerCoordinator;)Lkotlin/jvm/functions/Function1;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/workflow/LayoutRunnerCoordinator$attach$2;->$view:Landroid/view/View;

    invoke-interface {v1, v2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/workflow/ui/LayoutRunner;

    invoke-static {v0, v1}, Lcom/squareup/workflow/LayoutRunnerCoordinatorKt;->access$setRunner(Landroid/view/View;Lcom/squareup/workflow/ui/LayoutRunner;)V

    .line 34
    :cond_0
    iget-object v0, p0, Lcom/squareup/workflow/LayoutRunnerCoordinator$attach$2;->$view:Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/workflow/LayoutRunnerCoordinatorKt;->access$getRunner(Landroid/view/View;)Lcom/squareup/workflow/ui/LayoutRunner;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    const-string v1, "screen"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/squareup/workflow/LayoutRunnerCoordinator$attach$2;->this$0:Lcom/squareup/workflow/LayoutRunnerCoordinator;

    invoke-static {v1}, Lcom/squareup/workflow/LayoutRunnerCoordinator;->access$getContainerHints$p(Lcom/squareup/workflow/LayoutRunnerCoordinator;)Lcom/squareup/workflow/ui/ContainerHints;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/squareup/workflow/ui/LayoutRunner;->showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
