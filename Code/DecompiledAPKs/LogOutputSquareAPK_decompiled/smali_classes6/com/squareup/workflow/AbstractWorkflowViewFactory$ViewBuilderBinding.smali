.class public final Lcom/squareup/workflow/AbstractWorkflowViewFactory$ViewBuilderBinding;
.super Ljava/lang/Object;
.source "AbstractWorkflowViewFactory.kt"

# interfaces
.implements Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ViewBuilderBinding"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<D:",
        "Ljava/lang/Object;",
        "E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding<",
        "TD;TE;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\\\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u0002*\u0008\u0008\u0001\u0010\u0003*\u00020\u00022\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u00030\u0004B\u00ab\u0001\u0008\u0000\u0012\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0085\u0001\u0010\t\u001a\u0080\u0001\u0012%\u0012#\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u000c0\u000b\u00a2\u0006\u000c\u0008\r\u0012\u0008\u0008\u000e\u0012\u0004\u0008\u0008(\u000f\u0012\u0013\u0012\u00110\u0010\u00a2\u0006\u000c\u0008\r\u0012\u0008\u0008\u000e\u0012\u0004\u0008\u0008(\u0011\u0012\u0015\u0012\u0013\u0018\u00010\u0012\u00a2\u0006\u000c\u0008\r\u0012\u0008\u0008\u000e\u0012\u0004\u0008\u0008(\u0013\u0012\u0013\u0012\u00110\u0014\u00a2\u0006\u000c\u0008\r\u0012\u0008\u0008\u000e\u0012\u0004\u0008\u0008(\u0015\u0012\u0004\u0012\u00020\u00160\nj\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001`\u0017\u00a2\u0006\u0002\u0010\u0018JU\u0010\u001d\u001a\u00020\u00162\u0006\u0010\u0011\u001a\u00020\u00102\u0008\u0010\u0013\u001a\u0004\u0018\u00010\u00122\u0006\u0010\u0015\u001a\u00020\u00142\u0012\u0010\u001e\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u001f2\u0018\u0010\u000f\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u000cj\u0002` 0\u000bH\u0000\u00a2\u0006\u0002\u0008!R\u008d\u0001\u0010\t\u001a\u0080\u0001\u0012%\u0012#\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u000c0\u000b\u00a2\u0006\u000c\u0008\r\u0012\u0008\u0008\u000e\u0012\u0004\u0008\u0008(\u000f\u0012\u0013\u0012\u00110\u0010\u00a2\u0006\u000c\u0008\r\u0012\u0008\u0008\u000e\u0012\u0004\u0008\u0008(\u0011\u0012\u0015\u0012\u0013\u0018\u00010\u0012\u00a2\u0006\u000c\u0008\r\u0012\u0008\u0008\u000e\u0012\u0004\u0008\u0008(\u0013\u0012\u0013\u0012\u00110\u0014\u00a2\u0006\u000c\u0008\r\u0012\u0008\u0008\u000e\u0012\u0004\u0008\u0008(\u0015\u0012\u0004\u0012\u00020\u00160\nj\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001`\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u00020\u0008X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u001aR \u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0006X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u001c\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory$ViewBuilderBinding;",
        "D",
        "",
        "E",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;",
        "key",
        "Lcom/squareup/workflow/legacy/Screen$Key;",
        "hint",
        "Lcom/squareup/workflow/ScreenHint;",
        "builderForScreens",
        "Lkotlin/Function4;",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lkotlin/ParameterName;",
        "name",
        "screens",
        "Landroid/content/Context;",
        "contextForNewView",
        "Landroid/view/ViewGroup;",
        "container",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "containerHints",
        "Landroid/view/View;",
        "Lcom/squareup/workflow/ViewBuilder;",
        "(Lcom/squareup/workflow/legacy/Screen$Key;Lcom/squareup/workflow/ScreenHint;Lkotlin/jvm/functions/Function4;)V",
        "getHint",
        "()Lcom/squareup/workflow/ScreenHint;",
        "getKey",
        "()Lcom/squareup/workflow/legacy/Screen$Key;",
        "build",
        "screenKey",
        "Lcom/squareup/workflow/legacy/AnyScreenKey;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "build$android_release",
        "android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final builderForScreens:Lkotlin/jvm/functions/Function4;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function4<",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "TD;TE;>;>;",
            "Landroid/content/Context;",
            "Landroid/view/ViewGroup;",
            "Lcom/squareup/workflow/ui/ContainerHints;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final hint:Lcom/squareup/workflow/ScreenHint;

.field private final key:Lcom/squareup/workflow/legacy/Screen$Key;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "TD;TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/workflow/legacy/Screen$Key;Lcom/squareup/workflow/ScreenHint;Lkotlin/jvm/functions/Function4;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "TD;TE;>;",
            "Lcom/squareup/workflow/ScreenHint;",
            "Lkotlin/jvm/functions/Function4<",
            "-",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "TD;TE;>;>;-",
            "Landroid/content/Context;",
            "-",
            "Landroid/view/ViewGroup;",
            "-",
            "Lcom/squareup/workflow/ui/ContainerHints;",
            "+",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "hint"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "builderForScreens"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/workflow/AbstractWorkflowViewFactory$ViewBuilderBinding;->key:Lcom/squareup/workflow/legacy/Screen$Key;

    iput-object p2, p0, Lcom/squareup/workflow/AbstractWorkflowViewFactory$ViewBuilderBinding;->hint:Lcom/squareup/workflow/ScreenHint;

    iput-object p3, p0, Lcom/squareup/workflow/AbstractWorkflowViewFactory$ViewBuilderBinding;->builderForScreens:Lkotlin/jvm/functions/Function4;

    return-void
.end method


# virtual methods
.method public final build$android_release(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/squareup/workflow/ui/ContainerHints;Lcom/squareup/workflow/legacy/Screen$Key;Lio/reactivex/Observable;)Landroid/view/View;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/view/ViewGroup;",
            "Lcom/squareup/workflow/ui/ContainerHints;",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "**>;",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;)",
            "Landroid/view/View;"
        }
    .end annotation

    const-string v0, "contextForNewView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screenKey"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screens"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 151
    invoke-virtual {p0}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$ViewBuilderBinding;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "build: %s"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 152
    invoke-virtual {p0}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$ViewBuilderBinding;->getKey()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    invoke-virtual {v0, p4}, Lcom/squareup/workflow/legacy/Screen$Key;->cast(Lcom/squareup/workflow/legacy/Screen$Key;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p4

    invoke-static {p5, p4}, Lcom/squareup/workflow/legacy/ScreenKt;->ofKeyType(Lio/reactivex/Observable;Lcom/squareup/workflow/legacy/Screen$Key;)Lio/reactivex/Observable;

    move-result-object p4

    .line 153
    iget-object p5, p0, Lcom/squareup/workflow/AbstractWorkflowViewFactory$ViewBuilderBinding;->builderForScreens:Lkotlin/jvm/functions/Function4;

    invoke-interface {p5, p4, p1, p2, p3}, Lkotlin/jvm/functions/Function4;->invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    return-object p1
.end method

.method public getHint()Lcom/squareup/workflow/ScreenHint;
    .locals 1

    .line 140
    iget-object v0, p0, Lcom/squareup/workflow/AbstractWorkflowViewFactory$ViewBuilderBinding;->hint:Lcom/squareup/workflow/ScreenHint;

    return-object v0
.end method

.method public getKey()Lcom/squareup/workflow/legacy/Screen$Key;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "TD;TE;>;"
        }
    .end annotation

    .line 139
    iget-object v0, p0, Lcom/squareup/workflow/AbstractWorkflowViewFactory$ViewBuilderBinding;->key:Lcom/squareup/workflow/legacy/Screen$Key;

    return-object v0
.end method
