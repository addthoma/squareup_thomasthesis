.class public final Lcom/squareup/workflow/diagnostic/ChainedDiagnosticListenerKt;
.super Ljava/lang/Object;
.source "ChainedDiagnosticListener.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nChainedDiagnosticListener.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ChainedDiagnosticListener.kt\ncom/squareup/workflow/diagnostic/ChainedDiagnosticListenerKt\n*L\n1#1,163:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a\u0012\u0010\u0000\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0001\u00a8\u0006\u0003"
    }
    d2 = {
        "andThen",
        "Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;",
        "next",
        "workflow-runtime"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final andThen(Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;)Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;
    .locals 1

    const-string v0, "$this$andThen"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "next"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    instance-of v0, p0, Lcom/squareup/workflow/diagnostic/ChainedDiagnosticListener;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    move-object v0, p0

    :goto_0
    check-cast v0, Lcom/squareup/workflow/diagnostic/ChainedDiagnosticListener;

    if-eqz v0, :cond_1

    goto :goto_1

    .line 30
    :cond_1
    new-instance v0, Lcom/squareup/workflow/diagnostic/ChainedDiagnosticListener;

    invoke-direct {v0, p0}, Lcom/squareup/workflow/diagnostic/ChainedDiagnosticListener;-><init>(Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;)V

    .line 31
    :goto_1
    invoke-virtual {v0, p1}, Lcom/squareup/workflow/diagnostic/ChainedDiagnosticListener;->addVisitor(Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;)V

    check-cast v0, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    return-object v0
.end method
