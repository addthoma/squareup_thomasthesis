.class public final Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;
.super Ljava/lang/Object;
.source "WorkflowUpdateDebugInfo.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Kind;,
        Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Source;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nWorkflowUpdateDebugInfo.kt\nKotlin\n*S Kotlin\n*F\n+ 1 WorkflowUpdateDebugInfo.kt\ncom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo\n*L\n1#1,168:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\r\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0005\u0008\u0086\u0008\u0018\u00002\u00020\u0001:\u0002\u0019\u001aB\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\u0011\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u0012\u001a\u00020\u00132\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0015\u001a\u00020\u0016H\u00d6\u0001J\u0006\u0010\u0017\u001a\u00020\u0003J\u0008\u0010\u0018\u001a\u00020\u0003H\u0016R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u001b\u0010\t\u001a\u00020\u00038BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u000c\u0010\r\u001a\u0004\u0008\n\u0010\u000bR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000b\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;",
        "",
        "workflowType",
        "",
        "kind",
        "Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Kind;",
        "(Ljava/lang/String;Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Kind;)V",
        "getKind",
        "()Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Kind;",
        "lazyDescription",
        "getLazyDescription",
        "()Ljava/lang/String;",
        "lazyDescription$delegate",
        "Lkotlin/Lazy;",
        "getWorkflowType",
        "component1",
        "component2",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toDescriptionString",
        "toString",
        "Kind",
        "Source",
        "workflow-runtime"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private final kind:Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Kind;

.field private final lazyDescription$delegate:Lkotlin/Lazy;

.field private final workflowType:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lkotlin/jvm/internal/PropertyReference1Impl;

    const-class v2, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-string v3, "lazyDescription"

    const-string v4, "getLazyDescription()Ljava/lang/String;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->property1(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Kind;)V
    .locals 1

    const-string v0, "workflowType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "kind"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;->workflowType:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;->kind:Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Kind;

    .line 116
    sget-object p1, Lkotlin/LazyThreadSafetyMode;->PUBLICATION:Lkotlin/LazyThreadSafetyMode;

    new-instance p2, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$lazyDescription$2;

    invoke-direct {p2, p0}, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$lazyDescription$2;-><init>(Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;)V

    check-cast p2, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, p2}, Lkotlin/LazyKt;->lazy(Lkotlin/LazyThreadSafetyMode;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;->lazyDescription$delegate:Lkotlin/Lazy;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;Ljava/lang/String;Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Kind;ILjava/lang/Object;)Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;->workflowType:Ljava/lang/String;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;->kind:Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Kind;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;->copy(Ljava/lang/String;Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Kind;)Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;

    move-result-object p0

    return-object p0
.end method

.method private final getLazyDescription()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;->lazyDescription$delegate:Lkotlin/Lazy;

    sget-object v1, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;->workflowType:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Kind;
    .locals 1

    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;->kind:Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Kind;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Kind;)Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;
    .locals 1

    const-string v0, "workflowType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "kind"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;

    invoke-direct {v0, p1, p2}, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;-><init>(Ljava/lang/String;Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Kind;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;

    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;->workflowType:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;->workflowType:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;->kind:Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Kind;

    iget-object p1, p1, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;->kind:Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Kind;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getKind()Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Kind;
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;->kind:Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Kind;

    return-object v0
.end method

.method public final getWorkflowType()Ljava/lang/String;
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;->workflowType:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;->workflowType:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;->kind:Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Kind;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public final toDescriptionString()Ljava/lang/String;
    .locals 1

    .line 131
    invoke-direct {p0}, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;->getLazyDescription()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 122
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\n    |WorkflowUpdateDebugInfo(\n    |"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124
    invoke-virtual {p0}, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;->toDescriptionString()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v1}, Lkotlin/text/StringsKt;->trimEnd(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "  "

    invoke-static {v1, v2}, Lkotlin/text/StringsKt;->prependIndent(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n    |)\n  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "|"

    .line 126
    invoke-static {v0, v1}, Lkotlin/text/StringsKt;->trimMargin(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 124
    :cond_0
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.CharSequence"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
