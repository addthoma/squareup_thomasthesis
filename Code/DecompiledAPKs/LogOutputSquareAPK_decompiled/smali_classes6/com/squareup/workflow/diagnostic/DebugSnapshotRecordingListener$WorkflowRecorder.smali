.class final Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;
.super Ljava/lang/Object;
.source "DebugSnapshotRecordingListener.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "WorkflowRecorder"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDebugSnapshotRecordingListener.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DebugSnapshotRecordingListener.kt\ncom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,215:1\n1313#2:216\n1382#2,3:217\n1313#2:220\n1382#2,3:221\n*E\n*S KotlinDebug\n*F\n+ 1 DebugSnapshotRecordingListener.kt\ncom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder\n*L\n58#1:216\n58#1,3:217\n62#1:220\n62#1,3:221\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010!\n\u0002\u0008\u0016\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0002\u0018\u00002\u00020\u0001B\u001f\u0012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0007J.\u0010\u001f\u001a\u00020 2\u0012\u0010!\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00000\"2\u0012\u0010#\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020$0\"R\u0017\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\u00030\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0015\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\n\n\u0002\u0010\u0010\u001a\u0004\u0008\u000e\u0010\u000fR\u001c\u0010\u0011\u001a\u0004\u0018\u00010\u0001X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013\"\u0004\u0008\u0014\u0010\u0015R\u001c\u0010\u0016\u001a\u0004\u0018\u00010\u0001X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0017\u0010\u0013\"\u0004\u0008\u0018\u0010\u0015R\u001c\u0010\u0019\u001a\u0004\u0018\u00010\u0001X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u001a\u0010\u0013\"\u0004\u0008\u001b\u0010\u0015R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001c\u0010\rR\u0017\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\u00030\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001e\u0010\u000b\u00a8\u0006%"
    }
    d2 = {
        "Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;",
        "",
        "parentId",
        "",
        "type",
        "",
        "key",
        "(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)V",
        "childrenIds",
        "",
        "getChildrenIds",
        "()Ljava/util/List;",
        "getKey",
        "()Ljava/lang/String;",
        "getParentId",
        "()Ljava/lang/Long;",
        "Ljava/lang/Long;",
        "props",
        "getProps",
        "()Ljava/lang/Object;",
        "setProps",
        "(Ljava/lang/Object;)V",
        "rendering",
        "getRendering",
        "setRendering",
        "state",
        "getState",
        "setState",
        "getType",
        "workerIds",
        "getWorkerIds",
        "snapshot",
        "Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;",
        "recordersById",
        "",
        "workersById",
        "Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkerRecorder;",
        "workflow-runtime"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final childrenIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final key:Ljava/lang/String;

.field private final parentId:Ljava/lang/Long;

.field private props:Ljava/lang/Object;

.field private rendering:Ljava/lang/Object;

.field private state:Ljava/lang/Object;

.field private final type:Ljava/lang/String;

.field private final workerIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string v0, "type"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "key"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;->parentId:Ljava/lang/Long;

    iput-object p2, p0, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;->type:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;->key:Ljava/lang/String;

    .line 46
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/List;

    iput-object p1, p0, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;->childrenIds:Ljava/util/List;

    .line 47
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/List;

    iput-object p1, p0, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;->workerIds:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final getChildrenIds()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 46
    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;->childrenIds:Ljava/util/List;

    return-object v0
.end method

.method public final getKey()Ljava/lang/String;
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;->key:Ljava/lang/String;

    return-object v0
.end method

.method public final getParentId()Ljava/lang/Long;
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;->parentId:Ljava/lang/Long;

    return-object v0
.end method

.method public final getProps()Ljava/lang/Object;
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;->props:Ljava/lang/Object;

    return-object v0
.end method

.method public final getRendering()Ljava/lang/Object;
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;->rendering:Ljava/lang/Object;

    return-object v0
.end method

.method public final getState()Ljava/lang/Object;
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;->state:Ljava/lang/Object;

    return-object v0
.end method

.method public final getType()Ljava/lang/String;
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;->type:Ljava/lang/String;

    return-object v0
.end method

.method public final getWorkerIds()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 47
    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;->workerIds:Ljava/util/List;

    return-object v0
.end method

.method public final setProps(Ljava/lang/Object;)V
    .locals 0

    .line 43
    iput-object p1, p0, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;->props:Ljava/lang/Object;

    return-void
.end method

.method public final setRendering(Ljava/lang/Object;)V
    .locals 0

    .line 45
    iput-object p1, p0, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;->rendering:Ljava/lang/Object;

    return-void
.end method

.method public final setState(Ljava/lang/Object;)V
    .locals 0

    .line 44
    iput-object p1, p0, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;->state:Ljava/lang/Object;

    return-void
.end method

.method public final snapshot(Ljava/util/Map;Ljava/util/Map;)Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkerRecorder;",
            ">;)",
            "Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;"
        }
    .end annotation

    const-string v0, "recordersById"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "workersById"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    iget-object v2, p0, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;->type:Ljava/lang/String;

    .line 55
    iget-object v3, p0, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;->props:Ljava/lang/Object;

    .line 56
    iget-object v4, p0, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;->state:Ljava/lang/Object;

    .line 57
    iget-object v5, p0, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;->rendering:Ljava/lang/Object;

    .line 58
    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;->childrenIds:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 216
    new-instance v1, Ljava/util/ArrayList;

    const/16 v6, 0xa

    invoke-static {v0, v6}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v7

    invoke-direct {v1, v7}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 217
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    .line 218
    check-cast v7, Ljava/lang/Number;

    invoke-virtual {v7}, Ljava/lang/Number;->longValue()J

    move-result-wide v7

    .line 59
    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-static {p1, v7}, Lkotlin/collections/MapsKt;->getValue(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;

    .line 60
    new-instance v8, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot$ChildWorkflow;

    iget-object v9, v7, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;->key:Ljava/lang/String;

    invoke-virtual {v7, p1, p2}, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;->snapshot(Ljava/util/Map;Ljava/util/Map;)Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;

    move-result-object v7

    invoke-direct {v8, v9, v7}, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot$ChildWorkflow;-><init>(Ljava/lang/String;Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;)V

    invoke-interface {v1, v8}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 219
    :cond_0
    move-object p1, v1

    check-cast p1, Ljava/util/List;

    .line 62
    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkflowRecorder;->workerIds:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 220
    new-instance v1, Ljava/util/ArrayList;

    invoke-static {v0, v6}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v6

    invoke-direct {v1, v6}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 221
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    .line 222
    check-cast v6, Ljava/lang/Number;

    invoke-virtual {v6}, Ljava/lang/Number;->longValue()J

    move-result-wide v6

    .line 63
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-static {p2, v6}, Lkotlin/collections/MapsKt;->getValue(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkerRecorder;

    .line 64
    new-instance v7, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot$ChildWorker;

    invoke-virtual {v6}, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkerRecorder;->getKey()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6}, Lcom/squareup/workflow/diagnostic/DebugSnapshotRecordingListener$WorkerRecorder;->getDescription()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v7, v8, v6}, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot$ChildWorker;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v7}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 223
    :cond_1
    move-object v7, v1

    check-cast v7, Ljava/util/List;

    .line 53
    new-instance p2, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;

    move-object v1, p2

    move-object v6, p1

    invoke-direct/range {v1 .. v7}, Lcom/squareup/workflow/diagnostic/WorkflowHierarchyDebugSnapshot;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/util/List;Ljava/util/List;)V

    return-object p2
.end method
