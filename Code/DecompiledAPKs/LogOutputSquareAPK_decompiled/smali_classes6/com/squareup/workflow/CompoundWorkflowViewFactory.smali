.class public Lcom/squareup/workflow/CompoundWorkflowViewFactory;
.super Ljava/lang/Object;
.source "CompoundWorkflowViewFactory.kt"

# interfaces
.implements Lcom/squareup/workflow/WorkflowViewFactory;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCompoundWorkflowViewFactory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CompoundWorkflowViewFactory.kt\ncom/squareup/workflow/CompoundWorkflowViewFactory\n+ 2 _Arrays.kt\nkotlin/collections/ArraysKt___ArraysKt\n*L\n1#1,103:1\n8603#2:104\n8682#2,5:105\n9654#2,9:110\n11416#2,2:119\n9663#2:121\n9654#2,9:122\n11416#2,2:131\n9663#2:133\n9654#2,9:134\n11416#2,2:143\n9663#2:145\n*E\n*S KotlinDebug\n*F\n+ 1 CompoundWorkflowViewFactory.kt\ncom/squareup/workflow/CompoundWorkflowViewFactory\n*L\n24#1:104\n24#1,5:105\n31#1,9:110\n31#1,2:119\n31#1:121\n43#1,9:122\n43#1,2:131\n43#1:133\n56#1,9:134\n56#1,2:143\n56#1:145\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0008\u0002\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0016\u0018\u00002\u00020\u0001B\u0019\u0012\u0012\u0010\u0002\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00010\u0003\"\u00020\u0001\u00a2\u0006\u0002\u0010\u0004JH\u0010\u000c\u001a\u000c\u0012\u0006\u0008\u0001\u0012\u00020\u000e\u0018\u00010\r2\u0012\u0010\u000f\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00082\u0018\u0010\u0010\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0012j\u0002`\u00130\u00112\u0006\u0010\u0014\u001a\u00020\u0015H\u0016JR\u0010\u0016\u001a\u0004\u0018\u00010\u00172\u0012\u0010\u000f\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00082\u0018\u0010\u0010\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0012j\u0002`\u00130\u00112\u0008\u0010\u0018\u001a\u0004\u0018\u00010\u00192\u0006\u0010\u001a\u001a\u00020\u00152\u0006\u0010\u001b\u001a\u00020\u001cH\u0016J(\u0010\u001d\u001a\u00020\u001e2\n\u0010\u001f\u001a\u0006\u0012\u0002\u0008\u00030 2\u0012\u0010\u000f\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u0008H\u0002J\u001e\u0010!\u001a\u0004\u0018\u00010\"2\u0012\u0010\u000f\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u0008H\u0016J\u0008\u0010#\u001a\u00020$H\u0016R&\u0010\u0005\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u00068VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\nR\u0018\u0010\u0002\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00010\u0003X\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010\u000b\u00a8\u0006%"
    }
    d2 = {
        "Lcom/squareup/workflow/CompoundWorkflowViewFactory;",
        "Lcom/squareup/workflow/WorkflowViewFactory;",
        "viewFactories",
        "",
        "([Lcom/squareup/workflow/WorkflowViewFactory;)V",
        "keys",
        "",
        "Lcom/squareup/workflow/legacy/Screen$Key;",
        "Lcom/squareup/workflow/legacy/AnyScreenKey;",
        "getKeys",
        "()Ljava/util/Set;",
        "[Lcom/squareup/workflow/WorkflowViewFactory;",
        "buildDialog",
        "Lio/reactivex/Single;",
        "Landroid/app/Dialog;",
        "screenKey",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "contextForNewDialog",
        "Landroid/content/Context;",
        "buildView",
        "Landroid/view/View;",
        "container",
        "Landroid/view/ViewGroup;",
        "contextForNewView",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "checkAtMostOne",
        "",
        "matches",
        "",
        "getHintForKey",
        "Lcom/squareup/workflow/ScreenHint;",
        "toString",
        "",
        "android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final viewFactories:[Lcom/squareup/workflow/WorkflowViewFactory;


# direct methods
.method public varargs constructor <init>([Lcom/squareup/workflow/WorkflowViewFactory;)V
    .locals 1

    const-string v0, "viewFactories"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/workflow/CompoundWorkflowViewFactory;->viewFactories:[Lcom/squareup/workflow/WorkflowViewFactory;

    .line 27
    iget-object p1, p0, Lcom/squareup/workflow/CompoundWorkflowViewFactory;->viewFactories:[Lcom/squareup/workflow/WorkflowViewFactory;

    invoke-static {p1}, Lcom/squareup/workflow/CompoundWorkflowViewFactoryKt;->access$requireUniqueKeys([Lcom/squareup/workflow/WorkflowViewFactory;)V

    return-void
.end method

.method private final checkAtMostOne(Ljava/util/List;Lcom/squareup/workflow/legacy/Screen$Key;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "*>;",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "**>;)V"
        }
    .end annotation

    .line 72
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    const/4 v0, 0x1

    if-gt p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    return-void

    :cond_1
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Multiple view factories matched key "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2
.end method


# virtual methods
.method public buildDialog(Lcom/squareup/workflow/legacy/Screen$Key;Lio/reactivex/Observable;Landroid/content/Context;)Lio/reactivex/Single;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "**>;",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    const-string v0, "screenKey"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screens"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "contextForNewDialog"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    iget-object v0, p0, Lcom/squareup/workflow/CompoundWorkflowViewFactory;->viewFactories:[Lcom/squareup/workflow/WorkflowViewFactory;

    .line 134
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 143
    array-length v2, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    aget-object v4, v0, v3

    .line 56
    invoke-interface {v4, p1, p2, p3}, Lcom/squareup/workflow/WorkflowViewFactory;->buildDialog(Lcom/squareup/workflow/legacy/Screen$Key;Lio/reactivex/Observable;Landroid/content/Context;)Lio/reactivex/Single;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 142
    invoke-interface {v1, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 145
    :cond_1
    check-cast v1, Ljava/util/List;

    .line 57
    invoke-direct {p0, v1, p1}, Lcom/squareup/workflow/CompoundWorkflowViewFactory;->checkAtMostOne(Ljava/util/List;Lcom/squareup/workflow/legacy/Screen$Key;)V

    .line 58
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->singleOrNull(Ljava/util/List;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lio/reactivex/Single;

    return-object p1
.end method

.method public buildView(Lcom/squareup/workflow/legacy/Screen$Key;Lio/reactivex/Observable;Landroid/view/ViewGroup;Landroid/content/Context;Lcom/squareup/workflow/ui/ContainerHints;)Landroid/view/View;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "**>;",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;",
            "Landroid/view/ViewGroup;",
            "Landroid/content/Context;",
            "Lcom/squareup/workflow/ui/ContainerHints;",
            ")",
            "Landroid/view/View;"
        }
    .end annotation

    const-string v0, "screenKey"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screens"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "contextForNewView"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    iget-object v0, p0, Lcom/squareup/workflow/CompoundWorkflowViewFactory;->viewFactories:[Lcom/squareup/workflow/WorkflowViewFactory;

    .line 122
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 131
    array-length v2, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    aget-object v4, v0, v3

    move-object v5, p1

    move-object v6, p2

    move-object v7, p3

    move-object v8, p4

    move-object v9, p5

    .line 44
    invoke-interface/range {v4 .. v9}, Lcom/squareup/workflow/WorkflowViewFactory;->buildView(Lcom/squareup/workflow/legacy/Screen$Key;Lio/reactivex/Observable;Landroid/view/ViewGroup;Landroid/content/Context;Lcom/squareup/workflow/ui/ContainerHints;)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 130
    invoke-interface {v1, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 133
    :cond_1
    check-cast v1, Ljava/util/List;

    .line 46
    invoke-direct {p0, v1, p1}, Lcom/squareup/workflow/CompoundWorkflowViewFactory;->checkAtMostOne(Ljava/util/List;Lcom/squareup/workflow/legacy/Screen$Key;)V

    .line 47
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->singleOrNull(Ljava/util/List;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    return-object p1
.end method

.method public getHintForKey(Lcom/squareup/workflow/legacy/Screen$Key;)Lcom/squareup/workflow/ScreenHint;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "**>;)",
            "Lcom/squareup/workflow/ScreenHint;"
        }
    .end annotation

    const-string v0, "screenKey"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    iget-object v0, p0, Lcom/squareup/workflow/CompoundWorkflowViewFactory;->viewFactories:[Lcom/squareup/workflow/WorkflowViewFactory;

    .line 110
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 119
    array-length v2, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    aget-object v4, v0, v3

    .line 31
    invoke-interface {v4, p1}, Lcom/squareup/workflow/WorkflowViewFactory;->getHintForKey(Lcom/squareup/workflow/legacy/Screen$Key;)Lcom/squareup/workflow/ScreenHint;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 118
    invoke-interface {v1, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 121
    :cond_1
    check-cast v1, Ljava/util/List;

    .line 32
    invoke-direct {p0, v1, p1}, Lcom/squareup/workflow/CompoundWorkflowViewFactory;->checkAtMostOne(Ljava/util/List;Lcom/squareup/workflow/legacy/Screen$Key;)V

    .line 33
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->singleOrNull(Ljava/util/List;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/ScreenHint;

    return-object p1
.end method

.method public getKeys()Ljava/util/Set;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "**>;>;"
        }
    .end annotation

    .line 24
    iget-object v0, p0, Lcom/squareup/workflow/CompoundWorkflowViewFactory;->viewFactories:[Lcom/squareup/workflow/WorkflowViewFactory;

    .line 104
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 105
    array-length v2, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_0

    aget-object v4, v0, v3

    .line 24
    invoke-interface {v4}, Lcom/squareup/workflow/WorkflowViewFactory;->getKeys()Ljava/util/Set;

    move-result-object v4

    check-cast v4, Ljava/lang/Iterable;

    .line 107
    invoke-static {v1, v4}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 109
    :cond_0
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 24
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->toSet(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 61
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/workflow/CompoundWorkflowViewFactory;->viewFactories:[Lcom/squareup/workflow/WorkflowViewFactory;

    invoke-static {v1}, Lkotlin/collections/ArraysKt;->toList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
