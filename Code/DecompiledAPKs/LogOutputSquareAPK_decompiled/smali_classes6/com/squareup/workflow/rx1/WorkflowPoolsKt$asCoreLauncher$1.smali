.class public final Lcom/squareup/workflow/rx1/WorkflowPoolsKt$asCoreLauncher$1;
.super Ljava/lang/Object;
.source "WorkflowPools.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/rx1/WorkflowPoolsKt;->asCoreLauncher(Lcom/squareup/workflow/rx1/WorkflowLauncher;)Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher<",
        "TS;TE;TO;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u0001J/\u0010\u0002\u001a\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u00032\u0006\u0010\u0004\u001a\u00028\u00002\u0006\u0010\u0005\u001a\u00020\u0006H\u0016\u00a2\u0006\u0002\u0010\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "com/squareup/workflow/rx1/WorkflowPoolsKt$asCoreLauncher$1",
        "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;",
        "launch",
        "Lcom/squareup/workflow/legacy/Workflow;",
        "initialState",
        "workflows",
        "Lcom/squareup/workflow/legacy/WorkflowPool;",
        "(Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;",
        "pure-rx1"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $this_asCoreLauncher:Lcom/squareup/workflow/rx1/WorkflowLauncher;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/rx1/WorkflowLauncher;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/rx1/WorkflowLauncher<",
            "TS;TE;+TO;>;)V"
        }
    .end annotation

    .line 31
    iput-object p1, p0, Lcom/squareup/workflow/rx1/WorkflowPoolsKt$asCoreLauncher$1;->$this_asCoreLauncher:Lcom/squareup/workflow/rx1/WorkflowLauncher;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public launch(Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            ")",
            "Lcom/squareup/workflow/legacy/Workflow<",
            "TS;TE;TO;>;"
        }
    .end annotation

    const-string v0, "initialState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "workflows"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    iget-object v0, p0, Lcom/squareup/workflow/rx1/WorkflowPoolsKt$asCoreLauncher$1;->$this_asCoreLauncher:Lcom/squareup/workflow/rx1/WorkflowLauncher;

    invoke-interface {v0, p1, p2}, Lcom/squareup/workflow/rx1/WorkflowLauncher;->launch(Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/workflow/rx1/WorkflowKt;->toCoreWorkflow(Lcom/squareup/workflow/rx1/Workflow;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    return-object p1
.end method
