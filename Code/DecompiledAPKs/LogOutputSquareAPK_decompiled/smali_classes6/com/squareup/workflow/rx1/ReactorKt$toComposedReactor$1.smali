.class public final Lcom/squareup/workflow/rx1/ReactorKt$toComposedReactor$1;
.super Ljava/lang/Object;
.source "Reactor.kt"

# interfaces
.implements Lcom/squareup/workflow/rx1/ComposedReactor;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/rx1/ReactorKt;->toComposedReactor(Lcom/squareup/workflow/rx1/Reactor;)Lcom/squareup/workflow/rx1/ComposedReactor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/rx1/ComposedReactor<",
        "TS;TE;TO;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000/\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u0001J/\u0010\u0002\u001a\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u00032\u0006\u0010\u0004\u001a\u00028\u00002\u0006\u0010\u0005\u001a\u00020\u0006H\u0016\u00a2\u0006\u0002\u0010\u0007J?\u0010\u0008\u001a\u0016\u0012\u0012\u0008\u0001\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00020\n0\t2\u0006\u0010\u000b\u001a\u00028\u00002\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00028\u00010\r2\u0006\u0010\u0005\u001a\u00020\u0006H\u0016\u00a2\u0006\u0002\u0010\u000e\u00a8\u0006\u000f"
    }
    d2 = {
        "com/squareup/workflow/rx1/ReactorKt$toComposedReactor$1",
        "Lcom/squareup/workflow/rx1/ComposedReactor;",
        "launch",
        "Lcom/squareup/workflow/rx1/Workflow;",
        "initialState",
        "workflows",
        "Lcom/squareup/workflow/legacy/WorkflowPool;",
        "(Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/rx1/Workflow;",
        "onReact",
        "Lrx/Single;",
        "Lcom/squareup/workflow/legacy/Reaction;",
        "state",
        "events",
        "Lcom/squareup/workflow/rx1/EventChannel;",
        "(Ljava/lang/Object;Lcom/squareup/workflow/rx1/EventChannel;Lcom/squareup/workflow/legacy/WorkflowPool;)Lrx/Single;",
        "pure-rx1"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $this_toComposedReactor:Lcom/squareup/workflow/rx1/Reactor;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/rx1/Reactor;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/rx1/Reactor<",
            "TS;TE;+TO;>;)V"
        }
    .end annotation

    .line 39
    iput-object p1, p0, Lcom/squareup/workflow/rx1/ReactorKt$toComposedReactor$1;->$this_toComposedReactor:Lcom/squareup/workflow/rx1/Reactor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public launch(Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/rx1/Workflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            ")",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "TS;TE;TO;>;"
        }
    .end annotation

    const-string v0, "initialState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "workflows"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    iget-object p2, p0, Lcom/squareup/workflow/rx1/ReactorKt$toComposedReactor$1;->$this_toComposedReactor:Lcom/squareup/workflow/rx1/Reactor;

    invoke-static {p2, p1}, Lcom/squareup/workflow/rx1/ReactorKt;->startWorkflow(Lcom/squareup/workflow/rx1/Reactor;Ljava/lang/Object;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object p1

    return-object p1
.end method

.method public onReact(Ljava/lang/Object;Lcom/squareup/workflow/rx1/EventChannel;Lcom/squareup/workflow/legacy/WorkflowPool;)Lrx/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;",
            "Lcom/squareup/workflow/rx1/EventChannel<",
            "TE;>;",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            ")",
            "Lrx/Single<",
            "+",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "TS;TO;>;>;"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "events"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "workflows"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    iget-object p3, p0, Lcom/squareup/workflow/rx1/ReactorKt$toComposedReactor$1;->$this_toComposedReactor:Lcom/squareup/workflow/rx1/Reactor;

    invoke-interface {p3, p1, p2}, Lcom/squareup/workflow/rx1/Reactor;->onReact(Ljava/lang/Object;Lcom/squareup/workflow/rx1/EventChannel;)Lrx/Single;

    move-result-object p1

    return-object p1
.end method
