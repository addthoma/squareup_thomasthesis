.class public final Lcom/squareup/workflow/rx1/WorkflowPoolsKt;
.super Ljava/lang/Object;
.source "WorkflowPools.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u001aN\u0010\u0000\u001a\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0005\"\u0008\u0008\u0001\u0010\u0003*\u00020\u0005\"\u0008\u0008\u0002\u0010\u0004*\u00020\u0005*\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u0006H\u0000\u001aK\u0010\u0007\u001a\u00020\u0008\"\n\u0008\u0000\u0010\u0002\u0018\u0001*\u00020\u0005\"\n\u0008\u0001\u0010\u0003\u0018\u0001*\u00020\u0005\"\n\u0008\u0002\u0010\u0004\u0018\u0001*\u00020\u0005*\u00020\t2\u0018\u0010\n\u001a\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u0006H\u0087\u0008\u001a^\u0010\u0007\u001a\u00020\u0008\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0005\"\u0008\u0008\u0001\u0010\u0003*\u00020\u0005\"\u0008\u0008\u0002\u0010\u0004*\u00020\u0005*\u00020\t2\u0018\u0010\n\u001a\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u00062\u0018\u0010\u000b\u001a\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u000cH\u0001\u00a8\u0006\r"
    }
    d2 = {
        "asCoreLauncher",
        "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;",
        "S",
        "E",
        "O",
        "",
        "Lcom/squareup/workflow/rx1/WorkflowLauncher;",
        "register",
        "",
        "Lcom/squareup/workflow/legacy/WorkflowPool;",
        "factory",
        "type",
        "Lcom/squareup/workflow/legacy/WorkflowPool$Type;",
        "pure-rx1"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final asCoreLauncher(Lcom/squareup/workflow/rx1/WorkflowLauncher;)Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/rx1/WorkflowLauncher<",
            "TS;TE;+TO;>;)",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher<",
            "TS;TE;TO;>;"
        }
    .end annotation

    const-string v0, "$this$asCoreLauncher"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    new-instance v0, Lcom/squareup/workflow/rx1/WorkflowPoolsKt$asCoreLauncher$1;

    invoke-direct {v0, p0}, Lcom/squareup/workflow/rx1/WorkflowPoolsKt$asCoreLauncher$1;-><init>(Lcom/squareup/workflow/rx1/WorkflowLauncher;)V

    check-cast v0, Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;

    return-object v0
.end method

.method public static final synthetic register(Lcom/squareup/workflow/legacy/WorkflowPool;Lcom/squareup/workflow/rx1/WorkflowLauncher;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            "Lcom/squareup/workflow/rx1/WorkflowLauncher<",
            "TS;TE;+TO;>;)V"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Use com.squareup.workflow.Workflow"
    .end annotation

    const-string v0, "$this$register"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "factory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    new-instance v0, Lcom/squareup/workflow/legacy/WorkflowPool$Type;

    const/4 v1, 0x4

    const-string v2, "S"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v2, Ljava/lang/Object;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-string v3, "E"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v3, Ljava/lang/Object;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    const-string v4, "O"

    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v1, Ljava/lang/Object;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-direct {v0, v2, v3, v1}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;-><init>(Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;)V

    invoke-static {p0, p1, v0}, Lcom/squareup/workflow/rx1/WorkflowPoolsKt;->register(Lcom/squareup/workflow/legacy/WorkflowPool;Lcom/squareup/workflow/rx1/WorkflowLauncher;Lcom/squareup/workflow/legacy/WorkflowPool$Type;)V

    return-void
.end method

.method public static final register(Lcom/squareup/workflow/legacy/WorkflowPool;Lcom/squareup/workflow/rx1/WorkflowLauncher;Lcom/squareup/workflow/legacy/WorkflowPool$Type;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            "Lcom/squareup/workflow/rx1/WorkflowLauncher<",
            "TS;TE;+TO;>;",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Type<",
            "TS;-TE;+TO;>;)V"
        }
    .end annotation

    const-string v0, "$this$register"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "factory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "type"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-static {p1}, Lcom/squareup/workflow/rx1/WorkflowPoolsKt;->asCoreLauncher(Lcom/squareup/workflow/rx1/WorkflowLauncher;)Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;

    move-result-object p1

    invoke-virtual {p0, p1, p2}, Lcom/squareup/workflow/legacy/WorkflowPool;->register(Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;Lcom/squareup/workflow/legacy/WorkflowPool$Type;)V

    return-void
.end method
