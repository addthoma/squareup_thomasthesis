.class public final Lcom/squareup/utilities/threeten/LocalDatesKt;
.super Ljava/lang/Object;
.source "LocalDates.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0001\u001a\u0012\u0010\u0002\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004\u001a\n\u0010\u0005\u001a\u00020\u0001*\u00020\u0001\u001a\n\u0010\u0006\u001a\u00020\u0001*\u00020\u0001\u001a\u0012\u0010\u0007\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004\u001a\n\u0010\u0008\u001a\u00020\u0001*\u00020\u0001\u001a\n\u0010\t\u001a\u00020\n*\u00020\u0001\u001a\u0012\u0010\u000b\u001a\u00020\n*\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004\u001a\n\u0010\u000c\u001a\u00020\n*\u00020\u0001\u001a\u0012\u0010\r\u001a\u00020\n*\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004\u001a\n\u0010\u000e\u001a\u00020\u0001*\u00020\u0001\u001a\n\u0010\u000f\u001a\u00020\u0001*\u00020\u0001\u001a\n\u0010\u0010\u001a\u00020\u0001*\u00020\u0001\u001a\n\u0010\u0011\u001a\u00020\u0001*\u00020\u0012\u001a\u000c\u0010\u0013\u001a\u0004\u0018\u00010\u0001*\u00020\u0012\u001a\n\u0010\u0014\u001a\u00020\u0001*\u00020\u0001\u001a\u0012\u0010\u0015\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004\u001a\n\u0010\u0016\u001a\u00020\u0001*\u00020\u0001\u001a\u0012\u0010\u0017\u001a\u00020\u0018*\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004\u001a\u0012\u0010\u0019\u001a\u00020\u001a*\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u0001\u001a\u0014\u0010\u001d\u001a\u00020\u001a*\u00020\u001b2\u0008\u0010\u001c\u001a\u0004\u0018\u00010\u0001\u001a\n\u0010\u001e\u001a\u00020\u0001*\u00020\u0001\u00a8\u0006\u001f"
    }
    d2 = {
        "endOfLastMonth",
        "Lorg/threeten/bp/LocalDate;",
        "endOfLastWeek",
        "locale",
        "Ljava/util/Locale;",
        "endOfLastYear",
        "endOfMonth",
        "endOfWeek",
        "endOfYear",
        "isFirstDayOfMonth",
        "",
        "isFirstDayOfWeek",
        "isLastDayOfMonth",
        "isLastDayOfWeek",
        "oneMonthAgo",
        "oneWeekAgo",
        "oneYearAgo",
        "readLocalDate",
        "Lokio/BufferedSource;",
        "readNullableLocalDate",
        "startOfMonth",
        "startOfWeek",
        "startOfYear",
        "weekOfMonth",
        "",
        "writeLocalDate",
        "",
        "Lokio/BufferedSink;",
        "localDate",
        "writeNullableLocalDate",
        "yesterday",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final endOfLastMonth(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/LocalDate;
    .locals 1

    const-string v0, "$this$endOfLastMonth"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    invoke-static {p0}, Lcom/squareup/utilities/threeten/LocalDatesKt;->oneMonthAgo(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/LocalDate;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/utilities/threeten/LocalDatesKt;->endOfMonth(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/LocalDate;

    move-result-object p0

    return-object p0
.end method

.method public static final endOfLastWeek(Lorg/threeten/bp/LocalDate;Ljava/util/Locale;)Lorg/threeten/bp/LocalDate;
    .locals 1

    const-string v0, "$this$endOfLastWeek"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locale"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-static {p0}, Lcom/squareup/utilities/threeten/LocalDatesKt;->oneWeekAgo(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/LocalDate;

    move-result-object p0

    invoke-static {p0, p1}, Lcom/squareup/utilities/threeten/LocalDatesKt;->endOfWeek(Lorg/threeten/bp/LocalDate;Ljava/util/Locale;)Lorg/threeten/bp/LocalDate;

    move-result-object p0

    return-object p0
.end method

.method public static final endOfLastYear(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/LocalDate;
    .locals 1

    const-string v0, "$this$endOfLastYear"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    invoke-static {p0}, Lcom/squareup/utilities/threeten/LocalDatesKt;->oneYearAgo(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/LocalDate;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/utilities/threeten/LocalDatesKt;->endOfYear(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/LocalDate;

    move-result-object p0

    return-object p0
.end method

.method public static final endOfMonth(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/LocalDate;
    .locals 1

    const-string v0, "$this$endOfMonth"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    invoke-static {}, Lorg/threeten/bp/temporal/TemporalAdjusters;->lastDayOfMonth()Lorg/threeten/bp/temporal/TemporalAdjuster;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/threeten/bp/LocalDate;->with(Lorg/threeten/bp/temporal/TemporalAdjuster;)Lorg/threeten/bp/LocalDate;

    move-result-object p0

    const-string v0, "with(lastDayOfMonth())"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final endOfWeek(Lorg/threeten/bp/LocalDate;Ljava/util/Locale;)Lorg/threeten/bp/LocalDate;
    .locals 2

    const-string v0, "$this$endOfWeek"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locale"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-static {p0, p1}, Lcom/squareup/utilities/threeten/LocalDatesKt;->startOfWeek(Lorg/threeten/bp/LocalDate;Ljava/util/Locale;)Lorg/threeten/bp/LocalDate;

    move-result-object p0

    const-wide/16 v0, 0x6

    invoke-virtual {p0, v0, v1}, Lorg/threeten/bp/LocalDate;->plusDays(J)Lorg/threeten/bp/LocalDate;

    move-result-object p0

    const-string p1, "startOfWeek(locale).plusDays(6)"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final endOfYear(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/LocalDate;
    .locals 1

    const-string v0, "$this$endOfYear"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    invoke-static {}, Lorg/threeten/bp/temporal/TemporalAdjusters;->lastDayOfYear()Lorg/threeten/bp/temporal/TemporalAdjuster;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/threeten/bp/LocalDate;->with(Lorg/threeten/bp/temporal/TemporalAdjuster;)Lorg/threeten/bp/LocalDate;

    move-result-object p0

    const-string v0, "with(lastDayOfYear())"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final isFirstDayOfMonth(Lorg/threeten/bp/LocalDate;)Z
    .locals 1

    const-string v0, "$this$isFirstDayOfMonth"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    invoke-virtual {p0}, Lorg/threeten/bp/LocalDate;->getDayOfMonth()I

    move-result p0

    const/4 v0, 0x1

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static final isFirstDayOfWeek(Lorg/threeten/bp/LocalDate;Ljava/util/Locale;)Z
    .locals 1

    const-string v0, "$this$isFirstDayOfWeek"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locale"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    invoke-virtual {p0}, Lorg/threeten/bp/LocalDate;->getDayOfWeek()Lorg/threeten/bp/DayOfWeek;

    move-result-object v0

    invoke-static {p0, p1}, Lcom/squareup/utilities/threeten/LocalDatesKt;->startOfWeek(Lorg/threeten/bp/LocalDate;Ljava/util/Locale;)Lorg/threeten/bp/LocalDate;

    move-result-object p0

    invoke-virtual {p0}, Lorg/threeten/bp/LocalDate;->getDayOfWeek()Lorg/threeten/bp/DayOfWeek;

    move-result-object p0

    if-ne v0, p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static final isLastDayOfMonth(Lorg/threeten/bp/LocalDate;)Z
    .locals 1

    const-string v0, "$this$isLastDayOfMonth"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    invoke-virtual {p0}, Lorg/threeten/bp/LocalDate;->getDayOfMonth()I

    move-result v0

    invoke-virtual {p0}, Lorg/threeten/bp/LocalDate;->lengthOfMonth()I

    move-result p0

    if-ne v0, p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static final isLastDayOfWeek(Lorg/threeten/bp/LocalDate;Ljava/util/Locale;)Z
    .locals 1

    const-string v0, "$this$isLastDayOfWeek"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locale"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    invoke-virtual {p0}, Lorg/threeten/bp/LocalDate;->getDayOfWeek()Lorg/threeten/bp/DayOfWeek;

    move-result-object v0

    invoke-static {p0, p1}, Lcom/squareup/utilities/threeten/LocalDatesKt;->endOfWeek(Lorg/threeten/bp/LocalDate;Ljava/util/Locale;)Lorg/threeten/bp/LocalDate;

    move-result-object p0

    invoke-virtual {p0}, Lorg/threeten/bp/LocalDate;->getDayOfWeek()Lorg/threeten/bp/DayOfWeek;

    move-result-object p0

    if-ne v0, p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static final oneMonthAgo(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/LocalDate;
    .locals 2

    const-string v0, "$this$oneMonthAgo"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v0, 0x1

    .line 18
    invoke-virtual {p0, v0, v1}, Lorg/threeten/bp/LocalDate;->minusMonths(J)Lorg/threeten/bp/LocalDate;

    move-result-object p0

    const-string v0, "minusMonths(1)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final oneWeekAgo(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/LocalDate;
    .locals 2

    const-string v0, "$this$oneWeekAgo"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v0, 0x1

    .line 16
    invoke-virtual {p0, v0, v1}, Lorg/threeten/bp/LocalDate;->minusWeeks(J)Lorg/threeten/bp/LocalDate;

    move-result-object p0

    const-string v0, "minusWeeks(1)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final oneYearAgo(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/LocalDate;
    .locals 2

    const-string v0, "$this$oneYearAgo"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v0, 0x1

    .line 20
    invoke-virtual {p0, v0, v1}, Lorg/threeten/bp/LocalDate;->minusYears(J)Lorg/threeten/bp/LocalDate;

    move-result-object p0

    const-string v0, "minusYears(1)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final readLocalDate(Lokio/BufferedSource;)Lorg/threeten/bp/LocalDate;
    .locals 2

    const-string v0, "$this$readLocalDate"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    invoke-interface {p0}, Lokio/BufferedSource;->readInt()I

    move-result v0

    .line 88
    invoke-interface {p0}, Lokio/BufferedSource;->readInt()I

    move-result v1

    .line 89
    invoke-interface {p0}, Lokio/BufferedSource;->readInt()I

    move-result p0

    .line 86
    invoke-static {v0, v1, p0}, Lorg/threeten/bp/LocalDate;->of(III)Lorg/threeten/bp/LocalDate;

    move-result-object p0

    const-string v0, "LocalDate.of(\n      read\u2026nt(),\n      readInt()\n  )"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final readNullableLocalDate(Lokio/BufferedSource;)Lorg/threeten/bp/LocalDate;
    .locals 2

    const-string v0, "$this$readNullableLocalDate"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    invoke-interface {p0}, Lokio/BufferedSource;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    .line 108
    :cond_0
    invoke-interface {p0}, Lokio/BufferedSource;->readInt()I

    move-result v0

    .line 109
    invoke-interface {p0}, Lokio/BufferedSource;->readInt()I

    move-result v1

    .line 110
    invoke-interface {p0}, Lokio/BufferedSource;->readInt()I

    move-result p0

    .line 107
    invoke-static {v0, v1, p0}, Lorg/threeten/bp/LocalDate;->of(III)Lorg/threeten/bp/LocalDate;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method public static final startOfMonth(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/LocalDate;
    .locals 1

    const-string v0, "$this$startOfMonth"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-static {}, Lorg/threeten/bp/temporal/TemporalAdjusters;->firstDayOfMonth()Lorg/threeten/bp/temporal/TemporalAdjuster;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/threeten/bp/LocalDate;->with(Lorg/threeten/bp/temporal/TemporalAdjuster;)Lorg/threeten/bp/LocalDate;

    move-result-object p0

    const-string v0, "with(firstDayOfMonth())"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final startOfWeek(Lorg/threeten/bp/LocalDate;Ljava/util/Locale;)Lorg/threeten/bp/LocalDate;
    .locals 1

    const-string v0, "$this$startOfWeek"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locale"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-static {p1}, Lorg/threeten/bp/temporal/WeekFields;->of(Ljava/util/Locale;)Lorg/threeten/bp/temporal/WeekFields;

    move-result-object p1

    const-string v0, "WeekFields.of(locale)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/threeten/bp/temporal/WeekFields;->getFirstDayOfWeek()Lorg/threeten/bp/DayOfWeek;

    move-result-object p1

    invoke-static {p1}, Lorg/threeten/bp/temporal/TemporalAdjusters;->previousOrSame(Lorg/threeten/bp/DayOfWeek;)Lorg/threeten/bp/temporal/TemporalAdjuster;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/threeten/bp/LocalDate;->with(Lorg/threeten/bp/temporal/TemporalAdjuster;)Lorg/threeten/bp/LocalDate;

    move-result-object p0

    const-string p1, "with(previousOrSame(Week\u2026(locale).firstDayOfWeek))"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final startOfYear(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/LocalDate;
    .locals 1

    const-string v0, "$this$startOfYear"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    invoke-static {}, Lorg/threeten/bp/temporal/TemporalAdjusters;->firstDayOfYear()Lorg/threeten/bp/temporal/TemporalAdjuster;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/threeten/bp/LocalDate;->with(Lorg/threeten/bp/temporal/TemporalAdjuster;)Lorg/threeten/bp/LocalDate;

    move-result-object p0

    const-string v0, "with(firstDayOfYear())"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final weekOfMonth(Lorg/threeten/bp/LocalDate;Ljava/util/Locale;)I
    .locals 1

    const-string v0, "$this$weekOfMonth"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locale"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    invoke-static {p1}, Lorg/threeten/bp/temporal/WeekFields;->of(Ljava/util/Locale;)Lorg/threeten/bp/temporal/WeekFields;

    move-result-object p1

    invoke-virtual {p1}, Lorg/threeten/bp/temporal/WeekFields;->weekOfMonth()Lorg/threeten/bp/temporal/TemporalField;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/threeten/bp/LocalDate;->get(Lorg/threeten/bp/temporal/TemporalField;)I

    move-result p0

    return p0
.end method

.method public static final writeLocalDate(Lokio/BufferedSink;Lorg/threeten/bp/LocalDate;)V
    .locals 1

    const-string v0, "$this$writeLocalDate"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "localDate"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    invoke-virtual {p1}, Lorg/threeten/bp/LocalDate;->getYear()I

    move-result v0

    invoke-interface {p0, v0}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    .line 81
    invoke-virtual {p1}, Lorg/threeten/bp/LocalDate;->getMonthValue()I

    move-result v0

    invoke-interface {p0, v0}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    .line 82
    invoke-virtual {p1}, Lorg/threeten/bp/LocalDate;->getDayOfMonth()I

    move-result p1

    invoke-interface {p0, p1}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    return-void
.end method

.method public static final writeNullableLocalDate(Lokio/BufferedSink;Lorg/threeten/bp/LocalDate;)V
    .locals 1

    const-string v0, "$this$writeNullableLocalDate"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 94
    invoke-interface {p0, p1}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    .line 96
    invoke-interface {p0, v0}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    .line 97
    invoke-virtual {p1}, Lorg/threeten/bp/LocalDate;->getYear()I

    move-result v0

    invoke-interface {p0, v0}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    .line 98
    invoke-virtual {p1}, Lorg/threeten/bp/LocalDate;->getMonthValue()I

    move-result v0

    invoke-interface {p0, v0}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    .line 99
    invoke-virtual {p1}, Lorg/threeten/bp/LocalDate;->getDayOfMonth()I

    move-result p1

    invoke-interface {p0, p1}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    :goto_0
    return-void
.end method

.method public static final yesterday(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/LocalDate;
    .locals 2

    const-string v0, "$this$yesterday"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v0, 0x1

    .line 14
    invoke-virtual {p0, v0, v1}, Lorg/threeten/bp/LocalDate;->minusDays(J)Lorg/threeten/bp/LocalDate;

    move-result-object p0

    const-string v0, "minusDays(1)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method
