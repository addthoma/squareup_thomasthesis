.class public final Lcom/squareup/CommonAppModule_ProvideCrashnadoReporterFactory;
.super Ljava/lang/Object;
.source "CommonAppModule_ProvideCrashnadoReporterFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/crashnado/CrashnadoReporter;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final ohSnapLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final reporterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/CrashReporter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/CrashReporter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;)V"
        }
    .end annotation

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/squareup/CommonAppModule_ProvideCrashnadoReporterFactory;->analyticsProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p2, p0, Lcom/squareup/CommonAppModule_ProvideCrashnadoReporterFactory;->reporterProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p3, p0, Lcom/squareup/CommonAppModule_ProvideCrashnadoReporterFactory;->ohSnapLoggerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/CommonAppModule_ProvideCrashnadoReporterFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/CrashReporter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;)",
            "Lcom/squareup/CommonAppModule_ProvideCrashnadoReporterFactory;"
        }
    .end annotation

    .line 42
    new-instance v0, Lcom/squareup/CommonAppModule_ProvideCrashnadoReporterFactory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/CommonAppModule_ProvideCrashnadoReporterFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideCrashnadoReporter(Lcom/squareup/analytics/Analytics;Lcom/squareup/log/CrashReporter;Lcom/squareup/log/OhSnapLogger;)Lcom/squareup/crashnado/CrashnadoReporter;
    .locals 0

    .line 47
    invoke-static {p0, p1, p2}, Lcom/squareup/CommonAppModule;->provideCrashnadoReporter(Lcom/squareup/analytics/Analytics;Lcom/squareup/log/CrashReporter;Lcom/squareup/log/OhSnapLogger;)Lcom/squareup/crashnado/CrashnadoReporter;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/crashnado/CrashnadoReporter;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/crashnado/CrashnadoReporter;
    .locals 3

    .line 36
    iget-object v0, p0, Lcom/squareup/CommonAppModule_ProvideCrashnadoReporterFactory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/analytics/Analytics;

    iget-object v1, p0, Lcom/squareup/CommonAppModule_ProvideCrashnadoReporterFactory;->reporterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/log/CrashReporter;

    iget-object v2, p0, Lcom/squareup/CommonAppModule_ProvideCrashnadoReporterFactory;->ohSnapLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/log/OhSnapLogger;

    invoke-static {v0, v1, v2}, Lcom/squareup/CommonAppModule_ProvideCrashnadoReporterFactory;->provideCrashnadoReporter(Lcom/squareup/analytics/Analytics;Lcom/squareup/log/CrashReporter;Lcom/squareup/log/OhSnapLogger;)Lcom/squareup/crashnado/CrashnadoReporter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/CommonAppModule_ProvideCrashnadoReporterFactory;->get()Lcom/squareup/crashnado/CrashnadoReporter;

    move-result-object v0

    return-object v0
.end method
