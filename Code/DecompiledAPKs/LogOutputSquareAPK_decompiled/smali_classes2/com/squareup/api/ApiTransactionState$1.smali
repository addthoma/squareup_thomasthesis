.class Lcom/squareup/api/ApiTransactionState$1;
.super Lcom/squareup/mortar/BundlerAdapter;
.source "ApiTransactionState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/api/ApiTransactionState;->getBundler()Lmortar/bundler/Bundler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/api/ApiTransactionState;


# direct methods
.method constructor <init>(Lcom/squareup/api/ApiTransactionState;Ljava/lang/String;)V
    .locals 0

    .line 130
    iput-object p1, p0, Lcom/squareup/api/ApiTransactionState$1;->this$0:Lcom/squareup/api/ApiTransactionState;

    invoke-direct {p0, p2}, Lcom/squareup/mortar/BundlerAdapter;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 8

    .line 137
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionState$1;->this$0:Lcom/squareup/api/ApiTransactionState;

    invoke-static {v0}, Lcom/squareup/api/ApiTransactionState;->access$000(Lcom/squareup/api/ApiTransactionState;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 140
    :cond_0
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionState$1;->this$0:Lcom/squareup/api/ApiTransactionState;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/api/ApiTransactionState;->access$002(Lcom/squareup/api/ApiTransactionState;Z)Z

    if-eqz p1, :cond_3

    .line 142
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionState$1;->this$0:Lcom/squareup/api/ApiTransactionState;

    const-string v1, "CHARGE_REQUEST"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/squareup/api/ApiTransactionState;->access$102(Lcom/squareup/api/ApiTransactionState;Z)Z

    const-string v0, "API_VERSION"

    .line 144
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/api/ApiVersion;->parse(Ljava/lang/String;)Lcom/squareup/api/ApiVersion;

    move-result-object v3

    const-string v0, "TIMEOUT"

    .line 145
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    const-string v0, "TENDER_TYPES"

    .line 147
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getIntegerArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    .line 150
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 151
    invoke-static {}, Lcom/squareup/api/ApiTenderType;->values()[Lcom/squareup/api/ApiTenderType;

    move-result-object v2

    .line 152
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    .line 153
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    aget-object v6, v2, v6

    invoke-interface {v1, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 155
    :cond_1
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    move-object v2, v0

    goto :goto_1

    :cond_2
    move-object v2, v1

    :goto_1
    const-string v0, "ALLOW_SPLIT_TENDER"

    .line 157
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    const-string v0, "DELAY_CAPTURE"

    .line 158
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v7

    .line 160
    iget-object p1, p0, Lcom/squareup/api/ApiTransactionState$1;->this$0:Lcom/squareup/api/ApiTransactionState;

    new-instance v0, Lcom/squareup/api/ApiTransactionParams;

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/api/ApiTransactionParams;-><init>(Ljava/util/Set;Lcom/squareup/api/ApiVersion;JZZ)V

    invoke-static {p1, v0}, Lcom/squareup/api/ApiTransactionState;->access$202(Lcom/squareup/api/ApiTransactionState;Lcom/squareup/api/ApiTransactionParams;)Lcom/squareup/api/ApiTransactionParams;

    :cond_3
    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 3

    .line 166
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionState$1;->this$0:Lcom/squareup/api/ApiTransactionState;

    invoke-static {v0}, Lcom/squareup/api/ApiTransactionState;->access$200(Lcom/squareup/api/ApiTransactionState;)Lcom/squareup/api/ApiTransactionParams;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/api/ApiTransactionParams;->apiVersion:Lcom/squareup/api/ApiVersion;

    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionState$1;->this$0:Lcom/squareup/api/ApiTransactionState;

    .line 168
    invoke-static {v0}, Lcom/squareup/api/ApiTransactionState;->access$200(Lcom/squareup/api/ApiTransactionState;)Lcom/squareup/api/ApiTransactionParams;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/api/ApiTransactionParams;->apiVersion:Lcom/squareup/api/ApiVersion;

    iget-object v0, v0, Lcom/squareup/api/ApiVersion;->versionString:Ljava/lang/String;

    const-string v1, "API_VERSION"

    .line 167
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    :cond_0
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionState$1;->this$0:Lcom/squareup/api/ApiTransactionState;

    invoke-static {v0}, Lcom/squareup/api/ApiTransactionState;->access$100(Lcom/squareup/api/ApiTransactionState;)Z

    move-result v0

    const-string v1, "CHARGE_REQUEST"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 171
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionState$1;->this$0:Lcom/squareup/api/ApiTransactionState;

    invoke-static {v0}, Lcom/squareup/api/ApiTransactionState;->access$200(Lcom/squareup/api/ApiTransactionState;)Lcom/squareup/api/ApiTransactionParams;

    move-result-object v0

    iget-wide v0, v0, Lcom/squareup/api/ApiTransactionParams;->timeout:J

    const-string v2, "TIMEOUT"

    invoke-virtual {p1, v2, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 172
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionState$1;->this$0:Lcom/squareup/api/ApiTransactionState;

    invoke-static {v0}, Lcom/squareup/api/ApiTransactionState;->access$200(Lcom/squareup/api/ApiTransactionState;)Lcom/squareup/api/ApiTransactionParams;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/api/ApiTransactionParams;->tenderTypes:Ljava/util/Set;

    if-eqz v0, :cond_2

    .line 173
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 174
    iget-object v1, p0, Lcom/squareup/api/ApiTransactionState$1;->this$0:Lcom/squareup/api/ApiTransactionState;

    invoke-static {v1}, Lcom/squareup/api/ApiTransactionState;->access$200(Lcom/squareup/api/ApiTransactionState;)Lcom/squareup/api/ApiTransactionParams;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/api/ApiTransactionParams;->tenderTypes:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/api/ApiTenderType;

    .line 175
    invoke-virtual {v2}, Lcom/squareup/api/ApiTenderType;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    const-string v1, "TENDER_TYPES"

    .line 177
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putIntegerArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 179
    :cond_2
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionState$1;->this$0:Lcom/squareup/api/ApiTransactionState;

    invoke-static {v0}, Lcom/squareup/api/ApiTransactionState;->access$200(Lcom/squareup/api/ApiTransactionState;)Lcom/squareup/api/ApiTransactionParams;

    move-result-object v0

    iget-boolean v0, v0, Lcom/squareup/api/ApiTransactionParams;->allowSplitTender:Z

    const-string v1, "ALLOW_SPLIT_TENDER"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 180
    iget-object v0, p0, Lcom/squareup/api/ApiTransactionState$1;->this$0:Lcom/squareup/api/ApiTransactionState;

    invoke-static {v0}, Lcom/squareup/api/ApiTransactionState;->access$200(Lcom/squareup/api/ApiTransactionState;)Lcom/squareup/api/ApiTransactionParams;

    move-result-object v0

    iget-boolean v0, v0, Lcom/squareup/api/ApiTransactionParams;->delayCapture:Z

    const-string v1, "DELAY_CAPTURE"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method
