.class Lcom/squareup/api/ApiReaderSettingsController$1;
.super Lcom/squareup/mortar/BundlerAdapter;
.source "ApiReaderSettingsController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/api/ApiReaderSettingsController;->getBundler()Lmortar/bundler/Bundler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/api/ApiReaderSettingsController;


# direct methods
.method constructor <init>(Lcom/squareup/api/ApiReaderSettingsController;Ljava/lang/String;)V
    .locals 0

    .line 105
    iput-object p1, p0, Lcom/squareup/api/ApiReaderSettingsController$1;->this$0:Lcom/squareup/api/ApiReaderSettingsController;

    invoke-direct {p0, p2}, Lcom/squareup/mortar/BundlerAdapter;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public synthetic lambda$onEnterScope$0$ApiReaderSettingsController$1(Lkotlin/Unit;)V
    .locals 1

    .line 109
    iget-object p1, p0, Lcom/squareup/api/ApiReaderSettingsController$1;->this$0:Lcom/squareup/api/ApiReaderSettingsController;

    invoke-virtual {p1}, Lcom/squareup/api/ApiReaderSettingsController;->isApiReaderSettingsRequest()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 110
    iget-object p1, p0, Lcom/squareup/api/ApiReaderSettingsController$1;->this$0:Lcom/squareup/api/ApiReaderSettingsController;

    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/squareup/api/ApiReaderSettingsController;->access$300(Lcom/squareup/api/ApiReaderSettingsController;Z)V

    :cond_0
    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 108
    iget-object p1, p0, Lcom/squareup/api/ApiReaderSettingsController$1;->this$0:Lcom/squareup/api/ApiReaderSettingsController;

    invoke-static {p1}, Lcom/squareup/api/ApiReaderSettingsController;->access$000(Lcom/squareup/api/ApiReaderSettingsController;)Lcom/squareup/api/ApiRequestController;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/api/ApiRequestController;->onStaleApiRequest()Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/api/-$$Lambda$ApiReaderSettingsController$1$MCxjsVxqjjScix8Jw7IVc-C73sA;

    invoke-direct {v0, p0}, Lcom/squareup/api/-$$Lambda$ApiReaderSettingsController$1$MCxjsVxqjjScix8Jw7IVc-C73sA;-><init>(Lcom/squareup/api/ApiReaderSettingsController$1;)V

    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 2

    .line 117
    iget-object v0, p0, Lcom/squareup/api/ApiReaderSettingsController$1;->this$0:Lcom/squareup/api/ApiReaderSettingsController;

    invoke-static {v0}, Lcom/squareup/api/ApiReaderSettingsController;->access$100(Lcom/squareup/api/ApiReaderSettingsController;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 120
    :cond_0
    iget-object v0, p0, Lcom/squareup/api/ApiReaderSettingsController$1;->this$0:Lcom/squareup/api/ApiReaderSettingsController;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/api/ApiReaderSettingsController;->access$102(Lcom/squareup/api/ApiReaderSettingsController;Z)Z

    if-eqz p1, :cond_1

    .line 122
    iget-object v0, p0, Lcom/squareup/api/ApiReaderSettingsController$1;->this$0:Lcom/squareup/api/ApiReaderSettingsController;

    const-string v1, "readerSettingsApiRequestInFlight"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    invoke-static {v0, p1}, Lcom/squareup/api/ApiReaderSettingsController;->access$202(Lcom/squareup/api/ApiReaderSettingsController;Z)Z

    :cond_1
    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 127
    iget-object v0, p0, Lcom/squareup/api/ApiReaderSettingsController$1;->this$0:Lcom/squareup/api/ApiReaderSettingsController;

    invoke-static {v0}, Lcom/squareup/api/ApiReaderSettingsController;->access$200(Lcom/squareup/api/ApiReaderSettingsController;)Z

    move-result v0

    const-string v1, "readerSettingsApiRequestInFlight"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method
