.class public final Lcom/squareup/api/multipassauth/MultipassServiceModule;
.super Ljava/lang/Object;
.source "MultipassServiceModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u00c7\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u00020\u00042\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0006H\u0007\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/api/multipassauth/MultipassServiceModule;",
        "",
        "()V",
        "provideMultipassService",
        "Lcom/squareup/api/multipassauth/MultipassService;",
        "serviceCreator",
        "Lcom/squareup/api/ServiceCreator;",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/api/multipassauth/MultipassServiceModule;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 12
    new-instance v0, Lcom/squareup/api/multipassauth/MultipassServiceModule;

    invoke-direct {v0}, Lcom/squareup/api/multipassauth/MultipassServiceModule;-><init>()V

    sput-object v0, Lcom/squareup/api/multipassauth/MultipassServiceModule;->INSTANCE:Lcom/squareup/api/multipassauth/MultipassServiceModule;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final provideMultipassService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/api/multipassauth/MultipassService;
    .locals 1
    .param p1    # Lcom/squareup/api/ServiceCreator;
        .annotation runtime Lcom/squareup/api/RetrofitAuthenticated;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/api/RealService;
    .end annotation

    .annotation runtime Lcom/squareup/dagger/SingleIn;
        value = Lcom/squareup/dagger/AppScope;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    const-string v0, "serviceCreator"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    const-class v0, Lcom/squareup/api/multipassauth/MultipassService;

    invoke-interface {p1, v0}, Lcom/squareup/api/ServiceCreator;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    const-string v0, "serviceCreator.create(Mu\u2026ipassService::class.java)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/api/multipassauth/MultipassService;

    return-object p1
.end method
