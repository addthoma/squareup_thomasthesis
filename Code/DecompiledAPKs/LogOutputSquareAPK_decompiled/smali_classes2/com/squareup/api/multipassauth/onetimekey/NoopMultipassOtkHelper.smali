.class public final Lcom/squareup/api/multipassauth/onetimekey/NoopMultipassOtkHelper;
.super Ljava/lang/Object;
.source "NoopMultipassOtkHelper.kt"

# interfaces
.implements Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNoopMultipassOtkHelper.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NoopMultipassOtkHelper.kt\ncom/squareup/api/multipassauth/onetimekey/NoopMultipassOtkHelper\n+ 2 Objects.kt\ncom/squareup/util/Objects\n*L\n1#1,8:1\n151#2,2:9\n*E\n*S KotlinDebug\n*F\n+ 1 NoopMultipassOtkHelper.kt\ncom/squareup/api/multipassauth/onetimekey/NoopMultipassOtkHelper\n*L\n7#1,2:9\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u0017\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u0007H\u0096\u0001\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/api/multipassauth/onetimekey/NoopMultipassOtkHelper;",
        "Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper;",
        "()V",
        "authAndRedirectToUrl",
        "Lio/reactivex/Single;",
        "Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper$MultipassOtkUri;",
        "targetUrl",
        "",
        "impl-noop_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final synthetic $$delegate_0:Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper;


# direct methods
.method public constructor <init>()V
    .locals 5
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 9
    move-object v1, v0

    check-cast v1, Ljava/lang/String;

    .line 10
    const-class v2, Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper;

    const/4 v3, 0x0

    const/4 v4, 0x4

    invoke-static {v2, v1, v3, v4, v0}, Lcom/squareup/util/Objects;->allMethodsThrowUnsupportedOperation$default(Ljava/lang/Class;Ljava/lang/String;ZILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper;

    iput-object v0, p0, Lcom/squareup/api/multipassauth/onetimekey/NoopMultipassOtkHelper;->$$delegate_0:Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper;

    return-void
.end method


# virtual methods
.method public authAndRedirectToUrl(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper$MultipassOtkUri;",
            ">;"
        }
    .end annotation

    const-string v0, "targetUrl"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/api/multipassauth/onetimekey/NoopMultipassOtkHelper;->$$delegate_0:Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper;

    invoke-interface {v0, p1}, Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper;->authAndRedirectToUrl(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
