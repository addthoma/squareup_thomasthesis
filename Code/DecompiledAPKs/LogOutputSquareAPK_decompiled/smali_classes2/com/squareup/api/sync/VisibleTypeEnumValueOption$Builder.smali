.class public final Lcom/squareup/api/sync/VisibleTypeEnumValueOption$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "VisibleTypeEnumValueOption.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/sync/VisibleTypeEnumValueOption;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/sync/VisibleTypeEnumValueOption;",
        "Lcom/squareup/api/sync/VisibleTypeEnumValueOption$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public type:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 79
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 80
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/api/sync/VisibleTypeEnumValueOption$Builder;->type:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/api/sync/VisibleTypeEnumValueOption;
    .locals 3

    .line 91
    new-instance v0, Lcom/squareup/api/sync/VisibleTypeEnumValueOption;

    iget-object v1, p0, Lcom/squareup/api/sync/VisibleTypeEnumValueOption$Builder;->type:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/api/sync/VisibleTypeEnumValueOption;-><init>(Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 76
    invoke-virtual {p0}, Lcom/squareup/api/sync/VisibleTypeEnumValueOption$Builder;->build()Lcom/squareup/api/sync/VisibleTypeEnumValueOption;

    move-result-object v0

    return-object v0
.end method

.method public type(Ljava/util/List;)Lcom/squareup/api/sync/VisibleTypeEnumValueOption$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectType;",
            ">;)",
            "Lcom/squareup/api/sync/VisibleTypeEnumValueOption$Builder;"
        }
    .end annotation

    .line 84
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 85
    iput-object p1, p0, Lcom/squareup/api/sync/VisibleTypeEnumValueOption$Builder;->type:Ljava/util/List;

    return-object p0
.end method
