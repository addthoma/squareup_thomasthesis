.class public final Lcom/squareup/api/sync/GetRequest;
.super Lcom/squareup/wire/Message;
.source "GetRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/sync/GetRequest$ProtoAdapter_GetRequest;,
        Lcom/squareup/api/sync/GetRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/api/sync/GetRequest;",
        "Lcom/squareup/api/sync/GetRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/sync/GetRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_APPLIED_BAZAAR_VERSION:Ljava/lang/Long;

.field public static final DEFAULT_APPLIED_SERVER_VERSION:Ljava/lang/Long;

.field public static final DEFAULT_MAX_BATCH_SIZE:Ljava/lang/Long;

.field public static final DEFAULT_PAGINATION_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_SYNC_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_USE_PAGINATION_IN_INITIAL_SYNC:Ljava/lang/Boolean;

.field public static final DEFAULT_VISIBILITY:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final applied_bazaar_version:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x3e8
    .end annotation
.end field

.field public final applied_server_version:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x1
    .end annotation
.end field

.field public final max_batch_size:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x2
    .end annotation
.end field

.field public final pagination_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final sync_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3ea
    .end annotation
.end field

.field public final use_pagination_in_initial_sync:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field

.field public final visibility:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x3e9
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 27
    new-instance v0, Lcom/squareup/api/sync/GetRequest$ProtoAdapter_GetRequest;

    invoke-direct {v0}, Lcom/squareup/api/sync/GetRequest$ProtoAdapter_GetRequest;-><init>()V

    sput-object v0, Lcom/squareup/api/sync/GetRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 31
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/api/sync/GetRequest;->DEFAULT_APPLIED_SERVER_VERSION:Ljava/lang/Long;

    .line 33
    sput-object v0, Lcom/squareup/api/sync/GetRequest;->DEFAULT_MAX_BATCH_SIZE:Ljava/lang/Long;

    const/4 v1, 0x0

    .line 35
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    sput-object v2, Lcom/squareup/api/sync/GetRequest;->DEFAULT_USE_PAGINATION_IN_INITIAL_SYNC:Ljava/lang/Boolean;

    .line 41
    sput-object v0, Lcom/squareup/api/sync/GetRequest;->DEFAULT_APPLIED_BAZAAR_VERSION:Ljava/lang/Long;

    .line 43
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/api/sync/GetRequest;->DEFAULT_VISIBILITY:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Integer;)V
    .locals 9

    .line 122
    sget-object v8, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/api/sync/GetRequest;-><init>(Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Integer;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Integer;Lokio/ByteString;)V
    .locals 1

    .line 128
    sget-object v0, Lcom/squareup/api/sync/GetRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p8}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 129
    iput-object p1, p0, Lcom/squareup/api/sync/GetRequest;->applied_server_version:Ljava/lang/Long;

    .line 130
    iput-object p2, p0, Lcom/squareup/api/sync/GetRequest;->max_batch_size:Ljava/lang/Long;

    .line 131
    iput-object p3, p0, Lcom/squareup/api/sync/GetRequest;->use_pagination_in_initial_sync:Ljava/lang/Boolean;

    .line 132
    iput-object p4, p0, Lcom/squareup/api/sync/GetRequest;->pagination_token:Ljava/lang/String;

    .line 133
    iput-object p5, p0, Lcom/squareup/api/sync/GetRequest;->sync_token:Ljava/lang/String;

    .line 134
    iput-object p6, p0, Lcom/squareup/api/sync/GetRequest;->applied_bazaar_version:Ljava/lang/Long;

    .line 135
    iput-object p7, p0, Lcom/squareup/api/sync/GetRequest;->visibility:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 155
    :cond_0
    instance-of v1, p1, Lcom/squareup/api/sync/GetRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 156
    :cond_1
    check-cast p1, Lcom/squareup/api/sync/GetRequest;

    .line 157
    invoke-virtual {p0}, Lcom/squareup/api/sync/GetRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/api/sync/GetRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/GetRequest;->applied_server_version:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/api/sync/GetRequest;->applied_server_version:Ljava/lang/Long;

    .line 158
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/GetRequest;->max_batch_size:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/api/sync/GetRequest;->max_batch_size:Ljava/lang/Long;

    .line 159
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/GetRequest;->use_pagination_in_initial_sync:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/api/sync/GetRequest;->use_pagination_in_initial_sync:Ljava/lang/Boolean;

    .line 160
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/GetRequest;->pagination_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/sync/GetRequest;->pagination_token:Ljava/lang/String;

    .line 161
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/GetRequest;->sync_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/sync/GetRequest;->sync_token:Ljava/lang/String;

    .line 162
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/GetRequest;->applied_bazaar_version:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/api/sync/GetRequest;->applied_bazaar_version:Ljava/lang/Long;

    .line 163
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/GetRequest;->visibility:Ljava/lang/Integer;

    iget-object p1, p1, Lcom/squareup/api/sync/GetRequest;->visibility:Ljava/lang/Integer;

    .line 164
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 169
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_7

    .line 171
    invoke-virtual {p0}, Lcom/squareup/api/sync/GetRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 172
    iget-object v1, p0, Lcom/squareup/api/sync/GetRequest;->applied_server_version:Ljava/lang/Long;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 173
    iget-object v1, p0, Lcom/squareup/api/sync/GetRequest;->max_batch_size:Ljava/lang/Long;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 174
    iget-object v1, p0, Lcom/squareup/api/sync/GetRequest;->use_pagination_in_initial_sync:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 175
    iget-object v1, p0, Lcom/squareup/api/sync/GetRequest;->pagination_token:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 176
    iget-object v1, p0, Lcom/squareup/api/sync/GetRequest;->sync_token:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 177
    iget-object v1, p0, Lcom/squareup/api/sync/GetRequest;->applied_bazaar_version:Ljava/lang/Long;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 178
    iget-object v1, p0, Lcom/squareup/api/sync/GetRequest;->visibility:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :cond_6
    add-int/2addr v0, v2

    .line 179
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_7
    return v0
.end method

.method public newBuilder()Lcom/squareup/api/sync/GetRequest$Builder;
    .locals 2

    .line 140
    new-instance v0, Lcom/squareup/api/sync/GetRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/api/sync/GetRequest$Builder;-><init>()V

    .line 141
    iget-object v1, p0, Lcom/squareup/api/sync/GetRequest;->applied_server_version:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/api/sync/GetRequest$Builder;->applied_server_version:Ljava/lang/Long;

    .line 142
    iget-object v1, p0, Lcom/squareup/api/sync/GetRequest;->max_batch_size:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/api/sync/GetRequest$Builder;->max_batch_size:Ljava/lang/Long;

    .line 143
    iget-object v1, p0, Lcom/squareup/api/sync/GetRequest;->use_pagination_in_initial_sync:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/api/sync/GetRequest$Builder;->use_pagination_in_initial_sync:Ljava/lang/Boolean;

    .line 144
    iget-object v1, p0, Lcom/squareup/api/sync/GetRequest;->pagination_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/sync/GetRequest$Builder;->pagination_token:Ljava/lang/String;

    .line 145
    iget-object v1, p0, Lcom/squareup/api/sync/GetRequest;->sync_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/sync/GetRequest$Builder;->sync_token:Ljava/lang/String;

    .line 146
    iget-object v1, p0, Lcom/squareup/api/sync/GetRequest;->applied_bazaar_version:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/api/sync/GetRequest$Builder;->applied_bazaar_version:Ljava/lang/Long;

    .line 147
    iget-object v1, p0, Lcom/squareup/api/sync/GetRequest;->visibility:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/api/sync/GetRequest$Builder;->visibility:Ljava/lang/Integer;

    .line 148
    invoke-virtual {p0}, Lcom/squareup/api/sync/GetRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/sync/GetRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 26
    invoke-virtual {p0}, Lcom/squareup/api/sync/GetRequest;->newBuilder()Lcom/squareup/api/sync/GetRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 186
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 187
    iget-object v1, p0, Lcom/squareup/api/sync/GetRequest;->applied_server_version:Ljava/lang/Long;

    if-eqz v1, :cond_0

    const-string v1, ", applied_server_version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/GetRequest;->applied_server_version:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 188
    :cond_0
    iget-object v1, p0, Lcom/squareup/api/sync/GetRequest;->max_batch_size:Ljava/lang/Long;

    if-eqz v1, :cond_1

    const-string v1, ", max_batch_size="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/GetRequest;->max_batch_size:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 189
    :cond_1
    iget-object v1, p0, Lcom/squareup/api/sync/GetRequest;->use_pagination_in_initial_sync:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", use_pagination_in_initial_sync="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/GetRequest;->use_pagination_in_initial_sync:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 190
    :cond_2
    iget-object v1, p0, Lcom/squareup/api/sync/GetRequest;->pagination_token:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", pagination_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/GetRequest;->pagination_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 191
    :cond_3
    iget-object v1, p0, Lcom/squareup/api/sync/GetRequest;->sync_token:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", sync_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/GetRequest;->sync_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 192
    :cond_4
    iget-object v1, p0, Lcom/squareup/api/sync/GetRequest;->applied_bazaar_version:Ljava/lang/Long;

    if-eqz v1, :cond_5

    const-string v1, ", applied_bazaar_version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/GetRequest;->applied_bazaar_version:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 193
    :cond_5
    iget-object v1, p0, Lcom/squareup/api/sync/GetRequest;->visibility:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    const-string v1, ", visibility="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/GetRequest;->visibility:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_6
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "GetRequest{"

    .line 194
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
