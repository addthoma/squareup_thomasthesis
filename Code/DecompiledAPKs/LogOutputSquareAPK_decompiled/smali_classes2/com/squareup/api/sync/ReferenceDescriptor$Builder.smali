.class public final Lcom/squareup/api/sync/ReferenceDescriptor$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ReferenceDescriptor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/sync/ReferenceDescriptor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/sync/ReferenceDescriptor;",
        "Lcom/squareup/api/sync/ReferenceDescriptor$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public external:Ljava/lang/Boolean;

.field public on_delete:Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;

.field public type:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 116
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 117
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/api/sync/ReferenceDescriptor$Builder;->type:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/api/sync/ReferenceDescriptor;
    .locals 5

    .line 141
    new-instance v0, Lcom/squareup/api/sync/ReferenceDescriptor;

    iget-object v1, p0, Lcom/squareup/api/sync/ReferenceDescriptor$Builder;->type:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/api/sync/ReferenceDescriptor$Builder;->on_delete:Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;

    iget-object v3, p0, Lcom/squareup/api/sync/ReferenceDescriptor$Builder;->external:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/api/sync/ReferenceDescriptor;-><init>(Ljava/util/List;Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 109
    invoke-virtual {p0}, Lcom/squareup/api/sync/ReferenceDescriptor$Builder;->build()Lcom/squareup/api/sync/ReferenceDescriptor;

    move-result-object v0

    return-object v0
.end method

.method public external(Ljava/lang/Boolean;)Lcom/squareup/api/sync/ReferenceDescriptor$Builder;
    .locals 0

    .line 135
    iput-object p1, p0, Lcom/squareup/api/sync/ReferenceDescriptor$Builder;->external:Ljava/lang/Boolean;

    return-object p0
.end method

.method public on_delete(Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;)Lcom/squareup/api/sync/ReferenceDescriptor$Builder;
    .locals 0

    .line 127
    iput-object p1, p0, Lcom/squareup/api/sync/ReferenceDescriptor$Builder;->on_delete:Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;

    return-object p0
.end method

.method public type(Ljava/util/List;)Lcom/squareup/api/sync/ReferenceDescriptor$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectType;",
            ">;)",
            "Lcom/squareup/api/sync/ReferenceDescriptor$Builder;"
        }
    .end annotation

    .line 121
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 122
    iput-object p1, p0, Lcom/squareup/api/sync/ReferenceDescriptor$Builder;->type:Ljava/util/List;

    return-object p0
.end method
