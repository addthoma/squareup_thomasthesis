.class public final Lcom/squareup/api/rpc/Response$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Response.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/rpc/Response;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/rpc/Response;",
        "Lcom/squareup/api/rpc/Response$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public create_session_response:Lcom/squareup/api/sync/CreateSessionResponse;

.field public error:Lcom/squareup/api/rpc/Error;

.field public get_response:Lcom/squareup/api/sync/GetResponse;

.field public id:Ljava/lang/Long;

.field public inventory_adjust_response:Lcom/squareup/api/items/InventoryAdjustResponse;

.field public put_response:Lcom/squareup/api/sync/PutResponse;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 172
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/api/rpc/Response;
    .locals 9

    .line 214
    new-instance v8, Lcom/squareup/api/rpc/Response;

    iget-object v1, p0, Lcom/squareup/api/rpc/Response$Builder;->id:Ljava/lang/Long;

    iget-object v2, p0, Lcom/squareup/api/rpc/Response$Builder;->error:Lcom/squareup/api/rpc/Error;

    iget-object v3, p0, Lcom/squareup/api/rpc/Response$Builder;->create_session_response:Lcom/squareup/api/sync/CreateSessionResponse;

    iget-object v4, p0, Lcom/squareup/api/rpc/Response$Builder;->get_response:Lcom/squareup/api/sync/GetResponse;

    iget-object v5, p0, Lcom/squareup/api/rpc/Response$Builder;->put_response:Lcom/squareup/api/sync/PutResponse;

    iget-object v6, p0, Lcom/squareup/api/rpc/Response$Builder;->inventory_adjust_response:Lcom/squareup/api/items/InventoryAdjustResponse;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/api/rpc/Response;-><init>(Ljava/lang/Long;Lcom/squareup/api/rpc/Error;Lcom/squareup/api/sync/CreateSessionResponse;Lcom/squareup/api/sync/GetResponse;Lcom/squareup/api/sync/PutResponse;Lcom/squareup/api/items/InventoryAdjustResponse;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 159
    invoke-virtual {p0}, Lcom/squareup/api/rpc/Response$Builder;->build()Lcom/squareup/api/rpc/Response;

    move-result-object v0

    return-object v0
.end method

.method public create_session_response(Lcom/squareup/api/sync/CreateSessionResponse;)Lcom/squareup/api/rpc/Response$Builder;
    .locals 0

    .line 193
    iput-object p1, p0, Lcom/squareup/api/rpc/Response$Builder;->create_session_response:Lcom/squareup/api/sync/CreateSessionResponse;

    return-object p0
.end method

.method public error(Lcom/squareup/api/rpc/Error;)Lcom/squareup/api/rpc/Response$Builder;
    .locals 0

    .line 188
    iput-object p1, p0, Lcom/squareup/api/rpc/Response$Builder;->error:Lcom/squareup/api/rpc/Error;

    return-object p0
.end method

.method public get_response(Lcom/squareup/api/sync/GetResponse;)Lcom/squareup/api/rpc/Response$Builder;
    .locals 0

    .line 198
    iput-object p1, p0, Lcom/squareup/api/rpc/Response$Builder;->get_response:Lcom/squareup/api/sync/GetResponse;

    return-object p0
.end method

.method public id(Ljava/lang/Long;)Lcom/squareup/api/rpc/Response$Builder;
    .locals 0

    .line 179
    iput-object p1, p0, Lcom/squareup/api/rpc/Response$Builder;->id:Ljava/lang/Long;

    return-object p0
.end method

.method public inventory_adjust_response(Lcom/squareup/api/items/InventoryAdjustResponse;)Lcom/squareup/api/rpc/Response$Builder;
    .locals 0

    .line 208
    iput-object p1, p0, Lcom/squareup/api/rpc/Response$Builder;->inventory_adjust_response:Lcom/squareup/api/items/InventoryAdjustResponse;

    return-object p0
.end method

.method public put_response(Lcom/squareup/api/sync/PutResponse;)Lcom/squareup/api/rpc/Response$Builder;
    .locals 0

    .line 203
    iput-object p1, p0, Lcom/squareup/api/rpc/Response$Builder;->put_response:Lcom/squareup/api/sync/PutResponse;

    return-object p0
.end method
