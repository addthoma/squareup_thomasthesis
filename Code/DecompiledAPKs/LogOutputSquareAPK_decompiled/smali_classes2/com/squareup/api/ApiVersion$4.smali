.class final enum Lcom/squareup/api/ApiVersion$4;
.super Lcom/squareup/api/ApiVersion;
.source "ApiVersion.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/ApiVersion;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4008
    name = null
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 77
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/api/ApiVersion;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/squareup/api/ApiVersion$1;)V

    return-void
.end method


# virtual methods
.method upgrade(Landroid/content/Intent;)V
    .locals 2

    const-string v0, "com.squareup.pos.CUSTOMER_ID"

    .line 79
    invoke-virtual {p1, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 80
    invoke-static {p1}, Lcom/squareup/api/ApiTenderType;->extractTenderTypes(Landroid/content/Intent;)Ljava/util/Set;

    move-result-object v0

    .line 81
    sget-object v1, Lcom/squareup/api/ApiTenderType;->CARD_ON_FILE:Lcom/squareup/api/ApiTenderType;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 82
    invoke-static {v0}, Ljava/util/EnumSet;->copyOf(Ljava/util/Collection;)Ljava/util/EnumSet;

    move-result-object v0

    .line 83
    sget-object v1, Lcom/squareup/api/ApiTenderType;->CARD_ON_FILE:Lcom/squareup/api/ApiTenderType;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 84
    invoke-static {p1, v0}, Lcom/squareup/api/ApiTenderType;->putTenderTypes(Landroid/content/Intent;Ljava/util/Set;)V

    :cond_0
    return-void
.end method
