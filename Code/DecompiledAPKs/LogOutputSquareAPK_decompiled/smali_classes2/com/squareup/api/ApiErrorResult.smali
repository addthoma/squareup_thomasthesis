.class public final enum Lcom/squareup/api/ApiErrorResult;
.super Ljava/lang/Enum;
.source "ApiErrorResult.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/ApiErrorResult$Constants;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/api/ApiErrorResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/api/ApiErrorResult;

.field public static final enum ADD_CARD_ON_FILE_CANCELED:Lcom/squareup/api/ApiErrorResult;

.field public static final enum APPLICATION_NOT_FOUND:Lcom/squareup/api/ApiErrorResult;

.field public static final enum BUYER_FLOW_CANCELED_CLIENT_INITIATED_APP_TERMINATION:Lcom/squareup/api/ApiErrorResult;

.field public static final enum BUYER_FLOW_CANCELED_CLIENT_INITIATED_TIMEOUT:Lcom/squareup/api/ApiErrorResult;

.field public static final enum BUYER_FLOW_CANCELED_HUMAN_INITIATED:Lcom/squareup/api/ApiErrorResult;

.field public static final enum BUYER_FLOW_CANCELED_READER_INITIATED:Lcom/squareup/api/ApiErrorResult;

.field public static final enum BUYER_FLOW_CANCELED_UNEXPECTED:Lcom/squareup/api/ApiErrorResult;

.field public static final enum CANNOT_ENCODE_CERTIFICATE:Lcom/squareup/api/ApiErrorResult;

.field public static final enum CANNOT_PARSE_CERTIFICATE:Lcom/squareup/api/ApiErrorResult;

.field public static final enum CUSTOMER_EXCEPTION_NOT_A_NETWORK_ERROR:Lcom/squareup/api/ApiErrorResult;

.field public static final enum CUSTOMER_INVALID_ID:Lcom/squareup/api/ApiErrorResult;

.field public static final enum CUSTOMER_NOT_SUPPORTED:Lcom/squareup/api/ApiErrorResult;

.field public static final enum CUSTOMER_NO_NETWORK:Lcom/squareup/api/ApiErrorResult;

.field public static final enum CUSTOMER_NO_SERVER:Lcom/squareup/api/ApiErrorResult;

.field public static final enum CUSTOMER_UNEXPECTED_HTTP_ERROR:Lcom/squareup/api/ApiErrorResult;

.field public static final enum CUSTOMER_UNKNOWN_ERROR:Lcom/squareup/api/ApiErrorResult;

.field public static final enum GET_LOCATION_DENIED:Lcom/squareup/api/ApiErrorResult;

.field public static final enum INVALID_AMOUNT_TOO_HIGH:Lcom/squareup/api/ApiErrorResult;

.field public static final enum INVALID_AMOUNT_TOO_LOW:Lcom/squareup/api/ApiErrorResult;

.field public static final enum INVALID_API_VERSION:Lcom/squareup/api/ApiErrorResult;

.field public static final enum INVALID_CHARGE_AMOUNT:Lcom/squareup/api/ApiErrorResult;

.field public static final enum INVALID_CURRENCY:Lcom/squareup/api/ApiErrorResult;

.field public static final enum INVALID_MERCHANT_ID:Lcom/squareup/api/ApiErrorResult;

.field public static final enum INVALID_PACKAGE:Lcom/squareup/api/ApiErrorResult;

.field public static final enum INVALID_SANDBOX_CLIENT_ID:Lcom/squareup/api/ApiErrorResult;

.field public static final enum INVALID_START_METHOD:Lcom/squareup/api/ApiErrorResult;

.field public static final enum INVALID_TIMEOUT:Lcom/squareup/api/ApiErrorResult;

.field public static final enum INVALID_WEB_CALLBACK_URI:Lcom/squareup/api/ApiErrorResult;

.field public static final enum MISSING_API_VERSION:Lcom/squareup/api/ApiErrorResult;

.field public static final enum MISSING_CHARGE_AMOUNT:Lcom/squareup/api/ApiErrorResult;

.field public static final enum MISSING_CLIENT_ID:Lcom/squareup/api/ApiErrorResult;

.field public static final enum MISSING_CURRENCY:Lcom/squareup/api/ApiErrorResult;

.field public static final enum MISSING_SIGNATURES:Lcom/squareup/api/ApiErrorResult;

.field public static final enum MISSING_TENDER_TYPE:Lcom/squareup/api/ApiErrorResult;

.field public static final enum MISSING_WEB_CALLBACK_URI:Lcom/squareup/api/ApiErrorResult;

.field public static final enum NOTE_TOO_LONG:Lcom/squareup/api/ApiErrorResult;

.field public static final enum NO_EMPLOYEE_LOGGED_IN:Lcom/squareup/api/ApiErrorResult;

.field public static final enum NO_RESULT_AVAILABLE:Lcom/squareup/api/ApiErrorResult;

.field public static final enum SETTINGS_EXCEPTION_NOT_A_NETWORK_ERROR:Lcom/squareup/api/ApiErrorResult;

.field public static final enum SETTINGS_INVALID_CLIENT_ID:Lcom/squareup/api/ApiErrorResult;

.field public static final enum SETTINGS_NO_NETWORK:Lcom/squareup/api/ApiErrorResult;

.field public static final enum SETTINGS_NO_SERVER:Lcom/squareup/api/ApiErrorResult;

.field public static final enum SETTINGS_UNEXPECTED_HTTP_ERROR:Lcom/squareup/api/ApiErrorResult;

.field public static final enum SETTINGS_UNKNOWN_ERROR:Lcom/squareup/api/ApiErrorResult;

.field public static final enum SHA1_NOT_AVAILABLE:Lcom/squareup/api/ApiErrorResult;

.field public static final enum STALE_TRANSACTION_REQUEST:Lcom/squareup/api/ApiErrorResult;

.field public static final enum TENDER_FLOW_CANCELED:Lcom/squareup/api/ApiErrorResult;

.field public static final enum TIMEOUT_TOO_HIGH:Lcom/squareup/api/ApiErrorResult;

.field public static final enum TIMEOUT_TOO_LOW:Lcom/squareup/api/ApiErrorResult;

.field public static final enum TRANSACTION_IN_PROGRESS:Lcom/squareup/api/ApiErrorResult;

.field public static final enum UNEXPECTED_HOME_SCREEN:Lcom/squareup/api/ApiErrorResult;

.field public static final enum UNKNOWN_FINGERPRINT:Lcom/squareup/api/ApiErrorResult;

.field public static final enum UNSUPPORTED_API_VERSION:Lcom/squareup/api/ApiErrorResult;

.field public static final enum UNSUPPORTED_WEB_API_VERSION:Lcom/squareup/api/ApiErrorResult;

.field public static final enum USER_NOT_ACTIVATED:Lcom/squareup/api/ApiErrorResult;

.field public static final enum USER_NOT_LOGGED_IN:Lcom/squareup/api/ApiErrorResult;

.field public static final enum VALIDATION_CANCELED:Lcom/squareup/api/ApiErrorResult;

.field public static final enum X509_NOT_AVAILABLE:Lcom/squareup/api/ApiErrorResult;


# instance fields
.field public final errorCode:Ljava/lang/String;

.field public final errorDescriptionResourceId:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 29
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->devplat_error_missing_signature:I

    const/4 v2, 0x0

    const-string v3, "com.squareup.pos.ERROR_INVALID_REQUEST"

    const-string v4, "APPLICATION_NOT_FOUND"

    invoke-direct {v0, v4, v2, v3, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->APPLICATION_NOT_FOUND:Lcom/squareup/api/ApiErrorResult;

    .line 30
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->devplat_error_transaction_canceled:I

    const/4 v4, 0x1

    const-string v5, "com.squareup.pos.ERROR_TRANSACTION_CANCELED"

    const-string v6, "BUYER_FLOW_CANCELED_CLIENT_INITIATED_APP_TERMINATION"

    invoke-direct {v0, v6, v4, v5, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->BUYER_FLOW_CANCELED_CLIENT_INITIATED_APP_TERMINATION:Lcom/squareup/api/ApiErrorResult;

    .line 32
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->devplat_error_transaction_canceled:I

    const/4 v6, 0x2

    const-string v7, "BUYER_FLOW_CANCELED_CLIENT_INITIATED_TIMEOUT"

    invoke-direct {v0, v7, v6, v5, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->BUYER_FLOW_CANCELED_CLIENT_INITIATED_TIMEOUT:Lcom/squareup/api/ApiErrorResult;

    .line 34
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->devplat_error_transaction_canceled:I

    const/4 v7, 0x3

    const-string v8, "BUYER_FLOW_CANCELED_HUMAN_INITIATED"

    invoke-direct {v0, v8, v7, v5, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->BUYER_FLOW_CANCELED_HUMAN_INITIATED:Lcom/squareup/api/ApiErrorResult;

    .line 36
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->devplat_error_transaction_canceled:I

    const/4 v8, 0x4

    const-string v9, "BUYER_FLOW_CANCELED_READER_INITIATED"

    invoke-direct {v0, v9, v8, v5, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->BUYER_FLOW_CANCELED_READER_INITIATED:Lcom/squareup/api/ApiErrorResult;

    .line 38
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->devplat_error_transaction_canceled:I

    const/4 v9, 0x5

    const-string v10, "BUYER_FLOW_CANCELED_UNEXPECTED"

    invoke-direct {v0, v10, v9, v5, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->BUYER_FLOW_CANCELED_UNEXPECTED:Lcom/squareup/api/ApiErrorResult;

    .line 40
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->devplat_error_cannot_verify_fingerprint:I

    const/4 v10, 0x6

    const-string v11, "CANNOT_ENCODE_CERTIFICATE"

    invoke-direct {v0, v11, v10, v3, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->CANNOT_ENCODE_CERTIFICATE:Lcom/squareup/api/ApiErrorResult;

    .line 42
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->devplat_error_cannot_verify_fingerprint:I

    const/4 v11, 0x7

    const-string v12, "CANNOT_PARSE_CERTIFICATE"

    invoke-direct {v0, v12, v11, v3, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->CANNOT_PARSE_CERTIFICATE:Lcom/squareup/api/ApiErrorResult;

    .line 44
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->devplat_error_unknown_error:I

    const/16 v12, 0x8

    const-string v13, "com.squareup.pos.ERROR_UNEXPECTED"

    const-string v14, "CUSTOMER_EXCEPTION_NOT_A_NETWORK_ERROR"

    invoke-direct {v0, v14, v12, v13, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->CUSTOMER_EXCEPTION_NOT_A_NETWORK_ERROR:Lcom/squareup/api/ApiErrorResult;

    .line 45
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->devplat_error_invalid_customer_id:I

    const/16 v14, 0x9

    const-string v15, "CUSTOMER_INVALID_ID"

    const-string v12, "com.squareup.pos.ERROR_INVALID_CUSTOMER_ID"

    invoke-direct {v0, v15, v14, v12, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->CUSTOMER_INVALID_ID:Lcom/squareup/api/ApiErrorResult;

    .line 46
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->devplat_error_no_network:I

    const-string v12, "com.squareup.pos.ERROR_NO_NETWORK"

    const/16 v15, 0xa

    const-string v14, "CUSTOMER_NO_NETWORK"

    invoke-direct {v0, v14, v15, v12, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->CUSTOMER_NO_NETWORK:Lcom/squareup/api/ApiErrorResult;

    .line 47
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->devplat_error_unknown_error:I

    const-string v14, "CUSTOMER_NO_SERVER"

    const/16 v15, 0xb

    invoke-direct {v0, v14, v15, v13, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->CUSTOMER_NO_SERVER:Lcom/squareup/api/ApiErrorResult;

    .line 48
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->devplat_error_customer_not_supported:I

    const-string v14, "CUSTOMER_NOT_SUPPORTED"

    const/16 v15, 0xc

    const-string v11, "com.squareup.pos.ERROR_CUSTOMER_MANAGEMENT_NOT_SUPPORTED"

    invoke-direct {v0, v14, v15, v11, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->CUSTOMER_NOT_SUPPORTED:Lcom/squareup/api/ApiErrorResult;

    .line 50
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->devplat_error_unknown_error:I

    const-string v11, "CUSTOMER_UNEXPECTED_HTTP_ERROR"

    const/16 v14, 0xd

    invoke-direct {v0, v11, v14, v13, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->CUSTOMER_UNEXPECTED_HTTP_ERROR:Lcom/squareup/api/ApiErrorResult;

    .line 51
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->devplat_error_unknown_error:I

    const-string v11, "CUSTOMER_UNKNOWN_ERROR"

    const/16 v14, 0xe

    invoke-direct {v0, v11, v14, v13, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->CUSTOMER_UNKNOWN_ERROR:Lcom/squareup/api/ApiErrorResult;

    .line 52
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->devplat_error_transaction_canceled:I

    const-string v11, "GET_LOCATION_DENIED"

    const/16 v14, 0xf

    invoke-direct {v0, v11, v14, v5, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->GET_LOCATION_DENIED:Lcom/squareup/api/ApiErrorResult;

    .line 54
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->reader_sdk_invalid_amount_too_low:I

    const-string v11, "INVALID_AMOUNT_TOO_LOW"

    const/16 v14, 0x10

    invoke-direct {v0, v11, v14, v3, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->INVALID_AMOUNT_TOO_LOW:Lcom/squareup/api/ApiErrorResult;

    .line 55
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->reader_sdk_invalid_amount_too_high:I

    const-string v11, "INVALID_AMOUNT_TOO_HIGH"

    const/16 v14, 0x11

    invoke-direct {v0, v11, v14, v3, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->INVALID_AMOUNT_TOO_HIGH:Lcom/squareup/api/ApiErrorResult;

    .line 56
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->devplat_error_invalid_api_version:I

    const-string v11, "INVALID_API_VERSION"

    const/16 v14, 0x12

    invoke-direct {v0, v11, v14, v3, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->INVALID_API_VERSION:Lcom/squareup/api/ApiErrorResult;

    .line 57
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->devplat_error_invalid_charge_amount:I

    const-string v11, "INVALID_CHARGE_AMOUNT"

    const/16 v14, 0x13

    invoke-direct {v0, v11, v14, v3, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->INVALID_CHARGE_AMOUNT:Lcom/squareup/api/ApiErrorResult;

    .line 58
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->devplat_error_invalid_currency:I

    const-string v11, "INVALID_CURRENCY"

    const/16 v14, 0x14

    invoke-direct {v0, v11, v14, v3, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->INVALID_CURRENCY:Lcom/squareup/api/ApiErrorResult;

    .line 59
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->devplat_error_illegal_location_id:I

    const-string v11, "INVALID_MERCHANT_ID"

    const/16 v14, 0x15

    const-string v15, "com.squareup.pos.ERROR_ILLEGAL_LOCATION_ID"

    invoke-direct {v0, v11, v14, v15, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->INVALID_MERCHANT_ID:Lcom/squareup/api/ApiErrorResult;

    .line 61
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->devplat_error_unknown_package:I

    const-string v11, "INVALID_PACKAGE"

    const/16 v14, 0x16

    invoke-direct {v0, v11, v14, v3, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->INVALID_PACKAGE:Lcom/squareup/api/ApiErrorResult;

    .line 62
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->devplat_error_invalid_sandbox_client_id:I

    const-string v11, "INVALID_SANDBOX_CLIENT_ID"

    const/16 v14, 0x17

    invoke-direct {v0, v11, v14, v3, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->INVALID_SANDBOX_CLIENT_ID:Lcom/squareup/api/ApiErrorResult;

    .line 64
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->devplat_error_invalid_start_method:I

    const-string v11, "INVALID_START_METHOD"

    const/16 v14, 0x18

    invoke-direct {v0, v11, v14, v3, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->INVALID_START_METHOD:Lcom/squareup/api/ApiErrorResult;

    .line 65
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->devplat_error_invalid_timeout:I

    const-string v11, "INVALID_TIMEOUT"

    const/16 v14, 0x19

    invoke-direct {v0, v11, v14, v3, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->INVALID_TIMEOUT:Lcom/squareup/api/ApiErrorResult;

    .line 66
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->devplat_error_invalid_web_callback_uri:I

    const-string v11, "INVALID_WEB_CALLBACK_URI"

    const/16 v14, 0x1a

    invoke-direct {v0, v11, v14, v3, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->INVALID_WEB_CALLBACK_URI:Lcom/squareup/api/ApiErrorResult;

    .line 68
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->devplat_error_missing_charge_amount:I

    const-string v11, "MISSING_CHARGE_AMOUNT"

    const/16 v14, 0x1b

    invoke-direct {v0, v11, v14, v3, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->MISSING_CHARGE_AMOUNT:Lcom/squareup/api/ApiErrorResult;

    .line 69
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->devplat_error_missing_api_version:I

    const-string v11, "MISSING_API_VERSION"

    const/16 v14, 0x1c

    invoke-direct {v0, v11, v14, v3, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->MISSING_API_VERSION:Lcom/squareup/api/ApiErrorResult;

    .line 70
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->devplat_error_missing_client_id:I

    const-string v11, "MISSING_CLIENT_ID"

    const/16 v14, 0x1d

    invoke-direct {v0, v11, v14, v3, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->MISSING_CLIENT_ID:Lcom/squareup/api/ApiErrorResult;

    .line 71
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->devplat_error_missing_currency:I

    const-string v11, "MISSING_CURRENCY"

    const/16 v14, 0x1e

    invoke-direct {v0, v11, v14, v3, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->MISSING_CURRENCY:Lcom/squareup/api/ApiErrorResult;

    .line 72
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->devplat_error_missing_signature:I

    const-string v11, "MISSING_SIGNATURES"

    const/16 v14, 0x1f

    invoke-direct {v0, v11, v14, v3, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->MISSING_SIGNATURES:Lcom/squareup/api/ApiErrorResult;

    .line 73
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->devplat_error_missing_tender_type:I

    const-string v11, "MISSING_TENDER_TYPE"

    const/16 v14, 0x20

    invoke-direct {v0, v11, v14, v3, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->MISSING_TENDER_TYPE:Lcom/squareup/api/ApiErrorResult;

    .line 74
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->devplat_error_missing_web_callback_uri:I

    const-string v11, "MISSING_WEB_CALLBACK_URI"

    const/16 v14, 0x21

    invoke-direct {v0, v11, v14, v3, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->MISSING_WEB_CALLBACK_URI:Lcom/squareup/api/ApiErrorResult;

    .line 76
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->devplat_error_no_employee_logged_in:I

    const-string v11, "NO_EMPLOYEE_LOGGED_IN"

    const/16 v14, 0x22

    const-string v15, "com.squareup.pos.ERROR_NO_EMPLOYEE_LOGGED_IN"

    invoke-direct {v0, v11, v14, v15, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->NO_EMPLOYEE_LOGGED_IN:Lcom/squareup/api/ApiErrorResult;

    .line 78
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->devplat_error_missing_result:I

    const-string v11, "NO_RESULT_AVAILABLE"

    const/16 v14, 0x23

    const-string v15, "com.squareup.pos.ERROR_NO_RESULT"

    invoke-direct {v0, v11, v14, v15, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->NO_RESULT_AVAILABLE:Lcom/squareup/api/ApiErrorResult;

    .line 79
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->devplat_error_note_too_long:I

    const-string v11, "NOTE_TOO_LONG"

    const/16 v14, 0x24

    invoke-direct {v0, v11, v14, v3, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->NOTE_TOO_LONG:Lcom/squareup/api/ApiErrorResult;

    .line 80
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->devplat_error_unknown_error:I

    const-string v11, "SETTINGS_EXCEPTION_NOT_A_NETWORK_ERROR"

    const/16 v14, 0x25

    invoke-direct {v0, v11, v14, v13, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->SETTINGS_EXCEPTION_NOT_A_NETWORK_ERROR:Lcom/squareup/api/ApiErrorResult;

    .line 81
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->devplat_error_invalid_client_id:I

    const-string v11, "SETTINGS_INVALID_CLIENT_ID"

    const/16 v14, 0x26

    invoke-direct {v0, v11, v14, v3, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->SETTINGS_INVALID_CLIENT_ID:Lcom/squareup/api/ApiErrorResult;

    .line 82
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->devplat_error_no_network:I

    const-string v11, "SETTINGS_NO_NETWORK"

    const/16 v14, 0x27

    invoke-direct {v0, v11, v14, v12, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->SETTINGS_NO_NETWORK:Lcom/squareup/api/ApiErrorResult;

    .line 83
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->devplat_error_unknown_error:I

    const-string v11, "SETTINGS_NO_SERVER"

    const/16 v12, 0x28

    invoke-direct {v0, v11, v12, v13, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->SETTINGS_NO_SERVER:Lcom/squareup/api/ApiErrorResult;

    .line 84
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->devplat_error_unknown_error:I

    const-string v11, "SETTINGS_UNEXPECTED_HTTP_ERROR"

    const/16 v12, 0x29

    invoke-direct {v0, v11, v12, v13, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->SETTINGS_UNEXPECTED_HTTP_ERROR:Lcom/squareup/api/ApiErrorResult;

    .line 85
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->devplat_error_unknown_error:I

    const-string v11, "SETTINGS_UNKNOWN_ERROR"

    const/16 v12, 0x2a

    invoke-direct {v0, v11, v12, v13, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->SETTINGS_UNKNOWN_ERROR:Lcom/squareup/api/ApiErrorResult;

    .line 86
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->devplat_error_cannot_verify_fingerprint:I

    const-string v11, "SHA1_NOT_AVAILABLE"

    const/16 v12, 0x2b

    invoke-direct {v0, v11, v12, v3, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->SHA1_NOT_AVAILABLE:Lcom/squareup/api/ApiErrorResult;

    .line 87
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->devplat_error_transaction_stale:I

    const-string v11, "STALE_TRANSACTION_REQUEST"

    const/16 v12, 0x2c

    invoke-direct {v0, v11, v12, v5, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->STALE_TRANSACTION_REQUEST:Lcom/squareup/api/ApiErrorResult;

    .line 89
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->devplat_error_transaction_canceled:I

    const-string v11, "TENDER_FLOW_CANCELED"

    const/16 v12, 0x2d

    invoke-direct {v0, v11, v12, v5, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->TENDER_FLOW_CANCELED:Lcom/squareup/api/ApiErrorResult;

    .line 90
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->devplat_error_timeout_too_low:I

    const-string v11, "TIMEOUT_TOO_LOW"

    const/16 v12, 0x2e

    invoke-direct {v0, v11, v12, v3, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->TIMEOUT_TOO_LOW:Lcom/squareup/api/ApiErrorResult;

    .line 91
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->devplat_error_timeout_too_high:I

    const-string v11, "TIMEOUT_TOO_HIGH"

    const/16 v12, 0x2f

    invoke-direct {v0, v11, v12, v3, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->TIMEOUT_TOO_HIGH:Lcom/squareup/api/ApiErrorResult;

    .line 92
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->devplat_error_transaction_in_progress:I

    const-string v11, "TRANSACTION_IN_PROGRESS"

    const/16 v12, 0x30

    const-string v13, "com.squareup.pos.ERROR_TRANSACTION_ALREADY_IN_PROGRESS"

    invoke-direct {v0, v11, v12, v13, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->TRANSACTION_IN_PROGRESS:Lcom/squareup/api/ApiErrorResult;

    .line 94
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->devplat_error_transaction_canceled:I

    const-string v11, "UNEXPECTED_HOME_SCREEN"

    const/16 v12, 0x31

    invoke-direct {v0, v11, v12, v5, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->UNEXPECTED_HOME_SCREEN:Lcom/squareup/api/ApiErrorResult;

    .line 96
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->devplat_error_invalid_fingerprint:I

    const-string v11, "UNKNOWN_FINGERPRINT"

    const/16 v12, 0x32

    invoke-direct {v0, v11, v12, v3, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->UNKNOWN_FINGERPRINT:Lcom/squareup/api/ApiErrorResult;

    .line 97
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->devplat_error_unsupported_api_version:I

    const-string v11, "UNSUPPORTED_API_VERSION"

    const/16 v12, 0x33

    const-string v13, "com.squareup.pos.UNSUPPORTED_API_VERSION"

    invoke-direct {v0, v11, v12, v13, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->UNSUPPORTED_API_VERSION:Lcom/squareup/api/ApiErrorResult;

    .line 99
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->devplat_error_unsupported_web_api_version:I

    const-string v11, "UNSUPPORTED_WEB_API_VERSION"

    const/16 v12, 0x34

    const-string v13, "com.squareup.pos.UNSUPPORTED_WEB_API_VERSION"

    invoke-direct {v0, v11, v12, v13, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->UNSUPPORTED_WEB_API_VERSION:Lcom/squareup/api/ApiErrorResult;

    .line 101
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->devplat_error_transaction_canceled:I

    const-string v11, "ADD_CARD_ON_FILE_CANCELED"

    const/16 v12, 0x35

    invoke-direct {v0, v11, v12, v5, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->ADD_CARD_ON_FILE_CANCELED:Lcom/squareup/api/ApiErrorResult;

    .line 102
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->devplat_error_user_not_activated:I

    const-string v11, "USER_NOT_ACTIVATED"

    const/16 v12, 0x36

    const-string v13, "com.squareup.pos.ERROR_USER_NOT_ACTIVATED"

    invoke-direct {v0, v11, v12, v13, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->USER_NOT_ACTIVATED:Lcom/squareup/api/ApiErrorResult;

    .line 103
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->devplat_error_user_not_logged_in:I

    const-string v11, "USER_NOT_LOGGED_IN"

    const/16 v12, 0x37

    const-string v13, "com.squareup.pos.ERROR_USER_NOT_LOGGED_IN"

    invoke-direct {v0, v11, v12, v13, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->USER_NOT_LOGGED_IN:Lcom/squareup/api/ApiErrorResult;

    .line 105
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->devplat_error_transaction_canceled:I

    const-string v11, "VALIDATION_CANCELED"

    const/16 v12, 0x38

    invoke-direct {v0, v11, v12, v5, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->VALIDATION_CANCELED:Lcom/squareup/api/ApiErrorResult;

    .line 106
    new-instance v0, Lcom/squareup/api/ApiErrorResult;

    sget v1, Lcom/squareup/common/strings/R$string;->devplat_error_cannot_verify_fingerprint:I

    const-string v5, "X509_NOT_AVAILABLE"

    const/16 v11, 0x39

    invoke-direct {v0, v5, v11, v3, v1}, Lcom/squareup/api/ApiErrorResult;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->X509_NOT_AVAILABLE:Lcom/squareup/api/ApiErrorResult;

    const/16 v0, 0x3a

    new-array v0, v0, [Lcom/squareup/api/ApiErrorResult;

    .line 28
    sget-object v1, Lcom/squareup/api/ApiErrorResult;->APPLICATION_NOT_FOUND:Lcom/squareup/api/ApiErrorResult;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->BUYER_FLOW_CANCELED_CLIENT_INITIATED_APP_TERMINATION:Lcom/squareup/api/ApiErrorResult;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->BUYER_FLOW_CANCELED_CLIENT_INITIATED_TIMEOUT:Lcom/squareup/api/ApiErrorResult;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->BUYER_FLOW_CANCELED_HUMAN_INITIATED:Lcom/squareup/api/ApiErrorResult;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->BUYER_FLOW_CANCELED_READER_INITIATED:Lcom/squareup/api/ApiErrorResult;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->BUYER_FLOW_CANCELED_UNEXPECTED:Lcom/squareup/api/ApiErrorResult;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->CANNOT_ENCODE_CERTIFICATE:Lcom/squareup/api/ApiErrorResult;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->CANNOT_PARSE_CERTIFICATE:Lcom/squareup/api/ApiErrorResult;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->CUSTOMER_EXCEPTION_NOT_A_NETWORK_ERROR:Lcom/squareup/api/ApiErrorResult;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->CUSTOMER_INVALID_ID:Lcom/squareup/api/ApiErrorResult;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->CUSTOMER_NO_NETWORK:Lcom/squareup/api/ApiErrorResult;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->CUSTOMER_NO_SERVER:Lcom/squareup/api/ApiErrorResult;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->CUSTOMER_NOT_SUPPORTED:Lcom/squareup/api/ApiErrorResult;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->CUSTOMER_UNEXPECTED_HTTP_ERROR:Lcom/squareup/api/ApiErrorResult;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->CUSTOMER_UNKNOWN_ERROR:Lcom/squareup/api/ApiErrorResult;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->GET_LOCATION_DENIED:Lcom/squareup/api/ApiErrorResult;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->INVALID_AMOUNT_TOO_LOW:Lcom/squareup/api/ApiErrorResult;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->INVALID_AMOUNT_TOO_HIGH:Lcom/squareup/api/ApiErrorResult;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->INVALID_API_VERSION:Lcom/squareup/api/ApiErrorResult;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->INVALID_CHARGE_AMOUNT:Lcom/squareup/api/ApiErrorResult;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->INVALID_CURRENCY:Lcom/squareup/api/ApiErrorResult;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->INVALID_MERCHANT_ID:Lcom/squareup/api/ApiErrorResult;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->INVALID_PACKAGE:Lcom/squareup/api/ApiErrorResult;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->INVALID_SANDBOX_CLIENT_ID:Lcom/squareup/api/ApiErrorResult;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->INVALID_START_METHOD:Lcom/squareup/api/ApiErrorResult;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->INVALID_TIMEOUT:Lcom/squareup/api/ApiErrorResult;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->INVALID_WEB_CALLBACK_URI:Lcom/squareup/api/ApiErrorResult;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->MISSING_CHARGE_AMOUNT:Lcom/squareup/api/ApiErrorResult;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->MISSING_API_VERSION:Lcom/squareup/api/ApiErrorResult;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->MISSING_CLIENT_ID:Lcom/squareup/api/ApiErrorResult;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->MISSING_CURRENCY:Lcom/squareup/api/ApiErrorResult;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->MISSING_SIGNATURES:Lcom/squareup/api/ApiErrorResult;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->MISSING_TENDER_TYPE:Lcom/squareup/api/ApiErrorResult;

    const/16 v2, 0x20

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->MISSING_WEB_CALLBACK_URI:Lcom/squareup/api/ApiErrorResult;

    const/16 v2, 0x21

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->NO_EMPLOYEE_LOGGED_IN:Lcom/squareup/api/ApiErrorResult;

    const/16 v2, 0x22

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->NO_RESULT_AVAILABLE:Lcom/squareup/api/ApiErrorResult;

    const/16 v2, 0x23

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->NOTE_TOO_LONG:Lcom/squareup/api/ApiErrorResult;

    const/16 v2, 0x24

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->SETTINGS_EXCEPTION_NOT_A_NETWORK_ERROR:Lcom/squareup/api/ApiErrorResult;

    const/16 v2, 0x25

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->SETTINGS_INVALID_CLIENT_ID:Lcom/squareup/api/ApiErrorResult;

    const/16 v2, 0x26

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->SETTINGS_NO_NETWORK:Lcom/squareup/api/ApiErrorResult;

    const/16 v2, 0x27

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->SETTINGS_NO_SERVER:Lcom/squareup/api/ApiErrorResult;

    const/16 v2, 0x28

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->SETTINGS_UNEXPECTED_HTTP_ERROR:Lcom/squareup/api/ApiErrorResult;

    const/16 v2, 0x29

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->SETTINGS_UNKNOWN_ERROR:Lcom/squareup/api/ApiErrorResult;

    const/16 v2, 0x2a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->SHA1_NOT_AVAILABLE:Lcom/squareup/api/ApiErrorResult;

    const/16 v2, 0x2b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->STALE_TRANSACTION_REQUEST:Lcom/squareup/api/ApiErrorResult;

    const/16 v2, 0x2c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->TENDER_FLOW_CANCELED:Lcom/squareup/api/ApiErrorResult;

    const/16 v2, 0x2d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->TIMEOUT_TOO_LOW:Lcom/squareup/api/ApiErrorResult;

    const/16 v2, 0x2e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->TIMEOUT_TOO_HIGH:Lcom/squareup/api/ApiErrorResult;

    const/16 v2, 0x2f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->TRANSACTION_IN_PROGRESS:Lcom/squareup/api/ApiErrorResult;

    const/16 v2, 0x30

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->UNEXPECTED_HOME_SCREEN:Lcom/squareup/api/ApiErrorResult;

    const/16 v2, 0x31

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->UNKNOWN_FINGERPRINT:Lcom/squareup/api/ApiErrorResult;

    const/16 v2, 0x32

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->UNSUPPORTED_API_VERSION:Lcom/squareup/api/ApiErrorResult;

    const/16 v2, 0x33

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->UNSUPPORTED_WEB_API_VERSION:Lcom/squareup/api/ApiErrorResult;

    const/16 v2, 0x34

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->ADD_CARD_ON_FILE_CANCELED:Lcom/squareup/api/ApiErrorResult;

    const/16 v2, 0x35

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->USER_NOT_ACTIVATED:Lcom/squareup/api/ApiErrorResult;

    const/16 v2, 0x36

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->USER_NOT_LOGGED_IN:Lcom/squareup/api/ApiErrorResult;

    const/16 v2, 0x37

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->VALIDATION_CANCELED:Lcom/squareup/api/ApiErrorResult;

    const/16 v2, 0x38

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->X509_NOT_AVAILABLE:Lcom/squareup/api/ApiErrorResult;

    const/16 v2, 0x39

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/api/ApiErrorResult;->$VALUES:[Lcom/squareup/api/ApiErrorResult;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .line 113
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 114
    iput-object p3, p0, Lcom/squareup/api/ApiErrorResult;->errorCode:Ljava/lang/String;

    .line 115
    iput p4, p0, Lcom/squareup/api/ApiErrorResult;->errorDescriptionResourceId:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/api/ApiErrorResult;
    .locals 1

    .line 28
    const-class v0, Lcom/squareup/api/ApiErrorResult;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/api/ApiErrorResult;

    return-object p0
.end method

.method public static values()[Lcom/squareup/api/ApiErrorResult;
    .locals 1

    .line 28
    sget-object v0, Lcom/squareup/api/ApiErrorResult;->$VALUES:[Lcom/squareup/api/ApiErrorResult;

    invoke-virtual {v0}, [Lcom/squareup/api/ApiErrorResult;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/api/ApiErrorResult;

    return-object v0
.end method
