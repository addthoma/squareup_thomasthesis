.class public Lcom/squareup/api/NewStoreCardEvent;
.super Lcom/squareup/api/RegisterApiEvent;
.source "NewStoreCardEvent.java"


# instance fields
.field public final cold_start:Z

.field private final sequenceUuid:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "api_sequence_uuid"
    .end annotation
.end field

.field public final startup_duration_millis:J


# direct methods
.method public constructor <init>(Lcom/squareup/util/Clock;Landroid/content/Intent;Ljava/lang/String;ZJJLcom/squareup/api/RequestParams;)V
    .locals 6

    .line 16
    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->API_NEW_STORE_CARD:Lcom/squareup/analytics/RegisterActionName;

    iget-object p2, p9, Lcom/squareup/api/RequestParams;->clientInfo:Lcom/squareup/api/ClientInfo;

    iget-object v3, p2, Lcom/squareup/api/ClientInfo;->clientId:Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move-wide v4, p7

    invoke-direct/range {v0 .. v5}, Lcom/squareup/api/RegisterApiEvent;-><init>(Lcom/squareup/util/Clock;Lcom/squareup/analytics/RegisterActionName;Ljava/lang/String;J)V

    .line 18
    iput-object p3, p0, Lcom/squareup/api/NewStoreCardEvent;->sequenceUuid:Ljava/lang/String;

    .line 19
    iput-boolean p4, p0, Lcom/squareup/api/NewStoreCardEvent;->cold_start:Z

    .line 20
    iput-wide p5, p0, Lcom/squareup/api/NewStoreCardEvent;->startup_duration_millis:J

    return-void
.end method
