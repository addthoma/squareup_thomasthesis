.class public final Lcom/squareup/api/items/TryToUsePromoResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "TryToUsePromoResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/TryToUsePromoResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/items/TryToUsePromoResponse;",
        "Lcom/squareup/api/items/TryToUsePromoResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public promo_consumption_result:Lcom/squareup/api/items/PromoConsumptionResult;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 88
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/api/items/TryToUsePromoResponse;
    .locals 3

    .line 106
    new-instance v0, Lcom/squareup/api/items/TryToUsePromoResponse;

    iget-object v1, p0, Lcom/squareup/api/items/TryToUsePromoResponse$Builder;->promo_consumption_result:Lcom/squareup/api/items/PromoConsumptionResult;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/api/items/TryToUsePromoResponse;-><init>(Lcom/squareup/api/items/PromoConsumptionResult;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 85
    invoke-virtual {p0}, Lcom/squareup/api/items/TryToUsePromoResponse$Builder;->build()Lcom/squareup/api/items/TryToUsePromoResponse;

    move-result-object v0

    return-object v0
.end method

.method public promo_consumption_result(Lcom/squareup/api/items/PromoConsumptionResult;)Lcom/squareup/api/items/TryToUsePromoResponse$Builder;
    .locals 0

    .line 100
    iput-object p1, p0, Lcom/squareup/api/items/TryToUsePromoResponse$Builder;->promo_consumption_result:Lcom/squareup/api/items/PromoConsumptionResult;

    return-object p0
.end method
