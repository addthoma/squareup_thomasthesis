.class public final Lcom/squareup/api/items/Promo;
.super Lcom/squareup/wire/Message;
.source "Promo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/items/Promo$ProtoAdapter_Promo;,
        Lcom/squareup/api/items/Promo$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/api/items/Promo;",
        "Lcom/squareup/api/items/Promo$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/items/Promo;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ACTIVE:Ljava/lang/Boolean;

.field public static final DEFAULT_ENDS_AT:Ljava/lang/Long;

.field public static final DEFAULT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_LIMITED_USE:Ljava/lang/Boolean;

.field public static final DEFAULT_NUM_TIMES_ALLOWED:Ljava/lang/Integer;

.field public static final DEFAULT_NUM_TIMES_USED:Ljava/lang/Integer;

.field public static final DEFAULT_PROMO_CODE:Ljava/lang/String; = ""

.field public static final DEFAULT_STARTS_AT:Ljava/lang/Long;

.field private static final serialVersionUID:J


# instance fields
.field public final active:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x8
    .end annotation
.end field

.field public final discount:Lcom/squareup/api/sync/ObjectId;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.sync.ObjectId#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final ends_at:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x7
    .end annotation
.end field

.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final limited_use:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x9
    .end annotation
.end field

.field public final num_times_allowed:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x4
    .end annotation
.end field

.field public final num_times_used:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x5
    .end annotation
.end field

.field public final promo_code:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final starts_at:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x6
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 24
    new-instance v0, Lcom/squareup/api/items/Promo$ProtoAdapter_Promo;

    invoke-direct {v0}, Lcom/squareup/api/items/Promo$ProtoAdapter_Promo;-><init>()V

    sput-object v0, Lcom/squareup/api/items/Promo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 32
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sput-object v1, Lcom/squareup/api/items/Promo;->DEFAULT_NUM_TIMES_ALLOWED:Ljava/lang/Integer;

    .line 34
    sput-object v1, Lcom/squareup/api/items/Promo;->DEFAULT_NUM_TIMES_USED:Ljava/lang/Integer;

    const-wide/16 v1, 0x0

    .line 36
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    sput-object v1, Lcom/squareup/api/items/Promo;->DEFAULT_STARTS_AT:Ljava/lang/Long;

    .line 38
    sput-object v1, Lcom/squareup/api/items/Promo;->DEFAULT_ENDS_AT:Ljava/lang/Long;

    const/4 v1, 0x1

    .line 40
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    sput-object v1, Lcom/squareup/api/items/Promo;->DEFAULT_ACTIVE:Ljava/lang/Boolean;

    .line 42
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/api/items/Promo;->DEFAULT_LIMITED_USE:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/api/sync/ObjectId;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 11

    .line 109
    sget-object v10, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/api/items/Promo;-><init>(Ljava/lang/String;Lcom/squareup/api/sync/ObjectId;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/api/sync/ObjectId;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 115
    sget-object v0, Lcom/squareup/api/items/Promo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p10}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 116
    iput-object p1, p0, Lcom/squareup/api/items/Promo;->id:Ljava/lang/String;

    .line 117
    iput-object p2, p0, Lcom/squareup/api/items/Promo;->discount:Lcom/squareup/api/sync/ObjectId;

    .line 118
    iput-object p3, p0, Lcom/squareup/api/items/Promo;->promo_code:Ljava/lang/String;

    .line 119
    iput-object p4, p0, Lcom/squareup/api/items/Promo;->num_times_allowed:Ljava/lang/Integer;

    .line 120
    iput-object p5, p0, Lcom/squareup/api/items/Promo;->num_times_used:Ljava/lang/Integer;

    .line 121
    iput-object p6, p0, Lcom/squareup/api/items/Promo;->starts_at:Ljava/lang/Long;

    .line 122
    iput-object p7, p0, Lcom/squareup/api/items/Promo;->ends_at:Ljava/lang/Long;

    .line 123
    iput-object p8, p0, Lcom/squareup/api/items/Promo;->active:Ljava/lang/Boolean;

    .line 124
    iput-object p9, p0, Lcom/squareup/api/items/Promo;->limited_use:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 146
    :cond_0
    instance-of v1, p1, Lcom/squareup/api/items/Promo;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 147
    :cond_1
    check-cast p1, Lcom/squareup/api/items/Promo;

    .line 148
    invoke-virtual {p0}, Lcom/squareup/api/items/Promo;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/api/items/Promo;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Promo;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/Promo;->id:Ljava/lang/String;

    .line 149
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Promo;->discount:Lcom/squareup/api/sync/ObjectId;

    iget-object v3, p1, Lcom/squareup/api/items/Promo;->discount:Lcom/squareup/api/sync/ObjectId;

    .line 150
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Promo;->promo_code:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/Promo;->promo_code:Ljava/lang/String;

    .line 151
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Promo;->num_times_allowed:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/api/items/Promo;->num_times_allowed:Ljava/lang/Integer;

    .line 152
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Promo;->num_times_used:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/api/items/Promo;->num_times_used:Ljava/lang/Integer;

    .line 153
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Promo;->starts_at:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/api/items/Promo;->starts_at:Ljava/lang/Long;

    .line 154
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Promo;->ends_at:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/api/items/Promo;->ends_at:Ljava/lang/Long;

    .line 155
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Promo;->active:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/api/items/Promo;->active:Ljava/lang/Boolean;

    .line 156
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Promo;->limited_use:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/api/items/Promo;->limited_use:Ljava/lang/Boolean;

    .line 157
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 162
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_9

    .line 164
    invoke-virtual {p0}, Lcom/squareup/api/items/Promo;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 165
    iget-object v1, p0, Lcom/squareup/api/items/Promo;->id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 166
    iget-object v1, p0, Lcom/squareup/api/items/Promo;->discount:Lcom/squareup/api/sync/ObjectId;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectId;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 167
    iget-object v1, p0, Lcom/squareup/api/items/Promo;->promo_code:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 168
    iget-object v1, p0, Lcom/squareup/api/items/Promo;->num_times_allowed:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 169
    iget-object v1, p0, Lcom/squareup/api/items/Promo;->num_times_used:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 170
    iget-object v1, p0, Lcom/squareup/api/items/Promo;->starts_at:Ljava/lang/Long;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 171
    iget-object v1, p0, Lcom/squareup/api/items/Promo;->ends_at:Ljava/lang/Long;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 172
    iget-object v1, p0, Lcom/squareup/api/items/Promo;->active:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 173
    iget-object v1, p0, Lcom/squareup/api/items/Promo;->limited_use:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_8
    add-int/2addr v0, v2

    .line 174
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_9
    return v0
.end method

.method public newBuilder()Lcom/squareup/api/items/Promo$Builder;
    .locals 2

    .line 129
    new-instance v0, Lcom/squareup/api/items/Promo$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/Promo$Builder;-><init>()V

    .line 130
    iget-object v1, p0, Lcom/squareup/api/items/Promo;->id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/Promo$Builder;->id:Ljava/lang/String;

    .line 131
    iget-object v1, p0, Lcom/squareup/api/items/Promo;->discount:Lcom/squareup/api/sync/ObjectId;

    iput-object v1, v0, Lcom/squareup/api/items/Promo$Builder;->discount:Lcom/squareup/api/sync/ObjectId;

    .line 132
    iget-object v1, p0, Lcom/squareup/api/items/Promo;->promo_code:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/Promo$Builder;->promo_code:Ljava/lang/String;

    .line 133
    iget-object v1, p0, Lcom/squareup/api/items/Promo;->num_times_allowed:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/api/items/Promo$Builder;->num_times_allowed:Ljava/lang/Integer;

    .line 134
    iget-object v1, p0, Lcom/squareup/api/items/Promo;->num_times_used:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/api/items/Promo$Builder;->num_times_used:Ljava/lang/Integer;

    .line 135
    iget-object v1, p0, Lcom/squareup/api/items/Promo;->starts_at:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/api/items/Promo$Builder;->starts_at:Ljava/lang/Long;

    .line 136
    iget-object v1, p0, Lcom/squareup/api/items/Promo;->ends_at:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/api/items/Promo$Builder;->ends_at:Ljava/lang/Long;

    .line 137
    iget-object v1, p0, Lcom/squareup/api/items/Promo;->active:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/api/items/Promo$Builder;->active:Ljava/lang/Boolean;

    .line 138
    iget-object v1, p0, Lcom/squareup/api/items/Promo;->limited_use:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/api/items/Promo$Builder;->limited_use:Ljava/lang/Boolean;

    .line 139
    invoke-virtual {p0}, Lcom/squareup/api/items/Promo;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/Promo$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/api/items/Promo;->newBuilder()Lcom/squareup/api/items/Promo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 181
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 182
    iget-object v1, p0, Lcom/squareup/api/items/Promo;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Promo;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 183
    :cond_0
    iget-object v1, p0, Lcom/squareup/api/items/Promo;->discount:Lcom/squareup/api/sync/ObjectId;

    if-eqz v1, :cond_1

    const-string v1, ", discount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Promo;->discount:Lcom/squareup/api/sync/ObjectId;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 184
    :cond_1
    iget-object v1, p0, Lcom/squareup/api/items/Promo;->promo_code:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", promo_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Promo;->promo_code:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 185
    :cond_2
    iget-object v1, p0, Lcom/squareup/api/items/Promo;->num_times_allowed:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    const-string v1, ", num_times_allowed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Promo;->num_times_allowed:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 186
    :cond_3
    iget-object v1, p0, Lcom/squareup/api/items/Promo;->num_times_used:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    const-string v1, ", num_times_used="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Promo;->num_times_used:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 187
    :cond_4
    iget-object v1, p0, Lcom/squareup/api/items/Promo;->starts_at:Ljava/lang/Long;

    if-eqz v1, :cond_5

    const-string v1, ", starts_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Promo;->starts_at:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 188
    :cond_5
    iget-object v1, p0, Lcom/squareup/api/items/Promo;->ends_at:Ljava/lang/Long;

    if-eqz v1, :cond_6

    const-string v1, ", ends_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Promo;->ends_at:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 189
    :cond_6
    iget-object v1, p0, Lcom/squareup/api/items/Promo;->active:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    const-string v1, ", active="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Promo;->active:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 190
    :cond_7
    iget-object v1, p0, Lcom/squareup/api/items/Promo;->limited_use:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    const-string v1, ", limited_use="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Promo;->limited_use:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_8
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Promo{"

    .line 191
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
