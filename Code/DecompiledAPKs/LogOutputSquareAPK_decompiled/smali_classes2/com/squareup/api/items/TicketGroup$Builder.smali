.class public final Lcom/squareup/api/items/TicketGroup$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "TicketGroup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/TicketGroup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/items/TicketGroup;",
        "Lcom/squareup/api/items/TicketGroup$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public id:Ljava/lang/String;

.field public label:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public naming_method:Lcom/squareup/api/items/TicketGroup$NamingMethod;

.field public ordinal:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 155
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/api/items/TicketGroup;
    .locals 8

    .line 192
    new-instance v7, Lcom/squareup/api/items/TicketGroup;

    iget-object v1, p0, Lcom/squareup/api/items/TicketGroup$Builder;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/api/items/TicketGroup$Builder;->name:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/api/items/TicketGroup$Builder;->ordinal:Ljava/lang/Integer;

    iget-object v4, p0, Lcom/squareup/api/items/TicketGroup$Builder;->naming_method:Lcom/squareup/api/items/TicketGroup$NamingMethod;

    iget-object v5, p0, Lcom/squareup/api/items/TicketGroup$Builder;->label:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/api/items/TicketGroup;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/squareup/api/items/TicketGroup$NamingMethod;Ljava/lang/String;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 144
    invoke-virtual {p0}, Lcom/squareup/api/items/TicketGroup$Builder;->build()Lcom/squareup/api/items/TicketGroup;

    move-result-object v0

    return-object v0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/api/items/TicketGroup$Builder;
    .locals 0

    .line 159
    iput-object p1, p0, Lcom/squareup/api/items/TicketGroup$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public label(Ljava/lang/String;)Lcom/squareup/api/items/TicketGroup$Builder;
    .locals 0

    .line 186
    iput-object p1, p0, Lcom/squareup/api/items/TicketGroup$Builder;->label:Ljava/lang/String;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/api/items/TicketGroup$Builder;
    .locals 0

    .line 164
    iput-object p1, p0, Lcom/squareup/api/items/TicketGroup$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public naming_method(Lcom/squareup/api/items/TicketGroup$NamingMethod;)Lcom/squareup/api/items/TicketGroup$Builder;
    .locals 0

    .line 177
    iput-object p1, p0, Lcom/squareup/api/items/TicketGroup$Builder;->naming_method:Lcom/squareup/api/items/TicketGroup$NamingMethod;

    return-object p0
.end method

.method public ordinal(Ljava/lang/Integer;)Lcom/squareup/api/items/TicketGroup$Builder;
    .locals 0

    .line 169
    iput-object p1, p0, Lcom/squareup/api/items/TicketGroup$Builder;->ordinal:Ljava/lang/Integer;

    return-object p0
.end method
