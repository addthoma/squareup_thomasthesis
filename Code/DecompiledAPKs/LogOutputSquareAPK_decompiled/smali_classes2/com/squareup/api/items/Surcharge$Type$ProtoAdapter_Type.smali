.class final Lcom/squareup/api/items/Surcharge$Type$ProtoAdapter_Type;
.super Lcom/squareup/wire/EnumAdapter;
.source "Surcharge.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/Surcharge$Type;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/EnumAdapter<",
        "Lcom/squareup/api/items/Surcharge$Type;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .line 321
    const-class v0, Lcom/squareup/api/items/Surcharge$Type;

    invoke-direct {p0, v0}, Lcom/squareup/wire/EnumAdapter;-><init>(Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method protected fromValue(I)Lcom/squareup/api/items/Surcharge$Type;
    .locals 0

    .line 326
    invoke-static {p1}, Lcom/squareup/api/items/Surcharge$Type;->fromValue(I)Lcom/squareup/api/items/Surcharge$Type;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic fromValue(I)Lcom/squareup/wire/WireEnum;
    .locals 0

    .line 319
    invoke-virtual {p0, p1}, Lcom/squareup/api/items/Surcharge$Type$ProtoAdapter_Type;->fromValue(I)Lcom/squareup/api/items/Surcharge$Type;

    move-result-object p1

    return-object p1
.end method
