.class public final enum Lcom/squareup/api/items/ItemModifierList$SelectionType;
.super Ljava/lang/Enum;
.source "ItemModifierList.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/ItemModifierList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SelectionType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/items/ItemModifierList$SelectionType$ProtoAdapter_SelectionType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/api/items/ItemModifierList$SelectionType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/api/items/ItemModifierList$SelectionType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/items/ItemModifierList$SelectionType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum MULTIPLE:Lcom/squareup/api/items/ItemModifierList$SelectionType;

.field public static final enum SINGLE:Lcom/squareup/api/items/ItemModifierList$SelectionType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 252
    new-instance v0, Lcom/squareup/api/items/ItemModifierList$SelectionType;

    const/4 v1, 0x0

    const-string v2, "SINGLE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/api/items/ItemModifierList$SelectionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/ItemModifierList$SelectionType;->SINGLE:Lcom/squareup/api/items/ItemModifierList$SelectionType;

    .line 257
    new-instance v0, Lcom/squareup/api/items/ItemModifierList$SelectionType;

    const/4 v2, 0x1

    const-string v3, "MULTIPLE"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/api/items/ItemModifierList$SelectionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/ItemModifierList$SelectionType;->MULTIPLE:Lcom/squareup/api/items/ItemModifierList$SelectionType;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/api/items/ItemModifierList$SelectionType;

    .line 248
    sget-object v3, Lcom/squareup/api/items/ItemModifierList$SelectionType;->SINGLE:Lcom/squareup/api/items/ItemModifierList$SelectionType;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/api/items/ItemModifierList$SelectionType;->MULTIPLE:Lcom/squareup/api/items/ItemModifierList$SelectionType;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/api/items/ItemModifierList$SelectionType;->$VALUES:[Lcom/squareup/api/items/ItemModifierList$SelectionType;

    .line 259
    new-instance v0, Lcom/squareup/api/items/ItemModifierList$SelectionType$ProtoAdapter_SelectionType;

    invoke-direct {v0}, Lcom/squareup/api/items/ItemModifierList$SelectionType$ProtoAdapter_SelectionType;-><init>()V

    sput-object v0, Lcom/squareup/api/items/ItemModifierList$SelectionType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 263
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 264
    iput p3, p0, Lcom/squareup/api/items/ItemModifierList$SelectionType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/api/items/ItemModifierList$SelectionType;
    .locals 1

    if-eqz p0, :cond_1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 273
    :cond_0
    sget-object p0, Lcom/squareup/api/items/ItemModifierList$SelectionType;->MULTIPLE:Lcom/squareup/api/items/ItemModifierList$SelectionType;

    return-object p0

    .line 272
    :cond_1
    sget-object p0, Lcom/squareup/api/items/ItemModifierList$SelectionType;->SINGLE:Lcom/squareup/api/items/ItemModifierList$SelectionType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/api/items/ItemModifierList$SelectionType;
    .locals 1

    .line 248
    const-class v0, Lcom/squareup/api/items/ItemModifierList$SelectionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/api/items/ItemModifierList$SelectionType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/api/items/ItemModifierList$SelectionType;
    .locals 1

    .line 248
    sget-object v0, Lcom/squareup/api/items/ItemModifierList$SelectionType;->$VALUES:[Lcom/squareup/api/items/ItemModifierList$SelectionType;

    invoke-virtual {v0}, [Lcom/squareup/api/items/ItemModifierList$SelectionType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/api/items/ItemModifierList$SelectionType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 280
    iget v0, p0, Lcom/squareup/api/items/ItemModifierList$SelectionType;->value:I

    return v0
.end method
