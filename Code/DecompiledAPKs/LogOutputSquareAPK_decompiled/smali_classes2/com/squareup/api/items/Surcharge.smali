.class public final Lcom/squareup/api/items/Surcharge;
.super Lcom/squareup/wire/Message;
.source "Surcharge.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/items/Surcharge$ProtoAdapter_Surcharge;,
        Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;,
        Lcom/squareup/api/items/Surcharge$Type;,
        Lcom/squareup/api/items/Surcharge$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/api/items/Surcharge;",
        "Lcom/squareup/api/items/Surcharge$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/items/Surcharge;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CALCULATION_PHASE:Lcom/squareup/api/items/CalculationPhase;

.field public static final DEFAULT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_PERCENTAGE:Ljava/lang/String; = ""

.field public static final DEFAULT_TAXABLE:Ljava/lang/Boolean;

.field public static final DEFAULT_TYPE:Lcom/squareup/api/items/Surcharge$Type;

.field private static final serialVersionUID:J


# instance fields
.field public final amount:Lcom/squareup/protos/common/dinero/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.dinero.Money#ADAPTER"
        tag = 0x8
    .end annotation
.end field

.field public final auto_gratuity:Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.Surcharge$AutoGratuitySettings#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final calculation_phase:Lcom/squareup/api/items/CalculationPhase;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.CalculationPhase#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.MerchantCatalogObjectReference#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final percentage:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final taxable:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5
    .end annotation
.end field

.field public final type:Lcom/squareup/api/items/Surcharge$Type;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.Surcharge$Type#ADAPTER"
        tag = 0x4
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 32
    new-instance v0, Lcom/squareup/api/items/Surcharge$ProtoAdapter_Surcharge;

    invoke-direct {v0}, Lcom/squareup/api/items/Surcharge$ProtoAdapter_Surcharge;-><init>()V

    sput-object v0, Lcom/squareup/api/items/Surcharge;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 42
    sget-object v0, Lcom/squareup/api/items/Surcharge$Type;->UNKNOWN:Lcom/squareup/api/items/Surcharge$Type;

    sput-object v0, Lcom/squareup/api/items/Surcharge;->DEFAULT_TYPE:Lcom/squareup/api/items/Surcharge$Type;

    const/4 v0, 0x1

    .line 44
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/api/items/Surcharge;->DEFAULT_TAXABLE:Ljava/lang/Boolean;

    .line 46
    sget-object v0, Lcom/squareup/api/items/CalculationPhase;->SURCHARGE_PHASE:Lcom/squareup/api/items/CalculationPhase;

    sput-object v0, Lcom/squareup/api/items/Surcharge;->DEFAULT_CALCULATION_PHASE:Lcom/squareup/api/items/CalculationPhase;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/dinero/Money;Lcom/squareup/api/items/Surcharge$Type;Ljava/lang/Boolean;Lcom/squareup/api/items/CalculationPhase;Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;Lcom/squareup/api/items/MerchantCatalogObjectReference;)V
    .locals 11

    .line 115
    sget-object v10, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/api/items/Surcharge;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/dinero/Money;Lcom/squareup/api/items/Surcharge$Type;Ljava/lang/Boolean;Lcom/squareup/api/items/CalculationPhase;Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;Lcom/squareup/api/items/MerchantCatalogObjectReference;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/dinero/Money;Lcom/squareup/api/items/Surcharge$Type;Ljava/lang/Boolean;Lcom/squareup/api/items/CalculationPhase;Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;Lcom/squareup/api/items/MerchantCatalogObjectReference;Lokio/ByteString;)V
    .locals 1

    .line 121
    sget-object v0, Lcom/squareup/api/items/Surcharge;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p10}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 122
    iput-object p1, p0, Lcom/squareup/api/items/Surcharge;->id:Ljava/lang/String;

    .line 123
    iput-object p2, p0, Lcom/squareup/api/items/Surcharge;->name:Ljava/lang/String;

    .line 124
    iput-object p3, p0, Lcom/squareup/api/items/Surcharge;->percentage:Ljava/lang/String;

    .line 125
    iput-object p4, p0, Lcom/squareup/api/items/Surcharge;->amount:Lcom/squareup/protos/common/dinero/Money;

    .line 126
    iput-object p5, p0, Lcom/squareup/api/items/Surcharge;->type:Lcom/squareup/api/items/Surcharge$Type;

    .line 127
    iput-object p6, p0, Lcom/squareup/api/items/Surcharge;->taxable:Ljava/lang/Boolean;

    .line 128
    iput-object p7, p0, Lcom/squareup/api/items/Surcharge;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    .line 129
    iput-object p8, p0, Lcom/squareup/api/items/Surcharge;->auto_gratuity:Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;

    .line 130
    iput-object p9, p0, Lcom/squareup/api/items/Surcharge;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 152
    :cond_0
    instance-of v1, p1, Lcom/squareup/api/items/Surcharge;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 153
    :cond_1
    check-cast p1, Lcom/squareup/api/items/Surcharge;

    .line 154
    invoke-virtual {p0}, Lcom/squareup/api/items/Surcharge;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/api/items/Surcharge;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Surcharge;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/Surcharge;->id:Ljava/lang/String;

    .line 155
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Surcharge;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/Surcharge;->name:Ljava/lang/String;

    .line 156
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Surcharge;->percentage:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/Surcharge;->percentage:Ljava/lang/String;

    .line 157
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Surcharge;->amount:Lcom/squareup/protos/common/dinero/Money;

    iget-object v3, p1, Lcom/squareup/api/items/Surcharge;->amount:Lcom/squareup/protos/common/dinero/Money;

    .line 158
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Surcharge;->type:Lcom/squareup/api/items/Surcharge$Type;

    iget-object v3, p1, Lcom/squareup/api/items/Surcharge;->type:Lcom/squareup/api/items/Surcharge$Type;

    .line 159
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Surcharge;->taxable:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/api/items/Surcharge;->taxable:Ljava/lang/Boolean;

    .line 160
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Surcharge;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    iget-object v3, p1, Lcom/squareup/api/items/Surcharge;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    .line 161
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Surcharge;->auto_gratuity:Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;

    iget-object v3, p1, Lcom/squareup/api/items/Surcharge;->auto_gratuity:Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;

    .line 162
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Surcharge;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    iget-object p1, p1, Lcom/squareup/api/items/Surcharge;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    .line 163
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 168
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_9

    .line 170
    invoke-virtual {p0}, Lcom/squareup/api/items/Surcharge;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 171
    iget-object v1, p0, Lcom/squareup/api/items/Surcharge;->id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 172
    iget-object v1, p0, Lcom/squareup/api/items/Surcharge;->name:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 173
    iget-object v1, p0, Lcom/squareup/api/items/Surcharge;->percentage:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 174
    iget-object v1, p0, Lcom/squareup/api/items/Surcharge;->amount:Lcom/squareup/protos/common/dinero/Money;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/common/dinero/Money;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 175
    iget-object v1, p0, Lcom/squareup/api/items/Surcharge;->type:Lcom/squareup/api/items/Surcharge$Type;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/api/items/Surcharge$Type;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 176
    iget-object v1, p0, Lcom/squareup/api/items/Surcharge;->taxable:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 177
    iget-object v1, p0, Lcom/squareup/api/items/Surcharge;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/api/items/CalculationPhase;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 178
    iget-object v1, p0, Lcom/squareup/api/items/Surcharge;->auto_gratuity:Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 179
    iget-object v1, p0, Lcom/squareup/api/items/Surcharge;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/api/items/MerchantCatalogObjectReference;->hashCode()I

    move-result v2

    :cond_8
    add-int/2addr v0, v2

    .line 180
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_9
    return v0
.end method

.method public newBuilder()Lcom/squareup/api/items/Surcharge$Builder;
    .locals 2

    .line 135
    new-instance v0, Lcom/squareup/api/items/Surcharge$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/Surcharge$Builder;-><init>()V

    .line 136
    iget-object v1, p0, Lcom/squareup/api/items/Surcharge;->id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/Surcharge$Builder;->id:Ljava/lang/String;

    .line 137
    iget-object v1, p0, Lcom/squareup/api/items/Surcharge;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/Surcharge$Builder;->name:Ljava/lang/String;

    .line 138
    iget-object v1, p0, Lcom/squareup/api/items/Surcharge;->percentage:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/Surcharge$Builder;->percentage:Ljava/lang/String;

    .line 139
    iget-object v1, p0, Lcom/squareup/api/items/Surcharge;->amount:Lcom/squareup/protos/common/dinero/Money;

    iput-object v1, v0, Lcom/squareup/api/items/Surcharge$Builder;->amount:Lcom/squareup/protos/common/dinero/Money;

    .line 140
    iget-object v1, p0, Lcom/squareup/api/items/Surcharge;->type:Lcom/squareup/api/items/Surcharge$Type;

    iput-object v1, v0, Lcom/squareup/api/items/Surcharge$Builder;->type:Lcom/squareup/api/items/Surcharge$Type;

    .line 141
    iget-object v1, p0, Lcom/squareup/api/items/Surcharge;->taxable:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/api/items/Surcharge$Builder;->taxable:Ljava/lang/Boolean;

    .line 142
    iget-object v1, p0, Lcom/squareup/api/items/Surcharge;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    iput-object v1, v0, Lcom/squareup/api/items/Surcharge$Builder;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    .line 143
    iget-object v1, p0, Lcom/squareup/api/items/Surcharge;->auto_gratuity:Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;

    iput-object v1, v0, Lcom/squareup/api/items/Surcharge$Builder;->auto_gratuity:Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;

    .line 144
    iget-object v1, p0, Lcom/squareup/api/items/Surcharge;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    iput-object v1, v0, Lcom/squareup/api/items/Surcharge$Builder;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    .line 145
    invoke-virtual {p0}, Lcom/squareup/api/items/Surcharge;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/Surcharge$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 31
    invoke-virtual {p0}, Lcom/squareup/api/items/Surcharge;->newBuilder()Lcom/squareup/api/items/Surcharge$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 187
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 188
    iget-object v1, p0, Lcom/squareup/api/items/Surcharge;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Surcharge;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 189
    :cond_0
    iget-object v1, p0, Lcom/squareup/api/items/Surcharge;->name:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Surcharge;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 190
    :cond_1
    iget-object v1, p0, Lcom/squareup/api/items/Surcharge;->percentage:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", percentage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Surcharge;->percentage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 191
    :cond_2
    iget-object v1, p0, Lcom/squareup/api/items/Surcharge;->amount:Lcom/squareup/protos/common/dinero/Money;

    if-eqz v1, :cond_3

    const-string v1, ", amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Surcharge;->amount:Lcom/squareup/protos/common/dinero/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 192
    :cond_3
    iget-object v1, p0, Lcom/squareup/api/items/Surcharge;->type:Lcom/squareup/api/items/Surcharge$Type;

    if-eqz v1, :cond_4

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Surcharge;->type:Lcom/squareup/api/items/Surcharge$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 193
    :cond_4
    iget-object v1, p0, Lcom/squareup/api/items/Surcharge;->taxable:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    const-string v1, ", taxable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Surcharge;->taxable:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 194
    :cond_5
    iget-object v1, p0, Lcom/squareup/api/items/Surcharge;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    if-eqz v1, :cond_6

    const-string v1, ", calculation_phase="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Surcharge;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 195
    :cond_6
    iget-object v1, p0, Lcom/squareup/api/items/Surcharge;->auto_gratuity:Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;

    if-eqz v1, :cond_7

    const-string v1, ", auto_gratuity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Surcharge;->auto_gratuity:Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 196
    :cond_7
    iget-object v1, p0, Lcom/squareup/api/items/Surcharge;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    if-eqz v1, :cond_8

    const-string v1, ", catalog_object_reference="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Surcharge;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_8
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Surcharge{"

    .line 197
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
