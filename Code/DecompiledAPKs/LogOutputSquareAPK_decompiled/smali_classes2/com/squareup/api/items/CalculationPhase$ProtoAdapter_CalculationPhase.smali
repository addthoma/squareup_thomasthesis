.class final Lcom/squareup/api/items/CalculationPhase$ProtoAdapter_CalculationPhase;
.super Lcom/squareup/wire/EnumAdapter;
.source "CalculationPhase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/CalculationPhase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_CalculationPhase"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/EnumAdapter<",
        "Lcom/squareup/api/items/CalculationPhase;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .line 59
    const-class v0, Lcom/squareup/api/items/CalculationPhase;

    invoke-direct {p0, v0}, Lcom/squareup/wire/EnumAdapter;-><init>(Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method protected fromValue(I)Lcom/squareup/api/items/CalculationPhase;
    .locals 0

    .line 64
    invoke-static {p1}, Lcom/squareup/api/items/CalculationPhase;->fromValue(I)Lcom/squareup/api/items/CalculationPhase;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic fromValue(I)Lcom/squareup/wire/WireEnum;
    .locals 0

    .line 57
    invoke-virtual {p0, p1}, Lcom/squareup/api/items/CalculationPhase$ProtoAdapter_CalculationPhase;->fromValue(I)Lcom/squareup/api/items/CalculationPhase;

    move-result-object p1

    return-object p1
.end method
