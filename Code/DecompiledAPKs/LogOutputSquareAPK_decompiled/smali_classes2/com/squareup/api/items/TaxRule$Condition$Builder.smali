.class public final Lcom/squareup/api/items/TaxRule$Condition$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "TaxRule.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/TaxRule$Condition;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/items/TaxRule$Condition;",
        "Lcom/squareup/api/items/TaxRule$Condition$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public dining_option_predicate:Lcom/squareup/api/items/ObjectPredicate;

.field public max_item_price:Lcom/squareup/protos/common/dinero/Money;

.field public max_total_amount:Lcom/squareup/protos/common/dinero/Money;

.field public min_item_price:Lcom/squareup/protos/common/dinero/Money;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 405
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/api/items/TaxRule$Condition;
    .locals 7

    .line 466
    new-instance v6, Lcom/squareup/api/items/TaxRule$Condition;

    iget-object v1, p0, Lcom/squareup/api/items/TaxRule$Condition$Builder;->dining_option_predicate:Lcom/squareup/api/items/ObjectPredicate;

    iget-object v2, p0, Lcom/squareup/api/items/TaxRule$Condition$Builder;->max_total_amount:Lcom/squareup/protos/common/dinero/Money;

    iget-object v3, p0, Lcom/squareup/api/items/TaxRule$Condition$Builder;->min_item_price:Lcom/squareup/protos/common/dinero/Money;

    iget-object v4, p0, Lcom/squareup/api/items/TaxRule$Condition$Builder;->max_item_price:Lcom/squareup/protos/common/dinero/Money;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/api/items/TaxRule$Condition;-><init>(Lcom/squareup/api/items/ObjectPredicate;Lcom/squareup/protos/common/dinero/Money;Lcom/squareup/protos/common/dinero/Money;Lcom/squareup/protos/common/dinero/Money;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 396
    invoke-virtual {p0}, Lcom/squareup/api/items/TaxRule$Condition$Builder;->build()Lcom/squareup/api/items/TaxRule$Condition;

    move-result-object v0

    return-object v0
.end method

.method public dining_option_predicate(Lcom/squareup/api/items/ObjectPredicate;)Lcom/squareup/api/items/TaxRule$Condition$Builder;
    .locals 0

    .line 414
    iput-object p1, p0, Lcom/squareup/api/items/TaxRule$Condition$Builder;->dining_option_predicate:Lcom/squareup/api/items/ObjectPredicate;

    return-object p0
.end method

.method public max_item_price(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/api/items/TaxRule$Condition$Builder;
    .locals 0

    .line 460
    iput-object p1, p0, Lcom/squareup/api/items/TaxRule$Condition$Builder;->max_item_price:Lcom/squareup/protos/common/dinero/Money;

    return-object p0
.end method

.method public max_total_amount(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/api/items/TaxRule$Condition$Builder;
    .locals 0

    .line 428
    iput-object p1, p0, Lcom/squareup/api/items/TaxRule$Condition$Builder;->max_total_amount:Lcom/squareup/protos/common/dinero/Money;

    return-object p0
.end method

.method public min_item_price(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/api/items/TaxRule$Condition$Builder;
    .locals 0

    .line 444
    iput-object p1, p0, Lcom/squareup/api/items/TaxRule$Condition$Builder;->min_item_price:Lcom/squareup/protos/common/dinero/Money;

    return-object p0
.end method
