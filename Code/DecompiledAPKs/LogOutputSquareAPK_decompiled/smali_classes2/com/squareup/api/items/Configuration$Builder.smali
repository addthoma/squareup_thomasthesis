.class public final Lcom/squareup/api/items/Configuration$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Configuration.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/Configuration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/items/Configuration;",
        "Lcom/squareup/api/items/Configuration$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public id:Ljava/lang/String;

.field public page_layout:Lcom/squareup/api/items/PageLayout;

.field public void_reasons:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/api/items/VoidReason;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 128
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 129
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/api/items/Configuration$Builder;->void_reasons:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/api/items/Configuration;
    .locals 5

    .line 157
    new-instance v0, Lcom/squareup/api/items/Configuration;

    iget-object v1, p0, Lcom/squareup/api/items/Configuration$Builder;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/api/items/Configuration$Builder;->page_layout:Lcom/squareup/api/items/PageLayout;

    iget-object v3, p0, Lcom/squareup/api/items/Configuration$Builder;->void_reasons:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/api/items/Configuration;-><init>(Ljava/lang/String;Lcom/squareup/api/items/PageLayout;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 121
    invoke-virtual {p0}, Lcom/squareup/api/items/Configuration$Builder;->build()Lcom/squareup/api/items/Configuration;

    move-result-object v0

    return-object v0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/api/items/Configuration$Builder;
    .locals 0

    .line 133
    iput-object p1, p0, Lcom/squareup/api/items/Configuration$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public page_layout(Lcom/squareup/api/items/PageLayout;)Lcom/squareup/api/items/Configuration$Builder;
    .locals 0

    .line 141
    iput-object p1, p0, Lcom/squareup/api/items/Configuration$Builder;->page_layout:Lcom/squareup/api/items/PageLayout;

    return-object p0
.end method

.method public void_reasons(Ljava/util/List;)Lcom/squareup/api/items/Configuration$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/VoidReason;",
            ">;)",
            "Lcom/squareup/api/items/Configuration$Builder;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 150
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 151
    iput-object p1, p0, Lcom/squareup/api/items/Configuration$Builder;->void_reasons:Ljava/util/List;

    return-object p0
.end method
