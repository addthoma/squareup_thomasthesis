.class public final Lcom/squareup/api/items/ObjectPredicate;
.super Lcom/squareup/wire/Message;
.source "ObjectPredicate.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/items/ObjectPredicate$ProtoAdapter_ObjectPredicate;,
        Lcom/squareup/api/items/ObjectPredicate$Type;,
        Lcom/squareup/api/items/ObjectPredicate$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/api/items/ObjectPredicate;",
        "Lcom/squareup/api/items/ObjectPredicate$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/items/ObjectPredicate;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_TYPE:Lcom/squareup/api/items/ObjectPredicate$Type;

.field private static final serialVersionUID:J


# instance fields
.field public final object_id:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.sync.ObjectId#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;"
        }
    .end annotation
.end field

.field public final type:Lcom/squareup/api/items/ObjectPredicate$Type;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.ObjectPredicate$Type#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    new-instance v0, Lcom/squareup/api/items/ObjectPredicate$ProtoAdapter_ObjectPredicate;

    invoke-direct {v0}, Lcom/squareup/api/items/ObjectPredicate$ProtoAdapter_ObjectPredicate;-><init>()V

    sput-object v0, Lcom/squareup/api/items/ObjectPredicate;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 28
    sget-object v0, Lcom/squareup/api/items/ObjectPredicate$Type;->UNKNOWN:Lcom/squareup/api/items/ObjectPredicate$Type;

    sput-object v0, Lcom/squareup/api/items/ObjectPredicate;->DEFAULT_TYPE:Lcom/squareup/api/items/ObjectPredicate$Type;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/api/items/ObjectPredicate$Type;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/api/items/ObjectPredicate$Type;",
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;)V"
        }
    .end annotation

    .line 47
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/api/items/ObjectPredicate;-><init>(Lcom/squareup/api/items/ObjectPredicate$Type;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/api/items/ObjectPredicate$Type;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/api/items/ObjectPredicate$Type;",
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 51
    sget-object v0, Lcom/squareup/api/items/ObjectPredicate;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 52
    iput-object p1, p0, Lcom/squareup/api/items/ObjectPredicate;->type:Lcom/squareup/api/items/ObjectPredicate$Type;

    const-string p1, "object_id"

    .line 53
    invoke-static {p1, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/api/items/ObjectPredicate;->object_id:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 68
    :cond_0
    instance-of v1, p1, Lcom/squareup/api/items/ObjectPredicate;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 69
    :cond_1
    check-cast p1, Lcom/squareup/api/items/ObjectPredicate;

    .line 70
    invoke-virtual {p0}, Lcom/squareup/api/items/ObjectPredicate;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/api/items/ObjectPredicate;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ObjectPredicate;->type:Lcom/squareup/api/items/ObjectPredicate$Type;

    iget-object v3, p1, Lcom/squareup/api/items/ObjectPredicate;->type:Lcom/squareup/api/items/ObjectPredicate$Type;

    .line 71
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ObjectPredicate;->object_id:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/api/items/ObjectPredicate;->object_id:Ljava/util/List;

    .line 72
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 77
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1

    .line 79
    invoke-virtual {p0}, Lcom/squareup/api/items/ObjectPredicate;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 80
    iget-object v1, p0, Lcom/squareup/api/items/ObjectPredicate;->type:Lcom/squareup/api/items/ObjectPredicate$Type;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/api/items/ObjectPredicate$Type;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 81
    iget-object v1, p0, Lcom/squareup/api/items/ObjectPredicate;->object_id:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 82
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/api/items/ObjectPredicate$Builder;
    .locals 2

    .line 58
    new-instance v0, Lcom/squareup/api/items/ObjectPredicate$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/ObjectPredicate$Builder;-><init>()V

    .line 59
    iget-object v1, p0, Lcom/squareup/api/items/ObjectPredicate;->type:Lcom/squareup/api/items/ObjectPredicate$Type;

    iput-object v1, v0, Lcom/squareup/api/items/ObjectPredicate$Builder;->type:Lcom/squareup/api/items/ObjectPredicate$Type;

    .line 60
    iget-object v1, p0, Lcom/squareup/api/items/ObjectPredicate;->object_id:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/api/items/ObjectPredicate$Builder;->object_id:Ljava/util/List;

    .line 61
    invoke-virtual {p0}, Lcom/squareup/api/items/ObjectPredicate;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/ObjectPredicate$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/api/items/ObjectPredicate;->newBuilder()Lcom/squareup/api/items/ObjectPredicate$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 89
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 90
    iget-object v1, p0, Lcom/squareup/api/items/ObjectPredicate;->type:Lcom/squareup/api/items/ObjectPredicate$Type;

    if-eqz v1, :cond_0

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ObjectPredicate;->type:Lcom/squareup/api/items/ObjectPredicate$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 91
    :cond_0
    iget-object v1, p0, Lcom/squareup/api/items/ObjectPredicate;->object_id:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ", object_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ObjectPredicate;->object_id:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ObjectPredicate{"

    .line 92
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
