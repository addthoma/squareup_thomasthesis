.class public final enum Lcom/squareup/api/items/ExternalType;
.super Ljava/lang/Enum;
.source "ExternalType.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/items/ExternalType$ProtoAdapter_ExternalType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/api/items/ExternalType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/api/items/ExternalType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/items/ExternalType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum ITEM_ARCHETYPE:Lcom/squareup/api/items/ExternalType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 14
    new-instance v0, Lcom/squareup/api/items/ExternalType;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v3, "ITEM_ARCHETYPE"

    invoke-direct {v0, v3, v2, v1}, Lcom/squareup/api/items/ExternalType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/ExternalType;->ITEM_ARCHETYPE:Lcom/squareup/api/items/ExternalType;

    new-array v0, v1, [Lcom/squareup/api/items/ExternalType;

    .line 13
    sget-object v1, Lcom/squareup/api/items/ExternalType;->ITEM_ARCHETYPE:Lcom/squareup/api/items/ExternalType;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/api/items/ExternalType;->$VALUES:[Lcom/squareup/api/items/ExternalType;

    .line 16
    new-instance v0, Lcom/squareup/api/items/ExternalType$ProtoAdapter_ExternalType;

    invoke-direct {v0}, Lcom/squareup/api/items/ExternalType$ProtoAdapter_ExternalType;-><init>()V

    sput-object v0, Lcom/squareup/api/items/ExternalType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 21
    iput p3, p0, Lcom/squareup/api/items/ExternalType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/api/items/ExternalType;
    .locals 1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 29
    :cond_0
    sget-object p0, Lcom/squareup/api/items/ExternalType;->ITEM_ARCHETYPE:Lcom/squareup/api/items/ExternalType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/api/items/ExternalType;
    .locals 1

    .line 13
    const-class v0, Lcom/squareup/api/items/ExternalType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/api/items/ExternalType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/api/items/ExternalType;
    .locals 1

    .line 13
    sget-object v0, Lcom/squareup/api/items/ExternalType;->$VALUES:[Lcom/squareup/api/items/ExternalType;

    invoke-virtual {v0}, [Lcom/squareup/api/items/ExternalType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/api/items/ExternalType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 36
    iget v0, p0, Lcom/squareup/api/items/ExternalType;->value:I

    return v0
.end method
