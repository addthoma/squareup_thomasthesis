.class public Lcom/squareup/api/ApiReaderSettingsController;
.super Ljava/lang/Object;
.source "ApiReaderSettingsController.java"


# static fields
.field private static final READER_SETTINGS_KEY:Ljava/lang/String; = "readerSettingsApiRequestInFlight"


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final clock:Lcom/squareup/util/Clock;

.field private loaded:Z

.field private readerSettingsRequestInFlight:Z

.field private final requestController:Lcom/squareup/api/ApiRequestController;


# direct methods
.method constructor <init>(Lcom/squareup/util/Clock;Lcom/squareup/analytics/Analytics;Lcom/squareup/api/ApiRequestController;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/squareup/api/ApiReaderSettingsController;->clock:Lcom/squareup/util/Clock;

    .line 40
    iput-object p2, p0, Lcom/squareup/api/ApiReaderSettingsController;->analytics:Lcom/squareup/analytics/Analytics;

    .line 41
    iput-object p3, p0, Lcom/squareup/api/ApiReaderSettingsController;->requestController:Lcom/squareup/api/ApiRequestController;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/api/ApiReaderSettingsController;)Lcom/squareup/api/ApiRequestController;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/squareup/api/ApiReaderSettingsController;->requestController:Lcom/squareup/api/ApiRequestController;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/api/ApiReaderSettingsController;)Z
    .locals 0

    .line 26
    iget-boolean p0, p0, Lcom/squareup/api/ApiReaderSettingsController;->loaded:Z

    return p0
.end method

.method static synthetic access$102(Lcom/squareup/api/ApiReaderSettingsController;Z)Z
    .locals 0

    .line 26
    iput-boolean p1, p0, Lcom/squareup/api/ApiReaderSettingsController;->loaded:Z

    return p1
.end method

.method static synthetic access$200(Lcom/squareup/api/ApiReaderSettingsController;)Z
    .locals 0

    .line 26
    iget-boolean p0, p0, Lcom/squareup/api/ApiReaderSettingsController;->readerSettingsRequestInFlight:Z

    return p0
.end method

.method static synthetic access$202(Lcom/squareup/api/ApiReaderSettingsController;Z)Z
    .locals 0

    .line 26
    iput-boolean p1, p0, Lcom/squareup/api/ApiReaderSettingsController;->readerSettingsRequestInFlight:Z

    return p1
.end method

.method static synthetic access$300(Lcom/squareup/api/ApiReaderSettingsController;Z)V
    .locals 0

    .line 26
    invoke-direct {p0, p1}, Lcom/squareup/api/ApiReaderSettingsController;->finish(Z)V

    return-void
.end method

.method private createErrorResultIntent(Lcom/squareup/api/ApiErrorResult;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .line 97
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 98
    invoke-virtual {p1}, Lcom/squareup/api/ApiErrorResult;->name()Ljava/lang/String;

    move-result-object p1

    const-string v1, "com.squareup.pos.ERROR_CODE"

    .line 99
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string p1, "com.squareup.pos.ERROR_DESCRIPTION"

    .line 100
    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method

.method private finish(Z)V
    .locals 8

    .line 76
    iget-object v0, p0, Lcom/squareup/api/ApiReaderSettingsController;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v7, Lcom/squareup/api/ReaderSettingsFlowSuccessEvent;

    iget-object v2, p0, Lcom/squareup/api/ApiReaderSettingsController;->clock:Lcom/squareup/util/Clock;

    iget-object v1, p0, Lcom/squareup/api/ApiReaderSettingsController;->requestController:Lcom/squareup/api/ApiRequestController;

    invoke-virtual {v1}, Lcom/squareup/api/ApiRequestController;->clientId()Ljava/lang/String;

    move-result-object v3

    iget-object v1, p0, Lcom/squareup/api/ApiReaderSettingsController;->requestController:Lcom/squareup/api/ApiRequestController;

    .line 77
    invoke-virtual {v1}, Lcom/squareup/api/ApiRequestController;->requestStartTime()J

    move-result-wide v4

    move-object v1, v7

    move v6, p1

    invoke-direct/range {v1 .. v6}, Lcom/squareup/api/ReaderSettingsFlowSuccessEvent;-><init>(Lcom/squareup/util/Clock;Ljava/lang/String;JZ)V

    .line 76
    invoke-interface {v0, v7}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 78
    iget-object p1, p0, Lcom/squareup/api/ApiReaderSettingsController;->requestController:Lcom/squareup/api/ApiRequestController;

    invoke-virtual {p1}, Lcom/squareup/api/ApiRequestController;->getActivity()Landroid/app/Activity;

    move-result-object p1

    const/4 v0, -0x1

    .line 79
    invoke-virtual {p1, v0}, Landroid/app/Activity;->setResult(I)V

    .line 80
    iget-object p1, p0, Lcom/squareup/api/ApiReaderSettingsController;->requestController:Lcom/squareup/api/ApiRequestController;

    invoke-virtual {p1}, Lcom/squareup/api/ApiRequestController;->finish()V

    const/4 p1, 0x0

    .line 81
    iput-boolean p1, p0, Lcom/squareup/api/ApiReaderSettingsController;->readerSettingsRequestInFlight:Z

    return-void
.end method

.method private finishWithError(Lcom/squareup/api/ApiErrorResult;)V
    .locals 9

    .line 85
    iget-object v0, p0, Lcom/squareup/api/ApiReaderSettingsController;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v8, Lcom/squareup/api/ReaderSettingsFlowFailureEvent;

    iget-object v2, p0, Lcom/squareup/api/ApiReaderSettingsController;->clock:Lcom/squareup/util/Clock;

    iget-object v1, p0, Lcom/squareup/api/ApiReaderSettingsController;->requestController:Lcom/squareup/api/ApiRequestController;

    invoke-virtual {v1}, Lcom/squareup/api/ApiRequestController;->sequenceUuid()Ljava/lang/String;

    move-result-object v3

    iget-object v1, p0, Lcom/squareup/api/ApiReaderSettingsController;->requestController:Lcom/squareup/api/ApiRequestController;

    .line 86
    invoke-virtual {v1}, Lcom/squareup/api/ApiRequestController;->clientId()Ljava/lang/String;

    move-result-object v4

    iget-object v1, p0, Lcom/squareup/api/ApiReaderSettingsController;->requestController:Lcom/squareup/api/ApiRequestController;

    invoke-virtual {v1}, Lcom/squareup/api/ApiRequestController;->requestStartTime()J

    move-result-wide v6

    move-object v1, v8

    move-object v5, p1

    invoke-direct/range {v1 .. v7}, Lcom/squareup/api/ReaderSettingsFlowFailureEvent;-><init>(Lcom/squareup/util/Clock;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/ApiErrorResult;J)V

    .line 85
    invoke-interface {v0, v8}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 87
    iget-object v0, p0, Lcom/squareup/api/ApiReaderSettingsController;->requestController:Lcom/squareup/api/ApiRequestController;

    invoke-virtual {v0}, Lcom/squareup/api/ApiRequestController;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 88
    iget v1, p1, Lcom/squareup/api/ApiErrorResult;->errorDescriptionResourceId:I

    .line 89
    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 88
    invoke-direct {p0, p1, v1}, Lcom/squareup/api/ApiReaderSettingsController;->createErrorResultIntent(Lcom/squareup/api/ApiErrorResult;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p1

    const/4 v1, 0x0

    .line 90
    invoke-virtual {v0, v1, p1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 91
    iget-object p1, p0, Lcom/squareup/api/ApiReaderSettingsController;->requestController:Lcom/squareup/api/ApiRequestController;

    invoke-virtual {p1}, Lcom/squareup/api/ApiRequestController;->finish()V

    .line 92
    iput-boolean v1, p0, Lcom/squareup/api/ApiReaderSettingsController;->readerSettingsRequestInFlight:Z

    return-void
.end method


# virtual methods
.method public getBundler()Lmortar/bundler/Bundler;
    .locals 2

    .line 105
    new-instance v0, Lcom/squareup/api/ApiReaderSettingsController$1;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/api/ApiReaderSettingsController$1;-><init>(Lcom/squareup/api/ApiReaderSettingsController;Ljava/lang/String;)V

    return-object v0
.end method

.method public handleReaderSettingsCanceled()Z
    .locals 2

    .line 52
    invoke-virtual {p0}, Lcom/squareup/api/ApiReaderSettingsController;->isApiReaderSettingsRequest()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 55
    :cond_0
    invoke-direct {p0, v1}, Lcom/squareup/api/ApiReaderSettingsController;->finish(Z)V

    const/4 v0, 0x1

    return v0
.end method

.method public handleReaderSettingsError(Lcom/squareup/api/ApiErrorResult;)V
    .locals 1

    const-string v0, "apiError"

    .line 60
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 62
    invoke-virtual {p0}, Lcom/squareup/api/ApiReaderSettingsController;->isApiReaderSettingsRequest()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 65
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/api/ApiReaderSettingsController;->finishWithError(Lcom/squareup/api/ApiErrorResult;)V

    return-void
.end method

.method public isApiReaderSettingsRequest()Z
    .locals 1

    .line 69
    iget-boolean v0, p0, Lcom/squareup/api/ApiReaderSettingsController;->readerSettingsRequestInFlight:Z

    return v0
.end method

.method public loadReaderSettingsController(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 2

    const/4 v0, 0x1

    .line 46
    iput-boolean v0, p0, Lcom/squareup/api/ApiReaderSettingsController;->readerSettingsRequestInFlight:Z

    .line 47
    iget-object v0, p0, Lcom/squareup/api/ApiReaderSettingsController;->requestController:Lcom/squareup/api/ApiRequestController;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/squareup/api/ApiRequestController;->startApiSession(Ljava/lang/String;Ljava/lang/String;J)V

    .line 48
    iget-object p2, p0, Lcom/squareup/api/ApiReaderSettingsController;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v0, Lcom/squareup/api/ReaderSettingsFlowStartedEvent;

    iget-object v1, p0, Lcom/squareup/api/ApiReaderSettingsController;->clock:Lcom/squareup/util/Clock;

    invoke-direct {v0, v1, p1, p3, p4}, Lcom/squareup/api/ReaderSettingsFlowStartedEvent;-><init>(Lcom/squareup/util/Clock;Ljava/lang/String;J)V

    invoke-interface {p2, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method
