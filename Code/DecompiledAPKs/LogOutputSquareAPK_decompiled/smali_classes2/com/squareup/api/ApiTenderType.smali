.class public final enum Lcom/squareup/api/ApiTenderType;
.super Ljava/lang/Enum;
.source "ApiTenderType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/api/ApiTenderType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/api/ApiTenderType;

.field public static final enum CARD_ON_FILE:Lcom/squareup/api/ApiTenderType;

.field public static final enum CASH:Lcom/squareup/api/ApiTenderType;

.field public static final enum KEYED_IN_CARD:Lcom/squareup/api/ApiTenderType;

.field public static final enum OTHER:Lcom/squareup/api/ApiTenderType;

.field public static final enum POS_API_CARD_PRESENT:Lcom/squareup/api/ApiTenderType;


# instance fields
.field public final nativeExtraKey:Ljava/lang/String;

.field public final webExtraKey:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 14
    new-instance v0, Lcom/squareup/api/ApiTenderType;

    const/4 v1, 0x0

    const-string v2, "CARD_ON_FILE"

    const-string v3, "com.squareup.pos.TENDER_CARD_ON_FILE"

    const-string v4, "card_on_file"

    invoke-direct {v0, v2, v1, v3, v4}, Lcom/squareup/api/ApiTenderType;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/api/ApiTenderType;->CARD_ON_FILE:Lcom/squareup/api/ApiTenderType;

    .line 15
    new-instance v0, Lcom/squareup/api/ApiTenderType;

    const/4 v2, 0x1

    const-string v3, "CASH"

    const-string v4, "com.squareup.pos.TENDER_CASH"

    const-string v5, "cash"

    invoke-direct {v0, v3, v2, v4, v5}, Lcom/squareup/api/ApiTenderType;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/api/ApiTenderType;->CASH:Lcom/squareup/api/ApiTenderType;

    .line 16
    new-instance v0, Lcom/squareup/api/ApiTenderType;

    const/4 v3, 0x2

    const-string v4, "OTHER"

    const-string v5, "com.squareup.pos.TENDER_OTHER"

    const-string v6, "other"

    invoke-direct {v0, v4, v3, v5, v6}, Lcom/squareup/api/ApiTenderType;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/api/ApiTenderType;->OTHER:Lcom/squareup/api/ApiTenderType;

    .line 18
    new-instance v0, Lcom/squareup/api/ApiTenderType;

    const/4 v4, 0x3

    const-string v5, "KEYED_IN_CARD"

    const-string v6, "com.squareup.pos.TENDER_KEYED_IN_CARD"

    const-string v7, "keyed_in_card"

    invoke-direct {v0, v5, v4, v6, v7}, Lcom/squareup/api/ApiTenderType;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/api/ApiTenderType;->KEYED_IN_CARD:Lcom/squareup/api/ApiTenderType;

    .line 21
    new-instance v0, Lcom/squareup/api/ApiTenderType;

    const/4 v5, 0x4

    const-string v6, "POS_API_CARD_PRESENT"

    const-string v7, "com.squareup.pos.TENDER_CARD_FROM_READER"

    const-string v8, "card_from_reader"

    invoke-direct {v0, v6, v5, v7, v8}, Lcom/squareup/api/ApiTenderType;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/api/ApiTenderType;->POS_API_CARD_PRESENT:Lcom/squareup/api/ApiTenderType;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/api/ApiTenderType;

    .line 12
    sget-object v6, Lcom/squareup/api/ApiTenderType;->CARD_ON_FILE:Lcom/squareup/api/ApiTenderType;

    aput-object v6, v0, v1

    sget-object v1, Lcom/squareup/api/ApiTenderType;->CASH:Lcom/squareup/api/ApiTenderType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/ApiTenderType;->OTHER:Lcom/squareup/api/ApiTenderType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/api/ApiTenderType;->KEYED_IN_CARD:Lcom/squareup/api/ApiTenderType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/api/ApiTenderType;->POS_API_CARD_PRESENT:Lcom/squareup/api/ApiTenderType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/api/ApiTenderType;->$VALUES:[Lcom/squareup/api/ApiTenderType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const/4 v0, 0x0

    .line 28
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/api/ApiTenderType;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 31
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 32
    iput-object p3, p0, Lcom/squareup/api/ApiTenderType;->nativeExtraKey:Ljava/lang/String;

    .line 33
    iput-object p4, p0, Lcom/squareup/api/ApiTenderType;->webExtraKey:Ljava/lang/String;

    return-void
.end method

.method public static extractTenderTypes(Landroid/content/Intent;)Ljava/util/Set;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Ljava/util/Set<",
            "Lcom/squareup/api/ApiTenderType;",
            ">;"
        }
    .end annotation

    const-string v0, "com.squareup.pos.TENDER_TYPES"

    .line 37
    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, "tender_types"

    .line 39
    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    if-nez p0, :cond_0

    .line 41
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object p0

    return-object p0

    :cond_0
    const-string v0, ","

    .line 43
    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 45
    :cond_1
    new-instance p0, Ljava/util/HashSet;

    invoke-direct {p0}, Ljava/util/HashSet;-><init>()V

    const-string v1, "V3_TENDER_CARD"

    .line 47
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 48
    sget-object v1, Lcom/squareup/api/ApiTenderType;->KEYED_IN_CARD:Lcom/squareup/api/ApiTenderType;

    invoke-interface {p0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 49
    sget-object v1, Lcom/squareup/api/ApiTenderType;->POS_API_CARD_PRESENT:Lcom/squareup/api/ApiTenderType;

    invoke-interface {p0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 51
    :cond_2
    invoke-static {}, Lcom/squareup/api/ApiTenderType;->values()[Lcom/squareup/api/ApiTenderType;

    move-result-object v1

    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_5

    aget-object v4, v1, v3

    .line 52
    iget-object v5, v4, Lcom/squareup/api/ApiTenderType;->nativeExtraKey:Ljava/lang/String;

    invoke-interface {v0, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    iget-object v5, v4, Lcom/squareup/api/ApiTenderType;->webExtraKey:Ljava/lang/String;

    invoke-interface {v0, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 54
    :cond_3
    invoke-interface {p0, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 57
    :cond_5
    invoke-static {p0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object p0

    return-object p0
.end method

.method public static putTenderTypes(Landroid/content/Intent;Ljava/util/Set;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            "Ljava/util/Set<",
            "Lcom/squareup/api/ApiTenderType;",
            ">;)V"
        }
    .end annotation

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 62
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/api/ApiTenderType;

    .line 63
    iget-object v1, v1, Lcom/squareup/api/ApiTenderType;->nativeExtraKey:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    const-string p1, "com.squareup.pos.TENDER_TYPES"

    .line 65
    invoke-virtual {p0, p1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/api/ApiTenderType;
    .locals 1

    .line 12
    const-class v0, Lcom/squareup/api/ApiTenderType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/api/ApiTenderType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/api/ApiTenderType;
    .locals 1

    .line 12
    sget-object v0, Lcom/squareup/api/ApiTenderType;->$VALUES:[Lcom/squareup/api/ApiTenderType;

    invoke-virtual {v0}, [Lcom/squareup/api/ApiTenderType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/api/ApiTenderType;

    return-object v0
.end method
