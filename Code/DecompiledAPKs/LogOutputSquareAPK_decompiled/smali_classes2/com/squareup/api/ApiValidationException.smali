.class final Lcom/squareup/api/ApiValidationException;
.super Ljava/lang/RuntimeException;
.source "ApiValidationException.java"


# instance fields
.field private final dynamicDebugDescription:Ljava/lang/String;

.field public final errorResult:Lcom/squareup/api/ApiErrorResult;


# direct methods
.method constructor <init>(Lcom/squareup/api/ApiErrorResult;)V
    .locals 1

    const/4 v0, 0x0

    .line 11
    invoke-direct {p0, p1, v0}, Lcom/squareup/api/ApiValidationException;-><init>(Lcom/squareup/api/ApiErrorResult;Ljava/lang/String;)V

    return-void
.end method

.method constructor <init>(Lcom/squareup/api/ApiErrorResult;Ljava/lang/String;)V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/RuntimeException;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/squareup/api/ApiValidationException;->errorResult:Lcom/squareup/api/ApiErrorResult;

    .line 16
    iput-object p2, p0, Lcom/squareup/api/ApiValidationException;->dynamicDebugDescription:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getErrorDescription(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/api/ApiValidationException;->dynamicDebugDescription:Ljava/lang/String;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/api/ApiValidationException;->errorResult:Lcom/squareup/api/ApiErrorResult;

    iget v0, v0, Lcom/squareup/api/ApiErrorResult;->errorDescriptionResourceId:I

    .line 22
    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0
.end method
