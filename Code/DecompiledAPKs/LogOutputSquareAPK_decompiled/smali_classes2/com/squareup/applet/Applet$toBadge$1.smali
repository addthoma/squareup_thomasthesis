.class final Lcom/squareup/applet/Applet$toBadge$1;
.super Ljava/lang/Object;
.source "Applet.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/applet/Applet;->toBadge(Lio/reactivex/Observable;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0004\u0008\u0004\u0010\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/applet/Applet$Badge;",
        "it",
        "",
        "apply",
        "(Ljava/lang/Integer;)Lcom/squareup/applet/Applet$Badge;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/applet/Applet$toBadge$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/applet/Applet$toBadge$1;

    invoke-direct {v0}, Lcom/squareup/applet/Applet$toBadge$1;-><init>()V

    sput-object v0, Lcom/squareup/applet/Applet$toBadge$1;->INSTANCE:Lcom/squareup/applet/Applet$toBadge$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Integer;)Lcom/squareup/applet/Applet$Badge;
    .locals 3

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    sget-object v0, Lcom/squareup/applet/Applet$Badge;->Companion:Lcom/squareup/applet/Applet$Badge$Companion;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-static {v0, p1, v1, v2, v1}, Lcom/squareup/applet/Applet$Badge$Companion;->toBadge$default(Lcom/squareup/applet/Applet$Badge$Companion;ILcom/squareup/applet/Applet$Badge$Priority;ILjava/lang/Object;)Lcom/squareup/applet/Applet$Badge;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 21
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/squareup/applet/Applet$toBadge$1;->apply(Ljava/lang/Integer;)Lcom/squareup/applet/Applet$Badge;

    move-result-object p1

    return-object p1
.end method
