.class public Lcom/squareup/applet/BadgePresenter;
.super Lmortar/Presenter;
.source "BadgePresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/applet/BadgePresenter$SharedScope;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/Presenter<",
        "Lcom/squareup/marin/widgets/Badgeable;",
        ">;"
    }
.end annotation


# instance fields
.field private final appletsBadgeCounter:Lcom/squareup/applet/AppletsBadgeCounter;

.field private final canBeVisible:Lio/reactivex/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/subjects/BehaviorSubject<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final subs:Lio/reactivex/disposables/CompositeDisposable;


# direct methods
.method public constructor <init>(Lcom/squareup/applet/AppletsBadgeCounter;Lio/reactivex/Scheduler;)V
    .locals 1
    .param p2    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 30
    invoke-direct {p0}, Lmortar/Presenter;-><init>()V

    const/4 v0, 0x1

    .line 25
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/subjects/BehaviorSubject;->createDefault(Ljava/lang/Object;)Lio/reactivex/subjects/BehaviorSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/applet/BadgePresenter;->canBeVisible:Lio/reactivex/subjects/BehaviorSubject;

    .line 26
    new-instance v0, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v0}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object v0, p0, Lcom/squareup/applet/BadgePresenter;->subs:Lio/reactivex/disposables/CompositeDisposable;

    .line 31
    iput-object p1, p0, Lcom/squareup/applet/BadgePresenter;->appletsBadgeCounter:Lcom/squareup/applet/AppletsBadgeCounter;

    .line 32
    iput-object p2, p0, Lcom/squareup/applet/BadgePresenter;->mainScheduler:Lio/reactivex/Scheduler;

    return-void
.end method

.method private updateFromAppletBadges(Lcom/squareup/marin/widgets/Badgeable;Lcom/squareup/applet/Applet$Badge;Z)V
    .locals 1

    if-eqz p3, :cond_3

    .line 75
    instance-of p3, p2, Lcom/squareup/applet/Applet$Badge$Visible;

    if-eqz p3, :cond_3

    .line 76
    check-cast p2, Lcom/squareup/applet/Applet$Badge$Visible;

    .line 77
    sget-object p3, Lcom/squareup/applet/BadgePresenter$1;->$SwitchMap$com$squareup$applet$Applet$Badge$Priority:[I

    invoke-virtual {p2}, Lcom/squareup/applet/Applet$Badge$Visible;->getPriority()Lcom/squareup/applet/Applet$Badge$Priority;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/applet/Applet$Badge$Priority;->ordinal()I

    move-result v0

    aget p3, p3, v0

    const/4 v0, 0x1

    if-eq p3, v0, :cond_2

    const/4 p2, 0x2

    if-eq p3, p2, :cond_1

    const/4 p2, 0x3

    if-eq p3, p2, :cond_0

    goto :goto_0

    .line 85
    :cond_0
    invoke-interface {p1}, Lcom/squareup/marin/widgets/Badgeable;->showFatalPriorityBadge()V

    goto :goto_0

    :cond_1
    const/4 p2, 0x0

    .line 82
    invoke-interface {p1, p2}, Lcom/squareup/marin/widgets/Badgeable;->showHighPriorityBadge(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 79
    :cond_2
    invoke-virtual {p2}, Lcom/squareup/applet/Applet$Badge$Visible;->getText()Ljava/lang/CharSequence;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/squareup/marin/widgets/Badgeable;->showBadge(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 89
    :cond_3
    invoke-interface {p1}, Lcom/squareup/marin/widgets/Badgeable;->hideBadge()V

    :goto_0
    return-void
.end method


# virtual methods
.method public dropView(Lcom/squareup/marin/widgets/Badgeable;)V
    .locals 2

    .line 51
    invoke-virtual {p0}, Lcom/squareup/applet/BadgePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/squareup/applet/BadgePresenter;->subs:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    .line 54
    iget-object v0, p0, Lcom/squareup/applet/BadgePresenter;->canBeVisible:Lio/reactivex/subjects/BehaviorSubject;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    .line 56
    :cond_0
    invoke-super {p0, p1}, Lmortar/Presenter;->dropView(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic dropView(Ljava/lang/Object;)V
    .locals 0

    .line 21
    check-cast p1, Lcom/squareup/marin/widgets/Badgeable;

    invoke-virtual {p0, p1}, Lcom/squareup/applet/BadgePresenter;->dropView(Lcom/squareup/marin/widgets/Badgeable;)V

    return-void
.end method

.method protected extractBundleService(Lcom/squareup/marin/widgets/Badgeable;)Lmortar/bundler/BundleService;
    .locals 0

    .line 60
    invoke-interface {p1}, Lcom/squareup/marin/widgets/Badgeable;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Landroid/content/Context;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic extractBundleService(Ljava/lang/Object;)Lmortar/bundler/BundleService;
    .locals 0

    .line 21
    check-cast p1, Lcom/squareup/marin/widgets/Badgeable;

    invoke-virtual {p0, p1}, Lcom/squareup/applet/BadgePresenter;->extractBundleService(Lcom/squareup/marin/widgets/Badgeable;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$0$BadgePresenter(Lcom/squareup/applet/Applet$Badge;Ljava/lang/Boolean;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 43
    invoke-virtual {p0}, Lcom/squareup/applet/BadgePresenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    invoke-virtual {p0}, Lcom/squareup/applet/BadgePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/Badgeable;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    invoke-direct {p0, v0, p1, p2}, Lcom/squareup/applet/BadgePresenter;->updateFromAppletBadges(Lcom/squareup/marin/widgets/Badgeable;Lcom/squareup/applet/Applet$Badge;Z)V

    :cond_0
    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 3

    .line 36
    iget-object p1, p0, Lcom/squareup/applet/BadgePresenter;->subs:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {p1}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    .line 38
    iget-object p1, p0, Lcom/squareup/applet/BadgePresenter;->subs:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v0, p0, Lcom/squareup/applet/BadgePresenter;->appletsBadgeCounter:Lcom/squareup/applet/AppletsBadgeCounter;

    .line 39
    invoke-virtual {v0}, Lcom/squareup/applet/AppletsBadgeCounter;->sampledTotalBadge()Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/applet/BadgePresenter;->canBeVisible:Lio/reactivex/subjects/BehaviorSubject;

    .line 40
    invoke-virtual {v1}, Lio/reactivex/subjects/BehaviorSubject;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {}, Lcom/squareup/util/Rx2Tuples;->toPair()Lio/reactivex/functions/BiFunction;

    move-result-object v2

    .line 39
    invoke-static {v0, v1, v2}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/applet/BadgePresenter;->mainScheduler:Lio/reactivex/Scheduler;

    .line 41
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/applet/-$$Lambda$BadgePresenter$5XsQxO044M9UxOYscfYv921i8uQ;

    invoke-direct {v1, p0}, Lcom/squareup/applet/-$$Lambda$BadgePresenter$5XsQxO044M9UxOYscfYv921i8uQ;-><init>(Lcom/squareup/applet/BadgePresenter;)V

    .line 42
    invoke-static {v1}, Lcom/squareup/util/Rx2Tuples;->expandPair(Lio/reactivex/functions/BiConsumer;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 38
    invoke-virtual {p1, v0}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method public toggleVisibility(Z)V
    .locals 1

    .line 68
    iget-object v0, p0, Lcom/squareup/applet/BadgePresenter;->canBeVisible:Lio/reactivex/subjects/BehaviorSubject;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lio/reactivex/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method
