.class Lcom/squareup/applet/AppletSectionsListView$SectionsAdapter;
.super Landroid/widget/BaseAdapter;
.source "AppletSectionsListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/applet/AppletSectionsListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SectionsAdapter"
.end annotation


# instance fields
.field private checkableRows:Z

.field final synthetic this$0:Lcom/squareup/applet/AppletSectionsListView;


# direct methods
.method constructor <init>(Lcom/squareup/applet/AppletSectionsListView;Z)V
    .locals 0

    .line 87
    iput-object p1, p0, Lcom/squareup/applet/AppletSectionsListView$SectionsAdapter;->this$0:Lcom/squareup/applet/AppletSectionsListView;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 88
    iput-boolean p2, p0, Lcom/squareup/applet/AppletSectionsListView$SectionsAdapter;->checkableRows:Z

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .line 92
    iget-object v0, p0, Lcom/squareup/applet/AppletSectionsListView$SectionsAdapter;->this$0:Lcom/squareup/applet/AppletSectionsListView;

    iget-object v0, v0, Lcom/squareup/applet/AppletSectionsListView;->presenter:Lcom/squareup/applet/AppletSectionsListPresenter;

    invoke-virtual {v0}, Lcom/squareup/applet/AppletSectionsListPresenter;->getSectionCount()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/squareup/applet/AppletSection;
    .locals 1

    .line 96
    iget-object v0, p0, Lcom/squareup/applet/AppletSectionsListView$SectionsAdapter;->this$0:Lcom/squareup/applet/AppletSectionsListView;

    iget-object v0, v0, Lcom/squareup/applet/AppletSectionsListView;->presenter:Lcom/squareup/applet/AppletSectionsListPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/applet/AppletSectionsListPresenter;->getEntry(I)Lcom/squareup/applet/AppletSectionsListEntry;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/applet/AppletSectionsListEntry;->getSection()Lcom/squareup/applet/AppletSection;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 0

    .line 83
    invoke-virtual {p0, p1}, Lcom/squareup/applet/AppletSectionsListView$SectionsAdapter;->getItem(I)Lcom/squareup/applet/AppletSection;

    move-result-object p1

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .line 104
    move-object v0, p2

    check-cast v0, Lcom/squareup/ui/account/view/SmartLineRow;

    .line 105
    iget-boolean v1, p0, Lcom/squareup/applet/AppletSectionsListView$SectionsAdapter;->checkableRows:Z

    if-eqz v1, :cond_1

    if-nez p2, :cond_0

    .line 107
    iget-object p2, p0, Lcom/squareup/applet/AppletSectionsListView$SectionsAdapter;->this$0:Lcom/squareup/applet/AppletSectionsListView;

    invoke-virtual {p2}, Lcom/squareup/applet/AppletSectionsListView;->appletSidebarRow()I

    move-result p2

    invoke-static {p2, p3}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/account/view/SmartLineRow;

    move-object v0, p2

    .line 109
    :cond_0
    iget-object p2, p0, Lcom/squareup/applet/AppletSectionsListView$SectionsAdapter;->this$0:Lcom/squareup/applet/AppletSectionsListView;

    iget-object p2, p2, Lcom/squareup/applet/AppletSectionsListView;->presenter:Lcom/squareup/applet/AppletSectionsListPresenter;

    invoke-virtual {p2, p1}, Lcom/squareup/applet/AppletSectionsListPresenter;->isSectionSelected(I)Z

    move-result p2

    invoke-virtual {v0, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setActivated(Z)V

    goto :goto_0

    :cond_1
    if-nez p2, :cond_2

    .line 112
    iget-object p2, p0, Lcom/squareup/applet/AppletSectionsListView$SectionsAdapter;->this$0:Lcom/squareup/applet/AppletSectionsListView;

    invoke-virtual {p2}, Lcom/squareup/applet/AppletSectionsListView;->appletListRow()I

    move-result p2

    invoke-static {p2, p3}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    move-object v0, p2

    check-cast v0, Lcom/squareup/ui/account/view/SmartLineRow;

    .line 116
    :cond_2
    :goto_0
    iget-object p2, p0, Lcom/squareup/applet/AppletSectionsListView$SectionsAdapter;->this$0:Lcom/squareup/applet/AppletSectionsListView;

    iget-object p2, p2, Lcom/squareup/applet/AppletSectionsListView;->presenter:Lcom/squareup/applet/AppletSectionsListPresenter;

    invoke-virtual {p2, p1}, Lcom/squareup/applet/AppletSectionsListPresenter;->getSectionName(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleText(Ljava/lang/CharSequence;)V

    .line 121
    invoke-static {v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetachNow(Landroid/view/View;)V

    .line 122
    new-instance p2, Lcom/squareup/applet/-$$Lambda$AppletSectionsListView$SectionsAdapter$hs1zTfNN0EjSStUcD5q7V6A4hSo;

    invoke-direct {p2, p0, p1, v0}, Lcom/squareup/applet/-$$Lambda$AppletSectionsListView$SectionsAdapter$hs1zTfNN0EjSStUcD5q7V6A4hSo;-><init>(Lcom/squareup/applet/AppletSectionsListView$SectionsAdapter;ILcom/squareup/ui/account/view/SmartLineRow;)V

    invoke-static {v0, p2}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 136
    new-instance p2, Lcom/squareup/applet/-$$Lambda$AppletSectionsListView$SectionsAdapter$HooZ9VzFy2XMXv96c_8l-1pwztw;

    invoke-direct {p2, p0, p1}, Lcom/squareup/applet/-$$Lambda$AppletSectionsListView$SectionsAdapter$HooZ9VzFy2XMXv96c_8l-1pwztw;-><init>(Lcom/squareup/applet/AppletSectionsListView$SectionsAdapter;I)V

    invoke-static {v0, p2}, Lcom/squareup/util/Views;->waitForMeasure(Landroid/view/View;Lcom/squareup/util/OnMeasuredCallback;)V

    return-object v0
.end method

.method public synthetic lambda$getView$1$AppletSectionsListView$SectionsAdapter(ILcom/squareup/ui/account/view/SmartLineRow;)Lio/reactivex/disposables/Disposable;
    .locals 1

    .line 123
    iget-object v0, p0, Lcom/squareup/applet/AppletSectionsListView$SectionsAdapter;->this$0:Lcom/squareup/applet/AppletSectionsListView;

    iget-object v0, v0, Lcom/squareup/applet/AppletSectionsListView;->presenter:Lcom/squareup/applet/AppletSectionsListPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/applet/AppletSectionsListPresenter;->getSectionValue(I)Lio/reactivex/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/applet/-$$Lambda$AppletSectionsListView$SectionsAdapter$RI7rvHLjlNHousC-XvCAlKbd0NE;

    invoke-direct {v0, p0, p2}, Lcom/squareup/applet/-$$Lambda$AppletSectionsListView$SectionsAdapter$RI7rvHLjlNHousC-XvCAlKbd0NE;-><init>(Lcom/squareup/applet/AppletSectionsListView$SectionsAdapter;Lcom/squareup/ui/account/view/SmartLineRow;)V

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$getView$2$AppletSectionsListView$SectionsAdapter(ILandroid/view/View;II)V
    .locals 0

    .line 137
    iget-object p2, p0, Lcom/squareup/applet/AppletSectionsListView$SectionsAdapter;->this$0:Lcom/squareup/applet/AppletSectionsListView;

    iget-object p2, p2, Lcom/squareup/applet/AppletSectionsListView;->presenter:Lcom/squareup/applet/AppletSectionsListPresenter;

    invoke-virtual {p2, p1}, Lcom/squareup/applet/AppletSectionsListPresenter;->rowDisplayed(I)V

    return-void
.end method

.method public synthetic lambda$null$0$AppletSectionsListView$SectionsAdapter(Lcom/squareup/ui/account/view/SmartLineRow;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 124
    invoke-static {p2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p2, 0x0

    .line 125
    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueVisible(Z)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    .line 127
    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueVisible(Z)V

    .line 128
    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueText(Ljava/lang/CharSequence;)V

    .line 129
    iget-object p2, p0, Lcom/squareup/applet/AppletSectionsListView$SectionsAdapter;->this$0:Lcom/squareup/applet/AppletSectionsListView;

    invoke-static {p2}, Lcom/squareup/applet/AppletSectionsListView;->access$000(Lcom/squareup/applet/AppletSectionsListView;)Landroid/content/res/ColorStateList;

    move-result-object p2

    if-eqz p2, :cond_1

    .line 130
    iget-object p2, p0, Lcom/squareup/applet/AppletSectionsListView$SectionsAdapter;->this$0:Lcom/squareup/applet/AppletSectionsListView;

    invoke-static {p2}, Lcom/squareup/applet/AppletSectionsListView;->access$000(Lcom/squareup/applet/AppletSectionsListView;)Landroid/content/res/ColorStateList;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueColorStateList(Landroid/content/res/ColorStateList;)V

    :cond_1
    :goto_0
    return-void
.end method
