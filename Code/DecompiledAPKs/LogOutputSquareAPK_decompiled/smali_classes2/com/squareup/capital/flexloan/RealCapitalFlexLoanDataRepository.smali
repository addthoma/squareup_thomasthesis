.class public final Lcom/squareup/capital/flexloan/RealCapitalFlexLoanDataRepository;
.super Ljava/lang/Object;
.source "RealCapitalFlexLoanDataRepository.kt"

# interfaces
.implements Lcom/squareup/capital/flexloan/CapitalFlexLoanDataRepository;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u000cH\u0016J\u0016\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\u000c2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016R\u0014\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R(\u0010\u0008\u001a\u001c\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0007 \n*\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u00060\u00060\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/capital/flexloan/RealCapitalFlexLoanDataRepository;",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanDataRepository;",
        "capitalFlexLoanRequestDataStore",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanRequestDataStore;",
        "(Lcom/squareup/capital/flexloan/CapitalFlexLoanRequestDataStore;)V",
        "capitalFlexLoanLoanStatusRequester",
        "Lio/reactivex/Single;",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;",
        "flexLoanStatusRequestRelay",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "kotlin.jvm.PlatformType",
        "fetchFlexLoanStatus",
        "Lio/reactivex/Observable;",
        "fetchFlexPlan",
        "Lcom/squareup/capital/flexloan/CapitalFlexPlanStatus;",
        "planId",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final capitalFlexLoanLoanStatusRequester:Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Single<",
            "Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;",
            ">;"
        }
    .end annotation
.end field

.field private final capitalFlexLoanRequestDataStore:Lcom/squareup/capital/flexloan/CapitalFlexLoanRequestDataStore;

.field private final flexLoanStatusRequestRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lio/reactivex/Single<",
            "Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/capital/flexloan/CapitalFlexLoanRequestDataStore;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "capitalFlexLoanRequestDataStore"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanDataRepository;->capitalFlexLoanRequestDataStore:Lcom/squareup/capital/flexloan/CapitalFlexLoanRequestDataStore;

    .line 18
    iget-object p1, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanDataRepository;->capitalFlexLoanRequestDataStore:Lcom/squareup/capital/flexloan/CapitalFlexLoanRequestDataStore;

    invoke-interface {p1}, Lcom/squareup/capital/flexloan/CapitalFlexLoanRequestDataStore;->fetchFlexLoanStatus()Lio/reactivex/Single;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanDataRepository;->capitalFlexLoanLoanStatusRequester:Lio/reactivex/Single;

    .line 20
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string v0, "BehaviorRelay.create<Sin\u2026CapitalFlexLoanStatus>>()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanDataRepository;->flexLoanStatusRequestRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-void
.end method

.method public static final synthetic access$getCapitalFlexLoanLoanStatusRequester$p(Lcom/squareup/capital/flexloan/RealCapitalFlexLoanDataRepository;)Lio/reactivex/Single;
    .locals 0

    .line 13
    iget-object p0, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanDataRepository;->capitalFlexLoanLoanStatusRequester:Lio/reactivex/Single;

    return-object p0
.end method

.method public static final synthetic access$getFlexLoanStatusRequestRelay$p(Lcom/squareup/capital/flexloan/RealCapitalFlexLoanDataRepository;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 13
    iget-object p0, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanDataRepository;->flexLoanStatusRequestRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method


# virtual methods
.method public fetchFlexLoanStatus()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;",
            ">;"
        }
    .end annotation

    .line 27
    iget-object v0, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanDataRepository;->flexLoanStatusRequestRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanDataRepository$fetchFlexLoanStatus$1;->INSTANCE:Lcom/squareup/capital/flexloan/RealCapitalFlexLoanDataRepository$fetchFlexLoanStatus$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->switchMapSingle(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 28
    new-instance v1, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanDataRepository$fetchFlexLoanStatus$2;

    invoke-direct {v1, p0}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanDataRepository$fetchFlexLoanStatus$2;-><init>(Lcom/squareup/capital/flexloan/RealCapitalFlexLoanDataRepository;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnSubscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "flexLoanStatusRequestRel\u2026tatusRequester)\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public fetchFlexPlan(Ljava/lang/String;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/capital/flexloan/CapitalFlexPlanStatus;",
            ">;"
        }
    .end annotation

    const-string v0, "planId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    iget-object v0, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanDataRepository;->capitalFlexLoanRequestDataStore:Lcom/squareup/capital/flexloan/CapitalFlexLoanRequestDataStore;

    invoke-interface {v0, p1}, Lcom/squareup/capital/flexloan/CapitalFlexLoanRequestDataStore;->fetchFlexPlan(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    .line 34
    invoke-virtual {p1}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object p1

    const-string v0, "capitalFlexLoanRequestDa\u2026)\n        .toObservable()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
