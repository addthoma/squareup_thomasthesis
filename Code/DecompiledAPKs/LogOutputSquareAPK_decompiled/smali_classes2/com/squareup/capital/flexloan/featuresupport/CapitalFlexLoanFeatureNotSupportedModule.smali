.class public abstract Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanFeatureNotSupportedModule;
.super Ljava/lang/Object;
.source "CapitalFlexLoanFeatureNotSupportedModule.kt"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/api/multipassauth/NoopMultipassModule;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\'\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H!J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH!J\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH!J\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H!\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanFeatureNotSupportedModule;",
        "",
        "()V",
        "bindCapitalDataRepository",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanDataRepository;",
        "capitalDataRepository",
        "Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanDataRepositoryNotSupported;",
        "bindCapitalFlexLoanAnalytics",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;",
        "capitalFlexLoanAnalytics",
        "Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanAnalyticsNotSupported;",
        "bindCapitalFlexLoanFeatureNotSupported",
        "Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanFeatureSupport;",
        "capitalFlexLoanFeatureSupport",
        "Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanFeatureNotSupported;",
        "bindCapitalFlexLoanOfferFormatter",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanOfferFormatter;",
        "capitalFlexLoanOfferFormatter",
        "Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanOfferFormatterNotSupported;",
        "impl-unsupported-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract bindCapitalDataRepository(Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanDataRepositoryNotSupported;)Lcom/squareup/capital/flexloan/CapitalFlexLoanDataRepository;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindCapitalFlexLoanAnalytics(Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanAnalyticsNotSupported;)Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindCapitalFlexLoanFeatureNotSupported(Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanFeatureNotSupported;)Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanFeatureSupport;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindCapitalFlexLoanOfferFormatter(Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanOfferFormatterNotSupported;)Lcom/squareup/capital/flexloan/CapitalFlexLoanOfferFormatter;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
