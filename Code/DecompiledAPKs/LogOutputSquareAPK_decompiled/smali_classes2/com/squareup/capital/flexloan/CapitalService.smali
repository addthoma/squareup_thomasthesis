.class public interface abstract Lcom/squareup/capital/flexloan/CapitalService;
.super Ljava/lang/Object;
.source "CapitalService.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0012\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005H\'J\u0018\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u00072\u0008\u0008\u0001\u0010\u0004\u001a\u00020\tH\'\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/capital/flexloan/CapitalService;",
        "",
        "retrievePlan",
        "Lcom/squareup/capital/flexloan/CapitalRetrievePlanResponse;",
        "request",
        "Lcom/squareup/protos/capital/servicing/plan/service/RetrievePlanRequest;",
        "searchCustomers",
        "Lcom/squareup/server/AcceptedResponse;",
        "Lcom/squareup/protos/capital/external/business/service/SearchCustomersResponse;",
        "Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract retrievePlan(Lcom/squareup/protos/capital/servicing/plan/service/RetrievePlanRequest;)Lcom/squareup/capital/flexloan/CapitalRetrievePlanResponse;
    .param p1    # Lcom/squareup/protos/capital/servicing/plan/service/RetrievePlanRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit2/http/Headers;
        value = {
            "Accept: application/x-protobuf"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/capital/servicing/plans/retrieve-plan"
    .end annotation
.end method

.method public abstract searchCustomers(Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/protos/capital/external/business/service/SearchCustomersResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/Headers;
        value = {
            "Accept: application/x-protobuf"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/capital/search_customers"
    .end annotation
.end method
