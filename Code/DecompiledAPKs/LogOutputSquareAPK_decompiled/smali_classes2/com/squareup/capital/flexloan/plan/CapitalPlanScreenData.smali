.class public final Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;
.super Ljava/lang/Object;
.source "CapitalPlanScreenData.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0010\u0008\n\u0002\u0008\u0004\n\u0002\u0010\u0007\n\u0002\u0008)\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001Bu\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0003\u0012\u0006\u0010\u0008\u001a\u00020\u0003\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\n\u0012\u0006\u0010\u000c\u001a\u00020\u0003\u0012\u0006\u0010\r\u001a\u00020\u0003\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u000f\u0012\u0006\u0010\u0011\u001a\u00020\u000f\u0012\u0006\u0010\u0012\u001a\u00020\u0003\u0012\u0006\u0010\u0013\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0014J\t\u0010&\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\'\u001a\u00020\u000fH\u00c6\u0003J\t\u0010(\u001a\u00020\u000fH\u00c6\u0003J\t\u0010)\u001a\u00020\u000fH\u00c6\u0003J\t\u0010*\u001a\u00020\u0003H\u00c6\u0003J\t\u0010+\u001a\u00020\u0003H\u00c6\u0003J\t\u0010,\u001a\u00020\u0005H\u00c6\u0003J\t\u0010-\u001a\u00020\u0005H\u00c6\u0003J\t\u0010.\u001a\u00020\u0003H\u00c6\u0003J\t\u0010/\u001a\u00020\u0003H\u00c6\u0003J\t\u00100\u001a\u00020\nH\u00c6\u0003J\t\u00101\u001a\u00020\nH\u00c6\u0003J\t\u00102\u001a\u00020\u0003H\u00c6\u0003J\t\u00103\u001a\u00020\u0003H\u00c6\u0003J\u0095\u0001\u00104\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u00032\u0008\u0008\u0002\u0010\t\u001a\u00020\n2\u0008\u0008\u0002\u0010\u000b\u001a\u00020\n2\u0008\u0008\u0002\u0010\u000c\u001a\u00020\u00032\u0008\u0008\u0002\u0010\r\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u000e\u001a\u00020\u000f2\u0008\u0008\u0002\u0010\u0010\u001a\u00020\u000f2\u0008\u0008\u0002\u0010\u0011\u001a\u00020\u000f2\u0008\u0008\u0002\u0010\u0012\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0013\u001a\u00020\u0003H\u00c6\u0001J\u0013\u00105\u001a\u00020\u00052\u0008\u00106\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u00107\u001a\u00020\nH\u00d6\u0001J\t\u00108\u001a\u000209H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0018R\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0018R\u0011\u0010\u0007\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u0016R\u0011\u0010\u0008\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u0016R\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u001cR\u0011\u0010\u000b\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001d\u0010\u001cR\u0011\u0010\u000c\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001e\u0010\u0016R\u0011\u0010\r\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001f\u0010\u0016R\u0011\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008 \u0010!R\u0011\u0010\u0010\u001a\u00020\u000f\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\"\u0010!R\u0011\u0010\u0011\u001a\u00020\u000f\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008#\u0010!R\u0011\u0010\u0012\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008$\u0010\u0016R\u0011\u0010\u0013\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008%\u0010\u0016\u00a8\u0006:"
    }
    d2 = {
        "Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;",
        "",
        "buttonLinkText",
        "",
        "displayAmountDue",
        "",
        "isPastDue",
        "outstandingAmountText",
        "paymentDueAmountText",
        "paymentDueBulletColor",
        "",
        "paymentDueDateColor",
        "paymentDueDateText",
        "paymentDueText",
        "percentDue",
        "",
        "percentOutstanding",
        "percentPaid",
        "percentPaidText",
        "totalPaid",
        "(Ljava/lang/CharSequence;ZZLjava/lang/CharSequence;Ljava/lang/CharSequence;IILjava/lang/CharSequence;Ljava/lang/CharSequence;FFFLjava/lang/CharSequence;Ljava/lang/CharSequence;)V",
        "getButtonLinkText",
        "()Ljava/lang/CharSequence;",
        "getDisplayAmountDue",
        "()Z",
        "getOutstandingAmountText",
        "getPaymentDueAmountText",
        "getPaymentDueBulletColor",
        "()I",
        "getPaymentDueDateColor",
        "getPaymentDueDateText",
        "getPaymentDueText",
        "getPercentDue",
        "()F",
        "getPercentOutstanding",
        "getPercentPaid",
        "getPercentPaidText",
        "getTotalPaid",
        "component1",
        "component10",
        "component11",
        "component12",
        "component13",
        "component14",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "component9",
        "copy",
        "equals",
        "other",
        "hashCode",
        "toString",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final buttonLinkText:Ljava/lang/CharSequence;

.field private final displayAmountDue:Z

.field private final isPastDue:Z

.field private final outstandingAmountText:Ljava/lang/CharSequence;

.field private final paymentDueAmountText:Ljava/lang/CharSequence;

.field private final paymentDueBulletColor:I

.field private final paymentDueDateColor:I

.field private final paymentDueDateText:Ljava/lang/CharSequence;

.field private final paymentDueText:Ljava/lang/CharSequence;

.field private final percentDue:F

.field private final percentOutstanding:F

.field private final percentPaid:F

.field private final percentPaidText:Ljava/lang/CharSequence;

.field private final totalPaid:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Ljava/lang/CharSequence;ZZLjava/lang/CharSequence;Ljava/lang/CharSequence;IILjava/lang/CharSequence;Ljava/lang/CharSequence;FFFLjava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 1

    const-string v0, "buttonLinkText"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "outstandingAmountText"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentDueAmountText"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentDueDateText"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentDueText"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "percentPaidText"

    invoke-static {p13, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "totalPaid"

    invoke-static {p14, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->buttonLinkText:Ljava/lang/CharSequence;

    iput-boolean p2, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->displayAmountDue:Z

    iput-boolean p3, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->isPastDue:Z

    iput-object p4, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->outstandingAmountText:Ljava/lang/CharSequence;

    iput-object p5, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->paymentDueAmountText:Ljava/lang/CharSequence;

    iput p6, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->paymentDueBulletColor:I

    iput p7, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->paymentDueDateColor:I

    iput-object p8, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->paymentDueDateText:Ljava/lang/CharSequence;

    iput-object p9, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->paymentDueText:Ljava/lang/CharSequence;

    iput p10, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->percentDue:F

    iput p11, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->percentOutstanding:F

    iput p12, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->percentPaid:F

    iput-object p13, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->percentPaidText:Ljava/lang/CharSequence;

    iput-object p14, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->totalPaid:Ljava/lang/CharSequence;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;Ljava/lang/CharSequence;ZZLjava/lang/CharSequence;Ljava/lang/CharSequence;IILjava/lang/CharSequence;Ljava/lang/CharSequence;FFFLjava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/Object;)Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;
    .locals 15

    move-object v0, p0

    move/from16 v1, p15

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->buttonLinkText:Ljava/lang/CharSequence;

    goto :goto_0

    :cond_0
    move-object/from16 v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-boolean v3, v0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->displayAmountDue:Z

    goto :goto_1

    :cond_1
    move/from16 v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-boolean v4, v0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->isPastDue:Z

    goto :goto_2

    :cond_2
    move/from16 v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->outstandingAmountText:Ljava/lang/CharSequence;

    goto :goto_3

    :cond_3
    move-object/from16 v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-object v6, v0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->paymentDueAmountText:Ljava/lang/CharSequence;

    goto :goto_4

    :cond_4
    move-object/from16 v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget v7, v0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->paymentDueBulletColor:I

    goto :goto_5

    :cond_5
    move/from16 v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget v8, v0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->paymentDueDateColor:I

    goto :goto_6

    :cond_6
    move/from16 v8, p7

    :goto_6
    and-int/lit16 v9, v1, 0x80

    if-eqz v9, :cond_7

    iget-object v9, v0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->paymentDueDateText:Ljava/lang/CharSequence;

    goto :goto_7

    :cond_7
    move-object/from16 v9, p8

    :goto_7
    and-int/lit16 v10, v1, 0x100

    if-eqz v10, :cond_8

    iget-object v10, v0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->paymentDueText:Ljava/lang/CharSequence;

    goto :goto_8

    :cond_8
    move-object/from16 v10, p9

    :goto_8
    and-int/lit16 v11, v1, 0x200

    if-eqz v11, :cond_9

    iget v11, v0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->percentDue:F

    goto :goto_9

    :cond_9
    move/from16 v11, p10

    :goto_9
    and-int/lit16 v12, v1, 0x400

    if-eqz v12, :cond_a

    iget v12, v0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->percentOutstanding:F

    goto :goto_a

    :cond_a
    move/from16 v12, p11

    :goto_a
    and-int/lit16 v13, v1, 0x800

    if-eqz v13, :cond_b

    iget v13, v0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->percentPaid:F

    goto :goto_b

    :cond_b
    move/from16 v13, p12

    :goto_b
    and-int/lit16 v14, v1, 0x1000

    if-eqz v14, :cond_c

    iget-object v14, v0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->percentPaidText:Ljava/lang/CharSequence;

    goto :goto_c

    :cond_c
    move-object/from16 v14, p13

    :goto_c
    and-int/lit16 v1, v1, 0x2000

    if-eqz v1, :cond_d

    iget-object v1, v0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->totalPaid:Ljava/lang/CharSequence;

    goto :goto_d

    :cond_d
    move-object/from16 v1, p14

    :goto_d
    move-object/from16 p1, v2

    move/from16 p2, v3

    move/from16 p3, v4

    move-object/from16 p4, v5

    move-object/from16 p5, v6

    move/from16 p6, v7

    move/from16 p7, v8

    move-object/from16 p8, v9

    move-object/from16 p9, v10

    move/from16 p10, v11

    move/from16 p11, v12

    move/from16 p12, v13

    move-object/from16 p13, v14

    move-object/from16 p14, v1

    invoke-virtual/range {p0 .. p14}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->copy(Ljava/lang/CharSequence;ZZLjava/lang/CharSequence;Ljava/lang/CharSequence;IILjava/lang/CharSequence;Ljava/lang/CharSequence;FFFLjava/lang/CharSequence;Ljava/lang/CharSequence;)Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->buttonLinkText:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final component10()F
    .locals 1

    iget v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->percentDue:F

    return v0
.end method

.method public final component11()F
    .locals 1

    iget v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->percentOutstanding:F

    return v0
.end method

.method public final component12()F
    .locals 1

    iget v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->percentPaid:F

    return v0
.end method

.method public final component13()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->percentPaidText:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final component14()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->totalPaid:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->displayAmountDue:Z

    return v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->isPastDue:Z

    return v0
.end method

.method public final component4()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->outstandingAmountText:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final component5()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->paymentDueAmountText:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final component6()I
    .locals 1

    iget v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->paymentDueBulletColor:I

    return v0
.end method

.method public final component7()I
    .locals 1

    iget v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->paymentDueDateColor:I

    return v0
.end method

.method public final component8()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->paymentDueDateText:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final component9()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->paymentDueText:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final copy(Ljava/lang/CharSequence;ZZLjava/lang/CharSequence;Ljava/lang/CharSequence;IILjava/lang/CharSequence;Ljava/lang/CharSequence;FFFLjava/lang/CharSequence;Ljava/lang/CharSequence;)Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;
    .locals 16

    const-string v0, "buttonLinkText"

    move-object/from16 v2, p1

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "outstandingAmountText"

    move-object/from16 v5, p4

    invoke-static {v5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentDueAmountText"

    move-object/from16 v6, p5

    invoke-static {v6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentDueDateText"

    move-object/from16 v9, p8

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentDueText"

    move-object/from16 v10, p9

    invoke-static {v10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "percentPaidText"

    move-object/from16 v14, p13

    invoke-static {v14, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "totalPaid"

    move-object/from16 v15, p14

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;

    move-object v1, v0

    move/from16 v3, p2

    move/from16 v4, p3

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v11, p10

    move/from16 v12, p11

    move/from16 v13, p12

    invoke-direct/range {v1 .. v15}, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;-><init>(Ljava/lang/CharSequence;ZZLjava/lang/CharSequence;Ljava/lang/CharSequence;IILjava/lang/CharSequence;Ljava/lang/CharSequence;FFFLjava/lang/CharSequence;Ljava/lang/CharSequence;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;

    iget-object v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->buttonLinkText:Ljava/lang/CharSequence;

    iget-object v1, p1, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->buttonLinkText:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->displayAmountDue:Z

    iget-boolean v1, p1, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->displayAmountDue:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->isPastDue:Z

    iget-boolean v1, p1, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->isPastDue:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->outstandingAmountText:Ljava/lang/CharSequence;

    iget-object v1, p1, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->outstandingAmountText:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->paymentDueAmountText:Ljava/lang/CharSequence;

    iget-object v1, p1, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->paymentDueAmountText:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->paymentDueBulletColor:I

    iget v1, p1, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->paymentDueBulletColor:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->paymentDueDateColor:I

    iget v1, p1, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->paymentDueDateColor:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->paymentDueDateText:Ljava/lang/CharSequence;

    iget-object v1, p1, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->paymentDueDateText:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->paymentDueText:Ljava/lang/CharSequence;

    iget-object v1, p1, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->paymentDueText:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->percentDue:F

    iget v1, p1, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->percentDue:F

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->percentOutstanding:F

    iget v1, p1, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->percentOutstanding:F

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->percentPaid:F

    iget v1, p1, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->percentPaid:F

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->percentPaidText:Ljava/lang/CharSequence;

    iget-object v1, p1, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->percentPaidText:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->totalPaid:Ljava/lang/CharSequence;

    iget-object p1, p1, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->totalPaid:Ljava/lang/CharSequence;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getButtonLinkText()Ljava/lang/CharSequence;
    .locals 1

    .line 4
    iget-object v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->buttonLinkText:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getDisplayAmountDue()Z
    .locals 1

    .line 5
    iget-boolean v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->displayAmountDue:Z

    return v0
.end method

.method public final getOutstandingAmountText()Ljava/lang/CharSequence;
    .locals 1

    .line 7
    iget-object v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->outstandingAmountText:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getPaymentDueAmountText()Ljava/lang/CharSequence;
    .locals 1

    .line 8
    iget-object v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->paymentDueAmountText:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getPaymentDueBulletColor()I
    .locals 1

    .line 9
    iget v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->paymentDueBulletColor:I

    return v0
.end method

.method public final getPaymentDueDateColor()I
    .locals 1

    .line 10
    iget v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->paymentDueDateColor:I

    return v0
.end method

.method public final getPaymentDueDateText()Ljava/lang/CharSequence;
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->paymentDueDateText:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getPaymentDueText()Ljava/lang/CharSequence;
    .locals 1

    .line 12
    iget-object v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->paymentDueText:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getPercentDue()F
    .locals 1

    .line 13
    iget v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->percentDue:F

    return v0
.end method

.method public final getPercentOutstanding()F
    .locals 1

    .line 14
    iget v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->percentOutstanding:F

    return v0
.end method

.method public final getPercentPaid()F
    .locals 1

    .line 15
    iget v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->percentPaid:F

    return v0
.end method

.method public final getPercentPaidText()Ljava/lang/CharSequence;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->percentPaidText:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getTotalPaid()Ljava/lang/CharSequence;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->totalPaid:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    iget-object v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->buttonLinkText:Ljava/lang/CharSequence;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->displayAmountDue:Z

    const/4 v3, 0x1

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->isPastDue:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->outstandingAmountText:Ljava/lang/CharSequence;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->paymentDueAmountText:Ljava/lang/CharSequence;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_4
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->paymentDueBulletColor:I

    invoke-static {v2}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->paymentDueDateColor:I

    invoke-static {v2}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->paymentDueDateText:Ljava/lang/CharSequence;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_5
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->paymentDueText:Ljava/lang/CharSequence;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_6
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->percentDue:F

    invoke-static {v2}, L$r8$java8methods$utility$Float$hashCode$IF;->hashCode(F)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->percentOutstanding:F

    invoke-static {v2}, L$r8$java8methods$utility$Float$hashCode$IF;->hashCode(F)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->percentPaid:F

    invoke-static {v2}, L$r8$java8methods$utility$Float$hashCode$IF;->hashCode(F)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->percentPaidText:Ljava/lang/CharSequence;

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_7
    const/4 v2, 0x0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->totalPaid:Ljava/lang/CharSequence;

    if-eqz v2, :cond_8

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_8
    add-int/2addr v0, v1

    return v0
.end method

.method public final isPastDue()Z
    .locals 1

    .line 6
    iget-boolean v0, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->isPastDue:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CapitalPlanScreenData(buttonLinkText="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->buttonLinkText:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const-string v1, ", displayAmountDue="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->displayAmountDue:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isPastDue="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->isPastDue:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", outstandingAmountText="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->outstandingAmountText:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const-string v1, ", paymentDueAmountText="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->paymentDueAmountText:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const-string v1, ", paymentDueBulletColor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->paymentDueBulletColor:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", paymentDueDateColor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->paymentDueDateColor:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", paymentDueDateText="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->paymentDueDateText:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const-string v1, ", paymentDueText="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->paymentDueText:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const-string v1, ", percentDue="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->percentDue:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", percentOutstanding="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->percentOutstanding:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", percentPaid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->percentPaid:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", percentPaidText="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->percentPaidText:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const-string v1, ", totalPaid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenData;->totalPaid:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
