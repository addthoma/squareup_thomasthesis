.class public final synthetic Lcom/squareup/cardreader/ble/-$$Lambda$BleConnectionStateMachine$i2ajRuGe7F8-LCZ8PCQBi32peog;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final synthetic f$0:Lcom/squareup/cardreader/ble/BleConnectionStateMachine;

.field private final synthetic f$1:Lcom/squareup/cardreader/ble/BleConnectionStateMachine$FailureMode;

.field private final synthetic f$2:Lcom/squareup/cardreader/WirelessConnection;

.field private final synthetic f$3:Lcom/squareup/dipper/events/BleErrorType;

.field private final synthetic f$4:I


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/cardreader/ble/BleConnectionStateMachine;Lcom/squareup/cardreader/ble/BleConnectionStateMachine$FailureMode;Lcom/squareup/cardreader/WirelessConnection;Lcom/squareup/dipper/events/BleErrorType;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/cardreader/ble/-$$Lambda$BleConnectionStateMachine$i2ajRuGe7F8-LCZ8PCQBi32peog;->f$0:Lcom/squareup/cardreader/ble/BleConnectionStateMachine;

    iput-object p2, p0, Lcom/squareup/cardreader/ble/-$$Lambda$BleConnectionStateMachine$i2ajRuGe7F8-LCZ8PCQBi32peog;->f$1:Lcom/squareup/cardreader/ble/BleConnectionStateMachine$FailureMode;

    iput-object p3, p0, Lcom/squareup/cardreader/ble/-$$Lambda$BleConnectionStateMachine$i2ajRuGe7F8-LCZ8PCQBi32peog;->f$2:Lcom/squareup/cardreader/WirelessConnection;

    iput-object p4, p0, Lcom/squareup/cardreader/ble/-$$Lambda$BleConnectionStateMachine$i2ajRuGe7F8-LCZ8PCQBi32peog;->f$3:Lcom/squareup/dipper/events/BleErrorType;

    iput p5, p0, Lcom/squareup/cardreader/ble/-$$Lambda$BleConnectionStateMachine$i2ajRuGe7F8-LCZ8PCQBi32peog;->f$4:I

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    iget-object v0, p0, Lcom/squareup/cardreader/ble/-$$Lambda$BleConnectionStateMachine$i2ajRuGe7F8-LCZ8PCQBi32peog;->f$0:Lcom/squareup/cardreader/ble/BleConnectionStateMachine;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/-$$Lambda$BleConnectionStateMachine$i2ajRuGe7F8-LCZ8PCQBi32peog;->f$1:Lcom/squareup/cardreader/ble/BleConnectionStateMachine$FailureMode;

    iget-object v2, p0, Lcom/squareup/cardreader/ble/-$$Lambda$BleConnectionStateMachine$i2ajRuGe7F8-LCZ8PCQBi32peog;->f$2:Lcom/squareup/cardreader/WirelessConnection;

    iget-object v3, p0, Lcom/squareup/cardreader/ble/-$$Lambda$BleConnectionStateMachine$i2ajRuGe7F8-LCZ8PCQBi32peog;->f$3:Lcom/squareup/dipper/events/BleErrorType;

    iget v4, p0, Lcom/squareup/cardreader/ble/-$$Lambda$BleConnectionStateMachine$i2ajRuGe7F8-LCZ8PCQBi32peog;->f$4:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->lambda$firePairingFailure$7$BleConnectionStateMachine(Lcom/squareup/cardreader/ble/BleConnectionStateMachine$FailureMode;Lcom/squareup/cardreader/WirelessConnection;Lcom/squareup/dipper/events/BleErrorType;I)V

    return-void
.end method
