.class public final Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleReceiverFactoryFactory;
.super Ljava/lang/Object;
.source "BleDeviceModule_ProvideBleReceiverFactoryFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/ble/BleReceiverFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final bleBackendProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleBackendLegacy;",
            ">;"
        }
    .end annotation
.end field

.field private final bleSenderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleSender;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderInfoProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderListenersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/RealCardReaderListeners;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleBackendLegacy;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleSender;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/RealCardReaderListeners;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;)V"
        }
    .end annotation

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleReceiverFactoryFactory;->bleBackendProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p2, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleReceiverFactoryFactory;->bleSenderProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p3, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleReceiverFactoryFactory;->cardReaderInfoProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p4, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleReceiverFactoryFactory;->cardReaderListenersProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p5, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleReceiverFactoryFactory;->mainThreadProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleReceiverFactoryFactory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleBackendLegacy;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleSender;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/RealCardReaderListeners;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;)",
            "Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleReceiverFactoryFactory;"
        }
    .end annotation

    .line 52
    new-instance v6, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleReceiverFactoryFactory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleReceiverFactoryFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static provideBleReceiverFactory(Lcom/squareup/cardreader/ble/BleBackendLegacy;Lcom/squareup/cardreader/ble/BleSender;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/RealCardReaderListeners;Lcom/squareup/thread/executor/MainThread;)Lcom/squareup/cardreader/ble/BleReceiverFactory;
    .locals 0

    .line 58
    invoke-static {p0, p1, p2, p3, p4}, Lcom/squareup/cardreader/ble/BleDeviceModule;->provideBleReceiverFactory(Lcom/squareup/cardreader/ble/BleBackendLegacy;Lcom/squareup/cardreader/ble/BleSender;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/RealCardReaderListeners;Lcom/squareup/thread/executor/MainThread;)Lcom/squareup/cardreader/ble/BleReceiverFactory;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/ble/BleReceiverFactory;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/ble/BleReceiverFactory;
    .locals 5

    .line 44
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleReceiverFactoryFactory;->bleBackendProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/ble/BleBackendLegacy;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleReceiverFactoryFactory;->bleSenderProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/ble/BleSender;

    iget-object v2, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleReceiverFactoryFactory;->cardReaderInfoProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cardreader/CardReaderInfo;

    iget-object v3, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleReceiverFactoryFactory;->cardReaderListenersProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/RealCardReaderListeners;

    iget-object v4, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleReceiverFactoryFactory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/thread/executor/MainThread;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleReceiverFactoryFactory;->provideBleReceiverFactory(Lcom/squareup/cardreader/ble/BleBackendLegacy;Lcom/squareup/cardreader/ble/BleSender;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/RealCardReaderListeners;Lcom/squareup/thread/executor/MainThread;)Lcom/squareup/cardreader/ble/BleReceiverFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleReceiverFactoryFactory;->get()Lcom/squareup/cardreader/ble/BleReceiverFactory;

    move-result-object v0

    return-object v0
.end method
