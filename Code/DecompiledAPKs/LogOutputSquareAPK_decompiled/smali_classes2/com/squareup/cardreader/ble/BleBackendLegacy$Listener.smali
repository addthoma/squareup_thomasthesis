.class public interface abstract Lcom/squareup/cardreader/ble/BleBackendLegacy$Listener;
.super Ljava/lang/Object;
.source "BleBackendLegacy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/ble/BleBackendLegacy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract forgetBond()V
.end method

.method public abstract readAckVector()V
.end method

.method public abstract readMtu()V
.end method

.method public abstract writeData([B)V
.end method
