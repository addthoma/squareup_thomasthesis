.class Lcom/squareup/cardreader/ble/R12Gatt;
.super Ljava/lang/Object;
.source "R12Gatt.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;,
        Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;
    }
.end annotation


# static fields
.field public static final DEVICE_INFORMATION_CHARACTERISTICS:[Ljava/util/UUID;

.field public static final LCR_CHARACTERISTICS:[Ljava/util/UUID;

.field public static final R12_AVAILABLE_TO_BOND_OFFSET:I = 0x0

.field public static final SQUARE_BLE_KEY:I = 0x827e

.field public static final UUID_CHARACTERISTIC_BOND_STATUS:Ljava/util/UUID;

.field public static final UUID_CHARACTERISTIC_COMMS_VERSION:Ljava/util/UUID;

.field public static final UUID_CHARACTERISTIC_CONNECTION_CONTROL:Ljava/util/UUID;

.field public static final UUID_CHARACTERISTIC_READ_ACK_VECTOR:Ljava/util/UUID;

.field public static final UUID_CHARACTERISTIC_READ_CONN_INTERVAL:Ljava/util/UUID;

.field public static final UUID_CHARACTERISTIC_READ_DATA:Ljava/util/UUID;

.field public static final UUID_CHARACTERISTIC_READ_MTU:Ljava/util/UUID;

.field public static final UUID_CHARACTERISTIC_SERIAL_NUMBER:Ljava/util/UUID;

.field public static final UUID_CHARACTERISTIC_SERVICE_VERSION:Ljava/util/UUID;

.field public static final UUID_CHARACTERISTIC_WRITE_DATA:Ljava/util/UUID;

.field public static final UUID_CLIENT_CHARACTERISTIC_CONFIG_DESCRIPTOR:Ljava/util/UUID;

.field public static final UUID_DEVICE_INFORMATION_SERVICE:Ljava/util/UUID;

.field public static final UUID_LCR_SERVICE:Ljava/util/UUID;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const-string v0, "00002902-0000-1000-8000-00805F9B34FB"

    .line 41
    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_CLIENT_CHARACTERISTIC_CONFIG_DESCRIPTOR:Ljava/util/UUID;

    const-string v0, "1581EE61-0815-4B7C-B117-BED8758FEE7C"

    .line 43
    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_LCR_SERVICE:Ljava/util/UUID;

    const-string v0, "0000180A-0000-1000-8000-00805F9B34FB"

    .line 44
    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_DEVICE_INFORMATION_SERVICE:Ljava/util/UUID;

    const-string v0, "EEE76878-4864-47AB-B5D7-3D6791A5725B"

    .line 46
    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_CHARACTERISTIC_BOND_STATUS:Ljava/util/UUID;

    const-string v0, "E0C56D1C-BE88-4ADA-AE89-42FEB563F4E9"

    .line 47
    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_CHARACTERISTIC_CONNECTION_CONTROL:Ljava/util/UUID;

    const-string v0, "E0AB0DD2-88BF-11E5-A9DA-24A074E98014"

    .line 48
    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_CHARACTERISTIC_SERVICE_VERSION:Ljava/util/UUID;

    const-string v0, "928A1007-C7B4-431E-B240-490BC916BDC5"

    .line 50
    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_CHARACTERISTIC_WRITE_DATA:Ljava/util/UUID;

    const-string v0, "0D9AD298-29AD-493B-B2CD-09A96C3EF209"

    .line 51
    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_CHARACTERISTIC_READ_ACK_VECTOR:Ljava/util/UUID;

    const-string v0, "59B935AD-C2D4-465C-9683-9CBB9C5181CB"

    .line 52
    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_CHARACTERISTIC_READ_DATA:Ljava/util/UUID;

    const-string v0, "FB973FB2-E496-4954-8FDC-CC49455A5CA7"

    .line 54
    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_CHARACTERISTIC_READ_CONN_INTERVAL:Ljava/util/UUID;

    const-string v0, "0F38F03D-D91C-49C1-A8B0-1B2BA0F01E3A"

    .line 55
    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_CHARACTERISTIC_READ_MTU:Ljava/util/UUID;

    const-string v0, "83668364-6E2E-11E5-B8FE-24A074E98014"

    .line 56
    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_CHARACTERISTIC_COMMS_VERSION:Ljava/util/UUID;

    const-string v0, "00002A25-0000-1000-8000-00805F9B34FB"

    .line 58
    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_CHARACTERISTIC_SERIAL_NUMBER:Ljava/util/UUID;

    const/16 v0, 0x9

    new-array v0, v0, [Ljava/util/UUID;

    .line 60
    sget-object v1, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_CHARACTERISTIC_BOND_STATUS:Ljava/util/UUID;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_CHARACTERISTIC_CONNECTION_CONTROL:Ljava/util/UUID;

    const/4 v3, 0x1

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_CHARACTERISTIC_SERVICE_VERSION:Ljava/util/UUID;

    const/4 v4, 0x2

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_CHARACTERISTIC_WRITE_DATA:Ljava/util/UUID;

    const/4 v4, 0x3

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_CHARACTERISTIC_READ_ACK_VECTOR:Ljava/util/UUID;

    const/4 v4, 0x4

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_CHARACTERISTIC_READ_DATA:Ljava/util/UUID;

    const/4 v4, 0x5

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_CHARACTERISTIC_READ_CONN_INTERVAL:Ljava/util/UUID;

    const/4 v4, 0x6

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_CHARACTERISTIC_READ_MTU:Ljava/util/UUID;

    const/4 v4, 0x7

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_CHARACTERISTIC_COMMS_VERSION:Ljava/util/UUID;

    const/16 v4, 0x8

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/cardreader/ble/R12Gatt;->LCR_CHARACTERISTICS:[Ljava/util/UUID;

    new-array v0, v3, [Ljava/util/UUID;

    .line 72
    sget-object v1, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_CHARACTERISTIC_SERIAL_NUMBER:Ljava/util/UUID;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/cardreader/ble/R12Gatt;->DEVICE_INFORMATION_CHARACTERISTICS:[Ljava/util/UUID;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
