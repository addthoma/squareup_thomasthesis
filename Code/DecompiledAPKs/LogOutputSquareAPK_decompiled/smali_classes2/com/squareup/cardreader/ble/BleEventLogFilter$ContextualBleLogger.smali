.class public Lcom/squareup/cardreader/ble/BleEventLogFilter$ContextualBleLogger;
.super Ljava/lang/Object;
.source "BleEventLogFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/ble/BleEventLogFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ContextualBleLogger"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/cardreader/ble/BleEventLogFilter;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/ble/BleEventLogFilter;)V
    .locals 0

    .line 103
    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleEventLogFilter$ContextualBleLogger;->this$0:Lcom/squareup/cardreader/ble/BleEventLogFilter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public logEvent(Lcom/squareup/analytics/ReaderEventName;)V
    .locals 1

    .line 105
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleEventLogFilter$ContextualBleLogger;->this$0:Lcom/squareup/cardreader/ble/BleEventLogFilter;

    invoke-static {p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent;->eventBuilder(Lcom/squareup/analytics/ReaderEventName;)Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;

    move-result-object p1

    invoke-static {v0, p0, p1}, Lcom/squareup/cardreader/ble/BleEventLogFilter;->access$000(Lcom/squareup/cardreader/ble/BleEventLogFilter;Lcom/squareup/cardreader/ble/BleEventLogFilter$ContextualBleLogger;Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;)V

    return-void
.end method

.method public logEvent(Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/WirelessConnection;)V
    .locals 1

    .line 110
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleEventLogFilter$ContextualBleLogger;->this$0:Lcom/squareup/cardreader/ble/BleEventLogFilter;

    .line 111
    invoke-static {p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent;->eventBuilder(Lcom/squareup/analytics/ReaderEventName;)Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;->forWirelessConnection(Lcom/squareup/cardreader/WirelessConnection;)Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;

    move-result-object p1

    .line 110
    invoke-static {v0, p0, p1}, Lcom/squareup/cardreader/ble/BleEventLogFilter;->access$000(Lcom/squareup/cardreader/ble/BleEventLogFilter;Lcom/squareup/cardreader/ble/BleEventLogFilter$ContextualBleLogger;Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;)V

    return-void
.end method

.method public logEvent(Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/WirelessConnection;Lcom/squareup/dipper/events/BleErrorType;)V
    .locals 1

    .line 120
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleEventLogFilter$ContextualBleLogger;->this$0:Lcom/squareup/cardreader/ble/BleEventLogFilter;

    .line 121
    invoke-static {p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent;->eventBuilder(Lcom/squareup/analytics/ReaderEventName;)Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;->forWirelessConnection(Lcom/squareup/cardreader/WirelessConnection;)Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;

    move-result-object p1

    .line 122
    invoke-virtual {p1, p3}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;->errorType(Lcom/squareup/dipper/events/BleErrorType;)Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;

    move-result-object p1

    .line 120
    invoke-static {v0, p0, p1}, Lcom/squareup/cardreader/ble/BleEventLogFilter;->access$000(Lcom/squareup/cardreader/ble/BleEventLogFilter;Lcom/squareup/cardreader/ble/BleEventLogFilter$ContextualBleLogger;Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;)V

    return-void
.end method

.method public logEvent(Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/dipper/events/BleErrorType;)V
    .locals 1

    .line 115
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleEventLogFilter$ContextualBleLogger;->this$0:Lcom/squareup/cardreader/ble/BleEventLogFilter;

    invoke-static {p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent;->eventBuilder(Lcom/squareup/analytics/ReaderEventName;)Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;->errorType(Lcom/squareup/dipper/events/BleErrorType;)Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;

    move-result-object p1

    invoke-static {v0, p0, p1}, Lcom/squareup/cardreader/ble/BleEventLogFilter;->access$000(Lcom/squareup/cardreader/ble/BleEventLogFilter;Lcom/squareup/cardreader/ble/BleEventLogFilter$ContextualBleLogger;Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;)V

    return-void
.end method
