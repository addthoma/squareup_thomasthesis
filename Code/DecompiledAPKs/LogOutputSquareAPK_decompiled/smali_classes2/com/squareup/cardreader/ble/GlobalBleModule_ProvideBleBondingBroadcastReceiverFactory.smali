.class public final Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleBondingBroadcastReceiverFactory;
.super Ljava/lang/Object;
.source "GlobalBleModule_ProvideBleBondingBroadcastReceiverFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;",
        ">;"
    }
.end annotation


# instance fields
.field private final module:Lcom/squareup/cardreader/ble/GlobalBleModule;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/ble/GlobalBleModule;)V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleBondingBroadcastReceiverFactory;->module:Lcom/squareup/cardreader/ble/GlobalBleModule;

    return-void
.end method

.method public static create(Lcom/squareup/cardreader/ble/GlobalBleModule;)Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleBondingBroadcastReceiverFactory;
    .locals 1

    .line 29
    new-instance v0, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleBondingBroadcastReceiverFactory;

    invoke-direct {v0, p0}, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleBondingBroadcastReceiverFactory;-><init>(Lcom/squareup/cardreader/ble/GlobalBleModule;)V

    return-object v0
.end method

.method public static provideBleBondingBroadcastReceiver(Lcom/squareup/cardreader/ble/GlobalBleModule;)Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;
    .locals 1

    .line 34
    invoke-virtual {p0}, Lcom/squareup/cardreader/ble/GlobalBleModule;->provideBleBondingBroadcastReceiver()Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleBondingBroadcastReceiverFactory;->module:Lcom/squareup/cardreader/ble/GlobalBleModule;

    invoke-static {v0}, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleBondingBroadcastReceiverFactory;->provideBleBondingBroadcastReceiver(Lcom/squareup/cardreader/ble/GlobalBleModule;)Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleBondingBroadcastReceiverFactory;->get()Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;

    move-result-object v0

    return-object v0
.end method
