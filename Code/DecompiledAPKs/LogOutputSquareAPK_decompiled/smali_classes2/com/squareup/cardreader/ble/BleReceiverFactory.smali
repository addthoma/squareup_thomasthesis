.class public Lcom/squareup/cardreader/ble/BleReceiverFactory;
.super Ljava/lang/Object;
.source "BleReceiverFactory.java"


# instance fields
.field private final address:Ljava/lang/String;

.field private final bleBackend:Lcom/squareup/cardreader/ble/BleBackendLegacy;

.field private final bleSender:Lcom/squareup/cardreader/ble/BleSender;

.field private final cardReaderListeners:Lcom/squareup/cardreader/RealCardReaderListeners;

.field private currentInstance:Lcom/squareup/cardreader/ble/BleReceiver;

.field private destroyed:Z

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/ble/BleBackendLegacy;Lcom/squareup/cardreader/ble/BleSender;Ljava/lang/String;Lcom/squareup/cardreader/RealCardReaderListeners;Lcom/squareup/thread/executor/MainThread;)V
    .locals 0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleReceiverFactory;->bleBackend:Lcom/squareup/cardreader/ble/BleBackendLegacy;

    .line 26
    iput-object p2, p0, Lcom/squareup/cardreader/ble/BleReceiverFactory;->bleSender:Lcom/squareup/cardreader/ble/BleSender;

    .line 27
    iput-object p3, p0, Lcom/squareup/cardreader/ble/BleReceiverFactory;->address:Ljava/lang/String;

    .line 28
    iput-object p4, p0, Lcom/squareup/cardreader/ble/BleReceiverFactory;->cardReaderListeners:Lcom/squareup/cardreader/RealCardReaderListeners;

    .line 29
    iput-object p5, p0, Lcom/squareup/cardreader/ble/BleReceiverFactory;->mainThread:Lcom/squareup/thread/executor/MainThread;

    return-void
.end method


# virtual methods
.method public destroyViolently()V
    .locals 4

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 47
    iget-object v2, p0, Lcom/squareup/cardreader/ble/BleReceiverFactory;->address:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "Destroying BleReceiverFactory for %s violently"

    invoke-static {v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 48
    iput-boolean v0, p0, Lcom/squareup/cardreader/ble/BleReceiverFactory;->destroyed:Z

    .line 50
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleReceiverFactory;->currentInstance:Lcom/squareup/cardreader/ble/BleReceiver;

    if-eqz v0, :cond_0

    .line 51
    invoke-virtual {v0}, Lcom/squareup/cardreader/ble/BleReceiver;->destroy()V

    :cond_0
    return-void
.end method

.method public newInstance(Lcom/squareup/cardreader/ble/BleConnectionStateMachine;)Landroid/bluetooth/BluetoothGattCallback;
    .locals 8

    .line 33
    iget-boolean v0, p0, Lcom/squareup/cardreader/ble/BleReceiverFactory;->destroyed:Z

    if-nez v0, :cond_1

    .line 37
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleReceiverFactory;->currentInstance:Lcom/squareup/cardreader/ble/BleReceiver;

    if-eqz v0, :cond_0

    .line 38
    invoke-virtual {v0}, Lcom/squareup/cardreader/ble/BleReceiver;->destroy()V

    .line 41
    :cond_0
    new-instance v0, Lcom/squareup/cardreader/ble/BleReceiver;

    iget-object v2, p0, Lcom/squareup/cardreader/ble/BleReceiverFactory;->address:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/cardreader/ble/BleReceiverFactory;->bleBackend:Lcom/squareup/cardreader/ble/BleBackendLegacy;

    iget-object v5, p0, Lcom/squareup/cardreader/ble/BleReceiverFactory;->bleSender:Lcom/squareup/cardreader/ble/BleSender;

    iget-object v6, p0, Lcom/squareup/cardreader/ble/BleReceiverFactory;->cardReaderListeners:Lcom/squareup/cardreader/RealCardReaderListeners;

    iget-object v7, p0, Lcom/squareup/cardreader/ble/BleReceiverFactory;->mainThread:Lcom/squareup/thread/executor/MainThread;

    move-object v1, v0

    move-object v3, p1

    invoke-direct/range {v1 .. v7}, Lcom/squareup/cardreader/ble/BleReceiver;-><init>(Ljava/lang/String;Lcom/squareup/cardreader/ble/BleConnectionStateMachine;Lcom/squareup/cardreader/ble/BleBackendLegacy;Lcom/squareup/cardreader/ble/BleSender;Lcom/squareup/cardreader/RealCardReaderListeners;Lcom/squareup/thread/executor/MainThread;)V

    iput-object v0, p0, Lcom/squareup/cardreader/ble/BleReceiverFactory;->currentInstance:Lcom/squareup/cardreader/ble/BleReceiver;

    .line 43
    iget-object p1, p0, Lcom/squareup/cardreader/ble/BleReceiverFactory;->currentInstance:Lcom/squareup/cardreader/ble/BleReceiver;

    return-object p1

    .line 34
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "BleReceiverFactory already destroyed!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
