.class public Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;
.super Landroid/content/BroadcastReceiver;
.source "BluetoothStatusReceiver.java"


# instance fields
.field private final bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

.field private final cardReaderFactory:Lcom/squareup/cardreader/CardReaderFactory;

.field private final cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

.field private final externalListeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/cardreader/ble/BluetoothStatusListener;",
            ">;"
        }
    .end annotation
.end field

.field private stateEnabled:Z


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/CardReaderFactory;Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/cardreader/BluetoothUtils;)V
    .locals 1

    .line 28
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    const/4 v0, 0x0

    .line 25
    iput-boolean v0, p0, Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;->stateEnabled:Z

    .line 29
    iput-object p1, p0, Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;->cardReaderFactory:Lcom/squareup/cardreader/CardReaderFactory;

    .line 30
    iput-object p2, p0, Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    .line 31
    new-instance p1, Ljava/util/LinkedHashSet;

    invoke-direct {p1}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object p1, p0, Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;->externalListeners:Ljava/util/Set;

    .line 32
    iput-object p3, p0, Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    return-void
.end method

.method private handleNewState(Z)V
    .locals 1

    .line 64
    iget-boolean v0, p0, Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;->stateEnabled:Z

    if-ne p1, v0, :cond_0

    return-void

    .line 68
    :cond_0
    iput-boolean p1, p0, Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;->stateEnabled:Z

    .line 69
    iget-boolean p1, p0, Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;->stateEnabled:Z

    if-eqz p1, :cond_1

    .line 70
    iget-object p1, p0, Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;->externalListeners:Ljava/util/Set;

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/ble/BluetoothStatusListener;

    .line 71
    invoke-interface {v0}, Lcom/squareup/cardreader/ble/BluetoothStatusListener;->onBluetoothEnabled()V

    goto :goto_0

    .line 74
    :cond_1
    iget-object p1, p0, Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;->cardReaderFactory:Lcom/squareup/cardreader/CardReaderFactory;

    invoke-interface {p1}, Lcom/squareup/cardreader/CardReaderFactory;->destroyAllBluetoothReaders()V

    .line 76
    iget-object p1, p0, Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;->externalListeners:Ljava/util/Set;

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/ble/BluetoothStatusListener;

    .line 77
    invoke-interface {v0}, Lcom/squareup/cardreader/ble/BluetoothStatusListener;->onBluetoothDisabled()V

    goto :goto_1

    :cond_2
    return-void
.end method

.method private logBluetoothState(I)V
    .locals 3

    packed-switch p1, :pswitch_data_0

    const-string p1, "Unknown"

    goto :goto_0

    :pswitch_0
    const-string p1, "Turning Off"

    goto :goto_0

    :pswitch_1
    const-string p1, "On"

    goto :goto_0

    :pswitch_2
    const-string p1, "Turning On"

    goto :goto_0

    :pswitch_3
    const-string p1, "Off"

    .line 102
    :goto_0
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    .line 103
    invoke-interface {v1}, Lcom/squareup/cardreader/BluetoothUtils;->supportsBle()Z

    move-result v1

    iget-object v2, p0, Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    .line 104
    invoke-interface {v2}, Lcom/squareup/cardreader/BluetoothUtils;->supportsBluetooth()Z

    move-result v2

    .line 103
    invoke-interface {v0, p1, v1, v2}, Lcom/squareup/cardreader/ReaderEventLogger;->logBluetoothStatusChanged(Ljava/lang/String;ZZ)V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public addBluetoothStatusListener(Lcom/squareup/cardreader/ble/BluetoothStatusListener;)V
    .locals 1

    .line 108
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;->externalListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public destroy(Landroid/content/Context;)V
    .locals 0

    .line 43
    invoke-virtual {p1, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public initialize(Landroid/content/Context;)V
    .locals 2

    .line 36
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    invoke-interface {v0}, Lcom/squareup/cardreader/BluetoothUtils;->isEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;->stateEnabled:Z

    .line 37
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 39
    iget-boolean p1, p0, Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;->stateEnabled:Z

    if-eqz p1, :cond_0

    const/16 p1, 0xc

    goto :goto_0

    :cond_0
    const/16 p1, 0xa

    :goto_0
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;->logBluetoothState(I)V

    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1

    const-string p1, "android.bluetooth.adapter.extra.STATE"

    const/4 v0, -0x1

    .line 47
    invoke-virtual {p2, p1, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p1

    .line 48
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;->logBluetoothState(I)V

    const/16 p2, 0xa

    if-eq p1, p2, :cond_1

    const/16 p2, 0xc

    if-eq p1, p2, :cond_0

    const/16 p2, 0xd

    if-eq p1, p2, :cond_1

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    .line 54
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;->handleNewState(Z)V

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 59
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;->handleNewState(Z)V

    :goto_0
    return-void
.end method

.method public removeBluetoothStatusListener(Lcom/squareup/cardreader/ble/BluetoothStatusListener;)V
    .locals 1

    .line 112
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;->externalListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-void
.end method
