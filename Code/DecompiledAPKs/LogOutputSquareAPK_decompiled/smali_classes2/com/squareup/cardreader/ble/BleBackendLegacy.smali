.class public Lcom/squareup/cardreader/ble/BleBackendLegacy;
.super Ljava/lang/Object;
.source "BleBackendLegacy.java"

# interfaces
.implements Lcom/squareup/cardreader/LcrBackend;
.implements Lcom/squareup/cardreader/ble/BleBackend;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/ble/BleBackendLegacy$Session;,
        Lcom/squareup/cardreader/ble/BleBackendLegacy$Listener;
    }
.end annotation


# instance fields
.field private bleBackend:Lcom/squareup/cardreader/ble/BleBackendLegacy$Session;

.field private final bleBackendNative:Lcom/squareup/cardreader/lcr/BleBackendNativeInterface;

.field private final cardReaderConnector:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderConnector;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderDispatch:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

.field private final cardReaderPointer:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;"
        }
    .end annotation
.end field

.field private final cardreaderNative:Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;

.field private commsVersion:[B

.field private final lcrExecutor:Ljava/util/concurrent/ExecutorService;

.field private final listener:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleBackendLegacy$Listener;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private final tmnTimings:Lcom/squareup/tmn/TmnTimings;


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Lcom/squareup/cardreader/CardReaderInfo;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljava/util/concurrent/ExecutorService;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cardreader/lcr/BleBackendNativeInterface;Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;Lcom/squareup/tmn/TmnTimings;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleBackendLegacy$Listener;",
            ">;",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderConnector;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/squareup/thread/executor/MainThread;",
            "Lcom/squareup/cardreader/lcr/BleBackendNativeInterface;",
            "Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;",
            "Lcom/squareup/tmn/TmnTimings;",
            ")V"
        }
    .end annotation

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleBackendLegacy;->listener:Ljavax/inject/Provider;

    .line 46
    iput-object p2, p0, Lcom/squareup/cardreader/ble/BleBackendLegacy;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    .line 47
    iput-object p3, p0, Lcom/squareup/cardreader/ble/BleBackendLegacy;->cardReaderConnector:Ljavax/inject/Provider;

    .line 48
    iput-object p4, p0, Lcom/squareup/cardreader/ble/BleBackendLegacy;->cardReaderDispatch:Ljavax/inject/Provider;

    .line 49
    iput-object p5, p0, Lcom/squareup/cardreader/ble/BleBackendLegacy;->cardReaderPointer:Ljavax/inject/Provider;

    .line 50
    iput-object p6, p0, Lcom/squareup/cardreader/ble/BleBackendLegacy;->lcrExecutor:Ljava/util/concurrent/ExecutorService;

    .line 51
    iput-object p7, p0, Lcom/squareup/cardreader/ble/BleBackendLegacy;->mainThread:Lcom/squareup/thread/executor/MainThread;

    .line 52
    iput-object p8, p0, Lcom/squareup/cardreader/ble/BleBackendLegacy;->bleBackendNative:Lcom/squareup/cardreader/lcr/BleBackendNativeInterface;

    .line 53
    iput-object p9, p0, Lcom/squareup/cardreader/ble/BleBackendLegacy;->cardreaderNative:Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;

    .line 54
    iput-object p10, p0, Lcom/squareup/cardreader/ble/BleBackendLegacy;->tmnTimings:Lcom/squareup/tmn/TmnTimings;

    return-void
.end method


# virtual methods
.method public declared-synchronized cardReaderInitialized()V
    .locals 2

    monitor-enter p0

    .line 82
    :try_start_0
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendLegacy;->cardreaderNative:Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleBackendLegacy;->cardReaderPointer:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/CardReaderPointer;

    invoke-virtual {v1}, Lcom/squareup/cardreader/CardReaderPointer;->getId()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;->cr_cardreader_notify_reader_plugged(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)Lcom/squareup/cardreader/lcr/CrCardreaderResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 83
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public forgetReader()V
    .locals 1

    .line 62
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendLegacy;->listener:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/ble/BleBackendLegacy$Listener;

    invoke-interface {v0}, Lcom/squareup/cardreader/ble/BleBackendLegacy$Listener;->forgetBond()V

    return-void
.end method

.method public initialize(Lcom/squareup/cardreader/TimerApiLegacy$TimerConfig;Lcom/squareup/cardreader/CardReaderFeatureLegacy$CardReaderFeatureListener;Lcom/squareup/cardreader/CardReaderFeatureLegacy;)Lcom/squareup/cardreader/CardReaderPointer;
    .locals 3

    .line 72
    iget-object p2, p0, Lcom/squareup/cardreader/ble/BleBackendLegacy;->bleBackend:Lcom/squareup/cardreader/ble/BleBackendLegacy$Session;

    if-nez p2, :cond_0

    .line 73
    new-instance p2, Lcom/squareup/cardreader/ble/BleBackendLegacy$Session;

    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendLegacy;->bleBackendNative:Lcom/squareup/cardreader/lcr/BleBackendNativeInterface;

    invoke-interface {v0}, Lcom/squareup/cardreader/lcr/BleBackendNativeInterface;->cr_comms_backend_ble_alloc()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;

    move-result-object v0

    invoke-direct {p2, p0, v0}, Lcom/squareup/cardreader/ble/BleBackendLegacy$Session;-><init>(Lcom/squareup/cardreader/ble/BleBackendLegacy;Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;)V

    iput-object p2, p0, Lcom/squareup/cardreader/ble/BleBackendLegacy;->bleBackend:Lcom/squareup/cardreader/ble/BleBackendLegacy$Session;

    .line 75
    new-instance p2, Lcom/squareup/cardreader/LcrBackend$BackendSession;

    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendLegacy;->bleBackendNative:Lcom/squareup/cardreader/lcr/BleBackendNativeInterface;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleBackendLegacy;->bleBackend:Lcom/squareup/cardreader/ble/BleBackendLegacy$Session;

    iget-object v1, v1, Lcom/squareup/cardreader/ble/BleBackendLegacy$Session;->backend:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;

    iget-object v2, p0, Lcom/squareup/cardreader/ble/BleBackendLegacy;->commsVersion:[B

    .line 76
    invoke-interface {v0, v1, v2, p0}, Lcom/squareup/cardreader/lcr/BleBackendNativeInterface;->initialize_backend_ble(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;[BLjava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_api_t;

    move-result-object v0

    invoke-direct {p2, v0}, Lcom/squareup/cardreader/LcrBackend$BackendSession;-><init>(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_api_t;)V

    .line 77
    new-instance v0, Lcom/squareup/cardreader/CardReaderPointer;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleBackendLegacy;->cardreaderNative:Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;

    iget-object p2, p2, Lcom/squareup/cardreader/LcrBackend$BackendSession;->api:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_api_t;

    iget-object p1, p1, Lcom/squareup/cardreader/TimerApiLegacy$TimerConfig;->timer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_crs_timer_api_t;

    invoke-interface {v1, p2, p1, p3}, Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;->cardreader_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_api_t;Lcom/squareup/cardreader/lcr/SWIGTYPE_p_crs_timer_api_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/cardreader/CardReaderPointer;-><init>(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)V

    return-object v0

    .line 72
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "BLE backend already exists!"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public initializeCardReader()V
    .locals 1

    .line 66
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendLegacy;->cardReaderDispatch:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderDispatch;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderDispatch;->initializeCardReader()V

    .line 67
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendLegacy;->cardReaderConnector:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderConnector;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderConnector;->onCardReaderConnected()V

    return-void
.end method

.method public synthetic lambda$onAckVectorReceived$1$BleBackendLegacy(Ljava/lang/Integer;)V
    .locals 2

    .line 117
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendLegacy;->bleBackend:Lcom/squareup/cardreader/ble/BleBackendLegacy$Session;

    if-eqz v0, :cond_1

    iget-object v0, v0, Lcom/squareup/cardreader/ble/BleBackendLegacy$Session;->backend:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;

    if-nez v0, :cond_0

    goto :goto_0

    .line 118
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendLegacy;->tmnTimings:Lcom/squareup/tmn/TmnTimings;

    sget-object v1, Lcom/squareup/tmn/What;->BLE_BACKEND_VECTOR_TO_LCR:Lcom/squareup/tmn/What;

    invoke-virtual {v0, v1}, Lcom/squareup/tmn/TmnTimings;->event(Lcom/squareup/tmn/What;)V

    .line 119
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendLegacy;->bleBackendNative:Lcom/squareup/cardreader/lcr/BleBackendNativeInterface;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleBackendLegacy;->bleBackend:Lcom/squareup/cardreader/ble/BleBackendLegacy$Session;

    iget-object v1, v1, Lcom/squareup/cardreader/ble/BleBackendLegacy$Session;->backend:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;

    .line 120
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    .line 119
    invoke-interface {v0, v1, p1}, Lcom/squareup/cardreader/lcr/BleBackendNativeInterface;->ble_received_data_from_characteristic_ack_vector(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;I)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_result_t;

    :cond_1
    :goto_0
    return-void
.end method

.method public synthetic lambda$onDataReceived$0$BleBackendLegacy([B)V
    .locals 3

    .line 104
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendLegacy;->bleBackend:Lcom/squareup/cardreader/ble/BleBackendLegacy$Session;

    if-eqz v0, :cond_1

    iget-object v0, v0, Lcom/squareup/cardreader/ble/BleBackendLegacy$Session;->backend:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;

    if-nez v0, :cond_0

    goto :goto_0

    .line 106
    :cond_0
    array-length v0, p1

    new-array v0, v0, [B

    .line 107
    array-length v1, p1

    const/4 v2, 0x0

    invoke-static {p1, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 108
    iget-object p1, p0, Lcom/squareup/cardreader/ble/BleBackendLegacy;->tmnTimings:Lcom/squareup/tmn/TmnTimings;

    sget-object v1, Lcom/squareup/tmn/What;->BLE_BACKEND_BYTES_TO_LCR:Lcom/squareup/tmn/What;

    invoke-virtual {p1, v1}, Lcom/squareup/tmn/TmnTimings;->event(Lcom/squareup/tmn/What;)V

    .line 109
    iget-object p1, p0, Lcom/squareup/cardreader/ble/BleBackendLegacy;->bleBackendNative:Lcom/squareup/cardreader/lcr/BleBackendNativeInterface;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleBackendLegacy;->bleBackend:Lcom/squareup/cardreader/ble/BleBackendLegacy$Session;

    iget-object v1, v1, Lcom/squareup/cardreader/ble/BleBackendLegacy$Session;->backend:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;

    invoke-interface {p1, v1, v0}, Lcom/squareup/cardreader/lcr/BleBackendNativeInterface;->ble_received_data_from_characteristic_data(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;[B)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_result_t;

    :cond_1
    :goto_0
    return-void
.end method

.method public synthetic lambda$onMtuReceived$2$BleBackendLegacy(Ljava/lang/Integer;)V
    .locals 2

    .line 129
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendLegacy;->bleBackend:Lcom/squareup/cardreader/ble/BleBackendLegacy$Session;

    if-eqz v0, :cond_1

    iget-object v0, v0, Lcom/squareup/cardreader/ble/BleBackendLegacy$Session;->backend:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;

    if-nez v0, :cond_0

    goto :goto_0

    .line 130
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendLegacy;->bleBackendNative:Lcom/squareup/cardreader/lcr/BleBackendNativeInterface;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleBackendLegacy;->bleBackend:Lcom/squareup/cardreader/ble/BleBackendLegacy$Session;

    iget-object v1, v1, Lcom/squareup/cardreader/ble/BleBackendLegacy$Session;->backend:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-interface {v0, v1, p1}, Lcom/squareup/cardreader/lcr/BleBackendNativeInterface;->ble_received_data_from_characteristic_mtu(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;I)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_result_t;

    :cond_1
    :goto_0
    return-void
.end method

.method public onAckVectorReceived(Ljava/lang/Integer;)V
    .locals 2

    .line 115
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendLegacy;->lcrExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/squareup/cardreader/ble/-$$Lambda$BleBackendLegacy$hPY3tOXWdxdzOTLu9gzNtxOtqWo;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/ble/-$$Lambda$BleBackendLegacy$hPY3tOXWdxdzOTLu9gzNtxOtqWo;-><init>(Lcom/squareup/cardreader/ble/BleBackendLegacy;Ljava/lang/Integer;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onConnectionIntervalReceived(I)V
    .locals 2

    .line 135
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendLegacy;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleBackendLegacy;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-static {v0, v1, p1}, Lcom/squareup/cardreader/CardReaderInfoExposer;->bleConnectionIntervalUpdated(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cardreader/CardReaderInfo;I)V

    return-void
.end method

.method public onDataReceived([B)V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 100
    invoke-static {p1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "Received data: %s"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 102
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendLegacy;->lcrExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/squareup/cardreader/ble/-$$Lambda$BleBackendLegacy$WZ9HQ6Ew1Us_0lM-3ntrCWRXn60;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/ble/-$$Lambda$BleBackendLegacy$WZ9HQ6Ew1Us_0lM-3ntrCWRXn60;-><init>(Lcom/squareup/cardreader/ble/BleBackendLegacy;[B)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onMtuReceived(Ljava/lang/Integer;)V
    .locals 3

    .line 125
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendLegacy;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleBackendLegacy;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/squareup/cardreader/CardReaderInfoExposer;->bleMtuUpdated(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cardreader/CardReaderInfo;I)V

    .line 127
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendLegacy;->lcrExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/squareup/cardreader/ble/-$$Lambda$BleBackendLegacy$yo2x_9xwvzAlpnkl6Y6C1Bq8edU;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/ble/-$$Lambda$BleBackendLegacy$yo2x_9xwvzAlpnkl6Y6C1Bq8edU;-><init>(Lcom/squareup/cardreader/ble/BleBackendLegacy;Ljava/lang/Integer;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onResume()V
    .locals 2

    .line 96
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Don\'t need to call resume on a BLE reader"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public powerOnReader()V
    .locals 2

    .line 92
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Don\'t need to call powerOn on a BLE reader"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public readFromCharacteristicAckVector()V
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "LCR is telling us to read from the ackVector"

    .line 160
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 161
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendLegacy;->tmnTimings:Lcom/squareup/tmn/TmnTimings;

    sget-object v1, Lcom/squareup/tmn/What;->BLE_BACKEND_BYTES_READ_ACK_VECTOR:Lcom/squareup/tmn/What;

    invoke-virtual {v0, v1}, Lcom/squareup/tmn/TmnTimings;->event(Lcom/squareup/tmn/What;)V

    .line 162
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendLegacy;->listener:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/ble/BleBackendLegacy$Listener;

    invoke-interface {v0}, Lcom/squareup/cardreader/ble/BleBackendLegacy$Listener;->readAckVector()V

    return-void
.end method

.method public readFromCharacteristicMTU()V
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "LCR is telling us to read the MTU"

    .line 167
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 168
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendLegacy;->listener:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/ble/BleBackendLegacy$Listener;

    invoke-interface {v0}, Lcom/squareup/cardreader/ble/BleBackendLegacy$Listener;->readMtu()V

    return-void
.end method

.method public declared-synchronized resetBackend()V
    .locals 2

    monitor-enter p0

    .line 86
    :try_start_0
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendLegacy;->bleBackendNative:Lcom/squareup/cardreader/lcr/BleBackendNativeInterface;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleBackendLegacy;->bleBackend:Lcom/squareup/cardreader/ble/BleBackendLegacy$Session;

    iget-object v1, v1, Lcom/squareup/cardreader/ble/BleBackendLegacy$Session;->backend:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/lcr/BleBackendNativeInterface;->cr_comms_backend_ble_shutdown(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;)V

    .line 87
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendLegacy;->bleBackendNative:Lcom/squareup/cardreader/lcr/BleBackendNativeInterface;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleBackendLegacy;->bleBackend:Lcom/squareup/cardreader/ble/BleBackendLegacy$Session;

    iget-object v1, v1, Lcom/squareup/cardreader/ble/BleBackendLegacy$Session;->backend:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/lcr/BleBackendNativeInterface;->cr_comms_backend_ble_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_result_t;

    const/4 v0, 0x0

    .line 88
    iput-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendLegacy;->bleBackend:Lcom/squareup/cardreader/ble/BleBackendLegacy$Session;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setCommsVersion([B)V
    .locals 0

    .line 58
    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleBackendLegacy;->commsVersion:[B

    return-void
.end method

.method public writeToCharacteristic([B)V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 153
    invoke-static {p1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "LCR is telling us to write something: %s"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 154
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendLegacy;->tmnTimings:Lcom/squareup/tmn/TmnTimings;

    sget-object v1, Lcom/squareup/tmn/What;->BLE_BACKEND_BYTES_FROM_LCR:Lcom/squareup/tmn/What;

    invoke-virtual {v0, v1}, Lcom/squareup/tmn/TmnTimings;->event(Lcom/squareup/tmn/What;)V

    .line 155
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendLegacy;->listener:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/ble/BleBackendLegacy$Listener;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/ble/BleBackendLegacy$Listener;->writeData([B)V

    return-void
.end method
