.class Lcom/squareup/cardreader/ble/BleSender$DiscoverServices;
.super Ljava/lang/Object;
.source "BleSender.java"

# interfaces
.implements Lcom/squareup/cardreader/ble/BleSender$GattAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/ble/BleSender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DiscoverServices"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/cardreader/ble/BleSender;


# direct methods
.method private constructor <init>(Lcom/squareup/cardreader/ble/BleSender;)V
    .locals 0

    .line 223
    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleSender$DiscoverServices;->this$0:Lcom/squareup/cardreader/ble/BleSender;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/cardreader/ble/BleSender;Lcom/squareup/cardreader/ble/BleSender$1;)V
    .locals 0

    .line 223
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/ble/BleSender$DiscoverServices;-><init>(Lcom/squareup/cardreader/ble/BleSender;)V

    return-void
.end method


# virtual methods
.method public perform()V
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Starting to discover services"

    .line 225
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 226
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleSender$DiscoverServices;->this$0:Lcom/squareup/cardreader/ble/BleSender;

    invoke-static {v0}, Lcom/squareup/cardreader/ble/BleSender;->access$600(Lcom/squareup/cardreader/ble/BleSender;)Landroid/bluetooth/BluetoothGatt;

    move-result-object v0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGatt;->discoverServices()Z

    move-result v0

    if-nez v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleSender$DiscoverServices;->this$0:Lcom/squareup/cardreader/ble/BleSender;

    invoke-static {v0}, Lcom/squareup/cardreader/ble/BleSender;->access$700(Lcom/squareup/cardreader/ble/BleSender;)Lcom/squareup/cardreader/CardReaderListeners;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object v0

    sget-object v1, Lcom/squareup/cardreader/ble/GattConnectionEventName;->SERVICE_DISCOVERY_FAILED_UNKNOWN:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    .line 228
    invoke-static {v1}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;->of(Lcom/squareup/cardreader/ble/GattConnectionEventName;)Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/ReaderEventLogger;->logGattConnectionEvent(Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;)V

    :cond_0
    return-void
.end method
