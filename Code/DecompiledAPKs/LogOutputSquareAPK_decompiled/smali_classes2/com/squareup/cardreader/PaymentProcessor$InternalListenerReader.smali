.class Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;
.super Ljava/lang/Object;
.source "PaymentProcessor.java"

# interfaces
.implements Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;
.implements Lcom/squareup/cardreader/CardReaderDispatch$NfcPaymentListener;
.implements Lcom/squareup/cardreader/CardReaderDispatch$CompletedPaymentListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/PaymentProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "InternalListenerReader"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/cardreader/PaymentProcessor;


# direct methods
.method constructor <init>(Lcom/squareup/cardreader/PaymentProcessor;)V
    .locals 0

    .line 215
    iput-object p1, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccountSelectionRequired(Ljava/util/List;Ljava/util/List;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/cardreader/lcr/CrPaymentAccountType;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 246
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$000(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/EmvListener;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/cardreader/EmvListener;->onAccountSelectionRequired(Ljava/util/List;Ljava/util/List;Ljava/lang/String;)V

    return-void
.end method

.method public onAudioRequest(Lcom/squareup/cardreader/lcr/CrsTmnAudio;)V
    .locals 2

    .line 297
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$500(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/RealCardReaderListeners;

    move-result-object v0

    new-instance v1, Lcom/squareup/dipper/events/TmnEvent$OnTmnAudioRequestEvent;

    invoke-direct {v1, p1}, Lcom/squareup/dipper/events/TmnEvent$OnTmnAudioRequestEvent;-><init>(Lcom/squareup/cardreader/lcr/CrsTmnAudio;)V

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/RealCardReaderListeners;->publishTmnEvent(Lcom/squareup/dipper/events/TmnEvent;)V

    return-void
.end method

.method public onAudioVisualRequest(Lcom/squareup/cardreader/lcr/CrAudioVisualId;)V
    .locals 1

    .line 301
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$600(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/NfcListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/NfcListener;->onAudioVisualRequest(Lcom/squareup/cardreader/lcr/CrAudioVisualId;)V

    return-void
.end method

.method public onCardError()V
    .locals 1

    .line 218
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$000(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/EmvListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/cardreader/EmvListener;->onCardError()V

    return-void
.end method

.method public onCardRemovedDuringPayment()V
    .locals 1

    .line 222
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$000(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/EmvListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/cardreader/EmvListener;->onCardRemovedDuringPayment()V

    return-void
.end method

.method public onCardholderNameReceived(Lcom/squareup/cardreader/CardInfo;)V
    .locals 1

    .line 378
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$000(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/EmvListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/EmvListener;->onCardholderNameReceived(Lcom/squareup/cardreader/CardInfo;)V

    return-void
.end method

.method public onDisplayRequest(Lcom/squareup/cardreader/lcr/CrsTmnMessage;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .line 291
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$500(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/RealCardReaderListeners;

    move-result-object v0

    new-instance v1, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;

    iget-object v2, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    .line 292
    invoke-static {v2}, Lcom/squareup/cardreader/PaymentProcessor;->access$100(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v2

    invoke-direct {v1, v2, p1, p2, p3}, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;-><init>(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrsTmnMessage;Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/RealCardReaderListeners;->publishTmnEvent(Lcom/squareup/dipper/events/TmnEvent;)V

    return-void
.end method

.method public onHardwarePinRequested(Lcom/squareup/securetouch/SecureTouchPinRequestData;)V
    .locals 2

    .line 255
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$400(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/PinRequestListener;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v1}, Lcom/squareup/cardreader/PaymentProcessor;->access$100(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/squareup/cardreader/PinRequestListener;->onHardwarePinRequested(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/securetouch/SecureTouchPinRequestData;)V

    return-void
.end method

.method public onListApplications([Lcom/squareup/cardreader/EmvApplication;)V
    .locals 1

    .line 241
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$000(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/EmvListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/EmvListener;->onListApplications([Lcom/squareup/cardreader/EmvApplication;)V

    return-void
.end method

.method public onMagSwipeFailed(Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;)V
    .locals 2

    .line 357
    iget-object p1, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {p1}, Lcom/squareup/cardreader/PaymentProcessor;->access$900(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/MagSwipeFailureFilter;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/cardreader/MagSwipeFailureFilter;->shouldFilter()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 358
    iget-object p1, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {p1}, Lcom/squareup/cardreader/PaymentProcessor;->access$500(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/RealCardReaderListeners;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/cardreader/RealCardReaderListeners;->readerEventLogger:Lcom/squareup/cardreader/ReaderEventLogger;

    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$100(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v0

    sget-object v1, Lcom/squareup/cardreader/CardReaderEventName;->MAGSWIPE_FAILURE_IGNORED:Lcom/squareup/cardreader/CardReaderEventName;

    invoke-interface {p1, v0, v1}, Lcom/squareup/cardreader/ReaderEventLogger;->logEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/ReaderEventLogger$CardReaderEvent;)V

    return-void

    .line 364
    :cond_0
    iget-object p1, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    .line 365
    invoke-static {p1}, Lcom/squareup/cardreader/PaymentProcessor;->access$100(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->failedSwipesDefaultToSwipeStraight()Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;->fromSwipeStraight(Ljava/lang/Boolean;)Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;

    move-result-object p1

    .line 366
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$100(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->isInPayment()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$200(Lcom/squareup/cardreader/PaymentProcessor;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 367
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$000(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/EmvListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/EmvListener;->onMagFallbackSwipeFailed(Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;)V

    goto :goto_0

    .line 369
    :cond_1
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$300(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/MagSwipeListener;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v1}, Lcom/squareup/cardreader/PaymentProcessor;->access$100(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/squareup/cardreader/MagSwipeListener;->onMagSwipeFailed(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;)V

    :goto_0
    return-void
.end method

.method public onMagSwipePassthrough(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
    .locals 2

    .line 374
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$300(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/MagSwipeListener;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v1}, Lcom/squareup/cardreader/PaymentProcessor;->access$100(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/squareup/cardreader/MagSwipeListener;->onMagSwipePassthrough(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V

    return-void
.end method

.method public onMagSwipeSuccess(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
    .locals 2

    .line 388
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$100(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->setNotInPayment()V

    .line 390
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$100(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->isInPayment()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$100(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->supportsSwipes()Z

    move-result v0

    if-nez v0, :cond_0

    .line 391
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$700(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/PaymentCompletionListener;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v1}, Lcom/squareup/cardreader/PaymentProcessor;->access$100(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/squareup/cardreader/PaymentCompletionListener;->onPaymentTerminatedDueToSwipe(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V

    goto :goto_0

    .line 393
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$300(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/MagSwipeListener;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v1}, Lcom/squareup/cardreader/PaymentProcessor;->access$100(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/squareup/cardreader/MagSwipeListener;->onMagSwipeSuccess(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V

    :goto_0
    return-void
.end method

.method public onMagswipeFallbackSuccess(Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)V
    .locals 0

    .line 399
    iget-object p3, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {p3}, Lcom/squareup/cardreader/PaymentProcessor;->access$100(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object p3

    invoke-virtual {p3}, Lcom/squareup/cardreader/CardReaderInfo;->setNotInPayment()V

    .line 400
    iget-object p3, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {p3}, Lcom/squareup/cardreader/PaymentProcessor;->access$000(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/EmvListener;

    move-result-object p3

    invoke-interface {p3, p1, p2}, Lcom/squareup/cardreader/EmvListener;->onMagFallbackSwipeSuccess(Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V

    return-void
.end method

.method public onNfcActionRequired()V
    .locals 2

    .line 450
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$600(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/NfcListener;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v1}, Lcom/squareup/cardreader/PaymentProcessor;->access$100(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/NfcListener;->onNfcActionRequired(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void
.end method

.method public onNfcCardBlocked()V
    .locals 2

    .line 441
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$600(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/NfcListener;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v1}, Lcom/squareup/cardreader/PaymentProcessor;->access$100(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/NfcListener;->onNfcCardBlocked(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void
.end method

.method public onNfcCardDeclined()V
    .locals 2

    .line 454
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$600(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/NfcListener;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v1}, Lcom/squareup/cardreader/PaymentProcessor;->access$100(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/NfcListener;->onNfcCardDeclined(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void
.end method

.method public onNfcCollision()V
    .locals 2

    .line 420
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$600(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/NfcListener;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v1}, Lcom/squareup/cardreader/PaymentProcessor;->access$100(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/NfcListener;->onNfcCollisionDetected(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void
.end method

.method public onNfcInterfaceUnavailable()V
    .locals 2

    .line 404
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$600(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/NfcListener;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v1}, Lcom/squareup/cardreader/PaymentProcessor;->access$100(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/NfcListener;->onNfcInterfaceUnavailable(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void
.end method

.method public onNfcLimitExceededInsertCard()V
    .locals 2

    .line 428
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$600(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/NfcListener;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v1}, Lcom/squareup/cardreader/PaymentProcessor;->access$100(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/NfcListener;->onNfcLimitExceededInsertCard(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void
.end method

.method public onNfcLimitExceededTryAnotherCard()V
    .locals 2

    .line 424
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$600(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/NfcListener;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v1}, Lcom/squareup/cardreader/PaymentProcessor;->access$100(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/NfcListener;->onNfcLimitExceededTryAnotherCard(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void
.end method

.method public onNfcPresentCardAgain()V
    .locals 2

    .line 445
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$100(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->setNotInPayment()V

    .line 446
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$600(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/NfcListener;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v1}, Lcom/squareup/cardreader/PaymentProcessor;->access$100(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/NfcListener;->onNfcPresentCardAgain(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void
.end method

.method public onNfcProcessingError()V
    .locals 2

    .line 458
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$100(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->setNotInPayment()V

    .line 459
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$600(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/NfcListener;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v1}, Lcom/squareup/cardreader/PaymentProcessor;->access$100(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/NfcListener;->onNfcProcessingError(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void
.end method

.method public onNfcSeePaymentDeviceForInstructions()V
    .locals 2

    .line 412
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$600(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/NfcListener;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v1}, Lcom/squareup/cardreader/PaymentProcessor;->access$100(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/NfcListener;->onNfcActionRequired(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void
.end method

.method public onNfcTryAnotherCard()V
    .locals 2

    .line 408
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$600(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/NfcListener;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v1}, Lcom/squareup/cardreader/PaymentProcessor;->access$100(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/NfcListener;->onNfcTryAnotherCard(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void
.end method

.method public onNfcUnlockPaymentDevice()V
    .locals 2

    .line 416
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$600(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/NfcListener;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v1}, Lcom/squareup/cardreader/PaymentProcessor;->access$100(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/NfcListener;->onNfcUnlockDevice(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void
.end method

.method public onPaymentContinuingDueToSwipe()V
    .locals 1

    .line 382
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$100(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->isInPayment()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 383
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$100(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->setNotInPayment()V

    :cond_0
    return-void
.end method

.method public onPaymentNfcTimedOut(Lcom/squareup/cardreader/PaymentTimings;)V
    .locals 2

    .line 436
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$100(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->setNotInPayment()V

    .line 437
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$600(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/NfcListener;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v1}, Lcom/squareup/cardreader/PaymentProcessor;->access$100(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/squareup/cardreader/NfcListener;->onNfcTimedOut(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/PaymentTimings;)V

    return-void
.end method

.method public onRequestTapCard()V
    .locals 2

    .line 432
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$600(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/NfcListener;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v1}, Lcom/squareup/cardreader/PaymentProcessor;->access$100(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/NfcListener;->onRequestTapCard(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void
.end method

.method public onSecureTouchDisabled()V
    .locals 1

    .line 272
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$500(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/RealCardReaderListeners;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardreader/RealCardReaderListeners;->publishSecureTouchDisabled()V

    return-void
.end method

.method public onSecureTouchEnabled()V
    .locals 1

    .line 268
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$500(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/RealCardReaderListeners;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardreader/RealCardReaderListeners;->publishSecureTouchEnabled()V

    return-void
.end method

.method public onSecureTouchPinPadEvent(Lcom/squareup/securetouch/SecureTouchFeatureEvent;)V
    .locals 1

    .line 264
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$500(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/RealCardReaderListeners;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/RealCardReaderListeners;->publishSecureTouchEvent(Lcom/squareup/securetouch/SecureTouchFeatureEvent;)V

    return-void
.end method

.method public onSigRequested()V
    .locals 1

    .line 250
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$000(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/EmvListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/cardreader/EmvListener;->onSigRequested()V

    return-void
.end method

.method public onSmartPaymentApproved(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/lcr/CrPaymentStandardMessage;ZLcom/squareup/cardreader/PaymentTimings;)V
    .locals 7

    .line 322
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->setNotInPayment()V

    .line 323
    new-instance v0, Lcom/squareup/dipper/events/BleDevice;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getReaderName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getAddress()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/dipper/events/BleDevice;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 324
    new-instance v1, Lcom/squareup/dipper/events/DipperEvent$PaymentApproved;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v2

    iget v2, v2, Lcom/squareup/cardreader/CardReaderId;->id:I

    invoke-direct {v1, v0, v2}, Lcom/squareup/dipper/events/DipperEvent$PaymentApproved;-><init>(Lcom/squareup/dipper/events/BleDevice;I)V

    .line 325
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$500(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/RealCardReaderListeners;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/RealCardReaderListeners;->publishDipperEvent(Lcom/squareup/dipper/events/DipperEvent;)V

    .line 326
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$700(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/PaymentCompletionListener;

    move-result-object v1

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move-object v6, p5

    invoke-interface/range {v1 .. v6}, Lcom/squareup/cardreader/PaymentCompletionListener;->onPaymentApproved(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/lcr/CrPaymentStandardMessage;ZLcom/squareup/cardreader/PaymentTimings;)V

    .line 328
    iget-object p1, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {p1}, Lcom/squareup/cardreader/PaymentProcessor;->access$800(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/FirmwareUpdater;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/cardreader/FirmwareUpdater;->resumeUpdates()V

    return-void
.end method

.method public onSmartPaymentDeclined(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/lcr/CrPaymentStandardMessage;Lcom/squareup/cardreader/PaymentTimings;)V
    .locals 1

    .line 342
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->setNotInPayment()V

    .line 343
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$700(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/PaymentCompletionListener;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/squareup/cardreader/PaymentCompletionListener;->onPaymentDeclined(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/lcr/CrPaymentStandardMessage;Lcom/squareup/cardreader/PaymentTimings;)V

    .line 345
    iget-object p1, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {p1}, Lcom/squareup/cardreader/PaymentProcessor;->access$800(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/FirmwareUpdater;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/cardreader/FirmwareUpdater;->resumeUpdates()V

    return-void
.end method

.method public onSmartPaymentReversed(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/lcr/CrPaymentStandardMessage;Lcom/squareup/cardreader/PaymentTimings;)V
    .locals 1

    .line 334
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->setNotInPayment()V

    .line 335
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$700(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/PaymentCompletionListener;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/squareup/cardreader/PaymentCompletionListener;->onPaymentReversed(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/lcr/CrPaymentStandardMessage;Lcom/squareup/cardreader/PaymentTimings;)V

    return-void
.end method

.method public onSmartPaymentTerminated(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;Lcom/squareup/cardreader/PaymentTimings;)V
    .locals 1

    .line 350
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->setNotInPayment()V

    .line 351
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$700(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/PaymentCompletionListener;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/cardreader/PaymentCompletionListener;->onPaymentTerminated(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;Lcom/squareup/cardreader/PaymentTimings;)V

    .line 353
    iget-object p1, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {p1}, Lcom/squareup/cardreader/PaymentProcessor;->access$800(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/FirmwareUpdater;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/cardreader/FirmwareUpdater;->resumeUpdates()V

    return-void
.end method

.method public onSoftwarePinRequested(Lcom/squareup/cardreader/PinRequestData;)V
    .locals 2

    .line 259
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$400(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/PinRequestListener;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v1}, Lcom/squareup/cardreader/PaymentProcessor;->access$100(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/squareup/cardreader/PinRequestListener;->onSoftwarePinRequested(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/PinRequestData;)V

    return-void
.end method

.method public onSwipeForFallback(Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;)V
    .locals 1

    .line 237
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$000(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/EmvListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/EmvListener;->onSwipeForFallback(Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;)V

    return-void
.end method

.method public onTmnDataToTmn(Ljava/lang/String;[B)V
    .locals 2

    .line 276
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$500(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/RealCardReaderListeners;

    move-result-object v0

    new-instance v1, Lcom/squareup/dipper/events/TmnEvent$OnTmnDataToTmnEvent;

    invoke-direct {v1, p1, p2}, Lcom/squareup/dipper/events/TmnEvent$OnTmnDataToTmnEvent;-><init>(Ljava/lang/String;[B)V

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/RealCardReaderListeners;->publishTmnEvent(Lcom/squareup/dipper/events/TmnEvent;)V

    return-void
.end method

.method public onTmnTransactionComplete(Lcom/squareup/cardreader/lcr/TmnTransactionResult;Lcom/squareup/cardreader/PaymentTimings;)V
    .locals 2

    .line 281
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$500(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/RealCardReaderListeners;

    move-result-object v0

    new-instance v1, Lcom/squareup/dipper/events/TmnEvent$OnTmnCompletionEvent;

    invoke-direct {v1, p1, p2}, Lcom/squareup/dipper/events/TmnEvent$OnTmnCompletionEvent;-><init>(Lcom/squareup/cardreader/lcr/TmnTransactionResult;Lcom/squareup/cardreader/PaymentTimings;)V

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/RealCardReaderListeners;->publishTmnEvent(Lcom/squareup/dipper/events/TmnEvent;)V

    return-void
.end method

.method public onTmnWriteNotify(II[B)V
    .locals 2

    .line 286
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$500(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/RealCardReaderListeners;

    move-result-object v0

    new-instance v1, Lcom/squareup/dipper/events/TmnEvent$OnTmnWriteNotify;

    invoke-direct {v1, p1, p2, p3}, Lcom/squareup/dipper/events/TmnEvent$OnTmnWriteNotify;-><init>(II[B)V

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/RealCardReaderListeners;->publishTmnEvent(Lcom/squareup/dipper/events/TmnEvent;)V

    return-void
.end method

.method public onUseChipCard()V
    .locals 2

    .line 229
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$100(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->isInPayment()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$200(Lcom/squareup/cardreader/PaymentProcessor;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 230
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$000(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/EmvListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/cardreader/EmvListener;->onUseChipCardDuringFallback()V

    goto :goto_0

    .line 232
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$300(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/MagSwipeListener;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v1}, Lcom/squareup/cardreader/PaymentProcessor;->access$100(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/MagSwipeListener;->onUseChipCard(Lcom/squareup/cardreader/CardReaderInfo;)V

    :goto_0
    return-void
.end method

.method public sendAuthorization([BZLcom/squareup/cardreader/CardInfo;)V
    .locals 1

    .line 306
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$000(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/EmvListener;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/cardreader/EmvListener;->sendAuthorization([BZLcom/squareup/cardreader/CardInfo;)V

    return-void
.end method

.method public sendContactlessAuthorization([BLcom/squareup/cardreader/CardInfo;)V
    .locals 2

    .line 311
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$600(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/NfcListener;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v1}, Lcom/squareup/cardreader/PaymentProcessor;->access$100(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v1

    invoke-interface {v0, v1, p1, p2}, Lcom/squareup/cardreader/NfcListener;->onNfcAuthorizationRequestReceived(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/CardInfo;)V

    return-void
.end method

.method public sendTmnAuthorization([B)V
    .locals 3

    .line 315
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentProcessor;->access$500(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/RealCardReaderListeners;

    move-result-object v0

    new-instance v1, Lcom/squareup/dipper/events/TmnEvent$OnTmnAuthRequest;

    iget-object v2, p0, Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;->this$0:Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v2}, Lcom/squareup/cardreader/PaymentProcessor;->access$100(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Lcom/squareup/dipper/events/TmnEvent$OnTmnAuthRequest;-><init>(Lcom/squareup/cardreader/CardReaderInfo;[B)V

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/RealCardReaderListeners;->publishTmnEvent(Lcom/squareup/dipper/events/TmnEvent;)V

    return-void
.end method
