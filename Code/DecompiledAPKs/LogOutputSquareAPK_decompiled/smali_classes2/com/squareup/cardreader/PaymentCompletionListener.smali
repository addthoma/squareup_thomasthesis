.class public interface abstract Lcom/squareup/cardreader/PaymentCompletionListener;
.super Ljava/lang/Object;
.source "PaymentCompletionListener.java"


# virtual methods
.method public abstract onPaymentApproved(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/lcr/CrPaymentStandardMessage;ZLcom/squareup/cardreader/PaymentTimings;)V
.end method

.method public abstract onPaymentDeclined(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/lcr/CrPaymentStandardMessage;Lcom/squareup/cardreader/PaymentTimings;)V
.end method

.method public abstract onPaymentReversed(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/lcr/CrPaymentStandardMessage;Lcom/squareup/cardreader/PaymentTimings;)V
.end method

.method public abstract onPaymentTerminated(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;Lcom/squareup/cardreader/PaymentTimings;)V
.end method

.method public abstract onPaymentTerminatedDueToSwipe(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
.end method
