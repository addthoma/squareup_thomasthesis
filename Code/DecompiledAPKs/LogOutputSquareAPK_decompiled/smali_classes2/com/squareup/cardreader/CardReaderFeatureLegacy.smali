.class public Lcom/squareup/cardreader/CardReaderFeatureLegacy;
.super Ljava/lang/Object;
.source "CardReaderFeatureLegacy.java"

# interfaces
.implements Lcom/squareup/cardreader/CardReaderFeature;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/CardReaderFeatureLegacy$CardReaderFeatureListener;
    }
.end annotation


# instance fields
.field private final backend:Lcom/squareup/cardreader/LcrBackend;

.field private final cardReaderConstants:Lcom/squareup/cardreader/CardReaderConstants;

.field private final cardReaderLogBridge:Lcom/squareup/cardreader/CardReaderLogBridge;

.field private final cardreaderNative:Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;

.field private final crashnado:Lcom/squareup/crashnado/Crashnado;

.field private featureListener:Lcom/squareup/cardreader/CardReaderFeatureLegacy$CardReaderFeatureListener;

.field private final lcrExecutor:Ljava/util/concurrent/ExecutorService;

.field private final loggingEnabled:Ljava/lang/Boolean;

.field private session:Lcom/squareup/cardreader/CardReaderPointer;

.field private final timerApi:Lcom/squareup/cardreader/TimerApiLegacy;


# direct methods
.method constructor <init>(Ljava/lang/Boolean;Lcom/squareup/crashnado/Crashnado;Lcom/squareup/cardreader/LcrBackend;Lcom/squareup/cardreader/TimerApiLegacy;Ljava/util/concurrent/ExecutorService;Lcom/squareup/cardreader/CardReaderLogBridge;Lcom/squareup/cardreader/CardReaderConstants;Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;)V
    .locals 0
    .param p1    # Ljava/lang/Boolean;
        .annotation runtime Lcom/squareup/cardreader/NativeLoggingEnabled;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy;->loggingEnabled:Ljava/lang/Boolean;

    .line 39
    iput-object p2, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy;->crashnado:Lcom/squareup/crashnado/Crashnado;

    .line 40
    iput-object p3, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy;->backend:Lcom/squareup/cardreader/LcrBackend;

    .line 41
    iput-object p4, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy;->timerApi:Lcom/squareup/cardreader/TimerApiLegacy;

    .line 42
    iput-object p5, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy;->lcrExecutor:Ljava/util/concurrent/ExecutorService;

    .line 43
    iput-object p6, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy;->cardReaderLogBridge:Lcom/squareup/cardreader/CardReaderLogBridge;

    .line 44
    iput-object p7, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy;->cardReaderConstants:Lcom/squareup/cardreader/CardReaderConstants;

    .line 45
    iput-object p8, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy;->cardreaderNative:Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;

    const/4 p1, 0x0

    .line 46
    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy;->session:Lcom/squareup/cardreader/CardReaderPointer;

    .line 47
    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy;->featureListener:Lcom/squareup/cardreader/CardReaderFeatureLegacy$CardReaderFeatureListener;

    return-void
.end method


# virtual methods
.method public forgetCardReader()V
    .locals 2

    .line 103
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy;->backend:Lcom/squareup/cardreader/LcrBackend;

    instance-of v1, v0, Lcom/squareup/cardreader/ble/BleBackendLegacy;

    if-eqz v1, :cond_0

    .line 104
    check-cast v0, Lcom/squareup/cardreader/ble/BleBackendLegacy;

    invoke-virtual {v0}, Lcom/squareup/cardreader/ble/BleBackendLegacy;->forgetReader()V

    :cond_0
    return-void
.end method

.method public getCardreader()Lcom/squareup/cardreader/CardReaderPointer;
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy;->session:Lcom/squareup/cardreader/CardReaderPointer;

    return-object v0
.end method

.method public getLcrCommsProtocolVersion()Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;
    .locals 2

    .line 91
    new-instance v0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy;->cardReaderConstants:Lcom/squareup/cardreader/CardReaderConstants;

    .line 92
    invoke-interface {v1}, Lcom/squareup/cardreader/CardReaderConstants;->transportProtocolVersion()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;->transport(Ljava/lang/Integer;)Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy;->cardReaderConstants:Lcom/squareup/cardreader/CardReaderConstants;

    .line 93
    invoke-interface {v1}, Lcom/squareup/cardreader/CardReaderConstants;->appProtocolVersion()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;->app(Ljava/lang/Integer;)Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy;->cardReaderConstants:Lcom/squareup/cardreader/CardReaderConstants;

    .line 94
    invoke-interface {v1}, Lcom/squareup/cardreader/CardReaderConstants;->epProtocolVersion()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;->ep(Ljava/lang/Integer;)Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;

    move-result-object v0

    .line 95
    invoke-virtual {v0}, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;->build()Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;

    move-result-object v0

    return-object v0
.end method

.method public initializeCardreader(Lcom/squareup/cardreader/CardReaderFeatureLegacy$CardReaderFeatureListener;)V
    .locals 2

    .line 56
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy;->session:Lcom/squareup/cardreader/CardReaderPointer;

    if-nez v0, :cond_1

    if-eqz p1, :cond_0

    .line 59
    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy;->featureListener:Lcom/squareup/cardreader/CardReaderFeatureLegacy$CardReaderFeatureListener;

    .line 61
    iget-object p1, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy;->cardReaderLogBridge:Lcom/squareup/cardreader/CardReaderLogBridge;

    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy;->loggingEnabled:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-interface {p1, v0}, Lcom/squareup/cardreader/CardReaderLogBridge;->setLoggingEnabled(Z)V

    .line 62
    iget-object p1, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy;->crashnado:Lcom/squareup/crashnado/Crashnado;

    invoke-interface {p1}, Lcom/squareup/crashnado/Crashnado;->prepareStack()V

    .line 64
    iget-object p1, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy;->timerApi:Lcom/squareup/cardreader/TimerApiLegacy;

    invoke-virtual {p1}, Lcom/squareup/cardreader/TimerApiLegacy;->initialize()Lcom/squareup/cardreader/TimerApiLegacy$TimerConfig;

    move-result-object p1

    .line 65
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy;->backend:Lcom/squareup/cardreader/LcrBackend;

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy;->featureListener:Lcom/squareup/cardreader/CardReaderFeatureLegacy$CardReaderFeatureListener;

    invoke-interface {v0, p1, v1, p0}, Lcom/squareup/cardreader/LcrBackend;->initialize(Lcom/squareup/cardreader/TimerApiLegacy$TimerConfig;Lcom/squareup/cardreader/CardReaderFeatureLegacy$CardReaderFeatureListener;Lcom/squareup/cardreader/CardReaderFeatureLegacy;)Lcom/squareup/cardreader/CardReaderPointer;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy;->session:Lcom/squareup/cardreader/CardReaderPointer;

    .line 66
    iget-object p1, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy;->backend:Lcom/squareup/cardreader/LcrBackend;

    invoke-interface {p1}, Lcom/squareup/cardreader/LcrBackend;->cardReaderInitialized()V

    const/4 p1, 0x1

    new-array p1, p1, [Ljava/lang/Object;

    const/4 v0, 0x0

    .line 68
    invoke-virtual {p0}, Lcom/squareup/cardreader/CardReaderFeatureLegacy;->getLcrCommsProtocolVersion()Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;

    move-result-object v1

    aput-object v1, p1, v0

    const-string v0, "Application protocol version: %s"

    invoke-static {v0, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 57
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "listener must not be null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 56
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "CardReaderFeature already initialized!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public onCommsVersionAcquired(IIII)V
    .locals 3

    .line 127
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CrCommsVersionResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrCommsVersionResult;

    move-result-object p1

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 128
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 129
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x3

    aput-object v1, v0, v2

    const-string v1, "LCR CommsVersion: %s tp=%d app=%d ep=%d"

    .line 128
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 130
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy;->featureListener:Lcom/squareup/cardreader/CardReaderFeatureLegacy$CardReaderFeatureListener;

    .line 131
    invoke-virtual {p0}, Lcom/squareup/cardreader/CardReaderFeatureLegacy;->getLcrCommsProtocolVersion()Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;

    move-result-object v1

    new-instance v2, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;-><init>()V

    .line 133
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {v2, p2}, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;->transport(Ljava/lang/Integer;)Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;

    move-result-object p2

    .line 134
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;->app(Ljava/lang/Integer;)Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;

    move-result-object p2

    .line 135
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;->ep(Ljava/lang/Integer;)Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;

    move-result-object p2

    .line 136
    invoke-virtual {p2}, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;->build()Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;

    move-result-object p2

    .line 130
    invoke-interface {v0, p1, v1, p2}, Lcom/squareup/cardreader/CardReaderFeatureLegacy$CardReaderFeatureListener;->onCommsVersionAcquired(Lcom/squareup/cardreader/lcr/CrCommsVersionResult;Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;)V

    return-void
.end method

.method public onReaderError()V
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Got ReaderError - Don\'t know what to do w/ it!"

    .line 121
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onReaderReady(I)V
    .locals 2

    .line 114
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CrCardreaderType;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrCardreaderType;

    move-result-object p1

    .line 115
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Reader is Ready "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 116
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy;->featureListener:Lcom/squareup/cardreader/CardReaderFeatureLegacy$CardReaderFeatureListener;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderFeatureLegacy$CardReaderFeatureListener;->onReaderReady(Lcom/squareup/cardreader/lcr/CrCardreaderType;)V

    return-void
.end method

.method public onRpcCallbackRecvd()V
    .locals 3

    .line 148
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy;->lcrExecutor:Ljava/util/concurrent/ExecutorService;

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy;->cardreaderNative:Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/cardreader/-$$Lambda$scxDfyn7WRjKrq9jN46EHfsoOqw;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/-$$Lambda$scxDfyn7WRjKrq9jN46EHfsoOqw;-><init>(Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;)V

    invoke-interface {v0, v2}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public powerOnReader()V
    .locals 1

    .line 99
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy;->backend:Lcom/squareup/cardreader/LcrBackend;

    invoke-interface {v0}, Lcom/squareup/cardreader/LcrBackend;->powerOnReader()V

    return-void
.end method

.method public reportError(ILjava/lang/String;)V
    .locals 3

    .line 142
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 p1, 0x1

    aput-object p2, v1, p1

    const-string p1, "Error from endpoint %d: %s "

    invoke-static {v0, p1, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 143
    new-instance p2, Ljava/lang/RuntimeException;

    invoke-direct {p2, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-static {p2, p1}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    return-void
.end method

.method public resetCardreader()V
    .locals 4

    .line 73
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy;->timerApi:Lcom/squareup/cardreader/TimerApiLegacy;

    monitor-enter v0

    .line 74
    :try_start_0
    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy;->session:Lcom/squareup/cardreader/CardReaderPointer;

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    .line 75
    iput-object v1, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy;->featureListener:Lcom/squareup/cardreader/CardReaderFeatureLegacy$CardReaderFeatureListener;

    .line 77
    iget-object v2, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy;->cardreaderNative:Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;

    iget-object v3, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy;->session:Lcom/squareup/cardreader/CardReaderPointer;

    invoke-virtual {v3}, Lcom/squareup/cardreader/CardReaderPointer;->getId()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;->cr_cardreader_notify_reader_unplugged(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)Lcom/squareup/cardreader/lcr/CrCardreaderResult;

    .line 78
    iget-object v2, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy;->cardreaderNative:Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;

    iget-object v3, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy;->session:Lcom/squareup/cardreader/CardReaderPointer;

    invoke-virtual {v3}, Lcom/squareup/cardreader/CardReaderPointer;->getId()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;->cr_cardreader_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)Lcom/squareup/cardreader/lcr/CrCardreaderResult;

    .line 79
    iget-object v2, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy;->cardreaderNative:Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;

    iget-object v3, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy;->session:Lcom/squareup/cardreader/CardReaderPointer;

    invoke-virtual {v3}, Lcom/squareup/cardreader/CardReaderPointer;->getId()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;->cr_cardreader_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)Lcom/squareup/cardreader/lcr/CrCardreaderResult;

    .line 80
    iput-object v1, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy;->session:Lcom/squareup/cardreader/CardReaderPointer;

    .line 83
    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy;->backend:Lcom/squareup/cardreader/LcrBackend;

    invoke-interface {v1}, Lcom/squareup/cardreader/LcrBackend;->resetBackend()V

    .line 84
    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderFeatureLegacy;->timerApi:Lcom/squareup/cardreader/TimerApiLegacy;

    invoke-virtual {v1}, Lcom/squareup/cardreader/TimerApiLegacy;->cancelTimers()V

    .line 86
    :cond_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
