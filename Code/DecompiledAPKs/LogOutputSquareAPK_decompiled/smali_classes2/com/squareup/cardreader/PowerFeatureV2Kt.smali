.class public final Lcom/squareup/cardreader/PowerFeatureV2Kt;
.super Ljava/lang/Object;
.source "PowerFeatureV2.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a\u000c\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "toBatteryMode",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$BatteryMode;",
        "Lcom/squareup/cardreader/lcr/CrsBatteryMode;",
        "cardreader-features_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$toBatteryMode(Lcom/squareup/cardreader/lcr/CrsBatteryMode;)Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$BatteryMode;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/cardreader/PowerFeatureV2Kt;->toBatteryMode(Lcom/squareup/cardreader/lcr/CrsBatteryMode;)Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$BatteryMode;

    move-result-object p0

    return-object p0
.end method

.method private static final toBatteryMode(Lcom/squareup/cardreader/lcr/CrsBatteryMode;)Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$BatteryMode;
    .locals 1

    .line 86
    sget-object v0, Lcom/squareup/cardreader/PowerFeatureV2Kt$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p0}, Lcom/squareup/cardreader/lcr/CrsBatteryMode;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_3

    const/4 v0, 0x2

    if-eq p0, v0, :cond_2

    const/4 v0, 0x3

    if-eq p0, v0, :cond_1

    const/4 v0, 0x4

    if-ne p0, v0, :cond_0

    .line 90
    sget-object p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$BatteryMode;->BATTERY_MODE_LOW_CRITICAL:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$BatteryMode;

    goto :goto_0

    :cond_0
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    .line 89
    :cond_1
    sget-object p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$BatteryMode;->BATTERY_MODE_CHARGED:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$BatteryMode;

    goto :goto_0

    .line 88
    :cond_2
    sget-object p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$BatteryMode;->BATTERY_MODE_CHARGING:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$BatteryMode;

    goto :goto_0

    .line 87
    :cond_3
    sget-object p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$BatteryMode;->BATTERY_MODE_DISCHARGING:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$BatteryMode;

    :goto_0
    return-object p0
.end method
