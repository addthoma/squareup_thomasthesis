.class public Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;
.super Ljava/lang/Object;
.source "ReaderEventLogger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/ReaderEventLogger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PaymentFeatureEvent"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;,
        Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;
    }
.end annotation


# instance fields
.field public final amountAuthorized:Ljava/lang/Long;

.field public final approvedOffline:Ljava/lang/Boolean;

.field public final brandId:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

.field public final cardAction:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

.field public final cardInfo:Lcom/squareup/cardreader/CardInfo;

.field public final cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

.field public final event:Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;

.field public final paymentResult:Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

.field public final present:Ljava/lang/Boolean;

.field public final requestType:Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

.field public final standardMessage:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

.field public final startPaymentResult:Lcom/squareup/cardreader/lcr/CrPaymentResult;

.field public final tmnTransactionResult:Lcom/squareup/cardreader/lcr/TmnTransactionResult;

.field public final transactionId:Ljava/lang/String;

.field public final transactionType:Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;

.field public final willContinuePayment:Ljava/lang/Boolean;


# direct methods
.method private constructor <init>(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;)V
    .locals 1

    .line 254
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 255
    invoke-static {p1}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->access$100(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;)Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;->event:Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;

    .line 256
    invoke-static {p1}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->access$200(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;)Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    .line 257
    invoke-static {p1}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->access$300(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;)Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;->paymentResult:Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

    .line 258
    invoke-static {p1}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->access$400(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;)Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;->standardMessage:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    .line 259
    invoke-static {p1}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->access$500(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;)Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;->cardAction:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    .line 260
    invoke-static {p1}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->access$600(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;->approvedOffline:Ljava/lang/Boolean;

    .line 261
    invoke-static {p1}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->access$700(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;)Lcom/squareup/cardreader/CardInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;->cardInfo:Lcom/squareup/cardreader/CardInfo;

    .line 262
    invoke-static {p1}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->access$800(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;->willContinuePayment:Ljava/lang/Boolean;

    .line 263
    invoke-static {p1}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->access$900(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;->present:Ljava/lang/Boolean;

    .line 264
    invoke-static {p1}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->access$1000(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;)Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;->transactionType:Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;

    .line 265
    invoke-static {p1}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->access$1100(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;->amountAuthorized:Ljava/lang/Long;

    .line 266
    invoke-static {p1}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->access$1200(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;->startPaymentResult:Lcom/squareup/cardreader/lcr/CrPaymentResult;

    .line 267
    invoke-static {p1}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->access$1300(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;)Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;->brandId:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    .line 268
    invoke-static {p1}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->access$1400(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;)Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;->requestType:Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    .line 269
    invoke-static {p1}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->access$1500(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;)Lcom/squareup/cardreader/lcr/TmnTransactionResult;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;->tmnTransactionResult:Lcom/squareup/cardreader/lcr/TmnTransactionResult;

    .line 270
    invoke-static {p1}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->access$1600(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;->transactionId:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;Lcom/squareup/cardreader/ReaderEventLogger$1;)V
    .locals 0

    .line 122
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;-><init>(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;)V

    return-void
.end method
