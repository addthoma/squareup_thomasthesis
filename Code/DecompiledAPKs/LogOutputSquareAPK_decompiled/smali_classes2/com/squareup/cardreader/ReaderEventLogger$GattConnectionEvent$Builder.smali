.class public Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;
.super Ljava/lang/Object;
.source "ReaderEventLogger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field public bleReceiverAlreadyDestroyed:Z

.field public callbackName:Ljava/lang/String;

.field public characteristicUuid:Ljava/util/UUID;

.field public descriptorUuid:Ljava/util/UUID;

.field public mtu:I

.field public final name:Lcom/squareup/cardreader/ble/GattConnectionEventName;

.field public newState:I

.field public serviceUuid:Ljava/util/UUID;

.field public status:I


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/ble/GattConnectionEventName;)V
    .locals 1

    .line 435
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 433
    iput-boolean v0, p0, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;->bleReceiverAlreadyDestroyed:Z

    .line 436
    iput-object p1, p0, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;->name:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    const/4 p1, -0x1

    .line 437
    iput p1, p0, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;->newState:I

    .line 438
    iput p1, p0, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;->mtu:I

    .line 439
    iput p1, p0, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;->status:I

    return-void
.end method


# virtual methods
.method public bleReceiverAlreadyDestroyed(Z)Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;
    .locals 0

    .line 497
    iput-boolean p1, p0, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;->bleReceiverAlreadyDestroyed:Z

    return-object p0
.end method

.method public build()Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;
    .locals 1

    .line 443
    new-instance v0, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;

    invoke-direct {v0, p0}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;-><init>(Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;)V

    return-object v0
.end method

.method public callbackName(Ljava/lang/String;)Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;
    .locals 0

    .line 492
    iput-object p1, p0, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;->callbackName:Ljava/lang/String;

    return-object p0
.end method

.method public characteristic(Landroid/bluetooth/BluetoothGattCharacteristic;)Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;
    .locals 0

    .line 452
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;->characteristicUuid:Ljava/util/UUID;

    return-object p0
.end method

.method public characteristicUuid(Ljava/util/UUID;)Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;
    .locals 0

    .line 457
    iput-object p1, p0, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;->characteristicUuid:Ljava/util/UUID;

    return-object p0
.end method

.method public descriptor(Landroid/bluetooth/BluetoothGattDescriptor;)Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;
    .locals 0

    .line 462
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGattDescriptor;->getUuid()Ljava/util/UUID;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;->descriptorUuid:Ljava/util/UUID;

    return-object p0
.end method

.method public mtu(I)Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;
    .locals 0

    .line 482
    iput p1, p0, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;->mtu:I

    return-object p0
.end method

.method public newState(I)Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;
    .locals 0

    .line 477
    iput p1, p0, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;->newState:I

    return-object p0
.end method

.method public serviceUuid(Ljava/util/UUID;)Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;
    .locals 0

    .line 447
    iput-object p1, p0, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;->serviceUuid:Ljava/util/UUID;

    return-object p0
.end method

.method public status(I)Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;
    .locals 0

    .line 487
    iput p1, p0, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;->status:I

    return-object p0
.end method
