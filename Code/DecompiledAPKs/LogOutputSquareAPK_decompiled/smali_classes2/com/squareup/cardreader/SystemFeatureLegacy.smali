.class public Lcom/squareup/cardreader/SystemFeatureLegacy;
.super Ljava/lang/Object;
.source "SystemFeatureLegacy.java"

# interfaces
.implements Lcom/squareup/cardreader/SystemFeature;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/SystemFeatureLegacy$SystemFeatureListener;
    }
.end annotation


# static fields
.field private static final FEATURE_ACCOUNT_SELECTION:Ljava/lang/String; = "account_type_selection"

.field private static final FEATURE_COMMON_DEBIT_SUPPORT:Ljava/lang/String; = "common_debit_support"

.field private static final FEATURE_DISABLED:S = 0x0s

.field private static final FEATURE_ENABLED:S = 0x1s

.field private static final FEATURE_FELICA_NOTIFICATION:Ljava/lang/String; = "felica_notification"

.field private static final FEATURE_PINBLOCK_V2:Ljava/lang/String; = "pinblock_format_v2"

.field private static final FEATURE_QUICK_CHIP:Ljava/lang/String; = "quickchip_fw209030"

.field private static final FEATURE_SONIC_BRANDING:Ljava/lang/String; = "sonic_branding"

.field private static final FEATURE_SPOC_PRNG_SEED:Ljava/lang/String; = "spoc_prng_seed"


# instance fields
.field private apiListener:Lcom/squareup/cardreader/SystemFeatureLegacy$SystemFeatureListener;

.field private final sessionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;"
        }
    .end annotation
.end field

.field private systemFeature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;

.field private final systemFeatureNative:Lcom/squareup/cardreader/lcr/SystemFeatureNativeInterface;


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Lcom/squareup/cardreader/lcr/SystemFeatureNativeInterface;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;",
            "Lcom/squareup/cardreader/lcr/SystemFeatureNativeInterface;",
            ")V"
        }
    .end annotation

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/squareup/cardreader/SystemFeatureLegacy;->sessionProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p2, p0, Lcom/squareup/cardreader/SystemFeatureLegacy;->systemFeatureNative:Lcom/squareup/cardreader/lcr/SystemFeatureNativeInterface;

    return-void
.end method

.method public static getCapabilityShortName(Lcom/squareup/cardreader/lcr/CrsCapability;)Ljava/lang/String;
    .locals 1

    .line 160
    sget-object v0, Lcom/squareup/cardreader/SystemFeatureLegacy$1;->$SwitchMap$com$squareup$cardreader$lcr$CrsCapability:[I

    invoke-virtual {p0}, Lcom/squareup/cardreader/lcr/CrsCapability;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_3

    const/4 v0, 0x2

    if-eq p0, v0, :cond_2

    const/4 v0, 0x3

    if-eq p0, v0, :cond_1

    const/4 v0, 0x4

    if-eq p0, v0, :cond_0

    const-string p0, "Unknown"

    return-object p0

    :cond_0
    const-string p0, "Signed Manifests"

    return-object p0

    :cond_1
    const-string p0, "SS V4"

    return-object p0

    :cond_2
    const-string p0, "Payment Profiling"

    return-object p0

    :cond_3
    const-string p0, "K400 Powerup Hint"

    return-object p0
.end method

.method private setFeatureFlag(Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 3

    const/4 v0, 0x0

    if-eqz p2, :cond_1

    .line 175
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    if-nez p2, :cond_0

    goto :goto_0

    :cond_0
    const/4 p2, 0x1

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p2, 0x0

    .line 176
    :goto_1
    iget-object v1, p0, Lcom/squareup/cardreader/SystemFeatureLegacy;->systemFeatureNative:Lcom/squareup/cardreader/lcr/SystemFeatureNativeInterface;

    iget-object v2, p0, Lcom/squareup/cardreader/SystemFeatureLegacy;->systemFeature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;

    .line 177
    invoke-interface {v1, v2, p1, p2}, Lcom/squareup/cardreader/lcr/SystemFeatureNativeInterface;->system_set_reader_feature_flag(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;Ljava/lang/String;S)Lcom/squareup/cardreader/lcr/CrSystemResult;

    move-result-object p2

    .line 178
    sget-object v1, Lcom/squareup/cardreader/lcr/CrSystemResult;->CR_SYSTEM_RESULT_SUCCESS:Lcom/squareup/cardreader/lcr/CrSystemResult;

    if-eq p2, v1, :cond_2

    .line 179
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "cr_system_set_reader_feature_flag failed! feature = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " result = "

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-array p2, v0, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ltimber/log/Timber;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_2
    return-void
.end method


# virtual methods
.method public getCapabilityForByteValue(B)Lcom/squareup/cardreader/lcr/CrsCapability;
    .locals 5

    .line 135
    invoke-static {}, Lcom/squareup/cardreader/lcr/CrsCapability;->values()[Lcom/squareup/cardreader/lcr/CrsCapability;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 136
    invoke-virtual {v3}, Lcom/squareup/cardreader/lcr/CrsCapability;->swigValue()I

    move-result v4

    if-ne v4, p1, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method getFeaturePointer()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;
    .locals 1

    .line 69
    iget-object v0, p0, Lcom/squareup/cardreader/SystemFeatureLegacy;->systemFeature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;

    return-object v0
.end method

.method public initialize(Lcom/squareup/cardreader/SystemFeatureLegacy$SystemFeatureListener;)V
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/squareup/cardreader/SystemFeatureLegacy;->systemFeature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;

    if-nez v0, :cond_1

    if-eqz p1, :cond_0

    .line 47
    iput-object p1, p0, Lcom/squareup/cardreader/SystemFeatureLegacy;->apiListener:Lcom/squareup/cardreader/SystemFeatureLegacy$SystemFeatureListener;

    .line 48
    iget-object p1, p0, Lcom/squareup/cardreader/SystemFeatureLegacy;->systemFeatureNative:Lcom/squareup/cardreader/lcr/SystemFeatureNativeInterface;

    iget-object v0, p0, Lcom/squareup/cardreader/SystemFeatureLegacy;->sessionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderPointer;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderPointer;->getId()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;

    move-result-object v0

    invoke-interface {p1, v0, p0}, Lcom/squareup/cardreader/lcr/SystemFeatureNativeInterface;->system_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cardreader/SystemFeatureLegacy;->systemFeature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;

    return-void

    .line 44
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "apiListener cannot be null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 41
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "SystemFeature is already initialized!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public onCapabilitiesReceived(Z[B)V
    .locals 7

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    if-eqz p1, :cond_0

    const-string v2, "supported"

    goto :goto_0

    :cond_0
    const-string v2, "unsupported"

    :goto_0
    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "Capabilities %s"

    .line 96
    invoke-static {v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 98
    new-instance v1, Ljava/util/ArrayList;

    array-length v2, p2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 99
    array-length v2, p2

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v2, :cond_2

    aget-byte v5, p2, v4

    .line 100
    invoke-virtual {p0, v5}, Lcom/squareup/cardreader/SystemFeatureLegacy;->getCapabilityForByteValue(B)Lcom/squareup/cardreader/lcr/CrsCapability;

    move-result-object v6

    if-nez v6, :cond_1

    new-array v6, v0, [Ljava/lang/Object;

    .line 102
    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v5

    aput-object v5, v6, v3

    const-string v5, "Received unknown capability byte %d"

    invoke-static {v5, v6}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 104
    :cond_1
    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 108
    :cond_2
    iget-object p2, p0, Lcom/squareup/cardreader/SystemFeatureLegacy;->apiListener:Lcom/squareup/cardreader/SystemFeatureLegacy$SystemFeatureListener;

    invoke-interface {p2, p1, v1}, Lcom/squareup/cardreader/SystemFeatureLegacy$SystemFeatureListener;->onCapabilitiesReceived(ZLjava/util/List;)V

    return-void
.end method

.method public onChargeCycleCountReceived(I)V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 90
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "Charge Cycle Count: %d"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 91
    iget-object v0, p0, Lcom/squareup/cardreader/SystemFeatureLegacy;->apiListener:Lcom/squareup/cardreader/SystemFeatureLegacy$SystemFeatureListener;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/SystemFeatureLegacy$SystemFeatureListener;->onChargeCycleCountReceived(I)V

    return-void
.end method

.method public onFirmwareVersionReceived(Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "Firmware Version: %s"

    .line 84
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 85
    iget-object v0, p0, Lcom/squareup/cardreader/SystemFeatureLegacy;->apiListener:Lcom/squareup/cardreader/SystemFeatureLegacy$SystemFeatureListener;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/SystemFeatureLegacy$SystemFeatureListener;->onFirmwareVersionReceived(Ljava/lang/String;)V

    return-void
.end method

.method public onHardwareSerialNumberReceived(Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "Hardware SN: %s"

    .line 78
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 79
    iget-object v0, p0, Lcom/squareup/cardreader/SystemFeatureLegacy;->apiListener:Lcom/squareup/cardreader/SystemFeatureLegacy$SystemFeatureListener;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/SystemFeatureLegacy$SystemFeatureListener;->onHardwareSerialNumberReceived(Ljava/lang/String;)V

    return-void
.end method

.method public onReaderErrorReceived(I)V
    .locals 1

    .line 113
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CrsReaderError;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrsReaderError;

    move-result-object p1

    .line 114
    iget-object v0, p0, Lcom/squareup/cardreader/SystemFeatureLegacy;->apiListener:Lcom/squareup/cardreader/SystemFeatureLegacy$SystemFeatureListener;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/SystemFeatureLegacy$SystemFeatureListener;->onReaderError(Lcom/squareup/cardreader/lcr/CrsReaderError;)V

    return-void
.end method

.method public requestSystemInfo()V
    .locals 2

    .line 65
    iget-object v0, p0, Lcom/squareup/cardreader/SystemFeatureLegacy;->systemFeatureNative:Lcom/squareup/cardreader/lcr/SystemFeatureNativeInterface;

    iget-object v1, p0, Lcom/squareup/cardreader/SystemFeatureLegacy;->systemFeature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/lcr/SystemFeatureNativeInterface;->cr_system_read_system_info(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;)Lcom/squareup/cardreader/lcr/CrSystemResult;

    return-void
.end method

.method public resetSystemFeature()V
    .locals 2

    .line 52
    iget-object v0, p0, Lcom/squareup/cardreader/SystemFeatureLegacy;->systemFeature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;

    if-eqz v0, :cond_0

    .line 53
    iget-object v1, p0, Lcom/squareup/cardreader/SystemFeatureLegacy;->systemFeatureNative:Lcom/squareup/cardreader/lcr/SystemFeatureNativeInterface;

    invoke-interface {v1, v0}, Lcom/squareup/cardreader/lcr/SystemFeatureNativeInterface;->cr_system_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;)Lcom/squareup/cardreader/lcr/CrSystemResult;

    .line 54
    iget-object v0, p0, Lcom/squareup/cardreader/SystemFeatureLegacy;->systemFeatureNative:Lcom/squareup/cardreader/lcr/SystemFeatureNativeInterface;

    iget-object v1, p0, Lcom/squareup/cardreader/SystemFeatureLegacy;->systemFeature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/lcr/SystemFeatureNativeInterface;->cr_system_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;)Lcom/squareup/cardreader/lcr/CrSystemResult;

    const/4 v0, 0x0

    .line 55
    iput-object v0, p0, Lcom/squareup/cardreader/SystemFeatureLegacy;->systemFeature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;

    .line 56
    iput-object v0, p0, Lcom/squareup/cardreader/SystemFeatureLegacy;->apiListener:Lcom/squareup/cardreader/SystemFeatureLegacy$SystemFeatureListener;

    :cond_0
    return-void
.end method

.method public setReaderFeatureFlags(Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;)V
    .locals 2

    .line 145
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->quickchip_fw209030:Ljava/lang/Boolean;

    const-string v1, "quickchip_fw209030"

    invoke-direct {p0, v1, v0}, Lcom/squareup/cardreader/SystemFeatureLegacy;->setFeatureFlag(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 146
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->pinblock_format_v2:Ljava/lang/Boolean;

    const-string v1, "pinblock_format_v2"

    invoke-direct {p0, v1, v0}, Lcom/squareup/cardreader/SystemFeatureLegacy;->setFeatureFlag(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 147
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->account_type_selection:Ljava/lang/Boolean;

    const-string v1, "account_type_selection"

    invoke-direct {p0, v1, v0}, Lcom/squareup/cardreader/SystemFeatureLegacy;->setFeatureFlag(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 148
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->spoc_prng_seed:Ljava/lang/Boolean;

    const-string v1, "spoc_prng_seed"

    invoke-direct {p0, v1, v0}, Lcom/squareup/cardreader/SystemFeatureLegacy;->setFeatureFlag(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 149
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->common_debit_support:Ljava/lang/Boolean;

    const-string v1, "common_debit_support"

    invoke-direct {p0, v1, v0}, Lcom/squareup/cardreader/SystemFeatureLegacy;->setFeatureFlag(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 150
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->sonic_branding:Ljava/lang/Boolean;

    const-string v1, "sonic_branding"

    invoke-direct {p0, v1, v0}, Lcom/squareup/cardreader/SystemFeatureLegacy;->setFeatureFlag(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 151
    iget-object p1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->felica_notification:Ljava/lang/Boolean;

    const-string v0, "felica_notification"

    invoke-direct {p0, v0, p1}, Lcom/squareup/cardreader/SystemFeatureLegacy;->setFeatureFlag(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 152
    iget-object p1, p0, Lcom/squareup/cardreader/SystemFeatureLegacy;->systemFeatureNative:Lcom/squareup/cardreader/lcr/SystemFeatureNativeInterface;

    iget-object v0, p0, Lcom/squareup/cardreader/SystemFeatureLegacy;->systemFeature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;

    .line 153
    invoke-interface {p1, v0}, Lcom/squareup/cardreader/lcr/SystemFeatureNativeInterface;->cr_system_mark_feature_flags_ready_to_send(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_system_t;)Lcom/squareup/cardreader/lcr/CrSystemResult;

    move-result-object p1

    .line 154
    sget-object v0, Lcom/squareup/cardreader/lcr/CrSystemResult;->CR_SYSTEM_RESULT_SUCCESS:Lcom/squareup/cardreader/lcr/CrSystemResult;

    if-eq p1, v0, :cond_0

    .line 155
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "cr_system_mark_feature_flags_ready_to_send failed! "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1, v0}, Ltimber/log/Timber;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method
