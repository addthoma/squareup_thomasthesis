.class Lcom/squareup/cardreader/SecureTouchFeatureLegacy;
.super Ljava/lang/Object;
.source "SecureTouchFeatureLegacy.java"

# interfaces
.implements Lcom/squareup/cardreader/SecureTouchFeatureInterface;
.implements Lcom/squareup/cardreader/SecureTouchFeature;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/SecureTouchFeatureLegacy$SkippingNotifyLcrException;,
        Lcom/squareup/cardreader/SecureTouchFeatureLegacy$SpeTouchDriverRequest;
    }
.end annotation


# instance fields
.field private final cardReaderPointerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;"
        }
    .end annotation
.end field

.field private final lcrExecutor:Ljava/util/concurrent/ExecutorService;

.field private listener:Lcom/squareup/securetouch/SecureTouchFeatureNativeListener;

.field private mostRecentSpeTouchDriverRequest:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference<",
            "Lcom/squareup/cardreader/SecureTouchFeatureLegacy$SpeTouchDriverRequest;",
            ">;"
        }
    .end annotation
.end field

.field private final nativeFeature:Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeInterface;

.field private secureTouch:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;

.field private final squidInterfaceScheduler:Lio/reactivex/Scheduler;

.field private subs:Lio/reactivex/disposables/CompositeDisposable;

.field private final touchReporting:Lcom/squareup/securetouch/TouchReporting;


# direct methods
.method constructor <init>(Ljavax/inject/Provider;Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeInterface;Lcom/squareup/securetouch/TouchReporting;Ljava/util/concurrent/ExecutorService;Lio/reactivex/Scheduler;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;",
            "Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeInterface;",
            "Lcom/squareup/securetouch/TouchReporting;",
            "Ljava/util/concurrent/ExecutorService;",
            "Lio/reactivex/Scheduler;",
            ")V"
        }
    .end annotation

    .line 133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 124
    new-instance v0, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v0}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object v0, p0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy;->subs:Lio/reactivex/disposables/CompositeDisposable;

    .line 125
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    sget-object v1, Lcom/squareup/cardreader/SecureTouchFeatureLegacy$SpeTouchDriverRequest;->NONE:Lcom/squareup/cardreader/SecureTouchFeatureLegacy$SpeTouchDriverRequest;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy;->mostRecentSpeTouchDriverRequest:Ljava/util/concurrent/atomic/AtomicReference;

    .line 134
    iput-object p1, p0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy;->cardReaderPointerProvider:Ljavax/inject/Provider;

    .line 135
    iput-object p2, p0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy;->nativeFeature:Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeInterface;

    .line 136
    iput-object p3, p0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy;->touchReporting:Lcom/squareup/securetouch/TouchReporting;

    .line 137
    iput-object p4, p0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy;->lcrExecutor:Ljava/util/concurrent/ExecutorService;

    .line 138
    iput-object p5, p0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy;->squidInterfaceScheduler:Lio/reactivex/Scheduler;

    return-void
.end method

.method private static forStmButton(Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;)Lcom/squareup/securetouch/SecureKey;
    .locals 3

    .line 441
    sget-object v0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy$1;->$SwitchMap$com$squareup$cardreader$lcr$CrsStmPinPadEventId:[I

    invoke-virtual {p0}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_4

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_1

    const/4 v1, 0x6

    if-eq v0, v1, :cond_0

    packed-switch v0, :pswitch_data_0

    .line 473
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Don\'t have this button: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 461
    :pswitch_0
    sget-object p0, Lcom/squareup/securetouch/SecureKey;->Nine:Lcom/squareup/securetouch/SecureKey;

    return-object p0

    .line 459
    :pswitch_1
    sget-object p0, Lcom/squareup/securetouch/SecureKey;->Eight:Lcom/squareup/securetouch/SecureKey;

    return-object p0

    .line 457
    :pswitch_2
    sget-object p0, Lcom/squareup/securetouch/SecureKey;->Seven:Lcom/squareup/securetouch/SecureKey;

    return-object p0

    .line 455
    :pswitch_3
    sget-object p0, Lcom/squareup/securetouch/SecureKey;->Six:Lcom/squareup/securetouch/SecureKey;

    return-object p0

    .line 453
    :pswitch_4
    sget-object p0, Lcom/squareup/securetouch/SecureKey;->Five:Lcom/squareup/securetouch/SecureKey;

    return-object p0

    .line 451
    :pswitch_5
    sget-object p0, Lcom/squareup/securetouch/SecureKey;->Four:Lcom/squareup/securetouch/SecureKey;

    return-object p0

    .line 449
    :pswitch_6
    sget-object p0, Lcom/squareup/securetouch/SecureKey;->Three:Lcom/squareup/securetouch/SecureKey;

    return-object p0

    .line 447
    :pswitch_7
    sget-object p0, Lcom/squareup/securetouch/SecureKey;->Two:Lcom/squareup/securetouch/SecureKey;

    return-object p0

    .line 445
    :pswitch_8
    sget-object p0, Lcom/squareup/securetouch/SecureKey;->One:Lcom/squareup/securetouch/SecureKey;

    return-object p0

    .line 443
    :pswitch_9
    sget-object p0, Lcom/squareup/securetouch/SecureKey;->Zero:Lcom/squareup/securetouch/SecureKey;

    return-object p0

    .line 471
    :cond_0
    sget-object p0, Lcom/squareup/securetouch/SecureKey;->Accessibility:Lcom/squareup/securetouch/SecureKey;

    return-object p0

    .line 469
    :cond_1
    sget-object p0, Lcom/squareup/securetouch/SecureKey;->Delete:Lcom/squareup/securetouch/SecureKey;

    return-object p0

    .line 467
    :cond_2
    sget-object p0, Lcom/squareup/securetouch/SecureKey;->Clear:Lcom/squareup/securetouch/SecureKey;

    return-object p0

    .line 465
    :cond_3
    sget-object p0, Lcom/squareup/securetouch/SecureKey;->Cancel:Lcom/squareup/securetouch/SecureKey;

    return-object p0

    .line 463
    :cond_4
    sget-object p0, Lcom/squareup/securetouch/SecureKey;->Done:Lcom/squareup/securetouch/SecureKey;

    return-object p0

    :pswitch_data_0
    .packed-switch 0xe
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic lambda$enableSquidTouchDriver$2(Lcom/squareup/securetouch/TouchReportingEvent$TouchReportingEnabled;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const-string v3, "SecureTouchFeature: enableTouchReporting => %s"

    .line 204
    invoke-static {v3, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 205
    invoke-virtual {p0}, Lcom/squareup/securetouch/TouchReportingEvent$TouchReportingEnabled;->getResult()Lcom/squareup/securetouch/TouchReportingResult;

    move-result-object p0

    sget-object v1, Lcom/squareup/securetouch/TouchReportingResult;->SUCCESS:Lcom/squareup/securetouch/TouchReportingResult;

    if-ne p0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string p0, "Failed to enable touch reporting!"

    invoke-static {v0, p0}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    return-void
.end method

.method private notifyLcrIfMostRecentTouchDriverRequestIsDisable(Ljava/lang/String;Ljava/lang/Runnable;)V
    .locals 1

    .line 394
    new-instance v0, Lcom/squareup/cardreader/-$$Lambda$SecureTouchFeatureLegacy$3-xshRmj51ki7CfCMJRisX2IKEw;

    invoke-direct {v0, p0, p2, p1}, Lcom/squareup/cardreader/-$$Lambda$SecureTouchFeatureLegacy$3-xshRmj51ki7CfCMJRisX2IKEw;-><init>(Lcom/squareup/cardreader/SecureTouchFeatureLegacy;Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 396
    new-instance p2, Lcom/squareup/cardreader/-$$Lambda$SecureTouchFeatureLegacy$uZ-dPFw09VO4GxwCbT6CBvtaOkw;

    invoke-direct {p2, p0, v0}, Lcom/squareup/cardreader/-$$Lambda$SecureTouchFeatureLegacy$uZ-dPFw09VO4GxwCbT6CBvtaOkw;-><init>(Lcom/squareup/cardreader/SecureTouchFeatureLegacy;Ljava/lang/Runnable;)V

    .line 397
    invoke-direct {p0, p2, p1}, Lcom/squareup/cardreader/SecureTouchFeatureLegacy;->runIfCurrentTouchDriverRequestIsDisable(Ljava/lang/Runnable;Ljava/lang/String;)V

    return-void
.end method

.method private onInitiateSecureTouch()V
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "SecureTouchFeature: Calling LCR start_secure_touch"

    .line 301
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 302
    iget-object v0, p0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy;->lcrExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$SecureTouchFeatureLegacy$20YXLJG8uCUqUPXH4UvJaNuIoPs;

    invoke-direct {v1, p0}, Lcom/squareup/cardreader/-$$Lambda$SecureTouchFeatureLegacy$20YXLJG8uCUqUPXH4UvJaNuIoPs;-><init>(Lcom/squareup/cardreader/SecureTouchFeatureLegacy;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private onSecureTouchKeypadActive(Lcom/squareup/securetouch/SecureTouchKeypadActive;)V
    .locals 7

    .line 311
    invoke-virtual {p1}, Lcom/squareup/securetouch/SecureTouchKeypadActive;->getCoordinates()Ljava/util/Map;

    move-result-object v0

    .line 312
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SecureTouchFeature: Keypad visible: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v1, v3}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v1, 0x0

    .line 314
    :goto_0
    sget-object v3, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_BUTTON_ID_MAX:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    invoke-virtual {v3}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->swigValue()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 315
    invoke-static {v1}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    move-result-object v3

    invoke-static {v3}, Lcom/squareup/cardreader/SecureTouchFeatureLegacy;->forStmButton(Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;)Lcom/squareup/securetouch/SecureKey;

    move-result-object v3

    .line 319
    invoke-interface {v0, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    goto :goto_1

    .line 323
    :cond_0
    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/securetouch/SecureTouchRect;

    .line 324
    new-instance v4, Lcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;

    invoke-direct {v4}, Lcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;-><init>()V

    .line 325
    invoke-virtual {v4, v1}, Lcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;->setButton_id(I)V

    .line 326
    invoke-virtual {v3}, Lcom/squareup/securetouch/SecureTouchRect;->getLeft()I

    move-result v5

    int-to-short v5, v5

    invoke-virtual {v4, v5}, Lcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;->setX_start(I)V

    .line 327
    invoke-virtual {v3}, Lcom/squareup/securetouch/SecureTouchRect;->getTop()I

    move-result v5

    int-to-short v5, v5

    invoke-virtual {v4, v5}, Lcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;->setY_start(I)V

    .line 328
    invoke-virtual {v3}, Lcom/squareup/securetouch/SecureTouchRect;->getRight()I

    move-result v5

    invoke-virtual {v3}, Lcom/squareup/securetouch/SecureTouchRect;->getLeft()I

    move-result v6

    sub-int/2addr v5, v6

    int-to-short v5, v5

    invoke-virtual {v4, v5}, Lcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;->setX_size(I)V

    .line 329
    invoke-virtual {v3}, Lcom/squareup/securetouch/SecureTouchRect;->getBottom()I

    move-result v5

    invoke-virtual {v3}, Lcom/squareup/securetouch/SecureTouchRect;->getTop()I

    move-result v3

    sub-int/2addr v5, v3

    int-to-short v3, v5

    invoke-virtual {v4, v3}, Lcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;->setY_size(I)V

    .line 331
    new-instance v3, Lcom/squareup/cardreader/-$$Lambda$SecureTouchFeatureLegacy$sA85oNnhE3s1AOqhBUem70f_Fco;

    invoke-direct {v3, p0, v4}, Lcom/squareup/cardreader/-$$Lambda$SecureTouchFeatureLegacy$sA85oNnhE3s1AOqhBUem70f_Fco;-><init>(Lcom/squareup/cardreader/SecureTouchFeatureLegacy;Lcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;)V

    const-string v4, "regular_set_button_location"

    invoke-direct {p0, v4, v3}, Lcom/squareup/cardreader/SecureTouchFeatureLegacy;->notifyLcrIfMostRecentTouchDriverRequestIsDisable(Ljava/lang/String;Ljava/lang/Runnable;)V

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 336
    :cond_1
    instance-of v1, p1, Lcom/squareup/securetouch/SecureTouchAccessibilityKeypadActive;

    if-eqz v1, :cond_2

    .line 337
    sget-object v3, Lcom/squareup/securetouch/SecureKey;->AccessibilityKeypadArea:Lcom/squareup/securetouch/SecureKey;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/securetouch/SecureTouchRect;

    .line 339
    new-instance v3, Lcom/squareup/cardreader/lcr/CrsStmAccessibilityPinPadConfig;

    invoke-direct {v3}, Lcom/squareup/cardreader/lcr/CrsStmAccessibilityPinPadConfig;-><init>()V

    .line 341
    invoke-virtual {v0}, Lcom/squareup/securetouch/SecureTouchRect;->getRight()I

    move-result v4

    int-to-long v4, v4

    invoke-virtual {v3, v4, v5}, Lcom/squareup/cardreader/lcr/CrsStmAccessibilityPinPadConfig;->setX_max(J)V

    .line 342
    invoke-virtual {v0}, Lcom/squareup/securetouch/SecureTouchRect;->getBottom()I

    move-result v0

    int-to-long v4, v0

    invoke-virtual {v3, v4, v5}, Lcom/squareup/cardreader/lcr/CrsStmAccessibilityPinPadConfig;->setY_max(J)V

    .line 343
    check-cast p1, Lcom/squareup/securetouch/SecureTouchAccessibilityKeypadActive;

    .line 344
    invoke-virtual {p1}, Lcom/squareup/securetouch/SecureTouchAccessibilityKeypadActive;->getKeypadBorderSize()I

    move-result v0

    int-to-long v4, v0

    .line 343
    invoke-virtual {v3, v4, v5}, Lcom/squareup/cardreader/lcr/CrsStmAccessibilityPinPadConfig;->setBorder_width(J)V

    .line 346
    invoke-virtual {p1}, Lcom/squareup/securetouch/SecureTouchAccessibilityKeypadActive;->getKeypadDigitRadius()I

    move-result p1

    int-to-long v4, p1

    .line 345
    invoke-virtual {v3, v4, v5}, Lcom/squareup/cardreader/lcr/CrsStmAccessibilityPinPadConfig;->setCenter_button_radius(J)V

    .line 348
    new-instance p1, Lcom/squareup/cardreader/-$$Lambda$SecureTouchFeatureLegacy$3EcLlmxDclzmlL7rATKe55nXr54;

    invoke-direct {p1, p0, v3}, Lcom/squareup/cardreader/-$$Lambda$SecureTouchFeatureLegacy$3EcLlmxDclzmlL7rATKe55nXr54;-><init>(Lcom/squareup/cardreader/SecureTouchFeatureLegacy;Lcom/squareup/cardreader/lcr/CrsStmAccessibilityPinPadConfig;)V

    const-string v0, "set_accessibility_configs"

    invoke-direct {p0, v0, p1}, Lcom/squareup/cardreader/SecureTouchFeatureLegacy;->notifyLcrIfMostRecentTouchDriverRequestIsDisable(Ljava/lang/String;Ljava/lang/Runnable;)V

    :cond_2
    if-eqz v1, :cond_3

    .line 355
    sget-object p1, Lcom/squareup/cardreader/lcr/CrsStmPinPadConfigType;->CRS_STM_PINPAD_CONFIG_ACCESS:Lcom/squareup/cardreader/lcr/CrsStmPinPadConfigType;

    goto :goto_2

    .line 357
    :cond_3
    sget-object p1, Lcom/squareup/cardreader/lcr/CrsStmPinPadConfigType;->CRS_STM_PINPAD_CONFIG_REGULAR:Lcom/squareup/cardreader/lcr/CrsStmPinPadConfigType;

    .line 360
    :goto_2
    new-instance v0, Lcom/squareup/cardreader/-$$Lambda$SecureTouchFeatureLegacy$cMMMRcA2uBbMXsuEUMoQC-sp7P0;

    invoke-direct {v0, p0, p1}, Lcom/squareup/cardreader/-$$Lambda$SecureTouchFeatureLegacy$cMMMRcA2uBbMXsuEUMoQC-sp7P0;-><init>(Lcom/squareup/cardreader/SecureTouchFeatureLegacy;Lcom/squareup/cardreader/lcr/CrsStmPinPadConfigType;)V

    const-string p1, "sent_pinpad_configs"

    invoke-direct {p0, p1, v0}, Lcom/squareup/cardreader/SecureTouchFeatureLegacy;->notifyLcrIfMostRecentTouchDriverRequestIsDisable(Ljava/lang/String;Ljava/lang/Runnable;)V

    new-array p1, v2, [Ljava/lang/Object;

    const-string v0, "SecureTouchFeature: Done sending coordinates"

    .line 366
    invoke-static {v0, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method private pinPadEventToKeyPressEvent(Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;)Lcom/squareup/securetouch/SecureTouchFeatureEvent;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/squareup/cardreader/UnhandledPinPadEvent;
        }
    .end annotation

    .line 410
    sget-object v0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy$1;->$SwitchMap$com$squareup$cardreader$lcr$CrsStmPinPadEventId:[I

    invoke-virtual {p1}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 436
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Received unexpected keypad event: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 434
    :pswitch_0
    sget-object p1, Lcom/squareup/securetouch/KeypadInvalidAction$TooFewDigits;->INSTANCE:Lcom/squareup/securetouch/KeypadInvalidAction$TooFewDigits;

    return-object p1

    .line 432
    :pswitch_1
    sget-object p1, Lcom/squareup/securetouch/KeypadInvalidAction$TooManyDigits;->INSTANCE:Lcom/squareup/securetouch/KeypadInvalidAction$TooManyDigits;

    return-object p1

    .line 430
    :pswitch_2
    sget-object p1, Lcom/squareup/securetouch/KeypadInvalidAction$Error;->INSTANCE:Lcom/squareup/securetouch/KeypadInvalidAction$Error;

    return-object p1

    .line 426
    :pswitch_3
    new-instance p1, Lcom/squareup/cardreader/UnhandledPinPadEvent;

    const-string v0, "TODO(RA-37026): Handle pin pad event TOUCH_DOWN"

    invoke-direct {p1, v0}, Lcom/squareup/cardreader/UnhandledPinPadEvent;-><init>(Ljava/lang/String;)V

    throw p1

    .line 424
    :pswitch_4
    sget-object p1, Lcom/squareup/securetouch/KeypadDigitPressed;->INSTANCE:Lcom/squareup/securetouch/KeypadDigitPressed;

    return-object p1

    .line 422
    :pswitch_5
    sget-object p1, Lcom/squareup/securetouch/KeypadAccessibilityPressed;->INSTANCE:Lcom/squareup/securetouch/KeypadAccessibilityPressed;

    return-object p1

    .line 420
    :pswitch_6
    sget-object p1, Lcom/squareup/securetouch/KeypadDeletePressed;->INSTANCE:Lcom/squareup/securetouch/KeypadDeletePressed;

    return-object p1

    .line 418
    :pswitch_7
    sget-object p1, Lcom/squareup/securetouch/KeypadClearPressed;->INSTANCE:Lcom/squareup/securetouch/KeypadClearPressed;

    return-object p1

    .line 416
    :pswitch_8
    sget-object p1, Lcom/squareup/securetouch/SecureTouchUserCancelled$SecureTouchUserSwipeCancelled;->INSTANCE:Lcom/squareup/securetouch/SecureTouchUserCancelled$SecureTouchUserSwipeCancelled;

    return-object p1

    .line 414
    :pswitch_9
    sget-object p1, Lcom/squareup/securetouch/SecureTouchUserCancelled$SecureTouchUserButtonCancelled;->INSTANCE:Lcom/squareup/securetouch/SecureTouchUserCancelled$SecureTouchUserButtonCancelled;

    return-object p1

    .line 412
    :pswitch_a
    sget-object p1, Lcom/squareup/securetouch/SecureTouchUserDone;->INSTANCE:Lcom/squareup/securetouch/SecureTouchUserDone;

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private runIfCurrentTouchDriverRequestIsDisable(Ljava/lang/Runnable;Ljava/lang/String;)V
    .locals 2

    .line 401
    iget-object v0, p0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy;->mostRecentSpeTouchDriverRequest:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/squareup/cardreader/SecureTouchFeatureLegacy$SpeTouchDriverRequest;->DISABLE_TOUCH_DRIVER:Lcom/squareup/cardreader/SecureTouchFeatureLegacy$SpeTouchDriverRequest;

    if-ne v0, v1, :cond_0

    .line 402
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 404
    :cond_0
    new-instance p1, Lcom/squareup/cardreader/SecureTouchFeatureLegacy$SkippingNotifyLcrException;

    invoke-direct {p1, p0, p2}, Lcom/squareup/cardreader/SecureTouchFeatureLegacy$SkippingNotifyLcrException;-><init>(Lcom/squareup/cardreader/SecureTouchFeatureLegacy;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method private subscribeToReportingEvent(Lio/reactivex/Single;Lio/reactivex/functions/Consumer;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/securetouch/TouchReportingEvent;",
            ">(",
            "Lio/reactivex/Single<",
            "TT;>;",
            "Lio/reactivex/functions/Consumer<",
            "-TT;>;)V"
        }
    .end annotation

    .line 371
    iget-object v0, p0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy;->subs:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy;->squidInterfaceScheduler:Lio/reactivex/Scheduler;

    .line 372
    invoke-virtual {p1, v1}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object p1

    .line 373
    invoke-virtual {p1, p2}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    .line 371
    invoke-virtual {v0, p1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method


# virtual methods
.method public disableSquidTouchDriver()V
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "SecureTouchFeature: SPE says disableSquidTouchDriver"

    .line 172
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 174
    iget-object v0, p0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy;->mostRecentSpeTouchDriverRequest:Ljava/util/concurrent/atomic/AtomicReference;

    sget-object v1, Lcom/squareup/cardreader/SecureTouchFeatureLegacy$SpeTouchDriverRequest;->DISABLE_TOUCH_DRIVER:Lcom/squareup/cardreader/SecureTouchFeatureLegacy$SpeTouchDriverRequest;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 176
    iget-object v0, p0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy;->touchReporting:Lcom/squareup/securetouch/TouchReporting;

    invoke-interface {v0}, Lcom/squareup/securetouch/TouchReporting;->disableTouchReporting()Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$SecureTouchFeatureLegacy$kTic9bJ2fa_Q4A0CRwCTnq5MNJA;

    invoke-direct {v1, p0}, Lcom/squareup/cardreader/-$$Lambda$SecureTouchFeatureLegacy$kTic9bJ2fa_Q4A0CRwCTnq5MNJA;-><init>(Lcom/squareup/cardreader/SecureTouchFeatureLegacy;)V

    invoke-direct {p0, v0, v1}, Lcom/squareup/cardreader/SecureTouchFeatureLegacy;->subscribeToReportingEvent(Lio/reactivex/Single;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public enableSquidTouchDriver()V
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "SecureTouchFeature: SPE says enableSquidTouchDriver"

    .line 199
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 201
    iget-object v0, p0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy;->mostRecentSpeTouchDriverRequest:Ljava/util/concurrent/atomic/AtomicReference;

    sget-object v1, Lcom/squareup/cardreader/SecureTouchFeatureLegacy$SpeTouchDriverRequest;->ENABLE_TOUCH_DRIVER:Lcom/squareup/cardreader/SecureTouchFeatureLegacy$SpeTouchDriverRequest;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 203
    iget-object v0, p0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy;->touchReporting:Lcom/squareup/securetouch/TouchReporting;

    invoke-interface {v0}, Lcom/squareup/securetouch/TouchReporting;->enableTouchReporting()Lio/reactivex/Single;

    move-result-object v0

    sget-object v1, Lcom/squareup/cardreader/-$$Lambda$SecureTouchFeatureLegacy$0DRzjTyklWY1f4J7KfOqzVeQAtk;->INSTANCE:Lcom/squareup/cardreader/-$$Lambda$SecureTouchFeatureLegacy$0DRzjTyklWY1f4J7KfOqzVeQAtk;

    invoke-direct {p0, v0, v1}, Lcom/squareup/cardreader/SecureTouchFeatureLegacy;->subscribeToReportingEvent(Lio/reactivex/Single;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public hideSecureTouchPinPad()V
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "SecureTouchFeature: SPE says hideSecureTouchPinPad"

    .line 255
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 257
    iget-object v0, p0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy;->listener:Lcom/squareup/securetouch/SecureTouchFeatureNativeListener;

    invoke-interface {v0}, Lcom/squareup/securetouch/SecureTouchFeatureNativeListener;->secureTouchDisabled()V

    .line 261
    new-instance v0, Lcom/squareup/cardreader/-$$Lambda$SecureTouchFeatureLegacy$giabRgZaDQ0KR4lua43MCxpHtIk;

    invoke-direct {v0, p0}, Lcom/squareup/cardreader/-$$Lambda$SecureTouchFeatureLegacy$giabRgZaDQ0KR4lua43MCxpHtIk;-><init>(Lcom/squareup/cardreader/SecureTouchFeatureLegacy;)V

    const-string v1, "pin_pad_is_hidden"

    invoke-direct {p0, v1, v0}, Lcom/squareup/cardreader/SecureTouchFeatureLegacy;->notifyLcrIfMostRecentTouchDriverRequestIsDisable(Ljava/lang/String;Ljava/lang/Runnable;)V

    return-void
.end method

.method public initialize(Lcom/squareup/securetouch/SecureTouchFeatureNativeListener;)V
    .locals 3

    .line 142
    iget-object v0, p0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy;->secureTouch:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v2, "SecureTouchFeature already initialized!"

    invoke-static {v0, v2}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    new-array v0, v1, [Ljava/lang/Object;

    const-string v1, "SecureTouchFeature: initialize"

    .line 144
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 146
    iput-object p1, p0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy;->listener:Lcom/squareup/securetouch/SecureTouchFeatureNativeListener;

    .line 148
    iget-object p1, p0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy;->nativeFeature:Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeInterface;

    iget-object v0, p0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy;->cardReaderPointerProvider:Ljavax/inject/Provider;

    .line 150
    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderPointer;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderPointer;->getId()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;

    move-result-object v0

    .line 149
    invoke-interface {p1, v0, p0}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeInterface;->secure_touch_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy;->secureTouch:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;

    return-void
.end method

.method public synthetic lambda$disableSquidTouchDriver$1$SecureTouchFeatureLegacy(Lcom/squareup/securetouch/TouchReportingEvent$TouchReportingDisabled;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const-string v3, "SecureTouchFeature: disableTouchReporting => %s"

    .line 177
    invoke-static {v3, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 178
    invoke-virtual {p1}, Lcom/squareup/securetouch/TouchReportingEvent$TouchReportingDisabled;->getResult()Lcom/squareup/securetouch/TouchReportingResult;

    move-result-object p1

    sget-object v1, Lcom/squareup/securetouch/TouchReportingResult;->SUCCESS:Lcom/squareup/securetouch/TouchReportingResult;

    if-ne p1, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string p1, "Failed to disable touch reporting!"

    invoke-static {v0, p1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 180
    iget-object p1, p0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy;->listener:Lcom/squareup/securetouch/SecureTouchFeatureNativeListener;

    invoke-interface {p1}, Lcom/squareup/securetouch/SecureTouchFeatureNativeListener;->secureTouchEnabled()V

    .line 182
    new-instance p1, Lcom/squareup/cardreader/-$$Lambda$SecureTouchFeatureLegacy$4z7bx3V1eBfrBaJsckZpz7SbD8U;

    invoke-direct {p1, p0}, Lcom/squareup/cardreader/-$$Lambda$SecureTouchFeatureLegacy$4z7bx3V1eBfrBaJsckZpz7SbD8U;-><init>(Lcom/squareup/cardreader/SecureTouchFeatureLegacy;)V

    const-string v0, "disable_squid_touch_driver_result"

    invoke-direct {p0, v0, p1}, Lcom/squareup/cardreader/SecureTouchFeatureLegacy;->notifyLcrIfMostRecentTouchDriverRequestIsDisable(Ljava/lang/String;Ljava/lang/Runnable;)V

    return-void
.end method

.method public synthetic lambda$hideSecureTouchPinPad$3$SecureTouchFeatureLegacy()V
    .locals 3

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "SecureTouchFeature: Calling LCR pin_pad_is_hidden"

    .line 262
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 263
    iget-object v0, p0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy;->nativeFeature:Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeInterface;

    iget-object v1, p0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy;->secureTouch:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;

    new-instance v2, Lcom/squareup/cardreader/lcr/CrsStmHidePinPadResult;

    invoke-direct {v2}, Lcom/squareup/cardreader/lcr/CrsStmHidePinPadResult;-><init>()V

    invoke-interface {v0, v1, v2}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeInterface;->cr_secure_touch_mode_pin_pad_is_hidden(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;Lcom/squareup/cardreader/lcr/CrsStmHidePinPadResult;)V

    return-void
.end method

.method public synthetic lambda$notifyLcrIfMostRecentTouchDriverRequestIsDisable$8$SecureTouchFeatureLegacy(Ljava/lang/Runnable;Ljava/lang/String;)V
    .locals 0

    .line 395
    invoke-direct {p0, p1, p2}, Lcom/squareup/cardreader/SecureTouchFeatureLegacy;->runIfCurrentTouchDriverRequestIsDisable(Ljava/lang/Runnable;Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$notifyLcrIfMostRecentTouchDriverRequestIsDisable$9$SecureTouchFeatureLegacy(Ljava/lang/Runnable;)V
    .locals 1

    .line 396
    iget-object v0, p0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy;->lcrExecutor:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public synthetic lambda$null$0$SecureTouchFeatureLegacy()V
    .locals 3

    .line 185
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsStmDisableSquidTouchDriverResult;

    invoke-direct {v0}, Lcom/squareup/cardreader/lcr/CrsStmDisableSquidTouchDriverResult;-><init>()V

    .line 187
    sget-object v1, Lcom/squareup/cardreader/lcr/CrSecureTouchResult;->CR_SECURE_TOUCH_MODE_FEATURE_RESULT_SUCCESS:Lcom/squareup/cardreader/lcr/CrSecureTouchResult;

    .line 188
    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrSecureTouchResult;->swigValue()I

    move-result v1

    int-to-short v1, v1

    .line 187
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/lcr/CrsStmDisableSquidTouchDriverResult;->setError_code(S)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "SecureTouchFeature: Calling LCR disable_squid_touch_driver_result"

    .line 189
    invoke-static {v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 190
    iget-object v1, p0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy;->nativeFeature:Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeInterface;

    iget-object v2, p0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy;->secureTouch:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;

    invoke-interface {v1, v2, v0}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeInterface;->cr_secure_touch_mode_feature_disable_squid_touch_driver_result(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;Lcom/squareup/cardreader/lcr/CrsStmDisableSquidTouchDriverResult;)V

    return-void
.end method

.method public synthetic lambda$onInitiateSecureTouch$4$SecureTouchFeatureLegacy()V
    .locals 2

    .line 303
    iget-object v0, p0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy;->nativeFeature:Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeInterface;

    iget-object v1, p0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy;->secureTouch:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeInterface;->cr_secure_touch_mode_feature_start_secure_touch(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;)V

    return-void
.end method

.method public synthetic lambda$onSecureTouchKeypadActive$5$SecureTouchFeatureLegacy(Lcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;)V
    .locals 2

    .line 332
    iget-object v0, p0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy;->nativeFeature:Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeInterface;

    iget-object v1, p0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy;->secureTouch:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;

    invoke-interface {v0, v1, p1}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeInterface;->cr_secure_touch_mode_feature_regular_set_button_location(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;Lcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;)V

    return-void
.end method

.method public synthetic lambda$onSecureTouchKeypadActive$6$SecureTouchFeatureLegacy(Lcom/squareup/cardreader/lcr/CrsStmAccessibilityPinPadConfig;)V
    .locals 2

    .line 349
    iget-object v0, p0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy;->nativeFeature:Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeInterface;

    iget-object v1, p0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy;->secureTouch:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;

    invoke-interface {v0, v1, p1}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeInterface;->cr_secure_touch_mode_feature_set_accessibility_configs(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;Lcom/squareup/cardreader/lcr/CrsStmAccessibilityPinPadConfig;)V

    return-void
.end method

.method public synthetic lambda$onSecureTouchKeypadActive$7$SecureTouchFeatureLegacy(Lcom/squareup/cardreader/lcr/CrsStmPinPadConfigType;)V
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "SecureTouchFeature: Calling LCR sent_pinpad_configs"

    .line 362
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 363
    iget-object v0, p0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy;->nativeFeature:Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeInterface;

    iget-object v1, p0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy;->secureTouch:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;

    invoke-interface {v0, v1, p1}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeInterface;->cr_secure_touch_mode_feature_sent_pinpad_configs(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;Lcom/squareup/cardreader/lcr/CrsStmPinPadConfigType;)V

    return-void
.end method

.method public onSecureTouchAccessibilityPinPadCenter(III)V
    .locals 3

    .line 239
    invoke-static {p3}, Lcom/squareup/cardreader/lcr/CrsStmAccessibilityPinPadType;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrsStmAccessibilityPinPadType;

    move-result-object p3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p3, v0, v1

    const-string v1, "SecureTouchFeature: SPE says onSecureTouchAccessibilityPinPadCenter: %s"

    .line 240
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 243
    sget-object v0, Lcom/squareup/cardreader/lcr/CrsStmAccessibilityPinPadType;->CRS_STM_ACCESSIBILITY_PIN_PAD_1_9:Lcom/squareup/cardreader/lcr/CrsStmAccessibilityPinPadType;

    if-ne p3, v0, :cond_0

    .line 244
    sget-object p3, Lcom/squareup/securetouch/AccessibilityKeypadType;->KeypadOneThroughNine:Lcom/squareup/securetouch/AccessibilityKeypadType;

    goto :goto_0

    .line 246
    :cond_0
    sget-object p3, Lcom/squareup/securetouch/AccessibilityKeypadType;->KeypadZero:Lcom/squareup/securetouch/AccessibilityKeypadType;

    .line 249
    :goto_0
    iget-object v0, p0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy;->listener:Lcom/squareup/securetouch/SecureTouchFeatureNativeListener;

    new-instance v1, Lcom/squareup/securetouch/AccessibilityKeypadCenter;

    new-instance v2, Lcom/squareup/securetouch/SecureTouchPoint;

    invoke-direct {v2, p1, p2}, Lcom/squareup/securetouch/SecureTouchPoint;-><init>(II)V

    invoke-direct {v1, v2, p3}, Lcom/squareup/securetouch/AccessibilityKeypadCenter;-><init>(Lcom/squareup/securetouch/SecureTouchPoint;Lcom/squareup/securetouch/AccessibilityKeypadType;)V

    invoke-interface {v0, v1}, Lcom/squareup/securetouch/SecureTouchFeatureNativeListener;->secureTouchFeatureEvent(Lcom/squareup/securetouch/SecureTouchFeatureEvent;)V

    return-void
.end method

.method public onSecureTouchApplicationEvent(Lcom/squareup/securetouch/SecureTouchApplicationEvent;)V
    .locals 3

    .line 159
    instance-of v0, p1, Lcom/squareup/securetouch/SecureTouchKeypadActive;

    if-eqz v0, :cond_0

    .line 160
    check-cast p1, Lcom/squareup/securetouch/SecureTouchKeypadActive;

    invoke-direct {p0, p1}, Lcom/squareup/cardreader/SecureTouchFeatureLegacy;->onSecureTouchKeypadActive(Lcom/squareup/securetouch/SecureTouchKeypadActive;)V

    goto :goto_0

    .line 161
    :cond_0
    instance-of v0, p1, Lcom/squareup/securetouch/InitiateSecureTouch;

    if-eqz v0, :cond_1

    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Object;

    const-string v0, "InitiateSecureTouch"

    .line 162
    invoke-static {v0, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 163
    invoke-direct {p0}, Lcom/squareup/cardreader/SecureTouchFeatureLegacy;->onInitiateSecureTouch()V

    :goto_0
    return-void

    .line 165
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SecureTouchFeature: No handler for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onSecureTouchKeepaliveFailed(I)V
    .locals 1

    .line 270
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Secure Touch native keepalive failed. Let us all die now."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public onSecureTouchPinPadEvent(I)V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 276
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "SecureTouchFeature: SPE says pin pad event occurred: %s"

    .line 275
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 279
    :try_start_0
    iget-object v0, p0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy;->listener:Lcom/squareup/securetouch/SecureTouchFeatureNativeListener;

    .line 280
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/cardreader/SecureTouchFeatureLegacy;->pinPadEventToKeyPressEvent(Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;)Lcom/squareup/securetouch/SecureTouchFeatureEvent;

    move-result-object p1

    .line 279
    invoke-interface {v0, p1}, Lcom/squareup/securetouch/SecureTouchFeatureNativeListener;->secureTouchFeatureEvent(Lcom/squareup/securetouch/SecureTouchFeatureEvent;)V
    :try_end_0
    .catch Lcom/squareup/cardreader/UnhandledPinPadEvent; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 282
    invoke-virtual {p1}, Lcom/squareup/cardreader/UnhandledPinPadEvent;->getMessage()Ljava/lang/String;

    move-result-object p1

    new-array v0, v2, [Ljava/lang/Object;

    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public reset()V
    .locals 1

    .line 154
    iget-object v0, p0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy;->subs:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    return-void
.end method

.method public showSecureTouchPinPad(Lcom/squareup/cardreader/CardInfo;I)V
    .locals 3

    .line 212
    invoke-static {p2}, Lcom/squareup/cardreader/lcr/CrSecureTouchPinTry;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrSecureTouchPinTry;

    move-result-object p1

    const/4 p2, 0x1

    new-array v0, p2, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v2, "SecureTouchFeature: SPE says showSecureTouchPinPad: %s"

    .line 213
    invoke-static {v2, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 216
    sget-object v0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy$1;->$SwitchMap$com$squareup$cardreader$lcr$CrSecureTouchPinTry:[I

    invoke-virtual {p1}, Lcom/squareup/cardreader/lcr/CrSecureTouchPinTry;->ordinal()I

    move-result v2

    aget v0, v0, v2

    if-eq v0, p2, :cond_3

    const/4 p2, 0x2

    if-eq v0, p2, :cond_2

    const/4 p2, 0x3

    if-eq v0, p2, :cond_1

    const/4 p2, 0x4

    if-ne v0, p2, :cond_0

    .line 228
    sget-object p1, Lcom/squareup/securetouch/PinLastTry;->INSTANCE:Lcom/squareup/securetouch/PinLastTry;

    goto :goto_0

    .line 231
    :cond_0
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown value: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2

    .line 225
    :cond_1
    sget-object p1, Lcom/squareup/securetouch/PinRetry;->INSTANCE:Lcom/squareup/securetouch/PinRetry;

    goto :goto_0

    .line 222
    :cond_2
    sget-object p1, Lcom/squareup/securetouch/PinFirstTry;->INSTANCE:Lcom/squareup/securetouch/PinFirstTry;

    goto :goto_0

    :cond_3
    new-array p1, v1, [Ljava/lang/Object;

    const-string p2, "Received pin try NONE. Seems strange but non-fatal. Treating it as \'first try\'."

    .line 218
    invoke-static {p2, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 219
    sget-object p1, Lcom/squareup/securetouch/PinFirstTry;->INSTANCE:Lcom/squareup/securetouch/PinFirstTry;

    .line 234
    :goto_0
    iget-object p2, p0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy;->listener:Lcom/squareup/securetouch/SecureTouchFeatureNativeListener;

    new-instance v0, Lcom/squareup/securetouch/SecureTouchPinRequestData;

    invoke-direct {v0, p1}, Lcom/squareup/securetouch/SecureTouchPinRequestData;-><init>(Lcom/squareup/securetouch/SecureTouchPinEntryState;)V

    invoke-interface {p2, v0}, Lcom/squareup/securetouch/SecureTouchFeatureNativeListener;->hardwarePinRequested(Lcom/squareup/securetouch/SecureTouchPinRequestData;)V

    return-void
.end method
