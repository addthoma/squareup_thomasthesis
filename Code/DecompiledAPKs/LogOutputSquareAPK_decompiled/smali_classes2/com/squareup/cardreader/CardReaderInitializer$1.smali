.class synthetic Lcom/squareup/cardreader/CardReaderInitializer$1;
.super Ljava/lang/Object;
.source "CardReaderInitializer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/CardReaderInitializer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$cardreader$lcr$CrCommsVersionResult:[I

.field static final synthetic $SwitchMap$com$squareup$cardreader$lcr$CrTamperStatus:[I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 330
    invoke-static {}, Lcom/squareup/cardreader/lcr/CrTamperStatus;->values()[Lcom/squareup/cardreader/lcr/CrTamperStatus;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/cardreader/CardReaderInitializer$1;->$SwitchMap$com$squareup$cardreader$lcr$CrTamperStatus:[I

    const/4 v0, 0x1

    :try_start_0
    sget-object v1, Lcom/squareup/cardreader/CardReaderInitializer$1;->$SwitchMap$com$squareup$cardreader$lcr$CrTamperStatus:[I

    sget-object v2, Lcom/squareup/cardreader/lcr/CrTamperStatus;->CR_TAMPER_STATUS_FLAGGED:Lcom/squareup/cardreader/lcr/CrTamperStatus;

    invoke-virtual {v2}, Lcom/squareup/cardreader/lcr/CrTamperStatus;->ordinal()I

    move-result v2

    aput v0, v1, v2
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 v1, 0x2

    :try_start_1
    sget-object v2, Lcom/squareup/cardreader/CardReaderInitializer$1;->$SwitchMap$com$squareup$cardreader$lcr$CrTamperStatus:[I

    sget-object v3, Lcom/squareup/cardreader/lcr/CrTamperStatus;->CR_TAMPER_STATUS_NORMAL:Lcom/squareup/cardreader/lcr/CrTamperStatus;

    invoke-virtual {v3}, Lcom/squareup/cardreader/lcr/CrTamperStatus;->ordinal()I

    move-result v3

    aput v1, v2, v3
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    const/4 v2, 0x3

    :try_start_2
    sget-object v3, Lcom/squareup/cardreader/CardReaderInitializer$1;->$SwitchMap$com$squareup$cardreader$lcr$CrTamperStatus:[I

    sget-object v4, Lcom/squareup/cardreader/lcr/CrTamperStatus;->CR_TAMPER_STATUS_TAMPERED:Lcom/squareup/cardreader/lcr/CrTamperStatus;

    invoke-virtual {v4}, Lcom/squareup/cardreader/lcr/CrTamperStatus;->ordinal()I

    move-result v4

    aput v2, v3, v4
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    :try_start_3
    sget-object v3, Lcom/squareup/cardreader/CardReaderInitializer$1;->$SwitchMap$com$squareup$cardreader$lcr$CrTamperStatus:[I

    sget-object v4, Lcom/squareup/cardreader/lcr/CrTamperStatus;->CR_TAMPER_STATUS_UNKNOWN:Lcom/squareup/cardreader/lcr/CrTamperStatus;

    invoke-virtual {v4}, Lcom/squareup/cardreader/lcr/CrTamperStatus;->ordinal()I

    move-result v4

    const/4 v5, 0x4

    aput v5, v3, v4
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    .line 211
    :catch_3
    invoke-static {}, Lcom/squareup/cardreader/lcr/CrCommsVersionResult;->values()[Lcom/squareup/cardreader/lcr/CrCommsVersionResult;

    move-result-object v3

    array-length v3, v3

    new-array v3, v3, [I

    sput-object v3, Lcom/squareup/cardreader/CardReaderInitializer$1;->$SwitchMap$com$squareup$cardreader$lcr$CrCommsVersionResult:[I

    :try_start_4
    sget-object v3, Lcom/squareup/cardreader/CardReaderInitializer$1;->$SwitchMap$com$squareup$cardreader$lcr$CrCommsVersionResult:[I

    sget-object v4, Lcom/squareup/cardreader/lcr/CrCommsVersionResult;->CR_CARDREADER_COMMS_VERSION_RESULT_OK:Lcom/squareup/cardreader/lcr/CrCommsVersionResult;

    invoke-virtual {v4}, Lcom/squareup/cardreader/lcr/CrCommsVersionResult;->ordinal()I

    move-result v4

    aput v0, v3, v4
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :catch_4
    :try_start_5
    sget-object v0, Lcom/squareup/cardreader/CardReaderInitializer$1;->$SwitchMap$com$squareup$cardreader$lcr$CrCommsVersionResult:[I

    sget-object v3, Lcom/squareup/cardreader/lcr/CrCommsVersionResult;->CR_CARDREADER_COMMS_VERSION_RESULT_FIRMWARE_UPDATE_REQUIRED:Lcom/squareup/cardreader/lcr/CrCommsVersionResult;

    invoke-virtual {v3}, Lcom/squareup/cardreader/lcr/CrCommsVersionResult;->ordinal()I

    move-result v3

    aput v1, v0, v3
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_5

    :catch_5
    :try_start_6
    sget-object v0, Lcom/squareup/cardreader/CardReaderInitializer$1;->$SwitchMap$com$squareup$cardreader$lcr$CrCommsVersionResult:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrCommsVersionResult;->CR_CARDREADER_COMMS_VERSION_RESULT_CARDREADER_UPDATE_REQUIRED:Lcom/squareup/cardreader/lcr/CrCommsVersionResult;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrCommsVersionResult;->ordinal()I

    move-result v1

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_6

    :catch_6
    return-void
.end method
