.class public Lcom/squareup/cardreader/lcr/CrsStmDisableSquidTouchDriverResult;
.super Ljava/lang/Object;
.source "CrsStmDisableSquidTouchDriverResult.java"


# instance fields
.field protected swigCMemOwn:Z

.field private swigCPtr:J


# direct methods
.method public constructor <init>()V
    .locals 3

    .line 47
    invoke-static {}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;->new_CrsStmDisableSquidTouchDriverResult()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/squareup/cardreader/lcr/CrsStmDisableSquidTouchDriverResult;-><init>(JZ)V

    return-void
.end method

.method protected constructor <init>(JZ)V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-boolean p3, p0, Lcom/squareup/cardreader/lcr/CrsStmDisableSquidTouchDriverResult;->swigCMemOwn:Z

    .line 17
    iput-wide p1, p0, Lcom/squareup/cardreader/lcr/CrsStmDisableSquidTouchDriverResult;->swigCPtr:J

    return-void
.end method

.method protected static getCPtr(Lcom/squareup/cardreader/lcr/CrsStmDisableSquidTouchDriverResult;)J
    .locals 2

    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    goto :goto_0

    .line 21
    :cond_0
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/CrsStmDisableSquidTouchDriverResult;->swigCPtr:J

    :goto_0
    return-wide v0
.end method


# virtual methods
.method public declared-synchronized delete()V
    .locals 5

    monitor-enter p0

    .line 29
    :try_start_0
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/CrsStmDisableSquidTouchDriverResult;->swigCPtr:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_1

    .line 30
    iget-boolean v0, p0, Lcom/squareup/cardreader/lcr/CrsStmDisableSquidTouchDriverResult;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 31
    iput-boolean v0, p0, Lcom/squareup/cardreader/lcr/CrsStmDisableSquidTouchDriverResult;->swigCMemOwn:Z

    .line 32
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/CrsStmDisableSquidTouchDriverResult;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;->delete_CrsStmDisableSquidTouchDriverResult(J)V

    .line 34
    :cond_0
    iput-wide v2, p0, Lcom/squareup/cardreader/lcr/CrsStmDisableSquidTouchDriverResult;->swigCPtr:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 36
    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected finalize()V
    .locals 0

    .line 25
    invoke-virtual {p0}, Lcom/squareup/cardreader/lcr/CrsStmDisableSquidTouchDriverResult;->delete()V

    return-void
.end method

.method public getError_code()S
    .locals 2

    .line 43
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/CrsStmDisableSquidTouchDriverResult;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;->CrsStmDisableSquidTouchDriverResult_error_code_get(JLcom/squareup/cardreader/lcr/CrsStmDisableSquidTouchDriverResult;)S

    move-result v0

    return v0
.end method

.method public setError_code(S)V
    .locals 2

    .line 39
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/CrsStmDisableSquidTouchDriverResult;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;->CrsStmDisableSquidTouchDriverResult_error_code_set(JLcom/squareup/cardreader/lcr/CrsStmDisableSquidTouchDriverResult;S)V

    return-void
.end method
