.class public Lcom/squareup/cardreader/lcr/AudioBackendNativeJNI;
.super Ljava/lang/Object;
.source "AudioBackendNativeJNI.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final native cr_comms_backend_audio_alloc()J
.end method

.method public static final native cr_comms_backend_audio_enable_tx_for_connection(J)V
.end method

.method public static final native cr_comms_backend_audio_free(J)J
.end method

.method public static final native cr_comms_backend_audio_notify_phy_tx_complete(J)V
.end method

.method public static final native cr_comms_backend_audio_shutdown(J)V
.end method

.method public static final native decode_r4_packet(JI[S)Ljava/lang/Object;
.end method

.method public static final native feed_audio_samples(JLjava/lang/Object;II)V
.end method

.method public static final native initialize_backend_audio(IIJJLjava/lang/Object;)J
.end method

.method public static final native set_legacy_reader_type(JI)V
.end method
