.class public final enum Lcom/squareup/cardreader/lcr/CrsTmnRequestType;
.super Ljava/lang/Enum;
.source "CrsTmnRequestType.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/lcr/CrsTmnRequestType$SwigNext;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/cardreader/lcr/CrsTmnRequestType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

.field public static final enum TMN_REQUEST_CANCELLATION:Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

.field public static final enum TMN_REQUEST_CHECK_BALANCE:Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

.field public static final enum TMN_REQUEST_CHECK_HISTORY:Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

.field public static final enum TMN_REQUEST_CHECK_RESULT:Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

.field public static final enum TMN_REQUEST_ONLINE_TEST:Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

.field public static final enum TMN_REQUEST_TRANSACTION:Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

.field public static final enum TMN_REQUEST_VOID_UNKNOWN:Lcom/squareup/cardreader/lcr/CrsTmnRequestType;


# instance fields
.field private final swigValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .line 12
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "TMN_REQUEST_TRANSACTION"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/cardreader/lcr/CrsTmnRequestType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsTmnRequestType;->TMN_REQUEST_TRANSACTION:Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    .line 13
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    const/4 v3, 0x3

    const-string v4, "TMN_REQUEST_CHECK_BALANCE"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/cardreader/lcr/CrsTmnRequestType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsTmnRequestType;->TMN_REQUEST_CHECK_BALANCE:Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    .line 14
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    const/4 v4, 0x2

    const/4 v5, 0x4

    const-string v6, "TMN_REQUEST_CANCELLATION"

    invoke-direct {v0, v6, v4, v5}, Lcom/squareup/cardreader/lcr/CrsTmnRequestType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsTmnRequestType;->TMN_REQUEST_CANCELLATION:Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    .line 15
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    const/4 v6, 0x5

    const-string v7, "TMN_REQUEST_CHECK_HISTORY"

    invoke-direct {v0, v7, v3, v6}, Lcom/squareup/cardreader/lcr/CrsTmnRequestType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsTmnRequestType;->TMN_REQUEST_CHECK_HISTORY:Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    .line 16
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    const-string v7, "TMN_REQUEST_ONLINE_TEST"

    const/16 v8, 0x600

    invoke-direct {v0, v7, v5, v8}, Lcom/squareup/cardreader/lcr/CrsTmnRequestType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsTmnRequestType;->TMN_REQUEST_ONLINE_TEST:Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    .line 17
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    const-string v7, "TMN_REQUEST_CHECK_RESULT"

    const/16 v8, 0x200

    invoke-direct {v0, v7, v6, v8}, Lcom/squareup/cardreader/lcr/CrsTmnRequestType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsTmnRequestType;->TMN_REQUEST_CHECK_RESULT:Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    .line 18
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    const/4 v7, 0x6

    const-string v8, "TMN_REQUEST_VOID_UNKNOWN"

    const/16 v9, 0x9

    invoke-direct {v0, v8, v7, v9}, Lcom/squareup/cardreader/lcr/CrsTmnRequestType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsTmnRequestType;->TMN_REQUEST_VOID_UNKNOWN:Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    .line 11
    sget-object v8, Lcom/squareup/cardreader/lcr/CrsTmnRequestType;->TMN_REQUEST_TRANSACTION:Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    aput-object v8, v0, v1

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnRequestType;->TMN_REQUEST_CHECK_BALANCE:Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnRequestType;->TMN_REQUEST_CANCELLATION:Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnRequestType;->TMN_REQUEST_CHECK_HISTORY:Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnRequestType;->TMN_REQUEST_ONLINE_TEST:Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnRequestType;->TMN_REQUEST_CHECK_RESULT:Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnRequestType;->TMN_REQUEST_VOID_UNKNOWN:Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    aput-object v1, v0, v7

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsTmnRequestType;->$VALUES:[Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 35
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 36
    invoke-static {}, Lcom/squareup/cardreader/lcr/CrsTmnRequestType$SwigNext;->access$008()I

    move-result p1

    iput p1, p0, Lcom/squareup/cardreader/lcr/CrsTmnRequestType;->swigValue:I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 40
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 41
    iput p3, p0, Lcom/squareup/cardreader/lcr/CrsTmnRequestType;->swigValue:I

    add-int/lit8 p3, p3, 0x1

    .line 42
    invoke-static {p3}, Lcom/squareup/cardreader/lcr/CrsTmnRequestType$SwigNext;->access$002(I)I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/squareup/cardreader/lcr/CrsTmnRequestType;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/lcr/CrsTmnRequestType;",
            ")V"
        }
    .end annotation

    .line 46
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 47
    iget p1, p3, Lcom/squareup/cardreader/lcr/CrsTmnRequestType;->swigValue:I

    iput p1, p0, Lcom/squareup/cardreader/lcr/CrsTmnRequestType;->swigValue:I

    .line 48
    iget p1, p0, Lcom/squareup/cardreader/lcr/CrsTmnRequestType;->swigValue:I

    add-int/lit8 p1, p1, 0x1

    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CrsTmnRequestType$SwigNext;->access$002(I)I

    return-void
.end method

.method public static swigToEnum(I)Lcom/squareup/cardreader/lcr/CrsTmnRequestType;
    .locals 6

    .line 25
    const-class v0, Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    .line 26
    array-length v2, v1

    if-ge p0, v2, :cond_0

    if-ltz p0, :cond_0

    aget-object v2, v1, p0

    iget v2, v2, Lcom/squareup/cardreader/lcr/CrsTmnRequestType;->swigValue:I

    if-ne v2, p0, :cond_0

    .line 27
    aget-object p0, v1, p0

    return-object p0

    .line 28
    :cond_0
    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_2

    aget-object v4, v1, v3

    .line 29
    iget v5, v4, Lcom/squareup/cardreader/lcr/CrsTmnRequestType;->swigValue:I

    if-ne v5, p0, :cond_1

    return-object v4

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 31
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "No enum "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, " with value "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/lcr/CrsTmnRequestType;
    .locals 1

    .line 11
    const-class v0, Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/cardreader/lcr/CrsTmnRequestType;
    .locals 1

    .line 11
    sget-object v0, Lcom/squareup/cardreader/lcr/CrsTmnRequestType;->$VALUES:[Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    invoke-virtual {v0}, [Lcom/squareup/cardreader/lcr/CrsTmnRequestType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    return-object v0
.end method


# virtual methods
.method public final swigValue()I
    .locals 1

    .line 21
    iget v0, p0, Lcom/squareup/cardreader/lcr/CrsTmnRequestType;->swigValue:I

    return v0
.end method
