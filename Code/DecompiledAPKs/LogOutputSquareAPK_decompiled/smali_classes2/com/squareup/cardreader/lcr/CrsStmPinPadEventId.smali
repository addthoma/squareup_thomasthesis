.class public final enum Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;
.super Ljava/lang/Enum;
.source "CrsStmPinPadEventId.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId$SwigNext;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

.field public static final enum CRS_STM_EVENT_ACCESS_OUT_OF_BOUNDS:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

.field public static final enum CRS_STM_EVENT_BUTTON_0:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

.field public static final enum CRS_STM_EVENT_BUTTON_1:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

.field public static final enum CRS_STM_EVENT_BUTTON_2:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

.field public static final enum CRS_STM_EVENT_BUTTON_3:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

.field public static final enum CRS_STM_EVENT_BUTTON_4:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

.field public static final enum CRS_STM_EVENT_BUTTON_5:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

.field public static final enum CRS_STM_EVENT_BUTTON_6:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

.field public static final enum CRS_STM_EVENT_BUTTON_7:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

.field public static final enum CRS_STM_EVENT_BUTTON_8:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

.field public static final enum CRS_STM_EVENT_BUTTON_9:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

.field public static final enum CRS_STM_EVENT_BUTTON_ACCESSIBILITY:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

.field public static final enum CRS_STM_EVENT_BUTTON_BACKSPACE:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

.field public static final enum CRS_STM_EVENT_BUTTON_CANCEL:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

.field public static final enum CRS_STM_EVENT_BUTTON_CLEAR:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

.field public static final enum CRS_STM_EVENT_BUTTON_DONE:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

.field public static final enum CRS_STM_EVENT_BUTTON_GENERIC_NUMBER:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

.field public static final enum CRS_STM_EVENT_BUTTON_ID_MAX:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

.field public static final enum CRS_STM_EVENT_BUTTON_INVALID:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

.field public static final enum CRS_STM_EVENT_ID_MAX:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

.field public static final enum CRS_STM_EVENT_IGNORE_ME:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

.field public static final enum CRS_STM_EVENT_SWIPE_CANCEL:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

.field public static final enum CRS_STM_EVENT_TOO_FEW_DIGITS:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

.field public static final enum CRS_STM_EVENT_TOO_MANY_DIGITS:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

.field public static final enum CRS_STM_EVENT_TOO_MANY_TOUCHES:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

.field public static final enum CRS_STM_EVENT_TOUCH_DOWN:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;


# instance fields
.field private final swigValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 12
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    const/4 v1, 0x0

    const-string v2, "CRS_STM_EVENT_BUTTON_0"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_BUTTON_0:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    .line 13
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    const/4 v2, 0x1

    const-string v3, "CRS_STM_EVENT_BUTTON_1"

    invoke-direct {v0, v3, v2}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_BUTTON_1:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    .line 14
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    const/4 v3, 0x2

    const-string v4, "CRS_STM_EVENT_BUTTON_2"

    invoke-direct {v0, v4, v3}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_BUTTON_2:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    .line 15
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    const/4 v4, 0x3

    const-string v5, "CRS_STM_EVENT_BUTTON_3"

    invoke-direct {v0, v5, v4}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_BUTTON_3:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    .line 16
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    const/4 v5, 0x4

    const-string v6, "CRS_STM_EVENT_BUTTON_4"

    invoke-direct {v0, v6, v5}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_BUTTON_4:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    .line 17
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    const/4 v6, 0x5

    const-string v7, "CRS_STM_EVENT_BUTTON_5"

    invoke-direct {v0, v7, v6}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_BUTTON_5:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    .line 18
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    const/4 v7, 0x6

    const-string v8, "CRS_STM_EVENT_BUTTON_6"

    invoke-direct {v0, v8, v7}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_BUTTON_6:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    .line 19
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    const/4 v8, 0x7

    const-string v9, "CRS_STM_EVENT_BUTTON_7"

    invoke-direct {v0, v9, v8}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_BUTTON_7:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    .line 20
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    const/16 v9, 0x8

    const-string v10, "CRS_STM_EVENT_BUTTON_8"

    invoke-direct {v0, v10, v9}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_BUTTON_8:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    .line 21
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    const/16 v10, 0x9

    const-string v11, "CRS_STM_EVENT_BUTTON_9"

    invoke-direct {v0, v11, v10}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_BUTTON_9:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    .line 22
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    const/16 v11, 0xa

    const-string v12, "CRS_STM_EVENT_BUTTON_DONE"

    invoke-direct {v0, v12, v11}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_BUTTON_DONE:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    .line 23
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    const/16 v12, 0xb

    const-string v13, "CRS_STM_EVENT_BUTTON_CANCEL"

    invoke-direct {v0, v13, v12}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_BUTTON_CANCEL:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    .line 24
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    const/16 v13, 0xc

    const-string v14, "CRS_STM_EVENT_BUTTON_CLEAR"

    invoke-direct {v0, v14, v13}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_BUTTON_CLEAR:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    .line 25
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    const/16 v14, 0xd

    const-string v15, "CRS_STM_EVENT_BUTTON_BACKSPACE"

    invoke-direct {v0, v15, v14}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_BUTTON_BACKSPACE:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    .line 26
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    const/16 v15, 0xe

    const-string v14, "CRS_STM_EVENT_BUTTON_ACCESSIBILITY"

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_BUTTON_ACCESSIBILITY:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    .line 27
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    const-string v14, "CRS_STM_EVENT_BUTTON_ID_MAX"

    const/16 v15, 0xf

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_BUTTON_ID_MAX:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    .line 28
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    const-string v14, "CRS_STM_EVENT_BUTTON_GENERIC_NUMBER"

    const/16 v15, 0x10

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_BUTTON_GENERIC_NUMBER:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    .line 29
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    const-string v14, "CRS_STM_EVENT_BUTTON_INVALID"

    const/16 v15, 0x11

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_BUTTON_INVALID:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    .line 30
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    const-string v14, "CRS_STM_EVENT_TOUCH_DOWN"

    const/16 v15, 0x12

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_TOUCH_DOWN:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    .line 31
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    const-string v14, "CRS_STM_EVENT_ACCESS_OUT_OF_BOUNDS"

    const/16 v15, 0x13

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_ACCESS_OUT_OF_BOUNDS:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    .line 32
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    const-string v14, "CRS_STM_EVENT_TOO_MANY_TOUCHES"

    const/16 v15, 0x14

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_TOO_MANY_TOUCHES:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    .line 33
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    const-string v14, "CRS_STM_EVENT_IGNORE_ME"

    const/16 v15, 0x15

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_IGNORE_ME:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    .line 34
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    const-string v14, "CRS_STM_EVENT_TOO_MANY_DIGITS"

    const/16 v15, 0x16

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_TOO_MANY_DIGITS:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    .line 35
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    const-string v14, "CRS_STM_EVENT_SWIPE_CANCEL"

    const/16 v15, 0x17

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_SWIPE_CANCEL:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    .line 36
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    const-string v14, "CRS_STM_EVENT_TOO_FEW_DIGITS"

    const/16 v15, 0x18

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_TOO_FEW_DIGITS:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    .line 37
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    const-string v14, "CRS_STM_EVENT_ID_MAX"

    const/16 v15, 0x19

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_ID_MAX:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    const/16 v0, 0x1a

    new-array v0, v0, [Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    .line 11
    sget-object v14, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_BUTTON_0:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    aput-object v14, v0, v1

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_BUTTON_1:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_BUTTON_2:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_BUTTON_3:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_BUTTON_4:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_BUTTON_5:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_BUTTON_6:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_BUTTON_7:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_BUTTON_8:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_BUTTON_9:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_BUTTON_DONE:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_BUTTON_CANCEL:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_BUTTON_CLEAR:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    aput-object v1, v0, v13

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_BUTTON_BACKSPACE:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_BUTTON_ACCESSIBILITY:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_BUTTON_ID_MAX:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_BUTTON_GENERIC_NUMBER:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_BUTTON_INVALID:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_TOUCH_DOWN:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_ACCESS_OUT_OF_BOUNDS:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_TOO_MANY_TOUCHES:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_IGNORE_ME:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_TOO_MANY_DIGITS:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_SWIPE_CANCEL:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_TOO_FEW_DIGITS:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_ID_MAX:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->$VALUES:[Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 54
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 55
    invoke-static {}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId$SwigNext;->access$008()I

    move-result p1

    iput p1, p0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->swigValue:I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 59
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 60
    iput p3, p0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->swigValue:I

    add-int/lit8 p3, p3, 0x1

    .line 61
    invoke-static {p3}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId$SwigNext;->access$002(I)I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/squareup/cardreader/lcr/CrsStmPinPadEventId;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;",
            ")V"
        }
    .end annotation

    .line 65
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 66
    iget p1, p3, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->swigValue:I

    iput p1, p0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->swigValue:I

    .line 67
    iget p1, p0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->swigValue:I

    add-int/lit8 p1, p1, 0x1

    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId$SwigNext;->access$002(I)I

    return-void
.end method

.method public static swigToEnum(I)Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;
    .locals 6

    .line 44
    const-class v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    .line 45
    array-length v2, v1

    if-ge p0, v2, :cond_0

    if-ltz p0, :cond_0

    aget-object v2, v1, p0

    iget v2, v2, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->swigValue:I

    if-ne v2, p0, :cond_0

    .line 46
    aget-object p0, v1, p0

    return-object p0

    .line 47
    :cond_0
    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_2

    aget-object v4, v1, v3

    .line 48
    iget v5, v4, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->swigValue:I

    if-ne v5, p0, :cond_1

    return-object v4

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 50
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "No enum "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, " with value "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;
    .locals 1

    .line 11
    const-class v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    return-object p0
.end method

.method public static values()[Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;
    .locals 1

    .line 11
    sget-object v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->$VALUES:[Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    invoke-virtual {v0}, [Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    return-object v0
.end method


# virtual methods
.method public final swigValue()I
    .locals 1

    .line 40
    iget v0, p0, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->swigValue:I

    return v0
.end method
