.class public Lcom/squareup/cardreader/lcr/TimerNativePassthrough;
.super Ljava/lang/Object;
.source "TimerNativePassthrough.java"

# interfaces
.implements Lcom/squareup/cardreader/lcr/TimerNativeInterface;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public initialize_timer_api(Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_crs_timer_api_t;
    .locals 0

    .line 9
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/TimerNative;->initialize_timer_api(Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_crs_timer_api_t;

    move-result-object p1

    return-object p1
.end method

.method public on_timer_expired(JJJ)V
    .locals 0

    .line 14
    invoke-static/range {p1 .. p6}, Lcom/squareup/cardreader/lcr/TimerNative;->on_timer_expired(JJJ)V

    return-void
.end method
