.class public final enum Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;
.super Ljava/lang/Enum;
.source "CrPaymentStandardMessage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage$SwigNext;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_AMOUNT:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_AMOUNT_OK:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_APPROVED:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_APPROVED_PLS_SIGN:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_AUTHORISING_PLS_WAIT:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_CALL_YOUR_BANK:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_CANCEL_OR_ENTER:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_CARD_ERROR:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_CARD_READ_OK_PLS_REMOVE_CARD:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_DECLINED:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_ENTER_AMOUNT:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_ENTER_PIN:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_INCORRECT_PIN:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_INSERT_CARD:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_INSERT_SWIPE_OR_TRY_ANOTHER_CARD:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_NONE:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_NOT_ACCEPTED:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_NO_MSG:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_PIN_OK:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_PLEASE_WAIT:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_PLS_INSERT_CARD:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_PLS_INSERT_OR_SWIPE_CARD:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_PLS_PRESENT_ONE_CARD:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_PRESENT_CARD:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_PRESENT_CARD_AGAIN:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_PROCESSING:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_PROCESSING_ERROR:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_REMOVE_CARD:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_SEE_PHONE_FOR_INSTRUCTIONS:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_TOO_MANY_TAP:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_TRY_AGAIN:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_UNLOCK_PHONE_TO_PAY:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_USE_CHIP_READER:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_USE_MAG_STRIP:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

.field public static final enum CR_PAYMENT_STD_MSG_WELCOME:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;


# instance fields
.field private final swigValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 12
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const/4 v1, 0x0

    const-string v2, "CR_PAYMENT_STD_MSG_NONE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_NONE:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    .line 13
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const/4 v2, 0x1

    const-string v3, "CR_PAYMENT_STD_MSG_AMOUNT"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_AMOUNT:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    .line 14
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const/4 v3, 0x2

    const-string v4, "CR_PAYMENT_STD_MSG_AMOUNT_OK"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_AMOUNT_OK:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    .line 15
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const/4 v4, 0x3

    const-string v5, "CR_PAYMENT_STD_MSG_APPROVED"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_APPROVED:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    .line 16
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const/4 v5, 0x4

    const-string v6, "CR_PAYMENT_STD_MSG_CALL_YOUR_BANK"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_CALL_YOUR_BANK:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    .line 17
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const/4 v6, 0x5

    const-string v7, "CR_PAYMENT_STD_MSG_CANCEL_OR_ENTER"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_CANCEL_OR_ENTER:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    .line 18
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const/4 v7, 0x6

    const-string v8, "CR_PAYMENT_STD_MSG_CARD_ERROR"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_CARD_ERROR:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    .line 19
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const/4 v8, 0x7

    const-string v9, "CR_PAYMENT_STD_MSG_DECLINED"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_DECLINED:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    .line 20
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const/16 v9, 0x8

    const-string v10, "CR_PAYMENT_STD_MSG_ENTER_AMOUNT"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_ENTER_AMOUNT:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    .line 21
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const/16 v10, 0x9

    const-string v11, "CR_PAYMENT_STD_MSG_ENTER_PIN"

    invoke-direct {v0, v11, v10, v10}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_ENTER_PIN:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    .line 22
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const/16 v11, 0xa

    const-string v12, "CR_PAYMENT_STD_MSG_INCORRECT_PIN"

    invoke-direct {v0, v12, v11, v11}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_INCORRECT_PIN:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    .line 23
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const/16 v12, 0xb

    const-string v13, "CR_PAYMENT_STD_MSG_INSERT_CARD"

    invoke-direct {v0, v13, v12, v12}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_INSERT_CARD:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    .line 24
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const/16 v13, 0xc

    const-string v14, "CR_PAYMENT_STD_MSG_NOT_ACCEPTED"

    invoke-direct {v0, v14, v13, v13}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_NOT_ACCEPTED:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    .line 25
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const/16 v14, 0xd

    const-string v15, "CR_PAYMENT_STD_MSG_PIN_OK"

    invoke-direct {v0, v15, v14, v14}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_PIN_OK:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    .line 26
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const/16 v15, 0xe

    const-string v14, "CR_PAYMENT_STD_MSG_PLEASE_WAIT"

    invoke-direct {v0, v14, v15, v15}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_PLEASE_WAIT:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    .line 27
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const-string v14, "CR_PAYMENT_STD_MSG_PROCESSING_ERROR"

    const/16 v15, 0xf

    const/16 v13, 0xf

    invoke-direct {v0, v14, v15, v13}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_PROCESSING_ERROR:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    .line 28
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const-string v13, "CR_PAYMENT_STD_MSG_REMOVE_CARD"

    const/16 v14, 0x10

    const/16 v15, 0x10

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_REMOVE_CARD:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    .line 29
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const-string v13, "CR_PAYMENT_STD_MSG_USE_CHIP_READER"

    const/16 v14, 0x11

    const/16 v15, 0x11

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_USE_CHIP_READER:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    .line 30
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const-string v13, "CR_PAYMENT_STD_MSG_USE_MAG_STRIP"

    const/16 v14, 0x12

    const/16 v15, 0x12

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_USE_MAG_STRIP:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    .line 31
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const-string v13, "CR_PAYMENT_STD_MSG_TRY_AGAIN"

    const/16 v14, 0x13

    const/16 v15, 0x13

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_TRY_AGAIN:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    .line 32
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const-string v13, "CR_PAYMENT_STD_MSG_WELCOME"

    const/16 v14, 0x14

    const/16 v15, 0x14

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_WELCOME:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    .line 33
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const-string v13, "CR_PAYMENT_STD_MSG_PRESENT_CARD"

    const/16 v14, 0x15

    const/16 v15, 0x15

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_PRESENT_CARD:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    .line 34
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const-string v13, "CR_PAYMENT_STD_MSG_PROCESSING"

    const/16 v14, 0x16

    const/16 v15, 0x16

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_PROCESSING:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    .line 35
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const-string v13, "CR_PAYMENT_STD_MSG_CARD_READ_OK_PLS_REMOVE_CARD"

    const/16 v14, 0x17

    const/16 v15, 0x17

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_CARD_READ_OK_PLS_REMOVE_CARD:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    .line 36
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const-string v13, "CR_PAYMENT_STD_MSG_PLS_INSERT_OR_SWIPE_CARD"

    const/16 v14, 0x18

    const/16 v15, 0x18

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_PLS_INSERT_OR_SWIPE_CARD:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    .line 37
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const-string v13, "CR_PAYMENT_STD_MSG_PLS_PRESENT_ONE_CARD"

    const/16 v14, 0x19

    const/16 v15, 0x19

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_PLS_PRESENT_ONE_CARD:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    .line 38
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const-string v13, "CR_PAYMENT_STD_MSG_APPROVED_PLS_SIGN"

    const/16 v14, 0x1a

    const/16 v15, 0x1a

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_APPROVED_PLS_SIGN:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    .line 39
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const-string v13, "CR_PAYMENT_STD_MSG_AUTHORISING_PLS_WAIT"

    const/16 v14, 0x1b

    const/16 v15, 0x1b

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_AUTHORISING_PLS_WAIT:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    .line 40
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const-string v13, "CR_PAYMENT_STD_MSG_INSERT_SWIPE_OR_TRY_ANOTHER_CARD"

    const/16 v14, 0x1c

    const/16 v15, 0x1c

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_INSERT_SWIPE_OR_TRY_ANOTHER_CARD:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    .line 41
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const-string v13, "CR_PAYMENT_STD_MSG_PLS_INSERT_CARD"

    const/16 v14, 0x1d

    const/16 v15, 0x1d

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_PLS_INSERT_CARD:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    .line 42
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const-string v13, "CR_PAYMENT_STD_MSG_NO_MSG"

    const/16 v14, 0x1e

    const/16 v15, 0x1e

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_NO_MSG:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    .line 43
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const-string v13, "CR_PAYMENT_STD_MSG_SEE_PHONE_FOR_INSTRUCTIONS"

    const/16 v14, 0x1f

    const/16 v15, 0x20

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_SEE_PHONE_FOR_INSTRUCTIONS:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    .line 44
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const-string v13, "CR_PAYMENT_STD_MSG_PRESENT_CARD_AGAIN"

    const/16 v14, 0x20

    const/16 v15, 0x21

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_PRESENT_CARD_AGAIN:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    .line 45
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const-string v13, "CR_PAYMENT_STD_MSG_UNLOCK_PHONE_TO_PAY"

    const/16 v14, 0x21

    const/16 v15, 0x22

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_UNLOCK_PHONE_TO_PAY:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    .line 46
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const-string v13, "CR_PAYMENT_STD_MSG_TOO_MANY_TAP"

    const/16 v14, 0x22

    const/16 v15, 0x23

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_TOO_MANY_TAP:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const/16 v0, 0x23

    new-array v0, v0, [Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    .line 11
    sget-object v13, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_NONE:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    aput-object v13, v0, v1

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_AMOUNT:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_AMOUNT_OK:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_APPROVED:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_CALL_YOUR_BANK:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_CANCEL_OR_ENTER:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_CARD_ERROR:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_DECLINED:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_ENTER_AMOUNT:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_ENTER_PIN:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_INCORRECT_PIN:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_INSERT_CARD:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_NOT_ACCEPTED:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_PIN_OK:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_PLEASE_WAIT:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_PROCESSING_ERROR:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_REMOVE_CARD:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_USE_CHIP_READER:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_USE_MAG_STRIP:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_TRY_AGAIN:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_WELCOME:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_PRESENT_CARD:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_PROCESSING:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_CARD_READ_OK_PLS_REMOVE_CARD:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_PLS_INSERT_OR_SWIPE_CARD:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_PLS_PRESENT_ONE_CARD:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_APPROVED_PLS_SIGN:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_AUTHORISING_PLS_WAIT:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_INSERT_SWIPE_OR_TRY_ANOTHER_CARD:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_PLS_INSERT_CARD:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_NO_MSG:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_SEE_PHONE_FOR_INSTRUCTIONS:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_PRESENT_CARD_AGAIN:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const/16 v2, 0x20

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_UNLOCK_PHONE_TO_PAY:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const/16 v2, 0x21

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_TOO_MANY_TAP:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    const/16 v2, 0x22

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->$VALUES:[Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 63
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 64
    invoke-static {}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage$SwigNext;->access$008()I

    move-result p1

    iput p1, p0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->swigValue:I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 68
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 69
    iput p3, p0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->swigValue:I

    add-int/lit8 p3, p3, 0x1

    .line 70
    invoke-static {p3}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage$SwigNext;->access$002(I)I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;",
            ")V"
        }
    .end annotation

    .line 74
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 75
    iget p1, p3, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->swigValue:I

    iput p1, p0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->swigValue:I

    .line 76
    iget p1, p0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->swigValue:I

    add-int/lit8 p1, p1, 0x1

    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage$SwigNext;->access$002(I)I

    return-void
.end method

.method public static swigToEnum(I)Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;
    .locals 6

    .line 53
    const-class v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    .line 54
    array-length v2, v1

    if-ge p0, v2, :cond_0

    if-ltz p0, :cond_0

    aget-object v2, v1, p0

    iget v2, v2, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->swigValue:I

    if-ne v2, p0, :cond_0

    .line 55
    aget-object p0, v1, p0

    return-object p0

    .line 56
    :cond_0
    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_2

    aget-object v4, v1, v3

    .line 57
    iget v5, v4, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->swigValue:I

    if-ne v5, p0, :cond_1

    return-object v4

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 59
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "No enum "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, " with value "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;
    .locals 1

    .line 11
    const-class v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    return-object p0
.end method

.method public static values()[Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;
    .locals 1

    .line 11
    sget-object v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->$VALUES:[Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    invoke-virtual {v0}, [Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    return-object v0
.end method


# virtual methods
.method public final swigValue()I
    .locals 1

    .line 49
    iget v0, p0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->swigValue:I

    return v0
.end method
