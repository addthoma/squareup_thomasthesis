.class public Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;
.super Ljava/lang/Object;
.source "SecureTouchFeatureNativeJNI.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final native CrsStmAccessibilityPinPadConfig_border_width_get(JLcom/squareup/cardreader/lcr/CrsStmAccessibilityPinPadConfig;)J
.end method

.method public static final native CrsStmAccessibilityPinPadConfig_border_width_set(JLcom/squareup/cardreader/lcr/CrsStmAccessibilityPinPadConfig;J)V
.end method

.method public static final native CrsStmAccessibilityPinPadConfig_center_button_radius_get(JLcom/squareup/cardreader/lcr/CrsStmAccessibilityPinPadConfig;)J
.end method

.method public static final native CrsStmAccessibilityPinPadConfig_center_button_radius_set(JLcom/squareup/cardreader/lcr/CrsStmAccessibilityPinPadConfig;J)V
.end method

.method public static final native CrsStmAccessibilityPinPadConfig_x_max_get(JLcom/squareup/cardreader/lcr/CrsStmAccessibilityPinPadConfig;)J
.end method

.method public static final native CrsStmAccessibilityPinPadConfig_x_max_set(JLcom/squareup/cardreader/lcr/CrsStmAccessibilityPinPadConfig;J)V
.end method

.method public static final native CrsStmAccessibilityPinPadConfig_y_max_get(JLcom/squareup/cardreader/lcr/CrsStmAccessibilityPinPadConfig;)J
.end method

.method public static final native CrsStmAccessibilityPinPadConfig_y_max_set(JLcom/squareup/cardreader/lcr/CrsStmAccessibilityPinPadConfig;J)V
.end method

.method public static final native CrsStmDisableSquidTouchDriverResult_error_code_get(JLcom/squareup/cardreader/lcr/CrsStmDisableSquidTouchDriverResult;)S
.end method

.method public static final native CrsStmDisableSquidTouchDriverResult_error_code_set(JLcom/squareup/cardreader/lcr/CrsStmDisableSquidTouchDriverResult;S)V
.end method

.method public static final native CrsStmHidePinPadResult_error_code_get(JLcom/squareup/cardreader/lcr/CrsStmHidePinPadResult;)S
.end method

.method public static final native CrsStmHidePinPadResult_error_code_set(JLcom/squareup/cardreader/lcr/CrsStmHidePinPadResult;S)V
.end method

.method public static final native CrsStmPinPadButtonInfo_button_id_get(JLcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;)I
.end method

.method public static final native CrsStmPinPadButtonInfo_button_id_set(JLcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;I)V
.end method

.method public static final native CrsStmPinPadButtonInfo_x_size_get(JLcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;)I
.end method

.method public static final native CrsStmPinPadButtonInfo_x_size_set(JLcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;I)V
.end method

.method public static final native CrsStmPinPadButtonInfo_x_start_get(JLcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;)I
.end method

.method public static final native CrsStmPinPadButtonInfo_x_start_set(JLcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;I)V
.end method

.method public static final native CrsStmPinPadButtonInfo_y_size_get(JLcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;)I
.end method

.method public static final native CrsStmPinPadButtonInfo_y_size_set(JLcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;I)V
.end method

.method public static final native CrsStmPinPadButtonInfo_y_start_get(JLcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;)I
.end method

.method public static final native CrsStmPinPadButtonInfo_y_start_set(JLcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;I)V
.end method

.method public static final native CrsStmPinPadEvent_button_id_get(JLcom/squareup/cardreader/lcr/CrsStmPinPadEvent;)S
.end method

.method public static final native CrsStmPinPadEvent_button_id_set(JLcom/squareup/cardreader/lcr/CrsStmPinPadEvent;S)V
.end method

.method public static final native CrsStmShowPinPadResult_access_pin_pad_config_get(JLcom/squareup/cardreader/lcr/CrsStmShowPinPadResult;)J
.end method

.method public static final native CrsStmShowPinPadResult_access_pin_pad_config_set(JLcom/squareup/cardreader/lcr/CrsStmShowPinPadResult;JLcom/squareup/cardreader/lcr/CrsStmAccessibilityPinPadConfig;)V
.end method

.method public static final native CrsStmShowPinPadResult_buttons_get(JLcom/squareup/cardreader/lcr/CrsStmShowPinPadResult;)J
.end method

.method public static final native CrsStmShowPinPadResult_buttons_set(JLcom/squareup/cardreader/lcr/CrsStmShowPinPadResult;JLcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;)V
.end method

.method public static final native CrsStmShowPinPadResult_config_mode_get(JLcom/squareup/cardreader/lcr/CrsStmShowPinPadResult;)S
.end method

.method public static final native CrsStmShowPinPadResult_config_mode_set(JLcom/squareup/cardreader/lcr/CrsStmShowPinPadResult;S)V
.end method

.method public static final native CrsStmShowPinPadResult_number_of_buttons_get(JLcom/squareup/cardreader/lcr/CrsStmShowPinPadResult;)S
.end method

.method public static final native CrsStmShowPinPadResult_number_of_buttons_set(JLcom/squareup/cardreader/lcr/CrsStmShowPinPadResult;S)V
.end method

.method public static final native cr_secure_touch_mode_feature_alloc()J
.end method

.method public static final native cr_secure_touch_mode_feature_disable_squid_touch_driver_result(JJLcom/squareup/cardreader/lcr/CrsStmDisableSquidTouchDriverResult;)V
.end method

.method public static final native cr_secure_touch_mode_feature_event_api_t_context_get(JLcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;)J
.end method

.method public static final native cr_secure_touch_mode_feature_event_api_t_context_set(JLcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;J)V
.end method

.method public static final native cr_secure_touch_mode_feature_event_api_t_disable_squid_touch_driver_request_get(JLcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;)J
.end method

.method public static final native cr_secure_touch_mode_feature_event_api_t_disable_squid_touch_driver_request_set(JLcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;J)V
.end method

.method public static final native cr_secure_touch_mode_feature_event_api_t_enable_squid_touch_driver_request_get(JLcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;)J
.end method

.method public static final native cr_secure_touch_mode_feature_event_api_t_enable_squid_touch_driver_request_set(JLcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;J)V
.end method

.method public static final native cr_secure_touch_mode_feature_event_api_t_hide_pin_pad_request_get(JLcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;)J
.end method

.method public static final native cr_secure_touch_mode_feature_event_api_t_hide_pin_pad_request_set(JLcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;J)V
.end method

.method public static final native cr_secure_touch_mode_feature_event_api_t_on_keepalive_failed_get(JLcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;)J
.end method

.method public static final native cr_secure_touch_mode_feature_event_api_t_on_keepalive_failed_set(JLcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;J)V
.end method

.method public static final native cr_secure_touch_mode_feature_event_api_t_on_pin_pad_center_point_get(JLcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;)J
.end method

.method public static final native cr_secure_touch_mode_feature_event_api_t_on_pin_pad_center_point_set(JLcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;J)V
.end method

.method public static final native cr_secure_touch_mode_feature_event_api_t_on_pin_pad_event_get(JLcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;)J
.end method

.method public static final native cr_secure_touch_mode_feature_event_api_t_on_pin_pad_event_set(JLcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;J)V
.end method

.method public static final native cr_secure_touch_mode_feature_event_api_t_show_pin_pad_request_get(JLcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;)J
.end method

.method public static final native cr_secure_touch_mode_feature_event_api_t_show_pin_pad_request_set(JLcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;J)V
.end method

.method public static final native cr_secure_touch_mode_feature_free(J)I
.end method

.method public static final native cr_secure_touch_mode_feature_init(JJJLcom/squareup/cardreader/lcr/cr_secure_touch_mode_feature_event_api_t;)I
.end method

.method public static final native cr_secure_touch_mode_feature_regular_set_button_location(JJLcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;)V
.end method

.method public static final native cr_secure_touch_mode_feature_sent_pinpad_configs(JI)V
.end method

.method public static final native cr_secure_touch_mode_feature_set_accessibility_configs(JJLcom/squareup/cardreader/lcr/CrsStmAccessibilityPinPadConfig;)V
.end method

.method public static final native cr_secure_touch_mode_feature_start_secure_touch(J)V
.end method

.method public static final native cr_secure_touch_mode_feature_stop_secure_touch(J)V
.end method

.method public static final native cr_secure_touch_mode_feature_term(J)I
.end method

.method public static final native cr_secure_touch_mode_pin_pad_is_hidden(JJLcom/squareup/cardreader/lcr/CrsStmHidePinPadResult;)V
.end method

.method public static final native crs_stm_card_info_t_issuer_id_get(JLcom/squareup/cardreader/lcr/crs_stm_card_info_t;)S
.end method

.method public static final native crs_stm_card_info_t_issuer_id_set(JLcom/squareup/cardreader/lcr/crs_stm_card_info_t;S)V
.end method

.method public static final native crs_stm_card_info_t_last4_get(JLcom/squareup/cardreader/lcr/crs_stm_card_info_t;)J
.end method

.method public static final native crs_stm_card_info_t_last4_set(JLcom/squareup/cardreader/lcr/crs_stm_card_info_t;J)V
.end method

.method public static final native crs_stm_card_info_t_name_get(JLcom/squareup/cardreader/lcr/crs_stm_card_info_t;)Ljava/lang/String;
.end method

.method public static final native crs_stm_card_info_t_name_set(JLcom/squareup/cardreader/lcr/crs_stm_card_info_t;Ljava/lang/String;)V
.end method

.method public static final native crs_stm_pin_pad_accessibility_config_t_dummy_get(JLcom/squareup/cardreader/lcr/crs_stm_pin_pad_accessibility_config_t;)S
.end method

.method public static final native crs_stm_pin_pad_accessibility_config_t_dummy_set(JLcom/squareup/cardreader/lcr/crs_stm_pin_pad_accessibility_config_t;S)V
.end method

.method public static final native crs_stm_pin_pad_center_point_t_pin_pad_type_get(JLcom/squareup/cardreader/lcr/crs_stm_pin_pad_center_point_t;)S
.end method

.method public static final native crs_stm_pin_pad_center_point_t_pin_pad_type_set(JLcom/squareup/cardreader/lcr/crs_stm_pin_pad_center_point_t;S)V
.end method

.method public static final native crs_stm_pin_pad_center_point_t_x_get(JLcom/squareup/cardreader/lcr/crs_stm_pin_pad_center_point_t;)J
.end method

.method public static final native crs_stm_pin_pad_center_point_t_x_set(JLcom/squareup/cardreader/lcr/crs_stm_pin_pad_center_point_t;J)V
.end method

.method public static final native crs_stm_pin_pad_center_point_t_y_get(JLcom/squareup/cardreader/lcr/crs_stm_pin_pad_center_point_t;)J
.end method

.method public static final native crs_stm_pin_pad_center_point_t_y_set(JLcom/squareup/cardreader/lcr/crs_stm_pin_pad_center_point_t;J)V
.end method

.method public static final native crs_stm_show_pin_pad_req_t_card_info_get(JLcom/squareup/cardreader/lcr/crs_stm_show_pin_pad_req_t;)J
.end method

.method public static final native crs_stm_show_pin_pad_req_t_card_info_set(JLcom/squareup/cardreader/lcr/crs_stm_show_pin_pad_req_t;JLcom/squareup/cardreader/lcr/crs_stm_card_info_t;)V
.end method

.method public static final native crs_stm_spe_keepalive_t_nonce_get(JLcom/squareup/cardreader/lcr/crs_stm_spe_keepalive_t;)J
.end method

.method public static final native crs_stm_spe_keepalive_t_nonce_set(JLcom/squareup/cardreader/lcr/crs_stm_spe_keepalive_t;J)V
.end method

.method public static final native delete_CrsStmAccessibilityPinPadConfig(J)V
.end method

.method public static final native delete_CrsStmDisableSquidTouchDriverResult(J)V
.end method

.method public static final native delete_CrsStmHidePinPadResult(J)V
.end method

.method public static final native delete_CrsStmPinPadButtonInfo(J)V
.end method

.method public static final native delete_CrsStmPinPadEvent(J)V
.end method

.method public static final native delete_CrsStmShowPinPadResult(J)V
.end method

.method public static final native delete_cr_secure_touch_mode_feature_event_api_t(J)V
.end method

.method public static final native delete_crs_stm_card_info_t(J)V
.end method

.method public static final native delete_crs_stm_pin_pad_accessibility_config_t(J)V
.end method

.method public static final native delete_crs_stm_pin_pad_center_point_t(J)V
.end method

.method public static final native delete_crs_stm_show_pin_pad_req_t(J)V
.end method

.method public static final native delete_crs_stm_spe_keepalive_t(J)V
.end method

.method public static final native new_CrsStmAccessibilityPinPadConfig()J
.end method

.method public static final native new_CrsStmDisableSquidTouchDriverResult()J
.end method

.method public static final native new_CrsStmHidePinPadResult()J
.end method

.method public static final native new_CrsStmPinPadButtonInfo()J
.end method

.method public static final native new_CrsStmPinPadEvent()J
.end method

.method public static final native new_CrsStmShowPinPadResult()J
.end method

.method public static final native new_cr_secure_touch_mode_feature_event_api_t()J
.end method

.method public static final native new_crs_stm_card_info_t()J
.end method

.method public static final native new_crs_stm_pin_pad_accessibility_config_t()J
.end method

.method public static final native new_crs_stm_pin_pad_center_point_t()J
.end method

.method public static final native new_crs_stm_show_pin_pad_req_t()J
.end method

.method public static final native new_crs_stm_spe_keepalive_t()J
.end method

.method public static final native secure_touch_initialize(JLjava/lang/Object;)J
.end method
