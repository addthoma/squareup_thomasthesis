.class public interface abstract Lcom/squareup/cardreader/lcr/SystemFeatureNativeConstants;
.super Ljava/lang/Object;
.source "SystemFeatureNativeConstants.java"


# static fields
.field public static final CRS_CAPABILITY_SYSTEM_KEEPALIVE_MINIMUM_FIRMWARE_VERSION:I = 0x31150

.field public static final CRS_SYSTEM_CAPABILITIES_BUF_MAX_LEN:I = 0x100

.field public static final CRS_SYSTEM_FEATURE_FLAGS_MAX_LEN:I = 0x100

.field public static final CRS_SYSTEM_KEEPALIVE_MESSAGE_RECEIVE_TIMEOUT_MS:I = 0x1194

.field public static final CRS_SYSTEM_KEEPALIVE_MESSAGE_SEND_PERIOD_MS:I = 0x7d0
