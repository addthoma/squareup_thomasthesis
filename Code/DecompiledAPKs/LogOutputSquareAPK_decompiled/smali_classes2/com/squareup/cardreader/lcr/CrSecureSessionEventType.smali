.class public final enum Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;
.super Ljava/lang/Enum;
.source "CrSecureSessionEventType.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/lcr/CrSecureSessionEventType$SwigNext;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;

.field public static final enum CR_SECURESESSION_FEATURE_EVENT_TYPE_SESSION_INVALID:Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;

.field public static final enum CR_SECURESESSION_FEATURE_EVENT_TYPE_SESSION_VALID:Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;


# instance fields
.field private final swigValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 12
    new-instance v0, Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;

    const/4 v1, 0x0

    const-string v2, "CR_SECURESESSION_FEATURE_EVENT_TYPE_SESSION_VALID"

    invoke-direct {v0, v2, v1}, Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;->CR_SECURESESSION_FEATURE_EVENT_TYPE_SESSION_VALID:Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;

    .line 13
    new-instance v0, Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;

    const/4 v2, 0x1

    const-string v3, "CR_SECURESESSION_FEATURE_EVENT_TYPE_SESSION_INVALID"

    invoke-direct {v0, v3, v2}, Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;->CR_SECURESESSION_FEATURE_EVENT_TYPE_SESSION_INVALID:Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;

    .line 11
    sget-object v3, Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;->CR_SECURESESSION_FEATURE_EVENT_TYPE_SESSION_VALID:Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;->CR_SECURESESSION_FEATURE_EVENT_TYPE_SESSION_INVALID:Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;->$VALUES:[Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 30
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 31
    invoke-static {}, Lcom/squareup/cardreader/lcr/CrSecureSessionEventType$SwigNext;->access$008()I

    move-result p1

    iput p1, p0, Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;->swigValue:I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 35
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 36
    iput p3, p0, Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;->swigValue:I

    add-int/lit8 p3, p3, 0x1

    .line 37
    invoke-static {p3}, Lcom/squareup/cardreader/lcr/CrSecureSessionEventType$SwigNext;->access$002(I)I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/squareup/cardreader/lcr/CrSecureSessionEventType;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;",
            ")V"
        }
    .end annotation

    .line 41
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 42
    iget p1, p3, Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;->swigValue:I

    iput p1, p0, Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;->swigValue:I

    .line 43
    iget p1, p0, Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;->swigValue:I

    add-int/lit8 p1, p1, 0x1

    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CrSecureSessionEventType$SwigNext;->access$002(I)I

    return-void
.end method

.method public static swigToEnum(I)Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;
    .locals 6

    .line 20
    const-class v0, Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;

    .line 21
    array-length v2, v1

    if-ge p0, v2, :cond_0

    if-ltz p0, :cond_0

    aget-object v2, v1, p0

    iget v2, v2, Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;->swigValue:I

    if-ne v2, p0, :cond_0

    .line 22
    aget-object p0, v1, p0

    return-object p0

    .line 23
    :cond_0
    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_2

    aget-object v4, v1, v3

    .line 24
    iget v5, v4, Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;->swigValue:I

    if-ne v5, p0, :cond_1

    return-object v4

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 26
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "No enum "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, " with value "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;
    .locals 1

    .line 11
    const-class v0, Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;
    .locals 1

    .line 11
    sget-object v0, Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;->$VALUES:[Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;

    invoke-virtual {v0}, [Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;

    return-object v0
.end method


# virtual methods
.method public final swigValue()I
    .locals 1

    .line 16
    iget v0, p0, Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;->swigValue:I

    return v0
.end method
