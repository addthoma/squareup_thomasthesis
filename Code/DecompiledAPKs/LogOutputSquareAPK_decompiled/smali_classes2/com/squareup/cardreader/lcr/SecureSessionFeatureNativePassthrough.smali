.class public Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativePassthrough;
.super Ljava/lang/Object;
.source "SecureSessionFeatureNativePassthrough.java"

# interfaces
.implements Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeInterface;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public cr_securesession_feature_establish_session(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;)Lcom/squareup/cardreader/lcr/CrSecureSessionResult;
    .locals 0

    .line 28
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNative;->cr_securesession_feature_establish_session(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;)Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    move-result-object p1

    return-object p1
.end method

.method public cr_securesession_feature_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;)Lcom/squareup/cardreader/lcr/CrSecureSessionResult;
    .locals 0

    .line 16
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNative;->cr_securesession_feature_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;)Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    move-result-object p1

    return-object p1
.end method

.method public cr_securesession_feature_notify_server_error(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;)Lcom/squareup/cardreader/lcr/CrSecureSessionResult;
    .locals 0

    .line 22
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNative;->cr_securesession_feature_notify_server_error(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;)Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    move-result-object p1

    return-object p1
.end method

.method public cr_securesession_feature_pin_bypass(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;)Lcom/squareup/cardreader/lcr/CrSecureSessionResult;
    .locals 0

    .line 34
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNative;->cr_securesession_feature_pin_bypass(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;)Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    move-result-object p1

    return-object p1
.end method

.method public cr_securesession_feature_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;)Lcom/squareup/cardreader/lcr/CrSecureSessionResult;
    .locals 0

    .line 10
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNative;->cr_securesession_feature_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;)Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    move-result-object p1

    return-object p1
.end method

.method public pin_add_digit(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;I)Z
    .locals 0

    .line 56
    invoke-static {p1, p2}, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNative;->pin_add_digit(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;I)Z

    move-result p1

    return p1
.end method

.method public pin_reset(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;)V
    .locals 0

    .line 51
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNative;->pin_reset(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;)V

    return-void
.end method

.method public pin_submit(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;)Lcom/squareup/cardreader/lcr/CrSecureSessionResult;
    .locals 0

    .line 61
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNative;->pin_submit(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;)Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    move-result-object p1

    return-object p1
.end method

.method public securesession_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Ljava/lang/Object;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;
    .locals 0

    .line 40
    invoke-static {p1, p2, p3}, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNative;->securesession_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Ljava/lang/Object;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;

    move-result-object p1

    return-object p1
.end method

.method public securesession_recv_server_message(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;[B)Lcom/squareup/cardreader/lcr/CrSecureSessionResultError;
    .locals 0

    .line 46
    invoke-static {p1, p2}, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNative;->securesession_recv_server_message(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;[B)Lcom/squareup/cardreader/lcr/CrSecureSessionResultError;

    move-result-object p1

    return-object p1
.end method

.method public set_kb(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;[B)Z
    .locals 0

    .line 66
    invoke-static {p1, p2}, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNative;->set_kb(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;[B)Z

    move-result p1

    return p1
.end method
