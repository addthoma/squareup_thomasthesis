.class public final enum Lcom/squareup/cardreader/lcr/CrPaymentCardAction;
.super Ljava/lang/Enum;
.source "CrPaymentCardAction.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/lcr/CrPaymentCardAction$SwigNext;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/cardreader/lcr/CrPaymentCardAction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

.field public static final enum CR_PAYMENT_CARD_ACTION_CONTACTLESS_CARD_ERROR_TRY_AGAIN:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

.field public static final enum CR_PAYMENT_CARD_ACTION_CONTACTLESS_CARD_ERROR_TRY_ANOTHER_CARD:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

.field public static final enum CR_PAYMENT_CARD_ACTION_CONTACTLESS_CARD_LIMIT_EXCEEDED_ERROR_TRY_ANOTHER_CARD:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

.field public static final enum CR_PAYMENT_CARD_ACTION_CONTACTLESS_CARD_LIMIT_EXCEEDED_INSERT_CARD:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

.field public static final enum CR_PAYMENT_CARD_ACTION_CONTACTLESS_PRESENT_ONLY_ONE:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

.field public static final enum CR_PAYMENT_CARD_ACTION_CONTACTLESS_SEE_PHONE_FOR_INSTRUCTION:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

.field public static final enum CR_PAYMENT_CARD_ACTION_CONTACTLESS_UNLOCK_PHONE_TO_PAY:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

.field public static final enum CR_PAYMENT_CARD_ACTION_INSERT:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

.field public static final enum CR_PAYMENT_CARD_ACTION_INSERT_AGAIN:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

.field public static final enum CR_PAYMENT_CARD_ACTION_INSERT_FROM_CONTACTLESS:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

.field public static final enum CR_PAYMENT_CARD_ACTION_NONE:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

.field public static final enum CR_PAYMENT_CARD_ACTION_REQUEST_TAP:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

.field public static final enum CR_PAYMENT_CARD_ACTION_SWIPE_AGAIN:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

.field public static final enum CR_PAYMENT_CARD_ACTION_SWIPE_SCHEME:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

.field public static final enum CR_PAYMENT_CARD_ACTION_SWIPE_TECHNICAL:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;


# instance fields
.field private final swigValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 12
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    const/4 v1, 0x0

    const-string v2, "CR_PAYMENT_CARD_ACTION_NONE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->CR_PAYMENT_CARD_ACTION_NONE:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    .line 13
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    const/4 v2, 0x1

    const-string v3, "CR_PAYMENT_CARD_ACTION_INSERT"

    invoke-direct {v0, v3, v2}, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->CR_PAYMENT_CARD_ACTION_INSERT:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    .line 14
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    const/4 v3, 0x2

    const-string v4, "CR_PAYMENT_CARD_ACTION_INSERT_AGAIN"

    invoke-direct {v0, v4, v3}, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->CR_PAYMENT_CARD_ACTION_INSERT_AGAIN:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    .line 15
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    const/4 v4, 0x3

    const-string v5, "CR_PAYMENT_CARD_ACTION_SWIPE_TECHNICAL"

    invoke-direct {v0, v5, v4}, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->CR_PAYMENT_CARD_ACTION_SWIPE_TECHNICAL:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    .line 16
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    const/4 v5, 0x4

    const-string v6, "CR_PAYMENT_CARD_ACTION_SWIPE_SCHEME"

    invoke-direct {v0, v6, v5}, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->CR_PAYMENT_CARD_ACTION_SWIPE_SCHEME:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    .line 17
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    const/4 v6, 0x5

    const-string v7, "CR_PAYMENT_CARD_ACTION_SWIPE_AGAIN"

    invoke-direct {v0, v7, v6}, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->CR_PAYMENT_CARD_ACTION_SWIPE_AGAIN:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    .line 18
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    const/4 v7, 0x6

    const-string v8, "CR_PAYMENT_CARD_ACTION_INSERT_FROM_CONTACTLESS"

    invoke-direct {v0, v8, v7}, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->CR_PAYMENT_CARD_ACTION_INSERT_FROM_CONTACTLESS:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    .line 19
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    const/4 v8, 0x7

    const-string v9, "CR_PAYMENT_CARD_ACTION_CONTACTLESS_CARD_ERROR_TRY_ANOTHER_CARD"

    invoke-direct {v0, v9, v8}, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->CR_PAYMENT_CARD_ACTION_CONTACTLESS_CARD_ERROR_TRY_ANOTHER_CARD:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    .line 20
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    const/16 v9, 0x8

    const-string v10, "CR_PAYMENT_CARD_ACTION_CONTACTLESS_CARD_ERROR_TRY_AGAIN"

    invoke-direct {v0, v10, v9}, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->CR_PAYMENT_CARD_ACTION_CONTACTLESS_CARD_ERROR_TRY_AGAIN:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    .line 21
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    const/16 v10, 0x9

    const-string v11, "CR_PAYMENT_CARD_ACTION_CONTACTLESS_SEE_PHONE_FOR_INSTRUCTION"

    invoke-direct {v0, v11, v10}, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->CR_PAYMENT_CARD_ACTION_CONTACTLESS_SEE_PHONE_FOR_INSTRUCTION:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    .line 22
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    const/16 v11, 0xa

    const-string v12, "CR_PAYMENT_CARD_ACTION_CONTACTLESS_PRESENT_ONLY_ONE"

    invoke-direct {v0, v12, v11}, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->CR_PAYMENT_CARD_ACTION_CONTACTLESS_PRESENT_ONLY_ONE:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    .line 23
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    const/16 v12, 0xb

    const-string v13, "CR_PAYMENT_CARD_ACTION_CONTACTLESS_UNLOCK_PHONE_TO_PAY"

    invoke-direct {v0, v13, v12}, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->CR_PAYMENT_CARD_ACTION_CONTACTLESS_UNLOCK_PHONE_TO_PAY:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    .line 24
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    const/16 v13, 0xc

    const-string v14, "CR_PAYMENT_CARD_ACTION_CONTACTLESS_CARD_LIMIT_EXCEEDED_ERROR_TRY_ANOTHER_CARD"

    invoke-direct {v0, v14, v13}, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->CR_PAYMENT_CARD_ACTION_CONTACTLESS_CARD_LIMIT_EXCEEDED_ERROR_TRY_ANOTHER_CARD:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    .line 25
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    const/16 v14, 0xd

    const-string v15, "CR_PAYMENT_CARD_ACTION_CONTACTLESS_CARD_LIMIT_EXCEEDED_INSERT_CARD"

    invoke-direct {v0, v15, v14}, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->CR_PAYMENT_CARD_ACTION_CONTACTLESS_CARD_LIMIT_EXCEEDED_INSERT_CARD:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    .line 26
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    const/16 v15, 0xe

    const-string v14, "CR_PAYMENT_CARD_ACTION_REQUEST_TAP"

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->CR_PAYMENT_CARD_ACTION_REQUEST_TAP:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    const/16 v0, 0xf

    new-array v0, v0, [Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    .line 11
    sget-object v14, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->CR_PAYMENT_CARD_ACTION_NONE:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    aput-object v14, v0, v1

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->CR_PAYMENT_CARD_ACTION_INSERT:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->CR_PAYMENT_CARD_ACTION_INSERT_AGAIN:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->CR_PAYMENT_CARD_ACTION_SWIPE_TECHNICAL:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->CR_PAYMENT_CARD_ACTION_SWIPE_SCHEME:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->CR_PAYMENT_CARD_ACTION_SWIPE_AGAIN:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->CR_PAYMENT_CARD_ACTION_INSERT_FROM_CONTACTLESS:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->CR_PAYMENT_CARD_ACTION_CONTACTLESS_CARD_ERROR_TRY_ANOTHER_CARD:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->CR_PAYMENT_CARD_ACTION_CONTACTLESS_CARD_ERROR_TRY_AGAIN:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->CR_PAYMENT_CARD_ACTION_CONTACTLESS_SEE_PHONE_FOR_INSTRUCTION:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->CR_PAYMENT_CARD_ACTION_CONTACTLESS_PRESENT_ONLY_ONE:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->CR_PAYMENT_CARD_ACTION_CONTACTLESS_UNLOCK_PHONE_TO_PAY:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->CR_PAYMENT_CARD_ACTION_CONTACTLESS_CARD_LIMIT_EXCEEDED_ERROR_TRY_ANOTHER_CARD:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    aput-object v1, v0, v13

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->CR_PAYMENT_CARD_ACTION_CONTACTLESS_CARD_LIMIT_EXCEEDED_INSERT_CARD:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->CR_PAYMENT_CARD_ACTION_REQUEST_TAP:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    aput-object v1, v0, v15

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->$VALUES:[Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 43
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 44
    invoke-static {}, Lcom/squareup/cardreader/lcr/CrPaymentCardAction$SwigNext;->access$008()I

    move-result p1

    iput p1, p0, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->swigValue:I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 48
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 49
    iput p3, p0, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->swigValue:I

    add-int/lit8 p3, p3, 0x1

    .line 50
    invoke-static {p3}, Lcom/squareup/cardreader/lcr/CrPaymentCardAction$SwigNext;->access$002(I)I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/squareup/cardreader/lcr/CrPaymentCardAction;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/lcr/CrPaymentCardAction;",
            ")V"
        }
    .end annotation

    .line 54
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 55
    iget p1, p3, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->swigValue:I

    iput p1, p0, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->swigValue:I

    .line 56
    iget p1, p0, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->swigValue:I

    add-int/lit8 p1, p1, 0x1

    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CrPaymentCardAction$SwigNext;->access$002(I)I

    return-void
.end method

.method public static swigToEnum(I)Lcom/squareup/cardreader/lcr/CrPaymentCardAction;
    .locals 6

    .line 33
    const-class v0, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    .line 34
    array-length v2, v1

    if-ge p0, v2, :cond_0

    if-ltz p0, :cond_0

    aget-object v2, v1, p0

    iget v2, v2, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->swigValue:I

    if-ne v2, p0, :cond_0

    .line 35
    aget-object p0, v1, p0

    return-object p0

    .line 36
    :cond_0
    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_2

    aget-object v4, v1, v3

    .line 37
    iget v5, v4, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->swigValue:I

    if-ne v5, p0, :cond_1

    return-object v4

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 39
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "No enum "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, " with value "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/lcr/CrPaymentCardAction;
    .locals 1

    .line 11
    const-class v0, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    return-object p0
.end method

.method public static values()[Lcom/squareup/cardreader/lcr/CrPaymentCardAction;
    .locals 1

    .line 11
    sget-object v0, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->$VALUES:[Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    invoke-virtual {v0}, [Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    return-object v0
.end method


# virtual methods
.method public final swigValue()I
    .locals 1

    .line 29
    iget v0, p0, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->swigValue:I

    return v0
.end method
