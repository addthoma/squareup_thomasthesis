.class public Lcom/squareup/cardreader/lcr/CoredumpFeatureNative;
.super Ljava/lang/Object;
.source "CoredumpFeatureNative.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static coredump_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;
    .locals 3

    .line 33
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)J

    move-result-wide v0

    invoke-static {v0, v1, p1}, Lcom/squareup/cardreader/lcr/CoredumpFeatureNativeJNI;->coredump_initialize(JLjava/lang/Object;)J

    move-result-wide p0

    const-wide/16 v0, 0x0

    cmp-long v2, p0, v0

    if-nez v2, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    .line 34
    :cond_0
    new-instance v0, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;-><init>(JZ)V

    move-object p0, v0

    :goto_0
    return-object p0
.end method

.method public static coredump_trigger_dump(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;)V
    .locals 2

    .line 38
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/CoredumpFeatureNativeJNI;->coredump_trigger_dump(J)V

    return-void
.end method

.method public static cr_coredump_erase(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;)Lcom/squareup/cardreader/lcr/CrCoredumpResult;
    .locals 2

    .line 29
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/CoredumpFeatureNativeJNI;->cr_coredump_erase(J)I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrCoredumpResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrCoredumpResult;

    move-result-object p0

    return-object p0
.end method

.method public static cr_coredump_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;)Lcom/squareup/cardreader/lcr/CrCoredumpResult;
    .locals 2

    .line 17
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/CoredumpFeatureNativeJNI;->cr_coredump_free(J)I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrCoredumpResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrCoredumpResult;

    move-result-object p0

    return-object p0
.end method

.method public static cr_coredump_get_data(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;)Lcom/squareup/cardreader/lcr/CrCoredumpResult;
    .locals 2

    .line 25
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/CoredumpFeatureNativeJNI;->cr_coredump_get_data(J)I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrCoredumpResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrCoredumpResult;

    move-result-object p0

    return-object p0
.end method

.method public static cr_coredump_get_info(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;)Lcom/squareup/cardreader/lcr/CrCoredumpResult;
    .locals 2

    .line 21
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/CoredumpFeatureNativeJNI;->cr_coredump_get_info(J)I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrCoredumpResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrCoredumpResult;

    move-result-object p0

    return-object p0
.end method

.method public static cr_coredump_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;)Lcom/squareup/cardreader/lcr/CrCoredumpResult;
    .locals 2

    .line 13
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/CoredumpFeatureNativeJNI;->cr_coredump_term(J)I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrCoredumpResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrCoredumpResult;

    move-result-object p0

    return-object p0
.end method
