.class public Lcom/squareup/cardreader/bluetooth/BluetoothDiscoveryBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "BluetoothDiscoveryBroadcastReceiver.java"


# static fields
.field public static final MIURA_DEVICE_NAME:Ljava/lang/String; = "Miura"


# instance fields
.field private final bluetoothDiscoverer:Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;)V
    .locals 0

    .line 21
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/cardreader/bluetooth/BluetoothDiscoveryBroadcastReceiver;->bluetoothDiscoverer:Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;

    return-void
.end method

.method static deviceIsMiuraDevice(Landroid/bluetooth/BluetoothDevice;)Z
    .locals 1

    .line 60
    invoke-virtual {p0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 61
    invoke-virtual {p0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object p0

    const-string v0, "Miura"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method


# virtual methods
.method public destroy(Landroid/content/Context;)V
    .locals 0

    .line 34
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public initialize(Landroid/content/Context;)V
    .locals 2

    .line 26
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.bluetooth.adapter.action.DISCOVERY_STARTED"

    .line 27
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.bluetooth.device.action.FOUND"

    .line 28
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.bluetooth.adapter.action.DISCOVERY_FINISHED"

    .line 29
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 30
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .line 38
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const-string v3, "Got event %s"

    .line 39
    invoke-static {v3, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 40
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    const v3, -0x6a269925

    const/4 v4, 0x2

    if-eq v1, v3, :cond_2

    const v3, 0x6724d8

    if-eq v1, v3, :cond_1

    const v3, 0x459717c3

    if-eq v1, v3, :cond_0

    goto :goto_0

    :cond_0
    const-string v1, "android.bluetooth.device.action.FOUND"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    const-string v1, "android.bluetooth.adapter.action.DISCOVERY_STARTED"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    const-string v1, "android.bluetooth.adapter.action.DISCOVERY_FINISHED"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x2

    goto :goto_1

    :cond_3
    :goto_0
    const/4 v1, -0x1

    :goto_1
    if-eqz v1, :cond_6

    if-eq v1, v0, :cond_5

    if-ne v1, v4, :cond_4

    .line 52
    iget-object p1, p0, Lcom/squareup/cardreader/bluetooth/BluetoothDiscoveryBroadcastReceiver;->bluetoothDiscoverer:Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;

    invoke-virtual {p1}, Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;->onDiscoveryFinished()V

    goto :goto_2

    .line 55
    :cond_4
    new-instance p2, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unrecognized action: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p2

    :cond_5
    const-string p1, "android.bluetooth.device.extra.DEVICE"

    .line 45
    invoke-virtual {p2, p1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Landroid/bluetooth/BluetoothDevice;

    new-array p2, v0, [Ljava/lang/Object;

    .line 46
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, p2, v2

    const-string v0, "Device name: %s"

    invoke-static {v0, p2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 47
    invoke-static {p1}, Lcom/squareup/cardreader/bluetooth/BluetoothDiscoveryBroadcastReceiver;->deviceIsMiuraDevice(Landroid/bluetooth/BluetoothDevice;)Z

    move-result p2

    if-eqz p2, :cond_7

    .line 48
    iget-object p2, p0, Lcom/squareup/cardreader/bluetooth/BluetoothDiscoveryBroadcastReceiver;->bluetoothDiscoverer:Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;

    new-instance v0, Lcom/squareup/cardreader/ble/RealBleScanResult;

    invoke-direct {v0, p1, v2}, Lcom/squareup/cardreader/ble/RealBleScanResult;-><init>(Landroid/bluetooth/BluetoothDevice;Z)V

    invoke-virtual {p2, v0}, Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;->onDeviceFound(Lcom/squareup/cardreader/WirelessConnection;)V

    goto :goto_2

    .line 42
    :cond_6
    iget-object p1, p0, Lcom/squareup/cardreader/bluetooth/BluetoothDiscoveryBroadcastReceiver;->bluetoothDiscoverer:Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;

    invoke-virtual {p1}, Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;->onDiscoveryStarted()V

    :cond_7
    :goto_2
    return-void
.end method
