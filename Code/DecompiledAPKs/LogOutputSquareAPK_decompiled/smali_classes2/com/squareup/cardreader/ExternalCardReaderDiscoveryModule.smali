.class public abstract Lcom/squareup/cardreader/ExternalCardReaderDiscoveryModule;
.super Ljava/lang/Object;
.source "ExternalCardReaderDiscoveryModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideBleConnector(Lcom/squareup/cardreader/ble/BleConnector;)Lmortar/Scoped;
    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method

.method abstract provideBluetoothAclConnectionReceiver(Lcom/squareup/logging/BluetoothAclConnectionReceiver;)Lmortar/Scoped;
    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method

.method abstract provideBluetoothReceivers(Lcom/squareup/cardreader/ble/ScopedBluetoothReceivers;)Lmortar/Scoped;
    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method

.method abstract provideHeadset(Lcom/squareup/wavpool/swipe/ScopedHeadset;)Lmortar/Scoped;
    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method

.method abstract provideHeadsetStateInitializer(Lcom/squareup/cardreader/dipper/HeadsetStateInitializer;)Lmortar/Scoped;
    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method
