.class public final synthetic Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$rH4hsxohnbywessSrA04QGBFLws;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final synthetic f$0:Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;

.field private final synthetic f$1:Lcom/squareup/cardreader/CardReaderInfo;

.field private final synthetic f$2:[B

.field private final synthetic f$3:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

.field private final synthetic f$4:Lcom/squareup/cardreader/PaymentTimings;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/lcr/CrPaymentStandardMessage;Lcom/squareup/cardreader/PaymentTimings;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$rH4hsxohnbywessSrA04QGBFLws;->f$0:Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;

    iput-object p2, p0, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$rH4hsxohnbywessSrA04QGBFLws;->f$1:Lcom/squareup/cardreader/CardReaderInfo;

    iput-object p3, p0, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$rH4hsxohnbywessSrA04QGBFLws;->f$2:[B

    iput-object p4, p0, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$rH4hsxohnbywessSrA04QGBFLws;->f$3:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    iput-object p5, p0, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$rH4hsxohnbywessSrA04QGBFLws;->f$4:Lcom/squareup/cardreader/PaymentTimings;

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    iget-object v0, p0, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$rH4hsxohnbywessSrA04QGBFLws;->f$0:Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;

    iget-object v1, p0, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$rH4hsxohnbywessSrA04QGBFLws;->f$1:Lcom/squareup/cardreader/CardReaderInfo;

    iget-object v2, p0, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$rH4hsxohnbywessSrA04QGBFLws;->f$2:[B

    iget-object v3, p0, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$rH4hsxohnbywessSrA04QGBFLws;->f$3:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    iget-object v4, p0, Lcom/squareup/cardreader/-$$Lambda$CardReaderSwig$InternalPaymentListener$rH4hsxohnbywessSrA04QGBFLws;->f$4:Lcom/squareup/cardreader/PaymentTimings;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/squareup/cardreader/CardReaderSwig$InternalPaymentListener;->lambda$onICCDeclined$14$CardReaderSwig$InternalPaymentListener(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/lcr/CrPaymentStandardMessage;Lcom/squareup/cardreader/PaymentTimings;)V

    return-void
.end method
