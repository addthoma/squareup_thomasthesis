.class public final Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;
.super Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput;
.source "ReaderMessage.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OnPowerStatus"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0012\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B5\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\t\u0010\u0014\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0017\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0018\u001a\u00020\u0008H\u00c6\u0003J\t\u0010\u0019\u001a\u00020\nH\u00c6\u0003JE\u0010\u001a\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0002\u0010\t\u001a\u00020\nH\u00c6\u0001J\u0013\u0010\u001b\u001a\u00020\u00082\u0008\u0010\u001c\u001a\u0004\u0018\u00010\u001dH\u00d6\u0003J\t\u0010\u001e\u001a\u00020\u0003H\u00d6\u0001J\t\u0010\u001f\u001a\u00020 H\u00d6\u0001R\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0010R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u000fR\u0011\u0010\u0006\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u000fR\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u000f\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput;",
        "percentage",
        "",
        "current",
        "voltage",
        "temperature",
        "isCritical",
        "",
        "batteryMode",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$BatteryMode;",
        "(IIIIZLcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$BatteryMode;)V",
        "getBatteryMode",
        "()Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$BatteryMode;",
        "getCurrent",
        "()I",
        "()Z",
        "getPercentage",
        "getTemperature",
        "getVoltage",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "lcr-data-models_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final batteryMode:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$BatteryMode;

.field private final current:I

.field private final isCritical:Z

.field private final percentage:I

.field private final temperature:I

.field private final voltage:I


# direct methods
.method public constructor <init>(IIIIZLcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$BatteryMode;)V
    .locals 1

    const-string v0, "batteryMode"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 497
    invoke-direct {p0, v0}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput p1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;->percentage:I

    iput p2, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;->current:I

    iput p3, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;->voltage:I

    iput p4, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;->temperature:I

    iput-boolean p5, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;->isCritical:Z

    iput-object p6, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;->batteryMode:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$BatteryMode;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;IIIIZLcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$BatteryMode;ILjava/lang/Object;)Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    iget p1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;->percentage:I

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    iget p2, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;->current:I

    :cond_1
    move p8, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    iget p3, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;->voltage:I

    :cond_2
    move v0, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    iget p4, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;->temperature:I

    :cond_3
    move v1, p4

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    iget-boolean p5, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;->isCritical:Z

    :cond_4
    move v2, p5

    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_5

    iget-object p6, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;->batteryMode:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$BatteryMode;

    :cond_5
    move-object v3, p6

    move-object p2, p0

    move p3, p1

    move p4, p8

    move p5, v0

    move p6, v1

    move p7, v2

    move-object p8, v3

    invoke-virtual/range {p2 .. p8}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;->copy(IIIIZLcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$BatteryMode;)Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;->percentage:I

    return v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;->current:I

    return v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;->voltage:I

    return v0
.end method

.method public final component4()I
    .locals 1

    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;->temperature:I

    return v0
.end method

.method public final component5()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;->isCritical:Z

    return v0
.end method

.method public final component6()Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$BatteryMode;
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;->batteryMode:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$BatteryMode;

    return-object v0
.end method

.method public final copy(IIIIZLcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$BatteryMode;)Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;
    .locals 8

    const-string v0, "batteryMode"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;

    move-object v1, v0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move-object v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;-><init>(IIIIZLcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$BatteryMode;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;

    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;->percentage:I

    iget v1, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;->percentage:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;->current:I

    iget v1, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;->current:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;->voltage:I

    iget v1, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;->voltage:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;->temperature:I

    iget v1, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;->temperature:I

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;->isCritical:Z

    iget-boolean v1, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;->isCritical:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;->batteryMode:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$BatteryMode;

    iget-object p1, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;->batteryMode:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$BatteryMode;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getBatteryMode()Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$BatteryMode;
    .locals 1

    .line 496
    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;->batteryMode:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$BatteryMode;

    return-object v0
.end method

.method public final getCurrent()I
    .locals 1

    .line 492
    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;->current:I

    return v0
.end method

.method public final getPercentage()I
    .locals 1

    .line 491
    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;->percentage:I

    return v0
.end method

.method public final getTemperature()I
    .locals 1

    .line 494
    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;->temperature:I

    return v0
.end method

.method public final getVoltage()I
    .locals 1

    .line 493
    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;->voltage:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;->percentage:I

    invoke-static {v0}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;->current:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;->voltage:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;->temperature:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;->isCritical:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :cond_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;->batteryMode:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$BatteryMode;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    return v0
.end method

.method public final isCritical()Z
    .locals 1

    .line 495
    iget-boolean v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;->isCritical:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OnPowerStatus(percentage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;->percentage:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", current="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;->current:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", voltage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;->voltage:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", temperature="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;->temperature:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", isCritical="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;->isCritical:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", batteryMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$OnPowerStatus;->batteryMode:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PowerFeatureOutput$BatteryMode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
