.class public final Lcom/squareup/cardreader/CardReaderModule_ProvidePaymentProcessorFactory;
.super Ljava/lang/Object;
.source "CardReaderModule_ProvidePaymentProcessorFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/PaymentProcessor;",
        ">;"
    }
.end annotation


# instance fields
.field private final cardReaderInfoProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderListenersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/RealCardReaderListeners;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch;",
            ">;"
        }
    .end annotation
.end field

.field private final firmwareUpdaterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/FirmwareUpdater;",
            ">;"
        }
    .end annotation
.end field

.field private final magSwipeFailureFilterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/MagSwipeFailureFilter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/RealCardReaderListeners;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/MagSwipeFailureFilter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/FirmwareUpdater;",
            ">;)V"
        }
    .end annotation

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderModule_ProvidePaymentProcessorFactory;->cardReaderListenersProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p2, p0, Lcom/squareup/cardreader/CardReaderModule_ProvidePaymentProcessorFactory;->cardReaderProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p3, p0, Lcom/squareup/cardreader/CardReaderModule_ProvidePaymentProcessorFactory;->cardReaderInfoProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p4, p0, Lcom/squareup/cardreader/CardReaderModule_ProvidePaymentProcessorFactory;->magSwipeFailureFilterProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p5, p0, Lcom/squareup/cardreader/CardReaderModule_ProvidePaymentProcessorFactory;->firmwareUpdaterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/CardReaderModule_ProvidePaymentProcessorFactory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/RealCardReaderListeners;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/MagSwipeFailureFilter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/FirmwareUpdater;",
            ">;)",
            "Lcom/squareup/cardreader/CardReaderModule_ProvidePaymentProcessorFactory;"
        }
    .end annotation

    .line 51
    new-instance v6, Lcom/squareup/cardreader/CardReaderModule_ProvidePaymentProcessorFactory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/cardreader/CardReaderModule_ProvidePaymentProcessorFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static providePaymentProcessor(Lcom/squareup/cardreader/RealCardReaderListeners;Ljavax/inject/Provider;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/MagSwipeFailureFilter;Lcom/squareup/cardreader/FirmwareUpdater;)Lcom/squareup/cardreader/PaymentProcessor;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/RealCardReaderListeners;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch;",
            ">;",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            "Lcom/squareup/cardreader/MagSwipeFailureFilter;",
            "Lcom/squareup/cardreader/FirmwareUpdater;",
            ")",
            "Lcom/squareup/cardreader/PaymentProcessor;"
        }
    .end annotation

    .line 58
    invoke-static {p0, p1, p2, p3, p4}, Lcom/squareup/cardreader/CardReaderModule;->providePaymentProcessor(Lcom/squareup/cardreader/RealCardReaderListeners;Ljavax/inject/Provider;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/MagSwipeFailureFilter;Lcom/squareup/cardreader/FirmwareUpdater;)Lcom/squareup/cardreader/PaymentProcessor;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/PaymentProcessor;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/PaymentProcessor;
    .locals 5

    .line 42
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderModule_ProvidePaymentProcessorFactory;->cardReaderListenersProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/RealCardReaderListeners;

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderModule_ProvidePaymentProcessorFactory;->cardReaderProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/squareup/cardreader/CardReaderModule_ProvidePaymentProcessorFactory;->cardReaderInfoProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cardreader/CardReaderInfo;

    iget-object v3, p0, Lcom/squareup/cardreader/CardReaderModule_ProvidePaymentProcessorFactory;->magSwipeFailureFilterProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/MagSwipeFailureFilter;

    iget-object v4, p0, Lcom/squareup/cardreader/CardReaderModule_ProvidePaymentProcessorFactory;->firmwareUpdaterProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/cardreader/FirmwareUpdater;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/cardreader/CardReaderModule_ProvidePaymentProcessorFactory;->providePaymentProcessor(Lcom/squareup/cardreader/RealCardReaderListeners;Ljavax/inject/Provider;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/MagSwipeFailureFilter;Lcom/squareup/cardreader/FirmwareUpdater;)Lcom/squareup/cardreader/PaymentProcessor;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/cardreader/CardReaderModule_ProvidePaymentProcessorFactory;->get()Lcom/squareup/cardreader/PaymentProcessor;

    move-result-object v0

    return-object v0
.end method
