.class Lcom/squareup/cardreader/TimerApiLegacy$ExpiredTimer;
.super Ljava/util/TimerTask;
.source "TimerApiLegacy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/TimerApiLegacy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ExpiredTimer"
.end annotation


# instance fields
.field private final contextPtr:J

.field private final methodPtr:J

.field final synthetic this$0:Lcom/squareup/cardreader/TimerApiLegacy;

.field private final timerId:J


# direct methods
.method private constructor <init>(Lcom/squareup/cardreader/TimerApiLegacy;JJJ)V
    .locals 0

    .line 73
    iput-object p1, p0, Lcom/squareup/cardreader/TimerApiLegacy$ExpiredTimer;->this$0:Lcom/squareup/cardreader/TimerApiLegacy;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 74
    iput-wide p2, p0, Lcom/squareup/cardreader/TimerApiLegacy$ExpiredTimer;->timerId:J

    .line 75
    iput-wide p4, p0, Lcom/squareup/cardreader/TimerApiLegacy$ExpiredTimer;->methodPtr:J

    .line 76
    iput-wide p6, p0, Lcom/squareup/cardreader/TimerApiLegacy$ExpiredTimer;->contextPtr:J

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/cardreader/TimerApiLegacy;JJJLcom/squareup/cardreader/TimerApiLegacy$1;)V
    .locals 0

    .line 68
    invoke-direct/range {p0 .. p7}, Lcom/squareup/cardreader/TimerApiLegacy$ExpiredTimer;-><init>(Lcom/squareup/cardreader/TimerApiLegacy;JJJ)V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/cardreader/TimerApiLegacy$ExpiredTimer;)J
    .locals 2

    .line 68
    iget-wide v0, p0, Lcom/squareup/cardreader/TimerApiLegacy$ExpiredTimer;->timerId:J

    return-wide v0
.end method

.method static synthetic access$200(Lcom/squareup/cardreader/TimerApiLegacy$ExpiredTimer;)J
    .locals 2

    .line 68
    iget-wide v0, p0, Lcom/squareup/cardreader/TimerApiLegacy$ExpiredTimer;->methodPtr:J

    return-wide v0
.end method

.method static synthetic access$300(Lcom/squareup/cardreader/TimerApiLegacy$ExpiredTimer;)J
    .locals 2

    .line 68
    iget-wide v0, p0, Lcom/squareup/cardreader/TimerApiLegacy$ExpiredTimer;->contextPtr:J

    return-wide v0
.end method


# virtual methods
.method public synthetic lambda$run$0$TimerApiLegacy$ExpiredTimer()V
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/squareup/cardreader/TimerApiLegacy$ExpiredTimer;->this$0:Lcom/squareup/cardreader/TimerApiLegacy;

    invoke-static {v0, p0}, Lcom/squareup/cardreader/TimerApiLegacy;->access$500(Lcom/squareup/cardreader/TimerApiLegacy;Lcom/squareup/cardreader/TimerApiLegacy$ExpiredTimer;)V

    return-void
.end method

.method public run()V
    .locals 2

    .line 80
    iget-object v0, p0, Lcom/squareup/cardreader/TimerApiLegacy$ExpiredTimer;->this$0:Lcom/squareup/cardreader/TimerApiLegacy;

    invoke-static {v0}, Lcom/squareup/cardreader/TimerApiLegacy;->access$400(Lcom/squareup/cardreader/TimerApiLegacy;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$TimerApiLegacy$ExpiredTimer$cbzk2JqAcSN0aVRnDES1L7PMdZU;

    invoke-direct {v1, p0}, Lcom/squareup/cardreader/-$$Lambda$TimerApiLegacy$ExpiredTimer$cbzk2JqAcSN0aVRnDES1L7PMdZU;-><init>(Lcom/squareup/cardreader/TimerApiLegacy$ExpiredTimer;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
