.class public final synthetic Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I

.field public static final synthetic $EnumSwitchMapping$1:[I

.field public static final synthetic $EnumSwitchMapping$2:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 17

    invoke-static {}, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->values()[Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->CR_PAYMENT_CARD_ACTION_NONE:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->CR_PAYMENT_CARD_ACTION_INSERT:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->ordinal()I

    move-result v1

    const/4 v3, 0x2

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->CR_PAYMENT_CARD_ACTION_INSERT_AGAIN:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->ordinal()I

    move-result v1

    const/4 v4, 0x3

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->CR_PAYMENT_CARD_ACTION_SWIPE_TECHNICAL:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->ordinal()I

    move-result v1

    const/4 v5, 0x4

    aput v5, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->CR_PAYMENT_CARD_ACTION_SWIPE_SCHEME:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->ordinal()I

    move-result v1

    const/4 v6, 0x5

    aput v6, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->CR_PAYMENT_CARD_ACTION_SWIPE_AGAIN:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->ordinal()I

    move-result v1

    const/4 v7, 0x6

    aput v7, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->CR_PAYMENT_CARD_ACTION_INSERT_FROM_CONTACTLESS:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->ordinal()I

    move-result v1

    const/4 v8, 0x7

    aput v8, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->CR_PAYMENT_CARD_ACTION_CONTACTLESS_CARD_ERROR_TRY_ANOTHER_CARD:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->ordinal()I

    move-result v1

    const/16 v9, 0x8

    aput v9, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->CR_PAYMENT_CARD_ACTION_CONTACTLESS_CARD_ERROR_TRY_AGAIN:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->ordinal()I

    move-result v1

    const/16 v10, 0x9

    aput v10, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->CR_PAYMENT_CARD_ACTION_CONTACTLESS_SEE_PHONE_FOR_INSTRUCTION:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->ordinal()I

    move-result v1

    const/16 v11, 0xa

    aput v11, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->CR_PAYMENT_CARD_ACTION_CONTACTLESS_PRESENT_ONLY_ONE:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->ordinal()I

    move-result v1

    const/16 v12, 0xb

    aput v12, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->CR_PAYMENT_CARD_ACTION_CONTACTLESS_UNLOCK_PHONE_TO_PAY:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->ordinal()I

    move-result v1

    const/16 v13, 0xc

    aput v13, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->CR_PAYMENT_CARD_ACTION_CONTACTLESS_CARD_LIMIT_EXCEEDED_ERROR_TRY_ANOTHER_CARD:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->ordinal()I

    move-result v1

    const/16 v14, 0xd

    aput v14, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->CR_PAYMENT_CARD_ACTION_CONTACTLESS_CARD_LIMIT_EXCEEDED_INSERT_CARD:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->ordinal()I

    move-result v1

    const/16 v15, 0xe

    aput v15, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->CR_PAYMENT_CARD_ACTION_REQUEST_TAP:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->ordinal()I

    move-result v1

    const/16 v16, 0xf

    aput v16, v0, v1

    invoke-static {}, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;->values()[Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;->CR_PAYMENT_PAYMENT_RESULT_TERMINATED:Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;->CR_PAYMENT_PAYMENT_RESULT_SUCCESS_ICC_APPROVE:Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;->CR_PAYMENT_PAYMENT_RESULT_SUCCESS_ICC_APPROVE_WITH_SIGNATURE:Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;->ordinal()I

    move-result v1

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;->CR_PAYMENT_PAYMENT_RESULT_FAILURE_ICC_DECLINE:Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;->ordinal()I

    move-result v1

    aput v5, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;->CR_PAYMENT_PAYMENT_RESULT_FAILURE_ICC_REVERSE:Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;->ordinal()I

    move-result v1

    aput v6, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;->CR_PAYMENT_PAYMENT_RESULT_SUCCESS_MAGSWIPE:Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;->ordinal()I

    move-result v1

    aput v7, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;->CR_PAYMENT_PAYMENT_RESULT_SUCCESS_MAGSWIPE_SCHEME_FALLBACK:Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;->ordinal()I

    move-result v1

    aput v8, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;->CR_PAYMENT_PAYMENT_RESULT_SUCCESS_MAGSWIPE_TECHNICAL_FALLBACK:Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;->ordinal()I

    move-result v1

    aput v9, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;->CR_PAYMENT_PAYMENT_RESULT_TIMED_OUT:Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;->ordinal()I

    move-result v1

    aput v10, v0, v1

    invoke-static {}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->values()[Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_NONE:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_AMOUNT:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_AMOUNT_OK:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->ordinal()I

    move-result v1

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_APPROVED:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->ordinal()I

    move-result v1

    aput v5, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_CALL_YOUR_BANK:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->ordinal()I

    move-result v1

    aput v6, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_CANCEL_OR_ENTER:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->ordinal()I

    move-result v1

    aput v7, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_CARD_ERROR:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->ordinal()I

    move-result v1

    aput v8, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_DECLINED:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->ordinal()I

    move-result v1

    aput v9, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_ENTER_AMOUNT:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->ordinal()I

    move-result v1

    aput v10, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_ENTER_PIN:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->ordinal()I

    move-result v1

    aput v11, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_INCORRECT_PIN:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->ordinal()I

    move-result v1

    aput v12, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_INSERT_CARD:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->ordinal()I

    move-result v1

    aput v13, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_NOT_ACCEPTED:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->ordinal()I

    move-result v1

    aput v14, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_PIN_OK:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->ordinal()I

    move-result v1

    aput v15, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_PLEASE_WAIT:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->ordinal()I

    move-result v1

    aput v16, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_PROCESSING_ERROR:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->ordinal()I

    move-result v1

    const/16 v2, 0x10

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_REMOVE_CARD:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->ordinal()I

    move-result v1

    const/16 v2, 0x11

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_USE_CHIP_READER:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->ordinal()I

    move-result v1

    const/16 v2, 0x12

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_USE_MAG_STRIP:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->ordinal()I

    move-result v1

    const/16 v2, 0x13

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_TRY_AGAIN:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->ordinal()I

    move-result v1

    const/16 v2, 0x14

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_WELCOME:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->ordinal()I

    move-result v1

    const/16 v2, 0x15

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_PRESENT_CARD:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->ordinal()I

    move-result v1

    const/16 v2, 0x16

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_PROCESSING:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->ordinal()I

    move-result v1

    const/16 v2, 0x17

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_CARD_READ_OK_PLS_REMOVE_CARD:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->ordinal()I

    move-result v1

    const/16 v2, 0x18

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_PLS_INSERT_OR_SWIPE_CARD:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->ordinal()I

    move-result v1

    const/16 v2, 0x19

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_PLS_PRESENT_ONE_CARD:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->ordinal()I

    move-result v1

    const/16 v2, 0x1a

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_APPROVED_PLS_SIGN:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->ordinal()I

    move-result v1

    const/16 v2, 0x1b

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_AUTHORISING_PLS_WAIT:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->ordinal()I

    move-result v1

    const/16 v2, 0x1c

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_INSERT_SWIPE_OR_TRY_ANOTHER_CARD:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->ordinal()I

    move-result v1

    const/16 v2, 0x1d

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_PLS_INSERT_CARD:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->ordinal()I

    move-result v1

    const/16 v2, 0x1e

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_NO_MSG:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->ordinal()I

    move-result v1

    const/16 v2, 0x1f

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_SEE_PHONE_FOR_INSTRUCTIONS:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->ordinal()I

    move-result v1

    const/16 v2, 0x20

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_PRESENT_CARD_AGAIN:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->ordinal()I

    move-result v1

    const/16 v2, 0x21

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_UNLOCK_PHONE_TO_PAY:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->ordinal()I

    move-result v1

    const/16 v2, 0x22

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_TOO_MANY_TAP:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->ordinal()I

    move-result v1

    const/16 v2, 0x23

    aput v2, v0, v1

    return-void
.end method
