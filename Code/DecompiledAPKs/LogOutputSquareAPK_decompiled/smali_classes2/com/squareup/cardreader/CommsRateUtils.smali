.class public Lcom/squareup/cardreader/CommsRateUtils;
.super Ljava/lang/Object;
.source "CommsRateUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getShortName(Lcom/squareup/cardreader/lcr/CrCommsRate;)Ljava/lang/String;
    .locals 3

    .line 7
    sget-object v0, Lcom/squareup/cardreader/CommsRateUtils$1;->$SwitchMap$com$squareup$cardreader$lcr$CrCommsRate:[I

    invoke-virtual {p0}, Lcom/squareup/cardreader/lcr/CrCommsRate;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 23
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const-string p0, "Unknown comms rate %s"

    invoke-static {p0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const-string p0, "6000"

    return-object p0

    :pswitch_1
    const-string p0, "1500"

    return-object p0

    :pswitch_2
    const-string p0, "Man Stereo"

    return-object p0

    :pswitch_3
    const-string p0, "LFSR Stereo"

    return-object p0

    :pswitch_4
    const-string p0, "Man Mono"

    return-object p0

    :pswitch_5
    const-string p0, "LFSR Mono"

    return-object p0

    :pswitch_6
    const-string p0, "?"

    return-object p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
