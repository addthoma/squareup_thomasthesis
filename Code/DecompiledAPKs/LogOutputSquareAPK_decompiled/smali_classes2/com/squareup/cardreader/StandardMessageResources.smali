.class public Lcom/squareup/cardreader/StandardMessageResources;
.super Ljava/lang/Object;
.source "StandardMessageResources.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/StandardMessageResources$MessageResources;
    }
.end annotation


# static fields
.field private static messages:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;",
            "Lcom/squareup/cardreader/StandardMessageResources$MessageResources;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 29
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/squareup/cardreader/StandardMessageResources;->messages:Ljava/util/Map;

    .line 40
    sget-object v0, Lcom/squareup/cardreader/StandardMessageResources;->messages:Ljava/util/Map;

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_NONE:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    new-instance v2, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;

    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v4, Lcom/squareup/cardreader/R$string;->payment_failed:I

    sget v5, Lcom/squareup/cardreader/R$string;->card_not_supported_message:I

    invoke-direct {v2, v3, v4, v5}, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;-><init>(Lcom/squareup/glyph/GlyphTypeface$Glyph;II)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    sget-object v0, Lcom/squareup/cardreader/StandardMessageResources;->messages:Ljava/util/Map;

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_CALL_YOUR_BANK:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    new-instance v2, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;

    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v4, Lcom/squareup/cardreader/R$string;->call_your_bank_title:I

    sget v5, Lcom/squareup/cardreader/R$string;->call_your_bank_message:I

    invoke-direct {v2, v3, v4, v5}, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;-><init>(Lcom/squareup/glyph/GlyphTypeface$Glyph;II)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    sget-object v0, Lcom/squareup/cardreader/StandardMessageResources;->messages:Ljava/util/Map;

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_DECLINED:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    new-instance v2, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;

    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v4, Lcom/squareup/cardreader/R$string;->emv_declined:I

    sget v5, Lcom/squareup/cardreader/R$string;->call_your_bank_message:I

    invoke-direct {v2, v3, v4, v5}, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;-><init>(Lcom/squareup/glyph/GlyphTypeface$Glyph;II)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    sget-object v0, Lcom/squareup/cardreader/StandardMessageResources;->messages:Ljava/util/Map;

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_PROCESSING_ERROR:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    sget-object v2, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_DECLINED:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    invoke-static {v2}, Lcom/squareup/cardreader/StandardMessageResources;->forMessage(Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)Lcom/squareup/cardreader/StandardMessageResources$MessageResources;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    sget-object v0, Lcom/squareup/cardreader/StandardMessageResources;->messages:Ljava/util/Map;

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_NOT_ACCEPTED:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    new-instance v2, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;

    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_CHIP_CARD_NOT_USABLE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v4, Lcom/squareup/cardreader/R$string;->payment_failed:I

    sget v5, Lcom/squareup/cardreader/R$string;->card_not_supported_message:I

    invoke-direct {v2, v3, v4, v5}, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;-><init>(Lcom/squareup/glyph/GlyphTypeface$Glyph;II)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    sget-object v0, Lcom/squareup/cardreader/StandardMessageResources;->messages:Ljava/util/Map;

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_SEE_PHONE_FOR_INSTRUCTIONS:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    new-instance v2, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;

    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v4, Lcom/squareup/cardreader/R$string;->contactless_action_required_title:I

    sget v5, Lcom/squareup/cardreader/R$string;->contactless_action_required_message:I

    invoke-direct {v2, v3, v4, v5}, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;-><init>(Lcom/squareup/glyph/GlyphTypeface$Glyph;II)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    sget-object v0, Lcom/squareup/cardreader/StandardMessageResources;->messages:Ljava/util/Map;

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_UNLOCK_PHONE_TO_PAY:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    new-instance v2, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;

    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v4, Lcom/squareup/cardreader/R$string;->contactless_unlock_phone_to_pay_title:I

    sget v5, Lcom/squareup/cardreader/R$string;->contactless_unlock_phone_and_try_again_message:I

    invoke-direct {v2, v3, v4, v5}, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;-><init>(Lcom/squareup/glyph/GlyphTypeface$Glyph;II)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    sget-object v0, Lcom/squareup/cardreader/StandardMessageResources;->messages:Ljava/util/Map;

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_TRY_AGAIN:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    new-instance v2, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;

    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v4, Lcom/squareup/cardreader/R$string;->emv_std_msg_try_again_title:I

    sget v5, Lcom/squareup/cardreader/R$string;->emv_std_msg_try_again_message:I

    invoke-direct {v2, v3, v4, v5}, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;-><init>(Lcom/squareup/glyph/GlyphTypeface$Glyph;II)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    sget-object v0, Lcom/squareup/cardreader/StandardMessageResources;->messages:Ljava/util/Map;

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_TOO_MANY_TAP:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    new-instance v2, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;

    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v4, Lcom/squareup/cardreader/R$string;->payment_failed:I

    sget v5, Lcom/squareup/cardreader/R$string;->contactless_too_many_taps_message:I

    invoke-direct {v2, v3, v4, v5}, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;-><init>(Lcom/squareup/glyph/GlyphTypeface$Glyph;II)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    sget-object v0, Lcom/squareup/cardreader/StandardMessageResources;->messages:Ljava/util/Map;

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_INSERT_SWIPE_OR_TRY_ANOTHER_CARD:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    new-instance v2, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;

    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v4, Lcom/squareup/cardreader/R$string;->contactless_interface_unavailable_title:I

    sget v5, Lcom/squareup/cardreader/R$string;->contactless_interface_unavailable_message:I

    invoke-direct {v2, v3, v4, v5}, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;-><init>(Lcom/squareup/glyph/GlyphTypeface$Glyph;II)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static forMessage(Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)Lcom/squareup/cardreader/StandardMessageResources$MessageResources;
    .locals 2

    .line 108
    sget-object v0, Lcom/squareup/cardreader/StandardMessageResources;->messages:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const-string p0, "No particular message for %s. Defaulting to plain old \'Payment Failed\'."

    .line 110
    invoke-static {p0, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 112
    sget-object p0, Lcom/squareup/cardreader/StandardMessageResources;->messages:Ljava/util/Map;

    sget-object v0, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_NONE:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;

    return-object p0

    :cond_0
    return-object v0
.end method
