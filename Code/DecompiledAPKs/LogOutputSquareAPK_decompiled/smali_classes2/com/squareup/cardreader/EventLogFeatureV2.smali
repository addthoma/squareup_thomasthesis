.class public final Lcom/squareup/cardreader/EventLogFeatureV2;
.super Ljava/lang/Object;
.source "EventLogFeatureV2.kt"

# interfaces
.implements Lcom/squareup/cardreader/CanReset;
.implements Lcom/squareup/cardreader/EventLogFeature;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0018\u00002\u00020\u00012\u00020\u0002B\u001b\u0012\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u000e\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012J\u0008\u0010\u0013\u001a\u00020\u0014H\u0002J(\u0010\u0015\u001a\u00020\u00102\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u00192\u0006\u0010\u0011\u001a\u00020\u001bH\u0016J\u0008\u0010\u001c\u001a\u00020\u0010H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\t\u001a\u00020\nX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000b\u0010\u000c\"\u0004\u0008\r\u0010\u000eR\u0014\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/cardreader/EventLogFeatureV2;",
        "Lcom/squareup/cardreader/CanReset;",
        "Lcom/squareup/cardreader/EventLogFeature;",
        "posSender",
        "Lcom/squareup/cardreader/SendsToPos;",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$EventLogFeatureOutput;",
        "cardreaderProvider",
        "Lcom/squareup/cardreader/CardreaderPointerProvider;",
        "(Lcom/squareup/cardreader/SendsToPos;Lcom/squareup/cardreader/CardreaderPointerProvider;)V",
        "featurePointer",
        "Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_eventlog_t;",
        "getFeaturePointer",
        "()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_eventlog_t;",
        "setFeaturePointer",
        "(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_eventlog_t;)V",
        "handleMessage",
        "",
        "message",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderInput$EventLogFeatureMessage;",
        "initialize",
        "Lcom/squareup/cardreader/lcr/CrEventLogResult;",
        "onEventLogReceived",
        "timestamp",
        "",
        "event",
        "",
        "sourceCode",
        "",
        "resetIfInitilized",
        "cardreader-features_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final cardreaderProvider:Lcom/squareup/cardreader/CardreaderPointerProvider;

.field public featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_eventlog_t;

.field private final posSender:Lcom/squareup/cardreader/SendsToPos;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cardreader/SendsToPos<",
            "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$EventLogFeatureOutput;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/SendsToPos;Lcom/squareup/cardreader/CardreaderPointerProvider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/SendsToPos<",
            "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$EventLogFeatureOutput;",
            ">;",
            "Lcom/squareup/cardreader/CardreaderPointerProvider;",
            ")V"
        }
    .end annotation

    const-string v0, "posSender"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardreaderProvider"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/cardreader/EventLogFeatureV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    iput-object p2, p0, Lcom/squareup/cardreader/EventLogFeatureV2;->cardreaderProvider:Lcom/squareup/cardreader/CardreaderPointerProvider;

    return-void
.end method

.method private final initialize()Lcom/squareup/cardreader/lcr/CrEventLogResult;
    .locals 2

    .line 27
    iget-object v0, p0, Lcom/squareup/cardreader/EventLogFeatureV2;->cardreaderProvider:Lcom/squareup/cardreader/CardreaderPointerProvider;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardreaderPointerProvider;->cardreaderPointer()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/squareup/cardreader/lcr/EventlogFeatureNative;->eventlog_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_eventlog_t;

    move-result-object v0

    const-string v1, "EventlogFeatureNative.ev\u2026ardreaderPointer(), this)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/cardreader/EventLogFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_eventlog_t;

    .line 28
    sget-object v0, Lcom/squareup/cardreader/lcr/CrEventLogResult;->CR_EVENTLOG_RESULT_SUCCESS:Lcom/squareup/cardreader/lcr/CrEventLogResult;

    return-object v0
.end method


# virtual methods
.method public final getFeaturePointer()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_eventlog_t;
    .locals 2

    .line 17
    iget-object v0, p0, Lcom/squareup/cardreader/EventLogFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_eventlog_t;

    if-nez v0, :cond_0

    const-string v1, "featurePointer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final handleMessage(Lcom/squareup/cardreader/ReaderMessage$ReaderInput$EventLogFeatureMessage;)V
    .locals 1

    const-string v0, "message"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    instance-of p1, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$EventLogFeatureMessage$Initialize;

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/squareup/cardreader/EventLogFeatureV2;->initialize()Lcom/squareup/cardreader/lcr/CrEventLogResult;

    return-void

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public onEventLogReceived(JIILjava/lang/String;)V
    .locals 8

    const-string v0, "message"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    .line 49
    invoke-static {p5, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 50
    iget-object v0, p0, Lcom/squareup/cardreader/EventLogFeatureV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    .line 51
    new-instance v7, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$EventLogFeatureOutput$OnEventLogReceived;

    move-object v1, v7

    move-wide v2, p1

    move v4, p3

    move v5, p4

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$EventLogFeatureOutput$OnEventLogReceived;-><init>(JIILjava/lang/String;)V

    check-cast v7, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;

    .line 50
    invoke-interface {v0, v7}, Lcom/squareup/cardreader/SendsToPos;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V

    return-void
.end method

.method public resetIfInitilized()V
    .locals 2

    .line 32
    move-object v0, p0

    check-cast v0, Lcom/squareup/cardreader/EventLogFeatureV2;

    iget-object v0, v0, Lcom/squareup/cardreader/EventLogFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_eventlog_t;

    if-eqz v0, :cond_2

    .line 33
    iget-object v0, p0, Lcom/squareup/cardreader/EventLogFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_eventlog_t;

    const-string v1, "featurePointer"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {v0}, Lcom/squareup/cardreader/lcr/EventlogFeatureNative;->cr_eventlog_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_eventlog_t;)Lcom/squareup/cardreader/lcr/CrEventLogResult;

    .line 34
    iget-object v0, p0, Lcom/squareup/cardreader/EventLogFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_eventlog_t;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-static {v0}, Lcom/squareup/cardreader/lcr/EventlogFeatureNative;->cr_eventlog_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_eventlog_t;)Lcom/squareup/cardreader/lcr/CrEventLogResult;

    :cond_2
    return-void
.end method

.method public final setFeaturePointer(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_eventlog_t;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    iput-object p1, p0, Lcom/squareup/cardreader/EventLogFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_eventlog_t;

    return-void
.end method
