.class public interface abstract Lcom/squareup/cardreader/WirelessSearcher;
.super Ljava/lang/Object;
.source "WirelessSearcher.java"


# virtual methods
.method public abstract isSearching()Z
.end method

.method public abstract startSearch()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/cardreader/WirelessConnection;",
            ">;"
        }
    .end annotation
.end method

.method public abstract stopSearch()V
.end method
