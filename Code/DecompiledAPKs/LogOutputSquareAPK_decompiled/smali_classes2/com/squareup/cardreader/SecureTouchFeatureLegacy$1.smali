.class synthetic Lcom/squareup/cardreader/SecureTouchFeatureLegacy$1;
.super Ljava/lang/Object;
.source "SecureTouchFeatureLegacy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/SecureTouchFeatureLegacy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$cardreader$lcr$CrSecureTouchPinTry:[I

.field static final synthetic $SwitchMap$com$squareup$cardreader$lcr$CrsStmPinPadEventId:[I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 410
    invoke-static {}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->values()[Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy$1;->$SwitchMap$com$squareup$cardreader$lcr$CrsStmPinPadEventId:[I

    const/4 v0, 0x1

    :try_start_0
    sget-object v1, Lcom/squareup/cardreader/SecureTouchFeatureLegacy$1;->$SwitchMap$com$squareup$cardreader$lcr$CrsStmPinPadEventId:[I

    sget-object v2, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_BUTTON_DONE:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    invoke-virtual {v2}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->ordinal()I

    move-result v2

    aput v0, v1, v2
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 v1, 0x2

    :try_start_1
    sget-object v2, Lcom/squareup/cardreader/SecureTouchFeatureLegacy$1;->$SwitchMap$com$squareup$cardreader$lcr$CrsStmPinPadEventId:[I

    sget-object v3, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_BUTTON_CANCEL:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    invoke-virtual {v3}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->ordinal()I

    move-result v3

    aput v1, v2, v3
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    const/4 v2, 0x3

    :try_start_2
    sget-object v3, Lcom/squareup/cardreader/SecureTouchFeatureLegacy$1;->$SwitchMap$com$squareup$cardreader$lcr$CrsStmPinPadEventId:[I

    sget-object v4, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_SWIPE_CANCEL:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    invoke-virtual {v4}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->ordinal()I

    move-result v4

    aput v2, v3, v4
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    const/4 v3, 0x4

    :try_start_3
    sget-object v4, Lcom/squareup/cardreader/SecureTouchFeatureLegacy$1;->$SwitchMap$com$squareup$cardreader$lcr$CrsStmPinPadEventId:[I

    sget-object v5, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_BUTTON_CLEAR:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    invoke-virtual {v5}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->ordinal()I

    move-result v5

    aput v3, v4, v5
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    :try_start_4
    sget-object v4, Lcom/squareup/cardreader/SecureTouchFeatureLegacy$1;->$SwitchMap$com$squareup$cardreader$lcr$CrsStmPinPadEventId:[I

    sget-object v5, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_BUTTON_BACKSPACE:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    invoke-virtual {v5}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->ordinal()I

    move-result v5

    const/4 v6, 0x5

    aput v6, v4, v5
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :catch_4
    :try_start_5
    sget-object v4, Lcom/squareup/cardreader/SecureTouchFeatureLegacy$1;->$SwitchMap$com$squareup$cardreader$lcr$CrsStmPinPadEventId:[I

    sget-object v5, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_BUTTON_ACCESSIBILITY:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    invoke-virtual {v5}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->ordinal()I

    move-result v5

    const/4 v6, 0x6

    aput v6, v4, v5
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_5

    :catch_5
    :try_start_6
    sget-object v4, Lcom/squareup/cardreader/SecureTouchFeatureLegacy$1;->$SwitchMap$com$squareup$cardreader$lcr$CrsStmPinPadEventId:[I

    sget-object v5, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_BUTTON_GENERIC_NUMBER:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    invoke-virtual {v5}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->ordinal()I

    move-result v5

    const/4 v6, 0x7

    aput v6, v4, v5
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_6

    :catch_6
    :try_start_7
    sget-object v4, Lcom/squareup/cardreader/SecureTouchFeatureLegacy$1;->$SwitchMap$com$squareup$cardreader$lcr$CrsStmPinPadEventId:[I

    sget-object v5, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_TOUCH_DOWN:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    invoke-virtual {v5}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->ordinal()I

    move-result v5

    const/16 v6, 0x8

    aput v6, v4, v5
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_7

    :catch_7
    :try_start_8
    sget-object v4, Lcom/squareup/cardreader/SecureTouchFeatureLegacy$1;->$SwitchMap$com$squareup$cardreader$lcr$CrsStmPinPadEventId:[I

    sget-object v5, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_ACCESS_OUT_OF_BOUNDS:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    invoke-virtual {v5}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->ordinal()I

    move-result v5

    const/16 v6, 0x9

    aput v6, v4, v5
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_8

    :catch_8
    :try_start_9
    sget-object v4, Lcom/squareup/cardreader/SecureTouchFeatureLegacy$1;->$SwitchMap$com$squareup$cardreader$lcr$CrsStmPinPadEventId:[I

    sget-object v5, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_BUTTON_INVALID:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    invoke-virtual {v5}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->ordinal()I

    move-result v5

    const/16 v6, 0xa

    aput v6, v4, v5
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_9

    :catch_9
    :try_start_a
    sget-object v4, Lcom/squareup/cardreader/SecureTouchFeatureLegacy$1;->$SwitchMap$com$squareup$cardreader$lcr$CrsStmPinPadEventId:[I

    sget-object v5, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_TOO_MANY_TOUCHES:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    invoke-virtual {v5}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->ordinal()I

    move-result v5

    const/16 v6, 0xb

    aput v6, v4, v5
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_a

    :catch_a
    :try_start_b
    sget-object v4, Lcom/squareup/cardreader/SecureTouchFeatureLegacy$1;->$SwitchMap$com$squareup$cardreader$lcr$CrsStmPinPadEventId:[I

    sget-object v5, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_TOO_MANY_DIGITS:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    invoke-virtual {v5}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->ordinal()I

    move-result v5

    const/16 v6, 0xc

    aput v6, v4, v5
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_b

    :catch_b
    :try_start_c
    sget-object v4, Lcom/squareup/cardreader/SecureTouchFeatureLegacy$1;->$SwitchMap$com$squareup$cardreader$lcr$CrsStmPinPadEventId:[I

    sget-object v5, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_TOO_FEW_DIGITS:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    invoke-virtual {v5}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->ordinal()I

    move-result v5

    const/16 v6, 0xd

    aput v6, v4, v5
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_c

    :catch_c
    :try_start_d
    sget-object v4, Lcom/squareup/cardreader/SecureTouchFeatureLegacy$1;->$SwitchMap$com$squareup$cardreader$lcr$CrsStmPinPadEventId:[I

    sget-object v5, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_BUTTON_0:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    invoke-virtual {v5}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->ordinal()I

    move-result v5

    const/16 v6, 0xe

    aput v6, v4, v5
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_d

    :catch_d
    :try_start_e
    sget-object v4, Lcom/squareup/cardreader/SecureTouchFeatureLegacy$1;->$SwitchMap$com$squareup$cardreader$lcr$CrsStmPinPadEventId:[I

    sget-object v5, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_BUTTON_1:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    invoke-virtual {v5}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->ordinal()I

    move-result v5

    const/16 v6, 0xf

    aput v6, v4, v5
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_e

    :catch_e
    :try_start_f
    sget-object v4, Lcom/squareup/cardreader/SecureTouchFeatureLegacy$1;->$SwitchMap$com$squareup$cardreader$lcr$CrsStmPinPadEventId:[I

    sget-object v5, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_BUTTON_2:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    invoke-virtual {v5}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->ordinal()I

    move-result v5

    const/16 v6, 0x10

    aput v6, v4, v5
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_f

    :catch_f
    :try_start_10
    sget-object v4, Lcom/squareup/cardreader/SecureTouchFeatureLegacy$1;->$SwitchMap$com$squareup$cardreader$lcr$CrsStmPinPadEventId:[I

    sget-object v5, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_BUTTON_3:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    invoke-virtual {v5}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->ordinal()I

    move-result v5

    const/16 v6, 0x11

    aput v6, v4, v5
    :try_end_10
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10 .. :try_end_10} :catch_10

    :catch_10
    :try_start_11
    sget-object v4, Lcom/squareup/cardreader/SecureTouchFeatureLegacy$1;->$SwitchMap$com$squareup$cardreader$lcr$CrsStmPinPadEventId:[I

    sget-object v5, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_BUTTON_4:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    invoke-virtual {v5}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->ordinal()I

    move-result v5

    const/16 v6, 0x12

    aput v6, v4, v5
    :try_end_11
    .catch Ljava/lang/NoSuchFieldError; {:try_start_11 .. :try_end_11} :catch_11

    :catch_11
    :try_start_12
    sget-object v4, Lcom/squareup/cardreader/SecureTouchFeatureLegacy$1;->$SwitchMap$com$squareup$cardreader$lcr$CrsStmPinPadEventId:[I

    sget-object v5, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_BUTTON_5:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    invoke-virtual {v5}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->ordinal()I

    move-result v5

    const/16 v6, 0x13

    aput v6, v4, v5
    :try_end_12
    .catch Ljava/lang/NoSuchFieldError; {:try_start_12 .. :try_end_12} :catch_12

    :catch_12
    :try_start_13
    sget-object v4, Lcom/squareup/cardreader/SecureTouchFeatureLegacy$1;->$SwitchMap$com$squareup$cardreader$lcr$CrsStmPinPadEventId:[I

    sget-object v5, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_BUTTON_6:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    invoke-virtual {v5}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->ordinal()I

    move-result v5

    const/16 v6, 0x14

    aput v6, v4, v5
    :try_end_13
    .catch Ljava/lang/NoSuchFieldError; {:try_start_13 .. :try_end_13} :catch_13

    :catch_13
    :try_start_14
    sget-object v4, Lcom/squareup/cardreader/SecureTouchFeatureLegacy$1;->$SwitchMap$com$squareup$cardreader$lcr$CrsStmPinPadEventId:[I

    sget-object v5, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_BUTTON_7:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    invoke-virtual {v5}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->ordinal()I

    move-result v5

    const/16 v6, 0x15

    aput v6, v4, v5
    :try_end_14
    .catch Ljava/lang/NoSuchFieldError; {:try_start_14 .. :try_end_14} :catch_14

    :catch_14
    :try_start_15
    sget-object v4, Lcom/squareup/cardreader/SecureTouchFeatureLegacy$1;->$SwitchMap$com$squareup$cardreader$lcr$CrsStmPinPadEventId:[I

    sget-object v5, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_BUTTON_8:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    invoke-virtual {v5}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->ordinal()I

    move-result v5

    const/16 v6, 0x16

    aput v6, v4, v5
    :try_end_15
    .catch Ljava/lang/NoSuchFieldError; {:try_start_15 .. :try_end_15} :catch_15

    :catch_15
    :try_start_16
    sget-object v4, Lcom/squareup/cardreader/SecureTouchFeatureLegacy$1;->$SwitchMap$com$squareup$cardreader$lcr$CrsStmPinPadEventId:[I

    sget-object v5, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->CRS_STM_EVENT_BUTTON_9:Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;

    invoke-virtual {v5}, Lcom/squareup/cardreader/lcr/CrsStmPinPadEventId;->ordinal()I

    move-result v5

    const/16 v6, 0x17

    aput v6, v4, v5
    :try_end_16
    .catch Ljava/lang/NoSuchFieldError; {:try_start_16 .. :try_end_16} :catch_16

    .line 216
    :catch_16
    invoke-static {}, Lcom/squareup/cardreader/lcr/CrSecureTouchPinTry;->values()[Lcom/squareup/cardreader/lcr/CrSecureTouchPinTry;

    move-result-object v4

    array-length v4, v4

    new-array v4, v4, [I

    sput-object v4, Lcom/squareup/cardreader/SecureTouchFeatureLegacy$1;->$SwitchMap$com$squareup$cardreader$lcr$CrSecureTouchPinTry:[I

    :try_start_17
    sget-object v4, Lcom/squareup/cardreader/SecureTouchFeatureLegacy$1;->$SwitchMap$com$squareup$cardreader$lcr$CrSecureTouchPinTry:[I

    sget-object v5, Lcom/squareup/cardreader/lcr/CrSecureTouchPinTry;->CR_SECURE_TOUCH_MODE_PIN_TRY_NONE:Lcom/squareup/cardreader/lcr/CrSecureTouchPinTry;

    invoke-virtual {v5}, Lcom/squareup/cardreader/lcr/CrSecureTouchPinTry;->ordinal()I

    move-result v5

    aput v0, v4, v5
    :try_end_17
    .catch Ljava/lang/NoSuchFieldError; {:try_start_17 .. :try_end_17} :catch_17

    :catch_17
    :try_start_18
    sget-object v0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy$1;->$SwitchMap$com$squareup$cardreader$lcr$CrSecureTouchPinTry:[I

    sget-object v4, Lcom/squareup/cardreader/lcr/CrSecureTouchPinTry;->CR_SECURE_TOUCH_MODE_PIN_TRY_FIRST:Lcom/squareup/cardreader/lcr/CrSecureTouchPinTry;

    invoke-virtual {v4}, Lcom/squareup/cardreader/lcr/CrSecureTouchPinTry;->ordinal()I

    move-result v4

    aput v1, v0, v4
    :try_end_18
    .catch Ljava/lang/NoSuchFieldError; {:try_start_18 .. :try_end_18} :catch_18

    :catch_18
    :try_start_19
    sget-object v0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy$1;->$SwitchMap$com$squareup$cardreader$lcr$CrSecureTouchPinTry:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrSecureTouchPinTry;->CR_SECURE_TOUCH_MODE_PIN_TRY_RETRY:Lcom/squareup/cardreader/lcr/CrSecureTouchPinTry;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrSecureTouchPinTry;->ordinal()I

    move-result v1

    aput v2, v0, v1
    :try_end_19
    .catch Ljava/lang/NoSuchFieldError; {:try_start_19 .. :try_end_19} :catch_19

    :catch_19
    :try_start_1a
    sget-object v0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy$1;->$SwitchMap$com$squareup$cardreader$lcr$CrSecureTouchPinTry:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrSecureTouchPinTry;->CR_SECURE_TOUCH_MODE_PIN_TRY_FINAL:Lcom/squareup/cardreader/lcr/CrSecureTouchPinTry;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrSecureTouchPinTry;->ordinal()I

    move-result v1

    aput v3, v0, v1
    :try_end_1a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1a .. :try_end_1a} :catch_1a

    :catch_1a
    return-void
.end method
