.class public final Lcom/squareup/cardreader/GlobalCardReaderModule_ProdWithoutCardReaderFactory_ProvideRealCardReaderFactoryFactory;
.super Ljava/lang/Object;
.source "GlobalCardReaderModule_ProdWithoutCardReaderFactory_ProvideRealCardReaderFactoryFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/RealCardReaderFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final cardReaderHubProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final module:Lcom/squareup/cardreader/GlobalCardReaderModule$ProdWithoutCardReaderFactory;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/GlobalCardReaderModule$ProdWithoutCardReaderFactory;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/GlobalCardReaderModule$ProdWithoutCardReaderFactory;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/cardreader/GlobalCardReaderModule_ProdWithoutCardReaderFactory_ProvideRealCardReaderFactoryFactory;->module:Lcom/squareup/cardreader/GlobalCardReaderModule$ProdWithoutCardReaderFactory;

    .line 28
    iput-object p2, p0, Lcom/squareup/cardreader/GlobalCardReaderModule_ProdWithoutCardReaderFactory_ProvideRealCardReaderFactoryFactory;->cardReaderHubProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/cardreader/GlobalCardReaderModule_ProdWithoutCardReaderFactory_ProvideRealCardReaderFactoryFactory;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Lcom/squareup/cardreader/GlobalCardReaderModule$ProdWithoutCardReaderFactory;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/GlobalCardReaderModule_ProdWithoutCardReaderFactory_ProvideRealCardReaderFactoryFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/GlobalCardReaderModule$ProdWithoutCardReaderFactory;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/cardreader/GlobalCardReaderModule_ProdWithoutCardReaderFactory_ProvideRealCardReaderFactoryFactory;"
        }
    .end annotation

    .line 40
    new-instance v0, Lcom/squareup/cardreader/GlobalCardReaderModule_ProdWithoutCardReaderFactory_ProvideRealCardReaderFactoryFactory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/cardreader/GlobalCardReaderModule_ProdWithoutCardReaderFactory_ProvideRealCardReaderFactoryFactory;-><init>(Lcom/squareup/cardreader/GlobalCardReaderModule$ProdWithoutCardReaderFactory;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideRealCardReaderFactory(Lcom/squareup/cardreader/GlobalCardReaderModule$ProdWithoutCardReaderFactory;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/settings/server/Features;)Lcom/squareup/cardreader/RealCardReaderFactory;
    .locals 0

    .line 46
    invoke-virtual {p0, p1, p2}, Lcom/squareup/cardreader/GlobalCardReaderModule$ProdWithoutCardReaderFactory;->provideRealCardReaderFactory(Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/settings/server/Features;)Lcom/squareup/cardreader/RealCardReaderFactory;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/RealCardReaderFactory;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/RealCardReaderFactory;
    .locals 3

    .line 34
    iget-object v0, p0, Lcom/squareup/cardreader/GlobalCardReaderModule_ProdWithoutCardReaderFactory_ProvideRealCardReaderFactoryFactory;->module:Lcom/squareup/cardreader/GlobalCardReaderModule$ProdWithoutCardReaderFactory;

    iget-object v1, p0, Lcom/squareup/cardreader/GlobalCardReaderModule_ProdWithoutCardReaderFactory_ProvideRealCardReaderFactoryFactory;->cardReaderHubProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/CardReaderHub;

    iget-object v2, p0, Lcom/squareup/cardreader/GlobalCardReaderModule_ProdWithoutCardReaderFactory_ProvideRealCardReaderFactoryFactory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/settings/server/Features;

    invoke-static {v0, v1, v2}, Lcom/squareup/cardreader/GlobalCardReaderModule_ProdWithoutCardReaderFactory_ProvideRealCardReaderFactoryFactory;->provideRealCardReaderFactory(Lcom/squareup/cardreader/GlobalCardReaderModule$ProdWithoutCardReaderFactory;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/settings/server/Features;)Lcom/squareup/cardreader/RealCardReaderFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/cardreader/GlobalCardReaderModule_ProdWithoutCardReaderFactory_ProvideRealCardReaderFactoryFactory;->get()Lcom/squareup/cardreader/RealCardReaderFactory;

    move-result-object v0

    return-object v0
.end method
