.class Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_badEventSink;
.super Ljava/lang/Object;
.source "DaggerAudioCardReaderContextComponent.java"

# interfaces
.implements Ljavax/inject/Provider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "com_squareup_cardreader_NonX2CardReaderContextParent_badEventSink"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljavax/inject/Provider<",
        "Lcom/squareup/badbus/BadEventSink;",
        ">;"
    }
.end annotation


# instance fields
.field private final nonX2CardReaderContextParent:Lcom/squareup/cardreader/NonX2CardReaderContextParent;


# direct methods
.method constructor <init>(Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V
    .locals 0

    .line 918
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 919
    iput-object p1, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_badEventSink;->nonX2CardReaderContextParent:Lcom/squareup/cardreader/NonX2CardReaderContextParent;

    return-void
.end method


# virtual methods
.method public get()Lcom/squareup/badbus/BadEventSink;
    .locals 2

    .line 924
    iget-object v0, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_badEventSink;->nonX2CardReaderContextParent:Lcom/squareup/cardreader/NonX2CardReaderContextParent;

    invoke-interface {v0}, Lcom/squareup/cardreader/NonX2CardReaderContextParent;->badEventSink()Lcom/squareup/badbus/BadEventSink;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable component method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/badbus/BadEventSink;

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 914
    invoke-virtual {p0}, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_badEventSink;->get()Lcom/squareup/badbus/BadEventSink;

    move-result-object v0

    return-object v0
.end method
