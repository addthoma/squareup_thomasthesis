.class public abstract Lcom/squareup/cardreader/ReleaseCardReaderCompleteModule;
.super Ljava/lang/Object;
.source "ReleaseCardReaderCompleteModule.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/cardreader/dagger/CardReaderCompleteModule;,
        Lcom/squareup/cardreader/GlobalCardReaderModule$Prod;,
        Lcom/squareup/cardreader/ble/GlobalBleModule$Prod;,
        Lcom/squareup/ms/ReleaseMinesweeperModule;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
