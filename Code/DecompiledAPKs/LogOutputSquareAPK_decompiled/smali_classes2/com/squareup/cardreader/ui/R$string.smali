.class public final Lcom/squareup/cardreader/ui/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/ui/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final battery_level_hud_content:I = 0x7f12016a

.field public static final ble_pairing_failed_title:I = 0x7f120185

.field public static final ble_pairing_failed_version_message:I = 0x7f120186

.field public static final ble_pairing_failed_version_title:I = 0x7f120187

.field public static final blocked_audio_message:I = 0x7f120188

.field public static final blocked_audio_title:I = 0x7f120189

.field public static final card_reader_details_help_message:I = 0x7f12033a

.field public static final card_reader_details_help_message_no_contactless:I = 0x7f12033b

.field public static final check_compatibility_url:I = 0x7f1203ff

.field public static final emv_app_update_required_button:I = 0x7f120a43

.field public static final emv_app_update_required_message:I = 0x7f120a44

.field public static final emv_app_update_required_title:I = 0x7f120a45

.field public static final emv_card_removed_msg:I = 0x7f120a4a

.field public static final emv_card_removed_title:I = 0x7f120a4c

.field public static final emv_contact_support:I = 0x7f120a4d

.field public static final emv_device_unsupported_msg:I = 0x7f120a4f

.field public static final emv_device_unsupported_title:I = 0x7f120a50

.field public static final emv_fwup_fail_msg:I = 0x7f120a53

.field public static final emv_fwup_fail_title:I = 0x7f120a54

.field public static final emv_must_dip_msg:I = 0x7f120a55

.field public static final emv_must_dip_title:I = 0x7f120a56

.field public static final emv_reader_error_msg:I = 0x7f120a5e

.field public static final emv_reader_tampered_msg:I = 0x7f120a5f

.field public static final emv_reader_tampered_title:I = 0x7f120a60

.field public static final emv_request_tap_payment_flow_card_message:I = 0x7f120a64

.field public static final emv_request_tap_payment_flow_card_title:I = 0x7f120a65

.field public static final emv_securesession_denied_msg:I = 0x7f120a68

.field public static final emv_securesession_failed_msg:I = 0x7f120a69

.field public static final emv_securesession_failed_title:I = 0x7f120a6a

.field public static final emv_unknown_error:I = 0x7f120a6f

.field public static final emv_warning_screen_done:I = 0x7f120a70

.field public static final gen2_denial_body:I = 0x7f120ae6

.field public static final gen2_denial_cancel:I = 0x7f120ae7

.field public static final gen2_denial_confirm:I = 0x7f120ae8

.field public static final gen2_denial_learn_more:I = 0x7f120ae9

.field public static final gen2_denial_title:I = 0x7f120aea

.field public static final gen2_eol_learn_more_url:I = 0x7f120aeb

.field public static final gen2_eol_request_reader_url:I = 0x7f120aec

.field public static final get_new_reader:I = 0x7f120af1

.field public static final get_started:I = 0x7f120af3

.field public static final go_to_google_play:I = 0x7f120b45

.field public static final hud_charge_reader_charging_title:I = 0x7f120be0

.field public static final hud_charge_reader_dead_title:I = 0x7f120be1

.field public static final hud_charge_reader_low_title:I = 0x7f120be2

.field public static final hud_charge_reader_message:I = 0x7f120be3

.field public static final hud_chip_reader_connected:I = 0x7f120be4

.field public static final hud_chip_reader_disconnected:I = 0x7f120be5

.field public static final hud_contactless_chip_reader_connected:I = 0x7f120be6

.field public static final hud_contactless_chip_reader_disconnected:I = 0x7f120be7

.field public static final hud_legacy_reader_connected:I = 0x7f120be8

.field public static final hud_legacy_reader_disconnected:I = 0x7f120be9

.field public static final hud_miura_reader_disconnected:I = 0x7f120bea

.field public static final messagebar_reader_fwup_blocking_progress:I = 0x7f120fc9

.field public static final messagebar_reader_offline_swipe_only:I = 0x7f120fd1

.field public static final native_library_load_error_msg:I = 0x7f121050

.field public static final native_library_load_error_msg_blocked:I = 0x7f121051

.field public static final native_library_load_error_title:I = 0x7f121052

.field public static final nfc_enabled_visit_settings_button:I = 0x7f121077

.field public static final nfc_enabled_warning_message:I = 0x7f121078

.field public static final nfc_enabled_warning_title:I = 0x7f121079

.field public static final o1_reminder_ja_body:I = 0x7f1210c7

.field public static final o1_reminder_ja_confirm:I = 0x7f1210c8

.field public static final o1_reminder_ja_title:I = 0x7f1210c9

.field public static final o1_reminder_ja_url:I = 0x7f1210ca

.field public static final pairing_confirmation_wrong_reader:I = 0x7f12130b

.field public static final pairing_fallback_help_url:I = 0x7f12130d

.field public static final pairing_help:I = 0x7f12130f

.field public static final pairing_help_devices:I = 0x7f121310

.field public static final pairing_help_support:I = 0x7f121311

.field public static final pairing_help_video:I = 0x7f121312

.field public static final pairing_screen_help:I = 0x7f121318

.field public static final pairing_screen_help_verbose:I = 0x7f121319

.field public static final pairing_screen_instructions:I = 0x7f12131a

.field public static final pairing_screen_title:I = 0x7f12131b

.field public static final payment_devices_connect_reader:I = 0x7f1213b5

.field public static final payment_failed_above_maximum:I = 0x7f1213be

.field public static final payment_failed_below_minimum:I = 0x7f1213bf

.field public static final payment_failed_gift_card_not_charged:I = 0x7f1213c1

.field public static final play_store_intent_uri:I = 0x7f12143c

.field public static final please_check_your_network_connection:I = 0x7f12143f

.field public static final please_contact_support:I = 0x7f121441

.field public static final please_restore_internet_connectivity:I = 0x7f121444

.field public static final please_try_again_or_contact_support:I = 0x7f121446

.field public static final please_wait:I = 0x7f121448

.field public static final processing_payments_many:I = 0x7f1214f5

.field public static final processing_payments_one:I = 0x7f1214f6

.field public static final reader_battery_very_low:I = 0x7f121568

.field public static final reader_detail_accepts_name:I = 0x7f12156a

.field public static final reader_detail_battery_name:I = 0x7f12156d

.field public static final reader_detail_battery_percentage:I = 0x7f12156e

.field public static final reader_detail_connection_name:I = 0x7f121571

.field public static final reader_detail_firmware_name:I = 0x7f121572

.field public static final reader_detail_forget_reader:I = 0x7f121573

.field public static final reader_detail_identify_reader:I = 0x7f121574

.field public static final reader_detail_nickname_message:I = 0x7f121575

.field public static final reader_detail_serial_number_name:I = 0x7f121576

.field public static final reader_detail_serial_number_name_short:I = 0x7f121577

.field public static final reader_detail_status_name:I = 0x7f12157a

.field public static final reader_failed_after_rebooting_fwup_message:I = 0x7f12157f

.field public static final reader_failed_to_connect:I = 0x7f121580

.field public static final reader_message_warning_declined_card:I = 0x7f121581

.field public static final reader_message_warning_declined_card_contactless:I = 0x7f121582

.field public static final reader_message_warning_reinsertion:I = 0x7f121583

.field public static final reader_status_battery_advice:I = 0x7f121589

.field public static final reader_status_battery_dead_headline:I = 0x7f12158a

.field public static final reader_status_battery_low_headline:I = 0x7f12158b

.field public static final reader_status_battery_percent_confirmation:I = 0x7f12158c

.field public static final reader_status_battery_percent_r12:I = 0x7f12158d

.field public static final reader_status_connecting_advice:I = 0x7f12158e

.field public static final reader_status_connecting_advice_r6_taking_forever:I = 0x7f12158f

.field public static final reader_status_connecting_headline:I = 0x7f121590

.field public static final reader_status_fwup_advice:I = 0x7f121591

.field public static final reader_status_fwup_checking_for_updates_advice:I = 0x7f121592

.field public static final reader_status_fwup_checking_for_updates_headline:I = 0x7f121593

.field public static final reader_status_fwup_failed_headline:I = 0x7f121594

.field public static final reader_status_fwup_inprogress_headline:I = 0x7f121595

.field public static final reader_status_fwup_rebooting_headline:I = 0x7f121596

.field public static final reader_status_offline_r12_advice:I = 0x7f121597

.field public static final reader_status_offline_r12_headline:I = 0x7f121598

.field public static final reader_status_offline_r6_advice:I = 0x7f121599

.field public static final reader_status_offline_r6_headline:I = 0x7f12159a

.field public static final reader_status_ss_connecting_advice:I = 0x7f12159b

.field public static final reader_status_ss_connecting_headline:I = 0x7f12159c

.field public static final reader_status_tamper_advice:I = 0x7f12159d

.field public static final reader_status_tamper_headline:I = 0x7f12159e

.field public static final reader_status_unavailable_advice:I = 0x7f12159f

.field public static final reader_status_unavailable_headline:I = 0x7f1215a0

.field public static final reader_status_wired_connect_failed_advice:I = 0x7f1215a1

.field public static final reader_status_wireless_connect_failed_advice:I = 0x7f1215a2

.field public static final reader_timed_out_waiting_for_card_message:I = 0x7f1215a3

.field public static final reader_timed_out_waiting_for_card_title:I = 0x7f1215a4

.field public static final remove_reader_heading:I = 0x7f121671

.field public static final remove_reader_message:I = 0x7f121672

.field public static final sample_rate_unset_message:I = 0x7f12176c

.field public static final sample_rate_unset_title:I = 0x7f12176d

.field public static final secure_session_required_for_dip_title:I = 0x7f121797

.field public static final secure_session_required_for_swipe_message:I = 0x7f121798

.field public static final secure_session_required_for_swipe_title:I = 0x7f121799

.field public static final smart_reader_updating:I = 0x7f121827

.field public static final square_reader_help:I = 0x7f12189a

.field public static final square_readers:I = 0x7f12189d

.field public static final square_shop:I = 0x7f1218a3

.field public static final support_center:I = 0x7f1218d9

.field public static final support_center_r12_help_url:I = 0x7f1218da

.field public static final support_center_r6_help_url:I = 0x7f1218db

.field public static final supported_hardware_url:I = 0x7f1218e0

.field public static final supported_reader_url:I = 0x7f1218e1

.field public static final swipe_card_to_charge:I = 0x7f1218e2

.field public static final swipe_chip_cards_url:I = 0x7f1218ec

.field public static final talk_back_go_to_accessibility_setting:I = 0x7f121912

.field public static final talk_back_please_close:I = 0x7f121913

.field public static final talk_back_please_close_header:I = 0x7f121914

.field public static final talk_back_warning_header:I = 0x7f121915

.field public static final talk_back_warning_message:I = 0x7f121916

.field public static final terminal_damaged_message:I = 0x7f121955

.field public static final terminal_damaged_title:I = 0x7f121956

.field public static final tms_invalid_contact_support:I = 0x7f1219e4

.field public static final tms_invalid_dismiss:I = 0x7f1219e5

.field public static final tms_invalid_message:I = 0x7f1219e6

.field public static final tms_invalid_title:I = 0x7f1219e7

.field public static final try_again:I = 0x7f121a67

.field public static final unable_to_process_chip_cards:I = 0x7f121ae1

.field public static final updating_r12_message:I = 0x7f121af6

.field public static final updating_r12_title:I = 0x7f121af7

.field public static final uppercase_my_readers:I = 0x7f121b52

.field public static final uppercase_nickname:I = 0x7f121b53

.field public static final uppercase_pairing_fallback_available_readers:I = 0x7f121b5b

.field public static final uppercase_reader_information:I = 0x7f121b69


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
