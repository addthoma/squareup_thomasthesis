.class public final Lcom/squareup/cardreader/ui/api/ButtonDescriptor;
.super Ljava/lang/Object;
.source "ButtonDescriptor.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field public final callback:Lcom/squareup/debounce/DebouncedOnClickListener;

.field public final enabled:Z

.field public final localizedText:Ljava/lang/String;

.field public final textId:I


# direct methods
.method public constructor <init>(ILcom/squareup/debounce/DebouncedOnClickListener;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 21
    invoke-direct {p0, p1, v0, p2, v1}, Lcom/squareup/cardreader/ui/api/ButtonDescriptor;-><init>(ILjava/lang/String;Lcom/squareup/debounce/DebouncedOnClickListener;Z)V

    return-void
.end method

.method private constructor <init>(ILjava/lang/String;Lcom/squareup/debounce/DebouncedOnClickListener;Z)V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput p1, p0, Lcom/squareup/cardreader/ui/api/ButtonDescriptor;->textId:I

    .line 27
    iput-object p2, p0, Lcom/squareup/cardreader/ui/api/ButtonDescriptor;->localizedText:Ljava/lang/String;

    .line 28
    iput-object p3, p0, Lcom/squareup/cardreader/ui/api/ButtonDescriptor;->callback:Lcom/squareup/debounce/DebouncedOnClickListener;

    .line 29
    iput-boolean p4, p0, Lcom/squareup/cardreader/ui/api/ButtonDescriptor;->enabled:Z

    return-void
.end method

.method public static forDisabledButton(Ljava/lang/String;)Lcom/squareup/cardreader/ui/api/ButtonDescriptor;
    .locals 3

    .line 17
    new-instance v0, Lcom/squareup/cardreader/ui/api/ButtonDescriptor;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2, v1}, Lcom/squareup/cardreader/ui/api/ButtonDescriptor;-><init>(ILjava/lang/String;Lcom/squareup/debounce/DebouncedOnClickListener;Z)V

    return-object v0
.end method
