.class public final Lcom/squareup/cardreader/LcrModule_ProvidePaymentFeatureDelegateFactory;
.super Ljava/lang/Object;
.source "LcrModule_ProvidePaymentFeatureDelegateFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/PaymentFeatureV2;",
        ">;"
    }
.end annotation


# instance fields
.field private final posSenderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/PaymentFeatureDelegateSender;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/PaymentFeatureDelegateSender;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/cardreader/LcrModule_ProvidePaymentFeatureDelegateFactory;->posSenderProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/cardreader/LcrModule_ProvidePaymentFeatureDelegateFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/PaymentFeatureDelegateSender;",
            ">;)",
            "Lcom/squareup/cardreader/LcrModule_ProvidePaymentFeatureDelegateFactory;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/cardreader/LcrModule_ProvidePaymentFeatureDelegateFactory;

    invoke-direct {v0, p0}, Lcom/squareup/cardreader/LcrModule_ProvidePaymentFeatureDelegateFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static providePaymentFeatureDelegate(Lcom/squareup/cardreader/PaymentFeatureDelegateSender;)Lcom/squareup/cardreader/PaymentFeatureV2;
    .locals 1

    .line 36
    invoke-static {p0}, Lcom/squareup/cardreader/LcrModule;->providePaymentFeatureDelegate(Lcom/squareup/cardreader/PaymentFeatureDelegateSender;)Lcom/squareup/cardreader/PaymentFeatureV2;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/PaymentFeatureV2;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/PaymentFeatureV2;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/cardreader/LcrModule_ProvidePaymentFeatureDelegateFactory;->posSenderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/PaymentFeatureDelegateSender;

    invoke-static {v0}, Lcom/squareup/cardreader/LcrModule_ProvidePaymentFeatureDelegateFactory;->providePaymentFeatureDelegate(Lcom/squareup/cardreader/PaymentFeatureDelegateSender;)Lcom/squareup/cardreader/PaymentFeatureV2;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/cardreader/LcrModule_ProvidePaymentFeatureDelegateFactory;->get()Lcom/squareup/cardreader/PaymentFeatureV2;

    move-result-object v0

    return-object v0
.end method
