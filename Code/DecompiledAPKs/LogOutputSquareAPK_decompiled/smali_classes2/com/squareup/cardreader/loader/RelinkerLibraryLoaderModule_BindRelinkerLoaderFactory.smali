.class public final Lcom/squareup/cardreader/loader/RelinkerLibraryLoaderModule_BindRelinkerLoaderFactory;
.super Ljava/lang/Object;
.source "RelinkerLibraryLoaderModule_BindRelinkerLoaderFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/loader/LibraryLoader;",
        ">;"
    }
.end annotation


# instance fields
.field private final applicationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final crashnadoProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crashnado/Crashnado;",
            ">;"
        }
    .end annotation
.end field

.field private final libraryLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/loader/LibraryLoader$NativeLibraryLogger;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crashnado/Crashnado;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/loader/LibraryLoader$NativeLibraryLogger;",
            ">;)V"
        }
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/cardreader/loader/RelinkerLibraryLoaderModule_BindRelinkerLoaderFactory;->applicationProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p2, p0, Lcom/squareup/cardreader/loader/RelinkerLibraryLoaderModule_BindRelinkerLoaderFactory;->crashnadoProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p3, p0, Lcom/squareup/cardreader/loader/RelinkerLibraryLoaderModule_BindRelinkerLoaderFactory;->libraryLoggerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static bindRelinkerLoader(Landroid/app/Application;Lcom/squareup/crashnado/Crashnado;Lcom/squareup/cardreader/loader/LibraryLoader$NativeLibraryLogger;)Lcom/squareup/cardreader/loader/LibraryLoader;
    .locals 0

    .line 46
    invoke-static {p0, p1, p2}, Lcom/squareup/cardreader/loader/RelinkerLibraryLoaderModule;->bindRelinkerLoader(Landroid/app/Application;Lcom/squareup/crashnado/Crashnado;Lcom/squareup/cardreader/loader/LibraryLoader$NativeLibraryLogger;)Lcom/squareup/cardreader/loader/LibraryLoader;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/loader/LibraryLoader;

    return-object p0
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/loader/RelinkerLibraryLoaderModule_BindRelinkerLoaderFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crashnado/Crashnado;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/loader/LibraryLoader$NativeLibraryLogger;",
            ">;)",
            "Lcom/squareup/cardreader/loader/RelinkerLibraryLoaderModule_BindRelinkerLoaderFactory;"
        }
    .end annotation

    .line 41
    new-instance v0, Lcom/squareup/cardreader/loader/RelinkerLibraryLoaderModule_BindRelinkerLoaderFactory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/cardreader/loader/RelinkerLibraryLoaderModule_BindRelinkerLoaderFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/loader/LibraryLoader;
    .locals 3

    .line 35
    iget-object v0, p0, Lcom/squareup/cardreader/loader/RelinkerLibraryLoaderModule_BindRelinkerLoaderFactory;->applicationProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    iget-object v1, p0, Lcom/squareup/cardreader/loader/RelinkerLibraryLoaderModule_BindRelinkerLoaderFactory;->crashnadoProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/crashnado/Crashnado;

    iget-object v2, p0, Lcom/squareup/cardreader/loader/RelinkerLibraryLoaderModule_BindRelinkerLoaderFactory;->libraryLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cardreader/loader/LibraryLoader$NativeLibraryLogger;

    invoke-static {v0, v1, v2}, Lcom/squareup/cardreader/loader/RelinkerLibraryLoaderModule_BindRelinkerLoaderFactory;->bindRelinkerLoader(Landroid/app/Application;Lcom/squareup/crashnado/Crashnado;Lcom/squareup/cardreader/loader/LibraryLoader$NativeLibraryLogger;)Lcom/squareup/cardreader/loader/LibraryLoader;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/cardreader/loader/RelinkerLibraryLoaderModule_BindRelinkerLoaderFactory;->get()Lcom/squareup/cardreader/loader/LibraryLoader;

    move-result-object v0

    return-object v0
.end method
