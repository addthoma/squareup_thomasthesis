.class public final Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy_Factory;
.super Ljava/lang/Object;
.source "FirmwareUpdateFeatureLegacy_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;",
        ">;"
    }
.end annotation


# instance fields
.field private final cardReaderConstantsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderConstants;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderPointerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;"
        }
    .end annotation
.end field

.field private final firmwareUpdateFeatureNativeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/FirmwareUpdateFeatureNativeInterface;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderConstants;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/FirmwareUpdateFeatureNativeInterface;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy_Factory;->cardReaderPointerProvider:Ljavax/inject/Provider;

    .line 27
    iput-object p2, p0, Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy_Factory;->cardReaderConstantsProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p3, p0, Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy_Factory;->firmwareUpdateFeatureNativeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderConstants;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/FirmwareUpdateFeatureNativeInterface;",
            ">;)",
            "Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy_Factory;"
        }
    .end annotation

    .line 40
    new-instance v0, Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Ljavax/inject/Provider;Lcom/squareup/cardreader/CardReaderConstants;Lcom/squareup/cardreader/lcr/FirmwareUpdateFeatureNativeInterface;)Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;",
            "Lcom/squareup/cardreader/CardReaderConstants;",
            "Lcom/squareup/cardreader/lcr/FirmwareUpdateFeatureNativeInterface;",
            ")",
            "Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;"
        }
    .end annotation

    .line 46
    new-instance v0, Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;-><init>(Ljavax/inject/Provider;Lcom/squareup/cardreader/CardReaderConstants;Lcom/squareup/cardreader/lcr/FirmwareUpdateFeatureNativeInterface;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;
    .locals 3

    .line 33
    iget-object v0, p0, Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy_Factory;->cardReaderPointerProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy_Factory;->cardReaderConstantsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/CardReaderConstants;

    iget-object v2, p0, Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy_Factory;->firmwareUpdateFeatureNativeProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cardreader/lcr/FirmwareUpdateFeatureNativeInterface;

    invoke-static {v0, v1, v2}, Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy_Factory;->newInstance(Ljavax/inject/Provider;Lcom/squareup/cardreader/CardReaderConstants;Lcom/squareup/cardreader/lcr/FirmwareUpdateFeatureNativeInterface;)Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy_Factory;->get()Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;

    move-result-object v0

    return-object v0
.end method
