.class public final Lcom/squareup/cardreader/UserInteractionFeatureV2;
.super Ljava/lang/Object;
.source "UserInteractionFeatureV2.kt"

# interfaces
.implements Lcom/squareup/cardreader/CanReset;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001B\u001b\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u000e\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011J\u0010\u0010\u0012\u001a\n \u0014*\u0004\u0018\u00010\u00130\u0013H\u0002J\u0008\u0010\u0015\u001a\u00020\u000fH\u0002J\u0008\u0010\u0016\u001a\u00020\u000fH\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0008\u001a\u00020\tX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\n\u0010\u000b\"\u0004\u0008\u000c\u0010\rR\u0014\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/cardreader/UserInteractionFeatureV2;",
        "Lcom/squareup/cardreader/CanReset;",
        "posSender",
        "Lcom/squareup/cardreader/SendsToPos;",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$UserInteractionFeatureOutput;",
        "cardreaderProvider",
        "Lcom/squareup/cardreader/CardreaderPointerProvider;",
        "(Lcom/squareup/cardreader/SendsToPos;Lcom/squareup/cardreader/CardreaderPointerProvider;)V",
        "featurePointer",
        "Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_user_interaction_t;",
        "getFeaturePointer",
        "()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_user_interaction_t;",
        "setFeaturePointer",
        "(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_user_interaction_t;)V",
        "handleMessage",
        "",
        "message",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderInput$UserInteractionFeatureMessage;",
        "identify",
        "Lcom/squareup/cardreader/lcr/CrUserInteractionResult;",
        "kotlin.jvm.PlatformType",
        "initialize",
        "resetIfInitilized",
        "cardreader-features_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final cardreaderProvider:Lcom/squareup/cardreader/CardreaderPointerProvider;

.field public featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_user_interaction_t;

.field private final posSender:Lcom/squareup/cardreader/SendsToPos;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cardreader/SendsToPos<",
            "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$UserInteractionFeatureOutput;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/SendsToPos;Lcom/squareup/cardreader/CardreaderPointerProvider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/SendsToPos<",
            "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$UserInteractionFeatureOutput;",
            ">;",
            "Lcom/squareup/cardreader/CardreaderPointerProvider;",
            ")V"
        }
    .end annotation

    const-string v0, "posSender"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardreaderProvider"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/cardreader/UserInteractionFeatureV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    iput-object p2, p0, Lcom/squareup/cardreader/UserInteractionFeatureV2;->cardreaderProvider:Lcom/squareup/cardreader/CardreaderPointerProvider;

    return-void
.end method

.method private final identify()Lcom/squareup/cardreader/lcr/CrUserInteractionResult;
    .locals 2

    .line 37
    iget-object v0, p0, Lcom/squareup/cardreader/UserInteractionFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_user_interaction_t;

    if-nez v0, :cond_0

    const-string v1, "featurePointer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {v0}, Lcom/squareup/cardreader/lcr/UserInteractionFeatureNative;->cr_user_interaction_identify_reader(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_user_interaction_t;)Lcom/squareup/cardreader/lcr/CrUserInteractionResult;

    move-result-object v0

    return-object v0
.end method

.method private final initialize()V
    .locals 2

    .line 25
    iget-object v0, p0, Lcom/squareup/cardreader/UserInteractionFeatureV2;->cardreaderProvider:Lcom/squareup/cardreader/CardreaderPointerProvider;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardreaderPointerProvider;->cardreaderPointer()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;

    move-result-object v0

    .line 24
    invoke-static {v0}, Lcom/squareup/cardreader/lcr/UserInteractionFeatureNative;->user_interaction_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_user_interaction_t;

    move-result-object v0

    const-string v1, "UserInteractionFeatureNa\u2026cardreaderPointer()\n    )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/cardreader/UserInteractionFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_user_interaction_t;

    return-void
.end method


# virtual methods
.method public final getFeaturePointer()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_user_interaction_t;
    .locals 2

    .line 14
    iget-object v0, p0, Lcom/squareup/cardreader/UserInteractionFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_user_interaction_t;

    if-nez v0, :cond_0

    const-string v1, "featurePointer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final handleMessage(Lcom/squareup/cardreader/ReaderMessage$ReaderInput$UserInteractionFeatureMessage;)V
    .locals 1

    const-string v0, "message"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    sget-object v0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$UserInteractionFeatureMessage$Initialize;->INSTANCE:Lcom/squareup/cardreader/ReaderMessage$ReaderInput$UserInteractionFeatureMessage$Initialize;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/cardreader/UserInteractionFeatureV2;->initialize()V

    goto :goto_0

    .line 19
    :cond_0
    sget-object v0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$UserInteractionFeatureMessage$Identify;->INSTANCE:Lcom/squareup/cardreader/ReaderMessage$ReaderInput$UserInteractionFeatureMessage$Identify;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-direct {p0}, Lcom/squareup/cardreader/UserInteractionFeatureV2;->identify()Lcom/squareup/cardreader/lcr/CrUserInteractionResult;

    :cond_1
    :goto_0
    return-void
.end method

.method public resetIfInitilized()V
    .locals 2

    .line 30
    move-object v0, p0

    check-cast v0, Lcom/squareup/cardreader/UserInteractionFeatureV2;

    iget-object v0, v0, Lcom/squareup/cardreader/UserInteractionFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_user_interaction_t;

    if-eqz v0, :cond_2

    .line 31
    iget-object v0, p0, Lcom/squareup/cardreader/UserInteractionFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_user_interaction_t;

    const-string v1, "featurePointer"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {v0}, Lcom/squareup/cardreader/lcr/UserInteractionFeatureNative;->cr_user_interaction_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_user_interaction_t;)Lcom/squareup/cardreader/lcr/CrUserInteractionResult;

    .line 32
    iget-object v0, p0, Lcom/squareup/cardreader/UserInteractionFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_user_interaction_t;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-static {v0}, Lcom/squareup/cardreader/lcr/UserInteractionFeatureNative;->cr_user_interaction_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_user_interaction_t;)Lcom/squareup/cardreader/lcr/CrUserInteractionResult;

    :cond_2
    return-void
.end method

.method public final setFeaturePointer(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_user_interaction_t;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    iput-object p1, p0, Lcom/squareup/cardreader/UserInteractionFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_user_interaction_t;

    return-void
.end method
