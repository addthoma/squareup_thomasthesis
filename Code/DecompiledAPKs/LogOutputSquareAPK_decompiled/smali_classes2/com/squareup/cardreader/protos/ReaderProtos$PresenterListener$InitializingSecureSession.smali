.class public final Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$InitializingSecureSession;
.super Lcom/squareup/wire/Message;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "InitializingSecureSession"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$InitializingSecureSession$ProtoAdapter_InitializingSecureSession;,
        Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$InitializingSecureSession$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$InitializingSecureSession;",
        "Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$InitializingSecureSession$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$InitializingSecureSession;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 4575
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$InitializingSecureSession$ProtoAdapter_InitializingSecureSession;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$InitializingSecureSession$ProtoAdapter_InitializingSecureSession;-><init>()V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$InitializingSecureSession;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 4580
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, v0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$InitializingSecureSession;-><init>(Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lokio/ByteString;)V
    .locals 1

    .line 4584
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$InitializingSecureSession;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p1}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-ne p1, p0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 4597
    :cond_0
    instance-of v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$InitializingSecureSession;

    if-nez v0, :cond_1

    const/4 p1, 0x0

    return p1

    .line 4598
    :cond_1
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$InitializingSecureSession;

    .line 4599
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$InitializingSecureSession;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$InitializingSecureSession;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public hashCode()I
    .locals 1

    .line 4604
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$InitializingSecureSession;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    return v0
.end method

.method public newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$InitializingSecureSession$Builder;
    .locals 2

    .line 4589
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$InitializingSecureSession$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$InitializingSecureSession$Builder;-><init>()V

    .line 4590
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$InitializingSecureSession;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$InitializingSecureSession$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 4574
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$InitializingSecureSession;->newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$InitializingSecureSession$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 4609
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "InitializingSecureSession{"

    .line 4610
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
