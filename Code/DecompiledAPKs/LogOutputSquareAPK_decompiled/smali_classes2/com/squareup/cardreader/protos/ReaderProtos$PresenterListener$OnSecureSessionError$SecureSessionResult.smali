.class public final enum Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;
.super Ljava/lang/Enum;
.source "ReaderProtos.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SecureSessionResult"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult$ProtoAdapter_SecureSessionResult;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum AES:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

.field public static final enum ALREADY_INITIALIZED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

.field public static final enum ALREADY_TERMINATED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

.field public static final enum API_CALL:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

.field public static final enum APPROVAL_EXPIRED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

.field public static final enum APPROVAL_MISMATCH:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

.field public static final enum ARG:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

.field public static final enum BAD_DIGIT:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

.field public static final enum BAD_FIELD:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

.field public static final enum BAD_HMAC:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

.field public static final enum CALL_UNEXPECTED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

.field public static final enum CONTEXT:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

.field public static final enum CURVE:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

.field public static final enum DENIED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

.field public static final enum ENCODE_FAILURE:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

.field public static final enum GENERIC_ERROR:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

.field public static final enum HKDF:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

.field public static final enum INPUT_SIZE:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

.field public static final enum INVALID_KEY_UPDATE_MSG:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

.field public static final enum INVALID_PARAMETER:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

.field public static final enum INVALID_PIN_REQUEST:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

.field public static final enum MAX_READERS_CONNECTED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

.field public static final enum MINESWEEPER_CALL:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

.field public static final enum MODULE_GENERIC_ERROR:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

.field public static final enum MSG_TYPE:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

.field public static final enum NOT_INITIALIZED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

.field public static final enum NOT_TERMINATED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

.field public static final enum NO_READER:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

.field public static final enum NO_TXN_LEFT:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

.field public static final enum OUTPUT_SIZE:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

.field public static final enum OUT_OF_CONTEXTS:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

.field public static final enum PIN_FULL:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

.field public static final enum PIN_TOO_SHORT:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

.field public static final enum PROTOCOL_VERSION:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

.field public static final enum SERVER_DENY_ERROR:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

.field public static final enum SESSION_ERROR:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

.field public static final enum SESSION_ID:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

.field public static final enum SESSION_STATE:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

.field public static final enum SHA256:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

.field public static final enum SUCCESS:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

.field public static final enum TDES:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

.field public static final enum UNKNOWN:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

.field public static final enum WHITEBOX_KEY_DESERIALIZE:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 5321
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const/4 v1, 0x0

    const-string v2, "SUCCESS"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->SUCCESS:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    .line 5323
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const/4 v2, 0x1

    const-string v3, "INVALID_PARAMETER"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->INVALID_PARAMETER:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    .line 5325
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const/4 v3, 0x2

    const-string v4, "NOT_INITIALIZED"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->NOT_INITIALIZED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    .line 5327
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const/4 v4, 0x3

    const-string v5, "ALREADY_INITIALIZED"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->ALREADY_INITIALIZED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    .line 5329
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const/4 v5, 0x4

    const-string v6, "NOT_TERMINATED"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->NOT_TERMINATED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    .line 5331
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const/4 v6, 0x5

    const-string v7, "ALREADY_TERMINATED"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->ALREADY_TERMINATED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    .line 5333
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const/4 v7, 0x6

    const-string v8, "SESSION_ERROR"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->SESSION_ERROR:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    .line 5335
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const/4 v8, 0x7

    const-string v9, "CALL_UNEXPECTED"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->CALL_UNEXPECTED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    .line 5337
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const/16 v9, 0x8

    const-string v10, "GENERIC_ERROR"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->GENERIC_ERROR:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    .line 5339
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const/16 v10, 0x9

    const-string v11, "NO_READER"

    invoke-direct {v0, v11, v10, v10}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->NO_READER:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    .line 5341
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const/16 v11, 0xa

    const-string v12, "SERVER_DENY_ERROR"

    invoke-direct {v0, v12, v11, v11}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->SERVER_DENY_ERROR:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    .line 5343
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const/16 v12, 0xb

    const-string v13, "MODULE_GENERIC_ERROR"

    invoke-direct {v0, v13, v12, v12}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->MODULE_GENERIC_ERROR:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    .line 5345
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const/16 v13, 0xc

    const-string v14, "MAX_READERS_CONNECTED"

    invoke-direct {v0, v14, v13, v13}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->MAX_READERS_CONNECTED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    .line 5347
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const/16 v14, 0xd

    const-string v15, "ARG"

    invoke-direct {v0, v15, v14, v14}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->ARG:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    .line 5349
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const/16 v15, 0xe

    const-string v14, "SESSION_STATE"

    invoke-direct {v0, v14, v15, v15}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->SESSION_STATE:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    .line 5351
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const-string v14, "INPUT_SIZE"

    const/16 v15, 0xf

    const/16 v13, 0xf

    invoke-direct {v0, v14, v15, v13}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->INPUT_SIZE:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    .line 5353
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const-string v13, "OUTPUT_SIZE"

    const/16 v14, 0x10

    const/16 v15, 0x10

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->OUTPUT_SIZE:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    .line 5355
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const-string v13, "MSG_TYPE"

    const/16 v14, 0x11

    const/16 v15, 0x11

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->MSG_TYPE:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    .line 5357
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const-string v13, "SESSION_ID"

    const/16 v14, 0x12

    const/16 v15, 0x12

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->SESSION_ID:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    .line 5359
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const-string v13, "CURVE"

    const/16 v14, 0x13

    const/16 v15, 0x13

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->CURVE:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    .line 5361
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const-string v13, "HKDF"

    const/16 v14, 0x14

    const/16 v15, 0x14

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->HKDF:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    .line 5363
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const-string v13, "DENIED"

    const/16 v14, 0x15

    const/16 v15, 0x15

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->DENIED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    .line 5365
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const-string v13, "BAD_DIGIT"

    const/16 v14, 0x16

    const/16 v15, 0x16

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->BAD_DIGIT:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    .line 5367
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const-string v13, "PIN_FULL"

    const/16 v14, 0x17

    const/16 v15, 0x17

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->PIN_FULL:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    .line 5369
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const-string v13, "PIN_TOO_SHORT"

    const/16 v14, 0x18

    const/16 v15, 0x18

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->PIN_TOO_SHORT:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    .line 5371
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const-string v13, "INVALID_PIN_REQUEST"

    const/16 v14, 0x19

    const/16 v15, 0x19

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->INVALID_PIN_REQUEST:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    .line 5373
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const-string v13, "INVALID_KEY_UPDATE_MSG"

    const/16 v14, 0x1a

    const/16 v15, 0x1a

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->INVALID_KEY_UPDATE_MSG:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    .line 5375
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const-string v13, "AES"

    const/16 v14, 0x1b

    const/16 v15, 0x1b

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->AES:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    .line 5377
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const-string v13, "PROTOCOL_VERSION"

    const/16 v14, 0x1c

    const/16 v15, 0x1c

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->PROTOCOL_VERSION:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    .line 5379
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const-string v13, "APPROVAL_MISMATCH"

    const/16 v14, 0x1d

    const/16 v15, 0x1d

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->APPROVAL_MISMATCH:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    .line 5381
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const-string v13, "APPROVAL_EXPIRED"

    const/16 v14, 0x1e

    const/16 v15, 0x1e

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->APPROVAL_EXPIRED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    .line 5383
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const-string v13, "NO_TXN_LEFT"

    const/16 v14, 0x1f

    const/16 v15, 0x1f

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->NO_TXN_LEFT:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    .line 5385
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const-string v13, "API_CALL"

    const/16 v14, 0x20

    const/16 v15, 0x20

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->API_CALL:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    .line 5387
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const-string v13, "MINESWEEPER_CALL"

    const/16 v14, 0x21

    const/16 v15, 0x21

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->MINESWEEPER_CALL:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    .line 5389
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const-string v13, "SHA256"

    const/16 v14, 0x22

    const/16 v15, 0x22

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->SHA256:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    .line 5391
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const-string v13, "BAD_HMAC"

    const/16 v14, 0x23

    const/16 v15, 0x23

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->BAD_HMAC:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    .line 5393
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const-string v13, "TDES"

    const/16 v14, 0x24

    const/16 v15, 0x24

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->TDES:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    .line 5395
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const-string v13, "ENCODE_FAILURE"

    const/16 v14, 0x25

    const/16 v15, 0x25

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->ENCODE_FAILURE:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    .line 5397
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const-string v13, "CONTEXT"

    const/16 v14, 0x26

    const/16 v15, 0x26

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->CONTEXT:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    .line 5399
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const-string v13, "OUT_OF_CONTEXTS"

    const/16 v14, 0x27

    const/16 v15, 0x27

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->OUT_OF_CONTEXTS:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    .line 5401
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const-string v13, "BAD_FIELD"

    const/16 v14, 0x28

    const/16 v15, 0x28

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->BAD_FIELD:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    .line 5403
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const-string v13, "WHITEBOX_KEY_DESERIALIZE"

    const/16 v14, 0x29

    const/16 v15, 0x29

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->WHITEBOX_KEY_DESERIALIZE:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    .line 5405
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const-string v13, "UNKNOWN"

    const/16 v14, 0x2a

    const/16 v15, 0x2a

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->UNKNOWN:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const/16 v0, 0x2b

    new-array v0, v0, [Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    .line 5320
    sget-object v13, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->SUCCESS:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    aput-object v13, v0, v1

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->INVALID_PARAMETER:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->NOT_INITIALIZED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->ALREADY_INITIALIZED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->NOT_TERMINATED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->ALREADY_TERMINATED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->SESSION_ERROR:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->CALL_UNEXPECTED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->GENERIC_ERROR:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->NO_READER:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->SERVER_DENY_ERROR:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->MODULE_GENERIC_ERROR:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->MAX_READERS_CONNECTED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->ARG:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->SESSION_STATE:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->INPUT_SIZE:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->OUTPUT_SIZE:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->MSG_TYPE:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->SESSION_ID:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->CURVE:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->HKDF:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->DENIED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->BAD_DIGIT:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->PIN_FULL:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->PIN_TOO_SHORT:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->INVALID_PIN_REQUEST:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->INVALID_KEY_UPDATE_MSG:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->AES:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->PROTOCOL_VERSION:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->APPROVAL_MISMATCH:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->APPROVAL_EXPIRED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->NO_TXN_LEFT:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->API_CALL:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const/16 v2, 0x20

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->MINESWEEPER_CALL:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const/16 v2, 0x21

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->SHA256:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const/16 v2, 0x22

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->BAD_HMAC:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const/16 v2, 0x23

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->TDES:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const/16 v2, 0x24

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->ENCODE_FAILURE:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const/16 v2, 0x25

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->CONTEXT:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const/16 v2, 0x26

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->OUT_OF_CONTEXTS:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const/16 v2, 0x27

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->BAD_FIELD:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const/16 v2, 0x28

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->WHITEBOX_KEY_DESERIALIZE:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const/16 v2, 0x29

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->UNKNOWN:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    const/16 v2, 0x2a

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->$VALUES:[Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    .line 5407
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult$ProtoAdapter_SecureSessionResult;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult$ProtoAdapter_SecureSessionResult;-><init>()V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 5411
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 5412
    iput p3, p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 5462
    :pswitch_0
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->UNKNOWN:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    return-object p0

    .line 5461
    :pswitch_1
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->WHITEBOX_KEY_DESERIALIZE:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    return-object p0

    .line 5460
    :pswitch_2
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->BAD_FIELD:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    return-object p0

    .line 5459
    :pswitch_3
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->OUT_OF_CONTEXTS:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    return-object p0

    .line 5458
    :pswitch_4
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->CONTEXT:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    return-object p0

    .line 5457
    :pswitch_5
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->ENCODE_FAILURE:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    return-object p0

    .line 5456
    :pswitch_6
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->TDES:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    return-object p0

    .line 5455
    :pswitch_7
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->BAD_HMAC:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    return-object p0

    .line 5454
    :pswitch_8
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->SHA256:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    return-object p0

    .line 5453
    :pswitch_9
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->MINESWEEPER_CALL:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    return-object p0

    .line 5452
    :pswitch_a
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->API_CALL:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    return-object p0

    .line 5451
    :pswitch_b
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->NO_TXN_LEFT:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    return-object p0

    .line 5450
    :pswitch_c
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->APPROVAL_EXPIRED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    return-object p0

    .line 5449
    :pswitch_d
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->APPROVAL_MISMATCH:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    return-object p0

    .line 5448
    :pswitch_e
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->PROTOCOL_VERSION:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    return-object p0

    .line 5447
    :pswitch_f
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->AES:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    return-object p0

    .line 5446
    :pswitch_10
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->INVALID_KEY_UPDATE_MSG:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    return-object p0

    .line 5445
    :pswitch_11
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->INVALID_PIN_REQUEST:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    return-object p0

    .line 5444
    :pswitch_12
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->PIN_TOO_SHORT:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    return-object p0

    .line 5443
    :pswitch_13
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->PIN_FULL:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    return-object p0

    .line 5442
    :pswitch_14
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->BAD_DIGIT:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    return-object p0

    .line 5441
    :pswitch_15
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->DENIED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    return-object p0

    .line 5440
    :pswitch_16
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->HKDF:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    return-object p0

    .line 5439
    :pswitch_17
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->CURVE:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    return-object p0

    .line 5438
    :pswitch_18
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->SESSION_ID:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    return-object p0

    .line 5437
    :pswitch_19
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->MSG_TYPE:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    return-object p0

    .line 5436
    :pswitch_1a
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->OUTPUT_SIZE:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    return-object p0

    .line 5435
    :pswitch_1b
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->INPUT_SIZE:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    return-object p0

    .line 5434
    :pswitch_1c
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->SESSION_STATE:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    return-object p0

    .line 5433
    :pswitch_1d
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->ARG:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    return-object p0

    .line 5432
    :pswitch_1e
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->MAX_READERS_CONNECTED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    return-object p0

    .line 5431
    :pswitch_1f
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->MODULE_GENERIC_ERROR:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    return-object p0

    .line 5430
    :pswitch_20
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->SERVER_DENY_ERROR:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    return-object p0

    .line 5429
    :pswitch_21
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->NO_READER:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    return-object p0

    .line 5428
    :pswitch_22
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->GENERIC_ERROR:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    return-object p0

    .line 5427
    :pswitch_23
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->CALL_UNEXPECTED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    return-object p0

    .line 5426
    :pswitch_24
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->SESSION_ERROR:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    return-object p0

    .line 5425
    :pswitch_25
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->ALREADY_TERMINATED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    return-object p0

    .line 5424
    :pswitch_26
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->NOT_TERMINATED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    return-object p0

    .line 5423
    :pswitch_27
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->ALREADY_INITIALIZED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    return-object p0

    .line 5422
    :pswitch_28
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->NOT_INITIALIZED:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    return-object p0

    .line 5421
    :pswitch_29
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->INVALID_PARAMETER:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    return-object p0

    .line 5420
    :pswitch_2a
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->SUCCESS:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2a
        :pswitch_29
        :pswitch_28
        :pswitch_27
        :pswitch_26
        :pswitch_25
        :pswitch_24
        :pswitch_23
        :pswitch_22
        :pswitch_21
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;
    .locals 1

    .line 5320
    const-class v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    return-object p0
.end method

.method public static values()[Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;
    .locals 1

    .line 5320
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->$VALUES:[Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    invoke-virtual {v0}, [Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 5469
    iget v0, p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->value:I

    return v0
.end method
