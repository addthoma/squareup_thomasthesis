.class public final Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;
.super Lcom/squareup/wire/Message;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$CardReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "InitializeCardReaderFeatures"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures$ProtoAdapter_InitializeCardReaderFeatures;,
        Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;",
        "Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CURRENCY_CODE:Ljava/lang/Integer;

.field public static final DEFAULT_MCC:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final currency_code:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x2
    .end annotation
.end field

.field public final mcc:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x1
    .end annotation
.end field

.field public final reader_feature_flags:Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$ReaderFeatureFlags#ADAPTER"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 128
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures$ProtoAdapter_InitializeCardReaderFeatures;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures$ProtoAdapter_InitializeCardReaderFeatures;-><init>()V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 132
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;->DEFAULT_MCC:Ljava/lang/Integer;

    .line 134
    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;->DEFAULT_CURRENCY_CODE:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;)V
    .locals 1

    .line 156
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;Lokio/ByteString;)V
    .locals 1

    .line 161
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 162
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;->mcc:Ljava/lang/Integer;

    .line 163
    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;->currency_code:Ljava/lang/Integer;

    .line 164
    iput-object p3, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;->reader_feature_flags:Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 180
    :cond_0
    instance-of v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 181
    :cond_1
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;

    .line 182
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;->mcc:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;->mcc:Ljava/lang/Integer;

    .line 183
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;->currency_code:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;->currency_code:Ljava/lang/Integer;

    .line 184
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;->reader_feature_flags:Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;

    iget-object p1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;->reader_feature_flags:Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;

    .line 185
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 190
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 192
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 193
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;->mcc:Ljava/lang/Integer;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 194
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;->currency_code:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 195
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;->reader_feature_flags:Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 196
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures$Builder;
    .locals 2

    .line 169
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures$Builder;-><init>()V

    .line 170
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;->mcc:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures$Builder;->mcc:Ljava/lang/Integer;

    .line 171
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;->currency_code:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures$Builder;->currency_code:Ljava/lang/Integer;

    .line 172
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;->reader_feature_flags:Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures$Builder;->reader_feature_flags:Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;

    .line 173
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 127
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;->newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 203
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 204
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;->mcc:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    const-string v1, ", mcc="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;->mcc:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 205
    :cond_0
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;->currency_code:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    const-string v1, ", currency_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;->currency_code:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 206
    :cond_1
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;->reader_feature_flags:Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;

    if-eqz v1, :cond_2

    const-string v1, ", reader_feature_flags="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;->reader_feature_flags:Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "InitializeCardReaderFeatures{"

    .line 207
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
