.class public final Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;
.super Lcom/squareup/wire/Message;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$CardReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StartPayment"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment$ProtoAdapter_StartPayment;,
        Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;",
        "Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_AMOUNT_AUTHORIZED:Ljava/lang/Long;

.field public static final DEFAULT_CURRENT_TIME_MILLIS:Ljava/lang/Long;

.field private static final serialVersionUID:J


# instance fields
.field public final amount_authorized:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x1
    .end annotation
.end field

.field public final current_time_millis:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1171
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment$ProtoAdapter_StartPayment;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment$ProtoAdapter_StartPayment;-><init>()V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 1175
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;->DEFAULT_AMOUNT_AUTHORIZED:Ljava/lang/Long;

    .line 1177
    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;->DEFAULT_CURRENT_TIME_MILLIS:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/Long;)V
    .locals 1

    .line 1192
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;-><init>(Ljava/lang/Long;Ljava/lang/Long;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/Long;Lokio/ByteString;)V
    .locals 1

    .line 1197
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 1198
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;->amount_authorized:Ljava/lang/Long;

    .line 1199
    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;->current_time_millis:Ljava/lang/Long;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 1214
    :cond_0
    instance-of v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 1215
    :cond_1
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;

    .line 1216
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;->amount_authorized:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;->amount_authorized:Ljava/lang/Long;

    .line 1217
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;->current_time_millis:Ljava/lang/Long;

    iget-object p1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;->current_time_millis:Ljava/lang/Long;

    .line 1218
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 1223
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 1225
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 1226
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;->amount_authorized:Ljava/lang/Long;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1227
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;->current_time_millis:Ljava/lang/Long;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 1228
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment$Builder;
    .locals 2

    .line 1204
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment$Builder;-><init>()V

    .line 1205
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;->amount_authorized:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment$Builder;->amount_authorized:Ljava/lang/Long;

    .line 1206
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;->current_time_millis:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment$Builder;->current_time_millis:Ljava/lang/Long;

    .line 1207
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 1170
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;->newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 1235
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1236
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;->amount_authorized:Ljava/lang/Long;

    if-eqz v1, :cond_0

    const-string v1, ", amount_authorized="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;->amount_authorized:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1237
    :cond_0
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;->current_time_millis:Ljava/lang/Long;

    if-eqz v1, :cond_1

    const-string v1, ", current_time_millis="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$StartPayment;->current_time_millis:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "StartPayment{"

    .line 1238
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
