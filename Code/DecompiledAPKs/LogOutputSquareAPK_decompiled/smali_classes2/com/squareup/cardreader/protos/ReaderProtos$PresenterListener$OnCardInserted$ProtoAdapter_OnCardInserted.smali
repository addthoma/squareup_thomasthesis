.class final Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted$ProtoAdapter_OnCardInserted;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_OnCardInserted"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 5760
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 5775
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted$Builder;-><init>()V

    .line 5776
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 5777
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 5780
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 5784
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 5785
    invoke-virtual {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 5758
    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted$ProtoAdapter_OnCardInserted;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 5770
    invoke-virtual {p2}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 5758
    check-cast p2, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted$ProtoAdapter_OnCardInserted;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted;)I
    .locals 0

    .line 5765
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    return p1
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 5758
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted$ProtoAdapter_OnCardInserted;->encodedSize(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted;)Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted;
    .locals 0

    .line 5790
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted;->newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted$Builder;

    move-result-object p1

    .line 5791
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 5792
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 5758
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted$ProtoAdapter_OnCardInserted;->redact(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted;)Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted;

    move-result-object p1

    return-object p1
.end method
