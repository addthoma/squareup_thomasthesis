.class public final Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;",
        "Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public address:Ljava/lang/String;

.field public battery_info:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;

.field public card_presence:Ljava/lang/Boolean;

.field public charge_cycle_count:Ljava/lang/Integer;

.field public comms_status:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;

.field public connected:Ljava/lang/Boolean;

.field public connection_type:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ConnectionType;

.field public firmware_update_in_progress:Ljava/lang/Boolean;

.field public firmware_version:Ljava/lang/String;

.field public hardware_serial_number:Ljava/lang/String;

.field public id:Ljava/lang/Integer;

.field public reader_name:Ljava/lang/String;

.field public reader_requires_firmware_update:Ljava/lang/Boolean;

.field public reader_type:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

.field public secure_session:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;

.field public server_requires_firmware_update:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6491
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public address(Ljava/lang/String;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;
    .locals 0

    .line 6505
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->address:Ljava/lang/String;

    return-object p0
.end method

.method public battery_info(Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;
    .locals 0

    .line 6555
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->battery_info:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderBatteryInfo;

    return-object p0
.end method

.method public build()Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;
    .locals 2

    .line 6576
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;-><init>(Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 6458
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;

    move-result-object v0

    return-object v0
.end method

.method public card_presence(Ljava/lang/Boolean;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;
    .locals 0

    .line 6535
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->card_presence:Ljava/lang/Boolean;

    return-object p0
.end method

.method public charge_cycle_count(Ljava/lang/Integer;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;
    .locals 0

    .line 6550
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->charge_cycle_count:Ljava/lang/Integer;

    return-object p0
.end method

.method public comms_status(Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;
    .locals 0

    .line 6525
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->comms_status:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;

    return-object p0
.end method

.method public connected(Ljava/lang/Boolean;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;
    .locals 0

    .line 6520
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->connected:Ljava/lang/Boolean;

    return-object p0
.end method

.method public connection_type(Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ConnectionType;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;
    .locals 0

    .line 6500
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->connection_type:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ConnectionType;

    return-object p0
.end method

.method public firmware_update_in_progress(Ljava/lang/Boolean;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;
    .locals 0

    .line 6560
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->firmware_update_in_progress:Ljava/lang/Boolean;

    return-object p0
.end method

.method public firmware_version(Ljava/lang/String;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;
    .locals 0

    .line 6540
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->firmware_version:Ljava/lang/String;

    return-object p0
.end method

.method public hardware_serial_number(Ljava/lang/String;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;
    .locals 0

    .line 6545
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->hardware_serial_number:Ljava/lang/String;

    return-object p0
.end method

.method public id(Ljava/lang/Integer;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;
    .locals 0

    .line 6495
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->id:Ljava/lang/Integer;

    return-object p0
.end method

.method public reader_name(Ljava/lang/String;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;
    .locals 0

    .line 6510
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->reader_name:Ljava/lang/String;

    return-object p0
.end method

.method public reader_requires_firmware_update(Ljava/lang/Boolean;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;
    .locals 0

    .line 6565
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->reader_requires_firmware_update:Ljava/lang/Boolean;

    return-object p0
.end method

.method public reader_type(Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;
    .locals 0

    .line 6515
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->reader_type:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    return-object p0
.end method

.method public secure_session(Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;
    .locals 0

    .line 6530
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->secure_session:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$SecureSessionState;

    return-object p0
.end method

.method public server_requires_firmware_update(Ljava/lang/Boolean;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;
    .locals 0

    .line 6570
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$Builder;->server_requires_firmware_update:Ljava/lang/Boolean;

    return-object p0
.end method
