.class public final Lcom/squareup/cardreader/protos/ReaderProtos$Asset;
.super Lcom/squareup/wire/Message;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Asset"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/protos/ReaderProtos$Asset$ProtoAdapter_Asset;,
        Lcom/squareup/cardreader/protos/ReaderProtos$Asset$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$Asset;",
        "Lcom/squareup/cardreader/protos/ReaderProtos$Asset$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/cardreader/protos/ReaderProtos$Asset;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_BLOCK_INDEX_TABLE:Lokio/ByteString;

.field public static final DEFAULT_ENCRYPTED_DATA:Lokio/ByteString;

.field public static final DEFAULT_HEADER:Lokio/ByteString;

.field public static final DEFAULT_IS_BLOCKING:Ljava/lang/Boolean;

.field public static final DEFAULT_REQUIRES_REBOOT:Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

.field private static final serialVersionUID:J


# instance fields
.field public final block_index_table:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        tag = 0x4
    .end annotation
.end field

.field public final encrypted_data:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        tag = 0x1
    .end annotation
.end field

.field public final header:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        tag = 0x3
    .end annotation
.end field

.field public final is_blocking:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field

.field public final requires_reboot:Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$Reboot#ADAPTER"
        tag = 0x6
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 2168
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset$ProtoAdapter_Asset;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$Asset$ProtoAdapter_Asset;-><init>()V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 2172
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;->DEFAULT_ENCRYPTED_DATA:Lokio/ByteString;

    const/4 v0, 0x0

    .line 2174
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;->DEFAULT_IS_BLOCKING:Ljava/lang/Boolean;

    .line 2176
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;->DEFAULT_HEADER:Lokio/ByteString;

    .line 2178
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;->DEFAULT_BLOCK_INDEX_TABLE:Lokio/ByteString;

    .line 2180
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;->NO:Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;->DEFAULT_REQUIRES_REBOOT:Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    return-void
.end method

.method public constructor <init>(Lokio/ByteString;Ljava/lang/Boolean;Lokio/ByteString;Lokio/ByteString;Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;)V
    .locals 7

    .line 2217
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;-><init>(Lokio/ByteString;Ljava/lang/Boolean;Lokio/ByteString;Lokio/ByteString;Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lokio/ByteString;Ljava/lang/Boolean;Lokio/ByteString;Lokio/ByteString;Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;Lokio/ByteString;)V
    .locals 1

    .line 2222
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 2223
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;->encrypted_data:Lokio/ByteString;

    .line 2224
    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;->is_blocking:Ljava/lang/Boolean;

    .line 2225
    iput-object p3, p0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;->header:Lokio/ByteString;

    .line 2226
    iput-object p4, p0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;->block_index_table:Lokio/ByteString;

    .line 2227
    iput-object p5, p0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;->requires_reboot:Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 2245
    :cond_0
    instance-of v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 2246
    :cond_1
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;

    .line 2247
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;->encrypted_data:Lokio/ByteString;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;->encrypted_data:Lokio/ByteString;

    .line 2248
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;->is_blocking:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;->is_blocking:Ljava/lang/Boolean;

    .line 2249
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;->header:Lokio/ByteString;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;->header:Lokio/ByteString;

    .line 2250
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;->block_index_table:Lokio/ByteString;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;->block_index_table:Lokio/ByteString;

    .line 2251
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;->requires_reboot:Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    iget-object p1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;->requires_reboot:Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    .line 2252
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 2257
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_5

    .line 2259
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 2260
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;->encrypted_data:Lokio/ByteString;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2261
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;->is_blocking:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2262
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;->header:Lokio/ByteString;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2263
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;->block_index_table:Lokio/ByteString;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2264
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;->requires_reboot:Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    .line 2265
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_5
    return v0
.end method

.method public newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$Asset$Builder;
    .locals 2

    .line 2232
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$Asset$Builder;-><init>()V

    .line 2233
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;->encrypted_data:Lokio/ByteString;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset$Builder;->encrypted_data:Lokio/ByteString;

    .line 2234
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;->is_blocking:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset$Builder;->is_blocking:Ljava/lang/Boolean;

    .line 2235
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;->header:Lokio/ByteString;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset$Builder;->header:Lokio/ByteString;

    .line 2236
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;->block_index_table:Lokio/ByteString;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset$Builder;->block_index_table:Lokio/ByteString;

    .line 2237
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;->requires_reboot:Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset$Builder;->requires_reboot:Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    .line 2238
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/protos/ReaderProtos$Asset$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 2167
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;->newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$Asset$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 2272
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 2273
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;->encrypted_data:Lokio/ByteString;

    if-eqz v1, :cond_0

    const-string v1, ", encrypted_data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;->encrypted_data:Lokio/ByteString;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2274
    :cond_0
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;->is_blocking:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", is_blocking="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;->is_blocking:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2275
    :cond_1
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;->header:Lokio/ByteString;

    if-eqz v1, :cond_2

    const-string v1, ", header="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;->header:Lokio/ByteString;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2276
    :cond_2
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;->block_index_table:Lokio/ByteString;

    if-eqz v1, :cond_3

    const-string v1, ", block_index_table="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;->block_index_table:Lokio/ByteString;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2277
    :cond_3
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;->requires_reboot:Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    if-eqz v1, :cond_4

    const-string v1, ", requires_reboot="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;->requires_reboot:Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Asset{"

    .line 2278
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
