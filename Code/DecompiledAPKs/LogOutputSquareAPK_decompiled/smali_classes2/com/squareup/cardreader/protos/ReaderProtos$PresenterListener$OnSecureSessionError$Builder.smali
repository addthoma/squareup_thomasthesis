.class public final Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError;",
        "Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public result:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 5306
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError;
    .locals 3

    .line 5316
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$Builder;->result:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError;-><init>(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 5303
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError;

    move-result-object v0

    return-object v0
.end method

.method public result(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;)Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$Builder;
    .locals 0

    .line 5310
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$Builder;->result:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    return-object p0
.end method
