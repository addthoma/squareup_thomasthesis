.class public final Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError;
.super Lcom/squareup/wire/Message;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OnSecureSessionError"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$ProtoAdapter_OnSecureSessionError;,
        Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;,
        Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError;",
        "Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_RESULT:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

.field private static final serialVersionUID:J


# instance fields
.field public final result:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 5247
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$ProtoAdapter_OnSecureSessionError;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$ProtoAdapter_OnSecureSessionError;-><init>()V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 5251
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->SUCCESS:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError;->DEFAULT_RESULT:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;)V
    .locals 1

    .line 5260
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, v0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError;-><init>(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;Lokio/ByteString;)V
    .locals 1

    .line 5264
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 5265
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError;->result:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 5279
    :cond_0
    instance-of v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 5280
    :cond_1
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError;

    .line 5281
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError;->result:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    iget-object p1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError;->result:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    .line 5282
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 5287
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1

    .line 5289
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 5290
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError;->result:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 5291
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$Builder;
    .locals 2

    .line 5270
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$Builder;-><init>()V

    .line 5271
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError;->result:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$Builder;->result:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    .line 5272
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 5246
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError;->newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 5298
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 5299
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError;->result:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    if-eqz v1, :cond_0

    const-string v1, ", result="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError;->result:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError$SecureSessionResult;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "OnSecureSessionError{"

    .line 5300
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
