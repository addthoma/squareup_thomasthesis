.class public final enum Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;
.super Ljava/lang/Enum;
.source "ReaderProtos.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "FirmwareUpdateResult"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult$ProtoAdapter_FirmwareUpdateResult;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum BAD_ARGUMENT:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

.field public static final enum BAD_ENCRYPTION_KEY:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

.field public static final enum BAD_HEADER:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

.field public static final enum BAD_WRITE_ALIGNMENT:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

.field public static final enum CRQ_UNRESPONSIVE:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

.field public static final enum DECRYPTION_FAILURE:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

.field public static final enum DUPLICATE_UPDATE_TO_SLOT:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

.field public static final enum ENCRYPTED_UPDATE_REQUIRED:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

.field public static final enum ERROR_UNKNOWN:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

.field public static final enum FLASH_FAILURE:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

.field public static final enum INVALID_IMAGE:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

.field public static final enum INVALID_IMAGE_HEADER:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

.field public static final enum INVALID_IMAGE_VERSION:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

.field public static final enum NONE:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

.field public static final enum NOT_INITIALIZED:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

.field public static final enum SEND_DATA:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

.field public static final enum SEND_DATA_INVALID_IMAGE:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

.field public static final enum SEND_DATA_INVALID_IMAGE_HEADER:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

.field public static final enum SEND_DATA_INVALID_IMAGE_WILL_RETRY:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

.field public static final enum SUCCESS:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 3469
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    const/4 v1, 0x0

    const-string v2, "SUCCESS"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->SUCCESS:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    .line 3471
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    const/4 v2, 0x1

    const-string v3, "SEND_DATA"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->SEND_DATA:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    .line 3473
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    const/4 v3, 0x2

    const-string v4, "SEND_DATA_INVALID_IMAGE_HEADER"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->SEND_DATA_INVALID_IMAGE_HEADER:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    .line 3475
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    const/4 v4, 0x3

    const-string v5, "SEND_DATA_INVALID_IMAGE"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->SEND_DATA_INVALID_IMAGE:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    .line 3477
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    const/4 v5, 0x4

    const-string v6, "SEND_DATA_INVALID_IMAGE_WILL_RETRY"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->SEND_DATA_INVALID_IMAGE_WILL_RETRY:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    .line 3479
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    const/4 v6, 0x5

    const-string v7, "NOT_INITIALIZED"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->NOT_INITIALIZED:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    .line 3481
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    const/4 v7, 0x6

    const-string v8, "INVALID_IMAGE_HEADER"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->INVALID_IMAGE_HEADER:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    .line 3483
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    const/4 v8, 0x7

    const-string v9, "INVALID_IMAGE"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->INVALID_IMAGE:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    .line 3485
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    const/16 v9, 0x8

    const-string v10, "DECRYPTION_FAILURE"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->DECRYPTION_FAILURE:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    .line 3487
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    const/16 v10, 0x9

    const-string v11, "FLASH_FAILURE"

    invoke-direct {v0, v11, v10, v10}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->FLASH_FAILURE:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    .line 3489
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    const/16 v11, 0xa

    const-string v12, "BAD_ARGUMENT"

    invoke-direct {v0, v12, v11, v11}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->BAD_ARGUMENT:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    .line 3491
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    const/16 v12, 0xb

    const-string v13, "BAD_HEADER"

    invoke-direct {v0, v13, v12, v12}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->BAD_HEADER:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    .line 3493
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    const/16 v13, 0xc

    const-string v14, "BAD_WRITE_ALIGNMENT"

    invoke-direct {v0, v14, v13, v13}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->BAD_WRITE_ALIGNMENT:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    .line 3495
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    const/16 v14, 0xd

    const-string v15, "BAD_ENCRYPTION_KEY"

    invoke-direct {v0, v15, v14, v14}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->BAD_ENCRYPTION_KEY:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    .line 3497
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    const/16 v15, 0xe

    const-string v14, "DUPLICATE_UPDATE_TO_SLOT"

    invoke-direct {v0, v14, v15, v15}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->DUPLICATE_UPDATE_TO_SLOT:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    .line 3499
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    const-string v14, "ENCRYPTED_UPDATE_REQUIRED"

    const/16 v15, 0xf

    const/16 v13, 0xf

    invoke-direct {v0, v14, v15, v13}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->ENCRYPTED_UPDATE_REQUIRED:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    .line 3501
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    const-string v13, "ERROR_UNKNOWN"

    const/16 v14, 0x10

    const/16 v15, 0x10

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->ERROR_UNKNOWN:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    .line 3503
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    const-string v13, "INVALID_IMAGE_VERSION"

    const/16 v14, 0x11

    const/16 v15, 0x11

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->INVALID_IMAGE_VERSION:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    .line 3505
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    const-string v13, "CRQ_UNRESPONSIVE"

    const/16 v14, 0x12

    const/16 v15, 0x12

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->CRQ_UNRESPONSIVE:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    .line 3507
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    const-string v13, "NONE"

    const/16 v14, 0x13

    const/16 v15, 0xff

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->NONE:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    const/16 v0, 0x14

    new-array v0, v0, [Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    .line 3468
    sget-object v13, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->SUCCESS:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    aput-object v13, v0, v1

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->SEND_DATA:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->SEND_DATA_INVALID_IMAGE_HEADER:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->SEND_DATA_INVALID_IMAGE:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->SEND_DATA_INVALID_IMAGE_WILL_RETRY:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->NOT_INITIALIZED:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->INVALID_IMAGE_HEADER:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->INVALID_IMAGE:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->DECRYPTION_FAILURE:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->FLASH_FAILURE:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->BAD_ARGUMENT:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->BAD_HEADER:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->BAD_WRITE_ALIGNMENT:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->BAD_ENCRYPTION_KEY:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->DUPLICATE_UPDATE_TO_SLOT:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->ENCRYPTED_UPDATE_REQUIRED:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->ERROR_UNKNOWN:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->INVALID_IMAGE_VERSION:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->CRQ_UNRESPONSIVE:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->NONE:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->$VALUES:[Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    .line 3509
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult$ProtoAdapter_FirmwareUpdateResult;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult$ProtoAdapter_FirmwareUpdateResult;-><init>()V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 3513
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 3514
    iput p3, p0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;
    .locals 1

    const/16 v0, 0xff

    if-eq p0, v0, :cond_0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 3540
    :pswitch_0
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->CRQ_UNRESPONSIVE:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    return-object p0

    .line 3539
    :pswitch_1
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->INVALID_IMAGE_VERSION:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    return-object p0

    .line 3538
    :pswitch_2
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->ERROR_UNKNOWN:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    return-object p0

    .line 3537
    :pswitch_3
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->ENCRYPTED_UPDATE_REQUIRED:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    return-object p0

    .line 3536
    :pswitch_4
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->DUPLICATE_UPDATE_TO_SLOT:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    return-object p0

    .line 3535
    :pswitch_5
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->BAD_ENCRYPTION_KEY:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    return-object p0

    .line 3534
    :pswitch_6
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->BAD_WRITE_ALIGNMENT:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    return-object p0

    .line 3533
    :pswitch_7
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->BAD_HEADER:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    return-object p0

    .line 3532
    :pswitch_8
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->BAD_ARGUMENT:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    return-object p0

    .line 3531
    :pswitch_9
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->FLASH_FAILURE:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    return-object p0

    .line 3530
    :pswitch_a
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->DECRYPTION_FAILURE:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    return-object p0

    .line 3529
    :pswitch_b
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->INVALID_IMAGE:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    return-object p0

    .line 3528
    :pswitch_c
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->INVALID_IMAGE_HEADER:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    return-object p0

    .line 3527
    :pswitch_d
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->NOT_INITIALIZED:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    return-object p0

    .line 3526
    :pswitch_e
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->SEND_DATA_INVALID_IMAGE_WILL_RETRY:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    return-object p0

    .line 3525
    :pswitch_f
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->SEND_DATA_INVALID_IMAGE:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    return-object p0

    .line 3524
    :pswitch_10
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->SEND_DATA_INVALID_IMAGE_HEADER:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    return-object p0

    .line 3523
    :pswitch_11
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->SEND_DATA:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    return-object p0

    .line 3522
    :pswitch_12
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->SUCCESS:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    return-object p0

    .line 3541
    :cond_0
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->NONE:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;
    .locals 1

    .line 3468
    const-class v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    return-object p0
.end method

.method public static values()[Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;
    .locals 1

    .line 3468
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->$VALUES:[Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    invoke-virtual {v0}, [Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 3548
    iget v0, p0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->value:I

    return v0
.end method
