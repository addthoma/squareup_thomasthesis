.class public abstract Lcom/squareup/cardreader/PaymentInteraction;
.super Ljava/lang/Object;
.source "PaymentInteraction.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001B\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u0082\u0001\u0002\u0007\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/cardreader/PaymentInteraction;",
        "",
        "amountAuthorized",
        "",
        "(J)V",
        "getAmountAuthorized",
        "()J",
        "Lcom/squareup/cardreader/EmvPaymentInteraction;",
        "Lcom/squareup/cardreader/TmnPaymentInteraction;",
        "dipper_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final amountAuthorized:J


# direct methods
.method private constructor <init>(J)V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/squareup/cardreader/PaymentInteraction;->amountAuthorized:J

    return-void
.end method

.method public synthetic constructor <init>(JLkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 13
    invoke-direct {p0, p1, p2}, Lcom/squareup/cardreader/PaymentInteraction;-><init>(J)V

    return-void
.end method


# virtual methods
.method public getAmountAuthorized()J
    .locals 2

    .line 13
    iget-wide v0, p0, Lcom/squareup/cardreader/PaymentInteraction;->amountAuthorized:J

    return-wide v0
.end method
