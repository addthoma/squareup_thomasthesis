.class public Lcom/squareup/cardreader/StandardMessageResources$MessageResources;
.super Ljava/lang/Object;
.source "StandardMessageResources.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/StandardMessageResources;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MessageResources"
.end annotation


# instance fields
.field public final glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public final localizedMessage:Ljava/lang/String;

.field public final localizedTitle:Ljava/lang/String;

.field public final messageId:I

.field public final titleId:I


# direct methods
.method public constructor <init>(Lcom/squareup/glyph/GlyphTypeface$Glyph;II)V
    .locals 0

    .line 136
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 137
    iput-object p1, p0, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 138
    iput p2, p0, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;->titleId:I

    .line 139
    iput p3, p0, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;->messageId:I

    const/4 p1, 0x0

    .line 140
    iput-object p1, p0, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;->localizedTitle:Ljava/lang/String;

    .line 141
    iput-object p1, p0, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;->localizedMessage:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 144
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 145
    iput-object p1, p0, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 p1, 0x0

    .line 146
    iput p1, p0, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;->titleId:I

    .line 147
    iput p1, p0, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;->messageId:I

    .line 148
    iput-object p2, p0, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;->localizedTitle:Ljava/lang/String;

    .line 149
    iput-object p3, p0, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;->localizedMessage:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 1

    .line 153
    iget-object v0, p0, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object v0
.end method

.method public getMessage(Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 1

    .line 166
    iget-object v0, p0, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;->localizedMessage:Ljava/lang/String;

    if-eqz v0, :cond_0

    return-object v0

    .line 169
    :cond_0
    iget v0, p0, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;->messageId:I

    if-nez v0, :cond_1

    const/4 p1, 0x0

    goto :goto_0

    :cond_1
    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public getTitle(Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 1

    .line 157
    iget-object v0, p0, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;->localizedTitle:Ljava/lang/String;

    if-eqz v0, :cond_0

    return-object v0

    .line 160
    :cond_0
    iget v0, p0, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;->titleId:I

    if-eqz v0, :cond_1

    .line 161
    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 160
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "MessageResources must have a title!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
