.class public final enum Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;
.super Ljava/lang/Enum;
.source "ReaderMessage.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CardAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\u0011\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\u0008\u0003j\u0002\u0008\u0004j\u0002\u0008\u0005j\u0002\u0008\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008j\u0002\u0008\tj\u0002\u0008\nj\u0002\u0008\u000bj\u0002\u0008\u000cj\u0002\u0008\rj\u0002\u0008\u000ej\u0002\u0008\u000fj\u0002\u0008\u0010j\u0002\u0008\u0011\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;",
        "",
        "(Ljava/lang/String;I)V",
        "None",
        "InsertCard",
        "ReinsertCard",
        "RequestSwipeTechnical",
        "RequestSwipeScheme",
        "SwipeAgain",
        "InsertFromContactless",
        "ContactlessErrorTryAnotherCard",
        "ContactlessErrorTryAgain",
        "ContactlessSeePhone",
        "ContactlessPresentOnlyOne",
        "ContactlessUnlockPhoneToPay",
        "ContactlessCardLimitExceededTryAnotherCard",
        "ContactlessLimitExceededInsertCard",
        "RequestTap",
        "lcr-data-models_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

.field public static final enum ContactlessCardLimitExceededTryAnotherCard:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

.field public static final enum ContactlessErrorTryAgain:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

.field public static final enum ContactlessErrorTryAnotherCard:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

.field public static final enum ContactlessLimitExceededInsertCard:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

.field public static final enum ContactlessPresentOnlyOne:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

.field public static final enum ContactlessSeePhone:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

.field public static final enum ContactlessUnlockPhoneToPay:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

.field public static final enum InsertCard:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

.field public static final enum InsertFromContactless:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

.field public static final enum None:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

.field public static final enum ReinsertCard:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

.field public static final enum RequestSwipeScheme:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

.field public static final enum RequestSwipeTechnical:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

.field public static final enum RequestTap:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

.field public static final enum SwipeAgain:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v0, 0xf

    new-array v0, v0, [Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

    const/4 v2, 0x0

    const-string v3, "None"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;->None:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

    const/4 v2, 0x1

    const-string v3, "InsertCard"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;->InsertCard:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

    const/4 v2, 0x2

    const-string v3, "ReinsertCard"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;->ReinsertCard:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

    const/4 v2, 0x3

    const-string v3, "RequestSwipeTechnical"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;->RequestSwipeTechnical:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

    const/4 v2, 0x4

    const-string v3, "RequestSwipeScheme"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;->RequestSwipeScheme:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

    const/4 v2, 0x5

    const-string v3, "SwipeAgain"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;->SwipeAgain:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

    const/4 v2, 0x6

    const-string v3, "InsertFromContactless"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;->InsertFromContactless:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

    const/4 v2, 0x7

    const-string v3, "ContactlessErrorTryAnotherCard"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;->ContactlessErrorTryAnotherCard:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

    const/16 v2, 0x8

    const-string v3, "ContactlessErrorTryAgain"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;->ContactlessErrorTryAgain:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

    const/16 v2, 0x9

    const-string v3, "ContactlessSeePhone"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;->ContactlessSeePhone:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

    const/16 v2, 0xa

    const-string v3, "ContactlessPresentOnlyOne"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;->ContactlessPresentOnlyOne:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

    const/16 v2, 0xb

    const-string v3, "ContactlessUnlockPhoneToPay"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;->ContactlessUnlockPhoneToPay:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

    const/16 v2, 0xc

    const-string v3, "ContactlessCardLimitExceededTryAnotherCard"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;->ContactlessCardLimitExceededTryAnotherCard:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

    const/16 v2, 0xd

    const-string v3, "ContactlessLimitExceededInsertCard"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;->ContactlessLimitExceededInsertCard:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

    const/16 v2, 0xe

    const-string v3, "RequestTap"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;->RequestTap:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;->$VALUES:[Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 470
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;
    .locals 1

    const-class v0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

    return-object p0
.end method

.method public static values()[Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;
    .locals 1

    sget-object v0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;->$VALUES:[Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

    invoke-virtual {v0}, [Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

    return-object v0
.end method
