.class public final Lcom/squareup/cardreader/CardReaderModule_ProvideNfcListenerFactory;
.super Ljava/lang/Object;
.source "CardReaderModule_ProvideNfcListenerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/CardReaderDispatch$NfcPaymentListener;",
        ">;"
    }
.end annotation


# instance fields
.field private final paymentProcessorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/PaymentProcessor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/PaymentProcessor;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderModule_ProvideNfcListenerFactory;->paymentProcessorProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/cardreader/CardReaderModule_ProvideNfcListenerFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/PaymentProcessor;",
            ">;)",
            "Lcom/squareup/cardreader/CardReaderModule_ProvideNfcListenerFactory;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/cardreader/CardReaderModule_ProvideNfcListenerFactory;

    invoke-direct {v0, p0}, Lcom/squareup/cardreader/CardReaderModule_ProvideNfcListenerFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideNfcListener(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/CardReaderDispatch$NfcPaymentListener;
    .locals 1

    .line 36
    invoke-static {p0}, Lcom/squareup/cardreader/CardReaderModule;->provideNfcListener(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/CardReaderDispatch$NfcPaymentListener;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/CardReaderDispatch$NfcPaymentListener;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/CardReaderDispatch$NfcPaymentListener;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderModule_ProvideNfcListenerFactory;->paymentProcessorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/PaymentProcessor;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderModule_ProvideNfcListenerFactory;->provideNfcListener(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/CardReaderDispatch$NfcPaymentListener;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/cardreader/CardReaderModule_ProvideNfcListenerFactory;->get()Lcom/squareup/cardreader/CardReaderDispatch$NfcPaymentListener;

    move-result-object v0

    return-object v0
.end method
