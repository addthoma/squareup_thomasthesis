.class public abstract Lcom/squareup/cardreader/squid/common/SpeCardReaderModule;
.super Ljava/lang/Object;
.source "SpeCardReaderModule.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/ms/NoopMinesweeperModule;
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/squid/common/SpeCardReaderModule$NoOpBluetoothUtils;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideCardReaderAddress()Ljava/lang/String;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method static provideCardReaderGraphInitializer()Lcom/squareup/cardreader/CardReaderFactory$CardReaderGraphInitializer;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 58
    new-instance v0, Lcom/squareup/cardreader/squid/common/SpeCardReaderModule$1;

    invoke-direct {v0}, Lcom/squareup/cardreader/squid/common/SpeCardReaderModule$1;-><init>()V

    return-object v0
.end method

.method static provideCardReaderName()Ljava/lang/String;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method static provideConnectionType()Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 33
    sget-object v0, Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;->RPC:Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

    return-object v0
.end method

.method static provideFakeBluetoothUtils()Lcom/squareup/cardreader/BluetoothUtils;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 69
    new-instance v0, Lcom/squareup/cardreader/squid/common/SpeCardReaderModule$NoOpBluetoothUtils;

    invoke-direct {v0}, Lcom/squareup/cardreader/squid/common/SpeCardReaderModule$NoOpBluetoothUtils;-><init>()V

    return-object v0
.end method

.method static provideLcrBackend(Lcom/squareup/cardreader/squid/common/SpeBackend;)Lcom/squareup/cardreader/LcrBackend;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    return-object p0
.end method

.method static provideX2Backend(Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;)Lcom/squareup/cardreader/squid/common/SpeBackend;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 48
    new-instance v0, Lcom/squareup/cardreader/squid/common/SpeBackend;

    invoke-direct {v0, p0}, Lcom/squareup/cardreader/squid/common/SpeBackend;-><init>(Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;)V

    return-object v0
.end method
