.class public Lcom/squareup/cardreader/squid/common/SpeBackend;
.super Ljava/lang/Object;
.source "SpeBackend.java"

# interfaces
.implements Lcom/squareup/cardreader/LcrBackend;


# instance fields
.field private final cardreaderNative:Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;)V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/squareup/cardreader/squid/common/SpeBackend;->cardreaderNative:Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;

    return-void
.end method


# virtual methods
.method public cardReaderInitialized()V
    .locals 0

    return-void
.end method

.method public initialize(Lcom/squareup/cardreader/TimerApiLegacy$TimerConfig;Lcom/squareup/cardreader/CardReaderFeatureLegacy$CardReaderFeatureListener;Lcom/squareup/cardreader/CardReaderFeatureLegacy;)Lcom/squareup/cardreader/CardReaderPointer;
    .locals 1

    .line 22
    new-instance p2, Lcom/squareup/cardreader/CardReaderPointer;

    iget-object v0, p0, Lcom/squareup/cardreader/squid/common/SpeBackend;->cardreaderNative:Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;

    iget-object p1, p1, Lcom/squareup/cardreader/TimerApiLegacy$TimerConfig;->timer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_crs_timer_api_t;

    .line 23
    invoke-interface {v0, p1, p3}, Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;->cardreader_initialize_rpc(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_crs_timer_api_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/squareup/cardreader/CardReaderPointer;-><init>(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)V

    return-object p2
.end method

.method public onResume()V
    .locals 0

    return-void
.end method

.method public powerOnReader()V
    .locals 0

    return-void
.end method

.method public resetBackend()V
    .locals 0

    return-void
.end method
