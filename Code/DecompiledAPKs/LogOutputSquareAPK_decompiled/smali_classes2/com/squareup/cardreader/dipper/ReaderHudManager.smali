.class public Lcom/squareup/cardreader/dipper/ReaderHudManager;
.super Ljava/lang/Object;
.source "ReaderHudManager.java"


# instance fields
.field private final application:Landroid/app/Application;

.field private final cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final hudToaster:Lcom/squareup/hudtoaster/HudToaster;

.field private final paymentCounter:Lcom/squareup/cardreader/PaymentCounter;

.field private final readerBatteryStatusHandler:Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;

.field private final readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

.field private final res:Lcom/squareup/util/Res;

.field private final storedCardReaders:Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;


# direct methods
.method public constructor <init>(Landroid/app/Application;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/cardreader/PaymentCounter;Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;Lcom/squareup/log/ReaderEventLogger;Lcom/squareup/settings/server/Features;Lcom/squareup/util/Res;Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/squareup/cardreader/dipper/ReaderHudManager;->application:Landroid/app/Application;

    .line 44
    iput-object p3, p0, Lcom/squareup/cardreader/dipper/ReaderHudManager;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    .line 45
    iput-object p2, p0, Lcom/squareup/cardreader/dipper/ReaderHudManager;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    .line 46
    iput-object p4, p0, Lcom/squareup/cardreader/dipper/ReaderHudManager;->paymentCounter:Lcom/squareup/cardreader/PaymentCounter;

    .line 47
    iput-object p5, p0, Lcom/squareup/cardreader/dipper/ReaderHudManager;->readerBatteryStatusHandler:Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;

    .line 48
    iput-object p6, p0, Lcom/squareup/cardreader/dipper/ReaderHudManager;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    .line 49
    iput-object p7, p0, Lcom/squareup/cardreader/dipper/ReaderHudManager;->features:Lcom/squareup/settings/server/Features;

    .line 50
    iput-object p8, p0, Lcom/squareup/cardreader/dipper/ReaderHudManager;->res:Lcom/squareup/util/Res;

    .line 51
    iput-object p9, p0, Lcom/squareup/cardreader/dipper/ReaderHudManager;->storedCardReaders:Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;

    return-void
.end method

.method private static disconnected(Lcom/squareup/protos/client/bills/CardData$ReaderType;)I
    .locals 1

    .line 158
    sget-object v0, Lcom/squareup/cardreader/dipper/ReaderHudManager$1;->$SwitchMap$com$squareup$protos$client$bills$CardData$ReaderType:[I

    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardData$ReaderType;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x6

    if-eq p0, v0, :cond_0

    const/4 v0, 0x7

    if-eq p0, v0, :cond_0

    .line 163
    sget p0, Lcom/squareup/cardreader/vector/icons/R$drawable;->icon_audio_reader_slash_120:I

    return p0

    .line 161
    :cond_0
    sget p0, Lcom/squareup/cardreader/vector/icons/R$drawable;->icon_r12_reader_slash_120:I

    return p0
.end method

.method private static hud(Lcom/squareup/protos/client/bills/CardData$ReaderType;)I
    .locals 3

    .line 146
    sget-object v0, Lcom/squareup/cardreader/dipper/ReaderHudManager$1;->$SwitchMap$com$squareup$protos$client$bills$CardData$ReaderType:[I

    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardData$ReaderType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_1

    const/4 v1, 0x7

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 153
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot create hud for: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 149
    :cond_1
    :goto_0
    sget p0, Lcom/squareup/cardreader/vector/icons/R$drawable;->icon_r12_reader_120:I

    return p0

    .line 151
    :cond_2
    sget p0, Lcom/squareup/cardreader/vector/icons/R$drawable;->icon_audio_reader_120:I

    return p0
.end method

.method private permissionsAllowToast()Z
    .locals 2

    .line 141
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderHudManager;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->IGNORE_SYSTEM_PERMISSIONS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/squareup/systempermissions/SystemPermission;->MICROPHONE:Lcom/squareup/systempermissions/SystemPermission;

    iget-object v1, p0, Lcom/squareup/cardreader/dipper/ReaderHudManager;->application:Landroid/app/Application;

    .line 142
    invoke-virtual {v0, v1}, Lcom/squareup/systempermissions/SystemPermission;->hasPermission(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method


# virtual methods
.method public toastBatteryStatus(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 1

    .line 129
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderHudManager;->readerBatteryStatusHandler:Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;->toastBatteryLevel(Lcom/squareup/cardreader/CardReaderInfo;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderHudManager;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    invoke-virtual {v0, p1}, Lcom/squareup/log/ReaderEventLogger;->logBatteryHud(Lcom/squareup/cardreader/CardReaderInfo;)V

    :cond_0
    return-void
.end method

.method public toastCrucialFirmwareUpdate(Lcom/squareup/cardreader/CardReaderInfo;)Z
    .locals 4

    .line 91
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderHudManager;->storedCardReaders:Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getAddress()Ljava/lang/String;

    move-result-object v1

    .line 92
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v2

    .line 91
    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;->lookupNickname(Ljava/lang/String;Lcom/squareup/protos/client/bills/CardData$ReaderType;)Ljava/lang/String;

    move-result-object v0

    .line 93
    iget-object v1, p0, Lcom/squareup/cardreader/dipper/ReaderHudManager;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/cardreader/ui/R$string;->smart_reader_updating:I

    .line 94
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    const-string v2, "reader"

    invoke-virtual {v1, v2, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 96
    iget-object v1, p0, Lcom/squareup/cardreader/dipper/ReaderHudManager;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/cardreader/dipper/ReaderHudManager;->hud(Lcom/squareup/protos/client/bills/CardData$ReaderType;)I

    move-result p1

    iget-object v2, p0, Lcom/squareup/cardreader/dipper/ReaderHudManager;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/cardreader/ui/R$string;->please_wait:I

    .line 97
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 96
    invoke-interface {v1, p1, v0, v2}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(ILjava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p1

    return p1
.end method

.method public toastIfAnyReaderBatteryLow()V
    .locals 4

    .line 110
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderHudManager;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderHub;->getCardReaders()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/CardReader;

    .line 111
    iget-object v2, p0, Lcom/squareup/cardreader/dipper/ReaderHudManager;->paymentCounter:Lcom/squareup/cardreader/PaymentCounter;

    invoke-interface {v1}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/cardreader/PaymentCounter;->shouldDisplayPowerHud(Lcom/squareup/cardreader/CardReaderId;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 112
    invoke-interface {v1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v2

    .line 114
    iget-object v3, p0, Lcom/squareup/cardreader/dipper/ReaderHudManager;->readerBatteryStatusHandler:Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;

    invoke-virtual {v3, v2}, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;->toastBatteryLevelIfLow(Lcom/squareup/cardreader/CardReaderInfo;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 115
    iget-object v3, p0, Lcom/squareup/cardreader/dipper/ReaderHudManager;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    invoke-virtual {v3, v2}, Lcom/squareup/log/ReaderEventLogger;->logBatteryHud(Lcom/squareup/cardreader/CardReaderInfo;)V

    .line 116
    iget-object v2, p0, Lcom/squareup/cardreader/dipper/ReaderHudManager;->paymentCounter:Lcom/squareup/cardreader/PaymentCounter;

    invoke-interface {v1}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/squareup/cardreader/PaymentCounter;->powerHudDisplayed(Lcom/squareup/cardreader/CardReaderId;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public toastIfBatteryDead(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 1

    .line 123
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->isBatteryDead()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 124
    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/dipper/ReaderHudManager;->toastBatteryStatus(Lcom/squareup/cardreader/CardReaderInfo;)V

    :cond_0
    return-void
.end method

.method public toastLegacyReaderConnected()Z
    .locals 6

    .line 55
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderHudManager;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_MAGSTRIPE_ONLY_READERS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 56
    invoke-direct {p0}, Lcom/squareup/cardreader/dipper/ReaderHudManager;->permissionsAllowToast()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderHudManager;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget v2, Lcom/squareup/cardreader/vector/icons/R$drawable;->icon_audio_reader_120:I

    sget v3, Lcom/squareup/cardreader/ui/R$string;->hud_legacy_reader_connected:I

    iget-object v4, p0, Lcom/squareup/cardreader/dipper/ReaderHudManager;->features:Lcom/squareup/settings/server/Features;

    sget-object v5, Lcom/squareup/settings/server/Features$Feature;->COUNTRY_PREFERS_EMV:Lcom/squareup/settings/server/Features$Feature;

    .line 60
    invoke-interface {v4, v5}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x0

    goto :goto_0

    :cond_0
    sget v4, Lcom/squareup/cardreader/ui/R$string;->swipe_card_to_charge:I

    .line 57
    :goto_0
    invoke-interface {v0, v2, v3, v4}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(III)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method public toastReaderDisconnected(Lcom/squareup/protos/client/bills/CardData$ReaderType;)Z
    .locals 3

    .line 66
    sget-object v0, Lcom/squareup/cardreader/dipper/ReaderHudManager$1;->$SwitchMap$com$squareup$protos$client$bills$CardData$ReaderType:[I

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData$ReaderType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x0

    packed-switch v0, :pswitch_data_0

    .line 84
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot handle "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 81
    :pswitch_0
    sget v0, Lcom/squareup/cardreader/ui/R$string;->hud_contactless_chip_reader_disconnected:I

    goto :goto_0

    .line 77
    :pswitch_1
    sget v0, Lcom/squareup/cardreader/ui/R$string;->hud_chip_reader_disconnected:I

    goto :goto_0

    .line 71
    :pswitch_2
    invoke-direct {p0}, Lcom/squareup/cardreader/dipper/ReaderHudManager;->permissionsAllowToast()Z

    move-result v0

    if-nez v0, :cond_0

    return v1

    .line 74
    :cond_0
    sget v0, Lcom/squareup/cardreader/ui/R$string;->hud_legacy_reader_disconnected:I

    .line 87
    :goto_0
    iget-object v2, p0, Lcom/squareup/cardreader/dipper/ReaderHudManager;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    invoke-static {p1}, Lcom/squareup/cardreader/dipper/ReaderHudManager;->disconnected(Lcom/squareup/protos/client/bills/CardData$ReaderType;)I

    move-result p1

    invoke-interface {v2, p1, v0, v1}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(III)Z

    move-result p1

    return p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public toastReaderStillConnecting(I)V
    .locals 3

    .line 135
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderHudManager;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v2, Lcom/squareup/cardreader/ui/R$string;->secure_session_required_for_swipe_message:I

    invoke-interface {v0, v1, p1, v2}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/glyph/GlyphTypeface$Glyph;II)Z

    return-void
.end method

.method public toastReinsertChipCardToCharge()Z
    .locals 2

    .line 101
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderHudManager;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v1, Lcom/squareup/ui/cardreader/HudToasts;->MUST_REINSERT_CARD_FOR_CHARGE:Lcom/squareup/ui/cardreader/HudToasts;

    invoke-interface {v0, v1}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/hudtoaster/HudToaster$ToastBundle;)Z

    move-result v0

    return v0
.end method

.method public toastRemoveChipCard()Z
    .locals 2

    .line 106
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderHudManager;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v1, Lcom/squareup/ui/cardreader/HudToasts;->MUST_REMOVE_CARD:Lcom/squareup/ui/cardreader/HudToasts;

    invoke-interface {v0, v1}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/hudtoaster/HudToaster$ToastBundle;)Z

    move-result v0

    return v0
.end method
