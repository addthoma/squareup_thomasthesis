.class public Lcom/squareup/cardreader/dipper/HeadsetStateInitializer;
.super Ljava/lang/Object;
.source "HeadsetStateInitializer.java"

# interfaces
.implements Lmortar/Scoped;


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;

.field private final headsetStateDispatcher:Lcom/squareup/cardreader/HeadsetStateDispatcher;


# direct methods
.method constructor <init>(Lcom/squareup/cardreader/HeadsetStateDispatcher;Lcom/squareup/settings/server/Features;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/cardreader/dipper/HeadsetStateInitializer;->headsetStateDispatcher:Lcom/squareup/cardreader/HeadsetStateDispatcher;

    .line 22
    iput-object p2, p0, Lcom/squareup/cardreader/dipper/HeadsetStateInitializer;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 26
    iget-object p1, p0, Lcom/squareup/cardreader/dipper/HeadsetStateInitializer;->headsetStateDispatcher:Lcom/squareup/cardreader/HeadsetStateDispatcher;

    iget-object v0, p0, Lcom/squareup/cardreader/dipper/HeadsetStateInitializer;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ELIGIBLE_FOR_SQUARE_CARD_PROCESSING:Lcom/squareup/settings/server/Features$Feature;

    .line 27
    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 26
    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/HeadsetStateDispatcher;->setHeadsetEnabled(Ljava/lang/Boolean;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method
