.class public final synthetic Lcom/squareup/cardreader/dipper/-$$Lambda$ReaderUiEventSink$FZRiQKL44bGAL7emsd7O5U5vYrM;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# instance fields
.field private final synthetic f$0:Lcom/squareup/cardreader/dipper/ReaderUiEventSink;

.field private final synthetic f$1:Lcom/squareup/cardreader/CardReader;

.field private final synthetic f$2:Lcom/squareup/cardreader/CardReaderInfo;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/cardreader/dipper/ReaderUiEventSink;Lcom/squareup/cardreader/CardReader;Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/cardreader/dipper/-$$Lambda$ReaderUiEventSink$FZRiQKL44bGAL7emsd7O5U5vYrM;->f$0:Lcom/squareup/cardreader/dipper/ReaderUiEventSink;

    iput-object p2, p0, Lcom/squareup/cardreader/dipper/-$$Lambda$ReaderUiEventSink$FZRiQKL44bGAL7emsd7O5U5vYrM;->f$1:Lcom/squareup/cardreader/CardReader;

    iput-object p3, p0, Lcom/squareup/cardreader/dipper/-$$Lambda$ReaderUiEventSink$FZRiQKL44bGAL7emsd7O5U5vYrM;->f$2:Lcom/squareup/cardreader/CardReaderInfo;

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Object;)V
    .locals 3

    iget-object v0, p0, Lcom/squareup/cardreader/dipper/-$$Lambda$ReaderUiEventSink$FZRiQKL44bGAL7emsd7O5U5vYrM;->f$0:Lcom/squareup/cardreader/dipper/ReaderUiEventSink;

    iget-object v1, p0, Lcom/squareup/cardreader/dipper/-$$Lambda$ReaderUiEventSink$FZRiQKL44bGAL7emsd7O5U5vYrM;->f$1:Lcom/squareup/cardreader/CardReader;

    iget-object v2, p0, Lcom/squareup/cardreader/dipper/-$$Lambda$ReaderUiEventSink$FZRiQKL44bGAL7emsd7O5U5vYrM;->f$2:Lcom/squareup/cardreader/CardReaderInfo;

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {v0, v1, v2, p1}, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;->lambda$onTamperData$2$ReaderUiEventSink(Lcom/squareup/cardreader/CardReader;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V

    return-void
.end method
