.class public Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;
.super Ljava/lang/Object;
.source "DefaultEmvCardInsertRemoveProcessor.java"

# interfaces
.implements Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;


# instance fields
.field private final badKeyboardHiderIsBad:Lcom/squareup/ui/main/BadKeyboardHider;

.field private final hudToaster:Lcom/squareup/payment/PaymentHudToaster;

.field private final readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

.field private final smartPaymentFlowStarter:Lcom/squareup/ui/main/SmartPaymentFlowStarter;

.field private final tenderInEdit:Lcom/squareup/payment/TenderInEdit;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method public constructor <init>(Lcom/squareup/payment/Transaction;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;Lcom/squareup/ui/main/BadKeyboardHider;Lcom/squareup/ui/main/SmartPaymentFlowStarter;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/payment/PaymentHudToaster;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;->transaction:Lcom/squareup/payment/Transaction;

    .line 39
    iput-object p2, p0, Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;->readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    .line 40
    iput-object p3, p0, Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;->badKeyboardHiderIsBad:Lcom/squareup/ui/main/BadKeyboardHider;

    .line 41
    iput-object p4, p0, Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;->smartPaymentFlowStarter:Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    .line 42
    iput-object p5, p0, Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    .line 43
    iput-object p6, p0, Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;->hudToaster:Lcom/squareup/payment/PaymentHudToaster;

    return-void
.end method


# virtual methods
.method public processEmvCardInserted(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 1

    .line 48
    iget-object p1, p0, Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->hasCard()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 49
    iget-object p1, p0, Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;->transaction:Lcom/squareup/payment/Transaction;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/payment/Transaction;->setCard(Lcom/squareup/Card;)V

    .line 54
    :cond_0
    iget-object p1, p0, Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_1

    return-void

    .line 59
    :cond_1
    iget-object p1, p0, Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->paymentIsWithinRange()Z

    move-result p1

    if-nez p1, :cond_2

    .line 60
    iget-object p1, p0, Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;->hudToaster:Lcom/squareup/payment/PaymentHudToaster;

    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1, v0}, Lcom/squareup/payment/PaymentHudToaster;->toastPaymentOutOfRange(Lcom/squareup/payment/Transaction;)Z

    return-void

    .line 64
    :cond_2
    iget-object p1, p0, Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;->badKeyboardHiderIsBad:Lcom/squareup/ui/main/BadKeyboardHider;

    invoke-virtual {p1}, Lcom/squareup/ui/main/BadKeyboardHider;->hideSoftKeyboard()V

    .line 65
    iget-object p1, p0, Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;->smartPaymentFlowStarter:Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    .line 66
    invoke-virtual {p1}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->getSingleTenderDipWithoutChargeButtonResult()Lcom/squareup/ui/main/SmartPaymentResult;

    move-result-object p1

    .line 67
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;->smartPaymentFlowStarter:Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->navigateForSmartPaymentResult(Lcom/squareup/ui/main/SmartPaymentResult;)V

    return-void
.end method

.method public processEmvCardRemoved(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 2

    .line 72
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->isCardPresenceRequired()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;->transaction:Lcom/squareup/payment/Transaction;

    .line 73
    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->hasBillPayment()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;->transaction:Lcom/squareup/payment/Transaction;

    .line 74
    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/payment/BillPayment;->hasSmartCardTenderInFlight()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;->transaction:Lcom/squareup/payment/Transaction;

    .line 75
    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/payment/BillPayment;->hasSmartCardTenderWithCaptureArgs()Z

    move-result p1

    if-nez p1, :cond_0

    .line 76
    iget-object p1, p0, Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;->transaction:Lcom/squareup/payment/Transaction;

    const/4 v0, 0x0

    sget-object v1, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_HUMAN_INITIATED:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/payment/Transaction;->dropPaymentOrTender(ZLcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    .line 77
    new-instance p1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    invoke-direct {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;-><init>()V

    sget-object v0, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->GENERIC_ERROR:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    .line 79
    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->warningType(Lcom/squareup/cardreader/ui/api/ReaderWarningType;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/cardreader/ui/R$string;->emv_card_removed_title:I

    .line 80
    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->titleId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/cardreader/ui/R$string;->emv_card_removed_msg:I

    .line 81
    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->messageId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 82
    invoke-virtual {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->build()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object p1

    .line 83
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;->readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    new-instance v1, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowWarningScreen;

    invoke-direct {v1, p1}, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowWarningScreen;-><init>(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;)V

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;->requestReaderIssueScreen(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;)V

    return-void

    .line 92
    :cond_0
    iget-object p1, p0, Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {p1}, Lcom/squareup/payment/TenderInEdit;->isSmartCardTender()Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    .line 93
    invoke-interface {p1}, Lcom/squareup/payment/TenderInEdit;->requireSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->hasStartedPaymentOnReader()Z

    move-result p1

    if-nez p1, :cond_1

    .line 94
    iget-object p1, p0, Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {p1}, Lcom/squareup/payment/TenderInEdit;->clearSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    .line 95
    new-instance p1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    invoke-direct {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;-><init>()V

    sget-object v0, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->GENERIC_ERROR:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    .line 97
    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->warningType(Lcom/squareup/cardreader/ui/api/ReaderWarningType;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/cardreader/ui/R$string;->emv_card_removed_title:I

    .line 98
    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->titleId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/cardreader/ui/R$string;->emv_card_removed_msg:I

    .line 99
    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->messageId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 100
    invoke-virtual {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->build()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object p1

    .line 101
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;->readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    new-instance v1, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowWarningScreen;

    invoke-direct {v1, p1}, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowWarningScreen;-><init>(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;)V

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;->requestReaderIssueScreen(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;)V

    :cond_1
    return-void
.end method
