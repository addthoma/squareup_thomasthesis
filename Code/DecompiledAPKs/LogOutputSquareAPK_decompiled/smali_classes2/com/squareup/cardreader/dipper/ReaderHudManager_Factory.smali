.class public final Lcom/squareup/cardreader/dipper/ReaderHudManager_Factory;
.super Ljava/lang/Object;
.source "ReaderHudManager_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/dipper/ReaderHudManager;",
        ">;"
    }
.end annotation


# instance fields
.field private final applicationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderHubProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final hudToasterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentCounterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/PaymentCounter;",
            ">;"
        }
    .end annotation
.end field

.field private final readerBatteryStatusHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final readerEventLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/ReaderEventLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final storedCardReadersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/PaymentCounter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/ReaderEventLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;",
            ">;)V"
        }
    .end annotation

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/squareup/cardreader/dipper/ReaderHudManager_Factory;->applicationProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p2, p0, Lcom/squareup/cardreader/dipper/ReaderHudManager_Factory;->cardReaderHubProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p3, p0, Lcom/squareup/cardreader/dipper/ReaderHudManager_Factory;->hudToasterProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p4, p0, Lcom/squareup/cardreader/dipper/ReaderHudManager_Factory;->paymentCounterProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p5, p0, Lcom/squareup/cardreader/dipper/ReaderHudManager_Factory;->readerBatteryStatusHandlerProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p6, p0, Lcom/squareup/cardreader/dipper/ReaderHudManager_Factory;->readerEventLoggerProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p7, p0, Lcom/squareup/cardreader/dipper/ReaderHudManager_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p8, p0, Lcom/squareup/cardreader/dipper/ReaderHudManager_Factory;->resProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p9, p0, Lcom/squareup/cardreader/dipper/ReaderHudManager_Factory;->storedCardReadersProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/dipper/ReaderHudManager_Factory;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/PaymentCounter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/ReaderEventLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;",
            ">;)",
            "Lcom/squareup/cardreader/dipper/ReaderHudManager_Factory;"
        }
    .end annotation

    .line 70
    new-instance v10, Lcom/squareup/cardreader/dipper/ReaderHudManager_Factory;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/cardreader/dipper/ReaderHudManager_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v10
.end method

.method public static newInstance(Landroid/app/Application;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/cardreader/PaymentCounter;Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;Lcom/squareup/log/ReaderEventLogger;Lcom/squareup/settings/server/Features;Lcom/squareup/util/Res;Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;)Lcom/squareup/cardreader/dipper/ReaderHudManager;
    .locals 11

    .line 77
    new-instance v10, Lcom/squareup/cardreader/dipper/ReaderHudManager;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/cardreader/dipper/ReaderHudManager;-><init>(Landroid/app/Application;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/cardreader/PaymentCounter;Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;Lcom/squareup/log/ReaderEventLogger;Lcom/squareup/settings/server/Features;Lcom/squareup/util/Res;Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;)V

    return-object v10
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/dipper/ReaderHudManager;
    .locals 10

    .line 61
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderHudManager_Factory;->applicationProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/app/Application;

    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderHudManager_Factory;->cardReaderHubProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/cardreader/CardReaderHub;

    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderHudManager_Factory;->hudToasterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/hudtoaster/HudToaster;

    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderHudManager_Factory;->paymentCounterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/cardreader/PaymentCounter;

    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderHudManager_Factory;->readerBatteryStatusHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;

    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderHudManager_Factory;->readerEventLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/log/ReaderEventLogger;

    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderHudManager_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderHudManager_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderHudManager_Factory;->storedCardReadersProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;

    invoke-static/range {v1 .. v9}, Lcom/squareup/cardreader/dipper/ReaderHudManager_Factory;->newInstance(Landroid/app/Application;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/cardreader/PaymentCounter;Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;Lcom/squareup/log/ReaderEventLogger;Lcom/squareup/settings/server/Features;Lcom/squareup/util/Res;Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;)Lcom/squareup/cardreader/dipper/ReaderHudManager;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/squareup/cardreader/dipper/ReaderHudManager_Factory;->get()Lcom/squareup/cardreader/dipper/ReaderHudManager;

    move-result-object v0

    return-object v0
.end method
