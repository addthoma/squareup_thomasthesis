.class public final Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler_Factory;
.super Ljava/lang/Object;
.source "ReaderBatteryStatusHandler_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;",
        ">;"
    }
.end annotation


# instance fields
.field private final batteryToasterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cardreader/BatteryLevelToaster;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderHubProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final readerIssueScreenRequestSinkProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cardreader/BatteryLevelToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;)V"
        }
    .end annotation

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler_Factory;->batteryToasterProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p2, p0, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler_Factory;->resProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p3, p0, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler_Factory;->readerIssueScreenRequestSinkProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p4, p0, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p5, p0, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler_Factory;->cardReaderHubProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cardreader/BatteryLevelToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;)",
            "Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler_Factory;"
        }
    .end annotation

    .line 52
    new-instance v6, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/ui/cardreader/BatteryLevelToaster;Lcom/squareup/util/Res;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;Ldagger/Lazy;Lcom/squareup/cardreader/CardReaderHub;)Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/cardreader/BatteryLevelToaster;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;",
            "Ldagger/Lazy<",
            "Lflow/Flow;",
            ">;",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ")",
            "Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;"
        }
    .end annotation

    .line 58
    new-instance v6, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;-><init>(Lcom/squareup/ui/cardreader/BatteryLevelToaster;Lcom/squareup/util/Res;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;Ldagger/Lazy;Lcom/squareup/cardreader/CardReaderHub;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;
    .locals 5

    .line 45
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler_Factory;->batteryToasterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/cardreader/BatteryLevelToaster;

    iget-object v1, p0, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v2, p0, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler_Factory;->readerIssueScreenRequestSinkProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    iget-object v3, p0, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-static {v3}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler_Factory;->cardReaderHubProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/cardreader/CardReaderHub;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler_Factory;->newInstance(Lcom/squareup/ui/cardreader/BatteryLevelToaster;Lcom/squareup/util/Res;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;Ldagger/Lazy;Lcom/squareup/cardreader/CardReaderHub;)Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler_Factory;->get()Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;

    move-result-object v0

    return-object v0
.end method
