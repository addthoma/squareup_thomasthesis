.class public final Lcom/squareup/cardreader/dipper/ReaderHudConnectionEventHandler_Factory;
.super Ljava/lang/Object;
.source "ReaderHudConnectionEventHandler_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/dipper/ReaderHudConnectionEventHandler;",
        ">;"
    }
.end annotation


# instance fields
.field private final authenticatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/LegacyAuthenticator;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final firmwareUpdateDispatcherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;",
            ">;"
        }
    .end annotation
.end field

.field private final readerHudManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderHudManager;",
            ">;"
        }
    .end annotation
.end field

.field private final readerIssueScreenRequestSinkProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/LegacyAuthenticator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderHudManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;",
            ">;)V"
        }
    .end annotation

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/squareup/cardreader/dipper/ReaderHudConnectionEventHandler_Factory;->authenticatorProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p2, p0, Lcom/squareup/cardreader/dipper/ReaderHudConnectionEventHandler_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p3, p0, Lcom/squareup/cardreader/dipper/ReaderHudConnectionEventHandler_Factory;->firmwareUpdateDispatcherProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p4, p0, Lcom/squareup/cardreader/dipper/ReaderHudConnectionEventHandler_Factory;->readerHudManagerProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p5, p0, Lcom/squareup/cardreader/dipper/ReaderHudConnectionEventHandler_Factory;->readerIssueScreenRequestSinkProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/dipper/ReaderHudConnectionEventHandler_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/LegacyAuthenticator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderHudManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;",
            ">;)",
            "Lcom/squareup/cardreader/dipper/ReaderHudConnectionEventHandler_Factory;"
        }
    .end annotation

    .line 50
    new-instance v6, Lcom/squareup/cardreader/dipper/ReaderHudConnectionEventHandler_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/cardreader/dipper/ReaderHudConnectionEventHandler_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/account/LegacyAuthenticator;Lcom/squareup/settings/server/Features;Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;Lcom/squareup/cardreader/dipper/ReaderHudManager;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;)Lcom/squareup/cardreader/dipper/ReaderHudConnectionEventHandler;
    .locals 7

    .line 57
    new-instance v6, Lcom/squareup/cardreader/dipper/ReaderHudConnectionEventHandler;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/cardreader/dipper/ReaderHudConnectionEventHandler;-><init>(Lcom/squareup/account/LegacyAuthenticator;Lcom/squareup/settings/server/Features;Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;Lcom/squareup/cardreader/dipper/ReaderHudManager;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/dipper/ReaderHudConnectionEventHandler;
    .locals 5

    .line 42
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderHudConnectionEventHandler_Factory;->authenticatorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/account/LegacyAuthenticator;

    iget-object v1, p0, Lcom/squareup/cardreader/dipper/ReaderHudConnectionEventHandler_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/settings/server/Features;

    iget-object v2, p0, Lcom/squareup/cardreader/dipper/ReaderHudConnectionEventHandler_Factory;->firmwareUpdateDispatcherProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;

    iget-object v3, p0, Lcom/squareup/cardreader/dipper/ReaderHudConnectionEventHandler_Factory;->readerHudManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/dipper/ReaderHudManager;

    iget-object v4, p0, Lcom/squareup/cardreader/dipper/ReaderHudConnectionEventHandler_Factory;->readerIssueScreenRequestSinkProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/cardreader/dipper/ReaderHudConnectionEventHandler_Factory;->newInstance(Lcom/squareup/account/LegacyAuthenticator;Lcom/squareup/settings/server/Features;Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;Lcom/squareup/cardreader/dipper/ReaderHudManager;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;)Lcom/squareup/cardreader/dipper/ReaderHudConnectionEventHandler;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/cardreader/dipper/ReaderHudConnectionEventHandler_Factory;->get()Lcom/squareup/cardreader/dipper/ReaderHudConnectionEventHandler;

    move-result-object v0

    return-object v0
.end method
