.class public Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;
.super Ljava/lang/Object;
.source "FirmwareUpdateDispatcher.java"

# interfaces
.implements Lcom/squareup/cardreader/FirmwareUpdater$Listener;
.implements Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;
.implements Lcom/squareup/cardreader/BlePairingListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FailureToReconnectRunner;,
        Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$ReconnectExpirationRunner;,
        Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FirmwareUpdateState;
    }
.end annotation


# static fields
.field static final REBOOT_TRACKING_MILLIS_R12:I = 0xafc8


# instance fields
.field private final expirationRunners:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$ReconnectExpirationRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final firmwareUpdateLogger:Lcom/squareup/log/ReaderEventLogger;

.field private final firmwareUpdateService:Lcom/squareup/cardreader/dipper/FirmwareUpdateService;

.field private final firmwareUpdateSessionIdsByReader:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final listeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/cardreader/dipper/FirmwareUpdateListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private final ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

.field private final readerUpdates:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FirmwareUpdateState;",
            ">;"
        }
    .end annotation
.end field

.field private final serviceStarter:Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationServiceStarter;

.field private final uuidGenerator:Lcom/squareup/log/UUIDGenerator;

.field private final waitingForReconnectReaders:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FailureToReconnectRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationServiceStarter;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/cardreader/dipper/FirmwareUpdateService;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/log/ReaderEventLogger;Lcom/squareup/log/UUIDGenerator;Lcom/squareup/cardreader/CardReaderListeners;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    iput-object p1, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->serviceStarter:Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationServiceStarter;

    .line 81
    iput-object p3, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->firmwareUpdateService:Lcom/squareup/cardreader/dipper/FirmwareUpdateService;

    .line 82
    iput-object p4, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->mainThread:Lcom/squareup/thread/executor/MainThread;

    .line 83
    iput-object p5, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    .line 84
    iput-object p6, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->firmwareUpdateLogger:Lcom/squareup/log/ReaderEventLogger;

    .line 85
    iput-object p7, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->uuidGenerator:Lcom/squareup/log/UUIDGenerator;

    .line 87
    new-instance p1, Ljava/util/LinkedHashSet;

    invoke-direct {p1}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object p1, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->listeners:Ljava/util/Set;

    .line 88
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object p1, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->firmwareUpdateSessionIdsByReader:Ljava/util/Map;

    .line 89
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object p1, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->readerUpdates:Ljava/util/Map;

    .line 90
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object p1, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->waitingForReconnectReaders:Ljava/util/Map;

    .line 91
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object p1, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->expirationRunners:Ljava/util/Map;

    .line 93
    invoke-interface {p8, p0}, Lcom/squareup/cardreader/CardReaderListeners;->addBlePairingListener(Lcom/squareup/cardreader/BlePairingListener;)V

    .line 94
    invoke-virtual {p2, p0}, Lcom/squareup/cardreader/CardReaderHub;->addCardReaderAttachListener(Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;)V

    return-void
.end method

.method static synthetic access$500(Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;)Ljava/util/Map;
    .locals 0

    .line 59
    iget-object p0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->expirationRunners:Ljava/util/Map;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;Ljava/lang/String;)V
    .locals 0

    .line 59
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->removeWaitingForReconnectTimer(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$700(Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;)Ljava/util/Set;
    .locals 0

    .line 59
    iget-object p0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->listeners:Ljava/util/Set;

    return-object p0
.end method

.method private addReaderRebootTimer(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 4

    .line 385
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getAddress()Ljava/lang/String;

    move-result-object v0

    .line 387
    invoke-direct {p0, v0}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->removeWaitingForReconnectTimer(Ljava/lang/String;)V

    .line 388
    new-instance v1, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FailureToReconnectRunner;

    invoke-direct {v1, p0, v0, p1}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FailureToReconnectRunner;-><init>(Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;Ljava/lang/String;Lcom/squareup/cardreader/CardReaderInfo;)V

    .line 389
    iget-object p1, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->waitingForReconnectReaders:Ljava/util/Map;

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 390
    iget-object p1, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->mainThread:Lcom/squareup/thread/executor/MainThread;

    const-wide/32 v2, 0xafc8

    invoke-interface {p1, v1, v2, v3}, Lcom/squareup/thread/executor/MainThread;->executeDelayed(Ljava/lang/Runnable;J)Z

    .line 392
    invoke-direct {p0, v0}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->removeReconnectExpirationRunner(Ljava/lang/String;)V

    .line 393
    new-instance p1, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$ReconnectExpirationRunner;

    invoke-direct {p1, p0, v0}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$ReconnectExpirationRunner;-><init>(Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;Ljava/lang/String;)V

    .line 394
    iget-object v1, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->expirationRunners:Ljava/util/Map;

    invoke-interface {v1, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 395
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->mainThread:Lcom/squareup/thread/executor/MainThread;

    invoke-interface {v0, p1, v2, v3}, Lcom/squareup/thread/executor/MainThread;->executeDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method static isRebootingAsset(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/protos/client/tarkin/Asset$Reboot;)Z
    .locals 3

    .line 366
    sget-object v0, Lcom/squareup/protos/client/tarkin/Asset$Reboot;->YES:Lcom/squareup/protos/client/tarkin/Asset$Reboot;

    const/4 v1, 0x1

    if-ne p1, v0, :cond_0

    return v1

    .line 367
    :cond_0
    sget-object v0, Lcom/squareup/protos/client/tarkin/Asset$Reboot;->NO:Lcom/squareup/protos/client/tarkin/Asset$Reboot;

    const/4 v2, 0x0

    if-ne p1, v0, :cond_1

    return v2

    .line 369
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/cardreader/CardReaderInfo;->isWireless()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/client/tarkin/Asset$Reboot;->BLE:Lcom/squareup/protos/client/tarkin/Asset$Reboot;

    if-ne p1, v0, :cond_2

    return v1

    .line 373
    :cond_2
    invoke-virtual {p0}, Lcom/squareup/cardreader/CardReaderInfo;->isAudio()Z

    move-result p0

    if-eqz p0, :cond_3

    sget-object p0, Lcom/squareup/protos/client/tarkin/Asset$Reboot;->AUDIO:Lcom/squareup/protos/client/tarkin/Asset$Reboot;

    if-ne p1, p0, :cond_3

    return v1

    :cond_3
    return v2
.end method

.method private removeReconnectExpirationRunner(Ljava/lang/String;)V
    .locals 2

    .line 404
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->expirationRunners:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Runnable;

    invoke-interface {v0, p1}, Lcom/squareup/thread/executor/MainThread;->cancel(Ljava/lang/Runnable;)V

    return-void
.end method

.method private removeWaitingForReconnectTimer(Ljava/lang/String;)V
    .locals 1

    .line 399
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->waitingForReconnectReaders:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FailureToReconnectRunner;

    .line 400
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->mainThread:Lcom/squareup/thread/executor/MainThread;

    invoke-interface {v0, p1}, Lcom/squareup/thread/executor/MainThread;->cancel(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method public addFirmwareUpdateListener(Lcom/squareup/cardreader/dipper/FirmwareUpdateListener;)V
    .locals 1

    .line 300
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 301
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->listeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method handleServerCallError(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/receiving/ReceivedResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            "Lcom/squareup/receiving/ReceivedResponse<",
            "+",
            "Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;",
            ">;)V"
        }
    .end annotation

    .line 352
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->firmwareUpdateLogger:Lcom/squareup/log/ReaderEventLogger;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->sessionIdForReader(Lcom/squareup/cardreader/CardReaderInfo;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/squareup/analytics/ReaderEventName;->FW_UPDATE_SERVER_ERROR:Lcom/squareup/analytics/ReaderEventName;

    invoke-virtual {v0, p1, v1, v2}, Lcom/squareup/log/ReaderEventLogger;->logFirmwareUpdateEvent(Lcom/squareup/cardreader/CardReaderInfo;Ljava/lang/String;Lcom/squareup/cardreader/ReaderEventLogger$CardReaderEvent;)V

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    const-string p2, "sendFirmwareManifestToServer failed: %s"

    .line 354
    invoke-static {p2, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 356
    iget-object p2, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->listeners:Ljava/util/Set;

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/dipper/FirmwareUpdateListener;

    .line 357
    invoke-interface {v0, p1}, Lcom/squareup/cardreader/dipper/FirmwareUpdateListener;->onFirmwareManifestServerResponseFailure(Lcom/squareup/cardreader/CardReaderInfo;)V

    goto :goto_0

    .line 360
    :cond_0
    iget-object p2, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->readerUpdates:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getAddress()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 361
    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->removeSessionIdForReader(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void
.end method

.method handleServerCallSuccess(Lcom/squareup/cardreader/CardReader;Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;)V
    .locals 3

    .line 339
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->listeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/dipper/FirmwareUpdateListener;

    .line 340
    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/cardreader/dipper/FirmwareUpdateListener;->onFirmwareManifestServerResponseSuccess(Lcom/squareup/cardreader/CardReaderInfo;)V

    goto :goto_0

    .line 347
    :cond_0
    invoke-interface {p1, p2}, Lcom/squareup/cardreader/CardReader;->processFirmwareUpdateResponse(Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;)V

    return-void
.end method

.method public hasRecentRebootAssetCompleted(Lcom/squareup/cardreader/CardReaderInfo;)Z
    .locals 1

    .line 319
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->expirationRunners:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getAddress()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method hasSessionIdForReader(Ljava/lang/String;)Z
    .locals 1

    .line 417
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->firmwareUpdateSessionIdsByReader:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method isFwupComplete(Ljava/lang/String;)Z
    .locals 1

    .line 381
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->readerUpdates:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    return p1
.end method

.method isInBlockingUpdate(Lcom/squareup/cardreader/CardReaderInfo;)Z
    .locals 1

    .line 314
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->readerUpdates:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getAddress()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FirmwareUpdateState;

    if-eqz p1, :cond_0

    .line 315
    invoke-static {p1}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FirmwareUpdateState;->access$000(Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FirmwareUpdateState;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public isProcessingBlockingR12FirmwareUpdate()Z
    .locals 4

    .line 329
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->readerUpdates:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FirmwareUpdateState;

    .line 330
    invoke-static {v1}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FirmwareUpdateState;->access$000(Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FirmwareUpdateState;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 331
    invoke-static {v1}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FirmwareUpdateState;->access$400(Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FirmwareUpdateState;)Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/cardreader/CardReaderInfo;->readerRequiresBlockingFirmwareUpdateScreen(Lcom/squareup/protos/client/bills/CardData$ReaderType;)Z

    move-result v1

    if-eqz v1, :cond_0

    return v2

    .line 335
    :cond_1
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->expirationRunners:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    xor-int/2addr v0, v2

    return v0
.end method

.method public synthetic lambda$null$0$FirmwareUpdateDispatcher(Lcom/squareup/cardreader/CardReader;Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 172
    invoke-virtual {p0, p1, p2}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->handleServerCallSuccess(Lcom/squareup/cardreader/CardReader;Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;)V

    return-void
.end method

.method public synthetic lambda$null$1$FirmwareUpdateDispatcher(Lcom/squareup/cardreader/CardReader;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 174
    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object p1

    invoke-virtual {p2}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getReceived()Lcom/squareup/receiving/ReceivedResponse;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->handleServerCallError(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/receiving/ReceivedResponse;)V

    return-void
.end method

.method public synthetic lambda$sendFirmwareManifestToServer$2$FirmwareUpdateDispatcher(Lcom/squareup/cardreader/CardReader;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 170
    new-instance v0, Lcom/squareup/cardreader/dipper/-$$Lambda$FirmwareUpdateDispatcher$odzyK02PbMwsLSRMUlhDcoDPu8E;

    invoke-direct {v0, p0, p1}, Lcom/squareup/cardreader/dipper/-$$Lambda$FirmwareUpdateDispatcher$odzyK02PbMwsLSRMUlhDcoDPu8E;-><init>(Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;Lcom/squareup/cardreader/CardReader;)V

    new-instance v1, Lcom/squareup/cardreader/dipper/-$$Lambda$FirmwareUpdateDispatcher$sQxSrwSoyhgFnDnwVurqsgp1zmA;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/dipper/-$$Lambda$FirmwareUpdateDispatcher$sQxSrwSoyhgFnDnwVurqsgp1zmA;-><init>(Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;Lcom/squareup/cardreader/CardReader;)V

    invoke-virtual {p2, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public onCardReaderAdded(Lcom/squareup/cardreader/CardReader;)V
    .locals 0

    .line 98
    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getAddress()Ljava/lang/String;

    move-result-object p1

    .line 99
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->removeWaitingForReconnectTimer(Ljava/lang/String;)V

    return-void
.end method

.method public onCardReaderRemoved(Lcom/squareup/cardreader/CardReader;)V
    .locals 4

    .line 103
    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object p1

    .line 105
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->readerUpdates:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 112
    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->hasRecentRebootAssetCompleted(Lcom/squareup/cardreader/CardReaderInfo;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 113
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->readerUpdates:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FirmwareUpdateState;

    .line 114
    iget-object v1, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->firmwareUpdateLogger:Lcom/squareup/log/ReaderEventLogger;

    .line 115
    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->sessionIdForReader(Lcom/squareup/cardreader/CardReaderInfo;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/squareup/analytics/ReaderEventName;->FW_UPDATE_CANCELED:Lcom/squareup/analytics/ReaderEventName;

    .line 114
    invoke-virtual {v1, p1, v2, v3}, Lcom/squareup/log/ReaderEventLogger;->logFirmwareUpdateEvent(Lcom/squareup/cardreader/CardReaderInfo;Ljava/lang/String;Lcom/squareup/cardreader/ReaderEventLogger$CardReaderEvent;)V

    .line 117
    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->removeSessionIdForReader(Lcom/squareup/cardreader/CardReaderInfo;)V

    .line 118
    iget-object v1, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->listeners:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cardreader/dipper/FirmwareUpdateListener;

    .line 119
    invoke-static {v0}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FirmwareUpdateState;->access$000(Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FirmwareUpdateState;)Z

    move-result v3

    invoke-interface {v2, p1, v3}, Lcom/squareup/cardreader/dipper/FirmwareUpdateListener;->onFirmwareUpdateAborted(Lcom/squareup/cardreader/CardReaderInfo;Z)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public onFirmwareUpdateAssetSuccess(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/FirmwareUpdater$AssetDescriptor;)V
    .locals 4

    .line 231
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->READER:Lcom/squareup/log/OhSnapLogger$EventType;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    const-string v3, "FWUP Asset Complete: %s"

    invoke-static {v3, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 235
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->readerUpdates:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 236
    iget-object p1, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object p2, Lcom/squareup/log/OhSnapLogger$EventType;->READER:Lcom/squareup/log/OhSnapLogger$EventType;

    const-string v0, "Ignoring onFirmwareUpdateAssetSuccess() notification from missing reader"

    invoke-interface {p1, p2, v0}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    return-void

    .line 241
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->readerUpdates:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FirmwareUpdateState;

    .line 242
    invoke-static {v0}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FirmwareUpdateState;->access$308(Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FirmwareUpdateState;)I

    .line 246
    iget-object p2, p2, Lcom/squareup/cardreader/FirmwareUpdater$AssetDescriptor;->reboot:Lcom/squareup/protos/client/tarkin/Asset$Reboot;

    invoke-static {p1, p2}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->isRebootingAsset(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/protos/client/tarkin/Asset$Reboot;)Z

    move-result p2

    if-eqz p2, :cond_1

    .line 247
    iget-object p2, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->firmwareUpdateLogger:Lcom/squareup/log/ReaderEventLogger;

    .line 248
    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->sessionIdForReader(Lcom/squareup/cardreader/CardReaderInfo;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->FW_UPDATE_REBOOT:Lcom/squareup/analytics/ReaderEventName;

    .line 247
    invoke-virtual {p2, p1, v0, v1}, Lcom/squareup/log/ReaderEventLogger;->logFirmwareUpdateEvent(Lcom/squareup/cardreader/CardReaderInfo;Ljava/lang/String;Lcom/squareup/cardreader/ReaderEventLogger$CardReaderEvent;)V

    .line 250
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->addReaderRebootTimer(Lcom/squareup/cardreader/CardReaderInfo;)V

    :cond_1
    return-void
.end method

.method public onFirmwareUpdateComplete(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 4

    .line 255
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->READER:Lcom/squareup/log/OhSnapLogger$EventType;

    const-string v2, "FWUP Complete!"

    invoke-interface {v0, v1, v2}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 256
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->firmwareUpdateLogger:Lcom/squareup/log/ReaderEventLogger;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->sessionIdForReader(Lcom/squareup/cardreader/CardReaderInfo;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/squareup/analytics/ReaderEventName;->FW_UPDATE_SUCCESS:Lcom/squareup/analytics/ReaderEventName;

    invoke-virtual {v0, p1, v1, v2}, Lcom/squareup/log/ReaderEventLogger;->logFirmwareUpdateEvent(Lcom/squareup/cardreader/CardReaderInfo;Ljava/lang/String;Lcom/squareup/cardreader/ReaderEventLogger$CardReaderEvent;)V

    .line 258
    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->removeSessionIdForReader(Lcom/squareup/cardreader/CardReaderInfo;)V

    .line 262
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->readerUpdates:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 263
    iget-object p1, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v0, Lcom/squareup/log/OhSnapLogger$EventType;->READER:Lcom/squareup/log/OhSnapLogger$EventType;

    const-string v1, "Ignoring onFirmwareUpdateComplete() notification from missing reader"

    invoke-interface {p1, v0, v1}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    return-void

    .line 268
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->readerUpdates:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FirmwareUpdateState;

    .line 270
    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->hasRecentRebootAssetCompleted(Lcom/squareup/cardreader/CardReaderInfo;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {v0}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FirmwareUpdateState;->access$000(Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FirmwareUpdateState;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 271
    iget-object v1, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v2, Lcom/squareup/log/OhSnapLogger$EventType;->READER:Lcom/squareup/log/OhSnapLogger$EventType;

    const-string v3, "Rebooting FWUP marked non-blocking. This may be an error."

    invoke-interface {v1, v2, v3}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 274
    :cond_1
    iget-object v1, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->listeners:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cardreader/dipper/FirmwareUpdateListener;

    .line 275
    invoke-static {v0}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FirmwareUpdateState;->access$000(Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FirmwareUpdateState;)Z

    move-result v3

    invoke-interface {v2, p1, v3}, Lcom/squareup/cardreader/dipper/FirmwareUpdateListener;->onFirmwareUpdateComplete(Lcom/squareup/cardreader/CardReaderInfo;Z)V

    goto :goto_0

    .line 278
    :cond_2
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->readerUpdates:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getAddress()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public onFirmwareUpdateError(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;)V
    .locals 4

    .line 283
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->READER:Lcom/squareup/log/OhSnapLogger$EventType;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "FWUP Error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 284
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->firmwareUpdateLogger:Lcom/squareup/log/ReaderEventLogger;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->sessionIdForReader(Lcom/squareup/cardreader/CardReaderInfo;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/squareup/analytics/ReaderEventName;->FW_UPDATE_FAILURE:Lcom/squareup/analytics/ReaderEventName;

    invoke-virtual {v0, p1, v1, v2}, Lcom/squareup/log/ReaderEventLogger;->logFirmwareUpdateEvent(Lcom/squareup/cardreader/CardReaderInfo;Ljava/lang/String;Lcom/squareup/cardreader/ReaderEventLogger$CardReaderEvent;)V

    .line 286
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->firmwareUpdateLogger:Lcom/squareup/log/ReaderEventLogger;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->sessionIdForReader(Lcom/squareup/cardreader/CardReaderInfo;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1, p2}, Lcom/squareup/log/ReaderEventLogger;->logFirmwareUpdateError(Lcom/squareup/cardreader/CardReaderInfo;Ljava/lang/String;Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;)V

    .line 289
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->readerUpdates:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FirmwareUpdateState;

    .line 290
    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->removeSessionIdForReader(Lcom/squareup/cardreader/CardReaderInfo;)V

    if-eqz v0, :cond_0

    .line 293
    iget-object v1, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->listeners:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cardreader/dipper/FirmwareUpdateListener;

    .line 294
    invoke-static {v0}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FirmwareUpdateState;->access$000(Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FirmwareUpdateState;)Z

    move-result v3

    invoke-interface {v2, p1, v3, p2}, Lcom/squareup/cardreader/dipper/FirmwareUpdateListener;->onFirmwareUpdateError(Lcom/squareup/cardreader/CardReaderInfo;ZLcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onFirmwareUpdateProgress(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/FirmwareUpdater$AssetDescriptor;I)V
    .locals 9

    const/16 v0, 0x63

    if-eq p3, v0, :cond_0

    .line 205
    rem-int/lit8 v0, p3, 0xa

    if-nez v0, :cond_1

    .line 206
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->READER:Lcom/squareup/log/OhSnapLogger$EventType;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    const/4 v4, 0x1

    .line 207
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const-string v4, "FWUP Progress: %s @ %d%%"

    invoke-static {v2, v4, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 206
    invoke-interface {v0, v1, v2}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 212
    :cond_1
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->readerUpdates:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 213
    iget-object p1, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object p2, Lcom/squareup/log/OhSnapLogger$EventType;->READER:Lcom/squareup/log/OhSnapLogger$EventType;

    const-string p3, "Ignoring onFirmwareUpdateProgress() notification from missing reader"

    invoke-interface {p1, p2, p3}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    return-void

    .line 218
    :cond_2
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->readerUpdates:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FirmwareUpdateState;

    .line 219
    invoke-static {v0}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FirmwareUpdateState;->access$000(Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FirmwareUpdateState;)Z

    move-result v7

    .line 220
    invoke-static {v0, p3}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FirmwareUpdateState;->access$200(Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FirmwareUpdateState;I)I

    move-result v0

    .line 221
    iget-object p2, p2, Lcom/squareup/cardreader/FirmwareUpdater$AssetDescriptor;->reboot:Lcom/squareup/protos/client/tarkin/Asset$Reboot;

    invoke-static {p1, p2}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->isRebootingAsset(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/protos/client/tarkin/Asset$Reboot;)Z

    move-result p2

    .line 223
    iget-object v1, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->listeners:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/dipper/FirmwareUpdateListener;

    move-object v2, p1

    move v3, v7

    move v4, v0

    move v5, p3

    move v6, p2

    .line 224
    invoke-interface/range {v1 .. v6}, Lcom/squareup/cardreader/dipper/FirmwareUpdateListener;->onFirmwareUpdateProgress(Lcom/squareup/cardreader/CardReaderInfo;ZIIZ)V

    goto :goto_0

    :cond_3
    return-void
.end method

.method public onFirmwareUpdateStarted(Lcom/squareup/cardreader/CardReaderInfo;Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            "Ljava/util/List<",
            "Lcom/squareup/cardreader/FirmwareUpdater$AssetDescriptor;",
            ">;)V"
        }
    .end annotation

    .line 180
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 182
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->firmwareUpdateLogger:Lcom/squareup/log/ReaderEventLogger;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->sessionIdForReader(Lcom/squareup/cardreader/CardReaderInfo;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/squareup/analytics/ReaderEventName;->FW_UPDATE_RECEIVED:Lcom/squareup/analytics/ReaderEventName;

    invoke-virtual {v0, p1, v1, v2}, Lcom/squareup/log/ReaderEventLogger;->logFirmwareUpdateEvent(Lcom/squareup/cardreader/CardReaderInfo;Ljava/lang/String;Lcom/squareup/cardreader/ReaderEventLogger$CardReaderEvent;)V

    .line 184
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->firmwareUpdateLogger:Lcom/squareup/log/ReaderEventLogger;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->sessionIdForReader(Lcom/squareup/cardreader/CardReaderInfo;)Ljava/lang/String;

    move-result-object v1

    .line 185
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->isFirmwareUpdateBlocking()Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v2, Lcom/squareup/analytics/ReaderEventName;->FW_UPDATE_REQUIRED:Lcom/squareup/analytics/ReaderEventName;

    goto :goto_0

    :cond_1
    sget-object v2, Lcom/squareup/analytics/ReaderEventName;->FW_UPDATE_SUGGESTED:Lcom/squareup/analytics/ReaderEventName;

    .line 184
    :goto_0
    invoke-virtual {v0, p1, v1, v2}, Lcom/squareup/log/ReaderEventLogger;->logFirmwareUpdateEvent(Lcom/squareup/cardreader/CardReaderInfo;Ljava/lang/String;Lcom/squareup/cardreader/ReaderEventLogger$CardReaderEvent;)V

    .line 187
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->serviceStarter:Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationServiceStarter;

    invoke-interface {v0}, Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationServiceStarter;->startFirmwareUpdateNotificationService()V

    .line 189
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getAddress()Ljava/lang/String;

    move-result-object v0

    .line 190
    iget-object v1, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->readerUpdates:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 191
    iget-object p1, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v0, Lcom/squareup/log/OhSnapLogger$EventType;->READER:Lcom/squareup/log/OhSnapLogger$EventType;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Resuming FWUP: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, v0, p2}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    goto :goto_2

    .line 193
    :cond_2
    iget-object v1, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v2, Lcom/squareup/log/OhSnapLogger$EventType;->READER:Lcom/squareup/log/OhSnapLogger$EventType;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Starting FWUP: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 194
    new-instance v1, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FirmwareUpdateState;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, p2, v2, v3}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FirmwareUpdateState;-><init>(Ljava/util/List;Lcom/squareup/protos/client/bills/CardData$ReaderType;Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$1;)V

    .line 195
    iget-object p2, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->readerUpdates:Ljava/util/Map;

    invoke-interface {p2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 196
    iget-object p2, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->listeners:Ljava/util/Set;

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/dipper/FirmwareUpdateListener;

    .line 197
    invoke-static {v1}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FirmwareUpdateState;->access$000(Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$FirmwareUpdateState;)Z

    move-result v2

    invoke-interface {v0, p1, v2}, Lcom/squareup/cardreader/dipper/FirmwareUpdateListener;->onFirmwareUpdateStarted(Lcom/squareup/cardreader/CardReaderInfo;Z)V

    goto :goto_1

    :cond_3
    :goto_2
    return-void
.end method

.method public onPairingFailed(Lcom/squareup/cardreader/WirelessConnection;Lcom/squareup/dipper/events/BleErrorType;)V
    .locals 0

    return-void
.end method

.method public onPairingSuccess(Lcom/squareup/cardreader/WirelessConnection;)V
    .locals 1

    .line 425
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->expirationRunners:Ljava/util/Map;

    .line 426
    invoke-interface {p1}, Lcom/squareup/cardreader/WirelessConnection;->getAddress()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$ReconnectExpirationRunner;

    if-eqz p1, :cond_0

    .line 428
    invoke-virtual {p1}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$ReconnectExpirationRunner;->run()V

    :cond_0
    return-void
.end method

.method public onReaderForceUnPair(Lcom/squareup/cardreader/WirelessConnection;)V
    .locals 0

    return-void
.end method

.method removeFirmwareUpdateListener(Lcom/squareup/cardreader/dipper/FirmwareUpdateListener;)V
    .locals 1

    .line 305
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 306
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->listeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method removeSessionIdForReader(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 1

    .line 421
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->firmwareUpdateSessionIdsByReader:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getAddress()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public sendFirmwareManifestToServer(Lcom/squareup/cardreader/CardReader;[BLcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;)V
    .locals 4

    .line 126
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->firmwareUpdateLogger:Lcom/squareup/log/ReaderEventLogger;

    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v1

    .line 127
    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->sessionIdForReader(Lcom/squareup/cardreader/CardReaderInfo;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/squareup/analytics/ReaderEventName;->INIT_SEND_FW_MANIFEST_UP:Lcom/squareup/analytics/ReaderEventName;

    .line 126
    invoke-virtual {v0, v1, v2, v3}, Lcom/squareup/log/ReaderEventLogger;->logFirmwareUpdateEvent(Lcom/squareup/cardreader/CardReaderInfo;Ljava/lang/String;Lcom/squareup/cardreader/ReaderEventLogger$CardReaderEvent;)V

    .line 130
    sget-object v0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher$1;->$SwitchMap$com$squareup$protos$client$bills$CardData$ReaderType:[I

    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/cardreader/CardReaderInfo;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardData$ReaderType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_4

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 146
    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->getLcrReaderType()Lcom/squareup/cardreader/lcr/CrCardreaderType;

    move-result-object v0

    sget-object v1, Lcom/squareup/cardreader/lcr/CrCardreaderType;->CR_CARDREADER_READER_TYPE_T2B:Lcom/squareup/cardreader/lcr/CrCardreaderType;

    if-ne v0, v1, :cond_0

    .line 148
    sget-object v0, Lcom/squareup/protos/client/tarkin/ReaderType;->T2B:Lcom/squareup/protos/client/tarkin/ReaderType;

    goto :goto_0

    .line 150
    :cond_0
    sget-object v0, Lcom/squareup/protos/client/tarkin/ReaderType;->T2:Lcom/squareup/protos/client/tarkin/ReaderType;

    goto :goto_0

    .line 154
    :cond_1
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Firmware manifest from invalid readerType: "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object p1

    .line 156
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object p1

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2

    .line 143
    :cond_2
    sget-object v0, Lcom/squareup/protos/client/tarkin/ReaderType;->X2:Lcom/squareup/protos/client/tarkin/ReaderType;

    goto :goto_0

    .line 140
    :cond_3
    sget-object v0, Lcom/squareup/protos/client/tarkin/ReaderType;->R6:Lcom/squareup/protos/client/tarkin/ReaderType;

    goto :goto_0

    .line 132
    :cond_4
    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->getLcrReaderType()Lcom/squareup/cardreader/lcr/CrCardreaderType;

    move-result-object v0

    sget-object v1, Lcom/squareup/cardreader/lcr/CrCardreaderType;->CR_CARDREADER_READER_TYPE_R12C:Lcom/squareup/cardreader/lcr/CrCardreaderType;

    if-ne v0, v1, :cond_5

    .line 134
    sget-object v0, Lcom/squareup/protos/client/tarkin/ReaderType;->R12C:Lcom/squareup/protos/client/tarkin/ReaderType;

    goto :goto_0

    .line 136
    :cond_5
    sget-object v0, Lcom/squareup/protos/client/tarkin/ReaderType;->R12:Lcom/squareup/protos/client/tarkin/ReaderType;

    .line 159
    :goto_0
    new-instance v1, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$Builder;-><init>()V

    invoke-static {p2}, Lokio/ByteString;->of([B)Lokio/ByteString;

    move-result-object p2

    invoke-virtual {v1, p2}, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$Builder;->manifest(Lokio/ByteString;)Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$Builder;

    move-result-object p2

    .line 160
    invoke-virtual {p2, p3}, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$Builder;->libcardreader_comms_version(Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;)Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$Builder;

    move-result-object p2

    .line 161
    invoke-virtual {p2, v0}, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$Builder;->reader_type(Lcom/squareup/protos/client/tarkin/ReaderType;)Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$Builder;

    move-result-object p2

    .line 162
    invoke-virtual {p2}, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$Builder;->build()Lcom/squareup/protos/client/tarkin/AssetUpdateRequest;

    move-result-object p2

    .line 164
    iget-object p3, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->listeners:Ljava/util/Set;

    invoke-interface {p3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_1
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/dipper/FirmwareUpdateListener;

    .line 165
    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/dipper/FirmwareUpdateListener;->onSendFirmwareManifestToServer(Lcom/squareup/cardreader/CardReaderInfo;)V

    goto :goto_1

    .line 168
    :cond_6
    iget-object p3, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->firmwareUpdateService:Lcom/squareup/cardreader/dipper/FirmwareUpdateService;

    invoke-interface {p3, p2}, Lcom/squareup/cardreader/dipper/FirmwareUpdateService;->send(Lcom/squareup/protos/client/tarkin/AssetUpdateRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object p2

    .line 169
    invoke-virtual {p2}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p2

    new-instance p3, Lcom/squareup/cardreader/dipper/-$$Lambda$FirmwareUpdateDispatcher$3ZdedpE4_p4D6YTFj1mOwS8eSuA;

    invoke-direct {p3, p0, p1}, Lcom/squareup/cardreader/dipper/-$$Lambda$FirmwareUpdateDispatcher$3ZdedpE4_p4D6YTFj1mOwS8eSuA;-><init>(Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;Lcom/squareup/cardreader/CardReader;)V

    .line 170
    invoke-virtual {p2, p3}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method sessionIdForReader(Lcom/squareup/cardreader/CardReaderInfo;)Ljava/lang/String;
    .locals 2

    .line 408
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getAddress()Ljava/lang/String;

    move-result-object p1

    .line 409
    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->hasSessionIdForReader(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 410
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->firmwareUpdateSessionIdsByReader:Ljava/util/Map;

    iget-object v1, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->uuidGenerator:Lcom/squareup/log/UUIDGenerator;

    invoke-interface {v1}, Lcom/squareup/log/UUIDGenerator;->randomUUID()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 413
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->firmwareUpdateSessionIdsByReader:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    return-object p1
.end method
