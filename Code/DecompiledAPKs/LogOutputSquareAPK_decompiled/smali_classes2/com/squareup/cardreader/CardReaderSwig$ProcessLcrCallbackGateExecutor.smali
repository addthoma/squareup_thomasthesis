.class Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;
.super Ljava/lang/Object;
.source "CardReaderSwig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/CardReaderSwig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ProcessLcrCallbackGateExecutor"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/cardreader/CardReaderSwig;


# direct methods
.method private constructor <init>(Lcom/squareup/cardreader/CardReaderSwig;)V
    .locals 0

    .line 863
    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/cardreader/CardReaderSwig;Lcom/squareup/cardreader/CardReaderSwig$1;)V
    .locals 0

    .line 863
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;-><init>(Lcom/squareup/cardreader/CardReaderSwig;)V

    return-void
.end method


# virtual methods
.method execute(Ljava/lang/Runnable;Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;)V
    .locals 2

    .line 865
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderSwig;->access$1200(Lcom/squareup/cardreader/CardReaderSwig;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 869
    :cond_0
    sget-object v0, Lcom/squareup/cardreader/CardReaderSwig$1;->$SwitchMap$com$squareup$cardreader$CardReaderSwig$ProcessLcrCallbackThread:[I

    invoke-virtual {p2}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 877
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 880
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Invalid process lcr callback enum: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 874
    :cond_2
    iget-object p2, p0, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {p2}, Lcom/squareup/cardreader/CardReaderSwig;->access$1500(Lcom/squareup/cardreader/CardReaderSwig;)Lcom/squareup/thread/executor/MainThread;

    move-result-object p2

    invoke-interface {p2, p1}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 871
    :cond_3
    iget-object p2, p0, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackGateExecutor;->this$0:Lcom/squareup/cardreader/CardReaderSwig;

    invoke-static {p2, p1}, Lcom/squareup/cardreader/CardReaderSwig;->access$1400(Lcom/squareup/cardreader/CardReaderSwig;Ljava/lang/Runnable;)V

    :goto_0
    return-void
.end method
