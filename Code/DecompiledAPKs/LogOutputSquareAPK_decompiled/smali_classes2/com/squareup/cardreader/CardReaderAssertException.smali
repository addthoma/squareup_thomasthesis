.class public final Lcom/squareup/cardreader/CardReaderAssertException;
.super Ljava/lang/RuntimeException;
.source "CardReaderAssertException.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/CardReaderAssertException$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0004\u0018\u0000 \n2\u00060\u0001j\u0002`\u0002:\u0001\nB\u001f\u0008\u0016\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\t\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/cardreader/CardReaderAssertException;",
        "Ljava/lang/RuntimeException;",
        "Lkotlin/RuntimeException;",
        "message",
        "",
        "clazz",
        "lineNumber",
        "",
        "(Ljava/lang/String;Ljava/lang/String;I)V",
        "(Ljava/lang/String;)V",
        "Companion",
        "lcr-data-models_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/cardreader/CardReaderAssertException$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/cardreader/CardReaderAssertException$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/cardreader/CardReaderAssertException$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/cardreader/CardReaderAssertException;->Companion:Lcom/squareup/cardreader/CardReaderAssertException$Companion;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    const-string v0, "message"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    invoke-direct {p0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2

    const-string v0, "message"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clazz"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/CardReaderAssertException;-><init>(Ljava/lang/String;)V

    const/4 p1, 0x1

    new-array p1, p1, [Ljava/lang/StackTraceElement;

    .line 12
    new-instance v0, Ljava/lang/StackTraceElement;

    const-string v1, "NativeMethod"

    invoke-direct {v0, p2, v1, p2, p3}, Ljava/lang/StackTraceElement;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    const/4 p2, 0x0

    aput-object v0, p1, p2

    .line 20
    invoke-virtual {p0}, Lcom/squareup/cardreader/CardReaderAssertException;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object p2

    const-string p3, "stackTrace"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, p2}, Lkotlin/collections/ArraysKt;->plus([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Ljava/lang/StackTraceElement;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/CardReaderAssertException;->setStackTrace([Ljava/lang/StackTraceElement;)V

    return-void
.end method

.method public static final handleAssert(Ljava/lang/String;Ljava/lang/String;I)Lcom/squareup/cardreader/CardReaderAssertException;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/cardreader/CardReaderAssertException;->Companion:Lcom/squareup/cardreader/CardReaderAssertException$Companion;

    invoke-virtual {v0, p0, p1, p2}, Lcom/squareup/cardreader/CardReaderAssertException$Companion;->handleAssert(Ljava/lang/String;Ljava/lang/String;I)Lcom/squareup/cardreader/CardReaderAssertException;

    move-result-object p0

    return-object p0
.end method
