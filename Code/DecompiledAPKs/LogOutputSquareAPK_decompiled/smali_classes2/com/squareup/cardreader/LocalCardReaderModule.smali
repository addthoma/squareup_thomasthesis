.class public abstract Lcom/squareup/cardreader/LocalCardReaderModule;
.super Ljava/lang/Object;
.source "LocalCardReaderModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideLocalCardReader(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/CardReaderInitializer;Lcom/squareup/cardreader/FirmwareUpdater;Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/LocalCardReader;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 14
    new-instance v0, Lcom/squareup/cardreader/LocalCardReader;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/cardreader/LocalCardReader;-><init>(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/CardReaderInitializer;Lcom/squareup/cardreader/FirmwareUpdater;Lcom/squareup/cardreader/PaymentProcessor;)V

    return-object v0
.end method


# virtual methods
.method abstract provideCardReader(Lcom/squareup/cardreader/LocalCardReader;)Lcom/squareup/cardreader/CardReader;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
