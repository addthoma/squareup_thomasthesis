.class final Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$doFetchAppliedLocationCount$2;
.super Ljava/lang/Object;
.source "RealCatalogAppliedLocationCountFetcher.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;->doFetchAppliedLocationCount(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/ObservableSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0010\u0000\u001a&\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002 \u0003*\u0012\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/catalog/online/CatalogAppliedLocationCount$Data;",
        "kotlin.jvm.PlatformType",
        "objectToken",
        "",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;


# direct methods
.method constructor <init>(Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$doFetchAppliedLocationCount$2;->this$0:Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/String;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/catalog/online/CatalogAppliedLocationCount$Data;",
            ">;"
        }
    .end annotation

    const-string v0, "objectToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, ""

    .line 95
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 98
    new-instance p1, Lcom/squareup/catalog/online/CatalogAppliedLocationCount$Data;

    const/4 v0, 0x1

    invoke-direct {p1, v0, v0}, Lcom/squareup/catalog/online/CatalogAppliedLocationCount$Data;-><init>(ZI)V

    .line 97
    invoke-static {p1}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object p1

    goto :goto_0

    .line 104
    :cond_0
    iget-object v0, p0, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$doFetchAppliedLocationCount$2;->this$0:Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;

    invoke-static {v0, p1}, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;->access$retrieveCatalogObject(Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object p1

    .line 105
    new-instance v0, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$doFetchAppliedLocationCount$2$1;

    invoke-direct {v0, p0}, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$doFetchAppliedLocationCount$2$1;-><init>(Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$doFetchAppliedLocationCount$2;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 26
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$doFetchAppliedLocationCount$2;->apply(Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method
