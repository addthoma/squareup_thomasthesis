.class final Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$onEnterScope$1;
.super Ljava/lang/Object;
.source "RealCatalogAppliedLocationCountFetcher.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lkotlin/Pair<",
        "+",
        "Lcom/squareup/connectivity/InternetState;",
        "+",
        "Lcom/squareup/catalog/online/CatalogAppliedLocationCount;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012F\u0010\u0002\u001aB\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00060\u0006 \u0005* \u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00060\u0006\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "Lcom/squareup/connectivity/InternetState;",
        "kotlin.jvm.PlatformType",
        "Lcom/squareup/catalog/online/CatalogAppliedLocationCount;",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;


# direct methods
.method constructor <init>(Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$onEnterScope$1;->this$0:Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 26
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$onEnterScope$1;->accept(Lkotlin/Pair;)V

    return-void
.end method

.method public final accept(Lkotlin/Pair;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "+",
            "Lcom/squareup/connectivity/InternetState;",
            "+",
            "Lcom/squareup/catalog/online/CatalogAppliedLocationCount;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/connectivity/InternetState;

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/catalog/online/CatalogAppliedLocationCount;

    .line 55
    sget-object v1, Lcom/squareup/connectivity/InternetState;->CONNECTED:Lcom/squareup/connectivity/InternetState;

    if-ne v0, v1, :cond_1

    sget-object v0, Lcom/squareup/catalog/online/CatalogAppliedLocationCount$Error;->INSTANCE:Lcom/squareup/catalog/online/CatalogAppliedLocationCount$Error;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$onEnterScope$1;->this$0:Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;

    invoke-static {p1}, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;->access$getFetchedObjectCogsId$p(Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 57
    iget-object p1, p0, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$onEnterScope$1;->this$0:Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;

    invoke-static {p1}, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;->access$getFetchedObjectCogsId$p(Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-static {p1, v0}, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;->access$doFetchAppliedLocationCount(Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;Ljava/lang/String;)V

    :cond_1
    return-void
.end method
