.class public Lcom/squareup/cashmanagement/CashReportPayload$HeaderSection;
.super Ljava/lang/Object;
.source "CashReportPayload.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cashmanagement/CashReportPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "HeaderSection"
.end annotation


# instance fields
.field public final dateText:Ljava/lang/String;

.field public final headerText:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/squareup/cashmanagement/CashReportPayload$HeaderSection;->headerText:Ljava/lang/String;

    .line 30
    iput-object p2, p0, Lcom/squareup/cashmanagement/CashReportPayload$HeaderSection;->dateText:Ljava/lang/String;

    return-void
.end method
