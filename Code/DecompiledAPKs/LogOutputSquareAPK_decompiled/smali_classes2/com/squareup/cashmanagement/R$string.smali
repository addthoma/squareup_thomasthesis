.class public final Lcom/squareup/cashmanagement/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cashmanagement/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final cash_drawer_actual_in_drawer:I = 0x7f120360

.field public static final cash_drawer_cash_refunds:I = 0x7f120361

.field public static final cash_drawer_cash_sales:I = 0x7f120362

.field public static final cash_drawer_description:I = 0x7f120364

.field public static final cash_drawer_difference:I = 0x7f120365

.field public static final cash_drawer_end_of_drawer:I = 0x7f120367

.field public static final cash_drawer_expected_in_drawer:I = 0x7f120368

.field public static final cash_drawer_paid_in_out:I = 0x7f12036a

.field public static final cash_drawer_print_report:I = 0x7f12036b

.field public static final cash_drawer_report_paid_in:I = 0x7f12036c

.field public static final cash_drawer_report_paid_out:I = 0x7f12036d

.field public static final cash_drawer_report_title:I = 0x7f12036e

.field public static final cash_drawer_start_of_drawer:I = 0x7f12036f

.field public static final cash_drawer_starting_cash:I = 0x7f120370

.field public static final current_drawer_closed_remotely_message:I = 0x7f12078a

.field public static final current_drawer_closed_remotely_title:I = 0x7f12078b

.field public static final paid_in_out_total:I = 0x7f121308

.field public static final uppercase_drawer_report_summary:I = 0x7f121b25

.field public static final uppercase_paid_in_out:I = 0x7f121b5a


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
