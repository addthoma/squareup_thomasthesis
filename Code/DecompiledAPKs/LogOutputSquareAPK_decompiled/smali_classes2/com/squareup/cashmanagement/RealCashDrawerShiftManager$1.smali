.class Lcom/squareup/cashmanagement/RealCashDrawerShiftManager$1;
.super Lcom/squareup/mortar/BundlerAdapter;
.source "RealCashDrawerShiftManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->getBundler()Lmortar/bundler/Bundler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;


# direct methods
.method constructor <init>(Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;Ljava/lang/String;)V
    .locals 0

    .line 528
    iput-object p1, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager$1;->this$0:Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;

    invoke-direct {p0, p2}, Lcom/squareup/mortar/BundlerAdapter;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onLoad(Landroid/os/Bundle;)V
    .locals 2

    .line 531
    iget-object v0, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager$1;->this$0:Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;

    invoke-static {v0}, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->access$000(Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;)Z

    move-result v0

    if-nez v0, :cond_1

    if-nez p1, :cond_0

    goto :goto_0

    .line 534
    :cond_0
    iget-object v0, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager$1;->this$0:Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;

    const-string v1, "cash-drawer-shift-loaded"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->access$002(Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;Z)Z

    .line 535
    iget-object v0, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager$1;->this$0:Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;

    invoke-static {v0}, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->access$000(Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 536
    iget-object v0, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager$1;->this$0:Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;

    invoke-static {v0}, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->access$100(Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;)Lcom/squareup/BundleKey;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/BundleKey;->get(Landroid/os/Bundle;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    if-eqz p1, :cond_1

    .line 538
    iget-object v0, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager$1;->this$0:Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;

    invoke-virtual {p1}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->newBuilder()Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->access$202(Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    :cond_1
    :goto_0
    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 544
    iget-object v0, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager$1;->this$0:Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;

    invoke-static {v0}, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->access$000(Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 545
    iget-object v0, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager$1;->this$0:Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;

    invoke-static {v0}, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->access$100(Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;)Lcom/squareup/BundleKey;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager$1;->this$0:Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;

    invoke-static {v1}, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->access$200(Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager$1;->this$0:Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;

    invoke-static {v1}, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->access$200(Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->build()Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    move-result-object v1

    :goto_0
    invoke-virtual {v0, p1, v1}, Lcom/squareup/BundleKey;->put(Landroid/os/Bundle;Ljava/lang/Object;)V

    .line 547
    :cond_1
    iget-object v0, p0, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager$1;->this$0:Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;

    invoke-static {v0}, Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;->access$000(Lcom/squareup/cashmanagement/RealCashDrawerShiftManager;)Z

    move-result v0

    const-string v1, "cash-drawer-shift-loaded"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method
