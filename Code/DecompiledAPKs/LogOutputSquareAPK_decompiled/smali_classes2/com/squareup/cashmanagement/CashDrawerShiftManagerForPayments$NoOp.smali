.class public Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments$NoOp;
.super Ljava/lang/Object;
.source "CashDrawerShiftManagerForPayments.java"

# interfaces
.implements Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NoOp"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public addTenderWithId(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;Ljava/lang/String;)V
    .locals 0

    .line 49
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1
.end method

.method public cashManagementEnabledAndIsOpenShift()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getBundler()Lmortar/bundler/Bundler;
    .locals 2

    .line 53
    new-instance v0, Lcom/squareup/mortar/BundlerAdapter;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/mortar/BundlerAdapter;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public getCurrentCashDrawerShift()Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;
    .locals 1

    .line 57
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getCurrentCashDrawerShiftId()Ljava/lang/String;
    .locals 1

    .line 44
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
