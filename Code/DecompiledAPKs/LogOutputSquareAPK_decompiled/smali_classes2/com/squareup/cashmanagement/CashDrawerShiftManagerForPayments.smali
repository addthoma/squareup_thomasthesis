.class public interface abstract Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;
.super Ljava/lang/Object;
.source "CashDrawerShiftManagerForPayments.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments$NoOp;
    }
.end annotation


# virtual methods
.method public abstract addTenderWithId(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;Ljava/lang/String;)V
.end method

.method public abstract cashManagementEnabledAndIsOpenShift()Z
.end method

.method public abstract getBundler()Lmortar/bundler/Bundler;
.end method

.method public abstract getCurrentCashDrawerShift()Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;
.end method

.method public abstract getCurrentCashDrawerShiftId()Ljava/lang/String;
.end method
