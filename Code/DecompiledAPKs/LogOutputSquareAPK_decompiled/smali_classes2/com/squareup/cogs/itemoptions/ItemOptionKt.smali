.class public final Lcom/squareup/cogs/itemoptions/ItemOptionKt;
.super Ljava/lang/Object;
.source "ItemOption.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nItemOption.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ItemOption.kt\ncom/squareup/cogs/itemoptions/ItemOptionKt\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,46:1\n1360#2:47\n1429#2,3:48\n*E\n*S KotlinDebug\n*F\n+ 1 ItemOption.kt\ncom/squareup/cogs/itemoptions/ItemOptionKt\n*L\n44#1:47\n44#1,3:48\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "toItemOption",
        "Lcom/squareup/cogs/itemoptions/ItemOption;",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;",
        "cogs_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final toItemOption(Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;)Lcom/squareup/cogs/itemoptions/ItemOption;
    .locals 6

    const-string v0, "$this$toItemOption"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;->getId()Ljava/lang/String;

    move-result-object v0

    const-string v1, "this.id"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "this.name"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "this.displayName"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;->getAllValues()Ljava/util/List;

    move-result-object p0

    const-string v3, "this.allValues"

    invoke-static {p0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Iterable;

    .line 47
    new-instance v3, Ljava/util/ArrayList;

    const/16 v4, 0xa

    invoke-static {p0, v4}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v3, Ljava/util/Collection;

    .line 48
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 49
    check-cast v4, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;

    const-string v5, "it"

    .line 44
    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v4}, Lcom/squareup/cogs/itemoptions/ItemOptionValueKt;->toItemOptionValue(Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;)Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 50
    :cond_0
    check-cast v3, Ljava/util/List;

    .line 40
    new-instance p0, Lcom/squareup/cogs/itemoptions/ItemOption;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/squareup/cogs/itemoptions/ItemOption;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    return-object p0
.end method
