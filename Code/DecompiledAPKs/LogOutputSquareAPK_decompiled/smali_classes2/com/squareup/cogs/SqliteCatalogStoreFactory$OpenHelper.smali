.class final Lcom/squareup/cogs/SqliteCatalogStoreFactory$OpenHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "SqliteCatalogStoreFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cogs/SqliteCatalogStoreFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "OpenHelper"
.end annotation


# instance fields
.field private final syntheticTables:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/shared/catalog/synthetictables/SyntheticTable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/io/File;Ljava/util/Set;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/io/File;",
            "Ljava/util/Set<",
            "Lcom/squareup/shared/catalog/synthetictables/SyntheticTable;",
            ">;)V"
        }
    .end annotation

    .line 99
    invoke-virtual {p2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object p2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2, v0, v1}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 100
    iput-object p3, p0, Lcom/squareup/cogs/SqliteCatalogStoreFactory$OpenHelper;->syntheticTables:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    .line 104
    new-instance v0, Lcom/squareup/shared/catalog/android/RealSQLDatabase;

    invoke-direct {v0, p1}, Lcom/squareup/shared/catalog/android/RealSQLDatabase;-><init>(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 105
    invoke-static {}, Lcom/squareup/shared/catalog/ObjectsTable;->instance()Lcom/squareup/shared/catalog/ObjectsTable;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/squareup/shared/catalog/ObjectsTable;->create(Lcom/squareup/shared/sql/SQLDatabase;)V

    .line 106
    invoke-static {}, Lcom/squareup/shared/catalog/ReferencesTable;->instance()Lcom/squareup/shared/catalog/ReferencesTable;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/squareup/shared/catalog/ReferencesTable;->create(Lcom/squareup/shared/sql/SQLDatabase;)V

    .line 107
    invoke-static {}, Lcom/squareup/shared/catalog/PendingWriteRequestsTable;->instance()Lcom/squareup/shared/catalog/PendingWriteRequestsTable;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/squareup/shared/catalog/PendingWriteRequestsTable;->create(Lcom/squareup/shared/sql/SQLDatabase;)V

    .line 108
    invoke-static {}, Lcom/squareup/shared/catalog/MetadataTable;->instance()Lcom/squareup/shared/catalog/MetadataTable;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/squareup/shared/catalog/MetadataTable;->create(Lcom/squareup/shared/sql/SQLDatabase;)V

    .line 109
    invoke-static {}, Lcom/squareup/shared/catalog/SyntheticVersionsTable;->instance()Lcom/squareup/shared/catalog/SyntheticVersionsTable;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/cogs/SqliteCatalogStoreFactory$OpenHelper;->syntheticTables:Ljava/util/Set;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/shared/catalog/SyntheticVersionsTable;->create(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/Set;)V

    .line 110
    iget-object p1, p0, Lcom/squareup/cogs/SqliteCatalogStoreFactory$OpenHelper;->syntheticTables:Ljava/util/Set;

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/synthetictables/SyntheticTable;

    .line 111
    invoke-interface {v1, v0}, Lcom/squareup/shared/catalog/synthetictables/SyntheticTable;->create(Lcom/squareup/shared/sql/SQLDatabase;)V

    goto :goto_0

    .line 115
    :cond_0
    invoke-static {}, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable;->instance()Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/squareup/shared/catalog/connectv2/tables/ConnectV2ObjectsTable;->create(Lcom/squareup/shared/sql/SQLDatabase;)V

    .line 116
    invoke-static {}, Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable;->instance()Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/squareup/shared/catalog/connectv2/tables/SyncedObjectTypesTable;->create(Lcom/squareup/shared/sql/SQLDatabase;)V

    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0

    .line 125
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1
.end method
