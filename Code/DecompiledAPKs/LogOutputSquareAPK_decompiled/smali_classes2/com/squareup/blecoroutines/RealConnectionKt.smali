.class public final Lcom/squareup/blecoroutines/RealConnectionKt;
.super Ljava/lang/Object;
.source "RealConnection.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0012\n\u0000\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0002"
    }
    d2 = {
        "ENABLE_INDICATION_VALUE",
        "",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final ENABLE_INDICATION_VALUE:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x2

    new-array v0, v0, [B

    .line 31
    fill-array-data v0, :array_0

    sput-object v0, Lcom/squareup/blecoroutines/RealConnectionKt;->ENABLE_INDICATION_VALUE:[B

    return-void

    nop

    :array_0
    .array-data 1
        0x2t
        0x0t
    .end array-data
.end method

.method public static final synthetic access$getENABLE_INDICATION_VALUE$p()[B
    .locals 1

    .line 1
    sget-object v0, Lcom/squareup/blecoroutines/RealConnectionKt;->ENABLE_INDICATION_VALUE:[B

    return-object v0
.end method
