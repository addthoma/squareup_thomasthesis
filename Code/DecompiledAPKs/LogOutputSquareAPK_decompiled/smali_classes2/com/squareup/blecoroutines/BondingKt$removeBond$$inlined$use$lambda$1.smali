.class final Lcom/squareup/blecoroutines/BondingKt$removeBond$$inlined$use$lambda$1;
.super Lkotlin/coroutines/jvm/internal/SuspendLambda;
.source "Bonding.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/blecoroutines/BondingKt;->removeBond(Landroid/bluetooth/BluetoothDevice;JLandroid/content/Context;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/coroutines/jvm/internal/SuspendLambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Lkotlinx/coroutines/CoroutineScope;",
        "Lkotlin/coroutines/Continuation<",
        "-",
        "Lcom/squareup/blecoroutines/BondResult;",
        ">;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\u008a@\u00a2\u0006\u0004\u0008\u0003\u0010\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/blecoroutines/BondResult;",
        "Lkotlinx/coroutines/CoroutineScope;",
        "invoke",
        "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;",
        "com/squareup/blecoroutines/BondingKt$removeBond$2$result$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $continuation$inlined:Lkotlin/coroutines/Continuation;

.field final synthetic $deferredBondingResult$inlined:Lkotlinx/coroutines/CompletableDeferred;

.field final synthetic $receiver$inlined:Lcom/squareup/blecoroutines/BondingReceiver;

.field final synthetic $this_removeBond$inlined:Landroid/bluetooth/BluetoothDevice;

.field final synthetic $timeoutMs$inlined:J

.field L$0:Ljava/lang/Object;

.field label:I

.field private p$:Lkotlinx/coroutines/CoroutineScope;


# direct methods
.method constructor <init>(Lkotlin/coroutines/Continuation;Landroid/bluetooth/BluetoothDevice;Lcom/squareup/blecoroutines/BondingReceiver;Lkotlin/coroutines/Continuation;JLkotlinx/coroutines/CompletableDeferred;)V
    .locals 0

    iput-object p2, p0, Lcom/squareup/blecoroutines/BondingKt$removeBond$$inlined$use$lambda$1;->$this_removeBond$inlined:Landroid/bluetooth/BluetoothDevice;

    iput-object p3, p0, Lcom/squareup/blecoroutines/BondingKt$removeBond$$inlined$use$lambda$1;->$receiver$inlined:Lcom/squareup/blecoroutines/BondingReceiver;

    iput-object p4, p0, Lcom/squareup/blecoroutines/BondingKt$removeBond$$inlined$use$lambda$1;->$continuation$inlined:Lkotlin/coroutines/Continuation;

    iput-wide p5, p0, Lcom/squareup/blecoroutines/BondingKt$removeBond$$inlined$use$lambda$1;->$timeoutMs$inlined:J

    iput-object p7, p0, Lcom/squareup/blecoroutines/BondingKt$removeBond$$inlined$use$lambda$1;->$deferredBondingResult$inlined:Lkotlinx/coroutines/CompletableDeferred;

    const/4 p2, 0x2

    invoke-direct {p0, p2, p1}, Lkotlin/coroutines/jvm/internal/SuspendLambda;-><init>(ILkotlin/coroutines/Continuation;)V

    return-void
.end method


# virtual methods
.method public final create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lkotlin/coroutines/Continuation<",
            "*>;)",
            "Lkotlin/coroutines/Continuation<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "completion"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/blecoroutines/BondingKt$removeBond$$inlined$use$lambda$1;

    iget-object v3, p0, Lcom/squareup/blecoroutines/BondingKt$removeBond$$inlined$use$lambda$1;->$this_removeBond$inlined:Landroid/bluetooth/BluetoothDevice;

    iget-object v4, p0, Lcom/squareup/blecoroutines/BondingKt$removeBond$$inlined$use$lambda$1;->$receiver$inlined:Lcom/squareup/blecoroutines/BondingReceiver;

    iget-object v5, p0, Lcom/squareup/blecoroutines/BondingKt$removeBond$$inlined$use$lambda$1;->$continuation$inlined:Lkotlin/coroutines/Continuation;

    iget-wide v6, p0, Lcom/squareup/blecoroutines/BondingKt$removeBond$$inlined$use$lambda$1;->$timeoutMs$inlined:J

    iget-object v8, p0, Lcom/squareup/blecoroutines/BondingKt$removeBond$$inlined$use$lambda$1;->$deferredBondingResult$inlined:Lkotlinx/coroutines/CompletableDeferred;

    move-object v1, v0

    move-object v2, p2

    invoke-direct/range {v1 .. v8}, Lcom/squareup/blecoroutines/BondingKt$removeBond$$inlined$use$lambda$1;-><init>(Lkotlin/coroutines/Continuation;Landroid/bluetooth/BluetoothDevice;Lcom/squareup/blecoroutines/BondingReceiver;Lkotlin/coroutines/Continuation;JLkotlinx/coroutines/CompletableDeferred;)V

    check-cast p1, Lkotlinx/coroutines/CoroutineScope;

    iput-object p1, v0, Lcom/squareup/blecoroutines/BondingKt$removeBond$$inlined$use$lambda$1;->p$:Lkotlinx/coroutines/CoroutineScope;

    return-object v0
.end method

.method public final invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p2, Lkotlin/coroutines/Continuation;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/blecoroutines/BondingKt$removeBond$$inlined$use$lambda$1;->create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;

    move-result-object p1

    check-cast p1, Lcom/squareup/blecoroutines/BondingKt$removeBond$$inlined$use$lambda$1;

    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, p2}, Lcom/squareup/blecoroutines/BondingKt$removeBond$$inlined$use$lambda$1;->invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    move-result-object v0

    .line 58
    iget v1, p0, Lcom/squareup/blecoroutines/BondingKt$removeBond$$inlined$use$lambda$1;->label:I

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    if-ne v1, v2, :cond_0

    iget-object v0, p0, Lcom/squareup/blecoroutines/BondingKt$removeBond$$inlined$use$lambda$1;->L$0:Ljava/lang/Object;

    check-cast v0, Lkotlinx/coroutines/CoroutineScope;

    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    iget-object p1, p0, Lcom/squareup/blecoroutines/BondingKt$removeBond$$inlined$use$lambda$1;->p$:Lkotlinx/coroutines/CoroutineScope;

    iget-object v1, p0, Lcom/squareup/blecoroutines/BondingKt$removeBond$$inlined$use$lambda$1;->$deferredBondingResult$inlined:Lkotlinx/coroutines/CompletableDeferred;

    iput-object p1, p0, Lcom/squareup/blecoroutines/BondingKt$removeBond$$inlined$use$lambda$1;->L$0:Ljava/lang/Object;

    iput v2, p0, Lcom/squareup/blecoroutines/BondingKt$removeBond$$inlined$use$lambda$1;->label:I

    invoke-interface {v1, p0}, Lkotlinx/coroutines/CompletableDeferred;->await(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object p1

    if-ne p1, v0, :cond_2

    return-object v0

    :cond_2
    :goto_0
    return-object p1
.end method
