.class public final Lcom/squareup/blecoroutines/BleErrorKt;
.super Ljava/lang/Object;
.source "BleError.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0012\n\u0000\u001a\u0017\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u0001*\u00020\u0003\u00a2\u0006\u0002\u0010\u0004\u001a\u0017\u0010\u0005\u001a\n \u0002*\u0004\u0018\u00010\u00010\u0001*\u00020\u0003\u00a2\u0006\u0002\u0010\u0004\u001a\u0012\u0010\u0006\u001a\u00020\u0003*\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u0008\u00a8\u0006\t"
    }
    d2 = {
        "asUint16",
        "",
        "kotlin.jvm.PlatformType",
        "Landroid/bluetooth/BluetoothGattCharacteristic;",
        "(Landroid/bluetooth/BluetoothGattCharacteristic;)Ljava/lang/Integer;",
        "asUint8",
        "withValue",
        "value",
        "",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final asUint16(Landroid/bluetooth/BluetoothGattCharacteristic;)Ljava/lang/Integer;
    .locals 2

    const-string v0, "$this$asUint16"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v0, 0x12

    const/4 v1, 0x0

    .line 49
    invoke-virtual {p0, v0, v1}, Landroid/bluetooth/BluetoothGattCharacteristic;->getIntValue(II)Ljava/lang/Integer;

    move-result-object p0

    return-object p0
.end method

.method public static final asUint8(Landroid/bluetooth/BluetoothGattCharacteristic;)Ljava/lang/Integer;
    .locals 2

    const-string v0, "$this$asUint8"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v0, 0x11

    const/4 v1, 0x0

    .line 48
    invoke-virtual {p0, v0, v1}, Landroid/bluetooth/BluetoothGattCharacteristic;->getIntValue(II)Ljava/lang/Integer;

    move-result-object p0

    return-object p0
.end method

.method public static final withValue(Landroid/bluetooth/BluetoothGattCharacteristic;[B)Landroid/bluetooth/BluetoothGattCharacteristic;
    .locals 1

    const-string v0, "$this$withValue"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    invoke-virtual {p0, p1}, Landroid/bluetooth/BluetoothGattCharacteristic;->setValue([B)Z

    return-object p0
.end method
