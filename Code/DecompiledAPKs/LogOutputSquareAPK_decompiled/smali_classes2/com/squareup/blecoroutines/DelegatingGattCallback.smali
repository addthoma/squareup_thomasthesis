.class public final Lcom/squareup/blecoroutines/DelegatingGattCallback;
.super Landroid/bluetooth/BluetoothGattCallback;
.source "DelegatingGattCallback.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0007\u0008\u0000\u0018\u00002\u00020\u0001:\u0001\u001bB\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0018\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0016J \u0010\r\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016J \u0010\u0010\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016J \u0010\u0011\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0012\u001a\u00020\u000fH\u0016J \u0010\u0013\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u000e\u001a\u00020\u000fH\u0016J \u0010\u0016\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u0017\u001a\u00020\u000f2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016J \u0010\u0018\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u0019\u001a\u00020\u000f2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016J\u0018\u0010\u001a\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/blecoroutines/DelegatingGattCallback;",
        "Landroid/bluetooth/BluetoothGattCallback;",
        "broker",
        "Lcom/squareup/blecoroutines/GattCallbackBroker;",
        "tmnTimings",
        "Lcom/squareup/tmn/TmnTimings;",
        "(Lcom/squareup/blecoroutines/GattCallbackBroker;Lcom/squareup/tmn/TmnTimings;)V",
        "onCharacteristicChanged",
        "",
        "gatt",
        "Landroid/bluetooth/BluetoothGatt;",
        "characteristic",
        "Landroid/bluetooth/BluetoothGattCharacteristic;",
        "onCharacteristicRead",
        "status",
        "",
        "onCharacteristicWrite",
        "onConnectionStateChange",
        "newState",
        "onDescriptorWrite",
        "descriptor",
        "Landroid/bluetooth/BluetoothGattDescriptor;",
        "onMtuChanged",
        "mtu",
        "onReadRemoteRssi",
        "rssi",
        "onServicesDiscovered",
        "CallbackData",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final broker:Lcom/squareup/blecoroutines/GattCallbackBroker;

.field private final tmnTimings:Lcom/squareup/tmn/TmnTimings;


# direct methods
.method public constructor <init>(Lcom/squareup/blecoroutines/GattCallbackBroker;Lcom/squareup/tmn/TmnTimings;)V
    .locals 1

    const-string v0, "broker"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tmnTimings"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-direct {p0}, Landroid/bluetooth/BluetoothGattCallback;-><init>()V

    iput-object p1, p0, Lcom/squareup/blecoroutines/DelegatingGattCallback;->broker:Lcom/squareup/blecoroutines/GattCallbackBroker;

    iput-object p2, p0, Lcom/squareup/blecoroutines/DelegatingGattCallback;->tmnTimings:Lcom/squareup/tmn/TmnTimings;

    return-void
.end method


# virtual methods
.method public onCharacteristicChanged(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattCharacteristic;)V
    .locals 2

    const-string v0, "gatt"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "characteristic"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 118
    iget-object v0, p0, Lcom/squareup/blecoroutines/DelegatingGattCallback;->tmnTimings:Lcom/squareup/tmn/TmnTimings;

    sget-object v1, Lcom/squareup/tmn/What;->GATT_CALLBACK_ON_CHARACTERISTIC_CHANGED:Lcom/squareup/tmn/What;

    invoke-virtual {v0, v1}, Lcom/squareup/tmn/TmnTimings;->event(Lcom/squareup/tmn/What;)V

    .line 119
    iget-object v0, p0, Lcom/squareup/blecoroutines/DelegatingGattCallback;->broker:Lcom/squareup/blecoroutines/GattCallbackBroker;

    new-instance v1, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnCharacteristicChanged;

    invoke-static {p2}, Lcom/squareup/blecoroutines/DelegatingGattCallbackKt;->copyWithValue(Landroid/bluetooth/BluetoothGattCharacteristic;)Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object p2

    invoke-direct {v1, p1, p2}, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnCharacteristicChanged;-><init>(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattCharacteristic;)V

    check-cast v1, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData;

    invoke-virtual {v0, v1}, Lcom/squareup/blecoroutines/GattCallbackBroker;->handle(Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData;)V

    return-void
.end method

.method public onCharacteristicRead(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattCharacteristic;I)V
    .locals 2

    const-string v0, "gatt"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "characteristic"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    iget-object v0, p0, Lcom/squareup/blecoroutines/DelegatingGattCallback;->tmnTimings:Lcom/squareup/tmn/TmnTimings;

    sget-object v1, Lcom/squareup/tmn/What;->GATT_CALLBACK_ON_CHARACTERISTIC_READ:Lcom/squareup/tmn/What;

    invoke-virtual {v0, v1}, Lcom/squareup/tmn/TmnTimings;->event(Lcom/squareup/tmn/What;)V

    .line 103
    iget-object v0, p0, Lcom/squareup/blecoroutines/DelegatingGattCallback;->broker:Lcom/squareup/blecoroutines/GattCallbackBroker;

    new-instance v1, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnCharacteristicRead;

    invoke-static {p2}, Lcom/squareup/blecoroutines/DelegatingGattCallbackKt;->copyWithValue(Landroid/bluetooth/BluetoothGattCharacteristic;)Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object p2

    invoke-direct {v1, p1, p2, p3}, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnCharacteristicRead;-><init>(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattCharacteristic;I)V

    check-cast v1, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData;

    invoke-virtual {v0, v1}, Lcom/squareup/blecoroutines/GattCallbackBroker;->handle(Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData;)V

    return-void
.end method

.method public onCharacteristicWrite(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattCharacteristic;I)V
    .locals 2

    const-string v0, "gatt"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "characteristic"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 111
    iget-object v0, p0, Lcom/squareup/blecoroutines/DelegatingGattCallback;->broker:Lcom/squareup/blecoroutines/GattCallbackBroker;

    new-instance v1, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnCharacteristicWrite;

    invoke-static {p2}, Lcom/squareup/blecoroutines/DelegatingGattCallbackKt;->copyWithValue(Landroid/bluetooth/BluetoothGattCharacteristic;)Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object p2

    invoke-direct {v1, p1, p2, p3}, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnCharacteristicWrite;-><init>(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattCharacteristic;I)V

    check-cast v1, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData;

    invoke-virtual {v0, v1}, Lcom/squareup/blecoroutines/GattCallbackBroker;->handle(Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData;)V

    return-void
.end method

.method public onConnectionStateChange(Landroid/bluetooth/BluetoothGatt;II)V
    .locals 2

    const-string v0, "gatt"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 127
    iget-object v0, p0, Lcom/squareup/blecoroutines/DelegatingGattCallback;->broker:Lcom/squareup/blecoroutines/GattCallbackBroker;

    new-instance v1, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnConnectionStateChanged;

    invoke-direct {v1, p1, p2, p3}, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnConnectionStateChanged;-><init>(Landroid/bluetooth/BluetoothGatt;II)V

    check-cast v1, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData;

    invoke-virtual {v0, v1}, Lcom/squareup/blecoroutines/GattCallbackBroker;->handle(Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData;)V

    return-void
.end method

.method public onDescriptorWrite(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattDescriptor;I)V
    .locals 2

    const-string v0, "gatt"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "descriptor"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 135
    iget-object v0, p0, Lcom/squareup/blecoroutines/DelegatingGattCallback;->broker:Lcom/squareup/blecoroutines/GattCallbackBroker;

    new-instance v1, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnDescriptorWrite;

    invoke-direct {v1, p1, p2, p3}, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnDescriptorWrite;-><init>(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattDescriptor;I)V

    check-cast v1, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData;

    invoke-virtual {v0, v1}, Lcom/squareup/blecoroutines/GattCallbackBroker;->handle(Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData;)V

    return-void
.end method

.method public onMtuChanged(Landroid/bluetooth/BluetoothGatt;II)V
    .locals 2

    const-string v0, "gatt"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 151
    iget-object v0, p0, Lcom/squareup/blecoroutines/DelegatingGattCallback;->broker:Lcom/squareup/blecoroutines/GattCallbackBroker;

    new-instance v1, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnMtuChanged;

    invoke-direct {v1, p1, p2, p3}, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnMtuChanged;-><init>(Landroid/bluetooth/BluetoothGatt;II)V

    check-cast v1, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData;

    invoke-virtual {v0, v1}, Lcom/squareup/blecoroutines/GattCallbackBroker;->handle(Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData;)V

    return-void
.end method

.method public onReadRemoteRssi(Landroid/bluetooth/BluetoothGatt;II)V
    .locals 2

    const-string v0, "gatt"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 143
    iget-object v0, p0, Lcom/squareup/blecoroutines/DelegatingGattCallback;->broker:Lcom/squareup/blecoroutines/GattCallbackBroker;

    new-instance v1, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnReadRemoteRssi;

    invoke-direct {v1, p1, p2, p3}, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnReadRemoteRssi;-><init>(Landroid/bluetooth/BluetoothGatt;II)V

    check-cast v1, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData;

    invoke-virtual {v0, v1}, Lcom/squareup/blecoroutines/GattCallbackBroker;->handle(Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData;)V

    return-void
.end method

.method public onServicesDiscovered(Landroid/bluetooth/BluetoothGatt;I)V
    .locals 2

    const-string v0, "gatt"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    iget-object v0, p0, Lcom/squareup/blecoroutines/DelegatingGattCallback;->broker:Lcom/squareup/blecoroutines/GattCallbackBroker;

    new-instance v1, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnServicesDiscovered;

    invoke-direct {v1, p1, p2}, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnServicesDiscovered;-><init>(Landroid/bluetooth/BluetoothGatt;I)V

    check-cast v1, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData;

    invoke-virtual {v0, v1}, Lcom/squareup/blecoroutines/GattCallbackBroker;->handle(Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData;)V

    return-void
.end method
