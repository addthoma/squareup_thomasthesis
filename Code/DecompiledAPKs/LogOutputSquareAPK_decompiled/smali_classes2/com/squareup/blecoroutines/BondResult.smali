.class public final Lcom/squareup/blecoroutines/BondResult;
.super Ljava/lang/Object;
.source "Bonding.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u000c\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000c\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\r\u001a\u00020\u0003H\u00c6\u0003J\'\u0010\u000e\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0012\u001a\u00020\u0003H\u00d6\u0001J\t\u0010\u0013\u001a\u00020\u0014H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\u0008R\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u0008\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/blecoroutines/BondResult;",
        "",
        "previousState",
        "",
        "newState",
        "reason",
        "(III)V",
        "getNewState",
        "()I",
        "getPreviousState",
        "getReason",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "toString",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final newState:I

.field private final previousState:I

.field private final reason:I


# direct methods
.method public constructor <init>(III)V
    .locals 0

    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/squareup/blecoroutines/BondResult;->previousState:I

    iput p2, p0, Lcom/squareup/blecoroutines/BondResult;->newState:I

    iput p3, p0, Lcom/squareup/blecoroutines/BondResult;->reason:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/blecoroutines/BondResult;IIIILjava/lang/Object;)Lcom/squareup/blecoroutines/BondResult;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget p1, p0, Lcom/squareup/blecoroutines/BondResult;->previousState:I

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget p2, p0, Lcom/squareup/blecoroutines/BondResult;->newState:I

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget p3, p0, Lcom/squareup/blecoroutines/BondResult;->reason:I

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/blecoroutines/BondResult;->copy(III)Lcom/squareup/blecoroutines/BondResult;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/squareup/blecoroutines/BondResult;->previousState:I

    return v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/squareup/blecoroutines/BondResult;->newState:I

    return v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/squareup/blecoroutines/BondResult;->reason:I

    return v0
.end method

.method public final copy(III)Lcom/squareup/blecoroutines/BondResult;
    .locals 1

    new-instance v0, Lcom/squareup/blecoroutines/BondResult;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/blecoroutines/BondResult;-><init>(III)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/blecoroutines/BondResult;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/blecoroutines/BondResult;

    iget v0, p0, Lcom/squareup/blecoroutines/BondResult;->previousState:I

    iget v1, p1, Lcom/squareup/blecoroutines/BondResult;->previousState:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/blecoroutines/BondResult;->newState:I

    iget v1, p1, Lcom/squareup/blecoroutines/BondResult;->newState:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/blecoroutines/BondResult;->reason:I

    iget p1, p1, Lcom/squareup/blecoroutines/BondResult;->reason:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getNewState()I
    .locals 1

    .line 115
    iget v0, p0, Lcom/squareup/blecoroutines/BondResult;->newState:I

    return v0
.end method

.method public final getPreviousState()I
    .locals 1

    .line 114
    iget v0, p0, Lcom/squareup/blecoroutines/BondResult;->previousState:I

    return v0
.end method

.method public final getReason()I
    .locals 1

    .line 116
    iget v0, p0, Lcom/squareup/blecoroutines/BondResult;->reason:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lcom/squareup/blecoroutines/BondResult;->previousState:I

    invoke-static {v0}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/blecoroutines/BondResult;->newState:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/blecoroutines/BondResult;->reason:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BondResult(previousState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/blecoroutines/BondResult;->previousState:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", newState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/blecoroutines/BondResult;->newState:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", reason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/blecoroutines/BondResult;->reason:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
