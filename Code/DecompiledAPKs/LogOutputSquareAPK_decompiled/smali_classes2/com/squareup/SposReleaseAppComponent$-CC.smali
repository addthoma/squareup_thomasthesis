.class public final synthetic Lcom/squareup/SposReleaseAppComponent$-CC;
.super Ljava/lang/Object;
.source "SposReleaseAppComponent.java"


# direct methods
.method public static synthetic $default$loggedInComponent(Lcom/squareup/SposReleaseAppComponent;)Lcom/squareup/CommonLoggedInComponent;
    .locals 1
    .param p0, "_this"    # Lcom/squareup/SposReleaseAppComponent;

    .line 10
    invoke-interface {p0}, Lcom/squareup/SposReleaseAppComponent;->loggedInComponent()Lcom/squareup/SposReleaseLoggedInComponent;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic $default$loggedInComponent(Lcom/squareup/SposReleaseAppComponent;)Lcom/squareup/RegisterAppDelegateLoggedInComponent;
    .locals 1
    .param p0, "_this"    # Lcom/squareup/SposReleaseAppComponent;

    .line 10
    invoke-interface {p0}, Lcom/squareup/SposReleaseAppComponent;->loggedInComponent()Lcom/squareup/SposReleaseLoggedInComponent;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic $default$loggedOutActivityComponent(Lcom/squareup/SposReleaseAppComponent;)Lcom/squareup/loggedout/CommonLoggedOutActivityComponent;
    .locals 1
    .param p0, "_this"    # Lcom/squareup/SposReleaseAppComponent;

    .line 10
    invoke-interface {p0}, Lcom/squareup/SposReleaseAppComponent;->loggedOutActivityComponent()Lcom/squareup/ui/root/SposReleaseLoggedOutActivityComponent;

    move-result-object v0

    return-object v0
.end method
