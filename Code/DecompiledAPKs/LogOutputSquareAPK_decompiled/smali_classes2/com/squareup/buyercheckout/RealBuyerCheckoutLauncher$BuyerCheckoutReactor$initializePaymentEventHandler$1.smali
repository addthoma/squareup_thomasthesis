.class final Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$initializePaymentEventHandler$1;
.super Ljava/lang/Object;
.source "RealBuyerCheckoutLauncher.kt"

# interfaces
.implements Lio/reactivex/SingleOnSubscribe;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->initializePaymentEventHandler(Z)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/SingleOnSubscribe<",
        "TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0014\u0010\u0002\u001a\u0010\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "single",
        "Lio/reactivex/SingleEmitter;",
        "Lcom/squareup/buyercheckout/PaymentInputHandlerResult;",
        "kotlin.jvm.PlatformType",
        "subscribe"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;


# direct methods
.method constructor <init>(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$initializePaymentEventHandler$1;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final subscribe(Lio/reactivex/SingleEmitter;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/SingleEmitter<",
            "Lcom/squareup/buyercheckout/PaymentInputHandlerResult;",
            ">;)V"
        }
    .end annotation

    const-string v0, "single"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 538
    iget-object v0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$initializePaymentEventHandler$1;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;

    invoke-virtual {v0}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->getPaymentInputHandler()Lcom/squareup/ui/main/errors/PaymentInputHandler;

    move-result-object v0

    .line 539
    new-instance v1, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$initializePaymentEventHandler$1$1;

    invoke-direct {v1, p1}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$initializePaymentEventHandler$1$1;-><init>(Lio/reactivex/SingleEmitter;)V

    check-cast v1, Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;

    .line 538
    invoke-virtual {v0, v1}, Lcom/squareup/ui/main/errors/PaymentInputHandler;->initializeWithNfcFieldOn(Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;)V

    return-void
.end method
