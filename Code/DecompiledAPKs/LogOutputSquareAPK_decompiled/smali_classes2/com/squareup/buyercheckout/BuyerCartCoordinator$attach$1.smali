.class final Lcom/squareup/buyercheckout/BuyerCartCoordinator$attach$1;
.super Ljava/lang/Object;
.source "BuyerCartCoordinator.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/buyercheckout/BuyerCartCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lkotlin/Pair<",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "Lcom/squareup/buyercheckout/BuyerCart$ScreenData;",
        "Lcom/squareup/buyercheckout/BuyerCheckoutEvent;",
        ">;+",
        "Lcom/squareup/locale/LocaleOverrideFactory;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012F\u0010\u0002\u001aB\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u0007\u0012\u0004\u0012\u00020\u0008 \t* \u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u0007\u0012\u0004\u0012\u00020\u0008\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\n"
    }
    d2 = {
        "<anonymous>",
        "",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/buyercheckout/BuyerCart$ScreenData;",
        "Lcom/squareup/buyercheckout/BuyerCheckoutEvent;",
        "Lcom/squareup/buyercheckout/BuyerCartScreen;",
        "Lcom/squareup/locale/LocaleOverrideFactory;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $view:Landroid/view/View;

.field final synthetic this$0:Lcom/squareup/buyercheckout/BuyerCartCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/buyercheckout/BuyerCartCoordinator;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/buyercheckout/BuyerCartCoordinator$attach$1;->this$0:Lcom/squareup/buyercheckout/BuyerCartCoordinator;

    iput-object p2, p0, Lcom/squareup/buyercheckout/BuyerCartCoordinator$attach$1;->$view:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 31
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/squareup/buyercheckout/BuyerCartCoordinator$attach$1;->accept(Lkotlin/Pair;)V

    return-void
.end method

.method public final accept(Lkotlin/Pair;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/buyercheckout/BuyerCart$ScreenData;",
            "Lcom/squareup/buyercheckout/BuyerCheckoutEvent;",
            ">;+",
            "Lcom/squareup/locale/LocaleOverrideFactory;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/legacy/Screen;

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/locale/LocaleOverrideFactory;

    .line 73
    iget-object v1, p0, Lcom/squareup/buyercheckout/BuyerCartCoordinator$attach$1;->this$0:Lcom/squareup/buyercheckout/BuyerCartCoordinator;

    iget-object v2, p0, Lcom/squareup/buyercheckout/BuyerCartCoordinator$attach$1;->$view:Landroid/view/View;

    invoke-static {v1, v2, v0, p1}, Lcom/squareup/buyercheckout/BuyerCartCoordinator;->access$update(Lcom/squareup/buyercheckout/BuyerCartCoordinator;Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/locale/LocaleOverrideFactory;)V

    return-void
.end method
