.class public final Lcom/squareup/buyercheckout/BuyerCartCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "BuyerCartCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/buyercheckout/BuyerCartCoordinator$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBuyerCartCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BuyerCartCoordinator.kt\ncom/squareup/buyercheckout/BuyerCartCoordinator\n+ 2 Views.kt\ncom/squareup/util/Views\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,142:1\n1103#2,7:143\n1103#2,7:150\n1103#2,7:157\n1429#3,3:164\n*E\n*S KotlinDebug\n*F\n+ 1 BuyerCartCoordinator.kt\ncom/squareup/buyercheckout/BuyerCartCoordinator\n*L\n83#1,7:143\n93#1,7:150\n97#1,7:157\n114#1,3:164\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0088\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001:\u0001.BE\u0008\u0002\u0012\u001c\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\u0002\u0010\u0010J\u0010\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001bH\u0016J\u0010\u0010\u001f\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001bH\u0002J\u0010\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020#H\u0002J$\u0010$\u001a\u0008\u0012\u0004\u0012\u00020&0%2\u000c\u0010\'\u001a\u0008\u0012\u0004\u0012\u00020(0%2\u0006\u0010)\u001a\u00020!H\u0002J0\u0010*\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001b2\u0016\u0010+\u001a\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00072\u0006\u0010,\u001a\u00020-H\u0002R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R$\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006/"
    }
    d2 = {
        "Lcom/squareup/buyercheckout/BuyerCartCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/buyercheckout/BuyerCart$ScreenData;",
        "Lcom/squareup/buyercheckout/BuyerCheckoutEvent;",
        "Lcom/squareup/buyercheckout/BuyerCartScreen;",
        "formattedTotalProvider",
        "Lcom/squareup/ui/buyer/FormattedTotalProvider;",
        "buyerCartFormatter",
        "Lcom/squareup/ui/cart/BuyerCartFormatter;",
        "res",
        "Lcom/squareup/util/Res;",
        "buyerLocaleOverride",
        "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
        "(Lio/reactivex/Observable;Lcom/squareup/ui/buyer/FormattedTotalProvider;Lcom/squareup/ui/cart/BuyerCartFormatter;Lcom/squareup/util/Res;Lcom/squareup/buyer/language/BuyerLocaleOverride;)V",
        "buyerLanguageButton",
        "Landroid/widget/Button;",
        "cart",
        "Landroidx/recyclerview/widget/RecyclerView;",
        "cartAdapter",
        "Lcom/squareup/ui/buyercart/BuyerCartAdapter;",
        "confirmAndPayButton",
        "layoutManager",
        "Landroidx/recyclerview/widget/LinearLayoutManager;",
        "upGlyph",
        "Landroid/view/View;",
        "attach",
        "",
        "view",
        "bindViews",
        "formatQuantity",
        "",
        "quantity",
        "",
        "fromDisplayItems",
        "",
        "Lcom/squareup/ui/buyercart/BuyerCartAdapterItem;",
        "displayItems",
        "Lcom/squareup/comms/protos/seller/DisplayItem;",
        "total",
        "update",
        "screen",
        "localeOverride",
        "Lcom/squareup/locale/LocaleOverrideFactory;",
        "Factory",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final buyerCartFormatter:Lcom/squareup/ui/cart/BuyerCartFormatter;

.field private buyerLanguageButton:Landroid/widget/Button;

.field private final buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

.field private cart:Landroidx/recyclerview/widget/RecyclerView;

.field private cartAdapter:Lcom/squareup/ui/buyercart/BuyerCartAdapter;

.field private confirmAndPayButton:Landroid/widget/Button;

.field private final formattedTotalProvider:Lcom/squareup/ui/buyer/FormattedTotalProvider;

.field private layoutManager:Landroidx/recyclerview/widget/LinearLayoutManager;

.field private final res:Lcom/squareup/util/Res;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/buyercheckout/BuyerCart$ScreenData;",
            "Lcom/squareup/buyercheckout/BuyerCheckoutEvent;",
            ">;>;"
        }
    .end annotation
.end field

.field private upGlyph:Landroid/view/View;


# direct methods
.method private constructor <init>(Lio/reactivex/Observable;Lcom/squareup/ui/buyer/FormattedTotalProvider;Lcom/squareup/ui/cart/BuyerCartFormatter;Lcom/squareup/util/Res;Lcom/squareup/buyer/language/BuyerLocaleOverride;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/buyercheckout/BuyerCart$ScreenData;",
            "Lcom/squareup/buyercheckout/BuyerCheckoutEvent;",
            ">;>;",
            "Lcom/squareup/ui/buyer/FormattedTotalProvider;",
            "Lcom/squareup/ui/cart/BuyerCartFormatter;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            ")V"
        }
    .end annotation

    .line 38
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/buyercheckout/BuyerCartCoordinator;->screens:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/buyercheckout/BuyerCartCoordinator;->formattedTotalProvider:Lcom/squareup/ui/buyer/FormattedTotalProvider;

    iput-object p3, p0, Lcom/squareup/buyercheckout/BuyerCartCoordinator;->buyerCartFormatter:Lcom/squareup/ui/cart/BuyerCartFormatter;

    iput-object p4, p0, Lcom/squareup/buyercheckout/BuyerCartCoordinator;->res:Lcom/squareup/util/Res;

    iput-object p5, p0, Lcom/squareup/buyercheckout/BuyerCartCoordinator;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    return-void
.end method

.method public synthetic constructor <init>(Lio/reactivex/Observable;Lcom/squareup/ui/buyer/FormattedTotalProvider;Lcom/squareup/ui/cart/BuyerCartFormatter;Lcom/squareup/util/Res;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 31
    invoke-direct/range {p0 .. p5}, Lcom/squareup/buyercheckout/BuyerCartCoordinator;-><init>(Lio/reactivex/Observable;Lcom/squareup/ui/buyer/FormattedTotalProvider;Lcom/squareup/ui/cart/BuyerCartFormatter;Lcom/squareup/util/Res;Lcom/squareup/buyer/language/BuyerLocaleOverride;)V

    return-void
.end method

.method public static final synthetic access$update(Lcom/squareup/buyercheckout/BuyerCartCoordinator;Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 0

    .line 31
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/buyercheckout/BuyerCartCoordinator;->update(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/locale/LocaleOverrideFactory;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 136
    sget v0, Lcom/squareup/tenderworkflow/R$id;->buyer_up_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/buyercheckout/BuyerCartCoordinator;->upGlyph:Landroid/view/View;

    .line 137
    sget v0, Lcom/squareup/tenderworkflow/R$id;->buyer_language_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/squareup/buyercheckout/BuyerCartCoordinator;->buyerLanguageButton:Landroid/widget/Button;

    .line 138
    sget v0, Lcom/squareup/tenderworkflow/R$id;->cart_recycler:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v0, p0, Lcom/squareup/buyercheckout/BuyerCartCoordinator;->cart:Landroidx/recyclerview/widget/RecyclerView;

    .line 139
    sget v0, Lcom/squareup/tenderworkflow/R$id;->confirm_and_pay_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lcom/squareup/buyercheckout/BuyerCartCoordinator;->confirmAndPayButton:Landroid/widget/Button;

    return-void
.end method

.method private final formatQuantity(I)Ljava/lang/String;
    .locals 3

    const/4 v0, 0x1

    if-gt p1, v0, :cond_0

    const-string p1, ""

    goto :goto_0

    .line 128
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/buyercheckout/BuyerCartCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/ui/buyercart/R$string;->buyer_cart_quantity:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    const-string v2, "quantity"

    .line 129
    invoke-virtual {v1, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 130
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 131
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private final fromDisplayItems(Ljava/util/List;Ljava/lang/String;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/comms/protos/seller/DisplayItem;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/ui/buyercart/BuyerCartAdapterItem;",
            ">;"
        }
    .end annotation

    .line 111
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    add-int/2addr v1, v2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 113
    new-instance v1, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BuyerCartHeader;

    invoke-direct {v1, p2}, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BuyerCartHeader;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 114
    check-cast p1, Ljava/lang/Iterable;

    .line 164
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    .line 165
    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    check-cast p2, Lcom/squareup/comms/protos/seller/DisplayItem;

    .line 115
    iget-object v3, p2, Lcom/squareup/comms/protos/seller/DisplayItem;->is_section_header_row:Ljava/lang/Boolean;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 116
    new-instance v3, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranSectionHeader;

    invoke-direct {v3, p2}, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranSectionHeader;-><init>(Lcom/squareup/comms/protos/seller/DisplayItem;)V

    check-cast v3, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem;

    goto :goto_1

    .line 118
    :cond_0
    new-instance v3, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem$BuyerCartItem;

    iget-object v4, p2, Lcom/squareup/comms/protos/seller/DisplayItem;->quantity:Ljava/lang/Integer;

    if-nez v4, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-direct {p0, v4}, Lcom/squareup/buyercheckout/BuyerCartCoordinator;->formatQuantity(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, p2, v4}, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem$BuyerCartItem;-><init>(Lcom/squareup/comms/protos/seller/DisplayItem;Ljava/lang/String;)V

    check-cast v3, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem;

    .line 115
    :goto_1
    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 166
    :cond_2
    move-object p1, v0

    check-cast p1, Ljava/util/Collection;

    .line 121
    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method private final update(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/buyercheckout/BuyerCart$ScreenData;",
            "Lcom/squareup/buyercheckout/BuyerCheckoutEvent;",
            ">;",
            "Lcom/squareup/locale/LocaleOverrideFactory;",
            ")V"
        }
    .end annotation

    .line 83
    iget-object v0, p0, Lcom/squareup/buyercheckout/BuyerCartCoordinator;->upGlyph:Landroid/view/View;

    if-nez v0, :cond_0

    const-string v1, "upGlyph"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 143
    :cond_0
    new-instance v1, Lcom/squareup/buyercheckout/BuyerCartCoordinator$update$$inlined$onClickDebounced$1;

    invoke-direct {v1, p2}, Lcom/squareup/buyercheckout/BuyerCartCoordinator$update$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 85
    iget-object v0, p0, Lcom/squareup/buyercheckout/BuyerCartCoordinator;->buyerLanguageButton:Landroid/widget/Button;

    const-string v1, "buyerLanguageButton"

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v0, Landroid/view/View;

    iget-object v2, p2, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v2, Lcom/squareup/buyercheckout/BuyerCart$ScreenData;

    invoke-virtual {v2}, Lcom/squareup/buyercheckout/BuyerCart$ScreenData;->getEnableLanguageSelection()Z

    move-result v2

    invoke-static {v0, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 86
    iget-object v0, p2, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/buyercheckout/BuyerCart$ScreenData;

    invoke-virtual {v0}, Lcom/squareup/buyercheckout/BuyerCart$ScreenData;->getEnableLanguageSelection()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 87
    iget-object v0, p0, Lcom/squareup/buyercheckout/BuyerCartCoordinator;->buyerLanguageButton:Landroid/widget/Button;

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 88
    :cond_2
    invoke-virtual {p3}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object v2

    sget v3, Lcom/squareup/activity/R$drawable;->buyer_language_icon:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    const/4 v3, 0x0

    .line 87
    invoke-virtual {v0, v2, v3, v3, v3}, Landroid/widget/Button;->setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 91
    iget-object v0, p0, Lcom/squareup/buyercheckout/BuyerCartCoordinator;->buyerLanguageButton:Landroid/widget/Button;

    if-nez v0, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    new-instance v2, Lcom/squareup/locale/LocaleFormatter;

    invoke-virtual {p3}, Lcom/squareup/locale/LocaleOverrideFactory;->getLocale()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/squareup/locale/LocaleFormatter;-><init>(Ljava/util/Locale;)V

    invoke-virtual {v2}, Lcom/squareup/locale/LocaleFormatter;->displayLanguage()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 93
    :cond_4
    iget-object v0, p0, Lcom/squareup/buyercheckout/BuyerCartCoordinator;->buyerLanguageButton:Landroid/widget/Button;

    if-nez v0, :cond_5

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    check-cast v0, Landroid/view/View;

    .line 150
    new-instance v1, Lcom/squareup/buyercheckout/BuyerCartCoordinator$update$$inlined$onClickDebounced$2;

    invoke-direct {v1, p2}, Lcom/squareup/buyercheckout/BuyerCartCoordinator$update$$inlined$onClickDebounced$2;-><init>(Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 95
    new-instance v0, Lcom/squareup/buyercheckout/BuyerCartCoordinator$update$3;

    invoke-direct {v0, p2}, Lcom/squareup/buyercheckout/BuyerCartCoordinator$update$3;-><init>(Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 96
    iget-object p1, p0, Lcom/squareup/buyercheckout/BuyerCartCoordinator;->confirmAndPayButton:Landroid/widget/Button;

    const-string v0, "confirmAndPayButton"

    if-nez p1, :cond_6

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    invoke-virtual {p3}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object p3

    sget v1, Lcom/squareup/tenderworkflow/R$string;->buyer_cart_confirm_and_pay:I

    invoke-interface {p3, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p3

    check-cast p3, Ljava/lang/CharSequence;

    invoke-virtual {p1, p3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 97
    iget-object p1, p0, Lcom/squareup/buyercheckout/BuyerCartCoordinator;->confirmAndPayButton:Landroid/widget/Button;

    if-nez p1, :cond_7

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    check-cast p1, Landroid/view/View;

    .line 157
    new-instance p3, Lcom/squareup/buyercheckout/BuyerCartCoordinator$update$$inlined$onClickDebounced$3;

    invoke-direct {p3, p2}, Lcom/squareup/buyercheckout/BuyerCartCoordinator$update$$inlined$onClickDebounced$3;-><init>(Lcom/squareup/workflow/legacy/Screen;)V

    check-cast p3, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, p3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 99
    iget-object p1, p0, Lcom/squareup/buyercheckout/BuyerCartCoordinator;->cartAdapter:Lcom/squareup/ui/buyercart/BuyerCartAdapter;

    if-nez p1, :cond_8

    const-string p2, "cartAdapter"

    invoke-static {p2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 101
    :cond_8
    iget-object p2, p0, Lcom/squareup/buyercheckout/BuyerCartCoordinator;->buyerCartFormatter:Lcom/squareup/ui/cart/BuyerCartFormatter;

    invoke-virtual {p2}, Lcom/squareup/ui/cart/BuyerCartFormatter;->getDisplayItems()Ljava/util/List;

    move-result-object p2

    .line 102
    iget-object p3, p0, Lcom/squareup/buyercheckout/BuyerCartCoordinator;->formattedTotalProvider:Lcom/squareup/ui/buyer/FormattedTotalProvider;

    invoke-interface {p3}, Lcom/squareup/ui/buyer/FormattedTotalProvider;->getFormattedTotalAmount()Ljava/lang/String;

    move-result-object p3

    .line 100
    invoke-direct {p0, p2, p3}, Lcom/squareup/buyercheckout/BuyerCartCoordinator;->fromDisplayItems(Ljava/util/List;Ljava/lang/String;)Ljava/util/List;

    move-result-object p2

    .line 99
    invoke-virtual {p1, p2}, Lcom/squareup/ui/buyercart/BuyerCartAdapter;->setDisplayItems(Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 4

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 63
    invoke-direct {p0, p1}, Lcom/squareup/buyercheckout/BuyerCartCoordinator;->bindViews(Landroid/view/View;)V

    .line 65
    new-instance v0, Lcom/squareup/ui/buyercart/BuyerCartAdapter;

    invoke-direct {v0}, Lcom/squareup/ui/buyercart/BuyerCartAdapter;-><init>()V

    iput-object v0, p0, Lcom/squareup/buyercheckout/BuyerCartCoordinator;->cartAdapter:Lcom/squareup/ui/buyercart/BuyerCartAdapter;

    .line 66
    new-instance v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/squareup/buyercheckout/BuyerCartCoordinator;->layoutManager:Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 68
    iget-object v0, p0, Lcom/squareup/buyercheckout/BuyerCartCoordinator;->cart:Landroidx/recyclerview/widget/RecyclerView;

    const-string v1, "cart"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object v2, p0, Lcom/squareup/buyercheckout/BuyerCartCoordinator;->layoutManager:Landroidx/recyclerview/widget/LinearLayoutManager;

    if-nez v2, :cond_1

    const-string v3, "layoutManager"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v2, Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    invoke-virtual {v0, v2}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 69
    iget-object v0, p0, Lcom/squareup/buyercheckout/BuyerCartCoordinator;->cart:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    iget-object v1, p0, Lcom/squareup/buyercheckout/BuyerCartCoordinator;->cartAdapter:Lcom/squareup/ui/buyercart/BuyerCartAdapter;

    if-nez v1, :cond_3

    const-string v2, "cartAdapter"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    check-cast v1, Landroidx/recyclerview/widget/RecyclerView$Adapter;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 71
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    iget-object v1, p0, Lcom/squareup/buyercheckout/BuyerCartCoordinator;->screens:Lio/reactivex/Observable;

    iget-object v2, p0, Lcom/squareup/buyercheckout/BuyerCartCoordinator;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    invoke-interface {v2}, Lcom/squareup/buyer/language/BuyerLocaleOverride;->localeOverrideFactory()Lio/reactivex/Observable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/util/rx2/Observables;->combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 72
    new-instance v1, Lcom/squareup/buyercheckout/BuyerCartCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/buyercheckout/BuyerCartCoordinator$attach$1;-><init>(Lcom/squareup/buyercheckout/BuyerCartCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "combineLatest(screens, b\u2026verrideFactory)\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method
