.class public interface abstract Lcom/squareup/buyercheckout/CustomerCheckoutSettings;
.super Ljava/lang/Object;
.source "CustomerCheckoutSettings.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0004\u0008f\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H&J\u0008\u0010\u0004\u001a\u00020\u0003H&J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0003H&J\u0010\u0010\u0008\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\u0003H&\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/buyercheckout/CustomerCheckoutSettings;",
        "",
        "isDefaultCustomerCheckoutEnabled",
        "",
        "isSkipCartScreenEnabled",
        "setDefaultCustomerCheckoutEnabled",
        "",
        "defaultBuyerCheckoutEnabled",
        "setSkipCartScreenEnabled",
        "skipCartScreenEnabled",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract isDefaultCustomerCheckoutEnabled()Z
.end method

.method public abstract isSkipCartScreenEnabled()Z
.end method

.method public abstract setDefaultCustomerCheckoutEnabled(Z)V
.end method

.method public abstract setSkipCartScreenEnabled(Z)V
.end method
