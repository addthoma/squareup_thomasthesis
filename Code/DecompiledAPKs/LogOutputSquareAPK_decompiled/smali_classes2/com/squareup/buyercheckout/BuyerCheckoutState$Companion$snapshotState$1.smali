.class final Lcom/squareup/buyercheckout/BuyerCheckoutState$Companion$snapshotState$1;
.super Lkotlin/jvm/internal/Lambda;
.source "BuyerCheckoutState.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/buyercheckout/BuyerCheckoutState$Companion;->snapshotState(Lcom/squareup/buyercheckout/BuyerCheckoutState;)Lcom/squareup/workflow/Snapshot;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lokio/BufferedSink;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lokio/BufferedSink;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/buyercheckout/BuyerCheckoutState;


# direct methods
.method constructor <init>(Lcom/squareup/buyercheckout/BuyerCheckoutState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/buyercheckout/BuyerCheckoutState$Companion$snapshotState$1;->$state:Lcom/squareup/buyercheckout/BuyerCheckoutState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 102
    check-cast p1, Lokio/BufferedSink;

    invoke-virtual {p0, p1}, Lcom/squareup/buyercheckout/BuyerCheckoutState$Companion$snapshotState$1;->invoke(Lokio/BufferedSink;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lokio/BufferedSink;)V
    .locals 2

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    iget-object v0, p0, Lcom/squareup/buyercheckout/BuyerCheckoutState$Companion$snapshotState$1;->$state:Lcom/squareup/buyercheckout/BuyerCheckoutState;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "state::class.java.name"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 117
    iget-object v0, p0, Lcom/squareup/buyercheckout/BuyerCheckoutState$Companion$snapshotState$1;->$state:Lcom/squareup/buyercheckout/BuyerCheckoutState;

    .line 118
    instance-of v1, v0, Lcom/squareup/buyercheckout/BuyerCheckoutState$Starting;

    if-eqz v1, :cond_0

    .line 119
    check-cast v0, Lcom/squareup/buyercheckout/BuyerCheckoutState$Starting;

    invoke-virtual {v0}, Lcom/squareup/buyercheckout/BuyerCheckoutState$Starting;->getSkipCart()Z

    move-result v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeBooleanAsInt(Lokio/BufferedSink;Z)Lokio/BufferedSink;

    goto/16 :goto_0

    .line 121
    :cond_0
    instance-of v1, v0, Lcom/squareup/buyercheckout/BuyerCheckoutState$InBuyerCart;

    if-eqz v1, :cond_1

    .line 122
    check-cast v0, Lcom/squareup/buyercheckout/BuyerCheckoutState$InBuyerCart;

    invoke-virtual {v0}, Lcom/squareup/buyercheckout/BuyerCheckoutState$InBuyerCart;->getData()Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;->writeToSnapshot(Lokio/BufferedSink;)Lokio/BufferedSink;

    goto/16 :goto_0

    .line 124
    :cond_1
    instance-of v1, v0, Lcom/squareup/buyercheckout/BuyerCheckoutState$InPaymentPrompt;

    if-eqz v1, :cond_2

    .line 125
    check-cast v0, Lcom/squareup/buyercheckout/BuyerCheckoutState$InPaymentPrompt;

    invoke-virtual {v0}, Lcom/squareup/buyercheckout/BuyerCheckoutState$InPaymentPrompt;->getData()Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;->writeToSnapshot(Lokio/BufferedSink;)Lokio/BufferedSink;

    goto/16 :goto_0

    .line 127
    :cond_2
    instance-of v1, v0, Lcom/squareup/buyercheckout/BuyerCheckoutState$StartTipping;

    if-eqz v1, :cond_3

    .line 128
    check-cast v0, Lcom/squareup/buyercheckout/BuyerCheckoutState$StartTipping;

    invoke-virtual {v0}, Lcom/squareup/buyercheckout/BuyerCheckoutState$StartTipping;->getData()Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;->writeToSnapshot(Lokio/BufferedSink;)Lokio/BufferedSink;

    goto :goto_0

    .line 130
    :cond_3
    instance-of v1, v0, Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;

    if-eqz v1, :cond_5

    .line 131
    check-cast v0, Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;

    invoke-virtual {v0}, Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;->getData()Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;->writeToSnapshot(Lokio/BufferedSink;)Lokio/BufferedSink;

    .line 133
    iget-object v0, p0, Lcom/squareup/buyercheckout/BuyerCheckoutState$Companion$snapshotState$1;->$state:Lcom/squareup/buyercheckout/BuyerCheckoutState;

    check-cast v0, Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;

    invoke-virtual {v0}, Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;->getTipWorkflowHandle()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/legacyintegration/LegacyState;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacyintegration/LegacyState;->getSnapshot()Lcom/squareup/workflow/Snapshot;

    move-result-object v0

    if-nez v0, :cond_4

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_4
    invoke-virtual {v0}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object v0

    .line 132
    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeByteStringWithLength(Lokio/BufferedSink;Lokio/ByteString;)Lokio/BufferedSink;

    goto :goto_0

    .line 136
    :cond_5
    instance-of v1, v0, Lcom/squareup/buyercheckout/BuyerCheckoutState$InLanguageSelection;

    if-eqz v1, :cond_7

    .line 137
    check-cast v0, Lcom/squareup/buyercheckout/BuyerCheckoutState$InLanguageSelection;

    invoke-virtual {v0}, Lcom/squareup/buyercheckout/BuyerCheckoutState$InLanguageSelection;->getData()Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;->writeToSnapshot(Lokio/BufferedSink;)Lokio/BufferedSink;

    .line 139
    sget-object v0, Lcom/squareup/buyercheckout/BuyerCheckoutState;->Companion:Lcom/squareup/buyercheckout/BuyerCheckoutState$Companion;

    iget-object v1, p0, Lcom/squareup/buyercheckout/BuyerCheckoutState$Companion$snapshotState$1;->$state:Lcom/squareup/buyercheckout/BuyerCheckoutState;

    check-cast v1, Lcom/squareup/buyercheckout/BuyerCheckoutState$InLanguageSelection;

    invoke-virtual {v1}, Lcom/squareup/buyercheckout/BuyerCheckoutState$InLanguageSelection;->getFromState()Lcom/squareup/buyercheckout/BuyerCheckoutState;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/buyercheckout/BuyerCheckoutState$Companion;->access$snapshotState(Lcom/squareup/buyercheckout/BuyerCheckoutState$Companion;Lcom/squareup/buyercheckout/BuyerCheckoutState;)Lcom/squareup/workflow/Snapshot;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object v0

    .line 138
    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeByteStringWithLength(Lokio/BufferedSink;Lokio/ByteString;)Lokio/BufferedSink;

    .line 142
    iget-object v0, p0, Lcom/squareup/buyercheckout/BuyerCheckoutState$Companion$snapshotState$1;->$state:Lcom/squareup/buyercheckout/BuyerCheckoutState;

    check-cast v0, Lcom/squareup/buyercheckout/BuyerCheckoutState$InLanguageSelection;

    invoke-virtual {v0}, Lcom/squareup/buyercheckout/BuyerCheckoutState$InLanguageSelection;->getWorkflowHandle()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/legacyintegration/LegacyState;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacyintegration/LegacyState;->getSnapshot()Lcom/squareup/workflow/Snapshot;

    move-result-object v0

    if-nez v0, :cond_6

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_6
    invoke-virtual {v0}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object v0

    .line 141
    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeByteStringWithLength(Lokio/BufferedSink;Lokio/ByteString;)Lokio/BufferedSink;

    :goto_0
    return-void

    .line 145
    :cond_7
    instance-of p1, v0, Lcom/squareup/buyercheckout/BuyerCheckoutState$Stopping;

    if-nez p1, :cond_9

    instance-of p1, v0, Lcom/squareup/buyercheckout/BuyerCheckoutState$WaitingForCancelPermission;

    if-eqz p1, :cond_8

    goto :goto_1

    .line 148
    :cond_8
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_9
    :goto_1
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
