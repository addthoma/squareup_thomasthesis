.class public Lcom/squareup/ActivityListener;
.super Lcom/squareup/util/ActivityLifecycleCallbacksAdapter;
.source "ActivityListener.java"

# interfaces
.implements Lcom/squareup/util/ForegroundActivityProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ActivityListener$ResumedPausedListener;
    }
.end annotation


# instance fields
.field private activityCreatedCount:I

.field private createdActivities:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private createdActivityRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/util/Optional<",
            "Landroid/app/Activity;",
            ">;>;"
        }
    .end annotation
.end field

.field private foregroundActivity:Landroid/app/Activity;

.field private final resumedActivities:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private volatile resumedActivityClassName:Ljava/lang/String;

.field private resumedActivityRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/util/Optional<",
            "Landroid/app/Activity;",
            ">;>;"
        }
    .end annotation
.end field

.field private final resumedPausedListenerList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ActivityListener$ResumedPausedListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 41
    invoke-direct {p0}, Lcom/squareup/util/ActivityLifecycleCallbacksAdapter;-><init>()V

    const/4 v0, 0x0

    .line 25
    iput v0, p0, Lcom/squareup/ActivityListener;->activityCreatedCount:I

    .line 26
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/ActivityListener;->resumedActivities:Ljava/util/Set;

    .line 33
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/squareup/ActivityListener;->createdActivities:Ljava/util/LinkedList;

    .line 36
    invoke-static {}, Lcom/squareup/util/Optional;->empty()Lcom/squareup/util/Optional;

    move-result-object v0

    invoke-static {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ActivityListener;->resumedActivityRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 39
    invoke-static {}, Lcom/squareup/util/Optional;->empty()Lcom/squareup/util/Optional;

    move-result-object v0

    invoke-static {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ActivityListener;->createdActivityRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/ActivityListener;->resumedPausedListenerList:Ljava/util/List;

    return-void
.end method

.method private getActivityName(Landroid/app/Activity;)Ljava/lang/String;
    .locals 0

    .line 107
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public getActivityCreatedCount()I
    .locals 1

    .line 115
    iget v0, p0, Lcom/squareup/ActivityListener;->activityCreatedCount:I

    return v0
.end method

.method public getForegroundActivity()Landroid/app/Activity;
    .locals 1

    .line 133
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 134
    iget-object v0, p0, Lcom/squareup/ActivityListener;->foregroundActivity:Landroid/app/Activity;

    return-object v0
.end method

.method public getResumedActivityClassName()Ljava/lang/String;
    .locals 1

    .line 111
    iget-object v0, p0, Lcom/squareup/ActivityListener;->resumedActivityClassName:Ljava/lang/String;

    return-object v0
.end method

.method public latestCreatedActivity()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/util/Optional<",
            "Landroid/app/Activity;",
            ">;>;"
        }
    .end annotation

    .line 142
    iget-object v0, p0, Lcom/squareup/ActivityListener;->createdActivityRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0

    .line 46
    iget p2, p0, Lcom/squareup/ActivityListener;->activityCreatedCount:I

    add-int/lit8 p2, p2, 0x1

    iput p2, p0, Lcom/squareup/ActivityListener;->activityCreatedCount:I

    .line 47
    iget-object p2, p0, Lcom/squareup/ActivityListener;->createdActivities:Ljava/util/LinkedList;

    invoke-virtual {p2, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 48
    iget-object p2, p0, Lcom/squareup/ActivityListener;->createdActivityRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-static {p1}, Lcom/squareup/util/Optional;->of(Ljava/lang/Object;)Lcom/squareup/util/Optional;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public onActivityDestroyed(Landroid/app/Activity;)V
    .locals 1

    .line 98
    iget-object v0, p0, Lcom/squareup/ActivityListener;->createdActivities:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 99
    iget-object p1, p0, Lcom/squareup/ActivityListener;->createdActivities:Ljava/util/LinkedList;

    invoke-virtual {p1}, Ljava/util/LinkedList;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 100
    iget-object p1, p0, Lcom/squareup/ActivityListener;->createdActivityRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-static {}, Lcom/squareup/util/Optional;->empty()Lcom/squareup/util/Optional;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    goto :goto_0

    .line 102
    :cond_0
    iget-object p1, p0, Lcom/squareup/ActivityListener;->createdActivityRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object v0, p0, Lcom/squareup/ActivityListener;->createdActivities:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Optional;->of(Ljava/lang/Object;)Lcom/squareup/util/Optional;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public onActivityPaused(Landroid/app/Activity;)V
    .locals 5

    .line 74
    iget-object v0, p0, Lcom/squareup/ActivityListener;->resumedActivities:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 75
    :goto_0
    iget-object v3, p0, Lcom/squareup/ActivityListener;->resumedActivities:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 76
    iget-object v3, p0, Lcom/squareup/ActivityListener;->resumedActivities:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    if-lez v3, :cond_1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    const/4 v2, 0x0

    if-nez v1, :cond_2

    .line 79
    iput-object v2, p0, Lcom/squareup/ActivityListener;->resumedActivityClassName:Ljava/lang/String;

    .line 85
    :cond_2
    iget-object v3, p0, Lcom/squareup/ActivityListener;->foregroundActivity:Landroid/app/Activity;

    if-ne v3, p1, :cond_3

    .line 86
    iput-object v2, p0, Lcom/squareup/ActivityListener;->foregroundActivity:Landroid/app/Activity;

    .line 87
    iget-object p1, p0, Lcom/squareup/ActivityListener;->resumedActivityRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-static {}, Lcom/squareup/util/Optional;->empty()Lcom/squareup/util/Optional;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    :cond_3
    if-eqz v0, :cond_4

    if-nez v1, :cond_4

    .line 91
    iget-object p1, p0, Lcom/squareup/ActivityListener;->resumedPausedListenerList:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ActivityListener$ResumedPausedListener;

    .line 92
    invoke-interface {v0}, Lcom/squareup/ActivityListener$ResumedPausedListener;->onPause()V

    goto :goto_2

    :cond_4
    return-void
.end method

.method public onActivityResumed(Landroid/app/Activity;)V
    .locals 5

    .line 53
    iget-object v0, p0, Lcom/squareup/ActivityListener;->resumedActivities:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 54
    :goto_0
    iget-object v3, p0, Lcom/squareup/ActivityListener;->resumedActivities:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 55
    iget-object v3, p0, Lcom/squareup/ActivityListener;->resumedActivities:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    if-lez v3, :cond_1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_2

    .line 58
    invoke-direct {p0, p1}, Lcom/squareup/ActivityListener;->getActivityName(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/squareup/ActivityListener;->resumedActivityClassName:Ljava/lang/String;

    .line 61
    :cond_2
    iput-object p1, p0, Lcom/squareup/ActivityListener;->foregroundActivity:Landroid/app/Activity;

    .line 62
    iget-object v2, p0, Lcom/squareup/ActivityListener;->resumedActivityRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-static {p1}, Lcom/squareup/util/Optional;->of(Ljava/lang/Object;)Lcom/squareup/util/Optional;

    move-result-object p1

    invoke-virtual {v2, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    if-nez v0, :cond_3

    if-eqz v1, :cond_3

    .line 65
    iget-object p1, p0, Lcom/squareup/ActivityListener;->resumedPausedListenerList:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ActivityListener$ResumedPausedListener;

    .line 66
    invoke-interface {v0}, Lcom/squareup/ActivityListener$ResumedPausedListener;->onResume()V

    goto :goto_2

    :cond_3
    return-void
.end method

.method public registerResumedPausedListener(Lcom/squareup/ActivityListener$ResumedPausedListener;)V
    .locals 1

    .line 122
    iget-object v0, p0, Lcom/squareup/ActivityListener;->resumedPausedListenerList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public resumedActivity()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/util/Optional<",
            "Landroid/app/Activity;",
            ">;>;"
        }
    .end annotation

    .line 138
    iget-object v0, p0, Lcom/squareup/ActivityListener;->resumedActivityRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public unregisterResumedPausedListener(Lcom/squareup/ActivityListener$ResumedPausedListener;)V
    .locals 1

    .line 129
    iget-object v0, p0, Lcom/squareup/ActivityListener;->resumedPausedListenerList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method
