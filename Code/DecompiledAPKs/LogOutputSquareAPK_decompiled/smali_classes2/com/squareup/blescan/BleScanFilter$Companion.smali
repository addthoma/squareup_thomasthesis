.class public final Lcom/squareup/blescan/BleScanFilter$Companion;
.super Ljava/lang/Object;
.source "BleScanFilter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/blescan/BleScanFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00082\u0006\u0010\t\u001a\u00020\u0006H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/blescan/BleScanFilter$Companion;",
        "",
        "()V",
        "SCAN_RECORD_TYPE_UUID",
        "",
        "SCAN_RECORD_TYPE_UUID_BYTES",
        "",
        "parseScanRecord",
        "Landroid/util/SparseArray;",
        "scanRecord",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 28
    invoke-direct {p0}, Lcom/squareup/blescan/BleScanFilter$Companion;-><init>()V

    return-void
.end method

.method public static final synthetic access$parseScanRecord(Lcom/squareup/blescan/BleScanFilter$Companion;[B)Landroid/util/SparseArray;
    .locals 0

    .line 28
    invoke-direct {p0, p1}, Lcom/squareup/blescan/BleScanFilter$Companion;->parseScanRecord([B)Landroid/util/SparseArray;

    move-result-object p0

    return-object p0
.end method

.method private final parseScanRecord([B)Landroid/util/SparseArray;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)",
            "Landroid/util/SparseArray<",
            "[B>;"
        }
    .end annotation

    .line 36
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    const/4 v1, 0x0

    .line 39
    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_2

    add-int/lit8 v2, v1, 0x1

    .line 40
    aget-byte v1, p1, v1

    if-nez v1, :cond_0

    goto :goto_1

    .line 44
    :cond_0
    aget-byte v3, p1, v2

    if-nez v3, :cond_1

    goto :goto_1

    :cond_1
    add-int/lit8 v4, v2, 0x1

    add-int/2addr v1, v2

    .line 48
    invoke-static {p1, v4, v1}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v2

    .line 50
    invoke-virtual {v0, v3, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0

    :cond_2
    :goto_1
    return-object v0
.end method
