.class public interface abstract Lcom/squareup/blescan/SystemBleScanner;
.super Ljava/lang/Object;
.source "SystemBleScanner.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/blescan/SystemBleScanner$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0008f\u0018\u0000 \u00052\u00020\u0001:\u0001\u0005J\u0008\u0010\u0002\u001a\u00020\u0003H&J\u0008\u0010\u0004\u001a\u00020\u0003H&\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/squareup/blescan/SystemBleScanner;",
        "",
        "startScan",
        "",
        "stopScan",
        "Companion",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/blescan/SystemBleScanner$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/squareup/blescan/SystemBleScanner$Companion;->$$INSTANCE:Lcom/squareup/blescan/SystemBleScanner$Companion;

    sput-object v0, Lcom/squareup/blescan/SystemBleScanner;->Companion:Lcom/squareup/blescan/SystemBleScanner$Companion;

    return-void
.end method


# virtual methods
.method public abstract startScan()V
.end method

.method public abstract stopScan()V
.end method
