.class public final Lcom/squareup/blescan/BleScanFilter;
.super Ljava/lang/Object;
.source "BleScanFilter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/blescan/BleScanFilter$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0012\n\u0002\u0008\u0002\u0018\u0000 \u000c2\u00020\u0001:\u0001\u000cB\u0005\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004H\u0007J\u000e\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\tJ\u000e\u0010\u0006\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\u000b\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/blescan/BleScanFilter;",
        "",
        "()V",
        "createScanFiltersForSquarePos",
        "",
        "Landroid/bluetooth/le/ScanFilter;",
        "isCompatibleDevice",
        "",
        "bleScanResult",
        "Lcom/squareup/blescan/BleScanResult;",
        "scanRecord",
        "",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/blescan/BleScanFilter$Companion;

# The value of this static final field might be set in the static constructor
.field private static final SCAN_RECORD_TYPE_UUID:I = 0x6

.field private static final SCAN_RECORD_TYPE_UUID_BYTES:[B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/blescan/BleScanFilter$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/blescan/BleScanFilter$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/blescan/BleScanFilter;->Companion:Lcom/squareup/blescan/BleScanFilter$Companion;

    const/4 v0, 0x6

    .line 29
    sput v0, Lcom/squareup/blescan/BleScanFilter;->SCAN_RECORD_TYPE_UUID:I

    const/16 v0, 0x10

    new-array v0, v0, [B

    .line 33
    fill-array-data v0, :array_0

    sput-object v0, Lcom/squareup/blescan/BleScanFilter;->SCAN_RECORD_TYPE_UUID_BYTES:[B

    return-void

    nop

    :array_0
    .array-data 1
        0x7ct
        -0x12t
        -0x71t
        0x75t
        -0x28t
        -0x42t
        0x17t
        -0x4ft
        0x7ct
        0x4bt
        0x15t
        0x8t
        0x61t
        -0x12t
        -0x7ft
        0x15t
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createScanFiltersForSquarePos()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Landroid/bluetooth/le/ScanFilter;",
            ">;"
        }
    .end annotation

    .line 18
    new-instance v0, Landroid/os/ParcelUuid;

    sget-object v1, Lcom/squareup/cardreader/ble/R12GattPublic;->Companion:Lcom/squareup/cardreader/ble/R12GattPublic$Companion;

    invoke-virtual {v1}, Lcom/squareup/cardreader/ble/R12GattPublic$Companion;->getUUID_LCR_SERVICE()Ljava/util/UUID;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    .line 19
    new-instance v1, Landroid/bluetooth/le/ScanFilter$Builder;

    invoke-direct {v1}, Landroid/bluetooth/le/ScanFilter$Builder;-><init>()V

    invoke-virtual {v1, v0}, Landroid/bluetooth/le/ScanFilter$Builder;->setServiceUuid(Landroid/os/ParcelUuid;)Landroid/bluetooth/le/ScanFilter$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/bluetooth/le/ScanFilter$Builder;->build()Landroid/bluetooth/le/ScanFilter;

    move-result-object v0

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final isCompatibleDevice(Lcom/squareup/blescan/BleScanResult;)Z
    .locals 1

    const-string v0, "bleScanResult"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-virtual {p1}, Lcom/squareup/blescan/BleScanResult;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object p1

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final isCompatibleDevice([B)Z
    .locals 1

    const-string v0, "scanRecord"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    sget-object v0, Lcom/squareup/blescan/BleScanFilter;->Companion:Lcom/squareup/blescan/BleScanFilter$Companion;

    invoke-static {v0, p1}, Lcom/squareup/blescan/BleScanFilter$Companion;->access$parseScanRecord(Lcom/squareup/blescan/BleScanFilter$Companion;[B)Landroid/util/SparseArray;

    move-result-object p1

    .line 24
    sget v0, Lcom/squareup/blescan/BleScanFilter;->SCAN_RECORD_TYPE_UUID:I

    invoke-virtual {p1, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [B

    .line 25
    sget-object v0, Lcom/squareup/blescan/BleScanFilter;->SCAN_RECORD_TYPE_UUID_BYTES:[B

    invoke-static {v0, p1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result p1

    return p1
.end method
