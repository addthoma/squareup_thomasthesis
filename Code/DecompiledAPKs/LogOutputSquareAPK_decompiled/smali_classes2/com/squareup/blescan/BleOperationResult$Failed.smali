.class public abstract Lcom/squareup/blescan/BleOperationResult$Failed;
.super Lcom/squareup/blescan/BleOperationResult;
.source "BleOperationResult.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/blescan/BleOperationResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Failed"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/blescan/BleOperationResult$Failed$AlreadyRunning;,
        Lcom/squareup/blescan/BleOperationResult$Failed$BleNotSupported;,
        Lcom/squareup/blescan/BleOperationResult$Failed$BleDisabled;,
        Lcom/squareup/blescan/BleOperationResult$Failed$BlePermissionNotGranted;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0004\u0004\u0005\u0006\u0007B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0003\u0082\u0001\u0004\u0008\t\n\u000b\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/blescan/BleOperationResult$Failed;",
        "Lcom/squareup/blescan/BleOperationResult;",
        "",
        "()V",
        "AlreadyRunning",
        "BleDisabled",
        "BleNotSupported",
        "BlePermissionNotGranted",
        "Lcom/squareup/blescan/BleOperationResult$Failed$AlreadyRunning;",
        "Lcom/squareup/blescan/BleOperationResult$Failed$BleNotSupported;",
        "Lcom/squareup/blescan/BleOperationResult$Failed$BleDisabled;",
        "Lcom/squareup/blescan/BleOperationResult$Failed$BlePermissionNotGranted;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 4
    invoke-direct {p0, v0}, Lcom/squareup/blescan/BleOperationResult;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 4
    invoke-direct {p0}, Lcom/squareup/blescan/BleOperationResult$Failed;-><init>()V

    return-void
.end method
