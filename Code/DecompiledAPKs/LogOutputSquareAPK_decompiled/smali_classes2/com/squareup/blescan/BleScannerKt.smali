.class public final Lcom/squareup/blescan/BleScannerKt;
.super Ljava/lang/Object;
.source "BleScanner.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBleScanner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BleScanner.kt\ncom/squareup/blescan/BleScannerKt\n*L\n1#1,182:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u0010\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\u0002\u00a8\u0006\u0004"
    }
    d2 = {
        "buildBleScanResult",
        "Lcom/squareup/blescan/BleScanResult;",
        "result",
        "Landroid/bluetooth/le/ScanResult;",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$buildBleScanResult(Landroid/bluetooth/le/ScanResult;)Lcom/squareup/blescan/BleScanResult;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/blescan/BleScannerKt;->buildBleScanResult(Landroid/bluetooth/le/ScanResult;)Lcom/squareup/blescan/BleScanResult;

    move-result-object p0

    return-object p0
.end method

.method private static final buildBleScanResult(Landroid/bluetooth/le/ScanResult;)Lcom/squareup/blescan/BleScanResult;
    .locals 3

    .line 173
    invoke-virtual {p0}, Landroid/bluetooth/le/ScanResult;->getScanRecord()Landroid/bluetooth/le/ScanRecord;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const v2, 0x827e

    .line 174
    invoke-virtual {v0, v2}, Landroid/bluetooth/le/ScanRecord;->getManufacturerSpecificData(I)[B

    move-result-object v0

    if-eqz v0, :cond_0

    .line 176
    aget-byte v0, v0, v1

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    const/4 v1, 0x1

    .line 180
    :cond_0
    new-instance v0, Lcom/squareup/blescan/BleScanResult;

    invoke-virtual {p0}, Landroid/bluetooth/le/ScanResult;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object p0

    const-string v2, "result.device"

    invoke-static {p0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p0, v1}, Lcom/squareup/blescan/BleScanResult;-><init>(Landroid/bluetooth/BluetoothDevice;Z)V

    return-object v0
.end method
