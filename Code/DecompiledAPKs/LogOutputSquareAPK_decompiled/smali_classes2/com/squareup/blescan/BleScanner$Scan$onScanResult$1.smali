.class final Lcom/squareup/blescan/BleScanner$Scan$onScanResult$1;
.super Ljava/lang/Object;
.source "BleScanner.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/blescan/BleScanner$Scan;->onScanResult(ILandroid/bluetooth/le/ScanResult;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "run"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $bleScanResult:Lcom/squareup/blescan/BleScanResult;

.field final synthetic this$0:Lcom/squareup/blescan/BleScanner$Scan;


# direct methods
.method constructor <init>(Lcom/squareup/blescan/BleScanner$Scan;Lcom/squareup/blescan/BleScanResult;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/blescan/BleScanner$Scan$onScanResult$1;->this$0:Lcom/squareup/blescan/BleScanner$Scan;

    iput-object p2, p0, Lcom/squareup/blescan/BleScanner$Scan$onScanResult$1;->$bleScanResult:Lcom/squareup/blescan/BleScanResult;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .line 89
    iget-object v0, p0, Lcom/squareup/blescan/BleScanner$Scan$onScanResult$1;->this$0:Lcom/squareup/blescan/BleScanner$Scan;

    iget-object v0, v0, Lcom/squareup/blescan/BleScanner$Scan;->this$0:Lcom/squareup/blescan/BleScanner;

    invoke-static {v0}, Lcom/squareup/blescan/BleScanner;->access$getBleScanFilter$p(Lcom/squareup/blescan/BleScanner;)Lcom/squareup/blescan/BleScanFilter;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/blescan/BleScanner$Scan$onScanResult$1;->$bleScanResult:Lcom/squareup/blescan/BleScanResult;

    invoke-virtual {v0, v1}, Lcom/squareup/blescan/BleScanFilter;->isCompatibleDevice(Lcom/squareup/blescan/BleScanResult;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 90
    :cond_0
    iget-object v0, p0, Lcom/squareup/blescan/BleScanner$Scan$onScanResult$1;->$bleScanResult:Lcom/squareup/blescan/BleScanResult;

    invoke-virtual {v0}, Lcom/squareup/blescan/BleScanResult;->getLookingToPair()Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    .line 91
    :cond_1
    iget-object v0, p0, Lcom/squareup/blescan/BleScanner$Scan$onScanResult$1;->this$0:Lcom/squareup/blescan/BleScanner$Scan;

    invoke-static {v0}, Lcom/squareup/blescan/BleScanner$Scan;->access$getCardreaders$p(Lcom/squareup/blescan/BleScanner$Scan;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/blescan/BleScanner$Scan$onScanResult$1;->$bleScanResult:Lcom/squareup/blescan/BleScanResult;

    invoke-virtual {v1}, Lcom/squareup/blescan/BleScanResult;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    return-void

    .line 92
    :cond_2
    iget-object v0, p0, Lcom/squareup/blescan/BleScanner$Scan$onScanResult$1;->this$0:Lcom/squareup/blescan/BleScanner$Scan;

    invoke-static {v0}, Lcom/squareup/blescan/BleScanner$Scan;->access$getCardreaders$p(Lcom/squareup/blescan/BleScanner$Scan;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/blescan/BleScanner$Scan$onScanResult$1;->$bleScanResult:Lcom/squareup/blescan/BleScanResult;

    invoke-virtual {v1}, Lcom/squareup/blescan/BleScanResult;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    const-string v2, "bleScanResult.device.address"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v3, Lcom/squareup/blescan/DiscoveredDevice;

    .line 93
    iget-object v4, p0, Lcom/squareup/blescan/BleScanner$Scan$onScanResult$1;->$bleScanResult:Lcom/squareup/blescan/BleScanResult;

    invoke-virtual {v4}, Lcom/squareup/blescan/BleScanResult;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v4

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/squareup/blescan/BleScanner$Scan$onScanResult$1;->$bleScanResult:Lcom/squareup/blescan/BleScanResult;

    invoke-virtual {v2}, Lcom/squareup/blescan/BleScanResult;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v2

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_3

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 94
    :cond_3
    iget-object v5, p0, Lcom/squareup/blescan/BleScanner$Scan$onScanResult$1;->$bleScanResult:Lcom/squareup/blescan/BleScanResult;

    invoke-virtual {v5}, Lcom/squareup/blescan/BleScanResult;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v5

    invoke-virtual {v5}, Landroid/bluetooth/BluetoothDevice;->getType()I

    move-result v5

    .line 92
    invoke-direct {v3, v4, v2, v5}, Lcom/squareup/blescan/DiscoveredDevice;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    iget-object v0, p0, Lcom/squareup/blescan/BleScanner$Scan$onScanResult$1;->this$0:Lcom/squareup/blescan/BleScanner$Scan;

    invoke-static {v0}, Lcom/squareup/blescan/BleScanner$Scan;->access$emitResults(Lcom/squareup/blescan/BleScanner$Scan;)V

    return-void
.end method
