.class final Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$LA_ComponentImpl;
.super Ljava/lang/Object;
.source "DaggerSposReleaseAppComponent.java"

# interfaces
.implements Lcom/squareup/ui/LocationActivity$Component;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "LA_ComponentImpl"
.end annotation


# instance fields
.field final synthetic this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;


# direct methods
.method private constructor <init>(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)V
    .locals 0

    .line 10266
    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$LA_ComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;Lcom/squareup/DaggerSposReleaseAppComponent$1;)V
    .locals 0

    .line 10265
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$LA_ComponentImpl;-><init>(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)V

    return-void
.end method

.method private getMediaButtonDisabler()Lcom/squareup/ui/MediaButtonDisabler;
    .locals 1

    .line 10271
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$LA_ComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$600(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/AppBootstrapModule;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/AppBootstrapModule_ProvideApplicationFactory;->provideApplication(Lcom/squareup/AppBootstrapModule;)Landroid/app/Application;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/ui/MediaButtonDisabler_Factory;->newInstance(Landroid/app/Application;)Lcom/squareup/ui/MediaButtonDisabler;

    move-result-object v0

    return-object v0
.end method

.method private injectLocationActivity(Lcom/squareup/ui/LocationActivity;)Lcom/squareup/ui/LocationActivity;
    .locals 1

    .line 10278
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$LA_ComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/analytics/Analytics;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectAnalytics(Lcom/squareup/ui/SquareActivity;Lcom/squareup/analytics/Analytics;)V

    .line 10279
    new-instance v0, Lcom/squareup/development/drawer/ReleaseContentViewInitializer;

    invoke-direct {v0}, Lcom/squareup/development/drawer/ReleaseContentViewInitializer;-><init>()V

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectContentViewInitializer(Lcom/squareup/ui/SquareActivity;Lcom/squareup/development/drawer/ContentViewInitializer;)V

    .line 10280
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$LA_ComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$2300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectLocationMonitor(Lcom/squareup/ui/SquareActivity;Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;)V

    .line 10281
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$LA_ComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$2400(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/radiography/FocusedActivityScanner;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectFocusedActivityScanner(Lcom/squareup/ui/SquareActivity;Lcom/squareup/radiography/FocusedActivityScanner;)V

    .line 10282
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$LA_ComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$2500(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderPauseAndResumer;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectCardReaderPauseAndResumer(Lcom/squareup/ui/SquareActivity;Lcom/squareup/cardreader/CardReaderPauseAndResumer;)V

    .line 10283
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$LA_ComponentImpl;->getMediaButtonDisabler()Lcom/squareup/ui/MediaButtonDisabler;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectMediaButtonDisabler(Lcom/squareup/ui/SquareActivity;Lcom/squareup/ui/MediaButtonDisabler;)V

    .line 10284
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$LA_ComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$2600(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectMinesweeperProvider(Lcom/squareup/ui/SquareActivity;Ljavax/inject/Provider;)V

    .line 10285
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$LA_ComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$2700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/internet/InternetStatusMonitor;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectInternetStatusMonitor(Lcom/squareup/ui/SquareActivity;Lcom/squareup/internet/InternetStatusMonitor;)V

    .line 10286
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$LA_ComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$2800(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/settings/server/FeatureFlagFeatures;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectFeatures(Lcom/squareup/ui/SquareActivity;Lcom/squareup/settings/server/Features;)V

    .line 10287
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$LA_ComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$2900(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/persistentbundle/PersistentBundleManager;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectPersistentBundleManager(Lcom/squareup/ui/SquareActivity;Lcom/squareup/persistentbundle/PersistentBundleManager;)V

    .line 10288
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$LA_ComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$3000(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/utilities/ui/RealDevice;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectDevice(Lcom/squareup/ui/SquareActivity;Lcom/squareup/util/Device;)V

    .line 10289
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$LA_ComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$3100(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ActivityResultHandler;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectActivityResultHandler(Lcom/squareup/ui/SquareActivity;Lcom/squareup/ui/ActivityResultHandler;)V

    .line 10290
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$LA_ComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$3200(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/AndroidConfigurationChangeMonitor;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectConfigurationChangeMonitor(Lcom/squareup/ui/SquareActivity;Lcom/squareup/util/AndroidConfigurationChangeMonitor;)V

    .line 10291
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$LA_ComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$2300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;

    invoke-static {p1, v0}, Lcom/squareup/ui/LocationActivity_MembersInjector;->injectLocationMonitor(Lcom/squareup/ui/LocationActivity;Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;)V

    .line 10292
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$LA_ComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$9700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/LocationActivity_MembersInjector;->injectLocationProvider(Lcom/squareup/ui/LocationActivity;Ljavax/inject/Provider;)V

    .line 10293
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$LA_ComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$2800(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/settings/server/FeatureFlagFeatures;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/LocationActivity_MembersInjector;->injectFeatures(Lcom/squareup/ui/LocationActivity;Lcom/squareup/settings/server/Features;)V

    .line 10294
    invoke-static {}, Lcom/squareup/ui/ResetTaskOnBack_Factory;->newInstance()Lcom/squareup/ui/ResetTaskOnBack;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/LocationActivity_MembersInjector;->injectLocationCanceledHandler(Lcom/squareup/ui/LocationActivity;Lcom/squareup/ui/LocationActivityBackHandler;)V

    return-object p1
.end method


# virtual methods
.method public inject(Lcom/squareup/ui/LocationActivity;)V
    .locals 0

    .line 10275
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$LA_ComponentImpl;->injectLocationActivity(Lcom/squareup/ui/LocationActivity;)Lcom/squareup/ui/LocationActivity;

    return-void
.end method
