.class public Lcom/squareup/backgroundjob/mock/MockBackgroundJobManager;
.super Ljava/lang/Object;
.source "MockBackgroundJobManager.java"

# interfaces
.implements Lcom/squareup/backgroundjob/BackgroundJobManager;


# instance fields
.field private final jobCreators:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/evernote/android/job/JobCreator;",
            ">;"
        }
    .end annotation
.end field

.field private final jobNotificationManager:Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;

.field private final jobRequestsById:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lcom/evernote/android/job/JobRequest;",
            ">;"
        }
    .end annotation
.end field

.field private final jobRequestsByTag:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Set<",
            "Lcom/evernote/android/job/JobRequest;",
            ">;>;"
        }
    .end annotation
.end field

.field private final jobsById:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lcom/squareup/backgroundjob/mock/MockJob;",
            ">;"
        }
    .end annotation
.end field

.field private final jobsByTag:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Set<",
            "Lcom/squareup/backgroundjob/mock/MockJob;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;)V
    .locals 1

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/backgroundjob/mock/MockBackgroundJobManager;->jobRequestsById:Ljava/util/Map;

    .line 32
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/backgroundjob/mock/MockBackgroundJobManager;->jobRequestsByTag:Ljava/util/Map;

    .line 33
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/backgroundjob/mock/MockBackgroundJobManager;->jobsById:Ljava/util/Map;

    .line 34
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/backgroundjob/mock/MockBackgroundJobManager;->jobsByTag:Ljava/util/Map;

    .line 35
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/backgroundjob/mock/MockBackgroundJobManager;->jobCreators:Ljava/util/Set;

    .line 62
    iput-object p2, p0, Lcom/squareup/backgroundjob/mock/MockBackgroundJobManager;->jobNotificationManager:Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;

    const/4 p2, 0x1

    .line 63
    invoke-static {p2}, Lcom/evernote/android/job/JobConfig;->setForceAllowApi14(Z)V

    .line 65
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/evernote/android/job/JobManager;->create(Landroid/content/Context;)Lcom/evernote/android/job/JobManager;

    return-void
.end method

.method private createJob(Ljava/lang/String;)Lcom/squareup/backgroundjob/mock/MockJob;
    .locals 2

    .line 191
    iget-object v0, p0, Lcom/squareup/backgroundjob/mock/MockBackgroundJobManager;->jobCreators:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/evernote/android/job/JobCreator;

    .line 192
    new-instance v1, Lcom/squareup/backgroundjob/mock/MockJob;

    invoke-interface {v0, p1}, Lcom/evernote/android/job/JobCreator;->create(Ljava/lang/String;)Lcom/evernote/android/job/Job;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/backgroundjob/mock/MockBackgroundJobManager;->jobNotificationManager:Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;

    invoke-direct {v1, p1, v0}, Lcom/squareup/backgroundjob/mock/MockJob;-><init>(Lcom/evernote/android/job/Job;Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1
.end method

.method private registerJob(Lcom/squareup/backgroundjob/mock/MockJob;Ljava/lang/String;I)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    .line 180
    :cond_0
    iget-object v0, p0, Lcom/squareup/backgroundjob/mock/MockBackgroundJobManager;->jobsById:Ljava/util/Map;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    invoke-interface {v0, p3, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 181
    iget-object p3, p0, Lcom/squareup/backgroundjob/mock/MockBackgroundJobManager;->jobsByTag:Ljava/util/Map;

    invoke-interface {p3, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/util/Set;

    if-nez p3, :cond_1

    .line 183
    new-instance p3, Ljava/util/LinkedHashSet;

    invoke-direct {p3}, Ljava/util/LinkedHashSet;-><init>()V

    .line 185
    :cond_1
    invoke-interface {p3, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 186
    iget-object p1, p0, Lcom/squareup/backgroundjob/mock/MockBackgroundJobManager;->jobsByTag:Ljava/util/Map;

    invoke-interface {p1, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private registerRequest(Lcom/evernote/android/job/JobRequest;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    .line 162
    :cond_0
    invoke-virtual {p1}, Lcom/evernote/android/job/JobRequest;->isUpdateCurrent()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 163
    invoke-virtual {p1}, Lcom/evernote/android/job/JobRequest;->getTag()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/backgroundjob/mock/MockBackgroundJobManager;->cancelAllForTag(Ljava/lang/String;)I

    .line 166
    :cond_1
    iget-object v0, p0, Lcom/squareup/backgroundjob/mock/MockBackgroundJobManager;->jobRequestsById:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/evernote/android/job/JobRequest;->getJobId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 167
    invoke-virtual {p1}, Lcom/evernote/android/job/JobRequest;->getTag()Ljava/lang/String;

    move-result-object v0

    .line 168
    iget-object v1, p0, Lcom/squareup/backgroundjob/mock/MockBackgroundJobManager;->jobRequestsByTag:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    if-nez v1, :cond_2

    .line 170
    new-instance v1, Ljava/util/LinkedHashSet;

    invoke-direct {v1}, Ljava/util/LinkedHashSet;-><init>()V

    .line 172
    :cond_2
    invoke-interface {v1, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 173
    iget-object p1, p0, Lcom/squareup/backgroundjob/mock/MockBackgroundJobManager;->jobRequestsByTag:Ljava/util/Map;

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public addJobCreator(Lcom/squareup/backgroundjob/BackgroundJobCreator;)V
    .locals 1

    .line 150
    iget-object v0, p0, Lcom/squareup/backgroundjob/mock/MockBackgroundJobManager;->jobCreators:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public cancel(I)Z
    .locals 2

    .line 100
    iget-object v0, p0, Lcom/squareup/backgroundjob/mock/MockBackgroundJobManager;->jobRequestsById:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/evernote/android/job/JobRequest;

    if-eqz v0, :cond_0

    .line 102
    invoke-virtual {v0}, Lcom/evernote/android/job/JobRequest;->cancelAndEdit()Lcom/evernote/android/job/JobRequest$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/evernote/android/job/JobRequest$Builder;->build()Lcom/evernote/android/job/JobRequest;

    .line 105
    :cond_0
    iget-object v1, p0, Lcom/squareup/backgroundjob/mock/MockBackgroundJobManager;->jobsById:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/backgroundjob/mock/MockJob;

    invoke-virtual {p1}, Lcom/squareup/backgroundjob/mock/MockJob;->getJob()Lcom/evernote/android/job/Job;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 107
    invoke-virtual {p1}, Lcom/evernote/android/job/Job;->cancel()V

    :cond_1
    if-nez v0, :cond_3

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    goto :goto_1

    :cond_3
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public cancelAll()I
    .locals 3

    .line 116
    invoke-virtual {p0}, Lcom/squareup/backgroundjob/mock/MockBackgroundJobManager;->getAllJobRequests()Ljava/util/Set;

    move-result-object v0

    .line 117
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/evernote/android/job/JobRequest;

    .line 118
    invoke-virtual {v2}, Lcom/evernote/android/job/JobRequest;->cancelAndEdit()Lcom/evernote/android/job/JobRequest$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/evernote/android/job/JobRequest$Builder;->build()Lcom/evernote/android/job/JobRequest;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 122
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/backgroundjob/mock/MockBackgroundJobManager;->getAllJobs()Ljava/util/Set;

    move-result-object v0

    .line 123
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/evernote/android/job/Job;

    .line 124
    invoke-virtual {v2}, Lcom/evernote/android/job/Job;->cancel()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    return v1
.end method

.method public cancelAllForTag(Ljava/lang/String;)I
    .locals 3

    .line 134
    invoke-virtual {p0, p1}, Lcom/squareup/backgroundjob/mock/MockBackgroundJobManager;->getAllJobRequestsForTag(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    .line 135
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/evernote/android/job/JobRequest;

    .line 136
    invoke-virtual {v2}, Lcom/evernote/android/job/JobRequest;->cancelAndEdit()Lcom/evernote/android/job/JobRequest$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/evernote/android/job/JobRequest$Builder;->build()Lcom/evernote/android/job/JobRequest;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 140
    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/backgroundjob/mock/MockBackgroundJobManager;->getAllJobsForTag(Ljava/lang/String;)Ljava/util/Set;

    move-result-object p1

    .line 141
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/evernote/android/job/Job;

    .line 142
    invoke-virtual {v0}, Lcom/evernote/android/job/Job;->cancel()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    return v1
.end method

.method public getAllJobRequests()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/evernote/android/job/JobRequest;",
            ">;"
        }
    .end annotation

    .line 79
    new-instance v0, Ljava/util/LinkedHashSet;

    iget-object v1, p0, Lcom/squareup/backgroundjob/mock/MockBackgroundJobManager;->jobRequestsById:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/LinkedHashSet;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getAllJobRequestsForTag(Ljava/lang/String;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set<",
            "Lcom/evernote/android/job/JobRequest;",
            ">;"
        }
    .end annotation

    .line 83
    iget-object v0, p0, Lcom/squareup/backgroundjob/mock/MockBackgroundJobManager;->jobRequestsByTag:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Set;

    .line 84
    new-instance v0, Ljava/util/LinkedHashSet;

    if-eqz p1, :cond_0

    invoke-direct {v0, p1}, Ljava/util/LinkedHashSet;-><init>(Ljava/util/Collection;)V

    goto :goto_0

    :cond_0
    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    :goto_0
    return-object v0
.end method

.method public getAllJobs()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/backgroundjob/BackgroundJob;",
            ">;"
        }
    .end annotation

    .line 92
    iget-object v0, p0, Lcom/squareup/backgroundjob/mock/MockBackgroundJobManager;->jobsById:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/backgroundjob/BackgroundJobs;->asBackgroundJobs(Ljava/util/Collection;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getAllJobsForTag(Ljava/lang/String;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set<",
            "Lcom/squareup/backgroundjob/BackgroundJob;",
            ">;"
        }
    .end annotation

    .line 96
    iget-object v0, p0, Lcom/squareup/backgroundjob/mock/MockBackgroundJobManager;->jobsByTag:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Collection;

    invoke-static {p1}, Lcom/squareup/backgroundjob/BackgroundJobs;->asBackgroundJobs(Ljava/util/Collection;)Ljava/util/Set;

    move-result-object p1

    return-object p1
.end method

.method public getJob(I)Lcom/evernote/android/job/Job;
    .locals 1

    .line 88
    iget-object v0, p0, Lcom/squareup/backgroundjob/mock/MockBackgroundJobManager;->jobsById:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/backgroundjob/mock/MockJob;

    invoke-virtual {p1}, Lcom/squareup/backgroundjob/mock/MockJob;->getJob()Lcom/evernote/android/job/Job;

    move-result-object p1

    return-object p1
.end method

.method public getJobRequest(I)Lcom/evernote/android/job/JobRequest;
    .locals 1

    .line 75
    iget-object v0, p0, Lcom/squareup/backgroundjob/mock/MockBackgroundJobManager;->jobRequestsById:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/evernote/android/job/JobRequest;

    return-object p1
.end method

.method public removeJobCreator(Lcom/squareup/backgroundjob/BackgroundJobCreator;)V
    .locals 1

    .line 154
    iget-object v0, p0, Lcom/squareup/backgroundjob/mock/MockBackgroundJobManager;->jobCreators:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public schedule(Lcom/evernote/android/job/JobRequest;)V
    .locals 2

    .line 69
    invoke-virtual {p1}, Lcom/evernote/android/job/JobRequest;->getTag()Ljava/lang/String;

    move-result-object v0

    .line 70
    invoke-direct {p0, p1}, Lcom/squareup/backgroundjob/mock/MockBackgroundJobManager;->registerRequest(Lcom/evernote/android/job/JobRequest;)V

    .line 71
    invoke-direct {p0, v0}, Lcom/squareup/backgroundjob/mock/MockBackgroundJobManager;->createJob(Ljava/lang/String;)Lcom/squareup/backgroundjob/mock/MockJob;

    move-result-object v1

    invoke-virtual {p1}, Lcom/evernote/android/job/JobRequest;->getJobId()I

    move-result p1

    invoke-direct {p0, v1, v0, p1}, Lcom/squareup/backgroundjob/mock/MockBackgroundJobManager;->registerJob(Lcom/squareup/backgroundjob/mock/MockJob;Ljava/lang/String;I)V

    return-void
.end method
