.class public final Lcom/squareup/anrchaperone/VisibilityTracker;
.super Lcom/squareup/util/ActivityLifecycleCallbacksAdapter;
.source "VisibilityTracker.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\rH\u0016J\u0010\u0010\u000e\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\rH\u0016R\u001e\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0004@BX\u0086\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/anrchaperone/VisibilityTracker;",
        "Lcom/squareup/util/ActivityLifecycleCallbacksAdapter;",
        "()V",
        "<set-?>",
        "",
        "hasVisibleActivities",
        "getHasVisibleActivities",
        "()Z",
        "startedActivityCount",
        "",
        "onActivityStarted",
        "",
        "activity",
        "Landroid/app/Activity;",
        "onActivityStopped",
        "anr-chaperone_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private hasVisibleActivities:Z

.field private startedActivityCount:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Lcom/squareup/util/ActivityLifecycleCallbacksAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final getHasVisibleActivities()Z
    .locals 1

    .line 15
    iget-boolean v0, p0, Lcom/squareup/anrchaperone/VisibilityTracker;->hasVisibleActivities:Z

    return v0
.end method

.method public onActivityStarted(Landroid/app/Activity;)V
    .locals 1

    const-string v0, "activity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    iget p1, p0, Lcom/squareup/anrchaperone/VisibilityTracker;->startedActivityCount:I

    const/4 v0, 0x1

    add-int/2addr p1, v0

    iput p1, p0, Lcom/squareup/anrchaperone/VisibilityTracker;->startedActivityCount:I

    .line 20
    iget-boolean p1, p0, Lcom/squareup/anrchaperone/VisibilityTracker;->hasVisibleActivities:Z

    if-nez p1, :cond_0

    iget p1, p0, Lcom/squareup/anrchaperone/VisibilityTracker;->startedActivityCount:I

    if-ne p1, v0, :cond_0

    .line 21
    iput-boolean v0, p0, Lcom/squareup/anrchaperone/VisibilityTracker;->hasVisibleActivities:Z

    :cond_0
    return-void
.end method

.method public onActivityStopped(Landroid/app/Activity;)V
    .locals 1

    const-string v0, "activity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    iget v0, p0, Lcom/squareup/anrchaperone/VisibilityTracker;->startedActivityCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/squareup/anrchaperone/VisibilityTracker;->startedActivityCount:I

    .line 27
    iget-boolean v0, p0, Lcom/squareup/anrchaperone/VisibilityTracker;->hasVisibleActivities:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/anrchaperone/VisibilityTracker;->startedActivityCount:I

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/app/Activity;->isChangingConfigurations()Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 28
    iput-boolean p1, p0, Lcom/squareup/anrchaperone/VisibilityTracker;->hasVisibleActivities:Z

    :cond_0
    return-void
.end method
