.class public Lcom/squareup/anrchaperone/AnrChaperoneModule;
.super Ljava/lang/Object;
.source "AnrChaperoneModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/anrchaperone/AnrChaperoneModule$Chaperone;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public provideChaperoneThread()Lcom/squareup/thread/executor/SerialExecutor;
    .locals 3
    .annotation runtime Ldagger/Provides;
    .end annotation

    const-string v0, "AnrChaperone"

    const/16 v1, 0xa

    const/4 v2, 0x1

    .line 24
    invoke-static {v0, v1, v2}, Lcom/squareup/thread/executor/Executors;->stoppableNamedThreadExecutor(Ljava/lang/String;IZ)Lcom/squareup/thread/executor/StoppableSerialExecutor;

    move-result-object v0

    return-object v0
.end method
