.class public final Lcom/squareup/billhistoryui/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/billhistoryui/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final activity_applet_bill_history_detail_view:I = 0x7f0d002b

.field public static final activity_applet_bill_history_view:I = 0x7f0d002c

.field public static final activity_applet_bill_history_view_loyalty_section:I = 0x7f0d002d

.field public static final activity_applet_bulk_settle_button:I = 0x7f0d002e

.field public static final activity_applet_bulk_settle_header_row:I = 0x7f0d002f

.field public static final activity_applet_bulk_settle_sort_row:I = 0x7f0d0030

.field public static final activity_applet_bulk_settle_tender_row:I = 0x7f0d0031

.field public static final activity_applet_bulk_settle_view:I = 0x7f0d0032

.field public static final activity_applet_bulk_settle_view_all_row:I = 0x7f0d0033

.field public static final activity_applet_instant_deposit_row_list:I = 0x7f0d0035

.field public static final activity_applet_instant_deposit_row_sidebar:I = 0x7f0d0036

.field public static final activity_applet_list_header_padding_row:I = 0x7f0d003a

.field public static final activity_applet_list_header_row:I = 0x7f0d003b

.field public static final activity_applet_list_row:I = 0x7f0d003c

.field public static final activity_applet_quicktip_editor:I = 0x7f0d003d

.field public static final activity_applet_select_gift_receipt_tender_view:I = 0x7f0d0042

.field public static final activity_applet_select_receipt_tender_view:I = 0x7f0d0043

.field public static final activity_applet_select_refund_tender_view:I = 0x7f0d0044

.field public static final activity_applet_sidebar_search_message:I = 0x7f0d0045

.field public static final activity_applet_tip_options_view:I = 0x7f0d0046

.field public static final activity_applet_transactions_history_list:I = 0x7f0d0047

.field public static final activity_applet_transactions_history_view:I = 0x7f0d0048

.field public static final bill_history_entry_row_contents:I = 0x7f0d008a

.field public static final bill_history_items_section:I = 0x7f0d008b

.field public static final bill_history_related_bill_section:I = 0x7f0d008c

.field public static final bill_history_related_bill_section_header:I = 0x7f0d008d

.field public static final bill_history_tax_breakdown_row:I = 0x7f0d008e

.field public static final bill_history_tax_breakdown_section:I = 0x7f0d008f

.field public static final bill_history_tender_section:I = 0x7f0d0090

.field public static final bill_history_tender_section_customer_row:I = 0x7f0d0091

.field public static final bill_history_tender_section_header:I = 0x7f0d0092

.field public static final bill_history_total_section:I = 0x7f0d0093

.field public static final instant_deposits_result_view:I = 0x7f0d02d6

.field public static final receipt_detail_settle_tip_row:I = 0x7f0d046d

.field public static final receipt_detail_settle_tip_row_contents:I = 0x7f0d046e

.field public static final transactions_history_list_header_row:I = 0x7f0d0561

.field public static final transactions_history_list_row:I = 0x7f0d0562

.field public static final transactions_history_sidebar_list_row:I = 0x7f0d0563


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 222
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
