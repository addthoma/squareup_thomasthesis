.class public interface abstract Lcom/squareup/Card$InputType$InputTypeHandler;
.super Ljava/lang/Object;
.source "Card.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/Card$InputType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "InputTypeHandler"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract handleA10(Lcom/squareup/Card$InputType;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/Card$InputType;",
            ")TE;"
        }
    .end annotation
.end method

.method public abstract handleGen2(Lcom/squareup/Card$InputType;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/Card$InputType;",
            ")TE;"
        }
    .end annotation
.end method

.method public abstract handleManual(Lcom/squareup/Card$InputType;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/Card$InputType;",
            ")TE;"
        }
    .end annotation
.end method

.method public abstract handleO1(Lcom/squareup/Card$InputType;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/Card$InputType;",
            ")TE;"
        }
    .end annotation
.end method

.method public abstract handleR4(Lcom/squareup/Card$InputType;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/Card$InputType;",
            ")TE;"
        }
    .end annotation
.end method

.method public abstract handleR6(Lcom/squareup/Card$InputType;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/Card$InputType;",
            ")TE;"
        }
    .end annotation
.end method

.method public abstract handleT2(Lcom/squareup/Card$InputType;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/Card$InputType;",
            ")TE;"
        }
    .end annotation
.end method

.method public abstract handleX2(Lcom/squareup/Card$InputType;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/Card$InputType;",
            ")TE;"
        }
    .end annotation
.end method
