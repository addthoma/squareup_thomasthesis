.class public Lcom/squareup/barcodescanners/BarcodeScannerTracker;
.super Ljava/lang/Object;
.source "BarcodeScannerTracker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedRunnable;,
        Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedListener;,
        Lcom/squareup/barcodescanners/BarcodeScannerTracker$ConnectionListener;
    }
.end annotation


# static fields
.field public static final BARCODE_SCANNED_LAST_CHARACTER_DELAY_MS:I = 0x64


# instance fields
.field private final barcodeScannedListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedListener;",
            ">;"
        }
    .end annotation
.end field

.field private final barcodeScannedRunnables:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/barcodescanners/BarcodeScanner;",
            "Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedRunnable;",
            ">;"
        }
    .end annotation
.end field

.field private final barcodeScanners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/barcodescanners/BarcodeScanner;",
            ">;"
        }
    .end annotation
.end field

.field private final connectionListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/barcodescanners/BarcodeScannerTracker$ConnectionListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;


# direct methods
.method public constructor <init>(Lcom/squareup/thread/executor/MainThread;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->mainThread:Lcom/squareup/thread/executor/MainThread;

    .line 30
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->barcodeScannedListeners:Ljava/util/List;

    .line 31
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->connectionListeners:Ljava/util/List;

    .line 32
    new-instance p1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {p1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object p1, p0, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->barcodeScannedRunnables:Ljava/util/Map;

    .line 33
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->barcodeScanners:Ljava/util/List;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/barcodescanners/BarcodeScannerTracker;)Ljava/util/Map;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->barcodeScannedRunnables:Ljava/util/Map;

    return-object p0
.end method


# virtual methods
.method public addBarcodeScannedListener(Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedListener;)V
    .locals 1

    .line 37
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 38
    iget-object v0, p0, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->barcodeScannedListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public addConnectionListener(Lcom/squareup/barcodescanners/BarcodeScannerTracker$ConnectionListener;)V
    .locals 1

    .line 47
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 48
    iget-object v0, p0, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->connectionListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public barcodeScanned(Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "Barcode scanned: %s"

    .line 134
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 135
    iget-object v0, p0, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v1, Lcom/squareup/barcodescanners/-$$Lambda$BarcodeScannerTracker$O2cp--r8TtX2fTxa8gCVOn2uJUc;

    invoke-direct {v1, p0, p1}, Lcom/squareup/barcodescanners/-$$Lambda$BarcodeScannerTracker$O2cp--r8TtX2fTxa8gCVOn2uJUc;-><init>(Lcom/squareup/barcodescanners/BarcodeScannerTracker;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->post(Ljava/lang/Runnable;)V

    return-void
.end method

.method barcodeScannerConnected(Lcom/squareup/barcodescanners/BarcodeScanner;)V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 95
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "Barcode scanner connected: %s"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 96
    iget-object v0, p0, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v1, Lcom/squareup/barcodescanners/-$$Lambda$BarcodeScannerTracker$V5Bk9jObFagjuSzysFUWrSq0Nr4;

    invoke-direct {v1, p0, p1}, Lcom/squareup/barcodescanners/-$$Lambda$BarcodeScannerTracker$V5Bk9jObFagjuSzysFUWrSq0Nr4;-><init>(Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/barcodescanners/BarcodeScanner;)V

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->post(Ljava/lang/Runnable;)V

    return-void
.end method

.method barcodeScannerDisconnected(Lcom/squareup/barcodescanners/BarcodeScanner;)V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 114
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "Barcode scanner disconnected: %s"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 115
    iget-object v0, p0, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->barcodeScannedRunnables:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedRunnable;

    if-eqz v0, :cond_0

    .line 117
    iget-object v1, p0, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->mainThread:Lcom/squareup/thread/executor/MainThread;

    invoke-interface {v1, v0}, Lcom/squareup/thread/executor/MainThread;->cancel(Ljava/lang/Runnable;)V

    .line 120
    :cond_0
    iget-object v0, p0, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v1, Lcom/squareup/barcodescanners/-$$Lambda$BarcodeScannerTracker$722e5IIuRZyryDk9eUw07FvNtgA;

    invoke-direct {v1, p0, p1}, Lcom/squareup/barcodescanners/-$$Lambda$BarcodeScannerTracker$722e5IIuRZyryDk9eUw07FvNtgA;-><init>(Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/barcodescanners/BarcodeScanner;)V

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->post(Ljava/lang/Runnable;)V

    return-void
.end method

.method characterScannedForBarcodeScanner(Lcom/squareup/barcodescanners/BarcodeScanner;C)V
    .locals 3

    .line 74
    iget-object v0, p0, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->barcodeScannedRunnables:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedRunnable;

    if-eqz v0, :cond_0

    .line 78
    iget-object v1, p0, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->mainThread:Lcom/squareup/thread/executor/MainThread;

    invoke-interface {v1, v0}, Lcom/squareup/thread/executor/MainThread;->cancel(Ljava/lang/Runnable;)V

    .line 79
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, v0, Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedRunnable;->barcode:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    .line 81
    :cond_0
    invoke-static {p2}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object p2

    .line 84
    :goto_0
    new-instance v0, Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedRunnable;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedRunnable;-><init>(Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/barcodescanners/BarcodeScanner;Ljava/lang/String;)V

    .line 86
    iget-object p2, p0, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->barcodeScannedRunnables:Ljava/util/Map;

    invoke-interface {p2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    iget-object p1, p0, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->mainThread:Lcom/squareup/thread/executor/MainThread;

    const-wide/16 v1, 0x64

    invoke-interface {p1, v0, v1, v2}, Lcom/squareup/thread/executor/MainThread;->executeDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public getAvailableBarcodeScannerCount()I
    .locals 1

    .line 64
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 65
    iget-object v0, p0, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->barcodeScanners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getAvailableBarcodeScanners()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/barcodescanners/BarcodeScanner;",
            ">;"
        }
    .end annotation

    .line 58
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 59
    iget-object v0, p0, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->barcodeScanners:Ljava/util/List;

    return-object v0
.end method

.method public synthetic lambda$barcodeScanned$2$BarcodeScannerTracker(Ljava/lang/String;)V
    .locals 2

    .line 136
    iget-object v0, p0, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->barcodeScannedListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedListener;

    .line 137
    invoke-interface {v1, p1}, Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedListener;->barcodeScanned(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public synthetic lambda$barcodeScannerConnected$0$BarcodeScannerTracker(Lcom/squareup/barcodescanners/BarcodeScanner;)V
    .locals 2

    .line 97
    iget-object v0, p0, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->barcodeScanners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 101
    iget-object v0, p0, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->barcodeScanners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 103
    iget-object v0, p0, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->connectionListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/barcodescanners/BarcodeScannerTracker$ConnectionListener;

    .line 104
    invoke-interface {v1, p1}, Lcom/squareup/barcodescanners/BarcodeScannerTracker$ConnectionListener;->barcodeScannerConnected(Lcom/squareup/barcodescanners/BarcodeScanner;)V

    goto :goto_0

    :cond_0
    return-void

    .line 98
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Unable to connect same barcode scanner twice."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public synthetic lambda$barcodeScannerDisconnected$1$BarcodeScannerTracker(Lcom/squareup/barcodescanners/BarcodeScanner;)V
    .locals 2

    .line 121
    iget-object v0, p0, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->barcodeScanners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 126
    iget-object v0, p0, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->connectionListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/barcodescanners/BarcodeScannerTracker$ConnectionListener;

    .line 127
    invoke-interface {v1, p1}, Lcom/squareup/barcodescanners/BarcodeScannerTracker$ConnectionListener;->barcodeScannerDisconnected(Lcom/squareup/barcodescanners/BarcodeScanner;)V

    goto :goto_0

    :cond_0
    return-void

    .line 123
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Unable to disconnect nonexistent barcode scanner."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public removeBarcodeScannedListener(Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedListener;)V
    .locals 1

    .line 42
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 43
    iget-object v0, p0, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->barcodeScannedListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public removeConnectionListener(Lcom/squareup/barcodescanners/BarcodeScannerTracker$ConnectionListener;)V
    .locals 1

    .line 52
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 53
    iget-object v0, p0, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->connectionListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method
