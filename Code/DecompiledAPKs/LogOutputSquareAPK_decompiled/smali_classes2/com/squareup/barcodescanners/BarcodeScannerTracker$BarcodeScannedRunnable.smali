.class Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedRunnable;
.super Ljava/lang/Object;
.source "BarcodeScannerTracker.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/barcodescanners/BarcodeScannerTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BarcodeScannedRunnable"
.end annotation


# instance fields
.field final barcode:Ljava/lang/String;

.field final barcodeScanner:Lcom/squareup/barcodescanners/BarcodeScanner;

.field final synthetic this$0:Lcom/squareup/barcodescanners/BarcodeScannerTracker;


# direct methods
.method constructor <init>(Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/barcodescanners/BarcodeScanner;Ljava/lang/String;)V
    .locals 0

    .line 166
    iput-object p1, p0, Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedRunnable;->this$0:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 167
    iput-object p2, p0, Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedRunnable;->barcodeScanner:Lcom/squareup/barcodescanners/BarcodeScanner;

    .line 168
    iput-object p3, p0, Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedRunnable;->barcode:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .line 172
    iget-object v0, p0, Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedRunnable;->this$0:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    iget-object v1, p0, Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedRunnable;->barcode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->barcodeScanned(Ljava/lang/String;)V

    .line 173
    iget-object v0, p0, Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedRunnable;->this$0:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    invoke-static {v0}, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->access$000(Lcom/squareup/barcodescanners/BarcodeScannerTracker;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedRunnable;->barcodeScanner:Lcom/squareup/barcodescanners/BarcodeScanner;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
