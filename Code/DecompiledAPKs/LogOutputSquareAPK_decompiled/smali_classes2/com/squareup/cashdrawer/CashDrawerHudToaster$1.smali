.class Lcom/squareup/cashdrawer/CashDrawerHudToaster$1;
.super Ljava/lang/Object;
.source "CashDrawerHudToaster.java"

# interfaces
.implements Lcom/squareup/cashdrawer/CashDrawerTracker$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/cashdrawer/CashDrawerHudToaster;->buildListener()Lcom/squareup/cashdrawer/CashDrawerTracker$Listener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/cashdrawer/CashDrawerHudToaster;


# direct methods
.method constructor <init>(Lcom/squareup/cashdrawer/CashDrawerHudToaster;)V
    .locals 0

    .line 37
    iput-object p1, p0, Lcom/squareup/cashdrawer/CashDrawerHudToaster$1;->this$0:Lcom/squareup/cashdrawer/CashDrawerHudToaster;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public cashDrawerConnected(Lcom/squareup/cashdrawer/CashDrawer;)V
    .locals 2

    .line 39
    iget-object p1, p0, Lcom/squareup/cashdrawer/CashDrawerHudToaster$1;->this$0:Lcom/squareup/cashdrawer/CashDrawerHudToaster;

    invoke-virtual {p1}, Lcom/squareup/cashdrawer/CashDrawerHudToaster;->showCashDrawerConnected()V

    .line 43
    iget-object p1, p0, Lcom/squareup/cashdrawer/CashDrawerHudToaster$1;->this$0:Lcom/squareup/cashdrawer/CashDrawerHudToaster;

    invoke-static {p1}, Lcom/squareup/cashdrawer/CashDrawerHudToaster;->access$000(Lcom/squareup/cashdrawer/CashDrawerHudToaster;)Lcom/squareup/cashdrawer/CashDrawerTracker;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/cashdrawer/CashDrawerTracker;->getAvailableUsbCashDrawerCount()I

    move-result p1

    .line 44
    iget-object v0, p0, Lcom/squareup/cashdrawer/CashDrawerHudToaster$1;->this$0:Lcom/squareup/cashdrawer/CashDrawerHudToaster;

    invoke-static {v0}, Lcom/squareup/cashdrawer/CashDrawerHudToaster;->access$000(Lcom/squareup/cashdrawer/CashDrawerHudToaster;)Lcom/squareup/cashdrawer/CashDrawerTracker;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cashdrawer/CashDrawerTracker;->getAvailablePrinterCashDrawerCount()I

    move-result v0

    .line 45
    iget-object v1, p0, Lcom/squareup/cashdrawer/CashDrawerHudToaster$1;->this$0:Lcom/squareup/cashdrawer/CashDrawerHudToaster;

    invoke-static {v1}, Lcom/squareup/cashdrawer/CashDrawerHudToaster;->access$100(Lcom/squareup/cashdrawer/CashDrawerHudToaster;)Lcom/squareup/analytics/Analytics;

    move-result-object v1

    invoke-static {p1, v0}, Lcom/squareup/cashdrawer/CashDrawersEvent;->forUsbCashDrawerConnected(II)Lcom/squareup/cashdrawer/CashDrawersEvent;

    move-result-object p1

    invoke-interface {v1, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public cashDrawerDisconnected(Lcom/squareup/cashdrawer/CashDrawer;Lcom/squareup/cashdrawer/CashDrawer$DisconnectReason;)V
    .locals 2

    .line 50
    iget-object p1, p0, Lcom/squareup/cashdrawer/CashDrawerHudToaster$1;->this$0:Lcom/squareup/cashdrawer/CashDrawerHudToaster;

    invoke-virtual {p1, p2}, Lcom/squareup/cashdrawer/CashDrawerHudToaster;->showCashDrawerDisconnected(Lcom/squareup/cashdrawer/CashDrawer$DisconnectReason;)V

    .line 54
    iget-object p1, p0, Lcom/squareup/cashdrawer/CashDrawerHudToaster$1;->this$0:Lcom/squareup/cashdrawer/CashDrawerHudToaster;

    invoke-static {p1}, Lcom/squareup/cashdrawer/CashDrawerHudToaster;->access$000(Lcom/squareup/cashdrawer/CashDrawerHudToaster;)Lcom/squareup/cashdrawer/CashDrawerTracker;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/cashdrawer/CashDrawerTracker;->getAvailableUsbCashDrawerCount()I

    move-result p1

    .line 55
    iget-object v0, p0, Lcom/squareup/cashdrawer/CashDrawerHudToaster$1;->this$0:Lcom/squareup/cashdrawer/CashDrawerHudToaster;

    invoke-static {v0}, Lcom/squareup/cashdrawer/CashDrawerHudToaster;->access$000(Lcom/squareup/cashdrawer/CashDrawerHudToaster;)Lcom/squareup/cashdrawer/CashDrawerTracker;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cashdrawer/CashDrawerTracker;->getAvailablePrinterCashDrawerCount()I

    move-result v0

    .line 56
    iget-object v1, p0, Lcom/squareup/cashdrawer/CashDrawerHudToaster$1;->this$0:Lcom/squareup/cashdrawer/CashDrawerHudToaster;

    invoke-static {v1}, Lcom/squareup/cashdrawer/CashDrawerHudToaster;->access$100(Lcom/squareup/cashdrawer/CashDrawerHudToaster;)Lcom/squareup/analytics/Analytics;

    move-result-object v1

    invoke-static {p1, v0, p2}, Lcom/squareup/cashdrawer/CashDrawersEvent;->forUsbCashDrawerDisconnected(IILcom/squareup/cashdrawer/CashDrawer$DisconnectReason;)Lcom/squareup/cashdrawer/CashDrawersEvent;

    move-result-object p1

    invoke-interface {v1, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public cashDrawersOpened()V
    .locals 3

    .line 61
    iget-object v0, p0, Lcom/squareup/cashdrawer/CashDrawerHudToaster$1;->this$0:Lcom/squareup/cashdrawer/CashDrawerHudToaster;

    invoke-static {v0}, Lcom/squareup/cashdrawer/CashDrawerHudToaster;->access$000(Lcom/squareup/cashdrawer/CashDrawerHudToaster;)Lcom/squareup/cashdrawer/CashDrawerTracker;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cashdrawer/CashDrawerTracker;->getAvailableUsbCashDrawerCount()I

    move-result v0

    .line 62
    iget-object v1, p0, Lcom/squareup/cashdrawer/CashDrawerHudToaster$1;->this$0:Lcom/squareup/cashdrawer/CashDrawerHudToaster;

    invoke-static {v1}, Lcom/squareup/cashdrawer/CashDrawerHudToaster;->access$000(Lcom/squareup/cashdrawer/CashDrawerHudToaster;)Lcom/squareup/cashdrawer/CashDrawerTracker;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/cashdrawer/CashDrawerTracker;->getAvailablePrinterCashDrawerCount()I

    move-result v1

    .line 63
    iget-object v2, p0, Lcom/squareup/cashdrawer/CashDrawerHudToaster$1;->this$0:Lcom/squareup/cashdrawer/CashDrawerHudToaster;

    invoke-static {v2}, Lcom/squareup/cashdrawer/CashDrawerHudToaster;->access$100(Lcom/squareup/cashdrawer/CashDrawerHudToaster;)Lcom/squareup/analytics/Analytics;

    move-result-object v2

    invoke-static {v0, v1}, Lcom/squareup/cashdrawer/CashDrawersEvent;->forCashDrawersOpened(II)Lcom/squareup/cashdrawer/CashDrawersEvent;

    move-result-object v0

    invoke-interface {v2, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method
