.class public Lcom/squareup/billhistory/model/UnknownTenderHistory$Builder;
.super Lcom/squareup/billhistory/model/TenderHistory$Builder;
.source "UnknownTenderHistory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/billhistory/model/UnknownTenderHistory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/billhistory/model/TenderHistory$Builder<",
        "Lcom/squareup/billhistory/model/UnknownTenderHistory;",
        "Lcom/squareup/billhistory/model/UnknownTenderHistory$Builder;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 12
    sget-object v0, Lcom/squareup/billhistory/model/TenderHistory$Type;->UNKNOWN:Lcom/squareup/billhistory/model/TenderHistory$Type;

    invoke-direct {p0, v0}, Lcom/squareup/billhistory/model/TenderHistory$Builder;-><init>(Lcom/squareup/billhistory/model/TenderHistory$Type;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/squareup/billhistory/model/TenderHistory;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/UnknownTenderHistory$Builder;->build()Lcom/squareup/billhistory/model/UnknownTenderHistory;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/squareup/billhistory/model/UnknownTenderHistory;
    .locals 2

    .line 16
    new-instance v0, Lcom/squareup/billhistory/model/UnknownTenderHistory;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/billhistory/model/UnknownTenderHistory;-><init>(Lcom/squareup/billhistory/model/UnknownTenderHistory$Builder;Lcom/squareup/billhistory/model/UnknownTenderHistory$1;)V

    return-object v0
.end method
