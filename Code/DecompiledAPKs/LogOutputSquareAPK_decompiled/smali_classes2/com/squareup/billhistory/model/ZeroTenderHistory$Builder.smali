.class public Lcom/squareup/billhistory/model/ZeroTenderHistory$Builder;
.super Lcom/squareup/billhistory/model/TenderHistory$Builder;
.source "ZeroTenderHistory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/billhistory/model/ZeroTenderHistory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/billhistory/model/TenderHistory$Builder<",
        "Lcom/squareup/billhistory/model/ZeroTenderHistory;",
        "Lcom/squareup/billhistory/model/ZeroTenderHistory$Builder;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 12
    sget-object v0, Lcom/squareup/billhistory/model/TenderHistory$Type;->ZERO_AMOUNT:Lcom/squareup/billhistory/model/TenderHistory$Type;

    invoke-direct {p0, v0}, Lcom/squareup/billhistory/model/TenderHistory$Builder;-><init>(Lcom/squareup/billhistory/model/TenderHistory$Type;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/TenderHistory$Builder;
    .locals 0

    .line 9
    invoke-virtual {p0, p1}, Lcom/squareup/billhistory/model/ZeroTenderHistory$Builder;->amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/ZeroTenderHistory$Builder;

    move-result-object p1

    return-object p1
.end method

.method public amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/ZeroTenderHistory$Builder;
    .locals 5

    .line 16
    iget-object v0, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    .line 20
    invoke-super {p0, p1}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object p1

    check-cast p1, Lcom/squareup/billhistory/model/ZeroTenderHistory$Builder;

    return-object p1

    .line 17
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/squareup/billhistory/model/ZeroTenderHistory;

    .line 18
    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " amount must be $0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public bridge synthetic build()Lcom/squareup/billhistory/model/TenderHistory;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/ZeroTenderHistory$Builder;->build()Lcom/squareup/billhistory/model/ZeroTenderHistory;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/squareup/billhistory/model/ZeroTenderHistory;
    .locals 2

    .line 24
    new-instance v0, Lcom/squareup/billhistory/model/ZeroTenderHistory;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/billhistory/model/ZeroTenderHistory;-><init>(Lcom/squareup/billhistory/model/ZeroTenderHistory$Builder;Lcom/squareup/billhistory/model/ZeroTenderHistory$1;)V

    return-object v0
.end method
