.class public Lcom/squareup/billhistory/model/CashTenderHistory;
.super Lcom/squareup/billhistory/model/TenderHistory;
.source "CashTenderHistory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/billhistory/model/CashTenderHistory$Builder;
    }
.end annotation


# instance fields
.field public final changeAmount:Lcom/squareup/protos/common/Money;

.field public final tenderedAmount:Lcom/squareup/protos/common/Money;


# direct methods
.method private constructor <init>(Lcom/squareup/billhistory/model/CashTenderHistory$Builder;)V
    .locals 1

    .line 44
    invoke-direct {p0, p1}, Lcom/squareup/billhistory/model/TenderHistory;-><init>(Lcom/squareup/billhistory/model/TenderHistory$Builder;)V

    .line 45
    invoke-static {p1}, Lcom/squareup/billhistory/model/CashTenderHistory$Builder;->access$100(Lcom/squareup/billhistory/model/CashTenderHistory$Builder;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/billhistory/model/CashTenderHistory;->tenderedAmount:Lcom/squareup/protos/common/Money;

    .line 46
    invoke-static {p1}, Lcom/squareup/billhistory/model/CashTenderHistory$Builder;->access$200(Lcom/squareup/billhistory/model/CashTenderHistory$Builder;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/billhistory/model/CashTenderHistory;->changeAmount:Lcom/squareup/protos/common/Money;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/billhistory/model/CashTenderHistory$Builder;Lcom/squareup/billhistory/model/CashTenderHistory$1;)V
    .locals 0

    .line 8
    invoke-direct {p0, p1}, Lcom/squareup/billhistory/model/CashTenderHistory;-><init>(Lcom/squareup/billhistory/model/CashTenderHistory$Builder;)V

    return-void
.end method


# virtual methods
.method public buildUpon()Lcom/squareup/billhistory/model/CashTenderHistory$Builder;
    .locals 1

    .line 50
    new-instance v0, Lcom/squareup/billhistory/model/CashTenderHistory$Builder;

    invoke-direct {v0}, Lcom/squareup/billhistory/model/CashTenderHistory$Builder;-><init>()V

    invoke-virtual {v0, p0}, Lcom/squareup/billhistory/model/CashTenderHistory$Builder;->from(Lcom/squareup/billhistory/model/CashTenderHistory;)Lcom/squareup/billhistory/model/CashTenderHistory$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic buildUpon()Lcom/squareup/billhistory/model/TenderHistory$Builder;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/CashTenderHistory;->buildUpon()Lcom/squareup/billhistory/model/CashTenderHistory$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getUnbrandedTenderGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/billhistory/model/CashTenderHistory;->amount:Lcom/squareup/protos/common/Money;

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0}, Lcom/squareup/util/ProtoGlyphs;->cash(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v0

    return-object v0
.end method
