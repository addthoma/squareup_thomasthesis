.class public Lcom/squareup/billhistory/model/UnknownTenderHistory;
.super Lcom/squareup/billhistory/model/TenderHistory;
.source "UnknownTenderHistory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/billhistory/model/UnknownTenderHistory$Builder;
    }
.end annotation


# direct methods
.method private constructor <init>(Lcom/squareup/billhistory/model/UnknownTenderHistory$Builder;)V
    .locals 0

    .line 21
    invoke-direct {p0, p1}, Lcom/squareup/billhistory/model/TenderHistory;-><init>(Lcom/squareup/billhistory/model/TenderHistory$Builder;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/billhistory/model/UnknownTenderHistory$Builder;Lcom/squareup/billhistory/model/UnknownTenderHistory$1;)V
    .locals 0

    .line 7
    invoke-direct {p0, p1}, Lcom/squareup/billhistory/model/UnknownTenderHistory;-><init>(Lcom/squareup/billhistory/model/UnknownTenderHistory$Builder;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic buildUpon()Lcom/squareup/billhistory/model/TenderHistory$Builder;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/UnknownTenderHistory;->buildUpon()Lcom/squareup/billhistory/model/UnknownTenderHistory$Builder;

    move-result-object v0

    return-object v0
.end method

.method public buildUpon()Lcom/squareup/billhistory/model/UnknownTenderHistory$Builder;
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/billhistory/model/UnknownTenderHistory$Builder;

    invoke-direct {v0}, Lcom/squareup/billhistory/model/UnknownTenderHistory$Builder;-><init>()V

    invoke-virtual {v0, p0}, Lcom/squareup/billhistory/model/UnknownTenderHistory$Builder;->from(Lcom/squareup/billhistory/model/TenderHistory;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/billhistory/model/UnknownTenderHistory$Builder;

    return-object v0
.end method
