.class public final Lcom/squareup/autocapture/AutoCaptureTimerStarter_Factory;
.super Ljava/lang/Object;
.source "AutoCaptureTimerStarter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/autocapture/AutoCaptureTimerStarter;",
        ">;"
    }
.end annotation


# instance fields
.field private final autoCaptureControlAlarmProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/autocapture/AutoCaptureControlAlarm;",
            ">;"
        }
    .end annotation
.end field

.field private final executorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/StoppableSerialExecutor;",
            ">;"
        }
    .end annotation
.end field

.field private final jobManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/backgroundjob/BackgroundJobManager;",
            ">;"
        }
    .end annotation
.end field

.field private final ohSnapLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final timerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/autocapture/AutoCaptureControlTimer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/autocapture/AutoCaptureControlTimer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/backgroundjob/BackgroundJobManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/StoppableSerialExecutor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/autocapture/AutoCaptureControlAlarm;",
            ">;)V"
        }
    .end annotation

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/squareup/autocapture/AutoCaptureTimerStarter_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p2, p0, Lcom/squareup/autocapture/AutoCaptureTimerStarter_Factory;->timerProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p3, p0, Lcom/squareup/autocapture/AutoCaptureTimerStarter_Factory;->ohSnapLoggerProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p4, p0, Lcom/squareup/autocapture/AutoCaptureTimerStarter_Factory;->jobManagerProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p5, p0, Lcom/squareup/autocapture/AutoCaptureTimerStarter_Factory;->executorProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p6, p0, Lcom/squareup/autocapture/AutoCaptureTimerStarter_Factory;->autoCaptureControlAlarmProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/autocapture/AutoCaptureTimerStarter_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/autocapture/AutoCaptureControlTimer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/backgroundjob/BackgroundJobManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/StoppableSerialExecutor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/autocapture/AutoCaptureControlAlarm;",
            ">;)",
            "Lcom/squareup/autocapture/AutoCaptureTimerStarter_Factory;"
        }
    .end annotation

    .line 56
    new-instance v7, Lcom/squareup/autocapture/AutoCaptureTimerStarter_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/autocapture/AutoCaptureTimerStarter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/autocapture/AutoCaptureControlTimer;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/backgroundjob/BackgroundJobManager;Lcom/squareup/thread/executor/StoppableSerialExecutor;Lcom/squareup/autocapture/AutoCaptureControlAlarm;)Lcom/squareup/autocapture/AutoCaptureTimerStarter;
    .locals 8

    .line 62
    new-instance v7, Lcom/squareup/autocapture/AutoCaptureTimerStarter;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/autocapture/AutoCaptureTimerStarter;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/autocapture/AutoCaptureControlTimer;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/backgroundjob/BackgroundJobManager;Lcom/squareup/thread/executor/StoppableSerialExecutor;Lcom/squareup/autocapture/AutoCaptureControlAlarm;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/autocapture/AutoCaptureTimerStarter;
    .locals 7

    .line 47
    iget-object v0, p0, Lcom/squareup/autocapture/AutoCaptureTimerStarter_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/autocapture/AutoCaptureTimerStarter_Factory;->timerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/autocapture/AutoCaptureControlTimer;

    iget-object v0, p0, Lcom/squareup/autocapture/AutoCaptureTimerStarter_Factory;->ohSnapLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/log/OhSnapLogger;

    iget-object v0, p0, Lcom/squareup/autocapture/AutoCaptureTimerStarter_Factory;->jobManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/backgroundjob/BackgroundJobManager;

    iget-object v0, p0, Lcom/squareup/autocapture/AutoCaptureTimerStarter_Factory;->executorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/thread/executor/StoppableSerialExecutor;

    iget-object v0, p0, Lcom/squareup/autocapture/AutoCaptureTimerStarter_Factory;->autoCaptureControlAlarmProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/autocapture/AutoCaptureControlAlarm;

    invoke-static/range {v1 .. v6}, Lcom/squareup/autocapture/AutoCaptureTimerStarter_Factory;->newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/autocapture/AutoCaptureControlTimer;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/backgroundjob/BackgroundJobManager;Lcom/squareup/thread/executor/StoppableSerialExecutor;Lcom/squareup/autocapture/AutoCaptureControlAlarm;)Lcom/squareup/autocapture/AutoCaptureTimerStarter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/autocapture/AutoCaptureTimerStarter_Factory;->get()Lcom/squareup/autocapture/AutoCaptureTimerStarter;

    move-result-object v0

    return-object v0
.end method
