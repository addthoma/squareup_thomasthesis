.class public final Lcom/squareup/autocapture/AutoCaptureJobCreator_Factory;
.super Ljava/lang/Object;
.source "AutoCaptureJobCreator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/autocapture/AutoCaptureJobCreator;",
        ">;"
    }
.end annotation


# instance fields
.field private final autoCaptureJobProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/autocapture/AutoCaptureJob;",
            ">;"
        }
    .end annotation
.end field

.field private final jobManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/backgroundjob/BackgroundJobManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/backgroundjob/BackgroundJobManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/autocapture/AutoCaptureJob;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/autocapture/AutoCaptureJobCreator_Factory;->jobManagerProvider:Ljavax/inject/Provider;

    .line 24
    iput-object p2, p0, Lcom/squareup/autocapture/AutoCaptureJobCreator_Factory;->autoCaptureJobProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/autocapture/AutoCaptureJobCreator_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/backgroundjob/BackgroundJobManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/autocapture/AutoCaptureJob;",
            ">;)",
            "Lcom/squareup/autocapture/AutoCaptureJobCreator_Factory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/autocapture/AutoCaptureJobCreator_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/autocapture/AutoCaptureJobCreator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/backgroundjob/BackgroundJobManager;Ljavax/inject/Provider;)Lcom/squareup/autocapture/AutoCaptureJobCreator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/backgroundjob/BackgroundJobManager;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/autocapture/AutoCaptureJob;",
            ">;)",
            "Lcom/squareup/autocapture/AutoCaptureJobCreator;"
        }
    .end annotation

    .line 40
    new-instance v0, Lcom/squareup/autocapture/AutoCaptureJobCreator;

    invoke-direct {v0, p0, p1}, Lcom/squareup/autocapture/AutoCaptureJobCreator;-><init>(Lcom/squareup/backgroundjob/BackgroundJobManager;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/autocapture/AutoCaptureJobCreator;
    .locals 2

    .line 29
    iget-object v0, p0, Lcom/squareup/autocapture/AutoCaptureJobCreator_Factory;->jobManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/backgroundjob/BackgroundJobManager;

    iget-object v1, p0, Lcom/squareup/autocapture/AutoCaptureJobCreator_Factory;->autoCaptureJobProvider:Ljavax/inject/Provider;

    invoke-static {v0, v1}, Lcom/squareup/autocapture/AutoCaptureJobCreator_Factory;->newInstance(Lcom/squareup/backgroundjob/BackgroundJobManager;Ljavax/inject/Provider;)Lcom/squareup/autocapture/AutoCaptureJobCreator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/autocapture/AutoCaptureJobCreator_Factory;->get()Lcom/squareup/autocapture/AutoCaptureJobCreator;

    move-result-object v0

    return-object v0
.end method
