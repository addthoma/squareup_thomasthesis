.class public final Lcom/squareup/charts/piechart/animators/SlidingPieChartAnimator;
.super Ljava/lang/Object;
.source "SlidingPieChartAnimator.kt"

# interfaces
.implements Lcom/squareup/charts/piechart/animators/PieChartAnimator;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSlidingPieChartAnimator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SlidingPieChartAnimator.kt\ncom/squareup/charts/piechart/animators/SlidingPieChartAnimator\n*L\n1#1,47:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0007\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0015\u001a\u00020\u0006H\u0002J\u0008\u0010\u0016\u001a\u00020\u0014H\u0016J\u0008\u0010\u0017\u001a\u00020\u0018H\u0016J\u0008\u0010\u0019\u001a\u00020\u0018H\u0016R\u001a\u0010\u0005\u001a\u00020\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008\"\u0004\u0008\t\u0010\nR\u001a\u0010\u000b\u001a\u00020\u000cX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\r\u0010\u000e\"\u0004\u0008\u000f\u0010\u0010R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0011\u001a\u0004\u0018\u00010\u0012X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/charts/piechart/animators/SlidingPieChartAnimator;",
        "Lcom/squareup/charts/piechart/animators/PieChartAnimator;",
        "pieChart",
        "Lcom/squareup/charts/piechart/PieChart;",
        "(Lcom/squareup/charts/piechart/PieChart;)V",
        "duration",
        "",
        "getDuration",
        "()J",
        "setDuration",
        "(J)V",
        "interpolator",
        "Landroid/view/animation/AccelerateDecelerateInterpolator;",
        "getInterpolator",
        "()Landroid/view/animation/AccelerateDecelerateInterpolator;",
        "setInterpolator",
        "(Landroid/view/animation/AccelerateDecelerateInterpolator;)V",
        "slideInAnimator",
        "Landroid/animation/Animator;",
        "slideInProgress",
        "",
        "defaultAnimationDuration",
        "progress",
        "startAnimating",
        "",
        "stopAnimating",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private duration:J

.field private interpolator:Landroid/view/animation/AccelerateDecelerateInterpolator;

.field private final pieChart:Lcom/squareup/charts/piechart/PieChart;

.field private slideInAnimator:Landroid/animation/Animator;

.field private slideInProgress:F


# direct methods
.method public constructor <init>(Lcom/squareup/charts/piechart/PieChart;)V
    .locals 2

    const-string v0, "pieChart"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/charts/piechart/animators/SlidingPieChartAnimator;->pieChart:Lcom/squareup/charts/piechart/PieChart;

    const/high16 p1, 0x3f800000    # 1.0f

    .line 16
    iput p1, p0, Lcom/squareup/charts/piechart/animators/SlidingPieChartAnimator;->slideInProgress:F

    .line 19
    invoke-direct {p0}, Lcom/squareup/charts/piechart/animators/SlidingPieChartAnimator;->defaultAnimationDuration()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/squareup/charts/piechart/animators/SlidingPieChartAnimator;->duration:J

    .line 20
    new-instance p1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {p1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    iput-object p1, p0, Lcom/squareup/charts/piechart/animators/SlidingPieChartAnimator;->interpolator:Landroid/view/animation/AccelerateDecelerateInterpolator;

    return-void
.end method

.method public static final synthetic access$getPieChart$p(Lcom/squareup/charts/piechart/animators/SlidingPieChartAnimator;)Lcom/squareup/charts/piechart/PieChart;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/squareup/charts/piechart/animators/SlidingPieChartAnimator;->pieChart:Lcom/squareup/charts/piechart/PieChart;

    return-object p0
.end method

.method public static final synthetic access$getSlideInProgress$p(Lcom/squareup/charts/piechart/animators/SlidingPieChartAnimator;)F
    .locals 0

    .line 15
    iget p0, p0, Lcom/squareup/charts/piechart/animators/SlidingPieChartAnimator;->slideInProgress:F

    return p0
.end method

.method public static final synthetic access$setSlideInProgress$p(Lcom/squareup/charts/piechart/animators/SlidingPieChartAnimator;F)V
    .locals 0

    .line 15
    iput p1, p0, Lcom/squareup/charts/piechart/animators/SlidingPieChartAnimator;->slideInProgress:F

    return-void
.end method

.method private final defaultAnimationDuration()J
    .locals 2

    .line 43
    iget-object v0, p0, Lcom/squareup/charts/piechart/animators/SlidingPieChartAnimator;->pieChart:Lcom/squareup/charts/piechart/PieChart;

    invoke-virtual {v0}, Lcom/squareup/charts/piechart/PieChart;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "pieChart.context"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/charts/piechart/R$integer;->default_animation_duration:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method


# virtual methods
.method public final getDuration()J
    .locals 2

    .line 19
    iget-wide v0, p0, Lcom/squareup/charts/piechart/animators/SlidingPieChartAnimator;->duration:J

    return-wide v0
.end method

.method public final getInterpolator()Landroid/view/animation/AccelerateDecelerateInterpolator;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/charts/piechart/animators/SlidingPieChartAnimator;->interpolator:Landroid/view/animation/AccelerateDecelerateInterpolator;

    return-object v0
.end method

.method public progress()F
    .locals 1

    .line 22
    iget v0, p0, Lcom/squareup/charts/piechart/animators/SlidingPieChartAnimator;->slideInProgress:F

    return v0
.end method

.method public final setDuration(J)V
    .locals 0

    .line 19
    iput-wide p1, p0, Lcom/squareup/charts/piechart/animators/SlidingPieChartAnimator;->duration:J

    return-void
.end method

.method public final setInterpolator(Landroid/view/animation/AccelerateDecelerateInterpolator;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    iput-object p1, p0, Lcom/squareup/charts/piechart/animators/SlidingPieChartAnimator;->interpolator:Landroid/view/animation/AccelerateDecelerateInterpolator;

    return-void
.end method

.method public startAnimating()V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [F

    .line 25
    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 27
    new-instance v1, Lcom/squareup/charts/piechart/animators/SlidingPieChartAnimator$startAnimating$$inlined$apply$lambda$1;

    invoke-direct {v1, p0}, Lcom/squareup/charts/piechart/animators/SlidingPieChartAnimator$startAnimating$$inlined$apply$lambda$1;-><init>(Lcom/squareup/charts/piechart/animators/SlidingPieChartAnimator;)V

    check-cast v1, Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 31
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->getDuration()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 32
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->getInterpolator()Landroid/animation/TimeInterpolator;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 33
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 26
    check-cast v0, Landroid/animation/Animator;

    iput-object v0, p0, Lcom/squareup/charts/piechart/animators/SlidingPieChartAnimator;->slideInAnimator:Landroid/animation/Animator;

    return-void

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public stopAnimating()V
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/squareup/charts/piechart/animators/SlidingPieChartAnimator;->slideInAnimator:Landroid/animation/Animator;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    :cond_0
    return-void
.end method
