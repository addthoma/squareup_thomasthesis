.class public final Lcom/squareup/charts/piechart/renderers/FullPieChartRenderer;
.super Lcom/squareup/charts/piechart/renderers/PieChartRenderer;
.source "FullPieChartRenderer.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nFullPieChartRenderer.kt\nKotlin\n*S Kotlin\n*F\n+ 1 FullPieChartRenderer.kt\ncom/squareup/charts/piechart/renderers/FullPieChartRenderer\n*L\n1#1,63:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0007\n\u0002\u0008\u0007\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\n\u001a\u00020\u00042\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000cH\u0002J.\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\r2\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000c2\u0006\u0010\u0013\u001a\u00020\u0004H\u0016R$\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0004@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007\"\u0004\u0008\u0008\u0010\t\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/charts/piechart/renderers/FullPieChartRenderer;",
        "Lcom/squareup/charts/piechart/renderers/PieChartRenderer;",
        "()V",
        "value",
        "",
        "gap",
        "getGap",
        "()F",
        "setGap",
        "(F)V",
        "calculateTotalLength",
        "slices",
        "",
        "Lcom/squareup/charts/piechart/Slice;",
        "onDrawPieChart",
        "",
        "canvas",
        "Landroid/graphics/Canvas;",
        "backgroundSlice",
        "chartProgress",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private gap:F


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Lcom/squareup/charts/piechart/renderers/PieChartRenderer;-><init>()V

    return-void
.end method

.method private final calculateTotalLength(Ljava/util/List;)F
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/charts/piechart/Slice;",
            ">;)F"
        }
    .end annotation

    .line 56
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    int-to-float p1, p1

    iget v0, p0, Lcom/squareup/charts/piechart/renderers/FullPieChartRenderer;->gap:F

    mul-float p1, p1, v0

    const/high16 v0, 0x42c80000    # 100.0f

    add-float/2addr p1, v0

    return p1
.end method


# virtual methods
.method public final getGap()F
    .locals 1

    .line 18
    iget v0, p0, Lcom/squareup/charts/piechart/renderers/FullPieChartRenderer;->gap:F

    return v0
.end method

.method public onDrawPieChart(Landroid/graphics/Canvas;Lcom/squareup/charts/piechart/Slice;Ljava/util/List;F)V
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Canvas;",
            "Lcom/squareup/charts/piechart/Slice;",
            "Ljava/util/List<",
            "Lcom/squareup/charts/piechart/Slice;",
            ">;F)V"
        }
    .end annotation

    move-object/from16 v6, p0

    move-object/from16 v0, p2

    move-object/from16 v7, p3

    move/from16 v8, p4

    const-string v1, "canvas"

    move-object/from16 v9, p1

    invoke-static {v9, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "backgroundSlice"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "slices"

    invoke-static {v7, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/charts/piechart/renderers/FullPieChartRenderer;->getDrawableArea()Landroid/graphics/RectF;

    move-result-object v10

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/4 v3, 0x4

    const/4 v4, 0x0

    .line 39
    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/charts/piechart/renderers/RendererUtilsKt;->calculateAngle$default(Lcom/squareup/charts/piechart/Slice;FFILjava/lang/Object;)F

    move-result v4

    .line 40
    invoke-virtual {v6, v0}, Lcom/squareup/charts/piechart/renderers/FullPieChartRenderer;->slicePaint(Lcom/squareup/charts/piechart/Slice;)Landroid/graphics/Paint;

    move-result-object v5

    const/4 v3, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object v2, v10

    .line 35
    invoke-virtual/range {v0 .. v5}, Lcom/squareup/charts/piechart/renderers/FullPieChartRenderer;->drawSlice(Landroid/graphics/Canvas;Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 43
    invoke-direct {v6, v7}, Lcom/squareup/charts/piechart/renderers/FullPieChartRenderer;->calculateTotalLength(Ljava/util/List;)F

    move-result v11

    .line 45
    move-object v0, v7

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v12

    const/high16 v0, -0x3d4c0000    # -90.0f

    const/4 v1, 0x0

    const/4 v13, 0x0

    const/high16 v14, -0x3d4c0000    # -90.0f

    :goto_0
    if-ge v13, v12, :cond_0

    .line 46
    invoke-interface {v7, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/charts/piechart/Slice;

    const/high16 v1, 0x43b40000    # 360.0f

    .line 47
    iget v2, v6, Lcom/squareup/charts/piechart/renderers/FullPieChartRenderer;->gap:F

    const/4 v3, 0x2

    int-to-float v15, v3

    div-float/2addr v2, v15

    mul-float v2, v2, v8

    mul-float v2, v2, v1

    div-float v16, v2, v11

    .line 48
    invoke-static {v0, v8, v11}, Lcom/squareup/charts/piechart/renderers/RendererUtilsKt;->calculateAngle(Lcom/squareup/charts/piechart/Slice;FF)F

    move-result v17

    add-float v3, v14, v16

    .line 50
    invoke-virtual {v6, v0}, Lcom/squareup/charts/piechart/renderers/FullPieChartRenderer;->slicePaint(Lcom/squareup/charts/piechart/Slice;)Landroid/graphics/Paint;

    move-result-object v5

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object v2, v10

    move/from16 v4, v17

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/charts/piechart/renderers/FullPieChartRenderer;->drawSlice(Landroid/graphics/Canvas;Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    mul-float v16, v16, v15

    add-float v17, v17, v16

    add-float v14, v14, v17

    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final setGap(F)V
    .locals 2

    .line 20
    iget v0, p0, Lcom/squareup/charts/piechart/renderers/FullPieChartRenderer;->gap:F

    cmpg-float v0, v0, p1

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_1

    const/high16 v0, 0x40a00000    # 5.0f

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_2

    .line 22
    iput p1, p0, Lcom/squareup/charts/piechart/renderers/FullPieChartRenderer;->gap:F

    return-void

    .line 21
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Gap ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string p1, ") must be between 0%-5%."

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
