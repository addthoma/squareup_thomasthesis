.class public final Lcom/squareup/ProductionServerModule_ProvideWeeblyApiServerFactory;
.super Ljava/lang/Object;
.source "ProductionServerModule_ProvideWeeblyApiServerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ProductionServerModule_ProvideWeeblyApiServerFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/http/Server;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/ProductionServerModule_ProvideWeeblyApiServerFactory;
    .locals 1

    .line 23
    invoke-static {}, Lcom/squareup/ProductionServerModule_ProvideWeeblyApiServerFactory$InstanceHolder;->access$000()Lcom/squareup/ProductionServerModule_ProvideWeeblyApiServerFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideWeeblyApiServer()Lcom/squareup/http/Server;
    .locals 2

    .line 27
    invoke-static {}, Lcom/squareup/ProductionServerModule;->provideWeeblyApiServer()Lcom/squareup/http/Server;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/http/Server;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/http/Server;
    .locals 1

    .line 19
    invoke-static {}, Lcom/squareup/ProductionServerModule_ProvideWeeblyApiServerFactory;->provideWeeblyApiServer()Lcom/squareup/http/Server;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ProductionServerModule_ProvideWeeblyApiServerFactory;->get()Lcom/squareup/http/Server;

    move-result-object v0

    return-object v0
.end method
