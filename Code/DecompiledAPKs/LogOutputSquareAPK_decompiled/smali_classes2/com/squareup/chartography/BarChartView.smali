.class public final Lcom/squareup/chartography/BarChartView;
.super Lcom/squareup/chartography/ChartView;
.source "BarChartView.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBarChartView.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BarChartView.kt\ncom/squareup/chartography/BarChartView\n*L\n1#1,78:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0007\n\u0002\u0008\u0008\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001B%\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J \u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00072\u0006\u0010\u0019\u001a\u00020\u000cH\u0002J\u0010\u0010\u001a\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017H\u0014R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u000b\u001a\u00020\u000cX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\r\u0010\u000e\"\u0004\u0008\u000f\u0010\u0010R\u001a\u0010\u0011\u001a\u00020\u000cX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0012\u0010\u000e\"\u0004\u0008\u0013\u0010\u0010\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/chartography/BarChartView;",
        "Lcom/squareup/chartography/ChartView;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "defStyleAttr",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "barPaint",
        "Landroid/graphics/Paint;",
        "barSpacing",
        "",
        "getBarSpacing",
        "()F",
        "setBarSpacing",
        "(F)V",
        "stepSpacing",
        "getStepSpacing",
        "setStepSpacing",
        "drawBars",
        "",
        "canvas",
        "Landroid/graphics/Canvas;",
        "step",
        "barWidth",
        "onDrawData",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final barPaint:Landroid/graphics/Paint;

.field private barSpacing:F

.field private stepSpacing:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/chartography/BarChartView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/chartography/BarChartView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/chartography/ChartView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 24
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iput-object v0, p0, Lcom/squareup/chartography/BarChartView;->barPaint:Landroid/graphics/Paint;

    .line 27
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object p1

    .line 29
    sget-object v0, Lcom/squareup/chartography/R$styleable;->BarChartView:[I

    const/4 v1, 0x0

    .line 27
    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 33
    :try_start_0
    sget p2, Lcom/squareup/chartography/R$styleable;->BarChartView_barSpacing:I

    invoke-virtual {p0}, Lcom/squareup/chartography/BarChartView;->getPixel$public_release()Lcom/squareup/chartography/util/Pixel;

    move-result-object p3

    const/high16 v0, 0x40000000    # 2.0f

    invoke-virtual {p3, v0}, Lcom/squareup/chartography/util/Pixel;->dp(F)F

    move-result p3

    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result p2

    iput p2, p0, Lcom/squareup/chartography/BarChartView;->barSpacing:F

    .line 34
    sget p2, Lcom/squareup/chartography/R$styleable;->BarChartView_stepSpacing:I

    invoke-virtual {p0}, Lcom/squareup/chartography/BarChartView;->getPixel$public_release()Lcom/squareup/chartography/util/Pixel;

    move-result-object p3

    const/high16 v0, 0x41000000    # 8.0f

    invoke-virtual {p3, v0}, Lcom/squareup/chartography/util/Pixel;->dp(F)F

    move-result p3

    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result p2

    iput p2, p0, Lcom/squareup/chartography/BarChartView;->stepSpacing:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 36
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void

    :catchall_0
    move-exception p2

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    throw p2
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    .line 15
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 16
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/chartography/BarChartView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private final drawBars(Landroid/graphics/Canvas;IF)V
    .locals 10

    int-to-float v0, p2

    .line 60
    invoke-virtual {p0}, Lcom/squareup/chartography/BarChartView;->getDataDrawRegion()Lcom/squareup/chartography/ChartView$DataDrawRegion;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/chartography/ChartView$DataDrawRegion;->getXStep()F

    move-result v1

    mul-float v0, v0, v1

    invoke-virtual {p0}, Lcom/squareup/chartography/BarChartView;->getDataDrawRegion()Lcom/squareup/chartography/ChartView$DataDrawRegion;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/chartography/ChartView$DataDrawRegion;->getX()F

    move-result v1

    add-float/2addr v0, v1

    iget v1, p0, Lcom/squareup/chartography/BarChartView;->stepSpacing:F

    const/4 v2, 0x2

    int-to-float v2, v2

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    .line 61
    invoke-virtual {p0}, Lcom/squareup/chartography/BarChartView;->getDataDrawRegion()Lcom/squareup/chartography/ChartView$DataDrawRegion;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/chartography/ChartView$DataDrawRegion;->getY()F

    move-result v1

    .line 63
    invoke-virtual {p0}, Lcom/squareup/chartography/BarChartView;->getAdapter()Lcom/squareup/chartography/ChartAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/chartography/ChartAdapter;->getNumDimensions()I

    move-result v8

    const/4 v2, 0x0

    move v9, v0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v8, :cond_1

    .line 64
    invoke-virtual {p0}, Lcom/squareup/chartography/BarChartView;->getAdapter()Lcom/squareup/chartography/ChartAdapter;

    move-result-object v2

    invoke-virtual {v2, p2, v0}, Lcom/squareup/chartography/ChartAdapter;->rangePercentage(II)D

    move-result-wide v2

    double-to-float v2, v2

    .line 66
    invoke-virtual {p0}, Lcom/squareup/chartography/BarChartView;->getDataDrawRegion()Lcom/squareup/chartography/ChartView$DataDrawRegion;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/chartography/ChartView$DataDrawRegion;->getHeight()F

    move-result v3

    mul-float v3, v3, v2

    invoke-virtual {p0}, Lcom/squareup/chartography/BarChartView;->getRangeScalingFactor()F

    move-result v2

    mul-float v3, v3, v2

    const/4 v2, 0x0

    invoke-static {v3, v2}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 68
    iget-object v3, p0, Lcom/squareup/chartography/BarChartView;->barPaint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/squareup/chartography/BarChartView;->getSelectedDomain()I

    move-result v4

    if-ne p2, v4, :cond_0

    .line 69
    invoke-virtual {p0}, Lcom/squareup/chartography/BarChartView;->getSelectedColorWheel$public_release()Lcom/squareup/chartography/util/ColorWheel;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/squareup/chartography/util/ColorWheel;->color(I)I

    move-result v4

    goto :goto_1

    .line 71
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/chartography/BarChartView;->getColorWheel$public_release()Lcom/squareup/chartography/util/ColorWheel;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/squareup/chartography/util/ColorWheel;->color(I)I

    move-result v4

    .line 68
    :goto_1
    invoke-static {v3, v4}, Lcom/squareup/chartography/extensions/PaintKt;->setColorResource-9Ee6Eao(Landroid/graphics/Paint;I)V

    sub-float v4, v1, v2

    add-float v5, v9, p3

    .line 73
    iget-object v7, p0, Lcom/squareup/chartography/BarChartView;->barPaint:Landroid/graphics/Paint;

    move-object v2, p1

    move v3, v9

    move v6, v1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 74
    iget v2, p0, Lcom/squareup/chartography/BarChartView;->barSpacing:F

    add-float/2addr v2, p3

    add-float/2addr v9, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public final getBarSpacing()F
    .locals 1

    .line 20
    iget v0, p0, Lcom/squareup/chartography/BarChartView;->barSpacing:F

    return v0
.end method

.method public final getStepSpacing()F
    .locals 1

    .line 22
    iget v0, p0, Lcom/squareup/chartography/BarChartView;->stepSpacing:F

    return v0
.end method

.method protected onDrawData(Landroid/graphics/Canvas;)V
    .locals 4

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    invoke-virtual {p0}, Lcom/squareup/chartography/BarChartView;->getDataDrawRegion()Lcom/squareup/chartography/ChartView$DataDrawRegion;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/chartography/ChartView$DataDrawRegion;->getXStep()F

    move-result v0

    iget v1, p0, Lcom/squareup/chartography/BarChartView;->barSpacing:F

    invoke-virtual {p0}, Lcom/squareup/chartography/BarChartView;->getAdapter()Lcom/squareup/chartography/ChartAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/chartography/ChartAdapter;->getNumDimensions()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    int-to-float v2, v2

    mul-float v1, v1, v2

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/squareup/chartography/BarChartView;->stepSpacing:F

    sub-float/2addr v0, v1

    .line 46
    invoke-virtual {p0}, Lcom/squareup/chartography/BarChartView;->getAdapter()Lcom/squareup/chartography/ChartAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/chartography/ChartAdapter;->getNumDimensions()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 47
    invoke-virtual {p0}, Lcom/squareup/chartography/BarChartView;->getDataDrawRegion()Lcom/squareup/chartography/ChartView$DataDrawRegion;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/chartography/ChartView$DataDrawRegion;->getX()F

    .line 49
    invoke-virtual {p0}, Lcom/squareup/chartography/BarChartView;->getAdapter()Lcom/squareup/chartography/ChartAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/chartography/ChartAdapter;->getDomainNumSteps()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    .line 50
    invoke-direct {p0, p1, v2, v0}, Lcom/squareup/chartography/BarChartView;->drawBars(Landroid/graphics/Canvas;IF)V

    .line 51
    invoke-virtual {p0}, Lcom/squareup/chartography/BarChartView;->getDataDrawRegion()Lcom/squareup/chartography/ChartView$DataDrawRegion;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/chartography/ChartView$DataDrawRegion;->getXStep()F

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final setBarSpacing(F)V
    .locals 0

    .line 20
    iput p1, p0, Lcom/squareup/chartography/BarChartView;->barSpacing:F

    return-void
.end method

.method public final setStepSpacing(F)V
    .locals 0

    .line 22
    iput p1, p0, Lcom/squareup/chartography/BarChartView;->stepSpacing:F

    return-void
.end method
