.class public final Lcom/squareup/chartography/util/ColorWheel;
.super Ljava/lang/Object;
.source "ColorWheel.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nColorWheel.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ColorWheel.kt\ncom/squareup/chartography/util/ColorWheel\n*L\n1#1,28:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0006\u0008\u0000\u0018\u00002\u00020\u0001B\u0016\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0005J\u0016\u0010\u000b\u001a\u00020\u00042\u0006\u0010\u000c\u001a\u00020\u0008\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\rR\u0019\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004\u00f8\u0001\u0000\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u0011\u0010\u0007\u001a\u00020\u00088F\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\n\u0082\u0002\u0004\n\u0002\u0008\u0019\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/chartography/util/ColorWheel;",
        "",
        "colors",
        "",
        "Lcom/squareup/chartography/util/ColorResource;",
        "([Lcom/squareup/chartography/util/ColorResource;)V",
        "[Lcom/squareup/chartography/util/ColorResource;",
        "size",
        "",
        "getSize",
        "()I",
        "color",
        "idx",
        "(I)I",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final colors:[Lcom/squareup/chartography/util/ColorResource;


# direct methods
.method public constructor <init>([Lcom/squareup/chartography/util/ColorResource;)V
    .locals 1

    const-string v0, "colors"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/chartography/util/ColorWheel;->colors:[Lcom/squareup/chartography/util/ColorResource;

    .line 11
    iget-object p1, p0, Lcom/squareup/chartography/util/ColorWheel;->colors:[Lcom/squareup/chartography/util/ColorResource;

    array-length p1, p1

    const/4 v0, 0x1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    xor-int/2addr p1, v0

    if-eqz p1, :cond_1

    return-void

    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "color array must not be empty"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method


# virtual methods
.method public final color(I)I
    .locals 2

    .line 25
    iget-object v0, p0, Lcom/squareup/chartography/util/ColorWheel;->colors:[Lcom/squareup/chartography/util/ColorResource;

    invoke-virtual {p0}, Lcom/squareup/chartography/util/ColorWheel;->getSize()I

    move-result v1

    rem-int/2addr p1, v1

    aget-object p1, v0, p1

    invoke-virtual {p1}, Lcom/squareup/chartography/util/ColorResource;->unbox-impl()I

    move-result p1

    return p1
.end method

.method public final getSize()I
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/chartography/util/ColorWheel;->colors:[Lcom/squareup/chartography/util/ColorResource;

    array-length v0, v0

    return v0
.end method
