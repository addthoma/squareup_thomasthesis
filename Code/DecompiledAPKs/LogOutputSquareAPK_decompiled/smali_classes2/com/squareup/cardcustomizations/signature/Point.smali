.class public Lcom/squareup/cardcustomizations/signature/Point;
.super Ljava/lang/Object;
.source "Point.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardcustomizations/signature/Point$Timestamped;
    }
.end annotation


# instance fields
.field public final x:F

.field public final y:F


# direct methods
.method constructor <init>(FF)V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput p1, p0, Lcom/squareup/cardcustomizations/signature/Point;->x:F

    .line 11
    iput p2, p0, Lcom/squareup/cardcustomizations/signature/Point;->y:F

    return-void
.end method


# virtual methods
.method public distanceTo(Lcom/squareup/cardcustomizations/signature/Point;)F
    .locals 2

    .line 15
    iget v0, p0, Lcom/squareup/cardcustomizations/signature/Point;->x:F

    iget v1, p1, Lcom/squareup/cardcustomizations/signature/Point;->x:F

    sub-float/2addr v0, v1

    .line 16
    iget v1, p0, Lcom/squareup/cardcustomizations/signature/Point;->y:F

    iget p1, p1, Lcom/squareup/cardcustomizations/signature/Point;->y:F

    sub-float/2addr v1, p1

    mul-float v0, v0, v0

    mul-float v1, v1, v1

    add-float/2addr v0, v1

    float-to-double v0, v0

    .line 17
    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float p1, v0

    return p1
.end method

.method public halfWayTo(Lcom/squareup/cardcustomizations/signature/Point;)Lcom/squareup/cardcustomizations/signature/Point;
    .locals 4

    .line 36
    iget v0, p1, Lcom/squareup/cardcustomizations/signature/Point;->x:F

    iget v1, p0, Lcom/squareup/cardcustomizations/signature/Point;->x:F

    sub-float/2addr v0, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v0, v2

    .line 37
    iget p1, p1, Lcom/squareup/cardcustomizations/signature/Point;->y:F

    iget v3, p0, Lcom/squareup/cardcustomizations/signature/Point;->y:F

    sub-float/2addr p1, v3

    div-float/2addr p1, v2

    .line 38
    new-instance v2, Lcom/squareup/cardcustomizations/signature/Point;

    add-float/2addr v1, v0

    add-float/2addr v3, p1

    invoke-direct {v2, v1, v3}, Lcom/squareup/cardcustomizations/signature/Point;-><init>(FF)V

    return-object v2
.end method

.method public oneThirdTo(Lcom/squareup/cardcustomizations/signature/Point;)Lcom/squareup/cardcustomizations/signature/Point;
    .locals 4

    .line 22
    iget v0, p1, Lcom/squareup/cardcustomizations/signature/Point;->x:F

    iget v1, p0, Lcom/squareup/cardcustomizations/signature/Point;->x:F

    sub-float/2addr v0, v1

    const/high16 v2, 0x40400000    # 3.0f

    div-float/2addr v0, v2

    .line 23
    iget p1, p1, Lcom/squareup/cardcustomizations/signature/Point;->y:F

    iget v3, p0, Lcom/squareup/cardcustomizations/signature/Point;->y:F

    sub-float/2addr p1, v3

    div-float/2addr p1, v2

    .line 24
    new-instance v2, Lcom/squareup/cardcustomizations/signature/Point;

    add-float/2addr v1, v0

    add-float/2addr v3, p1

    invoke-direct {v2, v1, v3}, Lcom/squareup/cardcustomizations/signature/Point;-><init>(FF)V

    return-object v2
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 42
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/cardcustomizations/signature/Point;->x:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/cardcustomizations/signature/Point;->y:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public twoThirdsTo(Lcom/squareup/cardcustomizations/signature/Point;)Lcom/squareup/cardcustomizations/signature/Point;
    .locals 5

    .line 29
    iget v0, p1, Lcom/squareup/cardcustomizations/signature/Point;->x:F

    iget v1, p0, Lcom/squareup/cardcustomizations/signature/Point;->x:F

    sub-float/2addr v0, v1

    const/high16 v2, 0x40000000    # 2.0f

    mul-float v0, v0, v2

    const/high16 v3, 0x40400000    # 3.0f

    div-float/2addr v0, v3

    .line 30
    iget p1, p1, Lcom/squareup/cardcustomizations/signature/Point;->y:F

    iget v4, p0, Lcom/squareup/cardcustomizations/signature/Point;->y:F

    sub-float/2addr p1, v4

    mul-float p1, p1, v2

    div-float/2addr p1, v3

    .line 31
    new-instance v2, Lcom/squareup/cardcustomizations/signature/Point;

    add-float/2addr v1, v0

    add-float/2addr v4, p1

    invoke-direct {v2, v1, v4}, Lcom/squareup/cardcustomizations/signature/Point;-><init>(FF)V

    return-object v2
.end method
