.class public final Lcom/squareup/cardcustomizations/signature/PhysicalCardKt;
.super Ljava/lang/Object;
.source "PhysicalCard.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPhysicalCard.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PhysicalCard.kt\ncom/squareup/cardcustomizations/signature/PhysicalCardKt\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 _Arrays.kt\nkotlin/collections/ArraysKt___ArraysKt\n+ 4 Point.kt\nandroidx/core/graphics/PointKt\n*L\n1#1,343:1\n1574#2:344\n1574#2,2:345\n1575#2:347\n1574#2,2:348\n10216#3,3:350\n111#4,3:353\n*E\n*S KotlinDebug\n*F\n+ 1 PhysicalCard.kt\ncom/squareup/cardcustomizations/signature/PhysicalCardKt\n*L\n41#1:344\n41#1,2:345\n41#1:347\n52#1,2:348\n150#1,3:350\n252#1,3:353\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000X\n\u0000\n\u0002\u0010\u0007\n\u0002\u0008\u000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u001a\u001a\u0010\u000c\u001a\u00020\r*\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0010\u001a\u00020\u0011\u001a-\u0010\u0012\u001a\u00020\u0013*\u00020\u00142\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u00012\u0008\u0010\u0016\u001a\u0004\u0018\u00010\u00012\u0008\u0008\u0003\u0010\u0017\u001a\u00020\u0018\u00a2\u0006\u0002\u0010\u0019\u001a2\u0010\u001a\u001a\u00020\u001b*\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001d2\u0008\u0008\u0003\u0010\u001e\u001a\u00020\u00182\u0008\u0008\u0003\u0010\u0017\u001a\u00020\u00182\n\u0008\u0002\u0010\u001f\u001a\u0004\u0018\u00010 \u001a2\u0010!\u001a\u00020\"*\u00020\u001b2\u0006\u0010#\u001a\u00020\u00112\u0006\u0010\u0010\u001a\u00020$2\u0006\u0010%\u001a\u00020&2\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\'\u001a\u00020\u0001\u001a\u0014\u0010(\u001a\u00020\u0018*\u00020\u00142\u0008\u0008\u0003\u0010\u0017\u001a\u00020\u0018\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0005\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0006\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0007\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0008\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\t\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\n\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u000b\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006)"
    }
    d2 = {
        "CARD_ASSET_HEIGHT",
        "",
        "CARD_ASSET_PADDING",
        "CARD_HEIGHT_MM",
        "CARD_PADDING_MM",
        "CARD_SIGNATURE_HEIGHT",
        "CARD_SIGNATURE_WIDTH",
        "CARD_STROKE_WIDTH_MM",
        "CARD_WIDTH_MM",
        "SIGNATURE_HEIGHT_MM",
        "SIGNATURE_WIDTH_MM",
        "STROKE_WIDTH",
        "animateToCardPosition",
        "Landroid/animation/ValueAnimator;",
        "Landroid/view/View;",
        "container",
        "bounds",
        "Landroid/graphics/RectF;",
        "checkInkLevel",
        "Lcom/squareup/cardcustomizations/signature/InkLevel;",
        "Landroid/graphics/Bitmap;",
        "minInkCoverage",
        "maxInkCoverage",
        "bgColor",
        "",
        "(Landroid/graphics/Bitmap;Ljava/lang/Float;Ljava/lang/Float;I)Lcom/squareup/cardcustomizations/signature/InkLevel;",
        "createScaledSignature",
        "Lcom/squareup/cardcustomizations/signature/Signature;",
        "signatureTransformation",
        "Lcom/squareup/cardcustomizations/signature/SignatureTransformation;",
        "signatureColor",
        "extraPainterProvider",
        "Lcom/squareup/cardcustomizations/signature/Signature$PainterProvider;",
        "findFreeRegion",
        "Landroid/graphics/Point;",
        "region",
        "Landroid/graphics/Rect;",
        "stampView",
        "Lcom/squareup/cardcustomizations/stampview/StampView;",
        "strokeWidth",
        "getInkLevel",
        "card-customization_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0xd
    }
.end annotation


# static fields
.field public static final CARD_ASSET_HEIGHT:F = 1600.0f

.field public static final CARD_ASSET_PADDING:F = 160.0f

.field public static final CARD_HEIGHT_MM:F = 53.98f

.field public static final CARD_PADDING_MM:F = 5.0f

.field private static final CARD_SIGNATURE_HEIGHT:F = 354.0f

.field private static final CARD_SIGNATURE_WIDTH:F = 1062.0f

.field public static final CARD_STROKE_WIDTH_MM:F = 0.45f

.field public static final CARD_WIDTH_MM:F = 85.6f

.field public static final SIGNATURE_HEIGHT_MM:F = 15.0f

.field public static final SIGNATURE_WIDTH_MM:F = 45.0f

.field public static final STROKE_WIDTH:F = 10.62f


# direct methods
.method public static final animateToCardPosition(Landroid/view/View;Landroid/view/View;Landroid/graphics/RectF;)Landroid/animation/ValueAnimator;
    .locals 14

    move-object v7, p0

    move-object/from16 v3, p2

    const-string v0, "receiver$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "container"

    move-object v1, p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bounds"

    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    int-to-float v0, v0

    .line 67
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    int-to-float v2, v2

    .line 68
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v1

    int-to-float v1, v1

    const v4, 0x3dcccccd    # 0.1f

    mul-float v4, v4, v0

    const/4 v5, 0x2

    int-to-float v6, v5

    mul-float v4, v4, v6

    sub-float v4, v0, v4

    const v8, 0x42ab3333    # 85.6f

    mul-float v8, v8, v4

    const v9, 0x4257eb85    # 53.98f

    div-float/2addr v8, v9

    const/high16 v10, 0x40a00000    # 5.0f

    mul-float v10, v10, v4

    div-float/2addr v10, v9

    .line 79
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->height()F

    move-result v11

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->width()F

    move-result v12

    div-float/2addr v11, v12

    const v12, 0x3eaaaaab

    cmpl-float v11, v11, v12

    if-lez v11, :cond_0

    const/high16 v11, 0x41700000    # 15.0f

    mul-float v11, v11, v4

    div-float/2addr v11, v9

    .line 82
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->width()F

    move-result v9

    mul-float v9, v9, v11

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->height()F

    move-result v12

    div-float/2addr v9, v12

    move v13, v11

    move v11, v9

    move v9, v13

    goto :goto_0

    :cond_0
    const/high16 v11, 0x42340000    # 45.0f

    mul-float v11, v11, v4

    div-float/2addr v11, v9

    .line 86
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->height()F

    move-result v9

    mul-float v9, v9, v11

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->width()F

    move-result v12

    div-float/2addr v9, v12

    :goto_0
    div-float/2addr v0, v6

    add-float/2addr v1, v0

    div-float/2addr v4, v6

    add-float/2addr v1, v4

    sub-float/2addr v1, v10

    sub-float v12, v1, v9

    div-float/2addr v2, v6

    div-float/2addr v8, v6

    add-float/2addr v2, v8

    sub-float/2addr v2, v10

    sub-float v6, v2, v11

    .line 92
    iget v0, v3, Landroid/graphics/RectF;->left:F

    invoke-virtual {p0, v0}, Landroid/view/View;->setPivotX(F)V

    .line 93
    iget v0, v3, Landroid/graphics/RectF;->top:F

    invoke-virtual {p0, v0}, Landroid/view/View;->setPivotY(F)V

    new-array v0, v5, [I

    .line 95
    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v8

    const-string v0, "animator"

    .line 96
    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Landroid/view/animation/OvershootInterpolator;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {v0, v1}, Landroid/view/animation/OvershootInterpolator;-><init>(F)V

    check-cast v0, Landroid/animation/TimeInterpolator;

    invoke-virtual {v8, v0}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 97
    new-instance v10, Lcom/squareup/cardcustomizations/signature/PhysicalCardKt$animateToCardPosition$1;

    move-object v0, v10

    move-object v1, p0

    move v2, v11

    move-object/from16 v3, p2

    move v4, v9

    move v5, v6

    move v6, v12

    invoke-direct/range {v0 .. v6}, Lcom/squareup/cardcustomizations/signature/PhysicalCardKt$animateToCardPosition$1;-><init>(Landroid/view/View;FLandroid/graphics/RectF;FFF)V

    check-cast v10, Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {v8, v10}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 103
    new-instance v0, Lcom/squareup/cardcustomizations/signature/PhysicalCardKt$animateToCardPosition$2;

    invoke-direct {v0, p0}, Lcom/squareup/cardcustomizations/signature/PhysicalCardKt$animateToCardPosition$2;-><init>(Landroid/view/View;)V

    check-cast v0, Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v8, v0}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    return-object v8

    nop

    :array_0
    .array-data 4
        0x0
        0x1
    .end array-data
.end method

.method public static final checkInkLevel(Landroid/graphics/Bitmap;Ljava/lang/Float;Ljava/lang/Float;I)Lcom/squareup/cardcustomizations/signature/InkLevel;
    .locals 3

    const-string v0, "receiver$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 127
    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    cmpg-float v1, v1, v0

    if-gez v1, :cond_1

    :cond_0
    if-eqz p2, :cond_6

    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v1

    cmpg-float v1, v1, v0

    if-gtz v1, :cond_1

    goto :goto_2

    .line 130
    :cond_1
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    mul-int v1, v1, v2

    if-eqz p1, :cond_2

    .line 131
    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result p1

    int-to-float v2, v1

    mul-float p1, p1, v2

    float-to-int p1, p1

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    :goto_0
    if-eqz p2, :cond_3

    .line 132
    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    cmpl-float v0, v2, v0

    if-lez v0, :cond_3

    .line 133
    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result p2

    int-to-float v0, v1

    mul-float p2, p2, v0

    float-to-int v1, p2

    .line 137
    :cond_3
    invoke-static {p0, p3}, Lcom/squareup/cardcustomizations/signature/PhysicalCardKt;->getInkLevel(Landroid/graphics/Bitmap;I)I

    move-result p0

    if-le p0, v1, :cond_4

    .line 139
    sget-object p0, Lcom/squareup/cardcustomizations/signature/InkLevel;->TOO_MUCH:Lcom/squareup/cardcustomizations/signature/InkLevel;

    return-object p0

    :cond_4
    if-lt p0, p1, :cond_5

    .line 142
    sget-object p0, Lcom/squareup/cardcustomizations/signature/InkLevel;->JUST_RIGHT:Lcom/squareup/cardcustomizations/signature/InkLevel;

    goto :goto_1

    :cond_5
    sget-object p0, Lcom/squareup/cardcustomizations/signature/InkLevel;->TOO_LITTLE:Lcom/squareup/cardcustomizations/signature/InkLevel;

    :goto_1
    return-object p0

    .line 128
    :cond_6
    :goto_2
    sget-object p0, Lcom/squareup/cardcustomizations/signature/InkLevel;->JUST_RIGHT:Lcom/squareup/cardcustomizations/signature/InkLevel;

    return-object p0
.end method

.method public static synthetic checkInkLevel$default(Landroid/graphics/Bitmap;Ljava/lang/Float;Ljava/lang/Float;IILjava/lang/Object;)Lcom/squareup/cardcustomizations/signature/InkLevel;
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, -0x1

    .line 124
    :cond_0
    invoke-static {p0, p1, p2, p3}, Lcom/squareup/cardcustomizations/signature/PhysicalCardKt;->checkInkLevel(Landroid/graphics/Bitmap;Ljava/lang/Float;Ljava/lang/Float;I)Lcom/squareup/cardcustomizations/signature/InkLevel;

    move-result-object p0

    return-object p0
.end method

.method public static final createScaledSignature(Lcom/squareup/cardcustomizations/signature/Signature;Lcom/squareup/cardcustomizations/signature/SignatureTransformation;IILcom/squareup/cardcustomizations/signature/Signature$PainterProvider;)Lcom/squareup/cardcustomizations/signature/Signature;
    .locals 8

    const-string v0, "receiver$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "signatureTransformation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const v0, 0x4484c000    # 1062.0f

    float-to-int v2, v0

    const/high16 v0, 0x43b10000    # 354.0f

    float-to-int v3, v0

    .line 33
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 34
    new-instance v7, Landroid/graphics/Canvas;

    invoke-direct {v7, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 35
    invoke-virtual {v7, p3}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 37
    new-instance p3, Lcom/squareup/cardcustomizations/signature/Signature;

    if-eqz p4, :cond_0

    goto :goto_0

    .line 38
    :cond_0
    iget-object p4, p0, Lcom/squareup/cardcustomizations/signature/Signature;->painterProvider:Lcom/squareup/cardcustomizations/signature/Signature$PainterProvider;

    :goto_0
    move-object v6, p4

    const v4, 0x4129eb85    # 10.62f

    move-object v1, p3

    move v5, p2

    .line 37
    invoke-direct/range {v1 .. v6}, Lcom/squareup/cardcustomizations/signature/Signature;-><init>(IIFILcom/squareup/cardcustomizations/signature/Signature$PainterProvider;)V

    const/4 p2, 0x2

    new-array p2, p2, [F

    .line 39
    fill-array-data p2, :array_0

    .line 40
    invoke-virtual {p3, v0}, Lcom/squareup/cardcustomizations/signature/Signature;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 41
    new-instance p4, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/squareup/cardcustomizations/signature/Signature;->glyphs()Ljava/util/List;

    move-result-object p0

    check-cast p0, Ljava/util/Collection;

    invoke-direct {p4, p0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    check-cast p4, Ljava/lang/Iterable;

    .line 344
    invoke-interface {p4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result p4

    if-eqz p4, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Lcom/squareup/cardcustomizations/signature/Signature$Glyph;

    .line 42
    invoke-virtual {p3}, Lcom/squareup/cardcustomizations/signature/Signature;->startGlyph()V

    const-string v0, "glyph"

    .line 43
    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p4, Ljava/lang/Iterable;

    .line 345
    invoke-interface {p4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p4

    :goto_2
    invoke-interface {p4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardcustomizations/signature/Point$Timestamped;

    .line 44
    iget v1, v0, Lcom/squareup/cardcustomizations/signature/Point$Timestamped;->x:F

    const/4 v2, 0x0

    aput v1, p2, v2

    .line 45
    iget v1, v0, Lcom/squareup/cardcustomizations/signature/Point$Timestamped;->y:F

    const/4 v3, 0x1

    aput v1, p2, v3

    .line 46
    invoke-virtual {p1}, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->getSignatureMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 47
    aget v1, p2, v2

    aget v2, p2, v3

    iget-wide v3, v0, Lcom/squareup/cardcustomizations/signature/Point$Timestamped;->time:J

    invoke-virtual {p3, v1, v2, v3, v4}, Lcom/squareup/cardcustomizations/signature/Signature;->extendGlyph(FFJ)V

    goto :goto_2

    .line 49
    :cond_1
    invoke-virtual {p3}, Lcom/squareup/cardcustomizations/signature/Signature;->finishGlyph()V

    goto :goto_1

    .line 52
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->getStamps()Ljava/util/Collection;

    move-result-object p0

    check-cast p0, Ljava/lang/Iterable;

    .line 348
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_3
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_3

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;

    .line 53
    new-instance p4, Landroid/graphics/Matrix;

    invoke-virtual {p2}, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->getTransform()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-direct {p4, v0}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    .line 54
    invoke-virtual {p1}, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->getStampMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {p4, v0}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 55
    invoke-virtual {p2}, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->getRenderedStamp()Lcom/squareup/cardcustomizations/stampview/Stamp;

    move-result-object p2

    iget-object v0, p3, Lcom/squareup/cardcustomizations/signature/Signature;->bitmapPaint:Landroid/graphics/Paint;

    const-string v1, "newSignature.bitmapPaint"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2, v7, v0, p4}, Lcom/squareup/cardcustomizations/stampview/Stamp;->draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/Matrix;)V

    goto :goto_3

    :cond_3
    return-object p3

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method public static synthetic createScaledSignature$default(Lcom/squareup/cardcustomizations/signature/Signature;Lcom/squareup/cardcustomizations/signature/SignatureTransformation;IILcom/squareup/cardcustomizations/signature/Signature$PainterProvider;ILjava/lang/Object;)Lcom/squareup/cardcustomizations/signature/Signature;
    .locals 0

    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_0

    const/high16 p2, -0x1000000

    :cond_0
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_1

    const/4 p3, -0x1

    :cond_1
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_2

    const/4 p4, 0x0

    .line 31
    check-cast p4, Lcom/squareup/cardcustomizations/signature/Signature$PainterProvider;

    :cond_2
    invoke-static {p0, p1, p2, p3, p4}, Lcom/squareup/cardcustomizations/signature/PhysicalCardKt;->createScaledSignature(Lcom/squareup/cardcustomizations/signature/Signature;Lcom/squareup/cardcustomizations/signature/SignatureTransformation;IILcom/squareup/cardcustomizations/signature/Signature$PainterProvider;)Lcom/squareup/cardcustomizations/signature/Signature;

    move-result-object p0

    return-object p0
.end method

.method public static final findFreeRegion(Lcom/squareup/cardcustomizations/signature/Signature;Landroid/graphics/RectF;Landroid/graphics/Rect;Lcom/squareup/cardcustomizations/stampview/StampView;Lcom/squareup/cardcustomizations/signature/SignatureTransformation;F)Landroid/graphics/Point;
    .locals 25

    move-object/from16 v0, p2

    const-string v1, "receiver$0"

    move-object/from16 v2, p0

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "region"

    move-object/from16 v3, p1

    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "bounds"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "stampView"

    move-object/from16 v4, p3

    invoke-static {v4, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "signatureTransformation"

    move-object/from16 v5, p4

    invoke-static {v5, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 179
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Rect;->width()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    new-array v1, v1, [I

    .line 183
    new-instance v6, Ljava/util/ArrayDeque;

    invoke-direct {v6}, Ljava/util/ArrayDeque;-><init>()V

    .line 184
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/RectF;->width()F

    move-result v7

    float-to-int v7, v7

    .line 185
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/RectF;->height()F

    move-result v3

    float-to-int v3, v3

    .line 186
    new-instance v8, Landroid/graphics/Point;

    iget v9, v0, Landroid/graphics/Rect;->left:I

    iget v10, v0, Landroid/graphics/Rect;->right:I

    add-int/2addr v9, v10

    sub-int/2addr v9, v7

    div-int/lit8 v9, v9, 0x2

    .line 187
    iget v10, v0, Landroid/graphics/Rect;->top:I

    iget v11, v0, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v10, v11

    sub-int/2addr v10, v3

    div-int/lit8 v10, v10, 0x2

    .line 186
    invoke-direct {v8, v9, v10}, Landroid/graphics/Point;-><init>(II)V

    .line 188
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Rect;->width()I

    move-result v9

    new-array v9, v9, [I

    .line 189
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Rect;->width()I

    move-result v10

    new-array v15, v10, [I

    const/4 v10, 0x0

    .line 191
    check-cast v10, Landroid/graphics/Point;

    .line 196
    invoke-virtual/range {p3 .. p3}, Lcom/squareup/cardcustomizations/stampview/StampView;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v4

    .line 198
    iget v11, v0, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v11, v11, -0x1

    iget v12, v0, Landroid/graphics/Rect;->top:I

    invoke-static {v11, v12}, Lkotlin/ranges/RangesKt;->downTo(II)Lkotlin/ranges/IntProgression;

    move-result-object v11

    const/4 v14, 0x4

    int-to-float v12, v14

    div-float v12, p5, v12

    float-to-int v12, v12

    invoke-static {v11, v12}, Lkotlin/ranges/RangesKt;->step(Lkotlin/ranges/IntProgression;I)Lkotlin/ranges/IntProgression;

    move-result-object v11

    invoke-virtual {v11}, Lkotlin/ranges/IntProgression;->getFirst()I

    move-result v12

    invoke-virtual {v11}, Lkotlin/ranges/IntProgression;->getLast()I

    move-result v13

    invoke-virtual {v11}, Lkotlin/ranges/IntProgression;->getStep()I

    move-result v19

    if-lez v19, :cond_0

    if-gt v12, v13, :cond_e

    goto :goto_0

    :cond_0
    if-lt v12, v13, :cond_e

    :goto_0
    const v11, 0x7fffffff

    move-object/from16 v20, v10

    const v21, 0x7fffffff

    .line 199
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/cardcustomizations/signature/Signature;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v10

    const/16 v16, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/squareup/cardcustomizations/signature/Signature;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v11

    const-string v14, "bitmap"

    invoke-static {v11, v14}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v14

    .line 200
    iget v11, v0, Landroid/graphics/Rect;->left:I

    invoke-virtual/range {p4 .. p4}, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->getSignatureCoords()Landroid/graphics/Point;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Point;->x:I

    sub-int v2, v11, v2

    .line 201
    invoke-virtual/range {p4 .. p4}, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->getSignatureCoords()Landroid/graphics/Point;

    move-result-object v11

    iget v11, v11, Landroid/graphics/Point;->y:I

    sub-int v17, v12, v11

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Rect;->width()I

    move-result v18

    const/16 v22, 0x1

    move-object v11, v9

    move v5, v12

    move/from16 v12, v16

    move/from16 v23, v13

    move v13, v14

    const/16 v24, 0x4

    move v14, v2

    move-object v2, v15

    move/from16 v15, v17

    move/from16 v16, v18

    move/from16 v17, v22

    .line 199
    invoke-virtual/range {v10 .. v17}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    const/4 v13, 0x0

    .line 202
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v14

    .line 203
    iget v10, v0, Landroid/graphics/Rect;->left:I

    invoke-virtual/range {p4 .. p4}, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->getStampCoords()Landroid/graphics/Point;

    move-result-object v11

    iget v11, v11, Landroid/graphics/Point;->x:I

    sub-int v15, v10, v11

    .line 204
    invoke-virtual/range {p4 .. p4}, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->getStampCoords()Landroid/graphics/Point;

    move-result-object v10

    iget v10, v10, Landroid/graphics/Point;->y:I

    sub-int v16, v5, v10

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Rect;->width()I

    move-result v17

    const/16 v18, 0x1

    move-object v11, v4

    move-object v12, v2

    .line 202
    invoke-virtual/range {v11 .. v18}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 206
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Rect;->width()I

    move-result v10

    const/4 v11, 0x0

    const/4 v12, 0x0

    :goto_2
    if-ge v12, v10, :cond_2

    .line 209
    aget v13, v9, v12

    if-nez v13, :cond_1

    aget v13, v2, v12

    if-nez v13, :cond_1

    aget v13, v1, v12

    add-int/lit8 v13, v13, 0x4

    goto :goto_3

    :cond_1
    const/4 v13, 0x0

    .line 208
    :goto_3
    aput v13, v1, v12

    add-int/lit8 v12, v12, 0x1

    goto :goto_2

    .line 217
    :cond_2
    iget v10, v0, Landroid/graphics/Rect;->left:I

    iget v12, v0, Landroid/graphics/Rect;->right:I

    if-gt v10, v12, :cond_b

    .line 218
    :goto_4
    iget v13, v0, Landroid/graphics/Rect;->left:I

    sub-int v13, v10, v13

    aget v13, v1, v13

    if-le v13, v11, :cond_3

    if-le v13, v3, :cond_3

    .line 221
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v6, v14}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    .line 222
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v6, v11}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    move-object/from16 v16, v1

    move v11, v13

    goto/16 :goto_8

    :cond_3
    if-ge v13, v11, :cond_a

    move/from16 v14, v21

    .line 227
    :goto_5
    invoke-virtual {v6}, Ljava/util/ArrayDeque;->pop()Ljava/lang/Object;

    move-result-object v15

    const-string v0, "stack.pop()"

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v15, Ljava/lang/Number;

    invoke-virtual {v15}, Ljava/lang/Number;->intValue()I

    move-result v15

    move-object/from16 v16, v1

    .line 228
    invoke-virtual {v6}, Ljava/util/ArrayDeque;->pop()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    move-result v0

    sub-int v1, v10, v0

    if-lt v1, v7, :cond_7

    if-le v11, v3, :cond_7

    sub-int v1, v10, v7

    .line 232
    iget v11, v8, Landroid/graphics/Point;->x:I

    if-le v0, v11, :cond_4

    goto :goto_6

    :cond_4
    if-lt v1, v11, :cond_5

    iget v0, v8, Landroid/graphics/Point;->x:I

    goto :goto_7

    .line 233
    :cond_5
    :goto_6
    iget v11, v8, Landroid/graphics/Point;->x:I

    if-le v11, v1, :cond_6

    move v0, v1

    .line 234
    :cond_6
    :goto_7
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1, v0, v5}, Landroid/graphics/Point;-><init>(II)V

    .line 235
    invoke-static {v1, v8}, Lcom/squareup/cardcustomizations/geometry/PointKt;->distance(Landroid/graphics/Point;Landroid/graphics/Point;)F

    move-result v11

    float-to-int v11, v11

    if-ge v11, v14, :cond_7

    move-object/from16 v20, v1

    move v14, v11

    :cond_7
    if-lt v13, v15, :cond_9

    if-eqz v13, :cond_8

    .line 245
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    .line 246
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    :cond_8
    move v11, v13

    move/from16 v21, v14

    goto :goto_8

    :cond_9
    move-object/from16 v0, p2

    move v11, v15

    move-object/from16 v1, v16

    goto :goto_5

    :cond_a
    move-object/from16 v16, v1

    :goto_8
    if-eq v10, v12, :cond_c

    add-int/lit8 v10, v10, 0x1

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    goto/16 :goto_4

    :cond_b
    move-object/from16 v16, v1

    :cond_c
    move/from16 v0, v23

    if-eq v5, v0, :cond_d

    add-int v12, v5, v19

    move-object/from16 v5, p4

    move v13, v0

    move-object v15, v2

    move-object/from16 v1, v16

    const/4 v14, 0x4

    move-object/from16 v2, p0

    move-object/from16 v0, p2

    goto/16 :goto_1

    :cond_d
    move-object/from16 v10, v20

    :cond_e
    if-eqz v10, :cond_f

    goto :goto_9

    :cond_f
    move-object v10, v8

    .line 252
    :goto_9
    invoke-virtual/range {p4 .. p4}, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->getStampCoords()Landroid/graphics/Point;

    move-result-object v0

    .line 353
    new-instance v1, Landroid/graphics/Point;

    iget v2, v10, Landroid/graphics/Point;->x:I

    iget v3, v10, Landroid/graphics/Point;->y:I

    invoke-direct {v1, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    .line 354
    iget v2, v0, Landroid/graphics/Point;->x:I

    neg-int v2, v2

    iget v0, v0, Landroid/graphics/Point;->y:I

    neg-int v0, v0

    invoke-virtual {v1, v2, v0}, Landroid/graphics/Point;->offset(II)V

    return-object v1
.end method

.method public static final getInkLevel(Landroid/graphics/Bitmap;I)I
    .locals 13

    const-string v0, "receiver$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 146
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    new-array v0, v0, [I

    .line 148
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    :goto_0
    if-ge v11, v9, :cond_3

    const/4 v3, 0x0

    .line 149
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    const/4 v5, 0x0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    const/4 v8, 0x1

    move-object v1, p0

    move-object v2, v0

    move v6, v11

    invoke-virtual/range {v1 .. v8}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 351
    array-length v1, v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_1
    if-ge v2, v1, :cond_2

    aget v4, v0, v2

    if-eq v4, p1, :cond_0

    const/4 v4, 0x1

    goto :goto_2

    :cond_0
    const/4 v4, 0x0

    :goto_2
    if-eqz v4, :cond_1

    add-int/lit8 v3, v3, 0x1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    add-int/2addr v12, v3

    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    :cond_3
    return v12
.end method

.method public static synthetic getInkLevel$default(Landroid/graphics/Bitmap;IILjava/lang/Object;)I
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const/4 p1, -0x1

    .line 145
    :cond_0
    invoke-static {p0, p1}, Lcom/squareup/cardcustomizations/signature/PhysicalCardKt;->getInkLevel(Landroid/graphics/Bitmap;I)I

    move-result p0

    return p0
.end method
