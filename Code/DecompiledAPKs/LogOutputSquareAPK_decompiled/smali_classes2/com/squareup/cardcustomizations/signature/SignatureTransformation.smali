.class public final Lcom/squareup/cardcustomizations/signature/SignatureTransformation;
.super Ljava/lang/Object;
.source "PhysicalCard.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardcustomizations/signature/SignatureTransformation$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0002\u0008\u0015\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u0000 )2\u00020\u0001:\u0001)BC\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\u0003\u0012\u0006\u0010\t\u001a\u00020\u0005\u0012\u0006\u0010\n\u001a\u00020\u0007\u0012\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000c\u00a2\u0006\u0002\u0010\u000eJ\t\u0010\u001a\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001b\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001c\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u001d\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001e\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001f\u001a\u00020\u0007H\u00c6\u0003J\u000f\u0010 \u001a\u0008\u0012\u0004\u0012\u00020\r0\u000cH\u00c6\u0003JU\u0010!\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u00032\u0008\u0008\u0002\u0010\t\u001a\u00020\u00052\u0008\u0008\u0002\u0010\n\u001a\u00020\u00072\u000e\u0008\u0002\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000cH\u00c6\u0001J\u0013\u0010\"\u001a\u00020#2\u0008\u0010$\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010%\u001a\u00020&H\u00d6\u0001J\t\u0010\'\u001a\u00020(H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u0011\u0010\u0008\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0010R\u0011\u0010\n\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0012R\u0011\u0010\t\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0014R\u0017\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019\u00a8\u0006*"
    }
    d2 = {
        "Lcom/squareup/cardcustomizations/signature/SignatureTransformation;",
        "",
        "signatureBounds",
        "Landroid/graphics/RectF;",
        "signatureMatrix",
        "Landroid/graphics/Matrix;",
        "signatureCoords",
        "Landroid/graphics/Point;",
        "stampBounds",
        "stampMatrix",
        "stampCoords",
        "stamps",
        "",
        "Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;",
        "(Landroid/graphics/RectF;Landroid/graphics/Matrix;Landroid/graphics/Point;Landroid/graphics/RectF;Landroid/graphics/Matrix;Landroid/graphics/Point;Ljava/util/Collection;)V",
        "getSignatureBounds",
        "()Landroid/graphics/RectF;",
        "getSignatureCoords",
        "()Landroid/graphics/Point;",
        "getSignatureMatrix",
        "()Landroid/graphics/Matrix;",
        "getStampBounds",
        "getStampCoords",
        "getStampMatrix",
        "getStamps",
        "()Ljava/util/Collection;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "Companion",
        "card-customization_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xd
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/cardcustomizations/signature/SignatureTransformation$Companion;


# instance fields
.field private final signatureBounds:Landroid/graphics/RectF;

.field private final signatureCoords:Landroid/graphics/Point;

.field private final signatureMatrix:Landroid/graphics/Matrix;

.field private final stampBounds:Landroid/graphics/RectF;

.field private final stampCoords:Landroid/graphics/Point;

.field private final stampMatrix:Landroid/graphics/Matrix;

.field private final stamps:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection<",
            "Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/cardcustomizations/signature/SignatureTransformation$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/cardcustomizations/signature/SignatureTransformation$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->Companion:Lcom/squareup/cardcustomizations/signature/SignatureTransformation$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/graphics/RectF;Landroid/graphics/Matrix;Landroid/graphics/Point;Landroid/graphics/RectF;Landroid/graphics/Matrix;Landroid/graphics/Point;Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/RectF;",
            "Landroid/graphics/Matrix;",
            "Landroid/graphics/Point;",
            "Landroid/graphics/RectF;",
            "Landroid/graphics/Matrix;",
            "Landroid/graphics/Point;",
            "Ljava/util/Collection<",
            "Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;",
            ">;)V"
        }
    .end annotation

    const-string v0, "signatureBounds"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "signatureMatrix"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "signatureCoords"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "stampBounds"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "stampMatrix"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "stampCoords"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "stamps"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 260
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->signatureBounds:Landroid/graphics/RectF;

    iput-object p2, p0, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->signatureMatrix:Landroid/graphics/Matrix;

    iput-object p3, p0, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->signatureCoords:Landroid/graphics/Point;

    iput-object p4, p0, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->stampBounds:Landroid/graphics/RectF;

    iput-object p5, p0, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->stampMatrix:Landroid/graphics/Matrix;

    iput-object p6, p0, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->stampCoords:Landroid/graphics/Point;

    iput-object p7, p0, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->stamps:Ljava/util/Collection;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/cardcustomizations/signature/SignatureTransformation;Landroid/graphics/RectF;Landroid/graphics/Matrix;Landroid/graphics/Point;Landroid/graphics/RectF;Landroid/graphics/Matrix;Landroid/graphics/Point;Ljava/util/Collection;ILjava/lang/Object;)Lcom/squareup/cardcustomizations/signature/SignatureTransformation;
    .locals 5

    and-int/lit8 p9, p8, 0x1

    if-eqz p9, :cond_0

    iget-object p1, p0, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->signatureBounds:Landroid/graphics/RectF;

    :cond_0
    and-int/lit8 p9, p8, 0x2

    if-eqz p9, :cond_1

    iget-object p2, p0, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->signatureMatrix:Landroid/graphics/Matrix;

    :cond_1
    move-object p9, p2

    and-int/lit8 p2, p8, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->signatureCoords:Landroid/graphics/Point;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p8, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->stampBounds:Landroid/graphics/RectF;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p8, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->stampMatrix:Landroid/graphics/Matrix;

    :cond_4
    move-object v2, p5

    and-int/lit8 p2, p8, 0x20

    if-eqz p2, :cond_5

    iget-object p6, p0, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->stampCoords:Landroid/graphics/Point;

    :cond_5
    move-object v3, p6

    and-int/lit8 p2, p8, 0x40

    if-eqz p2, :cond_6

    iget-object p7, p0, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->stamps:Ljava/util/Collection;

    :cond_6
    move-object v4, p7

    move-object p2, p0

    move-object p3, p1

    move-object p4, p9

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    move-object p8, v3

    move-object p9, v4

    invoke-virtual/range {p2 .. p9}, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->copy(Landroid/graphics/RectF;Landroid/graphics/Matrix;Landroid/graphics/Point;Landroid/graphics/RectF;Landroid/graphics/Matrix;Landroid/graphics/Point;Ljava/util/Collection;)Lcom/squareup/cardcustomizations/signature/SignatureTransformation;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Landroid/graphics/RectF;
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->signatureBounds:Landroid/graphics/RectF;

    return-object v0
.end method

.method public final component2()Landroid/graphics/Matrix;
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->signatureMatrix:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public final component3()Landroid/graphics/Point;
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->signatureCoords:Landroid/graphics/Point;

    return-object v0
.end method

.method public final component4()Landroid/graphics/RectF;
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->stampBounds:Landroid/graphics/RectF;

    return-object v0
.end method

.method public final component5()Landroid/graphics/Matrix;
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->stampMatrix:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public final component6()Landroid/graphics/Point;
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->stampCoords:Landroid/graphics/Point;

    return-object v0
.end method

.method public final component7()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection<",
            "Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->stamps:Ljava/util/Collection;

    return-object v0
.end method

.method public final copy(Landroid/graphics/RectF;Landroid/graphics/Matrix;Landroid/graphics/Point;Landroid/graphics/RectF;Landroid/graphics/Matrix;Landroid/graphics/Point;Ljava/util/Collection;)Lcom/squareup/cardcustomizations/signature/SignatureTransformation;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/RectF;",
            "Landroid/graphics/Matrix;",
            "Landroid/graphics/Point;",
            "Landroid/graphics/RectF;",
            "Landroid/graphics/Matrix;",
            "Landroid/graphics/Point;",
            "Ljava/util/Collection<",
            "Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;",
            ">;)",
            "Lcom/squareup/cardcustomizations/signature/SignatureTransformation;"
        }
    .end annotation

    const-string v0, "signatureBounds"

    move-object v2, p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "signatureMatrix"

    move-object v3, p2

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "signatureCoords"

    move-object v4, p3

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "stampBounds"

    move-object v5, p4

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "stampMatrix"

    move-object v6, p5

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "stampCoords"

    move-object v7, p6

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "stamps"

    move-object/from16 v8, p7

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;-><init>(Landroid/graphics/RectF;Landroid/graphics/Matrix;Landroid/graphics/Point;Landroid/graphics/RectF;Landroid/graphics/Matrix;Landroid/graphics/Point;Ljava/util/Collection;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;

    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->signatureBounds:Landroid/graphics/RectF;

    iget-object v1, p1, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->signatureBounds:Landroid/graphics/RectF;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->signatureMatrix:Landroid/graphics/Matrix;

    iget-object v1, p1, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->signatureMatrix:Landroid/graphics/Matrix;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->signatureCoords:Landroid/graphics/Point;

    iget-object v1, p1, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->signatureCoords:Landroid/graphics/Point;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->stampBounds:Landroid/graphics/RectF;

    iget-object v1, p1, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->stampBounds:Landroid/graphics/RectF;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->stampMatrix:Landroid/graphics/Matrix;

    iget-object v1, p1, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->stampMatrix:Landroid/graphics/Matrix;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->stampCoords:Landroid/graphics/Point;

    iget-object v1, p1, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->stampCoords:Landroid/graphics/Point;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->stamps:Ljava/util/Collection;

    iget-object p1, p1, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->stamps:Ljava/util/Collection;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getSignatureBounds()Landroid/graphics/RectF;
    .locals 1

    .line 261
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->signatureBounds:Landroid/graphics/RectF;

    return-object v0
.end method

.method public final getSignatureCoords()Landroid/graphics/Point;
    .locals 1

    .line 263
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->signatureCoords:Landroid/graphics/Point;

    return-object v0
.end method

.method public final getSignatureMatrix()Landroid/graphics/Matrix;
    .locals 1

    .line 262
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->signatureMatrix:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public final getStampBounds()Landroid/graphics/RectF;
    .locals 1

    .line 264
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->stampBounds:Landroid/graphics/RectF;

    return-object v0
.end method

.method public final getStampCoords()Landroid/graphics/Point;
    .locals 1

    .line 266
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->stampCoords:Landroid/graphics/Point;

    return-object v0
.end method

.method public final getStampMatrix()Landroid/graphics/Matrix;
    .locals 1

    .line 265
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->stampMatrix:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public final getStamps()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection<",
            "Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;",
            ">;"
        }
    .end annotation

    .line 267
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->stamps:Ljava/util/Collection;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->signatureBounds:Landroid/graphics/RectF;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->signatureMatrix:Landroid/graphics/Matrix;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->signatureCoords:Landroid/graphics/Point;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->stampBounds:Landroid/graphics/RectF;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->stampMatrix:Landroid/graphics/Matrix;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->stampCoords:Landroid/graphics/Point;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_5
    const/4 v2, 0x0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->stamps:Ljava/util/Collection;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_6
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SignatureTransformation(signatureBounds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->signatureBounds:Landroid/graphics/RectF;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", signatureMatrix="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->signatureMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", signatureCoords="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->signatureCoords:Landroid/graphics/Point;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", stampBounds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->stampBounds:Landroid/graphics/RectF;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", stampMatrix="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->stampMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", stampCoords="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->stampCoords:Landroid/graphics/Point;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", stamps="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->stamps:Ljava/util/Collection;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
