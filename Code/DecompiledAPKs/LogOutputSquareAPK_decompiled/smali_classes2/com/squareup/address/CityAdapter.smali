.class Lcom/squareup/address/CityAdapter;
.super Landroid/widget/ArrayAdapter;
.source "CityAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field static final OTHER_ID:I = -0x1


# instance fields
.field private final postalLocations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/address/PostalLocation;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Lcom/squareup/server/address/PostalLocation;",
            ">;)V"
        }
    .end annotation

    .line 22
    sget v0, Lcom/squareup/address/R$layout;->city_spinner:I

    invoke-direct {p0, p1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 23
    sget v0, Lcom/squareup/address/R$layout;->city_spinner_dropdown:I

    invoke-virtual {p0, v0}, Lcom/squareup/address/CityAdapter;->setDropDownViewResource(I)V

    .line 25
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/server/address/PostalLocation;

    .line 26
    iget-object v1, v1, Lcom/squareup/server/address/PostalLocation;->city:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/squareup/address/CityAdapter;->add(Ljava/lang/Object;)V

    goto :goto_0

    .line 28
    :cond_0
    sget v0, Lcom/squareup/address/R$string;->postal_other:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/address/CityAdapter;->add(Ljava/lang/Object;)V

    .line 30
    invoke-static {p2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/address/CityAdapter;->postalLocations:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public getItemId(I)J
    .locals 2

    .line 34
    invoke-virtual {p0}, Lcom/squareup/address/CityAdapter;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_0

    const-wide/16 v0, -0x1

    goto :goto_0

    :cond_0
    int-to-long v0, p1

    :goto_0
    return-wide v0
.end method

.method public getPostalLocation(I)Lcom/squareup/server/address/PostalLocation;
    .locals 1

    .line 38
    invoke-virtual {p0}, Lcom/squareup/address/CityAdapter;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/address/CityAdapter;->postalLocations:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/server/address/PostalLocation;

    :goto_0
    return-object p1
.end method
