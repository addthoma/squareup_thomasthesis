.class public abstract Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput;
.super Ljava/lang/Object;
.source "AddressBodyScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/address/workflow/AddressBodyScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "LocationInput"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$ManualInput;,
        Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;,
        Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityResult;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0003\t\n\u000bB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0012\u0010\u0003\u001a\u00020\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006R\u0012\u0010\u0007\u001a\u00020\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\u0006\u0082\u0001\u0002\u000c\r\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput;",
        "",
        "()V",
        "city",
        "Lcom/squareup/workflow/text/WorkflowEditableText;",
        "getCity",
        "()Lcom/squareup/workflow/text/WorkflowEditableText;",
        "state",
        "getState",
        "ManualInput",
        "PickCityInput",
        "PickCityResult",
        "Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$ManualInput;",
        "Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 19
    invoke-direct {p0}, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getCity()Lcom/squareup/workflow/text/WorkflowEditableText;
.end method

.method public abstract getState()Lcom/squareup/workflow/text/WorkflowEditableText;
.end method
