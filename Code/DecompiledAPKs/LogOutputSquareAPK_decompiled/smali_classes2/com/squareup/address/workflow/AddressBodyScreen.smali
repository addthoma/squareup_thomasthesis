.class public final Lcom/squareup/address/workflow/AddressBodyScreen;
.super Ljava/lang/Object;
.source "AddressBodyScreen.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/V2Screen;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u001a\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001:\u00010BQ\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0008\u0010\u0008\u001a\u0004\u0018\u00010\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0008\u0008\u0002\u0010\u000c\u001a\u00020\r\u0012\u000e\u0008\u0002\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000f\u00a2\u0006\u0002\u0010\u0011J\t\u0010 \u001a\u00020\u0003H\u00c6\u0003J\t\u0010!\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\"\u001a\u00020\u0003H\u00c6\u0003J\t\u0010#\u001a\u00020\u0007H\u00c6\u0003J\u000b\u0010$\u001a\u0004\u0018\u00010\tH\u00c6\u0003J\t\u0010%\u001a\u00020\u000bH\u00c6\u0003J\t\u0010&\u001a\u00020\rH\u00c6\u0003J\u000f\u0010\'\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000fH\u00c6\u0003Ja\u0010(\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\t2\u0008\u0008\u0002\u0010\n\u001a\u00020\u000b2\u0008\u0008\u0002\u0010\u000c\u001a\u00020\r2\u000e\u0008\u0002\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000fH\u00c6\u0001J\u0013\u0010)\u001a\u00020\r2\u0008\u0010*\u001a\u0004\u0018\u00010+H\u00d6\u0003J\t\u0010,\u001a\u00020-H\u00d6\u0001J\t\u0010.\u001a\u00020/H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013R\u0013\u0010\u0008\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015R\u0011\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019R\u0017\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000f\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u001bR\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001c\u0010\u0013R\u0011\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001d\u0010\u001eR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001f\u0010\u0013\u00a8\u00061"
    }
    d2 = {
        "Lcom/squareup/address/workflow/AddressBodyScreen;",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        "street",
        "Lcom/squareup/workflow/text/WorkflowEditableText;",
        "apartment",
        "postal",
        "location",
        "Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput;",
        "country",
        "Lcom/squareup/CountryCode;",
        "postalScrubber",
        "Lcom/squareup/text/PostalScrubber;",
        "enabled",
        "",
        "onStateClicked",
        "Lkotlin/Function0;",
        "",
        "(Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput;Lcom/squareup/CountryCode;Lcom/squareup/text/PostalScrubber;ZLkotlin/jvm/functions/Function0;)V",
        "getApartment",
        "()Lcom/squareup/workflow/text/WorkflowEditableText;",
        "getCountry",
        "()Lcom/squareup/CountryCode;",
        "getEnabled",
        "()Z",
        "getLocation",
        "()Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput;",
        "getOnStateClicked",
        "()Lkotlin/jvm/functions/Function0;",
        "getPostal",
        "getPostalScrubber",
        "()Lcom/squareup/text/PostalScrubber;",
        "getStreet",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "LocationInput",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final apartment:Lcom/squareup/workflow/text/WorkflowEditableText;

.field private final country:Lcom/squareup/CountryCode;

.field private final enabled:Z

.field private final location:Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput;

.field private final onStateClicked:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final postal:Lcom/squareup/workflow/text/WorkflowEditableText;

.field private final postalScrubber:Lcom/squareup/text/PostalScrubber;

.field private final street:Lcom/squareup/workflow/text/WorkflowEditableText;


# direct methods
.method public constructor <init>(Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput;Lcom/squareup/CountryCode;Lcom/squareup/text/PostalScrubber;ZLkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/text/WorkflowEditableText;",
            "Lcom/squareup/workflow/text/WorkflowEditableText;",
            "Lcom/squareup/workflow/text/WorkflowEditableText;",
            "Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput;",
            "Lcom/squareup/CountryCode;",
            "Lcom/squareup/text/PostalScrubber;",
            "Z",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "street"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apartment"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "postal"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "location"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "postalScrubber"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onStateClicked"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/address/workflow/AddressBodyScreen;->street:Lcom/squareup/workflow/text/WorkflowEditableText;

    iput-object p2, p0, Lcom/squareup/address/workflow/AddressBodyScreen;->apartment:Lcom/squareup/workflow/text/WorkflowEditableText;

    iput-object p3, p0, Lcom/squareup/address/workflow/AddressBodyScreen;->postal:Lcom/squareup/workflow/text/WorkflowEditableText;

    iput-object p4, p0, Lcom/squareup/address/workflow/AddressBodyScreen;->location:Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput;

    iput-object p5, p0, Lcom/squareup/address/workflow/AddressBodyScreen;->country:Lcom/squareup/CountryCode;

    iput-object p6, p0, Lcom/squareup/address/workflow/AddressBodyScreen;->postalScrubber:Lcom/squareup/text/PostalScrubber;

    iput-boolean p7, p0, Lcom/squareup/address/workflow/AddressBodyScreen;->enabled:Z

    iput-object p8, p0, Lcom/squareup/address/workflow/AddressBodyScreen;->onStateClicked:Lkotlin/jvm/functions/Function0;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput;Lcom/squareup/CountryCode;Lcom/squareup/text/PostalScrubber;ZLkotlin/jvm/functions/Function0;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 11

    move/from16 v0, p9

    and-int/lit8 v1, v0, 0x40

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    const/4 v9, 0x1

    goto :goto_0

    :cond_0
    move/from16 v9, p7

    :goto_0
    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_1

    .line 17
    sget-object v0, Lcom/squareup/address/workflow/AddressBodyScreen$1;->INSTANCE:Lcom/squareup/address/workflow/AddressBodyScreen$1;

    check-cast v0, Lkotlin/jvm/functions/Function0;

    move-object v10, v0

    goto :goto_1

    :cond_1
    move-object/from16 v10, p8

    :goto_1
    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    invoke-direct/range {v2 .. v10}, Lcom/squareup/address/workflow/AddressBodyScreen;-><init>(Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput;Lcom/squareup/CountryCode;Lcom/squareup/text/PostalScrubber;ZLkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/address/workflow/AddressBodyScreen;Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput;Lcom/squareup/CountryCode;Lcom/squareup/text/PostalScrubber;ZLkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lcom/squareup/address/workflow/AddressBodyScreen;
    .locals 9

    move-object v0, p0

    move/from16 v1, p9

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/squareup/address/workflow/AddressBodyScreen;->street:Lcom/squareup/workflow/text/WorkflowEditableText;

    goto :goto_0

    :cond_0
    move-object v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/squareup/address/workflow/AddressBodyScreen;->apartment:Lcom/squareup/workflow/text/WorkflowEditableText;

    goto :goto_1

    :cond_1
    move-object v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/squareup/address/workflow/AddressBodyScreen;->postal:Lcom/squareup/workflow/text/WorkflowEditableText;

    goto :goto_2

    :cond_2
    move-object v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/squareup/address/workflow/AddressBodyScreen;->location:Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput;

    goto :goto_3

    :cond_3
    move-object v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-object v6, v0, Lcom/squareup/address/workflow/AddressBodyScreen;->country:Lcom/squareup/CountryCode;

    goto :goto_4

    :cond_4
    move-object v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-object v7, v0, Lcom/squareup/address/workflow/AddressBodyScreen;->postalScrubber:Lcom/squareup/text/PostalScrubber;

    goto :goto_5

    :cond_5
    move-object v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-boolean v8, v0, Lcom/squareup/address/workflow/AddressBodyScreen;->enabled:Z

    goto :goto_6

    :cond_6
    move/from16 v8, p7

    :goto_6
    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_7

    iget-object v1, v0, Lcom/squareup/address/workflow/AddressBodyScreen;->onStateClicked:Lkotlin/jvm/functions/Function0;

    goto :goto_7

    :cond_7
    move-object/from16 v1, p8

    :goto_7
    move-object p1, v2

    move-object p2, v3

    move-object p3, v4

    move-object p4, v5

    move-object p5, v6

    move-object p6, v7

    move/from16 p7, v8

    move-object/from16 p8, v1

    invoke-virtual/range {p0 .. p8}, Lcom/squareup/address/workflow/AddressBodyScreen;->copy(Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput;Lcom/squareup/CountryCode;Lcom/squareup/text/PostalScrubber;ZLkotlin/jvm/functions/Function0;)Lcom/squareup/address/workflow/AddressBodyScreen;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Lcom/squareup/workflow/text/WorkflowEditableText;
    .locals 1

    iget-object v0, p0, Lcom/squareup/address/workflow/AddressBodyScreen;->street:Lcom/squareup/workflow/text/WorkflowEditableText;

    return-object v0
.end method

.method public final component2()Lcom/squareup/workflow/text/WorkflowEditableText;
    .locals 1

    iget-object v0, p0, Lcom/squareup/address/workflow/AddressBodyScreen;->apartment:Lcom/squareup/workflow/text/WorkflowEditableText;

    return-object v0
.end method

.method public final component3()Lcom/squareup/workflow/text/WorkflowEditableText;
    .locals 1

    iget-object v0, p0, Lcom/squareup/address/workflow/AddressBodyScreen;->postal:Lcom/squareup/workflow/text/WorkflowEditableText;

    return-object v0
.end method

.method public final component4()Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput;
    .locals 1

    iget-object v0, p0, Lcom/squareup/address/workflow/AddressBodyScreen;->location:Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput;

    return-object v0
.end method

.method public final component5()Lcom/squareup/CountryCode;
    .locals 1

    iget-object v0, p0, Lcom/squareup/address/workflow/AddressBodyScreen;->country:Lcom/squareup/CountryCode;

    return-object v0
.end method

.method public final component6()Lcom/squareup/text/PostalScrubber;
    .locals 1

    iget-object v0, p0, Lcom/squareup/address/workflow/AddressBodyScreen;->postalScrubber:Lcom/squareup/text/PostalScrubber;

    return-object v0
.end method

.method public final component7()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/address/workflow/AddressBodyScreen;->enabled:Z

    return v0
.end method

.method public final component8()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/address/workflow/AddressBodyScreen;->onStateClicked:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final copy(Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput;Lcom/squareup/CountryCode;Lcom/squareup/text/PostalScrubber;ZLkotlin/jvm/functions/Function0;)Lcom/squareup/address/workflow/AddressBodyScreen;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/text/WorkflowEditableText;",
            "Lcom/squareup/workflow/text/WorkflowEditableText;",
            "Lcom/squareup/workflow/text/WorkflowEditableText;",
            "Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput;",
            "Lcom/squareup/CountryCode;",
            "Lcom/squareup/text/PostalScrubber;",
            "Z",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/address/workflow/AddressBodyScreen;"
        }
    .end annotation

    const-string v0, "street"

    move-object v2, p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apartment"

    move-object v3, p2

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "postal"

    move-object v4, p3

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "location"

    move-object v5, p4

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "postalScrubber"

    move-object/from16 v7, p6

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onStateClicked"

    move-object/from16 v9, p8

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/address/workflow/AddressBodyScreen;

    move-object v1, v0

    move-object v6, p5

    move/from16 v8, p7

    invoke-direct/range {v1 .. v9}, Lcom/squareup/address/workflow/AddressBodyScreen;-><init>(Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput;Lcom/squareup/CountryCode;Lcom/squareup/text/PostalScrubber;ZLkotlin/jvm/functions/Function0;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/address/workflow/AddressBodyScreen;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/address/workflow/AddressBodyScreen;

    iget-object v0, p0, Lcom/squareup/address/workflow/AddressBodyScreen;->street:Lcom/squareup/workflow/text/WorkflowEditableText;

    iget-object v1, p1, Lcom/squareup/address/workflow/AddressBodyScreen;->street:Lcom/squareup/workflow/text/WorkflowEditableText;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/address/workflow/AddressBodyScreen;->apartment:Lcom/squareup/workflow/text/WorkflowEditableText;

    iget-object v1, p1, Lcom/squareup/address/workflow/AddressBodyScreen;->apartment:Lcom/squareup/workflow/text/WorkflowEditableText;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/address/workflow/AddressBodyScreen;->postal:Lcom/squareup/workflow/text/WorkflowEditableText;

    iget-object v1, p1, Lcom/squareup/address/workflow/AddressBodyScreen;->postal:Lcom/squareup/workflow/text/WorkflowEditableText;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/address/workflow/AddressBodyScreen;->location:Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput;

    iget-object v1, p1, Lcom/squareup/address/workflow/AddressBodyScreen;->location:Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/address/workflow/AddressBodyScreen;->country:Lcom/squareup/CountryCode;

    iget-object v1, p1, Lcom/squareup/address/workflow/AddressBodyScreen;->country:Lcom/squareup/CountryCode;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/address/workflow/AddressBodyScreen;->postalScrubber:Lcom/squareup/text/PostalScrubber;

    iget-object v1, p1, Lcom/squareup/address/workflow/AddressBodyScreen;->postalScrubber:Lcom/squareup/text/PostalScrubber;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/address/workflow/AddressBodyScreen;->enabled:Z

    iget-boolean v1, p1, Lcom/squareup/address/workflow/AddressBodyScreen;->enabled:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/address/workflow/AddressBodyScreen;->onStateClicked:Lkotlin/jvm/functions/Function0;

    iget-object p1, p1, Lcom/squareup/address/workflow/AddressBodyScreen;->onStateClicked:Lkotlin/jvm/functions/Function0;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getApartment()Lcom/squareup/workflow/text/WorkflowEditableText;
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/squareup/address/workflow/AddressBodyScreen;->apartment:Lcom/squareup/workflow/text/WorkflowEditableText;

    return-object v0
.end method

.method public final getCountry()Lcom/squareup/CountryCode;
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/squareup/address/workflow/AddressBodyScreen;->country:Lcom/squareup/CountryCode;

    return-object v0
.end method

.method public final getEnabled()Z
    .locals 1

    .line 16
    iget-boolean v0, p0, Lcom/squareup/address/workflow/AddressBodyScreen;->enabled:Z

    return v0
.end method

.method public final getLocation()Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput;
    .locals 1

    .line 13
    iget-object v0, p0, Lcom/squareup/address/workflow/AddressBodyScreen;->location:Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput;

    return-object v0
.end method

.method public final getOnStateClicked()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 17
    iget-object v0, p0, Lcom/squareup/address/workflow/AddressBodyScreen;->onStateClicked:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getPostal()Lcom/squareup/workflow/text/WorkflowEditableText;
    .locals 1

    .line 12
    iget-object v0, p0, Lcom/squareup/address/workflow/AddressBodyScreen;->postal:Lcom/squareup/workflow/text/WorkflowEditableText;

    return-object v0
.end method

.method public final getPostalScrubber()Lcom/squareup/text/PostalScrubber;
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/squareup/address/workflow/AddressBodyScreen;->postalScrubber:Lcom/squareup/text/PostalScrubber;

    return-object v0
.end method

.method public final getStreet()Lcom/squareup/workflow/text/WorkflowEditableText;
    .locals 1

    .line 10
    iget-object v0, p0, Lcom/squareup/address/workflow/AddressBodyScreen;->street:Lcom/squareup/workflow/text/WorkflowEditableText;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/address/workflow/AddressBodyScreen;->street:Lcom/squareup/workflow/text/WorkflowEditableText;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/address/workflow/AddressBodyScreen;->apartment:Lcom/squareup/workflow/text/WorkflowEditableText;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/address/workflow/AddressBodyScreen;->postal:Lcom/squareup/workflow/text/WorkflowEditableText;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/address/workflow/AddressBodyScreen;->location:Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/address/workflow/AddressBodyScreen;->country:Lcom/squareup/CountryCode;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/address/workflow/AddressBodyScreen;->postalScrubber:Lcom/squareup/text/PostalScrubber;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_5
    const/4 v2, 0x0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/address/workflow/AddressBodyScreen;->enabled:Z

    if-eqz v2, :cond_6

    const/4 v2, 0x1

    :cond_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/address/workflow/AddressBodyScreen;->onStateClicked:Lkotlin/jvm/functions/Function0;

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_7
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AddressBodyScreen(street="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/address/workflow/AddressBodyScreen;->street:Lcom/squareup/workflow/text/WorkflowEditableText;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", apartment="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/address/workflow/AddressBodyScreen;->apartment:Lcom/squareup/workflow/text/WorkflowEditableText;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", postal="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/address/workflow/AddressBodyScreen;->postal:Lcom/squareup/workflow/text/WorkflowEditableText;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", location="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/address/workflow/AddressBodyScreen;->location:Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", country="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/address/workflow/AddressBodyScreen;->country:Lcom/squareup/CountryCode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", postalScrubber="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/address/workflow/AddressBodyScreen;->postalScrubber:Lcom/squareup/text/PostalScrubber;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/address/workflow/AddressBodyScreen;->enabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", onStateClicked="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/address/workflow/AddressBodyScreen;->onStateClicked:Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
