.class public final Lcom/squareup/address/workflow/AddressLayoutRunner$showRendering$1;
.super Ljava/lang/Object;
.source "AddressLayoutRunner.kt"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/address/workflow/AddressLayoutRunner;->showRendering(Lcom/squareup/address/workflow/AddressBodyScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000+\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J,\u0010\u0002\u001a\u00020\u00032\n\u0010\u0004\u001a\u0006\u0012\u0002\u0008\u00030\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH\u0016J\u0014\u0010\u000c\u001a\u00020\u00032\n\u0010\u0004\u001a\u0006\u0012\u0002\u0008\u00030\u0005H\u0016\u00a8\u0006\r"
    }
    d2 = {
        "com/squareup/address/workflow/AddressLayoutRunner$showRendering$1",
        "Landroid/widget/AdapterView$OnItemSelectedListener;",
        "onItemSelected",
        "",
        "parent",
        "Landroid/widget/AdapterView;",
        "view",
        "Landroid/view/View;",
        "position",
        "",
        "id",
        "",
        "onNothingSelected",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $location:Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput;


# direct methods
.method constructor <init>(Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput;)V
    .locals 0

    .line 101
    iput-object p1, p0, Lcom/squareup/address/workflow/AddressLayoutRunner$showRendering$1;->$location:Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "view"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v0, -0x1

    cmp-long p2, p4, v0

    if-nez p2, :cond_0

    .line 108
    iget-object p1, p0, Lcom/squareup/address/workflow/AddressLayoutRunner$showRendering$1;->$location:Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput;

    check-cast p1, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;

    invoke-virtual {p1}, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;->getOnCityPicked()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    sget-object p2, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityResult$OtherCityPicked;->INSTANCE:Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityResult$OtherCityPicked;

    invoke-interface {p1, p2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 110
    :cond_0
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object p1

    invoke-interface {p1, p3}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_1

    check-cast p1, Lcom/squareup/address/workflow/PostalLocationAdapter$CityLocation;

    invoke-virtual {p1}, Lcom/squareup/address/workflow/PostalLocationAdapter$CityLocation;->getLocation()Lcom/squareup/server/address/PostalLocation;

    move-result-object p1

    .line 111
    iget-object p2, p0, Lcom/squareup/address/workflow/AddressLayoutRunner$showRendering$1;->$location:Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput;

    check-cast p2, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;

    invoke-virtual {p2}, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;->getOnCityPicked()Lkotlin/jvm/functions/Function1;

    move-result-object p2

    .line 112
    new-instance p4, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityResult$CityPicked;

    .line 113
    iget-object p5, p1, Lcom/squareup/server/address/PostalLocation;->city:Ljava/lang/String;

    .line 114
    iget-object p1, p1, Lcom/squareup/server/address/PostalLocation;->state_abbreviation:Ljava/lang/String;

    .line 112
    invoke-direct {p4, p5, p1, p3}, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityResult$CityPicked;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-interface {p2, p4}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return-void

    .line 110
    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type com.squareup.address.workflow.PostalLocationAdapter.CityLocation"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;)V"
        }
    .end annotation

    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 103
    iget-object p1, p0, Lcom/squareup/address/workflow/AddressLayoutRunner$showRendering$1;->$location:Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput;

    check-cast p1, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;

    invoke-virtual {p1}, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;->getOnCityPicked()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    sget-object v0, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityResult$NoCityPicked;->INSTANCE:Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityResult$NoCityPicked;

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
