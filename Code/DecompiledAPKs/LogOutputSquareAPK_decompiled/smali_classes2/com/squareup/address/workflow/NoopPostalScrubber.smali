.class public final Lcom/squareup/address/workflow/NoopPostalScrubber;
.super Lcom/squareup/text/PostalScrubber;
.source "NoopPostalScrubber.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/address/workflow/NoopPostalScrubber$NoopResult;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u00c0\u0002\u0018\u00002\u00020\u0001:\u0001\nB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u00020\u00042\u0008\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0014J\u0012\u0010\u0007\u001a\u00020\u00082\u0008\u0010\t\u001a\u0004\u0018\u00010\u0006H\u0014\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/address/workflow/NoopPostalScrubber;",
        "Lcom/squareup/text/PostalScrubber;",
        "()V",
        "isValidFormattedPostalCode",
        "",
        "scrubbedCode",
        "",
        "parsePostal",
        "Lcom/squareup/text/PostalScrubber$Result;",
        "proposed",
        "NoopResult",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/address/workflow/NoopPostalScrubber;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 5
    new-instance v0, Lcom/squareup/address/workflow/NoopPostalScrubber;

    invoke-direct {v0}, Lcom/squareup/address/workflow/NoopPostalScrubber;-><init>()V

    sput-object v0, Lcom/squareup/address/workflow/NoopPostalScrubber;->INSTANCE:Lcom/squareup/address/workflow/NoopPostalScrubber;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 5
    invoke-direct {p0}, Lcom/squareup/text/PostalScrubber;-><init>()V

    return-void
.end method


# virtual methods
.method protected isValidFormattedPostalCode(Ljava/lang/String;)Z
    .locals 0

    const/4 p1, 0x1

    return p1
.end method

.method protected parsePostal(Ljava/lang/String;)Lcom/squareup/text/PostalScrubber$Result;
    .locals 1

    .line 7
    new-instance v0, Lcom/squareup/address/workflow/NoopPostalScrubber$NoopResult;

    invoke-direct {v0, p1}, Lcom/squareup/address/workflow/NoopPostalScrubber$NoopResult;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/text/PostalScrubber$Result;

    return-object v0
.end method
