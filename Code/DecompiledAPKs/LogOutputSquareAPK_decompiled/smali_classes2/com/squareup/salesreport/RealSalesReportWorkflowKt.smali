.class public final Lcom/squareup/salesreport/RealSalesReportWorkflowKt;
.super Ljava/lang/Object;
.source "RealSalesReportWorkflow.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u000c\u0010\u0000\u001a\u00020\u0001*\u00020\u0001H\u0002\u00a8\u0006\u0002"
    }
    d2 = {
        "toggle",
        "Lcom/squareup/salesreport/SalesReportState$TopSectionState;",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$toggle(Lcom/squareup/salesreport/SalesReportState$TopSectionState;)Lcom/squareup/salesreport/SalesReportState$TopSectionState;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/salesreport/RealSalesReportWorkflowKt;->toggle(Lcom/squareup/salesreport/SalesReportState$TopSectionState;)Lcom/squareup/salesreport/SalesReportState$TopSectionState;

    move-result-object p0

    return-object p0
.end method

.method private static final toggle(Lcom/squareup/salesreport/SalesReportState$TopSectionState;)Lcom/squareup/salesreport/SalesReportState$TopSectionState;
    .locals 1

    .line 521
    sget-object v0, Lcom/squareup/salesreport/RealSalesReportWorkflowKt$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p0}, Lcom/squareup/salesreport/SalesReportState$TopSectionState;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-ne p0, v0, :cond_0

    .line 523
    sget-object p0, Lcom/squareup/salesreport/SalesReportState$TopSectionState;->SALES_OVERVIEW:Lcom/squareup/salesreport/SalesReportState$TopSectionState;

    goto :goto_0

    :cond_0
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    .line 522
    :cond_1
    sget-object p0, Lcom/squareup/salesreport/SalesReportState$TopSectionState;->SALES_DETAILS:Lcom/squareup/salesreport/SalesReportState$TopSectionState;

    :goto_0
    return-object p0
.end method
