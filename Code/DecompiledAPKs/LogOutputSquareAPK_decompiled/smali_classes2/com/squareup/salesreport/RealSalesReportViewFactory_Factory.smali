.class public final Lcom/squareup/salesreport/RealSalesReportViewFactory_Factory;
.super Ljava/lang/Object;
.source "RealSalesReportViewFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/salesreport/RealSalesReportViewFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/SalesReportCoordinator$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/export/email/EmailReportCoordinator$Factory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/SalesReportCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/export/email/EmailReportCoordinator$Factory;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/salesreport/RealSalesReportViewFactory_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/salesreport/RealSalesReportViewFactory_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/salesreport/RealSalesReportViewFactory_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 30
    iput-object p4, p0, Lcom/squareup/salesreport/RealSalesReportViewFactory_Factory;->arg3Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/salesreport/RealSalesReportViewFactory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/SalesReportCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/export/email/EmailReportCoordinator$Factory;",
            ">;)",
            "Lcom/squareup/salesreport/RealSalesReportViewFactory_Factory;"
        }
    .end annotation

    .line 43
    new-instance v0, Lcom/squareup/salesreport/RealSalesReportViewFactory_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/salesreport/RealSalesReportViewFactory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/salesreport/SalesReportCoordinator$Factory;Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$Factory;Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$Factory;Lcom/squareup/salesreport/export/email/EmailReportCoordinator$Factory;)Lcom/squareup/salesreport/RealSalesReportViewFactory;
    .locals 1

    .line 49
    new-instance v0, Lcom/squareup/salesreport/RealSalesReportViewFactory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/salesreport/RealSalesReportViewFactory;-><init>(Lcom/squareup/salesreport/SalesReportCoordinator$Factory;Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$Factory;Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$Factory;Lcom/squareup/salesreport/export/email/EmailReportCoordinator$Factory;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/salesreport/RealSalesReportViewFactory;
    .locals 4

    .line 35
    iget-object v0, p0, Lcom/squareup/salesreport/RealSalesReportViewFactory_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/salesreport/SalesReportCoordinator$Factory;

    iget-object v1, p0, Lcom/squareup/salesreport/RealSalesReportViewFactory_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$Factory;

    iget-object v2, p0, Lcom/squareup/salesreport/RealSalesReportViewFactory_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$Factory;

    iget-object v3, p0, Lcom/squareup/salesreport/RealSalesReportViewFactory_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/salesreport/export/email/EmailReportCoordinator$Factory;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/salesreport/RealSalesReportViewFactory_Factory;->newInstance(Lcom/squareup/salesreport/SalesReportCoordinator$Factory;Lcom/squareup/salesreport/customize/CustomizeReportAllFieldsCoordinator$Factory;Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$Factory;Lcom/squareup/salesreport/export/email/EmailReportCoordinator$Factory;)Lcom/squareup/salesreport/RealSalesReportViewFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/salesreport/RealSalesReportViewFactory_Factory;->get()Lcom/squareup/salesreport/RealSalesReportViewFactory;

    move-result-object v0

    return-object v0
.end method
