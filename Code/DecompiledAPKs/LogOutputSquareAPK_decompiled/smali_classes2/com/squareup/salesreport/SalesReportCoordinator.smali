.class public final Lcom/squareup/salesreport/SalesReportCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "SalesReportCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/salesreport/SalesReportCoordinator$Factory;,
        Lcom/squareup/salesreport/SalesReportCoordinator$ComparisonIconState;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSalesReportCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SalesReportCoordinator.kt\ncom/squareup/salesreport/SalesReportCoordinator\n+ 2 RecyclerFactory.kt\ncom/squareup/recycler/RecyclerFactory\n+ 3 Recycler.kt\ncom/squareup/cycler/Recycler$Companion\n+ 4 Recycler.kt\ncom/squareup/cycler/Recycler$Config\n+ 5 StandardRowSpec.kt\ncom/squareup/cycler/StandardRowSpec\n+ 6 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 7 Views.kt\ncom/squareup/util/Views\n*L\n1#1,1242:1\n49#2:1243\n50#2,3:1249\n53#2:1342\n599#3,4:1244\n601#3:1248\n310#4,6:1252\n310#4,6:1258\n310#4,6:1264\n310#4,6:1270\n310#4,6:1276\n310#4,6:1282\n310#4,6:1288\n310#4,6:1294\n310#4,6:1300\n310#4,6:1306\n310#4,6:1312\n310#4,6:1318\n310#4,6:1324\n355#4,3:1330\n358#4,3:1339\n35#5,6:1333\n310#6,7:1343\n1360#6:1350\n1429#6,3:1351\n1651#6,2:1354\n1642#6,12:1356\n1651#6,2:1368\n1642#6,12:1370\n704#6:1382\n777#6,2:1383\n1360#6:1385\n1429#6,3:1386\n704#6:1389\n777#6,2:1390\n1360#6:1392\n1429#6,3:1393\n1642#6,2:1396\n1360#6:1398\n1429#6,3:1399\n1103#7,7:1402\n1103#7,7:1409\n1103#7,7:1416\n*E\n*S KotlinDebug\n*F\n+ 1 SalesReportCoordinator.kt\ncom/squareup/salesreport/SalesReportCoordinator\n*L\n350#1:1243\n350#1,3:1249\n350#1:1342\n350#1,4:1244\n350#1:1248\n350#1,6:1252\n350#1,6:1258\n350#1,6:1264\n350#1,6:1270\n350#1,6:1276\n350#1,6:1282\n350#1,6:1288\n350#1,6:1294\n350#1,6:1300\n350#1,6:1306\n350#1,6:1312\n350#1,6:1318\n350#1,6:1324\n350#1,3:1330\n350#1,3:1339\n350#1,6:1333\n550#1,7:1343\n559#1:1350\n559#1,3:1351\n641#1,2:1354\n641#1,12:1356\n717#1,2:1368\n717#1,12:1370\n840#1:1382\n840#1,2:1383\n841#1:1385\n841#1,3:1386\n893#1:1389\n893#1,2:1390\n894#1:1392\n894#1,3:1393\n943#1,2:1396\n1002#1:1398\n1002#1,3:1399\n1078#1,7:1402\n1086#1,7:1409\n1106#1,7:1416\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00b8\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0004\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0010!\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\"\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0018\u00002\u00020\u0001:\u0004\u00af\u0001\u00b0\u0001B\u0083\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\"\u0010\u0006\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\u0008j\u0008\u0012\u0004\u0012\u00020\t`\u000b0\u0007\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u000c\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000f\u0012\u000c\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000f\u0012\u000c\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u000f\u0012\u000c\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u000f\u0012\u000c\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u000f\u0012\u0006\u0010\u0016\u001a\u00020\u0017\u0012\u0006\u0010\u0018\u001a\u00020\u0019\u0012\u0006\u0010\u001a\u001a\u00020\u001b\u0012\u0006\u0010\u001c\u001a\u00020\u001d\u0012\u000c\u0010\u001e\u001a\u0008\u0012\u0004\u0012\u00020\u001f0\u000f\u0012\u0006\u0010 \u001a\u00020!\u0012\u0006\u0010\"\u001a\u00020#\u0012\u0006\u0010$\u001a\u00020%\u0012\u0006\u0010&\u001a\u00020\'\u0012\u0006\u0010(\u001a\u00020)\u0012\u0006\u0010*\u001a\u00020+\u0012\u000c\u0010,\u001a\u0008\u0012\u0004\u0012\u00020.0-\u0012\u0006\u0010/\u001a\u000200\u0012\u0006\u00101\u001a\u000202\u00a2\u0006\u0002\u00103JL\u0010W\u001a\u00020X2\u0006\u0010Y\u001a\u00020\t2\u000c\u0010Z\u001a\u0008\u0012\u0004\u0012\u00020J0[2\u0006\u0010\\\u001a\u00020]2\u0006\u0010^\u001a\u00020_2\u0006\u0010`\u001a\u00020a2\u0006\u0010b\u001a\u00020T2\u000c\u0010c\u001a\u0008\u0012\u0004\u0012\u00020S0dH\u0002J6\u0010e\u001a\u00020X2\u000c\u0010Z\u001a\u0008\u0012\u0004\u0012\u00020J0[2\u0006\u0010f\u001a\u00020g2\u0006\u0010h\u001a\u00020i2\u0006\u0010j\u001a\u00020k2\u0006\u0010l\u001a\u00020mH\u0002J&\u0010n\u001a\u00020X2\u000c\u0010o\u001a\u0008\u0012\u0004\u0012\u00020p0[2\u0006\u0010q\u001a\u00020r2\u0006\u0010s\u001a\u00020aH\u0002JL\u0010t\u001a\u00020X2\u0006\u0010Y\u001a\u00020\t2\u000c\u0010Z\u001a\u0008\u0012\u0004\u0012\u00020J0[2\u0006\u0010u\u001a\u00020v2\u0006\u0010^\u001a\u00020_2\u0006\u0010`\u001a\u00020a2\u0006\u0010w\u001a\u00020T2\u000c\u0010x\u001a\u0008\u0012\u0004\u0012\u00020S0dH\u0002J.\u0010y\u001a\u00020X2\u0006\u0010Y\u001a\u00020\t2\u000c\u0010Z\u001a\u0008\u0012\u0004\u0012\u00020J0[2\u0006\u0010z\u001a\u00020{2\u0006\u0010`\u001a\u00020aH\u0002Jd\u0010|\u001a\u00020X2\u000c\u0010Z\u001a\u0008\u0012\u0004\u0012\u00020J0[2\u0006\u0010}\u001a\u00020~2\u0006\u0010^\u001a\u00020_2\u0006\u0010`\u001a\u00020a2\u0006\u0010\u007f\u001a\u00020a2\u0007\u0010\u0080\u0001\u001a\u00020a2\u0007\u0010\u0081\u0001\u001a\u00020a2\u001a\u0010\u0082\u0001\u001a\u0015\u0012\u0004\u0012\u00020<\u0012\u0004\u0012\u00020X0\u0083\u0001\u00a2\u0006\u0003\u0008\u0084\u0001H\u0002JD\u0010\u0085\u0001\u001a\u00020X2\u0006\u0010Y\u001a\u00020\t2\u000c\u0010Z\u001a\u0008\u0012\u0004\u0012\u00020J0[2\u0008\u0010\u0086\u0001\u001a\u00030\u0087\u00012\u0008\u0010\u0088\u0001\u001a\u00030\u0089\u00012\u0007\u0010\u008a\u0001\u001a\u00020a2\u0006\u0010`\u001a\u00020aH\u0002J\u0012\u0010\u008b\u0001\u001a\u00020X2\u0007\u0010\u008c\u0001\u001a\u00020<H\u0016J\u0012\u0010\u008d\u0001\u001a\u00020X2\u0007\u0010\u008e\u0001\u001a\u00020<H\u0002J-\u0010\u008f\u0001\u001a\u00020X2\u0006\u0010Y\u001a\u00020\t2\u0007\u0010\u0090\u0001\u001a\u00020a2\u0007\u0010\u0091\u0001\u001a\u00020a2\u0008\u0010\u0092\u0001\u001a\u00030\u0093\u0001H\u0002J,\u0010\u0094\u0001\u001a\n\u0012\u0005\u0012\u00030\u0096\u00010\u0095\u00012\u0006\u0010Y\u001a\u00020\t2\u0008\u0010\u0097\u0001\u001a\u00030\u0098\u00012\u0007\u0010\u0099\u0001\u001a\u00020SH\u0002J\u0019\u0010\u009a\u0001\u001a\n\u0012\u0005\u0012\u00030\u0096\u00010\u0095\u00012\u0006\u0010Y\u001a\u00020\tH\u0002J\n\u0010\u009b\u0001\u001a\u00030\u009c\u0001H\u0002J,\u0010\u009d\u0001\u001a\n\u0012\u0005\u0012\u00030\u0096\u00010\u0095\u00012\u0006\u0010Y\u001a\u00020\t2\u0008\u0010\u0097\u0001\u001a\u00030\u0098\u00012\u0007\u0010\u0099\u0001\u001a\u00020SH\u0002J+\u0010\u009e\u0001\u001a\u00020X2\u0006\u0010Y\u001a\u00020\t2\u0006\u0010`\u001a\u00020a2\u0007\u0010\u009f\u0001\u001a\u00020a2\u0007\u0010\u008c\u0001\u001a\u00020<H\u0002J\u0013\u0010\u00a0\u0001\u001a\u00020X2\u0008\u0010\u00a1\u0001\u001a\u00030\u00a2\u0001H\u0002J\u0012\u0010\u00a3\u0001\u001a\u00020X2\u0007\u0010\u008e\u0001\u001a\u00020<H\u0002J\u0012\u0010\u00a4\u0001\u001a\u00020X2\u0007\u0010\u00a5\u0001\u001a\u00020<H\u0002J?\u0010\u00a6\u0001\u001a\u00020X2\u0006\u0010Y\u001a\u00020\t2\u0006\u0010f\u001a\u00020g2\u0008\u0010\u0086\u0001\u001a\u00030\u0087\u00012\u0008\u0010\u0097\u0001\u001a\u00030\u0098\u00012\u0008\u0010\u00a7\u0001\u001a\u00030\u00a8\u00012\u0006\u0010`\u001a\u00020aH\u0002J?\u0010\u00a9\u0001\u001a\u00020X2\u0006\u0010Y\u001a\u00020\t2\u0008\u0010\u0086\u0001\u001a\u00030\u0087\u00012\u0006\u0010f\u001a\u00020g2\u0008\u0010\u0097\u0001\u001a\u00030\u0098\u00012\u0008\u0010\u00aa\u0001\u001a\u00030\u00ab\u00012\u0006\u0010`\u001a\u00020aH\u0002J!\u0010\u00ac\u0001\u001a\u00020S2\r\u0010\u00ad\u0001\u001a\u0008\u0012\u0004\u0012\u00020S0d2\u0007\u0010\u00ae\u0001\u001a\u00020SH\u0002R\u000e\u00104\u001a\u000205X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u00101\u001a\u000202X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u00106\u001a\u000207X\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u00108\u001a\u000207X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u00109\u001a\u00020:X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010;\u001a\u00020<X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010/\u001a\u000200X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010=\u001a\u00020:X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010>\u001a\u00020?X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020!X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010*\u001a\u00020+X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010,\u001a\u0008\u0012\u0004\u0012\u00020.0-X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u001dX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010@\u001a\u000207X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010A\u001a\u00020BX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010C\u001a\u00020DX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010E\u001a\u00020FX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010G\u001a\u00020FX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\"\u001a\u00020#X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010$\u001a\u00020%X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010(\u001a\u00020)X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010H\u001a\u0008\u0012\u0004\u0012\u00020J0IX\u0082.\u00a2\u0006\u0002\n\u0000R*\u0010\u0006\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\u0008j\u0008\u0012\u0004\u0012\u00020\t`\u000b0\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010&\u001a\u00020\'X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010K\u001a\u00020LX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010M\u001a\u00020NX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010O\u001a\u00020LX\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010P\u001a\u0004\u0018\u000107X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010Q\u001a\u0004\u0018\u000107X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u001e\u001a\u0008\u0012\u0004\u0012\u00020\u001f0\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0018\u0010R\u001a\u00020S*\u00020T8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008U\u0010V\u00a8\u0006\u00b1\u0001"
    }
    d2 = {
        "Lcom/squareup/salesreport/SalesReportCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "device",
        "Lcom/squareup/util/Device;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/salesreport/SalesReportScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "percentageChangeFormatter",
        "Lcom/squareup/salesreport/util/PercentageChangeFormatter;",
        "fullNumberFormatter",
        "Lcom/squareup/text/Formatter;",
        "",
        "compactNumberFormatter",
        "fullMoneyFormatter",
        "Lcom/squareup/protos/common/Money;",
        "compactMoneyFormatter",
        "compactShorterMoneyFormatter",
        "localTimeFormatter",
        "Lcom/squareup/salesreport/util/LocalTimeFormatter;",
        "currentTime",
        "Lcom/squareup/time/CurrentTime;",
        "recyclerFactory",
        "Lcom/squareup/recycler/RecyclerFactory;",
        "nameOrTranslationTypeFormatter",
        "Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;",
        "wholeNumberPercentageFormatter",
        "Lcom/squareup/util/Percentage;",
        "employeeManagement",
        "Lcom/squareup/permissions/EmployeeManagement;",
        "res",
        "Lcom/squareup/util/Res;",
        "resources",
        "Landroid/content/res/Resources;",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "salesReportAnalytics",
        "Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "localeProvider",
        "Ljavax/inject/Provider;",
        "Ljava/util/Locale;",
        "current24HourClockMode",
        "Lcom/squareup/time/Current24HourClockMode;",
        "backButtonConfig",
        "Lcom/squareup/salesreport/util/SalesReportBackButtonConfig;",
        "(Lcom/squareup/util/Device;Lio/reactivex/Scheduler;Lio/reactivex/Observable;Lcom/squareup/salesreport/util/PercentageChangeFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/salesreport/util/LocalTimeFormatter;Lcom/squareup/time/CurrentTime;Lcom/squareup/recycler/RecyclerFactory;Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/util/Res;Landroid/content/res/Resources;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics;Lcom/squareup/settings/server/Features;Ljavax/inject/Provider;Lcom/squareup/time/Current24HourClockMode;Lcom/squareup/salesreport/util/SalesReportBackButtonConfig;)V",
        "actionBar",
        "Lcom/squareup/salesreport/widget/NohoActionBarWithTwoActionIcons;",
        "chartActions",
        "Lcom/squareup/salesreport/widget/PopupActions;",
        "comparisonRangeActions",
        "comparisonRangeIcon",
        "Landroid/widget/ImageView;",
        "comparisonRangeSpacer",
        "Landroid/view/View;",
        "customizeReportIcon",
        "dashboardUrlLauncher",
        "Lcom/squareup/salesreport/util/DashboardUrlLauncher;",
        "overviewDetailsActions",
        "rangeSelectionRow",
        "Lcom/squareup/salesreport/widget/RangeSelectionRow;",
        "reportAnimator",
        "Lcom/squareup/widgets/SquareViewAnimator;",
        "reportDetailsEmptyMessageView",
        "Lcom/squareup/noho/NohoMessageView;",
        "reportDetailsFailureMessageView",
        "salesReportRecycler",
        "Lcom/squareup/cycler/Recycler;",
        "Lcom/squareup/salesreport/util/SalesReportRow;",
        "topBarSubtitleLabel",
        "Landroid/widget/TextView;",
        "topBarTitleContainer",
        "Landroid/view/ViewGroup;",
        "topBarTitleLabel",
        "topCategoriesActions",
        "topItemsActions",
        "popupActionThreshold",
        "",
        "Lcom/squareup/salesreport/SalesReportState$ViewCount;",
        "getPopupActionThreshold",
        "(Lcom/squareup/salesreport/SalesReportState$ViewCount;)I",
        "addCategoryRows",
        "",
        "screen",
        "rowList",
        "",
        "topCategoriesReport",
        "Lcom/squareup/customreport/data/SalesTopCategoriesReport;",
        "grossCountSelection",
        "Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;",
        "isPhoneOrPortrait",
        "",
        "categoryViewCount",
        "categoriesWithItemsShown",
        "",
        "addChartRows",
        "reportConfig",
        "Lcom/squareup/customreport/data/ReportConfig;",
        "salesSummary",
        "Lcom/squareup/customreport/data/WithSalesSummaryReport;",
        "salesChart",
        "Lcom/squareup/customreport/data/SalesChartReport;",
        "chartSalesSelection",
        "Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;",
        "addDiscountRows",
        "salesDetailRows",
        "Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow;",
        "salesDiscountsReport",
        "Lcom/squareup/customreport/data/SalesDiscountsReport;",
        "truncatedRows",
        "addItemRows",
        "topItemsReport",
        "Lcom/squareup/customreport/data/SalesTopItemsReport;",
        "itemViewCount",
        "itemsWithVariationsShown",
        "addPaymentMethodRows",
        "paymentMethodsReport",
        "Lcom/squareup/customreport/data/SalesPaymentMethodsReport;",
        "addSalesItemRow",
        "salesItem",
        "Lcom/squareup/customreport/data/SalesItem;",
        "subRow",
        "hasSubRows",
        "showingSubRows",
        "clickHandler",
        "Lkotlin/Function1;",
        "Lkotlin/ExtensionFunctionType;",
        "addTopSection",
        "salesReport",
        "Lcom/squareup/customreport/data/WithSalesReport;",
        "topSectionState",
        "Lcom/squareup/salesreport/SalesReportState$TopSectionState;",
        "showSalesOverview",
        "attach",
        "view",
        "bindViews",
        "mainView",
        "configureActionBar",
        "showCompareIcon",
        "showBackArrow",
        "state",
        "Lcom/squareup/salesreport/SalesReportState;",
        "getCategoryActions",
        "",
        "Lcom/squareup/salesreport/widget/PopupAction;",
        "uiSelectionState",
        "Lcom/squareup/salesreport/SalesReportState$UiSelectionState;",
        "rowCount",
        "getChartActions",
        "getCurrentDate",
        "Lorg/threeten/bp/LocalDate;",
        "getItemActions",
        "onScreen",
        "isPhoneOrPortraitLessThan10Inches",
        "setComparisonRangeIconVisibility",
        "comparisonIconState",
        "Lcom/squareup/salesreport/SalesReportCoordinator$ComparisonIconState;",
        "setupSalesReportRecyclerView",
        "showSummaryOverviewDetailsBottomSheetDialog",
        "anchorView",
        "updateActions",
        "context",
        "Landroid/content/Context;",
        "updateSalesReportRecycler",
        "uiDisplayState",
        "Lcom/squareup/salesreport/UiDisplayState;",
        "visibleExpandedRowCount",
        "shownRows",
        "visibleRowCount",
        "ComparisonIconState",
        "Factory",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/salesreport/widget/NohoActionBarWithTwoActionIcons;

.field private final backButtonConfig:Lcom/squareup/salesreport/util/SalesReportBackButtonConfig;

.field private chartActions:Lcom/squareup/salesreport/widget/PopupActions;

.field private final compactMoneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final compactNumberFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field private final compactShorterMoneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private comparisonRangeActions:Lcom/squareup/salesreport/widget/PopupActions;

.field private comparisonRangeIcon:Landroid/widget/ImageView;

.field private comparisonRangeSpacer:Landroid/view/View;

.field private final current24HourClockMode:Lcom/squareup/time/Current24HourClockMode;

.field private final currentTime:Lcom/squareup/time/CurrentTime;

.field private customizeReportIcon:Landroid/widget/ImageView;

.field private dashboardUrlLauncher:Lcom/squareup/salesreport/util/DashboardUrlLauncher;

.field private final device:Lcom/squareup/util/Device;

.field private final employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final fullMoneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final fullNumberFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field private final localTimeFormatter:Lcom/squareup/salesreport/util/LocalTimeFormatter;

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final nameOrTranslationTypeFormatter:Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;

.field private overviewDetailsActions:Lcom/squareup/salesreport/widget/PopupActions;

.field private final percentageChangeFormatter:Lcom/squareup/salesreport/util/PercentageChangeFormatter;

.field private rangeSelectionRow:Lcom/squareup/salesreport/widget/RangeSelectionRow;

.field private final recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

.field private reportAnimator:Lcom/squareup/widgets/SquareViewAnimator;

.field private reportDetailsEmptyMessageView:Lcom/squareup/noho/NohoMessageView;

.field private reportDetailsFailureMessageView:Lcom/squareup/noho/NohoMessageView;

.field private final res:Lcom/squareup/util/Res;

.field private final resources:Landroid/content/res/Resources;

.field private final salesReportAnalytics:Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics;

.field private salesReportRecycler:Lcom/squareup/cycler/Recycler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/Recycler<",
            "Lcom/squareup/salesreport/util/SalesReportRow;",
            ">;"
        }
    .end annotation
.end field

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;"
        }
    .end annotation
.end field

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private topBarSubtitleLabel:Landroid/widget/TextView;

.field private topBarTitleContainer:Landroid/view/ViewGroup;

.field private topBarTitleLabel:Landroid/widget/TextView;

.field private topCategoriesActions:Lcom/squareup/salesreport/widget/PopupActions;

.field private topItemsActions:Lcom/squareup/salesreport/widget/PopupActions;

.field private final wholeNumberPercentageFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/util/Device;Lio/reactivex/Scheduler;Lio/reactivex/Observable;Lcom/squareup/salesreport/util/PercentageChangeFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/salesreport/util/LocalTimeFormatter;Lcom/squareup/time/CurrentTime;Lcom/squareup/recycler/RecyclerFactory;Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/util/Res;Landroid/content/res/Resources;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics;Lcom/squareup/settings/server/Features;Ljavax/inject/Provider;Lcom/squareup/time/Current24HourClockMode;Lcom/squareup/salesreport/util/SalesReportBackButtonConfig;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Device;",
            "Lio/reactivex/Scheduler;",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;",
            "Lcom/squareup/salesreport/util/PercentageChangeFormatter;",
            "Lcom/squareup/text/Formatter<",
            "Ljava/lang/Number;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Ljava/lang/Number;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/salesreport/util/LocalTimeFormatter;",
            "Lcom/squareup/time/CurrentTime;",
            "Lcom/squareup/recycler/RecyclerFactory;",
            "Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            "Lcom/squareup/util/Res;",
            "Landroid/content/res/Resources;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics;",
            "Lcom/squareup/settings/server/Features;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/time/Current24HourClockMode;",
            "Lcom/squareup/salesreport/util/SalesReportBackButtonConfig;",
            ")V"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    move-object/from16 v15, p15

    move-object/from16 v0, p16

    const-string v0, "device"

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screens"

    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "percentageChangeFormatter"

    invoke-static {v4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fullNumberFormatter"

    invoke-static {v5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "compactNumberFormatter"

    invoke-static {v6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fullMoneyFormatter"

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "compactMoneyFormatter"

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "compactShorterMoneyFormatter"

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "localTimeFormatter"

    invoke-static {v10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currentTime"

    invoke-static {v11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "recyclerFactory"

    invoke-static {v12, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nameOrTranslationTypeFormatter"

    invoke-static {v13, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "wholeNumberPercentageFormatter"

    invoke-static {v14, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "employeeManagement"

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    move-object/from16 v15, p16

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "resources"

    move-object/from16 v15, p17

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "settings"

    move-object/from16 v15, p18

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "salesReportAnalytics"

    move-object/from16 v15, p19

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    move-object/from16 v15, p20

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "localeProvider"

    move-object/from16 v15, p21

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "current24HourClockMode"

    move-object/from16 v15, p22

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "backButtonConfig"

    move-object/from16 v15, p23

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 201
    invoke-direct/range {p0 .. p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    move-object/from16 v0, p0

    move-object/from16 v15, p16

    iput-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator;->device:Lcom/squareup/util/Device;

    iput-object v2, v0, Lcom/squareup/salesreport/SalesReportCoordinator;->mainScheduler:Lio/reactivex/Scheduler;

    iput-object v3, v0, Lcom/squareup/salesreport/SalesReportCoordinator;->screens:Lio/reactivex/Observable;

    iput-object v4, v0, Lcom/squareup/salesreport/SalesReportCoordinator;->percentageChangeFormatter:Lcom/squareup/salesreport/util/PercentageChangeFormatter;

    iput-object v5, v0, Lcom/squareup/salesreport/SalesReportCoordinator;->fullNumberFormatter:Lcom/squareup/text/Formatter;

    iput-object v6, v0, Lcom/squareup/salesreport/SalesReportCoordinator;->compactNumberFormatter:Lcom/squareup/text/Formatter;

    iput-object v7, v0, Lcom/squareup/salesreport/SalesReportCoordinator;->fullMoneyFormatter:Lcom/squareup/text/Formatter;

    iput-object v8, v0, Lcom/squareup/salesreport/SalesReportCoordinator;->compactMoneyFormatter:Lcom/squareup/text/Formatter;

    iput-object v9, v0, Lcom/squareup/salesreport/SalesReportCoordinator;->compactShorterMoneyFormatter:Lcom/squareup/text/Formatter;

    iput-object v10, v0, Lcom/squareup/salesreport/SalesReportCoordinator;->localTimeFormatter:Lcom/squareup/salesreport/util/LocalTimeFormatter;

    iput-object v11, v0, Lcom/squareup/salesreport/SalesReportCoordinator;->currentTime:Lcom/squareup/time/CurrentTime;

    iput-object v12, v0, Lcom/squareup/salesreport/SalesReportCoordinator;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    iput-object v13, v0, Lcom/squareup/salesreport/SalesReportCoordinator;->nameOrTranslationTypeFormatter:Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;

    iput-object v14, v0, Lcom/squareup/salesreport/SalesReportCoordinator;->wholeNumberPercentageFormatter:Lcom/squareup/text/Formatter;

    move-object/from16 v1, p15

    iput-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    iput-object v15, v0, Lcom/squareup/salesreport/SalesReportCoordinator;->res:Lcom/squareup/util/Res;

    move-object/from16 v1, p17

    move-object/from16 v2, p18

    iput-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator;->resources:Landroid/content/res/Resources;

    iput-object v2, v0, Lcom/squareup/salesreport/SalesReportCoordinator;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    move-object/from16 v1, p19

    move-object/from16 v2, p20

    iput-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator;->salesReportAnalytics:Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics;

    iput-object v2, v0, Lcom/squareup/salesreport/SalesReportCoordinator;->features:Lcom/squareup/settings/server/Features;

    move-object/from16 v1, p21

    move-object/from16 v2, p22

    iput-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator;->localeProvider:Ljavax/inject/Provider;

    iput-object v2, v0, Lcom/squareup/salesreport/SalesReportCoordinator;->current24HourClockMode:Lcom/squareup/time/Current24HourClockMode;

    move-object/from16 v1, p23

    iput-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator;->backButtonConfig:Lcom/squareup/salesreport/util/SalesReportBackButtonConfig;

    return-void
.end method

.method public static final synthetic access$getActionBar$p(Lcom/squareup/salesreport/SalesReportCoordinator;)Lcom/squareup/salesreport/widget/NohoActionBarWithTwoActionIcons;
    .locals 1

    .line 177
    iget-object p0, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->actionBar:Lcom/squareup/salesreport/widget/NohoActionBarWithTwoActionIcons;

    if-nez p0, :cond_0

    const-string v0, "actionBar"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getChartActions$p(Lcom/squareup/salesreport/SalesReportCoordinator;)Lcom/squareup/salesreport/widget/PopupActions;
    .locals 1

    .line 177
    iget-object p0, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->chartActions:Lcom/squareup/salesreport/widget/PopupActions;

    if-nez p0, :cond_0

    const-string v0, "chartActions"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getCompactNumberFormatter$p(Lcom/squareup/salesreport/SalesReportCoordinator;)Lcom/squareup/text/Formatter;
    .locals 0

    .line 177
    iget-object p0, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->compactNumberFormatter:Lcom/squareup/text/Formatter;

    return-object p0
.end method

.method public static final synthetic access$getCompactShorterMoneyFormatter$p(Lcom/squareup/salesreport/SalesReportCoordinator;)Lcom/squareup/text/Formatter;
    .locals 0

    .line 177
    iget-object p0, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->compactShorterMoneyFormatter:Lcom/squareup/text/Formatter;

    return-object p0
.end method

.method public static final synthetic access$getComparisonRangeActions$p(Lcom/squareup/salesreport/SalesReportCoordinator;)Lcom/squareup/salesreport/widget/PopupActions;
    .locals 1

    .line 177
    iget-object p0, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->comparisonRangeActions:Lcom/squareup/salesreport/widget/PopupActions;

    if-nez p0, :cond_0

    const-string v0, "comparisonRangeActions"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getCurrent24HourClockMode$p(Lcom/squareup/salesreport/SalesReportCoordinator;)Lcom/squareup/time/Current24HourClockMode;
    .locals 0

    .line 177
    iget-object p0, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->current24HourClockMode:Lcom/squareup/time/Current24HourClockMode;

    return-object p0
.end method

.method public static final synthetic access$getCurrentDate(Lcom/squareup/salesreport/SalesReportCoordinator;)Lorg/threeten/bp/LocalDate;
    .locals 0

    .line 177
    invoke-direct {p0}, Lcom/squareup/salesreport/SalesReportCoordinator;->getCurrentDate()Lorg/threeten/bp/LocalDate;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getDashboardUrlLauncher$p(Lcom/squareup/salesreport/SalesReportCoordinator;)Lcom/squareup/salesreport/util/DashboardUrlLauncher;
    .locals 1

    .line 177
    iget-object p0, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->dashboardUrlLauncher:Lcom/squareup/salesreport/util/DashboardUrlLauncher;

    if-nez p0, :cond_0

    const-string v0, "dashboardUrlLauncher"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getFullMoneyFormatter$p(Lcom/squareup/salesreport/SalesReportCoordinator;)Lcom/squareup/text/Formatter;
    .locals 0

    .line 177
    iget-object p0, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->fullMoneyFormatter:Lcom/squareup/text/Formatter;

    return-object p0
.end method

.method public static final synthetic access$getFullNumberFormatter$p(Lcom/squareup/salesreport/SalesReportCoordinator;)Lcom/squareup/text/Formatter;
    .locals 0

    .line 177
    iget-object p0, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->fullNumberFormatter:Lcom/squareup/text/Formatter;

    return-object p0
.end method

.method public static final synthetic access$getLocalTimeFormatter$p(Lcom/squareup/salesreport/SalesReportCoordinator;)Lcom/squareup/salesreport/util/LocalTimeFormatter;
    .locals 0

    .line 177
    iget-object p0, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->localTimeFormatter:Lcom/squareup/salesreport/util/LocalTimeFormatter;

    return-object p0
.end method

.method public static final synthetic access$getLocaleProvider$p(Lcom/squareup/salesreport/SalesReportCoordinator;)Ljavax/inject/Provider;
    .locals 0

    .line 177
    iget-object p0, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->localeProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method public static final synthetic access$getRes$p(Lcom/squareup/salesreport/SalesReportCoordinator;)Lcom/squareup/util/Res;
    .locals 0

    .line 177
    iget-object p0, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->res:Lcom/squareup/util/Res;

    return-object p0
.end method

.method public static final synthetic access$getResources$p(Lcom/squareup/salesreport/SalesReportCoordinator;)Landroid/content/res/Resources;
    .locals 0

    .line 177
    iget-object p0, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->resources:Landroid/content/res/Resources;

    return-object p0
.end method

.method public static final synthetic access$getSalesReportAnalytics$p(Lcom/squareup/salesreport/SalesReportCoordinator;)Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics;
    .locals 0

    .line 177
    iget-object p0, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->salesReportAnalytics:Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics;

    return-object p0
.end method

.method public static final synthetic access$getSettings$p(Lcom/squareup/salesreport/SalesReportCoordinator;)Lcom/squareup/settings/server/AccountStatusSettings;
    .locals 0

    .line 177
    iget-object p0, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    return-object p0
.end method

.method public static final synthetic access$getTopCategoriesActions$p(Lcom/squareup/salesreport/SalesReportCoordinator;)Lcom/squareup/salesreport/widget/PopupActions;
    .locals 0

    .line 177
    iget-object p0, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->topCategoriesActions:Lcom/squareup/salesreport/widget/PopupActions;

    return-object p0
.end method

.method public static final synthetic access$getTopItemsActions$p(Lcom/squareup/salesreport/SalesReportCoordinator;)Lcom/squareup/salesreport/widget/PopupActions;
    .locals 0

    .line 177
    iget-object p0, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->topItemsActions:Lcom/squareup/salesreport/widget/PopupActions;

    return-object p0
.end method

.method public static final synthetic access$getWholeNumberPercentageFormatter$p(Lcom/squareup/salesreport/SalesReportCoordinator;)Lcom/squareup/text/Formatter;
    .locals 0

    .line 177
    iget-object p0, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->wholeNumberPercentageFormatter:Lcom/squareup/text/Formatter;

    return-object p0
.end method

.method public static final synthetic access$onScreen(Lcom/squareup/salesreport/SalesReportCoordinator;Lcom/squareup/salesreport/SalesReportScreen;ZZLandroid/view/View;)V
    .locals 0

    .line 177
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/salesreport/SalesReportCoordinator;->onScreen(Lcom/squareup/salesreport/SalesReportScreen;ZZLandroid/view/View;)V

    return-void
.end method

.method public static final synthetic access$setActionBar$p(Lcom/squareup/salesreport/SalesReportCoordinator;Lcom/squareup/salesreport/widget/NohoActionBarWithTwoActionIcons;)V
    .locals 0

    .line 177
    iput-object p1, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->actionBar:Lcom/squareup/salesreport/widget/NohoActionBarWithTwoActionIcons;

    return-void
.end method

.method public static final synthetic access$setChartActions$p(Lcom/squareup/salesreport/SalesReportCoordinator;Lcom/squareup/salesreport/widget/PopupActions;)V
    .locals 0

    .line 177
    iput-object p1, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->chartActions:Lcom/squareup/salesreport/widget/PopupActions;

    return-void
.end method

.method public static final synthetic access$setComparisonRangeActions$p(Lcom/squareup/salesreport/SalesReportCoordinator;Lcom/squareup/salesreport/widget/PopupActions;)V
    .locals 0

    .line 177
    iput-object p1, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->comparisonRangeActions:Lcom/squareup/salesreport/widget/PopupActions;

    return-void
.end method

.method public static final synthetic access$setDashboardUrlLauncher$p(Lcom/squareup/salesreport/SalesReportCoordinator;Lcom/squareup/salesreport/util/DashboardUrlLauncher;)V
    .locals 0

    .line 177
    iput-object p1, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->dashboardUrlLauncher:Lcom/squareup/salesreport/util/DashboardUrlLauncher;

    return-void
.end method

.method public static final synthetic access$setTopCategoriesActions$p(Lcom/squareup/salesreport/SalesReportCoordinator;Lcom/squareup/salesreport/widget/PopupActions;)V
    .locals 0

    .line 177
    iput-object p1, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->topCategoriesActions:Lcom/squareup/salesreport/widget/PopupActions;

    return-void
.end method

.method public static final synthetic access$setTopItemsActions$p(Lcom/squareup/salesreport/SalesReportCoordinator;Lcom/squareup/salesreport/widget/PopupActions;)V
    .locals 0

    .line 177
    iput-object p1, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->topItemsActions:Lcom/squareup/salesreport/widget/PopupActions;

    return-void
.end method

.method public static final synthetic access$showSummaryOverviewDetailsBottomSheetDialog(Lcom/squareup/salesreport/SalesReportCoordinator;Landroid/view/View;)V
    .locals 0

    .line 177
    invoke-direct {p0, p1}, Lcom/squareup/salesreport/SalesReportCoordinator;->showSummaryOverviewDetailsBottomSheetDialog(Landroid/view/View;)V

    return-void
.end method

.method private final addCategoryRows(Lcom/squareup/salesreport/SalesReportScreen;Ljava/util/List;Lcom/squareup/customreport/data/SalesTopCategoriesReport;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;ZLcom/squareup/salesreport/SalesReportState$ViewCount;Ljava/util/Set;)V
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/salesreport/SalesReportScreen;",
            "Ljava/util/List<",
            "Lcom/squareup/salesreport/util/SalesReportRow;",
            ">;",
            "Lcom/squareup/customreport/data/SalesTopCategoriesReport;",
            "Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;",
            "Z",
            "Lcom/squareup/salesreport/SalesReportState$ViewCount;",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    move-object/from16 v9, p0

    move-object/from16 v10, p1

    move-object/from16 v11, p2

    move-object/from16 v0, p3

    move-object/from16 v12, p7

    .line 679
    instance-of v1, v0, Lcom/squareup/customreport/data/WithSalesTopCategoriesReport;

    if-eqz v1, :cond_5

    .line 680
    iget-object v1, v9, Lcom/squareup/salesreport/SalesReportCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/salesreport/impl/R$string;->sales_report_top_categories_header_uppercase:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 682
    iget-object v2, v9, Lcom/squareup/salesreport/SalesReportCoordinator;->topCategoriesActions:Lcom/squareup/salesreport/widget/PopupActions;

    if-nez v2, :cond_0

    .line 683
    new-instance v2, Lcom/squareup/salesreport/util/SalesReportRow$SectionHeaderRow;

    invoke-direct {v2, v1}, Lcom/squareup/salesreport/util/SalesReportRow$SectionHeaderRow;-><init>(Ljava/lang/String;)V

    check-cast v2, Lcom/squareup/salesreport/util/SalesReportRow;

    goto :goto_0

    .line 685
    :cond_0
    new-instance v2, Lcom/squareup/salesreport/util/SalesReportRow$SectionHeaderWithImageButtonRow;

    .line 687
    iget-object v3, v9, Lcom/squareup/salesreport/SalesReportCoordinator;->res:Lcom/squareup/util/Res;

    .line 688
    sget v4, Lcom/squareup/salesreport/impl/R$string;->sales_report_top_categories_overflow_options_button_content_description:I

    .line 687
    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 690
    new-instance v4, Lcom/squareup/salesreport/SalesReportCoordinator$addCategoryRows$1;

    invoke-direct {v4, v9}, Lcom/squareup/salesreport/SalesReportCoordinator$addCategoryRows$1;-><init>(Lcom/squareup/salesreport/SalesReportCoordinator;)V

    check-cast v4, Lkotlin/jvm/functions/Function1;

    .line 685
    invoke-direct {v2, v1, v3, v4}, Lcom/squareup/salesreport/util/SalesReportRow$SectionHeaderWithImageButtonRow;-><init>(Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lcom/squareup/salesreport/util/SalesReportRow;

    .line 681
    :goto_0
    invoke-interface {v11, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v1, 0x0

    const/4 v13, 0x1

    if-nez p5, :cond_1

    .line 699
    new-instance v2, Lcom/squareup/salesreport/util/SalesReportRow$SubsectionHeaderRow;

    .line 700
    iget-object v3, v9, Lcom/squareup/salesreport/SalesReportCoordinator;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/salesreport/impl/R$string;->sales_report_header_category_uppercase:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 701
    iget-object v4, v9, Lcom/squareup/salesreport/SalesReportCoordinator;->res:Lcom/squareup/util/Res;

    sget v5, Lcom/squareup/salesreport/impl/R$string;->sales_report_header_count_uppercase:I

    invoke-interface {v4, v5}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 702
    iget-object v5, v9, Lcom/squareup/salesreport/SalesReportCoordinator;->res:Lcom/squareup/util/Res;

    sget v6, Lcom/squareup/salesreport/impl/R$string;->sales_report_header_gross_uppercase:I

    invoke-interface {v5, v6}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 699
    invoke-direct {v2, v3, v4, v5}, Lcom/squareup/salesreport/util/SalesReportRow$SubsectionHeaderRow;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 698
    invoke-interface {v11, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object/from16 v8, p4

    goto :goto_2

    .line 707
    :cond_1
    new-instance v2, Lcom/squareup/salesreport/util/SalesReportRow$TwoTabToggleRow;

    .line 708
    iget-object v3, v9, Lcom/squareup/salesreport/SalesReportCoordinator;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/salesreport/impl/R$string;->sales_report_header_gross:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 709
    iget-object v3, v9, Lcom/squareup/salesreport/SalesReportCoordinator;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/salesreport/impl/R$string;->sales_report_header_count:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 710
    sget-object v3, Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;->GROSS:Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;

    move-object/from16 v8, p4

    if-ne v8, v3, :cond_2

    const/16 v17, 0x1

    goto :goto_1

    :cond_2
    const/16 v17, 0x0

    .line 711
    :goto_1
    new-instance v3, Lcom/squareup/salesreport/SalesReportCoordinator$addCategoryRows$2;

    invoke-direct {v3, v10}, Lcom/squareup/salesreport/SalesReportCoordinator$addCategoryRows$2;-><init>(Lcom/squareup/salesreport/SalesReportScreen;)V

    move-object/from16 v18, v3

    check-cast v18, Lkotlin/jvm/functions/Function1;

    .line 712
    new-instance v3, Lcom/squareup/salesreport/SalesReportCoordinator$addCategoryRows$3;

    invoke-direct {v3, v10}, Lcom/squareup/salesreport/SalesReportCoordinator$addCategoryRows$3;-><init>(Lcom/squareup/salesreport/SalesReportScreen;)V

    move-object/from16 v19, v3

    check-cast v19, Lkotlin/jvm/functions/Function1;

    move-object v14, v2

    .line 707
    invoke-direct/range {v14 .. v19}, Lcom/squareup/salesreport/util/SalesReportRow$TwoTabToggleRow;-><init>(Ljava/lang/String;Ljava/lang/String;ZLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 706
    invoke-interface {v11, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 716
    :goto_2
    check-cast v0, Lcom/squareup/customreport/data/WithSalesTopCategoriesReport;

    invoke-virtual {v0}, Lcom/squareup/customreport/data/WithSalesTopCategoriesReport;->getTopCategories()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-virtual/range {p6 .. p6}, Lcom/squareup/salesreport/SalesReportState$ViewCount;->getCount()I

    move-result v2

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->take(Ljava/lang/Iterable;I)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 1369
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v14

    const/4 v15, 0x0

    :goto_3
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    add-int/lit8 v16, v15, 0x1

    if-gez v15, :cond_3

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_3
    move-object/from16 v17, v0

    check-cast v17, Lcom/squareup/customreport/data/SalesItem;

    const/16 v18, 0x0

    .line 724
    invoke-virtual/range {v17 .. v17}, Lcom/squareup/customreport/data/SalesItem;->getSubItems()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/lit8 v19, v0, 0x1

    .line 725
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v12, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v20

    .line 726
    new-instance v21, Lcom/squareup/salesreport/SalesReportCoordinator$addCategoryRows$$inlined$forEachIndexed$lambda$1;

    move-object/from16 v0, v21

    move v1, v15

    move-object/from16 v2, p0

    move-object/from16 v3, p2

    move-object/from16 v4, p4

    move/from16 v5, p5

    move-object/from16 v6, p7

    move-object/from16 v7, p1

    invoke-direct/range {v0 .. v7}, Lcom/squareup/salesreport/SalesReportCoordinator$addCategoryRows$$inlined$forEachIndexed$lambda$1;-><init>(ILcom/squareup/salesreport/SalesReportCoordinator;Ljava/util/List;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;ZLjava/util/Set;Lcom/squareup/salesreport/SalesReportScreen;)V

    check-cast v21, Lkotlin/jvm/functions/Function1;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, v17

    move-object/from16 v3, p4

    move/from16 v4, p5

    move/from16 v5, v18

    move/from16 v6, v19

    move/from16 v7, v20

    move-object/from16 v8, v21

    .line 718
    invoke-direct/range {v0 .. v8}, Lcom/squareup/salesreport/SalesReportCoordinator;->addSalesItemRow(Ljava/util/List;Lcom/squareup/customreport/data/SalesItem;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;ZZZZLkotlin/jvm/functions/Function1;)V

    .line 728
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v12, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 729
    invoke-virtual/range {v17 .. v17}, Lcom/squareup/customreport/data/SalesItem;->getSubItems()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 1370
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_4
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/customreport/data/SalesItem;

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 738
    sget-object v0, Lcom/squareup/salesreport/SalesReportCoordinator$addCategoryRows$4$2$1;->INSTANCE:Lcom/squareup/salesreport/SalesReportCoordinator$addCategoryRows$4$2$1;

    move-object v8, v0

    check-cast v8, Lkotlin/jvm/functions/Function1;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v3, p4

    move/from16 v4, p5

    .line 730
    invoke-direct/range {v0 .. v8}, Lcom/squareup/salesreport/SalesReportCoordinator;->addSalesItemRow(Ljava/util/List;Lcom/squareup/customreport/data/SalesItem;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;ZZZZLkotlin/jvm/functions/Function1;)V

    goto :goto_4

    :cond_4
    move-object/from16 v8, p4

    move/from16 v15, v16

    goto/16 :goto_3

    :cond_5
    return-void
.end method

.method private final addChartRows(Ljava/util/List;Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/customreport/data/WithSalesSummaryReport;Lcom/squareup/customreport/data/SalesChartReport;Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/salesreport/util/SalesReportRow;",
            ">;",
            "Lcom/squareup/customreport/data/ReportConfig;",
            "Lcom/squareup/customreport/data/WithSalesSummaryReport;",
            "Lcom/squareup/customreport/data/SalesChartReport;",
            "Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;",
            ")V"
        }
    .end annotation

    .line 520
    instance-of v0, p4, Lcom/squareup/customreport/data/WithSalesChartReport;

    if-eqz v0, :cond_0

    .line 522
    new-instance v0, Lcom/squareup/salesreport/util/SalesReportRow$SectionHeaderWithImageButtonRow;

    .line 523
    iget-object v1, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->res:Lcom/squareup/util/Res;

    invoke-static {p2, v1, p5}, Lcom/squareup/salesreport/util/ReportConfigsKt;->chartTitle(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/util/Res;Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;)Ljava/lang/String;

    move-result-object v1

    .line 524
    iget-object v2, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->res:Lcom/squareup/util/Res;

    .line 525
    sget v3, Lcom/squareup/salesreport/impl/R$string;->sales_report_sales_chart_overflow_options_button_content_description:I

    .line 524
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 527
    new-instance v3, Lcom/squareup/salesreport/SalesReportCoordinator$addChartRows$1;

    invoke-direct {v3, p0}, Lcom/squareup/salesreport/SalesReportCoordinator$addChartRows$1;-><init>(Lcom/squareup/salesreport/SalesReportCoordinator;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    .line 522
    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/salesreport/util/SalesReportRow$SectionHeaderWithImageButtonRow;-><init>(Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    .line 521
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 533
    new-instance v0, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;

    .line 535
    invoke-static {p2}, Lcom/squareup/customreport/data/util/ReportConfigsKt;->comparisonConfig(Lcom/squareup/customreport/data/ReportConfig;)Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v6

    .line 537
    move-object v8, p4

    check-cast v8, Lcom/squareup/customreport/data/WithSalesChartReport;

    move-object v4, v0

    move-object v5, p2

    move-object v7, p3

    move-object v9, p5

    .line 533
    invoke-direct/range {v4 .. v9}, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;-><init>(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/customreport/data/WithSalesSummaryReport;Lcom/squareup/customreport/data/WithSalesChartReport;Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;)V

    .line 532
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method private final addDiscountRows(Ljava/util/List;Lcom/squareup/customreport/data/SalesDiscountsReport;Z)V
    .locals 28
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow;",
            ">;",
            "Lcom/squareup/customreport/data/SalesDiscountsReport;",
            "Z)V"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    .line 549
    instance-of v2, v1, Lcom/squareup/customreport/data/WithSalesDiscountsReport;

    if-eqz v2, :cond_6

    .line 1344
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 1345
    check-cast v5, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow;

    .line 551
    invoke-virtual {v5}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow;->getName()Lcom/squareup/util/ViewString;

    move-result-object v5

    new-instance v6, Lcom/squareup/util/ViewString$ResourceString;

    sget v7, Lcom/squareup/salesreport/impl/R$string;->sales_report_details_discounts_and_comps:I

    invoke-direct {v6, v7}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    const/4 v4, -0x1

    :goto_1
    const/4 v2, 0x1

    if-ltz v4, :cond_2

    const/4 v3, 0x1

    :cond_2
    if-eqz v3, :cond_5

    add-int/2addr v4, v2

    .line 559
    check-cast v1, Lcom/squareup/customreport/data/WithSalesDiscountsReport;

    invoke-virtual {v1}, Lcom/squareup/customreport/data/WithSalesDiscountsReport;->getDiscounts()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 1350
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v1, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 1351
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 1352
    check-cast v3, Lcom/squareup/customreport/data/SalesDiscount;

    if-eqz p3, :cond_3

    .line 561
    new-instance v15, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;

    .line 562
    new-instance v5, Lcom/squareup/util/ViewString$TextString;

    iget-object v6, v0, Lcom/squareup/salesreport/SalesReportCoordinator;->nameOrTranslationTypeFormatter:Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;

    invoke-virtual {v3}, Lcom/squareup/customreport/data/SalesDiscount;->getName()Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;->formatName(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;)Ljava/lang/String;

    move-result-object v6

    check-cast v6, Ljava/lang/CharSequence;

    invoke-direct {v5, v6}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    move-object v6, v5

    check-cast v6, Lcom/squareup/util/ViewString;

    .line 563
    iget-object v5, v0, Lcom/squareup/salesreport/SalesReportCoordinator;->fullMoneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {v3}, Lcom/squareup/customreport/data/SalesDiscount;->getDiscountMoney()Lcom/squareup/protos/common/Money;

    move-result-object v3

    invoke-interface {v5, v3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/16 v13, 0x70

    const/4 v14, 0x0

    move-object v5, v15

    .line 561
    invoke-direct/range {v5 .. v14}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;-><init>(Lcom/squareup/util/ViewString;Ljava/lang/String;ZZZZLkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v15, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow;

    goto :goto_3

    .line 568
    :cond_3
    new-instance v5, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$FullSalesDetailsRow;

    .line 569
    new-instance v6, Lcom/squareup/util/ViewString$TextString;

    iget-object v7, v0, Lcom/squareup/salesreport/SalesReportCoordinator;->nameOrTranslationTypeFormatter:Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;

    invoke-virtual {v3}, Lcom/squareup/customreport/data/SalesDiscount;->getName()Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;->formatName(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;)Ljava/lang/String;

    move-result-object v7

    check-cast v7, Ljava/lang/CharSequence;

    invoke-direct {v6, v7}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    move-object/from16 v17, v6

    check-cast v17, Lcom/squareup/util/ViewString;

    .line 571
    iget-object v6, v0, Lcom/squareup/salesreport/SalesReportCoordinator;->fullMoneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {v3}, Lcom/squareup/customreport/data/SalesDiscount;->getDiscountMoney()Lcom/squareup/protos/common/Money;

    move-result-object v3

    invoke-interface {v6, v3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v18

    const/16 v22, 0x1

    const/16 v21, 0x0

    const/16 v19, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x1d0

    const/16 v27, 0x0

    const-string v20, ""

    move-object/from16 v16, v5

    .line 568
    invoke-direct/range {v16 .. v27}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$FullSalesDetailsRow;-><init>(Lcom/squareup/util/ViewString;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/Integer;ZZZLkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    move-object v15, v5

    check-cast v15, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow;

    .line 575
    :goto_3
    invoke-interface {v2, v15}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 1353
    :cond_4
    check-cast v2, Ljava/util/List;

    check-cast v2, Ljava/util/Collection;

    move-object/from16 v1, p1

    .line 558
    invoke-interface {v1, v4, v2}, Ljava/util/List;->addAll(ILjava/util/Collection;)Z

    goto :goto_4

    .line 554
    :cond_5
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "There should be a sales details row for discounts"

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    :cond_6
    :goto_4
    return-void
.end method

.method private final addItemRows(Lcom/squareup/salesreport/SalesReportScreen;Ljava/util/List;Lcom/squareup/customreport/data/SalesTopItemsReport;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;ZLcom/squareup/salesreport/SalesReportState$ViewCount;Ljava/util/Set;)V
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/salesreport/SalesReportScreen;",
            "Ljava/util/List<",
            "Lcom/squareup/salesreport/util/SalesReportRow;",
            ">;",
            "Lcom/squareup/customreport/data/SalesTopItemsReport;",
            "Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;",
            "Z",
            "Lcom/squareup/salesreport/SalesReportState$ViewCount;",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    move-object/from16 v9, p0

    move-object/from16 v10, p1

    move-object/from16 v11, p2

    move-object/from16 v0, p3

    move-object/from16 v12, p7

    .line 602
    instance-of v1, v0, Lcom/squareup/customreport/data/WithSalesTopItemsReport;

    if-eqz v1, :cond_5

    .line 603
    iget-object v1, v9, Lcom/squareup/salesreport/SalesReportCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/salesreport/impl/R$string;->sales_report_top_items_header_uppercase:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 605
    iget-object v2, v9, Lcom/squareup/salesreport/SalesReportCoordinator;->topItemsActions:Lcom/squareup/salesreport/widget/PopupActions;

    if-nez v2, :cond_0

    .line 606
    new-instance v2, Lcom/squareup/salesreport/util/SalesReportRow$SectionHeaderRow;

    invoke-direct {v2, v1}, Lcom/squareup/salesreport/util/SalesReportRow$SectionHeaderRow;-><init>(Ljava/lang/String;)V

    check-cast v2, Lcom/squareup/salesreport/util/SalesReportRow;

    goto :goto_0

    .line 608
    :cond_0
    new-instance v2, Lcom/squareup/salesreport/util/SalesReportRow$SectionHeaderWithImageButtonRow;

    .line 610
    iget-object v3, v9, Lcom/squareup/salesreport/SalesReportCoordinator;->res:Lcom/squareup/util/Res;

    .line 611
    sget v4, Lcom/squareup/salesreport/impl/R$string;->sales_report_top_items_overflow_options_button_content_description:I

    .line 610
    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 613
    new-instance v4, Lcom/squareup/salesreport/SalesReportCoordinator$addItemRows$1;

    invoke-direct {v4, v9}, Lcom/squareup/salesreport/SalesReportCoordinator$addItemRows$1;-><init>(Lcom/squareup/salesreport/SalesReportCoordinator;)V

    check-cast v4, Lkotlin/jvm/functions/Function1;

    .line 608
    invoke-direct {v2, v1, v3, v4}, Lcom/squareup/salesreport/util/SalesReportRow$SectionHeaderWithImageButtonRow;-><init>(Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lcom/squareup/salesreport/util/SalesReportRow;

    .line 604
    :goto_0
    invoke-interface {v11, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v1, 0x0

    const/4 v13, 0x1

    if-nez p5, :cond_1

    .line 622
    new-instance v2, Lcom/squareup/salesreport/util/SalesReportRow$SubsectionHeaderRow;

    .line 623
    iget-object v3, v9, Lcom/squareup/salesreport/SalesReportCoordinator;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/salesreport/impl/R$string;->sales_report_header_item_uppercase:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 624
    iget-object v4, v9, Lcom/squareup/salesreport/SalesReportCoordinator;->res:Lcom/squareup/util/Res;

    sget v5, Lcom/squareup/salesreport/impl/R$string;->sales_report_header_count_uppercase:I

    invoke-interface {v4, v5}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 625
    iget-object v5, v9, Lcom/squareup/salesreport/SalesReportCoordinator;->res:Lcom/squareup/util/Res;

    sget v6, Lcom/squareup/salesreport/impl/R$string;->sales_report_header_gross_uppercase:I

    invoke-interface {v5, v6}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 622
    invoke-direct {v2, v3, v4, v5}, Lcom/squareup/salesreport/util/SalesReportRow$SubsectionHeaderRow;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 621
    invoke-interface {v11, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object/from16 v8, p4

    goto :goto_2

    .line 630
    :cond_1
    new-instance v2, Lcom/squareup/salesreport/util/SalesReportRow$TwoTabToggleRow;

    .line 631
    iget-object v3, v9, Lcom/squareup/salesreport/SalesReportCoordinator;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/salesreport/impl/R$string;->sales_report_header_gross:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 632
    iget-object v3, v9, Lcom/squareup/salesreport/SalesReportCoordinator;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/salesreport/impl/R$string;->sales_report_header_count:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 633
    sget-object v3, Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;->GROSS:Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;

    move-object/from16 v8, p4

    if-ne v8, v3, :cond_2

    const/16 v17, 0x1

    goto :goto_1

    :cond_2
    const/16 v17, 0x0

    .line 634
    :goto_1
    new-instance v3, Lcom/squareup/salesreport/SalesReportCoordinator$addItemRows$2;

    invoke-direct {v3, v10}, Lcom/squareup/salesreport/SalesReportCoordinator$addItemRows$2;-><init>(Lcom/squareup/salesreport/SalesReportScreen;)V

    move-object/from16 v18, v3

    check-cast v18, Lkotlin/jvm/functions/Function1;

    .line 635
    new-instance v3, Lcom/squareup/salesreport/SalesReportCoordinator$addItemRows$3;

    invoke-direct {v3, v10}, Lcom/squareup/salesreport/SalesReportCoordinator$addItemRows$3;-><init>(Lcom/squareup/salesreport/SalesReportScreen;)V

    move-object/from16 v19, v3

    check-cast v19, Lkotlin/jvm/functions/Function1;

    move-object v14, v2

    .line 630
    invoke-direct/range {v14 .. v19}, Lcom/squareup/salesreport/util/SalesReportRow$TwoTabToggleRow;-><init>(Ljava/lang/String;Ljava/lang/String;ZLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 629
    invoke-interface {v11, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 640
    :goto_2
    check-cast v0, Lcom/squareup/customreport/data/WithSalesTopItemsReport;

    invoke-virtual {v0}, Lcom/squareup/customreport/data/WithSalesTopItemsReport;->getTopItems()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-virtual/range {p6 .. p6}, Lcom/squareup/salesreport/SalesReportState$ViewCount;->getCount()I

    move-result v2

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->take(Ljava/lang/Iterable;I)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 1355
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v14

    const/4 v15, 0x0

    :goto_3
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    add-int/lit8 v16, v15, 0x1

    if-gez v15, :cond_3

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_3
    move-object/from16 v17, v0

    check-cast v17, Lcom/squareup/customreport/data/SalesItem;

    const/16 v18, 0x0

    .line 648
    invoke-virtual/range {v17 .. v17}, Lcom/squareup/customreport/data/SalesItem;->getSubItems()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/lit8 v19, v0, 0x1

    .line 649
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v12, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v20

    .line 650
    new-instance v21, Lcom/squareup/salesreport/SalesReportCoordinator$addItemRows$$inlined$forEachIndexed$lambda$1;

    move-object/from16 v0, v21

    move v1, v15

    move-object/from16 v2, p0

    move-object/from16 v3, p2

    move-object/from16 v4, p4

    move/from16 v5, p5

    move-object/from16 v6, p7

    move-object/from16 v7, p1

    invoke-direct/range {v0 .. v7}, Lcom/squareup/salesreport/SalesReportCoordinator$addItemRows$$inlined$forEachIndexed$lambda$1;-><init>(ILcom/squareup/salesreport/SalesReportCoordinator;Ljava/util/List;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;ZLjava/util/Set;Lcom/squareup/salesreport/SalesReportScreen;)V

    check-cast v21, Lkotlin/jvm/functions/Function1;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, v17

    move-object/from16 v3, p4

    move/from16 v4, p5

    move/from16 v5, v18

    move/from16 v6, v19

    move/from16 v7, v20

    move-object/from16 v8, v21

    .line 642
    invoke-direct/range {v0 .. v8}, Lcom/squareup/salesreport/SalesReportCoordinator;->addSalesItemRow(Ljava/util/List;Lcom/squareup/customreport/data/SalesItem;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;ZZZZLkotlin/jvm/functions/Function1;)V

    .line 652
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v12, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 653
    invoke-virtual/range {v17 .. v17}, Lcom/squareup/customreport/data/SalesItem;->getSubItems()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 1356
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_4
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/customreport/data/SalesItem;

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 662
    sget-object v0, Lcom/squareup/salesreport/SalesReportCoordinator$addItemRows$4$2$1;->INSTANCE:Lcom/squareup/salesreport/SalesReportCoordinator$addItemRows$4$2$1;

    move-object v8, v0

    check-cast v8, Lkotlin/jvm/functions/Function1;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v3, p4

    move/from16 v4, p5

    .line 654
    invoke-direct/range {v0 .. v8}, Lcom/squareup/salesreport/SalesReportCoordinator;->addSalesItemRow(Ljava/util/List;Lcom/squareup/customreport/data/SalesItem;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;ZZZZLkotlin/jvm/functions/Function1;)V

    goto :goto_4

    :cond_4
    move-object/from16 v8, p4

    move/from16 v15, v16

    goto/16 :goto_3

    :cond_5
    return-void
.end method

.method private final addPaymentMethodRows(Lcom/squareup/salesreport/SalesReportScreen;Ljava/util/List;Lcom/squareup/customreport/data/SalesPaymentMethodsReport;Z)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/salesreport/SalesReportScreen;",
            "Ljava/util/List<",
            "Lcom/squareup/salesreport/util/SalesReportRow;",
            ">;",
            "Lcom/squareup/customreport/data/SalesPaymentMethodsReport;",
            "Z)V"
        }
    .end annotation

    .line 928
    instance-of v0, p3, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;

    if-eqz v0, :cond_4

    .line 930
    new-instance v0, Lcom/squareup/salesreport/util/SalesReportRow$SectionHeaderWithButtonRow;

    .line 931
    iget-object v1, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/salesreport/impl/R$string;->sales_report_payment_methods_header_uppercase:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    const-string v3, ""

    move-object v1, v0

    .line 930
    invoke-direct/range {v1 .. v6}, Lcom/squareup/salesreport/util/SalesReportRow$SectionHeaderWithButtonRow;-><init>(Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 929
    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 937
    new-instance v0, Lcom/squareup/salesreport/util/SalesReportRow$PaymentMethodTotalRow;

    .line 938
    iget-object v1, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/salesreport/impl/R$string;->sales_report_payment_method_total_collected:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 939
    iget-object v2, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->fullMoneyFormatter:Lcom/squareup/text/Formatter;

    check-cast p3, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;

    invoke-virtual {p3}, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;->getTotalCollectedMoney()Lcom/squareup/protos/common/Money;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 937
    invoke-direct {v0, v1, v2}, Lcom/squareup/salesreport/util/SalesReportRow$PaymentMethodTotalRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 936
    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 943
    invoke-virtual {p3}, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;->getPaymentMethods()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 1396
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/customreport/data/SalesPaymentMethod;

    if-eqz p4, :cond_0

    .line 946
    new-instance v2, Lcom/squareup/salesreport/util/SalesReportRow$TruncatedPaymentMethodRow;

    .line 947
    iget-object v3, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->res:Lcom/squareup/util/Res;

    invoke-virtual {v1}, Lcom/squareup/customreport/data/SalesPaymentMethod;->getPaymentMethod()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    move-result-object v4

    invoke-static {v4}, Lcom/squareup/salesreport/util/PaymentMethodsKt;->getTitle(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;)I

    move-result v4

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 948
    invoke-virtual {v1}, Lcom/squareup/customreport/data/SalesPaymentMethod;->getPercentage()Lcom/squareup/util/Percentage;

    move-result-object v4

    .line 949
    iget-object v5, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->fullMoneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {v1}, Lcom/squareup/customreport/data/SalesPaymentMethod;->getNetCollected()Lcom/squareup/protos/common/Money;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 950
    invoke-virtual {v1}, Lcom/squareup/customreport/data/SalesPaymentMethod;->getPaymentMethod()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/salesreport/SalesReportCoordinatorKt;->access$getColorResId$p(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;)I

    move-result v1

    .line 946
    invoke-direct {v2, v3, v4, v5, v1}, Lcom/squareup/salesreport/util/SalesReportRow$TruncatedPaymentMethodRow;-><init>(Ljava/lang/String;Lcom/squareup/util/Percentage;Ljava/lang/String;I)V

    .line 945
    invoke-interface {p2, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 955
    :cond_0
    new-instance v2, Lcom/squareup/salesreport/util/SalesReportRow$PaymentMethodRow;

    .line 956
    iget-object v3, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->res:Lcom/squareup/util/Res;

    invoke-virtual {v1}, Lcom/squareup/customreport/data/SalesPaymentMethod;->getPaymentMethod()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    move-result-object v4

    invoke-static {v4}, Lcom/squareup/salesreport/util/PaymentMethodsKt;->getTitle(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;)I

    move-result v4

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 957
    invoke-virtual {v1}, Lcom/squareup/customreport/data/SalesPaymentMethod;->getPercentage()Lcom/squareup/util/Percentage;

    move-result-object v4

    .line 958
    iget-object v5, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->fullMoneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {v1}, Lcom/squareup/customreport/data/SalesPaymentMethod;->getNetCollected()Lcom/squareup/protos/common/Money;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 959
    invoke-virtual {v1}, Lcom/squareup/customreport/data/SalesPaymentMethod;->getPaymentMethod()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/salesreport/SalesReportCoordinatorKt;->access$getColorResId$p(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;)I

    move-result v1

    .line 955
    invoke-direct {v2, v3, v4, v5, v1}, Lcom/squareup/salesreport/util/SalesReportRow$PaymentMethodRow;-><init>(Ljava/lang/String;Lcom/squareup/util/Percentage;Ljava/lang/String;I)V

    .line 954
    invoke-interface {p2, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 965
    :cond_1
    invoke-virtual {p3}, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;->getFees()Lcom/squareup/protos/common/Money;

    move-result-object p4

    if-eqz p4, :cond_3

    .line 967
    invoke-virtual {p1}, Lcom/squareup/salesreport/SalesReportScreen;->getShowFeesLink()Z

    move-result p4

    if-eqz p4, :cond_2

    .line 968
    new-instance p4, Lcom/squareup/salesreport/util/SalesReportRow$FeesRow;

    .line 969
    iget-object v0, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/salesreport/impl/R$string;->sales_report_payment_method_fees:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 970
    iget-object v1, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->fullMoneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p3}, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;->getFees()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 971
    new-instance v2, Lcom/squareup/salesreport/SalesReportCoordinator$addPaymentMethodRows$2;

    invoke-direct {v2, p1}, Lcom/squareup/salesreport/SalesReportCoordinator$addPaymentMethodRows$2;-><init>(Lcom/squareup/salesreport/SalesReportScreen;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 968
    invoke-direct {p4, v0, v1, v2}, Lcom/squareup/salesreport/util/SalesReportRow$FeesRow;-><init>(Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    check-cast p4, Lcom/squareup/salesreport/util/SalesReportRow;

    goto :goto_1

    .line 974
    :cond_2
    new-instance p1, Lcom/squareup/salesreport/util/SalesReportRow$PaymentMethodTotalRow;

    .line 975
    iget-object p4, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/salesreport/impl/R$string;->sales_report_payment_method_fees:I

    invoke-interface {p4, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p4

    .line 976
    iget-object v0, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->fullMoneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p3}, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;->getFees()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 974
    invoke-direct {p1, p4, v0}, Lcom/squareup/salesreport/util/SalesReportRow$PaymentMethodTotalRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object p4, p1

    check-cast p4, Lcom/squareup/salesreport/util/SalesReportRow;

    .line 966
    :goto_1
    invoke-interface {p2, p4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 983
    :cond_3
    new-instance p1, Lcom/squareup/salesreport/util/SalesReportRow$PaymentMethodTotalRow;

    .line 984
    iget-object p4, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/salesreport/impl/R$string;->sales_report_payment_method_net_total:I

    invoke-interface {p4, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p4

    .line 985
    iget-object v0, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->fullMoneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p3}, Lcom/squareup/customreport/data/WithSalesPaymentMethodsReport;->getNetTotalMoney()Lcom/squareup/protos/common/Money;

    move-result-object p3

    invoke-interface {v0, p3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p3

    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p3

    .line 983
    invoke-direct {p1, p4, p3}, Lcom/squareup/salesreport/util/SalesReportRow$PaymentMethodTotalRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 982
    invoke-interface {p2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    return-void
.end method

.method private final addSalesItemRow(Ljava/util/List;Lcom/squareup/customreport/data/SalesItem;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;ZZZZLkotlin/jvm/functions/Function1;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/salesreport/util/SalesReportRow;",
            ">;",
            "Lcom/squareup/customreport/data/SalesItem;",
            "Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;",
            "ZZZZ",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Landroid/view/View;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    if-nez p4, :cond_0

    .line 758
    new-instance v13, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$FullSalesDetailsRow;

    .line 759
    new-instance v1, Lcom/squareup/util/ViewString$TextString;

    .line 760
    iget-object v2, v0, Lcom/squareup/salesreport/SalesReportCoordinator;->nameOrTranslationTypeFormatter:Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;

    invoke-virtual/range {p2 .. p2}, Lcom/squareup/customreport/data/SalesItem;->getName()Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;->formatName(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    .line 759
    invoke-direct {v1, v2}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    move-object v2, v1

    check-cast v2, Lcom/squareup/util/ViewString;

    .line 762
    iget-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator;->fullNumberFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual/range {p2 .. p2}, Lcom/squareup/customreport/data/SalesItem;->getCount()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v1, v3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 763
    iget-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator;->fullMoneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual/range {p2 .. p2}, Lcom/squareup/customreport/data/SalesItem;->getGrossSales()Lcom/squareup/protos/common/Money;

    move-result-object v3

    invoke-interface {v1, v3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v6, 0x0

    const/4 v4, 0x0

    const/16 v11, 0x10

    const/4 v12, 0x0

    move-object v1, v13

    move/from16 v7, p5

    move/from16 v8, p6

    move/from16 v9, p7

    move-object/from16 v10, p8

    .line 758
    invoke-direct/range {v1 .. v12}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$FullSalesDetailsRow;-><init>(Lcom/squareup/util/ViewString;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/Integer;ZZZLkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v13, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow;

    :goto_0
    move-object v1, p1

    goto :goto_2

    .line 771
    :cond_0
    new-instance v9, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;

    .line 772
    new-instance v1, Lcom/squareup/util/ViewString$TextString;

    .line 773
    iget-object v2, v0, Lcom/squareup/salesreport/SalesReportCoordinator;->nameOrTranslationTypeFormatter:Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;

    invoke-virtual/range {p2 .. p2}, Lcom/squareup/customreport/data/SalesItem;->getName()Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;->formatName(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    .line 772
    invoke-direct {v1, v2}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    move-object v2, v1

    check-cast v2, Lcom/squareup/util/ViewString;

    .line 775
    sget-object v1, Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;->GROSS:Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;

    move-object/from16 v3, p3

    if-ne v3, v1, :cond_1

    .line 776
    iget-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator;->fullMoneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual/range {p2 .. p2}, Lcom/squareup/customreport/data/SalesItem;->getGrossSales()Lcom/squareup/protos/common/Money;

    move-result-object v3

    invoke-interface {v1, v3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 777
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 779
    :cond_1
    iget-object v1, v0, Lcom/squareup/salesreport/SalesReportCoordinator;->fullNumberFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual/range {p2 .. p2}, Lcom/squareup/customreport/data/SalesItem;->getCount()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v1, v3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 780
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_1
    move-object v3, v1

    const/4 v4, 0x0

    move-object v1, v9

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    .line 771
    invoke-direct/range {v1 .. v8}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$TruncatedSalesDetailsRow;-><init>(Lcom/squareup/util/ViewString;Ljava/lang/String;ZZZZLkotlin/jvm/functions/Function1;)V

    move-object v13, v9

    check-cast v13, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow;

    goto :goto_0

    .line 756
    :goto_2
    invoke-interface {p1, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private final addTopSection(Lcom/squareup/salesreport/SalesReportScreen;Ljava/util/List;Lcom/squareup/customreport/data/WithSalesReport;Lcom/squareup/salesreport/SalesReportState$TopSectionState;ZZ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/salesreport/SalesReportScreen;",
            "Ljava/util/List<",
            "Lcom/squareup/salesreport/util/SalesReportRow;",
            ">;",
            "Lcom/squareup/customreport/data/WithSalesReport;",
            "Lcom/squareup/salesreport/SalesReportState$TopSectionState;",
            "ZZ)V"
        }
    .end annotation

    .line 450
    iget-object v0, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->res:Lcom/squareup/util/Res;

    invoke-static {p4}, Lcom/squareup/salesreport/SalesReportCoordinatorKt;->access$getTitleRes$p(Lcom/squareup/salesreport/SalesReportState$TopSectionState;)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz p5, :cond_1

    if-eqz p6, :cond_0

    .line 454
    new-instance p1, Lcom/squareup/salesreport/util/SalesReportRow$SectionHeaderWithImageButtonRow;

    .line 456
    iget-object p5, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->res:Lcom/squareup/util/Res;

    .line 457
    sget v1, Lcom/squareup/salesreport/impl/R$string;->sales_report_summary_overflow_options_button_content_description:I

    .line 456
    invoke-interface {p5, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p5

    .line 459
    new-instance v1, Lcom/squareup/salesreport/SalesReportCoordinator$addTopSection$1;

    invoke-direct {v1, p0}, Lcom/squareup/salesreport/SalesReportCoordinator$addTopSection$1;-><init>(Lcom/squareup/salesreport/SalesReportCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    .line 454
    invoke-direct {p1, v0, p5, v1}, Lcom/squareup/salesreport/util/SalesReportRow$SectionHeaderWithImageButtonRow;-><init>(Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    check-cast p1, Lcom/squareup/salesreport/util/SalesReportRow;

    goto :goto_0

    .line 464
    :cond_0
    new-instance p5, Lcom/squareup/salesreport/util/SalesReportRow$SectionHeaderWithButtonRow;

    .line 466
    iget-object v1, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->res:Lcom/squareup/util/Res;

    invoke-static {p4}, Lcom/squareup/salesreport/SalesReportCoordinatorKt;->access$getButtonRes$p(Lcom/squareup/salesreport/SalesReportState$TopSectionState;)I

    move-result v2

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 467
    new-instance v2, Lcom/squareup/salesreport/SalesReportCoordinator$addTopSection$2;

    invoke-direct {v2, p1}, Lcom/squareup/salesreport/SalesReportCoordinator$addTopSection$2;-><init>(Lcom/squareup/salesreport/SalesReportScreen;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 464
    invoke-direct {p5, v0, v1, v2}, Lcom/squareup/salesreport/util/SalesReportRow$SectionHeaderWithButtonRow;-><init>(Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    move-object p1, p5

    check-cast p1, Lcom/squareup/salesreport/util/SalesReportRow;

    goto :goto_0

    .line 471
    :cond_1
    new-instance p1, Lcom/squareup/salesreport/util/SalesReportRow$SectionHeaderRow;

    invoke-direct {p1, v0}, Lcom/squareup/salesreport/util/SalesReportRow$SectionHeaderRow;-><init>(Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/salesreport/util/SalesReportRow;

    .line 451
    :goto_0
    invoke-interface {p2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 474
    sget-object p1, Lcom/squareup/salesreport/SalesReportCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p4}, Lcom/squareup/salesreport/SalesReportState$TopSectionState;->ordinal()I

    move-result p4

    aget p1, p1, p4

    const/4 p4, 0x1

    if-eq p1, p4, :cond_4

    const/4 p4, 0x2

    if-eq p1, p4, :cond_2

    goto :goto_2

    :cond_2
    if-eqz p6, :cond_3

    .line 486
    invoke-virtual {p3}, Lcom/squareup/customreport/data/WithSalesReport;->getSalesSummaryReport()Lcom/squareup/customreport/data/WithSalesSummaryReport;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/customreport/data/WithSalesSummaryReport;->getSalesSummary()Lcom/squareup/customreport/data/SalesSummary;

    move-result-object p1

    .line 487
    iget-object p4, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->fullMoneyFormatter:Lcom/squareup/text/Formatter;

    .line 488
    iget-object p5, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 489
    iget-object v0, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->features:Lcom/squareup/settings/server/Features;

    .line 486
    invoke-static {p1, p4, p5, v0}, Lcom/squareup/salesreport/util/SalesSummariesKt;->truncatedSalesDetailsRows(Lcom/squareup/customreport/data/SalesSummary;Lcom/squareup/text/Formatter;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;)Ljava/util/List;

    move-result-object p1

    goto :goto_1

    .line 492
    :cond_3
    invoke-virtual {p3}, Lcom/squareup/customreport/data/WithSalesReport;->getSalesSummaryReport()Lcom/squareup/customreport/data/WithSalesSummaryReport;

    move-result-object p1

    .line 493
    iget-object p4, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->fullMoneyFormatter:Lcom/squareup/text/Formatter;

    .line 494
    iget-object p5, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->percentageChangeFormatter:Lcom/squareup/salesreport/util/PercentageChangeFormatter;

    .line 495
    iget-object v0, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 496
    iget-object v1, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->features:Lcom/squareup/settings/server/Features;

    .line 492
    invoke-static {p1, p4, p5, v0, v1}, Lcom/squareup/salesreport/util/SalesSummaryReportsKt;->salesDetailsRows(Lcom/squareup/customreport/data/WithSalesSummaryReport;Lcom/squareup/text/Formatter;Lcom/squareup/salesreport/util/PercentageChangeFormatter;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;)Ljava/util/List;

    move-result-object p1

    .line 485
    :goto_1
    check-cast p1, Ljava/util/Collection;

    .line 498
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->toMutableList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object p1

    .line 501
    invoke-virtual {p3}, Lcom/squareup/customreport/data/WithSalesReport;->getDiscountsReport()Lcom/squareup/customreport/data/SalesDiscountsReport;

    move-result-object p3

    .line 499
    invoke-direct {p0, p1, p3, p6}, Lcom/squareup/salesreport/SalesReportCoordinator;->addDiscountRows(Ljava/util/List;Lcom/squareup/customreport/data/SalesDiscountsReport;Z)V

    .line 504
    check-cast p1, Ljava/util/Collection;

    invoke-interface {p2, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    .line 477
    :cond_4
    invoke-virtual {p3}, Lcom/squareup/customreport/data/WithSalesReport;->getSalesSummaryReport()Lcom/squareup/customreport/data/WithSalesSummaryReport;

    move-result-object p1

    .line 478
    iget-object p3, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->compactNumberFormatter:Lcom/squareup/text/Formatter;

    .line 479
    iget-object p4, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->compactMoneyFormatter:Lcom/squareup/text/Formatter;

    .line 480
    iget-object p5, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->percentageChangeFormatter:Lcom/squareup/salesreport/util/PercentageChangeFormatter;

    .line 477
    invoke-static {p1, p3, p4, p5}, Lcom/squareup/salesreport/util/SalesSummaryReportsKt;->salesOverviewRows(Lcom/squareup/customreport/data/WithSalesSummaryReport;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/salesreport/util/PercentageChangeFormatter;)Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/util/Collection;

    .line 476
    invoke-interface {p2, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :goto_2
    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 327
    sget v0, Lcom/squareup/salesreport/impl/R$id;->action_bar_with_two_action_icons:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "mainView.findViewById(R.\u2026ar_with_two_action_icons)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/salesreport/widget/NohoActionBarWithTwoActionIcons;

    iput-object v0, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->actionBar:Lcom/squareup/salesreport/widget/NohoActionBarWithTwoActionIcons;

    .line 329
    sget v0, Lcom/squareup/salesreport/impl/R$id;->sales_report_animator:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "mainView.findViewById(R.id.sales_report_animator)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/widgets/SquareViewAnimator;

    iput-object v0, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->reportAnimator:Lcom/squareup/widgets/SquareViewAnimator;

    .line 331
    sget v0, Lcom/squareup/salesreport/impl/R$id;->sales_report_details_empty_message_view:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "mainView.findViewById(R.\u2026tails_empty_message_view)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/noho/NohoMessageView;

    iput-object v0, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->reportDetailsEmptyMessageView:Lcom/squareup/noho/NohoMessageView;

    .line 333
    sget v0, Lcom/squareup/salesreport/impl/R$id;->sales_report_details_failure_message_view:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "mainView.findViewById(R.\u2026ils_failure_message_view)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/noho/NohoMessageView;

    iput-object v0, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->reportDetailsFailureMessageView:Lcom/squareup/noho/NohoMessageView;

    .line 335
    sget v0, Lcom/squareup/salesreport/impl/R$id;->sales_report_top_bar_title_container:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "mainView.findViewById(R.\u2026_top_bar_title_container)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->topBarTitleContainer:Landroid/view/ViewGroup;

    .line 336
    sget v0, Lcom/squareup/salesreport/impl/R$id;->sales_report_top_bar_customize_icon:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "mainView.findViewById(R.\u2026t_top_bar_customize_icon)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->customizeReportIcon:Landroid/widget/ImageView;

    .line 337
    sget v0, Lcom/squareup/salesreport/impl/R$id;->sales_report_top_bar_compare_icon:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "mainView.findViewById(R.\u2026ort_top_bar_compare_icon)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->comparisonRangeIcon:Landroid/widget/ImageView;

    .line 338
    sget v0, Lcom/squareup/salesreport/impl/R$id;->sales_report_top_bar_compare_icon_spacer:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "mainView.findViewById(R.\u2026_bar_compare_icon_spacer)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->comparisonRangeSpacer:Landroid/view/View;

    .line 339
    sget v0, Lcom/squareup/salesreport/impl/R$id;->sales_report_time_selector_container:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "mainView.findViewById(R.\u2026_time_selector_container)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/salesreport/widget/RangeSelectionRow;

    iput-object v0, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->rangeSelectionRow:Lcom/squareup/salesreport/widget/RangeSelectionRow;

    .line 341
    sget v0, Lcom/squareup/salesreport/impl/R$id;->sales_report_top_bar_title:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "mainView.findViewById(R.\u2026les_report_top_bar_title)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->topBarTitleLabel:Landroid/widget/TextView;

    .line 342
    sget v0, Lcom/squareup/salesreport/impl/R$id;->sales_report_top_bar_subtitle:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "mainView.findViewById(R.\u2026_report_top_bar_subtitle)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->topBarSubtitleLabel:Landroid/widget/TextView;

    .line 344
    invoke-direct {p0, p1}, Lcom/squareup/salesreport/SalesReportCoordinator;->setupSalesReportRecyclerView(Landroid/view/View;)V

    .line 345
    new-instance v0, Lcom/squareup/salesreport/util/DashboardUrlLauncher;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string v1, "mainView.context"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->res:Lcom/squareup/util/Res;

    invoke-direct {v0, p1, v1}, Lcom/squareup/salesreport/util/DashboardUrlLauncher;-><init>(Landroid/content/Context;Lcom/squareup/util/Res;)V

    iput-object v0, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->dashboardUrlLauncher:Lcom/squareup/salesreport/util/DashboardUrlLauncher;

    return-void
.end method

.method private final configureActionBar(Lcom/squareup/salesreport/SalesReportScreen;ZZLcom/squareup/salesreport/SalesReportState;)V
    .locals 11

    .line 312
    iget-object v0, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->actionBar:Lcom/squareup/salesreport/widget/NohoActionBarWithTwoActionIcons;

    const-string v1, "actionBar"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 298
    :cond_0
    new-instance v10, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v10}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 300
    new-instance v2, Lcom/squareup/util/ViewString$ResourceString;

    sget v3, Lcom/squareup/salesreport/impl/R$string;->sales_report_action_bar_title:I

    invoke-direct {v2, v3}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v2, Lcom/squareup/resources/TextModel;

    invoke-virtual {v10, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 302
    sget v3, Lcom/squareup/salesreport/impl/R$drawable;->icon_export_24:I

    .line 303
    new-instance v2, Lcom/squareup/util/ViewString$ResourceString;

    sget v4, Lcom/squareup/salesreport/impl/R$string;->export_report_label:I

    invoke-direct {v2, v4}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    move-object v4, v2

    check-cast v4, Lcom/squareup/resources/TextModel;

    .line 304
    instance-of v5, p4, Lcom/squareup/salesreport/SalesReportState$ViewingReport;

    const/4 v6, 0x0

    .line 305
    new-instance v2, Lcom/squareup/salesreport/SalesReportCoordinator$configureActionBar$$inlined$apply$lambda$1;

    invoke-direct {v2, p0, p4, p1, p3}, Lcom/squareup/salesreport/SalesReportCoordinator$configureActionBar$$inlined$apply$lambda$1;-><init>(Lcom/squareup/salesreport/SalesReportCoordinator;Lcom/squareup/salesreport/SalesReportState;Lcom/squareup/salesreport/SalesReportScreen;Z)V

    move-object v7, v2

    check-cast v7, Lkotlin/jvm/functions/Function0;

    const/16 v8, 0x8

    const/4 v9, 0x0

    move-object v2, v10

    .line 301
    invoke-static/range {v2 .. v9}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setActionIcon$default(Lcom/squareup/noho/NohoActionBar$Config$Builder;ILcom/squareup/resources/TextModel;ZILkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 308
    iget-object v2, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->backButtonConfig:Lcom/squareup/salesreport/util/SalesReportBackButtonConfig;

    invoke-interface {v2}, Lcom/squareup/salesreport/util/SalesReportBackButtonConfig;->getAlwaysShowBackButton()Z

    move-result v2

    if-nez v2, :cond_1

    if-eqz p3, :cond_2

    .line 309
    :cond_1
    iget-object v2, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->backButtonConfig:Lcom/squareup/salesreport/util/SalesReportBackButtonConfig;

    invoke-interface {v2}, Lcom/squareup/salesreport/util/SalesReportBackButtonConfig;->getIcon()Lcom/squareup/noho/UpIcon;

    move-result-object v2

    new-instance v3, Lcom/squareup/salesreport/SalesReportCoordinator$configureActionBar$$inlined$apply$lambda$2;

    invoke-direct {v3, p0, p4, p1, p3}, Lcom/squareup/salesreport/SalesReportCoordinator$configureActionBar$$inlined$apply$lambda$2;-><init>(Lcom/squareup/salesreport/SalesReportCoordinator;Lcom/squareup/salesreport/SalesReportState;Lcom/squareup/salesreport/SalesReportScreen;Z)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v10, v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 312
    :cond_2
    invoke-virtual {v10}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/salesreport/widget/NohoActionBarWithTwoActionIcons;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    if-eqz p2, :cond_4

    .line 314
    iget-object p1, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->actionBar:Lcom/squareup/salesreport/widget/NohoActionBarWithTwoActionIcons;

    if-nez p1, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 315
    :cond_3
    sget p2, Lcom/squareup/salesreport/impl/R$drawable;->icon_compare_24:I

    .line 316
    new-instance p3, Lcom/squareup/util/ViewString$ResourceString;

    sget v0, Lcom/squareup/salesreport/impl/R$string;->compare_report_label:I

    invoke-direct {p3, v0}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast p3, Lcom/squareup/util/ViewString;

    .line 317
    invoke-static {p4}, Lcom/squareup/salesreport/SalesReportCoordinatorKt;->access$getShouldShowComparisonOption$p(Lcom/squareup/salesreport/SalesReportState;)Z

    move-result p4

    .line 318
    new-instance v0, Lcom/squareup/salesreport/SalesReportCoordinator$configureActionBar$2;

    invoke-direct {v0, p0}, Lcom/squareup/salesreport/SalesReportCoordinator$configureActionBar$2;-><init>(Lcom/squareup/salesreport/SalesReportCoordinator;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    .line 314
    invoke-virtual {p1, p2, p3, p4, v0}, Lcom/squareup/salesreport/widget/NohoActionBarWithTwoActionIcons;->setSecondActionIcon(ILcom/squareup/util/ViewString;ZLkotlin/jvm/functions/Function0;)V

    goto :goto_0

    .line 322
    :cond_4
    iget-object p1, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->actionBar:Lcom/squareup/salesreport/widget/NohoActionBarWithTwoActionIcons;

    if-nez p1, :cond_5

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {p1}, Lcom/squareup/salesreport/widget/NohoActionBarWithTwoActionIcons;->hideSecondActionIcon()V

    :goto_0
    return-void
.end method

.method private final getCategoryActions(Lcom/squareup/salesreport/SalesReportScreen;Lcom/squareup/salesreport/SalesReportState$UiSelectionState;I)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/salesreport/SalesReportScreen;",
            "Lcom/squareup/salesreport/SalesReportState$UiSelectionState;",
            "I)",
            "Ljava/util/List<",
            "Lcom/squareup/salesreport/widget/PopupAction;",
            ">;"
        }
    .end annotation

    .line 864
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 865
    invoke-virtual {p2}, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->getCategoryViewCount()Lcom/squareup/salesreport/SalesReportState$ViewCount;

    move-result-object v1

    .line 866
    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportState$ViewCount;->getCount()I

    move-result v2

    invoke-static {p3, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 868
    invoke-virtual {p2}, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->getCategoriesWithItemsShown()Ljava/util/Set;

    move-result-object p2

    .line 867
    invoke-direct {p0, p2, v2}, Lcom/squareup/salesreport/SalesReportCoordinator;->visibleExpandedRowCount(Ljava/util/Set;I)I

    move-result p2

    if-lez p3, :cond_0

    if-ge p2, v2, :cond_0

    .line 875
    new-instance v2, Lcom/squareup/salesreport/widget/PopupAction;

    iget-object v3, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/salesreport/impl/R$string;->sales_report_view_expand_category_details:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/squareup/salesreport/SalesReportCoordinator$getCategoryActions$1;

    invoke-direct {v4, p1}, Lcom/squareup/salesreport/SalesReportCoordinator$getCategoryActions$1;-><init>(Lcom/squareup/salesreport/SalesReportScreen;)V

    check-cast v4, Lkotlin/jvm/functions/Function0;

    invoke-direct {v2, v3, v4}, Lcom/squareup/salesreport/widget/PopupAction;-><init>(Ljava/lang/String;Lkotlin/jvm/functions/Function0;)V

    .line 874
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    if-lez p3, :cond_1

    if-lez p2, :cond_1

    .line 884
    new-instance p2, Lcom/squareup/salesreport/widget/PopupAction;

    iget-object v2, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/salesreport/impl/R$string;->sales_report_view_collapse_category_details:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/squareup/salesreport/SalesReportCoordinator$getCategoryActions$2;

    invoke-direct {v3, p1}, Lcom/squareup/salesreport/SalesReportCoordinator$getCategoryActions$2;-><init>(Lcom/squareup/salesreport/SalesReportScreen;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-direct {p2, v2, v3}, Lcom/squareup/salesreport/widget/PopupAction;-><init>(Ljava/lang/String;Lkotlin/jvm/functions/Function0;)V

    .line 883
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 891
    :cond_1
    invoke-static {}, Lcom/squareup/salesreport/SalesReportState$ViewCount;->values()[Lcom/squareup/salesreport/SalesReportState$ViewCount;

    move-result-object p2

    .line 892
    invoke-static {p2}, Lkotlin/collections/ArraysKt;->toList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p2

    check-cast p2, Ljava/lang/Iterable;

    .line 1389
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    check-cast v2, Ljava/util/Collection;

    .line 1390
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_2
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v4, v3

    check-cast v4, Lcom/squareup/salesreport/SalesReportState$ViewCount;

    if-eq v4, v1, :cond_3

    .line 893
    invoke-direct {p0, v4}, Lcom/squareup/salesreport/SalesReportCoordinator;->getPopupActionThreshold(Lcom/squareup/salesreport/SalesReportState$ViewCount;)I

    move-result v4

    if-le p3, v4, :cond_3

    const/4 v4, 0x1

    goto :goto_1

    :cond_3
    const/4 v4, 0x0

    :goto_1
    if-eqz v4, :cond_2

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1391
    :cond_4
    check-cast v2, Ljava/util/List;

    check-cast v2, Ljava/lang/Iterable;

    .line 1392
    new-instance p2, Ljava/util/ArrayList;

    const/16 p3, 0xa

    invoke-static {v2, p3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result p3

    invoke-direct {p2, p3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p2, Ljava/util/Collection;

    .line 1393
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_2
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 1394
    check-cast v1, Lcom/squareup/salesreport/SalesReportState$ViewCount;

    .line 895
    new-instance v2, Lcom/squareup/salesreport/widget/PopupAction;

    iget-object v3, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->res:Lcom/squareup/util/Res;

    invoke-static {v1}, Lcom/squareup/salesreport/SalesReportCoordinatorKt;->access$getActionDescription$p(Lcom/squareup/salesreport/SalesReportState$ViewCount;)I

    move-result v4

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/squareup/salesreport/SalesReportCoordinator$getCategoryActions$$inlined$map$lambda$1;

    invoke-direct {v4, v1, p0, v0, p1}, Lcom/squareup/salesreport/SalesReportCoordinator$getCategoryActions$$inlined$map$lambda$1;-><init>(Lcom/squareup/salesreport/SalesReportState$ViewCount;Lcom/squareup/salesreport/SalesReportCoordinator;Ljava/util/List;Lcom/squareup/salesreport/SalesReportScreen;)V

    check-cast v4, Lkotlin/jvm/functions/Function0;

    invoke-direct {v2, v3, v4}, Lcom/squareup/salesreport/widget/PopupAction;-><init>(Ljava/lang/String;Lkotlin/jvm/functions/Function0;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-result v1

    .line 897
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1395
    :cond_5
    check-cast p2, Ljava/util/List;

    .line 900
    iget-object p1, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->dashboardUrlLauncher:Lcom/squareup/salesreport/util/DashboardUrlLauncher;

    if-nez p1, :cond_6

    const-string p2, "dashboardUrlLauncher"

    invoke-static {p2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    invoke-virtual {p1}, Lcom/squareup/salesreport/util/DashboardUrlLauncher;->canOpenUrls()Z

    move-result p1

    if-eqz p1, :cond_7

    .line 902
    new-instance p1, Lcom/squareup/salesreport/widget/PopupAction;

    iget-object p2, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->res:Lcom/squareup/util/Res;

    sget p3, Lcom/squareup/salesreport/impl/R$string;->sales_report_view_in_dashboard:I

    invoke-interface {p2, p3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    new-instance p3, Lcom/squareup/salesreport/SalesReportCoordinator$getCategoryActions$5;

    invoke-direct {p3, p0}, Lcom/squareup/salesreport/SalesReportCoordinator$getCategoryActions$5;-><init>(Lcom/squareup/salesreport/SalesReportCoordinator;)V

    check-cast p3, Lkotlin/jvm/functions/Function0;

    invoke-direct {p1, p2, p3}, Lcom/squareup/salesreport/widget/PopupAction;-><init>(Ljava/lang/String;Lkotlin/jvm/functions/Function0;)V

    .line 901
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_7
    return-object v0
.end method

.method private final getChartActions(Lcom/squareup/salesreport/SalesReportScreen;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/salesreport/SalesReportScreen;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/salesreport/widget/PopupAction;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/salesreport/widget/PopupAction;

    .line 582
    new-instance v1, Lcom/squareup/salesreport/widget/PopupAction;

    iget-object v2, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/salesreport/impl/R$string;->sales_report_chart_type_gross_sales:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/squareup/salesreport/SalesReportCoordinator$getChartActions$1;

    invoke-direct {v3, p1}, Lcom/squareup/salesreport/SalesReportCoordinator$getChartActions$1;-><init>(Lcom/squareup/salesreport/SalesReportScreen;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-direct {v1, v2, v3}, Lcom/squareup/salesreport/widget/PopupAction;-><init>(Ljava/lang/String;Lkotlin/jvm/functions/Function0;)V

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 585
    new-instance v1, Lcom/squareup/salesreport/widget/PopupAction;

    iget-object v2, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/salesreport/impl/R$string;->sales_report_chart_type_net_sales:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/squareup/salesreport/SalesReportCoordinator$getChartActions$2;

    invoke-direct {v3, p1}, Lcom/squareup/salesreport/SalesReportCoordinator$getChartActions$2;-><init>(Lcom/squareup/salesreport/SalesReportScreen;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-direct {v1, v2, v3}, Lcom/squareup/salesreport/widget/PopupAction;-><init>(Ljava/lang/String;Lkotlin/jvm/functions/Function0;)V

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 588
    new-instance v1, Lcom/squareup/salesreport/widget/PopupAction;

    iget-object v2, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/salesreport/impl/R$string;->sales_report_chart_type_sales_count:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/squareup/salesreport/SalesReportCoordinator$getChartActions$3;

    invoke-direct {v3, p1}, Lcom/squareup/salesreport/SalesReportCoordinator$getChartActions$3;-><init>(Lcom/squareup/salesreport/SalesReportScreen;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-direct {v1, v2, v3}, Lcom/squareup/salesreport/widget/PopupAction;-><init>(Ljava/lang/String;Lkotlin/jvm/functions/Function0;)V

    const/4 p1, 0x2

    aput-object v1, v0, p1

    .line 581
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method private final getCurrentDate()Lorg/threeten/bp/LocalDate;
    .locals 1

    .line 1185
    iget-object v0, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->currentTime:Lcom/squareup/time/CurrentTime;

    invoke-interface {v0}, Lcom/squareup/time/CurrentTime;->localDate()Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0
.end method

.method private final getItemActions(Lcom/squareup/salesreport/SalesReportScreen;Lcom/squareup/salesreport/SalesReportState$UiSelectionState;I)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/salesreport/SalesReportScreen;",
            "Lcom/squareup/salesreport/SalesReportState$UiSelectionState;",
            "I)",
            "Ljava/util/List<",
            "Lcom/squareup/salesreport/widget/PopupAction;",
            ">;"
        }
    .end annotation

    .line 811
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 812
    invoke-virtual {p2}, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->getItemViewCount()Lcom/squareup/salesreport/SalesReportState$ViewCount;

    move-result-object v1

    .line 813
    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportState$ViewCount;->getCount()I

    move-result v2

    invoke-static {p3, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 815
    invoke-virtual {p2}, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->getItemsWithVariationsShown()Ljava/util/Set;

    move-result-object p2

    .line 814
    invoke-direct {p0, p2, v2}, Lcom/squareup/salesreport/SalesReportCoordinator;->visibleExpandedRowCount(Ljava/util/Set;I)I

    move-result p2

    if-lez v2, :cond_0

    if-ge p2, v2, :cond_0

    .line 822
    new-instance v3, Lcom/squareup/salesreport/widget/PopupAction;

    iget-object v4, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->res:Lcom/squareup/util/Res;

    sget v5, Lcom/squareup/salesreport/impl/R$string;->sales_report_view_expand_item_details:I

    invoke-interface {v4, v5}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/squareup/salesreport/SalesReportCoordinator$getItemActions$1;

    invoke-direct {v5, p1}, Lcom/squareup/salesreport/SalesReportCoordinator$getItemActions$1;-><init>(Lcom/squareup/salesreport/SalesReportScreen;)V

    check-cast v5, Lkotlin/jvm/functions/Function0;

    invoke-direct {v3, v4, v5}, Lcom/squareup/salesreport/widget/PopupAction;-><init>(Ljava/lang/String;Lkotlin/jvm/functions/Function0;)V

    .line 821
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    if-lez v2, :cond_1

    if-lez p2, :cond_1

    .line 831
    new-instance p2, Lcom/squareup/salesreport/widget/PopupAction;

    iget-object v2, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/salesreport/impl/R$string;->sales_report_view_collapse_item_details:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/squareup/salesreport/SalesReportCoordinator$getItemActions$2;

    invoke-direct {v3, p1}, Lcom/squareup/salesreport/SalesReportCoordinator$getItemActions$2;-><init>(Lcom/squareup/salesreport/SalesReportScreen;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-direct {p2, v2, v3}, Lcom/squareup/salesreport/widget/PopupAction;-><init>(Ljava/lang/String;Lkotlin/jvm/functions/Function0;)V

    .line 830
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 838
    :cond_1
    invoke-static {}, Lcom/squareup/salesreport/SalesReportState$ViewCount;->values()[Lcom/squareup/salesreport/SalesReportState$ViewCount;

    move-result-object p2

    .line 839
    invoke-static {p2}, Lkotlin/collections/ArraysKt;->toList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p2

    check-cast p2, Ljava/lang/Iterable;

    .line 1382
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    check-cast v2, Ljava/util/Collection;

    .line 1383
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_2
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v4, v3

    check-cast v4, Lcom/squareup/salesreport/SalesReportState$ViewCount;

    if-eq v4, v1, :cond_3

    .line 840
    invoke-direct {p0, v4}, Lcom/squareup/salesreport/SalesReportCoordinator;->getPopupActionThreshold(Lcom/squareup/salesreport/SalesReportState$ViewCount;)I

    move-result v4

    if-le p3, v4, :cond_3

    const/4 v4, 0x1

    goto :goto_1

    :cond_3
    const/4 v4, 0x0

    :goto_1
    if-eqz v4, :cond_2

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1384
    :cond_4
    check-cast v2, Ljava/util/List;

    check-cast v2, Ljava/lang/Iterable;

    .line 1385
    new-instance p2, Ljava/util/ArrayList;

    const/16 p3, 0xa

    invoke-static {v2, p3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result p3

    invoke-direct {p2, p3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p2, Ljava/util/Collection;

    .line 1386
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_2
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 1387
    check-cast v1, Lcom/squareup/salesreport/SalesReportState$ViewCount;

    .line 842
    new-instance v2, Lcom/squareup/salesreport/widget/PopupAction;

    iget-object v3, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->res:Lcom/squareup/util/Res;

    invoke-static {v1}, Lcom/squareup/salesreport/SalesReportCoordinatorKt;->access$getActionDescription$p(Lcom/squareup/salesreport/SalesReportState$ViewCount;)I

    move-result v4

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/squareup/salesreport/SalesReportCoordinator$getItemActions$$inlined$map$lambda$1;

    invoke-direct {v4, v1, p0, v0, p1}, Lcom/squareup/salesreport/SalesReportCoordinator$getItemActions$$inlined$map$lambda$1;-><init>(Lcom/squareup/salesreport/SalesReportState$ViewCount;Lcom/squareup/salesreport/SalesReportCoordinator;Ljava/util/List;Lcom/squareup/salesreport/SalesReportScreen;)V

    check-cast v4, Lkotlin/jvm/functions/Function0;

    invoke-direct {v2, v3, v4}, Lcom/squareup/salesreport/widget/PopupAction;-><init>(Ljava/lang/String;Lkotlin/jvm/functions/Function0;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-result v1

    .line 844
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1388
    :cond_5
    check-cast p2, Ljava/util/List;

    .line 847
    iget-object p1, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->dashboardUrlLauncher:Lcom/squareup/salesreport/util/DashboardUrlLauncher;

    if-nez p1, :cond_6

    const-string p2, "dashboardUrlLauncher"

    invoke-static {p2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    invoke-virtual {p1}, Lcom/squareup/salesreport/util/DashboardUrlLauncher;->canOpenUrls()Z

    move-result p1

    if-eqz p1, :cond_7

    .line 849
    new-instance p1, Lcom/squareup/salesreport/widget/PopupAction;

    iget-object p2, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->res:Lcom/squareup/util/Res;

    sget p3, Lcom/squareup/salesreport/impl/R$string;->sales_report_view_in_dashboard:I

    invoke-interface {p2, p3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    new-instance p3, Lcom/squareup/salesreport/SalesReportCoordinator$getItemActions$5;

    invoke-direct {p3, p0}, Lcom/squareup/salesreport/SalesReportCoordinator$getItemActions$5;-><init>(Lcom/squareup/salesreport/SalesReportCoordinator;)V

    check-cast p3, Lkotlin/jvm/functions/Function0;

    invoke-direct {p1, p2, p3}, Lcom/squareup/salesreport/widget/PopupAction;-><init>(Ljava/lang/String;Lkotlin/jvm/functions/Function0;)V

    .line 848
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_7
    return-object v0
.end method

.method private final getPopupActionThreshold(Lcom/squareup/salesreport/SalesReportState$ViewCount;)I
    .locals 1

    .line 917
    sget-object v0, Lcom/squareup/salesreport/SalesReportCoordinator$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p1}, Lcom/squareup/salesreport/SalesReportState$ViewCount;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 919
    sget-object p1, Lcom/squareup/salesreport/SalesReportState$ViewCount;->VIEW_COUNT_TEN:Lcom/squareup/salesreport/SalesReportState$ViewCount;

    invoke-virtual {p1}, Lcom/squareup/salesreport/SalesReportState$ViewCount;->getCount()I

    move-result p1

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 918
    :cond_1
    sget-object p1, Lcom/squareup/salesreport/SalesReportState$ViewCount;->VIEW_COUNT_FIVE:Lcom/squareup/salesreport/SalesReportState$ViewCount;

    invoke-virtual {p1}, Lcom/squareup/salesreport/SalesReportState$ViewCount;->getCount()I

    move-result p1

    :goto_0
    return p1
.end method

.method private final onScreen(Lcom/squareup/salesreport/SalesReportScreen;ZZLandroid/view/View;)V
    .locals 10

    .line 1068
    iget-object v0, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->comparisonRangeIcon:Landroid/widget/ImageView;

    const-string v1, "comparisonRangeIcon"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setSelected(Z)V

    const-string v0, "customizeReportIcon"

    const-string v3, "topBarTitleLabel"

    if-nez p2, :cond_5

    .line 1070
    iget-object v4, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->topBarTitleLabel:Landroid/widget/TextView;

    if-nez v4, :cond_1

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    iget-object v5, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->res:Lcom/squareup/util/Res;

    sget v6, Lcom/squareup/noho/R$color;->noho_label_default:I

    invoke-interface {v5, v6}, Lcom/squareup/util/Res;->getColor(I)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1071
    invoke-virtual {p1}, Lcom/squareup/salesreport/SalesReportScreen;->getUiDisplayState()Lcom/squareup/salesreport/UiDisplayState;

    move-result-object v4

    invoke-virtual {v4}, Lcom/squareup/salesreport/UiDisplayState;->getShowCompareTo()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1072
    sget-object v4, Lcom/squareup/salesreport/SalesReportCoordinator$ComparisonIconState;->VISIBLE:Lcom/squareup/salesreport/SalesReportCoordinator$ComparisonIconState;

    invoke-direct {p0, v4}, Lcom/squareup/salesreport/SalesReportCoordinator;->setComparisonRangeIconVisibility(Lcom/squareup/salesreport/SalesReportCoordinator$ComparisonIconState;)V

    goto :goto_0

    .line 1074
    :cond_2
    sget-object v4, Lcom/squareup/salesreport/SalesReportCoordinator$ComparisonIconState;->SPACER:Lcom/squareup/salesreport/SalesReportCoordinator$ComparisonIconState;

    invoke-direct {p0, v4}, Lcom/squareup/salesreport/SalesReportCoordinator;->setComparisonRangeIconVisibility(Lcom/squareup/salesreport/SalesReportCoordinator$ComparisonIconState;)V

    .line 1076
    :goto_0
    iget-object v4, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->customizeReportIcon:Landroid/widget/ImageView;

    if-nez v4, :cond_3

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    check-cast v4, Landroid/view/View;

    invoke-static {v4}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 1078
    iget-object v4, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->customizeReportIcon:Landroid/widget/ImageView;

    if-nez v4, :cond_4

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    check-cast v4, Landroid/view/View;

    .line 1402
    new-instance v0, Lcom/squareup/salesreport/SalesReportCoordinator$onScreen$$inlined$onClickDebounced$1;

    invoke-direct {v0, p1}, Lcom/squareup/salesreport/SalesReportCoordinator$onScreen$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/salesreport/SalesReportScreen;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 1082
    :cond_5
    iget-object v4, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->topBarTitleLabel:Landroid/widget/TextView;

    if-nez v4, :cond_6

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    iget-object v5, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->res:Lcom/squareup/util/Res;

    sget v6, Lcom/squareup/salesreport/impl/R$color;->sales_report_top_bar_title_text_color:I

    invoke-interface {v5, v6}, Lcom/squareup/util/Res;->getColor(I)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1083
    sget-object v4, Lcom/squareup/salesreport/SalesReportCoordinator$ComparisonIconState;->GONE:Lcom/squareup/salesreport/SalesReportCoordinator$ComparisonIconState;

    invoke-direct {p0, v4}, Lcom/squareup/salesreport/SalesReportCoordinator;->setComparisonRangeIconVisibility(Lcom/squareup/salesreport/SalesReportCoordinator$ComparisonIconState;)V

    .line 1084
    iget-object v4, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->customizeReportIcon:Landroid/widget/ImageView;

    if-nez v4, :cond_7

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    check-cast v4, Landroid/view/View;

    invoke-static {v4}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 1086
    :goto_1
    iget-object v0, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->topBarTitleContainer:Landroid/view/ViewGroup;

    if-nez v0, :cond_8

    const-string v4, "topBarTitleContainer"

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    check-cast v0, Landroid/view/View;

    .line 1409
    new-instance v4, Lcom/squareup/salesreport/SalesReportCoordinator$onScreen$$inlined$onClickDebounced$2;

    invoke-direct {v4, p1}, Lcom/squareup/salesreport/SalesReportCoordinator$onScreen$$inlined$onClickDebounced$2;-><init>(Lcom/squareup/salesreport/SalesReportScreen;)V

    check-cast v4, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1090
    invoke-virtual {p1}, Lcom/squareup/salesreport/SalesReportScreen;->getUiDisplayState()Lcom/squareup/salesreport/UiDisplayState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/salesreport/UiDisplayState;->getShowQuickDateSelections()Z

    move-result v0

    const-string v4, "rangeSelectionRow"

    if-eqz v0, :cond_b

    .line 1091
    iget-object v0, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->rangeSelectionRow:Lcom/squareup/salesreport/widget/RangeSelectionRow;

    if-nez v0, :cond_9

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 1092
    iget-object v0, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->rangeSelectionRow:Lcom/squareup/salesreport/widget/RangeSelectionRow;

    if-nez v0, :cond_a

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_a
    new-instance v5, Lcom/squareup/salesreport/SalesReportCoordinator$onScreen$3;

    invoke-direct {v5, p0, p1}, Lcom/squareup/salesreport/SalesReportCoordinator$onScreen$3;-><init>(Lcom/squareup/salesreport/SalesReportCoordinator;Lcom/squareup/salesreport/SalesReportScreen;)V

    check-cast v5, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v5}, Lcom/squareup/salesreport/widget/RangeSelectionRow;->setRangeSelectionHandler(Lkotlin/jvm/functions/Function1;)V

    goto :goto_2

    .line 1096
    :cond_b
    iget-object v0, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->rangeSelectionRow:Lcom/squareup/salesreport/widget/RangeSelectionRow;

    if-nez v0, :cond_c

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_c
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 1099
    :goto_2
    iget-object v0, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->reportDetailsEmptyMessageView:Lcom/squareup/noho/NohoMessageView;

    if-nez v0, :cond_d

    const-string v5, "reportDetailsEmptyMessageView"

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 1100
    :cond_d
    new-instance v5, Lcom/squareup/salesreport/SalesReportCoordinator$onScreen$4;

    invoke-direct {v5, p1}, Lcom/squareup/salesreport/SalesReportCoordinator$onScreen$4;-><init>(Lcom/squareup/salesreport/SalesReportScreen;)V

    check-cast v5, Ljava/lang/Runnable;

    invoke-static {v5}, Lcom/squareup/debounce/Debouncers;->debounceRunnable(Ljava/lang/Runnable;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v5

    const-string v6, "Debouncers.debounceRunna\u2026nEvent(CustomizeReport) }"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1099
    invoke-virtual {v0, v5}, Lcom/squareup/noho/NohoMessageView;->setPrimaryButtonOnClickListener(Lcom/squareup/debounce/DebouncedOnClickListener;)V

    .line 1102
    iget-object v0, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->reportDetailsFailureMessageView:Lcom/squareup/noho/NohoMessageView;

    if-nez v0, :cond_e

    const-string v5, "reportDetailsFailureMessageView"

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 1103
    :cond_e
    new-instance v5, Lcom/squareup/salesreport/SalesReportCoordinator$onScreen$5;

    invoke-direct {v5, p1}, Lcom/squareup/salesreport/SalesReportCoordinator$onScreen$5;-><init>(Lcom/squareup/salesreport/SalesReportScreen;)V

    check-cast v5, Ljava/lang/Runnable;

    invoke-static {v5}, Lcom/squareup/debounce/Debouncers;->debounceRunnable(Ljava/lang/Runnable;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v5

    const-string v6, "Debouncers.debounceRunna\u2026n.onEvent(ReloadReport) }"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1102
    invoke-virtual {v0, v5}, Lcom/squareup/noho/NohoMessageView;->setPrimaryButtonOnClickListener(Lcom/squareup/debounce/DebouncedOnClickListener;)V

    .line 1106
    invoke-virtual {p1}, Lcom/squareup/salesreport/SalesReportScreen;->getState()Lcom/squareup/salesreport/SalesReportState;

    move-result-object v0

    if-eqz p2, :cond_f

    .line 1109
    invoke-virtual {p1}, Lcom/squareup/salesreport/SalesReportScreen;->getUiDisplayState()Lcom/squareup/salesreport/UiDisplayState;

    move-result-object v5

    invoke-virtual {v5}, Lcom/squareup/salesreport/UiDisplayState;->getShowCompareTo()Z

    move-result v5

    if-eqz v5, :cond_f

    const/4 v2, 0x1

    .line 1107
    :cond_f
    invoke-direct {p0, p1, v2, p3, v0}, Lcom/squareup/salesreport/SalesReportCoordinator;->configureActionBar(Lcom/squareup/salesreport/SalesReportScreen;ZZLcom/squareup/salesreport/SalesReportState;)V

    .line 1113
    iget-object v2, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->topBarTitleLabel:Landroid/widget/TextView;

    if-nez v2, :cond_10

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_10
    invoke-virtual {v0}, Lcom/squareup/salesreport/SalesReportState;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v3

    iget-object v5, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->localTimeFormatter:Lcom/squareup/salesreport/util/LocalTimeFormatter;

    iget-object v6, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->res:Lcom/squareup/util/Res;

    iget-object v7, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->localeProvider:Ljavax/inject/Provider;

    invoke-static {v3, v5, v6, v7}, Lcom/squareup/salesreport/util/ReportConfigsKt;->title(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/salesreport/util/LocalTimeFormatter;Lcom/squareup/util/Res;Ljavax/inject/Provider;)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1114
    iget-object v2, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->topBarSubtitleLabel:Landroid/widget/TextView;

    if-nez v2, :cond_11

    const-string v3, "topBarSubtitleLabel"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_11
    invoke-virtual {v0}, Lcom/squareup/salesreport/SalesReportState;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v3

    .line 1115
    iget-object v5, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->res:Lcom/squareup/util/Res;

    .line 1116
    iget-object v6, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->localeProvider:Ljavax/inject/Provider;

    .line 1117
    iget-object v7, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-static {v7}, Lcom/squareup/salesreport/util/EmployeeManagementsKt;->isEnabled(Lcom/squareup/permissions/EmployeeManagement;)Z

    move-result v7

    .line 1118
    iget-object v8, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->localTimeFormatter:Lcom/squareup/salesreport/util/LocalTimeFormatter;

    .line 1114
    invoke-static {v3, v5, v6, v8, v7}, Lcom/squareup/salesreport/util/ReportConfigsKt;->configDescription(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/util/Res;Ljavax/inject/Provider;Lcom/squareup/salesreport/util/LocalTimeFormatter;Z)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1120
    iget-object v2, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->rangeSelectionRow:Lcom/squareup/salesreport/widget/RangeSelectionRow;

    if-nez v2, :cond_12

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_12
    invoke-virtual {v0}, Lcom/squareup/salesreport/SalesReportState;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/customreport/data/ReportConfig;->getRangeSelection()Lcom/squareup/customreport/data/RangeSelection;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/salesreport/widget/RangeSelectionRow;->setSelectedTime(Lcom/squareup/customreport/data/RangeSelection;)V

    .line 1121
    iget-object v2, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->comparisonRangeIcon:Landroid/widget/ImageView;

    if-nez v2, :cond_13

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_13
    invoke-static {v0}, Lcom/squareup/salesreport/SalesReportCoordinatorKt;->access$getShouldShowComparisonOption$p(Lcom/squareup/salesreport/SalesReportState;)Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1122
    invoke-static {v0}, Lcom/squareup/salesreport/SalesReportCoordinatorKt;->access$getShouldShowComparisonOption$p(Lcom/squareup/salesreport/SalesReportState;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 1123
    iget-object v2, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->comparisonRangeIcon:Landroid/widget/ImageView;

    if-nez v2, :cond_14

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_14
    check-cast v2, Landroid/view/View;

    .line 1416
    new-instance v1, Lcom/squareup/salesreport/SalesReportCoordinator$onScreen$$inlined$with$lambda$1;

    move-object v3, v1

    move-object v4, p0

    move-object v5, p1

    move v6, p2

    move v7, p3

    move-object v8, p4

    invoke-direct/range {v3 .. v8}, Lcom/squareup/salesreport/SalesReportCoordinator$onScreen$$inlined$with$lambda$1;-><init>(Lcom/squareup/salesreport/SalesReportCoordinator;Lcom/squareup/salesreport/SalesReportScreen;ZZLandroid/view/View;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1127
    :cond_15
    invoke-static {v0}, Lcom/squareup/salesreport/SalesReportCoordinatorKt;->access$getWithSalesReport$p(Lcom/squareup/salesreport/SalesReportState;)Lcom/squareup/customreport/data/WithSalesReport;

    move-result-object p3

    if-eqz p3, :cond_16

    .line 1130
    invoke-virtual {v0}, Lcom/squareup/salesreport/SalesReportState;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v5

    .line 1131
    invoke-virtual {v0}, Lcom/squareup/salesreport/SalesReportState;->getUiSelectionState()Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    move-result-object v7

    .line 1132
    invoke-virtual {p4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v8

    const-string/jumbo v1, "view.context"

    invoke-static {v8, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v3, p0

    move-object v4, p1

    move-object v6, p3

    move v9, p2

    .line 1128
    invoke-direct/range {v3 .. v9}, Lcom/squareup/salesreport/SalesReportCoordinator;->updateActions(Lcom/squareup/salesreport/SalesReportScreen;Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/customreport/data/WithSalesReport;Lcom/squareup/salesreport/SalesReportState$UiSelectionState;Landroid/content/Context;Z)V

    .line 1139
    invoke-virtual {v0}, Lcom/squareup/salesreport/SalesReportState;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v6

    .line 1140
    invoke-virtual {v0}, Lcom/squareup/salesreport/SalesReportState;->getUiSelectionState()Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    move-result-object v7

    .line 1141
    invoke-virtual {p1}, Lcom/squareup/salesreport/SalesReportScreen;->getUiDisplayState()Lcom/squareup/salesreport/UiDisplayState;

    move-result-object v8

    move-object v5, p3

    .line 1136
    invoke-direct/range {v3 .. v9}, Lcom/squareup/salesreport/SalesReportCoordinator;->updateSalesReportRecycler(Lcom/squareup/salesreport/SalesReportScreen;Lcom/squareup/customreport/data/WithSalesReport;Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/salesreport/SalesReportState$UiSelectionState;Lcom/squareup/salesreport/UiDisplayState;Z)V

    .line 1146
    :cond_16
    iget-object p2, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->reportAnimator:Lcom/squareup/widgets/SquareViewAnimator;

    if-nez p2, :cond_17

    const-string p3, "reportAnimator"

    invoke-static {p3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 1148
    :cond_17
    instance-of p3, v0, Lcom/squareup/salesreport/SalesReportState$ViewingReport;

    if-eqz p3, :cond_18

    goto :goto_3

    :cond_18
    instance-of p3, v0, Lcom/squareup/salesreport/SalesReportState$ExportingReport;

    if-eqz p3, :cond_19

    goto :goto_3

    :cond_19
    instance-of p3, v0, Lcom/squareup/salesreport/SalesReportState$CustomizingReport;

    if-eqz p3, :cond_1a

    goto :goto_3

    :cond_1a
    instance-of p3, v0, Lcom/squareup/salesreport/SalesReportState$ViewingFeesNote;

    if-eqz p3, :cond_1b

    .line 1149
    :goto_3
    sget p3, Lcom/squareup/salesreport/impl/R$id;->sales_report_recycler_view:I

    goto :goto_5

    .line 1151
    :cond_1b
    instance-of p3, v0, Lcom/squareup/salesreport/SalesReportState$InitialState;

    if-eqz p3, :cond_1c

    goto :goto_4

    :cond_1c
    instance-of p3, v0, Lcom/squareup/salesreport/SalesReportState$LoadingReport;

    if-eqz p3, :cond_1d

    :goto_4
    sget p3, Lcom/squareup/salesreport/impl/R$id;->sales_report_progress_bar:I

    goto :goto_5

    .line 1152
    :cond_1d
    instance-of p3, v0, Lcom/squareup/salesreport/SalesReportState$EmptyReport;

    if-eqz p3, :cond_1e

    sget p3, Lcom/squareup/salesreport/impl/R$id;->sales_report_details_empty_message_view:I

    goto :goto_5

    .line 1153
    :cond_1e
    instance-of p3, v0, Lcom/squareup/salesreport/SalesReportState$NetworkFailure;

    if-eqz p3, :cond_1f

    sget p3, Lcom/squareup/salesreport/impl/R$id;->sales_report_details_failure_message_view:I

    .line 1146
    :goto_5
    invoke-virtual {p2, p3}, Lcom/squareup/widgets/SquareViewAnimator;->setDisplayedChildById(I)V

    .line 1158
    new-instance p2, Lcom/squareup/salesreport/SalesReportCoordinator$onScreen$7;

    invoke-direct {p2, p1}, Lcom/squareup/salesreport/SalesReportCoordinator$onScreen$7;-><init>(Lcom/squareup/salesreport/SalesReportScreen;)V

    check-cast p2, Lkotlin/jvm/functions/Function0;

    invoke-static {p4, p2}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void

    .line 1153
    :cond_1f
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final setComparisonRangeIconVisibility(Lcom/squareup/salesreport/SalesReportCoordinator$ComparisonIconState;)V
    .locals 3

    .line 1162
    sget-object v0, Lcom/squareup/salesreport/SalesReportCoordinator$WhenMappings;->$EnumSwitchMapping$2:[I

    invoke-virtual {p1}, Lcom/squareup/salesreport/SalesReportCoordinator$ComparisonIconState;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    const-string v1, "comparisonRangeSpacer"

    const-string v2, "comparisonRangeIcon"

    if-eq p1, v0, :cond_6

    const/4 v0, 0x2

    if-eq p1, v0, :cond_3

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    goto :goto_0

    .line 1172
    :cond_0
    iget-object p1, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->comparisonRangeIcon:Landroid/widget/ImageView;

    if-nez p1, :cond_1

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 1173
    iget-object p1, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->comparisonRangeSpacer:Landroid/view/View;

    if-nez p1, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    goto :goto_0

    .line 1168
    :cond_3
    iget-object p1, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->comparisonRangeIcon:Landroid/widget/ImageView;

    if-nez p1, :cond_4

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 1169
    iget-object p1, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->comparisonRangeSpacer:Landroid/view/View;

    if-nez p1, :cond_5

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    goto :goto_0

    .line 1164
    :cond_6
    iget-object p1, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->comparisonRangeIcon:Landroid/widget/ImageView;

    if-nez p1, :cond_7

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 1165
    iget-object p1, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->comparisonRangeSpacer:Landroid/view/View;

    if-nez p1, :cond_8

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    :goto_0
    return-void
.end method

.method private final setupSalesReportRecyclerView(Landroid/view/View;)V
    .locals 12

    .line 350
    iget-object v0, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    sget v1, Lcom/squareup/salesreport/impl/R$id;->sales_report_recycler_view:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v1, "mainView.findViewById(R.\u2026les_report_recycler_view)"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView;

    .line 1243
    sget-object v1, Lcom/squareup/cycler/Recycler;->Companion:Lcom/squareup/cycler/Recycler$Companion;

    .line 1244
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1245
    new-instance v1, Lcom/squareup/cycler/Recycler$Config;

    invoke-direct {v1}, Lcom/squareup/cycler/Recycler$Config;-><init>()V

    .line 1249
    invoke-virtual {v0}, Lcom/squareup/recycler/RecyclerFactory;->getMainDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/cycler/Recycler$Config;->setMainDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 1250
    invoke-virtual {v0}, Lcom/squareup/recycler/RecyclerFactory;->getBackgroundDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->setBackgroundDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 1253
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v2, Lcom/squareup/salesreport/SalesReportCoordinator$$special$$inlined$row$1;->INSTANCE:Lcom/squareup/salesreport/SalesReportCoordinator$$special$$inlined$row$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v2}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 351
    invoke-static {v0}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt;->createSectionHeaderRow(Lcom/squareup/cycler/StandardRowSpec;)V

    .line 1253
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 1252
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 1259
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v2, Lcom/squareup/salesreport/SalesReportCoordinator$$special$$inlined$row$2;->INSTANCE:Lcom/squareup/salesreport/SalesReportCoordinator$$special$$inlined$row$2;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v2}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 352
    invoke-static {v0}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt;->createSectionHeaderWithButtonRow(Lcom/squareup/cycler/StandardRowSpec;)V

    .line 1259
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 1258
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 1265
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v2, Lcom/squareup/salesreport/SalesReportCoordinator$$special$$inlined$row$3;->INSTANCE:Lcom/squareup/salesreport/SalesReportCoordinator$$special$$inlined$row$3;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v2}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 353
    invoke-static {v0}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt;->createSectionHeaderWithImageButtonRow(Lcom/squareup/cycler/StandardRowSpec;)V

    .line 1265
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 1264
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 1271
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v2, Lcom/squareup/salesreport/SalesReportCoordinator$$special$$inlined$row$4;->INSTANCE:Lcom/squareup/salesreport/SalesReportCoordinator$$special$$inlined$row$4;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v2}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 354
    invoke-static {v0}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt;->createSalesOverviewRow(Lcom/squareup/cycler/StandardRowSpec;)V

    .line 1271
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 1270
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 1277
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v2, Lcom/squareup/salesreport/SalesReportCoordinator$$special$$inlined$row$5;->INSTANCE:Lcom/squareup/salesreport/SalesReportCoordinator$$special$$inlined$row$5;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v2}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 355
    invoke-static {p0}, Lcom/squareup/salesreport/SalesReportCoordinator;->access$getResources$p(Lcom/squareup/salesreport/SalesReportCoordinator;)Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt;->createSalesDetailsRow(Lcom/squareup/cycler/StandardRowSpec;Landroid/content/res/Resources;)V

    .line 1277
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 1276
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 1283
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v2, Lcom/squareup/salesreport/SalesReportCoordinator$$special$$inlined$row$6;->INSTANCE:Lcom/squareup/salesreport/SalesReportCoordinator$$special$$inlined$row$6;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v2}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 357
    invoke-static {p0}, Lcom/squareup/salesreport/SalesReportCoordinator;->access$getResources$p(Lcom/squareup/salesreport/SalesReportCoordinator;)Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt;->createTruncatedSalesDetailsRow(Lcom/squareup/cycler/StandardRowSpec;Landroid/content/res/Resources;)V

    .line 1283
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 1282
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 1289
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v2, Lcom/squareup/salesreport/SalesReportCoordinator$$special$$inlined$row$7;->INSTANCE:Lcom/squareup/salesreport/SalesReportCoordinator$$special$$inlined$row$7;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v2}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 361
    invoke-static {p0}, Lcom/squareup/salesreport/SalesReportCoordinator;->access$getLocaleProvider$p(Lcom/squareup/salesreport/SalesReportCoordinator;)Ljavax/inject/Provider;

    move-result-object v3

    .line 362
    invoke-static {p0}, Lcom/squareup/salesreport/SalesReportCoordinator;->access$getLocalTimeFormatter$p(Lcom/squareup/salesreport/SalesReportCoordinator;)Lcom/squareup/salesreport/util/LocalTimeFormatter;

    move-result-object v4

    .line 363
    invoke-static {p0}, Lcom/squareup/salesreport/SalesReportCoordinator;->access$getFullMoneyFormatter$p(Lcom/squareup/salesreport/SalesReportCoordinator;)Lcom/squareup/text/Formatter;

    move-result-object v6

    .line 364
    invoke-static {p0}, Lcom/squareup/salesreport/SalesReportCoordinator;->access$getFullNumberFormatter$p(Lcom/squareup/salesreport/SalesReportCoordinator;)Lcom/squareup/text/Formatter;

    move-result-object v7

    .line 365
    invoke-static {p0}, Lcom/squareup/salesreport/SalesReportCoordinator;->access$getCompactShorterMoneyFormatter$p(Lcom/squareup/salesreport/SalesReportCoordinator;)Lcom/squareup/text/Formatter;

    move-result-object v8

    .line 366
    invoke-static {p0}, Lcom/squareup/salesreport/SalesReportCoordinator;->access$getCompactNumberFormatter$p(Lcom/squareup/salesreport/SalesReportCoordinator;)Lcom/squareup/text/Formatter;

    move-result-object v9

    .line 367
    invoke-static {p0}, Lcom/squareup/salesreport/SalesReportCoordinator;->access$getSettings$p(Lcom/squareup/salesreport/SalesReportCoordinator;)Lcom/squareup/settings/server/AccountStatusSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v2

    const-string v5, "settings.userSettings"

    invoke-static {v2, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/squareup/settings/server/UserSettings;->getCurrency()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v10

    const-string v2, "settings.userSettings.currency"

    invoke-static {v10, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 368
    invoke-static {p0}, Lcom/squareup/salesreport/SalesReportCoordinator;->access$getRes$p(Lcom/squareup/salesreport/SalesReportCoordinator;)Lcom/squareup/util/Res;

    move-result-object v11

    .line 369
    invoke-static {p0}, Lcom/squareup/salesreport/SalesReportCoordinator;->access$getCurrent24HourClockMode$p(Lcom/squareup/salesreport/SalesReportCoordinator;)Lcom/squareup/time/Current24HourClockMode;

    move-result-object v5

    move-object v2, v0

    .line 360
    invoke-static/range {v2 .. v11}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt;->createSalesChartRow(Lcom/squareup/cycler/StandardRowSpec;Ljavax/inject/Provider;Lcom/squareup/salesreport/util/LocalTimeFormatter;Lcom/squareup/time/Current24HourClockMode;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/util/Res;)V

    .line 1289
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 1288
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 1295
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v2, Lcom/squareup/salesreport/SalesReportCoordinator$$special$$inlined$row$8;->INSTANCE:Lcom/squareup/salesreport/SalesReportCoordinator$$special$$inlined$row$8;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v2}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 372
    invoke-static {v0}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt;->createSubsectionHeaderRow(Lcom/squareup/cycler/StandardRowSpec;)V

    .line 1295
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 1294
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 1301
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v2, Lcom/squareup/salesreport/SalesReportCoordinator$$special$$inlined$row$9;->INSTANCE:Lcom/squareup/salesreport/SalesReportCoordinator$$special$$inlined$row$9;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v2}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 373
    invoke-static {p0}, Lcom/squareup/salesreport/SalesReportCoordinator;->access$getRes$p(Lcom/squareup/salesreport/SalesReportCoordinator;)Lcom/squareup/util/Res;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt;->createTwoTabToggleRow(Lcom/squareup/cycler/StandardRowSpec;Lcom/squareup/util/Res;)V

    .line 1301
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 1300
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 1307
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v2, Lcom/squareup/salesreport/SalesReportCoordinator$$special$$inlined$row$10;->INSTANCE:Lcom/squareup/salesreport/SalesReportCoordinator$$special$$inlined$row$10;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v2}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 375
    invoke-static {p0}, Lcom/squareup/salesreport/SalesReportCoordinator;->access$getWholeNumberPercentageFormatter$p(Lcom/squareup/salesreport/SalesReportCoordinator;)Lcom/squareup/text/Formatter;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt;->createPaymentMethodRow(Lcom/squareup/cycler/StandardRowSpec;Lcom/squareup/text/Formatter;)V

    .line 1307
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 1306
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 1313
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v2, Lcom/squareup/salesreport/SalesReportCoordinator$$special$$inlined$row$11;->INSTANCE:Lcom/squareup/salesreport/SalesReportCoordinator$$special$$inlined$row$11;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v2}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 377
    invoke-static {v0}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt;->createTruncatedPaymentMethodRow(Lcom/squareup/cycler/StandardRowSpec;)V

    .line 1313
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 1312
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 1319
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v2, Lcom/squareup/salesreport/SalesReportCoordinator$$special$$inlined$row$12;->INSTANCE:Lcom/squareup/salesreport/SalesReportCoordinator$$special$$inlined$row$12;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v2}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 378
    invoke-static {v0}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt;->createPaymentMethodTotalRow(Lcom/squareup/cycler/StandardRowSpec;)V

    .line 1319
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 1318
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 1325
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v2, Lcom/squareup/salesreport/SalesReportCoordinator$$special$$inlined$row$13;->INSTANCE:Lcom/squareup/salesreport/SalesReportCoordinator$$special$$inlined$row$13;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v2}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 379
    invoke-static {v0}, Lcom/squareup/salesreport/util/SalesReportStandardRowSpecsKt;->createFeesRow(Lcom/squareup/cycler/StandardRowSpec;)V

    .line 1325
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 1324
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 1331
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v2, Lcom/squareup/salesreport/SalesReportCoordinator$$special$$inlined$extraItem$1;->INSTANCE:Lcom/squareup/salesreport/SalesReportCoordinator$$special$$inlined$extraItem$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v2}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 380
    sget v2, Lcom/squareup/salesreport/impl/R$layout;->sales_report_recycler_view_space:I

    .line 1333
    new-instance v3, Lcom/squareup/salesreport/SalesReportCoordinator$$special$$inlined$create$1;

    invoke-direct {v3, v2}, Lcom/squareup/salesreport/SalesReportCoordinator$$special$$inlined$create$1;-><init>(I)V

    check-cast v3, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v3}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 1331
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 1330
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->extraItem(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 1247
    invoke-virtual {v1, p1}, Lcom/squareup/cycler/Recycler$Config;->setUp(Landroidx/recyclerview/widget/RecyclerView;)Lcom/squareup/cycler/Recycler;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->salesReportRecycler:Lcom/squareup/cycler/Recycler;

    return-void

    .line 1244
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "RecyclerView needs a layoutManager assigned."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final showSummaryOverviewDetailsBottomSheetDialog(Landroid/view/View;)V
    .locals 2

    .line 510
    iget-object v0, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->overviewDetailsActions:Lcom/squareup/salesreport/widget/PopupActions;

    if-nez v0, :cond_0

    const-string v1, "overviewDetailsActions"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, p1}, Lcom/squareup/salesreport/widget/PopupActions;->toggle(Landroid/view/View;)V

    return-void
.end method

.method private final updateActions(Lcom/squareup/salesreport/SalesReportScreen;Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/customreport/data/WithSalesReport;Lcom/squareup/salesreport/SalesReportState$UiSelectionState;Landroid/content/Context;Z)V
    .locals 6

    .line 1000
    iget-object v0, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->res:Lcom/squareup/util/Res;

    .line 1002
    invoke-virtual {p2}, Lcom/squareup/customreport/data/ReportConfig;->getRangeSelection()Lcom/squareup/customreport/data/RangeSelection;

    move-result-object p2

    invoke-static {p2}, Lcom/squareup/salesreport/util/RangeSelectionsKt;->getPossibleComparisonRanges(Lcom/squareup/customreport/data/RangeSelection;)Ljava/util/List;

    move-result-object p2

    check-cast p2, Ljava/lang/Iterable;

    .line 1398
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {p2, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 1399
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 1400
    check-cast v2, Lcom/squareup/customreport/data/ComparisonRange;

    .line 1003
    new-instance v3, Lcom/squareup/salesreport/widget/PopupAction;

    iget-object v4, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->res:Lcom/squareup/util/Res;

    invoke-static {v2, v4}, Lcom/squareup/salesreport/util/ComparisonRangesKt;->description(Lcom/squareup/customreport/data/ComparisonRange;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/squareup/salesreport/SalesReportCoordinator$updateActions$$inlined$map$lambda$1;

    invoke-direct {v5, v2, p0, p1}, Lcom/squareup/salesreport/SalesReportCoordinator$updateActions$$inlined$map$lambda$1;-><init>(Lcom/squareup/customreport/data/ComparisonRange;Lcom/squareup/salesreport/SalesReportCoordinator;Lcom/squareup/salesreport/SalesReportScreen;)V

    check-cast v5, Lkotlin/jvm/functions/Function0;

    invoke-direct {v3, v4, v5}, Lcom/squareup/salesreport/widget/PopupAction;-><init>(Ljava/lang/String;Lkotlin/jvm/functions/Function0;)V

    .line 1005
    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1401
    :cond_0
    check-cast v1, Ljava/util/List;

    .line 999
    new-instance p2, Lcom/squareup/salesreport/widget/PopupActions;

    invoke-direct {p2, v0, p5, v1, p6}, Lcom/squareup/salesreport/widget/PopupActions;-><init>(Lcom/squareup/util/Res;Landroid/content/Context;Ljava/util/List;Z)V

    iput-object p2, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->comparisonRangeActions:Lcom/squareup/salesreport/widget/PopupActions;

    .line 1010
    new-instance p2, Lcom/squareup/salesreport/widget/PopupActions;

    .line 1011
    iget-object v0, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->res:Lcom/squareup/util/Res;

    .line 1014
    new-instance v1, Lcom/squareup/salesreport/widget/PopupAction;

    invoke-virtual {p4}, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->getTopSectionState()Lcom/squareup/salesreport/SalesReportState$TopSectionState;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/salesreport/SalesReportCoordinatorKt;->access$getButtonRes$p(Lcom/squareup/salesreport/SalesReportState$TopSectionState;)I

    move-result v2

    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/squareup/salesreport/SalesReportCoordinator$updateActions$2;

    invoke-direct {v3, p1}, Lcom/squareup/salesreport/SalesReportCoordinator$updateActions$2;-><init>(Lcom/squareup/salesreport/SalesReportScreen;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-direct {v1, v2, v3}, Lcom/squareup/salesreport/widget/PopupAction;-><init>(Ljava/lang/String;Lkotlin/jvm/functions/Function0;)V

    .line 1013
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x1

    .line 1010
    invoke-direct {p2, v0, p5, v1, v2}, Lcom/squareup/salesreport/widget/PopupActions;-><init>(Lcom/squareup/util/Res;Landroid/content/Context;Ljava/util/List;Z)V

    iput-object p2, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->overviewDetailsActions:Lcom/squareup/salesreport/widget/PopupActions;

    .line 1020
    new-instance p2, Lcom/squareup/salesreport/widget/PopupActions;

    .line 1021
    iget-object v0, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->res:Lcom/squareup/util/Res;

    .line 1023
    invoke-direct {p0, p1}, Lcom/squareup/salesreport/SalesReportCoordinator;->getChartActions(Lcom/squareup/salesreport/SalesReportScreen;)Ljava/util/List;

    move-result-object v1

    .line 1020
    invoke-direct {p2, v0, p5, v1, p6}, Lcom/squareup/salesreport/widget/PopupActions;-><init>(Lcom/squareup/util/Res;Landroid/content/Context;Ljava/util/List;Z)V

    iput-object p2, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->chartActions:Lcom/squareup/salesreport/widget/PopupActions;

    .line 1030
    invoke-static {p3}, Lcom/squareup/salesreport/util/WithSalesReportsKt;->getItemCount(Lcom/squareup/customreport/data/WithSalesReport;)I

    move-result p2

    .line 1027
    invoke-direct {p0, p1, p4, p2}, Lcom/squareup/salesreport/SalesReportCoordinator;->getItemActions(Lcom/squareup/salesreport/SalesReportScreen;Lcom/squareup/salesreport/SalesReportState$UiSelectionState;I)Ljava/util/List;

    move-result-object p2

    .line 1033
    move-object v0, p2

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/2addr v0, v2

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 1034
    new-instance v0, Lcom/squareup/salesreport/widget/PopupActions;

    .line 1035
    iget-object v3, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->res:Lcom/squareup/util/Res;

    .line 1034
    invoke-direct {v0, v3, p5, p2, p6}, Lcom/squareup/salesreport/widget/PopupActions;-><init>(Lcom/squareup/util/Res;Landroid/content/Context;Ljava/util/List;Z)V

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 1033
    :goto_1
    iput-object v0, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->topItemsActions:Lcom/squareup/salesreport/widget/PopupActions;

    .line 1047
    invoke-static {p3}, Lcom/squareup/salesreport/util/WithSalesReportsKt;->getCategoryCount(Lcom/squareup/customreport/data/WithSalesReport;)I

    move-result p2

    .line 1044
    invoke-direct {p0, p1, p4, p2}, Lcom/squareup/salesreport/SalesReportCoordinator;->getCategoryActions(Lcom/squareup/salesreport/SalesReportScreen;Lcom/squareup/salesreport/SalesReportState$UiSelectionState;I)Ljava/util/List;

    move-result-object p1

    .line 1050
    move-object p2, p1

    check-cast p2, Ljava/util/Collection;

    invoke-interface {p2}, Ljava/util/Collection;->isEmpty()Z

    move-result p2

    xor-int/2addr p2, v2

    if-eqz p2, :cond_2

    .line 1051
    new-instance v1, Lcom/squareup/salesreport/widget/PopupActions;

    .line 1052
    iget-object p2, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->res:Lcom/squareup/util/Res;

    .line 1051
    invoke-direct {v1, p2, p5, p1, p6}, Lcom/squareup/salesreport/widget/PopupActions;-><init>(Lcom/squareup/util/Res;Landroid/content/Context;Ljava/util/List;Z)V

    .line 1050
    :cond_2
    iput-object v1, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->topCategoriesActions:Lcom/squareup/salesreport/widget/PopupActions;

    return-void
.end method

.method private final updateSalesReportRecycler(Lcom/squareup/salesreport/SalesReportScreen;Lcom/squareup/customreport/data/WithSalesReport;Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/salesreport/SalesReportState$UiSelectionState;Lcom/squareup/salesreport/UiDisplayState;Z)V
    .locals 9

    .line 392
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 397
    invoke-virtual {p4}, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->getTopSectionState()Lcom/squareup/salesreport/SalesReportState$TopSectionState;

    move-result-object v5

    .line 398
    invoke-virtual {p5}, Lcom/squareup/salesreport/UiDisplayState;->getShowSalesOverview()Z

    move-result v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, v0

    move-object v4, p2

    move v7, p6

    .line 393
    invoke-direct/range {v1 .. v7}, Lcom/squareup/salesreport/SalesReportCoordinator;->addTopSection(Lcom/squareup/salesreport/SalesReportScreen;Ljava/util/List;Lcom/squareup/customreport/data/WithSalesReport;Lcom/squareup/salesreport/SalesReportState$TopSectionState;ZZ)V

    .line 401
    invoke-virtual {p1}, Lcom/squareup/salesreport/SalesReportScreen;->getShowSalesCharts()Z

    move-result p5

    if-eqz p5, :cond_0

    .line 405
    invoke-virtual {p2}, Lcom/squareup/customreport/data/WithSalesReport;->getSalesSummaryReport()Lcom/squareup/customreport/data/WithSalesSummaryReport;

    move-result-object v4

    .line 406
    invoke-virtual {p2}, Lcom/squareup/customreport/data/WithSalesReport;->getSalesChartReport()Lcom/squareup/customreport/data/SalesChartReport;

    move-result-object v5

    .line 407
    invoke-virtual {p4}, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->getChartSalesSelection()Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;

    move-result-object v6

    move-object v1, p0

    move-object v2, v0

    move-object v3, p3

    .line 402
    invoke-direct/range {v1 .. v6}, Lcom/squareup/salesreport/SalesReportCoordinator;->addChartRows(Ljava/util/List;Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/customreport/data/WithSalesSummaryReport;Lcom/squareup/customreport/data/SalesChartReport;Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;)V

    .line 413
    :cond_0
    invoke-virtual {p2}, Lcom/squareup/customreport/data/WithSalesReport;->getPaymentMethodsReport()Lcom/squareup/customreport/data/SalesPaymentMethodsReport;

    move-result-object p3

    .line 410
    invoke-direct {p0, p1, v0, p3, p6}, Lcom/squareup/salesreport/SalesReportCoordinator;->addPaymentMethodRows(Lcom/squareup/salesreport/SalesReportScreen;Ljava/util/List;Lcom/squareup/customreport/data/SalesPaymentMethodsReport;Z)V

    .line 419
    invoke-virtual {p2}, Lcom/squareup/customreport/data/WithSalesReport;->getTopItemsReport()Lcom/squareup/customreport/data/SalesTopItemsReport;

    move-result-object v4

    .line 420
    invoke-virtual {p4}, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->getItemGrossCountSelection()Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;

    move-result-object v5

    .line 422
    invoke-virtual {p4}, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->getItemViewCount()Lcom/squareup/salesreport/SalesReportState$ViewCount;

    move-result-object v7

    .line 423
    invoke-virtual {p4}, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->getItemsWithVariationsShown()Ljava/util/Set;

    move-result-object v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, v0

    move v6, p6

    .line 416
    invoke-direct/range {v1 .. v8}, Lcom/squareup/salesreport/SalesReportCoordinator;->addItemRows(Lcom/squareup/salesreport/SalesReportScreen;Ljava/util/List;Lcom/squareup/customreport/data/SalesTopItemsReport;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;ZLcom/squareup/salesreport/SalesReportState$ViewCount;Ljava/util/Set;)V

    .line 428
    invoke-virtual {p2}, Lcom/squareup/customreport/data/WithSalesReport;->getTopCategoriesReport()Lcom/squareup/customreport/data/SalesTopCategoriesReport;

    move-result-object v4

    .line 429
    invoke-virtual {p4}, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->getCategoryGrossCountSelection()Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;

    move-result-object v5

    .line 431
    invoke-virtual {p4}, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->getCategoryViewCount()Lcom/squareup/salesreport/SalesReportState$ViewCount;

    move-result-object v7

    .line 432
    invoke-virtual {p4}, Lcom/squareup/salesreport/SalesReportState$UiSelectionState;->getCategoriesWithItemsShown()Ljava/util/Set;

    move-result-object v8

    .line 425
    invoke-direct/range {v1 .. v8}, Lcom/squareup/salesreport/SalesReportCoordinator;->addCategoryRows(Lcom/squareup/salesreport/SalesReportScreen;Ljava/util/List;Lcom/squareup/customreport/data/SalesTopCategoriesReport;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;ZLcom/squareup/salesreport/SalesReportState$ViewCount;Ljava/util/Set;)V

    .line 435
    iget-object p1, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->salesReportRecycler:Lcom/squareup/cycler/Recycler;

    if-nez p1, :cond_1

    const-string p2, "salesReportRecycler"

    invoke-static {p2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    new-instance p2, Lcom/squareup/salesreport/SalesReportCoordinator$updateSalesReportRecycler$1;

    invoke-direct {p2, v0}, Lcom/squareup/salesreport/SalesReportCoordinator$updateSalesReportRecycler$1;-><init>(Ljava/util/List;)V

    check-cast p2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, p2}, Lcom/squareup/cycler/Recycler;->update(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method private final visibleExpandedRowCount(Ljava/util/Set;I)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;I)I"
        }
    .end annotation

    const/4 v0, 0x0

    if-ltz p2, :cond_2

    const/4 v1, 0x0

    .line 798
    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    add-int/lit8 v1, v1, 0x1

    :cond_0
    if-eq v0, p2, :cond_1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    :cond_2
    return v0
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 3

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 275
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 276
    invoke-direct {p0, p1}, Lcom/squareup/salesreport/SalesReportCoordinator;->bindViews(Landroid/view/View;)V

    .line 278
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 279
    iget-object v1, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->screens:Lio/reactivex/Observable;

    iget-object v2, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->device:Lcom/squareup/util/Device;

    invoke-interface {v2}, Lcom/squareup/util/Device;->getScreenSize()Lio/reactivex/Observable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/util/rx2/Observables;->combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 280
    iget-object v1, p0, Lcom/squareup/salesreport/SalesReportCoordinator;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observables\n        .com\u2026.observeOn(mainScheduler)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 281
    new-instance v1, Lcom/squareup/salesreport/SalesReportCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/salesreport/SalesReportCoordinator$attach$1;-><init>(Lcom/squareup/salesreport/SalesReportCoordinator;Landroid/view/View;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method
