.class public final Lcom/squareup/salesreport/SalesReportModule_Companion_ProvideIncludeItemsInReportLocalSettingFactory;
.super Ljava/lang/Object;
.source "SalesReportModule_Companion_ProvideIncludeItemsInReportLocalSettingFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/settings/LocalSetting<",
        "Ljava/lang/Boolean;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final localSettingFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSettingFactory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSettingFactory;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/salesreport/SalesReportModule_Companion_ProvideIncludeItemsInReportLocalSettingFactory;->localSettingFactoryProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/salesreport/SalesReportModule_Companion_ProvideIncludeItemsInReportLocalSettingFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSettingFactory;",
            ">;)",
            "Lcom/squareup/salesreport/SalesReportModule_Companion_ProvideIncludeItemsInReportLocalSettingFactory;"
        }
    .end annotation

    .line 33
    new-instance v0, Lcom/squareup/salesreport/SalesReportModule_Companion_ProvideIncludeItemsInReportLocalSettingFactory;

    invoke-direct {v0, p0}, Lcom/squareup/salesreport/SalesReportModule_Companion_ProvideIncludeItemsInReportLocalSettingFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideIncludeItemsInReportLocalSetting(Lcom/squareup/settings/LocalSettingFactory;)Lcom/squareup/settings/LocalSetting;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/LocalSettingFactory;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 38
    sget-object v0, Lcom/squareup/salesreport/SalesReportModule;->Companion:Lcom/squareup/salesreport/SalesReportModule$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/salesreport/SalesReportModule$Companion;->provideIncludeItemsInReportLocalSetting(Lcom/squareup/settings/LocalSettingFactory;)Lcom/squareup/settings/LocalSetting;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/settings/LocalSetting;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/settings/LocalSetting;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 28
    iget-object v0, p0, Lcom/squareup/salesreport/SalesReportModule_Companion_ProvideIncludeItemsInReportLocalSettingFactory;->localSettingFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/LocalSettingFactory;

    invoke-static {v0}, Lcom/squareup/salesreport/SalesReportModule_Companion_ProvideIncludeItemsInReportLocalSettingFactory;->provideIncludeItemsInReportLocalSetting(Lcom/squareup/settings/LocalSettingFactory;)Lcom/squareup/settings/LocalSetting;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/salesreport/SalesReportModule_Companion_ProvideIncludeItemsInReportLocalSettingFactory;->get()Lcom/squareup/settings/LocalSetting;

    move-result-object v0

    return-object v0
.end method
