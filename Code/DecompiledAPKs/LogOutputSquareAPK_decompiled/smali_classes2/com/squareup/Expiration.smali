.class public Lcom/squareup/Expiration;
.super Ljava/lang/Object;
.source "Expiration.java"


# static fields
.field public static final PIVOT_YEARS:I = 0x14

.field private static final instance:Lcom/squareup/Expiration;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 16
    new-instance v0, Lcom/squareup/Expiration;

    invoke-direct {v0}, Lcom/squareup/Expiration;-><init>()V

    sput-object v0, Lcom/squareup/Expiration;->instance:Lcom/squareup/Expiration;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private expirationYear(Ljava/lang/String;I)I
    .locals 0

    .line 76
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p1

    add-int/lit8 p2, p2, 0x14

    add-int/lit16 p1, p1, 0x7d0

    if-lt p1, p2, :cond_0

    add-int/lit8 p1, p1, -0x64

    :cond_0
    return p1
.end method

.method public static isExpired(Ljava/lang/String;)Z
    .locals 3

    const/4 v0, 0x0

    if-eqz p0, :cond_1

    .line 25
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x4

    if-eq v1, v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x2

    .line 28
    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 29
    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    .line 30
    invoke-static {v0, p0}, Lcom/squareup/Expiration;->isExpired(Ljava/lang/String;Ljava/lang/String;)Z

    move-result p0

    return p0

    :cond_1
    :goto_0
    return v0
.end method

.method public static isExpired(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .line 43
    sget-object v0, Lcom/squareup/Expiration;->instance:Lcom/squareup/Expiration;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/Expiration;->expired(Ljava/lang/String;Ljava/lang/String;)Z

    move-result p0

    return p0
.end method


# virtual methods
.method expired(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5

    const/4 v0, 0x0

    if-eqz p2, :cond_3

    if-nez p1, :cond_0

    goto :goto_0

    .line 53
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/squareup/Expiration;->getCurrentTime()Ljava/util/Calendar;

    move-result-object v1

    const/4 v2, 0x1

    .line 54
    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v3

    const/4 v4, 0x2

    .line 55
    invoke-virtual {v1, v4}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 58
    invoke-direct {p0, p1, v3}, Lcom/squareup/Expiration;->expirationYear(Ljava/lang/String;I)I

    move-result p1

    .line 59
    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p2
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    sub-int/2addr p2, v2

    if-ltz p2, :cond_3

    const/16 v4, 0xb

    if-le p2, v4, :cond_1

    goto :goto_0

    :cond_1
    if-gt v3, p1, :cond_2

    if-ne v3, p1, :cond_3

    if-le v1, p2, :cond_3

    :cond_2
    const/4 v0, 0x1

    :catch_0
    :cond_3
    :goto_0
    return v0
.end method

.method protected getCurrentTime()Ljava/util/Calendar;
    .locals 1

    .line 94
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    return-object v0
.end method
