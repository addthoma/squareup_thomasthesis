.class public abstract Lcom/squareup/blueprint/ViewBlock;
.super Lcom/squareup/blueprint/Block;
.source "ViewBlock.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<C:",
        "Lcom/squareup/blueprint/UpdateContext;",
        "P:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/squareup/blueprint/Block<",
        "TC;TP;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nViewBlock.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ViewBlock.kt\ncom/squareup/blueprint/ViewBlock\n*L\n1#1,116:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\\\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0005\n\u0002\u0010\u0008\n\u0002\u0008\n\n\u0002\u0010\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008&\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u0002*\u0008\u0008\u0001\u0010\u0003*\u00020\u00042\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u00030\u0005B\u0005\u00a2\u0006\u0002\u0010\u0006J%\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00028\u00002\u0006\u0010\u001b\u001a\u00020\u000e2\u0006\u0010\u001c\u001a\u00020\u000eH&\u00a2\u0006\u0002\u0010\u001dJ%\u0010\u001e\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00028\u00002\u0006\u0010\u001b\u001a\u00020\u000e2\u0006\u0010\u001c\u001a\u00020\u000eH\u0016\u00a2\u0006\u0002\u0010\u001dJ \u0010\u001f\u001a\u00020\u000e2\u0006\u0010 \u001a\u00020\u00022\u0006\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020\u000eH\u0016J \u0010$\u001a\u00020\u000e2\u0006\u0010 \u001a\u00020\u00022\u0006\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020\u000eH\u0016J \u0010%\u001a\u00020&2\u0006\u0010 \u001a\u00020\u00022\u0006\u0010\'\u001a\u00020&2\u0006\u0010(\u001a\u00020)H\u0016J \u0010*\u001a\u00020&2\u0006\u0010 \u001a\u00020\u00022\u0006\u0010\'\u001a\u00020&2\u0006\u0010(\u001a\u00020+H\u0016J\u0018\u0010,\u001a\u00020\u00082\u000e\u0010-\u001a\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0005H\u0016J\u0010\u0010.\u001a\u00020/2\u0006\u0010 \u001a\u00020\u0002H\u0016J\u0008\u00100\u001a\u000201H\u0016J\u0010\u00102\u001a\u00020/2\u0006\u0010 \u001a\u00020\u0002H\u0016R\u0014\u0010\u0007\u001a\u00020\u00088VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\nR\u0014\u0010\u000b\u001a\u00020\u00088VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\nR\u0012\u0010\r\u001a\u00020\u000eX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000f\u0010\u0010R\u0018\u0010\u0011\u001a\u00020\u0008X\u00a6\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u0012\u0010\n\"\u0004\u0008\u0013\u0010\u0014R\u0018\u0010\u0015\u001a\u00020\u0008X\u00a6\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u0016\u0010\n\"\u0004\u0008\u0017\u0010\u0014\u00a8\u00063"
    }
    d2 = {
        "Lcom/squareup/blueprint/ViewBlock;",
        "C",
        "Lcom/squareup/blueprint/UpdateContext;",
        "P",
        "",
        "Lcom/squareup/blueprint/Block;",
        "()V",
        "dependableHorizontally",
        "",
        "getDependableHorizontally",
        "()Z",
        "dependableVertically",
        "getDependableVertically",
        "id",
        "",
        "getId",
        "()I",
        "wrapHorizontally",
        "getWrapHorizontally",
        "setWrapHorizontally",
        "(Z)V",
        "wrapVertically",
        "getWrapVertically",
        "setWrapVertically",
        "buildView",
        "",
        "updateContext",
        "width",
        "height",
        "(Lcom/squareup/blueprint/UpdateContext;II)V",
        "buildViews",
        "chainHorizontally",
        "context",
        "chainInfo",
        "Lcom/squareup/blueprint/ChainInfo;",
        "previousMargin",
        "chainVertically",
        "connectHorizontally",
        "Lcom/squareup/blueprint/IdsAndMargins;",
        "previousIds",
        "align",
        "Lcom/squareup/blueprint/HorizontalAlign;",
        "connectVertically",
        "Lcom/squareup/blueprint/VerticalAlign;",
        "isSameLayoutAs",
        "block",
        "startIds",
        "Lcom/squareup/blueprint/IdAndMarginCollection;",
        "toString",
        "",
        "topIds",
        "blueprint-core_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Lcom/squareup/blueprint/Block;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract buildView(Lcom/squareup/blueprint/UpdateContext;II)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TC;II)V"
        }
    .end annotation
.end method

.method public buildViews(Lcom/squareup/blueprint/UpdateContext;II)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TC;II)V"
        }
    .end annotation

    const-string v0, "updateContext"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-virtual {p0}, Lcom/squareup/blueprint/ViewBlock;->getWrapHorizontally()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    .line 40
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/blueprint/ViewBlock;->getWrapVertically()Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    const/4 p3, 0x0

    .line 41
    :goto_1
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/blueprint/ViewBlock;->buildView(Lcom/squareup/blueprint/UpdateContext;II)V

    return-void
.end method

.method public chainHorizontally(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/ChainInfo;I)I
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "chainInfo"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    invoke-virtual {p0}, Lcom/squareup/blueprint/ViewBlock;->getId()I

    move-result p1

    invoke-virtual {p2, p1, p3}, Lcom/squareup/blueprint/ChainInfo;->add(II)V

    const/4 p1, 0x0

    return p1
.end method

.method public chainVertically(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/ChainInfo;I)I
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "chainInfo"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 103
    invoke-virtual {p0}, Lcom/squareup/blueprint/ViewBlock;->getId()I

    move-result p1

    invoke-virtual {p2, p1, p3}, Lcom/squareup/blueprint/ChainInfo;->add(II)V

    const/4 p1, 0x0

    return p1
.end method

.method public connectHorizontally(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;Lcom/squareup/blueprint/HorizontalAlign;)Lcom/squareup/blueprint/IdsAndMargins;
    .locals 8

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "previousIds"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "align"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    sget-object v0, Lcom/squareup/blueprint/ViewBlock$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p3}, Lcom/squareup/blueprint/HorizontalAlign;->ordinal()I

    move-result p3

    aget p3, v0, p3

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eq p3, v0, :cond_1

    const/4 v0, 0x2

    if-eq p3, v0, :cond_1

    const/4 v0, 0x3

    if-eq p3, v0, :cond_0

    goto :goto_0

    .line 60
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/blueprint/ViewBlock;->getId()I

    move-result p3

    const/4 v0, 0x6

    invoke-interface {p2, p3, v0, v1, p1}, Lcom/squareup/blueprint/IdsAndMargins;->connectTo(IIILcom/squareup/blueprint/UpdateContext;)V

    goto :goto_0

    .line 54
    :cond_1
    invoke-interface {p2, p1}, Lcom/squareup/blueprint/IdsAndMargins;->asIdAndMargin(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdAndMargin;

    move-result-object p2

    .line 55
    invoke-virtual {p1}, Lcom/squareup/blueprint/UpdateContext;->getConstraintSet()Landroidx/constraintlayout/widget/ConstraintSet;

    move-result-object v2

    invoke-virtual {p0}, Lcom/squareup/blueprint/ViewBlock;->getId()I

    move-result v3

    const/4 v4, 0x6

    .line 56
    invoke-virtual {p2}, Lcom/squareup/blueprint/IdAndMargin;->getId()I

    move-result v5

    invoke-virtual {p2}, Lcom/squareup/blueprint/IdAndMargin;->getSide()I

    move-result v6

    invoke-virtual {p2}, Lcom/squareup/blueprint/IdAndMargin;->getMargin()I

    move-result v7

    .line 55
    invoke-virtual/range {v2 .. v7}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIIII)V

    .line 63
    :goto_0
    new-instance p1, Lcom/squareup/blueprint/IdAndMarginCollection;

    const/4 p2, 0x7

    invoke-virtual {p0}, Lcom/squareup/blueprint/ViewBlock;->getId()I

    move-result p3

    invoke-direct {p1, p2, p3, v1}, Lcom/squareup/blueprint/IdAndMarginCollection;-><init>(III)V

    check-cast p1, Lcom/squareup/blueprint/IdsAndMargins;

    return-object p1
.end method

.method public connectVertically(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;Lcom/squareup/blueprint/VerticalAlign;)Lcom/squareup/blueprint/IdsAndMargins;
    .locals 8

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "previousIds"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "align"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    sget-object v0, Lcom/squareup/blueprint/ViewBlock$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p3}, Lcom/squareup/blueprint/VerticalAlign;->ordinal()I

    move-result p3

    aget p3, v0, p3

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eq p3, v0, :cond_1

    const/4 v0, 0x2

    if-eq p3, v0, :cond_1

    const/4 v0, 0x3

    if-eq p3, v0, :cond_0

    goto :goto_0

    .line 80
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/blueprint/ViewBlock;->getId()I

    move-result p3

    invoke-interface {p2, p3, v0, v1, p1}, Lcom/squareup/blueprint/IdsAndMargins;->connectTo(IIILcom/squareup/blueprint/UpdateContext;)V

    goto :goto_0

    .line 74
    :cond_1
    invoke-interface {p2, p1}, Lcom/squareup/blueprint/IdsAndMargins;->asIdAndMargin(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdAndMargin;

    move-result-object p3

    .line 75
    invoke-virtual {p1}, Lcom/squareup/blueprint/UpdateContext;->getConstraintSet()Landroidx/constraintlayout/widget/ConstraintSet;

    move-result-object v2

    invoke-virtual {p0}, Lcom/squareup/blueprint/ViewBlock;->getId()I

    move-result v3

    const/4 v4, 0x3

    .line 76
    invoke-virtual {p3}, Lcom/squareup/blueprint/IdAndMargin;->getId()I

    move-result v5

    invoke-interface {p2}, Lcom/squareup/blueprint/IdsAndMargins;->getSide()I

    move-result v6

    invoke-virtual {p3}, Lcom/squareup/blueprint/IdAndMargin;->getMargin()I

    move-result v7

    .line 75
    invoke-virtual/range {v2 .. v7}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIIII)V

    .line 83
    :goto_0
    new-instance p1, Lcom/squareup/blueprint/IdAndMarginCollection;

    const/4 p2, 0x4

    invoke-virtual {p0}, Lcom/squareup/blueprint/ViewBlock;->getId()I

    move-result p3

    invoke-direct {p1, p2, p3, v1}, Lcom/squareup/blueprint/IdAndMarginCollection;-><init>(III)V

    check-cast p1, Lcom/squareup/blueprint/IdsAndMargins;

    return-object p1
.end method

.method public getDependableHorizontally()Z
    .locals 1

    .line 31
    invoke-virtual {p0}, Lcom/squareup/blueprint/ViewBlock;->getWrapHorizontally()Z

    move-result v0

    return v0
.end method

.method public getDependableVertically()Z
    .locals 1

    .line 32
    invoke-virtual {p0}, Lcom/squareup/blueprint/ViewBlock;->getWrapVertically()Z

    move-result v0

    return v0
.end method

.method public abstract getId()I
.end method

.method public abstract getWrapHorizontally()Z
.end method

.method public abstract getWrapVertically()Z
.end method

.method public isSameLayoutAs(Lcom/squareup/blueprint/Block;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/blueprint/Block<",
            "**>;)Z"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-super {p0, p1}, Lcom/squareup/blueprint/Block;->isSameLayoutAs(Lcom/squareup/blueprint/Block;)Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    .line 22
    check-cast p1, Lcom/squareup/blueprint/ViewBlock;

    .line 24
    invoke-virtual {p0}, Lcom/squareup/blueprint/ViewBlock;->getWrapHorizontally()Z

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/blueprint/ViewBlock;->getWrapHorizontally()Z

    move-result v3

    if-ne v0, v3, :cond_0

    invoke-virtual {p0}, Lcom/squareup/blueprint/ViewBlock;->getWrapVertically()Z

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/blueprint/ViewBlock;->getWrapVertically()Z

    move-result p1

    if-ne v0, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    return v1
.end method

.method public abstract setWrapHorizontally(Z)V
.end method

.method public abstract setWrapVertically(Z)V
.end method

.method public startIds(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdAndMarginCollection;
    .locals 3

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 112
    new-instance p1, Lcom/squareup/blueprint/IdAndMarginCollection;

    invoke-virtual {p0}, Lcom/squareup/blueprint/ViewBlock;->getId()I

    move-result v0

    const/4 v1, 0x6

    const/4 v2, 0x0

    invoke-direct {p1, v1, v0, v2}, Lcom/squareup/blueprint/IdAndMarginCollection;-><init>(III)V

    return-object p1
.end method

.method public bridge synthetic startIds(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdsAndMargins;
    .locals 0

    .line 15
    invoke-virtual {p0, p1}, Lcom/squareup/blueprint/ViewBlock;->startIds(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdAndMarginCollection;

    move-result-object p1

    check-cast p1, Lcom/squareup/blueprint/IdsAndMargins;

    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 114
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "(id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/blueprint/ViewBlock;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ",params="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/blueprint/ViewBlock;->getParams()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public topIds(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdAndMarginCollection;
    .locals 3

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 109
    new-instance p1, Lcom/squareup/blueprint/IdAndMarginCollection;

    invoke-virtual {p0}, Lcom/squareup/blueprint/ViewBlock;->getId()I

    move-result v0

    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-direct {p1, v1, v0, v2}, Lcom/squareup/blueprint/IdAndMarginCollection;-><init>(III)V

    return-object p1
.end method

.method public bridge synthetic topIds(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdsAndMargins;
    .locals 0

    .line 15
    invoke-virtual {p0, p1}, Lcom/squareup/blueprint/ViewBlock;->topIds(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdAndMarginCollection;

    move-result-object p1

    check-cast p1, Lcom/squareup/blueprint/IdsAndMargins;

    return-object p1
.end method
