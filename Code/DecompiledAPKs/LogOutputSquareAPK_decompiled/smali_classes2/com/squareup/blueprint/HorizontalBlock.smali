.class public final Lcom/squareup/blueprint/HorizontalBlock;
.super Lcom/squareup/blueprint/LinearBlock;
.source "HorizontalBlock.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<C:",
        "Lcom/squareup/blueprint/UpdateContext;",
        "P:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/squareup/blueprint/LinearBlock<",
        "TC;TP;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nHorizontalBlock.kt\nKotlin\n*S Kotlin\n*F\n+ 1 HorizontalBlock.kt\ncom/squareup/blueprint/HorizontalBlock\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 _Sequences.kt\nkotlin/sequences/SequencesKt___SequencesKt\n*L\n1#1,240:1\n300#2,7:241\n1313#2:248\n1382#2,3:249\n1600#2,3:252\n1600#2,3:255\n1600#2,3:258\n1600#2,3:261\n1591#2,2:264\n1591#2,2:266\n1591#2,2:268\n1591#2,2:270\n1591#2,2:272\n1499#2,3:280\n1084#3,2:274\n1084#3,2:276\n1084#3,2:278\n*E\n*S KotlinDebug\n*F\n+ 1 HorizontalBlock.kt\ncom/squareup/blueprint/HorizontalBlock\n*L\n48#1,7:241\n50#1:248\n50#1,3:249\n52#1,3:252\n70#1,3:255\n82#1,3:258\n106#1,3:261\n128#1,2:264\n149#1,2:266\n157#1,2:268\n172#1,2:270\n186#1,2:272\n43#1,3:280\n202#1,2:274\n228#1,2:276\n231#1,2:278\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000n\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\n\n\u0002\u0010 \n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0008\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u0002*\u0008\u0008\u0001\u0010\u0003*\u00020\u00042\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u00030\u0005B3\u0012\u0006\u0010\u0006\u001a\u00028\u0001\u0012\u001a\u0008\u0002\u0010\u0007\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\n0\t0\u0008\u0012\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rJ%\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00028\u00002\u0006\u0010\u001e\u001a\u00020\u000c2\u0006\u0010\u001f\u001a\u00020\u000cH\u0016\u00a2\u0006\u0002\u0010 J \u0010!\u001a\u00020\u000c2\u0006\u0010\"\u001a\u00020\u00022\u0006\u0010#\u001a\u00020$2\u0006\u0010%\u001a\u00020\u000cH\u0016J \u0010&\u001a\u00020\u000c2\u0006\u0010\"\u001a\u00020\u00022\u0006\u0010#\u001a\u00020$2\u0006\u0010%\u001a\u00020\u000cH\u0016J\u000e\u0010\'\u001a\u00028\u0001H\u00c6\u0003\u00a2\u0006\u0002\u0010\u0017J\u001b\u0010(\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\n0\t0\u0008H\u00c4\u0003J\t\u0010)\u001a\u00020\u000cH\u00c2\u0003J \u0010*\u001a\u00020+2\u0006\u0010\"\u001a\u00020\u00022\u0006\u0010,\u001a\u00020+2\u0006\u0010-\u001a\u00020.H\u0016J \u0010/\u001a\u00020+2\u0006\u0010\"\u001a\u00020\u00022\u0006\u0010,\u001a\u00020+2\u0006\u0010-\u001a\u000200H\u0016JJ\u00101\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00002\u0008\u0008\u0002\u0010\u0006\u001a\u00028\u00012\u001a\u0008\u0002\u0010\u0007\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\n0\t0\u00082\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u000cH\u00c6\u0001\u00a2\u0006\u0002\u00102J\u0013\u00103\u001a\u00020\u000f2\u0008\u00104\u001a\u0004\u0018\u00010\u0004H\u00d6\u0003J\t\u00105\u001a\u00020\u000cH\u00d6\u0001J\u0010\u00106\u001a\u00020+2\u0006\u0010\"\u001a\u00020\u0002H\u0016J\t\u00107\u001a\u000208H\u00d6\u0001J\u0010\u00109\u001a\u00020:2\u0006\u0010\"\u001a\u00020\u0002H\u0016R\u0014\u0010\u000e\u001a\u00020\u000fX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0014\u0010\u0012\u001a\u00020\u000fX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0011R&\u0010\u0007\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\n0\t0\u0008X\u0094\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0006\u001a\u00028\u0001X\u0096\u0004\u00a2\u0006\n\n\u0002\u0010\u0018\u001a\u0004\u0008\u0016\u0010\u0017R\u0014\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u001aX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006;"
    }
    d2 = {
        "Lcom/squareup/blueprint/HorizontalBlock;",
        "C",
        "Lcom/squareup/blueprint/UpdateContext;",
        "P",
        "",
        "Lcom/squareup/blueprint/LinearBlock;",
        "params",
        "elements",
        "",
        "Lcom/squareup/blueprint/Block;",
        "Lcom/squareup/blueprint/LinearBlock$Params;",
        "extendIndex",
        "",
        "(Ljava/lang/Object;Ljava/util/List;I)V",
        "dependableHorizontally",
        "",
        "getDependableHorizontally",
        "()Z",
        "dependableVertically",
        "getDependableVertically",
        "getElements",
        "()Ljava/util/List;",
        "getParams",
        "()Ljava/lang/Object;",
        "Ljava/lang/Object;",
        "spacing",
        "",
        "buildViews",
        "",
        "updateContext",
        "width",
        "height",
        "(Lcom/squareup/blueprint/UpdateContext;II)V",
        "chainHorizontally",
        "context",
        "chainInfo",
        "Lcom/squareup/blueprint/ChainInfo;",
        "previousMargin",
        "chainVertically",
        "component1",
        "component2",
        "component3",
        "connectHorizontally",
        "Lcom/squareup/blueprint/IdsAndMargins;",
        "previousIds",
        "align",
        "Lcom/squareup/blueprint/HorizontalAlign;",
        "connectVertically",
        "Lcom/squareup/blueprint/VerticalAlign;",
        "copy",
        "(Ljava/lang/Object;Ljava/util/List;I)Lcom/squareup/blueprint/HorizontalBlock;",
        "equals",
        "other",
        "hashCode",
        "startIds",
        "toString",
        "",
        "topIds",
        "Lcom/squareup/blueprint/IdAndMarginCollection;",
        "blueprint-core_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final dependableHorizontally:Z

.field private final dependableVertically:Z

.field private final elements:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/blueprint/Block<",
            "TC;",
            "Lcom/squareup/blueprint/LinearBlock$Params;",
            ">;>;"
        }
    .end annotation
.end field

.field private extendIndex:I

.field private final params:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TP;"
        }
    .end annotation
.end field

.field private spacing:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;Ljava/util/List;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;",
            "Ljava/util/List<",
            "Lcom/squareup/blueprint/Block<",
            "TC;",
            "Lcom/squareup/blueprint/LinearBlock$Params;",
            ">;>;I)V"
        }
    .end annotation

    const-string v0, "params"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "elements"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-direct {p0}, Lcom/squareup/blueprint/LinearBlock;-><init>()V

    iput-object p1, p0, Lcom/squareup/blueprint/HorizontalBlock;->params:Ljava/lang/Object;

    iput-object p2, p0, Lcom/squareup/blueprint/HorizontalBlock;->elements:Ljava/util/List;

    iput p3, p0, Lcom/squareup/blueprint/HorizontalBlock;->extendIndex:I

    .line 42
    iget p1, p0, Lcom/squareup/blueprint/HorizontalBlock;->extendIndex:I

    const/4 p2, 0x1

    const/4 p3, 0x0

    if-gez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-boolean p1, p0, Lcom/squareup/blueprint/HorizontalBlock;->dependableHorizontally:Z

    .line 43
    invoke-virtual {p0}, Lcom/squareup/blueprint/HorizontalBlock;->getElements()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 280
    instance-of v0, p1, Ljava/util/Collection;

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_1

    .line 281
    :cond_1
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/blueprint/Block;

    .line 43
    invoke-virtual {v0}, Lcom/squareup/blueprint/Block;->getDependableVertically()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 p3, 0x1

    .line 282
    :cond_3
    :goto_1
    iput-boolean p3, p0, Lcom/squareup/blueprint/HorizontalBlock;->dependableVertically:Z

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/Object;Ljava/util/List;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    .line 32
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    check-cast p2, Ljava/util/List;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, -0x1

    .line 33
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/blueprint/HorizontalBlock;-><init>(Ljava/lang/Object;Ljava/util/List;I)V

    return-void
.end method

.method private final component3()I
    .locals 1

    iget v0, p0, Lcom/squareup/blueprint/HorizontalBlock;->extendIndex:I

    return v0
.end method

.method public static synthetic copy$default(Lcom/squareup/blueprint/HorizontalBlock;Ljava/lang/Object;Ljava/util/List;IILjava/lang/Object;)Lcom/squareup/blueprint/HorizontalBlock;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    invoke-virtual {p0}, Lcom/squareup/blueprint/HorizontalBlock;->getParams()Ljava/lang/Object;

    move-result-object p1

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    invoke-virtual {p0}, Lcom/squareup/blueprint/HorizontalBlock;->getElements()Ljava/util/List;

    move-result-object p2

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget p3, p0, Lcom/squareup/blueprint/HorizontalBlock;->extendIndex:I

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/blueprint/HorizontalBlock;->copy(Ljava/lang/Object;Ljava/util/List;I)Lcom/squareup/blueprint/HorizontalBlock;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public buildViews(Lcom/squareup/blueprint/UpdateContext;II)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TC;II)V"
        }
    .end annotation

    const-string v0, "updateContext"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    invoke-virtual {p0}, Lcom/squareup/blueprint/HorizontalBlock;->getElements()Ljava/util/List;

    move-result-object v0

    .line 242
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 243
    check-cast v3, Lcom/squareup/blueprint/Block;

    .line 48
    invoke-virtual {v3}, Lcom/squareup/blueprint/Block;->getParams()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/blueprint/LinearBlock$Params;

    invoke-virtual {v3}, Lcom/squareup/blueprint/LinearBlock$Params;->getExtend()Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, -0x1

    .line 247
    :goto_1
    iput v2, p0, Lcom/squareup/blueprint/HorizontalBlock;->extendIndex:I

    .line 49
    invoke-virtual {p1}, Lcom/squareup/blueprint/UpdateContext;->getConstraintLayout()Landroidx/constraintlayout/widget/ConstraintLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/constraintlayout/widget/ConstraintLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 50
    invoke-virtual {p0}, Lcom/squareup/blueprint/HorizontalBlock;->getElements()Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    .line 248
    new-instance v3, Ljava/util/ArrayList;

    const/16 v4, 0xa

    invoke-static {v2, v4}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v3, Ljava/util/Collection;

    .line 249
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 250
    check-cast v4, Lcom/squareup/blueprint/Block;

    .line 50
    invoke-virtual {v4}, Lcom/squareup/blueprint/Block;->getParams()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/blueprint/LinearBlock$Params;

    invoke-virtual {v4}, Lcom/squareup/blueprint/LinearBlock$Params;->getSpacing$blueprint_core_release()Lcom/squareup/resources/DimenModel;

    move-result-object v4

    const-string v5, "context"

    invoke-static {v0, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Lcom/squareup/resources/DimenModel;->toOffset(Landroid/content/Context;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 251
    :cond_2
    check-cast v3, Ljava/util/List;

    iput-object v3, p0, Lcom/squareup/blueprint/HorizontalBlock;->spacing:Ljava/util/List;

    .line 52
    invoke-virtual {p0}, Lcom/squareup/blueprint/HorizontalBlock;->getElements()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 253
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    add-int/lit8 v3, v1, 0x1

    if-gez v1, :cond_3

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_3
    check-cast v2, Lcom/squareup/blueprint/Block;

    .line 53
    iget v4, p0, Lcom/squareup/blueprint/HorizontalBlock;->extendIndex:I

    if-ne v1, v4, :cond_4

    move v1, p2

    goto :goto_4

    :cond_4
    const/4 v1, -0x2

    .line 55
    :goto_4
    invoke-virtual {v2, p1, v1, p3}, Lcom/squareup/blueprint/Block;->buildViews(Lcom/squareup/blueprint/UpdateContext;II)V

    move v1, v3

    goto :goto_3

    :cond_5
    return-void
.end method

.method public chainHorizontally(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/ChainInfo;I)I
    .locals 3

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "chainInfo"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 197
    invoke-virtual {p0}, Lcom/squareup/blueprint/HorizontalBlock;->getDependableHorizontally()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 202
    invoke-virtual {p0}, Lcom/squareup/blueprint/HorizontalBlock;->getElements()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/blueprint/HorizontalBlock;->spacing:Ljava/util/List;

    if-nez v1, :cond_0

    const-string v2, "spacing"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v1, Ljava/lang/Iterable;

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/sequences/SequencesKt;->zip(Lkotlin/sequences/Sequence;Lkotlin/sequences/Sequence;)Lkotlin/sequences/Sequence;

    move-result-object v0

    .line 274
    invoke-interface {v0}, Lkotlin/sequences/Sequence;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlin/Pair;

    invoke-virtual {v1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/blueprint/Block;

    invoke-virtual {v1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    move-result v1

    add-int/2addr v1, p3

    .line 205
    invoke-virtual {v2, p1, p2, v1}, Lcom/squareup/blueprint/Block;->chainHorizontally(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/ChainInfo;I)I

    move-result p3

    goto :goto_0

    :cond_1
    return p3

    .line 197
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Cannot chain inside HorizontalBlock if there\'s a \'remaining\' item."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public chainVertically(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/ChainInfo;I)I
    .locals 6

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "chainInfo"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 216
    invoke-virtual {p2}, Lcom/squareup/blueprint/ChainInfo;->getSize()I

    move-result v0

    .line 217
    invoke-virtual {p0}, Lcom/squareup/blueprint/HorizontalBlock;->getElements()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/blueprint/Block;

    invoke-virtual {v1, p1, p2, p3}, Lcom/squareup/blueprint/Block;->chainVertically(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/ChainInfo;I)I

    move-result v1

    .line 218
    invoke-virtual {p2, v0}, Lcom/squareup/blueprint/ChainInfo;->idAt(I)I

    move-result v2

    .line 223
    invoke-virtual {p2, v0}, Lcom/squareup/blueprint/ChainInfo;->marginAt(I)I

    move-result v0

    sub-int/2addr v0, p3

    .line 224
    new-instance p3, Lcom/squareup/blueprint/IdAndMargin;

    const/4 v3, 0x3

    invoke-direct {p3, v2, v3, v0}, Lcom/squareup/blueprint/IdAndMargin;-><init>(III)V

    .line 225
    new-instance v0, Lcom/squareup/blueprint/IdAndMargin;

    invoke-virtual {p2}, Lcom/squareup/blueprint/ChainInfo;->lastId()I

    move-result p2

    const/4 v2, 0x4

    invoke-direct {v0, p2, v2, v1}, Lcom/squareup/blueprint/IdAndMargin;-><init>(III)V

    .line 227
    new-instance p2, Lcom/squareup/blueprint/IdAndMarginCollection;

    invoke-direct {p2, v3}, Lcom/squareup/blueprint/IdAndMarginCollection;-><init>(I)V

    .line 228
    invoke-virtual {p0}, Lcom/squareup/blueprint/HorizontalBlock;->getElements()Ljava/util/List;

    move-result-object v3

    check-cast v3, Ljava/lang/Iterable;

    invoke-static {v3}, Lkotlin/collections/CollectionsKt;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {v3, v4}, Lkotlin/sequences/SequencesKt;->drop(Lkotlin/sequences/Sequence;I)Lkotlin/sequences/Sequence;

    move-result-object v3

    .line 276
    invoke-interface {v3}, Lkotlin/sequences/Sequence;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/blueprint/Block;

    .line 228
    invoke-virtual {v5, p1}, Lcom/squareup/blueprint/Block;->topIds(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object v5

    invoke-virtual {p2, v5}, Lcom/squareup/blueprint/IdAndMarginCollection;->plusAssign(Lcom/squareup/blueprint/IdsAndMargins;)V

    goto :goto_0

    .line 229
    :cond_0
    check-cast p2, Lcom/squareup/blueprint/IdsAndMargins;

    invoke-virtual {p3, p1, p2}, Lcom/squareup/blueprint/IdAndMargin;->hookForTheSameSide(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;)Lcom/squareup/blueprint/IdAndMargin;

    move-result-object p2

    .line 230
    new-instance p3, Lcom/squareup/blueprint/IdAndMarginCollection;

    invoke-direct {p3, v2}, Lcom/squareup/blueprint/IdAndMarginCollection;-><init>(I)V

    .line 231
    invoke-virtual {p0}, Lcom/squareup/blueprint/HorizontalBlock;->getElements()Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object v2

    invoke-static {v2, v4}, Lkotlin/sequences/SequencesKt;->drop(Lkotlin/sequences/Sequence;I)Lkotlin/sequences/Sequence;

    move-result-object v2

    .line 278
    invoke-interface {v2}, Lkotlin/sequences/Sequence;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/blueprint/Block;

    .line 233
    move-object v4, p2

    check-cast v4, Lcom/squareup/blueprint/IdsAndMargins;

    sget-object v5, Lcom/squareup/blueprint/VerticalAlign;->CENTER:Lcom/squareup/blueprint/VerticalAlign;

    invoke-virtual {v3, p1, v4, v5}, Lcom/squareup/blueprint/Block;->connectVertically(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;Lcom/squareup/blueprint/VerticalAlign;)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object v3

    .line 232
    invoke-virtual {p3, v3}, Lcom/squareup/blueprint/IdAndMarginCollection;->plusAssign(Lcom/squareup/blueprint/IdsAndMargins;)V

    goto :goto_1

    .line 235
    :cond_1
    move-object p2, p3

    check-cast p2, Lcom/squareup/blueprint/IdsAndMargins;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/blueprint/IdAndMargin;->hookForTheSameSide(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;)Lcom/squareup/blueprint/IdAndMargin;

    move-result-object p2

    .line 236
    invoke-virtual {p3, p2, p1}, Lcom/squareup/blueprint/IdAndMarginCollection;->connectTo(Lcom/squareup/blueprint/IdAndMargin;Lcom/squareup/blueprint/UpdateContext;)V

    return v1
.end method

.method public final component1()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TP;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/squareup/blueprint/HorizontalBlock;->getParams()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected final component2()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/blueprint/Block<",
            "TC;",
            "Lcom/squareup/blueprint/LinearBlock$Params;",
            ">;>;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/squareup/blueprint/HorizontalBlock;->getElements()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public connectHorizontally(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;Lcom/squareup/blueprint/HorizontalAlign;)Lcom/squareup/blueprint/IdsAndMargins;
    .locals 6

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "previousIds"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "align"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    sget-object v0, Lcom/squareup/blueprint/HorizontalBlock$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p3}, Lcom/squareup/blueprint/HorizontalAlign;->ordinal()I

    move-result p3

    aget p3, v0, p3

    const/4 v0, 0x0

    const-string v1, "spacing"

    const/4 v2, 0x1

    if-eq p3, v2, :cond_b

    const/4 v2, 0x2

    if-eq p3, v2, :cond_5

    const/4 v2, 0x3

    if-ne p3, v2, :cond_4

    .line 103
    invoke-virtual {p0}, Lcom/squareup/blueprint/HorizontalBlock;->getDependableHorizontally()Z

    move-result p3

    if-eqz p3, :cond_3

    .line 106
    invoke-virtual {p0}, Lcom/squareup/blueprint/HorizontalBlock;->getElements()Ljava/util/List;

    move-result-object p3

    check-cast p3, Ljava/lang/Iterable;

    .line 262
    invoke-interface {p3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_0
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    add-int/lit8 v3, v0, 0x1

    if-gez v0, :cond_0

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_0
    check-cast v2, Lcom/squareup/blueprint/Block;

    .line 109
    iget-object v4, p0, Lcom/squareup/blueprint/HorizontalBlock;->spacing:Ljava/util/List;

    if-nez v4, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    invoke-interface {p2, v0}, Lcom/squareup/blueprint/IdsAndMargins;->withExtraMargin(I)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object p2

    .line 110
    sget-object v0, Lcom/squareup/blueprint/HorizontalAlign;->END:Lcom/squareup/blueprint/HorizontalAlign;

    .line 107
    invoke-virtual {v2, p1, p2, v0}, Lcom/squareup/blueprint/Block;->connectHorizontally(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;Lcom/squareup/blueprint/HorizontalAlign;)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object p2

    move v0, v3

    goto :goto_0

    :cond_2
    return-object p2

    .line 103
    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Cannot have a HorizontalBlock aligned RIGHT with a remaining block."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 113
    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 81
    :cond_5
    sget-object p3, Lcom/squareup/blueprint/HorizontalAlign;->START:Lcom/squareup/blueprint/HorizontalAlign;

    .line 82
    invoke-virtual {p0}, Lcom/squareup/blueprint/HorizontalBlock;->getElements()Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    .line 259
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    add-int/lit8 v4, v0, 0x1

    if-gez v0, :cond_6

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_6
    check-cast v3, Lcom/squareup/blueprint/Block;

    .line 83
    iget v5, p0, Lcom/squareup/blueprint/HorizontalBlock;->extendIndex:I

    if-eq v0, v5, :cond_8

    .line 87
    iget-object v5, p0, Lcom/squareup/blueprint/HorizontalBlock;->spacing:Ljava/util/List;

    if-nez v5, :cond_7

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    invoke-interface {p2, v0}, Lcom/squareup/blueprint/IdsAndMargins;->withExtraMargin(I)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object p2

    .line 85
    invoke-virtual {v3, p1, p2, p3}, Lcom/squareup/blueprint/Block;->connectHorizontally(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;Lcom/squareup/blueprint/HorizontalAlign;)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object p2

    goto :goto_2

    .line 93
    :cond_8
    iget-object p3, p0, Lcom/squareup/blueprint/HorizontalBlock;->spacing:Ljava/util/List;

    if-nez p3, :cond_9

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    invoke-interface {p3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/lang/Number;

    invoke-virtual {p3}, Ljava/lang/Number;->intValue()I

    move-result p3

    invoke-interface {p2, p3}, Lcom/squareup/blueprint/IdsAndMargins;->withExtraMargin(I)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object p2

    .line 94
    sget-object p3, Lcom/squareup/blueprint/HorizontalAlign;->CENTER:Lcom/squareup/blueprint/HorizontalAlign;

    .line 91
    invoke-virtual {v3, p1, p2, p3}, Lcom/squareup/blueprint/Block;->connectHorizontally(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;Lcom/squareup/blueprint/HorizontalAlign;)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object p2

    .line 96
    sget-object p3, Lcom/squareup/blueprint/HorizontalAlign;->END:Lcom/squareup/blueprint/HorizontalAlign;

    :goto_2
    move v0, v4

    goto :goto_1

    :cond_a
    return-object p2

    .line 67
    :cond_b
    invoke-virtual {p0}, Lcom/squareup/blueprint/HorizontalBlock;->getDependableHorizontally()Z

    move-result p3

    if-eqz p3, :cond_f

    .line 70
    invoke-virtual {p0}, Lcom/squareup/blueprint/HorizontalBlock;->getElements()Ljava/util/List;

    move-result-object p3

    check-cast p3, Ljava/lang/Iterable;

    .line 256
    invoke-interface {p3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_3
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_e

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    add-int/lit8 v3, v0, 0x1

    if-gez v0, :cond_c

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_c
    check-cast v2, Lcom/squareup/blueprint/Block;

    .line 73
    iget-object v4, p0, Lcom/squareup/blueprint/HorizontalBlock;->spacing:Ljava/util/List;

    if-nez v4, :cond_d

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_d
    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    invoke-interface {p2, v0}, Lcom/squareup/blueprint/IdsAndMargins;->withExtraMargin(I)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object p2

    .line 74
    sget-object v0, Lcom/squareup/blueprint/HorizontalAlign;->START:Lcom/squareup/blueprint/HorizontalAlign;

    .line 71
    invoke-virtual {v2, p1, p2, v0}, Lcom/squareup/blueprint/Block;->connectHorizontally(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;Lcom/squareup/blueprint/HorizontalAlign;)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object p2

    move v0, v3

    goto :goto_3

    :cond_e
    return-object p2

    .line 67
    :cond_f
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Cannot have a HorizontalBlock aligned LEFT with a remaining block."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public connectVertically(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;Lcom/squareup/blueprint/VerticalAlign;)Lcom/squareup/blueprint/IdsAndMargins;
    .locals 5

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "previousIds"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "align"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 123
    sget-object v0, Lcom/squareup/blueprint/HorizontalBlock$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p3}, Lcom/squareup/blueprint/VerticalAlign;->ordinal()I

    move-result p3

    aget p3, v0, p3

    const/4 v0, 0x1

    const/4 v1, 0x4

    if-eq p3, v0, :cond_8

    const/4 v0, 0x2

    if-eq p3, v0, :cond_6

    const/4 v0, 0x3

    if-ne p3, v0, :cond_5

    .line 155
    new-instance p3, Lcom/squareup/blueprint/IdAndMarginCollection;

    invoke-direct {p3, v0}, Lcom/squareup/blueprint/IdAndMarginCollection;-><init>(I)V

    .line 156
    new-instance v2, Lcom/squareup/blueprint/IdAndMarginCollection;

    invoke-direct {v2, v0}, Lcom/squareup/blueprint/IdAndMarginCollection;-><init>(I)V

    .line 157
    invoke-virtual {p0}, Lcom/squareup/blueprint/HorizontalBlock;->getElements()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 268
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/blueprint/Block;

    .line 158
    invoke-virtual {v3}, Lcom/squareup/blueprint/Block;->getDependableVertically()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 159
    invoke-virtual {v3, p1}, Lcom/squareup/blueprint/Block;->topIds(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object v3

    invoke-virtual {p3, v3}, Lcom/squareup/blueprint/IdAndMarginCollection;->plusAssign(Lcom/squareup/blueprint/IdsAndMargins;)V

    goto :goto_0

    .line 161
    :cond_0
    invoke-virtual {v3, p1}, Lcom/squareup/blueprint/Block;->topIds(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/blueprint/IdAndMarginCollection;->plusAssign(Lcom/squareup/blueprint/IdsAndMargins;)V

    goto :goto_0

    .line 164
    :cond_1
    invoke-virtual {p3}, Lcom/squareup/blueprint/IdAndMarginCollection;->isNotEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 167
    new-instance v0, Lcom/squareup/blueprint/IdAndMarginCollection;

    invoke-direct {v0, v1}, Lcom/squareup/blueprint/IdAndMarginCollection;-><init>(I)V

    .line 168
    invoke-virtual {p3, p1}, Lcom/squareup/blueprint/IdAndMarginCollection;->asIdAndMargin(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdAndMargin;

    move-result-object p3

    .line 169
    invoke-interface {p2, p3, p1}, Lcom/squareup/blueprint/IdsAndMargins;->connectTo(Lcom/squareup/blueprint/IdAndMargin;Lcom/squareup/blueprint/UpdateContext;)V

    .line 170
    check-cast v2, Lcom/squareup/blueprint/IdsAndMargins;

    invoke-virtual {p3, p1, v2}, Lcom/squareup/blueprint/IdAndMargin;->hookForTheSameSide(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;)Lcom/squareup/blueprint/IdAndMargin;

    move-result-object p3

    .line 171
    new-instance v1, Lcom/squareup/blueprint/IdAndMarginCollection;

    invoke-interface {p2}, Lcom/squareup/blueprint/IdsAndMargins;->getSide()I

    move-result p2

    invoke-direct {v1, p2}, Lcom/squareup/blueprint/IdAndMarginCollection;-><init>(I)V

    .line 172
    invoke-virtual {p0}, Lcom/squareup/blueprint/HorizontalBlock;->getElements()Ljava/util/List;

    move-result-object p2

    check-cast p2, Ljava/lang/Iterable;

    .line 270
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/blueprint/Block;

    .line 173
    invoke-virtual {v2}, Lcom/squareup/blueprint/Block;->getDependableVertically()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 176
    move-object v3, v1

    check-cast v3, Lcom/squareup/blueprint/IdsAndMargins;

    sget-object v4, Lcom/squareup/blueprint/VerticalAlign;->BOTTOM:Lcom/squareup/blueprint/VerticalAlign;

    .line 175
    invoke-virtual {v2, p1, v3, v4}, Lcom/squareup/blueprint/Block;->connectVertically(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;Lcom/squareup/blueprint/VerticalAlign;)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object v2

    goto :goto_2

    .line 178
    :cond_2
    move-object v3, p3

    check-cast v3, Lcom/squareup/blueprint/IdsAndMargins;

    sget-object v4, Lcom/squareup/blueprint/VerticalAlign;->CENTER:Lcom/squareup/blueprint/VerticalAlign;

    invoke-virtual {v2, p1, v3, v4}, Lcom/squareup/blueprint/Block;->connectVertically(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;Lcom/squareup/blueprint/VerticalAlign;)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object v2

    .line 173
    :goto_2
    invoke-virtual {v0, v2}, Lcom/squareup/blueprint/IdAndMarginCollection;->plusAssign(Lcom/squareup/blueprint/IdsAndMargins;)V

    goto :goto_1

    .line 181
    :cond_3
    check-cast v0, Lcom/squareup/blueprint/IdsAndMargins;

    return-object v0

    .line 164
    :cond_4
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "HorizontalBlock aligned BOTTOM need to have at least one dependable item."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 181
    :cond_5
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 147
    :cond_6
    new-instance p3, Lcom/squareup/blueprint/IdAndMarginCollection;

    invoke-direct {p3, v1}, Lcom/squareup/blueprint/IdAndMarginCollection;-><init>(I)V

    .line 148
    invoke-interface {p2, p1}, Lcom/squareup/blueprint/IdsAndMargins;->asIdAndMargin(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdAndMargin;

    move-result-object p2

    .line 149
    invoke-virtual {p0}, Lcom/squareup/blueprint/HorizontalBlock;->getElements()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 266
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/blueprint/Block;

    .line 150
    move-object v2, p2

    check-cast v2, Lcom/squareup/blueprint/IdsAndMargins;

    sget-object v3, Lcom/squareup/blueprint/VerticalAlign;->CENTER:Lcom/squareup/blueprint/VerticalAlign;

    invoke-virtual {v1, p1, v2, v3}, Lcom/squareup/blueprint/Block;->connectVertically(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;Lcom/squareup/blueprint/VerticalAlign;)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object v1

    invoke-virtual {p3, v1}, Lcom/squareup/blueprint/IdAndMarginCollection;->plusAssign(Lcom/squareup/blueprint/IdsAndMargins;)V

    goto :goto_3

    .line 152
    :cond_7
    check-cast p3, Lcom/squareup/blueprint/IdsAndMargins;

    return-object p3

    .line 125
    :cond_8
    invoke-interface {p2, p1}, Lcom/squareup/blueprint/IdsAndMargins;->asIdAndMargin(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdAndMargin;

    move-result-object p2

    .line 126
    new-instance p3, Lcom/squareup/blueprint/IdAndMarginCollection;

    invoke-direct {p3, v1}, Lcom/squareup/blueprint/IdAndMarginCollection;-><init>(I)V

    .line 127
    new-instance v0, Lcom/squareup/blueprint/IdAndMarginCollection;

    invoke-direct {v0, v1}, Lcom/squareup/blueprint/IdAndMarginCollection;-><init>(I)V

    .line 128
    invoke-virtual {p0}, Lcom/squareup/blueprint/HorizontalBlock;->getElements()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 264
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/blueprint/Block;

    .line 129
    invoke-virtual {v2}, Lcom/squareup/blueprint/Block;->getDependableVertically()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 130
    move-object v3, p2

    check-cast v3, Lcom/squareup/blueprint/IdsAndMargins;

    sget-object v4, Lcom/squareup/blueprint/VerticalAlign;->TOP:Lcom/squareup/blueprint/VerticalAlign;

    invoke-virtual {v2, p1, v3, v4}, Lcom/squareup/blueprint/Block;->connectVertically(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;Lcom/squareup/blueprint/VerticalAlign;)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object v2

    invoke-virtual {p3, v2}, Lcom/squareup/blueprint/IdAndMarginCollection;->plusAssign(Lcom/squareup/blueprint/IdsAndMargins;)V

    goto :goto_4

    .line 132
    :cond_9
    move-object v3, p2

    check-cast v3, Lcom/squareup/blueprint/IdsAndMargins;

    sget-object v4, Lcom/squareup/blueprint/VerticalAlign;->CENTER:Lcom/squareup/blueprint/VerticalAlign;

    invoke-virtual {v2, p1, v3, v4}, Lcom/squareup/blueprint/Block;->connectVertically(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;Lcom/squareup/blueprint/VerticalAlign;)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/blueprint/IdAndMarginCollection;->plusAssign(Lcom/squareup/blueprint/IdsAndMargins;)V

    goto :goto_4

    .line 135
    :cond_a
    invoke-virtual {p3}, Lcom/squareup/blueprint/IdAndMarginCollection;->isNotEmpty()Z

    move-result p2

    if-eqz p2, :cond_c

    .line 138
    invoke-virtual {v0}, Lcom/squareup/blueprint/IdAndMarginCollection;->isNotEmpty()Z

    move-result p2

    if-eqz p2, :cond_b

    .line 139
    invoke-virtual {p3, p1}, Lcom/squareup/blueprint/IdAndMarginCollection;->asIdAndMargin(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdAndMargin;

    move-result-object p2

    .line 141
    move-object v1, v0

    check-cast v1, Lcom/squareup/blueprint/IdsAndMargins;

    invoke-virtual {p2, p1, v1}, Lcom/squareup/blueprint/IdAndMargin;->hookForTheSameSide(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;)Lcom/squareup/blueprint/IdAndMargin;

    move-result-object p2

    .line 142
    invoke-virtual {v0, p2, p1}, Lcom/squareup/blueprint/IdAndMarginCollection;->connectTo(Lcom/squareup/blueprint/IdAndMargin;Lcom/squareup/blueprint/UpdateContext;)V

    .line 144
    :cond_b
    check-cast p3, Lcom/squareup/blueprint/IdsAndMargins;

    return-object p3

    .line 135
    :cond_c
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "HorizontalBlock aligned TOP need to have at least one dependable item."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public final copy(Ljava/lang/Object;Ljava/util/List;I)Lcom/squareup/blueprint/HorizontalBlock;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;",
            "Ljava/util/List<",
            "Lcom/squareup/blueprint/Block<",
            "TC;",
            "Lcom/squareup/blueprint/LinearBlock$Params;",
            ">;>;I)",
            "Lcom/squareup/blueprint/HorizontalBlock<",
            "TC;TP;>;"
        }
    .end annotation

    const-string v0, "params"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "elements"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/blueprint/HorizontalBlock;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/blueprint/HorizontalBlock;-><init>(Ljava/lang/Object;Ljava/util/List;I)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/blueprint/HorizontalBlock;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/blueprint/HorizontalBlock;

    invoke-virtual {p0}, Lcom/squareup/blueprint/HorizontalBlock;->getParams()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/blueprint/HorizontalBlock;->getParams()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/blueprint/HorizontalBlock;->getElements()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/blueprint/HorizontalBlock;->getElements()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/blueprint/HorizontalBlock;->extendIndex:I

    iget p1, p1, Lcom/squareup/blueprint/HorizontalBlock;->extendIndex:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getDependableHorizontally()Z
    .locals 1

    .line 42
    iget-boolean v0, p0, Lcom/squareup/blueprint/HorizontalBlock;->dependableHorizontally:Z

    return v0
.end method

.method public getDependableVertically()Z
    .locals 1

    .line 43
    iget-boolean v0, p0, Lcom/squareup/blueprint/HorizontalBlock;->dependableVertically:Z

    return v0
.end method

.method protected getElements()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/blueprint/Block<",
            "TC;",
            "Lcom/squareup/blueprint/LinearBlock$Params;",
            ">;>;"
        }
    .end annotation

    .line 32
    iget-object v0, p0, Lcom/squareup/blueprint/HorizontalBlock;->elements:Ljava/util/List;

    return-object v0
.end method

.method public getParams()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TP;"
        }
    .end annotation

    .line 31
    iget-object v0, p0, Lcom/squareup/blueprint/HorizontalBlock;->params:Ljava/lang/Object;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/blueprint/HorizontalBlock;->getParams()Ljava/lang/Object;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/blueprint/HorizontalBlock;->getElements()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/blueprint/HorizontalBlock;->extendIndex:I

    add-int/2addr v0, v1

    return v0
.end method

.method public startIds(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdsAndMargins;
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 190
    invoke-virtual {p0}, Lcom/squareup/blueprint/HorizontalBlock;->getElements()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/blueprint/Block;

    invoke-virtual {v0, p1}, Lcom/squareup/blueprint/Block;->startIds(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object p1

    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "HorizontalBlock(params="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/blueprint/HorizontalBlock;->getParams()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", elements="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/blueprint/HorizontalBlock;->getElements()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", extendIndex="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/blueprint/HorizontalBlock;->extendIndex:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public topIds(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdAndMarginCollection;
    .locals 3

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 186
    new-instance v0, Lcom/squareup/blueprint/IdAndMarginCollection;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Lcom/squareup/blueprint/IdAndMarginCollection;-><init>(I)V

    .line 187
    invoke-virtual {p0}, Lcom/squareup/blueprint/HorizontalBlock;->getElements()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 272
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/blueprint/Block;

    .line 187
    invoke-virtual {v2, p1}, Lcom/squareup/blueprint/Block;->topIds(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/blueprint/IdAndMarginCollection;->plusAssign(Lcom/squareup/blueprint/IdsAndMargins;)V

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public bridge synthetic topIds(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdsAndMargins;
    .locals 0

    .line 30
    invoke-virtual {p0, p1}, Lcom/squareup/blueprint/HorizontalBlock;->topIds(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdAndMarginCollection;

    move-result-object p1

    check-cast p1, Lcom/squareup/blueprint/IdsAndMargins;

    return-object p1
.end method
