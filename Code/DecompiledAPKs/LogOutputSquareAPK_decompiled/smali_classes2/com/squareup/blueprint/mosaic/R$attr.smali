.class public final Lcom/squareup/blueprint/mosaic/R$attr;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/blueprint/mosaic/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "attr"
.end annotation


# static fields
.field public static final alpha:I = 0x7f04002f

.field public static final barrierAllowsGoneWidgets:I = 0x7f040056

.field public static final barrierDirection:I = 0x7f040057

.field public static final chainUseRtl:I = 0x7f04009d

.field public static final constraintSet:I = 0x7f0400ef

.field public static final constraint_referenced_ids:I = 0x7f0400f0

.field public static final content:I = 0x7f0400f1

.field public static final coordinatorLayoutStyle:I = 0x7f040100

.field public static final emptyVisibility:I = 0x7f040155

.field public static final fastScrollEnabled:I = 0x7f04017e

.field public static final fastScrollHorizontalThumbDrawable:I = 0x7f04017f

.field public static final fastScrollHorizontalTrackDrawable:I = 0x7f040180

.field public static final fastScrollVerticalThumbDrawable:I = 0x7f040181

.field public static final fastScrollVerticalTrackDrawable:I = 0x7f040182

.field public static final font:I = 0x7f04018b

.field public static final fontProviderAuthority:I = 0x7f04018d

.field public static final fontProviderCerts:I = 0x7f04018e

.field public static final fontProviderFetchStrategy:I = 0x7f04018f

.field public static final fontProviderFetchTimeout:I = 0x7f040190

.field public static final fontProviderPackage:I = 0x7f040191

.field public static final fontProviderQuery:I = 0x7f040192

.field public static final fontStyle:I = 0x7f040193

.field public static final fontVariationSettings:I = 0x7f040194

.field public static final fontWeight:I = 0x7f040195

.field public static final keylines:I = 0x7f040233

.field public static final layoutManager:I = 0x7f040239

.field public static final layout_anchor:I = 0x7f04023a

.field public static final layout_anchorGravity:I = 0x7f04023b

.field public static final layout_behavior:I = 0x7f04023c

.field public static final layout_constrainedHeight:I = 0x7f04023f

.field public static final layout_constrainedWidth:I = 0x7f040240

.field public static final layout_constraintBaseline_creator:I = 0x7f040241

.field public static final layout_constraintBaseline_toBaselineOf:I = 0x7f040242

.field public static final layout_constraintBottom_creator:I = 0x7f040243

.field public static final layout_constraintBottom_toBottomOf:I = 0x7f040244

.field public static final layout_constraintBottom_toTopOf:I = 0x7f040245

.field public static final layout_constraintCircle:I = 0x7f040246

.field public static final layout_constraintCircleAngle:I = 0x7f040247

.field public static final layout_constraintCircleRadius:I = 0x7f040248

.field public static final layout_constraintDimensionRatio:I = 0x7f040249

.field public static final layout_constraintEnd_toEndOf:I = 0x7f04024a

.field public static final layout_constraintEnd_toStartOf:I = 0x7f04024b

.field public static final layout_constraintGuide_begin:I = 0x7f04024c

.field public static final layout_constraintGuide_end:I = 0x7f04024d

.field public static final layout_constraintGuide_percent:I = 0x7f04024e

.field public static final layout_constraintHeight_default:I = 0x7f04024f

.field public static final layout_constraintHeight_max:I = 0x7f040250

.field public static final layout_constraintHeight_min:I = 0x7f040251

.field public static final layout_constraintHeight_percent:I = 0x7f040252

.field public static final layout_constraintHorizontal_bias:I = 0x7f040253

.field public static final layout_constraintHorizontal_chainStyle:I = 0x7f040254

.field public static final layout_constraintHorizontal_weight:I = 0x7f040255

.field public static final layout_constraintLeft_creator:I = 0x7f040256

.field public static final layout_constraintLeft_toLeftOf:I = 0x7f040257

.field public static final layout_constraintLeft_toRightOf:I = 0x7f040258

.field public static final layout_constraintRight_creator:I = 0x7f040259

.field public static final layout_constraintRight_toLeftOf:I = 0x7f04025a

.field public static final layout_constraintRight_toRightOf:I = 0x7f04025b

.field public static final layout_constraintStart_toEndOf:I = 0x7f04025c

.field public static final layout_constraintStart_toStartOf:I = 0x7f04025d

.field public static final layout_constraintTop_creator:I = 0x7f04025e

.field public static final layout_constraintTop_toBottomOf:I = 0x7f04025f

.field public static final layout_constraintTop_toTopOf:I = 0x7f040260

.field public static final layout_constraintVertical_bias:I = 0x7f040261

.field public static final layout_constraintVertical_chainStyle:I = 0x7f040262

.field public static final layout_constraintVertical_weight:I = 0x7f040263

.field public static final layout_constraintWidth_default:I = 0x7f040264

.field public static final layout_constraintWidth_max:I = 0x7f040265

.field public static final layout_constraintWidth_min:I = 0x7f040266

.field public static final layout_constraintWidth_percent:I = 0x7f040267

.field public static final layout_dodgeInsetEdges:I = 0x7f040268

.field public static final layout_editor_absoluteX:I = 0x7f04026a

.field public static final layout_editor_absoluteY:I = 0x7f04026b

.field public static final layout_goneMarginBottom:I = 0x7f04026c

.field public static final layout_goneMarginEnd:I = 0x7f04026d

.field public static final layout_goneMarginLeft:I = 0x7f04026e

.field public static final layout_goneMarginRight:I = 0x7f04026f

.field public static final layout_goneMarginStart:I = 0x7f040270

.field public static final layout_goneMarginTop:I = 0x7f040271

.field public static final layout_insetEdge:I = 0x7f040272

.field public static final layout_keyline:I = 0x7f040274

.field public static final layout_optimizationLevel:I = 0x7f040275

.field public static final reverseLayout:I = 0x7f04032f

.field public static final spanCount:I = 0x7f04035d

.field public static final stackFromEnd:I = 0x7f0403d9

.field public static final statusBarBackground:I = 0x7f0403eb

.field public static final ttcIndex:I = 0x7f04048b


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
