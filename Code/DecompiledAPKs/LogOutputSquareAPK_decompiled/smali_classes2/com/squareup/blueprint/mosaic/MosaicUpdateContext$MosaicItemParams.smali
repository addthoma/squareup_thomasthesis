.class public final Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;
.super Ljava/lang/Object;
.source "MosaicUpdateContext.kt"

# interfaces
.implements Lcom/squareup/mosaic/lists/IdParams;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MosaicItemParams"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0010\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u0011\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\u0012\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u00d6\u0003J\t\u0010\u0017\u001a\u00020\u0003H\u00d6\u0001J\u0008\u0010\u0018\u001a\u00020\u0019H\u0016R\u001a\u0010\u0005\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007\"\u0004\u0008\u0008\u0010\u0004R\u001a\u0010\u0002\u001a\u00020\u0003X\u0096\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\t\u0010\u0007\"\u0004\u0008\n\u0010\u0004R\u001a\u0010\u000b\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000c\u0010\u0007\"\u0004\u0008\r\u0010\u0004R\u001a\u0010\u000e\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000f\u0010\u0007\"\u0004\u0008\u0010\u0010\u0004\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;",
        "Lcom/squareup/mosaic/lists/IdParams;",
        "itemId",
        "",
        "(I)V",
        "height",
        "getHeight",
        "()I",
        "setHeight",
        "getItemId",
        "setItemId",
        "viewId",
        "getViewId",
        "setViewId",
        "width",
        "getWidth",
        "setWidth",
        "component1",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "blueprint-mosaic_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private height:I

.field private itemId:I

.field private viewId:I

.field private width:I


# direct methods
.method public constructor <init>(I)V
    .locals 0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;->itemId:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;IILjava/lang/Object;)Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    invoke-virtual {p0}, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;->getItemId()I

    move-result p1

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;->copy(I)Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;->getItemId()I

    move-result v0

    return v0
.end method

.method public final copy(I)Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;
    .locals 1

    new-instance v0, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;

    invoke-direct {v0, p1}, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;-><init>(I)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;

    invoke-virtual {p0}, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;->getItemId()I

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;->getItemId()I

    move-result p1

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getHeight()I
    .locals 1

    .line 43
    iget v0, p0, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;->height:I

    return v0
.end method

.method public getItemId()I
    .locals 1

    .line 39
    iget v0, p0, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;->itemId:I

    return v0
.end method

.method public final getViewId()I
    .locals 1

    .line 41
    iget v0, p0, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;->viewId:I

    return v0
.end method

.method public final getWidth()I
    .locals 1

    .line 42
    iget v0, p0, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;->width:I

    return v0
.end method

.method public hashCode()I
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;->getItemId()I

    move-result v0

    return v0
.end method

.method public final setHeight(I)V
    .locals 0

    .line 43
    iput p1, p0, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;->height:I

    return-void
.end method

.method public setItemId(I)V
    .locals 0

    .line 39
    iput p1, p0, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;->itemId:I

    return-void
.end method

.method public final setViewId(I)V
    .locals 0

    .line 41
    iput p1, p0, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;->viewId:I

    return-void
.end method

.method public final setWidth(I)V
    .locals 0

    .line 42
    iput p1, p0, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;->width:I

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 46
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MosaicItemParams itemId "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;->getItemId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", viewId "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;->viewId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
