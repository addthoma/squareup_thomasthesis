.class public final Lcom/squareup/PosLoggedInComponent_Module_ProvideAdditionalServicesAsSetFactory;
.super Ljava/lang/Object;
.source "PosLoggedInComponent_Module_ProvideAdditionalServicesAsSetFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Ljava/util/Set<",
        "Lmortar/Scoped;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final apgVasarioCashDrawerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;",
            ">;"
        }
    .end annotation
.end field

.field private final cogsPurgerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/CogsPurger;",
            ">;"
        }
    .end annotation
.end field

.field private final crmBillPaymentListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/CrmBillPaymentListener;",
            ">;"
        }
    .end annotation
.end field

.field private final employeeCacheUpdaterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeCacheUpdater;",
            ">;"
        }
    .end annotation
.end field

.field private final hardwarePrintersAnalyticsLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/HardwarePrintersAnalyticsLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceUnitCacheProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoiceUnitCache;",
            ">;"
        }
    .end annotation
.end field

.field private final jailKeeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/jail/CogsJailKeeper;",
            ">;"
        }
    .end annotation
.end field

.field private final jumbotronMessageProducerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;",
            ">;"
        }
    .end annotation
.end field

.field private final ledgerUploaderRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/LedgerUploaderRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentNotifierProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/pending/PaymentNotifier;",
            ">;"
        }
    .end annotation
.end field

.field private final pricingEngineControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/prices/PricingEngineController;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketCountsCacheProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/TicketCountsCache;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/pending/PaymentNotifier;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/TicketCountsCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/HardwarePrintersAnalyticsLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoiceUnitCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/prices/PricingEngineController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/CogsPurger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/jail/CogsJailKeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeCacheUpdater;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/CrmBillPaymentListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/LedgerUploaderRunner;",
            ">;)V"
        }
    .end annotation

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput-object p1, p0, Lcom/squareup/PosLoggedInComponent_Module_ProvideAdditionalServicesAsSetFactory;->paymentNotifierProvider:Ljavax/inject/Provider;

    .line 68
    iput-object p2, p0, Lcom/squareup/PosLoggedInComponent_Module_ProvideAdditionalServicesAsSetFactory;->apgVasarioCashDrawerProvider:Ljavax/inject/Provider;

    .line 69
    iput-object p3, p0, Lcom/squareup/PosLoggedInComponent_Module_ProvideAdditionalServicesAsSetFactory;->ticketCountsCacheProvider:Ljavax/inject/Provider;

    .line 70
    iput-object p4, p0, Lcom/squareup/PosLoggedInComponent_Module_ProvideAdditionalServicesAsSetFactory;->jumbotronMessageProducerProvider:Ljavax/inject/Provider;

    .line 71
    iput-object p5, p0, Lcom/squareup/PosLoggedInComponent_Module_ProvideAdditionalServicesAsSetFactory;->hardwarePrintersAnalyticsLoggerProvider:Ljavax/inject/Provider;

    .line 72
    iput-object p6, p0, Lcom/squareup/PosLoggedInComponent_Module_ProvideAdditionalServicesAsSetFactory;->invoiceUnitCacheProvider:Ljavax/inject/Provider;

    .line 73
    iput-object p7, p0, Lcom/squareup/PosLoggedInComponent_Module_ProvideAdditionalServicesAsSetFactory;->pricingEngineControllerProvider:Ljavax/inject/Provider;

    .line 74
    iput-object p8, p0, Lcom/squareup/PosLoggedInComponent_Module_ProvideAdditionalServicesAsSetFactory;->cogsPurgerProvider:Ljavax/inject/Provider;

    .line 75
    iput-object p9, p0, Lcom/squareup/PosLoggedInComponent_Module_ProvideAdditionalServicesAsSetFactory;->jailKeeperProvider:Ljavax/inject/Provider;

    .line 76
    iput-object p10, p0, Lcom/squareup/PosLoggedInComponent_Module_ProvideAdditionalServicesAsSetFactory;->employeeCacheUpdaterProvider:Ljavax/inject/Provider;

    .line 77
    iput-object p11, p0, Lcom/squareup/PosLoggedInComponent_Module_ProvideAdditionalServicesAsSetFactory;->crmBillPaymentListenerProvider:Ljavax/inject/Provider;

    .line 78
    iput-object p12, p0, Lcom/squareup/PosLoggedInComponent_Module_ProvideAdditionalServicesAsSetFactory;->ledgerUploaderRunnerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/PosLoggedInComponent_Module_ProvideAdditionalServicesAsSetFactory;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/pending/PaymentNotifier;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/TicketCountsCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/HardwarePrintersAnalyticsLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoiceUnitCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/prices/PricingEngineController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/CogsPurger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/jail/CogsJailKeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeCacheUpdater;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/CrmBillPaymentListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/LedgerUploaderRunner;",
            ">;)",
            "Lcom/squareup/PosLoggedInComponent_Module_ProvideAdditionalServicesAsSetFactory;"
        }
    .end annotation

    .line 98
    new-instance v13, Lcom/squareup/PosLoggedInComponent_Module_ProvideAdditionalServicesAsSetFactory;

    move-object v0, v13

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    invoke-direct/range {v0 .. v12}, Lcom/squareup/PosLoggedInComponent_Module_ProvideAdditionalServicesAsSetFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v13
.end method

.method public static provideAdditionalServicesAsSet(Lcom/squareup/payment/pending/PaymentNotifier;Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;Lcom/squareup/tickets/TicketCountsCache;Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;Lcom/squareup/print/HardwarePrintersAnalyticsLogger;Lcom/squareup/invoicesappletapi/InvoiceUnitCache;Lcom/squareup/prices/PricingEngineController;Lcom/squareup/cogs/CogsPurger;Lcom/squareup/jail/CogsJailKeeper;Lcom/squareup/permissions/EmployeeCacheUpdater;Lcom/squareup/payment/CrmBillPaymentListener;Lcom/squareup/payment/ledger/LedgerUploaderRunner;)Ljava/util/Set;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/pending/PaymentNotifier;",
            "Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;",
            "Lcom/squareup/tickets/TicketCountsCache;",
            "Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;",
            "Lcom/squareup/print/HardwarePrintersAnalyticsLogger;",
            "Lcom/squareup/invoicesappletapi/InvoiceUnitCache;",
            "Lcom/squareup/prices/PricingEngineController;",
            "Lcom/squareup/cogs/CogsPurger;",
            "Lcom/squareup/jail/CogsJailKeeper;",
            "Lcom/squareup/permissions/EmployeeCacheUpdater;",
            "Lcom/squareup/payment/CrmBillPaymentListener;",
            "Lcom/squareup/payment/ledger/LedgerUploaderRunner;",
            ")",
            "Ljava/util/Set<",
            "Lmortar/Scoped;",
            ">;"
        }
    .end annotation

    .line 108
    invoke-static/range {p0 .. p11}, Lcom/squareup/PosLoggedInComponent$Module;->provideAdditionalServicesAsSet(Lcom/squareup/payment/pending/PaymentNotifier;Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;Lcom/squareup/tickets/TicketCountsCache;Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;Lcom/squareup/print/HardwarePrintersAnalyticsLogger;Lcom/squareup/invoicesappletapi/InvoiceUnitCache;Lcom/squareup/prices/PricingEngineController;Lcom/squareup/cogs/CogsPurger;Lcom/squareup/jail/CogsJailKeeper;Lcom/squareup/permissions/EmployeeCacheUpdater;Lcom/squareup/payment/CrmBillPaymentListener;Lcom/squareup/payment/ledger/LedgerUploaderRunner;)Ljava/util/Set;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/util/Set;

    return-object p0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/PosLoggedInComponent_Module_ProvideAdditionalServicesAsSetFactory;->get()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public get()Ljava/util/Set;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lmortar/Scoped;",
            ">;"
        }
    .end annotation

    .line 83
    iget-object v0, p0, Lcom/squareup/PosLoggedInComponent_Module_ProvideAdditionalServicesAsSetFactory;->paymentNotifierProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/payment/pending/PaymentNotifier;

    iget-object v0, p0, Lcom/squareup/PosLoggedInComponent_Module_ProvideAdditionalServicesAsSetFactory;->apgVasarioCashDrawerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;

    iget-object v0, p0, Lcom/squareup/PosLoggedInComponent_Module_ProvideAdditionalServicesAsSetFactory;->ticketCountsCacheProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/tickets/TicketCountsCache;

    iget-object v0, p0, Lcom/squareup/PosLoggedInComponent_Module_ProvideAdditionalServicesAsSetFactory;->jumbotronMessageProducerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;

    iget-object v0, p0, Lcom/squareup/PosLoggedInComponent_Module_ProvideAdditionalServicesAsSetFactory;->hardwarePrintersAnalyticsLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/print/HardwarePrintersAnalyticsLogger;

    iget-object v0, p0, Lcom/squareup/PosLoggedInComponent_Module_ProvideAdditionalServicesAsSetFactory;->invoiceUnitCacheProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

    iget-object v0, p0, Lcom/squareup/PosLoggedInComponent_Module_ProvideAdditionalServicesAsSetFactory;->pricingEngineControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/prices/PricingEngineController;

    iget-object v0, p0, Lcom/squareup/PosLoggedInComponent_Module_ProvideAdditionalServicesAsSetFactory;->cogsPurgerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/cogs/CogsPurger;

    iget-object v0, p0, Lcom/squareup/PosLoggedInComponent_Module_ProvideAdditionalServicesAsSetFactory;->jailKeeperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/jail/CogsJailKeeper;

    iget-object v0, p0, Lcom/squareup/PosLoggedInComponent_Module_ProvideAdditionalServicesAsSetFactory;->employeeCacheUpdaterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/permissions/EmployeeCacheUpdater;

    iget-object v0, p0, Lcom/squareup/PosLoggedInComponent_Module_ProvideAdditionalServicesAsSetFactory;->crmBillPaymentListenerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/payment/CrmBillPaymentListener;

    iget-object v0, p0, Lcom/squareup/PosLoggedInComponent_Module_ProvideAdditionalServicesAsSetFactory;->ledgerUploaderRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Lcom/squareup/payment/ledger/LedgerUploaderRunner;

    invoke-static/range {v1 .. v12}, Lcom/squareup/PosLoggedInComponent_Module_ProvideAdditionalServicesAsSetFactory;->provideAdditionalServicesAsSet(Lcom/squareup/payment/pending/PaymentNotifier;Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;Lcom/squareup/tickets/TicketCountsCache;Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;Lcom/squareup/print/HardwarePrintersAnalyticsLogger;Lcom/squareup/invoicesappletapi/InvoiceUnitCache;Lcom/squareup/prices/PricingEngineController;Lcom/squareup/cogs/CogsPurger;Lcom/squareup/jail/CogsJailKeeper;Lcom/squareup/permissions/EmployeeCacheUpdater;Lcom/squareup/payment/CrmBillPaymentListener;Lcom/squareup/payment/ledger/LedgerUploaderRunner;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
