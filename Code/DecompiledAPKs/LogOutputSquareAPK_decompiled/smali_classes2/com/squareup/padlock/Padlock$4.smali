.class Lcom/squareup/padlock/Padlock$4;
.super Lcom/squareup/padlock/Padlock$ButtonInfo;
.source "Padlock.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/padlock/Padlock;->buildDigit(ILjava/lang/String;Lcom/squareup/padlock/Padlock$Key;Lcom/squareup/util/Res;)Lcom/squareup/padlock/Padlock$ButtonInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/padlock/Padlock;

.field final synthetic val$digit:I


# direct methods
.method constructor <init>(Lcom/squareup/padlock/Padlock;Landroid/graphics/drawable/StateListDrawable;Landroid/text/TextPaint;Landroid/text/TextPaint;Ljava/lang/String;Landroid/text/TextPaint;Landroid/text/TextPaint;Ljava/lang/String;Lcom/squareup/padlock/Padlock$ContentType;Lcom/squareup/padlock/Padlock$Key;Lcom/squareup/util/Res;I)V
    .locals 0

    .line 1719
    iput-object p1, p0, Lcom/squareup/padlock/Padlock$4;->this$0:Lcom/squareup/padlock/Padlock;

    iput p12, p0, Lcom/squareup/padlock/Padlock$4;->val$digit:I

    invoke-direct/range {p0 .. p11}, Lcom/squareup/padlock/Padlock$ButtonInfo;-><init>(Lcom/squareup/padlock/Padlock;Landroid/graphics/drawable/StateListDrawable;Landroid/text/TextPaint;Landroid/text/TextPaint;Ljava/lang/String;Landroid/text/TextPaint;Landroid/text/TextPaint;Ljava/lang/String;Lcom/squareup/padlock/Padlock$ContentType;Lcom/squareup/padlock/Padlock$Key;Lcom/squareup/util/Res;)V

    return-void
.end method


# virtual methods
.method protected click(FF)V
    .locals 1

    .line 1721
    iget-object v0, p0, Lcom/squareup/padlock/Padlock$4;->this$0:Lcom/squareup/padlock/Padlock;

    iget-object v0, v0, Lcom/squareup/padlock/Padlock;->onKeyPressListener:Lcom/squareup/padlock/Padlock$OnKeyPressListener;

    if-eqz v0, :cond_1

    .line 1722
    iget-object v0, p0, Lcom/squareup/padlock/Padlock$4;->this$0:Lcom/squareup/padlock/Padlock;

    invoke-static {v0}, Lcom/squareup/padlock/Padlock;->access$900(Lcom/squareup/padlock/Padlock;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1723
    iget-object v0, p0, Lcom/squareup/padlock/Padlock$4;->this$0:Lcom/squareup/padlock/Padlock;

    iget-object v0, v0, Lcom/squareup/padlock/Padlock;->onKeyPressListener:Lcom/squareup/padlock/Padlock$OnKeyPressListener;

    invoke-interface {v0, p1, p2}, Lcom/squareup/padlock/Padlock$OnKeyPressListener;->onPinDigitEntered(FF)V

    goto :goto_0

    .line 1725
    :cond_0
    iget-object p1, p0, Lcom/squareup/padlock/Padlock$4;->this$0:Lcom/squareup/padlock/Padlock;

    iget-object p1, p1, Lcom/squareup/padlock/Padlock;->onKeyPressListener:Lcom/squareup/padlock/Padlock$OnKeyPressListener;

    iget p2, p0, Lcom/squareup/padlock/Padlock$4;->val$digit:I

    invoke-interface {p1, p2}, Lcom/squareup/padlock/Padlock$OnKeyPressListener;->onDigitClicked(I)V

    :cond_1
    :goto_0
    return-void
.end method
