.class Lcom/squareup/padlock/Padlock$5;
.super Lcom/squareup/padlock/Padlock$ButtonInfo;
.source "Padlock.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/padlock/Padlock;->buildDecimal(Lcom/squareup/util/Res;)Lcom/squareup/padlock/Padlock$ButtonInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/padlock/Padlock;


# direct methods
.method constructor <init>(Lcom/squareup/padlock/Padlock;Landroid/graphics/drawable/StateListDrawable;Landroid/text/TextPaint;Landroid/text/TextPaint;Ljava/lang/String;Lcom/squareup/padlock/Padlock$ContentType;Lcom/squareup/padlock/Padlock$Key;Lcom/squareup/util/Res;)V
    .locals 0

    .line 1734
    iput-object p1, p0, Lcom/squareup/padlock/Padlock$5;->this$0:Lcom/squareup/padlock/Padlock;

    invoke-direct/range {p0 .. p8}, Lcom/squareup/padlock/Padlock$ButtonInfo;-><init>(Lcom/squareup/padlock/Padlock;Landroid/graphics/drawable/StateListDrawable;Landroid/text/TextPaint;Landroid/text/TextPaint;Ljava/lang/String;Lcom/squareup/padlock/Padlock$ContentType;Lcom/squareup/padlock/Padlock$Key;Lcom/squareup/util/Res;)V

    return-void
.end method


# virtual methods
.method protected click(FF)V
    .locals 0

    .line 1736
    iget-object p1, p0, Lcom/squareup/padlock/Padlock$5;->this$0:Lcom/squareup/padlock/Padlock;

    iget-object p1, p1, Lcom/squareup/padlock/Padlock;->onKeyPressListener:Lcom/squareup/padlock/Padlock$OnKeyPressListener;

    if-eqz p1, :cond_0

    .line 1737
    iget-object p1, p0, Lcom/squareup/padlock/Padlock$5;->this$0:Lcom/squareup/padlock/Padlock;

    iget-object p1, p1, Lcom/squareup/padlock/Padlock;->onKeyPressListener:Lcom/squareup/padlock/Padlock$OnKeyPressListener;

    invoke-interface {p1}, Lcom/squareup/padlock/Padlock$OnKeyPressListener;->onDecimalClicked()V

    :cond_0
    return-void
.end method
