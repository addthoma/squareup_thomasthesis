.class public final enum Lcom/squareup/padlock/Padlock$Key;
.super Ljava/lang/Enum;
.source "Padlock.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/padlock/Padlock;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Key"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/padlock/Padlock$Key;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/padlock/Padlock$Key;

.field public static final enum BACKSPACE:Lcom/squareup/padlock/Padlock$Key;

.field public static final enum CHECK_OR_SKIP:Lcom/squareup/padlock/Padlock$Key;

.field public static final enum CLEAR_OR_CANCEL:Lcom/squareup/padlock/Padlock$Key;

.field public static final enum DECIMAL:Lcom/squareup/padlock/Padlock$Key;

.field public static final enum DOUBLE_ZERO:Lcom/squareup/padlock/Padlock$Key;

.field public static final enum EIGHT:Lcom/squareup/padlock/Padlock$Key;

.field public static final enum FIVE:Lcom/squareup/padlock/Padlock$Key;

.field public static final enum FOUR:Lcom/squareup/padlock/Padlock$Key;

.field public static final enum NINE:Lcom/squareup/padlock/Padlock$Key;

.field public static final enum ONE:Lcom/squareup/padlock/Padlock$Key;

.field public static final enum PLUS:Lcom/squareup/padlock/Padlock$Key;

.field public static final enum ROUNDED_PLUS:Lcom/squareup/padlock/Padlock$Key;

.field public static final enum SEVEN:Lcom/squareup/padlock/Padlock$Key;

.field public static final enum SIX:Lcom/squareup/padlock/Padlock$Key;

.field public static final enum SUBMIT:Lcom/squareup/padlock/Padlock$Key;

.field public static final enum THREE:Lcom/squareup/padlock/Padlock$Key;

.field public static final enum TWO:Lcom/squareup/padlock/Padlock$Key;

.field public static final enum UNKNOWN:Lcom/squareup/padlock/Padlock$Key;

.field public static final enum ZERO:Lcom/squareup/padlock/Padlock$Key;


# instance fields
.field private descriptionId:I

.field private displayValueId:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 162
    new-instance v0, Lcom/squareup/padlock/Padlock$Key;

    sget v1, Lcom/squareup/padlock/R$string;->text_0:I

    sget v2, Lcom/squareup/padlock/R$string;->button_0:I

    const/4 v3, 0x0

    const-string v4, "ZERO"

    invoke-direct {v0, v4, v3, v1, v2}, Lcom/squareup/padlock/Padlock$Key;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/squareup/padlock/Padlock$Key;->ZERO:Lcom/squareup/padlock/Padlock$Key;

    .line 163
    new-instance v0, Lcom/squareup/padlock/Padlock$Key;

    sget v1, Lcom/squareup/padlock/R$string;->text_1:I

    sget v2, Lcom/squareup/padlock/R$string;->button_1:I

    const/4 v4, 0x1

    const-string v5, "ONE"

    invoke-direct {v0, v5, v4, v1, v2}, Lcom/squareup/padlock/Padlock$Key;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/squareup/padlock/Padlock$Key;->ONE:Lcom/squareup/padlock/Padlock$Key;

    .line 164
    new-instance v0, Lcom/squareup/padlock/Padlock$Key;

    sget v1, Lcom/squareup/padlock/R$string;->text_2:I

    sget v2, Lcom/squareup/padlock/R$string;->button_2:I

    const/4 v5, 0x2

    const-string v6, "TWO"

    invoke-direct {v0, v6, v5, v1, v2}, Lcom/squareup/padlock/Padlock$Key;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/squareup/padlock/Padlock$Key;->TWO:Lcom/squareup/padlock/Padlock$Key;

    .line 165
    new-instance v0, Lcom/squareup/padlock/Padlock$Key;

    sget v1, Lcom/squareup/padlock/R$string;->text_3:I

    sget v2, Lcom/squareup/padlock/R$string;->button_3:I

    const/4 v6, 0x3

    const-string v7, "THREE"

    invoke-direct {v0, v7, v6, v1, v2}, Lcom/squareup/padlock/Padlock$Key;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/squareup/padlock/Padlock$Key;->THREE:Lcom/squareup/padlock/Padlock$Key;

    .line 166
    new-instance v0, Lcom/squareup/padlock/Padlock$Key;

    sget v1, Lcom/squareup/padlock/R$string;->text_4:I

    sget v2, Lcom/squareup/padlock/R$string;->button_4:I

    const/4 v7, 0x4

    const-string v8, "FOUR"

    invoke-direct {v0, v8, v7, v1, v2}, Lcom/squareup/padlock/Padlock$Key;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/squareup/padlock/Padlock$Key;->FOUR:Lcom/squareup/padlock/Padlock$Key;

    .line 167
    new-instance v0, Lcom/squareup/padlock/Padlock$Key;

    sget v1, Lcom/squareup/padlock/R$string;->text_5:I

    sget v2, Lcom/squareup/padlock/R$string;->button_5:I

    const/4 v8, 0x5

    const-string v9, "FIVE"

    invoke-direct {v0, v9, v8, v1, v2}, Lcom/squareup/padlock/Padlock$Key;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/squareup/padlock/Padlock$Key;->FIVE:Lcom/squareup/padlock/Padlock$Key;

    .line 168
    new-instance v0, Lcom/squareup/padlock/Padlock$Key;

    sget v1, Lcom/squareup/padlock/R$string;->text_6:I

    sget v2, Lcom/squareup/padlock/R$string;->button_6:I

    const/4 v9, 0x6

    const-string v10, "SIX"

    invoke-direct {v0, v10, v9, v1, v2}, Lcom/squareup/padlock/Padlock$Key;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/squareup/padlock/Padlock$Key;->SIX:Lcom/squareup/padlock/Padlock$Key;

    .line 169
    new-instance v0, Lcom/squareup/padlock/Padlock$Key;

    sget v1, Lcom/squareup/padlock/R$string;->text_7:I

    sget v2, Lcom/squareup/padlock/R$string;->button_7:I

    const/4 v10, 0x7

    const-string v11, "SEVEN"

    invoke-direct {v0, v11, v10, v1, v2}, Lcom/squareup/padlock/Padlock$Key;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/squareup/padlock/Padlock$Key;->SEVEN:Lcom/squareup/padlock/Padlock$Key;

    .line 170
    new-instance v0, Lcom/squareup/padlock/Padlock$Key;

    sget v1, Lcom/squareup/padlock/R$string;->text_8:I

    sget v2, Lcom/squareup/padlock/R$string;->button_8:I

    const/16 v11, 0x8

    const-string v12, "EIGHT"

    invoke-direct {v0, v12, v11, v1, v2}, Lcom/squareup/padlock/Padlock$Key;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/squareup/padlock/Padlock$Key;->EIGHT:Lcom/squareup/padlock/Padlock$Key;

    .line 171
    new-instance v0, Lcom/squareup/padlock/Padlock$Key;

    sget v1, Lcom/squareup/padlock/R$string;->text_9:I

    sget v2, Lcom/squareup/padlock/R$string;->button_9:I

    const/16 v12, 0x9

    const-string v13, "NINE"

    invoke-direct {v0, v13, v12, v1, v2}, Lcom/squareup/padlock/Padlock$Key;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/squareup/padlock/Padlock$Key;->NINE:Lcom/squareup/padlock/Padlock$Key;

    .line 172
    new-instance v0, Lcom/squareup/padlock/Padlock$Key;

    sget v1, Lcom/squareup/padlock/R$string;->text_00:I

    sget v2, Lcom/squareup/padlock/R$string;->button_00:I

    const/16 v13, 0xa

    const-string v14, "DOUBLE_ZERO"

    invoke-direct {v0, v14, v13, v1, v2}, Lcom/squareup/padlock/Padlock$Key;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/squareup/padlock/Padlock$Key;->DOUBLE_ZERO:Lcom/squareup/padlock/Padlock$Key;

    .line 173
    new-instance v0, Lcom/squareup/padlock/Padlock$Key;

    sget v1, Lcom/squareup/padlock/R$string;->text_submit:I

    sget v2, Lcom/squareup/padlock/R$string;->button_submit:I

    const/16 v14, 0xb

    const-string v15, "SUBMIT"

    invoke-direct {v0, v15, v14, v1, v2}, Lcom/squareup/padlock/Padlock$Key;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/squareup/padlock/Padlock$Key;->SUBMIT:Lcom/squareup/padlock/Padlock$Key;

    .line 174
    new-instance v0, Lcom/squareup/padlock/Padlock$Key;

    sget v1, Lcom/squareup/padlock/R$string;->text_backspace:I

    sget v2, Lcom/squareup/padlock/R$string;->button_backspace:I

    const/16 v15, 0xc

    const-string v14, "BACKSPACE"

    invoke-direct {v0, v14, v15, v1, v2}, Lcom/squareup/padlock/Padlock$Key;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/squareup/padlock/Padlock$Key;->BACKSPACE:Lcom/squareup/padlock/Padlock$Key;

    .line 175
    new-instance v0, Lcom/squareup/padlock/Padlock$Key;

    const/16 v1, 0xd

    const-string v2, "CLEAR_OR_CANCEL"

    invoke-direct {v0, v2, v1}, Lcom/squareup/padlock/Padlock$Key;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/padlock/Padlock$Key;->CLEAR_OR_CANCEL:Lcom/squareup/padlock/Padlock$Key;

    .line 176
    new-instance v0, Lcom/squareup/padlock/Padlock$Key;

    sget v2, Lcom/squareup/padlock/R$string;->text_decimal:I

    sget v14, Lcom/squareup/padlock/R$string;->button_decimal:I

    const/16 v1, 0xe

    const-string v15, "DECIMAL"

    invoke-direct {v0, v15, v1, v2, v14}, Lcom/squareup/padlock/Padlock$Key;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/squareup/padlock/Padlock$Key;->DECIMAL:Lcom/squareup/padlock/Padlock$Key;

    .line 177
    new-instance v0, Lcom/squareup/padlock/Padlock$Key;

    const-string v2, "CHECK_OR_SKIP"

    const/16 v14, 0xf

    invoke-direct {v0, v2, v14}, Lcom/squareup/padlock/Padlock$Key;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/padlock/Padlock$Key;->CHECK_OR_SKIP:Lcom/squareup/padlock/Padlock$Key;

    .line 178
    new-instance v0, Lcom/squareup/padlock/Padlock$Key;

    sget v2, Lcom/squareup/padlock/R$string;->text_plus:I

    sget v14, Lcom/squareup/padlock/R$string;->button_plus:I

    const-string v15, "PLUS"

    const/16 v1, 0x10

    invoke-direct {v0, v15, v1, v2, v14}, Lcom/squareup/padlock/Padlock$Key;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/squareup/padlock/Padlock$Key;->PLUS:Lcom/squareup/padlock/Padlock$Key;

    .line 179
    new-instance v0, Lcom/squareup/padlock/Padlock$Key;

    sget v1, Lcom/squareup/padlock/R$string;->text_plus:I

    sget v2, Lcom/squareup/padlock/R$string;->button_plus:I

    const-string v14, "ROUNDED_PLUS"

    const/16 v15, 0x11

    invoke-direct {v0, v14, v15, v1, v2}, Lcom/squareup/padlock/Padlock$Key;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/squareup/padlock/Padlock$Key;->ROUNDED_PLUS:Lcom/squareup/padlock/Padlock$Key;

    .line 180
    new-instance v0, Lcom/squareup/padlock/Padlock$Key;

    const-string v1, "UNKNOWN"

    const/16 v2, 0x12

    const/4 v14, -0x1

    const/4 v15, -0x1

    invoke-direct {v0, v1, v2, v14, v15}, Lcom/squareup/padlock/Padlock$Key;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/squareup/padlock/Padlock$Key;->UNKNOWN:Lcom/squareup/padlock/Padlock$Key;

    const/16 v0, 0x13

    new-array v0, v0, [Lcom/squareup/padlock/Padlock$Key;

    .line 161
    sget-object v1, Lcom/squareup/padlock/Padlock$Key;->ZERO:Lcom/squareup/padlock/Padlock$Key;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/padlock/Padlock$Key;->ONE:Lcom/squareup/padlock/Padlock$Key;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/padlock/Padlock$Key;->TWO:Lcom/squareup/padlock/Padlock$Key;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/padlock/Padlock$Key;->THREE:Lcom/squareup/padlock/Padlock$Key;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/padlock/Padlock$Key;->FOUR:Lcom/squareup/padlock/Padlock$Key;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/padlock/Padlock$Key;->FIVE:Lcom/squareup/padlock/Padlock$Key;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/padlock/Padlock$Key;->SIX:Lcom/squareup/padlock/Padlock$Key;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/padlock/Padlock$Key;->SEVEN:Lcom/squareup/padlock/Padlock$Key;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/padlock/Padlock$Key;->EIGHT:Lcom/squareup/padlock/Padlock$Key;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/padlock/Padlock$Key;->NINE:Lcom/squareup/padlock/Padlock$Key;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/padlock/Padlock$Key;->DOUBLE_ZERO:Lcom/squareup/padlock/Padlock$Key;

    aput-object v1, v0, v13

    sget-object v1, Lcom/squareup/padlock/Padlock$Key;->SUBMIT:Lcom/squareup/padlock/Padlock$Key;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/padlock/Padlock$Key;->BACKSPACE:Lcom/squareup/padlock/Padlock$Key;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/padlock/Padlock$Key;->CLEAR_OR_CANCEL:Lcom/squareup/padlock/Padlock$Key;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/padlock/Padlock$Key;->DECIMAL:Lcom/squareup/padlock/Padlock$Key;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/padlock/Padlock$Key;->CHECK_OR_SKIP:Lcom/squareup/padlock/Padlock$Key;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/padlock/Padlock$Key;->PLUS:Lcom/squareup/padlock/Padlock$Key;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/padlock/Padlock$Key;->ROUNDED_PLUS:Lcom/squareup/padlock/Padlock$Key;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/padlock/Padlock$Key;->UNKNOWN:Lcom/squareup/padlock/Padlock$Key;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/padlock/Padlock$Key;->$VALUES:[Lcom/squareup/padlock/Padlock$Key;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    const/4 v0, -0x1

    .line 194
    invoke-direct {p0, p1, p2, v0, v0}, Lcom/squareup/padlock/Padlock$Key;-><init>(Ljava/lang/String;III)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .line 197
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 198
    iput p3, p0, Lcom/squareup/padlock/Padlock$Key;->displayValueId:I

    .line 199
    iput p4, p0, Lcom/squareup/padlock/Padlock$Key;->descriptionId:I

    return-void
.end method

.method public static valueOf(I)Lcom/squareup/padlock/Padlock$Key;
    .locals 1

    .line 203
    invoke-static {}, Lcom/squareup/padlock/Padlock$Key;->values()[Lcom/squareup/padlock/Padlock$Key;

    move-result-object v0

    aget-object p0, v0, p0

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/padlock/Padlock$Key;
    .locals 1

    .line 161
    const-class v0, Lcom/squareup/padlock/Padlock$Key;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/padlock/Padlock$Key;

    return-object p0
.end method

.method public static values()[Lcom/squareup/padlock/Padlock$Key;
    .locals 1

    .line 161
    sget-object v0, Lcom/squareup/padlock/Padlock$Key;->$VALUES:[Lcom/squareup/padlock/Padlock$Key;

    invoke-virtual {v0}, [Lcom/squareup/padlock/Padlock$Key;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/padlock/Padlock$Key;

    return-object v0
.end method


# virtual methods
.method public getDescription(Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 2

    .line 190
    iget v0, p0, Lcom/squareup/padlock/Padlock$Key;->descriptionId:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public getDisplayValue(Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 2

    .line 186
    iget v0, p0, Lcom/squareup/padlock/Padlock$Key;->displayValueId:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method
