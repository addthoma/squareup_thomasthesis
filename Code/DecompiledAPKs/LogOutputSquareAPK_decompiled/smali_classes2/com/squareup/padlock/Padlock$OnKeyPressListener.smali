.class public interface abstract Lcom/squareup/padlock/Padlock$OnKeyPressListener;
.super Ljava/lang/Object;
.source "Padlock.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/padlock/Padlock;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnKeyPressListener"
.end annotation


# virtual methods
.method public abstract onBackspaceClicked()V
.end method

.method public abstract onCancelClicked()V
.end method

.method public abstract onClearClicked()V
.end method

.method public abstract onClearLongpressed()V
.end method

.method public abstract onDecimalClicked()V
.end method

.method public abstract onDigitClicked(I)V
.end method

.method public abstract onPinDigitEntered(FF)V
.end method

.method public abstract onSkipClicked()V
.end method

.method public abstract onSubmitClicked()V
.end method
