.class public Lcom/squareup/ApiUrlSelector$LoggedInUrlMonitor;
.super Ljava/lang/Object;
.source "ApiUrlSelector.java"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ApiUrlSelector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LoggedInUrlMonitor"
.end annotation


# instance fields
.field private final accountStatusProvider:Lcom/squareup/accountstatus/AccountStatusProvider;

.field private final apiUrlSelector:Lcom/squareup/ApiUrlSelector;


# direct methods
.method constructor <init>(Lcom/squareup/accountstatus/AccountStatusProvider;Lcom/squareup/ApiUrlSelector;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Lcom/squareup/ApiUrlSelector$LoggedInUrlMonitor;->accountStatusProvider:Lcom/squareup/accountstatus/AccountStatusProvider;

    .line 58
    iput-object p2, p0, Lcom/squareup/ApiUrlSelector$LoggedInUrlMonitor;->apiUrlSelector:Lcom/squareup/ApiUrlSelector;

    return-void
.end method


# virtual methods
.method public synthetic lambda$onEnterScope$0$ApiUrlSelector$LoggedInUrlMonitor(Lcom/squareup/server/account/protos/AccountStatusResponse;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 65
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->api_url_list:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->api_url_list:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 68
    :cond_0
    iget-object v0, p0, Lcom/squareup/ApiUrlSelector$LoggedInUrlMonitor;->apiUrlSelector:Lcom/squareup/ApiUrlSelector;

    iget-object p1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->api_url_list:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/squareup/ApiUrlSelector;->access$100(Lcom/squareup/ApiUrlSelector;Ljava/util/List;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 62
    iget-object v0, p0, Lcom/squareup/ApiUrlSelector$LoggedInUrlMonitor;->accountStatusProvider:Lcom/squareup/accountstatus/AccountStatusProvider;

    .line 63
    invoke-interface {v0}, Lcom/squareup/accountstatus/AccountStatusProvider;->latest()Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/squareup/util/OptionalExtensionsKt;->mapIfPresentObservable()Lio/reactivex/ObservableTransformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/-$$Lambda$ApiUrlSelector$LoggedInUrlMonitor$Dzf7j4ao_cA8mWG6Znr8Sj7ov6c;

    invoke-direct {v1, p0}, Lcom/squareup/-$$Lambda$ApiUrlSelector$LoggedInUrlMonitor$Dzf7j4ao_cA8mWG6Znr8Sj7ov6c;-><init>(Lcom/squareup/ApiUrlSelector$LoggedInUrlMonitor;)V

    .line 64
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 62
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public onExitScope()V
    .locals 2

    .line 74
    iget-object v0, p0, Lcom/squareup/ApiUrlSelector$LoggedInUrlMonitor;->apiUrlSelector:Lcom/squareup/ApiUrlSelector;

    invoke-static {v0}, Lcom/squareup/ApiUrlSelector;->access$000(Lcom/squareup/ApiUrlSelector;)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/ApiUrlSelector;->access$100(Lcom/squareup/ApiUrlSelector;Ljava/util/List;)V

    return-void
.end method
