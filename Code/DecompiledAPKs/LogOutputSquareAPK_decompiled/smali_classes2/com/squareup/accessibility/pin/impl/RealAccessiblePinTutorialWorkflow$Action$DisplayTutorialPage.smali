.class public final Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$Action$DisplayTutorialPage;
.super Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$Action;
.source "RealAccessiblePinTutorialWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$Action;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DisplayTutorialPage"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\u0008\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\t\u001a\u00020\n2\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u000cH\u00d6\u0003J\t\u0010\r\u001a\u00020\u0003H\u00d6\u0001J\t\u0010\u000e\u001a\u00020\u000fH\u00d6\u0001J\u0018\u0010\u0010\u001a\u00020\u0011*\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u00110\u0012H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$Action$DisplayTutorialPage;",
        "Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$Action;",
        "page",
        "",
        "(I)V",
        "getPage",
        "()I",
        "component1",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "apply",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/accessibility/pin/AccessiblePinTutorialState;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final page:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    const/4 v0, 0x0

    .line 91
    invoke-direct {p0, v0}, Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$Action;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput p1, p0, Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$Action$DisplayTutorialPage;->page:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$Action$DisplayTutorialPage;IILjava/lang/Object;)Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$Action$DisplayTutorialPage;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget p1, p0, Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$Action$DisplayTutorialPage;->page:I

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$Action$DisplayTutorialPage;->copy(I)Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$Action$DisplayTutorialPage;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/accessibility/pin/AccessiblePinTutorialState;",
            "-",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    new-instance v0, Lcom/squareup/accessibility/pin/DisplayingTutorial;

    iget v1, p0, Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$Action$DisplayTutorialPage;->page:I

    invoke-direct {v0, v1}, Lcom/squareup/accessibility/pin/DisplayingTutorial;-><init>(I)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    return-void
.end method

.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$Action$DisplayTutorialPage;->page:I

    return v0
.end method

.method public final copy(I)Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$Action$DisplayTutorialPage;
    .locals 1

    new-instance v0, Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$Action$DisplayTutorialPage;

    invoke-direct {v0, p1}, Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$Action$DisplayTutorialPage;-><init>(I)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$Action$DisplayTutorialPage;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$Action$DisplayTutorialPage;

    iget v0, p0, Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$Action$DisplayTutorialPage;->page:I

    iget p1, p1, Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$Action$DisplayTutorialPage;->page:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getPage()I
    .locals 1

    .line 91
    iget v0, p0, Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$Action$DisplayTutorialPage;->page:I

    return v0
.end method

.method public hashCode()I
    .locals 1

    iget v0, p0, Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$Action$DisplayTutorialPage;->page:I

    invoke-static {v0}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DisplayTutorialPage(page="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$Action$DisplayTutorialPage;->page:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
