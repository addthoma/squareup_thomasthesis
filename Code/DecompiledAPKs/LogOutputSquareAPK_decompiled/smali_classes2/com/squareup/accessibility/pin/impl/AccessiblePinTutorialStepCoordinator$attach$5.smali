.class final Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator$attach$5;
.super Lkotlin/jvm/internal/Lambda;
.source "AccessiblePinTutorialStepCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lkotlin/Pair<",
        "+",
        "Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepScreen;",
        "+",
        "Ljava/lang/Boolean;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAccessiblePinTutorialStepCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AccessiblePinTutorialStepCoordinator.kt\ncom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator$attach$5\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,128:1\n1103#2,7:129\n*E\n*S KotlinDebug\n*F\n+ 1 AccessiblePinTutorialStepCoordinator.kt\ncom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator$attach$5\n*L\n104#1,7:129\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0010\u0000\u001a\u00020\u00012\"\u0010\u0002\u001a\u001e\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00060\u00060\u0003H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepScreen;",
        "kotlin.jvm.PlatformType",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator$attach$5;->this$0:Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 31
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator$attach$5;->invoke(Lkotlin/Pair;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lkotlin/Pair;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepScreen;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepScreen;

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    .line 104
    iget-object v1, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator$attach$5;->this$0:Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;

    invoke-static {v1}, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;->access$getCancelButton$p(Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;)Landroid/widget/ImageView;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 129
    new-instance v2, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator$attach$5$$special$$inlined$onClickDebounced$1;

    invoke-direct {v2, p0, v0}, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator$attach$5$$special$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator$attach$5;Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepScreen;)V

    check-cast v2, Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const-string v1, "done"

    .line 106
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 107
    invoke-virtual {v0}, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepScreen;->getTutorialDone()Lkotlin/jvm/functions/Function0;

    move-result-object p1

    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    :cond_0
    return-void
.end method
