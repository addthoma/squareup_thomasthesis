.class final Lcom/squareup/caller/ProgressPopup$Progress$1;
.super Ljava/lang/Object;
.source "ProgressPopup.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/caller/ProgressPopup$Progress;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/squareup/caller/ProgressPopup$Progress;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/squareup/caller/ProgressPopup$Progress;
    .locals 2

    .line 57
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 58
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result p1

    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 59
    :goto_0
    new-instance p1, Lcom/squareup/caller/ProgressPopup$Progress;

    invoke-direct {p1, v0, v1}, Lcom/squareup/caller/ProgressPopup$Progress;-><init>(Ljava/lang/String;Z)V

    return-object p1
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 55
    invoke-virtual {p0, p1}, Lcom/squareup/caller/ProgressPopup$Progress$1;->createFromParcel(Landroid/os/Parcel;)Lcom/squareup/caller/ProgressPopup$Progress;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/squareup/caller/ProgressPopup$Progress;
    .locals 0

    .line 63
    new-array p1, p1, [Lcom/squareup/caller/ProgressPopup$Progress;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 55
    invoke-virtual {p0, p1}, Lcom/squareup/caller/ProgressPopup$Progress$1;->newArray(I)[Lcom/squareup/caller/ProgressPopup$Progress;

    move-result-object p1

    return-object p1
.end method
