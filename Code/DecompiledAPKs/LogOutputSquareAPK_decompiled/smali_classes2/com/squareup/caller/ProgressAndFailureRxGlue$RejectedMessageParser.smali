.class public interface abstract Lcom/squareup/caller/ProgressAndFailureRxGlue$RejectedMessageParser;
.super Ljava/lang/Object;
.source "ProgressAndFailureRxGlue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/caller/ProgressAndFailureRxGlue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "RejectedMessageParser"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract parse(Ljava/lang/Object;)Lcom/squareup/receiving/FailureMessage$Parts;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Lcom/squareup/receiving/FailureMessage$Parts;"
        }
    .end annotation
.end method
