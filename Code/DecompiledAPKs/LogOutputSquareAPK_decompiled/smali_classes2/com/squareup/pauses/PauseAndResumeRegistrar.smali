.class public interface abstract Lcom/squareup/pauses/PauseAndResumeRegistrar;
.super Ljava/lang/Object;
.source "PauseAndResumeRegistrar.java"


# virtual methods
.method public abstract isRunning()Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract isRunningState()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract register(Lmortar/MortarScope;Lcom/squareup/pauses/PausesAndResumes;)V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method
