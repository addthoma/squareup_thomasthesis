.class public interface abstract Lcom/squareup/SposReleaseLoggedInComponent;
.super Ljava/lang/Object;
.source "SposReleaseLoggedInComponent.java"

# interfaces
.implements Lcom/squareup/PosReleaseLoggedInComponent;
.implements Lcom/squareup/SposLoggedInComponent;


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/CommonLoggedInReleaseModule;,
        Lcom/squareup/PosReleaseLoggedInComponent$Module;,
        Lcom/squareup/print/PrintModule$Scouts;,
        Lcom/squareup/server/tickets/ReleaseTicketsServiceModule;,
        Lcom/squareup/SposLoggedInModule;,
        Lcom/squareup/pos/tour/SposWhatsNewTourProviderModule;,
        Lcom/squareup/ui/help/messages/SupportMessagingLoggedInModule;
    }
.end annotation


# virtual methods
.method public abstract mainActivity()Lcom/squareup/ui/root/SposReleaseMainActivityComponent;
.end method
