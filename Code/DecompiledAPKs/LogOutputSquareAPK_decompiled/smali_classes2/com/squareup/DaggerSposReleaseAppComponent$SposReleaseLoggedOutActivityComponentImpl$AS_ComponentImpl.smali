.class final Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl$AS_ComponentImpl;
.super Ljava/lang/Object;
.source "DaggerSposReleaseAppComponent.java"

# interfaces
.implements Lcom/squareup/ui/login/AuthenticatorScope$Component;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "AS_ComponentImpl"
.end annotation


# instance fields
.field private authenticatorScopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/login/AuthenticatorScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;


# direct methods
.method private constructor <init>(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl$AS_ComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl$AS_ComponentImpl;->initialize()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;Lcom/squareup/DaggerSposReleaseAppComponent$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl$AS_ComponentImpl;-><init>(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;)V

    return-void
.end method

.method private initialize()V
    .locals 2

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl$AS_ComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->access$268300(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl$AS_ComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->access$268400(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/ui/login/AuthenticatorScopeRunner_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/login/AuthenticatorScopeRunner_Factory;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl$AS_ComponentImpl;->authenticatorScopeRunnerProvider:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public authenticatorScopeRunner()Lcom/squareup/ui/login/AuthenticatorScopeRunner;
    .locals 1

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl$AS_ComponentImpl;->authenticatorScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/login/AuthenticatorScopeRunner;

    return-object v0
.end method
