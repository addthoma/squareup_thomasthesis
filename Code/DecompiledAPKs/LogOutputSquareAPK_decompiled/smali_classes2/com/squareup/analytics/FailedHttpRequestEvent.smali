.class public Lcom/squareup/analytics/FailedHttpRequestEvent;
.super Lcom/squareup/eventstream/v2/AppEvent;
.source "FailedHttpRequestEvent.java"


# static fields
.field private static final CATALOG_NAME:Ljava/lang/String; = "failed_http_request"


# instance fields
.field public final failed_http_request_elapsed_time_ms:J

.field public final failed_http_request_exception_class:Ljava/lang/String;

.field public final failed_http_request_exception_message:Ljava/lang/String;

.field public final failed_http_request_protocol:Ljava/lang/String;

.field public final failed_http_request_relative_path:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;J)V
    .locals 1

    const-string v0, "failed_http_request"

    .line 18
    invoke-direct {p0, v0}, Lcom/squareup/eventstream/v2/AppEvent;-><init>(Ljava/lang/String;)V

    .line 19
    iput-object p1, p0, Lcom/squareup/analytics/FailedHttpRequestEvent;->failed_http_request_protocol:Ljava/lang/String;

    .line 20
    iput-object p2, p0, Lcom/squareup/analytics/FailedHttpRequestEvent;->failed_http_request_relative_path:Ljava/lang/String;

    .line 21
    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/analytics/FailedHttpRequestEvent;->failed_http_request_exception_class:Ljava/lang/String;

    .line 22
    invoke-virtual {p3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/util/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/analytics/FailedHttpRequestEvent;->failed_http_request_exception_message:Ljava/lang/String;

    .line 23
    iput-wide p4, p0, Lcom/squareup/analytics/FailedHttpRequestEvent;->failed_http_request_elapsed_time_ms:J

    return-void
.end method


# virtual methods
.method public asOhSnapMessage()Ljava/lang/String;
    .locals 4

    .line 27
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/squareup/analytics/FailedHttpRequestEvent;->failed_http_request_protocol:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/squareup/analytics/FailedHttpRequestEvent;->failed_http_request_relative_path:Ljava/lang/String;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/squareup/analytics/FailedHttpRequestEvent;->failed_http_request_exception_class:Ljava/lang/String;

    const/4 v3, 0x2

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/squareup/analytics/FailedHttpRequestEvent;->failed_http_request_exception_message:Ljava/lang/String;

    const/4 v3, 0x3

    aput-object v2, v1, v3

    iget-wide v2, p0, Lcom/squareup/analytics/FailedHttpRequestEvent;->failed_http_request_elapsed_time_ms:J

    .line 30
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const/4 v3, 0x4

    aput-object v2, v1, v3

    const-string v2, "%s %s=%s (\"%s\") %dms"

    .line 27
    invoke-static {v0, v2, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
