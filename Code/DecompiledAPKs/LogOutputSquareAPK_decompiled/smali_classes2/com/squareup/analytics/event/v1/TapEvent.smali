.class public abstract Lcom/squareup/analytics/event/v1/TapEvent;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "TapEvent.java"


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/EventNamedTap;)V
    .locals 1

    .line 13
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->TAP:Lcom/squareup/eventstream/v1/EventStream$Name;

    invoke-interface {p1}, Lcom/squareup/analytics/EventNamedTap;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, v0, p1}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    return-void
.end method
