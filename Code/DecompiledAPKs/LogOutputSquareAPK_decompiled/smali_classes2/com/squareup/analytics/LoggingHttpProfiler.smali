.class public Lcom/squareup/analytics/LoggingHttpProfiler;
.super Ljava/lang/Object;
.source "LoggingHttpProfiler.java"

# interfaces
.implements Lcom/squareup/http/HttpProfiler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/analytics/LoggingHttpProfiler$ServerRequest;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/http/HttpProfiler<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field private static final EXEMPTED_PATHS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final analytics:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final ohSnapLogger:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string v0, "/1.0/log/eventstream"

    const-string v1, "/2.0/log/eventstream"

    .line 20
    filled-new-array {v0, v1}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/squareup/analytics/LoggingHttpProfiler;->EXEMPTED_PATHS:Ljava/util/List;

    return-void
.end method

.method constructor <init>(Ldagger/Lazy;Ldagger/Lazy;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;",
            "Ldagger/Lazy<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/analytics/LoggingHttpProfiler;->ohSnapLogger:Ldagger/Lazy;

    .line 27
    iput-object p2, p0, Lcom/squareup/analytics/LoggingHttpProfiler;->analytics:Ldagger/Lazy;

    return-void
.end method

.method private static shouldSendRequestInfo(Lcom/squareup/http/HttpProfiler$CallInformation;)Z
    .locals 1

    .line 65
    sget-object v0, Lcom/squareup/analytics/LoggingHttpProfiler;->EXEMPTED_PATHS:Ljava/util/List;

    iget-object p0, p0, Lcom/squareup/http/HttpProfiler$CallInformation;->relativePath:Ljava/lang/String;

    invoke-interface {v0, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p0

    xor-int/lit8 p0, p0, 0x1

    return p0
.end method


# virtual methods
.method public bridge synthetic afterCall(Lcom/squareup/http/HttpProfiler$CallInformation;JILjava/lang/Object;)V
    .locals 0

    .line 19
    check-cast p5, Ljava/lang/Void;

    invoke-virtual/range {p0 .. p5}, Lcom/squareup/analytics/LoggingHttpProfiler;->afterCall(Lcom/squareup/http/HttpProfiler$CallInformation;JILjava/lang/Void;)V

    return-void
.end method

.method public afterCall(Lcom/squareup/http/HttpProfiler$CallInformation;JILjava/lang/Void;)V
    .locals 16

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    .line 37
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, v1, Lcom/squareup/http/HttpProfiler$CallInformation;->baseUrl:Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    iget-object v4, v1, Lcom/squareup/http/HttpProfiler$CallInformation;->relativePath:Ljava/lang/String;

    const/4 v5, 0x1

    aput-object v4, v3, v5

    .line 39
    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x2

    aput-object v4, v3, v5

    iget-wide v4, v1, Lcom/squareup/http/HttpProfiler$CallInformation;->requestLength:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    const/4 v5, 0x3

    aput-object v4, v3, v5

    invoke-static/range {p2 .. p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    const/4 v5, 0x4

    aput-object v4, v3, v5

    const-string v4, "[%s]%s=%d (%d bytes) %dms"

    .line 38
    invoke-static {v2, v4, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 40
    iget-object v3, v0, Lcom/squareup/analytics/LoggingHttpProfiler;->ohSnapLogger:Ldagger/Lazy;

    invoke-interface {v3}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/log/OhSnapLogger;

    sget-object v4, Lcom/squareup/log/OhSnapLogger$EventType;->SERVER_CALL:Lcom/squareup/log/OhSnapLogger$EventType;

    invoke-interface {v3, v4, v2}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 42
    invoke-static/range {p1 .. p1}, Lcom/squareup/analytics/LoggingHttpProfiler;->shouldSendRequestInfo(Lcom/squareup/http/HttpProfiler$CallInformation;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 44
    iget-object v2, v0, Lcom/squareup/analytics/LoggingHttpProfiler;->analytics:Ldagger/Lazy;

    invoke-interface {v2}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/analytics/Analytics;

    new-instance v15, Lcom/squareup/analytics/LoggingHttpProfiler$ServerRequest;

    iget-object v4, v1, Lcom/squareup/http/HttpProfiler$CallInformation;->method:Ljava/lang/String;

    iget-object v5, v1, Lcom/squareup/http/HttpProfiler$CallInformation;->relativePath:Ljava/lang/String;

    iget-object v6, v1, Lcom/squareup/http/HttpProfiler$CallInformation;->contentType:Ljava/lang/String;

    iget-object v7, v1, Lcom/squareup/http/HttpProfiler$CallInformation;->baseUrl:Ljava/lang/String;

    iget-wide v8, v1, Lcom/squareup/http/HttpProfiler$CallInformation;->requestLength:J

    iget-wide v10, v1, Lcom/squareup/http/HttpProfiler$CallInformation;->responseLength:J

    move-object v3, v15

    move/from16 v12, p4

    move-wide/from16 v13, p2

    invoke-direct/range {v3 .. v14}, Lcom/squareup/analytics/LoggingHttpProfiler$ServerRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJIJ)V

    .line 45
    invoke-interface {v2, v15}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    :cond_0
    return-void
.end method

.method public bridge synthetic afterError(Lcom/squareup/http/HttpProfiler$CallInformation;Ljava/lang/Exception;JLjava/lang/Object;)V
    .locals 0

    .line 19
    check-cast p5, Ljava/lang/Void;

    invoke-virtual/range {p0 .. p5}, Lcom/squareup/analytics/LoggingHttpProfiler;->afterError(Lcom/squareup/http/HttpProfiler$CallInformation;Ljava/lang/Exception;JLjava/lang/Void;)V

    return-void
.end method

.method public afterError(Lcom/squareup/http/HttpProfiler$CallInformation;Ljava/lang/Exception;JLjava/lang/Void;)V
    .locals 6

    .line 54
    new-instance p5, Lcom/squareup/analytics/FailedHttpRequestEvent;

    iget-object v1, p1, Lcom/squareup/http/HttpProfiler$CallInformation;->protocol:Ljava/lang/String;

    iget-object v2, p1, Lcom/squareup/http/HttpProfiler$CallInformation;->relativePath:Ljava/lang/String;

    move-object v0, p5

    move-object v3, p2

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/squareup/analytics/FailedHttpRequestEvent;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;J)V

    .line 56
    iget-object p2, p0, Lcom/squareup/analytics/LoggingHttpProfiler;->ohSnapLogger:Ldagger/Lazy;

    invoke-interface {p2}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/log/OhSnapLogger;

    sget-object p3, Lcom/squareup/log/OhSnapLogger$EventType;->SERVER_CALL:Lcom/squareup/log/OhSnapLogger$EventType;

    .line 57
    invoke-virtual {p5}, Lcom/squareup/analytics/FailedHttpRequestEvent;->asOhSnapMessage()Ljava/lang/String;

    move-result-object p4

    .line 56
    invoke-interface {p2, p3, p4}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 58
    invoke-static {p1}, Lcom/squareup/analytics/LoggingHttpProfiler;->shouldSendRequestInfo(Lcom/squareup/http/HttpProfiler$CallInformation;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 59
    iget-object p1, p0, Lcom/squareup/analytics/LoggingHttpProfiler;->analytics:Ldagger/Lazy;

    invoke-interface {p1}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/analytics/Analytics;

    invoke-interface {p1, p5}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    :cond_0
    return-void
.end method

.method public bridge synthetic beforeCall()Ljava/lang/Object;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lcom/squareup/analytics/LoggingHttpProfiler;->beforeCall()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public beforeCall()Ljava/lang/Void;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method
