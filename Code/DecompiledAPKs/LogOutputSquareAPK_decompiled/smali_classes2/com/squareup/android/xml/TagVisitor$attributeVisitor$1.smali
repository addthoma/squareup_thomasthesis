.class public final Lcom/squareup/android/xml/TagVisitor$attributeVisitor$1;
.super Ljava/lang/Object;
.source "XmlResourceParsing.kt"

# interfaces
.implements Lcom/squareup/android/xml/StyledAttributesVisitor;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/android/xml/TagVisitor;-><init>(Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000#\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u000e\n\u0002\u0008\n*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001R\u001a\u0010\u0002\u001a\u00020\u0003X\u0096\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0004\u0010\u0005\"\u0004\u0008\u0006\u0010\u0007R\u001c\u0010\u0008\u001a\u0004\u0018\u00010\tX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\n\u0010\u000b\"\u0004\u0008\u000c\u0010\rR\u001c\u0010\u000e\u001a\n \u0010*\u0004\u0018\u00010\u000f0\u000f8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0011\u0010\u0012R\u0014\u0010\u0013\u001a\u00020\u00038VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0014\u0010\u0005R\u0014\u0010\u0015\u001a\u00020\t8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0016\u0010\u000bR\u001c\u0010\u0017\u001a\n \u0010*\u0004\u0018\u00010\u000f0\u000f8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0018\u0010\u0012\u00a8\u0006\u0019"
    }
    d2 = {
        "com/squareup/android/xml/TagVisitor$attributeVisitor$1",
        "Lcom/squareup/android/xml/StyledAttributesVisitor;",
        "index",
        "",
        "getIndex",
        "()I",
        "setIndex",
        "(I)V",
        "maybeStyled",
        "Landroid/content/res/TypedArray;",
        "getMaybeStyled",
        "()Landroid/content/res/TypedArray;",
        "setMaybeStyled",
        "(Landroid/content/res/TypedArray;)V",
        "name",
        "",
        "kotlin.jvm.PlatformType",
        "getName",
        "()Ljava/lang/String;",
        "nameResource",
        "getNameResource",
        "styled",
        "getStyled",
        "value",
        "getValue",
        "android-xml_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private index:I

.field private maybeStyled:Landroid/content/res/TypedArray;

.field final synthetic this$0:Lcom/squareup/android/xml/TagVisitor;


# direct methods
.method constructor <init>(Lcom/squareup/android/xml/TagVisitor;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 100
    iput-object p1, p0, Lcom/squareup/android/xml/TagVisitor$attributeVisitor$1;->this$0:Lcom/squareup/android/xml/TagVisitor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getIndex()I
    .locals 1

    .line 101
    iget v0, p0, Lcom/squareup/android/xml/TagVisitor$attributeVisitor$1;->index:I

    return v0
.end method

.method public final getMaybeStyled()Landroid/content/res/TypedArray;
    .locals 1

    .line 106
    iget-object v0, p0, Lcom/squareup/android/xml/TagVisitor$attributeVisitor$1;->maybeStyled:Landroid/content/res/TypedArray;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .line 104
    iget-object v0, p0, Lcom/squareup/android/xml/TagVisitor$attributeVisitor$1;->this$0:Lcom/squareup/android/xml/TagVisitor;

    invoke-virtual {v0}, Lcom/squareup/android/xml/TagVisitor;->getParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/android/xml/TagVisitor$attributeVisitor$1;->getIndex()I

    move-result v1

    invoke-interface {v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNameResource()I
    .locals 2

    .line 103
    iget-object v0, p0, Lcom/squareup/android/xml/TagVisitor$attributeVisitor$1;->this$0:Lcom/squareup/android/xml/TagVisitor;

    invoke-virtual {v0}, Lcom/squareup/android/xml/TagVisitor;->getAttrs()Landroid/util/AttributeSet;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/android/xml/TagVisitor$attributeVisitor$1;->getIndex()I

    move-result v1

    invoke-interface {v0, v1}, Landroid/util/AttributeSet;->getAttributeNameResource(I)I

    move-result v0

    return v0
.end method

.method public getStyled()Landroid/content/res/TypedArray;
    .locals 1

    .line 102
    iget-object v0, p0, Lcom/squareup/android/xml/TagVisitor$attributeVisitor$1;->maybeStyled:Landroid/content/res/TypedArray;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 2

    .line 105
    iget-object v0, p0, Lcom/squareup/android/xml/TagVisitor$attributeVisitor$1;->this$0:Lcom/squareup/android/xml/TagVisitor;

    invoke-virtual {v0}, Lcom/squareup/android/xml/TagVisitor;->getParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/android/xml/TagVisitor$attributeVisitor$1;->getIndex()I

    move-result v1

    invoke-interface {v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setIndex(I)V
    .locals 0

    .line 101
    iput p1, p0, Lcom/squareup/android/xml/TagVisitor$attributeVisitor$1;->index:I

    return-void
.end method

.method public final setMaybeStyled(Landroid/content/res/TypedArray;)V
    .locals 0

    .line 106
    iput-object p1, p0, Lcom/squareup/android/xml/TagVisitor$attributeVisitor$1;->maybeStyled:Landroid/content/res/TypedArray;

    return-void
.end method
