.class public final Lcom/squareup/android/xml/XmlResourceParsingKt;
.super Ljava/lang/Object;
.source "XmlResourceParsing.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nXmlResourceParsing.kt\nKotlin\n*S Kotlin\n*F\n+ 1 XmlResourceParsing.kt\ncom/squareup/android/xml/XmlResourceParsingKt\n*L\n1#1,214:1\n38#1:215\n*E\n*S KotlinDebug\n*F\n+ 1 XmlResourceParsing.kt\ncom/squareup/android/xml/XmlResourceParsingKt\n*L\n24#1:215\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000J\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001aG\u0010\u0000\u001a\u0002H\u0001\"\u0004\u0008\u0000\u0010\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052!\u0010\u0006\u001a\u001d\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u0002H\u00010\u0007j\u0008\u0012\u0004\u0012\u0002H\u0001`\t\u00a2\u0006\u0002\u0008\nH\u0086\u0008\u00a2\u0006\u0002\u0010\u000b\u001a\n\u0010\u000c\u001a\u00020\r*\u00020\u0003\u001a\n\u0010\u000e\u001a\u00020\r*\u00020\u0003\u001a\u0014\u0010\u000f\u001a\u00020\r*\u00020\u00032\u0006\u0010\u0010\u001a\u00020\u0011H\u0002\u001aC\u0010\u0000\u001a\u0002H\u0001\"\u0004\u0008\u0000\u0010\u0001*\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00112!\u0010\u0006\u001a\u001d\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u0002H\u00010\u0007j\u0008\u0012\u0004\u0012\u0002H\u0001`\t\u00a2\u0006\u0002\u0008\nH\u0086\u0008\u00a2\u0006\u0002\u0010\u0014*,\u0010\u0015\"\u0013\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020\u00170\u0007\u00a2\u0006\u0002\u0008\n2\u0013\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020\u00170\u0007\u00a2\u0006\u0002\u0008\n*,\u0010\u0018\"\u0013\u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u00170\u0007\u00a2\u0006\u0002\u0008\n2\u0013\u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u00170\u0007\u00a2\u0006\u0002\u0008\n*2\u0010\u001a\u001a\u0004\u0008\u0000\u0010\u0001\"\u0013\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u0002H\u00010\u0007\u00a2\u0006\u0002\u0008\n2\u0013\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u0002H\u00010\u0007\u00a2\u0006\u0002\u0008\n\u00a8\u0006\u001b"
    }
    d2 = {
        "visitXml",
        "R",
        "parser",
        "Lorg/xmlpull/v1/XmlPullParser;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "block",
        "Lkotlin/Function1;",
        "Lcom/squareup/android/xml/TagVisitor;",
        "Lcom/squareup/android/xml/TagVisitorBlock;",
        "Lkotlin/ExtensionFunctionType;",
        "(Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;",
        "nextChildTag",
        "",
        "nextSiblingTag",
        "nextTagWithDepth",
        "wantedDepth",
        "",
        "Landroid/content/res/Resources;",
        "resourceId",
        "(Landroid/content/res/Resources;ILkotlin/jvm/functions/Function1;)Ljava/lang/Object;",
        "AttributesVisitorBlock",
        "Lcom/squareup/android/xml/AttributesVisitor;",
        "",
        "StyledAttributesVisitorBlock",
        "Lcom/squareup/android/xml/StyledAttributesVisitor;",
        "TagVisitorBlock",
        "android-xml_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final nextChildTag(Lorg/xmlpull/v1/XmlPullParser;)Z
    .locals 1

    const-string v0, "$this$nextChildTag"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {p0, v0}, Lcom/squareup/android/xml/XmlResourceParsingKt;->nextTagWithDepth(Lorg/xmlpull/v1/XmlPullParser;I)Z

    move-result p0

    return p0
.end method

.method public static final nextSiblingTag(Lorg/xmlpull/v1/XmlPullParser;)Z
    .locals 1

    const-string v0, "$this$nextSiblingTag"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v0

    invoke-static {p0, v0}, Lcom/squareup/android/xml/XmlResourceParsingKt;->nextTagWithDepth(Lorg/xmlpull/v1/XmlPullParser;I)Z

    move-result p0

    return p0
.end method

.method private static final nextTagWithDepth(Lorg/xmlpull/v1/XmlPullParser;I)Z
    .locals 2

    .line 53
    :cond_0
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 54
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v0

    if-ne v0, p1, :cond_1

    const/4 p0, 0x1

    return p0

    .line 57
    :cond_1
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v0

    if-ge v0, p1, :cond_0

    const/4 p0, 0x0

    return p0
.end method

.method public static final visitXml(Landroid/content/res/Resources;ILkotlin/jvm/functions/Function1;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/content/res/Resources;",
            "I",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/android/xml/TagVisitor;",
            "+TR;>;)TR;"
        }
    .end annotation

    const-string v0, "$this$visitXml"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "block"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object p0

    const-string p1, "getXml(resourceId)"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x1

    .line 215
    :try_start_0
    new-instance v0, Lcom/squareup/android/xml/TagVisitor;

    move-object v1, p0

    check-cast v1, Lorg/xmlpull/v1/XmlPullParser;

    move-object v2, p0

    check-cast v2, Landroid/util/AttributeSet;

    invoke-direct {v0, v1, v2}, Lcom/squareup/android/xml/TagVisitor;-><init>(Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)V

    invoke-interface {p2, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {p1}, Lkotlin/jvm/internal/InlineMarker;->finallyStart(I)V

    .line 26
    invoke-interface {p0}, Landroid/content/res/XmlResourceParser;->close()V

    invoke-static {p1}, Lkotlin/jvm/internal/InlineMarker;->finallyEnd(I)V

    return-object p2

    :catchall_0
    move-exception p2

    .line 27
    invoke-static {p1}, Lkotlin/jvm/internal/InlineMarker;->finallyStart(I)V

    .line 26
    invoke-interface {p0}, Landroid/content/res/XmlResourceParser;->close()V

    invoke-static {p1}, Lkotlin/jvm/internal/InlineMarker;->finallyEnd(I)V

    throw p2
.end method

.method public static final visitXml(Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/xmlpull/v1/XmlPullParser;",
            "Landroid/util/AttributeSet;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/android/xml/TagVisitor;",
            "+TR;>;)TR;"
        }
    .end annotation

    const-string v0, "parser"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attrs"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "block"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    new-instance v0, Lcom/squareup/android/xml/TagVisitor;

    invoke-direct {v0, p0, p1}, Lcom/squareup/android/xml/TagVisitor;-><init>(Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)V

    invoke-interface {p2, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method
