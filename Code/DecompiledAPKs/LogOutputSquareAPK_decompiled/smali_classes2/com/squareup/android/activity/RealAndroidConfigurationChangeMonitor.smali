.class public final Lcom/squareup/android/activity/RealAndroidConfigurationChangeMonitor;
.super Ljava/lang/Object;
.source "RealAndroidConfigurationChangeMonitor.kt"

# interfaces
.implements Lcom/squareup/util/AndroidConfigurationChangeMonitor;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/dagger/AppScope;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u0007\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u000b\u001a\u00020\u0005H\u0016R\u001c\u0010\u0003\u001a\u0010\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00050\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0008X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/android/activity/RealAndroidConfigurationChangeMonitor;",
        "Lcom/squareup/util/AndroidConfigurationChangeMonitor;",
        "()V",
        "_onConfigurationMaybeChanged",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "",
        "kotlin.jvm.PlatformType",
        "onConfigurationMaybeChanged",
        "Lio/reactivex/Observable;",
        "getOnConfigurationMaybeChanged",
        "()Lio/reactivex/Observable;",
        "fireOnConfigurationMaybeChanged",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final _onConfigurationMaybeChanged:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onConfigurationMaybeChanged:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    const-string v1, "PublishRelay.create<Unit>()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/android/activity/RealAndroidConfigurationChangeMonitor;->_onConfigurationMaybeChanged:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 16
    iget-object v0, p0, Lcom/squareup/android/activity/RealAndroidConfigurationChangeMonitor;->_onConfigurationMaybeChanged:Lcom/jakewharton/rxrelay2/PublishRelay;

    check-cast v0, Lio/reactivex/Observable;

    iput-object v0, p0, Lcom/squareup/android/activity/RealAndroidConfigurationChangeMonitor;->onConfigurationMaybeChanged:Lio/reactivex/Observable;

    return-void
.end method


# virtual methods
.method public fireOnConfigurationMaybeChanged()V
    .locals 2

    .line 19
    iget-object v0, p0, Lcom/squareup/android/activity/RealAndroidConfigurationChangeMonitor;->_onConfigurationMaybeChanged:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public getOnConfigurationMaybeChanged()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 16
    iget-object v0, p0, Lcom/squareup/android/activity/RealAndroidConfigurationChangeMonitor;->onConfigurationMaybeChanged:Lio/reactivex/Observable;

    return-object v0
.end method
