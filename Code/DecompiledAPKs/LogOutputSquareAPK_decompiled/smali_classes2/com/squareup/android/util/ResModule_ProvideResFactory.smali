.class public final Lcom/squareup/android/util/ResModule_ProvideResFactory;
.super Ljava/lang/Object;
.source "ResModule_ProvideResFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/util/Res;",
        ">;"
    }
.end annotation


# instance fields
.field private final resourcesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/android/util/ResModule_ProvideResFactory;->resourcesProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/android/util/ResModule_ProvideResFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;)",
            "Lcom/squareup/android/util/ResModule_ProvideResFactory;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/android/util/ResModule_ProvideResFactory;

    invoke-direct {v0, p0}, Lcom/squareup/android/util/ResModule_ProvideResFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideRes(Landroid/content/res/Resources;)Lcom/squareup/util/Res;
    .locals 1

    .line 35
    sget-object v0, Lcom/squareup/android/util/ResModule;->INSTANCE:Lcom/squareup/android/util/ResModule;

    invoke-virtual {v0, p0}, Lcom/squareup/android/util/ResModule;->provideRes(Landroid/content/res/Resources;)Lcom/squareup/util/Res;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/util/Res;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/util/Res;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/android/util/ResModule_ProvideResFactory;->resourcesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    invoke-static {v0}, Lcom/squareup/android/util/ResModule_ProvideResFactory;->provideRes(Landroid/content/res/Resources;)Lcom/squareup/util/Res;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/android/util/ResModule_ProvideResFactory;->get()Lcom/squareup/util/Res;

    move-result-object v0

    return-object v0
.end method
