.class public final Lcom/squareup/android/util/AndroidUtilModule_ProvideAndroidIdFactory;
.super Ljava/lang/Object;
.source "AndroidUtilModule_ProvideAndroidIdFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private final contextProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/android/util/AndroidUtilModule_ProvideAndroidIdFactory;->contextProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/android/util/AndroidUtilModule_ProvideAndroidIdFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;)",
            "Lcom/squareup/android/util/AndroidUtilModule_ProvideAndroidIdFactory;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/android/util/AndroidUtilModule_ProvideAndroidIdFactory;

    invoke-direct {v0, p0}, Lcom/squareup/android/util/AndroidUtilModule_ProvideAndroidIdFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideAndroidId(Landroid/app/Application;)Ljava/lang/String;
    .locals 1

    .line 37
    sget-object v0, Lcom/squareup/android/util/AndroidUtilModule;->INSTANCE:Lcom/squareup/android/util/AndroidUtilModule;

    invoke-virtual {v0, p0}, Lcom/squareup/android/util/AndroidUtilModule;->provideAndroidId(Landroid/app/Application;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/android/util/AndroidUtilModule_ProvideAndroidIdFactory;->get()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public get()Ljava/lang/String;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/android/util/AndroidUtilModule_ProvideAndroidIdFactory;->contextProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    invoke-static {v0}, Lcom/squareup/android/util/AndroidUtilModule_ProvideAndroidIdFactory;->provideAndroidId(Landroid/app/Application;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
