.class final Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow$render$3;
.super Lkotlin/jvm/internal/Lambda;
.source "RealInstallmentsWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;->render(Lcom/squareup/checkoutflow/installments/InstallmentsInput;Lcom/squareup/checkoutflow/installments/InstallmentsState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/checkoutflow/installments/InstallmentsScreenData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lkotlin/Unit;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/checkoutflow/installments/InstallmentsState;",
        "+",
        "Lcom/squareup/checkoutflow/installments/InstallmentsOutput$ManualCardEntry;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0004\u0008\u0006\u0010\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/checkoutflow/installments/InstallmentsState;",
        "Lcom/squareup/checkoutflow/installments/InstallmentsOutput$ManualCardEntry;",
        "it",
        "",
        "invoke",
        "(Lkotlin/Unit;)Lcom/squareup/workflow/WorkflowAction;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $input:Lcom/squareup/checkoutflow/installments/InstallmentsInput;

.field final synthetic this$0:Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;Lcom/squareup/checkoutflow/installments/InstallmentsInput;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow$render$3;->this$0:Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;

    iput-object p2, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow$render$3;->$input:Lcom/squareup/checkoutflow/installments/InstallmentsInput;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lkotlin/Unit;)Lcom/squareup/workflow/WorkflowAction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Unit;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/checkoutflow/installments/InstallmentsState;",
            "Lcom/squareup/checkoutflow/installments/InstallmentsOutput$ManualCardEntry;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    iget-object p1, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow$render$3;->this$0:Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;

    invoke-static {p1}, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;->access$getAnalytics$p(Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;)Lcom/squareup/analytics/Analytics;

    move-result-object p1

    sget-object v0, Lcom/squareup/analytics/RegisterTapName;->INSTALLMENTS_ENTER_CARD_NUMBER:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 107
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 108
    new-instance v0, Lcom/squareup/checkoutflow/installments/InstallmentsOutput$ManualCardEntry;

    .line 109
    iget-object v1, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow$render$3;->this$0:Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;

    invoke-static {v1}, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;->access$getManualCardEntryScreenDataHelper$p(Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;)Lcom/squareup/checkoutflow/installments/InstallmentsCardEntryScreenDataHelper;

    move-result-object v1

    .line 110
    iget-object v2, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow$render$3;->$input:Lcom/squareup/checkoutflow/installments/InstallmentsInput;

    invoke-virtual {v2}, Lcom/squareup/checkoutflow/installments/InstallmentsInput;->getMoney()Lcom/squareup/protos/common/Money;

    move-result-object v2

    .line 109
    invoke-interface {v1, v2}, Lcom/squareup/checkoutflow/installments/InstallmentsCardEntryScreenDataHelper;->getScreenData(Lcom/squareup/protos/common/Money;)Lcom/squareup/checkoutflow/selecttender/tenderoption/ManualCardEntryScreenData;

    move-result-object v1

    .line 108
    invoke-direct {v0, v1}, Lcom/squareup/checkoutflow/installments/InstallmentsOutput$ManualCardEntry;-><init>(Lcom/squareup/checkoutflow/selecttender/tenderoption/ManualCardEntryScreenData;)V

    .line 107
    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 57
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow$render$3;->invoke(Lkotlin/Unit;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
