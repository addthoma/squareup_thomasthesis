.class public final Lcom/squareup/checkoutflow/installments/InstallmentsViewFactory_Factory;
.super Ljava/lang/Object;
.source "InstallmentsViewFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/checkoutflow/installments/InstallmentsViewFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsCoordinator$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/installments/linkOptions/InstallmentsLinkOptionsCoordinator$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/installments/SmsSent/InstallmentsSmsSentCoordinator$Factory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/installments/linkOptions/InstallmentsLinkOptionsCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/installments/SmsSent/InstallmentsSmsSentCoordinator$Factory;",
            ">;)V"
        }
    .end annotation

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/squareup/checkoutflow/installments/InstallmentsViewFactory_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 34
    iput-object p2, p0, Lcom/squareup/checkoutflow/installments/InstallmentsViewFactory_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 35
    iput-object p3, p0, Lcom/squareup/checkoutflow/installments/InstallmentsViewFactory_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 36
    iput-object p4, p0, Lcom/squareup/checkoutflow/installments/InstallmentsViewFactory_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 37
    iput-object p5, p0, Lcom/squareup/checkoutflow/installments/InstallmentsViewFactory_Factory;->arg4Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/checkoutflow/installments/InstallmentsViewFactory_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/installments/linkOptions/InstallmentsLinkOptionsCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/installments/SmsSent/InstallmentsSmsSentCoordinator$Factory;",
            ">;)",
            "Lcom/squareup/checkoutflow/installments/InstallmentsViewFactory_Factory;"
        }
    .end annotation

    .line 51
    new-instance v6, Lcom/squareup/checkoutflow/installments/InstallmentsViewFactory_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/checkoutflow/installments/InstallmentsViewFactory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsCoordinator$Factory;Lcom/squareup/checkoutflow/installments/linkOptions/InstallmentsLinkOptionsCoordinator$Factory;Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator$Factory;Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator$Factory;Lcom/squareup/checkoutflow/installments/SmsSent/InstallmentsSmsSentCoordinator$Factory;)Lcom/squareup/checkoutflow/installments/InstallmentsViewFactory;
    .locals 7

    .line 57
    new-instance v6, Lcom/squareup/checkoutflow/installments/InstallmentsViewFactory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/checkoutflow/installments/InstallmentsViewFactory;-><init>(Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsCoordinator$Factory;Lcom/squareup/checkoutflow/installments/linkOptions/InstallmentsLinkOptionsCoordinator$Factory;Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator$Factory;Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator$Factory;Lcom/squareup/checkoutflow/installments/SmsSent/InstallmentsSmsSentCoordinator$Factory;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/checkoutflow/installments/InstallmentsViewFactory;
    .locals 5

    .line 42
    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/InstallmentsViewFactory_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsCoordinator$Factory;

    iget-object v1, p0, Lcom/squareup/checkoutflow/installments/InstallmentsViewFactory_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkoutflow/installments/linkOptions/InstallmentsLinkOptionsCoordinator$Factory;

    iget-object v2, p0, Lcom/squareup/checkoutflow/installments/InstallmentsViewFactory_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator$Factory;

    iget-object v3, p0, Lcom/squareup/checkoutflow/installments/InstallmentsViewFactory_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator$Factory;

    iget-object v4, p0, Lcom/squareup/checkoutflow/installments/InstallmentsViewFactory_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/checkoutflow/installments/SmsSent/InstallmentsSmsSentCoordinator$Factory;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/checkoutflow/installments/InstallmentsViewFactory_Factory;->newInstance(Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsCoordinator$Factory;Lcom/squareup/checkoutflow/installments/linkOptions/InstallmentsLinkOptionsCoordinator$Factory;Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeCoordinator$Factory;Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator$Factory;Lcom/squareup/checkoutflow/installments/SmsSent/InstallmentsSmsSentCoordinator$Factory;)Lcom/squareup/checkoutflow/installments/InstallmentsViewFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/installments/InstallmentsViewFactory_Factory;->get()Lcom/squareup/checkoutflow/installments/InstallmentsViewFactory;

    move-result-object v0

    return-object v0
.end method
