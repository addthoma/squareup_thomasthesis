.class public Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;
.super Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;
.source "PayGiftCardPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter$GiftCardTenderEvent;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter<",
        "Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;",
        ">;"
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final bus:Lcom/squareup/badbus/BadBus;

.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final softInputPresenter:Lcom/squareup/ui/SoftInputPresenter;

.field private waitForStoredGiftCardAuth:Z


# direct methods
.method constructor <init>(Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Res;Lcom/squareup/ui/payment/SwipeHandler;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/giftcard/GiftCards;Lcom/squareup/text/Formatter;Lcom/squareup/badbus/BadBus;Lcom/squareup/ui/SoftInputPresenter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/settings/server/Features;Lcom/squareup/analytics/Analytics;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/tenderpayment/TenderScopeRunner;Lcom/squareup/crm/CustomerManagementSettings;Ljavax/inject/Provider;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/ui/payment/SwipeHandler;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/giftcard/GiftCards;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/badbus/BadBus;",
            "Lcom/squareup/ui/SoftInputPresenter;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/payment/TenderInEdit;",
            "Lcom/squareup/tenderpayment/TenderScopeRunner;",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/card/ThirdPartyGiftCardPanValidationStrategy;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v12, p0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p11

    move-object/from16 v8, p12

    move-object/from16 v9, p15

    move-object/from16 v10, p14

    move-object/from16 v11, p17

    .line 70
    invoke-direct/range {v0 .. v11}, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;-><init>(Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Res;Lcom/squareup/ui/payment/SwipeHandler;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/giftcard/GiftCards;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/settings/server/Features;Lcom/squareup/tenderpayment/TenderScopeRunner;Lcom/squareup/payment/TenderInEdit;Ljavax/inject/Provider;)V

    const/4 v0, 0x0

    .line 58
    iput-boolean v0, v12, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->waitForStoredGiftCardAuth:Z

    move-object/from16 v0, p7

    .line 73
    iput-object v0, v12, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    move-object/from16 v0, p8

    .line 74
    iput-object v0, v12, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->bus:Lcom/squareup/badbus/BadBus;

    move-object/from16 v0, p9

    .line 75
    iput-object v0, v12, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->softInputPresenter:Lcom/squareup/ui/SoftInputPresenter;

    move-object/from16 v0, p10

    .line 76
    iput-object v0, v12, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    move-object/from16 v0, p13

    .line 77
    iput-object v0, v12, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    move-object/from16 v0, p16

    .line 78
    iput-object v0, v12, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

    return-void
.end method

.method private getGiftCardInCart()Lcom/squareup/Card;
    .locals 2

    .line 209
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getCard()Lcom/squareup/Card;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 210
    instance-of v1, v0, Lcom/squareup/ui/library/giftcard/GiftCardWithClientId;

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public static synthetic lambda$LhLdSib_df2eA4mFKF5lmw9bwWs(Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;Lcom/squareup/payment/OrderEntryEvents$CartChanged;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->onPaymentChanged(Lcom/squareup/payment/OrderEntryEvents$CartChanged;)V

    return-void
.end method

.method private onPaymentChanged(Lcom/squareup/payment/OrderEntryEvents$CartChanged;)V
    .locals 1

    .line 192
    iget-boolean v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->waitForStoredGiftCardAuth:Z

    if-eqz v0, :cond_1

    iget-boolean p1, p1, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->paymentChanged:Z

    if-eqz p1, :cond_1

    .line 193
    invoke-direct {p0}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->getGiftCardInCart()Lcom/squareup/Card;

    move-result-object p1

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->hasView()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 194
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;->clearCard()V

    :cond_0
    const/4 p1, 0x0

    .line 196
    iput-boolean p1, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->waitForStoredGiftCardAuth:Z

    :cond_1
    return-void
.end method


# virtual methods
.method clearStoredCardInCart()V
    .locals 2

    .line 201
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->transaction:Lcom/squareup/payment/Transaction;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Transaction;->setCard(Lcom/squareup/Card;)V

    return-void
.end method

.method public dropView(Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;)V
    .locals 1

    .line 140
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    if-ne p1, v0, :cond_0

    const/4 v0, 0x0

    .line 141
    iput-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 143
    :cond_0
    invoke-super {p0, p1}, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->dropView(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic dropView(Ljava/lang/Object;)V
    .locals 0

    .line 49
    check-cast p1, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->dropView(Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;)V

    return-void
.end method

.method getStrategy()Lcom/squareup/register/widgets/card/PanValidationStrategy;
    .locals 1

    .line 151
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->giftCards:Lcom/squareup/giftcard/GiftCards;

    invoke-virtual {v0}, Lcom/squareup/giftcard/GiftCards;->isThirdPartyGiftCardFeatureEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->thirdPartyGiftCardStrategyProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/register/widgets/card/PanValidationStrategy;

    return-object v0

    .line 154
    :cond_0
    new-instance v0, Lcom/squareup/register/widgets/card/CardPanValidationStrategy;

    invoke-direct {v0}, Lcom/squareup/register/widgets/card/CardPanValidationStrategy;-><init>()V

    return-object v0
.end method

.method public bridge synthetic giftCardOnFileSelected()V
    .locals 0

    .line 49
    invoke-super {p0}, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->giftCardOnFileSelected()V

    return-void
.end method

.method isGiftCardPayment()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public isShowPlasticGiftCardsFeatureEnabled()Z
    .locals 2

    .line 220
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->GIFT_CARDS_SHOW_PLASTIC_GIFT_CARDS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic isThirdPartyGiftCardFeatureEnabled()Z
    .locals 1

    .line 49
    invoke-super {p0}, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->isThirdPartyGiftCardFeatureEnabled()Z

    move-result v0

    return v0
.end method

.method public synthetic lambda$onLoad$0$PayGiftCardPresenter()V
    .locals 1

    .line 98
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;

    .line 99
    invoke-static {v0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 100
    invoke-virtual {v0}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;->getCard()Lcom/squareup/Card;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->onChargeCard(Lcom/squareup/Card;)V

    return-void
.end method

.method public onCardChanged(Lcom/squareup/register/widgets/card/PartialCard;)V
    .locals 1

    .line 159
    invoke-virtual {p1}, Lcom/squareup/register/widgets/card/PartialCard;->isBlank()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 160
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->promptForPayment()V

    goto :goto_0

    .line 162
    :cond_0
    iget-object p1, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    sget-object v0, Lcom/squareup/comms/protos/common/TenderType;->GIFT_CARD:Lcom/squareup/comms/protos/common/TenderType;

    invoke-interface {p1, v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->configuringTender(Lcom/squareup/comms/protos/common/TenderType;)Z

    :goto_0
    return-void
.end method

.method public bridge synthetic onCardInvalid(Lcom/squareup/Card$PanWarning;)V
    .locals 0

    .line 49
    invoke-super {p0, p1}, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->onCardInvalid(Lcom/squareup/Card$PanWarning;)V

    return-void
.end method

.method public bridge synthetic onCardValid(Lcom/squareup/Card;)V
    .locals 0

    .line 49
    invoke-super {p0, p1}, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->onCardValid(Lcom/squareup/Card;)V

    return-void
.end method

.method public onChargeCard(Lcom/squareup/Card;)V
    .locals 2

    .line 179
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter$GiftCardTenderEvent;

    invoke-direct {v1, p0}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter$GiftCardTenderEvent;-><init>(Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    if-nez p1, :cond_0

    .line 181
    invoke-direct {p0}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->getGiftCardInCart()Lcom/squareup/Card;

    move-result-object p1

    const/4 v0, 0x1

    .line 183
    iput-boolean v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->waitForStoredGiftCardAuth:Z

    .line 185
    :cond_0
    invoke-super {p0, p1}, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->onChargeCard(Lcom/squareup/Card;)V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 82
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->bus:Lcom/squareup/badbus/BadBus;

    const-class v1, Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    invoke-virtual {v0, v1}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/checkoutflow/manualcardentry/-$$Lambda$PayGiftCardPresenter$LhLdSib_df2eA4mFKF5lmw9bwWs;

    invoke-direct {v1, p0}, Lcom/squareup/checkoutflow/manualcardentry/-$$Lambda$PayGiftCardPresenter$LhLdSib_df2eA4mFKF5lmw9bwWs;-><init>(Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 6

    .line 86
    invoke-super {p0, p1}, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 88
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 90
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;

    .line 92
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    iget-object v1, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->tenderScopeRunner:Lcom/squareup/tenderpayment/TenderScopeRunner;

    iget-object v2, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/checkoutflow/manualcardentry/impl/R$string;->pay_gift_card_title:I

    .line 93
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->tenderScopeRunner:Lcom/squareup/tenderpayment/TenderScopeRunner;

    .line 94
    invoke-interface {v3}, Lcom/squareup/tenderpayment/TenderScopeRunner;->getFormattedTotal()Ljava/lang/CharSequence;

    move-result-object v3

    const-string v4, "amount"

    invoke-virtual {v2, v4, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v2

    const/4 v3, 0x0

    .line 92
    invoke-interface {v1, v2, v3}, Lcom/squareup/tenderpayment/TenderScopeRunner;->buildTenderActionBarConfig(Ljava/lang/CharSequence;Z)Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    .line 95
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/checkoutflow/manualcardentry/impl/R$string;->pay_card_charge_button:I

    .line 96
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    new-instance v2, Lcom/squareup/checkoutflow/manualcardentry/-$$Lambda$PayGiftCardPresenter$Cko4zL5eIrVfdjssZYSzQoAnMlQ;

    invoke-direct {v2, p0}, Lcom/squareup/checkoutflow/manualcardentry/-$$Lambda$PayGiftCardPresenter$Cko4zL5eIrVfdjssZYSzQoAnMlQ;-><init>(Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;)V

    .line 97
    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/marin/R$drawable;->marin_white_border_bottom_light_gray_1px:I

    .line 102
    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setBackground(I)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 103
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    .line 92
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 105
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;->hasCard()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->setChargeButtonEnabled(Z)V

    .line 107
    new-instance v0, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/squareup/checkoutflow/manualcardentry/impl/R$string;->pay_gift_card_cnp_hint2:I

    const-string v2, "learn_more"

    .line 108
    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/common/strings/R$string;->gift_card_hint_url:I

    .line 109
    invoke-virtual {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/common/strings/R$string;->learn_more_lowercase_more:I

    .line 110
    invoke-virtual {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    .line 111
    invoke-virtual {v0}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object v0

    .line 107
    invoke-virtual {p1, v0}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;->setHelperText(Ljava/lang/CharSequence;)V

    .line 113
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getGiftCardSettings()Lcom/squareup/settings/server/GiftCardSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/GiftCardSettings;->getGiftCardTransactionMinimum()J

    move-result-wide v0

    .line 114
    iget-object v2, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v2}, Lcom/squareup/payment/Transaction;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v5, v2, v0

    if-gez v5, :cond_0

    .line 115
    iget-object v2, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/checkoutflow/manualcardentry/impl/R$string;->payment_type_below_minimum_gift_card:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v5, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    .line 116
    invoke-static {v0, v1, v5}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-interface {v3, v0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v2, v4, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 117
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 118
    invoke-virtual {p1, v0}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;->setEditorDisabled(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 119
    :cond_0
    invoke-direct {p0}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->getGiftCardInCart()Lcom/squareup/Card;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 120
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->res:Lcom/squareup/util/Res;

    sget-object v1, Lcom/squareup/Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/Card$Brand;

    .line 121
    invoke-direct {p0}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->getGiftCardInCart()Lcom/squareup/Card;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/Card;->getUnmaskedDigits()Ljava/lang/String;

    move-result-object v2

    .line 120
    invoke-static {v0, v1, v2}, Lcom/squareup/text/Cards;->formattedBrandAndUnmaskedDigits(Lcom/squareup/util/Res;Lcom/squareup/Card$Brand;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 122
    invoke-virtual {p1, v0}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;->showSwipedCard(Ljava/lang/CharSequence;)V

    const/4 v0, 0x1

    .line 123
    invoke-virtual {p1, v0}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;->updateChargeButton(Z)V

    .line 127
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasCustomer()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->paymentIsBelowMinimum()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

    .line 128
    invoke-interface {v0}, Lcom/squareup/crm/CustomerManagementSettings;->isCardOnFileEnabled()Z

    move-result v0

    if-nez v0, :cond_3

    .line 129
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;->hideCardOnFileRow()V

    .line 132
    :cond_3
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->x2ClearTender()V

    .line 134
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;->isCardBlank()Z

    move-result p1

    if-eqz p1, :cond_4

    .line 135
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->promptForPayment()V

    :cond_4
    return-void
.end method

.method public bridge synthetic onPanValid(Lcom/squareup/Card;Z)Z
    .locals 0

    .line 49
    invoke-super {p0, p1, p2}, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->onPanValid(Lcom/squareup/Card;Z)Z

    move-result p1

    return p1
.end method

.method onWindowFocusChanged(Z)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    .line 168
    :cond_0
    iget-object p1, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->asAuthPayment()Lcom/squareup/payment/RequiresAuthorization;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 169
    invoke-interface {p1}, Lcom/squareup/payment/RequiresAuthorization;->hasRequestedAuthorization()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 173
    iget-object p1, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->softInputPresenter:Lcom/squareup/ui/SoftInputPresenter;

    sget-object v0, Lcom/squareup/workflow/SoftInputMode;->HIDDEN:Lcom/squareup/workflow/SoftInputMode;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/SoftInputPresenter;->setSoftInputMode(Lcom/squareup/workflow/SoftInputMode;)V

    :cond_1
    return-void
.end method

.method public bridge synthetic promptForPayment()V
    .locals 0

    .line 49
    invoke-super {p0}, Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;->promptForPayment()V

    return-void
.end method

.method setChargeButtonEnabled(Z)V
    .locals 1

    .line 205
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    return-void
.end method
