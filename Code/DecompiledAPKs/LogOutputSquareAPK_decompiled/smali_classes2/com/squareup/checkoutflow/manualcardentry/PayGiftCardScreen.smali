.class public final Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreen;
.super Lcom/squareup/ui/tender/InTenderScope;
.source "PayGiftCardScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/HasSoftInputModeForPhone;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreen$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 41
    sget-object v0, Lcom/squareup/checkoutflow/manualcardentry/-$$Lambda$PayGiftCardScreen$h7cbZgeLT80G_ZtOJ5xaBQDnygE;->INSTANCE:Lcom/squareup/checkoutflow/manualcardentry/-$$Lambda$PayGiftCardScreen$h7cbZgeLT80G_ZtOJ5xaBQDnygE;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/tender/TenderScopeTreeKey;)V
    .locals 0

    .line 24
    invoke-direct {p0, p1}, Lcom/squareup/ui/tender/InTenderScope;-><init>(Lcom/squareup/ui/tender/TenderScopeTreeKey;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreen;
    .locals 1

    .line 42
    const-class v0, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/main/RegisterTreeKey;

    .line 43
    check-cast p0, Lcom/squareup/ui/tender/TenderScopeTreeKey;

    .line 44
    new-instance v0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreen;

    invoke-direct {v0, p0}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreen;-><init>(Lcom/squareup/ui/tender/TenderScopeTreeKey;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 38
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreen;->getParentKey()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getSoftInputMode()Lcom/squareup/workflow/SoftInputMode;
    .locals 1

    .line 28
    sget-object v0, Lcom/squareup/workflow/SoftInputMode;->ALWAYS_SHOW_RESIZE:Lcom/squareup/workflow/SoftInputMode;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 48
    sget v0, Lcom/squareup/checkoutflow/manualcardentry/impl/R$layout;->pay_gift_card_screen_view:I

    return v0
.end method
