.class public abstract Lcom/squareup/checkoutflow/CheckoutflowModule;
.super Ljava/lang/Object;
.source "CheckoutflowModule.kt"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/checkoutflow/selecttender/SelectTenderModule;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\'\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H!J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\'J\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH\'J\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\t\u001a\u00020\u0011H\'\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/CheckoutflowModule;",
        "",
        "()V",
        "bindBlockedByBuyerFacingDisplayViewFactory",
        "Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayViewFactory;",
        "viewFactory",
        "Lcom/squareup/checkoutflow/RealBlockedByBuyerFacingDisplayViewFactory;",
        "provideCheckoutflowConfigFactory",
        "Lcom/squareup/checkoutflow/CheckoutflowConfigFactory;",
        "sink",
        "Lcom/squareup/checkoutflow/DefaultCheckoutflowConfigFactory;",
        "provideCheckoutflowWorkflowStarter",
        "Lcom/squareup/checkoutflow/CheckoutWorkflow;",
        "starter",
        "Lcom/squareup/checkoutflow/RealCheckoutWorkflow;",
        "providePaymentProcessingEventSink",
        "Lcom/squareup/checkoutflow/PaymentProcessingEventSink;",
        "Lcom/squareup/checkoutflow/RealPaymentProcessingEventSink;",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract bindBlockedByBuyerFacingDisplayViewFactory(Lcom/squareup/checkoutflow/RealBlockedByBuyerFacingDisplayViewFactory;)Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayViewFactory;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract provideCheckoutflowConfigFactory(Lcom/squareup/checkoutflow/DefaultCheckoutflowConfigFactory;)Lcom/squareup/checkoutflow/CheckoutflowConfigFactory;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract provideCheckoutflowWorkflowStarter(Lcom/squareup/checkoutflow/RealCheckoutWorkflow;)Lcom/squareup/checkoutflow/CheckoutWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract providePaymentProcessingEventSink(Lcom/squareup/checkoutflow/RealPaymentProcessingEventSink;)Lcom/squareup/checkoutflow/PaymentProcessingEventSink;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
