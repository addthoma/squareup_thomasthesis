.class public final Lcom/squareup/checkoutflow/emoney/NightModeInflater;
.super Ljava/lang/Object;
.source "NightModeInflater.kt"

# interfaces
.implements Lcom/squareup/workflow/InflaterDelegate;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\"\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u00082\u0006\u0010\t\u001a\u00020\nH\u0016\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/emoney/NightModeInflater;",
        "Lcom/squareup/workflow/InflaterDelegate;",
        "()V",
        "inflate",
        "Landroid/view/View;",
        "contextForNewView",
        "Landroid/content/Context;",
        "container",
        "Landroid/view/ViewGroup;",
        "layoutId",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/checkoutflow/emoney/NightModeInflater;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 10
    new-instance v0, Lcom/squareup/checkoutflow/emoney/NightModeInflater;

    invoke-direct {v0}, Lcom/squareup/checkoutflow/emoney/NightModeInflater;-><init>()V

    sput-object v0, Lcom/squareup/checkoutflow/emoney/NightModeInflater;->INSTANCE:Lcom/squareup/checkoutflow/emoney/NightModeInflater;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public inflate(Landroid/content/Context;Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 1

    const-string v0, "contextForNewView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    sget-object v0, Lcom/squareup/workflow/InflaterDelegate$Real;->INSTANCE:Lcom/squareup/workflow/InflaterDelegate$Real;

    invoke-static {p1}, Lcom/squareup/noho/NightModeUtilsKt;->nohoNightMode(Landroid/content/Context;)Landroid/content/Context;

    move-result-object p1

    invoke-virtual {v0, p1, p2, p3}, Lcom/squareup/workflow/InflaterDelegate$Real;->inflate(Landroid/content/Context;Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method
