.class public final Lcom/squareup/checkoutflow/emoney/EmoneyPaymentProcessingScreenDarkModule$Companion;
.super Ljava/lang/Object;
.source "EmoneyPaymentProcessingScreenDarkModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutflow/emoney/EmoneyPaymentProcessingScreenDarkModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0087\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H\u0007\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/emoney/EmoneyPaymentProcessingScreenDarkModule$Companion;",
        "",
        "()V",
        "provideInflater",
        "Lcom/squareup/workflow/InflaterDelegate;",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 18
    invoke-direct {p0}, Lcom/squareup/checkoutflow/emoney/EmoneyPaymentProcessingScreenDarkModule$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final provideInflater()Lcom/squareup/workflow/InflaterDelegate;
    .locals 1
    .annotation runtime Lcom/squareup/checkoutflow/emoney/EmoneyPaymentProcessing;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 20
    sget-object v0, Lcom/squareup/checkoutflow/emoney/NightModeInflater;->INSTANCE:Lcom/squareup/checkoutflow/emoney/NightModeInflater;

    check-cast v0, Lcom/squareup/workflow/InflaterDelegate;

    return-object v0
.end method
