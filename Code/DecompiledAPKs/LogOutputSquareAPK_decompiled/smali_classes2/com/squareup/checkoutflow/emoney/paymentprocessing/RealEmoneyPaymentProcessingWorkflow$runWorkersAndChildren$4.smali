.class final Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$runWorkersAndChildren$4;
.super Lkotlin/jvm/internal/Lambda;
.source "RealEmoneyPaymentProcessingWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->runWorkersAndChildren(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;Lcom/squareup/workflow/RenderContext;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;",
        "+",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingResult;",
        "result",
        "Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $input:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;

.field final synthetic $state:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;

.field final synthetic this$0:Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$runWorkersAndChildren$4;->this$0:Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;

    iput-object p2, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$runWorkersAndChildren$4;->$input:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;

    iput-object p3, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$runWorkersAndChildren$4;->$state:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;)Lcom/squareup/workflow/WorkflowAction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingResult;",
            ">;"
        }
    .end annotation

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 467
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$runWorkersAndChildren$4;->this$0:Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;

    invoke-virtual {p1}, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;->getBalance()Ljava/lang/String;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$runWorkersAndChildren$4;->$input:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;

    invoke-virtual {v1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;->getMoney()Lcom/squareup/protos/common/Money;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    const-string v2, "input.money.currency_code"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p1, v1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->access$parseBalance(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Ljava/lang/String;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$runWorkersAndChildren$4;->$state:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;

    check-cast p1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$SuccessfulReaderResponseOnly;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$SuccessfulReaderResponseOnly;->getBalance()Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 469
    :goto_0
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$runWorkersAndChildren$4;->this$0:Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;

    .line 470
    new-instance v1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$SuccessfulReaderResponseOnly;

    iget-object v2, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$runWorkersAndChildren$4;->$state:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;

    check-cast v2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$SuccessfulReaderResponseOnly;

    invoke-virtual {v2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$SuccessfulReaderResponseOnly;->getFromMiryo()Z

    move-result v2

    invoke-direct {v1, p1, v2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$SuccessfulReaderResponseOnly;-><init>(Lcom/squareup/protos/common/Money;Z)V

    check-cast v1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;

    const-string p1, "New balance returned"

    .line 469
    invoke-static {v0, v1, p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->access$performEnterState(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 123
    check-cast p1, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$runWorkersAndChildren$4;->invoke(Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
