.class final Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$handleAuthResponseWithReaderResponse$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealEmoneyPaymentProcessingWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->handleAuthResponseWithReaderResponse(Lcom/squareup/workflow/RenderContext;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$SuccessfulReaderResponseOnly;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
        "+",
        "Lcom/squareup/protos/client/bills/AddTendersResponse;",
        ">;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;",
        "+",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingResult;",
        "authResult",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/bills/AddTendersResponse;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$SuccessfulReaderResponseOnly;

.field final synthetic this$0:Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$SuccessfulReaderResponseOnly;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$handleAuthResponseWithReaderResponse$1;->this$0:Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;

    iput-object p2, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$handleAuthResponseWithReaderResponse$1;->$state:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$SuccessfulReaderResponseOnly;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/workflow/WorkflowAction;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/bills/AddTendersResponse;",
            ">;)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingResult;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const-string v2, "authResult"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 727
    instance-of v2, v1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v2, :cond_0

    .line 728
    iget-object v2, v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$handleAuthResponseWithReaderResponse$1;->this$0:Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;

    invoke-static {v2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->access$getDanglingMiryo$p(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;)Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;

    move-result-object v2

    invoke-interface {v2}, Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;->clearLastAuth()V

    .line 729
    new-instance v2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentComplete;

    iget-object v3, v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$handleAuthResponseWithReaderResponse$1;->$state:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$SuccessfulReaderResponseOnly;

    invoke-virtual {v3}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$SuccessfulReaderResponseOnly;->getBalance()Lcom/squareup/protos/common/Money;

    move-result-object v3

    iget-object v4, v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$handleAuthResponseWithReaderResponse$1;->$state:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$SuccessfulReaderResponseOnly;

    invoke-virtual {v4}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$SuccessfulReaderResponseOnly;->getFromMiryo()Z

    move-result v4

    invoke-direct {v2, v3, v4}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentComplete;-><init>(Lcom/squareup/protos/common/Money;Z)V

    check-cast v2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;

    goto :goto_1

    .line 731
    :cond_0
    instance-of v2, v1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz v2, :cond_2

    .line 732
    move-object v2, v1

    check-cast v2, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    invoke-virtual {v2}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getRetryable()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 733
    new-instance v3, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError;

    .line 734
    sget-object v4, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError$EmoneyErrorType$AuthOnlyRetryablePaymentError;->INSTANCE:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError$EmoneyErrorType$AuthOnlyRetryablePaymentError;

    move-object v5, v4

    check-cast v5, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError$EmoneyErrorType;

    .line 735
    new-instance v4, Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage$ServerMessage;

    iget-object v6, v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$handleAuthResponseWithReaderResponse$1;->this$0:Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;

    invoke-static {v6, v2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->access$getAuthFailureMessage(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v2}, Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage$ServerMessage;-><init>(Ljava/lang/String;)V

    move-object v6, v4

    check-cast v6, Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;

    .line 736
    iget-object v2, v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$handleAuthResponseWithReaderResponse$1;->$state:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$SuccessfulReaderResponseOnly;

    invoke-virtual {v2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$SuccessfulReaderResponseOnly;->getBalance()Lcom/squareup/protos/common/Money;

    move-result-object v7

    const/4 v8, 0x0

    const/16 v9, 0x8

    const/4 v10, 0x0

    move-object v4, v3

    .line 733
    invoke-direct/range {v4 .. v10}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError;-><init>(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError$EmoneyErrorType;Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;Lcom/squareup/protos/common/Money;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    goto :goto_0

    .line 740
    :cond_1
    new-instance v3, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError;

    .line 741
    sget-object v4, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError$EmoneyErrorType$FatalPaymentError;->INSTANCE:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError$EmoneyErrorType$FatalPaymentError;

    move-object v12, v4

    check-cast v12, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError$EmoneyErrorType;

    new-instance v4, Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage$ServerMessage;

    iget-object v5, v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$handleAuthResponseWithReaderResponse$1;->this$0:Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;

    invoke-static {v5, v2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->access$getAuthFailureMessage(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v2}, Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage$ServerMessage;-><init>(Ljava/lang/String;)V

    move-object v13, v4

    check-cast v13, Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;

    iget-object v2, v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$handleAuthResponseWithReaderResponse$1;->$state:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$SuccessfulReaderResponseOnly;

    invoke-virtual {v2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$ProcessingPayment$SuccessfulReaderResponseOnly;->getBalance()Lcom/squareup/protos/common/Money;

    move-result-object v14

    const/4 v15, 0x0

    const/16 v16, 0x8

    const/16 v17, 0x0

    move-object v11, v3

    .line 740
    invoke-direct/range {v11 .. v17}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError;-><init>(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$PaymentError$EmoneyErrorType;Lcom/squareup/checkoutflow/emoney/EmoneyErrorMessage;Lcom/squareup/protos/common/Money;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 732
    :goto_0
    move-object v2, v3

    check-cast v2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;

    .line 747
    :goto_1
    iget-object v3, v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$handleAuthResponseWithReaderResponse$1;->this$0:Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;

    invoke-static {v3, v2, v1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->access$performEnterState(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    return-object v1

    .line 732
    :cond_2
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 123
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$handleAuthResponseWithReaderResponse$1;->invoke(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
