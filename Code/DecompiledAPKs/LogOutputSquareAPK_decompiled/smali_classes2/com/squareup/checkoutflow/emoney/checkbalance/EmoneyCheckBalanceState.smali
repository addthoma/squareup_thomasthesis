.class public abstract Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState;
.super Ljava/lang/Object;
.source "EmoneyCheckBalanceState.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState$ReaderNotReady;,
        Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState$ReaderDisconnected;,
        Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState$WaitingForTap;,
        Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState$CheckingBalance;,
        Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState$WaitingForRetap;,
        Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState$CheckBalanceComplete;,
        Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState$CheckBalanceError;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0007\u0003\u0004\u0005\u0006\u0007\u0008\tB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0007\n\u000b\u000c\r\u000e\u000f\u0010\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState;",
        "",
        "()V",
        "CheckBalanceComplete",
        "CheckBalanceError",
        "CheckingBalance",
        "ReaderDisconnected",
        "ReaderNotReady",
        "WaitingForRetap",
        "WaitingForTap",
        "Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState$ReaderNotReady;",
        "Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState$ReaderDisconnected;",
        "Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState$WaitingForTap;",
        "Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState$CheckingBalance;",
        "Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState$WaitingForRetap;",
        "Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState$CheckBalanceComplete;",
        "Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState$CheckBalanceError;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 7
    invoke-direct {p0}, Lcom/squareup/checkoutflow/emoney/checkbalance/EmoneyCheckBalanceState;-><init>()V

    return-void
.end method
