.class public final Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;
.super Ljava/lang/Object;
.source "CheckoutWorkflow.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutflow/CheckoutWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CheckoutflowConfig"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\u0008J\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0005H\u00c6\u0003J\u000b\u0010\u0011\u001a\u0004\u0018\u00010\u0007H\u00c6\u0003J)\u0010\u0012\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007H\u00c6\u0001J\t\u0010\u0013\u001a\u00020\u0014H\u00d6\u0001J\u0013\u0010\u0015\u001a\u00020\u00162\u0008\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u00d6\u0003J\t\u0010\u0019\u001a\u00020\u0014H\u00d6\u0001J\t\u0010\u001a\u001a\u00020\u001bH\u00d6\u0001J\u0019\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020\u0014H\u00d6\u0001R\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;",
        "Landroid/os/Parcelable;",
        "tenderConfig",
        "Lcom/squareup/tenderpayment/TenderPaymentConfig;",
        "paymentConfig",
        "Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;",
        "networkRequests",
        "Lcom/squareup/checkoutflow/services/NetworkRequestModifier;",
        "(Lcom/squareup/tenderpayment/TenderPaymentConfig;Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;Lcom/squareup/checkoutflow/services/NetworkRequestModifier;)V",
        "getNetworkRequests",
        "()Lcom/squareup/checkoutflow/services/NetworkRequestModifier;",
        "getPaymentConfig",
        "()Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;",
        "getTenderConfig",
        "()Lcom/squareup/tenderpayment/TenderPaymentConfig;",
        "component1",
        "component2",
        "component3",
        "copy",
        "describeContents",
        "",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final networkRequests:Lcom/squareup/checkoutflow/services/NetworkRequestModifier;

.field private final paymentConfig:Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;

.field private final tenderConfig:Lcom/squareup/tenderpayment/TenderPaymentConfig;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig$Creator;

    invoke-direct {v0}, Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig$Creator;-><init>()V

    sput-object v0, Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/tenderpayment/TenderPaymentConfig;Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;Lcom/squareup/checkoutflow/services/NetworkRequestModifier;)V
    .locals 1

    const-string v0, "tenderConfig"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentConfig"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;->tenderConfig:Lcom/squareup/tenderpayment/TenderPaymentConfig;

    iput-object p2, p0, Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;->paymentConfig:Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;

    iput-object p3, p0, Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;->networkRequests:Lcom/squareup/checkoutflow/services/NetworkRequestModifier;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;Lcom/squareup/tenderpayment/TenderPaymentConfig;Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;Lcom/squareup/checkoutflow/services/NetworkRequestModifier;ILjava/lang/Object;)Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;->tenderConfig:Lcom/squareup/tenderpayment/TenderPaymentConfig;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;->paymentConfig:Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;->networkRequests:Lcom/squareup/checkoutflow/services/NetworkRequestModifier;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;->copy(Lcom/squareup/tenderpayment/TenderPaymentConfig;Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;Lcom/squareup/checkoutflow/services/NetworkRequestModifier;)Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/tenderpayment/TenderPaymentConfig;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;->tenderConfig:Lcom/squareup/tenderpayment/TenderPaymentConfig;

    return-object v0
.end method

.method public final component2()Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;->paymentConfig:Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;

    return-object v0
.end method

.method public final component3()Lcom/squareup/checkoutflow/services/NetworkRequestModifier;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;->networkRequests:Lcom/squareup/checkoutflow/services/NetworkRequestModifier;

    return-object v0
.end method

.method public final copy(Lcom/squareup/tenderpayment/TenderPaymentConfig;Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;Lcom/squareup/checkoutflow/services/NetworkRequestModifier;)Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;
    .locals 1

    const-string v0, "tenderConfig"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentConfig"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;-><init>(Lcom/squareup/tenderpayment/TenderPaymentConfig;Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;Lcom/squareup/checkoutflow/services/NetworkRequestModifier;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;

    iget-object v0, p0, Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;->tenderConfig:Lcom/squareup/tenderpayment/TenderPaymentConfig;

    iget-object v1, p1, Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;->tenderConfig:Lcom/squareup/tenderpayment/TenderPaymentConfig;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;->paymentConfig:Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;

    iget-object v1, p1, Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;->paymentConfig:Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;->networkRequests:Lcom/squareup/checkoutflow/services/NetworkRequestModifier;

    iget-object p1, p1, Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;->networkRequests:Lcom/squareup/checkoutflow/services/NetworkRequestModifier;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getNetworkRequests()Lcom/squareup/checkoutflow/services/NetworkRequestModifier;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;->networkRequests:Lcom/squareup/checkoutflow/services/NetworkRequestModifier;

    return-object v0
.end method

.method public final getPaymentConfig()Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;->paymentConfig:Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;

    return-object v0
.end method

.method public final getTenderConfig()Lcom/squareup/tenderpayment/TenderPaymentConfig;
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;->tenderConfig:Lcom/squareup/tenderpayment/TenderPaymentConfig;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;->tenderConfig:Lcom/squareup/tenderpayment/TenderPaymentConfig;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;->paymentConfig:Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;->networkRequests:Lcom/squareup/checkoutflow/services/NetworkRequestModifier;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CheckoutflowConfig(tenderConfig="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;->tenderConfig:Lcom/squareup/tenderpayment/TenderPaymentConfig;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", paymentConfig="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;->paymentConfig:Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", networkRequests="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;->networkRequests:Lcom/squareup/checkoutflow/services/NetworkRequestModifier;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;->tenderConfig:Lcom/squareup/tenderpayment/TenderPaymentConfig;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;->paymentConfig:Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;->networkRequests:Lcom/squareup/checkoutflow/services/NetworkRequestModifier;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
