.class public abstract Lcom/squareup/checkoutflow/payother/PayOtherModule;
.super Ljava/lang/Object;
.source "PayOtherModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract bindPayOtherViewFactoryIntoMainActivity(Lcom/squareup/checkoutflow/payother/PayOtherViewFactory;)Lcom/squareup/workflow/WorkflowViewFactory;
    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method

.method abstract providePayOtherTenderCompleter(Lcom/squareup/checkoutflow/payother/RealPayOtherTenderCompleter;)Lcom/squareup/checkoutflow/payother/PayOtherTenderCompleter;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract providePayOtherViewFactory(Lcom/squareup/checkoutflow/payother/RealPayOtherViewFactory;)Lcom/squareup/checkoutflow/payother/PayOtherViewFactory;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract providePayOtherWorkflow(Lcom/squareup/checkoutflow/payother/RealPayOtherWorkflow;)Lcom/squareup/checkoutflow/payother/PayOtherWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
