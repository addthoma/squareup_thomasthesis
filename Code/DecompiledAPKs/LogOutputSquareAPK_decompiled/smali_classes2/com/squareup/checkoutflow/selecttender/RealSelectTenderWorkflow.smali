.class public final Lcom/squareup/checkoutflow/selecttender/RealSelectTenderWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealSelectTenderWorkflow.kt"

# interfaces
.implements Lcom/squareup/checkoutflow/selecttender/SelectTenderWorkflow;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/checkoutflow/selecttender/SelectTenderInput;",
        "Lcom/squareup/checkoutflow/selecttender/SelectTenderState;",
        "Lcom/squareup/checkoutflow/selecttender/SelectTenderResult;",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;",
        "Lcom/squareup/checkoutflow/selecttender/SelectTenderWorkflow;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u00012&\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0002B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0008J\u001a\u0010\t\u001a\u00020\u00042\u0006\u0010\n\u001a\u00020\u00032\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u000cH\u0016J8\u0010\r\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00072\u0006\u0010\n\u001a\u00020\u00032\u0006\u0010\u000e\u001a\u00020\u00042\u0012\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0010H\u0016J\u0010\u0010\u0011\u001a\u00020\u000c2\u0006\u0010\u000e\u001a\u00020\u0004H\u0016\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/selecttender/RealSelectTenderWorkflow;",
        "Lcom/squareup/checkoutflow/selecttender/SelectTenderWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/checkoutflow/selecttender/SelectTenderInput;",
        "Lcom/squareup/checkoutflow/selecttender/SelectTenderState;",
        "Lcom/squareup/checkoutflow/selecttender/SelectTenderResult;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "()V",
        "initialState",
        "input",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 13
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    return-void
.end method


# virtual methods
.method public initialState(Lcom/squareup/checkoutflow/selecttender/SelectTenderInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/checkoutflow/selecttender/SelectTenderState;
    .locals 0

    const-string p2, "input"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    sget-object p1, Lcom/squareup/checkoutflow/selecttender/SelectTenderState$SelectingTender;->INSTANCE:Lcom/squareup/checkoutflow/selecttender/SelectTenderState$SelectingTender;

    check-cast p1, Lcom/squareup/checkoutflow/selecttender/SelectTenderState;

    return-object p1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 13
    check-cast p1, Lcom/squareup/checkoutflow/selecttender/SelectTenderInput;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/checkoutflow/selecttender/RealSelectTenderWorkflow;->initialState(Lcom/squareup/checkoutflow/selecttender/SelectTenderInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/checkoutflow/selecttender/SelectTenderState;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/checkoutflow/selecttender/SelectTenderInput;Lcom/squareup/checkoutflow/selecttender/SelectTenderState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/Screen;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkoutflow/selecttender/SelectTenderInput;",
            "Lcom/squareup/checkoutflow/selecttender/SelectTenderState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/checkoutflow/selecttender/SelectTenderState;",
            "-",
            "Lcom/squareup/checkoutflow/selecttender/SelectTenderResult;",
            ">;)",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;"
        }
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    instance-of p2, p2, Lcom/squareup/checkoutflow/selecttender/SelectTenderState$SelectingTender;

    if-eqz p2, :cond_0

    .line 31
    new-instance p2, Lcom/squareup/checkoutflow/selecttender/SelectTenderScreenData;

    .line 32
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/selecttender/SelectTenderInput;->getMoney()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 33
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/selecttender/SelectTenderInput;->getSplitTenderEnabled()Z

    move-result p1

    .line 34
    sget-object v1, Lcom/squareup/checkoutflow/selecttender/RealSelectTenderWorkflow$render$1;->INSTANCE:Lcom/squareup/checkoutflow/selecttender/RealSelectTenderWorkflow$render$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v1}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v1

    .line 37
    sget-object v2, Lcom/squareup/checkoutflow/selecttender/RealSelectTenderWorkflow$render$2;->INSTANCE:Lcom/squareup/checkoutflow/selecttender/RealSelectTenderWorkflow$render$2;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v2}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p3

    .line 31
    invoke-direct {p2, v0, p1, v1, p3}, Lcom/squareup/checkoutflow/selecttender/SelectTenderScreenData;-><init>(Lcom/squareup/protos/common/Money;ZLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 30
    invoke-static {p2}, Lcom/squareup/checkoutflow/selecttender/SelectTenderScreenKt;->createSelectTenderScreen(Lcom/squareup/checkoutflow/selecttender/SelectTenderScreenData;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    return-object p1

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 13
    check-cast p1, Lcom/squareup/checkoutflow/selecttender/SelectTenderInput;

    check-cast p2, Lcom/squareup/checkoutflow/selecttender/SelectTenderState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/checkoutflow/selecttender/RealSelectTenderWorkflow;->render(Lcom/squareup/checkoutflow/selecttender/SelectTenderInput;Lcom/squareup/checkoutflow/selecttender/SelectTenderState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    return-object p1
.end method

.method public snapshotState(Lcom/squareup/checkoutflow/selecttender/SelectTenderState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    sget-object p1, Lcom/squareup/workflow/Snapshot;->EMPTY:Lcom/squareup/workflow/Snapshot;

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 13
    check-cast p1, Lcom/squareup/checkoutflow/selecttender/SelectTenderState;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/selecttender/RealSelectTenderWorkflow;->snapshotState(Lcom/squareup/checkoutflow/selecttender/SelectTenderState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
