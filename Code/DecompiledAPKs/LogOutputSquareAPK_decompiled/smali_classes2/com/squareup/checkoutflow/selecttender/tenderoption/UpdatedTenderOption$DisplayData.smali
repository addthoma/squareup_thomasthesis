.class public final Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;
.super Ljava/lang/Object;
.source "UpdatedTenderOption.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DisplayData"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0014\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B5\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0005\u0012\u0006\u0010\u0008\u001a\u00020\u0003\u0012\u0006\u0010\t\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\nJ\t\u0010\u0010\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0012\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0013\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0003H\u00c6\u0003JE\u0010\u0016\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u00032\u0008\u0008\u0002\u0010\t\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\u0017\u001a\u00020\u00052\u0008\u0010\u0018\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0019\u001a\u00020\u001aH\u00d6\u0001J\t\u0010\u001b\u001a\u00020\u001cH\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\rR\u0011\u0010\u0007\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\rR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0004\u0010\rR\u0011\u0010\u0008\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000cR\u0011\u0010\t\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u000c\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;",
        "",
        "amountToCollect",
        "Lcom/squareup/protos/common/Money;",
        "isSplitTender",
        "",
        "isConnectedToInternet",
        "isInOfflineMode",
        "transactionMaximum",
        "transactionMinimum",
        "(Lcom/squareup/protos/common/Money;ZZZLcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V",
        "getAmountToCollect",
        "()Lcom/squareup/protos/common/Money;",
        "()Z",
        "getTransactionMaximum",
        "getTransactionMinimum",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final amountToCollect:Lcom/squareup/protos/common/Money;

.field private final isConnectedToInternet:Z

.field private final isInOfflineMode:Z

.field private final isSplitTender:Z

.field private final transactionMaximum:Lcom/squareup/protos/common/Money;

.field private final transactionMinimum:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/common/Money;ZZZLcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V
    .locals 1

    const-string v0, "amountToCollect"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transactionMaximum"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transactionMinimum"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->amountToCollect:Lcom/squareup/protos/common/Money;

    iput-boolean p2, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->isSplitTender:Z

    iput-boolean p3, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->isConnectedToInternet:Z

    iput-boolean p4, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->isInOfflineMode:Z

    iput-object p5, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->transactionMaximum:Lcom/squareup/protos/common/Money;

    iput-object p6, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->transactionMinimum:Lcom/squareup/protos/common/Money;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;Lcom/squareup/protos/common/Money;ZZZLcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;ILjava/lang/Object;)Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    iget-object p1, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->amountToCollect:Lcom/squareup/protos/common/Money;

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    iget-boolean p2, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->isSplitTender:Z

    :cond_1
    move p8, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    iget-boolean p3, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->isConnectedToInternet:Z

    :cond_2
    move v0, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    iget-boolean p4, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->isInOfflineMode:Z

    :cond_3
    move v1, p4

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->transactionMaximum:Lcom/squareup/protos/common/Money;

    :cond_4
    move-object v2, p5

    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_5

    iget-object p6, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->transactionMinimum:Lcom/squareup/protos/common/Money;

    :cond_5
    move-object v3, p6

    move-object p2, p0

    move-object p3, p1

    move p4, p8

    move p5, v0

    move p6, v1

    move-object p7, v2

    move-object p8, v3

    invoke-virtual/range {p2 .. p8}, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->copy(Lcom/squareup/protos/common/Money;ZZZLcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/protos/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->amountToCollect:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->isSplitTender:Z

    return v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->isConnectedToInternet:Z

    return v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->isInOfflineMode:Z

    return v0
.end method

.method public final component5()Lcom/squareup/protos/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->transactionMaximum:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final component6()Lcom/squareup/protos/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->transactionMinimum:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final copy(Lcom/squareup/protos/common/Money;ZZZLcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;
    .locals 8

    const-string v0, "amountToCollect"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transactionMaximum"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transactionMinimum"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;

    move-object v1, v0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;-><init>(Lcom/squareup/protos/common/Money;ZZZLcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;

    iget-object v0, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->amountToCollect:Lcom/squareup/protos/common/Money;

    iget-object v1, p1, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->amountToCollect:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->isSplitTender:Z

    iget-boolean v1, p1, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->isSplitTender:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->isConnectedToInternet:Z

    iget-boolean v1, p1, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->isConnectedToInternet:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->isInOfflineMode:Z

    iget-boolean v1, p1, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->isInOfflineMode:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->transactionMaximum:Lcom/squareup/protos/common/Money;

    iget-object v1, p1, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->transactionMaximum:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->transactionMinimum:Lcom/squareup/protos/common/Money;

    iget-object p1, p1, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->transactionMinimum:Lcom/squareup/protos/common/Money;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAmountToCollect()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->amountToCollect:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final getTransactionMaximum()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->transactionMaximum:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final getTransactionMinimum()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->transactionMinimum:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    iget-object v0, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->amountToCollect:Lcom/squareup/protos/common/Money;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->isSplitTender:Z

    const/4 v3, 0x1

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->isConnectedToInternet:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->isInOfflineMode:Z

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :cond_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->transactionMaximum:Lcom/squareup/protos/common/Money;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_4
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->transactionMinimum:Lcom/squareup/protos/common/Money;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_5
    add-int/2addr v0, v1

    return v0
.end method

.method public final isConnectedToInternet()Z
    .locals 1

    .line 21
    iget-boolean v0, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->isConnectedToInternet:Z

    return v0
.end method

.method public final isInOfflineMode()Z
    .locals 1

    .line 22
    iget-boolean v0, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->isInOfflineMode:Z

    return v0
.end method

.method public final isSplitTender()Z
    .locals 1

    .line 20
    iget-boolean v0, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->isSplitTender:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DisplayData(amountToCollect="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->amountToCollect:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isSplitTender="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->isSplitTender:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isConnectedToInternet="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->isConnectedToInternet:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isInOfflineMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->isInOfflineMode:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", transactionMaximum="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->transactionMaximum:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", transactionMinimum="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->transactionMinimum:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
