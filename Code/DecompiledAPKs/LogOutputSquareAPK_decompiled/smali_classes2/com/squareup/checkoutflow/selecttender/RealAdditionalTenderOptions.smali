.class public final Lcom/squareup/checkoutflow/selecttender/RealAdditionalTenderOptions;
.super Ljava/lang/Object;
.source "RealAdditionalTenderOptions.kt"

# interfaces
.implements Lcom/squareup/checkoutflow/selecttender/tenderoption/AdditionalTenderOptions;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/selecttender/RealAdditionalTenderOptions;",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/AdditionalTenderOptions;",
        "buyerCheckoutAdditionalTenderOptions",
        "Lcom/squareup/checkoutflow/customercheckout/BuyerCheckoutAdditionalTenderOptions;",
        "(Lcom/squareup/checkoutflow/customercheckout/BuyerCheckoutAdditionalTenderOptions;)V",
        "getAdditionalTenderOptions",
        "",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final buyerCheckoutAdditionalTenderOptions:Lcom/squareup/checkoutflow/customercheckout/BuyerCheckoutAdditionalTenderOptions;


# direct methods
.method public constructor <init>(Lcom/squareup/checkoutflow/customercheckout/BuyerCheckoutAdditionalTenderOptions;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "buyerCheckoutAdditionalTenderOptions"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/selecttender/RealAdditionalTenderOptions;->buyerCheckoutAdditionalTenderOptions:Lcom/squareup/checkoutflow/customercheckout/BuyerCheckoutAdditionalTenderOptions;

    return-void
.end method


# virtual methods
.method public getAdditionalTenderOptions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption;",
            ">;"
        }
    .end annotation

    .line 12
    iget-object v0, p0, Lcom/squareup/checkoutflow/selecttender/RealAdditionalTenderOptions;->buyerCheckoutAdditionalTenderOptions:Lcom/squareup/checkoutflow/customercheckout/BuyerCheckoutAdditionalTenderOptions;

    invoke-interface {v0}, Lcom/squareup/checkoutflow/customercheckout/BuyerCheckoutAdditionalTenderOptions;->getAdditionalTenderOptions()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
