.class public interface abstract Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionFactory;
.super Ljava/lang/Object;
.source "TenderOptionFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H&J\u0008\u0010\u0004\u001a\u00020\u0005H&\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionFactory;",
        "",
        "getTenderOption",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;",
        "getTenderOptionKey",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getTenderOption()Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;
.end method

.method public abstract getTenderOptionKey()Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;
.end method
