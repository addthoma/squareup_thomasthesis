.class final Lcom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflow$render$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealCardProcessingWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflow;->render(Lcom/squareup/checkoutflow/core/auth/CardProcessingProps;Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/checkoutflow/core/auth/AuthResult;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowState;",
        "+",
        "Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowOutput;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowState;",
        "Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowOutput;",
        "it",
        "Lcom/squareup/checkoutflow/core/auth/AuthResult;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflow$render$1;->this$0:Lcom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflow;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/checkoutflow/core/auth/AuthResult;)Lcom/squareup/workflow/WorkflowAction;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkoutflow/core/auth/AuthResult;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowState;",
            "Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    instance-of v0, p1, Lcom/squareup/checkoutflow/core/auth/AuthResult$Success;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    .line 65
    iget-object p1, p0, Lcom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflow$render$1;->this$0:Lcom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflow;

    sget-object v0, Lcom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflow$render$1$1;->INSTANCE:Lcom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflow$render$1$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v2, v0, v1, v2}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 68
    :cond_0
    sget-object v0, Lcom/squareup/checkoutflow/core/auth/AuthResult$RemoteError;->INSTANCE:Lcom/squareup/checkoutflow/core/auth/AuthResult$RemoteError;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object p1, p0, Lcom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflow$render$1;->this$0:Lcom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflow;

    sget-object v0, Lcom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflow$render$1$2;->INSTANCE:Lcom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflow$render$1$2;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v2, v0, v1, v2}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 69
    :cond_1
    sget-object v0, Lcom/squareup/checkoutflow/core/auth/AuthResult$ClientError;->INSTANCE:Lcom/squareup/checkoutflow/core/auth/AuthResult$ClientError;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object p1, p0, Lcom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflow$render$1;->this$0:Lcom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflow;

    new-instance v0, Lcom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflow$render$1$3;

    invoke-direct {v0, p0}, Lcom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflow$render$1$3;-><init>(Lcom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflow$render$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v2, v0, v1, v2}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 72
    :cond_2
    instance-of v0, p1, Lcom/squareup/checkoutflow/core/auth/AuthResult$AuthRejected;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflow$render$1;->this$0:Lcom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflow;

    new-instance v3, Lcom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflow$render$1$4;

    invoke-direct {v3, p1}, Lcom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflow$render$1$4;-><init>(Lcom/squareup/checkoutflow/core/auth/AuthResult;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v2, v3, v1, v2}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 33
    check-cast p1, Lcom/squareup/checkoutflow/core/auth/AuthResult;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflow$render$1;->invoke(Lcom/squareup/checkoutflow/core/auth/AuthResult;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
