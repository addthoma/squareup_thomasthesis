.class public final Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Authorizing;
.super Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState;
.source "OrderPaymentState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Authorizing"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Authorizing$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B\u0011\u0012\n\u0008\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0004J\u000b\u0010\u0007\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u0015\u0010\u0008\u001a\u00020\u00002\n\u0008\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003H\u00c6\u0001J\t\u0010\t\u001a\u00020\nH\u00d6\u0001J\u0013\u0010\u000b\u001a\u00020\u000c2\u0008\u0010\r\u001a\u0004\u0018\u00010\u000eH\u00d6\u0003J\t\u0010\u000f\u001a\u00020\nH\u00d6\u0001J\t\u0010\u0010\u001a\u00020\u0011H\u00d6\u0001J\u0019\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\nH\u00d6\u0001R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Authorizing;",
        "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState;",
        "tipAmount",
        "Lcom/squareup/protos/common/Money;",
        "(Lcom/squareup/protos/common/Money;)V",
        "getTipAmount",
        "()Lcom/squareup/protos/common/Money;",
        "component1",
        "copy",
        "describeContents",
        "",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final tipAmount:Lcom/squareup/protos/common/Money;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Authorizing$Creator;

    invoke-direct {v0}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Authorizing$Creator;-><init>()V

    sput-object v0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Authorizing;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1, v0}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Authorizing;-><init>(Lcom/squareup/protos/common/Money;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/Money;)V
    .locals 1

    const/4 v0, 0x0

    .line 17
    invoke-direct {p0, v0}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Authorizing;->tipAmount:Lcom/squareup/protos/common/Money;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/protos/common/Money;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    .line 16
    check-cast p1, Lcom/squareup/protos/common/Money;

    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Authorizing;-><init>(Lcom/squareup/protos/common/Money;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Authorizing;Lcom/squareup/protos/common/Money;ILjava/lang/Object;)Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Authorizing;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Authorizing;->tipAmount:Lcom/squareup/protos/common/Money;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Authorizing;->copy(Lcom/squareup/protos/common/Money;)Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Authorizing;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/protos/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Authorizing;->tipAmount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final copy(Lcom/squareup/protos/common/Money;)Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Authorizing;
    .locals 1

    new-instance v0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Authorizing;

    invoke-direct {v0, p1}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Authorizing;-><init>(Lcom/squareup/protos/common/Money;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Authorizing;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Authorizing;

    iget-object v0, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Authorizing;->tipAmount:Lcom/squareup/protos/common/Money;

    iget-object p1, p1, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Authorizing;->tipAmount:Lcom/squareup/protos/common/Money;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getTipAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Authorizing;->tipAmount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Authorizing;->tipAmount:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Authorizing(tipAmount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Authorizing;->tipAmount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Authorizing;->tipAmount:Lcom/squareup/protos/common/Money;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    return-void
.end method
