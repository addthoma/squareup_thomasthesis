.class public final Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator$update$3;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "TipInputCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;->update(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/checkoutflow/core/tip/TipInputScreen;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006"
    }
    d2 = {
        "com/squareup/checkoutflow/core/tip/TipInputCoordinator$update$3",
        "Lcom/squareup/text/EmptyTextWatcher;",
        "afterTextChanged",
        "",
        "s",
        "Landroid/text/Editable;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $localeOverrideFactory:Lcom/squareup/locale/LocaleOverrideFactory;

.field final synthetic $screen:Lcom/squareup/checkoutflow/core/tip/TipInputScreen;

.field final synthetic this$0:Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;Lcom/squareup/checkoutflow/core/tip/TipInputScreen;Lcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkoutflow/core/tip/TipInputScreen;",
            "Lcom/squareup/locale/LocaleOverrideFactory;",
            ")V"
        }
    .end annotation

    .line 87
    iput-object p1, p0, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator$update$3;->this$0:Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;

    iput-object p2, p0, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator$update$3;->$screen:Lcom/squareup/checkoutflow/core/tip/TipInputScreen;

    iput-object p3, p0, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator$update$3;->$localeOverrideFactory:Lcom/squareup/locale/LocaleOverrideFactory;

    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    const-string v0, "s"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    iget-object p1, p0, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator$update$3;->this$0:Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;

    iget-object v0, p0, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator$update$3;->$screen:Lcom/squareup/checkoutflow/core/tip/TipInputScreen;

    iget-object v1, p0, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator$update$3;->$localeOverrideFactory:Lcom/squareup/locale/LocaleOverrideFactory;

    invoke-static {p1, v0, v1}, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;->access$updateTitleAndSubtitle(Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator;Lcom/squareup/checkoutflow/core/tip/TipInputScreen;Lcom/squareup/locale/LocaleOverrideFactory;)V

    return-void
.end method
