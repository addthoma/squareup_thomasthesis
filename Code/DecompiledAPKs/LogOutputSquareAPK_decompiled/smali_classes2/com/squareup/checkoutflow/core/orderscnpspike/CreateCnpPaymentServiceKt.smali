.class public final Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentServiceKt;
.super Ljava/lang/Object;
.source "CreateCnpPaymentService.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCreateCnpPaymentService.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CreateCnpPaymentService.kt\ncom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentServiceKt\n*L\n1#1,127:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a\u0014\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0004H\u0002\u001a\u000c\u0010\u0005\u001a\u00020\u0006*\u00020\u0007H\u0002\u001a\u000c\u0010\u0008\u001a\u00020\t*\u00020\nH\u0002\u00a8\u0006\u000b"
    }
    d2 = {
        "encodeToPaymentSourceId",
        "",
        "Lcom/squareup/Card;",
        "cardConverter",
        "Lcom/squareup/payment/CardConverter;",
        "toGeoLocation",
        "Lcom/squareup/protos/connect/v2/GeoLocation;",
        "Landroid/location/Location;",
        "toV2Money",
        "Lcom/squareup/protos/connect/v2/common/Money;",
        "Lcom/squareup/protos/common/Money;",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$encodeToPaymentSourceId(Lcom/squareup/Card;Lcom/squareup/payment/CardConverter;)Ljava/lang/String;
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentServiceKt;->encodeToPaymentSourceId(Lcom/squareup/Card;Lcom/squareup/payment/CardConverter;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$toGeoLocation(Landroid/location/Location;)Lcom/squareup/protos/connect/v2/GeoLocation;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentServiceKt;->toGeoLocation(Landroid/location/Location;)Lcom/squareup/protos/connect/v2/GeoLocation;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$toV2Money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/connect/v2/common/Money;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentServiceKt;->toV2Money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/connect/v2/common/Money;

    move-result-object p0

    return-object p0
.end method

.method private static final encodeToPaymentSourceId(Lcom/squareup/Card;Lcom/squareup/payment/CardConverter;)Ljava/lang/String;
    .locals 2

    .line 112
    invoke-virtual {p1, p0}, Lcom/squareup/payment/CardConverter;->getCardData(Lcom/squareup/Card;)Lcom/squareup/protos/client/bills/CardData;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/protos/client/bills/CardData;->encrypted_keyed_card:Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;

    iget-object p0, p0, Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;->encrypted_keyed_card_data:Lokio/ByteString;

    .line 113
    invoke-virtual {p0}, Lokio/ByteString;->toByteArray()[B

    move-result-object p0

    const/4 p1, 0x2

    .line 114
    invoke-static {p0, p1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object p0

    .line 115
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "{\"keyed_card\":{\"ciphertext\":\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "\"}}"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1, v0}, Lkotlin/text/StringsKt;->trimMargin$default(Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    .line 117
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ccet:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Lkotlin/text/Charsets;->UTF_8:Ljava/nio/charset/Charset;

    if-eqz p0, :cond_0

    invoke-virtual {p0, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p0

    const-string v1, "(this as java.lang.String).getBytes(charset)"

    invoke-static {p0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0, p1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_0
    new-instance p0, Lkotlin/TypeCastException;

    const-string p1, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {p0, p1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private static final toGeoLocation(Landroid/location/Location;)Lcom/squareup/protos/connect/v2/GeoLocation;
    .locals 5

    .line 121
    new-instance v0, Lcom/squareup/protos/connect/v2/GeoLocation;

    .line 122
    new-instance v1, Lcom/squareup/protos/connect/v2/common/Coordinates;

    invoke-virtual {p0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {p0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/squareup/protos/connect/v2/common/Coordinates;-><init>(Ljava/lang/Double;Ljava/lang/Double;)V

    .line 123
    invoke-virtual {p0}, Landroid/location/Location;->getAltitude()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    .line 124
    invoke-virtual {p0}, Landroid/location/Location;->getAccuracy()F

    move-result p0

    float-to-double v3, p0

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p0

    .line 121
    invoke-direct {v0, v1, v2, p0}, Lcom/squareup/protos/connect/v2/GeoLocation;-><init>(Lcom/squareup/protos/connect/v2/common/Coordinates;Ljava/lang/Double;Ljava/lang/Double;)V

    return-object v0
.end method

.method private static final toV2Money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/connect/v2/common/Money;
    .locals 2

    .line 98
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Money$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/common/Money$Builder;-><init>()V

    .line 99
    iget-object v1, p0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/common/Money$Builder;->amount(Ljava/lang/Long;)Lcom/squareup/protos/connect/v2/common/Money$Builder;

    move-result-object v0

    .line 100
    iget-object p0, p0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-virtual {p0}, Lcom/squareup/protos/common/CurrencyCode;->name()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/protos/connect/v2/common/Currency;->valueOf(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/common/Currency;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/protos/connect/v2/common/Money$Builder;->currency(Lcom/squareup/protos/connect/v2/common/Currency;)Lcom/squareup/protos/connect/v2/common/Money$Builder;

    move-result-object p0

    .line 101
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/common/Money$Builder;->build()Lcom/squareup/protos/connect/v2/common/Money;

    move-result-object p0

    const-string v0, "V2Money.Builder()\n      \u2026ode.name))\n      .build()"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method
