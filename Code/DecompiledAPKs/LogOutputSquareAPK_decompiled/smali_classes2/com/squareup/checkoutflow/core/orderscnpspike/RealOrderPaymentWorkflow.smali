.class public final Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealOrderPaymentWorkflow.kt"

# interfaces
.implements Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflow;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentProps;",
        "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState;",
        "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentOutput;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealOrderPaymentWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealOrderPaymentWorkflow.kt\ncom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n*L\n1#1,131:1\n32#2,12:132\n*E\n*S KotlinDebug\n*F\n+ 1 RealOrderPaymentWorkflow.kt\ncom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow\n*L\n51#1,12:132\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u00012<\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012&\u0012$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n0\u0002B/\u0008\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u00a2\u0006\u0002\u0010\u0015J\u001a\u0010\u0016\u001a\u00020\u00042\u0006\u0010\u0017\u001a\u00020\u00032\u0008\u0010\u0018\u001a\u0004\u0018\u00010\u0019H\u0016JN\u0010\u001a\u001a$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n2\u0006\u0010\u0017\u001a\u00020\u00032\u0006\u0010\u001b\u001a\u00020\u00042\u0012\u0010\u001c\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u001dH\u0016J\u0010\u0010\u001e\u001a\u00020\u00192\u0006\u0010\u001b\u001a\u00020\u0004H\u0016R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001f"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow;",
        "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentProps;",
        "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState;",
        "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentOutput;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "buyerLanguageSelectionWorkflow",
        "Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow;",
        "tipWorkflow",
        "Lcom/squareup/checkoutflow/core/tip/TipWorkflow;",
        "cardProcessingWorkflow",
        "Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflow;",
        "ordersPropsHelper",
        "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator;",
        "errorWorkflow",
        "Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflow;",
        "(Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow;Lcom/squareup/checkoutflow/core/tip/TipWorkflow;Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflow;Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator;Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflow;)V",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final buyerLanguageSelectionWorkflow:Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow;

.field private final cardProcessingWorkflow:Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflow;

.field private final errorWorkflow:Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflow;

.field private final ordersPropsHelper:Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator;

.field private final tipWorkflow:Lcom/squareup/checkoutflow/core/tip/TipWorkflow;


# direct methods
.method public constructor <init>(Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow;Lcom/squareup/checkoutflow/core/tip/TipWorkflow;Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflow;Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator;Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflow;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "buyerLanguageSelectionWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tipWorkflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardProcessingWorkflow"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ordersPropsHelper"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "errorWorkflow"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow;->buyerLanguageSelectionWorkflow:Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow;

    iput-object p2, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow;->tipWorkflow:Lcom/squareup/checkoutflow/core/tip/TipWorkflow;

    iput-object p3, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow;->cardProcessingWorkflow:Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflow;

    iput-object p4, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow;->ordersPropsHelper:Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator;

    iput-object p5, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow;->errorWorkflow:Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflow;

    return-void
.end method


# virtual methods
.method public initialState(Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState;
    .locals 5

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p2, :cond_4

    .line 132
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p2}, Lokio/ByteString;->size()I

    move-result v2

    const/4 v3, 0x0

    if-lez v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_1

    goto :goto_1

    :cond_1
    move-object p2, v1

    :goto_1
    if-eqz p2, :cond_3

    .line 137
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    const-string v4, "Parcel.obtain()"

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 138
    invoke-virtual {p2}, Lokio/ByteString;->toByteArray()[B

    move-result-object p2

    .line 139
    array-length v4, p2

    invoke-virtual {v2, p2, v3, v4}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 140
    invoke-virtual {v2, v3}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 141
    const-class p2, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p2

    invoke-virtual {v2, p2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p2

    if-nez p2, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string v3, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {p2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 142
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    goto :goto_2

    :cond_3
    move-object p2, v1

    .line 143
    :goto_2
    check-cast p2, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState;

    if-eqz p2, :cond_4

    goto :goto_3

    .line 51
    :cond_4
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentProps;->getTipConfiguration()Lcom/squareup/checkoutflow/core/orderscnpspike/TipConfiguration;

    move-result-object p1

    instance-of p1, p1, Lcom/squareup/checkoutflow/core/orderscnpspike/TipConfiguration$TipEnabled;

    if-eqz p1, :cond_5

    .line 53
    sget-object p1, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$CollectingTip;->INSTANCE:Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$CollectingTip;

    move-object p2, p1

    check-cast p2, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState;

    goto :goto_3

    .line 55
    :cond_5
    new-instance p1, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Authorizing;

    invoke-direct {p1, v1, v0, v1}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Authorizing;-><init>(Lcom/squareup/protos/common/Money;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    move-object p2, p1

    check-cast p2, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState;

    :goto_3
    return-object p2
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 36
    check-cast p1, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow;->initialState(Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 36
    check-cast p1, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentProps;

    check-cast p2, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow;->render(Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentProps;Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentProps;Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentProps;",
            "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState;",
            "-",
            "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentOutput;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    instance-of v0, p2, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Authorizing;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow;->cardProcessingWorkflow:Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflow;

    move-object v3, v0

    check-cast v3, Lcom/squareup/workflow/Workflow;

    .line 69
    iget-object v0, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow;->ordersPropsHelper:Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator;

    move-object v2, p2

    check-cast v2, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Authorizing;

    invoke-virtual {v2}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Authorizing;->getTipAmount()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-virtual {v0, p1, v2}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator;->createAuthProps(Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentProps;Lcom/squareup/protos/common/Money;)Lcom/squareup/checkoutflow/core/auth/CardProcessingProps;

    move-result-object v4

    const/4 v5, 0x0

    .line 70
    new-instance p1, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow$render$1;

    invoke-direct {p1, p0, p2}, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow$render$1;-><init>(Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow;Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState;)V

    move-object v6, p1

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v7, 0x4

    const/4 v8, 0x0

    move-object v2, p3

    .line 67
    invoke-static/range {v2 .. v8}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    goto/16 :goto_1

    .line 85
    :cond_0
    instance-of v0, p2, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$BuyerLanguageSelection;

    if-eqz v0, :cond_1

    .line 86
    iget-object p1, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow;->buyerLanguageSelectionWorkflow:Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow;

    move-object v3, p1

    check-cast v3, Lcom/squareup/workflow/Workflow;

    sget-object v4, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    const/4 v5, 0x0

    new-instance p1, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow$render$2;

    invoke-direct {p1, p0, p2}, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow$render$2;-><init>(Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow;Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState;)V

    move-object v6, p1

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v7, 0x4

    const/4 v8, 0x0

    move-object v2, p3

    invoke-static/range {v2 .. v8}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    goto/16 :goto_1

    .line 90
    :cond_1
    instance-of v0, p2, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$CollectingTip;

    if-eqz v0, :cond_5

    .line 91
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentProps;->getTipConfiguration()Lcom/squareup/checkoutflow/core/orderscnpspike/TipConfiguration;

    move-result-object v0

    .line 92
    instance-of v2, v0, Lcom/squareup/checkoutflow/core/orderscnpspike/TipConfiguration$TipDisabled;

    if-nez v2, :cond_4

    .line 93
    instance-of v2, v0, Lcom/squareup/checkoutflow/core/orderscnpspike/TipConfiguration$TipEnabled;

    if-eqz v2, :cond_3

    .line 94
    new-instance v2, Lcom/squareup/checkoutflow/core/tip/TipProps;

    .line 95
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentProps;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v4

    .line 96
    check-cast v0, Lcom/squareup/checkoutflow/core/orderscnpspike/TipConfiguration$TipEnabled;

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/core/orderscnpspike/TipConfiguration$TipEnabled;->getAutoGratuity()Lcom/squareup/protos/common/Money;

    move-result-object p1

    if-eqz p1, :cond_2

    const/4 p1, 0x1

    const/4 v5, 0x1

    goto :goto_0

    :cond_2
    const/4 v5, 0x0

    .line 97
    :goto_0
    invoke-virtual {v0}, Lcom/squareup/checkoutflow/core/orderscnpspike/TipConfiguration$TipEnabled;->isUsingCustomAmounts()Z

    move-result v6

    .line 98
    invoke-virtual {v0}, Lcom/squareup/checkoutflow/core/orderscnpspike/TipConfiguration$TipEnabled;->getTipOptions()Ljava/util/List;

    move-result-object v7

    .line 99
    invoke-virtual {v0}, Lcom/squareup/checkoutflow/core/orderscnpspike/TipConfiguration$TipEnabled;->getCustomTipMaxMoney()Lcom/squareup/protos/common/Money;

    move-result-object v8

    move-object v3, v2

    .line 94
    invoke-direct/range {v3 .. v8}, Lcom/squareup/checkoutflow/core/tip/TipProps;-><init>(Lcom/squareup/protos/common/Money;ZZLjava/util/List;Lcom/squareup/protos/common/Money;)V

    .line 102
    iget-object p1, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow;->tipWorkflow:Lcom/squareup/checkoutflow/core/tip/TipWorkflow;

    move-object v4, p1

    check-cast v4, Lcom/squareup/workflow/Workflow;

    const/4 v6, 0x0

    new-instance p1, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow$render$3;

    invoke-direct {p1, p0, p2}, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow$render$3;-><init>(Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow;Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState;)V

    move-object v7, p1

    check-cast v7, Lkotlin/jvm/functions/Function1;

    const/4 v8, 0x4

    const/4 v9, 0x0

    move-object v3, p3

    move-object v5, v2

    invoke-static/range {v3 .. v9}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    .line 112
    sget-object p2, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    goto :goto_1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 92
    :cond_4
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Tipping should not be disabled in the CollectingTip state"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 116
    :cond_5
    instance-of v0, p2, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Error;

    if-eqz v0, :cond_6

    .line 118
    iget-object v0, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow;->errorWorkflow:Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflow;

    move-object v3, v0

    check-cast v3, Lcom/squareup/workflow/Workflow;

    .line 119
    iget-object v0, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow;->ordersPropsHelper:Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator;

    check-cast p2, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Error;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator;->createErrorProps(Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentProps;Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Error;)Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowProps;

    move-result-object v4

    const/4 v5, 0x0

    .line 120
    new-instance p1, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow$render$4;

    invoke-direct {p1, p0}, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow$render$4;-><init>(Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow;)V

    move-object v6, p1

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v7, 0x4

    const/4 v8, 0x0

    move-object v2, p3

    .line 117
    invoke-static/range {v2 .. v8}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    .line 128
    :goto_1
    invoke-static {p1, v1}, Lcom/squareup/workflow/LayeredScreenKt;->withPersistence(Ljava/util/Map;Z)Ljava/util/Map;

    move-result-object p1

    return-object p1

    .line 117
    :cond_6
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 36
    check-cast p1, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/core/orderscnpspike/RealOrderPaymentWorkflow;->snapshotState(Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
