.class public interface abstract Lcom/squareup/checkoutflow/receipt/ReceiptService;
.super Ljava/lang/Object;
.source "ReceiptService.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0006H\'J\u0018\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020\tH\'J\u0018\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u000cH\'\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/receipt/ReceiptService;",
        "",
        "acceptSmsMarketingInvitation",
        "Lcom/squareup/server/AcceptedResponse;",
        "Lcom/squareup/protos/postoffice/sms/SubscribeResponse;",
        "request",
        "Lcom/squareup/protos/postoffice/sms/SubscribeRequest;",
        "declineSmsMarketingInvitation",
        "Lcom/squareup/protos/postoffice/sms/DeclineSubscriptionInvitationResponse;",
        "Lcom/squareup/protos/postoffice/sms/DeclineSubscriptionInvitationRequest;",
        "getSmsMarketingInvitation",
        "Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationResponse;",
        "Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationRequest;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract acceptSmsMarketingInvitation(Lcom/squareup/protos/postoffice/sms/SubscribeRequest;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Lcom/squareup/protos/postoffice/sms/SubscribeRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/postoffice/sms/SubscribeRequest;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/protos/postoffice/sms/SubscribeResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/Headers;
        value = {
            "Accept: application/x-protobuf"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/services/squareup.postoffice.sms.SmsSubscriptionService/Subscribe"
    .end annotation
.end method

.method public abstract declineSmsMarketingInvitation(Lcom/squareup/protos/postoffice/sms/DeclineSubscriptionInvitationRequest;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Lcom/squareup/protos/postoffice/sms/DeclineSubscriptionInvitationRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/postoffice/sms/DeclineSubscriptionInvitationRequest;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/protos/postoffice/sms/DeclineSubscriptionInvitationResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/Headers;
        value = {
            "Accept: application/x-protobuf"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/services/squareup.postoffice.sms.SmsSubscriptionService/DeclineSubscriptionInvitation"
    .end annotation
.end method

.method public abstract getSmsMarketingInvitation(Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationRequest;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationRequest;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/Headers;
        value = {
            "Accept: application/x-protobuf"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/services/squareup.postoffice.sms.SmsSubscriptionService/GetSubscriptionInvitation"
    .end annotation
.end method
