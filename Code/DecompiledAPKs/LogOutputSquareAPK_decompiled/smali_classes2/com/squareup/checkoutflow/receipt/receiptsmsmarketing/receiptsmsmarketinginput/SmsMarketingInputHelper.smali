.class public final Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketinginput/SmsMarketingInputHelper;
.super Ljava/lang/Object;
.source "SmsMarketingInputHelper.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0008R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketinginput/SmsMarketingInputHelper;",
        "",
        "buyerLocaleOverride",
        "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
        "(Lcom/squareup/buyer/language/BuyerLocaleOverride;)V",
        "screenTextData",
        "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketinginput/ReceiptSmsMarketingInputScreen$TextData;",
        "customerPhoneNumber",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;


# direct methods
.method public constructor <init>(Lcom/squareup/buyer/language/BuyerLocaleOverride;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "buyerLocaleOverride"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketinginput/SmsMarketingInputHelper;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    return-void
.end method


# virtual methods
.method public final screenTextData(Ljava/lang/String;)Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketinginput/ReceiptSmsMarketingInputScreen$TextData;
    .locals 4

    .line 12
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketinginput/SmsMarketingInputHelper;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    invoke-interface {v0}, Lcom/squareup/buyer/language/BuyerLocaleOverride;->getBuyerRes()Lcom/squareup/util/Res;

    move-result-object v0

    if-eqz p1, :cond_0

    .line 15
    sget v1, Lcom/squareup/checkoutflow/receipt/impl/R$string;->sms_marketing_input_is_this_correct:I

    goto :goto_0

    .line 17
    :cond_0
    sget v1, Lcom/squareup/checkoutflow/receipt/impl/R$string;->sms_marketing_input_enter_phone_number:I

    .line 13
    :goto_0
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 20
    sget v2, Lcom/squareup/checkoutflow/receipt/impl/R$string;->sms_marketing_input_detail:I

    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz p1, :cond_1

    goto :goto_1

    .line 21
    :cond_1
    sget p1, Lcom/squareup/checkoutflow/receipt/impl/R$string;->buyer_receipt_text_hint:I

    invoke-interface {v0, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 22
    :goto_1
    sget v3, Lcom/squareup/checkoutflow/receipt/impl/R$string;->sms_marketing_input_submit:I

    invoke-interface {v0, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 23
    new-instance v3, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketinginput/ReceiptSmsMarketingInputScreen$TextData;

    invoke-direct {v3, v1, v2, p1, v0}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketinginput/ReceiptSmsMarketingInputScreen$TextData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v3
.end method
