.class public final Lcom/squareup/checkoutflow/receipt/BillPaperReceiptSender_Factory;
.super Ljava/lang/Object;
.source "BillPaperReceiptSender_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/checkoutflow/receipt/BillPaperReceiptSender;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ReceiptForLastPayment;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrintSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ReceiptForLastPayment;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrintSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/BillPaperReceiptSender_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/checkoutflow/receipt/BillPaperReceiptSender_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/checkoutflow/receipt/BillPaperReceiptSender_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 30
    iput-object p4, p0, Lcom/squareup/checkoutflow/receipt/BillPaperReceiptSender_Factory;->arg3Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/checkoutflow/receipt/BillPaperReceiptSender_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ReceiptForLastPayment;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrintSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;",
            ">;)",
            "Lcom/squareup/checkoutflow/receipt/BillPaperReceiptSender_Factory;"
        }
    .end annotation

    .line 41
    new-instance v0, Lcom/squareup/checkoutflow/receipt/BillPaperReceiptSender_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/checkoutflow/receipt/BillPaperReceiptSender_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/payment/ReceiptForLastPayment;Lcom/squareup/print/PrinterStations;Lcom/squareup/print/PrintSettings;Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;)Lcom/squareup/checkoutflow/receipt/BillPaperReceiptSender;
    .locals 1

    .line 46
    new-instance v0, Lcom/squareup/checkoutflow/receipt/BillPaperReceiptSender;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/checkoutflow/receipt/BillPaperReceiptSender;-><init>(Lcom/squareup/payment/ReceiptForLastPayment;Lcom/squareup/print/PrinterStations;Lcom/squareup/print/PrintSettings;Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/checkoutflow/receipt/BillPaperReceiptSender;
    .locals 4

    .line 35
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/BillPaperReceiptSender_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/ReceiptForLastPayment;

    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/BillPaperReceiptSender_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/print/PrinterStations;

    iget-object v2, p0, Lcom/squareup/checkoutflow/receipt/BillPaperReceiptSender_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/print/PrintSettings;

    iget-object v3, p0, Lcom/squareup/checkoutflow/receipt/BillPaperReceiptSender_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/checkoutflow/receipt/BillPaperReceiptSender_Factory;->newInstance(Lcom/squareup/payment/ReceiptForLastPayment;Lcom/squareup/print/PrinterStations;Lcom/squareup/print/PrintSettings;Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;)Lcom/squareup/checkoutflow/receipt/BillPaperReceiptSender;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/receipt/BillPaperReceiptSender_Factory;->get()Lcom/squareup/checkoutflow/receipt/BillPaperReceiptSender;

    move-result-object v0

    return-object v0
.end method
