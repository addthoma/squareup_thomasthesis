.class public abstract Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen$ScreenState;
.super Ljava/lang/Object;
.source "ReceiptInputScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ScreenState"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen$ScreenState$EmailInput;,
        Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen$ScreenState$SmsInput;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0002\u0011\u0012B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006R\u001e\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\t0\u0008X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\n\u0010\u000bR-\u0010\u000c\u001a\u001d\u0012\u0013\u0012\u00110\u0004\u00a2\u0006\u000c\u0008\r\u0012\u0008\u0008\u000e\u0012\u0004\u0008\u0008(\u000f\u0012\u0004\u0012\u00020\t0\u0008X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0010\u0010\u000b\u0082\u0001\u0002\u0013\u0014\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen$ScreenState;",
        "",
        "()V",
        "customerData",
        "",
        "getCustomerData",
        "()Ljava/lang/String;",
        "onExitInputClicked",
        "Lkotlin/Function1;",
        "",
        "getOnExitInputClicked",
        "()Lkotlin/jvm/functions/Function1;",
        "onSendClicked",
        "Lkotlin/ParameterName;",
        "name",
        "input",
        "getOnSendClicked",
        "EmailInput",
        "SmsInput",
        "Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen$ScreenState$EmailInput;",
        "Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen$ScreenState$SmsInput;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 18
    invoke-direct {p0}, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen$ScreenState;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getCustomerData()Ljava/lang/String;
.end method

.method public abstract getOnExitInputClicked()Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getOnSendClicked()Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end method
