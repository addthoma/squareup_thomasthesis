.class public final Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow_Factory;
.super Ljava/lang/Object;
.source "ReceiptSmsMarketingWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/receipt/ReceiptService;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketinginput/SmsMarketingInputHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/receipt/ReceiptService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketinginput/SmsMarketingInputHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 35
    iput-object p2, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 36
    iput-object p3, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 37
    iput-object p4, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 38
    iput-object p5, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow_Factory;->arg4Provider:Ljavax/inject/Provider;

    .line 39
    iput-object p6, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow_Factory;->arg5Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/receipt/ReceiptService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketinginput/SmsMarketingInputHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow_Factory;"
        }
    .end annotation

    .line 51
    new-instance v7, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/analytics/Analytics;Lio/reactivex/Scheduler;Lcom/squareup/checkoutflow/receipt/ReceiptService;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketinginput/SmsMarketingInputHelper;Lcom/squareup/settings/server/Features;)Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;
    .locals 8

    .line 56
    new-instance v7, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;-><init>(Lcom/squareup/analytics/Analytics;Lio/reactivex/Scheduler;Lcom/squareup/checkoutflow/receipt/ReceiptService;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketinginput/SmsMarketingInputHelper;Lcom/squareup/settings/server/Features;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;
    .locals 7

    .line 44
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lio/reactivex/Scheduler;

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/checkoutflow/receipt/ReceiptService;

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/queue/retrofit/RetrofitQueue;

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketinginput/SmsMarketingInputHelper;

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow_Factory;->arg5Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/settings/server/Features;

    invoke-static/range {v1 .. v6}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow_Factory;->newInstance(Lcom/squareup/analytics/Analytics;Lio/reactivex/Scheduler;Lcom/squareup/checkoutflow/receipt/ReceiptService;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketinginput/SmsMarketingInputHelper;Lcom/squareup/settings/server/Features;)Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow_Factory;->get()Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;

    move-result-object v0

    return-object v0
.end method
