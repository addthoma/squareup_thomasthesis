.class public final Lcom/squareup/checkoutflow/receipt/impl/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutflow/receipt/impl/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final card_drawing_margin:I = 0x7f0700af

.field public static final card_elevation:I = 0x7f0700b0

.field public static final shadow_generator_elevation:I = 0x7f0704b9

.field public static final shadow_generator_margin:I = 0x7f0704ba

.field public static final sms_marketing_button_height:I = 0x7f0704bf

.field public static final sms_marketing_button_width:I = 0x7f0704c0

.field public static final sms_marketing_check_margin:I = 0x7f0704c1

.field public static final sms_marketing_check_size:I = 0x7f0704c2

.field public static final sms_marketing_coupon_contents_margin:I = 0x7f0704c3

.field public static final sms_marketing_coupon_height:I = 0x7f0704c4

.field public static final sms_marketing_coupon_terms_size:I = 0x7f0704c5

.field public static final sms_marketing_coupon_title_margin:I = 0x7f0704c6

.field public static final sms_marketing_coupon_title_size:I = 0x7f0704c7

.field public static final sms_marketing_coupon_value_margin:I = 0x7f0704c8

.field public static final sms_marketing_coupon_value_size:I = 0x7f0704c9

.field public static final sms_marketing_coupon_width:I = 0x7f0704ca

.field public static final sms_marketing_disclaimer_size:I = 0x7f0704cb

.field public static final sms_marketing_margin_large:I = 0x7f0704cc

.field public static final sms_marketing_margin_medium:I = 0x7f0704cd

.field public static final sms_marketing_title_size:I = 0x7f0704ce


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
