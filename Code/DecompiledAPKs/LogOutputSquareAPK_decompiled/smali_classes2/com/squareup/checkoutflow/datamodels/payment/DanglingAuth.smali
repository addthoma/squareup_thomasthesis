.class public interface abstract Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;
.super Ljava/lang/Object;
.source "DanglingAuth.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0008\u0002\u0008f\u0018\u00002\u00020\u0001J\u0008\u0010\u0006\u001a\u00020\u0007H&J\u0008\u0010\u0008\u001a\u00020\tH&J\u0010\u0010\n\u001a\u00020\u00072\u0006\u0010\u000b\u001a\u00020\u0003H&J\u001a\u0010\u000c\u001a\u0004\u0018\u00010\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H&J<\u0010\u0012\u001a\u00020\u00072\u0006\u0010\u0013\u001a\u00020\u00142\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u00032\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u00162\u000e\u0010\u0017\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00182\u0006\u0010\u0019\u001a\u00020\tH&R\u0014\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0004\u0010\u0005\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;",
        "",
        "danglingAuthBillId",
        "Lcom/squareup/protos/client/IdPair;",
        "getDanglingAuthBillId",
        "()Lcom/squareup/protos/client/IdPair;",
        "clearLastAuth",
        "",
        "hasDanglingAuth",
        "",
        "onFailedAuth",
        "billId",
        "voidLastAuth",
        "Lcom/squareup/protos/common/Money;",
        "reason",
        "Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;",
        "logReason",
        "",
        "writeLastAuth",
        "amountToReportOnAutoVoid",
        "",
        "paymentType",
        "Lcom/squareup/PaymentType;",
        "tenderIds",
        "",
        "hasCapturedTenders",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract clearLastAuth()V
.end method

.method public abstract getDanglingAuthBillId()Lcom/squareup/protos/client/IdPair;
.end method

.method public abstract hasDanglingAuth()Z
.end method

.method public abstract onFailedAuth(Lcom/squareup/protos/client/IdPair;)V
.end method

.method public abstract voidLastAuth(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;Ljava/lang/String;)Lcom/squareup/protos/common/Money;
.end method

.method public abstract writeLastAuth(JLcom/squareup/protos/client/IdPair;Lcom/squareup/PaymentType;Ljava/util/List;Z)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/squareup/protos/client/IdPair;",
            "Lcom/squareup/PaymentType;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/IdPair;",
            ">;Z)V"
        }
    .end annotation
.end method
