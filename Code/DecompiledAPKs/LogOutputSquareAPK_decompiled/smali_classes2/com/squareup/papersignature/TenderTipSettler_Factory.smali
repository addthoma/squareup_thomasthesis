.class public final Lcom/squareup/papersignature/TenderTipSettler_Factory;
.super Ljava/lang/Object;
.source "TenderTipSettler_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/papersignature/TenderTipSettler;",
        ">;"
    }
.end annotation


# instance fields
.field private final paperSignatureServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderStatusManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/TenderStatusManager;",
            ">;"
        }
    .end annotation
.end field

.field private final tippingCalculatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/TenderStatusManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/papersignature/TenderTipSettler_Factory;->paperSignatureServiceProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/papersignature/TenderTipSettler_Factory;->tenderStatusManagerProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/papersignature/TenderTipSettler_Factory;->tippingCalculatorProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/papersignature/TenderTipSettler_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/TenderStatusManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;",
            ">;)",
            "Lcom/squareup/papersignature/TenderTipSettler_Factory;"
        }
    .end annotation

    .line 41
    new-instance v0, Lcom/squareup/papersignature/TenderTipSettler_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/papersignature/TenderTipSettler_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;Lcom/squareup/papersignature/TenderStatusManager;Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;)Lcom/squareup/papersignature/TenderTipSettler;
    .locals 1

    .line 47
    new-instance v0, Lcom/squareup/papersignature/TenderTipSettler;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/papersignature/TenderTipSettler;-><init>(Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;Lcom/squareup/papersignature/TenderStatusManager;Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/papersignature/TenderTipSettler;
    .locals 3

    .line 34
    iget-object v0, p0, Lcom/squareup/papersignature/TenderTipSettler_Factory;->paperSignatureServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;

    iget-object v1, p0, Lcom/squareup/papersignature/TenderTipSettler_Factory;->tenderStatusManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/papersignature/TenderStatusManager;

    iget-object v2, p0, Lcom/squareup/papersignature/TenderTipSettler_Factory;->tippingCalculatorProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;

    invoke-static {v0, v1, v2}, Lcom/squareup/papersignature/TenderTipSettler_Factory;->newInstance(Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;Lcom/squareup/papersignature/TenderStatusManager;Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;)Lcom/squareup/papersignature/TenderTipSettler;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/papersignature/TenderTipSettler_Factory;->get()Lcom/squareup/papersignature/TenderTipSettler;

    move-result-object v0

    return-object v0
.end method
