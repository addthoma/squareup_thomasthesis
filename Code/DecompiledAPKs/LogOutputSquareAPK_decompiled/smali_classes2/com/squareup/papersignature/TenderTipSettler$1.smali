.class Lcom/squareup/papersignature/TenderTipSettler$1;
.super Ljava/lang/Object;
.source "TenderTipSettler.java"

# interfaces
.implements Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/papersignature/TenderTipSettler;->settleTips(Ljava/util/Collection;Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/papersignature/TenderTipSettler;

.field final synthetic val$callback:Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;

.field final synthetic val$tenders:Ljava/util/Collection;


# direct methods
.method constructor <init>(Lcom/squareup/papersignature/TenderTipSettler;Ljava/util/Collection;Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;)V
    .locals 0

    .line 47
    iput-object p1, p0, Lcom/squareup/papersignature/TenderTipSettler$1;->this$0:Lcom/squareup/papersignature/TenderTipSettler;

    iput-object p2, p0, Lcom/squareup/papersignature/TenderTipSettler$1;->val$tenders:Ljava/util/Collection;

    iput-object p3, p0, Lcom/squareup/papersignature/TenderTipSettler$1;->val$callback:Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/squareup/papersignature/TenderTipSettler$1;->val$callback:Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;

    invoke-interface {v0, p1, p2}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;->onFailure(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .line 47
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/squareup/papersignature/TenderTipSettler$1;->onSuccess(Ljava/lang/Void;)V

    return-void
.end method

.method public onSuccess(Ljava/lang/Void;)V
    .locals 2

    .line 49
    iget-object p1, p0, Lcom/squareup/papersignature/TenderTipSettler$1;->this$0:Lcom/squareup/papersignature/TenderTipSettler;

    iget-object v0, p0, Lcom/squareup/papersignature/TenderTipSettler$1;->val$tenders:Ljava/util/Collection;

    iget-object v1, p0, Lcom/squareup/papersignature/TenderTipSettler$1;->val$callback:Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;

    invoke-static {p1, v0, v1}, Lcom/squareup/papersignature/TenderTipSettler;->access$000(Lcom/squareup/papersignature/TenderTipSettler;Ljava/util/Collection;Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;)V

    return-void
.end method
