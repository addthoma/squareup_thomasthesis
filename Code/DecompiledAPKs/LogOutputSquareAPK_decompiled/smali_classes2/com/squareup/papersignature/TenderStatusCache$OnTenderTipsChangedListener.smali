.class public interface abstract Lcom/squareup/papersignature/TenderStatusCache$OnTenderTipsChangedListener;
.super Ljava/lang/Object;
.source "TenderStatusCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/papersignature/TenderStatusCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnTenderTipsChangedListener"
.end annotation


# virtual methods
.method public abstract onTenderTipsChanged(Ljava/util/Collection;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/squareup/billhistory/model/TenderHistory;",
            ">;)V"
        }
    .end annotation
.end method
