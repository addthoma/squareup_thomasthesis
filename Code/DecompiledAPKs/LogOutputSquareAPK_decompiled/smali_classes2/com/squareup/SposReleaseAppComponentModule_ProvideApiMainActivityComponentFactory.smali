.class public final Lcom/squareup/SposReleaseAppComponentModule_ProvideApiMainActivityComponentFactory;
.super Ljava/lang/Object;
.source "SposReleaseAppComponentModule_ProvideApiMainActivityComponentFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/SposReleaseAppComponentModule_ProvideApiMainActivityComponentFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Ljava/lang/Class<",
        "*>;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/SposReleaseAppComponentModule_ProvideApiMainActivityComponentFactory;
    .locals 1

    .line 18
    invoke-static {}, Lcom/squareup/SposReleaseAppComponentModule_ProvideApiMainActivityComponentFactory$InstanceHolder;->access$000()Lcom/squareup/SposReleaseAppComponentModule_ProvideApiMainActivityComponentFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideApiMainActivityComponent()Ljava/lang/Class;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 22
    invoke-static {}, Lcom/squareup/SposReleaseAppComponentModule;->provideApiMainActivityComponent()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    return-object v0
.end method


# virtual methods
.method public get()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 14
    invoke-static {}, Lcom/squareup/SposReleaseAppComponentModule_ProvideApiMainActivityComponentFactory;->provideApiMainActivityComponent()Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/SposReleaseAppComponentModule_ProvideApiMainActivityComponentFactory;->get()Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method
