.class final Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BuyerScopeComponentImpl$ES_ComponentImpl$PPDS_ComponentImpl;
.super Ljava/lang/Object;
.source "DaggerSposReleaseAppComponent.java"

# interfaces
.implements Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen$Component;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BuyerScopeComponentImpl$ES_ComponentImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "PPDS_ComponentImpl"
.end annotation


# instance fields
.field private pinPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final pinScreenModule:Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen$PinScreenModule;

.field private starGroupPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$4:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BuyerScopeComponentImpl$ES_ComponentImpl;


# direct methods
.method private constructor <init>(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BuyerScopeComponentImpl$ES_ComponentImpl;)V
    .locals 0

    .line 45536
    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BuyerScopeComponentImpl$ES_ComponentImpl$PPDS_ComponentImpl;->this$4:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BuyerScopeComponentImpl$ES_ComponentImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45537
    new-instance p1, Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen$PinScreenModule;

    invoke-direct {p1}, Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen$PinScreenModule;-><init>()V

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BuyerScopeComponentImpl$ES_ComponentImpl$PPDS_ComponentImpl;->pinScreenModule:Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen$PinScreenModule;

    .line 45538
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BuyerScopeComponentImpl$ES_ComponentImpl$PPDS_ComponentImpl;->initialize()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BuyerScopeComponentImpl$ES_ComponentImpl;Lcom/squareup/DaggerSposReleaseAppComponent$1;)V
    .locals 0

    .line 45529
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BuyerScopeComponentImpl$ES_ComponentImpl$PPDS_ComponentImpl;-><init>(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BuyerScopeComponentImpl$ES_ComponentImpl;)V

    return-void
.end method

.method private initialize()V
    .locals 4

    .line 45543
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BuyerScopeComponentImpl$ES_ComponentImpl$PPDS_ComponentImpl;->pinScreenModule:Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen$PinScreenModule;

    invoke-static {v0}, Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen_PinScreenModule_StarGroupPresenterFactory;->create(Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen$PinScreenModule;)Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen_PinScreenModule_StarGroupPresenterFactory;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BuyerScopeComponentImpl$ES_ComponentImpl$PPDS_ComponentImpl;->starGroupPresenterProvider:Ljavax/inject/Provider;

    .line 45544
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BuyerScopeComponentImpl$ES_ComponentImpl$PPDS_ComponentImpl;->pinScreenModule:Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen$PinScreenModule;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BuyerScopeComponentImpl$ES_ComponentImpl$PPDS_ComponentImpl;->starGroupPresenterProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BuyerScopeComponentImpl$ES_ComponentImpl$PPDS_ComponentImpl;->this$4:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BuyerScopeComponentImpl$ES_ComponentImpl;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BuyerScopeComponentImpl$ES_ComponentImpl;->access$164200(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BuyerScopeComponentImpl$ES_ComponentImpl;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BuyerScopeComponentImpl$ES_ComponentImpl$PPDS_ComponentImpl;->this$4:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BuyerScopeComponentImpl$ES_ComponentImpl;

    iget-object v3, v3, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BuyerScopeComponentImpl$ES_ComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BuyerScopeComponentImpl;

    iget-object v3, v3, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BuyerScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v3, v3, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v3}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$22000(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen_PinScreenModule_PinPresenterFactory;->create(Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen$PinScreenModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen_PinScreenModule_PinPresenterFactory;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BuyerScopeComponentImpl$ES_ComponentImpl$PPDS_ComponentImpl;->pinPresenterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method private injectPinView(Lcom/squareup/ui/buyer/emv/pinpad/PinView;)Lcom/squareup/ui/buyer/emv/pinpad/PinView;
    .locals 1

    .line 45561
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BuyerScopeComponentImpl$ES_ComponentImpl$PPDS_ComponentImpl;->pinPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/buyer/emv/pinpad/PinView_MembersInjector;->injectPresenter(Lcom/squareup/ui/buyer/emv/pinpad/PinView;Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;)V

    .line 45562
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BuyerScopeComponentImpl$ES_ComponentImpl$PPDS_ComponentImpl;->this$4:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BuyerScopeComponentImpl$ES_ComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BuyerScopeComponentImpl$ES_ComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BuyerScopeComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BuyerScopeComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$3000(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/utilities/ui/RealDevice;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/buyer/emv/pinpad/PinView_MembersInjector;->injectDevice(Lcom/squareup/ui/buyer/emv/pinpad/PinView;Lcom/squareup/util/Device;)V

    return-object p1
.end method

.method private injectStarGroupMessageView(Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessageView;)Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessageView;
    .locals 1

    .line 45556
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BuyerScopeComponentImpl$ES_ComponentImpl$PPDS_ComponentImpl;->starGroupPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessageView_MembersInjector;->injectPresenter(Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessageView;Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;)V

    return-object p1
.end method


# virtual methods
.method public inject(Lcom/squareup/ui/buyer/emv/pinpad/PinView;)V
    .locals 0

    .line 45553
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BuyerScopeComponentImpl$ES_ComponentImpl$PPDS_ComponentImpl;->injectPinView(Lcom/squareup/ui/buyer/emv/pinpad/PinView;)Lcom/squareup/ui/buyer/emv/pinpad/PinView;

    return-void
.end method

.method public inject(Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessageView;)V
    .locals 0

    .line 45549
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$BuyerScopeComponentImpl$ES_ComponentImpl$PPDS_ComponentImpl;->injectStarGroupMessageView(Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessageView;)Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessageView;

    return-void
.end method
