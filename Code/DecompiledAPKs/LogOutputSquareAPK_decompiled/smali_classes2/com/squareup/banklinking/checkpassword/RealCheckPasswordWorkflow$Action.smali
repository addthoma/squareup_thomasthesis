.class public abstract Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$Action;
.super Ljava/lang/Object;
.source "RealCheckPasswordWorkflow.kt"

# interfaces
.implements Lcom/squareup/workflow/WorkflowAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Action"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$Action$CheckPassword;,
        Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$Action$SucceedInPasswordCheck;,
        Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$Action$FailInPasswordCheck;,
        Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$Action$DismissWarning;,
        Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$Action$CancelPasswordCheck;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/banklinking/checkpassword/CheckPasswordState;",
        "Lcom/squareup/banklinking/checkpassword/CheckPasswordOutput;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0005\u0008\t\n\u000b\u000cB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0004J\u0018\u0010\u0005\u001a\u00020\u0006*\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0007H\u0016\u0082\u0001\u0005\r\u000e\u000f\u0010\u0011\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$Action;",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/banklinking/checkpassword/CheckPasswordState;",
        "Lcom/squareup/banklinking/checkpassword/CheckPasswordOutput;",
        "()V",
        "apply",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "CancelPasswordCheck",
        "CheckPassword",
        "DismissWarning",
        "FailInPasswordCheck",
        "SucceedInPasswordCheck",
        "Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$Action$CheckPassword;",
        "Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$Action$SucceedInPasswordCheck;",
        "Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$Action$FailInPasswordCheck;",
        "Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$Action$DismissWarning;",
        "Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$Action$CancelPasswordCheck;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 47
    invoke-direct {p0}, Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$Action;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/banklinking/checkpassword/CheckPasswordOutput;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/banklinking/checkpassword/CheckPasswordState;",
            ">;)",
            "Lcom/squareup/banklinking/checkpassword/CheckPasswordOutput;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Implement Updater.apply"
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    invoke-static {p0, p1}, Lcom/squareup/workflow/WorkflowAction$DefaultImpls;->apply(Lcom/squareup/workflow/WorkflowAction;Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/banklinking/checkpassword/CheckPasswordOutput;

    return-object p1
.end method

.method public bridge synthetic apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;
    .locals 0

    .line 47
    invoke-virtual {p0, p1}, Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$Action;->apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/banklinking/checkpassword/CheckPasswordOutput;

    move-result-object p1

    return-object p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/banklinking/checkpassword/CheckPasswordState;",
            "-",
            "Lcom/squareup/banklinking/checkpassword/CheckPasswordOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    instance-of v0, p0, Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$Action$CheckPassword;

    if-eqz v0, :cond_0

    .line 57
    new-instance v0, Lcom/squareup/banklinking/checkpassword/CheckPasswordState$CheckingPassword;

    move-object v1, p0

    check-cast v1, Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$Action$CheckPassword;

    invoke-virtual {v1}, Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$Action$CheckPassword;->getPassword()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/banklinking/checkpassword/CheckPasswordState$CheckingPassword;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    goto :goto_0

    .line 59
    :cond_0
    sget-object v0, Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$Action$SucceedInPasswordCheck;->INSTANCE:Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$Action$SucceedInPasswordCheck;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/banklinking/checkpassword/CheckPasswordOutput$CheckPasswordSucceeded;->INSTANCE:Lcom/squareup/banklinking/checkpassword/CheckPasswordOutput$CheckPasswordSucceeded;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    goto :goto_0

    .line 60
    :cond_1
    instance-of v0, p0, Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$Action$FailInPasswordCheck;

    if-eqz v0, :cond_2

    .line 61
    new-instance v0, Lcom/squareup/banklinking/checkpassword/CheckPasswordState$InvalidPassword;

    move-object v1, p0

    check-cast v1, Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$Action$FailInPasswordCheck;

    invoke-virtual {v1}, Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$Action$FailInPasswordCheck;->getWarning()Lcom/squareup/widgets/warning/Warning;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/banklinking/checkpassword/CheckPasswordState$InvalidPassword;-><init>(Lcom/squareup/widgets/warning/Warning;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    goto :goto_0

    .line 63
    :cond_2
    sget-object v0, Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$Action$DismissWarning;->INSTANCE:Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$Action$DismissWarning;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 64
    sget-object v0, Lcom/squareup/banklinking/checkpassword/CheckPasswordState$PrepareToCheckPassword;->INSTANCE:Lcom/squareup/banklinking/checkpassword/CheckPasswordState$PrepareToCheckPassword;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    goto :goto_0

    .line 66
    :cond_3
    sget-object v0, Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$Action$CancelPasswordCheck;->INSTANCE:Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$Action$CancelPasswordCheck;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/banklinking/checkpassword/CheckPasswordOutput$CheckPasswordCanceled;->INSTANCE:Lcom/squareup/banklinking/checkpassword/CheckPasswordOutput$CheckPasswordCanceled;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
