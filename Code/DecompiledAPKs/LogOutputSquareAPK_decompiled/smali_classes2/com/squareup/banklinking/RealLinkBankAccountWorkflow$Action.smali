.class public abstract Lcom/squareup/banklinking/RealLinkBankAccountWorkflow$Action;
.super Ljava/lang/Object;
.source "RealLinkBankAccountWorkflow.kt"

# interfaces
.implements Lcom/squareup/workflow/WorkflowAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/banklinking/RealLinkBankAccountWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Action"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/banklinking/RealLinkBankAccountWorkflow$Action$Finish;,
        Lcom/squareup/banklinking/RealLinkBankAccountWorkflow$Action$CheckPassword;,
        Lcom/squareup/banklinking/RealLinkBankAccountWorkflow$Action$LinkBank;,
        Lcom/squareup/banklinking/RealLinkBankAccountWorkflow$Action$ShowResult;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/banklinking/LinkBankAccountState;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0004\u0007\u0008\t\nB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0004J\u0018\u0010\u0005\u001a\u00020\u0003*\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0006H\u0016\u0082\u0001\u0004\u000b\u000c\r\u000e\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/banklinking/RealLinkBankAccountWorkflow$Action;",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/banklinking/LinkBankAccountState;",
        "",
        "()V",
        "apply",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "CheckPassword",
        "Finish",
        "LinkBank",
        "ShowResult",
        "Lcom/squareup/banklinking/RealLinkBankAccountWorkflow$Action$Finish;",
        "Lcom/squareup/banklinking/RealLinkBankAccountWorkflow$Action$CheckPassword;",
        "Lcom/squareup/banklinking/RealLinkBankAccountWorkflow$Action$LinkBank;",
        "Lcom/squareup/banklinking/RealLinkBankAccountWorkflow$Action$ShowResult;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 68
    invoke-direct {p0}, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow$Action;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;
    .locals 0

    .line 68
    invoke-virtual {p0, p1}, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow$Action;->apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lkotlin/Unit;

    move-result-object p1

    return-object p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lkotlin/Unit;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/banklinking/LinkBankAccountState;",
            ">;)",
            "Lkotlin/Unit;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Implement Updater.apply"
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    invoke-static {p0, p1}, Lcom/squareup/workflow/WorkflowAction$DefaultImpls;->apply(Lcom/squareup/workflow/WorkflowAction;Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/Unit;

    return-object p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/banklinking/LinkBankAccountState;",
            "-",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 88
    sget-object v0, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow$Action$Finish;->INSTANCE:Lcom/squareup/banklinking/RealLinkBankAccountWorkflow$Action$Finish;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    goto :goto_0

    .line 89
    :cond_0
    instance-of v0, p0, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow$Action$CheckPassword;

    if-eqz v0, :cond_1

    .line 90
    new-instance v0, Lcom/squareup/banklinking/LinkBankAccountState$CheckingPassword;

    move-object v1, p0

    check-cast v1, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow$Action$CheckPassword;

    invoke-virtual {v1}, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow$Action$CheckPassword;->getRequiresPassword()Z

    move-result v2

    invoke-virtual {v1}, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow$Action$CheckPassword;->getPrimaryInstitutionNumber()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow$Action$CheckPassword;->getVerificationState()Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    move-result-object v1

    invoke-direct {v0, v2, v3, v1}, Lcom/squareup/banklinking/LinkBankAccountState$CheckingPassword;-><init>(ZLjava/lang/String;Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    goto :goto_0

    .line 92
    :cond_1
    instance-of v0, p0, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow$Action$LinkBank;

    if-eqz v0, :cond_2

    .line 94
    new-instance v0, Lcom/squareup/banklinking/LinkBankAccountState$CheckingBankAccountInfo;

    move-object v1, p0

    check-cast v1, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow$Action$LinkBank;

    invoke-virtual {v1}, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow$Action$LinkBank;->getRequiresPassword()Z

    move-result v2

    invoke-virtual {v1}, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow$Action$LinkBank;->getPrimaryInstitutionNumber()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow$Action$LinkBank;->getVerificationState()Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    move-result-object v1

    invoke-direct {v0, v2, v3, v1}, Lcom/squareup/banklinking/LinkBankAccountState$CheckingBankAccountInfo;-><init>(ZLjava/lang/String;Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    goto :goto_0

    .line 96
    :cond_2
    instance-of v0, p0, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow$Action$ShowResult;

    if-eqz v0, :cond_3

    .line 97
    new-instance v0, Lcom/squareup/banklinking/LinkBankAccountState$ShowingResult;

    move-object v1, p0

    check-cast v1, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow$Action$ShowResult;

    invoke-virtual {v1}, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow$Action$ShowResult;->getLinkBankAccountState()Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;

    move-result-object v2

    invoke-virtual {v1}, Lcom/squareup/banklinking/RealLinkBankAccountWorkflow$Action$ShowResult;->getVerificationState()Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Lcom/squareup/banklinking/LinkBankAccountState$ShowingResult;-><init>(Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
