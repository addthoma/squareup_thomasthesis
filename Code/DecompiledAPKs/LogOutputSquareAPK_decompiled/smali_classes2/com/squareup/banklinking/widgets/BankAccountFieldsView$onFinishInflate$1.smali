.class final Lcom/squareup/banklinking/widgets/BankAccountFieldsView$onFinishInflate$1;
.super Ljava/lang/Object;
.source "BankAccountFieldsView.kt"

# interfaces
.implements Lcom/squareup/coordinators/CoordinatorProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/banklinking/widgets/BankAccountFieldsView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBankAccountFieldsView.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BankAccountFieldsView.kt\ncom/squareup/banklinking/widgets/BankAccountFieldsView$onFinishInflate$1\n*L\n1#1,93:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;",
        "it",
        "Landroid/view/View;",
        "provideCoordinator"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/banklinking/widgets/BankAccountFieldsView;


# direct methods
.method constructor <init>(Lcom/squareup/banklinking/widgets/BankAccountFieldsView;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/banklinking/widgets/BankAccountFieldsView$onFinishInflate$1;->this$0:Lcom/squareup/banklinking/widgets/BankAccountFieldsView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final provideCoordinator(Landroid/view/View;)Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;
    .locals 3

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    iget-object p1, p0, Lcom/squareup/banklinking/widgets/BankAccountFieldsView$onFinishInflate$1;->this$0:Lcom/squareup/banklinking/widgets/BankAccountFieldsView;

    invoke-static {p1}, Lcom/squareup/banklinking/widgets/BankAccountFieldsView;->access$resolveBankNumbersCoordinator(Lcom/squareup/banklinking/widgets/BankAccountFieldsView;)Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/banklinking/widgets/BankAccountFieldsView;->access$setCoordinator$p(Lcom/squareup/banklinking/widgets/BankAccountFieldsView;Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;)V

    .line 66
    iget-object p1, p0, Lcom/squareup/banklinking/widgets/BankAccountFieldsView$onFinishInflate$1;->this$0:Lcom/squareup/banklinking/widgets/BankAccountFieldsView;

    invoke-static {p1}, Lcom/squareup/banklinking/widgets/BankAccountFieldsView;->access$getCoordinator$p(Lcom/squareup/banklinking/widgets/BankAccountFieldsView;)Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;->getLayoutId()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/banklinking/widgets/BankAccountFieldsView$onFinishInflate$1;->this$0:Lcom/squareup/banklinking/widgets/BankAccountFieldsView;

    check-cast v1, Landroid/view/ViewGroup;

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    return-object p1
.end method

.method public bridge synthetic provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 0

    .line 24
    invoke-virtual {p0, p1}, Lcom/squareup/banklinking/widgets/BankAccountFieldsView$onFinishInflate$1;->provideCoordinator(Landroid/view/View;)Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;

    move-result-object p1

    check-cast p1, Lcom/squareup/coordinators/Coordinator;

    return-object p1
.end method
