.class public abstract Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult;
.super Ljava/lang/Object;
.source "BankFieldsCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ValidationResult"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult$NotApplicable;,
        Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult$Failure;,
        Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult$Success;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0003\t\n\u000bB\u001b\u0008\u0002\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0005R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\u0007\u0082\u0001\u0003\u000c\r\u000e\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult;",
        "",
        "iconRes",
        "",
        "colorRes",
        "(II)V",
        "getColorRes",
        "()I",
        "getIconRes",
        "Failure",
        "NotApplicable",
        "Success",
        "Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult$NotApplicable;",
        "Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult$Failure;",
        "Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult$Success;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final colorRes:I

.field private final iconRes:I


# direct methods
.method private constructor <init>(II)V
    .locals 0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult;->iconRes:I

    iput p2, p0, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult;->colorRes:I

    return-void
.end method

.method public synthetic constructor <init>(IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 37
    invoke-direct {p0, p1, p2}, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult;-><init>(II)V

    return-void
.end method


# virtual methods
.method public final getColorRes()I
    .locals 1

    .line 39
    iget v0, p0, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult;->colorRes:I

    return v0
.end method

.method public final getIconRes()I
    .locals 1

    .line 38
    iget v0, p0, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult;->iconRes:I

    return v0
.end method
