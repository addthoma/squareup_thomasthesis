.class public final Lcom/squareup/banklinking/widgets/BankAccountFieldsView_MembersInjector;
.super Ljava/lang/Object;
.source "BankAccountFieldsView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/banklinking/widgets/BankAccountFieldsView;",
        ">;"
    }
.end annotation


# instance fields
.field private final _countryCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;)V"
        }
    .end annotation

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/squareup/banklinking/widgets/BankAccountFieldsView_MembersInjector;->_countryCodeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/banklinking/widgets/BankAccountFieldsView;",
            ">;"
        }
    .end annotation

    .line 22
    new-instance v0, Lcom/squareup/banklinking/widgets/BankAccountFieldsView_MembersInjector;

    invoke-direct {v0, p0}, Lcom/squareup/banklinking/widgets/BankAccountFieldsView_MembersInjector;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static inject_countryCode(Lcom/squareup/banklinking/widgets/BankAccountFieldsView;Lcom/squareup/CountryCode;)V
    .locals 0

    .line 31
    iput-object p1, p0, Lcom/squareup/banklinking/widgets/BankAccountFieldsView;->_countryCode:Lcom/squareup/CountryCode;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/banklinking/widgets/BankAccountFieldsView;)V
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/banklinking/widgets/BankAccountFieldsView_MembersInjector;->_countryCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/CountryCode;

    invoke-static {p1, v0}, Lcom/squareup/banklinking/widgets/BankAccountFieldsView_MembersInjector;->inject_countryCode(Lcom/squareup/banklinking/widgets/BankAccountFieldsView;Lcom/squareup/CountryCode;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 9
    check-cast p1, Lcom/squareup/banklinking/widgets/BankAccountFieldsView;

    invoke-virtual {p0, p1}, Lcom/squareup/banklinking/widgets/BankAccountFieldsView_MembersInjector;->injectMembers(Lcom/squareup/banklinking/widgets/BankAccountFieldsView;)V

    return-void
.end method
