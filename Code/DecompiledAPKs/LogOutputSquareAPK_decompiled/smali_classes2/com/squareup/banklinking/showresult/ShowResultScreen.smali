.class public final Lcom/squareup/banklinking/showresult/ShowResultScreen;
.super Ljava/lang/Object;
.source "ShowResultScreen.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/V2Screen;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0014\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001BS\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0003\u0010\u0004\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u0003\u0012\u0008\u0008\u0003\u0010\u0007\u001a\u00020\u0003\u0012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u0012\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u00a2\u0006\u0002\u0010\u000cJ\t\u0010\u0016\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0017\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0018\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0019\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001a\u001a\u00020\u0003H\u00c6\u0003J\u000f\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u00020\n0\tH\u00c6\u0003J\u000f\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u00020\n0\tH\u00c6\u0003J[\u0010\u001d\u001a\u00020\u00002\u0008\u0008\u0003\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0003\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0003\u0010\u0005\u001a\u00020\u00032\u0008\u0008\u0003\u0010\u0006\u001a\u00020\u00032\u0008\u0008\u0003\u0010\u0007\u001a\u00020\u00032\u000e\u0008\u0002\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t2\u000e\u0008\u0002\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\n0\tH\u00c6\u0001J\u0013\u0010\u001e\u001a\u00020\u001f2\u0008\u0010 \u001a\u0004\u0018\u00010!H\u00d6\u0003J\t\u0010\"\u001a\u00020\u0003H\u00d6\u0001J\t\u0010#\u001a\u00020$H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u000eR\u0017\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0017\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0011R\u0011\u0010\u0006\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u000eR\u0011\u0010\u0007\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u000eR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u000e\u00a8\u0006%"
    }
    d2 = {
        "Lcom/squareup/banklinking/showresult/ShowResultScreen;",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        "drawable",
        "",
        "title",
        "message",
        "primaryButtonText",
        "secondaryButtonText",
        "onPrimaryButtonClicked",
        "Lkotlin/Function0;",
        "",
        "onSecondaryButtonClicked",
        "(IIIIILkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V",
        "getDrawable",
        "()I",
        "getMessage",
        "getOnPrimaryButtonClicked",
        "()Lkotlin/jvm/functions/Function0;",
        "getOnSecondaryButtonClicked",
        "getPrimaryButtonText",
        "getSecondaryButtonText",
        "getTitle",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final drawable:I

.field private final message:I

.field private final onPrimaryButtonClicked:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onSecondaryButtonClicked:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final primaryButtonText:I

.field private final secondaryButtonText:I

.field private final title:I


# direct methods
.method public constructor <init>(IIIIILkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIIII",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "onPrimaryButtonClicked"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onSecondaryButtonClicked"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/squareup/banklinking/showresult/ShowResultScreen;->drawable:I

    iput p2, p0, Lcom/squareup/banklinking/showresult/ShowResultScreen;->title:I

    iput p3, p0, Lcom/squareup/banklinking/showresult/ShowResultScreen;->message:I

    iput p4, p0, Lcom/squareup/banklinking/showresult/ShowResultScreen;->primaryButtonText:I

    iput p5, p0, Lcom/squareup/banklinking/showresult/ShowResultScreen;->secondaryButtonText:I

    iput-object p6, p0, Lcom/squareup/banklinking/showresult/ShowResultScreen;->onPrimaryButtonClicked:Lkotlin/jvm/functions/Function0;

    iput-object p7, p0, Lcom/squareup/banklinking/showresult/ShowResultScreen;->onSecondaryButtonClicked:Lkotlin/jvm/functions/Function0;

    return-void
.end method

.method public synthetic constructor <init>(IIIIILkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 10

    and-int/lit8 v0, p8, 0x2

    const/4 v1, -0x1

    if-eqz v0, :cond_0

    const/4 v4, -0x1

    goto :goto_0

    :cond_0
    move v4, p2

    :goto_0
    and-int/lit8 v0, p8, 0x10

    if-eqz v0, :cond_1

    const/4 v7, -0x1

    goto :goto_1

    :cond_1
    move v7, p5

    :goto_1
    move-object v2, p0

    move v3, p1

    move v5, p3

    move v6, p4

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    .line 13
    invoke-direct/range {v2 .. v9}, Lcom/squareup/banklinking/showresult/ShowResultScreen;-><init>(IIIIILkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/banklinking/showresult/ShowResultScreen;IIIIILkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lcom/squareup/banklinking/showresult/ShowResultScreen;
    .locals 5

    and-int/lit8 p9, p8, 0x1

    if-eqz p9, :cond_0

    iget p1, p0, Lcom/squareup/banklinking/showresult/ShowResultScreen;->drawable:I

    :cond_0
    and-int/lit8 p9, p8, 0x2

    if-eqz p9, :cond_1

    iget p2, p0, Lcom/squareup/banklinking/showresult/ShowResultScreen;->title:I

    :cond_1
    move p9, p2

    and-int/lit8 p2, p8, 0x4

    if-eqz p2, :cond_2

    iget p3, p0, Lcom/squareup/banklinking/showresult/ShowResultScreen;->message:I

    :cond_2
    move v0, p3

    and-int/lit8 p2, p8, 0x8

    if-eqz p2, :cond_3

    iget p4, p0, Lcom/squareup/banklinking/showresult/ShowResultScreen;->primaryButtonText:I

    :cond_3
    move v1, p4

    and-int/lit8 p2, p8, 0x10

    if-eqz p2, :cond_4

    iget p5, p0, Lcom/squareup/banklinking/showresult/ShowResultScreen;->secondaryButtonText:I

    :cond_4
    move v2, p5

    and-int/lit8 p2, p8, 0x20

    if-eqz p2, :cond_5

    iget-object p6, p0, Lcom/squareup/banklinking/showresult/ShowResultScreen;->onPrimaryButtonClicked:Lkotlin/jvm/functions/Function0;

    :cond_5
    move-object v3, p6

    and-int/lit8 p2, p8, 0x40

    if-eqz p2, :cond_6

    iget-object p7, p0, Lcom/squareup/banklinking/showresult/ShowResultScreen;->onSecondaryButtonClicked:Lkotlin/jvm/functions/Function0;

    :cond_6
    move-object v4, p7

    move-object p2, p0

    move p3, p1

    move p4, p9

    move p5, v0

    move p6, v1

    move p7, v2

    move-object p8, v3

    move-object p9, v4

    invoke-virtual/range {p2 .. p9}, Lcom/squareup/banklinking/showresult/ShowResultScreen;->copy(IIIIILkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)Lcom/squareup/banklinking/showresult/ShowResultScreen;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/squareup/banklinking/showresult/ShowResultScreen;->drawable:I

    return v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/squareup/banklinking/showresult/ShowResultScreen;->title:I

    return v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/squareup/banklinking/showresult/ShowResultScreen;->message:I

    return v0
.end method

.method public final component4()I
    .locals 1

    iget v0, p0, Lcom/squareup/banklinking/showresult/ShowResultScreen;->primaryButtonText:I

    return v0
.end method

.method public final component5()I
    .locals 1

    iget v0, p0, Lcom/squareup/banklinking/showresult/ShowResultScreen;->secondaryButtonText:I

    return v0
.end method

.method public final component6()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/banklinking/showresult/ShowResultScreen;->onPrimaryButtonClicked:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final component7()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/banklinking/showresult/ShowResultScreen;->onSecondaryButtonClicked:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final copy(IIIIILkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)Lcom/squareup/banklinking/showresult/ShowResultScreen;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIIII",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/banklinking/showresult/ShowResultScreen;"
        }
    .end annotation

    const-string v0, "onPrimaryButtonClicked"

    move-object v7, p6

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onSecondaryButtonClicked"

    move-object/from16 v8, p7

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/banklinking/showresult/ShowResultScreen;

    move-object v1, v0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v1 .. v8}, Lcom/squareup/banklinking/showresult/ShowResultScreen;-><init>(IIIIILkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/banklinking/showresult/ShowResultScreen;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/banklinking/showresult/ShowResultScreen;

    iget v0, p0, Lcom/squareup/banklinking/showresult/ShowResultScreen;->drawable:I

    iget v1, p1, Lcom/squareup/banklinking/showresult/ShowResultScreen;->drawable:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/banklinking/showresult/ShowResultScreen;->title:I

    iget v1, p1, Lcom/squareup/banklinking/showresult/ShowResultScreen;->title:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/banklinking/showresult/ShowResultScreen;->message:I

    iget v1, p1, Lcom/squareup/banklinking/showresult/ShowResultScreen;->message:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/banklinking/showresult/ShowResultScreen;->primaryButtonText:I

    iget v1, p1, Lcom/squareup/banklinking/showresult/ShowResultScreen;->primaryButtonText:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/banklinking/showresult/ShowResultScreen;->secondaryButtonText:I

    iget v1, p1, Lcom/squareup/banklinking/showresult/ShowResultScreen;->secondaryButtonText:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/banklinking/showresult/ShowResultScreen;->onPrimaryButtonClicked:Lkotlin/jvm/functions/Function0;

    iget-object v1, p1, Lcom/squareup/banklinking/showresult/ShowResultScreen;->onPrimaryButtonClicked:Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/banklinking/showresult/ShowResultScreen;->onSecondaryButtonClicked:Lkotlin/jvm/functions/Function0;

    iget-object p1, p1, Lcom/squareup/banklinking/showresult/ShowResultScreen;->onSecondaryButtonClicked:Lkotlin/jvm/functions/Function0;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getDrawable()I
    .locals 1

    .line 9
    iget v0, p0, Lcom/squareup/banklinking/showresult/ShowResultScreen;->drawable:I

    return v0
.end method

.method public final getMessage()I
    .locals 1

    .line 11
    iget v0, p0, Lcom/squareup/banklinking/showresult/ShowResultScreen;->message:I

    return v0
.end method

.method public final getOnPrimaryButtonClicked()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 14
    iget-object v0, p0, Lcom/squareup/banklinking/showresult/ShowResultScreen;->onPrimaryButtonClicked:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getOnSecondaryButtonClicked()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 15
    iget-object v0, p0, Lcom/squareup/banklinking/showresult/ShowResultScreen;->onSecondaryButtonClicked:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getPrimaryButtonText()I
    .locals 1

    .line 12
    iget v0, p0, Lcom/squareup/banklinking/showresult/ShowResultScreen;->primaryButtonText:I

    return v0
.end method

.method public final getSecondaryButtonText()I
    .locals 1

    .line 13
    iget v0, p0, Lcom/squareup/banklinking/showresult/ShowResultScreen;->secondaryButtonText:I

    return v0
.end method

.method public final getTitle()I
    .locals 1

    .line 10
    iget v0, p0, Lcom/squareup/banklinking/showresult/ShowResultScreen;->title:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget v0, p0, Lcom/squareup/banklinking/showresult/ShowResultScreen;->drawable:I

    invoke-static {v0}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/banklinking/showresult/ShowResultScreen;->title:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/banklinking/showresult/ShowResultScreen;->message:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/banklinking/showresult/ShowResultScreen;->primaryButtonText:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/banklinking/showresult/ShowResultScreen;->secondaryButtonText:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/banklinking/showresult/ShowResultScreen;->onPrimaryButtonClicked:Lkotlin/jvm/functions/Function0;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/banklinking/showresult/ShowResultScreen;->onSecondaryButtonClicked:Lkotlin/jvm/functions/Function0;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ShowResultScreen(drawable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/banklinking/showresult/ShowResultScreen;->drawable:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/banklinking/showresult/ShowResultScreen;->title:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", message="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/banklinking/showresult/ShowResultScreen;->message:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", primaryButtonText="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/banklinking/showresult/ShowResultScreen;->primaryButtonText:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", secondaryButtonText="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/banklinking/showresult/ShowResultScreen;->secondaryButtonText:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", onPrimaryButtonClicked="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/banklinking/showresult/ShowResultScreen;->onPrimaryButtonClicked:Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onSecondaryButtonClicked="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/banklinking/showresult/ShowResultScreen;->onSecondaryButtonClicked:Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
