.class public final Lcom/squareup/banklinking/showresult/RealShowResultWorkflow;
.super Lcom/squareup/workflow/StatelessWorkflow;
.source "RealShowResultWorkflow.kt"

# interfaces
.implements Lcom/squareup/banklinking/showresult/ShowResultWorkflow;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatelessWorkflow<",
        "Lcom/squareup/banklinking/showresult/ShowResultProps;",
        "Lkotlin/Unit;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/banklinking/showresult/ShowResultWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealShowResultWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealShowResultWorkflow.kt\ncom/squareup/banklinking/showresult/RealShowResultWorkflow\n+ 2 WorkflowAction.kt\ncom/squareup/workflow/WorkflowActionKt\n+ 3 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,101:1\n179#2,3:102\n199#2,4:105\n149#3,5:109\n*E\n*S KotlinDebug\n*F\n+ 1 RealShowResultWorkflow.kt\ncom/squareup/banklinking/showresult/RealShowResultWorkflow\n*L\n84#1,3:102\n84#1,4:105\n98#1,5:109\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0005\u0018\u00002\u00020\u000126\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012&\u0012$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t0\u0002B\u000f\u0008\u0007\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ.\u0010\r\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00082\u0018\u0010\u000e\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\u00040\u00100\u000fH\u0002JF\u0010\u0012\u001a$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t2\u0006\u0010\u0013\u001a\u00020\u00032\u0012\u0010\u0014\u001a\u000e\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\u00040\u0015H\u0016J`\u0010\u0016\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00082\u0018\u0010\u000e\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\u00040\u00100\u000f2\u0008\u0008\u0001\u0010\u0017\u001a\u00020\u00182\u0008\u0008\u0003\u0010\u0019\u001a\u00020\u00182\u0008\u0008\u0001\u0010\u001a\u001a\u00020\u00182\u0008\u0008\u0001\u0010\u001b\u001a\u00020\u00182\u0008\u0008\u0003\u0010\u001c\u001a\u00020\u0018H\u0002R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/banklinking/showresult/RealShowResultWorkflow;",
        "Lcom/squareup/banklinking/showresult/ShowResultWorkflow;",
        "Lcom/squareup/workflow/StatelessWorkflow;",
        "Lcom/squareup/banklinking/showresult/ShowResultProps;",
        "",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "browserLauncher",
        "Lcom/squareup/util/BrowserLauncher;",
        "(Lcom/squareup/util/BrowserLauncher;)V",
        "failureResultScreen",
        "sink",
        "Lcom/squareup/workflow/Sink;",
        "Lcom/squareup/workflow/WorkflowAction;",
        "",
        "render",
        "props",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "showResultScreen",
        "drawable",
        "",
        "title",
        "message",
        "primaryButtonText",
        "secondaryButtonText",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final browserLauncher:Lcom/squareup/util/BrowserLauncher;


# direct methods
.method public constructor <init>(Lcom/squareup/util/BrowserLauncher;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "browserLauncher"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-direct {p0}, Lcom/squareup/workflow/StatelessWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/banklinking/showresult/RealShowResultWorkflow;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    return-void
.end method

.method public static final synthetic access$getBrowserLauncher$p(Lcom/squareup/banklinking/showresult/RealShowResultWorkflow;)Lcom/squareup/util/BrowserLauncher;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/squareup/banklinking/showresult/RealShowResultWorkflow;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    return-object p0
.end method

.method private final failureResultScreen(Lcom/squareup/workflow/Sink;)Lcom/squareup/workflow/legacy/Screen;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Sink<",
            "-",
            "Lcom/squareup/workflow/WorkflowAction;",
            ">;)",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;"
        }
    .end annotation

    .line 69
    sget v2, Lcom/squareup/vectoricons/R$drawable;->ui_triangle_warning_80:I

    .line 70
    sget v4, Lcom/squareup/banklinking/impl/R$string;->bank_account_linking_failed_message:I

    .line 71
    sget v5, Lcom/squareup/common/strings/R$string;->okay:I

    .line 72
    sget v6, Lcom/squareup/banklinking/impl/R$string;->learn_more:I

    const/4 v3, 0x0

    const/4 v7, 0x4

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    .line 67
    invoke-static/range {v0 .. v8}, Lcom/squareup/banklinking/showresult/RealShowResultWorkflow;->showResultScreen$default(Lcom/squareup/banklinking/showresult/RealShowResultWorkflow;Lcom/squareup/workflow/Sink;IIIIIILjava/lang/Object;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    return-object p1
.end method

.method private final showResultScreen(Lcom/squareup/workflow/Sink;IIIII)Lcom/squareup/workflow/legacy/Screen;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Sink<",
            "-",
            "Lcom/squareup/workflow/WorkflowAction;",
            ">;IIIII)",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;"
        }
    .end annotation

    .line 105
    new-instance v0, Lcom/squareup/banklinking/showresult/RealShowResultWorkflow$showResultScreen$$inlined$action$1;

    const-string v1, ""

    invoke-direct {v0, v1}, Lcom/squareup/banklinking/showresult/RealShowResultWorkflow$showResultScreen$$inlined$action$1;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    .line 86
    new-instance v10, Lcom/squareup/banklinking/showresult/ShowResultScreen;

    .line 92
    new-instance v2, Lcom/squareup/banklinking/showresult/RealShowResultWorkflow$showResultScreen$1;

    move-object v3, p1

    invoke-direct {v2, p1, v0}, Lcom/squareup/banklinking/showresult/RealShowResultWorkflow$showResultScreen$1;-><init>(Lcom/squareup/workflow/Sink;Lcom/squareup/workflow/WorkflowAction;)V

    move-object v8, v2

    check-cast v8, Lkotlin/jvm/functions/Function0;

    .line 93
    new-instance v0, Lcom/squareup/banklinking/showresult/RealShowResultWorkflow$showResultScreen$2;

    move-object v11, p0

    invoke-direct {v0, p0}, Lcom/squareup/banklinking/showresult/RealShowResultWorkflow$showResultScreen$2;-><init>(Lcom/squareup/banklinking/showresult/RealShowResultWorkflow;)V

    move-object v9, v0

    check-cast v9, Lkotlin/jvm/functions/Function0;

    move-object v2, v10

    move v3, p2

    move v4, p3

    move/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    .line 86
    invoke-direct/range {v2 .. v9}, Lcom/squareup/banklinking/showresult/ShowResultScreen;-><init>(IIIIILkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    check-cast v10, Lcom/squareup/workflow/legacy/V2Screen;

    .line 110
    new-instance v0, Lcom/squareup/workflow/legacy/Screen;

    .line 111
    const-class v2, Lcom/squareup/banklinking/showresult/ShowResultScreen;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    .line 112
    sget-object v2, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v2}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v2

    .line 110
    invoke-direct {v0, v1, v10, v2}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object v0
.end method

.method static synthetic showResultScreen$default(Lcom/squareup/banklinking/showresult/RealShowResultWorkflow;Lcom/squareup/workflow/Sink;IIIIIILjava/lang/Object;)Lcom/squareup/workflow/legacy/Screen;
    .locals 9

    and-int/lit8 v0, p7, 0x4

    const/4 v1, -0x1

    if-eqz v0, :cond_0

    const/4 v5, -0x1

    goto :goto_0

    :cond_0
    move v5, p3

    :goto_0
    and-int/lit8 v0, p7, 0x20

    if-eqz v0, :cond_1

    const/4 v8, -0x1

    goto :goto_1

    :cond_1
    move v8, p6

    :goto_1
    move-object v2, p0

    move-object v3, p1

    move v4, p2

    move v6, p4

    move v7, p5

    .line 82
    invoke-direct/range {v2 .. v8}, Lcom/squareup/banklinking/showresult/RealShowResultWorkflow;->showResultScreen(Lcom/squareup/workflow/Sink;IIIII)Lcom/squareup/workflow/legacy/Screen;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic render(Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 27
    check-cast p1, Lcom/squareup/banklinking/showresult/ShowResultProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/banklinking/showresult/RealShowResultWorkflow;->render(Lcom/squareup/banklinking/showresult/ShowResultProps;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/banklinking/showresult/ShowResultProps;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/banklinking/showresult/ShowResultProps;",
            "Lcom/squareup/workflow/RenderContext;",
            ")",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-virtual {p1}, Lcom/squareup/banklinking/showresult/ShowResultProps;->getLinkBankAccountState()Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;

    move-result-object v0

    sget-object v1, Lcom/squareup/banklinking/showresult/RealShowResultWorkflow$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {v0}, Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x2

    const/4 v2, 0x1

    if-eq v0, v2, :cond_1

    if-ne v0, v1, :cond_0

    .line 62
    invoke-interface {p2}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/banklinking/showresult/RealShowResultWorkflow;->failureResultScreen(Lcom/squareup/workflow/Sink;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto/16 :goto_0

    .line 63
    :cond_0
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Unexpected linkBankAccountState: "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/squareup/banklinking/showresult/ShowResultProps;->getLinkBankAccountState()Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2

    .line 37
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/banklinking/showresult/ShowResultProps;->getVerificationState()Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    move-result-object v0

    if-eqz v0, :cond_5

    sget-object v3, Lcom/squareup/banklinking/showresult/RealShowResultWorkflow$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;->ordinal()I

    move-result v0

    aget v0, v3, v0

    if-eq v0, v2, :cond_4

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-ne v0, v1, :cond_5

    .line 59
    invoke-interface {p2}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/banklinking/showresult/RealShowResultWorkflow;->failureResultScreen(Lcom/squareup/workflow/Sink;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto :goto_0

    .line 53
    :cond_2
    invoke-interface {p2}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object v1

    .line 54
    sget v2, Lcom/squareup/vectoricons/R$drawable;->circle_circle_check_80:I

    .line 55
    sget v3, Lcom/squareup/banklinking/impl/R$string;->bank_account_verification_in_progress:I

    .line 56
    sget v4, Lcom/squareup/banklinking/impl/R$string;->bank_account_verification_in_progress_message:I

    .line 57
    sget v5, Lcom/squareup/common/strings/R$string;->done:I

    const/4 v6, 0x0

    const/16 v7, 0x20

    const/4 v8, 0x0

    move-object v0, p0

    .line 52
    invoke-static/range {v0 .. v8}, Lcom/squareup/banklinking/showresult/RealShowResultWorkflow;->showResultScreen$default(Lcom/squareup/banklinking/showresult/RealShowResultWorkflow;Lcom/squareup/workflow/Sink;IIIIIILjava/lang/Object;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto :goto_0

    .line 46
    :cond_3
    invoke-interface {p2}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object v1

    .line 47
    sget v2, Lcom/squareup/vectoricons/R$drawable;->circle_circle_check_80:I

    .line 48
    sget v3, Lcom/squareup/banklinking/impl/R$string;->bank_account_verified:I

    .line 49
    sget v4, Lcom/squareup/banklinking/impl/R$string;->bank_account_verified_message:I

    .line 50
    sget v5, Lcom/squareup/common/strings/R$string;->done:I

    const/4 v6, 0x0

    const/16 v7, 0x20

    const/4 v8, 0x0

    move-object v0, p0

    .line 45
    invoke-static/range {v0 .. v8}, Lcom/squareup/banklinking/showresult/RealShowResultWorkflow;->showResultScreen$default(Lcom/squareup/banklinking/showresult/RealShowResultWorkflow;Lcom/squareup/workflow/Sink;IIIIIILjava/lang/Object;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto :goto_0

    .line 39
    :cond_4
    invoke-interface {p2}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object v1

    .line 40
    sget v2, Lcom/squareup/vectoricons/R$drawable;->documents_envelope_80:I

    .line 41
    sget v3, Lcom/squareup/banklinking/impl/R$string;->check_your_inbox:I

    .line 42
    sget v4, Lcom/squareup/banklinking/impl/R$string;->check_your_inbox_message:I

    .line 43
    sget v5, Lcom/squareup/common/strings/R$string;->done:I

    const/4 v6, 0x0

    const/16 v7, 0x20

    const/4 v8, 0x0

    move-object v0, p0

    .line 38
    invoke-static/range {v0 .. v8}, Lcom/squareup/banklinking/showresult/RealShowResultWorkflow;->showResultScreen$default(Lcom/squareup/banklinking/showresult/RealShowResultWorkflow;Lcom/squareup/workflow/Sink;IIIIIILjava/lang/Object;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    .line 64
    :goto_0
    sget-object p2, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1

    .line 60
    :cond_5
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Unexpected verificationState: "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/squareup/banklinking/showresult/ShowResultProps;->getVerificationState()Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2
.end method
