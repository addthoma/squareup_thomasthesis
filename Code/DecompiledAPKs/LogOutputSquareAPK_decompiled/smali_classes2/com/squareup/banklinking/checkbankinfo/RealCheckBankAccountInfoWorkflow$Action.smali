.class public abstract Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Action;
.super Ljava/lang/Object;
.source "RealCheckBankAccountInfoWorkflow.kt"

# interfaces
.implements Lcom/squareup/workflow/WorkflowAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Action"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Action$LinkBank;,
        Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Action$GetDirectDebitInfo;,
        Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Action$CompleteBankLinking;,
        Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Action$ShowWarning;,
        Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Action$DismissWarning;,
        Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Action$CancelBankLinking;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState;",
        "Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoOutput;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0006\u0008\t\n\u000b\u000c\rB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0004J\u0018\u0010\u0005\u001a\u00020\u0006*\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0007H\u0016\u0082\u0001\u0006\u000e\u000f\u0010\u0011\u0012\u0013\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Action;",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState;",
        "Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoOutput;",
        "()V",
        "apply",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "CancelBankLinking",
        "CompleteBankLinking",
        "DismissWarning",
        "GetDirectDebitInfo",
        "LinkBank",
        "ShowWarning",
        "Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Action$LinkBank;",
        "Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Action$GetDirectDebitInfo;",
        "Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Action$CompleteBankLinking;",
        "Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Action$ShowWarning;",
        "Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Action$DismissWarning;",
        "Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Action$CancelBankLinking;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 67
    invoke-direct {p0}, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Action;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoOutput;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState;",
            ">;)",
            "Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoOutput;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Implement Updater.apply"
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    invoke-static {p0, p1}, Lcom/squareup/workflow/WorkflowAction$DefaultImpls;->apply(Lcom/squareup/workflow/WorkflowAction;Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoOutput;

    return-object p1
.end method

.method public bridge synthetic apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;
    .locals 0

    .line 67
    invoke-virtual {p0, p1}, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Action;->apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoOutput;

    move-result-object p1

    return-object p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState;",
            "-",
            "Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    instance-of v0, p0, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Action$LinkBank;

    if-eqz v0, :cond_0

    .line 86
    new-instance v0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState$LinkingBank;

    move-object v1, p0

    check-cast v1, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Action$LinkBank;

    invoke-virtual {v1}, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Action$LinkBank;->getBankAccountDetails()Lcom/squareup/protos/client/bankaccount/BankAccountDetails;

    move-result-object v2

    invoke-virtual {v1}, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Action$LinkBank;->getIdempotenceKey()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState$LinkingBank;-><init>(Lcom/squareup/protos/client/bankaccount/BankAccountDetails;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    goto :goto_0

    .line 88
    :cond_0
    instance-of v0, p0, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Action$GetDirectDebitInfo;

    if-eqz v0, :cond_1

    .line 89
    new-instance v0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState$GettingDirectDebitInfo;

    move-object v1, p0

    check-cast v1, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Action$GetDirectDebitInfo;

    invoke-virtual {v1}, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Action$GetDirectDebitInfo;->getBankAccountDetails()Lcom/squareup/protos/client/bankaccount/BankAccountDetails;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState$GettingDirectDebitInfo;-><init>(Lcom/squareup/protos/client/bankaccount/BankAccountDetails;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    goto :goto_0

    .line 91
    :cond_1
    instance-of v0, p0, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Action$CompleteBankLinking;

    if-eqz v0, :cond_2

    .line 92
    new-instance v0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoOutput$CheckBankAccountInfoCompleted;

    .line 93
    move-object v1, p0

    check-cast v1, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Action$CompleteBankLinking;

    invoke-virtual {v1}, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Action$CompleteBankLinking;->getLinkBankAccountState()Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;

    move-result-object v2

    invoke-virtual {v1}, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Action$CompleteBankLinking;->getVerificationState()Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    move-result-object v1

    .line 92
    invoke-direct {v0, v2, v1}, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoOutput$CheckBankAccountInfoCompleted;-><init>(Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;)V

    .line 91
    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    goto :goto_0

    .line 96
    :cond_2
    instance-of v0, p0, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Action$ShowWarning;

    if-eqz v0, :cond_3

    .line 97
    new-instance v0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState$InvalidBankInfo;

    move-object v1, p0

    check-cast v1, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Action$ShowWarning;

    invoke-virtual {v1}, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Action$ShowWarning;->getWarning()Lcom/squareup/widgets/warning/Warning;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState$InvalidBankInfo;-><init>(Lcom/squareup/widgets/warning/Warning;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    goto :goto_0

    .line 99
    :cond_3
    sget-object v0, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Action$DismissWarning;->INSTANCE:Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Action$DismissWarning;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 100
    sget-object v0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState$PrepareToLinkBank;->INSTANCE:Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState$PrepareToLinkBank;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    goto :goto_0

    .line 102
    :cond_4
    sget-object v0, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Action$CancelBankLinking;->INSTANCE:Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Action$CancelBankLinking;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object v0, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoOutput$CheckBankAccountInfoCanceled;->INSTANCE:Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoOutput$CheckBankAccountInfoCanceled;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_5
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
