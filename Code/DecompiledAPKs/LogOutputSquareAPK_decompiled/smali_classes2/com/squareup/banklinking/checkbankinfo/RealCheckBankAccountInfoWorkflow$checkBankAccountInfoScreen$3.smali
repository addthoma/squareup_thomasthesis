.class final Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$checkBankAccountInfoScreen$3;
.super Lkotlin/jvm/internal/Lambda;
.source "RealCheckBankAccountInfoWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;->checkBankAccountInfoScreen(Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoProps;Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState;Lcom/squareup/workflow/Sink;)Lcom/squareup/workflow/legacy/Screen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/protos/client/bankaccount/BankAccountDetails;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/protos/client/bankaccount/BankAccountDetails;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $sink:Lcom/squareup/workflow/Sink;

.field final synthetic this$0:Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;Lcom/squareup/workflow/Sink;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$checkBankAccountInfoScreen$3;->this$0:Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;

    iput-object p2, p0, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$checkBankAccountInfoScreen$3;->$sink:Lcom/squareup/workflow/Sink;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 53
    check-cast p1, Lcom/squareup/protos/client/bankaccount/BankAccountDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$checkBankAccountInfoScreen$3;->invoke(Lcom/squareup/protos/client/bankaccount/BankAccountDetails;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/protos/client/bankaccount/BankAccountDetails;)V
    .locals 4

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 174
    iget-object v0, p0, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$checkBankAccountInfoScreen$3;->$sink:Lcom/squareup/workflow/Sink;

    iget-object v1, p0, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$checkBankAccountInfoScreen$3;->this$0:Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;

    invoke-static {v1}, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;->access$getCountryCode$p(Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;)Lcom/squareup/CountryCode;

    move-result-object v1

    sget-object v2, Lcom/squareup/CountryCode;->GB:Lcom/squareup/CountryCode;

    if-ne v1, v2, :cond_0

    new-instance v1, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Action$GetDirectDebitInfo;

    invoke-direct {v1, p1}, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Action$GetDirectDebitInfo;-><init>(Lcom/squareup/protos/client/bankaccount/BankAccountDetails;)V

    goto :goto_0

    :cond_0
    new-instance v1, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Action$LinkBank;

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-direct {v1, p1, v3, v2, v3}, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Action$LinkBank;-><init>(Lcom/squareup/protos/client/bankaccount/BankAccountDetails;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    :goto_0
    check-cast v1, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Action;

    invoke-interface {v0, v1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    return-void
.end method
