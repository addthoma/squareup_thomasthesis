.class final Lcom/squareup/banklinking/RealBankAccountSettings$onLinkBankAccount$1;
.super Ljava/lang/Object;
.source "RealBankAccountSettings.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/banklinking/RealBankAccountSettings;->onLinkBankAccount(Lcom/squareup/protos/client/bankaccount/BankAccountDetails;ZLjava/lang/String;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/SingleSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lcom/squareup/banklinking/BankAccountSettings$State;",
        "it",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $isOnboarding:Z

.field final synthetic this$0:Lcom/squareup/banklinking/RealBankAccountSettings;


# direct methods
.method constructor <init>(Lcom/squareup/banklinking/RealBankAccountSettings;Z)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/banklinking/RealBankAccountSettings$onLinkBankAccount$1;->this$0:Lcom/squareup/banklinking/RealBankAccountSettings;

    iput-boolean p2, p0, Lcom/squareup/banklinking/RealBankAccountSettings$onLinkBankAccount$1;->$isOnboarding:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse;",
            ">;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/banklinking/BankAccountSettings$State;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 181
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/squareup/banklinking/RealBankAccountSettings$onLinkBankAccount$1;->this$0:Lcom/squareup/banklinking/RealBankAccountSettings;

    iget-boolean v0, p0, Lcom/squareup/banklinking/RealBankAccountSettings$onLinkBankAccount$1;->$isOnboarding:Z

    invoke-static {p1, v0}, Lcom/squareup/banklinking/RealBankAccountSettings;->access$onLinkBankAccountSuccess(Lcom/squareup/banklinking/RealBankAccountSettings;Z)Lio/reactivex/Single;

    move-result-object p1

    goto :goto_0

    .line 182
    :cond_0
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/banklinking/RealBankAccountSettings$onLinkBankAccount$1;->this$0:Lcom/squareup/banklinking/RealBankAccountSettings;

    iget-boolean v1, p0, Lcom/squareup/banklinking/RealBankAccountSettings$onLinkBankAccount$1;->$isOnboarding:Z

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    invoke-static {v0, v1, p1}, Lcom/squareup/banklinking/RealBankAccountSettings;->access$onLinkBankAccountFailure(Lcom/squareup/banklinking/RealBankAccountSettings;ZLcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lio/reactivex/Single;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 60
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/banklinking/RealBankAccountSettings$onLinkBankAccount$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
