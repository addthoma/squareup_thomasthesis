.class public Lcom/squareup/cnp/CnpFeesMessageHelper;
.super Ljava/lang/Object;
.source "CnpFeesMessageHelper.java"


# instance fields
.field private final context:Landroid/content/Context;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final linkSpanDataHelper:Lcom/squareup/cnp/LinkSpanDataHelper;

.field private final rateFormatter:Lcom/squareup/text/RateFormatter;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method constructor <init>(Landroid/app/Application;Lcom/squareup/settings/server/Features;Lcom/squareup/text/RateFormatter;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/cnp/LinkSpanDataHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/squareup/cnp/CnpFeesMessageHelper;->context:Landroid/content/Context;

    .line 36
    iput-object p2, p0, Lcom/squareup/cnp/CnpFeesMessageHelper;->features:Lcom/squareup/settings/server/Features;

    .line 37
    iput-object p3, p0, Lcom/squareup/cnp/CnpFeesMessageHelper;->rateFormatter:Lcom/squareup/text/RateFormatter;

    .line 38
    iput-object p4, p0, Lcom/squareup/cnp/CnpFeesMessageHelper;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 39
    iput-object p5, p0, Lcom/squareup/cnp/CnpFeesMessageHelper;->linkSpanDataHelper:Lcom/squareup/cnp/LinkSpanDataHelper;

    return-void
.end method

.method private messageWithRate(IILcom/squareup/server/account/protos/ProcessingFee;I)Ljava/lang/CharSequence;
    .locals 1

    .line 107
    iget-object v0, p0, Lcom/squareup/cnp/CnpFeesMessageHelper;->linkSpanDataHelper:Lcom/squareup/cnp/LinkSpanDataHelper;

    .line 108
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/cnp/CnpFeesMessageHelper;->messageWithRateData(IILcom/squareup/server/account/protos/ProcessingFee;I)Lcom/squareup/cnp/LinkSpanData;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/cnp/CnpFeesMessageHelper;->context:Landroid/content/Context;

    .line 107
    invoke-virtual {v0, p1, p2}, Lcom/squareup/cnp/LinkSpanDataHelper;->formatLinkSpanData(Lcom/squareup/cnp/LinkSpanData;Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method private messageWithRateData(IILcom/squareup/server/account/protos/ProcessingFee;I)Lcom/squareup/cnp/LinkSpanData;
    .locals 3

    .line 122
    iget-object v0, p0, Lcom/squareup/cnp/CnpFeesMessageHelper;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/PaymentSettings;->getCpFee()Lcom/squareup/server/account/protos/ProcessingFee;

    move-result-object v0

    .line 123
    iget-object v1, p3, Lcom/squareup/server/account/protos/ProcessingFee;->discount_basis_points:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v0, v0, Lcom/squareup/server/account/protos/ProcessingFee;->discount_basis_points:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-le v1, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    move p1, p2

    .line 126
    :cond_1
    iget-object p2, p0, Lcom/squareup/cnp/CnpFeesMessageHelper;->rateFormatter:Lcom/squareup/text/RateFormatter;

    iget-object v0, p3, Lcom/squareup/server/account/protos/ProcessingFee;->interchange_cents:Ljava/lang/Integer;

    .line 127
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    iget-object v2, p0, Lcom/squareup/cnp/CnpFeesMessageHelper;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v2}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/settings/server/UserSettings;->getCurrency()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    iget-object p3, p3, Lcom/squareup/server/account/protos/ProcessingFee;->discount_basis_points:Ljava/lang/Integer;

    .line 128
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result p3

    invoke-static {p3}, Lcom/squareup/util/Percentage;->fromBasisPoints(I)Lcom/squareup/util/Percentage;

    move-result-object p3

    .line 126
    invoke-virtual {p2, v0, p3}, Lcom/squareup/text/RateFormatter;->format(Lcom/squareup/protos/common/Money;Lcom/squareup/util/Percentage;)Ljava/lang/CharSequence;

    move-result-object p2

    .line 129
    new-instance p3, Lcom/squareup/cnp/LinkSpanData;

    new-instance v0, Lcom/squareup/cnp/LinkSpanData$PatternData;

    sget v1, Lcom/squareup/checkout/R$string;->pay_card_cnp_url:I

    const-string v2, "help_center"

    invoke-direct {v0, p1, v2, v1, p4}, Lcom/squareup/cnp/LinkSpanData$PatternData;-><init>(ILjava/lang/String;II)V

    new-instance p1, Lcom/squareup/cnp/LinkSpanData$PutData;

    const-string p4, "cnp_rate"

    invoke-direct {p1, p4, p2}, Lcom/squareup/cnp/LinkSpanData$PutData;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;)V

    invoke-direct {p3, v0, p1}, Lcom/squareup/cnp/LinkSpanData;-><init>(Lcom/squareup/cnp/LinkSpanData$PatternData;Lcom/squareup/cnp/LinkSpanData$PutData;)V

    return-object p3
.end method

.method private messageWithoutRate(II)Ljava/lang/CharSequence;
    .locals 1

    .line 99
    iget-object v0, p0, Lcom/squareup/cnp/CnpFeesMessageHelper;->linkSpanDataHelper:Lcom/squareup/cnp/LinkSpanDataHelper;

    .line 100
    invoke-direct {p0, p1, p2}, Lcom/squareup/cnp/CnpFeesMessageHelper;->messageWithoutRateData(II)Lcom/squareup/cnp/LinkSpanData;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/cnp/CnpFeesMessageHelper;->context:Landroid/content/Context;

    .line 99
    invoke-virtual {v0, p1, p2}, Lcom/squareup/cnp/LinkSpanDataHelper;->formatLinkSpanData(Lcom/squareup/cnp/LinkSpanData;Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method private messageWithoutRateData(II)Lcom/squareup/cnp/LinkSpanData;
    .locals 4

    .line 114
    new-instance v0, Lcom/squareup/cnp/LinkSpanData;

    new-instance v1, Lcom/squareup/cnp/LinkSpanData$PatternData;

    sget v2, Lcom/squareup/checkout/R$string;->pay_card_cnp_url:I

    const-string v3, "help_center"

    invoke-direct {v1, p1, v3, v2, p2}, Lcom/squareup/cnp/LinkSpanData$PatternData;-><init>(ILjava/lang/String;II)V

    const/4 p1, 0x0

    invoke-direct {v0, v1, p1}, Lcom/squareup/cnp/LinkSpanData;-><init>(Lcom/squareup/cnp/LinkSpanData$PatternData;Lcom/squareup/cnp/LinkSpanData$PutData;)V

    return-object v0
.end method


# virtual methods
.method public cardOnFileMessage()Ljava/lang/CharSequence;
    .locals 4

    .line 57
    iget-object v0, p0, Lcom/squareup/cnp/CnpFeesMessageHelper;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v0

    .line 58
    iget-object v1, p0, Lcom/squareup/cnp/CnpFeesMessageHelper;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->FEE_TRANSPARENCY:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/squareup/settings/server/PaymentSettings;->hasCofFee()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-nez v1, :cond_1

    .line 60
    sget v0, Lcom/squareup/checkout/R$string;->pay_card_card_on_file_hint:I

    sget v1, Lcom/squareup/checkout/R$string;->square_support:I

    invoke-direct {p0, v0, v1}, Lcom/squareup/cnp/CnpFeesMessageHelper;->messageWithoutRate(II)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0

    .line 62
    :cond_1
    invoke-virtual {v0}, Lcom/squareup/settings/server/PaymentSettings;->getCofFee()Lcom/squareup/server/account/protos/ProcessingFee;

    move-result-object v0

    .line 63
    sget v1, Lcom/squareup/checkout/R$string;->pay_card_card_on_file_hint_with_rate:I

    sget v2, Lcom/squareup/checkout/R$string;->pay_card_card_on_file_hint_with_rate_higher:I

    sget v3, Lcom/squareup/checkout/R$string;->square_support:I

    invoke-direct {p0, v1, v2, v0, v3}, Lcom/squareup/cnp/CnpFeesMessageHelper;->messageWithRate(IILcom/squareup/server/account/protos/ProcessingFee;I)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public invoiceAutomaticPaymentOptInMessage()Ljava/lang/CharSequence;
    .locals 4

    .line 68
    iget-object v0, p0, Lcom/squareup/cnp/CnpFeesMessageHelper;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v0

    .line 69
    iget-object v1, p0, Lcom/squareup/cnp/CnpFeesMessageHelper;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->FEE_TRANSPARENCY:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/squareup/settings/server/PaymentSettings;->hasCofFee()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-nez v1, :cond_1

    .line 71
    sget v0, Lcom/squareup/checkout/R$string;->invoice_automatic_payment_fee_message:I

    sget v1, Lcom/squareup/common/strings/R$string;->learn_more:I

    invoke-direct {p0, v0, v1}, Lcom/squareup/cnp/CnpFeesMessageHelper;->messageWithoutRate(II)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0

    .line 74
    :cond_1
    invoke-virtual {v0}, Lcom/squareup/settings/server/PaymentSettings;->getCofFee()Lcom/squareup/server/account/protos/ProcessingFee;

    move-result-object v0

    .line 77
    sget v1, Lcom/squareup/checkout/R$string;->invoice_automatic_payment_fee_message_with_rate:I

    sget v2, Lcom/squareup/checkout/R$string;->invoice_automatic_payment_fee_message_with_rate:I

    sget v3, Lcom/squareup/common/strings/R$string;->learn_more:I

    invoke-direct {p0, v1, v2, v0, v3}, Lcom/squareup/cnp/CnpFeesMessageHelper;->messageWithRate(IILcom/squareup/server/account/protos/ProcessingFee;I)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public invoiceAutomaticPaymentOptInMessageV2()Ljava/lang/CharSequence;
    .locals 4

    .line 83
    iget-object v0, p0, Lcom/squareup/cnp/CnpFeesMessageHelper;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v0

    .line 84
    invoke-virtual {v0}, Lcom/squareup/settings/server/PaymentSettings;->hasCofFee()Z

    move-result v1

    if-nez v1, :cond_0

    .line 86
    sget v0, Lcom/squareup/checkout/R$string;->invoice_automatic_payment_fee_message_v2:I

    sget v1, Lcom/squareup/common/strings/R$string;->learn_more:I

    invoke-direct {p0, v0, v1}, Lcom/squareup/cnp/CnpFeesMessageHelper;->messageWithoutRate(II)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0

    .line 89
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/settings/server/PaymentSettings;->getCofFee()Lcom/squareup/server/account/protos/ProcessingFee;

    move-result-object v0

    .line 92
    sget v1, Lcom/squareup/checkout/R$string;->invoice_automatic_payment_fee_message_with_rate_v2:I

    sget v2, Lcom/squareup/checkout/R$string;->invoice_automatic_payment_fee_message_with_rate_v2:I

    sget v3, Lcom/squareup/common/strings/R$string;->learn_more:I

    invoke-direct {p0, v1, v2, v0, v3}, Lcom/squareup/cnp/CnpFeesMessageHelper;->messageWithRate(IILcom/squareup/server/account/protos/ProcessingFee;I)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public manuallyEnteredCreditCardMessageData()Lcom/squareup/cnp/LinkSpanData;
    .locals 4

    .line 43
    iget-object v0, p0, Lcom/squareup/cnp/CnpFeesMessageHelper;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v0

    .line 44
    iget-object v1, p0, Lcom/squareup/cnp/CnpFeesMessageHelper;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->FEE_TRANSPARENCY:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/squareup/cnp/CnpFeesMessageHelper;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->FEE_TRANSPARENCY_CNP:Lcom/squareup/settings/server/Features$Feature;

    .line 45
    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 46
    invoke-virtual {v0}, Lcom/squareup/settings/server/PaymentSettings;->hasCpFee()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 47
    invoke-virtual {v0}, Lcom/squareup/settings/server/PaymentSettings;->hasCnpFee()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    .line 49
    sget v0, Lcom/squareup/checkout/R$string;->pay_card_cnp_hint:I

    sget v1, Lcom/squareup/checkout/R$string;->square_support:I

    invoke-direct {p0, v0, v1}, Lcom/squareup/cnp/CnpFeesMessageHelper;->messageWithoutRateData(II)Lcom/squareup/cnp/LinkSpanData;

    move-result-object v0

    return-object v0

    .line 51
    :cond_1
    sget v0, Lcom/squareup/checkout/R$string;->pay_card_cnp_hint_with_rate:I

    sget v1, Lcom/squareup/checkout/R$string;->pay_card_cnp_hint_with_rate_higher:I

    iget-object v2, p0, Lcom/squareup/cnp/CnpFeesMessageHelper;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 52
    invoke-virtual {v2}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/settings/server/PaymentSettings;->getCnpFee()Lcom/squareup/server/account/protos/ProcessingFee;

    move-result-object v2

    sget v3, Lcom/squareup/checkout/R$string;->square_support:I

    .line 51
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/squareup/cnp/CnpFeesMessageHelper;->messageWithRateData(IILcom/squareup/server/account/protos/ProcessingFee;I)Lcom/squareup/cnp/LinkSpanData;

    move-result-object v0

    return-object v0
.end method
