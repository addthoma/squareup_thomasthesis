.class public final Lcom/squareup/cnp/CnpFeesMessageHelper_Factory;
.super Ljava/lang/Object;
.source "CnpFeesMessageHelper_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cnp/CnpFeesMessageHelper;",
        ">;"
    }
.end annotation


# instance fields
.field private final applicationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final linkSpanDataHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cnp/LinkSpanDataHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final rateFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/RateFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/RateFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cnp/LinkSpanDataHelper;",
            ">;)V"
        }
    .end annotation

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/squareup/cnp/CnpFeesMessageHelper_Factory;->applicationProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p2, p0, Lcom/squareup/cnp/CnpFeesMessageHelper_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p3, p0, Lcom/squareup/cnp/CnpFeesMessageHelper_Factory;->rateFormatterProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p4, p0, Lcom/squareup/cnp/CnpFeesMessageHelper_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p5, p0, Lcom/squareup/cnp/CnpFeesMessageHelper_Factory;->linkSpanDataHelperProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cnp/CnpFeesMessageHelper_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/RateFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cnp/LinkSpanDataHelper;",
            ">;)",
            "Lcom/squareup/cnp/CnpFeesMessageHelper_Factory;"
        }
    .end annotation

    .line 50
    new-instance v6, Lcom/squareup/cnp/CnpFeesMessageHelper_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/cnp/CnpFeesMessageHelper_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Landroid/app/Application;Lcom/squareup/settings/server/Features;Lcom/squareup/text/RateFormatter;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/cnp/LinkSpanDataHelper;)Lcom/squareup/cnp/CnpFeesMessageHelper;
    .locals 7

    .line 56
    new-instance v6, Lcom/squareup/cnp/CnpFeesMessageHelper;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/cnp/CnpFeesMessageHelper;-><init>(Landroid/app/Application;Lcom/squareup/settings/server/Features;Lcom/squareup/text/RateFormatter;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/cnp/LinkSpanDataHelper;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/cnp/CnpFeesMessageHelper;
    .locals 5

    .line 43
    iget-object v0, p0, Lcom/squareup/cnp/CnpFeesMessageHelper_Factory;->applicationProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    iget-object v1, p0, Lcom/squareup/cnp/CnpFeesMessageHelper_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/settings/server/Features;

    iget-object v2, p0, Lcom/squareup/cnp/CnpFeesMessageHelper_Factory;->rateFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/text/RateFormatter;

    iget-object v3, p0, Lcom/squareup/cnp/CnpFeesMessageHelper_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v4, p0, Lcom/squareup/cnp/CnpFeesMessageHelper_Factory;->linkSpanDataHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/cnp/LinkSpanDataHelper;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/cnp/CnpFeesMessageHelper_Factory;->newInstance(Landroid/app/Application;Lcom/squareup/settings/server/Features;Lcom/squareup/text/RateFormatter;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/cnp/LinkSpanDataHelper;)Lcom/squareup/cnp/CnpFeesMessageHelper;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/cnp/CnpFeesMessageHelper_Factory;->get()Lcom/squareup/cnp/CnpFeesMessageHelper;

    move-result-object v0

    return-object v0
.end method
