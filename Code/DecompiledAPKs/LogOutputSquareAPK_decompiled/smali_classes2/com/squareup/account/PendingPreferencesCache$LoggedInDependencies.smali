.class public Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;
.super Ljava/lang/Object;
.source "PendingPreferencesCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/account/PendingPreferencesCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LoggedInDependencies"
.end annotation


# instance fields
.field private canonicalStatus:Lcom/squareup/server/account/protos/AccountStatusResponse;

.field private overlaidStatus:Lcom/squareup/server/account/protos/AccountStatusResponse;

.field private final overlaidStatusLock:Ljava/lang/Object;

.field public preferencesInProgress:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/server/account/protos/PreferencesRequest;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public preferencesOnDeck:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/server/account/protos/PreferencesRequest;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public userId:Ljava/lang/String;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/server/account/protos/AccountStatusResponse;)V
    .locals 1

    .line 136
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 134
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;->overlaidStatusLock:Ljava/lang/Object;

    .line 137
    iput-object p1, p0, Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;->canonicalStatus:Lcom/squareup/server/account/protos/AccountStatusResponse;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;Lcom/squareup/server/account/protos/AccountStatusResponse;)V
    .locals 0

    .line 125
    invoke-direct {p0, p1}, Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;->setCanonicalStatus(Lcom/squareup/server/account/protos/AccountStatusResponse;)V

    return-void
.end method

.method private overlayPreferencesOnStatus(Lcom/squareup/server/account/protos/PreferencesRequest;)V
    .locals 3

    .line 194
    iget-object v0, p0, Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;->overlaidStatusLock:Ljava/lang/Object;

    monitor-enter v0

    .line 195
    :try_start_0
    iget-object v1, p0, Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;->overlaidStatus:Lcom/squareup/server/account/protos/AccountStatusResponse;

    invoke-static {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getPreferences(Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/server/account/protos/Preferences;

    move-result-object v1

    .line 196
    invoke-static {v1, p1}, Lcom/squareup/server/account/PreferenceUtilsKt;->overlayPreferences(Lcom/squareup/server/account/protos/Preferences;Lcom/squareup/server/account/protos/PreferencesRequest;)Lcom/squareup/server/account/protos/Preferences;

    move-result-object v1

    .line 197
    iget-object v2, p0, Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;->overlaidStatus:Lcom/squareup/server/account/protos/AccountStatusResponse;

    invoke-virtual {v2}, Lcom/squareup/server/account/protos/AccountStatusResponse;->newBuilder()Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v2

    .line 198
    invoke-virtual {v2, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->preferences(Lcom/squareup/server/account/protos/Preferences;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;->overlaidStatus:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v2, v2, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    .line 203
    invoke-virtual {v2}, Lcom/squareup/server/account/protos/FlagsAndPermissions;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object v2

    iget-object p1, p1, Lcom/squareup/server/account/protos/PreferencesRequest;->using_time_tracking:Ljava/lang/Boolean;

    .line 204
    invoke-virtual {v2, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->using_time_tracking(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;

    move-result-object p1

    .line 205
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions;

    move-result-object p1

    .line 203
    invoke-virtual {v1, p1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->features(Lcom/squareup/server/account/protos/FlagsAndPermissions;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object p1

    .line 206
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->build()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;->overlaidStatus:Lcom/squareup/server/account/protos/AccountStatusResponse;

    .line 207
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method private setCanonicalStatus(Lcom/squareup/server/account/protos/AccountStatusResponse;)V
    .locals 1

    .line 158
    iget-object v0, p0, Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;->overlaidStatusLock:Ljava/lang/Object;

    monitor-enter v0

    .line 159
    :try_start_0
    iput-object p1, p0, Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;->canonicalStatus:Lcom/squareup/server/account/protos/AccountStatusResponse;

    const/4 p1, 0x0

    .line 160
    iput-object p1, p0, Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;->overlaidStatus:Lcom/squareup/server/account/protos/AccountStatusResponse;

    .line 161
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method


# virtual methods
.method getOverlaidStatus()Lcom/squareup/server/account/protos/AccountStatusResponse;
    .locals 3

    .line 142
    iget-object v0, p0, Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;->overlaidStatusLock:Ljava/lang/Object;

    monitor-enter v0

    .line 143
    :try_start_0
    iget-object v1, p0, Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;->overlaidStatus:Lcom/squareup/server/account/protos/AccountStatusResponse;

    if-nez v1, :cond_1

    .line 144
    iget-object v1, p0, Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;->canonicalStatus:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iput-object v1, p0, Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;->overlaidStatus:Lcom/squareup/server/account/protos/AccountStatusResponse;

    .line 146
    iget-object v1, p0, Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;->preferencesInProgress:Lcom/squareup/settings/LocalSetting;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/squareup/settings/LocalSetting;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/server/account/protos/PreferencesRequest;

    if-eqz v1, :cond_0

    .line 147
    invoke-direct {p0, v1}, Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;->overlayPreferencesOnStatus(Lcom/squareup/server/account/protos/PreferencesRequest;)V

    .line 149
    :cond_0
    iget-object v1, p0, Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;->preferencesOnDeck:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v1, v2}, Lcom/squareup/settings/LocalSetting;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/server/account/protos/PreferencesRequest;

    if-eqz v1, :cond_1

    .line 150
    invoke-direct {p0, v1}, Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;->overlayPreferencesOnStatus(Lcom/squareup/server/account/protos/PreferencesRequest;)V

    .line 153
    :cond_1
    iget-object v1, p0, Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;->overlaidStatus:Lcom/squareup/server/account/protos/AccountStatusResponse;

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    .line 154
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method pendingPreferencesChanged()V
    .locals 2

    .line 184
    iget-object v0, p0, Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;->overlaidStatusLock:Ljava/lang/Object;

    monitor-enter v0

    const/4 v1, 0x0

    .line 185
    :try_start_0
    iput-object v1, p0, Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;->overlaidStatus:Lcom/squareup/server/account/protos/AccountStatusResponse;

    .line 186
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method setPreferences(Lcom/squareup/server/account/protos/PreferencesRequest;)V
    .locals 2

    .line 166
    iget-object v0, p0, Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;->preferencesOnDeck:Lcom/squareup/settings/LocalSetting;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/squareup/settings/LocalSetting;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/PreferencesRequest;

    if-eqz v0, :cond_0

    .line 168
    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/PreferencesRequest;->overlay(Lcom/squareup/server/account/protos/PreferencesRequest;)Lcom/squareup/server/account/protos/PreferencesRequest;

    move-result-object p1

    .line 170
    :cond_0
    iget-object v0, p0, Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;->preferencesOnDeck:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v0, p1}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    .line 172
    iget-object v0, p0, Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;->overlaidStatusLock:Ljava/lang/Object;

    monitor-enter v0

    .line 174
    :try_start_0
    iget-object v1, p0, Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;->overlaidStatus:Lcom/squareup/server/account/protos/AccountStatusResponse;

    if-eqz v1, :cond_1

    .line 175
    invoke-direct {p0, p1}, Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;->overlayPreferencesOnStatus(Lcom/squareup/server/account/protos/PreferencesRequest;)V

    .line 177
    :cond_1
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 180
    iget-object p1, p0, Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;->taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    new-instance v0, Lcom/squareup/account/UpdatePreferencesTask;

    invoke-direct {v0}, Lcom/squareup/account/UpdatePreferencesTask;-><init>()V

    invoke-interface {p1, v0}, Lcom/squareup/queue/retrofit/RetrofitQueue;->add(Ljava/lang/Object;)V

    return-void

    :catchall_0
    move-exception p1

    .line 177
    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method
