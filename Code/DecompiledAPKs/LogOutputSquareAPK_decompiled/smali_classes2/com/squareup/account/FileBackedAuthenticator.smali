.class public final Lcom/squareup/account/FileBackedAuthenticator;
.super Ljava/lang/Object;
.source "FileBackedAuthenticator.kt"

# interfaces
.implements Lcom/squareup/account/LegacyAuthenticator;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/dagger/AppScope;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/account/FileBackedAuthenticator$LogOutData;,
        Lcom/squareup/account/FileBackedAuthenticator$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nFileBackedAuthenticator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 FileBackedAuthenticator.kt\ncom/squareup/account/FileBackedAuthenticator\n*L\n1#1,210:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u008a\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0007\u0018\u0000 72\u00020\u0001:\u000278B7\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJ\u0012\u0010\u001f\u001a\u0004\u0018\u00010 2\u0006\u0010\u001b\u001a\u00020\u001cH\u0002J\n\u0010!\u001a\u0004\u0018\u00010 H\u0016J\u0008\u0010\"\u001a\u00020\u001aH\u0002J\u0010\u0010#\u001a\u00020\u001a2\u0006\u0010$\u001a\u00020%H\u0016J\u0010\u0010&\u001a\u00020\u001a2\u0006\u0010$\u001a\u00020%H\u0002J\u0018\u0010\'\u001a\u00020\u001a2\u0006\u0010(\u001a\u00020 2\u0006\u0010)\u001a\u00020*H\u0016J\u000e\u0010+\u001a\u0008\u0012\u0004\u0012\u00020\u00140,H\u0016J\u0010\u0010-\u001a\u00020\u001a2\u0006\u0010.\u001a\u00020/H\u0016J\u0008\u00100\u001a\u00020\u001aH\u0016J\u000e\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u001a0,H\u0016J\u0018\u00101\u001a\u00020\u001a2\u0006\u0010(\u001a\u00020 2\u0006\u00102\u001a\u00020*H\u0002J\u0010\u00103\u001a\u00020\u001a2\u0006\u00104\u001a\u00020*H\u0002J\u000c\u00105\u001a\u000206*\u00020%H\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\u00020\u00108VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000f\u0010\u0011R\u0016\u0010\u0012\u001a\n\u0012\u0004\u0012\u00020\u0014\u0018\u00010\u0013X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0015\u001a\u0010\u0012\u000c\u0012\n \u0018*\u0004\u0018\u00010\u00170\u00170\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0019\u001a\u0010\u0012\u000c\u0012\n \u0018*\u0004\u0018\u00010\u001a0\u001a0\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u001b\u001a\u0004\u0018\u00010\u001c8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u001d\u0010\u001e\u00a8\u00069"
    }
    d2 = {
        "Lcom/squareup/account/FileBackedAuthenticator;",
        "Lcom/squareup/account/LegacyAuthenticator;",
        "accountService",
        "Lcom/squareup/account/PersistentAccountService;",
        "accountStatusService",
        "Lcom/squareup/accountstatus/PersistentAccountStatusService;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "snapLogger",
        "Lcom/squareup/log/OhSnapLogger;",
        "logoutService",
        "Lcom/squareup/server/account/LogoutService;",
        "corruptQueueRecorder",
        "Lcom/squareup/queue/CorruptQueueRecorder;",
        "(Lcom/squareup/account/PersistentAccountService;Lcom/squareup/accountstatus/PersistentAccountStatusService;Lcom/squareup/analytics/Analytics;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/server/account/LogoutService;Lcom/squareup/queue/CorruptQueueRecorder;)V",
        "isLoggedIn",
        "",
        "()Z",
        "loggedInStatusRelay",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus;",
        "logoutRelay",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "Lcom/squareup/account/FileBackedAuthenticator$LogOutData;",
        "kotlin.jvm.PlatformType",
        "onLoggingOut",
        "",
        "user",
        "Lcom/squareup/server/account/protos/User;",
        "getUser",
        "()Lcom/squareup/server/account/protos/User;",
        "getCountryAsString",
        "",
        "getSessionIdPII",
        "load",
        "logOut",
        "reason",
        "Lcom/squareup/account/LogOutReason;",
        "logOutLocally",
        "loggedIn",
        "sessionToken",
        "status",
        "Lcom/squareup/server/account/protos/AccountStatusResponse;",
        "loggedInStatus",
        "Lio/reactivex/Observable;",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "onLoginResponse",
        "loginResponse",
        "onStatusResponse",
        "response",
        "toClearCacheReason",
        "Lcom/squareup/account/accountservice/ClearCacheReason;",
        "Companion",
        "LogOutData",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/account/FileBackedAuthenticator$Companion;

.field public static final EVENT_LOG_OUT:Ljava/lang/String; = "Log out"

.field public static final PARAM_HAD_USER:Ljava/lang/String; = "had user"

.field public static final PARAM_REASON:Ljava/lang/String; = "reason"


# instance fields
.field private final accountService:Lcom/squareup/account/PersistentAccountService;

.field private final accountStatusService:Lcom/squareup/accountstatus/PersistentAccountStatusService;

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final corruptQueueRecorder:Lcom/squareup/queue/CorruptQueueRecorder;

.field private loggedInStatusRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus;",
            ">;"
        }
    .end annotation
.end field

.field private final logoutRelay:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lcom/squareup/account/FileBackedAuthenticator$LogOutData;",
            ">;"
        }
    .end annotation
.end field

.field private final logoutService:Lcom/squareup/server/account/LogoutService;

.field private final onLoggingOut:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final snapLogger:Lcom/squareup/log/OhSnapLogger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/account/FileBackedAuthenticator$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/account/FileBackedAuthenticator$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/account/FileBackedAuthenticator;->Companion:Lcom/squareup/account/FileBackedAuthenticator$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/account/PersistentAccountService;Lcom/squareup/accountstatus/PersistentAccountStatusService;Lcom/squareup/analytics/Analytics;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/server/account/LogoutService;Lcom/squareup/queue/CorruptQueueRecorder;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "accountService"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountStatusService"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "snapLogger"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "logoutService"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "corruptQueueRecorder"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/account/FileBackedAuthenticator;->accountService:Lcom/squareup/account/PersistentAccountService;

    iput-object p2, p0, Lcom/squareup/account/FileBackedAuthenticator;->accountStatusService:Lcom/squareup/accountstatus/PersistentAccountStatusService;

    iput-object p3, p0, Lcom/squareup/account/FileBackedAuthenticator;->analytics:Lcom/squareup/analytics/Analytics;

    iput-object p4, p0, Lcom/squareup/account/FileBackedAuthenticator;->snapLogger:Lcom/squareup/log/OhSnapLogger;

    iput-object p5, p0, Lcom/squareup/account/FileBackedAuthenticator;->logoutService:Lcom/squareup/server/account/LogoutService;

    iput-object p6, p0, Lcom/squareup/account/FileBackedAuthenticator;->corruptQueueRecorder:Lcom/squareup/queue/CorruptQueueRecorder;

    .line 52
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    const-string p2, "PublishRelay.create<Unit>()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/account/FileBackedAuthenticator;->onLoggingOut:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 53
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    const-string p2, "PublishRelay.create<LogOutData>()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/account/FileBackedAuthenticator;->logoutRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-void
.end method

.method public static final synthetic access$getLogoutService$p(Lcom/squareup/account/FileBackedAuthenticator;)Lcom/squareup/server/account/LogoutService;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/squareup/account/FileBackedAuthenticator;->logoutService:Lcom/squareup/server/account/LogoutService;

    return-object p0
.end method

.method public static final synthetic access$getOnLoggingOut$p(Lcom/squareup/account/FileBackedAuthenticator;)Lcom/jakewharton/rxrelay2/PublishRelay;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/squareup/account/FileBackedAuthenticator;->onLoggingOut:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object p0
.end method

.method public static final synthetic access$logOutLocally(Lcom/squareup/account/FileBackedAuthenticator;Lcom/squareup/account/LogOutReason;)V
    .locals 0

    .line 38
    invoke-direct {p0, p1}, Lcom/squareup/account/FileBackedAuthenticator;->logOutLocally(Lcom/squareup/account/LogOutReason;)V

    return-void
.end method

.method public static final synthetic access$onStatusResponse(Lcom/squareup/account/FileBackedAuthenticator;Lcom/squareup/server/account/protos/AccountStatusResponse;)V
    .locals 0

    .line 38
    invoke-direct {p0, p1}, Lcom/squareup/account/FileBackedAuthenticator;->onStatusResponse(Lcom/squareup/server/account/protos/AccountStatusResponse;)V

    return-void
.end method

.method private final getCountryAsString(Lcom/squareup/server/account/protos/User;)Ljava/lang/String;
    .locals 0

    .line 189
    invoke-static {p1}, Lcom/squareup/server/account/UserUtils;->getCountryCodeOrNull(Lcom/squareup/server/account/protos/User;)Lcom/squareup/CountryCode;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/squareup/CountryCode;->name()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method private final getUser()Lcom/squareup/server/account/protos/User;
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/squareup/account/FileBackedAuthenticator;->accountStatusService:Lcom/squareup/accountstatus/PersistentAccountStatusService;

    invoke-virtual {v0}, Lcom/squareup/accountstatus/PersistentAccountStatusService;->getStatus()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->user:Lcom/squareup/server/account/protos/User;

    return-object v0
.end method

.method private final load()V
    .locals 4

    .line 94
    iget-object v0, p0, Lcom/squareup/account/FileBackedAuthenticator;->loggedInStatusRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_6

    .line 95
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/account/FileBackedAuthenticator;->loggedInStatusRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 98
    iget-object v0, p0, Lcom/squareup/account/FileBackedAuthenticator;->accountService:Lcom/squareup/account/PersistentAccountService;

    invoke-virtual {v0}, Lcom/squareup/account/PersistentAccountService;->init()V

    .line 99
    iget-object v0, p0, Lcom/squareup/account/FileBackedAuthenticator;->accountStatusService:Lcom/squareup/accountstatus/PersistentAccountStatusService;

    invoke-virtual {v0}, Lcom/squareup/accountstatus/PersistentAccountStatusService;->init()V

    .line 101
    invoke-virtual {p0}, Lcom/squareup/account/FileBackedAuthenticator;->getSessionIdPII()Ljava/lang/String;

    move-result-object v0

    .line 102
    move-object v3, v0

    check-cast v3, Ljava/lang/CharSequence;

    if-eqz v3, :cond_2

    invoke-static {v3}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :cond_2
    :goto_1
    if-nez v1, :cond_4

    .line 103
    iget-object v1, p0, Lcom/squareup/account/FileBackedAuthenticator;->accountStatusService:Lcom/squareup/accountstatus/PersistentAccountStatusService;

    invoke-virtual {v1}, Lcom/squareup/accountstatus/PersistentAccountStatusService;->getStatus()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v1

    const-string v2, "status"

    .line 104
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/squareup/account/FileBackedAuthenticator;->onStatusResponse(Lcom/squareup/server/account/protos/AccountStatusResponse;)V

    .line 106
    iget-object v2, p0, Lcom/squareup/account/FileBackedAuthenticator;->loggedInStatusRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    if-nez v2, :cond_3

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_3
    new-instance v3, Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus$LoggedIn;

    invoke-direct {v3, v0, v1}, Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus$LoggedIn;-><init>(Ljava/lang/String;Lcom/squareup/server/account/protos/AccountStatusResponse;)V

    invoke-virtual {v2, v3}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    goto :goto_2

    .line 108
    :cond_4
    iget-object v0, p0, Lcom/squareup/account/FileBackedAuthenticator;->loggedInStatusRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    if-nez v0, :cond_5

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_5
    sget-object v1, Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus$LoggedOut;->INSTANCE:Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus$LoggedOut;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    :goto_2
    return-void

    .line 94
    :cond_6
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "load() should only be called once per process."

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method private final logOutLocally(Lcom/squareup/account/LogOutReason;)V
    .locals 7

    .line 132
    invoke-direct {p0}, Lcom/squareup/account/FileBackedAuthenticator;->getUser()Lcom/squareup/server/account/protos/User;

    move-result-object v0

    .line 133
    iget-object v1, p0, Lcom/squareup/account/FileBackedAuthenticator;->analytics:Lcom/squareup/analytics/Analytics;

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "reason"

    aput-object v4, v2, v3

    .line 135
    invoke-virtual {p1}, Lcom/squareup/account/LogOutReason;->getAnalyticsName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    aput-object v4, v2, v5

    const/4 v4, 0x2

    const-string v6, "had user"

    aput-object v6, v2, v4

    const/4 v4, 0x0

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, v0, Lcom/squareup/server/account/protos/User;->id:Ljava/lang/String;

    goto :goto_0

    :cond_0
    move-object v0, v4

    :goto_0
    if-eqz v0, :cond_1

    const/4 v3, 0x1

    :cond_1
    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x3

    aput-object v0, v2, v3

    const-string v0, "Log out"

    .line 133
    invoke-interface {v1, v0, v2}, Lcom/squareup/analytics/Analytics;->log(Ljava/lang/String;[Ljava/lang/String;)V

    .line 138
    iget-object v0, p0, Lcom/squareup/account/FileBackedAuthenticator;->snapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->LOG_OUT:Lcom/squareup/log/OhSnapLogger$EventType;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "logged out due to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/squareup/account/LogOutReason;->getAnalyticsName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 140
    iget-object v0, p0, Lcom/squareup/account/FileBackedAuthenticator;->accountService:Lcom/squareup/account/PersistentAccountService;

    invoke-direct {p0, p1}, Lcom/squareup/account/FileBackedAuthenticator;->toClearCacheReason(Lcom/squareup/account/LogOutReason;)Lcom/squareup/account/accountservice/ClearCacheReason;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/account/PersistentAccountService;->clearCache(Lcom/squareup/account/accountservice/ClearCacheReason;)V

    .line 143
    iget-object p1, p0, Lcom/squareup/account/FileBackedAuthenticator;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-interface {p1}, Lcom/squareup/analytics/Analytics;->clearUser()V

    .line 145
    iget-object p1, p0, Lcom/squareup/account/FileBackedAuthenticator;->corruptQueueRecorder:Lcom/squareup/queue/CorruptQueueRecorder;

    invoke-virtual {p1, v4}, Lcom/squareup/queue/CorruptQueueRecorder;->setLoggedInUser(Ljava/lang/String;)V

    .line 147
    iget-object p1, p0, Lcom/squareup/account/FileBackedAuthenticator;->loggedInStatusRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    if-nez p1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    sget-object v0, Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus$LoggedOut;->INSTANCE:Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus$LoggedOut;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method private final onLoginResponse(Ljava/lang/String;Lcom/squareup/server/account/protos/AccountStatusResponse;)V
    .locals 2

    .line 166
    invoke-direct {p0, p2}, Lcom/squareup/account/FileBackedAuthenticator;->onStatusResponse(Lcom/squareup/server/account/protos/AccountStatusResponse;)V

    .line 168
    iget-object v0, p0, Lcom/squareup/account/FileBackedAuthenticator;->loggedInStatusRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    new-instance v1, Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus$LoggedIn;

    invoke-direct {v1, p1, p2}, Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus$LoggedIn;-><init>(Ljava/lang/String;Lcom/squareup/server/account/protos/AccountStatusResponse;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 169
    iget-object p1, p0, Lcom/squareup/account/FileBackedAuthenticator;->accountStatusService:Lcom/squareup/accountstatus/PersistentAccountStatusService;

    invoke-virtual {p1}, Lcom/squareup/accountstatus/PersistentAccountStatusService;->onLoggedIn()V

    return-void
.end method

.method private final onStatusResponse(Lcom/squareup/server/account/protos/AccountStatusResponse;)V
    .locals 4

    .line 173
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->user:Lcom/squareup/server/account/protos/User;

    if-nez v0, :cond_1

    .line 177
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->success:Ljava/lang/Boolean;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/2addr v0, v1

    if-eqz v0, :cond_0

    return-void

    .line 178
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AccountStatusResponse should not be successful without a user: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 177
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 184
    :cond_1
    iget-object p1, p0, Lcom/squareup/account/FileBackedAuthenticator;->analytics:Lcom/squareup/analytics/Analytics;

    iget-object v1, v0, Lcom/squareup/server/account/protos/User;->token:Ljava/lang/String;

    iget-object v2, v0, Lcom/squareup/server/account/protos/User;->merchant_token:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/squareup/account/FileBackedAuthenticator;->getCountryAsString(Lcom/squareup/server/account/protos/User;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v1, v2, v3}, Lcom/squareup/analytics/Analytics;->setUser(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    iget-object p1, p0, Lcom/squareup/account/FileBackedAuthenticator;->corruptQueueRecorder:Lcom/squareup/queue/CorruptQueueRecorder;

    iget-object v0, v0, Lcom/squareup/server/account/protos/User;->id:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/queue/CorruptQueueRecorder;->setLoggedInUser(Ljava/lang/String;)V

    return-void
.end method

.method private final toClearCacheReason(Lcom/squareup/account/LogOutReason;)Lcom/squareup/account/accountservice/ClearCacheReason;
    .locals 1

    .line 193
    instance-of v0, p1, Lcom/squareup/account/LogOutReason$SwitchToDeviceCodeSession;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/account/accountservice/ClearCacheReason$SwitchToDeviceCodeSession;

    check-cast p1, Lcom/squareup/account/LogOutReason$SwitchToDeviceCodeSession;

    invoke-virtual {p1}, Lcom/squareup/account/LogOutReason$SwitchToDeviceCodeSession;->getDeviceCode()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/account/accountservice/ClearCacheReason$SwitchToDeviceCodeSession;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/account/accountservice/ClearCacheReason;

    goto :goto_1

    .line 194
    :cond_0
    instance-of v0, p1, Lcom/squareup/account/LogOutReason$Normal;

    if-eqz v0, :cond_1

    goto :goto_0

    .line 195
    :cond_1
    instance-of v0, p1, Lcom/squareup/account/LogOutReason$UserPasswordIncorrect;

    if-eqz v0, :cond_2

    goto :goto_0

    .line 196
    :cond_2
    instance-of v0, p1, Lcom/squareup/account/LogOutReason$SessionExpired;

    if-eqz v0, :cond_3

    goto :goto_0

    .line 197
    :cond_3
    instance-of p1, p1, Lcom/squareup/account/LogOutReason$Legacy;

    if-eqz p1, :cond_4

    :goto_0
    sget-object p1, Lcom/squareup/account/accountservice/ClearCacheReason$Default;->INSTANCE:Lcom/squareup/account/accountservice/ClearCacheReason$Default;

    move-object v0, p1

    check-cast v0, Lcom/squareup/account/accountservice/ClearCacheReason;

    :goto_1
    return-object v0

    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method


# virtual methods
.method public getSessionIdPII()Ljava/lang/String;
    .locals 1

    .line 89
    iget-object v0, p0, Lcom/squareup/account/FileBackedAuthenticator;->accountService:Lcom/squareup/account/PersistentAccountService;

    invoke-virtual {v0}, Lcom/squareup/account/PersistentAccountService;->getSessionToken()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isLoggedIn()Z
    .locals 3

    .line 58
    invoke-virtual {p0}, Lcom/squareup/account/FileBackedAuthenticator;->getSessionIdPII()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/squareup/account/FileBackedAuthenticator;->getUser()Lcom/squareup/server/account/protos/User;

    move-result-object v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    :cond_2
    return v1
.end method

.method public lastLoggedInStatus()Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus;
    .locals 1

    .line 38
    invoke-static {p0}, Lcom/squareup/account/LegacyAuthenticator$DefaultImpls;->lastLoggedInStatus(Lcom/squareup/account/LegacyAuthenticator;)Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus;

    move-result-object v0

    return-object v0
.end method

.method public logOut()V
    .locals 0

    .line 38
    invoke-static {p0}, Lcom/squareup/account/LegacyAuthenticator$DefaultImpls;->logOut(Lcom/squareup/account/LegacyAuthenticator;)V

    return-void
.end method

.method public logOut(Lcom/squareup/account/LogOutReason;)V
    .locals 3

    const-string v0, "reason"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 151
    invoke-virtual {p0}, Lcom/squareup/account/FileBackedAuthenticator;->getSessionIdPII()Ljava/lang/String;

    move-result-object v0

    .line 152
    move-object v1, v0

    check-cast v1, Ljava/lang/CharSequence;

    if-eqz v1, :cond_1

    invoke-static {v1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :goto_1
    if-nez v1, :cond_2

    .line 153
    iget-object v1, p0, Lcom/squareup/account/FileBackedAuthenticator;->logoutRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    new-instance v2, Lcom/squareup/account/FileBackedAuthenticator$LogOutData;

    invoke-direct {v2, v0, p1}, Lcom/squareup/account/FileBackedAuthenticator$LogOutData;-><init>(Ljava/lang/String;Lcom/squareup/account/LogOutReason;)V

    invoke-virtual {v1, v2}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    goto :goto_2

    .line 156
    :cond_2
    invoke-direct {p0, p1}, Lcom/squareup/account/FileBackedAuthenticator;->logOutLocally(Lcom/squareup/account/LogOutReason;)V

    :goto_2
    return-void
.end method

.method public loggedIn(Ljava/lang/String;Lcom/squareup/server/account/protos/AccountStatusResponse;)V
    .locals 2

    const-string v0, "sessionToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "status"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 126
    invoke-static {v0, v1, v0}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread$default(Ljava/lang/String;ILjava/lang/Object;)V

    .line 127
    iget-object v0, p0, Lcom/squareup/account/FileBackedAuthenticator;->accountService:Lcom/squareup/account/PersistentAccountService;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/account/PersistentAccountService;->onLoginResponseReceived(Ljava/lang/String;Lcom/squareup/server/account/protos/AccountStatusResponse;)V

    .line 128
    invoke-direct {p0, p1, p2}, Lcom/squareup/account/FileBackedAuthenticator;->onLoginResponse(Ljava/lang/String;Lcom/squareup/server/account/protos/AccountStatusResponse;)V

    return-void
.end method

.method public loggedInStatus()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus;",
            ">;"
        }
    .end annotation

    .line 113
    iget-object v0, p0, Lcom/squareup/account/FileBackedAuthenticator;->loggedInStatusRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    if-nez v0, :cond_0

    .line 114
    invoke-direct {p0}, Lcom/squareup/account/FileBackedAuthenticator;->load()V

    .line 116
    :cond_0
    iget-object v0, p0, Lcom/squareup/account/FileBackedAuthenticator;->loggedInStatusRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    if-nez v0, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    check-cast v0, Lio/reactivex/Observable;

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    iget-object v0, p0, Lcom/squareup/account/FileBackedAuthenticator;->accountStatusService:Lcom/squareup/accountstatus/PersistentAccountStatusService;

    invoke-virtual {v0}, Lcom/squareup/accountstatus/PersistentAccountStatusService;->accountStatusResponse()Lio/reactivex/Observable;

    move-result-object v0

    .line 66
    new-instance v1, Lcom/squareup/account/FileBackedAuthenticator$onEnterScope$1;

    invoke-direct {v1, p0}, Lcom/squareup/account/FileBackedAuthenticator$onEnterScope$1;-><init>(Lcom/squareup/account/FileBackedAuthenticator;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "accountStatusService.acc\u2026 { onStatusResponse(it) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 69
    iget-object v0, p0, Lcom/squareup/account/FileBackedAuthenticator;->logoutRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 70
    new-instance v1, Lcom/squareup/account/FileBackedAuthenticator$onEnterScope$2;

    invoke-direct {v1, p0}, Lcom/squareup/account/FileBackedAuthenticator$onEnterScope$2;-><init>(Lcom/squareup/account/FileBackedAuthenticator;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->flatMapSingle(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 79
    sget-object v1, Lcom/squareup/account/FileBackedAuthenticator$onEnterScope$3;->INSTANCE:Lcom/squareup/account/FileBackedAuthenticator$onEnterScope$3;

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "logoutRelay\n        .fla\u2026 logOut call finished\") }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onLoggingOut()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 84
    iget-object v0, p0, Lcom/squareup/account/FileBackedAuthenticator;->onLoggingOut:Lcom/jakewharton/rxrelay2/PublishRelay;

    check-cast v0, Lio/reactivex/Observable;

    return-object v0
.end method
