.class public abstract Lcom/squareup/account/AccountModule;
.super Ljava/lang/Object;
.source "AccountModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/account/AccountModule$Prod;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract bindLoggedInStatusProvider(Lcom/squareup/account/FileBackedAuthenticator;)Lcom/squareup/account/LoggedInStatusProvider;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideAuthenticator(Lcom/squareup/account/FileBackedAuthenticator;)Lcom/squareup/account/LegacyAuthenticator;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract providePersistentAccountCache(Lcom/squareup/account/PersistentAccountService;)Lcom/squareup/account/accountservice/AppAccountCache;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideSessionIdPIIProvider(Lcom/squareup/account/LegacyAuthenticator;)Lcom/squareup/account/SessionIdPIIProvider;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
