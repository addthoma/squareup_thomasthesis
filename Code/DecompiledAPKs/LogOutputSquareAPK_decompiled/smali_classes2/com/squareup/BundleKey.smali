.class public abstract Lcom/squareup/BundleKey;
.super Ljava/lang/Object;
.source "BundleKey.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field protected final key:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/squareup/BundleKey;->key:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Lcom/squareup/BundleKey$1;)V
    .locals 0

    .line 9
    invoke-direct {p0, p1}, Lcom/squareup/BundleKey;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public static forEnum(Ljava/lang/String;Ljava/lang/Class;)Lcom/squareup/BundleKey;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Enum<",
            "TT;>;>(",
            "Ljava/lang/String;",
            "Ljava/lang/Class<",
            "TT;>;)",
            "Lcom/squareup/BundleKey<",
            "TT;>;"
        }
    .end annotation

    .line 119
    new-instance v0, Lcom/squareup/BundleKey$3;

    invoke-direct {v0, p0, p1}, Lcom/squareup/BundleKey$3;-><init>(Ljava/lang/String;Ljava/lang/Class;)V

    return-object v0
.end method

.method public static json(Lcom/google/gson/Gson;Ljava/lang/String;Ljava/lang/Class;)Lcom/squareup/BundleKey;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/gson/Gson;",
            "Ljava/lang/String;",
            "Ljava/lang/Class<",
            "TT;>;)",
            "Lcom/squareup/BundleKey<",
            "TT;>;"
        }
    .end annotation

    .line 92
    new-instance v0, Lcom/squareup/BundleKey$2;

    invoke-direct {v0, p1, p0, p2}, Lcom/squareup/BundleKey$2;-><init>(Ljava/lang/String;Lcom/google/gson/Gson;Ljava/lang/Class;)V

    return-object v0
.end method

.method public static json(Lcom/google/gson/Gson;Ljava/lang/String;Ljava/lang/reflect/Type;)Lcom/squareup/BundleKey;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/gson/Gson;",
            "Ljava/lang/String;",
            "Ljava/lang/reflect/Type;",
            ")",
            "Lcom/squareup/BundleKey<",
            "TT;>;"
        }
    .end annotation

    .line 63
    new-instance v0, Lcom/squareup/BundleKey$1;

    invoke-direct {v0, p1, p0, p2}, Lcom/squareup/BundleKey$1;-><init>(Ljava/lang/String;Lcom/google/gson/Gson;Ljava/lang/reflect/Type;)V

    return-object v0
.end method

.method public static serializable(Ljava/lang/String;Ljava/lang/Class;)Lcom/squareup/BundleKey;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/io/Serializable;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/Class<",
            "TT;>;)",
            "Lcom/squareup/BundleKey<",
            "TT;>;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 149
    new-instance v0, Lcom/squareup/BundleKey$4;

    invoke-direct {v0, p0, p1}, Lcom/squareup/BundleKey$4;-><init>(Ljava/lang/String;Ljava/lang/Class;)V

    return-object v0
.end method

.method public static string(Ljava/lang/String;)Lcom/squareup/BundleKey;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/BundleKey<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 173
    new-instance v0, Lcom/squareup/BundleKey$5;

    invoke-direct {v0, p0}, Lcom/squareup/BundleKey$5;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public abstract get(Landroid/content/Intent;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")TT;"
        }
    .end annotation
.end method

.method public final get(Landroid/content/Intent;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            "TT;)TT;"
        }
    .end annotation

    .line 30
    invoke-virtual {p0, p1}, Lcom/squareup/BundleKey;->get(Landroid/content/Intent;)Ljava/lang/Object;

    move-result-object p1

    if-nez p1, :cond_0

    move-object p1, p2

    :cond_0
    return-object p1
.end method

.method public abstract get(Landroid/os/Bundle;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            ")TT;"
        }
    .end annotation
.end method

.method public final get(Landroid/os/Bundle;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "TT;)TT;"
        }
    .end annotation

    .line 18
    invoke-virtual {p0, p1}, Lcom/squareup/BundleKey;->get(Landroid/os/Bundle;)Ljava/lang/Object;

    move-result-object p1

    if-nez p1, :cond_0

    move-object p1, p2

    :cond_0
    return-object p1
.end method

.method public abstract put(Landroid/content/Intent;Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            "TT;)V"
        }
    .end annotation
.end method

.method public abstract put(Landroid/os/Bundle;Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "TT;)V"
        }
    .end annotation
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/squareup/BundleKey;->key:Ljava/lang/String;

    return-object v0
.end method
