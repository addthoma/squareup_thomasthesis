.class public final Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;
.super Lcom/squareup/backgroundjob/BackgroundJobCreator;
.source "AccountFreezeNotificationScheduler.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/dagger/LoggedInScope;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAccountFreezeNotificationScheduler.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AccountFreezeNotificationScheduler.kt\ncom/squareup/accountfreeze/AccountFreezeNotificationScheduler\n*L\n1#1,108:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0007\u0018\u0000 \u001c2\u00020\u0001:\u0001\u001cB?\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u000e\u0008\u0001\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r\u00a2\u0006\u0002\u0010\u000fJ\u0012\u0010\u0010\u001a\u0004\u0018\u00010\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0016J\u0008\u0010\u0014\u001a\u00020\u000eH\u0002J\u0006\u0010\u0015\u001a\u00020\u0016J\u0006\u0010\u0017\u001a\u00020\u0016J\u0008\u0010\u0018\u001a\u00020\u0016H\u0002J\u0006\u0010\u0019\u001a\u00020\u0016J\u0006\u0010\u001a\u001a\u00020\u001bR\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;",
        "Lcom/squareup/backgroundjob/BackgroundJobCreator;",
        "clock",
        "Lcom/squareup/util/Clock;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "jobManager",
        "Lcom/squareup/backgroundjob/BackgroundJobManager;",
        "jobNotificationManager",
        "Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;",
        "accountFreezeNotifications",
        "Lcom/squareup/accountfreeze/AccountFreezeNotificationManager;",
        "lastDismissedNotification",
        "Lcom/f2prateek/rx/preferences2/Preference;",
        "",
        "(Lcom/squareup/util/Clock;Lcom/squareup/analytics/Analytics;Lcom/squareup/backgroundjob/BackgroundJobManager;Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;Lcom/squareup/accountfreeze/AccountFreezeNotificationManager;Lcom/f2prateek/rx/preferences2/Preference;)V",
        "create",
        "Lcom/evernote/android/job/Job;",
        "tag",
        "",
        "millisToNextNotification",
        "onNotificationDismissed",
        "",
        "onNotificationTapped",
        "rescheduleNotification",
        "scheduleNotification",
        "unscheduleNotification",
        "",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler$Companion;

.field public static final SHOW_NOTIFICATION:Ljava/lang/String; = "account_freeze_notification"


# instance fields
.field private final accountFreezeNotifications:Lcom/squareup/accountfreeze/AccountFreezeNotificationManager;

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final clock:Lcom/squareup/util/Clock;

.field private final jobManager:Lcom/squareup/backgroundjob/BackgroundJobManager;

.field private final jobNotificationManager:Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;

.field private final lastDismissedNotification:Lcom/f2prateek/rx/preferences2/Preference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;->Companion:Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/util/Clock;Lcom/squareup/analytics/Analytics;Lcom/squareup/backgroundjob/BackgroundJobManager;Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;Lcom/squareup/accountfreeze/AccountFreezeNotificationManager;Lcom/f2prateek/rx/preferences2/Preference;)V
    .locals 1
    .param p6    # Lcom/f2prateek/rx/preferences2/Preference;
        .annotation runtime Lcom/squareup/accountfreeze/LastDismissedFreezeNotification;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Clock;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/backgroundjob/BackgroundJobManager;",
            "Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;",
            "Lcom/squareup/accountfreeze/AccountFreezeNotificationManager;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "clock"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "jobManager"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "jobNotificationManager"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountFreezeNotifications"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "lastDismissedNotification"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-direct {p0, p3}, Lcom/squareup/backgroundjob/BackgroundJobCreator;-><init>(Lcom/squareup/backgroundjob/BackgroundJobManager;)V

    iput-object p1, p0, Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;->clock:Lcom/squareup/util/Clock;

    iput-object p2, p0, Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;->analytics:Lcom/squareup/analytics/Analytics;

    iput-object p3, p0, Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;->jobManager:Lcom/squareup/backgroundjob/BackgroundJobManager;

    iput-object p4, p0, Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;->jobNotificationManager:Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;

    iput-object p5, p0, Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;->accountFreezeNotifications:Lcom/squareup/accountfreeze/AccountFreezeNotificationManager;

    iput-object p6, p0, Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;->lastDismissedNotification:Lcom/f2prateek/rx/preferences2/Preference;

    return-void
.end method

.method private final millisToNextNotification()J
    .locals 4

    .line 90
    iget-object v0, p0, Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;->lastDismissedNotification:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v0}, Lcom/f2prateek/rx/preferences2/Preference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    sget-object v2, Lcom/squareup/accountfreeze/RealAccountFreeze;->Companion:Lcom/squareup/accountfreeze/RealAccountFreeze$Companion;

    invoke-virtual {v2}, Lcom/squareup/accountfreeze/RealAccountFreeze$Companion;->getNOTIFICATION_INTERVAL()J

    move-result-wide v2

    add-long/2addr v0, v2

    iget-object v2, p0, Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v2}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x1

    .line 89
    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method private final rescheduleNotification()V
    .locals 0

    .line 100
    invoke-virtual {p0}, Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;->unscheduleNotification()I

    .line 101
    invoke-virtual {p0}, Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;->scheduleNotification()V

    return-void
.end method


# virtual methods
.method public create(Ljava/lang/String;)Lcom/evernote/android/job/Job;
    .locals 2

    const-string v0, "tag"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "account_freeze_notification"

    .line 34
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 35
    new-instance p1, Lcom/squareup/accountfreeze/ShowNotificationJob;

    iget-object v0, p0, Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;->accountFreezeNotifications:Lcom/squareup/accountfreeze/AccountFreezeNotificationManager;

    iget-object v1, p0, Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;->jobNotificationManager:Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;

    invoke-direct {p1, v0, v1}, Lcom/squareup/accountfreeze/ShowNotificationJob;-><init>(Lcom/squareup/accountfreeze/AccountFreezeNotificationManager;Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;)V

    check-cast p1, Lcom/evernote/android/job/Job;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public final onNotificationDismissed()V
    .locals 3

    .line 80
    iget-object v0, p0, Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;->lastDismissedNotification:Lcom/f2prateek/rx/preferences2/Preference;

    iget-object v1, p0, Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v1}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/f2prateek/rx/preferences2/Preference;->set(Ljava/lang/Object;)V

    .line 81
    invoke-direct {p0}, Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;->rescheduleNotification()V

    return-void
.end method

.method public final onNotificationTapped()V
    .locals 3

    .line 74
    iget-object v0, p0, Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {v0}, Lcom/squareup/accountfreeze/FreezeAnalyticsKt;->onClickedNotification(Lcom/squareup/analytics/Analytics;)V

    .line 75
    iget-object v0, p0, Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;->lastDismissedNotification:Lcom/f2prateek/rx/preferences2/Preference;

    iget-object v1, p0, Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v1}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/f2prateek/rx/preferences2/Preference;->set(Ljava/lang/Object;)V

    .line 76
    invoke-direct {p0}, Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;->rescheduleNotification()V

    return-void
.end method

.method public final scheduleNotification()V
    .locals 8

    .line 49
    iget-object v0, p0, Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;->jobManager:Lcom/squareup/backgroundjob/BackgroundJobManager;

    const-string v1, "account_freeze_notification"

    invoke-interface {v0, v1}, Lcom/squareup/backgroundjob/BackgroundJobManager;->getAllJobRequestsForTag(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    const-string v2, "jobManager.getAllJobRequ\u2026ForTag(SHOW_NOTIFICATION)"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    const/4 v2, 0x1

    xor-int/2addr v0, v2

    if-eqz v0, :cond_0

    return-void

    .line 57
    :cond_0
    invoke-direct {p0}, Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;->millisToNextNotification()J

    move-result-wide v3

    .line 58
    sget-object v0, Lcom/squareup/accountfreeze/RealAccountFreeze;->Companion:Lcom/squareup/accountfreeze/RealAccountFreeze$Companion;

    invoke-virtual {v0}, Lcom/squareup/accountfreeze/RealAccountFreeze$Companion;->getNOTIFICATION_WINDOW()J

    move-result-wide v5

    add-long/2addr v5, v3

    .line 60
    iget-object v0, p0, Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;->jobManager:Lcom/squareup/backgroundjob/BackgroundJobManager;

    .line 61
    new-instance v7, Lcom/evernote/android/job/JobRequest$Builder;

    invoke-direct {v7, v1}, Lcom/evernote/android/job/JobRequest$Builder;-><init>(Ljava/lang/String;)V

    .line 62
    invoke-virtual {v7, v3, v4, v5, v6}, Lcom/evernote/android/job/JobRequest$Builder;->setExecutionWindow(JJ)Lcom/evernote/android/job/JobRequest$Builder;

    move-result-object v1

    .line 63
    invoke-virtual {v1, v2}, Lcom/evernote/android/job/JobRequest$Builder;->setUpdateCurrent(Z)Lcom/evernote/android/job/JobRequest$Builder;

    move-result-object v1

    .line 64
    invoke-virtual {v1}, Lcom/evernote/android/job/JobRequest$Builder;->build()Lcom/evernote/android/job/JobRequest;

    move-result-object v1

    .line 60
    invoke-interface {v0, v1}, Lcom/squareup/backgroundjob/BackgroundJobManager;->schedule(Lcom/evernote/android/job/JobRequest;)V

    return-void
.end method

.method public final unscheduleNotification()I
    .locals 2

    .line 70
    iget-object v0, p0, Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;->jobManager:Lcom/squareup/backgroundjob/BackgroundJobManager;

    const-string v1, "account_freeze_notification"

    invoke-interface {v0, v1}, Lcom/squareup/backgroundjob/BackgroundJobManager;->cancelAllForTag(Ljava/lang/String;)I

    move-result v0

    return v0
.end method
