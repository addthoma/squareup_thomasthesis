.class public final Lcom/squareup/accountfreeze/AccountFreezeNotificationMonitor;
.super Ljava/lang/Object;
.source "AccountFreezeNotificationMonitor.kt"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u0001B\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0016J\u0008\u0010\r\u001a\u00020\nH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/accountfreeze/AccountFreezeNotificationMonitor;",
        "Lmortar/Scoped;",
        "accountFreeze",
        "Lcom/squareup/accountfreeze/AccountFreeze;",
        "accountFreezeNotificationScheduler",
        "Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;",
        "accountFreezeNotifications",
        "Lcom/squareup/accountfreeze/AccountFreezeNotificationManager;",
        "(Lcom/squareup/accountfreeze/AccountFreeze;Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;Lcom/squareup/accountfreeze/AccountFreezeNotificationManager;)V",
        "onEnterScope",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final accountFreeze:Lcom/squareup/accountfreeze/AccountFreeze;

.field private final accountFreezeNotificationScheduler:Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;

.field private final accountFreezeNotifications:Lcom/squareup/accountfreeze/AccountFreezeNotificationManager;


# direct methods
.method public constructor <init>(Lcom/squareup/accountfreeze/AccountFreeze;Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;Lcom/squareup/accountfreeze/AccountFreezeNotificationManager;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "accountFreeze"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountFreezeNotificationScheduler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountFreezeNotifications"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/accountfreeze/AccountFreezeNotificationMonitor;->accountFreeze:Lcom/squareup/accountfreeze/AccountFreeze;

    iput-object p2, p0, Lcom/squareup/accountfreeze/AccountFreezeNotificationMonitor;->accountFreezeNotificationScheduler:Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;

    iput-object p3, p0, Lcom/squareup/accountfreeze/AccountFreezeNotificationMonitor;->accountFreezeNotifications:Lcom/squareup/accountfreeze/AccountFreezeNotificationManager;

    return-void
.end method

.method public static final synthetic access$getAccountFreezeNotificationScheduler$p(Lcom/squareup/accountfreeze/AccountFreezeNotificationMonitor;)Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;
    .locals 0

    .line 14
    iget-object p0, p0, Lcom/squareup/accountfreeze/AccountFreezeNotificationMonitor;->accountFreezeNotificationScheduler:Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;

    return-object p0
.end method

.method public static final synthetic access$getAccountFreezeNotifications$p(Lcom/squareup/accountfreeze/AccountFreezeNotificationMonitor;)Lcom/squareup/accountfreeze/AccountFreezeNotificationManager;
    .locals 0

    .line 14
    iget-object p0, p0, Lcom/squareup/accountfreeze/AccountFreezeNotificationMonitor;->accountFreezeNotifications:Lcom/squareup/accountfreeze/AccountFreezeNotificationManager;

    return-object p0
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    iget-object v0, p0, Lcom/squareup/accountfreeze/AccountFreezeNotificationMonitor;->accountFreeze:Lcom/squareup/accountfreeze/AccountFreeze;

    invoke-interface {v0}, Lcom/squareup/accountfreeze/AccountFreeze;->canShowNotification()Lio/reactivex/Observable;

    move-result-object v0

    .line 23
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "accountFreeze.canShowNot\u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    new-instance v1, Lcom/squareup/accountfreeze/AccountFreezeNotificationMonitor$onEnterScope$1;

    invoke-direct {v1, p0}, Lcom/squareup/accountfreeze/AccountFreezeNotificationMonitor$onEnterScope$1;-><init>(Lcom/squareup/accountfreeze/AccountFreezeNotificationMonitor;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method
