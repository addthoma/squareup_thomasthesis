.class public final Lcom/squareup/accountfreeze/AccountFreezeDeepLinkHandler;
.super Ljava/lang/Object;
.source "AccountFreezeDeepLinkHandler.kt"

# interfaces
.implements Lcom/squareup/deeplinks/DeepLinkHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/accountfreeze/AccountFreezeDeepLinkHandler$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 \u00132\u00020\u0001:\u0001\u0013B%\u0008\u0007\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0016R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010\n\u001a\u00020\u00048BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\r\u0010\u000e\u001a\u0004\u0008\u000b\u0010\u000c\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/accountfreeze/AccountFreezeDeepLinkHandler;",
        "Lcom/squareup/deeplinks/DeepLinkHandler;",
        "lazyDeepLinks",
        "Ldagger/Lazy;",
        "Lcom/squareup/ui/main/DeepLinks;",
        "accountFreezeNotificationScheduler",
        "Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;",
        "accountFreezeNotificationManager",
        "Lcom/squareup/accountfreeze/AccountFreezeNotificationManager;",
        "(Ldagger/Lazy;Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;Lcom/squareup/accountfreeze/AccountFreezeNotificationManager;)V",
        "deepLinks",
        "getDeepLinks",
        "()Lcom/squareup/ui/main/DeepLinks;",
        "deepLinks$delegate",
        "Ldagger/Lazy;",
        "handleExternal",
        "Lcom/squareup/deeplinks/DeepLinkResult;",
        "uri",
        "Landroid/net/Uri;",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final Companion:Lcom/squareup/accountfreeze/AccountFreezeDeepLinkHandler$Companion;

.field public static final FREEZE_DEEP_LINK:Ljava/lang/String; = "square-register://account-freeze"

.field private static final FREEZE_HOST:Ljava/lang/String; = "account-freeze"


# instance fields
.field private final accountFreezeNotificationManager:Lcom/squareup/accountfreeze/AccountFreezeNotificationManager;

.field private final accountFreezeNotificationScheduler:Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;

.field private final deepLinks$delegate:Ldagger/Lazy;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lkotlin/jvm/internal/PropertyReference1Impl;

    const-class v2, Lcom/squareup/accountfreeze/AccountFreezeDeepLinkHandler;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-string v3, "deepLinks"

    const-string v4, "getDeepLinks()Lcom/squareup/ui/main/DeepLinks;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->property1(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/accountfreeze/AccountFreezeDeepLinkHandler;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/squareup/accountfreeze/AccountFreezeDeepLinkHandler$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/accountfreeze/AccountFreezeDeepLinkHandler$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/accountfreeze/AccountFreezeDeepLinkHandler;->Companion:Lcom/squareup/accountfreeze/AccountFreezeDeepLinkHandler$Companion;

    return-void
.end method

.method public constructor <init>(Ldagger/Lazy;Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;Lcom/squareup/accountfreeze/AccountFreezeNotificationManager;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/main/DeepLinks;",
            ">;",
            "Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;",
            "Lcom/squareup/accountfreeze/AccountFreezeNotificationManager;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "lazyDeepLinks"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountFreezeNotificationScheduler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountFreezeNotificationManager"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/squareup/accountfreeze/AccountFreezeDeepLinkHandler;->accountFreezeNotificationScheduler:Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;

    iput-object p3, p0, Lcom/squareup/accountfreeze/AccountFreezeDeepLinkHandler;->accountFreezeNotificationManager:Lcom/squareup/accountfreeze/AccountFreezeNotificationManager;

    .line 20
    iput-object p1, p0, Lcom/squareup/accountfreeze/AccountFreezeDeepLinkHandler;->deepLinks$delegate:Ldagger/Lazy;

    return-void
.end method

.method private final getDeepLinks()Lcom/squareup/ui/main/DeepLinks;
    .locals 3

    iget-object v0, p0, Lcom/squareup/accountfreeze/AccountFreezeDeepLinkHandler;->deepLinks$delegate:Ldagger/Lazy;

    sget-object v1, Lcom/squareup/accountfreeze/AccountFreezeDeepLinkHandler;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-static {v0, p0, v1}, Lcom/squareup/dagger/LazysKt;->getValue(Ldagger/Lazy;Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/DeepLinks;

    return-object v0
.end method


# virtual methods
.method public handleExternal(Landroid/net/Uri;)Lcom/squareup/deeplinks/DeepLinkResult;
    .locals 1

    const-string/jumbo v0, "uri"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object p1

    const-string v0, "account-freeze"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 24
    iget-object p1, p0, Lcom/squareup/accountfreeze/AccountFreezeDeepLinkHandler;->accountFreezeNotificationScheduler:Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;

    invoke-virtual {p1}, Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;->onNotificationTapped()V

    .line 25
    iget-object p1, p0, Lcom/squareup/accountfreeze/AccountFreezeDeepLinkHandler;->accountFreezeNotificationManager:Lcom/squareup/accountfreeze/AccountFreezeNotificationManager;

    invoke-virtual {p1}, Lcom/squareup/accountfreeze/AccountFreezeNotificationManager;->hideNotification()V

    .line 29
    invoke-direct {p0}, Lcom/squareup/accountfreeze/AccountFreezeDeepLinkHandler;->getDeepLinks()Lcom/squareup/ui/main/DeepLinks;

    move-result-object p1

    const-string v0, "square-register://deposits"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/main/DeepLinks;->handleExternalDeepLink(Landroid/net/Uri;)Lcom/squareup/deeplinks/DeepLinkResult;

    move-result-object p1

    const-string v0, "deepLinks.handleExternal\u2026AppletGateway.DEEP_LINK))"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 31
    :cond_0
    new-instance p1, Lcom/squareup/deeplinks/DeepLinkResult;

    sget-object v0, Lcom/squareup/deeplinks/DeepLinkStatus;->UNRECOGNIZED:Lcom/squareup/deeplinks/DeepLinkStatus;

    invoke-direct {p1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/deeplinks/DeepLinkStatus;)V

    :goto_0
    return-object p1
.end method
