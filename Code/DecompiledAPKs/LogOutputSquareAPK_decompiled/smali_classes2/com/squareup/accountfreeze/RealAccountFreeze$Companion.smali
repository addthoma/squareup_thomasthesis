.class public final Lcom/squareup/accountfreeze/RealAccountFreeze$Companion;
.super Ljava/lang/Object;
.source "RealAccountFreeze.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/accountfreeze/RealAccountFreeze;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0002\u0008\u0006\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0005\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007R\u0011\u0010\u0008\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\u0007\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/accountfreeze/RealAccountFreeze$Companion;",
        "",
        "()V",
        "CHECK_INTERVAL_MINUTES",
        "",
        "NOTIFICATION_INTERVAL",
        "getNOTIFICATION_INTERVAL",
        "()J",
        "NOTIFICATION_WINDOW",
        "getNOTIFICATION_WINDOW",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 96
    invoke-direct {p0}, Lcom/squareup/accountfreeze/RealAccountFreeze$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final getNOTIFICATION_INTERVAL()J
    .locals 2

    .line 98
    invoke-static {}, Lcom/squareup/accountfreeze/RealAccountFreeze;->access$getNOTIFICATION_INTERVAL$cp()J

    move-result-wide v0

    return-wide v0
.end method

.method public final getNOTIFICATION_WINDOW()J
    .locals 2

    .line 102
    invoke-static {}, Lcom/squareup/accountfreeze/RealAccountFreeze;->access$getNOTIFICATION_WINDOW$cp()J

    move-result-wide v0

    return-wide v0
.end method
