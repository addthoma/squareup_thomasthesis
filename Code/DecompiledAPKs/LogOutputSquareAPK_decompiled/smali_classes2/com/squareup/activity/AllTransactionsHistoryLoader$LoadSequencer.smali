.class Lcom/squareup/activity/AllTransactionsHistoryLoader$LoadSequencer;
.super Ljava/lang/Object;
.source "AllTransactionsHistoryLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/activity/AllTransactionsHistoryLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "LoadSequencer"
.end annotation


# instance fields
.field additionalPagesCount:I

.field disposable:Lio/reactivex/disposables/Disposable;

.field loadToken:Ljava/util/UUID;

.field private final loader:Lcom/squareup/activity/AllTransactionsHistoryLoader;

.field private final pendingTransactionsStore:Lcom/squareup/payment/pending/PendingTransactionsStore;

.field prepared:Z

.field private priorCount:I

.field private totalProcessedCount:I


# direct methods
.method constructor <init>(Lcom/squareup/activity/AllTransactionsHistoryLoader;Lcom/squareup/payment/pending/PendingTransactionsStore;)V
    .locals 2

    .line 191
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 184
    iput v0, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader$LoadSequencer;->priorCount:I

    .line 185
    iput v0, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader$LoadSequencer;->totalProcessedCount:I

    .line 186
    iput v0, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader$LoadSequencer;->additionalPagesCount:I

    const/4 v1, 0x0

    .line 187
    iput-object v1, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader$LoadSequencer;->loadToken:Ljava/util/UUID;

    .line 188
    iput-boolean v0, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader$LoadSequencer;->prepared:Z

    .line 192
    iput-object p1, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader$LoadSequencer;->loader:Lcom/squareup/activity/AllTransactionsHistoryLoader;

    .line 193
    iput-object p2, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader$LoadSequencer;->pendingTransactionsStore:Lcom/squareup/payment/pending/PendingTransactionsStore;

    return-void
.end method


# virtual methods
.method public synthetic lambda$prepare$0$AllTransactionsHistoryLoader$LoadSequencer(Ljava/lang/Integer;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 202
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget v1, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader$LoadSequencer;->priorCount:I

    if-ge v0, v1, :cond_0

    .line 203
    iget v0, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader$LoadSequencer;->totalProcessedCount:I

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    sub-int/2addr v1, v2

    add-int/2addr v0, v1

    iput v0, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader$LoadSequencer;->totalProcessedCount:I

    .line 204
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_0

    .line 206
    iget v0, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader$LoadSequencer;->totalProcessedCount:I

    int-to-double v0, v0

    const-wide/high16 v2, 0x4034000000000000L    # 20.0

    div-double/2addr v0, v2

    .line 207
    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader$LoadSequencer;->additionalPagesCount:I

    const/4 v0, 0x0

    .line 208
    iput v0, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader$LoadSequencer;->totalProcessedCount:I

    .line 209
    iget-object v0, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader$LoadSequencer;->loader:Lcom/squareup/activity/AllTransactionsHistoryLoader;

    invoke-virtual {v0}, Lcom/squareup/activity/AllTransactionsHistoryLoader;->load()Ljava/util/UUID;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader$LoadSequencer;->loadToken:Ljava/util/UUID;

    .line 212
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    iput p1, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader$LoadSequencer;->priorCount:I

    return-void
.end method

.method maybeLoadMore(Ljava/util/UUID;)V
    .locals 2

    .line 228
    iget-boolean v0, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader$LoadSequencer;->prepared:Z

    const-string v1, "LoadSequencer must be prepared before attempting to load more"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 229
    iget-object v0, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader$LoadSequencer;->loadToken:Ljava/util/UUID;

    invoke-virtual {p1, v0}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 231
    iput p1, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader$LoadSequencer;->additionalPagesCount:I

    return-void

    .line 234
    :cond_0
    iget p1, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader$LoadSequencer;->additionalPagesCount:I

    if-lez p1, :cond_1

    add-int/lit8 p1, p1, -0x1

    .line 235
    iput p1, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader$LoadSequencer;->additionalPagesCount:I

    .line 236
    iget-object p1, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader$LoadSequencer;->loader:Lcom/squareup/activity/AllTransactionsHistoryLoader;

    invoke-virtual {p1}, Lcom/squareup/activity/AllTransactionsHistoryLoader;->loadMore()V

    :cond_1
    return-void
.end method

.method prepare()V
    .locals 2

    .line 200
    iget-object v0, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader$LoadSequencer;->pendingTransactionsStore:Lcom/squareup/payment/pending/PendingTransactionsStore;

    invoke-interface {v0}, Lcom/squareup/payment/pending/PendingTransactionsStore;->smoothedAllPendingTransactionsCount()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/activity/-$$Lambda$AllTransactionsHistoryLoader$LoadSequencer$hHPQVJ7pyXcure4DsBUEzkAJ378;

    invoke-direct {v1, p0}, Lcom/squareup/activity/-$$Lambda$AllTransactionsHistoryLoader$LoadSequencer$hHPQVJ7pyXcure4DsBUEzkAJ378;-><init>(Lcom/squareup/activity/AllTransactionsHistoryLoader$LoadSequencer;)V

    .line 201
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader$LoadSequencer;->disposable:Lio/reactivex/disposables/Disposable;

    const/4 v0, 0x1

    .line 214
    iput-boolean v0, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader$LoadSequencer;->prepared:Z

    return-void
.end method

.method shutdown()V
    .locals 1

    .line 241
    iget-object v0, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader$LoadSequencer;->disposable:Lio/reactivex/disposables/Disposable;

    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->dispose()V

    return-void
.end method
