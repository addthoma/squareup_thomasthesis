.class public interface abstract Lcom/squareup/activity/ui/IssueReceiptCoordinator$EventHandler;
.super Ljava/lang/Object;
.source "IssueReceiptCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/activity/ui/IssueReceiptCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "EventHandler"
.end annotation


# virtual methods
.method public abstract languageSelectionClicked()V
.end method

.method public abstract onBackPressed()V
.end method

.method public abstract onEmailTextChanged(Ljava/lang/String;)V
.end method

.method public abstract onReceiptAnimationEnd()V
.end method

.method public abstract onSmsTextChanged(Ljava/lang/String;)V
.end method

.method public abstract printFormalReceipt()V
.end method

.method public abstract printReceipt()V
.end method

.method public abstract sendEmailReceipt()V
.end method

.method public abstract sendSmsReceipt()V
.end method
