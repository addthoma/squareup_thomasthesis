.class public final Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData;
.super Lcom/squareup/activity/ui/IssueReceiptScreenData;
.source "IssueReceiptScreenData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/activity/ui/IssueReceiptScreenData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AnimateOutViewData"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;
    }
.end annotation


# instance fields
.field public final emailReceiptEnabled:Z

.field public final iconGlyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public final iconGlyphVisible:Z

.field public final printReceiptEnabled:Z

.field public final smsReceiptEnabled:Z

.field public final subtitleVisible:Z

.field public final titleText:I

.field public final titleVisible:Z


# direct methods
.method public constructor <init>(Lcom/squareup/glyph/GlyphTypeface$Glyph;IZZZZZZ)V
    .locals 0

    .line 24
    invoke-direct {p0}, Lcom/squareup/activity/ui/IssueReceiptScreenData;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData;->iconGlyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 26
    iput p2, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData;->titleText:I

    .line 27
    iput-boolean p3, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData;->iconGlyphVisible:Z

    .line 28
    iput-boolean p4, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData;->titleVisible:Z

    .line 29
    iput-boolean p5, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData;->subtitleVisible:Z

    .line 30
    iput-boolean p6, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData;->emailReceiptEnabled:Z

    .line 31
    iput-boolean p7, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData;->smsReceiptEnabled:Z

    .line 32
    iput-boolean p8, p0, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData;->printReceiptEnabled:Z

    return-void
.end method


# virtual methods
.method public buildUpon()Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;
    .locals 1

    .line 36
    new-instance v0, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;

    invoke-direct {v0, p0}, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData$Builder;-><init>(Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData;)V

    return-object v0
.end method
