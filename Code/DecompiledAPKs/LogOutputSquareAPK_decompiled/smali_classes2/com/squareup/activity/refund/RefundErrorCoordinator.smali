.class public final Lcom/squareup/activity/refund/RefundErrorCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "RefundErrorCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/activity/refund/RefundErrorCoordinator$RefundErrorEventRunner;,
        Lcom/squareup/activity/refund/RefundErrorCoordinator$Factory;,
        Lcom/squareup/activity/refund/RefundErrorCoordinator$ItemizedRefundStage;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRefundErrorCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RefundErrorCoordinator.kt\ncom/squareup/activity/refund/RefundErrorCoordinator\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,125:1\n1103#2,7:126\n1103#2,7:133\n*E\n*S KotlinDebug\n*F\n+ 1 RefundErrorCoordinator.kt\ncom/squareup/activity/refund/RefundErrorCoordinator\n*L\n93#1,7:126\n103#1,7:133\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0018\u00002\u00020\u0001:\u0003\u0016\u0017\u0018B#\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0016J\u0010\u0010\u0014\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0002J\u0010\u0010\u0015\u001a\u00020\u00112\u0006\u0010\u0004\u001a\u00020\u0006H\u0002R\u000e\u0010\n\u001a\u00020\u000bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/activity/refund/RefundErrorCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "res",
        "Lcom/squareup/util/Res;",
        "data",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/receiving/FailureMessage;",
        "eventRunner",
        "Lcom/squareup/activity/refund/RefundErrorCoordinator$RefundErrorEventRunner;",
        "(Lcom/squareup/util/Res;Lio/reactivex/Observable;Lcom/squareup/activity/refund/RefundErrorCoordinator$RefundErrorEventRunner;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/ActionBarView;",
        "actionButton",
        "Lcom/squareup/marketfont/MarketButton;",
        "message",
        "Lcom/squareup/marin/widgets/MarinGlyphMessage;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "configureView",
        "Factory",
        "ItemizedRefundStage",
        "RefundErrorEventRunner",
        "activity_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private actionButton:Lcom/squareup/marketfont/MarketButton;

.field private final data:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/receiving/FailureMessage;",
            ">;"
        }
    .end annotation
.end field

.field private final eventRunner:Lcom/squareup/activity/refund/RefundErrorCoordinator$RefundErrorEventRunner;

.field private message:Lcom/squareup/marin/widgets/MarinGlyphMessage;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lio/reactivex/Observable;Lcom/squareup/activity/refund/RefundErrorCoordinator$RefundErrorEventRunner;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/receiving/FailureMessage;",
            ">;",
            "Lcom/squareup/activity/refund/RefundErrorCoordinator$RefundErrorEventRunner;",
            ")V"
        }
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "data"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "eventRunner"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/activity/refund/RefundErrorCoordinator;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/activity/refund/RefundErrorCoordinator;->data:Lio/reactivex/Observable;

    iput-object p3, p0, Lcom/squareup/activity/refund/RefundErrorCoordinator;->eventRunner:Lcom/squareup/activity/refund/RefundErrorCoordinator$RefundErrorEventRunner;

    return-void
.end method

.method public static final synthetic access$configureView(Lcom/squareup/activity/refund/RefundErrorCoordinator;Lcom/squareup/receiving/FailureMessage;)V
    .locals 0

    .line 23
    invoke-direct {p0, p1}, Lcom/squareup/activity/refund/RefundErrorCoordinator;->configureView(Lcom/squareup/receiving/FailureMessage;)V

    return-void
.end method

.method public static final synthetic access$getData$p(Lcom/squareup/activity/refund/RefundErrorCoordinator;)Lio/reactivex/Observable;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/squareup/activity/refund/RefundErrorCoordinator;->data:Lio/reactivex/Observable;

    return-object p0
.end method

.method public static final synthetic access$getEventRunner$p(Lcom/squareup/activity/refund/RefundErrorCoordinator;)Lcom/squareup/activity/refund/RefundErrorCoordinator$RefundErrorEventRunner;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/squareup/activity/refund/RefundErrorCoordinator;->eventRunner:Lcom/squareup/activity/refund/RefundErrorCoordinator$RefundErrorEventRunner;

    return-object p0
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 110
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/activity/refund/RefundErrorCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 111
    sget v0, Lcom/squareup/activity/R$id;->error_message_panel:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinGlyphMessage;

    iput-object v0, p0, Lcom/squareup/activity/refund/RefundErrorCoordinator;->message:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    .line 112
    sget v0, Lcom/squareup/activity/R$id;->refund_error_action_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketButton;

    iput-object p1, p0, Lcom/squareup/activity/refund/RefundErrorCoordinator;->actionButton:Lcom/squareup/marketfont/MarketButton;

    return-void
.end method

.method private final configureView(Lcom/squareup/receiving/FailureMessage;)V
    .locals 7

    .line 82
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundErrorCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string v1, "actionBar.presenter"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 69
    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 70
    iget-object v3, p0, Lcom/squareup/activity/refund/RefundErrorCoordinator;->eventRunner:Lcom/squareup/activity/refund/RefundErrorCoordinator$RefundErrorEventRunner;

    invoke-interface {v3}, Lcom/squareup/activity/refund/RefundErrorCoordinator$RefundErrorEventRunner;->itemizedRefundStage()Lcom/squareup/activity/refund/RefundErrorCoordinator$ItemizedRefundStage;

    move-result-object v3

    sget-object v4, Lcom/squareup/activity/refund/RefundErrorCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v3}, Lcom/squareup/activity/refund/RefundErrorCoordinator$ItemizedRefundStage;->ordinal()I

    move-result v3

    aget v3, v4, v3

    const/4 v4, 0x2

    const/4 v5, 0x1

    if-eq v3, v5, :cond_2

    if-ne v3, v4, :cond_1

    .line 72
    iget-object v3, p0, Lcom/squareup/activity/refund/RefundErrorCoordinator;->res:Lcom/squareup/util/Res;

    sget v6, Lcom/squareup/activity/R$string;->refund_issue:I

    invoke-interface {v3, v6}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 71
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getTitle()Ljava/lang/String;

    move-result-object v3

    .line 70
    :goto_0
    check-cast v3, Ljava/lang/CharSequence;

    .line 68
    invoke-virtual {v1, v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 75
    invoke-virtual {v1, v5}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 77
    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getRetryable()Z

    move-result v2

    if-ne v2, v5, :cond_3

    .line 78
    new-instance v2, Lcom/squareup/activity/refund/RefundErrorCoordinator$configureView$1;

    iget-object v3, p0, Lcom/squareup/activity/refund/RefundErrorCoordinator;->eventRunner:Lcom/squareup/activity/refund/RefundErrorCoordinator$RefundErrorEventRunner;

    invoke-direct {v2, v3}, Lcom/squareup/activity/refund/RefundErrorCoordinator$configureView$1;-><init>(Lcom/squareup/activity/refund/RefundErrorCoordinator$RefundErrorEventRunner;)V

    goto :goto_1

    :cond_3
    if-nez v2, :cond_10

    .line 79
    new-instance v2, Lcom/squareup/activity/refund/RefundErrorCoordinator$configureView$2;

    iget-object v3, p0, Lcom/squareup/activity/refund/RefundErrorCoordinator;->eventRunner:Lcom/squareup/activity/refund/RefundErrorCoordinator$RefundErrorEventRunner;

    invoke-direct {v2, v3}, Lcom/squareup/activity/refund/RefundErrorCoordinator$configureView$2;-><init>(Lcom/squareup/activity/refund/RefundErrorCoordinator$RefundErrorEventRunner;)V

    .line 77
    :goto_1
    check-cast v2, Lkotlin/jvm/functions/Function0;

    new-instance v3, Lcom/squareup/activity/refund/RefundErrorCoordinator$sam$java_lang_Runnable$0;

    invoke-direct {v3, v2}, Lcom/squareup/activity/refund/RefundErrorCoordinator$sam$java_lang_Runnable$0;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v3, Ljava/lang/Runnable;

    .line 76
    invoke-virtual {v1, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 82
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 84
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundErrorCoordinator;->message:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    const-string v1, "message"

    if-nez v0, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 85
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundErrorCoordinator;->message:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    if-nez v0, :cond_5

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getTitle()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setTitle(Ljava/lang/CharSequence;)V

    .line 86
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundErrorCoordinator;->message:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    if-nez v0, :cond_6

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getBody()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setMessage(Ljava/lang/CharSequence;)V

    .line 88
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundErrorCoordinator;->actionButton:Lcom/squareup/marketfont/MarketButton;

    const-string v1, "actionButton"

    if-nez v0, :cond_7

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/squareup/marketfont/MarketButton;->setVisibility(I)V

    .line 89
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundErrorCoordinator;->actionButton:Lcom/squareup/marketfont/MarketButton;

    if-nez v0, :cond_8

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    invoke-virtual {v0, v5}, Lcom/squareup/marketfont/MarketButton;->setEnabled(Z)V

    .line 91
    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getRetryable()Z

    move-result p1

    if-eqz p1, :cond_b

    .line 92
    iget-object p1, p0, Lcom/squareup/activity/refund/RefundErrorCoordinator;->actionButton:Lcom/squareup/marketfont/MarketButton;

    if-nez p1, :cond_9

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    sget v0, Lcom/squareup/common/strings/R$string;->retry:I

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketButton;->setText(I)V

    .line 93
    iget-object p1, p0, Lcom/squareup/activity/refund/RefundErrorCoordinator;->actionButton:Lcom/squareup/marketfont/MarketButton;

    if-nez p1, :cond_a

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_a
    check-cast p1, Landroid/view/View;

    .line 126
    new-instance v0, Lcom/squareup/activity/refund/RefundErrorCoordinator$configureView$$inlined$onClickDebounced$1;

    invoke-direct {v0, p0}, Lcom/squareup/activity/refund/RefundErrorCoordinator$configureView$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/activity/refund/RefundErrorCoordinator;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_3

    .line 97
    :cond_b
    iget-object p1, p0, Lcom/squareup/activity/refund/RefundErrorCoordinator;->actionButton:Lcom/squareup/marketfont/MarketButton;

    if-nez p1, :cond_c

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 98
    :cond_c
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundErrorCoordinator;->eventRunner:Lcom/squareup/activity/refund/RefundErrorCoordinator$RefundErrorEventRunner;

    invoke-interface {v0}, Lcom/squareup/activity/refund/RefundErrorCoordinator$RefundErrorEventRunner;->itemizedRefundStage()Lcom/squareup/activity/refund/RefundErrorCoordinator$ItemizedRefundStage;

    move-result-object v0

    sget-object v2, Lcom/squareup/activity/refund/RefundErrorCoordinator$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {v0}, Lcom/squareup/activity/refund/RefundErrorCoordinator$ItemizedRefundStage;->ordinal()I

    move-result v0

    aget v0, v2, v0

    if-eq v0, v5, :cond_e

    if-ne v0, v4, :cond_d

    .line 100
    sget v0, Lcom/squareup/cardreader/R$string;->cancel_refund:I

    goto :goto_2

    :cond_d
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 99
    :cond_e
    sget v0, Lcom/squareup/common/strings/R$string;->ok:I

    .line 97
    :goto_2
    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketButton;->setText(I)V

    .line 103
    iget-object p1, p0, Lcom/squareup/activity/refund/RefundErrorCoordinator;->actionButton:Lcom/squareup/marketfont/MarketButton;

    if-nez p1, :cond_f

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_f
    check-cast p1, Landroid/view/View;

    .line 133
    new-instance v0, Lcom/squareup/activity/refund/RefundErrorCoordinator$configureView$$inlined$onClickDebounced$2;

    invoke-direct {v0, p0}, Lcom/squareup/activity/refund/RefundErrorCoordinator$configureView$$inlined$onClickDebounced$2;-><init>(Lcom/squareup/activity/refund/RefundErrorCoordinator;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_3
    return-void

    .line 79
    :cond_10
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 57
    invoke-direct {p0, p1}, Lcom/squareup/activity/refund/RefundErrorCoordinator;->bindViews(Landroid/view/View;)V

    .line 60
    new-instance v0, Lcom/squareup/activity/refund/RefundErrorCoordinator$attach$1;

    invoke-direct {v0, p0}, Lcom/squareup/activity/refund/RefundErrorCoordinator$attach$1;-><init>(Lcom/squareup/activity/refund/RefundErrorCoordinator;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
