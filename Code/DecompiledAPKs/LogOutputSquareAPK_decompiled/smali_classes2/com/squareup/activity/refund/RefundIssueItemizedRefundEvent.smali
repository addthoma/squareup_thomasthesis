.class public final Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;
.super Lcom/squareup/activity/refund/RefundEvent;
.source "ItemizedRefundAnalytics.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0010\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u0000 \u001b2\u00020\u0001:\u0001\u001bB-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0006\u0012\u0006\u0010\u0008\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\tJ\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0006H\u00c6\u0003J\t\u0010\u0012\u001a\u00020\u0006H\u00c6\u0003J\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J;\u0010\u0014\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\u0015\u001a\u00020\u00032\u0008\u0010\u0016\u001a\u0004\u0018\u00010\u0017H\u00d6\u0003J\t\u0010\u0018\u001a\u00020\u0019H\u00d6\u0001J\t\u0010\u001a\u001a\u00020\u0006H\u00d6\u0001R\u0011\u0010\u0008\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0002\u0010\u000bR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0004\u0010\u000bR\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\u0007\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\r\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;",
        "Lcom/squareup/activity/refund/RefundEvent;",
        "is_all_items",
        "",
        "is_all_tips",
        "reason",
        "",
        "tender_types",
        "contains_unit_priced_item",
        "(ZZLjava/lang/String;Ljava/lang/String;Z)V",
        "getContains_unit_priced_item",
        "()Z",
        "getReason",
        "()Ljava/lang/String;",
        "getTender_types",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "Companion",
        "activity_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent$Companion;


# instance fields
.field private final contains_unit_priced_item:Z

.field private final is_all_items:Z

.field private final is_all_tips:Z

.field private final reason:Ljava/lang/String;

.field private final tender_types:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;->Companion:Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent$Companion;

    return-void
.end method

.method public constructor <init>(ZZLjava/lang/String;Ljava/lang/String;Z)V
    .locals 3

    const-string v0, "reason"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tender_types"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    const-string v1, "Itemized Refunds: Issue Itemized Refund"

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/squareup/activity/refund/RefundEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-boolean p1, p0, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;->is_all_items:Z

    iput-boolean p2, p0, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;->is_all_tips:Z

    iput-object p3, p0, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;->reason:Ljava/lang/String;

    iput-object p4, p0, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;->tender_types:Ljava/lang/String;

    iput-boolean p5, p0, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;->contains_unit_priced_item:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;ZZLjava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-boolean p1, p0, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;->is_all_items:Z

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-boolean p2, p0, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;->is_all_tips:Z

    :cond_1
    move p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;->reason:Ljava/lang/String;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;->tender_types:Ljava/lang/String;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget-boolean p5, p0, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;->contains_unit_priced_item:Z

    :cond_4
    move v2, p5

    move-object p2, p0

    move p3, p1

    move p4, p7

    move-object p5, v0

    move-object p6, v1

    move p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;->copy(ZZLjava/lang/String;Ljava/lang/String;Z)Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;

    move-result-object p0

    return-object p0
.end method

.method public static final of(Lcom/squareup/activity/refund/RefundData;)Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;->Companion:Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent$Companion;->of(Lcom/squareup/activity/refund/RefundData;)Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;->is_all_items:Z

    return v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;->is_all_tips:Z

    return v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;->reason:Ljava/lang/String;

    return-object v0
.end method

.method public final component4()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;->tender_types:Ljava/lang/String;

    return-object v0
.end method

.method public final component5()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;->contains_unit_priced_item:Z

    return v0
.end method

.method public final copy(ZZLjava/lang/String;Ljava/lang/String;Z)Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;
    .locals 7

    const-string v0, "reason"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tender_types"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;

    move-object v1, v0

    move v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;-><init>(ZZLjava/lang/String;Ljava/lang/String;Z)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;

    iget-boolean v0, p0, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;->is_all_items:Z

    iget-boolean v1, p1, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;->is_all_items:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;->is_all_tips:Z

    iget-boolean v1, p1, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;->is_all_tips:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;->reason:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;->reason:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;->tender_types:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;->tender_types:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;->contains_unit_priced_item:Z

    iget-boolean p1, p1, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;->contains_unit_priced_item:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getContains_unit_priced_item()Z
    .locals 1

    .line 74
    iget-boolean v0, p0, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;->contains_unit_priced_item:Z

    return v0
.end method

.method public final getReason()Ljava/lang/String;
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;->reason:Ljava/lang/String;

    return-object v0
.end method

.method public final getTender_types()Ljava/lang/String;
    .locals 1

    .line 73
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;->tender_types:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    iget-boolean v0, p0, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;->is_all_items:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;->is_all_tips:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;->reason:Ljava/lang/String;

    const/4 v3, 0x0

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;->tender_types:Ljava/lang/String;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v3

    :cond_3
    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;->contains_unit_priced_item:Z

    if-eqz v2, :cond_4

    goto :goto_1

    :cond_4
    move v1, v2

    :goto_1
    add-int/2addr v0, v1

    return v0
.end method

.method public final is_all_items()Z
    .locals 1

    .line 70
    iget-boolean v0, p0, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;->is_all_items:Z

    return v0
.end method

.method public final is_all_tips()Z
    .locals 1

    .line 71
    iget-boolean v0, p0, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;->is_all_tips:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RefundIssueItemizedRefundEvent(is_all_items="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;->is_all_items:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", is_all_tips="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;->is_all_tips:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", reason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;->reason:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", tender_types="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;->tender_types:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", contains_unit_priced_item="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;->contains_unit_priced_item:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
