.class public final Lcom/squareup/activity/refund/IssueRefundCoordinator$Factory;
.super Ljava/lang/Object;
.source "IssueRefundCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/activity/refund/IssueRefundCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B-\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\u001c\u0010\u000c\u001a\u00020\r2\u000c\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000f2\u0006\u0010\u0011\u001a\u00020\u0012R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/activity/refund/IssueRefundCoordinator$Factory;",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "priceLocaleHelper",
        "Lcom/squareup/money/PriceLocaleHelper;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "errorsBarPresenter",
        "Lcom/squareup/ui/ErrorsBarPresenter;",
        "(Lcom/squareup/util/Res;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/text/Formatter;Lcom/squareup/ui/ErrorsBarPresenter;)V",
        "create",
        "Lcom/squareup/activity/refund/IssueRefundCoordinator;",
        "data",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/activity/refund/RefundData;",
        "eventHandler",
        "Lcom/squareup/activity/refund/IssueRefundCoordinator$IssueRefundEventHandler;",
        "activity_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final errorsBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/text/Formatter;Lcom/squareup/ui/ErrorsBarPresenter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/money/PriceLocaleHelper;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "priceLocaleHelper"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "errorsBarPresenter"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator$Factory;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator$Factory;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iput-object p3, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p4, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator$Factory;->errorsBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    return-void
.end method


# virtual methods
.method public final create(Lio/reactivex/Observable;Lcom/squareup/activity/refund/IssueRefundCoordinator$IssueRefundEventHandler;)Lcom/squareup/activity/refund/IssueRefundCoordinator;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/activity/refund/RefundData;",
            ">;",
            "Lcom/squareup/activity/refund/IssueRefundCoordinator$IssueRefundEventHandler;",
            ")",
            "Lcom/squareup/activity/refund/IssueRefundCoordinator;"
        }
    .end annotation

    const-string v0, "data"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "eventHandler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 108
    new-instance v0, Lcom/squareup/activity/refund/IssueRefundCoordinator;

    .line 109
    iget-object v2, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator$Factory;->res:Lcom/squareup/util/Res;

    iget-object v5, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator$Factory;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iget-object v6, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 110
    iget-object v7, p0, Lcom/squareup/activity/refund/IssueRefundCoordinator$Factory;->errorsBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    move-object v1, v0

    move-object v3, p1

    move-object v4, p2

    .line 108
    invoke-direct/range {v1 .. v7}, Lcom/squareup/activity/refund/IssueRefundCoordinator;-><init>(Lcom/squareup/util/Res;Lio/reactivex/Observable;Lcom/squareup/activity/refund/IssueRefundCoordinator$IssueRefundEventHandler;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/text/Formatter;Lcom/squareup/ui/ErrorsBarPresenter;)V

    return-object v0
.end method
