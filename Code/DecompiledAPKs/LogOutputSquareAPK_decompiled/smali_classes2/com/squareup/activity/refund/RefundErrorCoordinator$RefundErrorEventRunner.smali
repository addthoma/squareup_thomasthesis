.class public interface abstract Lcom/squareup/activity/refund/RefundErrorCoordinator$RefundErrorEventRunner;
.super Ljava/lang/Object;
.source "RefundErrorCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/activity/refund/RefundErrorCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "RefundErrorEventRunner"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008f\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H&J\u0008\u0010\u0004\u001a\u00020\u0003H&J\u000e\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006H&J\u0008\u0010\u0008\u001a\u00020\tH&J\u0008\u0010\n\u001a\u00020\u0003H&\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/activity/refund/RefundErrorCoordinator$RefundErrorEventRunner;",
        "",
        "cancelAfterError",
        "",
        "dismissAfterClientError",
        "failureMessage",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/receiving/FailureMessage;",
        "itemizedRefundStage",
        "Lcom/squareup/activity/refund/RefundErrorCoordinator$ItemizedRefundStage;",
        "retryAfterClientError",
        "activity_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract cancelAfterError()V
.end method

.method public abstract dismissAfterClientError()V
.end method

.method public abstract failureMessage()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/receiving/FailureMessage;",
            ">;"
        }
    .end annotation
.end method

.method public abstract itemizedRefundStage()Lcom/squareup/activity/refund/RefundErrorCoordinator$ItemizedRefundStage;
.end method

.method public abstract retryAfterClientError()V
.end method
