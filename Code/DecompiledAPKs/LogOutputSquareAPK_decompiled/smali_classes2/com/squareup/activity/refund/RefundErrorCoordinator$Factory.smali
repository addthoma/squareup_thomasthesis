.class public final Lcom/squareup/activity/refund/RefundErrorCoordinator$Factory;
.super Ljava/lang/Object;
.source "RefundErrorCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/activity/refund/RefundErrorCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u001c\u0010\u0005\u001a\u00020\u00062\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u00082\u0006\u0010\n\u001a\u00020\u000bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/activity/refund/RefundErrorCoordinator$Factory;",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/util/Res;)V",
        "create",
        "Lcom/squareup/activity/refund/RefundErrorCoordinator;",
        "data",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/receiving/FailureMessage;",
        "eventRunner",
        "Lcom/squareup/activity/refund/RefundErrorCoordinator$RefundErrorEventRunner;",
        "activity_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/activity/refund/RefundErrorCoordinator$Factory;->res:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public final create(Lio/reactivex/Observable;Lcom/squareup/activity/refund/RefundErrorCoordinator$RefundErrorEventRunner;)Lcom/squareup/activity/refund/RefundErrorCoordinator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/receiving/FailureMessage;",
            ">;",
            "Lcom/squareup/activity/refund/RefundErrorCoordinator$RefundErrorEventRunner;",
            ")",
            "Lcom/squareup/activity/refund/RefundErrorCoordinator;"
        }
    .end annotation

    const-string v0, "data"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "eventRunner"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    new-instance v0, Lcom/squareup/activity/refund/RefundErrorCoordinator;

    iget-object v1, p0, Lcom/squareup/activity/refund/RefundErrorCoordinator$Factory;->res:Lcom/squareup/util/Res;

    invoke-direct {v0, v1, p1, p2}, Lcom/squareup/activity/refund/RefundErrorCoordinator;-><init>(Lcom/squareup/util/Res;Lio/reactivex/Observable;Lcom/squareup/activity/refund/RefundErrorCoordinator$RefundErrorEventRunner;)V

    return-object v0
.end method
