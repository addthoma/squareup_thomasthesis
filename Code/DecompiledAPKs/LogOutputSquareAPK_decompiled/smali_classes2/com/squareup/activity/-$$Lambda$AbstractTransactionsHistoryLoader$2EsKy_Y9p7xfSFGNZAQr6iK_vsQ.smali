.class public final synthetic Lcom/squareup/activity/-$$Lambda$AbstractTransactionsHistoryLoader$2EsKy_Y9p7xfSFGNZAQr6iK_vsQ;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lio/reactivex/functions/Function;


# instance fields
.field private final synthetic f$0:Lcom/squareup/activity/AbstractTransactionsHistoryLoader;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/activity/AbstractTransactionsHistoryLoader;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/activity/-$$Lambda$AbstractTransactionsHistoryLoader$2EsKy_Y9p7xfSFGNZAQr6iK_vsQ;->f$0:Lcom/squareup/activity/AbstractTransactionsHistoryLoader;

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/squareup/activity/-$$Lambda$AbstractTransactionsHistoryLoader$2EsKy_Y9p7xfSFGNZAQr6iK_vsQ;->f$0:Lcom/squareup/activity/AbstractTransactionsHistoryLoader;

    check-cast p1, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;

    invoke-static {v0, p1}, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->lambda$2EsKy_Y9p7xfSFGNZAQr6iK_vsQ(Lcom/squareup/activity/AbstractTransactionsHistoryLoader;Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;)Lio/reactivex/Observable;

    move-result-object p1

    check-cast p1, Lio/reactivex/ObservableSource;

    return-object p1
.end method
