.class public Lcom/squareup/activity/ExpiryCalculator;
.super Ljava/lang/Object;
.source "ExpiryCalculator.java"


# static fields
.field static final STORED_TRANSACTION_EXPIRES_SOON_THRESHOLD_MS:J


# instance fields
.field private final accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final clock:Lcom/squareup/util/Clock;

.field private final paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 27
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x1

    .line 28
    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/squareup/activity/ExpiryCalculator;->STORED_TRANSACTION_EXPIRES_SOON_THRESHOLD_MS:J

    return-void
.end method

.method constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/util/Clock;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/squareup/activity/ExpiryCalculator;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 37
    iput-object p2, p0, Lcom/squareup/activity/ExpiryCalculator;->paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

    .line 38
    iput-object p3, p0, Lcom/squareup/activity/ExpiryCalculator;->clock:Lcom/squareup/util/Clock;

    return-void
.end method

.method private currentlyInExpirationWindow(J)Z
    .locals 4

    .line 71
    iget-object v0, p0, Lcom/squareup/activity/ExpiryCalculator;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 72
    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getStoreAndForwardSettings()Lcom/squareup/settings/server/StoreAndForwardSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/StoreAndForwardSettings;->getPaymentExpirationMillis()J

    move-result-wide v0

    add-long/2addr p1, v0

    .line 73
    iget-object v0, p0, Lcom/squareup/activity/ExpiryCalculator;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v0

    sget-wide v2, Lcom/squareup/activity/ExpiryCalculator;->STORED_TRANSACTION_EXPIRES_SOON_THRESHOLD_MS:J

    sub-long/2addr p1, v2

    cmp-long v2, v0, p1

    if-ltz v2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private isTipExpiringSoon(Lcom/squareup/billhistory/model/BillHistory;)Z
    .locals 1

    .line 84
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/BillHistory;->getTenders()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/billhistory/model/TenderHistory;

    .line 85
    invoke-virtual {p0, v0}, Lcom/squareup/activity/ExpiryCalculator;->isTipExpiringSoon(Lcom/squareup/billhistory/model/TenderHistory;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method


# virtual methods
.method public isExpiringSoon(Lcom/squareup/billhistory/model/BillHistory;)Z
    .locals 1

    .line 42
    invoke-virtual {p0, p1}, Lcom/squareup/activity/ExpiryCalculator;->isStoredPaymentExpiringSoon(Lcom/squareup/billhistory/model/BillHistory;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0, p1}, Lcom/squareup/activity/ExpiryCalculator;->isTipExpiringSoon(Lcom/squareup/billhistory/model/BillHistory;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public isExpiringSoon(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;)Z
    .locals 1

    .line 46
    invoke-virtual {p0, p1}, Lcom/squareup/activity/ExpiryCalculator;->isStoredTransactionExpiringSoon(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;->getHasExpiringTip()Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public isStoredPaymentExpiringSoon(Lcom/squareup/billhistory/model/BillHistory;)Z
    .locals 2

    .line 50
    iget-boolean v0, p1, Lcom/squareup/billhistory/model/BillHistory;->pending:Z

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/squareup/billhistory/model/BillHistory;->storeAndForward:Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    sget-object v1, Lcom/squareup/transactionhistory/pending/StoreAndForwardState;->FORWARDED:Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/squareup/billhistory/model/BillHistory;->hasCardTender()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51
    iget-object p1, p1, Lcom/squareup/billhistory/model/BillHistory;->time:Ljava/util/Date;

    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/squareup/activity/ExpiryCalculator;->currentlyInExpirationWindow(J)Z

    move-result p1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public isStoredTransactionExpiringSoon(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;)Z
    .locals 2

    .line 57
    invoke-static {p1}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummaryKt;->isPending(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    invoke-virtual {p1}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;->getStoreAndForwardState()Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    move-result-object v0

    sget-object v1, Lcom/squareup/transactionhistory/pending/StoreAndForwardState;->FORWARDED:Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    if-eq v0, v1, :cond_0

    .line 59
    invoke-static {p1}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummaryKt;->hasCardTender(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    invoke-virtual {p1}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;->getDate()Ljava/util/Date;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/squareup/activity/ExpiryCalculator;->currentlyInExpirationWindow(J)Z

    move-result p1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public isTipExpiringSoon(Lcom/squareup/billhistory/model/TenderHistory;)Z
    .locals 5

    .line 77
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/TenderHistory;->isUnsettledCardTender()Z

    move-result v0

    .line 78
    iget-object p1, p1, Lcom/squareup/billhistory/model/TenderHistory;->timestamp:Ljava/util/Date;

    .line 79
    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    iget-object p1, p0, Lcom/squareup/activity/ExpiryCalculator;->paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

    invoke-virtual {p1}, Lcom/squareup/papersignature/PaperSignatureSettings;->getTipWarnPeriodMillis()J

    move-result-wide v3

    add-long/2addr v1, v3

    if-eqz v0, :cond_0

    .line 80
    iget-object p1, p0, Lcom/squareup/activity/ExpiryCalculator;->clock:Lcom/squareup/util/Clock;

    invoke-interface {p1}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v3

    cmp-long p1, v3, v1

    if-ltz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
