.class public final Lcom/squareup/activity/LegacyTransactionsHistory_Factory;
.super Ljava/lang/Object;
.source "LegacyTransactionsHistory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/activity/LegacyTransactionsHistory;",
        ">;"
    }
.end annotation


# instance fields
.field private final mainLoaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/AllTransactionsHistoryLoader;",
            ">;"
        }
    .end annotation
.end field

.field private final searchResultsLoaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/SearchResultsLoader;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/AllTransactionsHistoryLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/SearchResultsLoader;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/activity/LegacyTransactionsHistory_Factory;->mainLoaderProvider:Ljavax/inject/Provider;

    .line 24
    iput-object p2, p0, Lcom/squareup/activity/LegacyTransactionsHistory_Factory;->searchResultsLoaderProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/activity/LegacyTransactionsHistory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/AllTransactionsHistoryLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/SearchResultsLoader;",
            ">;)",
            "Lcom/squareup/activity/LegacyTransactionsHistory_Factory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/activity/LegacyTransactionsHistory_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/activity/LegacyTransactionsHistory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/activity/AllTransactionsHistoryLoader;Lcom/squareup/activity/SearchResultsLoader;)Lcom/squareup/activity/LegacyTransactionsHistory;
    .locals 1

    .line 40
    new-instance v0, Lcom/squareup/activity/LegacyTransactionsHistory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/activity/LegacyTransactionsHistory;-><init>(Lcom/squareup/activity/AllTransactionsHistoryLoader;Lcom/squareup/activity/SearchResultsLoader;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/activity/LegacyTransactionsHistory;
    .locals 2

    .line 29
    iget-object v0, p0, Lcom/squareup/activity/LegacyTransactionsHistory_Factory;->mainLoaderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/activity/AllTransactionsHistoryLoader;

    iget-object v1, p0, Lcom/squareup/activity/LegacyTransactionsHistory_Factory;->searchResultsLoaderProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/activity/SearchResultsLoader;

    invoke-static {v0, v1}, Lcom/squareup/activity/LegacyTransactionsHistory_Factory;->newInstance(Lcom/squareup/activity/AllTransactionsHistoryLoader;Lcom/squareup/activity/SearchResultsLoader;)Lcom/squareup/activity/LegacyTransactionsHistory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/activity/LegacyTransactionsHistory_Factory;->get()Lcom/squareup/activity/LegacyTransactionsHistory;

    move-result-object v0

    return-object v0
.end method
