.class public final Lcom/squareup/AppBootstrapModule_ProvideAppDelegateFactory;
.super Ljava/lang/Object;
.source "AppBootstrapModule_ProvideAppDelegateFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/AppDelegate;",
        ">;"
    }
.end annotation


# instance fields
.field private final module:Lcom/squareup/AppBootstrapModule;


# direct methods
.method public constructor <init>(Lcom/squareup/AppBootstrapModule;)V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/squareup/AppBootstrapModule_ProvideAppDelegateFactory;->module:Lcom/squareup/AppBootstrapModule;

    return-void
.end method

.method public static create(Lcom/squareup/AppBootstrapModule;)Lcom/squareup/AppBootstrapModule_ProvideAppDelegateFactory;
    .locals 1

    .line 28
    new-instance v0, Lcom/squareup/AppBootstrapModule_ProvideAppDelegateFactory;

    invoke-direct {v0, p0}, Lcom/squareup/AppBootstrapModule_ProvideAppDelegateFactory;-><init>(Lcom/squareup/AppBootstrapModule;)V

    return-object v0
.end method

.method public static provideAppDelegate(Lcom/squareup/AppBootstrapModule;)Lcom/squareup/AppDelegate;
    .locals 1

    .line 32
    invoke-virtual {p0}, Lcom/squareup/AppBootstrapModule;->provideAppDelegate()Lcom/squareup/AppDelegate;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/AppDelegate;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/AppDelegate;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/AppBootstrapModule_ProvideAppDelegateFactory;->module:Lcom/squareup/AppBootstrapModule;

    invoke-static {v0}, Lcom/squareup/AppBootstrapModule_ProvideAppDelegateFactory;->provideAppDelegate(Lcom/squareup/AppBootstrapModule;)Lcom/squareup/AppDelegate;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/AppBootstrapModule_ProvideAppDelegateFactory;->get()Lcom/squareup/AppDelegate;

    move-result-object v0

    return-object v0
.end method
