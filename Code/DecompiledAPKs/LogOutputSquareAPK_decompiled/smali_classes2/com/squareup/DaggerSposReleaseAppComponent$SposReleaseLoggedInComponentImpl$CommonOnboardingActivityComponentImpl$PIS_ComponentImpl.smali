.class final Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl$PIS_ComponentImpl;
.super Ljava/lang/Object;
.source "DaggerSposReleaseAppComponent.java"

# interfaces
.implements Lcom/squareup/ui/onboarding/PersonalInfoScreen$Component;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "PIS_ComponentImpl"
.end annotation


# instance fields
.field private final activationCallModule:Lcom/squareup/ui/onboarding/ActivationCallModule;

.field private personalInfoPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/PersonalInfoPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private provideApplyReceivedResponseProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/ShareableReceivedResponse<",
            "Lcom/squareup/server/activation/ApplyBody;",
            "Lcom/squareup/server/SimpleResponse;",
            ">;>;"
        }
    .end annotation
.end field

.field private provideApplyServerCallPresenterProvider:Ljavax/inject/Provider;

.field private provideSsnStringsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/SSNStrings;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;


# direct methods
.method private constructor <init>(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;)V
    .locals 0

    .line 10819
    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl$PIS_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10820
    new-instance p1, Lcom/squareup/ui/onboarding/ActivationCallModule;

    invoke-direct {p1}, Lcom/squareup/ui/onboarding/ActivationCallModule;-><init>()V

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl$PIS_ComponentImpl;->activationCallModule:Lcom/squareup/ui/onboarding/ActivationCallModule;

    .line 10821
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl$PIS_ComponentImpl;->initialize()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;Lcom/squareup/DaggerSposReleaseAppComponent$1;)V
    .locals 0

    .line 10807
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl$PIS_ComponentImpl;-><init>(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;)V

    return-void
.end method

.method private getSSNStrings()Lcom/squareup/ui/onboarding/SSNStrings;
    .locals 2

    .line 10825
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl$PIS_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$5400(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/CountryCode;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl$PIS_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$4400(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljava/util/Locale;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/ui/onboarding/PersonalInfoScreen_Module_ProvideSsnStringsFactory;->provideSsnStrings(Lcom/squareup/CountryCode;Ljava/util/Locale;)Lcom/squareup/ui/onboarding/SSNStrings;

    move-result-object v0

    return-object v0
.end method

.method private initialize()V
    .locals 13

    .line 10829
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl$PIS_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$10700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl$PIS_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$1700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/ui/onboarding/PersonalInfoScreen_Module_ProvideSsnStringsFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/onboarding/PersonalInfoScreen_Module_ProvideSsnStringsFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl$PIS_ComponentImpl;->provideSsnStringsProvider:Ljavax/inject/Provider;

    .line 10830
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl$PIS_ComponentImpl;->activationCallModule:Lcom/squareup/ui/onboarding/ActivationCallModule;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl$PIS_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$17800(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/ui/onboarding/ActivationCallModule_ProvideApplyReceivedResponseFactory;->create(Lcom/squareup/ui/onboarding/ActivationCallModule;Ljavax/inject/Provider;)Lcom/squareup/ui/onboarding/ActivationCallModule_ProvideApplyReceivedResponseFactory;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl$PIS_ComponentImpl;->provideApplyReceivedResponseProvider:Ljavax/inject/Provider;

    .line 10831
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl$PIS_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$6800(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl$PIS_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$17800(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl$PIS_ComponentImpl;->provideApplyReceivedResponseProvider:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl$PIS_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$4300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v4

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl$PIS_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;->access$20900(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v5

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl$PIS_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;->access$21500(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v6

    invoke-static/range {v1 .. v6}, Lcom/squareup/ui/onboarding/ActivationCallModule_ProvideApplyServerCallPresenterFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/onboarding/ActivationCallModule_ProvideApplyServerCallPresenterFactory;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl$PIS_ComponentImpl;->provideApplyServerCallPresenterProvider:Ljavax/inject/Provider;

    .line 10832
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl$PIS_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$4300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl$PIS_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;->access$20700(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl$PIS_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;->access$20800(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v3

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl$PIS_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;->access$20900(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v4

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl$PIS_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$14900(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v5

    iget-object v6, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl$PIS_ComponentImpl;->provideSsnStringsProvider:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl$PIS_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$5600(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v7

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl$PIS_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$8500(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v8

    iget-object v9, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl$PIS_ComponentImpl;->provideApplyReceivedResponseProvider:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl$PIS_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v10

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl$PIS_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$14200(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v11

    iget-object v12, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl$PIS_ComponentImpl;->provideApplyServerCallPresenterProvider:Ljavax/inject/Provider;

    invoke-static/range {v1 .. v12}, Lcom/squareup/ui/onboarding/PersonalInfoPresenter_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/onboarding/PersonalInfoPresenter_Factory;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl$PIS_ComponentImpl;->personalInfoPresenterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method private injectAddressLayout(Lcom/squareup/address/AddressLayout;)Lcom/squareup/address/AddressLayout;
    .locals 1

    .line 10844
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl$PIS_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;->access$21200(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/address/AddressLayoutRunner;

    invoke-static {p1, v0}, Lcom/squareup/address/AddressLayout_MembersInjector;->injectRunner(Lcom/squareup/address/AddressLayout;Lcom/squareup/address/AddressLayoutRunner;)V

    return-object p1
.end method

.method private injectPersonalInfoView(Lcom/squareup/ui/onboarding/PersonalInfoView;)Lcom/squareup/ui/onboarding/PersonalInfoView;
    .locals 1

    .line 10849
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl$PIS_ComponentImpl;->personalInfoPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/PersonalInfoPresenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/onboarding/PersonalInfoView_MembersInjector;->injectPresenter(Lcom/squareup/ui/onboarding/PersonalInfoView;Lcom/squareup/ui/onboarding/PersonalInfoPresenter;)V

    .line 10850
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl$PIS_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$21300(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/text/scrubber/PhoneNumberScrubber;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/onboarding/PersonalInfoView_MembersInjector;->injectPhoneNumberScrubber(Lcom/squareup/ui/onboarding/PersonalInfoView;Lcom/squareup/text/InsertingScrubber;)V

    .line 10851
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl$PIS_ComponentImpl;->getSSNStrings()Lcom/squareup/ui/onboarding/SSNStrings;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/onboarding/PersonalInfoView_MembersInjector;->injectSsnStrings(Lcom/squareup/ui/onboarding/PersonalInfoView;Lcom/squareup/ui/onboarding/SSNStrings;)V

    return-object p1
.end method


# virtual methods
.method public inject(Lcom/squareup/address/AddressLayout;)V
    .locals 0

    .line 10837
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl$PIS_ComponentImpl;->injectAddressLayout(Lcom/squareup/address/AddressLayout;)Lcom/squareup/address/AddressLayout;

    return-void
.end method

.method public inject(Lcom/squareup/ui/onboarding/PersonalInfoView;)V
    .locals 0

    .line 10841
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$CommonOnboardingActivityComponentImpl$PIS_ComponentImpl;->injectPersonalInfoView(Lcom/squareup/ui/onboarding/PersonalInfoView;)Lcom/squareup/ui/onboarding/PersonalInfoView;

    return-void
.end method
