.class public interface abstract Lcom/squareup/balance/core/server/transfers/TransfersService;
.super Ljava/lang/Object;
.source "TransfersService.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/core/server/transfers/TransfersService$CreateTransferStandardResponse;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008f\u0018\u00002\u00020\u0001:\u0001\u0006J\u0012\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005H\'\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/balance/core/server/transfers/TransfersService;",
        "",
        "createTransfer",
        "Lcom/squareup/balance/core/server/transfers/TransfersService$CreateTransferStandardResponse;",
        "request",
        "Lcom/squareup/protos/deposits/CreateTransferRequest;",
        "CreateTransferStandardResponse",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract createTransfer(Lcom/squareup/protos/deposits/CreateTransferRequest;)Lcom/squareup/balance/core/server/transfers/TransfersService$CreateTransferStandardResponse;
    .param p1    # Lcom/squareup/protos/deposits/CreateTransferRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/deposits/create-transfer"
    .end annotation
.end method
