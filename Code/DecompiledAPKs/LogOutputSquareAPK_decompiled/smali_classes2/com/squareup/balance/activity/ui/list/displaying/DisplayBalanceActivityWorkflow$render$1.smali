.class final Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$render$1;
.super Lkotlin/jvm/internal/Lambda;
.source "DisplayBalanceActivityWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow;->render(Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityProps;Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDisplayBalanceActivityWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DisplayBalanceActivityWorkflow.kt\ncom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$render$1\n*L\n1#1,186:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "activity",
        "Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $sink:Lcom/squareup/workflow/Sink;

.field final synthetic $state:Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/Sink;Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$render$1;->$sink:Lcom/squareup/workflow/Sink;

    iput-object p2, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$render$1;->$state:Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 34
    check-cast p1, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$render$1;->invoke(Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity;)V
    .locals 3

    const-string v0, "activity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    instance-of v0, p1, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    :cond_0
    check-cast p1, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;

    if-eqz p1, :cond_1

    .line 93
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$render$1;->$sink:Lcom/squareup/workflow/Sink;

    new-instance v1, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$Action$ActivityTapped;

    iget-object v2, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$render$1;->$state:Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState;

    invoke-virtual {p1}, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;->getUnifiedActivity()Lcom/squareup/protos/bizbank/UnifiedActivity;

    move-result-object p1

    invoke-direct {v1, v2, p1}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$Action$ActivityTapped;-><init>(Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState;Lcom/squareup/protos/bizbank/UnifiedActivity;)V

    invoke-interface {v0, v1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    :cond_1
    return-void
.end method
