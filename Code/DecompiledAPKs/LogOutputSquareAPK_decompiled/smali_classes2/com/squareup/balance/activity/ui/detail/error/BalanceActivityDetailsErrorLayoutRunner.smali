.class public final Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorLayoutRunner;
.super Ljava/lang/Object;
.source "BalanceActivityDetailsErrorLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorLayoutRunner$Binding;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorScreen;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBalanceActivityDetailsErrorLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BalanceActivityDetailsErrorLayoutRunner.kt\ncom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorLayoutRunner\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,58:1\n1103#2,7:59\n*E\n*S KotlinDebug\n*F\n+ 1 BalanceActivityDetailsErrorLayoutRunner.kt\ncom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorLayoutRunner\n*L\n29#1,7:59\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0012B\u000f\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0018\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00022\u0006\u0010\u0010\u001a\u00020\u0011H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0008\u001a\n \n*\u0004\u0018\u00010\t0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorScreen;",
        "view",
        "Landroid/view/View;",
        "(Landroid/view/View;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "retryButton",
        "Landroid/widget/Button;",
        "kotlin.jvm.PlatformType",
        "showBackButton",
        "",
        "showRendering",
        "",
        "rendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "Binding",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final retryButton:Landroid/widget/Button;

.field private final showBackButton:Z

.field private final view:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorLayoutRunner;->view:Landroid/view/View;

    .line 21
    sget-object p1, Lcom/squareup/noho/NohoActionBar;->Companion:Lcom/squareup/noho/NohoActionBar$Companion;

    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoActionBar$Companion;->findIn(Landroid/view/View;)Lcom/squareup/noho/NohoActionBar;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 22
    iget-object p1, p0, Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/balance/activity/impl/R$id;->balance_activity_details_retry_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorLayoutRunner;->retryButton:Landroid/widget/Button;

    .line 23
    iget-object p1, p0, Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorLayoutRunner;->view:Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/container/ContainerKt;->getScreenSize(Landroid/view/View;)Lcom/squareup/util/DeviceScreenSizeInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/util/DeviceScreenSizeInfo;->isMasterDetail()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    iput-boolean p1, p0, Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorLayoutRunner;->showBackButton:Z

    return-void
.end method

.method public static final synthetic access$getRetryButton$p(Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorLayoutRunner;)Landroid/widget/Button;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorLayoutRunner;->retryButton:Landroid/widget/Button;

    return-object p0
.end method


# virtual methods
.method public showRendering(Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 3

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    iget-object p2, p0, Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorLayoutRunner;->retryButton:Landroid/widget/Button;

    check-cast p2, Landroid/view/View;

    .line 59
    new-instance v0, Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorLayoutRunner$showRendering$$inlined$onClickDebounced$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorLayoutRunner$showRendering$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorLayoutRunner;Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorScreen;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 45
    iget-object p2, p0, Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 36
    invoke-virtual {p2}, Lcom/squareup/noho/NohoActionBar;->getConfig()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config;->buildUpon()Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 37
    invoke-virtual {p1}, Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorScreen;->getTitle()Lcom/squareup/util/ViewString;

    move-result-object v1

    check-cast v1, Lcom/squareup/resources/TextModel;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 39
    iget-boolean v1, p0, Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorLayoutRunner;->showBackButton:Z

    if-eqz v1, :cond_0

    .line 40
    sget-object v1, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    new-instance v2, Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorLayoutRunner$showRendering$$inlined$apply$lambda$1;

    invoke-direct {v2, p0, p1}, Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorLayoutRunner$showRendering$$inlined$apply$lambda$1;-><init>(Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorLayoutRunner;Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorScreen;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    goto :goto_0

    .line 42
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->hideUpButton()Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 45
    :goto_0
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 46
    iget-object p2, p0, Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorLayoutRunner;->view:Landroid/view/View;

    new-instance v0, Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorLayoutRunner$showRendering$3;

    invoke-direct {v0, p1}, Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorLayoutRunner$showRendering$3;-><init>(Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 18
    check-cast p1, Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorScreen;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorLayoutRunner;->showRendering(Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorScreen;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
