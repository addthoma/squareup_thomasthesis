.class final Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDataStore$Companion;
.super Ljava/lang/Object;
.source "RealRemoteBalanceActivityDataStore.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDataStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0008\n\u0000\u0008\u0082\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0017\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007R\u0017\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\u0007R\u000e\u0010\n\u001a\u00020\u000bX\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDataStore$Companion;",
        "",
        "()V",
        "BALANCE_ACTIVITY_CARD_SPEND",
        "",
        "Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;",
        "getBALANCE_ACTIVITY_CARD_SPEND",
        "()Ljava/util/List;",
        "BALANCE_ACTIVITY_TRANSFERS",
        "getBALANCE_ACTIVITY_TRANSFERS",
        "DEFAULT_BATCH_SIZE",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 104
    invoke-direct {p0}, Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDataStore$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final getBALANCE_ACTIVITY_CARD_SPEND()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;",
            ">;"
        }
    .end annotation

    .line 105
    invoke-static {}, Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDataStore;->access$getBALANCE_ACTIVITY_CARD_SPEND$cp()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final getBALANCE_ACTIVITY_TRANSFERS()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;",
            ">;"
        }
    .end annotation

    .line 110
    invoke-static {}, Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDataStore;->access$getBALANCE_ACTIVITY_TRANSFERS$cp()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
