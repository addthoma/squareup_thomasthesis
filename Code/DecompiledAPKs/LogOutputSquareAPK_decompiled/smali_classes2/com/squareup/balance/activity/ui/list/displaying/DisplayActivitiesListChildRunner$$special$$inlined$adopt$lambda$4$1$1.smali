.class final Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$adopt$lambda$4$1$1;
.super Ljava/lang/Object;
.source "DisplayActivitiesListChildRunner.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$adopt$lambda$4$1;->invoke(ILjava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00b8\u0001\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0006\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005\u00a8\u0006\t"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Landroid/view/View;",
        "kotlin.jvm.PlatformType",
        "onClick",
        "com/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$1$5$1$1$1",
        "com/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$bind$5$lambda$1",
        "com/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$create$4$lambda$1$1",
        "com/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$row$lambda$3$1$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$adopt$lambda$4$1;


# direct methods
.method constructor <init>(Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$adopt$lambda$4$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$adopt$lambda$4$1$1;->this$0:Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$adopt$lambda$4$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 0

    .line 145
    iget-object p1, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$adopt$lambda$4$1$1;->this$0:Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$adopt$lambda$4$1;

    iget-object p1, p1, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$adopt$lambda$4$1;->this$0:Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$adopt$lambda$4;

    iget-object p1, p1, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$$special$$inlined$adopt$lambda$4;->this$0:Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner;

    invoke-static {p1}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner;->access$getLastScreen$p(Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner;)Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildScreen;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildScreen;->getOnOpenTransferReports()Lkotlin/jvm/functions/Function0;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/Unit;

    :cond_0
    return-void
.end method
