.class final Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository$balanceActivityDetails$1;
.super Ljava/lang/Object;
.source "RealBalanceActivityDetailsRepository.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository;->balanceActivityDetails(Ljava/lang/String;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/balance/activity/data/BalanceActivityDetails;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "details",
        "Lcom/squareup/balance/activity/data/BalanceActivityDetails;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository;


# direct methods
.method constructor <init>(Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository$balanceActivityDetails$1;->this$0:Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/balance/activity/data/BalanceActivityDetails;)V
    .locals 1

    .line 25
    instance-of v0, p1, Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;

    if-eqz v0, :cond_0

    .line 26
    iget-object v0, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository$balanceActivityDetails$1;->this$0:Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository;

    check-cast p1, Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;

    invoke-static {v0, p1}, Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository;->access$addToLocalCache(Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository;Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;)V

    :cond_0
    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 10
    check-cast p1, Lcom/squareup/balance/activity/data/BalanceActivityDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository$balanceActivityDetails$1;->accept(Lcom/squareup/balance/activity/data/BalanceActivityDetails;)V

    return-void
.end method
