.class public final Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorWorkflow_Factory;
.super Ljava/lang/Object;
.source "BalanceActivityDetailsErrorWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;",
            ">;)V"
        }
    .end annotation

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorWorkflow_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;",
            ">;)",
            "Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorWorkflow_Factory;"
        }
    .end annotation

    .line 27
    new-instance v0, Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorWorkflow_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorWorkflow_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;)Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorWorkflow;
    .locals 1

    .line 32
    new-instance v0, Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorWorkflow;

    invoke-direct {v0, p0}, Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorWorkflow;-><init>(Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorWorkflow;
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;

    invoke-static {v0}, Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorWorkflow_Factory;->newInstance(Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;)Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorWorkflow_Factory;->get()Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorWorkflow;

    move-result-object v0

    return-object v0
.end method
