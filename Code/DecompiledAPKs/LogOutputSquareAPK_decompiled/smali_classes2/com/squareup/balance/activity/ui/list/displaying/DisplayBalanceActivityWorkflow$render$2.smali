.class final Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$render$2;
.super Lkotlin/jvm/internal/Lambda;
.source "DisplayBalanceActivityWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow;->render(Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityProps;Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $sink:Lcom/squareup/workflow/Sink;

.field final synthetic $state:Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState;


# direct methods
.method constructor <init>(Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState;Lcom/squareup/workflow/Sink;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$render$2;->$state:Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState;

    iput-object p2, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$render$2;->$sink:Lcom/squareup/workflow/Sink;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 34
    invoke-virtual {p0}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$render$2;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 3

    .line 97
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$render$2;->$state:Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState;

    instance-of v1, v0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState$ShowingBalanceActivities;

    if-eqz v1, :cond_0

    .line 98
    iget-object v1, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$render$2;->$sink:Lcom/squareup/workflow/Sink;

    new-instance v2, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$Action$LoadMoreActivity;

    check-cast v0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState$ShowingBalanceActivities;

    invoke-direct {v2, v0}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$Action$LoadMoreActivity;-><init>(Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState$ShowingBalanceActivities;)V

    invoke-interface {v1, v2}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method
