.class public final Lcom/squareup/balance/activity/ui/list/DisplayActivitiesRunner;
.super Ljava/lang/Object;
.source "DisplayActivitiesRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/activity/ui/list/DisplayActivitiesRunner$TabSelectedListener;,
        Lcom/squareup/balance/activity/ui/list/DisplayActivitiesRunner$Binding;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/balance/activity/ui/list/DisplayActivitiesScreen;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDisplayActivitiesRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DisplayActivitiesRunner.kt\ncom/squareup/balance/activity/ui/list/DisplayActivitiesRunner\n*L\n1#1,97:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\u0017\u0018B\u000f\u0008\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0018\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00022\u0006\u0010\u0014\u001a\u00020\u0015H\u0016J\u0010\u0010\u0016\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0002H\u0002R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0008\u001a\n \n*\u0004\u0018\u00010\t0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/balance/activity/ui/list/DisplayActivitiesRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/balance/activity/ui/list/DisplayActivitiesScreen;",
        "view",
        "Landroid/view/View;",
        "(Landroid/view/View;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "childContainer",
        "Lcom/squareup/workflow/ui/WorkflowViewStub;",
        "kotlin.jvm.PlatformType",
        "showBackButton",
        "",
        "tabLayout",
        "Lcom/google/android/material/tabs/TabLayout;",
        "tabListener",
        "Lcom/squareup/balance/activity/ui/list/DisplayActivitiesRunner$TabSelectedListener;",
        "showRendering",
        "",
        "rendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "updateActionBar",
        "Binding",
        "TabSelectedListener",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final childContainer:Lcom/squareup/workflow/ui/WorkflowViewStub;

.field private final showBackButton:Z

.field private final tabLayout:Lcom/google/android/material/tabs/TabLayout;

.field private tabListener:Lcom/squareup/balance/activity/ui/list/DisplayActivitiesRunner$TabSelectedListener;


# direct methods
.method private constructor <init>(Landroid/view/View;)V
    .locals 3

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    sget v0, Lcom/squareup/balance/activity/impl/R$id;->balance_activity_tab_layout:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.b\u2026ance_activity_tab_layout)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/google/android/material/tabs/TabLayout;

    iput-object v0, p0, Lcom/squareup/balance/activity/ui/list/DisplayActivitiesRunner;->tabLayout:Lcom/google/android/material/tabs/TabLayout;

    .line 25
    sget-object v0, Lcom/squareup/noho/NohoActionBar;->Companion:Lcom/squareup/noho/NohoActionBar$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar$Companion;->findIn(Landroid/view/View;)Lcom/squareup/noho/NohoActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/balance/activity/ui/list/DisplayActivitiesRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 27
    sget v0, Lcom/squareup/balance/activity/impl/R$id;->balance_activities_child_container:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/ui/WorkflowViewStub;

    iput-object v0, p0, Lcom/squareup/balance/activity/ui/list/DisplayActivitiesRunner;->childContainer:Lcom/squareup/workflow/ui/WorkflowViewStub;

    .line 29
    invoke-static {p1}, Lcom/squareup/container/ContainerKt;->getScreenSize(Landroid/view/View;)Lcom/squareup/util/DeviceScreenSizeInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/util/DeviceScreenSizeInfo;->isMasterDetail()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    iput-boolean p1, p0, Lcom/squareup/balance/activity/ui/list/DisplayActivitiesRunner;->showBackButton:Z

    .line 34
    iget-object p1, p0, Lcom/squareup/balance/activity/ui/list/DisplayActivitiesRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 32
    new-instance v0, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 33
    new-instance v1, Lcom/squareup/util/ViewString$ResourceString;

    sget v2, Lcom/squareup/balance/activity/impl/R$string;->balance_activity_action_bar_title:I

    invoke-direct {v1, v2}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v1, Lcom/squareup/resources/TextModel;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 34
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/view/View;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 23
    invoke-direct {p0, p1}, Lcom/squareup/balance/activity/ui/list/DisplayActivitiesRunner;-><init>(Landroid/view/View;)V

    return-void
.end method

.method private final updateActionBar(Lcom/squareup/balance/activity/ui/list/DisplayActivitiesScreen;)V
    .locals 4

    .line 67
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/list/DisplayActivitiesRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 59
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar;->getConfig()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config;->buildUpon()Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 61
    iget-boolean v2, p0, Lcom/squareup/balance/activity/ui/list/DisplayActivitiesRunner;->showBackButton:Z

    if-eqz v2, :cond_0

    .line 62
    sget-object v2, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    new-instance v3, Lcom/squareup/balance/activity/ui/list/DisplayActivitiesRunner$updateActionBar$$inlined$apply$lambda$1;

    invoke-direct {v3, p0, p1}, Lcom/squareup/balance/activity/ui/list/DisplayActivitiesRunner$updateActionBar$$inlined$apply$lambda$1;-><init>(Lcom/squareup/balance/activity/ui/list/DisplayActivitiesRunner;Lcom/squareup/balance/activity/ui/list/DisplayActivitiesScreen;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    goto :goto_0

    .line 64
    :cond_0
    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->hideUpButton()Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 67
    :goto_0
    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method


# virtual methods
.method public showRendering(Lcom/squareup/balance/activity/ui/list/DisplayActivitiesScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 3

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/list/DisplayActivitiesRunner;->tabListener:Lcom/squareup/balance/activity/ui/list/DisplayActivitiesRunner$TabSelectedListener;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/squareup/balance/activity/ui/list/DisplayActivitiesRunner;->tabLayout:Lcom/google/android/material/tabs/TabLayout;

    check-cast v0, Lcom/google/android/material/tabs/TabLayout$OnTabSelectedListener;

    invoke-virtual {v1, v0}, Lcom/google/android/material/tabs/TabLayout;->removeOnTabSelectedListener(Lcom/google/android/material/tabs/TabLayout$OnTabSelectedListener;)V

    .line 44
    :cond_0
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/list/DisplayActivitiesRunner;->tabLayout:Lcom/google/android/material/tabs/TabLayout;

    invoke-virtual {p1}, Lcom/squareup/balance/activity/ui/list/DisplayActivitiesScreen;->getTabSelected()Lcom/squareup/balance/activity/data/BalanceActivityType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/balance/activity/data/BalanceActivityType;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/material/tabs/TabLayout;->getTabAt(I)Lcom/google/android/material/tabs/TabLayout$Tab;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 45
    invoke-virtual {v0}, Lcom/google/android/material/tabs/TabLayout$Tab;->select()V

    .line 48
    :cond_1
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/list/DisplayActivitiesRunner;->tabLayout:Lcom/google/android/material/tabs/TabLayout;

    new-instance v1, Lcom/squareup/balance/activity/ui/list/DisplayActivitiesRunner$TabSelectedListener;

    invoke-virtual {p1}, Lcom/squareup/balance/activity/ui/list/DisplayActivitiesScreen;->getOnTabSelected()Lkotlin/jvm/functions/Function1;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/balance/activity/ui/list/DisplayActivitiesRunner$TabSelectedListener;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 49
    iput-object v1, p0, Lcom/squareup/balance/activity/ui/list/DisplayActivitiesRunner;->tabListener:Lcom/squareup/balance/activity/ui/list/DisplayActivitiesRunner$TabSelectedListener;

    .line 48
    check-cast v1, Lcom/google/android/material/tabs/TabLayout$OnTabSelectedListener;

    invoke-virtual {v0, v1}, Lcom/google/android/material/tabs/TabLayout;->addOnTabSelectedListener(Lcom/google/android/material/tabs/TabLayout$OnTabSelectedListener;)V

    .line 52
    invoke-direct {p0, p1}, Lcom/squareup/balance/activity/ui/list/DisplayActivitiesRunner;->updateActionBar(Lcom/squareup/balance/activity/ui/list/DisplayActivitiesScreen;)V

    .line 55
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/list/DisplayActivitiesRunner;->childContainer:Lcom/squareup/workflow/ui/WorkflowViewStub;

    invoke-virtual {p1}, Lcom/squareup/balance/activity/ui/list/DisplayActivitiesScreen;->getChildScreenData()Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildScreen;

    move-result-object p1

    invoke-virtual {v0, p1, p2}, Lcom/squareup/workflow/ui/WorkflowViewStub;->update(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)Landroid/view/View;

    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 23
    check-cast p1, Lcom/squareup/balance/activity/ui/list/DisplayActivitiesScreen;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/activity/ui/list/DisplayActivitiesRunner;->showRendering(Lcom/squareup/balance/activity/ui/list/DisplayActivitiesScreen;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
