.class public interface abstract Lcom/squareup/balance/activity/ui/BalanceActivityViewFactory;
.super Ljava/lang/Object;
.source "BalanceActivityViewFactory.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/activity/ui/BalanceActivityViewFactory$DefaultImpls;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u00020\u00032\u000e\u0008\u0002\u0010\u0004\u001a\u0008\u0012\u0002\u0008\u0003\u0018\u00010\u0005H&\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/squareup/balance/activity/ui/BalanceActivityViewFactory;",
        "",
        "create",
        "Lcom/squareup/workflow/WorkflowViewFactory;",
        "section",
        "Ljava/lang/Class;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract create(Ljava/lang/Class;)Lcom/squareup/workflow/WorkflowViewFactory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "*>;)",
            "Lcom/squareup/workflow/WorkflowViewFactory;"
        }
    .end annotation
.end method
