.class final Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper$subLabel$formattedDate$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealBalanceActivityMapper.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper;->subLabel(Lcom/squareup/protos/bizbank/UnifiedActivity;Z)Lcom/squareup/resources/TextModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $activity:Lcom/squareup/protos/bizbank/UnifiedActivity;

.field final synthetic this$0:Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper;


# direct methods
.method constructor <init>(Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper;Lcom/squareup/protos/bizbank/UnifiedActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper$subLabel$formattedDate$1;->this$0:Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper;

    iput-object p2, p0, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper$subLabel$formattedDate$1;->$activity:Lcom/squareup/protos/bizbank/UnifiedActivity;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 29
    invoke-virtual {p0}, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper$subLabel$formattedDate$1;->invoke()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final invoke()Ljava/lang/String;
    .locals 3

    .line 46
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper$subLabel$formattedDate$1;->$activity:Lcom/squareup/protos/bizbank/UnifiedActivity;

    iget-object v0, v0, Lcom/squareup/protos/bizbank/UnifiedActivity;->latest_event_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper$subLabel$formattedDate$1;->this$0:Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper;

    invoke-static {v0}, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper;->access$getDateFormatter$p(Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper;)Ljava/text/DateFormat;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper$subLabel$formattedDate$1;->$activity:Lcom/squareup/protos/bizbank/UnifiedActivity;

    iget-object v1, v1, Lcom/squareup/protos/bizbank/UnifiedActivity;->latest_event_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v2, p0, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper$subLabel$formattedDate$1;->this$0:Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper;

    invoke-static {v2}, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper;->access$getLocale$p(Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Locale;

    invoke-static {v1, v2}, Lcom/squareup/util/ProtoTimes;->asDate(Lcom/squareup/protos/common/time/DateTime;Ljava/util/Locale;)Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    return-object v0
.end method
