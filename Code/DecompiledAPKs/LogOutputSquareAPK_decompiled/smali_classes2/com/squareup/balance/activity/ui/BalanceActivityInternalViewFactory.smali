.class public final Lcom/squareup/balance/activity/ui/BalanceActivityInternalViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "RealBalanceActivityViewFactory.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/activity/ui/BalanceActivityInternalViewFactory$Factory;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001:\u0001\u0007B\u001d\u0008\u0000\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0002\u0008\u0003\u0018\u00010\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/balance/activity/ui/BalanceActivityInternalViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "section",
        "Ljava/lang/Class;",
        "displayActivitiesListChildRunner",
        "Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$Factory;",
        "(Ljava/lang/Class;Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$Factory;)V",
        "Factory",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/Class;Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$Factory;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$Factory;",
            ")V"
        }
    .end annotation

    const-string v0, "displayActivitiesListChildRunner"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 39
    new-instance v1, Lcom/squareup/balance/activity/ui/list/DisplayActivitiesRunner$Binding;

    invoke-direct {v1, p1}, Lcom/squareup/balance/activity/ui/list/DisplayActivitiesRunner$Binding;-><init>(Ljava/lang/Class;)V

    check-cast v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 40
    new-instance v1, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$Binding;

    invoke-direct {v1, p2}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$Binding;-><init>(Lcom/squareup/balance/activity/ui/list/displaying/DisplayActivitiesListChildRunner$Factory;)V

    check-cast v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 p2, 0x1

    aput-object v1, v0, p2

    .line 41
    new-instance p2, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner$Binding;

    invoke-direct {p2, p1}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner$Binding;-><init>(Ljava/lang/Class;)V

    check-cast p2, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 v1, 0x2

    aput-object p2, v0, v1

    .line 42
    new-instance p2, Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingLayoutRunner$Binding;

    invoke-direct {p2, p1}, Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingLayoutRunner$Binding;-><init>(Ljava/lang/Class;)V

    check-cast p2, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 v1, 0x3

    aput-object p2, v0, v1

    .line 43
    new-instance p2, Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorLayoutRunner$Binding;

    invoke-direct {p2, p1}, Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorLayoutRunner$Binding;-><init>(Ljava/lang/Class;)V

    check-cast p2, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 p1, 0x4

    aput-object p2, v0, p1

    .line 44
    sget-object p1, Lcom/squareup/balance/activity/ui/detail/communityreward/CommunityRewardDialogFactory$Binding;->INSTANCE:Lcom/squareup/balance/activity/ui/detail/communityreward/CommunityRewardDialogFactory$Binding;

    check-cast p1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 p2, 0x5

    aput-object p1, v0, p2

    .line 38
    invoke-direct {p0, v0}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
