.class public final Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;
.super Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity;
.source "UiBalanceActivity.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BalanceRow"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0011\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B9\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000e\u0010\u0004\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\t\u0010\u0015\u001a\u00020\u0003H\u00c6\u0003J\u0011\u0010\u0016\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u0005H\u00c6\u0003J\t\u0010\u0017\u001a\u00020\u0007H\u00c6\u0003J\u000b\u0010\u0018\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\t\u0010\u0019\u001a\u00020\nH\u00c6\u0003JE\u0010\u001a\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0010\u0008\u0002\u0010\u0004\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\u00032\u0008\u0008\u0002\u0010\t\u001a\u00020\nH\u00c6\u0001J\t\u0010\u001b\u001a\u00020\u001cH\u00d6\u0001J\u0013\u0010\u001d\u001a\u00020\u001e2\u0008\u0010\u001f\u001a\u0004\u0018\u00010 H\u00d6\u0003J\t\u0010!\u001a\u00020\u001cH\u00d6\u0001J\t\u0010\"\u001a\u00020#H\u00d6\u0001J\u0019\u0010$\u001a\u00020%2\u0006\u0010&\u001a\u00020\'2\u0006\u0010(\u001a\u00020\u001cH\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0013\u0010\u0008\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\rR\u0019\u0010\u0004\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014\u00a8\u0006)"
    }
    d2 = {
        "Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;",
        "Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity;",
        "label",
        "",
        "subLabel",
        "Lcom/squareup/resources/TextModel;",
        "transactionAmount",
        "Lcom/squareup/balance/activity/ui/common/Amount;",
        "runningBalance",
        "unifiedActivity",
        "Lcom/squareup/protos/bizbank/UnifiedActivity;",
        "(Ljava/lang/CharSequence;Lcom/squareup/resources/TextModel;Lcom/squareup/balance/activity/ui/common/Amount;Ljava/lang/CharSequence;Lcom/squareup/protos/bizbank/UnifiedActivity;)V",
        "getLabel",
        "()Ljava/lang/CharSequence;",
        "getRunningBalance",
        "getSubLabel",
        "()Lcom/squareup/resources/TextModel;",
        "getTransactionAmount",
        "()Lcom/squareup/balance/activity/ui/common/Amount;",
        "getUnifiedActivity",
        "()Lcom/squareup/protos/bizbank/UnifiedActivity;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "copy",
        "describeContents",
        "",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final label:Ljava/lang/CharSequence;

.field private final runningBalance:Ljava/lang/CharSequence;

.field private final subLabel:Lcom/squareup/resources/TextModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionAmount:Lcom/squareup/balance/activity/ui/common/Amount;

.field private final unifiedActivity:Lcom/squareup/protos/bizbank/UnifiedActivity;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow$Creator;

    invoke-direct {v0}, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow$Creator;-><init>()V

    sput-object v0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;Lcom/squareup/resources/TextModel;Lcom/squareup/balance/activity/ui/common/Amount;Ljava/lang/CharSequence;Lcom/squareup/protos/bizbank/UnifiedActivity;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "Lcom/squareup/resources/TextModel<",
            "+",
            "Ljava/lang/CharSequence;",
            ">;",
            "Lcom/squareup/balance/activity/ui/common/Amount;",
            "Ljava/lang/CharSequence;",
            "Lcom/squareup/protos/bizbank/UnifiedActivity;",
            ")V"
        }
    .end annotation

    const-string v0, "label"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transactionAmount"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "unifiedActivity"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 32
    invoke-direct {p0, v0}, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;->label:Ljava/lang/CharSequence;

    iput-object p2, p0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;->subLabel:Lcom/squareup/resources/TextModel;

    iput-object p3, p0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;->transactionAmount:Lcom/squareup/balance/activity/ui/common/Amount;

    iput-object p4, p0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;->runningBalance:Ljava/lang/CharSequence;

    iput-object p5, p0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;->unifiedActivity:Lcom/squareup/protos/bizbank/UnifiedActivity;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/CharSequence;Lcom/squareup/resources/TextModel;Lcom/squareup/balance/activity/ui/common/Amount;Ljava/lang/CharSequence;Lcom/squareup/protos/bizbank/UnifiedActivity;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 6

    and-int/lit8 p6, p6, 0x8

    if-eqz p6, :cond_0

    const/4 p4, 0x0

    .line 30
    check-cast p4, Ljava/lang/CharSequence;

    :cond_0
    move-object v4, p4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;-><init>(Ljava/lang/CharSequence;Lcom/squareup/resources/TextModel;Lcom/squareup/balance/activity/ui/common/Amount;Ljava/lang/CharSequence;Lcom/squareup/protos/bizbank/UnifiedActivity;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;Ljava/lang/CharSequence;Lcom/squareup/resources/TextModel;Lcom/squareup/balance/activity/ui/common/Amount;Ljava/lang/CharSequence;Lcom/squareup/protos/bizbank/UnifiedActivity;ILjava/lang/Object;)Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-object p1, p0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;->label:Ljava/lang/CharSequence;

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-object p2, p0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;->subLabel:Lcom/squareup/resources/TextModel;

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;->transactionAmount:Lcom/squareup/balance/activity/ui/common/Amount;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;->runningBalance:Ljava/lang/CharSequence;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;->unifiedActivity:Lcom/squareup/protos/bizbank/UnifiedActivity;

    :cond_4
    move-object v2, p5

    move-object p2, p0

    move-object p3, p1

    move-object p4, p7

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;->copy(Ljava/lang/CharSequence;Lcom/squareup/resources/TextModel;Lcom/squareup/balance/activity/ui/common/Amount;Ljava/lang/CharSequence;Lcom/squareup/protos/bizbank/UnifiedActivity;)Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;->label:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final component2()Lcom/squareup/resources/TextModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;->subLabel:Lcom/squareup/resources/TextModel;

    return-object v0
.end method

.method public final component3()Lcom/squareup/balance/activity/ui/common/Amount;
    .locals 1

    iget-object v0, p0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;->transactionAmount:Lcom/squareup/balance/activity/ui/common/Amount;

    return-object v0
.end method

.method public final component4()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;->runningBalance:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final component5()Lcom/squareup/protos/bizbank/UnifiedActivity;
    .locals 1

    iget-object v0, p0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;->unifiedActivity:Lcom/squareup/protos/bizbank/UnifiedActivity;

    return-object v0
.end method

.method public final copy(Ljava/lang/CharSequence;Lcom/squareup/resources/TextModel;Lcom/squareup/balance/activity/ui/common/Amount;Ljava/lang/CharSequence;Lcom/squareup/protos/bizbank/UnifiedActivity;)Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "Lcom/squareup/resources/TextModel<",
            "+",
            "Ljava/lang/CharSequence;",
            ">;",
            "Lcom/squareup/balance/activity/ui/common/Amount;",
            "Ljava/lang/CharSequence;",
            "Lcom/squareup/protos/bizbank/UnifiedActivity;",
            ")",
            "Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;"
        }
    .end annotation

    const-string v0, "label"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transactionAmount"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "unifiedActivity"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;-><init>(Ljava/lang/CharSequence;Lcom/squareup/resources/TextModel;Lcom/squareup/balance/activity/ui/common/Amount;Ljava/lang/CharSequence;Lcom/squareup/protos/bizbank/UnifiedActivity;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;

    iget-object v0, p0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;->label:Ljava/lang/CharSequence;

    iget-object v1, p1, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;->label:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;->subLabel:Lcom/squareup/resources/TextModel;

    iget-object v1, p1, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;->subLabel:Lcom/squareup/resources/TextModel;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;->transactionAmount:Lcom/squareup/balance/activity/ui/common/Amount;

    iget-object v1, p1, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;->transactionAmount:Lcom/squareup/balance/activity/ui/common/Amount;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;->runningBalance:Ljava/lang/CharSequence;

    iget-object v1, p1, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;->runningBalance:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;->unifiedActivity:Lcom/squareup/protos/bizbank/UnifiedActivity;

    iget-object p1, p1, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;->unifiedActivity:Lcom/squareup/protos/bizbank/UnifiedActivity;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getLabel()Ljava/lang/CharSequence;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;->label:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getRunningBalance()Ljava/lang/CharSequence;
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;->runningBalance:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getSubLabel()Lcom/squareup/resources/TextModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    .line 28
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;->subLabel:Lcom/squareup/resources/TextModel;

    return-object v0
.end method

.method public final getTransactionAmount()Lcom/squareup/balance/activity/ui/common/Amount;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;->transactionAmount:Lcom/squareup/balance/activity/ui/common/Amount;

    return-object v0
.end method

.method public final getUnifiedActivity()Lcom/squareup/protos/bizbank/UnifiedActivity;
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;->unifiedActivity:Lcom/squareup/protos/bizbank/UnifiedActivity;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;->label:Ljava/lang/CharSequence;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;->subLabel:Lcom/squareup/resources/TextModel;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;->transactionAmount:Lcom/squareup/balance/activity/ui/common/Amount;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;->runningBalance:Ljava/lang/CharSequence;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;->unifiedActivity:Lcom/squareup/protos/bizbank/UnifiedActivity;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_4
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BalanceRow(label="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;->label:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const-string v1, ", subLabel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;->subLabel:Lcom/squareup/resources/TextModel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", transactionAmount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;->transactionAmount:Lcom/squareup/balance/activity/ui/common/Amount;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", runningBalance="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;->runningBalance:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const-string v1, ", unifiedActivity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;->unifiedActivity:Lcom/squareup/protos/bizbank/UnifiedActivity;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;->label:Ljava/lang/CharSequence;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    iget-object v0, p0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;->subLabel:Lcom/squareup/resources/TextModel;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;->transactionAmount:Lcom/squareup/balance/activity/ui/common/Amount;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object p2, p0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;->runningBalance:Ljava/lang/CharSequence;

    invoke-static {p2, p1, v1}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    iget-object p2, p0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$BalanceRow;->unifiedActivity:Lcom/squareup/protos/bizbank/UnifiedActivity;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    return-void
.end method
