.class public interface abstract Lcom/squareup/balance/activity/data/BalanceActivityDetailsRepository;
.super Ljava/lang/Object;
.source "BalanceActivityDetailsRepository.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008f\u0018\u00002\u00020\u0001J\u0016\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0006\u0010\u0005\u001a\u00020\u0006H&J\u0008\u0010\u0007\u001a\u00020\u0008H&J\u0016\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\n0\u00032\u0006\u0010\u000b\u001a\u00020\nH&J\u0016\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\n0\u00032\u0006\u0010\u000b\u001a\u00020\nH&\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/balance/activity/data/BalanceActivityDetailsRepository;",
        "",
        "balanceActivityDetails",
        "Lio/reactivex/Single;",
        "Lcom/squareup/balance/activity/data/BalanceActivityDetails;",
        "activityToken",
        "",
        "clear",
        "",
        "updateCategoryToBusiness",
        "Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;",
        "activity",
        "updateCategoryToPersonal",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract balanceActivityDetails(Ljava/lang/String;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/balance/activity/data/BalanceActivityDetails;",
            ">;"
        }
    .end annotation
.end method

.method public abstract clear()V
.end method

.method public abstract updateCategoryToBusiness(Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;",
            ">;"
        }
    .end annotation
.end method

.method public abstract updateCategoryToPersonal(Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;",
            ">;"
        }
    .end annotation
.end method
