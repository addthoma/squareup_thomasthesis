.class final Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow$render$dialog$1;
.super Lkotlin/jvm/internal/Lambda;
.source "BalanceActivityDetailsSuccessWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow;->render(Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lkotlin/Unit;",
        "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow$Action$DisplayDetailsData;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\n\u0010\u0002\u001a\u00060\u0003j\u0002`\u0004H\n\u00a2\u0006\u0004\u0008\u0005\u0010\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow$Action$DisplayDetailsData;",
        "it",
        "",
        "Lcom/squareup/balance/activity/ui/detail/communityreward/Exit;",
        "invoke",
        "(Lkotlin/Unit;)Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow$Action$DisplayDetailsData;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState;


# direct methods
.method constructor <init>(Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow$render$dialog$1;->$state:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lkotlin/Unit;)Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow$Action$DisplayDetailsData;
    .locals 2

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    new-instance p1, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow$Action$DisplayDetailsData;

    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow$render$dialog$1;->$state:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState;

    check-cast v0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$DisplayingCommunityReward;

    invoke-virtual {v0}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessState$DisplayingCommunityReward;->getDetails()Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p1, v0, v1}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow$Action$DisplayDetailsData;-><init>(Lcom/squareup/protos/bizbank/UnifiedActivityDetails;Z)V

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 35
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow$render$dialog$1;->invoke(Lkotlin/Unit;)Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessWorkflow$Action$DisplayDetailsData;

    move-result-object p1

    return-object p1
.end method
