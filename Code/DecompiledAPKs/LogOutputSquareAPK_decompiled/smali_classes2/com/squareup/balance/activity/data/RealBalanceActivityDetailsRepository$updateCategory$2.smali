.class final Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository$updateCategory$2;
.super Ljava/lang/Object;
.source "RealBalanceActivityDetailsRepository.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository;->updateCategory(Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;Z)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealBalanceActivityDetailsRepository.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealBalanceActivityDetailsRepository.kt\ncom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository$updateCategory$2\n*L\n1#1,85:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $activity:Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;

.field final synthetic $isPersonal:Z

.field final synthetic this$0:Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository;


# direct methods
.method constructor <init>(Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository;Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;Z)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository$updateCategory$2;->this$0:Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository;

    iput-object p2, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository$updateCategory$2;->$activity:Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;

    iput-boolean p3, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository$updateCategory$2;->$isPersonal:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;
    .locals 3

    .line 55
    iget-object v0, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository$updateCategory$2;->this$0:Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository;

    iget-object v1, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository$updateCategory$2;->$activity:Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;

    iget-boolean v2, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository$updateCategory$2;->$isPersonal:Z

    invoke-static {v0, v1, v2}, Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository;->access$updateActivity(Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository;Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;Z)Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;

    move-result-object v0

    .line 56
    iget-object v1, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository$updateCategory$2;->this$0:Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository;

    invoke-static {v1, v0}, Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository;->access$addToLocalCache(Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository;Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;)V

    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository$updateCategory$2;->call()Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;

    move-result-object v0

    return-object v0
.end method
