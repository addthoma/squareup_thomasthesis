.class final Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDetailsDataStore$fetchDetails$1;
.super Ljava/lang/Object;
.source "RealRemoteBalanceActivityDetailsDataStore.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDetailsDataStore;->fetchDetails(Ljava/lang/String;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/balance/activity/data/BalanceActivityDetails;",
        "result",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDetailsDataStore$fetchDetails$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDetailsDataStore$fetchDetails$1;

    invoke-direct {v0}, Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDetailsDataStore$fetchDetails$1;-><init>()V

    sput-object v0, Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDetailsDataStore$fetchDetails$1;->INSTANCE:Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDetailsDataStore$fetchDetails$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/balance/activity/data/BalanceActivityDetails;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse;",
            ">;)",
            "Lcom/squareup/balance/activity/data/BalanceActivityDetails;"
        }
    .end annotation

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;

    .line 31
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse;

    iget-object v1, v1, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse;->response:Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;

    iget-object v1, v1, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;->unified_activity:Lcom/squareup/protos/bizbank/UnifiedActivity;

    const-string v2, "result.response.response.unified_activity"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse;

    iget-object p1, p1, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse;->response:Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;

    iget-object p1, p1, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;->unified_activity_details:Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    const-string v2, "result.response.response.unified_activity_details"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-direct {v0, v1, p1}, Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;-><init>(Lcom/squareup/protos/bizbank/UnifiedActivity;Lcom/squareup/protos/bizbank/UnifiedActivityDetails;)V

    check-cast v0, Lcom/squareup/balance/activity/data/BalanceActivityDetails;

    goto :goto_0

    .line 34
    :cond_0
    instance-of p1, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz p1, :cond_1

    sget-object p1, Lcom/squareup/balance/activity/data/BalanceActivityDetails$ErrorLoadingDetails;->INSTANCE:Lcom/squareup/balance/activity/data/BalanceActivityDetails$ErrorLoadingDetails;

    move-object v0, p1

    check-cast v0, Lcom/squareup/balance/activity/data/BalanceActivityDetails;

    :goto_0
    return-object v0

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 15
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/activity/data/RealRemoteBalanceActivityDetailsDataStore$fetchDetails$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/balance/activity/data/BalanceActivityDetails;

    move-result-object p1

    return-object p1
.end method
