.class final Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$CheckingAuthorization$subWorkflow$1;
.super Lkotlin/jvm/internal/Lambda;
.source "OrderSquareCardReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$CheckingAuthorization;-><init>(ZLcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;Lcom/squareup/workflow/rx1/Workflow;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/ScreenState<",
        "+",
        "Ljava/util/Map<",
        "Lcom/squareup/workflow/MainAndModal;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/workflow/ScreenState<",
        "+",
        "Ljava/util/Map<",
        "Lcom/squareup/workflow/MainAndModal;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0000\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\u0010\u0000\u001a \u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020\u0003\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0004j\u0002`\u00050\u00020\u0001\"\n\u0008\u0000\u0010\u0006 \u0001*\u00020\u00072$\u0010\u0008\u001a \u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020\u0003\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0004j\u0002`\u00050\u00020\u0001H\n\u00a2\u0006\u0002\u0008\t"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/ScreenState;",
        "",
        "Lcom/squareup/workflow/MainAndModal;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "O",
        "",
        "it",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$CheckingAuthorization;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$CheckingAuthorization;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$CheckingAuthorization$subWorkflow$1;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$CheckingAuthorization;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/workflow/ScreenState;)Lcom/squareup/workflow/ScreenState;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/ScreenState<",
            "+",
            "Ljava/util/Map<",
            "Lcom/squareup/workflow/MainAndModal;",
            "+",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;)",
            "Lcom/squareup/workflow/ScreenState<",
            "Ljava/util/Map<",
            "Lcom/squareup/workflow/MainAndModal;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 209
    sget-object v0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer;->Companion:Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer$Companion;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$CheckingAuthorization$subWorkflow$1;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$CheckingAuthorization;

    check-cast v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    iget-object v2, p1, Lcom/squareup/workflow/ScreenState;->snapshot:Lcom/squareup/workflow/Snapshot;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer$Companion;->snapshotState(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/Snapshot;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/ScreenState;->withSnapshot(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/ScreenState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 202
    check-cast p1, Lcom/squareup/workflow/ScreenState;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$CheckingAuthorization$subWorkflow$1;->invoke(Lcom/squareup/workflow/ScreenState;)Lcom/squareup/workflow/ScreenState;

    move-result-object p1

    return-object p1
.end method
