.class public final Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics;
.super Ljava/lang/Object;
.source "RealSquareCardOrderedAnalytics.kt"

# interfaces
.implements Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedAnalytics;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0008\n\u0002\u0010\u000e\n\u0002\u0008\u0019\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0005\u001a\u00020\u0006H\u0016J\u0008\u0010\u0007\u001a\u00020\u0006H\u0016J\u0008\u0010\u0008\u001a\u00020\u0006H\u0016J\u0008\u0010\t\u001a\u00020\u0006H\u0016J\u0008\u0010\n\u001a\u00020\u0006H\u0016J\u0008\u0010\u000b\u001a\u00020\u0006H\u0016J\u0008\u0010\u000c\u001a\u00020\u0006H\u0016J\u0010\u0010\r\u001a\u00020\u00062\u0006\u0010\u000e\u001a\u00020\u000fH\u0002J\u0008\u0010\u0010\u001a\u00020\u0006H\u0016J\u0008\u0010\u0011\u001a\u00020\u0006H\u0016J\u0008\u0010\u0012\u001a\u00020\u0006H\u0016J\u0008\u0010\u0013\u001a\u00020\u0006H\u0016J\u0008\u0010\u0014\u001a\u00020\u0006H\u0016J\u0008\u0010\u0015\u001a\u00020\u0006H\u0016J\u0008\u0010\u0016\u001a\u00020\u0006H\u0016J\u0008\u0010\u0017\u001a\u00020\u0006H\u0016J\u0008\u0010\u0018\u001a\u00020\u0006H\u0016J\u0010\u0010\u0019\u001a\u00020\u00062\u0006\u0010\u000e\u001a\u00020\u000fH\u0002J\u0008\u0010\u001a\u001a\u00020\u0006H\u0016J\u0008\u0010\u001b\u001a\u00020\u0006H\u0016J\u0008\u0010\u001c\u001a\u00020\u0006H\u0016J\u0008\u0010\u001d\u001a\u00020\u0006H\u0016J\u0008\u0010\u001e\u001a\u00020\u0006H\u0016J\u0008\u0010\u001f\u001a\u00020\u0006H\u0016J\u0008\u0010 \u001a\u00020\u0006H\u0016J\u0008\u0010!\u001a\u00020\u0006H\u0016J\u0008\u0010\"\u001a\u00020\u0006H\u0016J\u0008\u0010#\u001a\u00020\u0006H\u0016J\u0008\u0010$\u001a\u00020\u0006H\u0016J\u0008\u0010%\u001a\u00020\u0006H\u0016J\u0008\u0010&\u001a\u00020\u0006H\u0016J\u0008\u0010\'\u001a\u00020\u0006H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006("
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedAnalytics;",
        "squareCardAnalyticsLogger",
        "Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;",
        "(Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;)V",
        "logActivateCardClick",
        "",
        "logActivationCanceledClick",
        "logActivationCodeErrorScreen",
        "logActivationFinishedScreen",
        "logBillingAddressScreen",
        "logCardValidationCancelClick",
        "logCardValidationScreen",
        "logClick",
        "description",
        "",
        "logDepositsOnDemandCancelClick",
        "logDepositsOnDemandContinueClick",
        "logDepositsOnDemandLearnMoreClick",
        "logDepositsOnDemandScreen",
        "logEnterActivationCodeCancelClick",
        "logEnterActivationCodeContinueClick",
        "logEnterActivationCodeScreen",
        "logExitBillingAddressClick",
        "logFinishActivationClick",
        "logImpression",
        "logManualEntryFailure",
        "logManualEntrySuccess",
        "logPinEntryCancelClick",
        "logPinEntryContinueClick",
        "logPinEntryErrorScreen",
        "logPinEntryScreen",
        "logProblemWithActivatedCardClick",
        "logResendActivationEmailClick",
        "logSendActivationEmailErrorScreen",
        "logSquareCardPending2FAScreen",
        "logSquareCardPendingScreen",
        "logSubmitBillingAddressClick",
        "logSwipeCardEntryFailure",
        "logSwipeCardEntrySuccess",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final squareCardAnalyticsLogger:Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;


# direct methods
.method public constructor <init>(Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "squareCardAnalyticsLogger"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics;->squareCardAnalyticsLogger:Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;

    return-void
.end method

.method private final logClick(Ljava/lang/String;)V
    .locals 1

    .line 163
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics;->squareCardAnalyticsLogger:Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;

    invoke-virtual {v0, p1}, Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method private final logImpression(Ljava/lang/String;)V
    .locals 1

    .line 167
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics;->squareCardAnalyticsLogger:Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;

    invoke-virtual {v0, p1}, Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;->logImpression(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public logActivateCardClick()V
    .locals 1

    const-string v0, "Square Card: Activate Card"

    .line 43
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logActivationCanceledClick()V
    .locals 1

    const-string v0, "Square Card: Activate Card Cancel"

    .line 47
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logActivationCodeErrorScreen()V
    .locals 1

    const-string v0, "Activate Card: Confirmation Code Error"

    .line 143
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics;->logImpression(Ljava/lang/String;)V

    return-void
.end method

.method public logActivationFinishedScreen()V
    .locals 1

    const-string v0, "Activate Card: Card Now Active"

    .line 147
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics;->logImpression(Ljava/lang/String;)V

    return-void
.end method

.method public logBillingAddressScreen()V
    .locals 1

    const-string v0, "Activate Card: Billing Address"

    .line 159
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics;->logImpression(Ljava/lang/String;)V

    return-void
.end method

.method public logCardValidationCancelClick()V
    .locals 1

    const-string v0, "Activate Card: Card Validation Cancel"

    .line 59
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logCardValidationScreen()V
    .locals 1

    const-string v0, "Activate Card: Card Validation"

    .line 127
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics;->logImpression(Ljava/lang/String;)V

    return-void
.end method

.method public logDepositsOnDemandCancelClick()V
    .locals 1

    const-string v0, "Activate Card: Deposits On Demand Cancel"

    .line 71
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logDepositsOnDemandContinueClick()V
    .locals 1

    const-string v0, "Activate Card: Deposits On Demand Continue"

    .line 67
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logDepositsOnDemandLearnMoreClick()V
    .locals 1

    const-string v0, "Activate Card: Deposits On Demand Learn More"

    .line 63
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logDepositsOnDemandScreen()V
    .locals 1

    const-string v0, "Activate Card: Deposits On Demand"

    .line 131
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics;->logImpression(Ljava/lang/String;)V

    return-void
.end method

.method public logEnterActivationCodeCancelClick()V
    .locals 1

    const-string v0, "Activate Card: Confirmation Code Validation Cancel"

    .line 91
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logEnterActivationCodeContinueClick()V
    .locals 1

    const-string v0, "Activate Card: Confirmation Code Validation Continue"

    .line 87
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logEnterActivationCodeScreen()V
    .locals 1

    const-string v0, "Activate Card: Confirmation Code Validation"

    .line 139
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics;->logImpression(Ljava/lang/String;)V

    return-void
.end method

.method public logExitBillingAddressClick()V
    .locals 1

    const-string v0, "Activate Card: Billing Address Cancel"

    .line 79
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logFinishActivationClick()V
    .locals 1

    const-string v0, "Square Card: Finish Activation"

    .line 51
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logManualEntryFailure()V
    .locals 1

    const-string v0, "Activate Card: Manual Entry Fail"

    .line 107
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logManualEntrySuccess()V
    .locals 1

    const-string v0, "Activate Card: Manual Entry Success"

    .line 103
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logPinEntryCancelClick()V
    .locals 1

    const-string v0, "Activate Card: Create PIN Cancel"

    .line 111
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logPinEntryContinueClick()V
    .locals 1

    const-string v0, "Activate Card: Create PIN Continue"

    .line 115
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logPinEntryErrorScreen()V
    .locals 1

    const-string v0, "Activate Card: Create PIN Error"

    .line 155
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics;->logImpression(Ljava/lang/String;)V

    return-void
.end method

.method public logPinEntryScreen()V
    .locals 1

    const-string v0, "Activate Card: Create PIN"

    .line 151
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics;->logImpression(Ljava/lang/String;)V

    return-void
.end method

.method public logProblemWithActivatedCardClick()V
    .locals 1

    const-string v0, "Square Card: Active Page Problem With Card"

    .line 55
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logResendActivationEmailClick()V
    .locals 1

    const-string v0, "Activate Card: Resend Activation Email"

    .line 83
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logSendActivationEmailErrorScreen()V
    .locals 1

    const-string v0, "Activate Card: Send Activation Email Error"

    .line 135
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics;->logImpression(Ljava/lang/String;)V

    return-void
.end method

.method public logSquareCardPending2FAScreen()V
    .locals 1

    const-string v0, "Square Card: Pending 2FA Page"

    .line 123
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics;->logImpression(Ljava/lang/String;)V

    return-void
.end method

.method public logSquareCardPendingScreen()V
    .locals 1

    const-string v0, "Square Card: Card Pending"

    .line 119
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics;->logImpression(Ljava/lang/String;)V

    return-void
.end method

.method public logSubmitBillingAddressClick()V
    .locals 1

    const-string v0, "Activate Card: Billing Address Continue"

    .line 75
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logSwipeCardEntryFailure()V
    .locals 1

    const-string v0, "Activate Card: Swipe Card Fail"

    .line 99
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logSwipeCardEntrySuccess()V
    .locals 1

    const-string v0, "Activate Card: Swipe Card Success"

    .line 95
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method
