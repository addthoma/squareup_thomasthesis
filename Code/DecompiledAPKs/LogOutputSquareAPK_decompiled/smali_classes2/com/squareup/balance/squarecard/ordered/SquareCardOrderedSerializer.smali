.class public final Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedSerializer;
.super Ljava/lang/Object;
.source "SquareCardOrderedSerializer.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSquareCardOrderedSerializer.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SquareCardOrderedSerializer.kt\ncom/squareup/balance/squarecard/ordered/SquareCardOrderedSerializer\n+ 2 Snapshot.kt\ncom/squareup/workflow/SnapshotKt\n+ 3 BuffersProtos.kt\ncom/squareup/workflow/BuffersProtos\n*L\n1#1,119:1\n180#2:120\n148#2:122\n56#3:121\n*E\n*S KotlinDebug\n*F\n+ 1 SquareCardOrderedSerializer.kt\ncom/squareup/balance/squarecard/ordered/SquareCardOrderedSerializer\n*L\n76#1:120\n76#1:122\n76#1:121\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u000e\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0008\u001a\u00020\u0004J\u000c\u0010\t\u001a\u00020\n*\u00020\u0004H\u0002\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedSerializer;",
        "",
        "()V",
        "deserializeState",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "snapshotState",
        "state",
        "isNetworkRequestState",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedSerializer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 39
    new-instance v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedSerializer;

    invoke-direct {v0}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedSerializer;-><init>()V

    sput-object v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedSerializer;->INSTANCE:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedSerializer;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final isNetworkRequestState(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;)Z
    .locals 1

    .line 111
    instance-of v0, p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$StartingCardActivation;

    if-nez v0, :cond_1

    .line 112
    instance-of v0, p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingActivationCode;

    if-nez v0, :cond_1

    .line 113
    instance-of v0, p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingSquareCardInfo;

    if-nez v0, :cond_1

    .line 114
    instance-of p1, p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$SettingPin;

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method


# virtual methods
.method public final deserializeState(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;
    .locals 11

    const-string v0, "snapshot"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    invoke-virtual {p1}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    .line 120
    new-instance v0, Lokio/Buffer;

    invoke-direct {v0}, Lokio/Buffer;-><init>()V

    invoke-virtual {v0, p1}, Lokio/Buffer;->write(Lokio/ByteString;)Lokio/Buffer;

    move-result-object p1

    check-cast p1, Lokio/BufferedSource;

    .line 77
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v0

    .line 78
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-string v2, "Class.forName(className)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/jvm/JvmClassMappingKt;->getKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    .line 121
    sget-object v2, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const-class v3, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    invoke-virtual {v2, v3}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/squareup/workflow/BuffersProtos;->readProtoWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/Message;

    move-result-object v2

    .line 80
    move-object v4, v2

    check-cast v4, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    .line 82
    const-class v2, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$ShowingCardDetails;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$ShowingCardDetails;

    invoke-direct {p1, v4}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$ShowingCardDetails;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V

    check-cast p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;

    goto/16 :goto_0

    .line 83
    :cond_0
    const-class v2, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$ShowingDepositsInfo;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$ShowingDepositsInfo;

    invoke-direct {p1, v4}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$ShowingDepositsInfo;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V

    check-cast p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;

    goto/16 :goto_0

    .line 84
    :cond_1
    const-class v2, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$StartingCardActivationFailed;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$StartingCardActivationFailed;

    invoke-direct {p1, v4}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$StartingCardActivationFailed;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V

    check-cast p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;

    goto/16 :goto_0

    .line 85
    :cond_2
    const-class v2, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForActivationCode;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    new-instance p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForActivationCode;

    invoke-direct {p1, v4}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForActivationCode;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V

    check-cast p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;

    goto/16 :goto_0

    .line 86
    :cond_3
    const-class v2, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingActivationCodeFailed;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 122
    const-class v0, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Enum;

    invoke-interface {p1}, Lokio/BufferedSource;->readInt()I

    move-result p1

    aget-object p1, v0, p1

    const-string v0, "T::class.java.enumConstants[readInt()]"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;

    .line 86
    new-instance v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingActivationCodeFailed;

    invoke-direct {v0, v4, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingActivationCodeFailed;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;

    goto/16 :goto_0

    .line 89
    :cond_4
    const-class v2, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    new-instance v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;

    .line 90
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v5

    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v6

    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/16 v9, 0x10

    const/4 v10, 0x0

    move-object v3, v0

    .line 89
    invoke-direct/range {v3 .. v10}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;

    goto/16 :goto_0

    .line 92
    :cond_5
    const-class v2, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingSquareCardInfoFailed;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    new-instance v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingSquareCardInfoFailed;

    .line 93
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result v5

    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v6

    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v7

    .line 94
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v8

    move-object v3, v0

    .line 92
    invoke-direct/range {v3 .. v8}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingSquareCardInfoFailed;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;

    goto/16 :goto_0

    .line 96
    :cond_6
    const-class v2, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForPin;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    new-instance v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForPin;

    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result p1

    invoke-direct {v0, v4, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForPin;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Z)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;

    goto/16 :goto_0

    .line 97
    :cond_7
    const-class v2, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$SettingPinFailed;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    new-instance v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$SettingPinFailed;

    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result p1

    invoke-direct {v0, v4, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$SettingPinFailed;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Z)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;

    goto/16 :goto_0

    .line 98
    :cond_8
    const-class v2, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$ShowingSquareCardActivationComplete;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    new-instance p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$ShowingSquareCardActivationComplete;

    invoke-direct {p1, v4}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$ShowingSquareCardActivationComplete;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V

    check-cast p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;

    goto :goto_0

    .line 99
    :cond_9
    const-class v2, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$StartCardConfirmAddress;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    new-instance p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$StartCardConfirmAddress;

    invoke-direct {p1, v4}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$StartCardConfirmAddress;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V

    check-cast p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;

    goto :goto_0

    .line 100
    :cond_a
    const-class v2, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingToConfirmAddressInfo;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    new-instance v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingToConfirmAddressInfo;

    invoke-static {p1}, Lcom/squareup/address/AddressSerializationKt;->readAddress(Lokio/BufferedSource;)Lcom/squareup/address/Address;

    move-result-object p1

    invoke-direct {v0, v4, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingToConfirmAddressInfo;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Lcom/squareup/address/Address;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;

    goto :goto_0

    .line 101
    :cond_b
    const-class v2, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingCardAddress;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    new-instance v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingCardAddress;

    invoke-static {p1}, Lcom/squareup/address/AddressSerializationKt;->readAddress(Lokio/BufferedSource;)Lcom/squareup/address/Address;

    move-result-object p1

    invoke-direct {v0, v4, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingCardAddress;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Lcom/squareup/address/Address;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;

    goto :goto_0

    .line 102
    :cond_c
    const-class v2, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$MissingInformationWarning;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    new-instance v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$MissingInformationWarning;

    .line 103
    sget-object v1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedSerializer;->INSTANCE:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedSerializer;

    sget-object v2, Lcom/squareup/workflow/Snapshot;->Companion:Lcom/squareup/workflow/Snapshot$Companion;

    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readByteStringWithLength(Lokio/BufferedSource;)Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/workflow/Snapshot$Companion;->of(Lokio/ByteString;)Lcom/squareup/workflow/Snapshot;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedSerializer;->deserializeState(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;

    move-result-object v1

    invoke-interface {p1}, Lokio/BufferedSource;->readInt()I

    move-result v2

    .line 104
    invoke-interface {p1}, Lokio/BufferedSource;->readInt()I

    move-result p1

    .line 102
    invoke-direct {v0, v4, v1, v2, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$MissingInformationWarning;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;II)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;

    :goto_0
    return-object p1

    .line 106
    :cond_d
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unrecognized state "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public final snapshotState(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;)Lcom/squareup/workflow/Snapshot;
    .locals 2

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedSerializer;->isNetworkRequestState(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/workflow/Snapshot;->EMPTY:Lcom/squareup/workflow/Snapshot;

    return-object p1

    .line 42
    :cond_0
    sget-object v0, Lcom/squareup/workflow/Snapshot;->Companion:Lcom/squareup/workflow/Snapshot$Companion;

    new-instance v1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedSerializer$snapshotState$1;

    invoke-direct {v1, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedSerializer$snapshotState$1;-><init>(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/Snapshot$Companion;->write(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
