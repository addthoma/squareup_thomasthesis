.class final Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$updateDismissListener$1;
.super Ljava/lang/Object;
.source "OrderSquareCardStampsDialogFactory.kt"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory;->updateDismissListener(Lcom/squareup/workflow/legacy/WorkflowInput;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Landroid/content/DialogInterface;",
        "kotlin.jvm.PlatformType",
        "onDismiss"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

.field final synthetic this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$updateDismissListener$1;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$updateDismissListener$1;->$workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 1

    .line 80
    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$updateDismissListener$1;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory;

    invoke-static {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory;->access$getChosenStamp$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory;)Lcom/squareup/protos/client/bizbank/Stamp;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 81
    new-instance p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialog$Events$ExitWithStamp;

    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$updateDismissListener$1;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory;

    invoke-static {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory;->access$getChosenStamp$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory;)Lcom/squareup/protos/client/bizbank/Stamp;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-direct {p1, v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialog$Events$ExitWithStamp;-><init>(Lcom/squareup/protos/client/bizbank/Stamp;)V

    check-cast p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialog$Events;

    goto :goto_0

    .line 83
    :cond_1
    sget-object p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialog$Events$ExitWithoutSelectingStamps;->INSTANCE:Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialog$Events$ExitWithoutSelectingStamps;

    check-cast p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialog$Events;

    .line 85
    :goto_0
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardStampsDialogFactory$updateDismissListener$1;->$workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    invoke-interface {v0, p1}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method
