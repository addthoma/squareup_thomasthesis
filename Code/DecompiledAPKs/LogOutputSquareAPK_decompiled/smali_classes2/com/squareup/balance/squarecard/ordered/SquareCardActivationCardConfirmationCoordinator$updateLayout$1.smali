.class final Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator$updateLayout$1;
.super Ljava/lang/Object;
.source "SquareCardActivationCardConfirmationCoordinator.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;->updateLayout(Lcom/squareup/workflow/legacy/Screen;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "run"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $screen:Lcom/squareup/workflow/legacy/Screen;

.field final synthetic this$0:Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator$updateLayout$1;->this$0:Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator$updateLayout$1;->$screen:Lcom/squareup/workflow/legacy/Screen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .line 116
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator$updateLayout$1;->$screen:Lcom/squareup/workflow/legacy/Screen;

    iget-object v0, v0, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 117
    new-instance v1, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$Event$CardConfirmationManualEntry;

    .line 118
    iget-object v2, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator$updateLayout$1;->this$0:Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;

    invoke-static {v2}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;->access$getSquareCardValidationEntryView$p(Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;)Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->getCvv()Ljava/lang/String;

    move-result-object v2

    .line 119
    iget-object v3, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator$updateLayout$1;->this$0:Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;

    invoke-static {v3}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;->access$getSquareCardValidationEntryView$p(Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmationCoordinator;)Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->getExpiration()Ljava/lang/String;

    move-result-object v3

    .line 117
    invoke-direct {v1, v2, v3}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$Event$CardConfirmationManualEntry;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    invoke-interface {v0, v1}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method
