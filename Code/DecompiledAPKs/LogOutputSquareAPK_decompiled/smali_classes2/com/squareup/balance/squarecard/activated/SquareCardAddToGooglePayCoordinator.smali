.class public final Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "SquareCardAddToGooglePayCoordinator.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u0008\n\u0002\u0008\u0005\u0018\u00002\u00020\u0001B)\u0012\"\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\nH\u0016J\u0010\u0010\u0010\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\nH\u0002J\u0010\u0010\u0011\u001a\u00020\u000e2\u0006\u0010\u0012\u001a\u00020\u0005H\u0002J\u001c\u0010\u0013\u001a\u00020\u000e2\u0008\u0008\u0001\u0010\u0014\u001a\u00020\u00152\u0008\u0008\u0001\u0010\u0016\u001a\u00020\u0015H\u0002J\u0008\u0010\u0017\u001a\u00020\u000eH\u0002J\u001c\u0010\u0018\u001a\u00020\u000e2\u0008\u0008\u0001\u0010\u0014\u001a\u00020\u00152\u0008\u0008\u0001\u0010\u0016\u001a\u00020\u0015H\u0002J\u0018\u0010\u0019\u001a\u00020\u000e2\u0006\u0010\u0012\u001a\u00020\u00052\u0006\u0010\u000f\u001a\u00020\nH\u0002R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R*\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "(Lio/reactivex/Observable;)V",
        "loading",
        "Landroid/view/View;",
        "message",
        "Lcom/squareup/noho/NohoMessageView;",
        "attach",
        "",
        "view",
        "bindViews",
        "handleBack",
        "screen",
        "showError",
        "titleRes",
        "",
        "messageRes",
        "showLoading",
        "showSuccess",
        "update",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private loading:Landroid/view/View;

.field private message:Lcom/squareup/noho/NohoMessageView;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;)V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayCoordinator;->screens:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$handleBack(Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayCoordinator;Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayScreen;)V
    .locals 0

    .line 19
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayCoordinator;->handleBack(Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayScreen;)V

    return-void
.end method

.method public static final synthetic access$update(Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayCoordinator;Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayScreen;Landroid/view/View;)V
    .locals 0

    .line 19
    invoke-direct {p0, p1, p2}, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayCoordinator;->update(Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayScreen;Landroid/view/View;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 73
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->google_pay_message:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.google_pay_message)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/noho/NohoMessageView;

    iput-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayCoordinator;->message:Lcom/squareup/noho/NohoMessageView;

    .line 74
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->google_pay_loading:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo v0, "view.findViewById(R.id.google_pay_loading)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayCoordinator;->loading:Landroid/view/View;

    return-void
.end method

.method private final handleBack(Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayScreen;)V
    .locals 1

    .line 48
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayScreen;->getOnEvent()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    sget-object v0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayScreen$Event$GoBack;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayScreen$Event$GoBack;

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private final showError(II)V
    .locals 3

    .line 57
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayCoordinator;->loading:Landroid/view/View;

    if-nez v0, :cond_0

    const-string v1, "loading"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 58
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayCoordinator;->message:Lcom/squareup/noho/NohoMessageView;

    const-string v1, "message"

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoMessageView;->setVisibility(I)V

    .line 59
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayCoordinator;->message:Lcom/squareup/noho/NohoMessageView;

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoMessageView;->setTitle(I)V

    .line 60
    iget-object p1, p0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayCoordinator;->message:Lcom/squareup/noho/NohoMessageView;

    if-nez p1, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoMessageView;->setMessage(I)V

    .line 61
    iget-object p1, p0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayCoordinator;->message:Lcom/squareup/noho/NohoMessageView;

    if-nez p1, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    sget p2, Lcom/squareup/vectoricons/R$drawable;->ui_triangle_warning_80:I

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoMessageView;->setDrawable(I)V

    return-void
.end method

.method private final showLoading()V
    .locals 2

    .line 52
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayCoordinator;->loading:Landroid/view/View;

    if-nez v0, :cond_0

    const-string v1, "loading"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 53
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayCoordinator;->message:Lcom/squareup/noho/NohoMessageView;

    if-nez v0, :cond_1

    const-string v1, "message"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoMessageView;->setVisibility(I)V

    return-void
.end method

.method private final showSuccess(II)V
    .locals 3

    .line 65
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayCoordinator;->loading:Landroid/view/View;

    if-nez v0, :cond_0

    const-string v1, "loading"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 66
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayCoordinator;->message:Lcom/squareup/noho/NohoMessageView;

    const-string v1, "message"

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoMessageView;->setVisibility(I)V

    .line 67
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayCoordinator;->message:Lcom/squareup/noho/NohoMessageView;

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoMessageView;->setTitle(I)V

    .line 68
    iget-object p1, p0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayCoordinator;->message:Lcom/squareup/noho/NohoMessageView;

    if-nez p1, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoMessageView;->setMessage(I)V

    .line 69
    iget-object p1, p0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayCoordinator;->message:Lcom/squareup/noho/NohoMessageView;

    if-nez p1, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    sget p2, Lcom/squareup/vectoricons/R$drawable;->circle_check_96:I

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoMessageView;->setDrawable(I)V

    return-void
.end method

.method private final update(Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayScreen;Landroid/view/View;)V
    .locals 3

    .line 37
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayScreen;->getData()Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayScreen$ScreenData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayScreen$ScreenData;->getStatus()Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayScreen$Status;

    move-result-object v0

    .line 38
    instance-of v1, v0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayScreen$Status$UnknownError;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayScreen$Status$UnknownError;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayScreen$Status$UnknownError;->getTitle()I

    move-result v1

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayScreen$Status$UnknownError;->getMessage()I

    move-result v0

    invoke-direct {p0, v1, v0}, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayCoordinator;->showError(II)V

    goto :goto_0

    .line 39
    :cond_0
    instance-of v1, v0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayScreen$Status$CardAddedSuccessfully;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayScreen$Status$CardAddedSuccessfully;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayScreen$Status$CardAddedSuccessfully;->getTitle()I

    move-result v1

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayScreen$Status$CardAddedSuccessfully;->getMessage()I

    move-result v0

    invoke-direct {p0, v1, v0}, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayCoordinator;->showSuccess(II)V

    goto :goto_0

    .line 40
    :cond_1
    instance-of v0, v0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayScreen$Status$Loading;

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayCoordinator;->showLoading()V

    .line 43
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayCoordinator;->message:Lcom/squareup/noho/NohoMessageView;

    if-nez v0, :cond_3

    const-string v1, "message"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    new-instance v1, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayCoordinator$update$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayCoordinator$update$1;-><init>(Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayCoordinator;Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayScreen;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounceRunnable(Ljava/lang/Runnable;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v1

    const-string v2, "debounceRunnable { handleBack(screen) }"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoMessageView;->setPrimaryButtonOnClickListener(Lcom/squareup/debounce/DebouncedOnClickListener;)V

    .line 44
    new-instance v0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayCoordinator$update$2;

    invoke-direct {v0, p0, p1}, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayCoordinator$update$2;-><init>(Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayCoordinator;Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayCoordinator;->bindViews(Landroid/view/View;)V

    .line 28
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayCoordinator;->showLoading()V

    .line 30
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayCoordinator;->screens:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayCoordinator$attach$1;-><init>(Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayCoordinator;Landroid/view/View;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method
