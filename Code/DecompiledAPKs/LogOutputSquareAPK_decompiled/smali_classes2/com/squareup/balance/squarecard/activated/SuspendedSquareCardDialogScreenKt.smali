.class public final Lcom/squareup/balance/squarecard/activated/SuspendedSquareCardDialogScreenKt;
.super Ljava/lang/Object;
.source "SuspendedSquareCardDialogScreen.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "dismiss",
        "",
        "Lcom/squareup/balance/squarecard/activated/SuspendedSquareCardDialogScreen;",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final dismiss(Lcom/squareup/balance/squarecard/activated/SuspendedSquareCardDialogScreen;)V
    .locals 1

    const-string v0, "$this$dismiss"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/activated/SuspendedSquareCardDialogScreen;->getOnEvent()Lkotlin/jvm/functions/Function1;

    move-result-object p0

    sget-object v0, Lcom/squareup/balance/squarecard/activated/SuspendedSquareCardDialogScreen$DialogDismissed;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SuspendedSquareCardDialogScreen$DialogDismissed;

    invoke-interface {p0, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
