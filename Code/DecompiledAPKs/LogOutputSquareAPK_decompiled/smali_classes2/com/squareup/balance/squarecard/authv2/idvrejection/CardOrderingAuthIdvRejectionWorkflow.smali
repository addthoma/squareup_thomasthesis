.class public final Lcom/squareup/balance/squarecard/authv2/idvrejection/CardOrderingAuthIdvRejectionWorkflow;
.super Lcom/squareup/workflow/StatelessWorkflow;
.source "CardOrderingAuthIdvRejectionWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatelessWorkflow<",
        "Lkotlin/Unit;",
        "Lcom/squareup/balance/squarecard/authv2/idvrejection/CardOrderingAuthIdvRejectionOutput;",
        "Lcom/squareup/workflow/legacy/Screen;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000;\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0002*\u0001\r\u0018\u00002&\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0016\u0012\u0014\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00020\u00050\u0004j\u0006\u0012\u0002\u0008\u0003`\u00060\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ;\u0010\u000f\u001a\u0014\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00020\u00050\u0004j\u0006\u0012\u0002\u0008\u0003`\u00062\u0006\u0010\u0010\u001a\u00020\u00022\u0012\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00030\u0012H\u0016\u00a2\u0006\u0002\u0010\u0013R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\n\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00030\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010\u000e\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/authv2/idvrejection/CardOrderingAuthIdvRejectionWorkflow;",
        "Lcom/squareup/workflow/StatelessWorkflow;",
        "",
        "Lcom/squareup/balance/squarecard/authv2/idvrejection/CardOrderingAuthIdvRejectionOutput;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "analytics",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;",
        "(Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;)V",
        "finish",
        "Lcom/squareup/workflow/WorkflowAction;",
        "logAnalytics",
        "com/squareup/balance/squarecard/authv2/idvrejection/CardOrderingAuthIdvRejectionWorkflow$logAnalytics$1",
        "Lcom/squareup/balance/squarecard/authv2/idvrejection/CardOrderingAuthIdvRejectionWorkflow$logAnalytics$1;",
        "render",
        "props",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "(Lkotlin/Unit;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/Screen;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;

.field private final finish:Lcom/squareup/workflow/WorkflowAction;

.field private final logAnalytics:Lcom/squareup/balance/squarecard/authv2/idvrejection/CardOrderingAuthIdvRejectionWorkflow$logAnalytics$1;


# direct methods
.method public constructor <init>(Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "analytics"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0}, Lcom/squareup/workflow/StatelessWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/authv2/idvrejection/CardOrderingAuthIdvRejectionWorkflow;->analytics:Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;

    .line 25
    sget-object p1, Lcom/squareup/balance/squarecard/authv2/idvrejection/CardOrderingAuthIdvRejectionWorkflow$finish$1;->INSTANCE:Lcom/squareup/balance/squarecard/authv2/idvrejection/CardOrderingAuthIdvRejectionWorkflow$finish$1;

    check-cast p1, Lkotlin/jvm/functions/Function1;

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {p0, v0, p1, v1, v0}, Lcom/squareup/workflow/StatelessWorkflowKt;->action$default(Lcom/squareup/workflow/StatelessWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/balance/squarecard/authv2/idvrejection/CardOrderingAuthIdvRejectionWorkflow;->finish:Lcom/squareup/workflow/WorkflowAction;

    .line 26
    new-instance p1, Lcom/squareup/balance/squarecard/authv2/idvrejection/CardOrderingAuthIdvRejectionWorkflow$logAnalytics$1;

    invoke-direct {p1, p0}, Lcom/squareup/balance/squarecard/authv2/idvrejection/CardOrderingAuthIdvRejectionWorkflow$logAnalytics$1;-><init>(Lcom/squareup/balance/squarecard/authv2/idvrejection/CardOrderingAuthIdvRejectionWorkflow;)V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/authv2/idvrejection/CardOrderingAuthIdvRejectionWorkflow;->logAnalytics:Lcom/squareup/balance/squarecard/authv2/idvrejection/CardOrderingAuthIdvRejectionWorkflow$logAnalytics$1;

    return-void
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/balance/squarecard/authv2/idvrejection/CardOrderingAuthIdvRejectionWorkflow;)Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/squareup/balance/squarecard/authv2/idvrejection/CardOrderingAuthIdvRejectionWorkflow;->analytics:Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;

    return-object p0
.end method

.method public static final synthetic access$getFinish$p(Lcom/squareup/balance/squarecard/authv2/idvrejection/CardOrderingAuthIdvRejectionWorkflow;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/squareup/balance/squarecard/authv2/idvrejection/CardOrderingAuthIdvRejectionWorkflow;->finish:Lcom/squareup/workflow/WorkflowAction;

    return-object p0
.end method


# virtual methods
.method public render(Lkotlin/Unit;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/Screen;
    .locals 8

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "context"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    iget-object p1, p0, Lcom/squareup/balance/squarecard/authv2/idvrejection/CardOrderingAuthIdvRejectionWorkflow;->logAnalytics:Lcom/squareup/balance/squarecard/authv2/idvrejection/CardOrderingAuthIdvRejectionWorkflow$logAnalytics$1;

    check-cast p1, Lcom/squareup/workflow/Worker;

    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-static {p2, p1, v0, v1, v0}, Lcom/squareup/workflow/RenderContextKt;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;ILjava/lang/Object;)V

    .line 38
    new-instance p1, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;

    .line 39
    new-instance v7, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Failed;

    .line 40
    new-instance v0, Lcom/squareup/util/ViewString$ResourceString;

    sget v1, Lcom/squareup/balance/squarecard/impl/R$string;->order_card_customization_idv_error_title:I

    invoke-direct {v0, v1}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    move-object v1, v0

    check-cast v1, Lcom/squareup/util/ViewString;

    .line 41
    new-instance v0, Lcom/squareup/util/ViewString$ResourceString;

    sget v2, Lcom/squareup/balance/squarecard/impl/R$string;->order_card_customization_idv_error_message:I

    invoke-direct {v0, v2}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    move-object v2, v0

    check-cast v2, Lcom/squareup/util/ViewString;

    .line 42
    sget v3, Lcom/squareup/common/strings/R$string;->okay:I

    const/4 v4, 0x0

    const/16 v5, 0x8

    const/4 v6, 0x0

    move-object v0, v7

    .line 39
    invoke-direct/range {v0 .. v6}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Failed;-><init>(Lcom/squareup/util/ViewString;Lcom/squareup/util/ViewString;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v7, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData;

    .line 44
    new-instance v0, Lcom/squareup/balance/squarecard/authv2/idvrejection/CardOrderingAuthIdvRejectionWorkflow$render$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/balance/squarecard/authv2/idvrejection/CardOrderingAuthIdvRejectionWorkflow$render$1;-><init>(Lcom/squareup/balance/squarecard/authv2/idvrejection/CardOrderingAuthIdvRejectionWorkflow;Lcom/squareup/workflow/RenderContext;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 38
    invoke-direct {p1, v7, v0}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;-><init>(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData;Lkotlin/jvm/functions/Function1;)V

    .line 51
    invoke-static {p1}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreenKt;->asDetailScreen(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 22
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/squarecard/authv2/idvrejection/CardOrderingAuthIdvRejectionWorkflow;->render(Lkotlin/Unit;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    return-object p1
.end method
