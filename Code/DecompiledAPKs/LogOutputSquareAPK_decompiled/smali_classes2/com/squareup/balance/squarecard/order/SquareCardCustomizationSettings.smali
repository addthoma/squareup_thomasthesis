.class public final Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;
.super Ljava/lang/Object;
.source "SquareCardCustomizationSettings.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSquareCardCustomizationSettings.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SquareCardCustomizationSettings.kt\ncom/squareup/balance/squarecard/order/SquareCardCustomizationSettings\n*L\n1#1,107:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u0007\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0010\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u00002\u00020\u0001B-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0006\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0006H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\u0006H\u00c6\u0003J\t\u0010\u0017\u001a\u00020\tH\u00c6\u0003J;\u0010\u0018\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0008\u001a\u00020\tH\u00c6\u0001J\u0013\u0010\u0019\u001a\u00020\u001a2\u0008\u0010\u001b\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u001c\u001a\u00020\u001dH\u00d6\u0001J\u0006\u0010\u001e\u001a\u00020\u001aJ\t\u0010\u001f\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0011\u0010\u0007\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0010R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u000c\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;",
        "",
        "ownerName",
        "",
        "businessName",
        "minInkLevel",
        "",
        "maxInkLevel",
        "idvSettings",
        "Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;",
        "(Ljava/lang/String;Ljava/lang/String;FFLcom/squareup/balance/squarecard/order/SquareCardIdvSettings;)V",
        "getBusinessName",
        "()Ljava/lang/String;",
        "getIdvSettings",
        "()Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;",
        "getMaxInkLevel",
        "()F",
        "getMinInkLevel",
        "getOwnerName",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "isValidForCardOrdering",
        "toString",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final businessName:Ljava/lang/String;

.field private final idvSettings:Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;

.field private final maxInkLevel:F

.field private final minInkLevel:F

.field private final ownerName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;FFLcom/squareup/balance/squarecard/order/SquareCardIdvSettings;)V
    .locals 1

    const-string v0, "ownerName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "businessName"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "idvSettings"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;->ownerName:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;->businessName:Ljava/lang/String;

    iput p3, p0, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;->minInkLevel:F

    iput p4, p0, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;->maxInkLevel:F

    iput-object p5, p0, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;->idvSettings:Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;Ljava/lang/String;Ljava/lang/String;FFLcom/squareup/balance/squarecard/order/SquareCardIdvSettings;ILjava/lang/Object;)Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;->ownerName:Ljava/lang/String;

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-object p2, p0, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;->businessName:Ljava/lang/String;

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget p3, p0, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;->minInkLevel:F

    :cond_2
    move v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget p4, p0, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;->maxInkLevel:F

    :cond_3
    move v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;->idvSettings:Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;

    :cond_4
    move-object v2, p5

    move-object p2, p0

    move-object p3, p1

    move-object p4, p7

    move p5, v0

    move p6, v1

    move-object p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;->copy(Ljava/lang/String;Ljava/lang/String;FFLcom/squareup/balance/squarecard/order/SquareCardIdvSettings;)Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;->ownerName:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;->businessName:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()F
    .locals 1

    iget v0, p0, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;->minInkLevel:F

    return v0
.end method

.method public final component4()F
    .locals 1

    iget v0, p0, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;->maxInkLevel:F

    return v0
.end method

.method public final component5()Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;
    .locals 1

    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;->idvSettings:Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;Ljava/lang/String;FFLcom/squareup/balance/squarecard/order/SquareCardIdvSettings;)Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;
    .locals 7

    const-string v0, "ownerName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "businessName"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "idvSettings"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;-><init>(Ljava/lang/String;Ljava/lang/String;FFLcom/squareup/balance/squarecard/order/SquareCardIdvSettings;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;

    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;->ownerName:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;->ownerName:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;->businessName:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;->businessName:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;->minInkLevel:F

    iget v1, p1, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;->minInkLevel:F

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;->maxInkLevel:F

    iget v1, p1, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;->maxInkLevel:F

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;->idvSettings:Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;

    iget-object p1, p1, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;->idvSettings:Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getBusinessName()Ljava/lang/String;
    .locals 1

    .line 60
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;->businessName:Ljava/lang/String;

    return-object v0
.end method

.method public final getIdvSettings()Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;->idvSettings:Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;

    return-object v0
.end method

.method public final getMaxInkLevel()F
    .locals 1

    .line 62
    iget v0, p0, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;->maxInkLevel:F

    return v0
.end method

.method public final getMinInkLevel()F
    .locals 1

    .line 61
    iget v0, p0, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;->minInkLevel:F

    return v0
.end method

.method public final getOwnerName()Ljava/lang/String;
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;->ownerName:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;->ownerName:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;->businessName:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;->minInkLevel:F

    invoke-static {v2}, L$r8$java8methods$utility$Float$hashCode$IF;->hashCode(F)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;->maxInkLevel:F

    invoke-static {v2}, L$r8$java8methods$utility$Float$hashCode$IF;->hashCode(F)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;->idvSettings:Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public final isValidForCardOrdering()Z
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;->ownerName:Ljava/lang/String;

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SquareCardCustomizationSettings(ownerName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;->ownerName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", businessName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;->businessName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", minInkLevel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;->minInkLevel:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", maxInkLevel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;->maxInkLevel:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", idvSettings="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;->idvSettings:Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
