.class final Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$2;
.super Lkotlin/jvm/internal/Lambda;
.source "SquareCardActivatedWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->render(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedInput;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Boolean;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;",
        "+",
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;",
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedResult;",
        "isAvailable",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $screenState:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;

.field final synthetic $state:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$2;->$state:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$2;->$screenState:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Z)Lcom/squareup/workflow/WorkflowAction;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;",
            "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedResult;",
            ">;"
        }
    .end annotation

    .line 185
    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$2;->$state:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;

    iget-object v2, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$2;->$screenState:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;

    move-object v3, v2

    check-cast v3, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$ShowingCardDetails;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x3

    const/4 v8, 0x0

    move v6, p1

    invoke-static/range {v3 .. v8}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$ShowingCardDetails;->copy$default(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$ShowingCardDetails;ZZZILjava/lang/Object;)Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$ShowingCardDetails;

    move-result-object p1

    check-cast p1, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;

    invoke-static {v1, p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedWorkflowKt;->access$plus(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;)Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;

    move-result-object p1

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-static {v0, p1, v1, v2, v1}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 107
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$2;->invoke(Z)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
