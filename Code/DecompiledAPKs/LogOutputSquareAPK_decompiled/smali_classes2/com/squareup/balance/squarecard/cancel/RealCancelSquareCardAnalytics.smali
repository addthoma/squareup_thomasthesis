.class public final Lcom/squareup/balance/squarecard/cancel/RealCancelSquareCardAnalytics;
.super Ljava/lang/Object;
.source "RealCancelSquareCardAnalytics.kt"

# interfaces
.implements Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0015\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0005\u001a\u00020\u0006H\u0016J\u0010\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0008\u001a\u00020\tH\u0002J\u0008\u0010\n\u001a\u00020\u0006H\u0016J\u0008\u0010\u000b\u001a\u00020\u0006H\u0016J\u0008\u0010\u000c\u001a\u00020\u0006H\u0016J\u0008\u0010\r\u001a\u00020\u0006H\u0016J\u0008\u0010\u000e\u001a\u00020\u0006H\u0016J\u0008\u0010\u000f\u001a\u00020\u0006H\u0016J\u0008\u0010\u0010\u001a\u00020\u0006H\u0016J\u0010\u0010\u0011\u001a\u00020\u00062\u0006\u0010\u0008\u001a\u00020\tH\u0002J\u0008\u0010\u0012\u001a\u00020\u0006H\u0016J\u0008\u0010\u0013\u001a\u00020\u0006H\u0016J\u0008\u0010\u0014\u001a\u00020\u0006H\u0016J\u0008\u0010\u0015\u001a\u00020\u0006H\u0016J\u0008\u0010\u0016\u001a\u00020\u0006H\u0016J\u0008\u0010\u0017\u001a\u00020\u0006H\u0016J\u0008\u0010\u0018\u001a\u00020\u0006H\u0016J\u0008\u0010\u0019\u001a\u00020\u0006H\u0016J\u0008\u0010\u001a\u001a\u00020\u0006H\u0016J\u0008\u0010\u001b\u001a\u00020\u0006H\u0016J\u0008\u0010\u001c\u001a\u00020\u0006H\u0016J\u0008\u0010\u001d\u001a\u00020\u0006H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/cancel/RealCancelSquareCardAnalytics;",
        "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;",
        "squareCardAnalyticsLogger",
        "Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;",
        "(Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;)V",
        "logCancelCardErrorScreen",
        "",
        "logClick",
        "description",
        "",
        "logDeactivateCardGeneric",
        "logDeactivateLostCard",
        "logDeactivateNeverReceivedCard",
        "logDeactivateStolenCard",
        "logGenericCardDeactivatedScreen",
        "logGenericDeactivatedOkay",
        "logGenericDeactivatedQuit",
        "logImpression",
        "logLostCardDeactivatedScreen",
        "logNeverReceivedCardDeactivatedScreen",
        "logReplaceLostCard",
        "logReplaceNeverReceivedCard",
        "logReplaceStolenCard",
        "logReportCancelCardForGenericReason",
        "logReportLostCard",
        "logReportNeverReceivedCard",
        "logReportStolenCard",
        "logStolenCardDeactivatedScreen",
        "logSwitchToDepositsError",
        "logSwitchToRegularDeposits",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final squareCardAnalyticsLogger:Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;


# direct methods
.method public constructor <init>(Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "squareCardAnalyticsLogger"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/cancel/RealCancelSquareCardAnalytics;->squareCardAnalyticsLogger:Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;

    return-void
.end method

.method private final logClick(Ljava/lang/String;)V
    .locals 1

    .line 119
    iget-object v0, p0, Lcom/squareup/balance/squarecard/cancel/RealCancelSquareCardAnalytics;->squareCardAnalyticsLogger:Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;

    invoke-virtual {v0, p1}, Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method private final logImpression(Ljava/lang/String;)V
    .locals 1

    .line 123
    iget-object v0, p0, Lcom/squareup/balance/squarecard/cancel/RealCancelSquareCardAnalytics;->squareCardAnalyticsLogger:Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;

    invoke-virtual {v0, p1}, Lcom/squareup/balance/squarecard/SquareCardAnalyticsLogger;->logImpression(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public logCancelCardErrorScreen()V
    .locals 1

    const-string v0, "Cancel Card: Deactivation Error"

    .line 111
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/cancel/RealCancelSquareCardAnalytics;->logImpression(Ljava/lang/String;)V

    return-void
.end method

.method public logDeactivateCardGeneric()V
    .locals 1

    const-string v0, "Cancel Card: Generic Cancel Deactivate"

    .line 67
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/cancel/RealCancelSquareCardAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logDeactivateLostCard()V
    .locals 1

    const-string v0, "Cancel Card: Lost Card Deactivate"

    .line 55
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/cancel/RealCancelSquareCardAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logDeactivateNeverReceivedCard()V
    .locals 1

    const-string v0, "Cancel Card: Never Received Card Deactivate"

    .line 63
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/cancel/RealCancelSquareCardAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logDeactivateStolenCard()V
    .locals 1

    const-string v0, "Cancel Card: Card Stolen Deactivate"

    .line 59
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/cancel/RealCancelSquareCardAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logGenericCardDeactivatedScreen()V
    .locals 1

    const-string v0, "Cancel Card: Generic Cancel Card Deactivated"

    .line 107
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/cancel/RealCancelSquareCardAnalytics;->logImpression(Ljava/lang/String;)V

    return-void
.end method

.method public logGenericDeactivatedOkay()V
    .locals 1

    const-string v0, "Cancel Card: Generic Cancel Card Deactivated Okay"

    .line 91
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/cancel/RealCancelSquareCardAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logGenericDeactivatedQuit()V
    .locals 1

    const-string v0, "Cancel Card: Generic Cancel Card Deactivated Quit"

    .line 87
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/cancel/RealCancelSquareCardAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logLostCardDeactivatedScreen()V
    .locals 1

    const-string v0, "Cancel Card: Lost Card Deactivated"

    .line 95
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/cancel/RealCancelSquareCardAnalytics;->logImpression(Ljava/lang/String;)V

    return-void
.end method

.method public logNeverReceivedCardDeactivatedScreen()V
    .locals 1

    const-string v0, "Cancel Card: Never Received Card Deactivated"

    .line 103
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/cancel/RealCancelSquareCardAnalytics;->logImpression(Ljava/lang/String;)V

    return-void
.end method

.method public logReplaceLostCard()V
    .locals 1

    const-string v0, "Cancel Card: Lost Card Order Replacement"

    .line 71
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/cancel/RealCancelSquareCardAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logReplaceNeverReceivedCard()V
    .locals 1

    const-string v0, "Cancel Card: Never Received Card Order Replacement"

    .line 79
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/cancel/RealCancelSquareCardAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logReplaceStolenCard()V
    .locals 1

    const-string v0, "Cancel Card: Stolen Card Order Replacement"

    .line 75
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/cancel/RealCancelSquareCardAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logReportCancelCardForGenericReason()V
    .locals 1

    const-string v0, "Cancel Card: Generic Cancel"

    .line 51
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/cancel/RealCancelSquareCardAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logReportLostCard()V
    .locals 1

    const-string v0, "Cancel Card: Lost Card"

    .line 39
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/cancel/RealCancelSquareCardAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logReportNeverReceivedCard()V
    .locals 1

    const-string v0, "Cancel Card: Never Received Card"

    .line 47
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/cancel/RealCancelSquareCardAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logReportStolenCard()V
    .locals 1

    const-string v0, "Cancel Card: Card Stolen"

    .line 43
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/cancel/RealCancelSquareCardAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public logStolenCardDeactivatedScreen()V
    .locals 1

    const-string v0, "Cancel Card: Stolen Card Deactivated"

    .line 99
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/cancel/RealCancelSquareCardAnalytics;->logImpression(Ljava/lang/String;)V

    return-void
.end method

.method public logSwitchToDepositsError()V
    .locals 1

    const-string v0, "Cancel Card: Generic Cancel Switch To Regular Deposits Error"

    .line 115
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/cancel/RealCancelSquareCardAnalytics;->logImpression(Ljava/lang/String;)V

    return-void
.end method

.method public logSwitchToRegularDeposits()V
    .locals 1

    const-string v0, "Cancel Card: Generic Cancel Switch To Regular Deposits"

    .line 83
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/cancel/RealCancelSquareCardAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method
