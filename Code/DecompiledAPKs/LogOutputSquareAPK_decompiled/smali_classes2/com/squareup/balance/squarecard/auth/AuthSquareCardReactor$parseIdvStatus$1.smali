.class final Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$parseIdvStatus$1;
.super Ljava/lang/Object;
.source "AuthSquareCardReactor.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;->parseIdvStatus(Lcom/squareup/server/AcceptedResponse;Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001\"\u0008\u0008\u0000\u0010\u0004*\u00020\u00052\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u0002H\u00040\u0007H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/Reaction;",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardResult;",
        "T",
        "",
        "it",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $idvSettings:Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;

.field final synthetic $responseParser:Lkotlin/jvm/functions/Function1;

.field final synthetic this$0:Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;Lkotlin/jvm/functions/Function1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$parseIdvStatus$1;->this$0:Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$parseIdvStatus$1;->$idvSettings:Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;

    iput-object p3, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$parseIdvStatus$1;->$responseParser:Lkotlin/jvm/functions/Function1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/workflow/legacy/Reaction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "+TT;>;)",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardResult;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 311
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$parseIdvStatus$1;->this$0:Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$parseIdvStatus$1;->$idvSettings:Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;

    iget-object v2, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$parseIdvStatus$1;->$responseParser:Lkotlin/jvm/functions/Function1;

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    invoke-interface {v2, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;

    invoke-static {v0, v1, p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;->access$checkIdvStatus(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;)Lcom/squareup/workflow/legacy/Reaction;

    move-result-object p1

    goto :goto_0

    .line 312
    :cond_0
    instance-of p1, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz p1, :cond_1

    new-instance p1, Lcom/squareup/workflow/legacy/EnterState;

    iget-object v0, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$parseIdvStatus$1;->this$0:Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$parseIdvStatus$1;->$idvSettings:Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;

    invoke-static {v0, v1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;->access$onIdvStatusCheckFailed(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;)Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    check-cast p1, Lcom/squareup/workflow/legacy/Reaction;

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 78
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$parseIdvStatus$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/workflow/legacy/Reaction;

    move-result-object p1

    return-object p1
.end method
