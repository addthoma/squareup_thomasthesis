.class final Lcom/squareup/balance/squarecard/auth/AuthSquareCardMissingFieldDialogFactory$create$1;
.super Ljava/lang/Object;
.source "AuthSquareCardMissingFieldDialogFactory.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/auth/AuthSquareCardMissingFieldDialogFactory;->create(Landroid/content/Context;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAuthSquareCardMissingFieldDialogFactory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AuthSquareCardMissingFieldDialogFactory.kt\ncom/squareup/balance/squarecard/auth/AuthSquareCardMissingFieldDialogFactory$create$1\n*L\n1#1,45:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u0016\u0010\u0003\u001a\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u0007H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "Landroid/app/AlertDialog;",
        "kotlin.jvm.PlatformType",
        "screen",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardMissingFieldDialog$ScreenData;",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardMissingFieldDialog$Event;",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardMissingFieldDialogScreen;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardMissingFieldDialogFactory$create$1;->$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/workflow/legacy/Screen;)Landroid/app/AlertDialog;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardMissingFieldDialog$ScreenData;",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardMissingFieldDialog$Event;",
            ">;)",
            "Landroid/app/AlertDialog;"
        }
    .end annotation

    const-string v0, "screen"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    iget-object v0, p1, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 25
    iget-object p1, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast p1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardMissingFieldDialog$ScreenData;

    .line 26
    new-instance v1, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    iget-object v2, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardMissingFieldDialogFactory$create$1;->$context:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 27
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardMissingFieldDialog$ScreenData;->getMissingField()Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;->getTitle()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v1

    .line 28
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardMissingFieldDialog$ScreenData;->getMissingField()Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;->getMessage()I

    move-result p1

    invoke-virtual {v1, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 29
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setVerticalButtonOrientation()Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 31
    sget v1, Lcom/squareup/noho/R$drawable;->noho_selector_primary_button_background:I

    .line 30
    invoke-virtual {p1, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButtonBackground(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 34
    sget v1, Lcom/squareup/noho/R$color;->noho_color_selector_primary_button_text:I

    .line 33
    invoke-virtual {p1, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButtonTextColor(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 36
    sget v1, Lcom/squareup/common/strings/R$string;->okay:I

    new-instance v2, Lcom/squareup/balance/squarecard/auth/AuthSquareCardMissingFieldDialogFactory$create$1$$special$$inlined$with$lambda$1;

    invoke-direct {v2, p0, v0}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardMissingFieldDialogFactory$create$1$$special$$inlined$with$lambda$1;-><init>(Lcom/squareup/balance/squarecard/auth/AuthSquareCardMissingFieldDialogFactory$create$1;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v2, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {p1, v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 39
    new-instance v1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardMissingFieldDialogFactory$create$1$$special$$inlined$with$lambda$2;

    invoke-direct {v1, p0, v0}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardMissingFieldDialogFactory$create$1$$special$$inlined$with$lambda$2;-><init>(Lcom/squareup/balance/squarecard/auth/AuthSquareCardMissingFieldDialogFactory$create$1;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v1, Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {p1, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 40
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 15
    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardMissingFieldDialogFactory$create$1;->apply(Lcom/squareup/workflow/legacy/Screen;)Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method
