.class final Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;
.super Ljava/lang/Object;
.source "RealSquareCardOrderedWorkflowStarter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ScreenInput"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000^\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u0013\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005R\u0017\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0017\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\tR\u0017\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\tR\u0017\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\tR\u0017\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00140\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\tR\u0017\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00170\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\tR\u0017\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u001a0\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\tR\u0017\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u00020\u001d0\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001e\u0010\tR\u0017\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00020 0\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008!\u0010\t\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;",
        "",
        "reactor",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedEvent;",
        "(Lcom/squareup/workflow/legacy/WorkflowInput;)V",
        "forActivationComplete",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationComplete$DialogDismissed;",
        "getForActivationComplete",
        "()Lcom/squareup/workflow/legacy/WorkflowInput;",
        "forActivationCreatePin",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCreatePin$Event;",
        "getForActivationCreatePin",
        "forAddressConfirm",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddress$Event;",
        "getForAddressConfirm",
        "forCardConfirmation",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$Event;",
        "getForCardConfirmation",
        "forConfirmation",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmation$Event;",
        "getForConfirmation",
        "forDepositsInfo",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedDepositsInfo$Event;",
        "getForDepositsInfo",
        "forMissingInfoDialog",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWarning$Event;",
        "getForMissingInfoDialog",
        "forProgress",
        "Lcom/squareup/balance/squarecard/common/SquareCardProgress$Event;",
        "getForProgress",
        "forSquareCardOrdered",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$Event;",
        "getForSquareCardOrdered",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final forActivationComplete:Lcom/squareup/workflow/legacy/WorkflowInput;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationComplete$DialogDismissed;",
            ">;"
        }
    .end annotation
.end field

.field private final forActivationCreatePin:Lcom/squareup/workflow/legacy/WorkflowInput;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCreatePin$Event;",
            ">;"
        }
    .end annotation
.end field

.field private final forAddressConfirm:Lcom/squareup/workflow/legacy/WorkflowInput;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddress$Event;",
            ">;"
        }
    .end annotation
.end field

.field private final forCardConfirmation:Lcom/squareup/workflow/legacy/WorkflowInput;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$Event;",
            ">;"
        }
    .end annotation
.end field

.field private final forConfirmation:Lcom/squareup/workflow/legacy/WorkflowInput;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmation$Event;",
            ">;"
        }
    .end annotation
.end field

.field private final forDepositsInfo:Lcom/squareup/workflow/legacy/WorkflowInput;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedDepositsInfo$Event;",
            ">;"
        }
    .end annotation
.end field

.field private final forMissingInfoDialog:Lcom/squareup/workflow/legacy/WorkflowInput;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWarning$Event;",
            ">;"
        }
    .end annotation
.end field

.field private final forProgress:Lcom/squareup/workflow/legacy/WorkflowInput;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/balance/squarecard/common/SquareCardProgress$Event;",
            ">;"
        }
    .end annotation
.end field

.field private final forSquareCardOrdered:Lcom/squareup/workflow/legacy/WorkflowInput;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$Event;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedEvent;",
            ">;)V"
        }
    .end annotation

    const-string v0, "reactor"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 128
    sget-object v0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput$forProgress$1;->INSTANCE:Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput$forProgress$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInputKt;->adaptEvents(Lcom/squareup/workflow/legacy/WorkflowInput;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;->forProgress:Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 137
    sget-object v0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput$forConfirmation$1;->INSTANCE:Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput$forConfirmation$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInputKt;->adaptEvents(Lcom/squareup/workflow/legacy/WorkflowInput;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;->forConfirmation:Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 145
    sget-object v0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput$forSquareCardOrdered$1;->INSTANCE:Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput$forSquareCardOrdered$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInputKt;->adaptEvents(Lcom/squareup/workflow/legacy/WorkflowInput;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;->forSquareCardOrdered:Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 154
    sget-object v0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput$forCardConfirmation$1;->INSTANCE:Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput$forCardConfirmation$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInputKt;->adaptEvents(Lcom/squareup/workflow/legacy/WorkflowInput;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;->forCardConfirmation:Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 163
    sget-object v0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput$forDepositsInfo$1;->INSTANCE:Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput$forDepositsInfo$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInputKt;->adaptEvents(Lcom/squareup/workflow/legacy/WorkflowInput;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;->forDepositsInfo:Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 172
    sget-object v0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput$forActivationCreatePin$1;->INSTANCE:Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput$forActivationCreatePin$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInputKt;->adaptEvents(Lcom/squareup/workflow/legacy/WorkflowInput;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;->forActivationCreatePin:Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 178
    sget-object v0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput$forActivationComplete$1;->INSTANCE:Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput$forActivationComplete$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInputKt;->adaptEvents(Lcom/squareup/workflow/legacy/WorkflowInput;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;->forActivationComplete:Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 181
    sget-object v0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput$forAddressConfirm$1;->INSTANCE:Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput$forAddressConfirm$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInputKt;->adaptEvents(Lcom/squareup/workflow/legacy/WorkflowInput;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;->forAddressConfirm:Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 189
    sget-object v0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput$forMissingInfoDialog$1;->INSTANCE:Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput$forMissingInfoDialog$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInputKt;->adaptEvents(Lcom/squareup/workflow/legacy/WorkflowInput;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;->forMissingInfoDialog:Lcom/squareup/workflow/legacy/WorkflowInput;

    return-void
.end method


# virtual methods
.method public final getForActivationComplete()Lcom/squareup/workflow/legacy/WorkflowInput;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationComplete$DialogDismissed;",
            ">;"
        }
    .end annotation

    .line 178
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;->forActivationComplete:Lcom/squareup/workflow/legacy/WorkflowInput;

    return-object v0
.end method

.method public final getForActivationCreatePin()Lcom/squareup/workflow/legacy/WorkflowInput;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCreatePin$Event;",
            ">;"
        }
    .end annotation

    .line 171
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;->forActivationCreatePin:Lcom/squareup/workflow/legacy/WorkflowInput;

    return-object v0
.end method

.method public final getForAddressConfirm()Lcom/squareup/workflow/legacy/WorkflowInput;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddress$Event;",
            ">;"
        }
    .end annotation

    .line 181
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;->forAddressConfirm:Lcom/squareup/workflow/legacy/WorkflowInput;

    return-object v0
.end method

.method public final getForCardConfirmation()Lcom/squareup/workflow/legacy/WorkflowInput;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$Event;",
            ">;"
        }
    .end annotation

    .line 153
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;->forCardConfirmation:Lcom/squareup/workflow/legacy/WorkflowInput;

    return-object v0
.end method

.method public final getForConfirmation()Lcom/squareup/workflow/legacy/WorkflowInput;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmation$Event;",
            ">;"
        }
    .end annotation

    .line 136
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;->forConfirmation:Lcom/squareup/workflow/legacy/WorkflowInput;

    return-object v0
.end method

.method public final getForDepositsInfo()Lcom/squareup/workflow/legacy/WorkflowInput;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedDepositsInfo$Event;",
            ">;"
        }
    .end annotation

    .line 163
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;->forDepositsInfo:Lcom/squareup/workflow/legacy/WorkflowInput;

    return-object v0
.end method

.method public final getForMissingInfoDialog()Lcom/squareup/workflow/legacy/WorkflowInput;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWarning$Event;",
            ">;"
        }
    .end annotation

    .line 189
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;->forMissingInfoDialog:Lcom/squareup/workflow/legacy/WorkflowInput;

    return-object v0
.end method

.method public final getForProgress()Lcom/squareup/workflow/legacy/WorkflowInput;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/balance/squarecard/common/SquareCardProgress$Event;",
            ">;"
        }
    .end annotation

    .line 128
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;->forProgress:Lcom/squareup/workflow/legacy/WorkflowInput;

    return-object v0
.end method

.method public final getForSquareCardOrdered()Lcom/squareup/workflow/legacy/WorkflowInput;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$Event;",
            ">;"
        }
    .end annotation

    .line 145
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;->forSquareCardOrdered:Lcom/squareup/workflow/legacy/WorkflowInput;

    return-object v0
.end method
