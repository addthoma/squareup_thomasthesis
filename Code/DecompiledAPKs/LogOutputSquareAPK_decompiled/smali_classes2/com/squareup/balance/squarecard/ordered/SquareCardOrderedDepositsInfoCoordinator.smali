.class public final Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedDepositsInfoCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "SquareCardOrderedDepositsInfoCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSquareCardOrderedDepositsInfoCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SquareCardOrderedDepositsInfoCoordinator.kt\ncom/squareup/balance/squarecard/ordered/SquareCardOrderedDepositsInfoCoordinator\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,83:1\n1103#2,7:84\n*E\n*S KotlinDebug\n*F\n+ 1 SquareCardOrderedDepositsInfoCoordinator.kt\ncom/squareup/balance/squarecard/ordered/SquareCardOrderedDepositsInfoCoordinator\n*L\n48#1,7:84\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001B#\u0012\u001c\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\u000f\u001a\u00020\u00052\u0006\u0010\u0010\u001a\u00020\u000cH\u0016J\u0010\u0010\u0011\u001a\u00020\u00052\u0006\u0010\u0010\u001a\u00020\u000cH\u0002J\u001e\u0010\u0012\u001a\u00020\u00052\u0006\u0010\u0013\u001a\u00020\u00142\u000c\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0016H\u0002J\u0016\u0010\u0017\u001a\u00020\u00052\u000c\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0016H\u0002J(\u0010\u0018\u001a\u00020\u00052\u0006\u0010\u0010\u001a\u00020\u000c2\u0016\u0010\u0019\u001a\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u0007H\u0002R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R$\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedDepositsInfoCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedDepositsInfo$Event;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedDepositsInfoScreen;",
        "(Lio/reactivex/Observable;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "continueButton",
        "Landroid/view/View;",
        "depositsInfo",
        "Lcom/squareup/widgets/MessageView;",
        "attach",
        "view",
        "bindViews",
        "formatDepositsInfo",
        "context",
        "Landroid/content/Context;",
        "workflow",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "handleBack",
        "update",
        "screen",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/noho/NohoActionBar;

.field private continueButton:Landroid/view/View;

.field private depositsInfo:Lcom/squareup/widgets/MessageView;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lkotlin/Unit;",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedDepositsInfo$Event;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lkotlin/Unit;",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedDepositsInfo$Event;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedDepositsInfoCoordinator;->screens:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$handleBack(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedDepositsInfoCoordinator;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 0

    .line 28
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedDepositsInfoCoordinator;->handleBack(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-void
.end method

.method public static final synthetic access$update(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedDepositsInfoCoordinator;Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 0

    .line 28
    invoke-direct {p0, p1, p2}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedDepositsInfoCoordinator;->update(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 78
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->square_card_ordered_deposits_info:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedDepositsInfoCoordinator;->depositsInfo:Lcom/squareup/widgets/MessageView;

    .line 79
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->square_card_ordered_deposits_info_continue:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedDepositsInfoCoordinator;->continueButton:Landroid/view/View;

    .line 80
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoActionBar;

    iput-object p1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedDepositsInfoCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    return-void
.end method

.method private final formatDepositsInfo(Landroid/content/Context;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedDepositsInfo$Event;",
            ">;)V"
        }
    .end annotation

    .line 74
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedDepositsInfoCoordinator;->depositsInfo:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_0

    const-string v1, "depositsInfo"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 68
    :cond_0
    new-instance v1, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-direct {v1, p1}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    .line 69
    sget p1, Lcom/squareup/balance/squarecard/impl/R$string;->card_ordered_deposits_info_message:I

    const-string v2, "learn_more"

    invoke-virtual {v1, p1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    .line 70
    sget v1, Lcom/squareup/balance/squarecard/impl/R$string;->card_ordered_deposits_info_link_url:I

    invoke-virtual {p1, v1}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    .line 71
    new-instance v1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedDepositsInfoCoordinator$formatDepositsInfo$1;

    invoke-direct {v1, p2}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedDepositsInfoCoordinator$formatDepositsInfo$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {p1, v1}, Lcom/squareup/ui/LinkSpan$Builder;->onClick(Ljava/lang/Runnable;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    .line 72
    sget p2, Lcom/squareup/balance/squarecard/impl/R$string;->card_ordered_deposits_info_link:I

    invoke-virtual {p1, p2}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    .line 73
    sget p2, Lcom/squareup/marin/R$color;->marin_black:I

    invoke-virtual {p1, p2}, Lcom/squareup/ui/LinkSpan$Builder;->linkColor(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    .line 74
    invoke-virtual {p1}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final handleBack(Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedDepositsInfo$Event;",
            ">;)V"
        }
    .end annotation

    .line 61
    sget-object v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedDepositsInfo$Event$GoBack;->INSTANCE:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedDepositsInfo$Event$GoBack;

    invoke-interface {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method private final update(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lkotlin/Unit;",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedDepositsInfo$Event;",
            ">;)V"
        }
    .end annotation

    .line 46
    new-instance v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedDepositsInfoCoordinator$update$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedDepositsInfoCoordinator$update$1;-><init>(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedDepositsInfoCoordinator;Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 48
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedDepositsInfoCoordinator;->continueButton:Landroid/view/View;

    if-nez v0, :cond_0

    const-string v1, "continueButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 84
    :cond_0
    new-instance v1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedDepositsInfoCoordinator$update$$inlined$onClickDebounced$1;

    invoke-direct {v1, p2}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedDepositsInfoCoordinator$update$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 52
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string/jumbo v0, "view.context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p2, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    invoke-direct {p0, p1, v0}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedDepositsInfoCoordinator;->formatDepositsInfo(Landroid/content/Context;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 57
    iget-object p1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedDepositsInfoCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez p1, :cond_1

    const-string v0, "actionBar"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 54
    :cond_1
    new-instance v0, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 55
    new-instance v1, Lcom/squareup/util/ViewString$ResourceString;

    sget v2, Lcom/squareup/balance/squarecard/impl/R$string;->card_ordered_deposits_info_action_bar:I

    invoke-direct {v1, v2}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v1, Lcom/squareup/resources/TextModel;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 56
    sget-object v1, Lcom/squareup/noho/UpIcon;->X:Lcom/squareup/noho/UpIcon;

    new-instance v2, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedDepositsInfoCoordinator$update$3;

    invoke-direct {v2, p0, p2}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedDepositsInfoCoordinator$update$3;-><init>(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedDepositsInfoCoordinator;Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object p2

    .line 57
    invoke-virtual {p2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedDepositsInfoCoordinator;->bindViews(Landroid/view/View;)V

    .line 38
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedDepositsInfoCoordinator;->screens:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedDepositsInfoCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedDepositsInfoCoordinator$attach$1;-><init>(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedDepositsInfoCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "screens.subscribe { update(view, it) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method
