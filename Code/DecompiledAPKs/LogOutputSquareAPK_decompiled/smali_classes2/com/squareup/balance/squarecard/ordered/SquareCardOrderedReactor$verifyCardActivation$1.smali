.class final Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$verifyCardActivation$1;
.super Ljava/lang/Object;
.source "SquareCardOrderedReactor.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->verifyCardActivation(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Ljava/lang/String;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;",
        "successOrFailure",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

.field final synthetic $token:Ljava/lang/String;

.field final synthetic this$0:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$verifyCardActivation$1;->this$0:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$verifyCardActivation$1;->$card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    iput-object p3, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$verifyCardActivation$1;->$token:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse;",
            ">;)",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;"
        }
    .end annotation

    const-string v0, "successOrFailure"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 523
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse;

    iget-object p1, p1, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse;->result:Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;

    if-eqz p1, :cond_0

    sget-object v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 524
    new-instance p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;

    iget-object v2, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$verifyCardActivation$1;->$card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    iget-object v3, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$verifyCardActivation$1;->$token:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x1c

    const/4 v8, 0x0

    move-object v1, p1

    invoke-direct/range {v1 .. v8}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 523
    check-cast p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;

    goto :goto_0

    .line 525
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Unexpected successful state"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 527
    :cond_1
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz v0, :cond_2

    new-instance v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingActivationCodeFailed;

    .line 528
    iget-object v1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$verifyCardActivation$1;->$card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    iget-object v2, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$verifyCardActivation$1;->this$0:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    invoke-static {v2, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->access$toFailureResult(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;

    move-result-object p1

    .line 527
    invoke-direct {v0, v1, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingActivationCodeFailed;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;

    :goto_0
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 93
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$verifyCardActivation$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;

    move-result-object p1

    return-object p1
.end method
