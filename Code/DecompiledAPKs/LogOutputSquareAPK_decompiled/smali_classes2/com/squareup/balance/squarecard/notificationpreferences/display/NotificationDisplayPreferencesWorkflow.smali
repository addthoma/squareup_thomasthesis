.class public final Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "NotificationDisplayPreferencesWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesWorkflow$Action;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/balance/squarecard/notificationpreferences/PreferencesDataResult$Success;",
        "Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState;",
        "Lkotlin/Unit;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNotificationDisplayPreferencesWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NotificationDisplayPreferencesWorkflow.kt\ncom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n+ 3 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 4 Worker.kt\ncom/squareup/workflow/Worker$Companion\n+ 5 Worker.kt\ncom/squareup/workflow/WorkerKt\n+ 6 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,211:1\n32#2,12:212\n85#3:224\n240#4:225\n276#5:226\n149#6,5:227\n*E\n*S KotlinDebug\n*F\n+ 1 NotificationDisplayPreferencesWorkflow.kt\ncom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesWorkflow\n*L\n71#1,12:212\n100#1:224\n100#1:225\n100#1:226\n160#1,5:227\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002D\u0012\u0008\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0008\u0012\u00060\u0005j\u0002`\u0006\u0012&\u0012$\u0012\u0004\u0012\u00020\u0008\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\tj\u0002`\n0\u0007j\u0008\u0012\u0004\u0012\u00020\u0008`\u000b0\u0001:\u0001&B\u001d\u0008\u0007\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u000c\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000f\u00a2\u0006\u0002\u0010\u0011Jp\u0010\u0012\u001a$\u0012\u0004\u0012\u00020\u0008\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\tj\u0002`\n0\u0007j\u0008\u0012\u0004\u0012\u00020\u0008`\u000b2\u000c\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u00142\u0006\u0010\u0016\u001a\u00020\u00042\n\u0010\u0017\u001a\u00060\u0002j\u0002`\u00032\u0012\u0010\u0018\u001a\u000e\u0012\u0004\u0012\u00020\u001a\u0012\u0004\u0012\u00020\u00050\u00192\u000e\u0008\u0002\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u001cH\u0002J\u001e\u0010\u001d\u001a\u00020\u00042\n\u0010\u0017\u001a\u00060\u0002j\u0002`\u00032\u0008\u0010\u001e\u001a\u0004\u0018\u00010\u001fH\u0016JR\u0010 \u001a$\u0012\u0004\u0012\u00020\u0008\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\tj\u0002`\n0\u0007j\u0008\u0012\u0004\u0012\u00020\u0008`\u000b2\n\u0010\u0017\u001a\u00060\u0002j\u0002`\u00032\u0006\u0010\u0016\u001a\u00020\u00042\u0012\u0010!\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\"H\u0016J\u0010\u0010#\u001a\u00020\u001f2\u0006\u0010\u0016\u001a\u00020\u0004H\u0016J\u000c\u0010$\u001a\u00020%*\u00020\u001aH\u0002R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\'"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/balance/squarecard/notificationpreferences/PreferencesDataResult$Success;",
        "Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesProps;",
        "Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState;",
        "",
        "Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesOutput;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "dataStore",
        "Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesRemoteDataStore;",
        "updatingWorkflow",
        "Ljavax/inject/Provider;",
        "Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingWorkflow;",
        "(Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesRemoteDataStore;Ljavax/inject/Provider;)V",
        "displayingPreferencesScreen",
        "sink",
        "Lcom/squareup/workflow/Sink;",
        "Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesWorkflow$Action;",
        "state",
        "props",
        "onBack",
        "Lkotlin/Function1;",
        "Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;",
        "onErrorShown",
        "Lkotlin/Function0;",
        "initialState",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "toReceiverType",
        "Lcom/squareup/balance/squarecard/notificationpreferences/NotificationReceiverType;",
        "Action",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final dataStore:Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesRemoteDataStore;

.field private final updatingWorkflow:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingWorkflow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesRemoteDataStore;Ljavax/inject/Provider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesRemoteDataStore;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingWorkflow;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "dataStore"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "updatingWorkflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesWorkflow;->dataStore:Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesRemoteDataStore;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesWorkflow;->updatingWorkflow:Ljavax/inject/Provider;

    return-void
.end method

.method private final displayingPreferencesScreen(Lcom/squareup/workflow/Sink;Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState;Lcom/squareup/balance/squarecard/notificationpreferences/PreferencesDataResult$Success;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)Ljava/util/Map;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Sink<",
            "-",
            "Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesWorkflow$Action;",
            ">;",
            "Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState;",
            "Lcom/squareup/balance/squarecard/notificationpreferences/PreferencesDataResult$Success;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 144
    instance-of v0, p2, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState$UpdatingPreferences;

    if-eqz v0, :cond_0

    .line 145
    move-object v0, p2

    check-cast v0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState$UpdatingPreferences;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState$UpdatingPreferences;->getDesiredPreferences()Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;

    move-result-object v0

    goto :goto_0

    .line 147
    :cond_0
    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState;->getPreferences()Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;

    move-result-object v0

    :goto_0
    move-object v3, v0

    .line 150
    new-instance v0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesScreen;

    .line 152
    new-instance v1, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesWorkflow$displayingPreferencesScreen$2;

    invoke-direct {v1, p1, p2}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesWorkflow$displayingPreferencesScreen$2;-><init>(Lcom/squareup/workflow/Sink;Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState;)V

    move-object v7, v1

    check-cast v7, Lkotlin/jvm/functions/Function1;

    .line 156
    invoke-direct {p0, v3}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesWorkflow;->toReceiverType(Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;)Lcom/squareup/balance/squarecard/notificationpreferences/NotificationReceiverType;

    move-result-object v4

    .line 157
    invoke-virtual {p3}, Lcom/squareup/balance/squarecard/notificationpreferences/PreferencesDataResult$Success;->getEmail()Ljava/lang/String;

    move-result-object v2

    .line 159
    instance-of p1, p2, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState$DisplayingPreferences;

    if-nez p1, :cond_1

    const/4 p2, 0x0

    :cond_1
    check-cast p2, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState$DisplayingPreferences;

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState$DisplayingPreferences;->getShowError()Z

    move-result p1

    move v5, p1

    goto :goto_1

    :cond_2
    const/4 p1, 0x0

    const/4 v5, 0x0

    :goto_1
    move-object v1, v0

    move-object v6, p4

    move-object v8, p5

    .line 150
    invoke-direct/range {v1 .. v8}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesScreen;-><init>(Ljava/lang/String;Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;Lcom/squareup/balance/squarecard/notificationpreferences/NotificationReceiverType;ZLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 228
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 229
    const-class p2, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesScreen;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    const-string p3, ""

    invoke-static {p2, p3}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 230
    sget-object p3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {p3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p3

    .line 228
    invoke-direct {p1, p2, v0, p3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 161
    sget-object p2, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method static synthetic displayingPreferencesScreen$default(Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesWorkflow;Lcom/squareup/workflow/Sink;Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState;Lcom/squareup/balance/squarecard/notificationpreferences/PreferencesDataResult$Success;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Ljava/util/Map;
    .locals 6

    and-int/lit8 p6, p6, 0x10

    if-eqz p6, :cond_0

    .line 142
    sget-object p5, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesWorkflow$displayingPreferencesScreen$1;->INSTANCE:Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesWorkflow$displayingPreferencesScreen$1;

    check-cast p5, Lkotlin/jvm/functions/Function0;

    :cond_0
    move-object v5, p5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesWorkflow;->displayingPreferencesScreen(Lcom/squareup/workflow/Sink;Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState;Lcom/squareup/balance/squarecard/notificationpreferences/PreferencesDataResult$Success;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)Ljava/util/Map;

    move-result-object p0

    return-object p0
.end method

.method private final toReceiverType(Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;)Lcom/squareup/balance/squarecard/notificationpreferences/NotificationReceiverType;
    .locals 0

    .line 209
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;->getCardDeclines()Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreference;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreference;->isEnabled()Z

    move-result p1

    if-eqz p1, :cond_0

    sget-object p1, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationReceiverType;->EMAIL:Lcom/squareup/balance/squarecard/notificationpreferences/NotificationReceiverType;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationReceiverType;->NONE:Lcom/squareup/balance/squarecard/notificationpreferences/NotificationReceiverType;

    :goto_0
    return-object p1
.end method


# virtual methods
.method public initialState(Lcom/squareup/balance/squarecard/notificationpreferences/PreferencesDataResult$Success;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState;
    .locals 4

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x0

    if-eqz p2, :cond_4

    .line 212
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p2}, Lokio/ByteString;->size()I

    move-result v2

    if-lez v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_1

    goto :goto_1

    :cond_1
    move-object p2, v0

    :goto_1
    if-eqz p2, :cond_3

    .line 217
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    const-string v3, "Parcel.obtain()"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 218
    invoke-virtual {p2}, Lokio/ByteString;->toByteArray()[B

    move-result-object p2

    .line 219
    array-length v3, p2

    invoke-virtual {v2, p2, v1, v3}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 220
    invoke-virtual {v2, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 221
    const-class p2, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p2

    invoke-virtual {v2, p2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p2

    if-nez p2, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string v3, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {p2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 222
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    goto :goto_2

    :cond_3
    move-object p2, v0

    .line 223
    :goto_2
    check-cast p2, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState;

    if-eqz p2, :cond_4

    goto :goto_3

    .line 71
    :cond_4
    new-instance p2, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState$DisplayingPreferences;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/notificationpreferences/PreferencesDataResult$Success;->getPreferences()Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;

    move-result-object p1

    const/4 v2, 0x2

    invoke-direct {p2, p1, v1, v2, v0}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState$DisplayingPreferences;-><init>(Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast p2, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState;

    :goto_3
    return-object p2
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 58
    check-cast p1, Lcom/squareup/balance/squarecard/notificationpreferences/PreferencesDataResult$Success;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesWorkflow;->initialState(Lcom/squareup/balance/squarecard/notificationpreferences/PreferencesDataResult$Success;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 58
    check-cast p1, Lcom/squareup/balance/squarecard/notificationpreferences/PreferencesDataResult$Success;

    check-cast p2, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesWorkflow;->render(Lcom/squareup/balance/squarecard/notificationpreferences/PreferencesDataResult$Success;Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/balance/squarecard/notificationpreferences/PreferencesDataResult$Success;Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/notificationpreferences/PreferencesDataResult$Success;",
            "Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState;",
            "-",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    invoke-interface {p3}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object v2

    .line 81
    instance-of v0, p2, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState$DisplayingPreferences;

    if-eqz v0, :cond_0

    .line 85
    new-instance p3, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesWorkflow$render$1;

    invoke-direct {p3, p2, v2}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesWorkflow$render$1;-><init>(Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState;Lcom/squareup/workflow/Sink;)V

    move-object v5, p3

    check-cast v5, Lkotlin/jvm/functions/Function1;

    .line 97
    new-instance p3, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesWorkflow$render$2;

    invoke-direct {p3, v2, p2}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesWorkflow$render$2;-><init>(Lcom/squareup/workflow/Sink;Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState;)V

    move-object v6, p3

    check-cast v6, Lkotlin/jvm/functions/Function0;

    move-object v1, p0

    move-object v3, p2

    move-object v4, p1

    .line 81
    invoke-direct/range {v1 .. v6}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesWorkflow;->displayingPreferencesScreen(Lcom/squareup/workflow/Sink;Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState;Lcom/squareup/balance/squarecard/notificationpreferences/PreferencesDataResult$Success;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_0

    .line 99
    :cond_0
    instance-of v0, p2, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState$UpdatingPreferences;

    if-eqz v0, :cond_1

    .line 100
    iget-object v0, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesWorkflow;->dataStore:Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesRemoteDataStore;

    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState;->getPreferences()Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesRemoteDataStore;->updatePreferences(Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;)Lio/reactivex/Single;

    move-result-object v0

    .line 224
    sget-object v1, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v1, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesWorkflow$render$$inlined$asWorker$1;

    const/4 v3, 0x0

    invoke-direct {v1, v0, v3}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesWorkflow$render$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    .line 225
    invoke-static {v1}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v0

    .line 226
    const-class v1, Lcom/squareup/balance/squarecard/notificationpreferences/PreferencesDataResult;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v1

    new-instance v3, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v3, v1, v0}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v5, v3

    check-cast v5, Lcom/squareup/workflow/Worker;

    const/4 v6, 0x0

    .line 100
    new-instance v0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesWorkflow$render$3;

    invoke-direct {v0, p2}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesWorkflow$render$3;-><init>(Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState;)V

    move-object v7, v0

    check-cast v7, Lkotlin/jvm/functions/Function1;

    const/4 v8, 0x2

    const/4 v9, 0x0

    move-object v4, p3

    invoke-static/range {v4 .. v9}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 111
    new-instance p3, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesWorkflow$render$4;

    invoke-direct {p3, v2, p2}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesWorkflow$render$4;-><init>(Lcom/squareup/workflow/Sink;Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState;)V

    move-object v5, p3

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/16 v7, 0x10

    const/4 v8, 0x0

    move-object v1, p0

    move-object v3, p2

    move-object v4, p1

    .line 107
    invoke-static/range {v1 .. v8}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesWorkflow;->displayingPreferencesScreen$default(Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesWorkflow;Lcom/squareup/workflow/Sink;Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState;Lcom/squareup/balance/squarecard/notificationpreferences/PreferencesDataResult$Success;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Ljava/util/Map;

    move-result-object p1

    goto :goto_0

    .line 121
    :cond_1
    instance-of p1, p2, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState$UpdatingPreferencesOnExit;

    if-eqz p1, :cond_2

    .line 122
    iget-object p1, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesWorkflow;->updatingWorkflow:Ljavax/inject/Provider;

    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    const-string/jumbo v0, "updatingWorkflow.get()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v2, p1

    check-cast v2, Lcom/squareup/workflow/Workflow;

    .line 123
    move-object p1, p2

    check-cast p1, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState$UpdatingPreferencesOnExit;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState$UpdatingPreferencesOnExit;->getDesiredPreferences()Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;

    move-result-object v3

    const/4 v4, 0x0

    .line 124
    new-instance p1, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesWorkflow$render$5;

    invoke-direct {p1, p2}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesWorkflow$render$5;-><init>(Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState;)V

    move-object v5, p1

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v1, p3

    .line 121
    invoke-static/range {v1 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    :goto_0
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 135
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 58
    check-cast p1, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesWorkflow;->snapshotState(Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
