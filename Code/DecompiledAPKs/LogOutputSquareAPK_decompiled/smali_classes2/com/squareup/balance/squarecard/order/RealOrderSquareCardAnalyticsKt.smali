.class public final Lcom/squareup/balance/squarecard/order/RealOrderSquareCardAnalyticsKt;
.super Ljava/lang/Object;
.source "RealOrderSquareCardAnalytics.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u001b\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0005\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0006\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0007\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0008\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\t\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\n\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u000b\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u000c\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\r\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u000e\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u000f\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0010\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0011\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0012\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0013\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0014\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0015\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0016\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0017\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0018\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0019\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u001a\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u001b\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001c"
    }
    d2 = {
        "BUSINESS_INFO_SCREEN",
        "",
        "CANCEL_FROM_ORDER_SPLASH",
        "CARD_CUSTOMIZATION_SCREEN",
        "LOADING_BUSINESS_INFO_ERROR_SCREEN",
        "NO_OWNER_NAME_BUSINESS_INFO_ERROR_SCREEN",
        "ORDER_SQUARE_CARD",
        "ORDER_SQUARE_CARD_SCREEN",
        "ORDER_SQUARE_CARD_TERMS_OF_SERVICE",
        "PERSONALIZE_CARD",
        "SHIPPING_DETAILS_CONTINUE",
        "SHIPPING_DETAILS_CORRECTED_ADDRESS_ALERT_SCREEN",
        "SHIPPING_DETAILS_CORRECTED_ADDRESS_ORIGINAL_SELECTED",
        "SHIPPING_DETAILS_CORRECTED_ADDRESS_RECOMMENDED_SELECTED",
        "SHIPPING_DETAILS_CORRECTED_ADDRESS_SCREEN",
        "SHIPPING_DETAILS_MISSING_INFO_ERROR_SCREEN",
        "SHIPPING_DETAILS_SCREEN",
        "SHIPPING_DETAILS_SERVER_ERROR_SCREEN",
        "SHIPPING_DETAILS_UNVERIFIED_ADDRESS_BACK",
        "SHIPPING_DETAILS_UNVERIFIED_ADDRESS_CONFIRMED",
        "SHIPPING_DETAILS_UNVERIFIED_ADDRESS_ERROR_SCREEN",
        "SHIPPING_DETAILS_UNVERIFIED_ADDRESS_IMPRESSION",
        "SHIPPING_ORDER_CONFIRMED_SCREEN",
        "SHIPPING_ORDER_CONFIRMED_SIG_ONLY",
        "SHIPPING_ORDER_CONFIRMED_STAMPS_AND_SIG",
        "SHIPPING_ORDER_CONFIRMED_STAMPS_ONLY",
        "SHIPPING_ORDER_ERROR_SCREEN",
        "SIGNATURE_ERROR_SCREEN",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final BUSINESS_INFO_SCREEN:Ljava/lang/String; = "Create Card: Business Info"

.field private static final CANCEL_FROM_ORDER_SPLASH:Ljava/lang/String; = "Square Card: Create Card Cancel"

.field private static final CARD_CUSTOMIZATION_SCREEN:Ljava/lang/String; = "Create Card: Card Customization"

.field private static final LOADING_BUSINESS_INFO_ERROR_SCREEN:Ljava/lang/String; = "Create Card: Business Info Error"

.field private static final NO_OWNER_NAME_BUSINESS_INFO_ERROR_SCREEN:Ljava/lang/String; = "Create Card: Business Info No Owner Name Error"

.field private static final ORDER_SQUARE_CARD:Ljava/lang/String; = "Square Card: Create My Square Card"

.field private static final ORDER_SQUARE_CARD_SCREEN:Ljava/lang/String; = "Square Card: No Card Page"

.field private static final ORDER_SQUARE_CARD_TERMS_OF_SERVICE:Ljava/lang/String; = "Square Card: No Card Page Terms Of Service"

.field private static final PERSONALIZE_CARD:Ljava/lang/String; = "Create Card: Personalize Card"

.field private static final SHIPPING_DETAILS_CONTINUE:Ljava/lang/String; = "Create Card: Shipping Details Continue"

.field private static final SHIPPING_DETAILS_CORRECTED_ADDRESS_ALERT_SCREEN:Ljava/lang/String; = "Create Card: Shipping Details Corrected Address Alert"

.field private static final SHIPPING_DETAILS_CORRECTED_ADDRESS_ORIGINAL_SELECTED:Ljava/lang/String; = "Create Card: Shipping Details Continue With Old Address"

.field private static final SHIPPING_DETAILS_CORRECTED_ADDRESS_RECOMMENDED_SELECTED:Ljava/lang/String; = "Create Card: Shipping Details Continue With Recommended Address"

.field private static final SHIPPING_DETAILS_CORRECTED_ADDRESS_SCREEN:Ljava/lang/String; = "Create Card: Shipping Details Address Modified"

.field private static final SHIPPING_DETAILS_MISSING_INFO_ERROR_SCREEN:Ljava/lang/String; = "Create Card: Shipping Details Missing Info"

.field private static final SHIPPING_DETAILS_SCREEN:Ljava/lang/String; = "Create Card: Shipping Details"

.field private static final SHIPPING_DETAILS_SERVER_ERROR_SCREEN:Ljava/lang/String; = "Create Card: Shipping Details Server Error"

.field private static final SHIPPING_DETAILS_UNVERIFIED_ADDRESS_BACK:Ljava/lang/String; = "Create Card: Shipping Details Unrecognized Address Reenter"

.field private static final SHIPPING_DETAILS_UNVERIFIED_ADDRESS_CONFIRMED:Ljava/lang/String; = "Create Card: Shipping Details Unrecognized Address Continue"

.field private static final SHIPPING_DETAILS_UNVERIFIED_ADDRESS_ERROR_SCREEN:Ljava/lang/String; = "Create Card: Shipping Details Unverified Address"

.field private static final SHIPPING_DETAILS_UNVERIFIED_ADDRESS_IMPRESSION:Ljava/lang/String; = "Create Card: Shipping Details Unrecognized Address"

.field private static final SHIPPING_ORDER_CONFIRMED_SCREEN:Ljava/lang/String; = "Create Card: Order Confirmed"

.field private static final SHIPPING_ORDER_CONFIRMED_SIG_ONLY:Ljava/lang/String; = "Create Card: Order Confirmed Signature Only"

.field private static final SHIPPING_ORDER_CONFIRMED_STAMPS_AND_SIG:Ljava/lang/String; = "Create Card: Order Confirmed Stamps And Signature"

.field private static final SHIPPING_ORDER_CONFIRMED_STAMPS_ONLY:Ljava/lang/String; = "Create Card: Order Confirmed Stamps Only"

.field private static final SHIPPING_ORDER_ERROR_SCREEN:Ljava/lang/String; = "Create Card: Order Confirmation Server Error"

.field private static final SIGNATURE_ERROR_SCREEN:Ljava/lang/String; = "Create Card: Signature Error"
