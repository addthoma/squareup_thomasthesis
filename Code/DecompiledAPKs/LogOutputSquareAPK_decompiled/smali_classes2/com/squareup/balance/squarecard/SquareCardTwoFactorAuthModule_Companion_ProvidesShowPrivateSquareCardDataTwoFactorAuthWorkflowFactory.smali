.class public final Lcom/squareup/balance/squarecard/SquareCardTwoFactorAuthModule_Companion_ProvidesShowPrivateSquareCardDataTwoFactorAuthWorkflowFactory;
.super Ljava/lang/Object;
.source "SquareCardTwoFactorAuthModule_Companion_ProvidesShowPrivateSquareCardDataTwoFactorAuthWorkflowFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final dataStoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/balance/squarecard/SquareCardTwoFactorAuthModule_Companion_ProvidesShowPrivateSquareCardDataTwoFactorAuthWorkflowFactory;->dataStoreProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/balance/squarecard/SquareCardTwoFactorAuthModule_Companion_ProvidesShowPrivateSquareCardDataTwoFactorAuthWorkflowFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore;",
            ">;)",
            "Lcom/squareup/balance/squarecard/SquareCardTwoFactorAuthModule_Companion_ProvidesShowPrivateSquareCardDataTwoFactorAuthWorkflowFactory;"
        }
    .end annotation

    .line 33
    new-instance v0, Lcom/squareup/balance/squarecard/SquareCardTwoFactorAuthModule_Companion_ProvidesShowPrivateSquareCardDataTwoFactorAuthWorkflowFactory;

    invoke-direct {v0, p0}, Lcom/squareup/balance/squarecard/SquareCardTwoFactorAuthModule_Companion_ProvidesShowPrivateSquareCardDataTwoFactorAuthWorkflowFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static providesShowPrivateSquareCardDataTwoFactorAuthWorkflow(Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore;)Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthWorkflow;
    .locals 1

    .line 38
    sget-object v0, Lcom/squareup/balance/squarecard/SquareCardTwoFactorAuthModule;->Companion:Lcom/squareup/balance/squarecard/SquareCardTwoFactorAuthModule$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/balance/squarecard/SquareCardTwoFactorAuthModule$Companion;->providesShowPrivateSquareCardDataTwoFactorAuthWorkflow(Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore;)Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthWorkflow;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthWorkflow;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthWorkflow;
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/balance/squarecard/SquareCardTwoFactorAuthModule_Companion_ProvidesShowPrivateSquareCardDataTwoFactorAuthWorkflowFactory;->dataStoreProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore;

    invoke-static {v0}, Lcom/squareup/balance/squarecard/SquareCardTwoFactorAuthModule_Companion_ProvidesShowPrivateSquareCardDataTwoFactorAuthWorkflowFactory;->providesShowPrivateSquareCardDataTwoFactorAuthWorkflow(Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore;)Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/SquareCardTwoFactorAuthModule_Companion_ProvidesShowPrivateSquareCardDataTwoFactorAuthWorkflowFactory;->get()Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthWorkflow;

    move-result-object v0

    return-object v0
.end method
