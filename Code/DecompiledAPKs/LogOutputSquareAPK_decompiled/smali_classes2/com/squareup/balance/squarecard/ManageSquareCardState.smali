.class public abstract Lcom/squareup/balance/squarecard/ManageSquareCardState;
.super Ljava/lang/Object;
.source "ManageSquareCardState.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/ManageSquareCardState$LeafState;,
        Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow;,
        Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow;,
        Lcom/squareup/balance/squarecard/ManageSquareCardState$Factory;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0004\u0005\u0006\u0007\u0008B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H\u0016\u0082\u0001\u0003\t\n\u000b\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/ManageSquareCardState;",
        "",
        "()V",
        "toSnapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "Factory",
        "HostsV2Workflow",
        "HostsWorkflow",
        "LeafState",
        "Lcom/squareup/balance/squarecard/ManageSquareCardState$LeafState;",
        "Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow;",
        "Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/ManageSquareCardState;-><init>()V

    return-void
.end method


# virtual methods
.method public toSnapshot()Lcom/squareup/workflow/Snapshot;
    .locals 2

    .line 37
    sget-object v0, Lcom/squareup/workflow/Snapshot;->Companion:Lcom/squareup/workflow/Snapshot$Companion;

    new-instance v1, Lcom/squareup/balance/squarecard/ManageSquareCardState$toSnapshot$1;

    invoke-direct {v1, p0}, Lcom/squareup/balance/squarecard/ManageSquareCardState$toSnapshot$1;-><init>(Lcom/squareup/balance/squarecard/ManageSquareCardState;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/Snapshot$Companion;->write(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Snapshot;

    move-result-object v0

    return-object v0
.end method
