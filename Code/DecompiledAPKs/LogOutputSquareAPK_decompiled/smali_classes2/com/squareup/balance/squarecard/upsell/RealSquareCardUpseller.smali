.class public final Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller;
.super Ljava/lang/Object;
.source "RealSquareCardUpseller.kt"

# interfaces
.implements Lcom/squareup/balance/squarecard/upsell/SquareCardUpseller;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u0000 &2\u00020\u0001:\u0001&B?\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u0012\u0008\u0008\u0001\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0002\u0010\u000fJ\u000e\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u00020\u00170\u001dH\u0002J\u000e\u0010\u001e\u001a\u0008\u0012\u0004\u0012\u00020\u001b0\u001aH\u0002J\u0008\u0010\u001f\u001a\u00020\u001bH\u0002J\u000e\u0010 \u001a\u0008\u0012\u0004\u0012\u00020\u001b0\u001aH\u0002J\u0008\u0010!\u001a\u00020\u001bH\u0002J\u000e\u0010\"\u001a\u0008\u0012\u0004\u0012\u00020\u001b0\u001aH\u0016J\u000e\u0010#\u001a\u0008\u0012\u0004\u0012\u00020\u001b0\u001aH\u0016J\u0008\u0010$\u001a\u00020%H\u0016R\u001b\u0010\u0010\u001a\u00020\n8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u0013\u0010\u0014\u001a\u0004\u0008\u0011\u0010\u0012R\u001c\u0010\u0015\u001a\u0010\u0012\u000c\u0012\n \u0018*\u0004\u0018\u00010\u00170\u00170\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0019\u001a\u0010\u0012\u000c\u0012\n \u0018*\u0004\u0018\u00010\u001b0\u001b0\u001aX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\'"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller;",
        "Lcom/squareup/balance/squarecard/upsell/SquareCardUpseller;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "manageSquareCardSection",
        "Lcom/squareup/balance/squarecard/ManageSquareCardSection;",
        "squareCardRequester",
        "Lcom/squareup/balance/squarecard/SquareCardDataRequester;",
        "lazyBalanceAppletGateway",
        "Ldagger/Lazy;",
        "Lcom/squareup/ui/balance/BalanceAppletGateway;",
        "sharedPreferences",
        "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
        "squareCardUpsellerExperiment",
        "Lcom/squareup/experiments/SquareCardUpsellExperiment;",
        "(Lcom/squareup/settings/server/Features;Lcom/squareup/balance/squarecard/ManageSquareCardSection;Lcom/squareup/balance/squarecard/SquareCardDataRequester;Ldagger/Lazy;Lcom/f2prateek/rx/preferences2/RxSharedPreferences;Lcom/squareup/experiments/SquareCardUpsellExperiment;)V",
        "balanceAppletGateway",
        "getBalanceAppletGateway",
        "()Lcom/squareup/ui/balance/BalanceAppletGateway;",
        "balanceAppletGateway$delegate",
        "Ldagger/Lazy;",
        "cardStatus",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "Lcom/squareup/balance/squarecard/CardStatus;",
        "kotlin.jvm.PlatformType",
        "upsellShownPref",
        "Lio/reactivex/Observable;",
        "",
        "fetchCardStatus",
        "Lio/reactivex/Single;",
        "hasPermissionToSeeBalances",
        "isManageSquareCardSectionVisible",
        "onBalanceUpsellFeatureEnabled",
        "onInstantDepositsUpsellEnabled",
        "showUpsell",
        "showUpsellOnInstantDeposit",
        "upsellAcknowledged",
        "",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final Companion:Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller$Companion;

.field private static final SHARED_PREF_KEY_BALANCE_UPSELL:Ljava/lang/String; = "balance_upsell_pref_key"


# instance fields
.field private final balanceAppletGateway$delegate:Ldagger/Lazy;

.field private final cardStatus:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/balance/squarecard/CardStatus;",
            ">;"
        }
    .end annotation
.end field

.field private final features:Lcom/squareup/settings/server/Features;

.field private final manageSquareCardSection:Lcom/squareup/balance/squarecard/ManageSquareCardSection;

.field private final sharedPreferences:Lcom/f2prateek/rx/preferences2/RxSharedPreferences;

.field private final squareCardRequester:Lcom/squareup/balance/squarecard/SquareCardDataRequester;

.field private final squareCardUpsellerExperiment:Lcom/squareup/experiments/SquareCardUpsellExperiment;

.field private final upsellShownPref:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lkotlin/jvm/internal/PropertyReference1Impl;

    const-class v2, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-string v3, "balanceAppletGateway"

    const-string v4, "getBalanceAppletGateway()Lcom/squareup/ui/balance/BalanceAppletGateway;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->property1(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller;->Companion:Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/settings/server/Features;Lcom/squareup/balance/squarecard/ManageSquareCardSection;Lcom/squareup/balance/squarecard/SquareCardDataRequester;Ldagger/Lazy;Lcom/f2prateek/rx/preferences2/RxSharedPreferences;Lcom/squareup/experiments/SquareCardUpsellExperiment;)V
    .locals 1
    .param p5    # Lcom/f2prateek/rx/preferences2/RxSharedPreferences;
        .annotation runtime Lcom/squareup/settings/LoggedInSettings;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/balance/squarecard/ManageSquareCardSection;",
            "Lcom/squareup/balance/squarecard/SquareCardDataRequester;",
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/balance/BalanceAppletGateway;",
            ">;",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            "Lcom/squareup/experiments/SquareCardUpsellExperiment;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "features"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "manageSquareCardSection"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "squareCardRequester"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "lazyBalanceAppletGateway"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sharedPreferences"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "squareCardUpsellerExperiment"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller;->features:Lcom/squareup/settings/server/Features;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller;->manageSquareCardSection:Lcom/squareup/balance/squarecard/ManageSquareCardSection;

    iput-object p3, p0, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller;->squareCardRequester:Lcom/squareup/balance/squarecard/SquareCardDataRequester;

    iput-object p5, p0, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller;->sharedPreferences:Lcom/f2prateek/rx/preferences2/RxSharedPreferences;

    iput-object p6, p0, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller;->squareCardUpsellerExperiment:Lcom/squareup/experiments/SquareCardUpsellExperiment;

    .line 35
    iput-object p4, p0, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller;->balanceAppletGateway$delegate:Ldagger/Lazy;

    .line 36
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.create<CardStatus>()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller;->cardStatus:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 38
    iget-object p1, p0, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller;->sharedPreferences:Lcom/f2prateek/rx/preferences2/RxSharedPreferences;

    const/4 p2, 0x0

    .line 39
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    const-string p3, "balance_upsell_pref_key"

    invoke-virtual {p1, p3, p2}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getBoolean(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p1

    .line 40
    invoke-interface {p1}, Lcom/f2prateek/rx/preferences2/Preference;->asObservable()Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "sharedPreferences\n      \u2026se)\n      .asObservable()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller;->upsellShownPref:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$fetchCardStatus(Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller;)Lio/reactivex/Single;
    .locals 0

    .line 25
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller;->fetchCardStatus()Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getCardStatus$p(Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller;->cardStatus:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method private final fetchCardStatus()Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/balance/squarecard/CardStatus;",
            ">;"
        }
    .end annotation

    .line 83
    iget-object v0, p0, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller;->cardStatus:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->hasValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller;->squareCardRequester:Lcom/squareup/balance/squarecard/SquareCardDataRequester;

    invoke-interface {v0}, Lcom/squareup/balance/squarecard/SquareCardDataRequester;->fetchCardStatus()Lio/reactivex/Single;

    move-result-object v0

    .line 86
    new-instance v1, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller$fetchCardStatus$1;

    invoke-direct {v1, p0}, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller$fetchCardStatus$1;-><init>(Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->doOnSuccess(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "squareCardRequester.fetc\u2026            }\n          }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 92
    :cond_0
    iget-object v0, p0, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller;->cardStatus:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "cardStatus.firstOrError()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object v0
.end method

.method private final getBalanceAppletGateway()Lcom/squareup/ui/balance/BalanceAppletGateway;
    .locals 3

    iget-object v0, p0, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller;->balanceAppletGateway$delegate:Ldagger/Lazy;

    sget-object v1, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-static {v0, p0, v1}, Lcom/squareup/dagger/LazysKt;->getValue(Ldagger/Lazy;Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/balance/BalanceAppletGateway;

    return-object v0
.end method

.method private final hasPermissionToSeeBalances()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 112
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller;->getBalanceAppletGateway()Lcom/squareup/ui/balance/BalanceAppletGateway;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/ui/balance/BalanceAppletGateway;->hasPermissionToActivate()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method private final isManageSquareCardSectionVisible()Z
    .locals 1

    .line 98
    iget-object v0, p0, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller;->manageSquareCardSection:Lcom/squareup/balance/squarecard/ManageSquareCardSection;

    .line 99
    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/ManageSquareCardSection;->getAccessControl()Lcom/squareup/applet/SectionAccess;

    move-result-object v0

    .line 100
    invoke-virtual {v0}, Lcom/squareup/applet/SectionAccess;->determineVisibility()Z

    move-result v0

    return v0
.end method

.method private final onBalanceUpsellFeatureEnabled()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 104
    iget-object v0, p0, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_SQUARE_CARD_UPSELL_IN_DEPOSITS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->featureEnabled(Lcom/squareup/settings/server/Features$Feature;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "features.featureEnabled(\u2026_CARD_UPSELL_IN_DEPOSITS)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final onInstantDepositsUpsellEnabled()Z
    .locals 2

    .line 108
    iget-object v0, p0, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_SQUARE_CARD_UPSELL_IN_INSTANT_DEPOSITS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public showUpsell()Lio/reactivex/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 43
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller;->isManageSquareCardSectionVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 46
    iget-object v1, p0, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller;->upsellShownPref:Lio/reactivex/Observable;

    invoke-direct {p0}, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller;->hasPermissionToSeeBalances()Lio/reactivex/Observable;

    move-result-object v2

    invoke-direct {p0}, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller;->onBalanceUpsellFeatureEnabled()Lio/reactivex/Observable;

    move-result-object v3

    .line 45
    invoke-virtual {v0, v1, v2, v3}, Lcom/squareup/util/rx2/Observables;->combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 48
    new-instance v1, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller$showUpsell$1;

    invoke-direct {v1, p0}, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller$showUpsell$1;-><init>(Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->flatMapSingle(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 56
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observables\n          .c\u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 58
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observable.just(false)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object v0
.end method

.method public showUpsellOnInstantDeposit()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 63
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller;->isManageSquareCardSectionVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller;->onInstantDepositsUpsellEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller;->squareCardUpsellerExperiment:Lcom/squareup/experiments/SquareCardUpsellExperiment;

    .line 66
    invoke-virtual {v0}, Lcom/squareup/experiments/SquareCardUpsellExperiment;->squareCardUpsellBehavior()Lio/reactivex/Observable;

    move-result-object v0

    .line 67
    sget-object v1, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller$showUpsellOnInstantDeposit$1;->INSTANCE:Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller$showUpsellOnInstantDeposit$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "squareCardUpsellerExperi\u2026 .map { it.shouldShow() }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    :cond_0
    const/4 v0, 0x0

    .line 69
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observable.just(false)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public upsellAcknowledged()V
    .locals 2

    .line 77
    iget-object v0, p0, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller;->sharedPreferences:Lcom/f2prateek/rx/preferences2/RxSharedPreferences;

    const-string v1, "balance_upsell_pref_key"

    .line 78
    invoke-virtual {v0, v1}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getBoolean(Ljava/lang/String;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object v0

    const/4 v1, 0x1

    .line 79
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/f2prateek/rx/preferences2/Preference;->set(Ljava/lang/Object;)V

    return-void
.end method
