.class final synthetic Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureSignatureView$1;
.super Lkotlin/jvm/internal/FunctionReference;
.source "OrderSquareCardSignatureCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->configureSignatureView(Lio/reactivex/Observable;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/FunctionReference;",
        "Lkotlin/jvm/functions/Function2<",
        "Landroid/graphics/Canvas;",
        "Landroid/graphics/Paint;",
        "Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0015\u0010\u0002\u001a\u00110\u0003\u00a2\u0006\u000c\u0008\u0004\u0012\u0008\u0008\u0005\u0012\u0004\u0008\u0008(\u00062\u0015\u0010\u0007\u001a\u00110\u0008\u00a2\u0006\u000c\u0008\u0004\u0012\u0008\u0008\u0005\u0012\u0004\u0008\u0008(\t\u00a2\u0006\u0002\u0008\n"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;",
        "p1",
        "Landroid/graphics/Canvas;",
        "Lkotlin/ParameterName;",
        "name",
        "canvas",
        "p2",
        "Landroid/graphics/Paint;",
        "paint",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureSignatureView$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureSignatureView$1;

    invoke-direct {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureSignatureView$1;-><init>()V

    sput-object v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureSignatureView$1;->INSTANCE:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureSignatureView$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/FunctionReference;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    const-string v0, "<init>"

    return-object v0
.end method

.method public final getOwner()Lkotlin/reflect/KDeclarationContainer;
    .locals 1

    const-class v0, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    return-object v0
.end method

.method public final getSignature()Ljava/lang/String;
    .locals 1

    const-string v0, "<init>(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V"

    return-object v0
.end method

.method public final invoke(Landroid/graphics/Canvas;Landroid/graphics/Paint;)Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;
    .locals 1

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p2"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;

    .line 554
    invoke-direct {v0, p1, p2}, Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;-><init>(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 80
    check-cast p1, Landroid/graphics/Canvas;

    check-cast p2, Landroid/graphics/Paint;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureSignatureView$1;->invoke(Landroid/graphics/Canvas;Landroid/graphics/Paint;)Lcom/squareup/cardcustomizations/signature/DouglasPeuckerStrokeSimplifier;

    move-result-object p1

    return-object p1
.end method
