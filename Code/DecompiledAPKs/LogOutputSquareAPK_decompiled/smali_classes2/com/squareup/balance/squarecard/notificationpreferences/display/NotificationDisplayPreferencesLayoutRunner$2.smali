.class final Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner$2;
.super Lkotlin/jvm/internal/Lambda;
.source "NotificationDisplayPreferencesLayoutRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;-><init>(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lkotlin/Unit;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNotificationDisplayPreferencesLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NotificationDisplayPreferencesLayoutRunner.kt\ncom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner$2\n*L\n1#1,169:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0003*\u0004\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0004\u0008\u0004\u0010\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "kotlin.jvm.PlatformType",
        "invoke",
        "(Lkotlin/Unit;)V"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner$2;->this$0:Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 32
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner$2;->invoke(Lkotlin/Unit;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lkotlin/Unit;)V
    .locals 2

    .line 59
    iget-object p1, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner$2;->this$0:Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;

    invoke-static {p1}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;->access$getLatestRendering$p(Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;)Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesScreen;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 60
    iget-object v0, p0, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner$2;->this$0:Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;

    invoke-static {v0}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;->access$notificationPreferences(Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;)Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;

    move-result-object v0

    .line 61
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesScreen;->getPreferences()Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferences;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 62
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesScreen;->getOnUpdatePreferences()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method
