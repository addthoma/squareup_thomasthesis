.class public final Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;
.super Ljava/lang/Object;
.source "CardOrderingAuthSquareCardPersonalInfoRendering.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/V2Screen;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0012\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001BC\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\u0012\u0010\u0008\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b0\t\u0012\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\r\u00a2\u0006\u0002\u0010\u000eJ\t\u0010\u0019\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010\u001a\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003J\u000b\u0010\u001b\u001a\u0004\u0018\u00010\u0007H\u00c6\u0003J\u0015\u0010\u001c\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b0\tH\u00c6\u0003J\u000f\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\rH\u00c6\u0003JQ\u0010\u001e\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00072\u0014\u0008\u0002\u0010\u0008\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b0\t2\u000e\u0008\u0002\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\rH\u00c6\u0001J\u0013\u0010\u001f\u001a\u00020 2\u0008\u0010!\u001a\u0004\u0018\u00010\"H\u00d6\u0003J\t\u0010#\u001a\u00020$H\u00d6\u0001J\t\u0010%\u001a\u00020\u0003H\u00d6\u0001R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0017\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\r\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u001d\u0010\u0008\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b0\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0018\u00a8\u0006&"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        "ownerName",
        "",
        "address",
        "Lcom/squareup/address/Address;",
        "birthDate",
        "Lorg/threeten/bp/LocalDate;",
        "onSubmitPersonalInfo",
        "Lkotlin/Function1;",
        "Lcom/squareup/balance/squarecard/authv2/personalinfo/SubmitPersonalInfoData;",
        "",
        "onDeclinePersonalInfo",
        "Lkotlin/Function0;",
        "(Ljava/lang/String;Lcom/squareup/address/Address;Lorg/threeten/bp/LocalDate;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V",
        "getAddress",
        "()Lcom/squareup/address/Address;",
        "getBirthDate",
        "()Lorg/threeten/bp/LocalDate;",
        "getOnDeclinePersonalInfo",
        "()Lkotlin/jvm/functions/Function0;",
        "getOnSubmitPersonalInfo",
        "()Lkotlin/jvm/functions/Function1;",
        "getOwnerName",
        "()Ljava/lang/String;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final address:Lcom/squareup/address/Address;

.field private final birthDate:Lorg/threeten/bp/LocalDate;

.field private final onDeclinePersonalInfo:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onSubmitPersonalInfo:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/balance/squarecard/authv2/personalinfo/SubmitPersonalInfoData;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final ownerName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/squareup/address/Address;Lorg/threeten/bp/LocalDate;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/address/Address;",
            "Lorg/threeten/bp/LocalDate;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/balance/squarecard/authv2/personalinfo/SubmitPersonalInfoData;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "ownerName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onSubmitPersonalInfo"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onDeclinePersonalInfo"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;->ownerName:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;->address:Lcom/squareup/address/Address;

    iput-object p3, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;->birthDate:Lorg/threeten/bp/LocalDate;

    iput-object p4, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;->onSubmitPersonalInfo:Lkotlin/jvm/functions/Function1;

    iput-object p5, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;->onDeclinePersonalInfo:Lkotlin/jvm/functions/Function0;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;Ljava/lang/String;Lcom/squareup/address/Address;Lorg/threeten/bp/LocalDate;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-object p1, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;->ownerName:Ljava/lang/String;

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-object p2, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;->address:Lcom/squareup/address/Address;

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;->birthDate:Lorg/threeten/bp/LocalDate;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;->onSubmitPersonalInfo:Lkotlin/jvm/functions/Function1;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;->onDeclinePersonalInfo:Lkotlin/jvm/functions/Function0;

    :cond_4
    move-object v2, p5

    move-object p2, p0

    move-object p3, p1

    move-object p4, p7

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;->copy(Ljava/lang/String;Lcom/squareup/address/Address;Lorg/threeten/bp/LocalDate;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;->ownerName:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Lcom/squareup/address/Address;
    .locals 1

    iget-object v0, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;->address:Lcom/squareup/address/Address;

    return-object v0
.end method

.method public final component3()Lorg/threeten/bp/LocalDate;
    .locals 1

    iget-object v0, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;->birthDate:Lorg/threeten/bp/LocalDate;

    return-object v0
.end method

.method public final component4()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/balance/squarecard/authv2/personalinfo/SubmitPersonalInfoData;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;->onSubmitPersonalInfo:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final component5()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;->onDeclinePersonalInfo:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;Lcom/squareup/address/Address;Lorg/threeten/bp/LocalDate;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/address/Address;",
            "Lorg/threeten/bp/LocalDate;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/balance/squarecard/authv2/personalinfo/SubmitPersonalInfoData;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;"
        }
    .end annotation

    const-string v0, "ownerName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onSubmitPersonalInfo"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onDeclinePersonalInfo"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;-><init>(Ljava/lang/String;Lcom/squareup/address/Address;Lorg/threeten/bp/LocalDate;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;

    iget-object v0, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;->ownerName:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;->ownerName:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;->address:Lcom/squareup/address/Address;

    iget-object v1, p1, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;->address:Lcom/squareup/address/Address;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;->birthDate:Lorg/threeten/bp/LocalDate;

    iget-object v1, p1, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;->birthDate:Lorg/threeten/bp/LocalDate;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;->onSubmitPersonalInfo:Lkotlin/jvm/functions/Function1;

    iget-object v1, p1, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;->onSubmitPersonalInfo:Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;->onDeclinePersonalInfo:Lkotlin/jvm/functions/Function0;

    iget-object p1, p1, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;->onDeclinePersonalInfo:Lkotlin/jvm/functions/Function0;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAddress()Lcom/squareup/address/Address;
    .locals 1

    .line 10
    iget-object v0, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;->address:Lcom/squareup/address/Address;

    return-object v0
.end method

.method public final getBirthDate()Lorg/threeten/bp/LocalDate;
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;->birthDate:Lorg/threeten/bp/LocalDate;

    return-object v0
.end method

.method public final getOnDeclinePersonalInfo()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 13
    iget-object v0, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;->onDeclinePersonalInfo:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getOnSubmitPersonalInfo()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/balance/squarecard/authv2/personalinfo/SubmitPersonalInfoData;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 12
    iget-object v0, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;->onSubmitPersonalInfo:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getOwnerName()Ljava/lang/String;
    .locals 1

    .line 9
    iget-object v0, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;->ownerName:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;->ownerName:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;->address:Lcom/squareup/address/Address;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;->birthDate:Lorg/threeten/bp/LocalDate;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;->onSubmitPersonalInfo:Lkotlin/jvm/functions/Function1;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;->onDeclinePersonalInfo:Lkotlin/jvm/functions/Function0;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_4
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CardOrderingAuthSquareCardPersonalInfoRendering(ownerName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;->ownerName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", address="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;->address:Lcom/squareup/address/Address;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", birthDate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;->birthDate:Lorg/threeten/bp/LocalDate;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onSubmitPersonalInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;->onSubmitPersonalInfo:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onDeclinePersonalInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;->onDeclinePersonalInfo:Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
