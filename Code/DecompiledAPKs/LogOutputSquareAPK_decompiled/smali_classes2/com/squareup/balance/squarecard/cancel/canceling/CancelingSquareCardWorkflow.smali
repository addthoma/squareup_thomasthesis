.class public final Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardWorkflow;
.super Lcom/squareup/workflow/StatelessWorkflow;
.source "CancelingSquareCardWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatelessWorkflow<",
        "Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardProps;",
        "Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardOutput;",
        "Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCancelingSquareCardWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CancelingSquareCardWorkflow.kt\ncom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardWorkflow\n+ 2 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 3 Worker.kt\ncom/squareup/workflow/Worker$Companion\n+ 4 Worker.kt\ncom/squareup/workflow/WorkerKt\n*L\n1#1,94:1\n85#2:95\n240#3:96\n276#4:97\n*E\n*S KotlinDebug\n*F\n+ 1 CancelingSquareCardWorkflow.kt\ncom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardWorkflow\n*L\n82#1:95\n82#1:96\n82#1:97\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u0014\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u001e\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\u00030\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\rH\u0002J$\u0010\u000e\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\u00022\u0012\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00020\u0012\u0012\u0004\u0012\u00020\u00030\u0011H\u0016J\u000c\u0010\u0013\u001a\u00020\r*\u00020\u0014H\u0002R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardWorkflow;",
        "Lcom/squareup/workflow/StatelessWorkflow;",
        "Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardProps;",
        "Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardOutput;",
        "Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;",
        "bizbankService",
        "Lcom/squareup/balance/core/server/bizbank/BizbankService;",
        "(Lcom/squareup/balance/core/server/bizbank/BizbankService;)V",
        "cancelSquareCard",
        "Lcom/squareup/workflow/Worker;",
        "card",
        "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
        "deactivationReason",
        "Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;",
        "render",
        "props",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "",
        "toServiceReason",
        "Lcom/squareup/balance/squarecard/cancel/DeactivationReason;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;


# direct methods
.method public constructor <init>(Lcom/squareup/balance/core/server/bizbank/BizbankService;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "bizbankService"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    invoke-direct {p0}, Lcom/squareup/workflow/StatelessWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardWorkflow;->bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

    return-void
.end method

.method private final cancelSquareCard(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;)Lcom/squareup/workflow/Worker;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
            "Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;",
            ")",
            "Lcom/squareup/workflow/Worker<",
            "Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardOutput;",
            ">;"
        }
    .end annotation

    .line 69
    new-instance v0, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$Builder;-><init>()V

    .line 70
    iget-object p1, p1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->card_token:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$Builder;->card_token(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$Builder;

    move-result-object p1

    .line 71
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$Builder;->deactivation_reason(Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;)Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$Builder;

    move-result-object p1

    .line 72
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$Builder;->build()Lcom/squareup/protos/client/bizbank/DeactivateCardRequest;

    move-result-object p1

    .line 74
    iget-object p2, p0, Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardWorkflow;->bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

    const-string v0, "deactivateCardRequest"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, p1}, Lcom/squareup/balance/core/server/bizbank/BizbankService;->deactivateCard(Lcom/squareup/protos/client/bizbank/DeactivateCardRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 75
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 76
    sget-object p2, Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardWorkflow$cancelSquareCard$1;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardWorkflow$cancelSquareCard$1;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "bizbankService.deactivat\u2026led\n          }\n        }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    sget-object p2, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance p2, Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardWorkflow$cancelSquareCard$$inlined$asWorker$1;

    const/4 v0, 0x0

    invoke-direct {p2, p1, v0}, Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardWorkflow$cancelSquareCard$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast p2, Lkotlin/jvm/functions/Function1;

    .line 96
    invoke-static {p2}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 97
    const-class p2, Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardOutput;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object p2

    new-instance v0, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v0, p2, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v0, Lcom/squareup/workflow/Worker;

    return-object v0
.end method

.method private final toServiceReason(Lcom/squareup/balance/squarecard/cancel/DeactivationReason;)Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;
    .locals 1

    .line 87
    sget-object v0, Lcom/squareup/balance/squarecard/cancel/DeactivationReason$Lost;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/DeactivationReason$Lost;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;->LOST:Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;

    goto :goto_0

    .line 88
    :cond_0
    sget-object v0, Lcom/squareup/balance/squarecard/cancel/DeactivationReason$Stolen;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/DeactivationReason$Stolen;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object p1, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;->STOLEN:Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;

    goto :goto_0

    .line 89
    :cond_1
    sget-object v0, Lcom/squareup/balance/squarecard/cancel/DeactivationReason$NeverReceived;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/DeactivationReason$NeverReceived;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object p1, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;->CARD_NOT_RECEIVED:Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;

    goto :goto_0

    .line 90
    :cond_2
    sget-object v0, Lcom/squareup/balance/squarecard/cancel/DeactivationReason$Generic;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/DeactivationReason$Generic;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    sget-object p1, Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;->CANCEL:Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;

    :goto_0
    return-object p1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method


# virtual methods
.method public render(Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardProps;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;
    .locals 7

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardProps;->getCardData()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object v0

    .line 51
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardProps;->getDeactivationReason()Lcom/squareup/balance/squarecard/cancel/DeactivationReason;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardWorkflow;->toServiceReason(Lcom/squareup/balance/squarecard/cancel/DeactivationReason;)Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;

    move-result-object p1

    .line 49
    invoke-direct {p0, v0, p1}, Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardWorkflow;->cancelSquareCard(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Lcom/squareup/protos/client/bizbank/DeactivateCardRequest$DeactivationReason;)Lcom/squareup/workflow/Worker;

    move-result-object v2

    .line 53
    new-instance p1, Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardWorkflow$render$1;

    invoke-direct {p1, p0}, Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardWorkflow$render$1;-><init>(Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardWorkflow;)V

    move-object v4, p1

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v3, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x0

    move-object v1, p2

    .line 48
    invoke-static/range {v1 .. v6}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 55
    new-instance p1, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;

    .line 56
    new-instance v0, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Loading;

    sget v1, Lcom/squareup/balance/squarecard/impl/R$string;->canceling_card_spinner_message:I

    invoke-direct {v0, v1}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Loading;-><init>(I)V

    check-cast v0, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData;

    .line 57
    new-instance v1, Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardWorkflow$render$2;

    invoke-direct {v1, p0, p2}, Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardWorkflow$render$2;-><init>(Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardWorkflow;Lcom/squareup/workflow/RenderContext;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    .line 55
    invoke-direct {p1, v0, v1}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;-><init>(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData;Lkotlin/jvm/functions/Function1;)V

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 41
    check-cast p1, Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardWorkflow;->render(Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardProps;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;

    move-result-object p1

    return-object p1
.end method
