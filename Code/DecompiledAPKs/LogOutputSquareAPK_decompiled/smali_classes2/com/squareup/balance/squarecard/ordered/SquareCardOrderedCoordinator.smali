.class public final Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "SquareCardOrderedCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedCoordinator$ReportProblemLinkSpan;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSquareCardOrderedCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SquareCardOrderedCoordinator.kt\ncom/squareup/balance/squarecard/ordered/SquareCardOrderedCoordinator\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,116:1\n1103#2,7:117\n*E\n*S KotlinDebug\n*F\n+ 1 SquareCardOrderedCoordinator.kt\ncom/squareup/balance/squarecard/ordered/SquareCardOrderedCoordinator\n*L\n72#1,7:117\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u00002\u00020\u0001:\u0001\u001fB#\u0012\u001c\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015H\u0016J\u0010\u0010\u0016\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015H\u0002J \u0010\u0017\u001a\u00020\u00132\u0016\u0010\u0018\u001a\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u0007H\u0002J\u0010\u0010\u0019\u001a\u00020\u00132\u0006\u0010\u001a\u001a\u00020\u001bH\u0002J(\u0010\u001c\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00152\u0016\u0010\u0018\u001a\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u0007H\u0002J \u0010\u001d\u001a\u00020\u00132\u0016\u0010\u0018\u001a\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u0007H\u0002J(\u0010\u001e\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00152\u0016\u0010\u0018\u001a\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u0007H\u0002R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R$\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$Event;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedScreen;",
        "(Lio/reactivex/Observable;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "activateTrigger",
        "Landroid/widget/TextView;",
        "getHelpWithCard",
        "Lcom/squareup/widgets/MessageView;",
        "message",
        "squareCard",
        "Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "handleBack",
        "screen",
        "setNameAndLast4",
        "card",
        "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
        "update",
        "updateActionBar",
        "updateLayout",
        "ReportProblemLinkSpan",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/noho/NohoActionBar;

.field private activateTrigger:Landroid/widget/TextView;

.field private getHelpWithCard:Lcom/squareup/widgets/MessageView;

.field private message:Landroid/widget/TextView;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$Event;",
            ">;>;"
        }
    .end annotation
.end field

.field private squareCard:Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$Event;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedCoordinator;->screens:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$handleBack(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedCoordinator;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 0

    .line 31
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedCoordinator;->handleBack(Lcom/squareup/workflow/legacy/Screen;)V

    return-void
.end method

.method public static final synthetic access$update(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedCoordinator;Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 0

    .line 31
    invoke-direct {p0, p1, p2}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedCoordinator;->update(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 100
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->ordered_square_card_message:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedCoordinator;->message:Landroid/widget/TextView;

    .line 101
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->ordered_square_card_activate_trigger:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedCoordinator;->activateTrigger:Landroid/widget/TextView;

    .line 102
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->get_help_with_card:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedCoordinator;->getHelpWithCard:Lcom/squareup/widgets/MessageView;

    .line 103
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->square_card:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;

    iput-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedCoordinator;->squareCard:Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;

    .line 104
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoActionBar;

    iput-object p1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    return-void
.end method

.method private final handleBack(Lcom/squareup/workflow/legacy/Screen;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$Event;",
            ">;)V"
        }
    .end annotation

    .line 57
    iget-object v0, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->getAllowBack()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    iget-object p1, p1, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    sget-object v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$Event$GoBack;->INSTANCE:Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$Event$GoBack;

    invoke-interface {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private final setNameAndLast4(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V
    .locals 3

    .line 95
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedCoordinator;->squareCard:Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;

    const-string v1, "squareCard"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object v2, p1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->name_on_card:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->setName(Ljava/lang/String;)V

    .line 96
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedCoordinator;->squareCard:Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    iget-object p1, p1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->pan_last_four:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->setPanLast4(Ljava/lang/String;)V

    return-void
.end method

.method private final update(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$Event;",
            ">;)V"
        }
    .end annotation

    .line 50
    new-instance v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedCoordinator$update$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedCoordinator$update$1;-><init>(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedCoordinator;Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 52
    invoke-direct {p0, p2}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedCoordinator;->updateActionBar(Lcom/squareup/workflow/legacy/Screen;)V

    .line 53
    invoke-direct {p0, p1, p2}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedCoordinator;->updateLayout(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V

    return-void
.end method

.method private final updateActionBar(Lcom/squareup/workflow/legacy/Screen;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$Event;",
            ">;)V"
        }
    .end annotation

    .line 91
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 87
    :cond_0
    new-instance v1, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 88
    new-instance v2, Lcom/squareup/util/ViewString$ResourceString;

    sget v3, Lcom/squareup/balance/squarecard/impl/R$string;->square_card:I

    invoke-direct {v2, v3}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v2, Lcom/squareup/resources/TextModel;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 89
    sget-object v2, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    new-instance v3, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedCoordinator$updateActionBar$1;

    invoke-direct {v3, p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedCoordinator$updateActionBar$1;-><init>(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedCoordinator;Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 90
    iget-object p1, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->getAllowBack()Z

    move-result p1

    if-nez p1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->hideUpButton()Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 91
    :cond_1
    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method

.method private final updateLayout(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$Event;",
            ">;)V"
        }
    .end annotation

    .line 66
    iget-object v0, p2, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;

    .line 67
    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->getCard()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedCoordinator;->setNameAndLast4(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V

    .line 68
    iget-object v1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedCoordinator;->message:Landroid/widget/TextView;

    if-nez v1, :cond_0

    const-string v2, "message"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->getOrderedMessage()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 69
    iget-object v1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedCoordinator;->activateTrigger:Landroid/widget/TextView;

    const-string v2, "activateTrigger"

    if-nez v1, :cond_1

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->getActivateMessage()I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 70
    iget-object v1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedCoordinator;->activateTrigger:Landroid/widget/TextView;

    if-nez v1, :cond_2

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->getEnableActivation()Z

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 71
    iget-object v1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedCoordinator;->activateTrigger:Landroid/widget/TextView;

    if-nez v1, :cond_3

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v1}, Landroid/widget/TextView;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 72
    iget-object v1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedCoordinator;->activateTrigger:Landroid/widget/TextView;

    if-nez v1, :cond_4

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    check-cast v1, Landroid/view/View;

    .line 117
    new-instance v2, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedCoordinator$updateLayout$$inlined$onClickDebounced$1;

    invoke-direct {v2, p2}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedCoordinator$updateLayout$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v2, Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 76
    :cond_5
    iget-object v1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedCoordinator;->getHelpWithCard:Lcom/squareup/widgets/MessageView;

    const-string v2, "getHelpWithCard"

    if-nez v1, :cond_6

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    check-cast v1, Landroid/view/View;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->getShowGetHelpWithCard()Z

    move-result v3

    invoke-static {v1, v3}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 77
    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrdered$ScreenData;->getShowGetHelpWithCard()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 82
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedCoordinator;->getHelpWithCard:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_7

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 78
    :cond_7
    new-instance v1, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    .line 79
    sget v2, Lcom/squareup/balance/squarecard/impl/R$string;->card_help:I

    const-string v3, "get_help_with_card"

    invoke-virtual {v1, v2, v3}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    .line 80
    sget v2, Lcom/squareup/balance/squarecard/impl/R$string;->get_help_with_card:I

    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    .line 81
    new-instance v2, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedCoordinator$ReportProblemLinkSpan;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string/jumbo v3, "view.context"

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p2, p2, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    invoke-direct {v2, p1, p2}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedCoordinator$ReportProblemLinkSpan;-><init>(Landroid/content/Context;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v2, Lcom/squareup/ui/LinkSpan;

    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->clickableSpan(Lcom/squareup/ui/LinkSpan;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    .line 82
    invoke-virtual {p1}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    :cond_8
    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedCoordinator;->bindViews(Landroid/view/View;)V

    .line 42
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedCoordinator;->screens:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedCoordinator$attach$1;-><init>(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "screens.subscribe { update(view, it) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method
