.class public final Lcom/squareup/balance/squarecard/ui/SignatureOutlineView;
.super Landroid/widget/FrameLayout;
.source "SignatureOutlineView.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0015\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0008\u0018\u00002\u00020\u0001B#\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\r0\u0010J0\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00072\u0006\u0010\u0016\u001a\u00020\u00072\u0006\u0010\u0017\u001a\u00020\u00072\u0006\u0010\u0018\u001a\u00020\u0007H\u0014J\u0018\u0010\u0019\u001a\u00020\u00122\u0006\u0010\u001a\u001a\u00020\u00072\u0006\u0010\u001b\u001a\u00020\u0007H\u0014R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/ui/SignatureOutlineView;",
        "Landroid/widget/FrameLayout;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "defStyleAttr",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "location",
        "",
        "onLayoutChange",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "Landroid/graphics/Rect;",
        "rect",
        "layoutChange",
        "Lio/reactivex/Observable;",
        "onLayout",
        "",
        "changed",
        "",
        "left",
        "top",
        "right",
        "bottom",
        "onMeasure",
        "widthMeasureSpec",
        "heightMeasureSpec",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final location:[I

.field private final onLayoutChange:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Landroid/graphics/Rect;",
            ">;"
        }
    .end annotation
.end field

.field private final rect:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/balance/squarecard/ui/SignatureOutlineView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p1, 0x2

    new-array p1, p1, [I

    .line 23
    iput-object p1, p0, Lcom/squareup/balance/squarecard/ui/SignatureOutlineView;->location:[I

    .line 24
    new-instance p1, Landroid/graphics/Rect;

    invoke-direct {p1}, Landroid/graphics/Rect;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/ui/SignatureOutlineView;->rect:Landroid/graphics/Rect;

    .line 25
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    const-string p2, "PublishRelay.create()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/ui/SignatureOutlineView;->onLayoutChange:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 28
    sget p1, Lcom/squareup/balance/squarecard/impl/R$drawable;->square_card_signature_outline:I

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/ui/SignatureOutlineView;->setBackgroundResource(I)V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    .line 21
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/balance/squarecard/ui/SignatureOutlineView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method public final layoutChange()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Landroid/graphics/Rect;",
            ">;"
        }
    .end annotation

    .line 32
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ui/SignatureOutlineView;->onLayoutChange:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "onLayoutChange.observeOn(mainThread())"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method protected onLayout(ZIIII)V
    .locals 0

    .line 59
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 60
    iget-object p1, p0, Lcom/squareup/balance/squarecard/ui/SignatureOutlineView;->onLayoutChange:Lcom/jakewharton/rxrelay2/PublishRelay;

    iget-object p2, p0, Lcom/squareup/balance/squarecard/ui/SignatureOutlineView;->location:[I

    iget-object p3, p0, Lcom/squareup/balance/squarecard/ui/SignatureOutlineView;->rect:Landroid/graphics/Rect;

    invoke-static {p0, p2, p3}, Lcom/squareup/util/Views;->locationOfViewOnScreen(Landroid/view/View;[ILandroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 3

    .line 38
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 39
    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/ui/SignatureOutlineView;->getMeasuredHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/ui/SignatureOutlineView;->getMeasuredWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x3

    const/high16 v2, 0x40000000    # 2.0f

    if-le v0, v1, :cond_0

    .line 42
    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/ui/SignatureOutlineView;->getMeasuredWidth()I

    move-result p2

    div-int/lit8 p2, p2, 0x3

    invoke-static {p2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    .line 40
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    goto :goto_0

    .line 44
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/ui/SignatureOutlineView;->getMeasuredWidth()I

    move-result p1

    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/ui/SignatureOutlineView;->getMeasuredHeight()I

    move-result v0

    mul-int/lit8 v0, v0, 0x3

    if-le p1, v0, :cond_1

    .line 46
    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/ui/SignatureOutlineView;->getMeasuredHeight()I

    move-result p1

    mul-int/lit8 p1, p1, 0x3

    invoke-static {p1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    .line 45
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    :cond_1
    :goto_0
    return-void
.end method
