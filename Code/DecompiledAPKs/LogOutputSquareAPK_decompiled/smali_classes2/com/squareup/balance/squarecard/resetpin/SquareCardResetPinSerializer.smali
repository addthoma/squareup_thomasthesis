.class public final Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinSerializer;
.super Ljava/lang/Object;
.source "SquareCardResetPinSerializer.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSquareCardResetPinSerializer.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SquareCardResetPinSerializer.kt\ncom/squareup/balance/squarecard/resetpin/SquareCardResetPinSerializer\n+ 2 Snapshot.kt\ncom/squareup/workflow/SnapshotKt\n*L\n1#1,51:1\n180#2:52\n*E\n*S KotlinDebug\n*F\n+ 1 SquareCardResetPinSerializer.kt\ncom/squareup/balance/squarecard/resetpin/SquareCardResetPinSerializer\n*L\n30#1:52\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u000e\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0008\u001a\u00020\u0004\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinSerializer;",
        "",
        "()V",
        "deserializeState",
        "Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState;",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "snapshotState",
        "state",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinSerializer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 16
    new-instance v0, Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinSerializer;

    invoke-direct {v0}, Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinSerializer;-><init>()V

    sput-object v0, Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinSerializer;->INSTANCE:Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinSerializer;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserializeState(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState;
    .locals 3

    const-string v0, "snapshot"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-virtual {p1}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    .line 52
    new-instance v0, Lokio/Buffer;

    invoke-direct {v0}, Lokio/Buffer;-><init>()V

    invoke-virtual {v0, p1}, Lokio/Buffer;->write(Lokio/ByteString;)Lokio/Buffer;

    move-result-object p1

    check-cast p1, Lokio/BufferedSource;

    .line 31
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v0

    .line 32
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-string v2, "Class.forName(className)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/jvm/JvmClassMappingKt;->getKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    .line 35
    const-class v2, Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState$StartTwoFactorAuth;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object p1, Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState$StartTwoFactorAuth;->INSTANCE:Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState$StartTwoFactorAuth;

    check-cast p1, Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState;

    goto :goto_1

    .line 36
    :cond_0
    const-class v2, Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState$PromptingForPin;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    .line 39
    :cond_1
    const-class v2, Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState$SettingPin;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    :goto_0
    sget-object p1, Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState$PromptingForPin;->INSTANCE:Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState$PromptingForPin;

    check-cast p1, Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState;

    goto :goto_1

    .line 40
    :cond_2
    const-class v2, Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState$SettingPinSuccess;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    sget-object p1, Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState$SettingPinSuccess;->INSTANCE:Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState$SettingPinSuccess;

    check-cast p1, Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState;

    goto :goto_1

    .line 41
    :cond_3
    const-class v2, Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState$SettingPinError;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    new-instance v0, Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState$SettingPinError;

    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result p1

    invoke-direct {v0, p1}, Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState$SettingPinError;-><init>(Z)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState;

    :goto_1
    return-object p1

    .line 43
    :cond_4
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unrecognized state "

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " while deserializing SquareCardResetPinSerializer"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 42
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public final snapshotState(Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState;)Lcom/squareup/workflow/Snapshot;
    .locals 2

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    sget-object v0, Lcom/squareup/workflow/Snapshot;->Companion:Lcom/squareup/workflow/Snapshot$Companion;

    new-instance v1, Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinSerializer$snapshotState$1;

    invoke-direct {v1, p1}, Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinSerializer$snapshotState$1;-><init>(Lcom/squareup/balance/squarecard/resetpin/SquareCardResetPinState;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/Snapshot$Companion;->write(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
