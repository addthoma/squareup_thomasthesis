.class final Lcom/squareup/balance/squarecard/RealSquareCardDataRequester$handleCardStatusFetchSuccess$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealSquareCardDataRequester.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/RealSquareCardDataRequester;->handleCardStatusFetchSuccess(Ljava/util/List;)Lcom/squareup/balance/squarecard/CardStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "isRelevant",
        "",
        "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/balance/squarecard/RealSquareCardDataRequester$handleCardStatusFetchSuccess$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/balance/squarecard/RealSquareCardDataRequester$handleCardStatusFetchSuccess$1;

    invoke-direct {v0}, Lcom/squareup/balance/squarecard/RealSquareCardDataRequester$handleCardStatusFetchSuccess$1;-><init>()V

    sput-object v0, Lcom/squareup/balance/squarecard/RealSquareCardDataRequester$handleCardStatusFetchSuccess$1;->INSTANCE:Lcom/squareup/balance/squarecard/RealSquareCardDataRequester$handleCardStatusFetchSuccess$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 35
    check-cast p1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/RealSquareCardDataRequester$handleCardStatusFetchSuccess$1;->invoke(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)Z
    .locals 3

    const-string v0, "$this$isRelevant"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    .line 108
    sget-object v1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->ISSUED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->SHIPPED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->DISABLED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->ACTIVE_PENDING_2FA:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->ACTIVE:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->SUSPENDED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->REVIEW_PENDING:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    .line 109
    sget-object v1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->CUSTOMIZATION_APPROVED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    .line 107
    invoke-static {v0}, Lkotlin/collections/SetsKt;->setOf([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->card_state:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method
