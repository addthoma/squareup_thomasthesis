.class public final Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner;
.super Ljava/lang/Object;
.source "CardOrderingAuthSquareCardPersonalInfoLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCardOrderingAuthSquareCardPersonalInfoLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CardOrderingAuthSquareCardPersonalInfoLayoutRunner.kt\ncom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,70:1\n1103#2,7:71\n*E\n*S KotlinDebug\n*F\n+ 1 CardOrderingAuthSquareCardPersonalInfoLayoutRunner.kt\ncom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner\n*L\n39#1,7:71\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 \u00152\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0015B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0002H\u0002J\u0018\u0010\u0012\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00022\u0006\u0010\u0013\u001a\u00020\u0014H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;",
        "view",
        "Landroid/view/View;",
        "(Landroid/view/View;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "continueButton",
        "ownerAddress",
        "Lcom/squareup/address/AddressLayout;",
        "ownerBirthDate",
        "Lcom/squareup/register/widgets/LegacyNohoDatePickerView;",
        "ownerName",
        "Landroid/widget/EditText;",
        "configureActionBar",
        "",
        "rendering",
        "showRendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner$Companion;


# instance fields
.field private final actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final continueButton:Landroid/view/View;

.field private final ownerAddress:Lcom/squareup/address/AddressLayout;

.field private final ownerBirthDate:Lcom/squareup/register/widgets/LegacyNohoDatePickerView;

.field private final ownerName:Landroid/widget/EditText;

.field private final view:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner;->Companion:Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner;->view:Landroid/view/View;

    .line 27
    sget-object p1, Lcom/squareup/noho/NohoActionBar;->Companion:Lcom/squareup/noho/NohoActionBar$Companion;

    iget-object v0, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoActionBar$Companion;->findIn(Landroid/view/View;)Lcom/squareup/noho/NohoActionBar;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 28
    iget-object p1, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->owner_name:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo v0, "view.findViewById(R.id.owner_name)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/EditText;

    iput-object p1, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner;->ownerName:Landroid/widget/EditText;

    .line 29
    iget-object p1, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->owner_address:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo v0, "view.findViewById(R.id.owner_address)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/address/AddressLayout;

    iput-object p1, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner;->ownerAddress:Lcom/squareup/address/AddressLayout;

    .line 30
    iget-object p1, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->owner_birthdate:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo v0, "view.findViewById(R.id.owner_birthdate)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;

    iput-object p1, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner;->ownerBirthDate:Lcom/squareup/register/widgets/LegacyNohoDatePickerView;

    .line 31
    iget-object p1, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->continue_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo v0, "view.findViewById(R.id.continue_button)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner;->continueButton:Landroid/view/View;

    return-void
.end method

.method public static final synthetic access$getOwnerAddress$p(Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner;)Lcom/squareup/address/AddressLayout;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner;->ownerAddress:Lcom/squareup/address/AddressLayout;

    return-object p0
.end method

.method public static final synthetic access$getOwnerBirthDate$p(Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner;)Lcom/squareup/register/widgets/LegacyNohoDatePickerView;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner;->ownerBirthDate:Lcom/squareup/register/widgets/LegacyNohoDatePickerView;

    return-object p0
.end method

.method private final configureActionBar(Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;)V
    .locals 4

    .line 61
    iget-object v0, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 58
    new-instance v1, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 59
    new-instance v2, Lcom/squareup/util/ViewString$ResourceString;

    sget v3, Lcom/squareup/balance/squarecard/impl/R$string;->auth_square_card_title:I

    invoke-direct {v2, v3}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v2, Lcom/squareup/resources/TextModel;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 60
    sget-object v2, Lcom/squareup/noho/UpIcon;->X:Lcom/squareup/noho/UpIcon;

    new-instance v3, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner$configureActionBar$1;

    invoke-direct {v3, p1}, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner$configureActionBar$1;-><init>(Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object p1

    .line 61
    invoke-virtual {p1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method


# virtual methods
.method public showRendering(Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 2

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    iget-object p2, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner;->view:Landroid/view/View;

    new-instance v0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner$showRendering$1;

    invoke-direct {v0, p1}, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner$showRendering$1;-><init>(Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 39
    iget-object p2, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner;->continueButton:Landroid/view/View;

    .line 71
    new-instance v0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner$showRendering$$inlined$onClickDebounced$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner$showRendering$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner;Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 48
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner;->configureActionBar(Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;)V

    .line 50
    iget-object p2, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner;->ownerName:Landroid/widget/EditText;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;->getOwnerName()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 51
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;->getAddress()Lcom/squareup/address/Address;

    move-result-object p2

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner;->ownerAddress:Lcom/squareup/address/AddressLayout;

    invoke-virtual {v0, p2}, Lcom/squareup/address/AddressLayout;->setAddress(Lcom/squareup/address/Address;)V

    .line 52
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;->getBirthDate()Lorg/threeten/bp/LocalDate;

    move-result-object p1

    if-eqz p1, :cond_1

    iget-object p2, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner;->ownerBirthDate:Lcom/squareup/register/widgets/LegacyNohoDatePickerView;

    invoke-virtual {p2, p1}, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->setDate(Lorg/threeten/bp/LocalDate;)V

    .line 54
    :cond_1
    iget-object p1, p0, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner;->ownerBirthDate:Lcom/squareup/register/widgets/LegacyNohoDatePickerView;

    invoke-virtual {p1}, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->getDateFormat()Ljava/text/DateFormat;

    move-result-object p2

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    const-string v1, "Calendar.getInstance()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p1, p2}, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->setHint(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 24
    check-cast p1, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoLayoutRunner;->showRendering(Lcom/squareup/balance/squarecard/authv2/personalinfo/CardOrderingAuthSquareCardPersonalInfoRendering;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
