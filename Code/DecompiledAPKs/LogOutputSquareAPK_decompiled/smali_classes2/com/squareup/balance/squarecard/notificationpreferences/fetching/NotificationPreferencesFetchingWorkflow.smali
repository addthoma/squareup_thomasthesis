.class public final Lcom/squareup/balance/squarecard/notificationpreferences/fetching/NotificationPreferencesFetchingWorkflow;
.super Lcom/squareup/workflow/StatelessWorkflow;
.source "NotificationPreferencesFetchingWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatelessWorkflow<",
        "Lkotlin/Unit;",
        "Lcom/squareup/balance/squarecard/notificationpreferences/fetching/FetchingResult;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNotificationPreferencesFetchingWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NotificationPreferencesFetchingWorkflow.kt\ncom/squareup/balance/squarecard/notificationpreferences/fetching/NotificationPreferencesFetchingWorkflow\n+ 2 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 3 Worker.kt\ncom/squareup/workflow/Worker$Companion\n+ 4 Worker.kt\ncom/squareup/workflow/WorkerKt\n+ 5 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,46:1\n85#2:47\n240#3:48\n276#4:49\n149#5,5:50\n*E\n*S KotlinDebug\n*F\n+ 1 NotificationPreferencesFetchingWorkflow.kt\ncom/squareup/balance/squarecard/notificationpreferences/fetching/NotificationPreferencesFetchingWorkflow\n*L\n36#1:47\n36#1:48\n36#1:49\n42#1,5:50\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0008\u0002\u0018\u00002:\u0012\u0008\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u0004\u0012&\u0012$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t0\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJK\u0010\r\u001a$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t2\u0006\u0010\u000e\u001a\u00020\u00022\u0012\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\u00040\u0010H\u0016\u00a2\u0006\u0002\u0010\u0012R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/notificationpreferences/fetching/NotificationPreferencesFetchingWorkflow;",
        "Lcom/squareup/workflow/StatelessWorkflow;",
        "",
        "Lcom/squareup/balance/squarecard/notificationpreferences/fetching/NotificationPreferencesFetchingProps;",
        "Lcom/squareup/balance/squarecard/notificationpreferences/fetching/FetchingResult;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "dataStore",
        "Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesRemoteDataStore;",
        "(Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesRemoteDataStore;)V",
        "render",
        "props",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "",
        "(Lkotlin/Unit;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final dataStore:Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesRemoteDataStore;


# direct methods
.method public constructor <init>(Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesRemoteDataStore;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "dataStore"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-direct {p0}, Lcom/squareup/workflow/StatelessWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/notificationpreferences/fetching/NotificationPreferencesFetchingWorkflow;->dataStore:Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesRemoteDataStore;

    return-void
.end method


# virtual methods
.method public bridge synthetic render(Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 26
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/squarecard/notificationpreferences/fetching/NotificationPreferencesFetchingWorkflow;->render(Lkotlin/Unit;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lkotlin/Unit;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Unit;",
            "Lcom/squareup/workflow/RenderContext;",
            ")",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "context"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    iget-object p1, p0, Lcom/squareup/balance/squarecard/notificationpreferences/fetching/NotificationPreferencesFetchingWorkflow;->dataStore:Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesRemoteDataStore;

    invoke-interface {p1}, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesRemoteDataStore;->getPreferences()Lio/reactivex/Single;

    move-result-object p1

    .line 47
    sget-object v0, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v0, Lcom/squareup/balance/squarecard/notificationpreferences/fetching/NotificationPreferencesFetchingWorkflow$render$$inlined$asWorker$1;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/squareup/balance/squarecard/notificationpreferences/fetching/NotificationPreferencesFetchingWorkflow$render$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 48
    invoke-static {v0}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 49
    const-class v0, Lcom/squareup/balance/squarecard/notificationpreferences/PreferencesDataResult;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v0

    new-instance v1, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v1, v0, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v3, v1

    check-cast v3, Lcom/squareup/workflow/Worker;

    .line 36
    new-instance p1, Lcom/squareup/balance/squarecard/notificationpreferences/fetching/NotificationPreferencesFetchingWorkflow$render$1;

    invoke-direct {p1, p0}, Lcom/squareup/balance/squarecard/notificationpreferences/fetching/NotificationPreferencesFetchingWorkflow$render$1;-><init>(Lcom/squareup/balance/squarecard/notificationpreferences/fetching/NotificationPreferencesFetchingWorkflow;)V

    move-object v5, p1

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x0

    const/4 v6, 0x2

    const/4 v7, 0x0

    move-object v2, p2

    invoke-static/range {v2 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 40
    new-instance p1, Lcom/squareup/balance/squarecard/notificationpreferences/common/NotificationPreferencesLoadingScreen;

    .line 41
    new-instance v0, Lcom/squareup/balance/squarecard/notificationpreferences/fetching/NotificationPreferencesFetchingWorkflow$render$2;

    invoke-direct {v0, p0, p2}, Lcom/squareup/balance/squarecard/notificationpreferences/fetching/NotificationPreferencesFetchingWorkflow$render$2;-><init>(Lcom/squareup/balance/squarecard/notificationpreferences/fetching/NotificationPreferencesFetchingWorkflow;Lcom/squareup/workflow/RenderContext;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    .line 40
    invoke-direct {p1, v0}, Lcom/squareup/balance/squarecard/notificationpreferences/common/NotificationPreferencesLoadingScreen;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 51
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 52
    const-class v0, Lcom/squareup/balance/squarecard/notificationpreferences/common/NotificationPreferencesLoadingScreen;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    .line 53
    sget-object v1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    .line 51
    invoke-direct {p2, v0, p1, v1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 43
    sget-object p1, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {p2, p1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method
