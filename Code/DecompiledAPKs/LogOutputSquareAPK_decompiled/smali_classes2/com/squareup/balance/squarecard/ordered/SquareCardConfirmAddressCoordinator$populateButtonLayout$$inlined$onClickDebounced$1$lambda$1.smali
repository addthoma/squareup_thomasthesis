.class final Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator$populateButtonLayout$$inlined$onClickDebounced$1$lambda$1;
.super Lkotlin/jvm/internal/Lambda;
.source "SquareCardConfirmAddressCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator$populateButtonLayout$$inlined$onClickDebounced$1;->doClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke",
        "com/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator$populateButtonLayout$1$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator$populateButtonLayout$$inlined$onClickDebounced$1;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator$populateButtonLayout$$inlined$onClickDebounced$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator$populateButtonLayout$$inlined$onClickDebounced$1$lambda$1;->this$0:Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator$populateButtonLayout$$inlined$onClickDebounced$1;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 27
    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator$populateButtonLayout$$inlined$onClickDebounced$1$lambda$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 4

    .line 72
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator$populateButtonLayout$$inlined$onClickDebounced$1$lambda$1;->this$0:Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator$populateButtonLayout$$inlined$onClickDebounced$1;

    iget-object v0, v0, Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator$populateButtonLayout$$inlined$onClickDebounced$1;->$workflow$inlined:Lcom/squareup/workflow/legacy/WorkflowInput;

    new-instance v1, Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddress$Event$SendAddress;

    iget-object v2, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator$populateButtonLayout$$inlined$onClickDebounced$1$lambda$1;->this$0:Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator$populateButtonLayout$$inlined$onClickDebounced$1;

    iget-object v2, v2, Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator$populateButtonLayout$$inlined$onClickDebounced$1;->this$0:Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator;

    invoke-static {v2}, Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator;->access$getBillingAddress$p(Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator;)Lcom/squareup/address/NohoAddressLayout;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/address/NohoAddressLayout;->getAddress()Lcom/squareup/address/Address;

    move-result-object v2

    const-string v3, "billingAddress.address"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddress$Event$SendAddress;-><init>(Lcom/squareup/address/Address;)V

    invoke-interface {v0, v1}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method
