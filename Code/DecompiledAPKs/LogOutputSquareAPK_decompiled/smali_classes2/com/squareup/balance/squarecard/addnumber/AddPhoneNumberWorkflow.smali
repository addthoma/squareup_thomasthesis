.class public final Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "AddPhoneNumberWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow$SubmitPhoneResult;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lkotlin/Unit;",
        "Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberState;",
        "Lkotlin/Unit;",
        "Ljava/util/Map<",
        "+",
        "Lcom/squareup/container/ContainerLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAddPhoneNumberWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AddPhoneNumberWorkflow.kt\ncom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow\n+ 2 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n+ 3 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 4 Worker.kt\ncom/squareup/workflow/Worker$Companion\n+ 5 Worker.kt\ncom/squareup/workflow/WorkerKt\n*L\n1#1,183:1\n149#2,5:184\n85#3:189\n240#4:190\n276#5:191\n*E\n*S KotlinDebug\n*F\n+ 1 AddPhoneNumberWorkflow.kt\ncom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow\n*L\n113#1,5:184\n157#1:189\n157#1:190\n157#1:191\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u008c\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u000028\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0002\u0012\"\u0012 \u0012\u0006\u0008\u0001\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\u0002`\u00080\u0001:\u0001-B\u000f\u0008\u0007\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ:\u0010\u000c\u001a\u0018\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e0\u0006j\u0008\u0012\u0004\u0012\u00020\r`\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0012\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0014\u0012\u0004\u0012\u00020\u00020\u0013H\u0002J2\u0010\u0015\u001a\u0018\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020\u000e0\u0006j\u0008\u0012\u0004\u0012\u00020\u0016`\u000f2\u0012\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0017\u0012\u0004\u0012\u00020\u00020\u0013H\u0002J\u0014\u0010\u0018\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00020\u0019H\u0002J\u001f\u0010\u001a\u001a\u00020\u00032\u0006\u0010\u001b\u001a\u00020\u00022\u0008\u0010\u001c\u001a\u0004\u0018\u00010\u001dH\u0016\u00a2\u0006\u0002\u0010\u001eJO\u0010\u001f\u001a \u0012\u0006\u0008\u0001\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\u0002`\u00082\u0006\u0010\u001b\u001a\u00020\u00022\u0006\u0010\u0010\u001a\u00020\u00032\u0012\u0010 \u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00020!H\u0016\u00a2\u0006\u0002\u0010\"J\u0010\u0010#\u001a\u00020\u001d2\u0006\u0010\u0010\u001a\u00020\u0003H\u0016J\u0016\u0010$\u001a\u0008\u0012\u0004\u0012\u00020&0%2\u0006\u0010\'\u001a\u00020(H\u0002J2\u0010)\u001a\u0018\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e0\u0006j\u0008\u0012\u0004\u0012\u00020\r`\u000f2\u0012\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0014\u0012\u0004\u0012\u00020\u00020\u0013H\u0002J\u0012\u0010*\u001a\u00020&*\u0008\u0012\u0004\u0012\u00020,0+H\u0002R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006."
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "",
        "Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberState;",
        "",
        "Lcom/squareup/container/ContainerLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberScreen;",
        "bizbankService",
        "Lcom/squareup/balance/core/server/bizbank/BizbankService;",
        "(Lcom/squareup/balance/core/server/bizbank/BizbankService;)V",
        "addPhoneNumberErrorScreen",
        "Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "state",
        "Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberState$AddPhoneNumberError;",
        "onEvent",
        "Lkotlin/Function1;",
        "Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Event;",
        "enterPhoneNumberScreen",
        "Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberScreen;",
        "Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberScreen$Event;",
        "finishAndExit",
        "Lcom/squareup/workflow/WorkflowAction;",
        "initialState",
        "input",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberState;",
        "render",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "(Lkotlin/Unit;Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;",
        "snapshotState",
        "submitPhoneNumber",
        "Lcom/squareup/workflow/Worker;",
        "Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow$SubmitPhoneResult;",
        "phoneNumber",
        "",
        "submittingPhoneNumberScreen",
        "toSubmitPhoneResult",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;",
        "Lcom/squareup/protos/client/bizbank/AddMobileNumberForSmsNotificationsResponse;",
        "SubmitPhoneResult",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;


# direct methods
.method public constructor <init>(Lcom/squareup/balance/core/server/bizbank/BizbankService;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "bizbankService"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow;->bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

    return-void
.end method

.method public static final synthetic access$finishAndExit(Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 52
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow;->finishAndExit()Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$toSubmitPhoneResult(Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow$SubmitPhoneResult;
    .locals 0

    .line 52
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow;->toSubmitPhoneResult(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow$SubmitPhoneResult;

    move-result-object p0

    return-object p0
.end method

.method private final addPhoneNumberErrorScreen(Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberState$AddPhoneNumberError;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/Screen;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberState$AddPhoneNumberError;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Event;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/workflow/legacy/Screen;"
        }
    .end annotation

    .line 130
    new-instance v0, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;

    .line 131
    new-instance v1, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Failed;

    .line 132
    new-instance v2, Lcom/squareup/util/ViewString$TextString;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberState$AddPhoneNumberError;->getTitle()Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-direct {v2, v3}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v2, Lcom/squareup/util/ViewString;

    .line 133
    new-instance v3, Lcom/squareup/util/ViewString$TextString;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberState$AddPhoneNumberError;->getMessage()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-direct {v3, p1}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v3, Lcom/squareup/util/ViewString;

    .line 134
    sget p1, Lcom/squareup/common/strings/R$string;->retry:I

    .line 135
    sget v4, Lcom/squareup/common/strings/R$string;->cancel:I

    .line 131
    invoke-direct {v1, v2, v3, p1, v4}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Failed;-><init>(Lcom/squareup/util/ViewString;Lcom/squareup/util/ViewString;II)V

    check-cast v1, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData;

    .line 130
    invoke-direct {v0, v1, p2}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;-><init>(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData;Lkotlin/jvm/functions/Function1;)V

    .line 139
    invoke-static {v0}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreenKt;->asSheetScreen(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    return-object p1
.end method

.method private final enterPhoneNumberScreen(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/Screen;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberScreen$Event;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/workflow/legacy/Screen;"
        }
    .end annotation

    .line 113
    new-instance v0, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberScreen;

    invoke-direct {v0, p1}, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberScreen;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 185
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 186
    const-class v1, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberScreen;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    const-string v2, ""

    invoke-static {v1, v2}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    .line 187
    sget-object v2, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v2}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v2

    .line 185
    invoke-direct {p1, v1, v0, v2}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object p1
.end method

.method private final finishAndExit()Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberState;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 108
    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v0

    return-object v0
.end method

.method private final submitPhoneNumber(Ljava/lang/String;)Lcom/squareup/workflow/Worker;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/workflow/Worker<",
            "Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow$SubmitPhoneResult;",
            ">;"
        }
    .end annotation

    .line 145
    new-instance v0, Lcom/squareup/protos/client/bizbank/AddMobileNumberForSmsNotificationsRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/AddMobileNumberForSmsNotificationsRequest$Builder;-><init>()V

    .line 146
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bizbank/AddMobileNumberForSmsNotificationsRequest$Builder;->mobile_number(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/AddMobileNumberForSmsNotificationsRequest$Builder;

    move-result-object p1

    .line 147
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/AddMobileNumberForSmsNotificationsRequest$Builder;->build()Lcom/squareup/protos/client/bizbank/AddMobileNumberForSmsNotificationsRequest;

    move-result-object p1

    .line 148
    iget-object v0, p0, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow;->bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

    const-string v1, "request"

    .line 149
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, p1}, Lcom/squareup/balance/core/server/bizbank/BizbankService;->addMobileNumberForSmsNotifications(Lcom/squareup/protos/client/bizbank/AddMobileNumberForSmsNotificationsRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 150
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 151
    new-instance v0, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow$submitPhoneNumber$1;

    invoke-direct {v0, p0}, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow$submitPhoneNumber$1;-><init>(Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "bizbankService\n        .\u2026t()\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 189
    sget-object v0, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v0, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow$submitPhoneNumber$$inlined$asWorker$1;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow$submitPhoneNumber$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 190
    invoke-static {v0}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 191
    const-class v0, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow$SubmitPhoneResult;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v0

    new-instance v1, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v1, v0, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v1, Lcom/squareup/workflow/Worker;

    return-object v1
.end method

.method private final submittingPhoneNumberScreen(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/Screen;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Event;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/workflow/legacy/Screen;"
        }
    .end annotation

    .line 119
    new-instance v0, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;

    .line 120
    new-instance v1, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Loading;

    sget v2, Lcom/squareup/balance/squarecard/impl/R$string;->square_card_submitting_phone_number:I

    invoke-direct {v1, v2}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Loading;-><init>(I)V

    check-cast v1, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData;

    .line 119
    invoke-direct {v0, v1, p1}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;-><init>(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData;Lkotlin/jvm/functions/Function1;)V

    .line 123
    invoke-static {v0}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreenKt;->asSheetScreen(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    return-object p1
.end method

.method private final toSubmitPhoneResult(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow$SubmitPhoneResult;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure<",
            "Lcom/squareup/protos/client/bizbank/AddMobileNumberForSmsNotificationsResponse;",
            ">;)",
            "Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow$SubmitPhoneResult;"
        }
    .end annotation

    .line 162
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getOkayResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/bizbank/AddMobileNumberForSmsNotificationsResponse;

    if-eqz p1, :cond_3

    iget-object p1, p1, Lcom/squareup/protos/client/bizbank/AddMobileNumberForSmsNotificationsResponse;->status:Lcom/squareup/protos/client/Status;

    if-eqz p1, :cond_3

    .line 163
    iget-object v0, p1, Lcom/squareup/protos/client/Status;->localized_description:Ljava/lang/String;

    const-string v1, "status.localized_description"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/squareup/protos/client/Status;->localized_title:Ljava/lang/String;

    const-string v4, "status.localized_title"

    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    if-eqz v2, :cond_2

    .line 164
    new-instance v0, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow$SubmitPhoneResult$RetryableError;

    .line 165
    iget-object v2, p1, Lcom/squareup/protos/client/Status;->localized_title:Ljava/lang/String;

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 166
    iget-object p1, p1, Lcom/squareup/protos/client/Status;->localized_description:Ljava/lang/String;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 164
    invoke-direct {v0, v2, p1}, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow$SubmitPhoneResult$RetryableError;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow$SubmitPhoneResult;

    goto :goto_2

    .line 169
    :cond_2
    sget-object p1, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow$SubmitPhoneResult$UncorrectableError;->INSTANCE:Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow$SubmitPhoneResult$UncorrectableError;

    move-object v0, p1

    check-cast v0, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow$SubmitPhoneResult;

    :goto_2
    if-eqz v0, :cond_3

    goto :goto_3

    .line 171
    :cond_3
    sget-object p1, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow$SubmitPhoneResult$UncorrectableError;->INSTANCE:Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow$SubmitPhoneResult$UncorrectableError;

    move-object v0, p1

    check-cast v0, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow$SubmitPhoneResult;

    :goto_3
    return-object v0
.end method


# virtual methods
.method public initialState(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberState;
    .locals 1

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    .line 59
    sget-object p1, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberSerializer;->INSTANCE:Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberSerializer;

    invoke-virtual {p1, p2}, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberSerializer;->deserializeState(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberState;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberState$EnterPhoneNumber;->INSTANCE:Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberState$EnterPhoneNumber;

    check-cast p1, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberState;

    :goto_0
    return-object p1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 52
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow;->initialState(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 52
    check-cast p1, Lkotlin/Unit;

    check-cast p2, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow;->render(Lkotlin/Unit;Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lkotlin/Unit;Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Unit;",
            "Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberState;",
            "-",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "state"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "context"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    sget-object p1, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberState$EnterPhoneNumber;->INSTANCE:Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberState$EnterPhoneNumber;

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    new-instance p1, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow$render$screen$1;

    invoke-direct {p1, p0}, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow$render$screen$1;-><init>(Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, p1}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow;->enterPhoneNumberScreen(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto :goto_0

    .line 73
    :cond_0
    instance-of p1, p2, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberState$SubmittingPhoneNumber;

    if-eqz p1, :cond_1

    .line 74
    move-object p1, p2

    check-cast p1, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberState$SubmittingPhoneNumber;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberState$SubmittingPhoneNumber;->getPhoneNumber()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow;->submitPhoneNumber(Ljava/lang/String;)Lcom/squareup/workflow/Worker;

    move-result-object v1

    const/4 v2, 0x0

    new-instance p1, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow$render$screen$2;

    invoke-direct {p1, p0}, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow$render$screen$2;-><init>(Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow;)V

    move-object v3, p1

    check-cast v3, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object v0, p3

    invoke-static/range {v0 .. v5}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 87
    new-instance p1, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow$render$screen$3;

    invoke-direct {p1, p2}, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow$render$screen$3;-><init>(Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberState;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, p1}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow;->submittingPhoneNumberScreen(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto :goto_0

    .line 94
    :cond_1
    instance-of p1, p2, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberState$AddPhoneNumberError;

    if-eqz p1, :cond_2

    check-cast p2, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberState$AddPhoneNumberError;

    new-instance p1, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow$render$screen$4;

    invoke-direct {p1, p0}, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow$render$screen$4;-><init>(Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, p1}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p1

    invoke-direct {p0, p2, p1}, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow;->addPhoneNumberErrorScreen(Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberState$AddPhoneNumberError;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    .line 103
    :goto_0
    sget-object p2, Lcom/squareup/container/PosLayering;->SHEET:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1

    .line 94
    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    invoke-static {p1}, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberSerializerKt;->toSnapshot(Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 52
    check-cast p1, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberState;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow;->snapshotState(Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
