.class public final Lcom/squareup/balance/squarecard/ui/BottomSheetIndicator;
.super Landroid/widget/FrameLayout;
.source "BottomSheetIndicator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBottomSheetIndicator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BottomSheetIndicator.kt\ncom/squareup/balance/squarecard/ui/BottomSheetIndicator\n+ 2 StyledAttributes.kt\ncom/squareup/util/StyledAttributesKt\n*L\n1#1,93:1\n37#2,6:94\n*E\n*S KotlinDebug\n*F\n+ 1 BottomSheetIndicator.kt\ncom/squareup/balance/squarecard/ui/BottomSheetIndicator\n*L\n46#1,6:94\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B%\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0002J\u0008\u0010\u0014\u001a\u00020\u0007H\u0002J\u0010\u0010\u0015\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0014R\u000e\u0010\t\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/ui/BottomSheetIndicator;",
        "Landroid/widget/FrameLayout;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "defStyleAttr",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "indicatorHeight",
        "indicatorOffset",
        "indicatorWidth",
        "showIndicator",
        "",
        "tabPaint",
        "Landroid/graphics/Paint;",
        "drawIndicator",
        "",
        "canvas",
        "Landroid/graphics/Canvas;",
        "extraPaddingTop",
        "onDraw",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final indicatorHeight:I

.field private final indicatorOffset:I

.field private final indicatorWidth:I

.field private showIndicator:Z

.field private final tabPaint:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/balance/squarecard/ui/BottomSheetIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/balance/squarecard/ui/BottomSheetIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/squareup/balance/squarecard/ui/BottomSheetIndicator;->tabPaint:Landroid/graphics/Paint;

    .line 36
    iput-boolean v1, p0, Lcom/squareup/balance/squarecard/ui/BottomSheetIndicator;->showIndicator:Z

    .line 37
    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/ui/BottomSheetIndicator;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 38
    sget v2, Lcom/squareup/balance/squarecard/impl/R$dimen;->bottom_sheet_indicator_width:I

    .line 37
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/squareup/balance/squarecard/ui/BottomSheetIndicator;->indicatorWidth:I

    .line 41
    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/ui/BottomSheetIndicator;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/squareup/balance/squarecard/impl/R$dimen;->bottom_sheet_indicator_offset:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/squareup/balance/squarecard/ui/BottomSheetIndicator;->indicatorOffset:I

    .line 43
    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/ui/BottomSheetIndicator;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/squareup/balance/squarecard/impl/R$dimen;->bottom_sheet_indicator_height:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/squareup/balance/squarecard/ui/BottomSheetIndicator;->indicatorHeight:I

    .line 48
    sget-object v0, Lcom/squareup/balance/squarecard/impl/R$styleable;->BottomSheetIndicator:[I

    const-string v2, "R.styleable.BottomSheetIndicator"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    sget v2, Lcom/squareup/balance/squarecard/impl/R$style;->Widget_BottomSheetIndicator:I

    .line 94
    invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    :try_start_0
    const-string p2, "a"

    .line 96
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    sget p2, Lcom/squareup/balance/squarecard/impl/R$styleable;->BottomSheetIndicator_sqIndicatorColor:I

    .line 55
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    sget v0, Lcom/squareup/noho/R$color;->noho_row_icon:I

    invoke-virtual {p3, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result p3

    .line 53
    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p2

    .line 57
    invoke-static {p0}, Lcom/squareup/balance/squarecard/ui/BottomSheetIndicator;->access$getTabPaint$p(Lcom/squareup/balance/squarecard/ui/BottomSheetIndicator;)Landroid/graphics/Paint;

    move-result-object p3

    invoke-virtual {p3, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 60
    sget p2, Lcom/squareup/balance/squarecard/impl/R$styleable;->BottomSheetIndicator_sqShowIndicator:I

    invoke-virtual {p1, p2, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p2

    invoke-static {p0, p2}, Lcom/squareup/balance/squarecard/ui/BottomSheetIndicator;->access$setShowIndicator$p(Lcom/squareup/balance/squarecard/ui/BottomSheetIndicator;Z)V

    .line 62
    invoke-static {p0}, Lcom/squareup/balance/squarecard/ui/BottomSheetIndicator;->access$getShowIndicator$p(Lcom/squareup/balance/squarecard/ui/BottomSheetIndicator;)Z

    move-result p2

    if-eqz p2, :cond_0

    .line 63
    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/ui/BottomSheetIndicator;->getPaddingLeft()I

    move-result p2

    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/ui/BottomSheetIndicator;->getPaddingTop()I

    move-result p3

    invoke-static {p0}, Lcom/squareup/balance/squarecard/ui/BottomSheetIndicator;->access$extraPaddingTop(Lcom/squareup/balance/squarecard/ui/BottomSheetIndicator;)I

    move-result v0

    add-int/2addr p3, v0

    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/ui/BottomSheetIndicator;->getPaddingRight()I

    move-result v0

    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/ui/BottomSheetIndicator;->getPaddingBottom()I

    move-result v1

    invoke-virtual {p0, p2, p3, v0, v1}, Lcom/squareup/balance/squarecard/ui/BottomSheetIndicator;->setPadding(IIII)V

    const/4 p2, 0x0

    .line 64
    invoke-virtual {p0, p2}, Lcom/squareup/balance/squarecard/ui/BottomSheetIndicator;->setWillNotDraw(Z)V

    .line 66
    :cond_0
    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 98
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void

    :catchall_0
    move-exception p2

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    throw p2
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    .line 31
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 32
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/balance/squarecard/ui/BottomSheetIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public static final synthetic access$extraPaddingTop(Lcom/squareup/balance/squarecard/ui/BottomSheetIndicator;)I
    .locals 0

    .line 29
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/ui/BottomSheetIndicator;->extraPaddingTop()I

    move-result p0

    return p0
.end method

.method public static final synthetic access$getShowIndicator$p(Lcom/squareup/balance/squarecard/ui/BottomSheetIndicator;)Z
    .locals 0

    .line 29
    iget-boolean p0, p0, Lcom/squareup/balance/squarecard/ui/BottomSheetIndicator;->showIndicator:Z

    return p0
.end method

.method public static final synthetic access$getTabPaint$p(Lcom/squareup/balance/squarecard/ui/BottomSheetIndicator;)Landroid/graphics/Paint;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/squareup/balance/squarecard/ui/BottomSheetIndicator;->tabPaint:Landroid/graphics/Paint;

    return-object p0
.end method

.method public static final synthetic access$setShowIndicator$p(Lcom/squareup/balance/squarecard/ui/BottomSheetIndicator;Z)V
    .locals 0

    .line 29
    iput-boolean p1, p0, Lcom/squareup/balance/squarecard/ui/BottomSheetIndicator;->showIndicator:Z

    return-void
.end method

.method private final drawIndicator(Landroid/graphics/Canvas;)V
    .locals 11

    .line 78
    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/ui/BottomSheetIndicator;->getRight()I

    move-result v0

    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/ui/BottomSheetIndicator;->getLeft()I

    move-result v1

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    .line 79
    iget v1, p0, Lcom/squareup/balance/squarecard/ui/BottomSheetIndicator;->indicatorWidth:I

    div-int/lit8 v1, v1, 0x2

    .line 80
    iget v2, p0, Lcom/squareup/balance/squarecard/ui/BottomSheetIndicator;->indicatorOffset:I

    int-to-float v5, v2

    sub-int v2, v0, v1

    int-to-float v4, v2

    add-int/2addr v0, v1

    int-to-float v6, v0

    .line 84
    iget v0, p0, Lcom/squareup/balance/squarecard/ui/BottomSheetIndicator;->indicatorHeight:I

    int-to-float v1, v0

    add-float v7, v5, v1

    int-to-float v1, v0

    const/high16 v2, 0x40000000    # 2.0f

    div-float v8, v1, v2

    int-to-float v0, v0

    div-float v9, v0, v2

    .line 85
    iget-object v10, p0, Lcom/squareup/balance/squarecard/ui/BottomSheetIndicator;->tabPaint:Landroid/graphics/Paint;

    move-object v3, p1

    .line 82
    invoke-virtual/range {v3 .. v10}, Landroid/graphics/Canvas;->drawRoundRect(FFFFFFLandroid/graphics/Paint;)V

    return-void
.end method

.method private final extraPaddingTop()I
    .locals 2

    .line 90
    iget v0, p0, Lcom/squareup/balance/squarecard/ui/BottomSheetIndicator;->indicatorHeight:I

    iget v1, p0, Lcom/squareup/balance/squarecard/ui/BottomSheetIndicator;->indicatorOffset:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 72
    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/ui/BottomSheetIndicator;->showIndicator:Z

    if-eqz v0, :cond_0

    .line 73
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/ui/BottomSheetIndicator;->drawIndicator(Landroid/graphics/Canvas;)V

    :cond_0
    return-void
.end method
