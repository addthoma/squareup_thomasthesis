.class public abstract Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingWorkflow$Action;
.super Ljava/lang/Object;
.source "NotificationPreferencesUpdatingWorkflow.kt"

# interfaces
.implements Lcom/squareup/workflow/WorkflowAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Action"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingWorkflow$Action$DisplayError;,
        Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingWorkflow$Action$Exit;,
        Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingWorkflow$Action$Retry;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingState;",
        "Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingOutput;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u0012\u0012\u0008\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u00040\u0001:\u0003\t\n\u000bB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0005J\u001c\u0010\u0006\u001a\u00020\u0007*\u0012\u0012\u0008\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u00040\u0008H\u0016\u0082\u0001\u0003\u000c\r\u000e\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingWorkflow$Action;",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingState;",
        "Lcom/squareup/balance/squarecard/notificationpreferences/updating/State;",
        "Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingOutput;",
        "()V",
        "apply",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "DisplayError",
        "Exit",
        "Retry",
        "Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingWorkflow$Action$DisplayError;",
        "Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingWorkflow$Action$Exit;",
        "Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingWorkflow$Action$Retry;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 111
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingWorkflow$Action;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingOutput;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingState;",
            ">;)",
            "Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingOutput;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Implement Updater.apply"
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 111
    invoke-static {p0, p1}, Lcom/squareup/workflow/WorkflowAction$DefaultImpls;->apply(Lcom/squareup/workflow/WorkflowAction;Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingOutput;

    return-object p1
.end method

.method public bridge synthetic apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;
    .locals 0

    .line 111
    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingWorkflow$Action;->apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingOutput;

    move-result-object p1

    return-object p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingState;",
            "-",
            "Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    sget-object v0, Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingWorkflow$Action$DisplayError;->INSTANCE:Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingWorkflow$Action$DisplayError;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 115
    sget-object v0, Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingState$ErrorUpdatingPreferences;->INSTANCE:Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingState$ErrorUpdatingPreferences;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    goto :goto_0

    .line 117
    :cond_0
    sget-object v0, Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingWorkflow$Action$Retry;->INSTANCE:Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingWorkflow$Action$Retry;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingOutput$OnRetry;->INSTANCE:Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingOutput$OnRetry;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    goto :goto_0

    .line 118
    :cond_1
    sget-object v0, Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingWorkflow$Action$Exit;->INSTANCE:Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingWorkflow$Action$Exit;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingOutput$Updated;->INSTANCE:Lcom/squareup/balance/squarecard/notificationpreferences/updating/NotificationPreferencesUpdatingOutput$Updated;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    :cond_2
    :goto_0
    return-void
.end method
