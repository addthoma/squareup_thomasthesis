.class public final Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator$initPhoneNumberField$1;
.super Lcom/squareup/text/ScrubbingTextWatcher;
.source "EnterPhoneNumberCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;->initPhoneNumberField()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006"
    }
    d2 = {
        "com/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator$initPhoneNumberField$1",
        "Lcom/squareup/text/ScrubbingTextWatcher;",
        "afterTextChanged",
        "",
        "editable",
        "Landroid/text/Editable;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;Lcom/squareup/text/InsertingScrubber;Lcom/squareup/text/HasSelectableText;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/text/InsertingScrubber;",
            "Lcom/squareup/text/HasSelectableText;",
            ")V"
        }
    .end annotation

    .line 71
    iput-object p1, p0, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator$initPhoneNumberField$1;->this$0:Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;

    invoke-direct {p0, p2, p3}, Lcom/squareup/text/ScrubbingTextWatcher;-><init>(Lcom/squareup/text/InsertingScrubber;Lcom/squareup/text/HasSelectableText;)V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 1

    const-string v0, "editable"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    invoke-super {p0, p1}, Lcom/squareup/text/ScrubbingTextWatcher;->afterTextChanged(Landroid/text/Editable;)V

    .line 76
    iget-object p1, p0, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator$initPhoneNumberField$1;->this$0:Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;

    invoke-static {p1}, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;->access$updateActionState(Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;)V

    return-void
.end method
