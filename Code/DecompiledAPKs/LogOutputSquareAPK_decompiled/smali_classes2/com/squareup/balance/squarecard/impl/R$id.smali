.class public final Lcom/squareup/balance/squarecard/impl/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/squarecard/impl/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final add_to_google_pay:I = 0x7f0a0174

.field public static final cancel_square_card_choice:I = 0x7f0a029b

.field public static final cancel_square_card_message:I = 0x7f0a029c

.field public static final canceled_card_feedback:I = 0x7f0a029e

.field public static final canceled_card_submit_feedback:I = 0x7f0a029f

.field public static final card_activation_confirm_code_title:I = 0x7f0a02b2

.field public static final card_disabled:I = 0x7f0a02b6

.field public static final card_ordered_deposits_info_title:I = 0x7f0a02bd

.field public static final card_preview:I = 0x7f0a02c1

.field public static final card_suspended_message:I = 0x7f0a02ca

.field public static final clear:I = 0x7f0a033d

.field public static final closed:I = 0x7f0a035f

.field public static final continue_button:I = 0x7f0a03a9

.field public static final drawing_mode:I = 0x7f0a0605

.field public static final get_help_with_card:I = 0x7f0a0789

.field public static final google_pay_loading:I = 0x7f0a07ba

.field public static final google_pay_message:I = 0x7f0a07bb

.field public static final lost_square_card_choice:I = 0x7f0a0964

.field public static final never_received_square_card_choice:I = 0x7f0a0a1a

.field public static final notification_error_action_bar:I = 0x7f0a0a5a

.field public static final notification_error_message:I = 0x7f0a0a5b

.field public static final notification_loading_action_bar:I = 0x7f0a0a5e

.field public static final notification_loading_progress_bar:I = 0x7f0a0a5f

.field public static final notification_preferences_email_row:I = 0x7f0a0a65

.field public static final notification_preferences_email_value_row:I = 0x7f0a0a66

.field public static final notification_preferences_pref_card_declines:I = 0x7f0a0a67

.field public static final open:I = 0x7f0a0aad

.field public static final order_square_card:I = 0x7f0a0ae2

.field public static final order_square_card_business_account_name_row:I = 0x7f0a0ae3

.field public static final order_square_card_business_name_continue_button:I = 0x7f0a0ae4

.field public static final order_square_card_business_name_help_message:I = 0x7f0a0ae5

.field public static final order_square_card_business_name_row:I = 0x7f0a0ae6

.field public static final order_square_card_business_name_warning:I = 0x7f0a0ae7

.field public static final order_square_card_cardholder_agreement:I = 0x7f0a0ae8

.field public static final order_square_card_fetching_business_info_message:I = 0x7f0a0ae9

.field public static final order_square_card_message:I = 0x7f0a0aea

.field public static final order_square_card_title:I = 0x7f0a0aeb

.field public static final ordered_square_card_activate_trigger:I = 0x7f0a0af4

.field public static final ordered_square_card_message:I = 0x7f0a0af5

.field public static final owner_address:I = 0x7f0a0b7d

.field public static final owner_birthdate:I = 0x7f0a0b7e

.field public static final owner_name:I = 0x7f0a0b7f

.field public static final owner_ssn:I = 0x7f0a0b80

.field public static final reset_card_pin:I = 0x7f0a0d6e

.field public static final root_view:I = 0x7f0a0d9b

.field public static final signature_hint:I = 0x7f0a0e91

.field public static final signature_outline:I = 0x7f0a0e93

.field public static final signature_pad:I = 0x7f0a0e94

.field public static final signature_view:I = 0x7f0a0e95

.field public static final square_card:I = 0x7f0a0ef8

.field public static final square_card_activation_code_confirmation_continue_button:I = 0x7f0a0ef9

.field public static final square_card_activation_code_confirmation_help:I = 0x7f0a0efa

.field public static final square_card_activation_code_confirmation_input:I = 0x7f0a0efb

.field public static final square_card_activation_code_help_message:I = 0x7f0a0efc

.field public static final square_card_activation_complete_continue_button:I = 0x7f0a0efd

.field public static final square_card_activation_complete_message:I = 0x7f0a0efe

.field public static final square_card_activation_create_pin_message:I = 0x7f0a0eff

.field public static final square_card_activation_pin_confirmation_entry:I = 0x7f0a0f00

.field public static final square_card_activation_pin_entry:I = 0x7f0a0f01

.field public static final square_card_activation_submit_pin:I = 0x7f0a0f02

.field public static final square_card_confirm_address:I = 0x7f0a0f03

.field public static final square_card_confirm_address_button:I = 0x7f0a0f04

.field public static final square_card_confirm_address_message:I = 0x7f0a0f05

.field public static final square_card_confirmation_card_confirmation_continue_button:I = 0x7f0a0f06

.field public static final square_card_enter_phone_number:I = 0x7f0a0f07

.field public static final square_card_enter_phone_number_submit:I = 0x7f0a0f08

.field public static final square_card_ordered_deposits_info:I = 0x7f0a0f0f

.field public static final square_card_ordered_deposits_info_continue:I = 0x7f0a0f10

.field public static final square_card_progress_result:I = 0x7f0a0f12

.field public static final square_card_progress_spinner_holder:I = 0x7f0a0f13

.field public static final square_card_progress_spinner_message:I = 0x7f0a0f14

.field public static final square_card_validation_entry_view:I = 0x7f0a0f18

.field public static final stable_action_bar:I = 0x7f0a0f1c

.field public static final stamp_view:I = 0x7f0a0f1d

.field public static final stamps_loading_view:I = 0x7f0a0f1e

.field public static final stamps_mode:I = 0x7f0a0f1f

.field public static final stamps_recycler_view:I = 0x7f0a0f20

.field public static final stamps_selected_indicator:I = 0x7f0a0f21

.field public static final stolen_square_card_choice:I = 0x7f0a0f3f

.field public static final street:I = 0x7f0a0f41

.field public static final svg_view:I = 0x7f0a0f51

.field public static final toggle_card_state:I = 0x7f0a104b

.field public static final toggle_show_card_details:I = 0x7f0a104c

.field public static final trash_stamp:I = 0x7f0a108a

.field public static final undo:I = 0x7f0a10b0

.field public static final view_billing_address:I = 0x7f0a10f2

.field public static final view_billing_address_layout:I = 0x7f0a10f3

.field public static final view_notifications:I = 0x7f0a10fb


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
