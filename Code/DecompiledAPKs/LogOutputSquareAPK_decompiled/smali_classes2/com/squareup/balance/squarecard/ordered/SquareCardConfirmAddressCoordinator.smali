.class public final Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "SquareCardConfirmAddressCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSquareCardConfirmAddressCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SquareCardConfirmAddressCoordinator.kt\ncom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,116:1\n1103#2,7:117\n*E\n*S KotlinDebug\n*F\n+ 1 SquareCardConfirmAddressCoordinator.kt\ncom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator\n*L\n70#1,7:117\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B#\u0012\u001c\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0016J\u0010\u0010\u0013\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0002J\u0016\u0010\u0014\u001a\u00020\u00102\u000c\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0016H\u0002J\u0016\u0010\u0017\u001a\u00020\u00102\u000c\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0016H\u0002J\u0010\u0010\u0018\u001a\u00020\u00102\u0006\u0010\u0019\u001a\u00020\u001aH\u0002J\u0016\u0010\u001b\u001a\u00020\u00102\u000c\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0016H\u0002J(\u0010\u001c\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0016\u0010\u001d\u001a\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u0007H\u0002J$\u0010\u001e\u001a\u00020\u00102\u000c\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00162\u000c\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00020\u00100 H\u0002J\u000c\u0010!\u001a\u00020\"*\u00020#H\u0002R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R$\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006$"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddress$ScreenData;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddress$Event;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressScreen;",
        "(Lio/reactivex/Observable;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "activateButton",
        "Lcom/squareup/noho/NohoButton;",
        "billingAddress",
        "Lcom/squareup/address/NohoAddressLayout;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "handleBack",
        "workflow",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "populateActionBar",
        "populateAddressLayout",
        "address",
        "Lcom/squareup/address/Address;",
        "populateButtonLayout",
        "update",
        "screen",
        "validateAddress",
        "proceedBlock",
        "Lkotlin/Function0;",
        "isValidPostal",
        "",
        "Lcom/squareup/address/AddressLayout;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/noho/NohoActionBar;

.field private activateButton:Lcom/squareup/noho/NohoButton;

.field private billingAddress:Lcom/squareup/address/NohoAddressLayout;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddress$ScreenData;",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddress$Event;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddress$ScreenData;",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddress$Event;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator;->screens:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$getBillingAddress$p(Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator;)Lcom/squareup/address/NohoAddressLayout;
    .locals 1

    .line 27
    iget-object p0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator;->billingAddress:Lcom/squareup/address/NohoAddressLayout;

    if-nez p0, :cond_0

    const-string v0, "billingAddress"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$handleBack(Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 0

    .line 27
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator;->handleBack(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-void
.end method

.method public static final synthetic access$setBillingAddress$p(Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator;Lcom/squareup/address/NohoAddressLayout;)V
    .locals 0

    .line 27
    iput-object p1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator;->billingAddress:Lcom/squareup/address/NohoAddressLayout;

    return-void
.end method

.method public static final synthetic access$update(Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator;Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 0

    .line 27
    invoke-direct {p0, p1, p2}, Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator;->update(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V

    return-void
.end method

.method public static final synthetic access$validateAddress(Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator;Lcom/squareup/workflow/legacy/WorkflowInput;Lkotlin/jvm/functions/Function0;)V
    .locals 0

    .line 27
    invoke-direct {p0, p1, p2}, Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator;->validateAddress(Lcom/squareup/workflow/legacy/WorkflowInput;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 111
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->square_card_confirm_address:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/address/NohoAddressLayout;

    iput-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator;->billingAddress:Lcom/squareup/address/NohoAddressLayout;

    .line 112
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->square_card_confirm_address_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoButton;

    iput-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator;->activateButton:Lcom/squareup/noho/NohoButton;

    .line 113
    sget-object v0, Lcom/squareup/noho/NohoActionBar;->Companion:Lcom/squareup/noho/NohoActionBar$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar$Companion;->findIn(Landroid/view/View;)Lcom/squareup/noho/NohoActionBar;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    return-void
.end method

.method private final handleBack(Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddress$Event;",
            ">;)V"
        }
    .end annotation

    .line 65
    sget-object v0, Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddress$Event$GoBack;->INSTANCE:Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddress$Event$GoBack;

    invoke-interface {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method private final isValidPostal(Lcom/squareup/address/AddressLayout;)Z
    .locals 1

    .line 104
    invoke-virtual {p1}, Lcom/squareup/address/AddressLayout;->getPostal()Landroid/widget/EditText;

    move-result-object p1

    const-string v0, "postal"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 106
    invoke-static {p1}, Lcom/squareup/text/PostalCodes;->isUsZipCode(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p1}, Lcom/squareup/text/PostalCodes;->isCaPostalCode(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method private final populateActionBar(Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddress$Event;",
            ">;)V"
        }
    .end annotation

    .line 61
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 58
    :cond_0
    new-instance v1, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 59
    new-instance v2, Lcom/squareup/util/ViewString$ResourceString;

    sget v3, Lcom/squareup/balance/squarecard/impl/R$string;->card_ordered_confirm_address_action_bar:I

    invoke-direct {v2, v3}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v2, Lcom/squareup/resources/TextModel;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 60
    sget-object v2, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    new-instance v3, Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator$populateActionBar$1;

    invoke-direct {v3, p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator$populateActionBar$1;-><init>(Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object p1

    .line 61
    invoke-virtual {p1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method

.method private final populateAddressLayout(Lcom/squareup/address/Address;)V
    .locals 2

    .line 78
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator;->billingAddress:Lcom/squareup/address/NohoAddressLayout;

    if-nez v0, :cond_0

    const-string v1, "billingAddress"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/squareup/address/NohoAddressLayout;->setAddress(Lcom/squareup/address/Address;Z)V

    .line 81
    new-instance p1, Landroid/os/Handler;

    invoke-direct {p1}, Landroid/os/Handler;-><init>()V

    new-instance v0, Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator$populateAddressLayout$1;

    invoke-direct {v0, p0}, Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator$populateAddressLayout$1;-><init>(Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator;)V

    check-cast v0, Ljava/lang/Runnable;

    invoke-virtual {p1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private final populateButtonLayout(Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddress$Event;",
            ">;)V"
        }
    .end annotation

    .line 69
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator;->activateButton:Lcom/squareup/noho/NohoButton;

    if-nez v0, :cond_0

    const-string v1, "activateButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/view/View;

    .line 117
    new-instance v1, Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator$populateButtonLayout$$inlined$onClickDebounced$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator$populateButtonLayout$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private final update(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddress$ScreenData;",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddress$Event;",
            ">;)V"
        }
    .end annotation

    .line 49
    iget-object v0, p2, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 50
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator;->populateButtonLayout(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 51
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator;->populateActionBar(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 52
    iget-object p2, p2, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast p2, Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddress$ScreenData;

    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddress$ScreenData;->getAddress()Lcom/squareup/address/Address;

    move-result-object p2

    invoke-direct {p0, p2}, Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator;->populateAddressLayout(Lcom/squareup/address/Address;)V

    .line 54
    new-instance p2, Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator$update$1;

    invoke-direct {p2, p0, v0}, Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator$update$1;-><init>(Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast p2, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, p2}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private final validateAddress(Lcom/squareup/workflow/legacy/WorkflowInput;Lkotlin/jvm/functions/Function0;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddress$Event;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 90
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator;->billingAddress:Lcom/squareup/address/NohoAddressLayout;

    const-string v1, "billingAddress"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/address/NohoAddressLayout;->getEmptyField()Landroid/widget/TextView;

    move-result-object v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator;->billingAddress:Lcom/squareup/address/NohoAddressLayout;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v0, Lcom/squareup/address/AddressLayout;

    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator;->isValidPostal(Lcom/squareup/address/AddressLayout;)Z

    move-result v0

    if-nez v0, :cond_2

    goto :goto_0

    .line 99
    :cond_2
    invoke-interface {p2}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    goto :goto_1

    .line 93
    :cond_3
    :goto_0
    new-instance p2, Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddress$Event$MissingInformation;

    .line 94
    sget v0, Lcom/squareup/balance/squarecard/impl/R$string;->card_ordered_missing_address_title:I

    .line 95
    sget v1, Lcom/squareup/balance/squarecard/impl/R$string;->card_ordered_missing_address_message:I

    .line 93
    invoke-direct {p2, v0, v1}, Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddress$Event$MissingInformation;-><init>(II)V

    .line 92
    invoke-interface {p1, p2}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    :goto_1
    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 3

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator;->bindViews(Landroid/view/View;)V

    .line 38
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator;->screens:Lio/reactivex/Observable;

    const-wide/16 v1, 0x1

    .line 40
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->take(J)Lio/reactivex/Observable;

    move-result-object v0

    .line 41
    new-instance v1, Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator$attach$1;-><init>(Lcom/squareup/balance/squarecard/ordered/SquareCardConfirmAddressCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "screens\n        // We on\u2026ribe { update(view, it) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method
