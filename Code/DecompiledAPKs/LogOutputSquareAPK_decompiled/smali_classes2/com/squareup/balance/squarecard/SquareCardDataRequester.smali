.class public interface abstract Lcom/squareup/balance/squarecard/SquareCardDataRequester;
.super Ljava/lang/Object;
.source "SquareCardDataRequester.kt"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0008f\u0018\u00002\u00020\u0001J\u000e\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H&J\u0016\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00032\u0006\u0010\u0007\u001a\u00020\u0006H&J\u0010\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0006H&\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/SquareCardDataRequester;",
        "Lmortar/Scoped;",
        "fetchCardStatus",
        "Lio/reactivex/Single;",
        "Lcom/squareup/balance/squarecard/CardStatus;",
        "singleNewCardData",
        "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
        "currentCardData",
        "toggleCard",
        "",
        "cardData",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract fetchCardStatus()Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/balance/squarecard/CardStatus;",
            ">;"
        }
    .end annotation
.end method

.method public abstract singleNewCardData(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
            ">;"
        }
    .end annotation
.end method

.method public abstract toggleCard(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V
.end method
