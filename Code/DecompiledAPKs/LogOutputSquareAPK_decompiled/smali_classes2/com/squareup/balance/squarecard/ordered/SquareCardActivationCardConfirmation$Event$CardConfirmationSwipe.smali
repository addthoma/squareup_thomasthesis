.class public final Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$Event$CardConfirmationSwipe;
.super Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$Event;
.source "SquareCardActivationCardConfirmationScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$Event;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CardConfirmationSwipe"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$Event$CardConfirmationSwipe;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$Event;",
        "card",
        "Lcom/squareup/Card;",
        "(Lcom/squareup/Card;)V",
        "getCard",
        "()Lcom/squareup/Card;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final card:Lcom/squareup/Card;


# direct methods
.method public constructor <init>(Lcom/squareup/Card;)V
    .locals 1

    const-string v0, "card"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 26
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$Event;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$Event$CardConfirmationSwipe;->card:Lcom/squareup/Card;

    return-void
.end method


# virtual methods
.method public final getCard()Lcom/squareup/Card;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$Event$CardConfirmationSwipe;->card:Lcom/squareup/Card;

    return-object v0
.end method
