.class public final Lcom/squareup/balance/squarecard/cancel/success/CancelSquareCardSuccessWorkflow;
.super Lcom/squareup/workflow/StatelessWorkflow;
.source "CancelSquareCardSuccessWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatelessWorkflow<",
        "Lcom/squareup/balance/squarecard/cancel/DeactivationReason;",
        "Lcom/squareup/balance/squarecard/cancel/success/CancelSquareCardSuccessOutput;",
        "Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0000\n\u0002\u0010\u0008\n\u0000\u0018\u00002\u0018\u0012\u0008\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0006J(\u0010\u0007\u001a\u00020\u00052\n\u0010\u0008\u001a\u00060\u0002j\u0002`\u00032\u0012\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u00040\nH\u0016J\u000c\u0010\u000c\u001a\u00020\r*\u00020\u0002H\u0002\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/cancel/success/CancelSquareCardSuccessWorkflow;",
        "Lcom/squareup/workflow/StatelessWorkflow;",
        "Lcom/squareup/balance/squarecard/cancel/DeactivationReason;",
        "Lcom/squareup/balance/squarecard/cancel/success/CancelSquareCardSuccessProps;",
        "Lcom/squareup/balance/squarecard/cancel/success/CancelSquareCardSuccessOutput;",
        "Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;",
        "()V",
        "render",
        "props",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "",
        "toActionBarText",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 36
    invoke-direct {p0}, Lcom/squareup/workflow/StatelessWorkflow;-><init>()V

    return-void
.end method

.method private final toActionBarText(Lcom/squareup/balance/squarecard/cancel/DeactivationReason;)I
    .locals 1

    .line 66
    sget-object v0, Lcom/squareup/balance/squarecard/cancel/DeactivationReason$Lost;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/DeactivationReason$Lost;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget p1, Lcom/squareup/balance/squarecard/impl/R$string;->cancel_lost_card_action_bar:I

    goto :goto_0

    .line 67
    :cond_0
    sget-object v0, Lcom/squareup/balance/squarecard/cancel/DeactivationReason$Stolen;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/DeactivationReason$Stolen;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget p1, Lcom/squareup/balance/squarecard/impl/R$string;->cancel_stolen_card_action_bar:I

    goto :goto_0

    .line 68
    :cond_1
    sget-object v0, Lcom/squareup/balance/squarecard/cancel/DeactivationReason$NeverReceived;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/DeactivationReason$NeverReceived;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget p1, Lcom/squareup/balance/squarecard/impl/R$string;->cancel_never_received_card_action_bar:I

    goto :goto_0

    .line 69
    :cond_2
    sget-object v0, Lcom/squareup/balance/squarecard/cancel/DeactivationReason$Generic;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/DeactivationReason$Generic;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    sget p1, Lcom/squareup/balance/squarecard/impl/R$string;->cancel_card_action_bar:I

    :goto_0
    return p1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method


# virtual methods
.method public render(Lcom/squareup/balance/squarecard/cancel/DeactivationReason;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;
    .locals 12

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    sget-object v0, Lcom/squareup/balance/squarecard/cancel/success/CancelSquareCardSuccessWorkflow$render$sink$1;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/success/CancelSquareCardSuccessWorkflow$render$sink$1;

    check-cast v0, Lkotlin/jvm/functions/Function2;

    invoke-static {p2, v0}, Lcom/squareup/workflow/RenderContextKt;->makeEventSink(Lcom/squareup/workflow/RenderContext;Lkotlin/jvm/functions/Function2;)Lcom/squareup/workflow/Sink;

    move-result-object p2

    .line 47
    new-instance v0, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;

    .line 48
    new-instance v11, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Succeeded;

    .line 49
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/cancel/success/CancelSquareCardSuccessWorkflow;->toActionBarText(Lcom/squareup/balance/squarecard/cancel/DeactivationReason;)I

    move-result v2

    .line 50
    sget v3, Lcom/squareup/balance/squarecard/impl/R$string;->canceled_card_success_title:I

    .line 51
    sget v4, Lcom/squareup/balance/squarecard/impl/R$string;->canceled_card_success_message:I

    .line 52
    sget v5, Lcom/squareup/balance/squarecard/impl/R$string;->canceled_card_success_order_replacement_button_text:I

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x70

    const/4 v10, 0x0

    move-object v1, v11

    .line 48
    invoke-direct/range {v1 .. v10}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Succeeded;-><init>(IIIIIILjava/util/Map;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v11, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData;

    .line 54
    new-instance p1, Lcom/squareup/balance/squarecard/cancel/success/CancelSquareCardSuccessWorkflow$render$1;

    invoke-direct {p1, p2}, Lcom/squareup/balance/squarecard/cancel/success/CancelSquareCardSuccessWorkflow$render$1;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    .line 47
    invoke-direct {v0, v11, p1}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;-><init>(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData;Lkotlin/jvm/functions/Function1;)V

    return-object v0
.end method

.method public bridge synthetic render(Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 35
    check-cast p1, Lcom/squareup/balance/squarecard/cancel/DeactivationReason;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/squarecard/cancel/success/CancelSquareCardSuccessWorkflow;->render(Lcom/squareup/balance/squarecard/cancel/DeactivationReason;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;

    move-result-object p1

    return-object p1
.end method
