.class public abstract Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$WithCard;
.super Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow;
.source "ManageSquareCardState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "WithCard"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$WithCard$CardOrdered;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<O:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow<",
        "TO;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u0000*\u0008\u0008\u0001\u0010\u0001*\u00020\u00022\u0008\u0012\u0004\u0012\u0002H\u00010\u0003:\u0001\u000bB\u000f\u0008\u0002\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0008\u0010\t\u001a\u00020\nH\u0016R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008\u0082\u0001\u0001\u000c\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$WithCard;",
        "O",
        "",
        "Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow;",
        "card",
        "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
        "(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V",
        "getCard",
        "()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
        "toSnapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "CardOrdered",
        "Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$WithCard$CardOrdered;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;


# direct methods
.method private constructor <init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V
    .locals 1

    const/4 v0, 0x0

    .line 105
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$WithCard;->card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 103
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$WithCard;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V

    return-void
.end method


# virtual methods
.method public final getCard()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;
    .locals 1

    .line 104
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$WithCard;->card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    return-object v0
.end method

.method public toSnapshot()Lcom/squareup/workflow/Snapshot;
    .locals 2

    .line 106
    sget-object v0, Lcom/squareup/workflow/Snapshot;->Companion:Lcom/squareup/workflow/Snapshot$Companion;

    new-instance v1, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$WithCard$toSnapshot$1;

    invoke-direct {v1, p0}, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$WithCard$toSnapshot$1;-><init>(Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$WithCard;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/Snapshot$Companion;->write(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Snapshot;

    move-result-object v0

    return-object v0
.end method
