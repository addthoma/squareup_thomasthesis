.class final Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressCoordinator$attach$2;
.super Lkotlin/jvm/internal/Lambda;
.source "DisplayAddressCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressScreen;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "screen",
        "Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressScreen;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $view:Landroid/view/View;

.field final synthetic this$0:Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressCoordinator;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressCoordinator$attach$2;->this$0:Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressCoordinator;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressCoordinator$attach$2;->$view:Landroid/view/View;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 19
    check-cast p1, Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressScreen;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressCoordinator$attach$2;->invoke(Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressScreen;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressScreen;)V
    .locals 2

    .line 33
    iget-object v0, p0, Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressCoordinator$attach$2;->this$0:Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressCoordinator;

    const-string v1, "screen"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressCoordinator$attach$2;->$view:Landroid/view/View;

    invoke-static {v0, p1, v1}, Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressCoordinator;->access$updateUi(Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressCoordinator;Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressScreen;Landroid/view/View;)V

    return-void
.end method
