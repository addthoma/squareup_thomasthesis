.class final Lcom/squareup/checkoutfe/CompleteCheckoutResponse$ProtoAdapter_CompleteCheckoutResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "CompleteCheckoutResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutfe/CompleteCheckoutResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_CompleteCheckoutResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/checkoutfe/CompleteCheckoutResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 117
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/checkoutfe/CompleteCheckoutResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/checkoutfe/CompleteCheckoutResponse;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 136
    new-instance v0, Lcom/squareup/checkoutfe/CompleteCheckoutResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/checkoutfe/CompleteCheckoutResponse$Builder;-><init>()V

    .line 137
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 138
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 143
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 141
    :cond_0
    sget-object v3, Lcom/squareup/orders/model/Order;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/orders/model/Order;

    invoke-virtual {v0, v3}, Lcom/squareup/checkoutfe/CompleteCheckoutResponse$Builder;->order(Lcom/squareup/orders/model/Order;)Lcom/squareup/checkoutfe/CompleteCheckoutResponse$Builder;

    goto :goto_0

    .line 140
    :cond_1
    iget-object v3, v0, Lcom/squareup/checkoutfe/CompleteCheckoutResponse$Builder;->errors:Ljava/util/List;

    sget-object v4, Lcom/squareup/checkoutfe/LocalizedError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 147
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/checkoutfe/CompleteCheckoutResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 148
    invoke-virtual {v0}, Lcom/squareup/checkoutfe/CompleteCheckoutResponse$Builder;->build()Lcom/squareup/checkoutfe/CompleteCheckoutResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 115
    invoke-virtual {p0, p1}, Lcom/squareup/checkoutfe/CompleteCheckoutResponse$ProtoAdapter_CompleteCheckoutResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/checkoutfe/CompleteCheckoutResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/checkoutfe/CompleteCheckoutResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 129
    sget-object v0, Lcom/squareup/checkoutfe/LocalizedError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/checkoutfe/CompleteCheckoutResponse;->errors:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 130
    sget-object v0, Lcom/squareup/orders/model/Order;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/checkoutfe/CompleteCheckoutResponse;->order:Lcom/squareup/orders/model/Order;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 131
    invoke-virtual {p2}, Lcom/squareup/checkoutfe/CompleteCheckoutResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 115
    check-cast p2, Lcom/squareup/checkoutfe/CompleteCheckoutResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/checkoutfe/CompleteCheckoutResponse$ProtoAdapter_CompleteCheckoutResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/checkoutfe/CompleteCheckoutResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/checkoutfe/CompleteCheckoutResponse;)I
    .locals 4

    .line 122
    sget-object v0, Lcom/squareup/checkoutfe/LocalizedError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/checkoutfe/CompleteCheckoutResponse;->errors:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/orders/model/Order;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/checkoutfe/CompleteCheckoutResponse;->order:Lcom/squareup/orders/model/Order;

    const/4 v3, 0x2

    .line 123
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 124
    invoke-virtual {p1}, Lcom/squareup/checkoutfe/CompleteCheckoutResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 115
    check-cast p1, Lcom/squareup/checkoutfe/CompleteCheckoutResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutfe/CompleteCheckoutResponse$ProtoAdapter_CompleteCheckoutResponse;->encodedSize(Lcom/squareup/checkoutfe/CompleteCheckoutResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/checkoutfe/CompleteCheckoutResponse;)Lcom/squareup/checkoutfe/CompleteCheckoutResponse;
    .locals 2

    .line 153
    invoke-virtual {p1}, Lcom/squareup/checkoutfe/CompleteCheckoutResponse;->newBuilder()Lcom/squareup/checkoutfe/CompleteCheckoutResponse$Builder;

    move-result-object p1

    .line 154
    iget-object v0, p1, Lcom/squareup/checkoutfe/CompleteCheckoutResponse$Builder;->errors:Ljava/util/List;

    sget-object v1, Lcom/squareup/checkoutfe/LocalizedError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 155
    iget-object v0, p1, Lcom/squareup/checkoutfe/CompleteCheckoutResponse$Builder;->order:Lcom/squareup/orders/model/Order;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/orders/model/Order;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/checkoutfe/CompleteCheckoutResponse$Builder;->order:Lcom/squareup/orders/model/Order;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orders/model/Order;

    iput-object v0, p1, Lcom/squareup/checkoutfe/CompleteCheckoutResponse$Builder;->order:Lcom/squareup/orders/model/Order;

    .line 156
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/checkoutfe/CompleteCheckoutResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 157
    invoke-virtual {p1}, Lcom/squareup/checkoutfe/CompleteCheckoutResponse$Builder;->build()Lcom/squareup/checkoutfe/CompleteCheckoutResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 115
    check-cast p1, Lcom/squareup/checkoutfe/CompleteCheckoutResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutfe/CompleteCheckoutResponse$ProtoAdapter_CompleteCheckoutResponse;->redact(Lcom/squareup/checkoutfe/CompleteCheckoutResponse;)Lcom/squareup/checkoutfe/CompleteCheckoutResponse;

    move-result-object p1

    return-object p1
.end method
