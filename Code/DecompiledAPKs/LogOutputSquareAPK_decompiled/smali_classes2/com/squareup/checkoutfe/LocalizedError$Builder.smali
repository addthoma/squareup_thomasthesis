.class public final Lcom/squareup/checkoutfe/LocalizedError$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "LocalizedError.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutfe/LocalizedError;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/checkoutfe/LocalizedError;",
        "Lcom/squareup/checkoutfe/LocalizedError$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public error:Lcom/squareup/protos/connect/v2/resources/Error;

.field public localized_description:Ljava/lang/String;

.field public localized_title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 116
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/checkoutfe/LocalizedError;
    .locals 5

    .line 136
    new-instance v0, Lcom/squareup/checkoutfe/LocalizedError;

    iget-object v1, p0, Lcom/squareup/checkoutfe/LocalizedError$Builder;->error:Lcom/squareup/protos/connect/v2/resources/Error;

    iget-object v2, p0, Lcom/squareup/checkoutfe/LocalizedError$Builder;->localized_title:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/checkoutfe/LocalizedError$Builder;->localized_description:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/checkoutfe/LocalizedError;-><init>(Lcom/squareup/protos/connect/v2/resources/Error;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 109
    invoke-virtual {p0}, Lcom/squareup/checkoutfe/LocalizedError$Builder;->build()Lcom/squareup/checkoutfe/LocalizedError;

    move-result-object v0

    return-object v0
.end method

.method public error(Lcom/squareup/protos/connect/v2/resources/Error;)Lcom/squareup/checkoutfe/LocalizedError$Builder;
    .locals 0

    .line 120
    iput-object p1, p0, Lcom/squareup/checkoutfe/LocalizedError$Builder;->error:Lcom/squareup/protos/connect/v2/resources/Error;

    return-object p0
.end method

.method public localized_description(Ljava/lang/String;)Lcom/squareup/checkoutfe/LocalizedError$Builder;
    .locals 0

    .line 130
    iput-object p1, p0, Lcom/squareup/checkoutfe/LocalizedError$Builder;->localized_description:Ljava/lang/String;

    return-object p0
.end method

.method public localized_title(Ljava/lang/String;)Lcom/squareup/checkoutfe/LocalizedError$Builder;
    .locals 0

    .line 125
    iput-object p1, p0, Lcom/squareup/checkoutfe/LocalizedError$Builder;->localized_title:Ljava/lang/String;

    return-object p0
.end method
