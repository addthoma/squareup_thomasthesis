.class final Lcom/squareup/checkoutfe/CheckoutClientDetails$ProtoAdapter_CheckoutClientDetails;
.super Lcom/squareup/wire/ProtoAdapter;
.source "CheckoutClientDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutfe/CheckoutClientDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_CheckoutClientDetails"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/checkoutfe/CheckoutClientDetails;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 269
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/checkoutfe/CheckoutClientDetails;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/checkoutfe/CheckoutClientDetails;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 286
    new-instance v0, Lcom/squareup/checkoutfe/CheckoutClientDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/checkoutfe/CheckoutClientDetails$Builder;-><init>()V

    .line 287
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 288
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 292
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 290
    :cond_0
    sget-object v3, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;

    invoke-virtual {v0, v3}, Lcom/squareup/checkoutfe/CheckoutClientDetails$Builder;->checkout_payer(Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;)Lcom/squareup/checkoutfe/CheckoutClientDetails$Builder;

    goto :goto_0

    .line 296
    :cond_1
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/checkoutfe/CheckoutClientDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 297
    invoke-virtual {v0}, Lcom/squareup/checkoutfe/CheckoutClientDetails$Builder;->build()Lcom/squareup/checkoutfe/CheckoutClientDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 267
    invoke-virtual {p0, p1}, Lcom/squareup/checkoutfe/CheckoutClientDetails$ProtoAdapter_CheckoutClientDetails;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/checkoutfe/CheckoutClientDetails;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/checkoutfe/CheckoutClientDetails;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 280
    sget-object v0, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/checkoutfe/CheckoutClientDetails;->checkout_payer:Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 281
    invoke-virtual {p2}, Lcom/squareup/checkoutfe/CheckoutClientDetails;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 267
    check-cast p2, Lcom/squareup/checkoutfe/CheckoutClientDetails;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/checkoutfe/CheckoutClientDetails$ProtoAdapter_CheckoutClientDetails;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/checkoutfe/CheckoutClientDetails;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/checkoutfe/CheckoutClientDetails;)I
    .locals 3

    .line 274
    sget-object v0, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/checkoutfe/CheckoutClientDetails;->checkout_payer:Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 275
    invoke-virtual {p1}, Lcom/squareup/checkoutfe/CheckoutClientDetails;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 267
    check-cast p1, Lcom/squareup/checkoutfe/CheckoutClientDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutfe/CheckoutClientDetails$ProtoAdapter_CheckoutClientDetails;->encodedSize(Lcom/squareup/checkoutfe/CheckoutClientDetails;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/checkoutfe/CheckoutClientDetails;)Lcom/squareup/checkoutfe/CheckoutClientDetails;
    .locals 2

    .line 302
    invoke-virtual {p1}, Lcom/squareup/checkoutfe/CheckoutClientDetails;->newBuilder()Lcom/squareup/checkoutfe/CheckoutClientDetails$Builder;

    move-result-object p1

    .line 303
    iget-object v0, p1, Lcom/squareup/checkoutfe/CheckoutClientDetails$Builder;->checkout_payer:Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/checkoutfe/CheckoutClientDetails$Builder;->checkout_payer:Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;

    iput-object v0, p1, Lcom/squareup/checkoutfe/CheckoutClientDetails$Builder;->checkout_payer:Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;

    .line 304
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/checkoutfe/CheckoutClientDetails$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 305
    invoke-virtual {p1}, Lcom/squareup/checkoutfe/CheckoutClientDetails$Builder;->build()Lcom/squareup/checkoutfe/CheckoutClientDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 267
    check-cast p1, Lcom/squareup/checkoutfe/CheckoutClientDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutfe/CheckoutClientDetails$ProtoAdapter_CheckoutClientDetails;->redact(Lcom/squareup/checkoutfe/CheckoutClientDetails;)Lcom/squareup/checkoutfe/CheckoutClientDetails;

    move-result-object p1

    return-object p1
.end method
