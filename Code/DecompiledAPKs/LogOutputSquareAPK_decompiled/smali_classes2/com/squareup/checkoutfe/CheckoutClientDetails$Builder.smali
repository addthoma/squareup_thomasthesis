.class public final Lcom/squareup/checkoutfe/CheckoutClientDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CheckoutClientDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutfe/CheckoutClientDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/checkoutfe/CheckoutClientDetails;",
        "Lcom/squareup/checkoutfe/CheckoutClientDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public checkout_payer:Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 86
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/checkoutfe/CheckoutClientDetails;
    .locals 3

    .line 96
    new-instance v0, Lcom/squareup/checkoutfe/CheckoutClientDetails;

    iget-object v1, p0, Lcom/squareup/checkoutfe/CheckoutClientDetails$Builder;->checkout_payer:Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/checkoutfe/CheckoutClientDetails;-><init>(Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 83
    invoke-virtual {p0}, Lcom/squareup/checkoutfe/CheckoutClientDetails$Builder;->build()Lcom/squareup/checkoutfe/CheckoutClientDetails;

    move-result-object v0

    return-object v0
.end method

.method public checkout_payer(Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;)Lcom/squareup/checkoutfe/CheckoutClientDetails$Builder;
    .locals 0

    .line 90
    iput-object p1, p0, Lcom/squareup/checkoutfe/CheckoutClientDetails$Builder;->checkout_payer:Lcom/squareup/checkoutfe/CheckoutClientDetails$CheckoutPayer;

    return-object p0
.end method
