.class Lcom/squareup/calc/AdjustedItem$2;
.super Ljava/util/EnumMap;
.source "AdjustedItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/calc/AdjustedItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/EnumMap<",
        "Lcom/squareup/calc/constants/CalculationPhase;",
        "Ljava/math/BigDecimal;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/calc/AdjustedItem;


# direct methods
.method constructor <init>(Lcom/squareup/calc/AdjustedItem;Ljava/lang/Class;)V
    .locals 0

    .line 68
    iput-object p1, p0, Lcom/squareup/calc/AdjustedItem$2;->this$0:Lcom/squareup/calc/AdjustedItem;

    invoke-direct {p0, p2}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 68
    invoke-virtual {p0, p1}, Lcom/squareup/calc/AdjustedItem$2;->get(Ljava/lang/Object;)Ljava/math/BigDecimal;

    move-result-object p1

    return-object p1
.end method

.method public get(Ljava/lang/Object;)Ljava/math/BigDecimal;
    .locals 0

    .line 70
    invoke-super {p0, p1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/math/BigDecimal;

    if-nez p1, :cond_0

    .line 71
    sget-object p1, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    :cond_0
    return-object p1
.end method
