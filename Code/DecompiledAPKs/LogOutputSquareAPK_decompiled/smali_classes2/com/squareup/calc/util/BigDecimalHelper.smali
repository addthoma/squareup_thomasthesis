.class public Lcom/squareup/calc/util/BigDecimalHelper;
.super Ljava/lang/Object;
.source "BigDecimalHelper.java"


# static fields
.field static final DIV_PRECISION:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    sget-object v0, Ljava/math/MathContext;->DECIMAL128:Ljava/math/MathContext;

    invoke-virtual {v0}, Ljava/math/MathContext;->getPrecision()I

    move-result v0

    sput v0, Lcom/squareup/calc/util/BigDecimalHelper;->DIV_PRECISION:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static divide(Ljava/math/BigDecimal;Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Ljava/math/BigDecimal;
    .locals 2

    .line 55
    new-instance v0, Ljava/math/MathContext;

    sget v1, Lcom/squareup/calc/util/BigDecimalHelper;->DIV_PRECISION:I

    invoke-direct {v0, v1, p2}, Ljava/math/MathContext;-><init>(ILjava/math/RoundingMode;)V

    invoke-virtual {p0, p1, v0}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;Ljava/math/MathContext;)Ljava/math/BigDecimal;

    move-result-object p0

    return-object p0
.end method

.method public static multiply(JLjava/math/BigDecimal;Ljava/math/RoundingMode;)J
    .locals 0

    .line 43
    invoke-static {p0, p1}, Lcom/squareup/calc/util/BigDecimalHelper;->newBigDecimal(J)Ljava/math/BigDecimal;

    move-result-object p0

    invoke-virtual {p0, p2}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object p0

    .line 44
    invoke-static {p0, p3}, Lcom/squareup/calc/util/BigDecimalHelper;->round(Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object p0

    .line 46
    invoke-virtual {p0}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide p0

    return-wide p0
.end method

.method public static newBigDecimal(J)Ljava/math/BigDecimal;
    .locals 1

    .line 27
    new-instance v0, Ljava/math/BigDecimal;

    invoke-direct {v0, p0, p1}, Ljava/math/BigDecimal;-><init>(J)V

    return-object v0
.end method

.method public static newBigDecimal(Ljava/lang/String;)Ljava/math/BigDecimal;
    .locals 1

    .line 31
    new-instance v0, Ljava/math/BigDecimal;

    invoke-direct {v0, p0}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static round(Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Ljava/math/BigDecimal;
    .locals 1

    const/4 v0, 0x0

    .line 87
    invoke-virtual {p0, v0, p1}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object p0

    return-object p0
.end method
