.class public Lcom/squareup/calc/AdjustedItem;
.super Ljava/lang/Object;
.source "AdjustedItem.java"


# instance fields
.field private final collectedPerAdjustment:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/math/BigDecimal;",
            ">;"
        }
    .end annotation
.end field

.field private final collectedPerPhase:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/calc/constants/CalculationPhase;",
            "Ljava/math/BigDecimal;",
            ">;"
        }
    .end annotation
.end field

.field private final nonTaxBasisModifyingAdjustmentsCollectedPerPhase:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/calc/constants/CalculationPhase;",
            "Ljava/math/BigDecimal;",
            ">;"
        }
    .end annotation
.end field

.field final rawItem:Lcom/squareup/calc/order/Item;

.field private subtotal:Ljava/math/BigDecimal;

.field private surChargePhase_preFeeAmount:Ljava/math/BigDecimal;


# direct methods
.method constructor <init>(Lcom/squareup/calc/order/Item;)V
    .locals 2

    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/calc/AdjustedItem;->collectedPerAdjustment:Ljava/util/Map;

    .line 54
    new-instance v0, Lcom/squareup/calc/AdjustedItem$1;

    const-class v1, Lcom/squareup/calc/constants/CalculationPhase;

    invoke-direct {v0, p0, v1}, Lcom/squareup/calc/AdjustedItem$1;-><init>(Lcom/squareup/calc/AdjustedItem;Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/squareup/calc/AdjustedItem;->collectedPerPhase:Ljava/util/Map;

    .line 67
    new-instance v0, Lcom/squareup/calc/AdjustedItem$2;

    const-class v1, Lcom/squareup/calc/constants/CalculationPhase;

    invoke-direct {v0, p0, v1}, Lcom/squareup/calc/AdjustedItem$2;-><init>(Lcom/squareup/calc/AdjustedItem;Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/squareup/calc/AdjustedItem;->nonTaxBasisModifyingAdjustmentsCollectedPerPhase:Ljava/util/Map;

    .line 99
    iput-object p1, p0, Lcom/squareup/calc/AdjustedItem;->rawItem:Lcom/squareup/calc/order/Item;

    return-void
.end method

.method private getNonTaxBasisModifyingDiscountAdjustment()Ljava/math/BigDecimal;
    .locals 3

    .line 179
    iget-object v0, p0, Lcom/squareup/calc/AdjustedItem;->nonTaxBasisModifyingAdjustmentsCollectedPerPhase:Ljava/util/Map;

    sget-object v1, Lcom/squareup/calc/constants/CalculationPhase;->DISCOUNT_PERCENTAGE_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/math/BigDecimal;

    iget-object v1, p0, Lcom/squareup/calc/AdjustedItem;->nonTaxBasisModifyingAdjustmentsCollectedPerPhase:Ljava/util/Map;

    sget-object v2, Lcom/squareup/calc/constants/CalculationPhase;->DISCOUNT_AMOUNT_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

    .line 180
    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/math/BigDecimal;

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method

.method private getOrderAdjustment(Ljava/lang/String;)Lcom/squareup/calc/order/Adjustment;
    .locals 1

    .line 328
    iget-object v0, p0, Lcom/squareup/calc/AdjustedItem;->rawItem:Lcom/squareup/calc/order/Item;

    invoke-interface {v0}, Lcom/squareup/calc/order/Item;->appliedTaxes()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 329
    iget-object v0, p0, Lcom/squareup/calc/AdjustedItem;->rawItem:Lcom/squareup/calc/order/Item;

    invoke-interface {v0}, Lcom/squareup/calc/order/Item;->appliedTaxes()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/calc/order/Adjustment;

    return-object p1

    .line 331
    :cond_0
    iget-object v0, p0, Lcom/squareup/calc/AdjustedItem;->rawItem:Lcom/squareup/calc/order/Item;

    invoke-interface {v0}, Lcom/squareup/calc/order/Item;->appliedDiscounts()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/calc/order/Adjustment;

    return-object p1
.end method

.method private getPreFeeAmount()Ljava/math/BigDecimal;
    .locals 8

    .line 272
    sget-object v0, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    .line 273
    sget-object v1, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    .line 274
    sget-object v2, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    .line 275
    sget-object v3, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    .line 279
    iget-object v4, p0, Lcom/squareup/calc/AdjustedItem;->rawItem:Lcom/squareup/calc/order/Item;

    invoke-interface {v4}, Lcom/squareup/calc/order/Item;->appliedTaxes()Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/calc/order/Adjustment;

    .line 280
    invoke-interface {v5}, Lcom/squareup/calc/order/Adjustment;->inclusionType()Lcom/squareup/calc/constants/InclusionType;

    move-result-object v6

    sget-object v7, Lcom/squareup/calc/constants/InclusionType;->INCLUSIVE:Lcom/squareup/calc/constants/InclusionType;

    if-ne v6, v7, :cond_1

    .line 281
    invoke-interface {v5}, Lcom/squareup/calc/order/Adjustment;->phase()Lcom/squareup/calc/constants/CalculationPhase;

    move-result-object v6

    sget-object v7, Lcom/squareup/calc/constants/CalculationPhase;->FEE_SUBTOTAL_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

    if-ne v6, v7, :cond_0

    .line 282
    invoke-interface {v5}, Lcom/squareup/calc/order/Adjustment;->rate()Ljava/math/BigDecimal;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    goto :goto_0

    .line 284
    :cond_0
    invoke-interface {v5}, Lcom/squareup/calc/order/Adjustment;->phase()Lcom/squareup/calc/constants/CalculationPhase;

    move-result-object v6

    sget-object v7, Lcom/squareup/calc/constants/CalculationPhase;->FEE_TOTAL_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

    if-ne v6, v7, :cond_3

    .line 285
    invoke-interface {v5}, Lcom/squareup/calc/order/Adjustment;->rate()Ljava/math/BigDecimal;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v1

    goto :goto_0

    .line 288
    :cond_1
    invoke-interface {v5}, Lcom/squareup/calc/order/Adjustment;->inclusionType()Lcom/squareup/calc/constants/InclusionType;

    move-result-object v6

    sget-object v7, Lcom/squareup/calc/constants/InclusionType;->ADDITIVE:Lcom/squareup/calc/constants/InclusionType;

    if-ne v6, v7, :cond_3

    .line 289
    invoke-interface {v5}, Lcom/squareup/calc/order/Adjustment;->phase()Lcom/squareup/calc/constants/CalculationPhase;

    move-result-object v6

    sget-object v7, Lcom/squareup/calc/constants/CalculationPhase;->FEE_SUBTOTAL_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

    if-ne v6, v7, :cond_2

    .line 290
    invoke-interface {v5}, Lcom/squareup/calc/order/Adjustment;->rate()Ljava/math/BigDecimal;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v2

    goto :goto_0

    .line 292
    :cond_2
    invoke-interface {v5}, Lcom/squareup/calc/order/Adjustment;->phase()Lcom/squareup/calc/constants/CalculationPhase;

    move-result-object v6

    sget-object v7, Lcom/squareup/calc/constants/CalculationPhase;->FEE_TOTAL_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

    if-ne v6, v7, :cond_3

    .line 293
    invoke-interface {v5}, Lcom/squareup/calc/order/Adjustment;->rate()Ljava/math/BigDecimal;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v3

    goto :goto_0

    .line 299
    :cond_3
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected phase ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 300
    invoke-interface {v5}, Lcom/squareup/calc/order/Adjustment;->phase()Lcom/squareup/calc/constants/CalculationPhase;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, ") or inclusion type ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 302
    invoke-interface {v5}, Lcom/squareup/calc/order/Adjustment;->inclusionType()Lcom/squareup/calc/constants/InclusionType;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 306
    :cond_4
    sget-object v3, Ljava/math/BigDecimal;->ONE:Ljava/math/BigDecimal;

    invoke-virtual {v3, v0}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v2

    .line 309
    sget-object v3, Ljava/math/BigDecimal;->ONE:Ljava/math/BigDecimal;

    invoke-virtual {v3, v0}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 310
    invoke-virtual {v1, v2}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 313
    invoke-virtual {p0}, Lcom/squareup/calc/AdjustedItem;->getSubtotal()Ljava/math/BigDecimal;

    move-result-object v1

    invoke-direct {p0}, Lcom/squareup/calc/AdjustedItem;->getNonTaxBasisModifyingDiscountAdjustment()Ljava/math/BigDecimal;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v1

    .line 315
    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v1, v0, v2}, Lcom/squareup/calc/util/BigDecimalHelper;->divide(Ljava/math/BigDecimal;Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method

.method private getTotalDiscountAdjustment()Ljava/math/BigDecimal;
    .locals 3

    .line 323
    iget-object v0, p0, Lcom/squareup/calc/AdjustedItem;->collectedPerPhase:Ljava/util/Map;

    sget-object v1, Lcom/squareup/calc/constants/CalculationPhase;->DISCOUNT_PERCENTAGE_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/math/BigDecimal;

    iget-object v1, p0, Lcom/squareup/calc/AdjustedItem;->collectedPerPhase:Ljava/util/Map;

    sget-object v2, Lcom/squareup/calc/constants/CalculationPhase;->DISCOUNT_AMOUNT_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

    .line 324
    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/math/BigDecimal;

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAdjustedTotal()J
    .locals 5

    .line 191
    invoke-virtual {p0}, Lcom/squareup/calc/AdjustedItem;->getSubtotal()Ljava/math/BigDecimal;

    move-result-object v0

    .line 192
    iget-object v1, p0, Lcom/squareup/calc/AdjustedItem;->collectedPerAdjustment:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 193
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/squareup/calc/AdjustedItem;->getOrderAdjustment(Ljava/lang/String;)Lcom/squareup/calc/order/Adjustment;

    move-result-object v3

    .line 194
    invoke-interface {v3}, Lcom/squareup/calc/order/Adjustment;->phase()Lcom/squareup/calc/constants/CalculationPhase;

    move-result-object v4

    invoke-virtual {v4}, Lcom/squareup/calc/constants/CalculationPhase;->isFee()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Lcom/squareup/calc/order/Adjustment;->inclusionType()Lcom/squareup/calc/constants/InclusionType;

    move-result-object v3

    sget-object v4, Lcom/squareup/calc/constants/InclusionType;->ADDITIVE:Lcom/squareup/calc/constants/InclusionType;

    if-ne v3, v4, :cond_0

    .line 195
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/math/BigDecimal;

    invoke-virtual {v0, v2}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    goto :goto_0

    .line 198
    :cond_1
    invoke-virtual {v0}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getBaseAmount()J
    .locals 2

    .line 156
    iget-object v0, p0, Lcom/squareup/calc/AdjustedItem;->rawItem:Lcom/squareup/calc/order/Item;

    invoke-interface {v0}, Lcom/squareup/calc/order/Item;->baseAmount()J

    move-result-wide v0

    return-wide v0
.end method

.method getBasisForPhase(Lcom/squareup/calc/constants/CalculationPhase;)Ljava/math/BigDecimal;
    .locals 3

    if-eqz p1, :cond_0

    .line 231
    sget-object v0, Lcom/squareup/calc/AdjustedItem$3;->$SwitchMap$com$squareup$calc$constants$CalculationPhase:[I

    invoke-virtual {p1}, Lcom/squareup/calc/constants/CalculationPhase;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 248
    :pswitch_0
    sget-object p1, Lcom/squareup/calc/constants/CalculationPhase;->FEE_TOTAL_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

    invoke-virtual {p0, p1}, Lcom/squareup/calc/AdjustedItem;->getBasisForPhase(Lcom/squareup/calc/constants/CalculationPhase;)Ljava/math/BigDecimal;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/calc/AdjustedItem;->collectedPerPhase:Ljava/util/Map;

    sget-object v1, Lcom/squareup/calc/constants/CalculationPhase;->FEE_TOTAL_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/math/BigDecimal;

    invoke-virtual {p1, v0}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object p1

    return-object p1

    .line 245
    :pswitch_1
    sget-object p1, Lcom/squareup/calc/constants/CalculationPhase;->FEE_SUBTOTAL_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

    invoke-virtual {p0, p1}, Lcom/squareup/calc/AdjustedItem;->getBasisForPhase(Lcom/squareup/calc/constants/CalculationPhase;)Ljava/math/BigDecimal;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/calc/AdjustedItem;->collectedPerPhase:Ljava/util/Map;

    sget-object v1, Lcom/squareup/calc/constants/CalculationPhase;->FEE_SUBTOTAL_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

    .line 246
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/math/BigDecimal;

    invoke-virtual {p1, v0}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object p1

    return-object p1

    .line 243
    :pswitch_2
    invoke-direct {p0}, Lcom/squareup/calc/AdjustedItem;->getPreFeeAmount()Ljava/math/BigDecimal;

    move-result-object p1

    return-object p1

    .line 237
    :pswitch_3
    sget-object p1, Lcom/squareup/calc/constants/CalculationPhase;->DISCOUNT_PERCENTAGE_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

    invoke-virtual {p0, p1}, Lcom/squareup/calc/AdjustedItem;->getBasisForPhase(Lcom/squareup/calc/constants/CalculationPhase;)Ljava/math/BigDecimal;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/calc/AdjustedItem;->collectedPerPhase:Ljava/util/Map;

    sget-object v1, Lcom/squareup/calc/constants/CalculationPhase;->DISCOUNT_AMOUNT_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

    .line 239
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/math/BigDecimal;

    invoke-virtual {p1, v0}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object p1

    return-object p1

    .line 233
    :pswitch_4
    new-instance p1, Ljava/math/BigDecimal;

    iget-object v0, p0, Lcom/squareup/calc/AdjustedItem;->rawItem:Lcom/squareup/calc/order/Item;

    invoke-interface {v0}, Lcom/squareup/calc/order/Item;->baseAmount()J

    move-result-wide v0

    invoke-direct {p1, v0, v1}, Ljava/math/BigDecimal;-><init>(J)V

    iget-object v0, p0, Lcom/squareup/calc/AdjustedItem;->collectedPerPhase:Ljava/util/Map;

    sget-object v1, Lcom/squareup/calc/constants/CalculationPhase;->DISCOUNT_PERCENTAGE_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

    .line 235
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/math/BigDecimal;

    invoke-virtual {p1, v0}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object p1

    return-object p1

    .line 251
    :cond_0
    :goto_0
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unrecognized calculation phase: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getSubtotal()Ljava/math/BigDecimal;
    .locals 4

    .line 167
    iget-object v0, p0, Lcom/squareup/calc/AdjustedItem;->subtotal:Ljava/math/BigDecimal;

    if-nez v0, :cond_0

    .line 168
    new-instance v0, Ljava/math/BigDecimal;

    iget-object v1, p0, Lcom/squareup/calc/AdjustedItem;->rawItem:Lcom/squareup/calc/order/Item;

    .line 169
    invoke-interface {v1}, Lcom/squareup/calc/order/Item;->baseAmount()J

    move-result-wide v1

    sget-object v3, Ljava/math/MathContext;->UNLIMITED:Ljava/math/MathContext;

    invoke-direct {v0, v1, v2, v3}, Ljava/math/BigDecimal;-><init>(JLjava/math/MathContext;)V

    invoke-direct {p0}, Lcom/squareup/calc/AdjustedItem;->getTotalDiscountAdjustment()Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/calc/AdjustedItem;->subtotal:Ljava/math/BigDecimal;

    .line 171
    :cond_0
    iget-object v0, p0, Lcom/squareup/calc/AdjustedItem;->subtotal:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public getTotalCollectedForAllDiscounts()J
    .locals 2

    .line 120
    invoke-direct {p0}, Lcom/squareup/calc/AdjustedItem;->getTotalDiscountAdjustment()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getTotalCollectedForAllTaxes()J
    .locals 5

    .line 142
    sget-object v0, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    .line 143
    iget-object v1, p0, Lcom/squareup/calc/AdjustedItem;->collectedPerAdjustment:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 144
    iget-object v3, p0, Lcom/squareup/calc/AdjustedItem;->rawItem:Lcom/squareup/calc/order/Item;

    invoke-interface {v3}, Lcom/squareup/calc/order/Item;->appliedTaxes()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 145
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/math/BigDecimal;

    invoke-virtual {v0, v2}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    goto :goto_0

    .line 148
    :cond_1
    invoke-virtual {v0}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getTotalCollectedForInclusiveTaxes()J
    .locals 6

    .line 129
    iget-object v0, p0, Lcom/squareup/calc/AdjustedItem;->collectedPerAdjustment:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const-wide/16 v1, 0x0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 130
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/squareup/calc/AdjustedItem;->getOrderAdjustment(Ljava/lang/String;)Lcom/squareup/calc/order/Adjustment;

    move-result-object v4

    invoke-interface {v4}, Lcom/squareup/calc/order/Adjustment;->inclusionType()Lcom/squareup/calc/constants/InclusionType;

    move-result-object v4

    sget-object v5, Lcom/squareup/calc/constants/InclusionType;->INCLUSIVE:Lcom/squareup/calc/constants/InclusionType;

    if-ne v4, v5, :cond_0

    .line 131
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/math/BigDecimal;

    invoke-virtual {v3}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v3

    add-long/2addr v1, v3

    goto :goto_0

    :cond_1
    return-wide v1
.end method

.method public getTotalCollectedPerAdjustment()Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 108
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 109
    iget-object v1, p0, Lcom/squareup/calc/AdjustedItem;->collectedPerAdjustment:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 110
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/math/BigDecimal;

    invoke-virtual {v2}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method recordAdjustment(Lcom/squareup/calc/order/Adjustment;Ljava/math/BigDecimal;)V
    .locals 3

    .line 212
    invoke-interface {p1}, Lcom/squareup/calc/order/Adjustment;->phase()Lcom/squareup/calc/constants/CalculationPhase;

    move-result-object v0

    .line 214
    iget-object v1, p0, Lcom/squareup/calc/AdjustedItem;->collectedPerPhase:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/math/BigDecimal;

    invoke-virtual {v2, p2}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 216
    invoke-interface {p1}, Lcom/squareup/calc/order/Adjustment;->taxBasis()Lcom/squareup/calc/constants/ModifyTaxBasis;

    move-result-object v1

    sget-object v2, Lcom/squareup/calc/constants/ModifyTaxBasis;->DO_NOT_MODIFY_TAX_BASIS:Lcom/squareup/calc/constants/ModifyTaxBasis;

    if-ne v1, v2, :cond_0

    .line 217
    iget-object v1, p0, Lcom/squareup/calc/AdjustedItem;->nonTaxBasisModifyingAdjustmentsCollectedPerPhase:Ljava/util/Map;

    .line 219
    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/math/BigDecimal;

    invoke-virtual {v2, p2}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v2

    .line 217
    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 222
    :cond_0
    iget-object v0, p0, Lcom/squareup/calc/AdjustedItem;->collectedPerAdjustment:Ljava/util/Map;

    invoke-interface {p1}, Lcom/squareup/calc/order/Adjustment;->id()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method roundPreFeeAmount()V
    .locals 4

    .line 262
    invoke-virtual {p0}, Lcom/squareup/calc/AdjustedItem;->getSubtotal()Ljava/math/BigDecimal;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/calc/AdjustedItem;->surChargePhase_preFeeAmount:Ljava/math/BigDecimal;

    .line 263
    iget-object v0, p0, Lcom/squareup/calc/AdjustedItem;->collectedPerAdjustment:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 264
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/squareup/calc/AdjustedItem;->getOrderAdjustment(Ljava/lang/String;)Lcom/squareup/calc/order/Adjustment;

    move-result-object v2

    invoke-interface {v2}, Lcom/squareup/calc/order/Adjustment;->inclusionType()Lcom/squareup/calc/constants/InclusionType;

    move-result-object v2

    sget-object v3, Lcom/squareup/calc/constants/InclusionType;->INCLUSIVE:Lcom/squareup/calc/constants/InclusionType;

    if-ne v2, v3, :cond_0

    .line 265
    iget-object v2, p0, Lcom/squareup/calc/AdjustedItem;->surChargePhase_preFeeAmount:Ljava/math/BigDecimal;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/math/BigDecimal;

    invoke-virtual {v2, v1}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/calc/AdjustedItem;->surChargePhase_preFeeAmount:Ljava/math/BigDecimal;

    goto :goto_0

    :cond_1
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 202
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AdjustedItem{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/calc/AdjustedItem;->rawItem:Lcom/squareup/calc/order/Item;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
