.class public final Lcom/squareup/CommonLoggedInModule_ProvideServerClockFactory;
.super Ljava/lang/Object;
.source "CommonLoggedInModule_ProvideServerClockFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/account/ServerClock;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountStatusServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountstatus/PersistentAccountStatusService;",
            ">;"
        }
    .end annotation
.end field

.field private final clockProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;"
        }
    .end annotation
.end field

.field private final loggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/logging/RemoteLogger;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountstatus/PersistentAccountStatusService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/logging/RemoteLogger;",
            ">;)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/CommonLoggedInModule_ProvideServerClockFactory;->accountStatusServiceProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p2, p0, Lcom/squareup/CommonLoggedInModule_ProvideServerClockFactory;->clockProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p3, p0, Lcom/squareup/CommonLoggedInModule_ProvideServerClockFactory;->loggerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/CommonLoggedInModule_ProvideServerClockFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountstatus/PersistentAccountStatusService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/logging/RemoteLogger;",
            ">;)",
            "Lcom/squareup/CommonLoggedInModule_ProvideServerClockFactory;"
        }
    .end annotation

    .line 43
    new-instance v0, Lcom/squareup/CommonLoggedInModule_ProvideServerClockFactory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/CommonLoggedInModule_ProvideServerClockFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideServerClock(Lcom/squareup/accountstatus/PersistentAccountStatusService;Lcom/squareup/util/Clock;Lcom/squareup/logging/RemoteLogger;)Lcom/squareup/account/ServerClock;
    .locals 0

    .line 48
    invoke-static {p0, p1, p2}, Lcom/squareup/CommonLoggedInModule;->provideServerClock(Lcom/squareup/accountstatus/PersistentAccountStatusService;Lcom/squareup/util/Clock;Lcom/squareup/logging/RemoteLogger;)Lcom/squareup/account/ServerClock;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/account/ServerClock;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/account/ServerClock;
    .locals 3

    .line 37
    iget-object v0, p0, Lcom/squareup/CommonLoggedInModule_ProvideServerClockFactory;->accountStatusServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/accountstatus/PersistentAccountStatusService;

    iget-object v1, p0, Lcom/squareup/CommonLoggedInModule_ProvideServerClockFactory;->clockProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Clock;

    iget-object v2, p0, Lcom/squareup/CommonLoggedInModule_ProvideServerClockFactory;->loggerProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/logging/RemoteLogger;

    invoke-static {v0, v1, v2}, Lcom/squareup/CommonLoggedInModule_ProvideServerClockFactory;->provideServerClock(Lcom/squareup/accountstatus/PersistentAccountStatusService;Lcom/squareup/util/Clock;Lcom/squareup/logging/RemoteLogger;)Lcom/squareup/account/ServerClock;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/CommonLoggedInModule_ProvideServerClockFactory;->get()Lcom/squareup/account/ServerClock;

    move-result-object v0

    return-object v0
.end method
