.class public abstract Lcom/squareup/NoSquareDeviceTourModule;
.super Ljava/lang/Object;
.source "NoSquareDeviceTourModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideSquareDeviceTour(Lcom/squareup/SquareDeviceTour$NoSquareDeviceTour;)Lcom/squareup/SquareDeviceTour;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideSquareDeviceTourSettings(Lcom/squareup/SquareDeviceTourSettings$NoTourSettings;)Lcom/squareup/SquareDeviceTourSettings;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
