.class public final Lcom/squareup/SposAppModule_ProvideOpenTicketsAsSavedCartsFactory;
.super Ljava/lang/Object;
.source "SposAppModule_ProvideOpenTicketsAsSavedCartsFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/SposAppModule_ProvideOpenTicketsAsSavedCartsFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/SposAppModule_ProvideOpenTicketsAsSavedCartsFactory;
    .locals 1

    .line 18
    invoke-static {}, Lcom/squareup/SposAppModule_ProvideOpenTicketsAsSavedCartsFactory$InstanceHolder;->access$000()Lcom/squareup/SposAppModule_ProvideOpenTicketsAsSavedCartsFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideOpenTicketsAsSavedCarts()Ljava/lang/Boolean;
    .locals 2

    .line 22
    invoke-static {}, Lcom/squareup/SposAppModule;->provideOpenTicketsAsSavedCarts()Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    return-object v0
.end method


# virtual methods
.method public get()Ljava/lang/Boolean;
    .locals 1

    .line 14
    invoke-static {}, Lcom/squareup/SposAppModule_ProvideOpenTicketsAsSavedCartsFactory;->provideOpenTicketsAsSavedCarts()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/SposAppModule_ProvideOpenTicketsAsSavedCartsFactory;->get()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
