.class public final Lcom/squareup/CommonAppModule_ProvideEnsureThumborFactory;
.super Ljava/lang/Object;
.source "CommonAppModule_ProvideEnsureThumborFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/picasso/pollexor/EnsureThumborRequestTransformer;",
        ">;"
    }
.end annotation


# instance fields
.field private final thumborProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pollexor/Thumbor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pollexor/Thumbor;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/CommonAppModule_ProvideEnsureThumborFactory;->thumborProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/CommonAppModule_ProvideEnsureThumborFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pollexor/Thumbor;",
            ">;)",
            "Lcom/squareup/CommonAppModule_ProvideEnsureThumborFactory;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/CommonAppModule_ProvideEnsureThumborFactory;

    invoke-direct {v0, p0}, Lcom/squareup/CommonAppModule_ProvideEnsureThumborFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideEnsureThumbor(Lcom/squareup/pollexor/Thumbor;)Lcom/squareup/picasso/pollexor/EnsureThumborRequestTransformer;
    .locals 1

    .line 36
    invoke-static {p0}, Lcom/squareup/CommonAppModule;->provideEnsureThumbor(Lcom/squareup/pollexor/Thumbor;)Lcom/squareup/picasso/pollexor/EnsureThumborRequestTransformer;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/picasso/pollexor/EnsureThumborRequestTransformer;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/picasso/pollexor/EnsureThumborRequestTransformer;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/CommonAppModule_ProvideEnsureThumborFactory;->thumborProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/pollexor/Thumbor;

    invoke-static {v0}, Lcom/squareup/CommonAppModule_ProvideEnsureThumborFactory;->provideEnsureThumbor(Lcom/squareup/pollexor/Thumbor;)Lcom/squareup/picasso/pollexor/EnsureThumborRequestTransformer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/CommonAppModule_ProvideEnsureThumborFactory;->get()Lcom/squareup/picasso/pollexor/EnsureThumborRequestTransformer;

    move-result-object v0

    return-object v0
.end method
