.class public final Lcom/squareup/buyer/language/BuyerLanguageSelectionBootstrapScreen;
.super Lcom/squareup/container/BootstrapTreeKey;
.source "BuyerLanguageSelectionBootstrapScreen.kt"


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u00c7\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u0008\u0010\u0007\u001a\u00020\u0008H\u0016\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/buyer/language/BuyerLanguageSelectionBootstrapScreen;",
        "Lcom/squareup/container/BootstrapTreeKey;",
        "()V",
        "doRegister",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "getParentKey",
        "Lcom/squareup/buyer/language/BuyerLanguageSelectionScope;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/buyer/language/BuyerLanguageSelectionBootstrapScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 8
    new-instance v0, Lcom/squareup/buyer/language/BuyerLanguageSelectionBootstrapScreen;

    invoke-direct {v0}, Lcom/squareup/buyer/language/BuyerLanguageSelectionBootstrapScreen;-><init>()V

    sput-object v0, Lcom/squareup/buyer/language/BuyerLanguageSelectionBootstrapScreen;->INSTANCE:Lcom/squareup/buyer/language/BuyerLanguageSelectionBootstrapScreen;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Lcom/squareup/container/BootstrapTreeKey;-><init>()V

    return-void
.end method


# virtual methods
.method public doRegister(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    sget-object v0, Lcom/squareup/buyer/language/BuyerLanguageSelectionWorkflowRunner;->Companion:Lcom/squareup/buyer/language/BuyerLanguageSelectionWorkflowRunner$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/buyer/language/BuyerLanguageSelectionWorkflowRunner$Companion;->get(Lmortar/MortarScope;)Lcom/squareup/buyer/language/BuyerLanguageSelectionWorkflowRunner;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/buyer/language/BuyerLanguageSelectionWorkflowRunner;->startWorkflow()V

    return-void
.end method

.method public getParentKey()Lcom/squareup/buyer/language/BuyerLanguageSelectionScope;
    .locals 1

    .line 9
    sget-object v0, Lcom/squareup/buyer/language/BuyerLanguageSelectionScope;->INSTANCE:Lcom/squareup/buyer/language/BuyerLanguageSelectionScope;

    return-object v0
.end method

.method public bridge synthetic getParentKey()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/buyer/language/BuyerLanguageSelectionBootstrapScreen;->getParentKey()Lcom/squareup/buyer/language/BuyerLanguageSelectionScope;

    move-result-object v0

    return-object v0
.end method
