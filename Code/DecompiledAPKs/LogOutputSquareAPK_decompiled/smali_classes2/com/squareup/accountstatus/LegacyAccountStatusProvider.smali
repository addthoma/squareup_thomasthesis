.class Lcom/squareup/accountstatus/LegacyAccountStatusProvider;
.super Ljava/lang/Object;
.source "LegacyAccountStatusProvider.java"

# interfaces
.implements Lcom/squareup/accountstatus/AccountStatusProvider;


# instance fields
.field private final delegate:Lcom/squareup/accountstatus/PersistentAccountStatusService;

.field private final loggedInStatusProvider:Lcom/squareup/account/LoggedInStatusProvider;

.field private final sessionTokenProvider:Lcom/squareup/account/SessionIdPIIProvider;


# direct methods
.method constructor <init>(Lcom/squareup/accountstatus/PersistentAccountStatusService;Lcom/squareup/account/SessionIdPIIProvider;Lcom/squareup/account/LoggedInStatusProvider;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/squareup/accountstatus/LegacyAccountStatusProvider;->delegate:Lcom/squareup/accountstatus/PersistentAccountStatusService;

    .line 40
    iput-object p2, p0, Lcom/squareup/accountstatus/LegacyAccountStatusProvider;->sessionTokenProvider:Lcom/squareup/account/SessionIdPIIProvider;

    .line 41
    iput-object p3, p0, Lcom/squareup/accountstatus/LegacyAccountStatusProvider;->loggedInStatusProvider:Lcom/squareup/account/LoggedInStatusProvider;

    return-void
.end method

.method static synthetic lambda$latest$0(Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus;Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/util/Optional;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 49
    instance-of p0, p0, Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus$LoggedIn;

    if-eqz p0, :cond_0

    .line 50
    invoke-static {p1}, Lcom/squareup/util/Optional;->of(Ljava/lang/Object;)Lcom/squareup/util/Optional;

    move-result-object p0

    return-object p0

    .line 52
    :cond_0
    invoke-static {}, Lcom/squareup/util/Optional;->empty()Lcom/squareup/util/Optional;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$maybeUpdatePreferences$1(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const-string v0, "saving preferences to server"

    .line 84
    invoke-static {p0, v0}, Lcom/squareup/receiving/SuccessOrFailureLogger;->logFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public fetch()Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;>;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 58
    invoke-virtual {p0, v0}, Lcom/squareup/accountstatus/LegacyAccountStatusProvider;->fetch(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method public fetch(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;>;"
        }
    .end annotation

    .line 63
    invoke-static {p1}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    iget-object p1, p0, Lcom/squareup/accountstatus/LegacyAccountStatusProvider;->sessionTokenProvider:Lcom/squareup/account/SessionIdPIIProvider;

    invoke-interface {p1}, Lcom/squareup/account/SessionIdPIIProvider;->getSessionIdPII()Ljava/lang/String;

    move-result-object p1

    .line 67
    :cond_0
    iget-object v0, p0, Lcom/squareup/accountstatus/LegacyAccountStatusProvider;->delegate:Lcom/squareup/accountstatus/PersistentAccountStatusService;

    invoke-virtual {v0, p1}, Lcom/squareup/accountstatus/PersistentAccountStatusService;->observeStatus(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public latest()Lio/reactivex/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;>;"
        }
    .end annotation

    .line 45
    iget-object v0, p0, Lcom/squareup/accountstatus/LegacyAccountStatusProvider;->loggedInStatusProvider:Lcom/squareup/account/LoggedInStatusProvider;

    .line 46
    invoke-interface {v0}, Lcom/squareup/account/LoggedInStatusProvider;->loggedInStatus()Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/accountstatus/LegacyAccountStatusProvider;->delegate:Lcom/squareup/accountstatus/PersistentAccountStatusService;

    .line 47
    invoke-virtual {v1}, Lcom/squareup/accountstatus/PersistentAccountStatusService;->accountStatusResponse()Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/accountstatus/-$$Lambda$LegacyAccountStatusProvider$IcbEEqn0jPeQbJWFMA-Sf6Txr9o;->INSTANCE:Lcom/squareup/accountstatus/-$$Lambda$LegacyAccountStatusProvider$IcbEEqn0jPeQbJWFMA-Sf6Txr9o;

    .line 45
    invoke-static {v0, v1, v2}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public maybeUpdatePreferences(Lcom/squareup/server/account/protos/PreferencesRequest;)V
    .locals 2

    .line 73
    iget-object v0, p0, Lcom/squareup/accountstatus/LegacyAccountStatusProvider;->delegate:Lcom/squareup/accountstatus/PersistentAccountStatusService;

    invoke-virtual {v0}, Lcom/squareup/accountstatus/PersistentAccountStatusService;->getStatus()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v0

    .line 74
    iget-object v1, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object v0, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->uses_device_profile:Ljava/lang/Boolean;

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->DEFAULT_USES_DEVICE_PROFILE:Ljava/lang/Boolean;

    .line 75
    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 81
    :cond_0
    iget-object v0, p0, Lcom/squareup/accountstatus/LegacyAccountStatusProvider;->delegate:Lcom/squareup/accountstatus/PersistentAccountStatusService;

    .line 82
    invoke-virtual {v0, p1}, Lcom/squareup/accountstatus/PersistentAccountStatusService;->setPreferences(Lcom/squareup/server/account/protos/PreferencesRequest;)Lio/reactivex/Single;

    move-result-object p1

    sget-object v0, Lcom/squareup/accountstatus/-$$Lambda$LegacyAccountStatusProvider$mc1dfot5zJenTfDxdqSbNnN2OmM;->INSTANCE:Lcom/squareup/accountstatus/-$$Lambda$LegacyAccountStatusProvider$mc1dfot5zJenTfDxdqSbNnN2OmM;

    .line 83
    invoke-virtual {p1, v0}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    return-void
.end method
