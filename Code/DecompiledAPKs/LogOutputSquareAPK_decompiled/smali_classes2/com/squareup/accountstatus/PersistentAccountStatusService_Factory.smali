.class public final Lcom/squareup/accountstatus/PersistentAccountStatusService_Factory;
.super Ljava/lang/Object;
.source "PersistentAccountStatusService_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/accountstatus/PersistentAccountStatusService;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/DeviceIdProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/accountstatus/AccountStatusService;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountstatus/TransitionalAccountStatusResponseCache;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/CrashReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;"
        }
    .end annotation
.end field

.field private final arg6Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/DeviceIdProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/accountstatus/AccountStatusService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountstatus/TransitionalAccountStatusResponseCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/CrashReporter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/squareup/accountstatus/PersistentAccountStatusService_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 39
    iput-object p2, p0, Lcom/squareup/accountstatus/PersistentAccountStatusService_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 40
    iput-object p3, p0, Lcom/squareup/accountstatus/PersistentAccountStatusService_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 41
    iput-object p4, p0, Lcom/squareup/accountstatus/PersistentAccountStatusService_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 42
    iput-object p5, p0, Lcom/squareup/accountstatus/PersistentAccountStatusService_Factory;->arg4Provider:Ljavax/inject/Provider;

    .line 43
    iput-object p6, p0, Lcom/squareup/accountstatus/PersistentAccountStatusService_Factory;->arg5Provider:Ljavax/inject/Provider;

    .line 44
    iput-object p7, p0, Lcom/squareup/accountstatus/PersistentAccountStatusService_Factory;->arg6Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/accountstatus/PersistentAccountStatusService_Factory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/DeviceIdProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/accountstatus/AccountStatusService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountstatus/TransitionalAccountStatusResponseCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/CrashReporter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/accountstatus/PersistentAccountStatusService_Factory;"
        }
    .end annotation

    .line 58
    new-instance v8, Lcom/squareup/accountstatus/PersistentAccountStatusService_Factory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/accountstatus/PersistentAccountStatusService_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static newInstance(Lcom/squareup/settings/DeviceIdProvider;Ldagger/Lazy;Lcom/squareup/badbus/BadEventSink;Lcom/squareup/accountstatus/TransitionalAccountStatusResponseCache;Lcom/squareup/log/CrashReporter;Lcom/squareup/util/Clock;Ljavax/inject/Provider;)Lcom/squareup/accountstatus/PersistentAccountStatusService;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/DeviceIdProvider;",
            "Ldagger/Lazy<",
            "Lcom/squareup/server/accountstatus/AccountStatusService;",
            ">;",
            "Lcom/squareup/badbus/BadEventSink;",
            "Lcom/squareup/accountstatus/TransitionalAccountStatusResponseCache;",
            "Lcom/squareup/log/CrashReporter;",
            "Lcom/squareup/util/Clock;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/accountstatus/PersistentAccountStatusService;"
        }
    .end annotation

    .line 65
    new-instance v8, Lcom/squareup/accountstatus/PersistentAccountStatusService;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/accountstatus/PersistentAccountStatusService;-><init>(Lcom/squareup/settings/DeviceIdProvider;Ldagger/Lazy;Lcom/squareup/badbus/BadEventSink;Lcom/squareup/accountstatus/TransitionalAccountStatusResponseCache;Lcom/squareup/log/CrashReporter;Lcom/squareup/util/Clock;Ljavax/inject/Provider;)V

    return-object v8
.end method


# virtual methods
.method public get()Lcom/squareup/accountstatus/PersistentAccountStatusService;
    .locals 8

    .line 49
    iget-object v0, p0, Lcom/squareup/accountstatus/PersistentAccountStatusService_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/settings/DeviceIdProvider;

    iget-object v0, p0, Lcom/squareup/accountstatus/PersistentAccountStatusService_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v2

    iget-object v0, p0, Lcom/squareup/accountstatus/PersistentAccountStatusService_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/badbus/BadEventSink;

    iget-object v0, p0, Lcom/squareup/accountstatus/PersistentAccountStatusService_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/accountstatus/TransitionalAccountStatusResponseCache;

    iget-object v0, p0, Lcom/squareup/accountstatus/PersistentAccountStatusService_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/log/CrashReporter;

    iget-object v0, p0, Lcom/squareup/accountstatus/PersistentAccountStatusService_Factory;->arg5Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/util/Clock;

    iget-object v7, p0, Lcom/squareup/accountstatus/PersistentAccountStatusService_Factory;->arg6Provider:Ljavax/inject/Provider;

    invoke-static/range {v1 .. v7}, Lcom/squareup/accountstatus/PersistentAccountStatusService_Factory;->newInstance(Lcom/squareup/settings/DeviceIdProvider;Ldagger/Lazy;Lcom/squareup/badbus/BadEventSink;Lcom/squareup/accountstatus/TransitionalAccountStatusResponseCache;Lcom/squareup/log/CrashReporter;Lcom/squareup/util/Clock;Ljavax/inject/Provider;)Lcom/squareup/accountstatus/PersistentAccountStatusService;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/accountstatus/PersistentAccountStatusService_Factory;->get()Lcom/squareup/accountstatus/PersistentAccountStatusService;

    move-result-object v0

    return-object v0
.end method
