.class public final Lcom/squareup/checkout/Checkout;
.super Ljava/lang/Object;
.source "Checkout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkout/Checkout$Response;,
        Lcom/squareup/checkout/Checkout$Request;,
        Lcom/squareup/checkout/Checkout$Callback;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static takePayment(Lcom/squareup/checkout/Checkout$Request;Lcom/squareup/checkout/Checkout$Callback;)V
    .locals 0

    .line 25
    new-instance p0, Lcom/squareup/checkout/Checkout$Response;

    invoke-direct {p0}, Lcom/squareup/checkout/Checkout$Response;-><init>()V

    invoke-interface {p1, p0}, Lcom/squareup/checkout/Checkout$Callback;->onResponse(Lcom/squareup/checkout/Checkout$Response;)V

    return-void
.end method
