.class public final Lcom/squareup/checkout/Discount$Builder;
.super Lcom/squareup/checkout/Adjustment$Builder;
.source "Discount.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkout/Discount;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/checkout/Adjustment$Builder<",
        "Lcom/squareup/checkout/Discount$Builder;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDiscount.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Discount.kt\ncom/squareup/checkout/Discount$Builder\n*L\n1#1,558:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000x\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0016\u00a2\u0006\u0002\u0010\u0002B\u000f\u0008\u0016\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005B\u000f\u0008\u0016\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008B\u0017\u0008\u0016\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rB\u000f\u0008\u0016\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\u0002\u0010\u0010B!\u0008\u0016\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0008\u0010\u0013\u001a\u0004\u0018\u00010\u0014\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\u0015J\u0010\u0010\u0016\u001a\u00020\u00002\u0008\u0010\u0016\u001a\u0004\u0018\u00010\u0017J\u0006\u0010\'\u001a\u00020\u0004J\u000e\u0010\u0018\u001a\u00020\u00002\u0006\u0010\u0018\u001a\u00020\u0014J\u000e\u0010\u0019\u001a\u00020\u00002\u0006\u0010(\u001a\u00020\u0014J\u000e\u0010\u001a\u001a\u00020\u00002\u0006\u0010\u001a\u001a\u00020\u001bJ\u000e\u0010\u001c\u001a\u00020\u00002\u0006\u0010\u001c\u001a\u00020\u0014J\u000e\u0010\u001f\u001a\u00020\u00002\u0006\u0010\u001f\u001a\u00020 J\u0010\u0010)\u001a\u00020\u00002\u0006\u0010\t\u001a\u00020\nH\u0002J\u0010\u0010*\u001a\u00020+2\u0006\u0010\u0003\u001a\u00020\u001eH\u0002J\u0010\u0010,\u001a\u00020\u00002\u0006\u0010,\u001a\u00020-H\u0016J\u000e\u0010!\u001a\u00020\u00002\u0006\u0010!\u001a\u00020\"J\u000e\u0010#\u001a\u00020\u00002\u0006\u0010#\u001a\u00020\u000cJ\u000e\u0010$\u001a\u00020\u00002\u0006\u0010$\u001a\u00020\u000cJ\u000e\u0010%\u001a\u00020\u00002\u0006\u0010%\u001a\u00020&R\u0010\u0010\u0016\u001a\u0004\u0018\u00010\u0017X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0014X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0019\u001a\u0004\u0018\u00010\u0014X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001c\u001a\u0004\u0018\u00010\u0014X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u001eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001f\u001a\u00020 X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010!\u001a\u00020\"X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010#\u001a\u00020\u000cX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010$\u001a\u00020\u000cX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010%\u001a\u00020&X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006."
    }
    d2 = {
        "Lcom/squareup/checkout/Discount$Builder;",
        "Lcom/squareup/checkout/Adjustment$Builder;",
        "()V",
        "discount",
        "Lcom/squareup/checkout/Discount;",
        "(Lcom/squareup/checkout/Discount;)V",
        "details",
        "Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;",
        "(Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;)V",
        "coupon",
        "Lcom/squareup/protos/client/coupons/Coupon;",
        "discountApplicationIdEnabled",
        "",
        "(Lcom/squareup/protos/client/coupons/Coupon;Z)V",
        "catalogDiscount",
        "Lcom/squareup/shared/catalog/models/CatalogDiscount;",
        "(Lcom/squareup/shared/catalog/models/CatalogDiscount;)V",
        "discountLineItem",
        "Lcom/squareup/protos/client/bills/DiscountLineItem;",
        "parentItemSelectedVariationId",
        "",
        "(Lcom/squareup/protos/client/bills/DiscountLineItem;Ljava/lang/String;Z)V",
        "applicationMethod",
        "Lcom/squareup/api/items/Discount$ApplicationMethod;",
        "color",
        "couponDefinitionToken",
        "couponReason",
        "Lcom/squareup/protos/client/coupons/Coupon$Reason;",
        "couponToken",
        "discountProto",
        "Lcom/squareup/api/items/Discount;",
        "discountType",
        "Lcom/squareup/api/items/Discount$DiscountType;",
        "matches",
        "Lcom/squareup/checkout/Discount$Matches;",
        "pinRequired",
        "recalledFromTicket",
        "scope",
        "Lcom/squareup/checkout/Discount$Scope;",
        "build",
        "token",
        "eligibleItemCategoryAndVariationIds",
        "fromDiscountProto",
        "",
        "inclusionType",
        "Lcom/squareup/api/items/Fee$InclusionType;",
        "checkout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private applicationMethod:Lcom/squareup/api/items/Discount$ApplicationMethod;

.field private color:Ljava/lang/String;

.field private couponDefinitionToken:Ljava/lang/String;

.field private couponReason:Lcom/squareup/protos/client/coupons/Coupon$Reason;

.field private couponToken:Ljava/lang/String;

.field private discountProto:Lcom/squareup/api/items/Discount;

.field private discountType:Lcom/squareup/api/items/Discount$DiscountType;

.field private matches:Lcom/squareup/checkout/Discount$Matches;

.field private pinRequired:Z

.field private recalledFromTicket:Z

.field private scope:Lcom/squareup/checkout/Discount$Scope;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 309
    invoke-direct {p0}, Lcom/squareup/checkout/Adjustment$Builder;-><init>()V

    const-string v0, ""

    .line 295
    iput-object v0, p0, Lcom/squareup/checkout/Discount$Builder;->color:Ljava/lang/String;

    .line 298
    sget-object v0, Lcom/squareup/protos/client/coupons/Coupon$Reason;->UNKNOWN_REASON:Lcom/squareup/protos/client/coupons/Coupon$Reason;

    iput-object v0, p0, Lcom/squareup/checkout/Discount$Builder;->couponReason:Lcom/squareup/protos/client/coupons/Coupon$Reason;

    .line 301
    sget-object v0, Lcom/squareup/api/items/Discount;->DEFAULT_DISCOUNT_TYPE:Lcom/squareup/api/items/Discount$DiscountType;

    const-string v1, "DEFAULT_DISCOUNT_TYPE"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/checkout/Discount$Builder;->discountType:Lcom/squareup/api/items/Discount$DiscountType;

    .line 302
    sget-object v0, Lcom/squareup/api/items/Discount;->DEFAULT_PIN_REQUIRED:Ljava/lang/Boolean;

    const-string v1, "DEFAULT_PIN_REQUIRED"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/checkout/Discount$Builder;->pinRequired:Z

    .line 304
    sget-object v0, Lcom/squareup/checkout/Discount$Scope;->CART:Lcom/squareup/checkout/Discount$Scope;

    iput-object v0, p0, Lcom/squareup/checkout/Discount$Builder;->scope:Lcom/squareup/checkout/Discount$Scope;

    .line 306
    sget-object v0, Lcom/squareup/checkout/Discount$Matches$AllItems;->INSTANCE:Lcom/squareup/checkout/Discount$Matches$AllItems;

    check-cast v0, Lcom/squareup/checkout/Discount$Matches;

    iput-object v0, p0, Lcom/squareup/checkout/Discount$Builder;->matches:Lcom/squareup/checkout/Discount$Matches;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/checkout/Discount;)V
    .locals 2

    const-string v0, "discount"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 312
    invoke-direct {p0}, Lcom/squareup/checkout/Adjustment$Builder;-><init>()V

    const-string v0, ""

    .line 295
    iput-object v0, p0, Lcom/squareup/checkout/Discount$Builder;->color:Ljava/lang/String;

    .line 298
    sget-object v0, Lcom/squareup/protos/client/coupons/Coupon$Reason;->UNKNOWN_REASON:Lcom/squareup/protos/client/coupons/Coupon$Reason;

    iput-object v0, p0, Lcom/squareup/checkout/Discount$Builder;->couponReason:Lcom/squareup/protos/client/coupons/Coupon$Reason;

    .line 301
    sget-object v0, Lcom/squareup/api/items/Discount;->DEFAULT_DISCOUNT_TYPE:Lcom/squareup/api/items/Discount$DiscountType;

    const-string v1, "DEFAULT_DISCOUNT_TYPE"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/checkout/Discount$Builder;->discountType:Lcom/squareup/api/items/Discount$DiscountType;

    .line 302
    sget-object v0, Lcom/squareup/api/items/Discount;->DEFAULT_PIN_REQUIRED:Ljava/lang/Boolean;

    const-string v1, "DEFAULT_PIN_REQUIRED"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/checkout/Discount$Builder;->pinRequired:Z

    .line 304
    sget-object v0, Lcom/squareup/checkout/Discount$Scope;->CART:Lcom/squareup/checkout/Discount$Scope;

    iput-object v0, p0, Lcom/squareup/checkout/Discount$Builder;->scope:Lcom/squareup/checkout/Discount$Scope;

    .line 306
    sget-object v0, Lcom/squareup/checkout/Discount$Matches$AllItems;->INSTANCE:Lcom/squareup/checkout/Discount$Matches$AllItems;

    check-cast v0, Lcom/squareup/checkout/Discount$Matches;

    iput-object v0, p0, Lcom/squareup/checkout/Discount$Builder;->matches:Lcom/squareup/checkout/Discount$Matches;

    .line 312
    iget-object v0, p1, Lcom/squareup/checkout/Discount;->id:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/Discount$Builder;->id(Ljava/lang/String;)Lcom/squareup/checkout/Adjustment$Builder;

    .line 313
    iget-object v0, p1, Lcom/squareup/checkout/Discount;->idPair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/Discount$Builder;->idPair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/checkout/Adjustment$Builder;

    .line 314
    iget-object v0, p1, Lcom/squareup/checkout/Discount;->name:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/Discount$Builder;->name(Ljava/lang/String;)Lcom/squareup/checkout/Adjustment$Builder;

    .line 315
    iget-object v0, p1, Lcom/squareup/checkout/Discount;->percentage:Lcom/squareup/util/Percentage;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/Discount$Builder;->percentage(Lcom/squareup/util/Percentage;)Lcom/squareup/checkout/Adjustment$Builder;

    .line 316
    iget-object v0, p1, Lcom/squareup/checkout/Discount;->amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/Discount$Builder;->amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/checkout/Adjustment$Builder;

    .line 317
    iget-object v0, p1, Lcom/squareup/checkout/Discount;->maxAmount:Lcom/squareup/protos/common/Money;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/Discount$Builder;->maxAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/checkout/Adjustment$Builder;

    .line 318
    iget-object v0, p1, Lcom/squareup/checkout/Discount;->phase:Lcom/squareup/api/items/CalculationPhase;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/Discount$Builder;->phase(Lcom/squareup/api/items/CalculationPhase;)Lcom/squareup/checkout/Adjustment$Builder;

    .line 319
    iget-boolean v0, p1, Lcom/squareup/checkout/Discount;->enabled:Z

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/Discount$Builder;->enabled(Z)Lcom/squareup/checkout/Adjustment$Builder;

    .line 320
    invoke-static {p1}, Lcom/squareup/checkout/Discount;->access$getDiscountProto$p(Lcom/squareup/checkout/Discount;)Lcom/squareup/api/items/Discount;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/checkout/Discount$Builder;->discountProto:Lcom/squareup/api/items/Discount;

    .line 321
    iget-object v0, p1, Lcom/squareup/checkout/Discount;->createdAt:Ljava/util/Date;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/Discount$Builder;->createdAt(Ljava/util/Date;)Lcom/squareup/checkout/Adjustment$Builder;

    .line 322
    invoke-virtual {p1}, Lcom/squareup/checkout/Discount;->getScope()Lcom/squareup/checkout/Discount$Scope;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/Discount$Builder;->scope(Lcom/squareup/checkout/Discount$Scope;)Lcom/squareup/checkout/Discount$Builder;

    .line 323
    iget-object v0, p1, Lcom/squareup/checkout/Discount;->taxBasis:Lcom/squareup/api/items/Discount$ModifyTaxBasis;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/Discount$Builder;->taxBasis(Lcom/squareup/api/items/Discount$ModifyTaxBasis;)Lcom/squareup/checkout/Adjustment$Builder;

    .line 324
    invoke-virtual {p1}, Lcom/squareup/checkout/Discount;->getCouponToken()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    move-object v1, p0

    check-cast v1, Lcom/squareup/checkout/Discount$Builder;

    invoke-virtual {v1, v0}, Lcom/squareup/checkout/Discount$Builder;->couponToken(Ljava/lang/String;)Lcom/squareup/checkout/Discount$Builder;

    .line 325
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/checkout/Discount;->getCouponDefinitionToken()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    move-object v1, p0

    check-cast v1, Lcom/squareup/checkout/Discount$Builder;

    invoke-virtual {v1, v0}, Lcom/squareup/checkout/Discount$Builder;->couponDefinitionToken(Ljava/lang/String;)Lcom/squareup/checkout/Discount$Builder;

    .line 326
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/checkout/Discount;->getCouponReason()Lcom/squareup/protos/client/coupons/Coupon$Reason;

    move-result-object v0

    if-eqz v0, :cond_2

    move-object v1, p0

    check-cast v1, Lcom/squareup/checkout/Discount$Builder;

    invoke-virtual {v1, v0}, Lcom/squareup/checkout/Discount$Builder;->couponReason(Lcom/squareup/protos/client/coupons/Coupon$Reason;)Lcom/squareup/checkout/Discount$Builder;

    .line 327
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/checkout/Discount;->getMatches()Lcom/squareup/checkout/Discount$Matches;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/Discount$Builder;->matches(Lcom/squareup/checkout/Discount$Matches;)Lcom/squareup/checkout/Discount$Builder;

    .line 328
    iget-object p1, p1, Lcom/squareup/checkout/Discount;->applicationMethod:Lcom/squareup/api/items/Discount$ApplicationMethod;

    if-eqz p1, :cond_3

    move-object v0, p0

    check-cast v0, Lcom/squareup/checkout/Discount$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/checkout/Discount$Builder;->applicationMethod(Lcom/squareup/api/items/Discount$ApplicationMethod;)Lcom/squareup/checkout/Discount$Builder;

    :cond_3
    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;)V
    .locals 2

    const-string v0, "details"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 333
    invoke-direct {p0}, Lcom/squareup/checkout/Adjustment$Builder;-><init>()V

    const-string v0, ""

    .line 295
    iput-object v0, p0, Lcom/squareup/checkout/Discount$Builder;->color:Ljava/lang/String;

    .line 298
    sget-object v0, Lcom/squareup/protos/client/coupons/Coupon$Reason;->UNKNOWN_REASON:Lcom/squareup/protos/client/coupons/Coupon$Reason;

    iput-object v0, p0, Lcom/squareup/checkout/Discount$Builder;->couponReason:Lcom/squareup/protos/client/coupons/Coupon$Reason;

    .line 301
    sget-object v0, Lcom/squareup/api/items/Discount;->DEFAULT_DISCOUNT_TYPE:Lcom/squareup/api/items/Discount$DiscountType;

    const-string v1, "DEFAULT_DISCOUNT_TYPE"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/checkout/Discount$Builder;->discountType:Lcom/squareup/api/items/Discount$DiscountType;

    .line 302
    sget-object v0, Lcom/squareup/api/items/Discount;->DEFAULT_PIN_REQUIRED:Ljava/lang/Boolean;

    const-string v1, "DEFAULT_PIN_REQUIRED"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/checkout/Discount$Builder;->pinRequired:Z

    .line 304
    sget-object v0, Lcom/squareup/checkout/Discount$Scope;->CART:Lcom/squareup/checkout/Discount$Scope;

    iput-object v0, p0, Lcom/squareup/checkout/Discount$Builder;->scope:Lcom/squareup/checkout/Discount$Scope;

    .line 306
    sget-object v0, Lcom/squareup/checkout/Discount$Matches$AllItems;->INSTANCE:Lcom/squareup/checkout/Discount$Matches$AllItems;

    check-cast v0, Lcom/squareup/checkout/Discount$Matches;

    iput-object v0, p0, Lcom/squareup/checkout/Discount$Builder;->matches:Lcom/squareup/checkout/Discount$Matches;

    .line 333
    sget-object v0, Lcom/squareup/api/items/Discount$ApplicationMethod;->COMP:Lcom/squareup/api/items/Discount$ApplicationMethod;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/Discount$Builder;->applicationMethod(Lcom/squareup/api/items/Discount$ApplicationMethod;)Lcom/squareup/checkout/Discount$Builder;

    .line 334
    sget-object v0, Lcom/squareup/api/items/Discount$DiscountType;->FIXED:Lcom/squareup/api/items/Discount$DiscountType;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/Discount$Builder;->discountType(Lcom/squareup/api/items/Discount$DiscountType;)Lcom/squareup/checkout/Discount$Builder;

    .line 335
    iget-object v0, p1, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->cogs_object_id:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/Discount$Builder;->id(Ljava/lang/String;)Lcom/squareup/checkout/Adjustment$Builder;

    .line 336
    iget-object v0, p1, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->name:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/Discount$Builder;->name(Ljava/lang/String;)Lcom/squareup/checkout/Adjustment$Builder;

    .line 337
    sget-object v0, Lcom/squareup/util/Percentage;->Companion:Lcom/squareup/util/Percentage$Companion;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->percentage:Ljava/lang/String;

    const-string v1, "details.percentage"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/squareup/util/Percentage$Companion;->fromString(Ljava/lang/String;)Lcom/squareup/util/Percentage;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/checkout/Discount$Builder;->percentage(Lcom/squareup/util/Percentage;)Lcom/squareup/checkout/Adjustment$Builder;

    .line 338
    sget-object p1, Lcom/squareup/api/items/CalculationPhase;->DISCOUNT_PERCENTAGE_PHASE:Lcom/squareup/api/items/CalculationPhase;

    invoke-virtual {p0, p1}, Lcom/squareup/checkout/Discount$Builder;->phase(Lcom/squareup/api/items/CalculationPhase;)Lcom/squareup/checkout/Adjustment$Builder;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bills/DiscountLineItem;Ljava/lang/String;Z)V
    .locals 2

    const-string v0, "discountLineItem"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 390
    invoke-direct {p0}, Lcom/squareup/checkout/Adjustment$Builder;-><init>()V

    const-string v0, ""

    .line 295
    iput-object v0, p0, Lcom/squareup/checkout/Discount$Builder;->color:Ljava/lang/String;

    .line 298
    sget-object v0, Lcom/squareup/protos/client/coupons/Coupon$Reason;->UNKNOWN_REASON:Lcom/squareup/protos/client/coupons/Coupon$Reason;

    iput-object v0, p0, Lcom/squareup/checkout/Discount$Builder;->couponReason:Lcom/squareup/protos/client/coupons/Coupon$Reason;

    .line 301
    sget-object v0, Lcom/squareup/api/items/Discount;->DEFAULT_DISCOUNT_TYPE:Lcom/squareup/api/items/Discount$DiscountType;

    const-string v1, "DEFAULT_DISCOUNT_TYPE"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/checkout/Discount$Builder;->discountType:Lcom/squareup/api/items/Discount$DiscountType;

    .line 302
    sget-object v0, Lcom/squareup/api/items/Discount;->DEFAULT_PIN_REQUIRED:Ljava/lang/Boolean;

    const-string v1, "DEFAULT_PIN_REQUIRED"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/checkout/Discount$Builder;->pinRequired:Z

    .line 304
    sget-object v0, Lcom/squareup/checkout/Discount$Scope;->CART:Lcom/squareup/checkout/Discount$Scope;

    iput-object v0, p0, Lcom/squareup/checkout/Discount$Builder;->scope:Lcom/squareup/checkout/Discount$Scope;

    .line 306
    sget-object v0, Lcom/squareup/checkout/Discount$Matches$AllItems;->INSTANCE:Lcom/squareup/checkout/Discount$Matches$AllItems;

    check-cast v0, Lcom/squareup/checkout/Discount$Matches;

    iput-object v0, p0, Lcom/squareup/checkout/Discount$Builder;->matches:Lcom/squareup/checkout/Discount$Matches;

    .line 390
    iget-object v0, p1, Lcom/squareup/protos/client/bills/DiscountLineItem;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/Discount$Builder;->tryParseCreateAt(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/checkout/Adjustment$Builder;

    .line 391
    iget-object v0, p1, Lcom/squareup/protos/client/bills/DiscountLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->discount:Lcom/squareup/api/items/Discount;

    const-string v1, "discountLineItem.write_o\u2026_backing_details.discount"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/squareup/checkout/Discount$Builder;->fromDiscountProto(Lcom/squareup/api/items/Discount;)V

    .line 392
    iget-object v0, p1, Lcom/squareup/protos/client/bills/DiscountLineItem;->discount_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/Discount$Builder;->idPair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/checkout/Adjustment$Builder;

    .line 393
    iget-object v0, p1, Lcom/squareup/protos/client/bills/DiscountLineItem;->application_scope:Lcom/squareup/protos/client/bills/ApplicationScope;

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/squareup/checkout/DiscountKt;->toScope(Lcom/squareup/protos/client/bills/ApplicationScope;)Lcom/squareup/checkout/Discount$Scope;

    move-result-object v0

    if-eqz v0, :cond_0

    move-object v1, p0

    check-cast v1, Lcom/squareup/checkout/Discount$Builder;

    invoke-virtual {v1, v0}, Lcom/squareup/checkout/Discount$Builder;->scope(Lcom/squareup/checkout/Discount$Scope;)Lcom/squareup/checkout/Discount$Builder;

    :cond_0
    if-eqz p3, :cond_2

    .line 399
    iget-object v0, p1, Lcom/squareup/protos/client/bills/DiscountLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->coupon_ids:Ljava/util/List;

    if-eqz v0, :cond_1

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->firstOrNull(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_1

    goto :goto_0

    .line 400
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/bills/DiscountLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->coupon_id:Ljava/lang/String;

    goto :goto_0

    .line 402
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/client/bills/DiscountLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->coupon_id:Ljava/lang/String;

    :goto_0
    if-eqz v0, :cond_4

    .line 406
    invoke-virtual {p0, v0}, Lcom/squareup/checkout/Discount$Builder;->couponToken(Ljava/lang/String;)Lcom/squareup/checkout/Discount$Builder;

    if-eqz p3, :cond_3

    .line 408
    invoke-virtual {p0, v0}, Lcom/squareup/checkout/Discount$Builder;->id(Ljava/lang/String;)Lcom/squareup/checkout/Adjustment$Builder;

    .line 412
    :cond_3
    iget-object p3, p1, Lcom/squareup/protos/client/bills/DiscountLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;

    iget-object p3, p3, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->discount:Lcom/squareup/api/items/Discount;

    iget-object p3, p3, Lcom/squareup/api/items/Discount;->id:Ljava/lang/String;

    const-string v0, "discountLineItem.write_o\u2026cking_details.discount.id"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, p3}, Lcom/squareup/checkout/Discount$Builder;->couponDefinitionToken(Ljava/lang/String;)Lcom/squareup/checkout/Discount$Builder;

    .line 419
    iget-object p3, p0, Lcom/squareup/checkout/Discount$Builder;->scope:Lcom/squareup/checkout/Discount$Scope;

    iget-boolean p3, p3, Lcom/squareup/checkout/Discount$Scope;->atItemScope:Z

    if-eqz p3, :cond_4

    if-eqz p2, :cond_4

    .line 420
    new-instance p3, Lcom/squareup/checkout/Discount$Matches$Items;

    invoke-static {p2}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p2

    invoke-direct {p3, p2}, Lcom/squareup/checkout/Discount$Matches$Items;-><init>(Ljava/util/List;)V

    check-cast p3, Lcom/squareup/checkout/Discount$Matches;

    invoke-virtual {p0, p3}, Lcom/squareup/checkout/Discount$Builder;->matches(Lcom/squareup/checkout/Discount$Matches;)Lcom/squareup/checkout/Discount$Builder;

    .line 424
    :cond_4
    iget-object p1, p1, Lcom/squareup/protos/client/bills/DiscountLineItem;->configuration:Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;

    const/4 p2, 0x0

    if-eqz p1, :cond_5

    .line 425
    iget-object p3, p1, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;->variable_percentage:Ljava/lang/String;

    goto :goto_1

    :cond_5
    move-object p3, p2

    :goto_1
    if-nez p3, :cond_7

    if-eqz p1, :cond_6

    iget-object p3, p1, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;->variable_amount_money:Lcom/squareup/protos/common/Money;

    goto :goto_2

    :cond_6
    move-object p3, p2

    :goto_2
    if-eqz p3, :cond_9

    .line 426
    :cond_7
    iget-object p3, p1, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;->variable_percentage:Ljava/lang/String;

    invoke-static {p3, p2}, Lcom/squareup/util/Numbers;->parsePercentage(Ljava/lang/String;Lcom/squareup/util/Percentage;)Lcom/squareup/util/Percentage;

    move-result-object p2

    invoke-virtual {p0, p2}, Lcom/squareup/checkout/Discount$Builder;->percentage(Lcom/squareup/util/Percentage;)Lcom/squareup/checkout/Adjustment$Builder;

    .line 427
    iget-object p2, p1, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;->variable_amount_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {p0, p2}, Lcom/squareup/checkout/Discount$Builder;->amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/checkout/Adjustment$Builder;

    .line 428
    iget-object p2, p1, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;->variable_percentage:Ljava/lang/String;

    if-eqz p2, :cond_8

    .line 429
    sget-object p1, Lcom/squareup/api/items/CalculationPhase;->DISCOUNT_PERCENTAGE_PHASE:Lcom/squareup/api/items/CalculationPhase;

    invoke-virtual {p0, p1}, Lcom/squareup/checkout/Discount$Builder;->phase(Lcom/squareup/api/items/CalculationPhase;)Lcom/squareup/checkout/Adjustment$Builder;

    goto :goto_3

    .line 430
    :cond_8
    iget-object p1, p1, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;->variable_amount_money:Lcom/squareup/protos/common/Money;

    if-eqz p1, :cond_9

    .line 431
    sget-object p1, Lcom/squareup/api/items/CalculationPhase;->DISCOUNT_AMOUNT_PHASE:Lcom/squareup/api/items/CalculationPhase;

    invoke-virtual {p0, p1}, Lcom/squareup/checkout/Discount$Builder;->phase(Lcom/squareup/api/items/CalculationPhase;)Lcom/squareup/checkout/Adjustment$Builder;

    :cond_9
    :goto_3
    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/coupons/Coupon;Z)V
    .locals 2

    const-string v0, "coupon"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 354
    invoke-direct {p0}, Lcom/squareup/checkout/Adjustment$Builder;-><init>()V

    const-string v0, ""

    .line 295
    iput-object v0, p0, Lcom/squareup/checkout/Discount$Builder;->color:Ljava/lang/String;

    .line 298
    sget-object v0, Lcom/squareup/protos/client/coupons/Coupon$Reason;->UNKNOWN_REASON:Lcom/squareup/protos/client/coupons/Coupon$Reason;

    iput-object v0, p0, Lcom/squareup/checkout/Discount$Builder;->couponReason:Lcom/squareup/protos/client/coupons/Coupon$Reason;

    .line 301
    sget-object v0, Lcom/squareup/api/items/Discount;->DEFAULT_DISCOUNT_TYPE:Lcom/squareup/api/items/Discount$DiscountType;

    const-string v1, "DEFAULT_DISCOUNT_TYPE"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/checkout/Discount$Builder;->discountType:Lcom/squareup/api/items/Discount$DiscountType;

    .line 302
    sget-object v0, Lcom/squareup/api/items/Discount;->DEFAULT_PIN_REQUIRED:Ljava/lang/Boolean;

    const-string v1, "DEFAULT_PIN_REQUIRED"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/checkout/Discount$Builder;->pinRequired:Z

    .line 304
    sget-object v0, Lcom/squareup/checkout/Discount$Scope;->CART:Lcom/squareup/checkout/Discount$Scope;

    iput-object v0, p0, Lcom/squareup/checkout/Discount$Builder;->scope:Lcom/squareup/checkout/Discount$Scope;

    .line 306
    sget-object v0, Lcom/squareup/checkout/Discount$Matches$AllItems;->INSTANCE:Lcom/squareup/checkout/Discount$Matches$AllItems;

    check-cast v0, Lcom/squareup/checkout/Discount$Matches;

    iput-object v0, p0, Lcom/squareup/checkout/Discount$Builder;->matches:Lcom/squareup/checkout/Discount$Matches;

    .line 354
    iget-object v0, p1, Lcom/squareup/protos/client/coupons/Coupon;->coupon_id:Ljava/lang/String;

    const-string v1, "coupon.coupon_id"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/Discount$Builder;->couponToken(Ljava/lang/String;)Lcom/squareup/checkout/Discount$Builder;

    .line 355
    invoke-static {p1}, Lcom/squareup/checkout/Discounts;->couponDefinitionToken(Lcom/squareup/protos/client/coupons/Coupon;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/Discount$Builder;->couponDefinitionToken(Ljava/lang/String;)Lcom/squareup/checkout/Discount$Builder;

    .line 356
    iget-object v0, p1, Lcom/squareup/protos/client/coupons/Coupon;->reason:Lcom/squareup/protos/client/coupons/Coupon$Reason;

    if-eqz v0, :cond_0

    move-object v1, p0

    check-cast v1, Lcom/squareup/checkout/Discount$Builder;

    invoke-virtual {v1, v0}, Lcom/squareup/checkout/Discount$Builder;->couponReason(Lcom/squareup/protos/client/coupons/Coupon$Reason;)Lcom/squareup/checkout/Discount$Builder;

    :cond_0
    if-eqz p2, :cond_1

    .line 359
    iget-object p2, p1, Lcom/squareup/protos/client/coupons/Coupon;->coupon_id:Ljava/lang/String;

    invoke-virtual {p0, p2}, Lcom/squareup/checkout/Discount$Builder;->id(Ljava/lang/String;)Lcom/squareup/checkout/Adjustment$Builder;

    goto :goto_0

    .line 361
    :cond_1
    iget-object p2, p1, Lcom/squareup/protos/client/coupons/Coupon;->discount:Lcom/squareup/api/items/Discount;

    iget-object p2, p2, Lcom/squareup/api/items/Discount;->id:Ljava/lang/String;

    invoke-virtual {p0, p2}, Lcom/squareup/checkout/Discount$Builder;->id(Ljava/lang/String;)Lcom/squareup/checkout/Adjustment$Builder;

    .line 364
    :goto_0
    iget-object p2, p1, Lcom/squareup/protos/client/coupons/Coupon;->discount:Lcom/squareup/api/items/Discount;

    iget-object p2, p2, Lcom/squareup/api/items/Discount;->name:Ljava/lang/String;

    invoke-virtual {p0, p2}, Lcom/squareup/checkout/Discount$Builder;->name(Ljava/lang/String;)Lcom/squareup/checkout/Adjustment$Builder;

    .line 365
    iget-object p2, p1, Lcom/squareup/protos/client/coupons/Coupon;->discount:Lcom/squareup/api/items/Discount;

    iget-object p2, p2, Lcom/squareup/api/items/Discount;->amount:Lcom/squareup/protos/common/dinero/Money;

    invoke-static {p2}, Lcom/squareup/shared/catalog/utils/Dineros;->toMoney(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p2

    invoke-virtual {p0, p2}, Lcom/squareup/checkout/Discount$Builder;->amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/checkout/Adjustment$Builder;

    .line 366
    iget-object p2, p1, Lcom/squareup/protos/client/coupons/Coupon;->discount:Lcom/squareup/api/items/Discount;

    iget-object p2, p2, Lcom/squareup/api/items/Discount;->maximum_amount:Lcom/squareup/protos/common/dinero/Money;

    invoke-static {p2}, Lcom/squareup/shared/catalog/utils/Dineros;->toMoney(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p2

    invoke-virtual {p0, p2}, Lcom/squareup/checkout/Discount$Builder;->maxAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/checkout/Adjustment$Builder;

    .line 367
    sget-object p2, Lcom/squareup/checkout/Discount$Scope;->CART:Lcom/squareup/checkout/Discount$Scope;

    invoke-virtual {p0, p2}, Lcom/squareup/checkout/Discount$Builder;->scope(Lcom/squareup/checkout/Discount$Scope;)Lcom/squareup/checkout/Discount$Builder;

    .line 369
    iget-object p2, p1, Lcom/squareup/protos/client/coupons/Coupon;->discount:Lcom/squareup/api/items/Discount;

    iget-object p2, p2, Lcom/squareup/api/items/Discount;->percentage:Ljava/lang/String;

    if-eqz p2, :cond_2

    .line 370
    sget-object p2, Lcom/squareup/util/Percentage;->Companion:Lcom/squareup/util/Percentage$Companion;

    iget-object v0, p1, Lcom/squareup/protos/client/coupons/Coupon;->discount:Lcom/squareup/api/items/Discount;

    iget-object v0, v0, Lcom/squareup/api/items/Discount;->percentage:Ljava/lang/String;

    const-string v1, "coupon.discount.percentage"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2, v0}, Lcom/squareup/util/Percentage$Companion;->fromString(Ljava/lang/String;)Lcom/squareup/util/Percentage;

    move-result-object p2

    invoke-virtual {p0, p2}, Lcom/squareup/checkout/Discount$Builder;->percentage(Lcom/squareup/util/Percentage;)Lcom/squareup/checkout/Adjustment$Builder;

    .line 371
    sget-object p2, Lcom/squareup/api/items/CalculationPhase;->DISCOUNT_PERCENTAGE_PHASE:Lcom/squareup/api/items/CalculationPhase;

    invoke-virtual {p0, p2}, Lcom/squareup/checkout/Discount$Builder;->phase(Lcom/squareup/api/items/CalculationPhase;)Lcom/squareup/checkout/Adjustment$Builder;

    goto :goto_1

    .line 373
    :cond_2
    sget-object p2, Lcom/squareup/api/items/CalculationPhase;->DISCOUNT_AMOUNT_PHASE:Lcom/squareup/api/items/CalculationPhase;

    invoke-virtual {p0, p2}, Lcom/squareup/checkout/Discount$Builder;->phase(Lcom/squareup/api/items/CalculationPhase;)Lcom/squareup/checkout/Adjustment$Builder;

    .line 376
    :goto_1
    invoke-direct {p0, p1}, Lcom/squareup/checkout/Discount$Builder;->eligibleItemCategoryAndVariationIds(Lcom/squareup/protos/client/coupons/Coupon;)Lcom/squareup/checkout/Discount$Builder;

    .line 377
    iget-object p1, p1, Lcom/squareup/protos/client/coupons/Coupon;->discount:Lcom/squareup/api/items/Discount;

    const-string p2, "coupon.discount"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/checkout/Discount$Builder;->discountProto:Lcom/squareup/api/items/Discount;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/shared/catalog/models/CatalogDiscount;)V
    .locals 2

    const-string v0, "catalogDiscount"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 381
    invoke-direct {p0}, Lcom/squareup/checkout/Adjustment$Builder;-><init>()V

    const-string v0, ""

    .line 295
    iput-object v0, p0, Lcom/squareup/checkout/Discount$Builder;->color:Ljava/lang/String;

    .line 298
    sget-object v0, Lcom/squareup/protos/client/coupons/Coupon$Reason;->UNKNOWN_REASON:Lcom/squareup/protos/client/coupons/Coupon$Reason;

    iput-object v0, p0, Lcom/squareup/checkout/Discount$Builder;->couponReason:Lcom/squareup/protos/client/coupons/Coupon$Reason;

    .line 301
    sget-object v0, Lcom/squareup/api/items/Discount;->DEFAULT_DISCOUNT_TYPE:Lcom/squareup/api/items/Discount$DiscountType;

    const-string v1, "DEFAULT_DISCOUNT_TYPE"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/checkout/Discount$Builder;->discountType:Lcom/squareup/api/items/Discount$DiscountType;

    .line 302
    sget-object v0, Lcom/squareup/api/items/Discount;->DEFAULT_PIN_REQUIRED:Ljava/lang/Boolean;

    const-string v1, "DEFAULT_PIN_REQUIRED"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/checkout/Discount$Builder;->pinRequired:Z

    .line 304
    sget-object v0, Lcom/squareup/checkout/Discount$Scope;->CART:Lcom/squareup/checkout/Discount$Scope;

    iput-object v0, p0, Lcom/squareup/checkout/Discount$Builder;->scope:Lcom/squareup/checkout/Discount$Scope;

    .line 306
    sget-object v0, Lcom/squareup/checkout/Discount$Matches$AllItems;->INSTANCE:Lcom/squareup/checkout/Discount$Matches$AllItems;

    check-cast v0, Lcom/squareup/checkout/Discount$Matches;

    iput-object v0, p0, Lcom/squareup/checkout/Discount$Builder;->matches:Lcom/squareup/checkout/Discount$Matches;

    .line 381
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    invoke-virtual {p0, v0}, Lcom/squareup/checkout/Discount$Builder;->createdAt(Ljava/util/Date;)Lcom/squareup/checkout/Adjustment$Builder;

    .line 382
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->object()Lcom/squareup/wire/Message;

    move-result-object p1

    const-string v0, "catalogDiscount.`object`()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/api/items/Discount;

    invoke-direct {p0, p1}, Lcom/squareup/checkout/Discount$Builder;->fromDiscountProto(Lcom/squareup/api/items/Discount;)V

    return-void
.end method

.method public static final synthetic access$getDiscountProto$p(Lcom/squareup/checkout/Discount$Builder;)Lcom/squareup/api/items/Discount;
    .locals 1

    .line 294
    iget-object p0, p0, Lcom/squareup/checkout/Discount$Builder;->discountProto:Lcom/squareup/api/items/Discount;

    if-nez p0, :cond_0

    const-string v0, "discountProto"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$setDiscountProto$p(Lcom/squareup/checkout/Discount$Builder;Lcom/squareup/api/items/Discount;)V
    .locals 0

    .line 294
    iput-object p1, p0, Lcom/squareup/checkout/Discount$Builder;->discountProto:Lcom/squareup/api/items/Discount;

    return-void
.end method

.method private final eligibleItemCategoryAndVariationIds(Lcom/squareup/protos/client/coupons/Coupon;)Lcom/squareup/checkout/Discount$Builder;
    .locals 5

    .line 492
    move-object v0, p0

    check-cast v0, Lcom/squareup/checkout/Discount$Builder;

    .line 493
    iget-object v1, p1, Lcom/squareup/protos/client/coupons/Coupon;->reward:Lcom/squareup/protos/client/coupons/CouponReward;

    if-eqz v1, :cond_0

    iget-object v1, v1, Lcom/squareup/protos/client/coupons/CouponReward;->scope:Lcom/squareup/protos/client/coupons/Scope;

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    sget-object v2, Lcom/squareup/protos/client/coupons/Scope;->ITEM:Lcom/squareup/protos/client/coupons/Scope;

    if-ne v1, v2, :cond_8

    .line 494
    iget-object v1, p1, Lcom/squareup/protos/client/coupons/Coupon;->reward:Lcom/squareup/protos/client/coupons/CouponReward;

    iget-object v1, v1, Lcom/squareup/protos/client/coupons/CouponReward;->item_constraint:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ne v1, v3, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_7

    .line 498
    iget-object v1, p1, Lcom/squareup/protos/client/coupons/Coupon;->reward:Lcom/squareup/protos/client/coupons/CouponReward;

    iget-object v1, v1, Lcom/squareup/protos/client/coupons/CouponReward;->item_constraint:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/coupons/ItemConstraint;

    .line 499
    iget-object v4, v1, Lcom/squareup/protos/client/coupons/ItemConstraint;->quantity:Ljava/lang/Integer;

    if-nez v4, :cond_2

    goto :goto_2

    :cond_2
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-ne v4, v3, :cond_3

    const/4 v2, 0x1

    :cond_3
    :goto_2
    if-eqz v2, :cond_6

    .line 503
    iget-object p1, p1, Lcom/squareup/protos/client/coupons/Coupon;->item_constraint_type:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    if-eqz p1, :cond_5

    sget-object v2, Lcom/squareup/checkout/Discount$Builder$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;->ordinal()I

    move-result p1

    aget p1, v2, p1

    const-string v2, "constraint.constraint_id"

    if-eq p1, v3, :cond_4

    const/4 v3, 0x2

    if-ne p1, v3, :cond_5

    .line 505
    new-instance p1, Lcom/squareup/checkout/Discount$Matches$Items;

    iget-object v1, v1, Lcom/squareup/protos/client/coupons/ItemConstraint;->constraint_id:Ljava/util/List;

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p1, v1}, Lcom/squareup/checkout/Discount$Matches$Items;-><init>(Ljava/util/List;)V

    check-cast p1, Lcom/squareup/checkout/Discount$Matches;

    invoke-virtual {v0, p1}, Lcom/squareup/checkout/Discount$Builder;->matches(Lcom/squareup/checkout/Discount$Matches;)Lcom/squareup/checkout/Discount$Builder;

    goto :goto_3

    .line 504
    :cond_4
    new-instance p1, Lcom/squareup/checkout/Discount$Matches$Categories;

    iget-object v1, v1, Lcom/squareup/protos/client/coupons/ItemConstraint;->constraint_id:Ljava/util/List;

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p1, v1}, Lcom/squareup/checkout/Discount$Matches$Categories;-><init>(Ljava/util/List;)V

    check-cast p1, Lcom/squareup/checkout/Discount$Matches;

    invoke-virtual {v0, p1}, Lcom/squareup/checkout/Discount$Builder;->matches(Lcom/squareup/checkout/Discount$Matches;)Lcom/squareup/checkout/Discount$Builder;

    goto :goto_3

    .line 506
    :cond_5
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Unexpected item_constraint_type"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 499
    :cond_6
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Item constraint quantity must be 1."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 494
    :cond_7
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "ITEM-scoped coupon reward must have exactly 1 item constraint."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :cond_8
    :goto_3
    return-object v0
.end method

.method private final fromDiscountProto(Lcom/squareup/api/items/Discount;)V
    .locals 3

    .line 437
    iput-object p1, p0, Lcom/squareup/checkout/Discount$Builder;->discountProto:Lcom/squareup/api/items/Discount;

    .line 438
    iget-object p1, p0, Lcom/squareup/checkout/Discount$Builder;->discountProto:Lcom/squareup/api/items/Discount;

    const-string v0, "discountProto"

    if-nez p1, :cond_0

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object p1, p1, Lcom/squareup/api/items/Discount;->percentage:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz p1, :cond_1

    invoke-static {p1, v1}, Lcom/squareup/util/Numbers;->parsePercentage(Ljava/lang/String;Lcom/squareup/util/Percentage;)Lcom/squareup/util/Percentage;

    move-result-object v1

    .line 440
    :cond_1
    iget-object p1, p0, Lcom/squareup/checkout/Discount$Builder;->discountProto:Lcom/squareup/api/items/Discount;

    if-nez p1, :cond_2

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    iget-object p1, p1, Lcom/squareup/api/items/Discount;->id:Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/squareup/checkout/Discount$Builder;->id(Ljava/lang/String;)Lcom/squareup/checkout/Adjustment$Builder;

    .line 441
    iget-object p1, p0, Lcom/squareup/checkout/Discount$Builder;->discountProto:Lcom/squareup/api/items/Discount;

    if-nez p1, :cond_3

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    iget-object p1, p1, Lcom/squareup/api/items/Discount;->color:Ljava/lang/String;

    if-eqz p1, :cond_4

    move-object v2, p0

    check-cast v2, Lcom/squareup/checkout/Discount$Builder;

    invoke-virtual {v2, p1}, Lcom/squareup/checkout/Discount$Builder;->color(Ljava/lang/String;)Lcom/squareup/checkout/Discount$Builder;

    .line 442
    :cond_4
    iget-object p1, p0, Lcom/squareup/checkout/Discount$Builder;->discountProto:Lcom/squareup/api/items/Discount;

    if-nez p1, :cond_5

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    iget-object p1, p1, Lcom/squareup/api/items/Discount;->pin_required:Ljava/lang/Boolean;

    if-eqz p1, :cond_6

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    move-object v2, p0

    check-cast v2, Lcom/squareup/checkout/Discount$Builder;

    invoke-virtual {v2, p1}, Lcom/squareup/checkout/Discount$Builder;->pinRequired(Z)Lcom/squareup/checkout/Discount$Builder;

    .line 443
    :cond_6
    iget-object p1, p0, Lcom/squareup/checkout/Discount$Builder;->discountProto:Lcom/squareup/api/items/Discount;

    if-nez p1, :cond_7

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    iget-object p1, p1, Lcom/squareup/api/items/Discount;->discount_type:Lcom/squareup/api/items/Discount$DiscountType;

    if-eqz p1, :cond_8

    move-object v2, p0

    check-cast v2, Lcom/squareup/checkout/Discount$Builder;

    invoke-virtual {v2, p1}, Lcom/squareup/checkout/Discount$Builder;->discountType(Lcom/squareup/api/items/Discount$DiscountType;)Lcom/squareup/checkout/Discount$Builder;

    .line 444
    :cond_8
    iget-object p1, p0, Lcom/squareup/checkout/Discount$Builder;->discountProto:Lcom/squareup/api/items/Discount;

    if-nez p1, :cond_9

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    iget-object p1, p1, Lcom/squareup/api/items/Discount;->application_method:Lcom/squareup/api/items/Discount$ApplicationMethod;

    invoke-virtual {p0, p1}, Lcom/squareup/checkout/Discount$Builder;->applicationMethod(Lcom/squareup/api/items/Discount$ApplicationMethod;)Lcom/squareup/checkout/Discount$Builder;

    .line 445
    iget-object p1, p0, Lcom/squareup/checkout/Discount$Builder;->discountProto:Lcom/squareup/api/items/Discount;

    if-nez p1, :cond_a

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_a
    iget-object p1, p1, Lcom/squareup/api/items/Discount;->name:Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/squareup/checkout/Discount$Builder;->name(Ljava/lang/String;)Lcom/squareup/checkout/Adjustment$Builder;

    .line 446
    iget-object p1, p0, Lcom/squareup/checkout/Discount$Builder;->discountProto:Lcom/squareup/api/items/Discount;

    if-nez p1, :cond_b

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_b
    iget-object p1, p1, Lcom/squareup/api/items/Discount;->amount:Lcom/squareup/protos/common/dinero/Money;

    invoke-static {p1}, Lcom/squareup/shared/catalog/utils/Dineros;->toMoney(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/checkout/Discount$Builder;->amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/checkout/Adjustment$Builder;

    .line 447
    iget-object p1, p0, Lcom/squareup/checkout/Discount$Builder;->discountProto:Lcom/squareup/api/items/Discount;

    if-nez p1, :cond_c

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_c
    iget-object p1, p1, Lcom/squareup/api/items/Discount;->maximum_amount:Lcom/squareup/protos/common/dinero/Money;

    invoke-static {p1}, Lcom/squareup/shared/catalog/utils/Dineros;->toMoney(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/checkout/Discount$Builder;->maxAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/checkout/Adjustment$Builder;

    .line 448
    invoke-virtual {p0, v1}, Lcom/squareup/checkout/Discount$Builder;->percentage(Lcom/squareup/util/Percentage;)Lcom/squareup/checkout/Adjustment$Builder;

    .line 449
    iget-object p1, p0, Lcom/squareup/checkout/Discount$Builder;->discountProto:Lcom/squareup/api/items/Discount;

    if-nez p1, :cond_d

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_d
    iget-object p1, p1, Lcom/squareup/api/items/Discount;->percentage:Ljava/lang/String;

    if-eqz p1, :cond_e

    .line 450
    sget-object p1, Lcom/squareup/api/items/CalculationPhase;->DISCOUNT_PERCENTAGE_PHASE:Lcom/squareup/api/items/CalculationPhase;

    invoke-virtual {p0, p1}, Lcom/squareup/checkout/Discount$Builder;->phase(Lcom/squareup/api/items/CalculationPhase;)Lcom/squareup/checkout/Adjustment$Builder;

    goto :goto_0

    .line 452
    :cond_e
    sget-object p1, Lcom/squareup/api/items/CalculationPhase;->DISCOUNT_AMOUNT_PHASE:Lcom/squareup/api/items/CalculationPhase;

    invoke-virtual {p0, p1}, Lcom/squareup/checkout/Discount$Builder;->phase(Lcom/squareup/api/items/CalculationPhase;)Lcom/squareup/checkout/Adjustment$Builder;

    .line 454
    :goto_0
    iget-object p1, p0, Lcom/squareup/checkout/Discount$Builder;->discountProto:Lcom/squareup/api/items/Discount;

    if-nez p1, :cond_f

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_f
    iget-object p1, p1, Lcom/squareup/api/items/Discount;->modify_tax_basis:Lcom/squareup/api/items/Discount$ModifyTaxBasis;

    invoke-virtual {p0, p1}, Lcom/squareup/checkout/Discount$Builder;->taxBasis(Lcom/squareup/api/items/Discount$ModifyTaxBasis;)Lcom/squareup/checkout/Adjustment$Builder;

    return-void
.end method


# virtual methods
.method public final applicationMethod(Lcom/squareup/api/items/Discount$ApplicationMethod;)Lcom/squareup/checkout/Discount$Builder;
    .locals 2

    .line 475
    move-object v0, p0

    check-cast v0, Lcom/squareup/checkout/Discount$Builder;

    .line 476
    iput-object p1, v0, Lcom/squareup/checkout/Discount$Builder;->applicationMethod:Lcom/squareup/api/items/Discount$ApplicationMethod;

    .line 477
    sget-object v1, Lcom/squareup/api/items/Discount$ApplicationMethod;->COMP:Lcom/squareup/api/items/Discount$ApplicationMethod;

    if-ne p1, v1, :cond_0

    .line 478
    sget-object p1, Lcom/squareup/calc/constants/CalculationPriority;->HIGH:Lcom/squareup/calc/constants/CalculationPriority;

    invoke-virtual {v0, p1}, Lcom/squareup/checkout/Discount$Builder;->priority(Lcom/squareup/calc/constants/CalculationPriority;)Lcom/squareup/checkout/Adjustment$Builder;

    :cond_0
    return-object v0
.end method

.method public final build()Lcom/squareup/checkout/Discount;
    .locals 13

    .line 513
    iget-object v0, p0, Lcom/squareup/checkout/Discount$Builder;->percentage:Lcom/squareup/util/Percentage;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/checkout/Discount$Builder;->amount:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_7

    .line 518
    move-object v0, p0

    check-cast v0, Lcom/squareup/checkout/Discount$Builder;

    iget-object v0, v0, Lcom/squareup/checkout/Discount$Builder;->discountProto:Lcom/squareup/api/items/Discount;

    if-nez v0, :cond_5

    .line 519
    new-instance v0, Lcom/squareup/api/items/Discount$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/Discount$Builder;-><init>()V

    .line 520
    iget-object v1, p0, Lcom/squareup/checkout/Discount$Builder;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/Discount$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/Discount$Builder;

    move-result-object v0

    .line 521
    iget-object v1, p0, Lcom/squareup/checkout/Discount$Builder;->color:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/Discount$Builder;->color(Ljava/lang/String;)Lcom/squareup/api/items/Discount$Builder;

    move-result-object v0

    .line 522
    iget-object v1, p0, Lcom/squareup/checkout/Discount$Builder;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/Discount$Builder;->name(Ljava/lang/String;)Lcom/squareup/api/items/Discount$Builder;

    move-result-object v0

    .line 523
    iget-object v1, p0, Lcom/squareup/checkout/Discount$Builder;->percentage:Lcom/squareup/util/Percentage;

    const/4 v2, 0x0

    if-nez v1, :cond_2

    move-object v1, v2

    goto :goto_2

    :cond_2
    iget-object v1, p0, Lcom/squareup/checkout/Discount$Builder;->percentage:Lcom/squareup/util/Percentage;

    invoke-virtual {v1}, Lcom/squareup/util/Percentage;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_2
    invoke-virtual {v0, v1}, Lcom/squareup/api/items/Discount$Builder;->percentage(Ljava/lang/String;)Lcom/squareup/api/items/Discount$Builder;

    move-result-object v0

    .line 524
    iget-object v1, p0, Lcom/squareup/checkout/Discount$Builder;->amount:Lcom/squareup/protos/common/Money;

    if-nez v1, :cond_3

    move-object v1, v2

    goto :goto_3

    :cond_3
    iget-object v1, p0, Lcom/squareup/checkout/Discount$Builder;->amount:Lcom/squareup/protos/common/Money;

    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/Dineros;->toDinero(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/dinero/Money;

    move-result-object v1

    :goto_3
    invoke-virtual {v0, v1}, Lcom/squareup/api/items/Discount$Builder;->amount(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/api/items/Discount$Builder;

    move-result-object v0

    .line 525
    iget-object v1, p0, Lcom/squareup/checkout/Discount$Builder;->maxAmount:Lcom/squareup/protos/common/Money;

    if-nez v1, :cond_4

    goto :goto_4

    :cond_4
    iget-object v1, p0, Lcom/squareup/checkout/Discount$Builder;->maxAmount:Lcom/squareup/protos/common/Money;

    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/Dineros;->toDinero(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/dinero/Money;

    move-result-object v2

    :goto_4
    invoke-virtual {v0, v2}, Lcom/squareup/api/items/Discount$Builder;->maximum_amount(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/api/items/Discount$Builder;

    move-result-object v0

    .line 526
    iget-boolean v1, p0, Lcom/squareup/checkout/Discount$Builder;->pinRequired:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/Discount$Builder;->pin_required(Ljava/lang/Boolean;)Lcom/squareup/api/items/Discount$Builder;

    move-result-object v0

    .line 527
    iget-object v1, p0, Lcom/squareup/checkout/Discount$Builder;->discountType:Lcom/squareup/api/items/Discount$DiscountType;

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/Discount$Builder;->discount_type(Lcom/squareup/api/items/Discount$DiscountType;)Lcom/squareup/api/items/Discount$Builder;

    move-result-object v0

    .line 528
    iget-object v1, p0, Lcom/squareup/checkout/Discount$Builder;->taxBasis:Lcom/squareup/api/items/Discount$ModifyTaxBasis;

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/Discount$Builder;->modify_tax_basis(Lcom/squareup/api/items/Discount$ModifyTaxBasis;)Lcom/squareup/api/items/Discount$Builder;

    move-result-object v0

    .line 529
    invoke-virtual {v0}, Lcom/squareup/api/items/Discount$Builder;->build()Lcom/squareup/api/items/Discount;

    move-result-object v0

    const-string v1, "com.squareup.api.items.D\u2026sis)\n            .build()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/checkout/Discount$Builder;->discountProto:Lcom/squareup/api/items/Discount;

    .line 532
    :cond_5
    new-instance v0, Lcom/squareup/checkout/Discount;

    .line 534
    iget-object v11, p0, Lcom/squareup/checkout/Discount$Builder;->discountProto:Lcom/squareup/api/items/Discount;

    if-nez v11, :cond_6

    const-string v1, "discountProto"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 535
    :cond_6
    iget-object v7, p0, Lcom/squareup/checkout/Discount$Builder;->scope:Lcom/squareup/checkout/Discount$Scope;

    .line 536
    iget-object v4, p0, Lcom/squareup/checkout/Discount$Builder;->couponToken:Ljava/lang/String;

    .line 537
    iget-object v5, p0, Lcom/squareup/checkout/Discount$Builder;->couponDefinitionToken:Ljava/lang/String;

    .line 538
    iget-object v6, p0, Lcom/squareup/checkout/Discount$Builder;->couponReason:Lcom/squareup/protos/client/coupons/Coupon$Reason;

    .line 539
    iget-object v9, p0, Lcom/squareup/checkout/Discount$Builder;->applicationMethod:Lcom/squareup/api/items/Discount$ApplicationMethod;

    .line 540
    iget-boolean v10, p0, Lcom/squareup/checkout/Discount$Builder;->recalledFromTicket:Z

    .line 541
    iget-object v8, p0, Lcom/squareup/checkout/Discount$Builder;->matches:Lcom/squareup/checkout/Discount$Matches;

    const/4 v12, 0x0

    move-object v2, v0

    move-object v3, p0

    .line 532
    invoke-direct/range {v2 .. v12}, Lcom/squareup/checkout/Discount;-><init>(Lcom/squareup/checkout/Discount$Builder;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/coupons/Coupon$Reason;Lcom/squareup/checkout/Discount$Scope;Lcom/squareup/checkout/Discount$Matches;Lcom/squareup/api/items/Discount$ApplicationMethod;ZLcom/squareup/api/items/Discount;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0

    .line 513
    :cond_7
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Discounts must have a percentage, fixed amount, or both"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public final color(Ljava/lang/String;)Lcom/squareup/checkout/Discount$Builder;
    .locals 1

    const-string v0, "color"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 463
    move-object v0, p0

    check-cast v0, Lcom/squareup/checkout/Discount$Builder;

    iput-object p1, v0, Lcom/squareup/checkout/Discount$Builder;->color:Ljava/lang/String;

    return-object v0
.end method

.method public final couponDefinitionToken(Ljava/lang/String;)Lcom/squareup/checkout/Discount$Builder;
    .locals 1

    const-string v0, "token"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 486
    move-object v0, p0

    check-cast v0, Lcom/squareup/checkout/Discount$Builder;

    iput-object p1, v0, Lcom/squareup/checkout/Discount$Builder;->couponDefinitionToken:Ljava/lang/String;

    return-object v0
.end method

.method public final couponReason(Lcom/squareup/protos/client/coupons/Coupon$Reason;)Lcom/squareup/checkout/Discount$Builder;
    .locals 1

    const-string v0, "couponReason"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 488
    move-object v0, p0

    check-cast v0, Lcom/squareup/checkout/Discount$Builder;

    .line 489
    iput-object p1, v0, Lcom/squareup/checkout/Discount$Builder;->couponReason:Lcom/squareup/protos/client/coupons/Coupon$Reason;

    return-object v0
.end method

.method public final couponToken(Ljava/lang/String;)Lcom/squareup/checkout/Discount$Builder;
    .locals 1

    const-string v0, "couponToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 484
    move-object v0, p0

    check-cast v0, Lcom/squareup/checkout/Discount$Builder;

    iput-object p1, v0, Lcom/squareup/checkout/Discount$Builder;->couponToken:Ljava/lang/String;

    return-object v0
.end method

.method public final discountType(Lcom/squareup/api/items/Discount$DiscountType;)Lcom/squareup/checkout/Discount$Builder;
    .locals 1

    const-string v0, "discountType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 459
    move-object v0, p0

    check-cast v0, Lcom/squareup/checkout/Discount$Builder;

    .line 460
    iput-object p1, v0, Lcom/squareup/checkout/Discount$Builder;->discountType:Lcom/squareup/api/items/Discount$DiscountType;

    return-object v0
.end method

.method public bridge synthetic inclusionType(Lcom/squareup/api/items/Fee$InclusionType;)Lcom/squareup/checkout/Adjustment$Builder;
    .locals 0

    .line 294
    invoke-virtual {p0, p1}, Lcom/squareup/checkout/Discount$Builder;->inclusionType(Lcom/squareup/api/items/Fee$InclusionType;)Lcom/squareup/checkout/Discount$Builder;

    move-result-object p1

    check-cast p1, Lcom/squareup/checkout/Adjustment$Builder;

    return-object p1
.end method

.method public inclusionType(Lcom/squareup/api/items/Fee$InclusionType;)Lcom/squareup/checkout/Discount$Builder;
    .locals 2

    const-string v0, "inclusionType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 471
    move-object v0, p0

    check-cast v0, Lcom/squareup/checkout/Discount$Builder;

    .line 472
    iget-object v1, v0, Lcom/squareup/checkout/Discount$Builder;->inclusionType:Lcom/squareup/api/items/Fee$InclusionType;

    if-ne v1, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    return-object v0

    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Cannot change Discount\'s inclusion type."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public final matches(Lcom/squareup/checkout/Discount$Matches;)Lcom/squareup/checkout/Discount$Builder;
    .locals 1

    const-string v0, "matches"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 482
    move-object v0, p0

    check-cast v0, Lcom/squareup/checkout/Discount$Builder;

    iput-object p1, v0, Lcom/squareup/checkout/Discount$Builder;->matches:Lcom/squareup/checkout/Discount$Matches;

    return-object v0
.end method

.method public final pinRequired(Z)Lcom/squareup/checkout/Discount$Builder;
    .locals 1

    .line 457
    move-object v0, p0

    check-cast v0, Lcom/squareup/checkout/Discount$Builder;

    iput-boolean p1, v0, Lcom/squareup/checkout/Discount$Builder;->pinRequired:Z

    return-object v0
.end method

.method public final recalledFromTicket(Z)Lcom/squareup/checkout/Discount$Builder;
    .locals 1

    .line 467
    move-object v0, p0

    check-cast v0, Lcom/squareup/checkout/Discount$Builder;

    .line 468
    iput-boolean p1, v0, Lcom/squareup/checkout/Discount$Builder;->recalledFromTicket:Z

    return-object v0
.end method

.method public final scope(Lcom/squareup/checkout/Discount$Scope;)Lcom/squareup/checkout/Discount$Builder;
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 465
    move-object v0, p0

    check-cast v0, Lcom/squareup/checkout/Discount$Builder;

    iput-object p1, v0, Lcom/squareup/checkout/Discount$Builder;->scope:Lcom/squareup/checkout/Discount$Scope;

    return-object v0
.end method
