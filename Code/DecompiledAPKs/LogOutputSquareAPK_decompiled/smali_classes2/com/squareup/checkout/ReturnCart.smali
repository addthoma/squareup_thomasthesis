.class public final Lcom/squareup/checkout/ReturnCart;
.super Ljava/lang/Object;
.source "ReturnCart.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkout/ReturnCart$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nReturnCart.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ReturnCart.kt\ncom/squareup/checkout/ReturnCart\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 Maps.kt\nkotlin/collections/MapsKt__MapsKt\n*L\n1#1,206:1\n1360#2:207\n1429#2,3:208\n1265#2,12:211\n1288#2:223\n1313#2,3:224\n1316#2,3:234\n1360#2:237\n1429#2,2:238\n1360#2:240\n1429#2,3:241\n1431#2:244\n1360#2:245\n1429#2,3:246\n1360#2:249\n1429#2,3:250\n1360#2:253\n1429#2,3:254\n1265#2,12:257\n1288#2:269\n1313#2,3:270\n1316#2,3:280\n1360#2:283\n1429#2,2:284\n1360#2:286\n1429#2,3:287\n1431#2:290\n704#2:291\n777#2,2:292\n1360#2:294\n1429#2,3:295\n704#2:298\n777#2,2:299\n1360#2:301\n1429#2,3:302\n1360#2:305\n1429#2,3:306\n1360#2:309\n1429#2,3:310\n1587#2,3:313\n1360#2:316\n1429#2,3:317\n347#3,7:227\n347#3,7:273\n*E\n*S KotlinDebug\n*F\n+ 1 ReturnCart.kt\ncom/squareup/checkout/ReturnCart\n*L\n31#1:207\n31#1,3:208\n37#1,12:211\n46#1:223\n46#1,3:224\n46#1,3:234\n48#1:237\n48#1,2:238\n48#1:240\n48#1,3:241\n48#1:244\n60#1:245\n60#1,3:246\n65#1:249\n65#1,3:250\n71#1:253\n71#1,3:254\n77#1,12:257\n86#1:269\n86#1,3:270\n86#1,3:280\n88#1:283\n88#1,2:284\n88#1:286\n88#1,3:287\n88#1:290\n100#1:291\n100#1,2:292\n101#1:294\n101#1,3:295\n106#1:298\n106#1,2:299\n114#1:301\n114#1,3:302\n116#1:305\n116#1,3:306\n133#1:309\n133#1,3:310\n134#1,3:313\n138#1:316\n138#1,3:317\n46#1,7:227\n86#1,7:273\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000z\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\r\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0008\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u0000 @2\u00020\u0001:\u0001@BI\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u000e\u0008\u0002\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u0012\u000e\u0008\u0002\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\t\u0012\n\u0008\u0002\u0010\r\u001a\u0004\u0018\u00010\u000e\u00a2\u0006\u0002\u0010\u000fJ\u000c\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u00020\u001c0\tJ\t\u0010\u001d\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001e\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001f\u001a\u00020\u0007H\u00c6\u0003J\u000f\u0010 \u001a\u0008\u0012\u0004\u0012\u00020\n0\tH\u00c6\u0003J\u000f\u0010!\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\tH\u00c6\u0003J\u000b\u0010\"\u001a\u0004\u0018\u00010\u000eH\u00c6\u0003JS\u0010#\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u000e\u0008\u0002\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t2\u000e\u0008\u0002\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\t2\n\u0008\u0002\u0010\r\u001a\u0004\u0018\u00010\u000eH\u00c6\u0001J\u0013\u0010$\u001a\u00020%2\u0008\u0010&\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\u000c\u0010\'\u001a\u0008\u0012\u0004\u0012\u00020(0\tJ\u0006\u0010)\u001a\u00020\u0007J\u000c\u0010*\u001a\u0008\u0012\u0004\u0012\u00020+0\tJ\u0006\u0010,\u001a\u00020\u0007J\u0014\u0010-\u001a\u0010\u0012\u000c\u0012\n /*\u0004\u0018\u00010.0.0\tJ\u000c\u00100\u001a\u0008\u0012\u0004\u0012\u0002010\tJ\u000c\u00102\u001a\u0008\u0012\u0004\u0012\u0002030\tJ\u000c\u00104\u001a\u0008\u0012\u0004\u0012\u00020(0\tJ\u0008\u00105\u001a\u0004\u0018\u00010\u0007J\u0006\u00106\u001a\u00020\u0007J\t\u00107\u001a\u000208H\u00d6\u0001J\u001c\u00109\u001a\u0008\u0012\u0004\u0012\u00020+0\t2\u000c\u0010:\u001a\u0008\u0012\u0004\u0012\u00020+0\tH\u0002J\u001c\u0010;\u001a\u0008\u0012\u0004\u0012\u00020(0\t2\u000c\u0010<\u001a\u0008\u0012\u0004\u0012\u00020(0\tH\u0002J\u0006\u0010=\u001a\u00020>J\t\u0010?\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013R\u0017\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015R\u0017\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0015R\u0013\u0010\r\u001a\u0004\u0018\u00010\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0018R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u001a\u00a8\u0006A"
    }
    d2 = {
        "Lcom/squareup/checkout/ReturnCart;",
        "",
        "billServerId",
        "",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "totalReturnAmount",
        "Lcom/squareup/protos/common/Money;",
        "returnItems",
        "",
        "Lcom/squareup/checkout/ReturnCartItem;",
        "returnTips",
        "Lcom/squareup/checkout/ReturnTip;",
        "roundingAdjustment",
        "Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;",
        "(Ljava/lang/String;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;)V",
        "getBillServerId",
        "()Ljava/lang/String;",
        "getCurrencyCode",
        "()Lcom/squareup/protos/common/CurrencyCode;",
        "getReturnItems",
        "()Ljava/util/List;",
        "getReturnTips",
        "getRoundingAdjustment",
        "()Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;",
        "getTotalReturnAmount",
        "()Lcom/squareup/protos/common/Money;",
        "buildTopLevelFeeLineItems",
        "Lcom/squareup/protos/client/bills/FeeLineItem;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "copy",
        "equals",
        "",
        "other",
        "getAdditiveReturnTaxes",
        "Lcom/squareup/checkout/ReturnTax;",
        "getAdditiveReturnTaxesAmount",
        "getReturnDiscounts",
        "Lcom/squareup/checkout/ReturnDiscount;",
        "getReturnDiscountsAmount",
        "getReturnDiscountsAsAdjustments",
        "Lcom/squareup/payment/OrderAdjustment;",
        "kotlin.jvm.PlatformType",
        "getReturnDiscountsAsDiscounts",
        "Lcom/squareup/checkout/Discount;",
        "getReturnItemsAsCartItems",
        "Lcom/squareup/checkout/CartItem;",
        "getReturnTaxes",
        "getSwedishRoundingAdjustment",
        "getTipAmount",
        "hashCode",
        "",
        "rollUpDiscounts",
        "allReturnDiscounts",
        "rollUpTaxes",
        "allReturnTaxes",
        "toReturnLineItems",
        "Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;",
        "toString",
        "Companion",
        "checkout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/checkout/ReturnCart$Companion;


# instance fields
.field private final billServerId:Ljava/lang/String;

.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final returnItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/checkout/ReturnCartItem;",
            ">;"
        }
    .end annotation
.end field

.field private final returnTips:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/checkout/ReturnTip;",
            ">;"
        }
    .end annotation
.end field

.field private final roundingAdjustment:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

.field private final totalReturnAmount:Lcom/squareup/protos/common/Money;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/checkout/ReturnCart$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/checkout/ReturnCart$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/checkout/ReturnCart;->Companion:Lcom/squareup/checkout/ReturnCart$Companion;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/ReturnCartItem;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/ReturnTip;",
            ">;",
            "Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;",
            ")V"
        }
    .end annotation

    const-string v0, "billServerId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "totalReturnAmount"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "returnItems"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "returnTips"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkout/ReturnCart;->billServerId:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/checkout/ReturnCart;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iput-object p3, p0, Lcom/squareup/checkout/ReturnCart;->totalReturnAmount:Lcom/squareup/protos/common/Money;

    iput-object p4, p0, Lcom/squareup/checkout/ReturnCart;->returnItems:Ljava/util/List;

    iput-object p5, p0, Lcom/squareup/checkout/ReturnCart;->returnTips:Ljava/util/List;

    iput-object p6, p0, Lcom/squareup/checkout/ReturnCart;->roundingAdjustment:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 7

    and-int/lit8 p8, p7, 0x8

    if-eqz p8, :cond_0

    .line 26
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p4

    :cond_0
    move-object v4, p4

    and-int/lit8 p4, p7, 0x10

    if-eqz p4, :cond_1

    .line 27
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p5

    :cond_1
    move-object v5, p5

    and-int/lit8 p4, p7, 0x20

    if-eqz p4, :cond_2

    const/4 p4, 0x0

    .line 28
    move-object p6, p4

    check-cast p6, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    :cond_2
    move-object v6, p6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v6}, Lcom/squareup/checkout/ReturnCart;-><init>(Ljava/lang/String;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/checkout/ReturnCart;Ljava/lang/String;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;ILjava/lang/Object;)Lcom/squareup/checkout/ReturnCart;
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    iget-object p1, p0, Lcom/squareup/checkout/ReturnCart;->billServerId:Ljava/lang/String;

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    iget-object p2, p0, Lcom/squareup/checkout/ReturnCart;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    :cond_1
    move-object p8, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/checkout/ReturnCart;->totalReturnAmount:Lcom/squareup/protos/common/Money;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/checkout/ReturnCart;->returnItems:Ljava/util/List;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/checkout/ReturnCart;->returnTips:Ljava/util/List;

    :cond_4
    move-object v2, p5

    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_5

    iget-object p6, p0, Lcom/squareup/checkout/ReturnCart;->roundingAdjustment:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    :cond_5
    move-object v3, p6

    move-object p2, p0

    move-object p3, p1

    move-object p4, p8

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    move-object p8, v3

    invoke-virtual/range {p2 .. p8}, Lcom/squareup/checkout/ReturnCart;->copy(Ljava/lang/String;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;)Lcom/squareup/checkout/ReturnCart;

    move-result-object p0

    return-object p0
.end method

.method public static final fromBill(Lcom/squareup/protos/client/bills/Bill;Lcom/squareup/util/Res;Lcom/squareup/checkout/OrderVariationNamer;Z)Lcom/squareup/checkout/ReturnCart;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/checkout/ReturnCart;->Companion:Lcom/squareup/checkout/ReturnCart$Companion;

    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/squareup/checkout/ReturnCart$Companion;->fromBill(Lcom/squareup/protos/client/bills/Bill;Lcom/squareup/util/Res;Lcom/squareup/checkout/OrderVariationNamer;Z)Lcom/squareup/checkout/ReturnCart;

    move-result-object p0

    return-object p0
.end method

.method private final rollUpDiscounts(Ljava/util/List;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/ReturnDiscount;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/ReturnDiscount;",
            ">;"
        }
    .end annotation

    .line 45
    check-cast p1, Ljava/lang/Iterable;

    .line 223
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v0, Ljava/util/Map;

    .line 224
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 225
    move-object v2, v1

    check-cast v2, Lcom/squareup/checkout/ReturnDiscount;

    .line 46
    invoke-virtual {v2}, Lcom/squareup/checkout/ReturnDiscount;->getDiscount()Lcom/squareup/checkout/Discount;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/checkout/Discount;->id:Ljava/lang/String;

    .line 227
    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_0

    .line 226
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 230
    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 226
    :cond_0
    check-cast v3, Ljava/util/List;

    .line 234
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 236
    :cond_1
    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 237
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 238
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 239
    check-cast v2, Ljava/util/List;

    .line 50
    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/checkout/ReturnDiscount;

    invoke-virtual {v3}, Lcom/squareup/checkout/ReturnDiscount;->getDiscount()Lcom/squareup/checkout/Discount;

    move-result-object v3

    .line 51
    check-cast v2, Ljava/lang/Iterable;

    .line 240
    new-instance v4, Ljava/util/ArrayList;

    invoke-static {v2, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v5

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v4, Ljava/util/Collection;

    .line 241
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 242
    check-cast v5, Lcom/squareup/checkout/ReturnDiscount;

    .line 51
    invoke-virtual {v5}, Lcom/squareup/checkout/ReturnDiscount;->getAppliedAmount()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 243
    :cond_2
    check-cast v4, Ljava/util/List;

    check-cast v4, Ljava/lang/Iterable;

    .line 51
    invoke-static {v4}, Lkotlin/collections/CollectionsKt;->sumOfLong(Ljava/lang/Iterable;)J

    move-result-wide v4

    .line 49
    new-instance v2, Lcom/squareup/checkout/ReturnDiscount;

    invoke-direct {v2, v3, v4, v5}, Lcom/squareup/checkout/ReturnDiscount;-><init>(Lcom/squareup/checkout/Discount;J)V

    .line 52
    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 244
    :cond_3
    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method private final rollUpTaxes(Ljava/util/List;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/ReturnTax;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/ReturnTax;",
            ">;"
        }
    .end annotation

    .line 85
    check-cast p1, Ljava/lang/Iterable;

    .line 269
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v0, Ljava/util/Map;

    .line 270
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 271
    move-object v2, v1

    check-cast v2, Lcom/squareup/checkout/ReturnTax;

    .line 86
    invoke-virtual {v2}, Lcom/squareup/checkout/ReturnTax;->getTax()Lcom/squareup/checkout/Tax;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    .line 273
    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_0

    .line 272
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 276
    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 272
    :cond_0
    check-cast v3, Ljava/util/List;

    .line 280
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 282
    :cond_1
    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 283
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 284
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 285
    check-cast v2, Ljava/util/List;

    .line 90
    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/checkout/ReturnTax;

    invoke-virtual {v3}, Lcom/squareup/checkout/ReturnTax;->getTax()Lcom/squareup/checkout/Tax;

    move-result-object v3

    .line 91
    check-cast v2, Ljava/lang/Iterable;

    .line 286
    new-instance v4, Ljava/util/ArrayList;

    invoke-static {v2, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v5

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v4, Ljava/util/Collection;

    .line 287
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 288
    check-cast v5, Lcom/squareup/checkout/ReturnTax;

    .line 91
    invoke-virtual {v5}, Lcom/squareup/checkout/ReturnTax;->getAppliedAmount()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 289
    :cond_2
    check-cast v4, Ljava/util/List;

    check-cast v4, Ljava/lang/Iterable;

    .line 91
    invoke-static {v4}, Lkotlin/collections/CollectionsKt;->sumOfLong(Ljava/lang/Iterable;)J

    move-result-wide v4

    .line 89
    new-instance v2, Lcom/squareup/checkout/ReturnTax;

    invoke-direct {v2, v3, v4, v5}, Lcom/squareup/checkout/ReturnTax;-><init>(Lcom/squareup/checkout/Tax;J)V

    .line 92
    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 290
    :cond_3
    check-cast v0, Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public final buildTopLevelFeeLineItems()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/FeeLineItem;",
            ">;"
        }
    .end annotation

    .line 137
    invoke-virtual {p0}, Lcom/squareup/checkout/ReturnCart;->getReturnTaxes()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 316
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 317
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 318
    check-cast v2, Lcom/squareup/checkout/ReturnTax;

    .line 139
    new-instance v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v2}, Lcom/squareup/checkout/ReturnTax;->getAppliedAmount()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    iget-object v5, p0, Lcom/squareup/checkout/ReturnCart;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-direct {v3, v4, v5}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    .line 140
    invoke-virtual {v2}, Lcom/squareup/checkout/ReturnTax;->getTax()Lcom/squareup/checkout/Tax;

    move-result-object v2

    .line 141
    new-instance v4, Lcom/squareup/protos/common/Money;

    .line 142
    invoke-static {p0, v2}, Lcom/squareup/payment/ReturnTaxCalculations;->calculateAppliedToAmountForReturn(Lcom/squareup/checkout/ReturnCart;Lcom/squareup/checkout/Tax;)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    .line 145
    iget-object v6, p0, Lcom/squareup/checkout/ReturnCart;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    .line 141
    invoke-direct {v4, v5, v6}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    .line 147
    new-instance v5, Lcom/squareup/protos/common/Money;

    .line 148
    invoke-static {p0, v2}, Lcom/squareup/payment/ReturnTaxCalculations;->calculateAfterApplicationItemizationsTotalAmountForReturn(Lcom/squareup/checkout/ReturnCart;Lcom/squareup/checkout/Tax;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    .line 149
    iget-object v7, p0, Lcom/squareup/checkout/ReturnCart;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    .line 147
    invoke-direct {v5, v6, v7}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    .line 151
    invoke-virtual {v2, v3, v4, v5}, Lcom/squareup/checkout/Tax;->buildFeeLineItem(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/FeeLineItem;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 319
    :cond_0
    check-cast v1, Ljava/util/List;

    return-object v1
.end method

.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkout/ReturnCart;->billServerId:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Lcom/squareup/protos/common/CurrencyCode;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkout/ReturnCart;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    return-object v0
.end method

.method public final component3()Lcom/squareup/protos/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkout/ReturnCart;->totalReturnAmount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final component4()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/ReturnCartItem;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/checkout/ReturnCart;->returnItems:Ljava/util/List;

    return-object v0
.end method

.method public final component5()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/ReturnTip;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/checkout/ReturnCart;->returnTips:Ljava/util/List;

    return-object v0
.end method

.method public final component6()Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkout/ReturnCart;->roundingAdjustment:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;)Lcom/squareup/checkout/ReturnCart;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/ReturnCartItem;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/ReturnTip;",
            ">;",
            "Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;",
            ")",
            "Lcom/squareup/checkout/ReturnCart;"
        }
    .end annotation

    const-string v0, "billServerId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "totalReturnAmount"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "returnItems"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "returnTips"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/checkout/ReturnCart;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/squareup/checkout/ReturnCart;-><init>(Ljava/lang/String;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/checkout/ReturnCart;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/checkout/ReturnCart;

    iget-object v0, p0, Lcom/squareup/checkout/ReturnCart;->billServerId:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/checkout/ReturnCart;->billServerId:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkout/ReturnCart;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iget-object v1, p1, Lcom/squareup/checkout/ReturnCart;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkout/ReturnCart;->totalReturnAmount:Lcom/squareup/protos/common/Money;

    iget-object v1, p1, Lcom/squareup/checkout/ReturnCart;->totalReturnAmount:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkout/ReturnCart;->returnItems:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/checkout/ReturnCart;->returnItems:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkout/ReturnCart;->returnTips:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/checkout/ReturnCart;->returnTips:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkout/ReturnCart;->roundingAdjustment:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    iget-object p1, p1, Lcom/squareup/checkout/ReturnCart;->roundingAdjustment:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAdditiveReturnTaxes()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/ReturnTax;",
            ">;"
        }
    .end annotation

    .line 105
    invoke-virtual {p0}, Lcom/squareup/checkout/ReturnCart;->getReturnTaxes()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 298
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 299
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/squareup/checkout/ReturnTax;

    .line 106
    invoke-virtual {v3}, Lcom/squareup/checkout/ReturnTax;->getTax()Lcom/squareup/checkout/Tax;

    move-result-object v3

    iget-object v3, v3, Lcom/squareup/checkout/Tax;->inclusionType:Lcom/squareup/api/items/Fee$InclusionType;

    sget-object v4, Lcom/squareup/api/items/Fee$InclusionType;->ADDITIVE:Lcom/squareup/api/items/Fee$InclusionType;

    if-ne v3, v4, :cond_1

    const/4 v3, 0x1

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    :goto_1
    if-eqz v3, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 300
    :cond_2
    check-cast v1, Ljava/util/List;

    return-object v1
.end method

.method public final getAdditiveReturnTaxesAmount()Lcom/squareup/protos/common/Money;
    .locals 5

    .line 99
    invoke-virtual {p0}, Lcom/squareup/checkout/ReturnCart;->getReturnTaxes()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 291
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 292
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/squareup/checkout/ReturnTax;

    .line 100
    invoke-virtual {v3}, Lcom/squareup/checkout/ReturnTax;->getTax()Lcom/squareup/checkout/Tax;

    move-result-object v3

    iget-object v3, v3, Lcom/squareup/checkout/Tax;->inclusionType:Lcom/squareup/api/items/Fee$InclusionType;

    sget-object v4, Lcom/squareup/api/items/Fee$InclusionType;->ADDITIVE:Lcom/squareup/api/items/Fee$InclusionType;

    if-ne v3, v4, :cond_1

    const/4 v3, 0x1

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    :goto_1
    if-eqz v3, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 293
    :cond_2
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 294
    new-instance v0, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v1, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 295
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 296
    check-cast v2, Lcom/squareup/checkout/ReturnTax;

    .line 101
    invoke-virtual {v2}, Lcom/squareup/checkout/ReturnTax;->getAppliedAmount()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 297
    :cond_3
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 102
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->sumOfLong(Ljava/lang/Iterable;)J

    move-result-wide v0

    .line 103
    new-instance v2, Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/checkout/ReturnCart;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-direct {v2, v0, v1}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    return-object v2
.end method

.method public final getBillServerId()Ljava/lang/String;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/checkout/ReturnCart;->billServerId:Ljava/lang/String;

    return-object v0
.end method

.method public final getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/checkout/ReturnCart;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    return-object v0
.end method

.method public final getReturnDiscounts()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/ReturnDiscount;",
            ">;"
        }
    .end annotation

    .line 37
    iget-object v0, p0, Lcom/squareup/checkout/ReturnCart;->returnItems:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 211
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 218
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 219
    check-cast v2, Lcom/squareup/checkout/ReturnCartItem;

    .line 37
    invoke-virtual {v2}, Lcom/squareup/checkout/ReturnCartItem;->getReturnDiscounts()Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    .line 220
    invoke-static {v1, v2}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_0

    .line 222
    :cond_0
    check-cast v1, Ljava/util/List;

    .line 38
    move-object v0, p0

    check-cast v0, Lcom/squareup/checkout/ReturnCart;

    invoke-direct {v0, v1}, Lcom/squareup/checkout/ReturnCart;->rollUpDiscounts(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final getReturnDiscountsAmount()Lcom/squareup/protos/common/Money;
    .locals 4

    .line 59
    invoke-virtual {p0}, Lcom/squareup/checkout/ReturnCart;->getReturnDiscounts()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 245
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 246
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 247
    check-cast v2, Lcom/squareup/checkout/ReturnDiscount;

    .line 60
    invoke-virtual {v2}, Lcom/squareup/checkout/ReturnDiscount;->getAppliedAmount()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 248
    :cond_0
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 61
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->sumOfLong(Ljava/lang/Iterable;)J

    move-result-wide v0

    .line 62
    new-instance v2, Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/checkout/ReturnCart;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-direct {v2, v0, v1}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    return-object v2
.end method

.method public final getReturnDiscountsAsAdjustments()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/payment/OrderAdjustment;",
            ">;"
        }
    .end annotation

    .line 64
    invoke-virtual {p0}, Lcom/squareup/checkout/ReturnCart;->getReturnDiscounts()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 249
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 250
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 251
    check-cast v2, Lcom/squareup/checkout/ReturnDiscount;

    .line 67
    invoke-virtual {v2}, Lcom/squareup/checkout/ReturnDiscount;->getDiscount()Lcom/squareup/checkout/Discount;

    move-result-object v3

    new-instance v4, Lcom/squareup/protos/common/Money;

    invoke-virtual {v2}, Lcom/squareup/checkout/ReturnDiscount;->getAppliedAmount()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v5, p0, Lcom/squareup/checkout/ReturnCart;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-direct {v4, v2, v5}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    .line 66
    invoke-static {v3, v4}, Lcom/squareup/payment/OrderAdjustment;->fromDiscount(Lcom/squareup/checkout/Discount;Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/OrderAdjustment;

    move-result-object v2

    .line 68
    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 252
    :cond_0
    check-cast v1, Ljava/util/List;

    return-object v1
.end method

.method public final getReturnDiscountsAsDiscounts()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/Discount;",
            ">;"
        }
    .end annotation

    .line 71
    invoke-virtual {p0}, Lcom/squareup/checkout/ReturnCart;->getReturnDiscounts()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 253
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 254
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 255
    check-cast v2, Lcom/squareup/checkout/ReturnDiscount;

    .line 71
    invoke-virtual {v2}, Lcom/squareup/checkout/ReturnDiscount;->getDiscount()Lcom/squareup/checkout/Discount;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 256
    :cond_0
    check-cast v1, Ljava/util/List;

    return-object v1
.end method

.method public final getReturnItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/ReturnCartItem;",
            ">;"
        }
    .end annotation

    .line 26
    iget-object v0, p0, Lcom/squareup/checkout/ReturnCart;->returnItems:Ljava/util/List;

    return-object v0
.end method

.method public final getReturnItemsAsCartItems()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;"
        }
    .end annotation

    .line 31
    iget-object v0, p0, Lcom/squareup/checkout/ReturnCart;->returnItems:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 207
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 208
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 209
    check-cast v2, Lcom/squareup/checkout/ReturnCartItem;

    .line 31
    invoke-virtual {v2}, Lcom/squareup/checkout/ReturnCartItem;->getCartItem()Lcom/squareup/checkout/CartItem;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 210
    :cond_0
    check-cast v1, Ljava/util/List;

    return-object v1
.end method

.method public final getReturnTaxes()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/ReturnTax;",
            ">;"
        }
    .end annotation

    .line 77
    iget-object v0, p0, Lcom/squareup/checkout/ReturnCart;->returnItems:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 257
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 264
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 265
    check-cast v2, Lcom/squareup/checkout/ReturnCartItem;

    .line 77
    invoke-virtual {v2}, Lcom/squareup/checkout/ReturnCartItem;->getReturnTaxes()Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    .line 266
    invoke-static {v1, v2}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_0

    .line 268
    :cond_0
    check-cast v1, Ljava/util/List;

    .line 78
    move-object v0, p0

    check-cast v0, Lcom/squareup/checkout/ReturnCart;

    invoke-direct {v0, v1}, Lcom/squareup/checkout/ReturnCart;->rollUpTaxes(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final getReturnTips()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/ReturnTip;",
            ">;"
        }
    .end annotation

    .line 27
    iget-object v0, p0, Lcom/squareup/checkout/ReturnCart;->returnTips:Ljava/util/List;

    return-object v0
.end method

.method public final getRoundingAdjustment()Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/checkout/ReturnCart;->roundingAdjustment:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    return-object v0
.end method

.method public final getSwedishRoundingAdjustment()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 108
    iget-object v0, p0, Lcom/squareup/checkout/ReturnCart;->roundingAdjustment:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->amounts:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts;->applied_money:Lcom/squareup/protos/common/Money;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final getTipAmount()Lcom/squareup/protos/common/Money;
    .locals 4

    .line 132
    iget-object v0, p0, Lcom/squareup/checkout/ReturnCart;->returnTips:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 309
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 310
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 311
    check-cast v2, Lcom/squareup/checkout/ReturnTip;

    .line 133
    invoke-virtual {v2}, Lcom/squareup/checkout/ReturnTip;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 312
    :cond_0
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 134
    new-instance v0, Lcom/squareup/protos/common/Money;

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/checkout/ReturnCart;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-direct {v0, v2, v3}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    .line 314
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/common/Money;

    .line 134
    invoke-static {v0, v2}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    goto :goto_1

    :cond_1
    const-string v1, "returnTips\n      .map { \u2026ncyCode), MoneyMath::sum)"

    .line 315
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final getTotalReturnAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/checkout/ReturnCart;->totalReturnAmount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/checkout/ReturnCart;->billServerId:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkout/ReturnCart;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkout/ReturnCart;->totalReturnAmount:Lcom/squareup/protos/common/Money;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkout/ReturnCart;->returnItems:Ljava/util/List;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkout/ReturnCart;->returnTips:Ljava/util/List;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkout/ReturnCart;->roundingAdjustment:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_5
    add-int/2addr v0, v1

    return v0
.end method

.method public final toReturnLineItems()Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;
    .locals 5

    .line 114
    iget-object v0, p0, Lcom/squareup/checkout/ReturnCart;->returnItems:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 301
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 302
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 303
    check-cast v3, Lcom/squareup/checkout/ReturnCartItem;

    .line 114
    invoke-virtual {v3}, Lcom/squareup/checkout/ReturnCartItem;->toReturnItemizationProto()Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 304
    :cond_0
    check-cast v1, Ljava/util/List;

    .line 116
    iget-object v0, p0, Lcom/squareup/checkout/ReturnCart;->returnTips:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 305
    new-instance v3, Ljava/util/ArrayList;

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v3, Ljava/util/Collection;

    .line 306
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 307
    check-cast v2, Lcom/squareup/checkout/ReturnTip;

    .line 116
    invoke-virtual {v2}, Lcom/squareup/checkout/ReturnTip;->toReturnTipLineItem()Lcom/squareup/protos/client/bills/ReturnTipLineItem;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 308
    :cond_1
    check-cast v3, Ljava/util/List;

    .line 118
    invoke-virtual {p0}, Lcom/squareup/checkout/ReturnCart;->buildTopLevelFeeLineItems()Ljava/util/List;

    move-result-object v0

    .line 120
    new-instance v2, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;-><init>()V

    .line 121
    iget-object v4, p0, Lcom/squareup/checkout/ReturnCart;->billServerId:Ljava/lang/String;

    invoke-virtual {v2, v4}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->source_bill_server_id(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;

    move-result-object v2

    .line 122
    invoke-virtual {v2, v1}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->return_itemization(Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;

    move-result-object v1

    .line 123
    invoke-virtual {v1, v3}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->return_tip_line_item(Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;

    move-result-object v1

    .line 124
    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->fee_line_item(Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;

    move-result-object v0

    .line 125
    iget-object v1, p0, Lcom/squareup/checkout/ReturnCart;->roundingAdjustment:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->rounding_adjustment(Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;)Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;

    move-result-object v0

    .line 126
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->build()Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;

    move-result-object v0

    const-string v1, "Builder()\n        .sourc\u2026ustment)\n        .build()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ReturnCart(billServerId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkout/ReturnCart;->billServerId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", currencyCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkout/ReturnCart;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", totalReturnAmount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkout/ReturnCart;->totalReturnAmount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", returnItems="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkout/ReturnCart;->returnItems:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", returnTips="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkout/ReturnCart;->returnTips:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", roundingAdjustment="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkout/ReturnCart;->roundingAdjustment:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
