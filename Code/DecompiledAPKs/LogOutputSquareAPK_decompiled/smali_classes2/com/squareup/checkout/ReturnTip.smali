.class public final Lcom/squareup/checkout/ReturnTip;
.super Ljava/lang/Object;
.source "ReturnTip.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkout/ReturnTip$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u000f\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u0000 \u001e2\u00020\u0001:\u0001\u001eB\'\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0008\u0010\u0008\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\tJ\t\u0010\u0011\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0012\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0013\u001a\u00020\u0007H\u00c6\u0003J\u000b\u0010\u0014\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J3\u0010\u0015\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\u0003H\u00c6\u0001J\u0013\u0010\u0016\u001a\u00020\u00172\u0008\u0010\u0018\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0019\u001a\u00020\u001aH\u00d6\u0001J\u0006\u0010\u001b\u001a\u00020\u001cJ\t\u0010\u001d\u001a\u00020\u0007H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0013\u0010\u0008\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\rR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010\u00a8\u0006\u001f"
    }
    d2 = {
        "Lcom/squareup/checkout/ReturnTip;",
        "",
        "idPair",
        "Lcom/squareup/protos/client/IdPair;",
        "amount",
        "Lcom/squareup/protos/common/Money;",
        "tenderServerToken",
        "",
        "sourceTipLineItemIdPair",
        "(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/common/Money;Ljava/lang/String;Lcom/squareup/protos/client/IdPair;)V",
        "getAmount",
        "()Lcom/squareup/protos/common/Money;",
        "getIdPair",
        "()Lcom/squareup/protos/client/IdPair;",
        "getSourceTipLineItemIdPair",
        "getTenderServerToken",
        "()Ljava/lang/String;",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toReturnTipLineItem",
        "Lcom/squareup/protos/client/bills/ReturnTipLineItem;",
        "toString",
        "Companion",
        "checkout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/checkout/ReturnTip$Companion;


# instance fields
.field private final amount:Lcom/squareup/protos/common/Money;

.field private final idPair:Lcom/squareup/protos/client/IdPair;

.field private final sourceTipLineItemIdPair:Lcom/squareup/protos/client/IdPair;

.field private final tenderServerToken:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/checkout/ReturnTip$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/checkout/ReturnTip$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/checkout/ReturnTip;->Companion:Lcom/squareup/checkout/ReturnTip$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/common/Money;Ljava/lang/String;Lcom/squareup/protos/client/IdPair;)V
    .locals 1

    const-string v0, "idPair"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amount"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tenderServerToken"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkout/ReturnTip;->idPair:Lcom/squareup/protos/client/IdPair;

    iput-object p2, p0, Lcom/squareup/checkout/ReturnTip;->amount:Lcom/squareup/protos/common/Money;

    iput-object p3, p0, Lcom/squareup/checkout/ReturnTip;->tenderServerToken:Ljava/lang/String;

    iput-object p4, p0, Lcom/squareup/checkout/ReturnTip;->sourceTipLineItemIdPair:Lcom/squareup/protos/client/IdPair;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/checkout/ReturnTip;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/common/Money;Ljava/lang/String;Lcom/squareup/protos/client/IdPair;ILjava/lang/Object;)Lcom/squareup/checkout/ReturnTip;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/squareup/checkout/ReturnTip;->idPair:Lcom/squareup/protos/client/IdPair;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/squareup/checkout/ReturnTip;->amount:Lcom/squareup/protos/common/Money;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-object p3, p0, Lcom/squareup/checkout/ReturnTip;->tenderServerToken:Ljava/lang/String;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lcom/squareup/checkout/ReturnTip;->sourceTipLineItemIdPair:Lcom/squareup/protos/client/IdPair;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/checkout/ReturnTip;->copy(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/common/Money;Ljava/lang/String;Lcom/squareup/protos/client/IdPair;)Lcom/squareup/checkout/ReturnTip;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/protos/client/IdPair;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkout/ReturnTip;->idPair:Lcom/squareup/protos/client/IdPair;

    return-object v0
.end method

.method public final component2()Lcom/squareup/protos/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkout/ReturnTip;->amount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkout/ReturnTip;->tenderServerToken:Ljava/lang/String;

    return-object v0
.end method

.method public final component4()Lcom/squareup/protos/client/IdPair;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkout/ReturnTip;->sourceTipLineItemIdPair:Lcom/squareup/protos/client/IdPair;

    return-object v0
.end method

.method public final copy(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/common/Money;Ljava/lang/String;Lcom/squareup/protos/client/IdPair;)Lcom/squareup/checkout/ReturnTip;
    .locals 1

    const-string v0, "idPair"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amount"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tenderServerToken"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/checkout/ReturnTip;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/checkout/ReturnTip;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/common/Money;Ljava/lang/String;Lcom/squareup/protos/client/IdPair;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/checkout/ReturnTip;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/checkout/ReturnTip;

    iget-object v0, p0, Lcom/squareup/checkout/ReturnTip;->idPair:Lcom/squareup/protos/client/IdPair;

    iget-object v1, p1, Lcom/squareup/checkout/ReturnTip;->idPair:Lcom/squareup/protos/client/IdPair;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkout/ReturnTip;->amount:Lcom/squareup/protos/common/Money;

    iget-object v1, p1, Lcom/squareup/checkout/ReturnTip;->amount:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkout/ReturnTip;->tenderServerToken:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/checkout/ReturnTip;->tenderServerToken:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkout/ReturnTip;->sourceTipLineItemIdPair:Lcom/squareup/protos/client/IdPair;

    iget-object p1, p1, Lcom/squareup/checkout/ReturnTip;->sourceTipLineItemIdPair:Lcom/squareup/protos/client/IdPair;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/checkout/ReturnTip;->amount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final getIdPair()Lcom/squareup/protos/client/IdPair;
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/squareup/checkout/ReturnTip;->idPair:Lcom/squareup/protos/client/IdPair;

    return-object v0
.end method

.method public final getSourceTipLineItemIdPair()Lcom/squareup/protos/client/IdPair;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/checkout/ReturnTip;->sourceTipLineItemIdPair:Lcom/squareup/protos/client/IdPair;

    return-object v0
.end method

.method public final getTenderServerToken()Ljava/lang/String;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/squareup/checkout/ReturnTip;->tenderServerToken:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/checkout/ReturnTip;->idPair:Lcom/squareup/protos/client/IdPair;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkout/ReturnTip;->amount:Lcom/squareup/protos/common/Money;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkout/ReturnTip;->tenderServerToken:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkout/ReturnTip;->sourceTipLineItemIdPair:Lcom/squareup/protos/client/IdPair;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public final toReturnTipLineItem()Lcom/squareup/protos/client/bills/ReturnTipLineItem;
    .locals 4

    .line 24
    new-instance v0, Lcom/squareup/protos/client/bills/ReturnTipLineItem$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/ReturnTipLineItem$Builder;-><init>()V

    .line 26
    new-instance v1, Lcom/squareup/protos/client/bills/TipLineItem$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/TipLineItem$Builder;-><init>()V

    .line 27
    new-instance v2, Lcom/squareup/protos/client/bills/TipLineItem$Amounts$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/bills/TipLineItem$Amounts$Builder;-><init>()V

    iget-object v3, p0, Lcom/squareup/checkout/ReturnTip;->amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/bills/TipLineItem$Amounts$Builder;->applied_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/TipLineItem$Amounts$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/protos/client/bills/TipLineItem$Amounts$Builder;->build()Lcom/squareup/protos/client/bills/TipLineItem$Amounts;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/TipLineItem$Builder;->amounts(Lcom/squareup/protos/client/bills/TipLineItem$Amounts;)Lcom/squareup/protos/client/bills/TipLineItem$Builder;

    move-result-object v1

    .line 28
    iget-object v2, p0, Lcom/squareup/checkout/ReturnTip;->idPair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/TipLineItem$Builder;->tip_line_item_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/TipLineItem$Builder;

    move-result-object v1

    .line 29
    iget-object v2, p0, Lcom/squareup/checkout/ReturnTip;->tenderServerToken:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/TipLineItem$Builder;->tender_server_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/TipLineItem$Builder;

    move-result-object v1

    .line 30
    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/TipLineItem$Builder;->build()Lcom/squareup/protos/client/bills/TipLineItem;

    move-result-object v1

    .line 25
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/ReturnTipLineItem$Builder;->tip_line_item(Lcom/squareup/protos/client/bills/TipLineItem;)Lcom/squareup/protos/client/bills/ReturnTipLineItem$Builder;

    move-result-object v0

    .line 32
    iget-object v1, p0, Lcom/squareup/checkout/ReturnTip;->sourceTipLineItemIdPair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/ReturnTipLineItem$Builder;->source_tip_line_item_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/ReturnTipLineItem$Builder;

    move-result-object v0

    .line 33
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/ReturnTipLineItem$Builder;->build()Lcom/squareup/protos/client/bills/ReturnTipLineItem;

    move-result-object v0

    const-string v1, "ReturnTipLineItem.Builde\u2026temIdPair)\n      .build()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ReturnTip(idPair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkout/ReturnTip;->idPair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkout/ReturnTip;->amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", tenderServerToken="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkout/ReturnTip;->tenderServerToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", sourceTipLineItemIdPair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkout/ReturnTip;->sourceTipLineItemIdPair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
