.class public abstract Lcom/squareup/checkout/Surcharge;
.super Ljava/lang/Object;
.source "Surcharge.kt"

# interfaces
.implements Lcom/squareup/calc/order/Surcharge;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkout/Surcharge$CustomSurcharge;,
        Lcom/squareup/checkout/Surcharge$AutoGratuity;,
        Lcom/squareup/checkout/Surcharge$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSurcharge.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Surcharge.kt\ncom/squareup/checkout/Surcharge\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,201:1\n704#2:202\n777#2,2:203\n1113#2,2:205\n1143#2,4:207\n704#2:211\n777#2,2:212\n1360#2:214\n1429#2,3:215\n*E\n*S KotlinDebug\n*F\n+ 1 Surcharge.kt\ncom/squareup/checkout/Surcharge\n*L\n80#1:202\n80#1,2:203\n81#1,2:205\n81#1,4:207\n85#1:211\n85#1,2:212\n167#1:214\n167#1,3:215\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0084\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010%\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0007\n\u0002\u0010\t\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010$\n\u0002\u0008\u0004\n\u0002\u0010\"\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u0000 62\u00020\u0001:\u0003567B#\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005\u00a2\u0006\u0002\u0010\u0008J\u000f\u0010\u0019\u001a\u0004\u0018\u00010\u001aH\u0016\u00a2\u0006\u0002\u0010\u001bJ\u0016\u0010\u001c\u001a\u0010\u0012\u0004\u0012\u00020\u0006\u0012\u0006\u0008\u0001\u0012\u00020\u001d0\u0005H\u0016J \u0010\u001e\u001a\u0004\u0018\u00010\u00062\u0006\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020$J\u001e\u0010%\u001a\u00020\u00062\u0006\u0010\u001f\u001a\u00020 2\u0006\u0010&\u001a\u00020\u00122\u0006\u0010#\u001a\u00020$J\u0008\u0010\'\u001a\u00020\u0006H\u0016J\n\u0010(\u001a\u0004\u0018\u00010)H\u0016J\u0008\u0010*\u001a\u00020+H\u0016J\"\u0010,\u001a\u00020\u00032\u0006\u0010-\u001a\u00020\u000e2\u0012\u0010.\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u000e0/J\u000e\u00100\u001a\u00020\u00002\u0006\u00101\u001a\u00020\u0006J\u0014\u00102\u001a\u00020\u00002\u000c\u00103\u001a\u0008\u0012\u0004\u0012\u00020\u000604R\u0014\u0010\t\u001a\u00020\n8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000b\u0010\u000cR\u001a\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0011\u0010\u0011\u001a\u00020\u0012\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0013R\u0016\u0010\u0014\u001a\u0004\u0018\u00010\u00068BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0015\u0010\u0016R\u0014\u0010\u0002\u001a\u00020\u0003X\u0084\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0018\u0082\u0001\u000289\u00a8\u0006:"
    }
    d2 = {
        "Lcom/squareup/checkout/Surcharge;",
        "Lcom/squareup/calc/order/Surcharge;",
        "surchargeProto",
        "Lcom/squareup/protos/client/bills/SurchargeLineItem;",
        "disabledFees",
        "",
        "",
        "Lcom/squareup/protos/client/bills/FeeLineItem;",
        "(Lcom/squareup/protos/client/bills/SurchargeLineItem;Ljava/util/Map;)V",
        "catalogSurcharge",
        "Lcom/squareup/api/items/Surcharge;",
        "getCatalogSurcharge",
        "()Lcom/squareup/api/items/Surcharge;",
        "historicalAmount",
        "Lcom/squareup/protos/common/Money;",
        "getHistoricalAmount",
        "()Lcom/squareup/protos/common/Money;",
        "isTaxable",
        "",
        "()Z",
        "percentageString",
        "getPercentageString",
        "()Ljava/lang/String;",
        "getSurchargeProto",
        "()Lcom/squareup/protos/client/bills/SurchargeLineItem;",
        "amount",
        "",
        "()Ljava/lang/Long;",
        "appliedTaxes",
        "Lcom/squareup/calc/order/Adjustment;",
        "disclosureText",
        "res",
        "Lcom/squareup/util/Res;",
        "seatCount",
        "",
        "countryCode",
        "Lcom/squareup/CountryCode;",
        "formattedName",
        "isCustomerFacing",
        "id",
        "percentage",
        "Ljava/math/BigDecimal;",
        "phase",
        "Lcom/squareup/calc/constants/CalculationPhase;",
        "toSurchargeLineItem",
        "totalAmount",
        "taxAmountById",
        "",
        "withReappliedTax",
        "removedTaxId",
        "withoutTaxes",
        "removedTaxIds",
        "",
        "AutoGratuity",
        "Companion",
        "CustomSurcharge",
        "Lcom/squareup/checkout/Surcharge$CustomSurcharge;",
        "Lcom/squareup/checkout/Surcharge$AutoGratuity;",
        "checkout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/checkout/Surcharge$Companion;


# instance fields
.field private final disabledFees:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/bills/FeeLineItem;",
            ">;"
        }
    .end annotation
.end field

.field private final historicalAmount:Lcom/squareup/protos/common/Money;

.field private final isTaxable:Z

.field private final surchargeProto:Lcom/squareup/protos/client/bills/SurchargeLineItem;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/checkout/Surcharge$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/checkout/Surcharge$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/checkout/Surcharge;->Companion:Lcom/squareup/checkout/Surcharge$Companion;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/protos/client/bills/SurchargeLineItem;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/SurchargeLineItem;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/bills/FeeLineItem;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkout/Surcharge;->surchargeProto:Lcom/squareup/protos/client/bills/SurchargeLineItem;

    iput-object p2, p0, Lcom/squareup/checkout/Surcharge;->disabledFees:Ljava/util/Map;

    .line 44
    invoke-direct {p0}, Lcom/squareup/checkout/Surcharge;->getCatalogSurcharge()Lcom/squareup/api/items/Surcharge;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/api/items/Surcharge;->taxable:Ljava/lang/Boolean;

    const-string p2, "catalogSurcharge.taxable"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/checkout/Surcharge;->isTaxable:Z

    .line 51
    iget-object p1, p0, Lcom/squareup/checkout/Surcharge;->surchargeProto:Lcom/squareup/protos/client/bills/SurchargeLineItem;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/SurchargeLineItem;->amounts:Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts;->applied_money:Lcom/squareup/protos/common/Money;

    const-string p2, "surchargeProto.amounts.applied_money"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/checkout/Surcharge;->historicalAmount:Lcom/squareup/protos/common/Money;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/protos/client/bills/SurchargeLineItem;Ljava/util/Map;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 23
    invoke-direct {p0, p1, p2}, Lcom/squareup/checkout/Surcharge;-><init>(Lcom/squareup/protos/client/bills/SurchargeLineItem;Ljava/util/Map;)V

    return-void
.end method

.method public static final fromProto(Lcom/squareup/protos/client/bills/SurchargeLineItem;)Lcom/squareup/checkout/Surcharge;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/checkout/Surcharge;->Companion:Lcom/squareup/checkout/Surcharge$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/checkout/Surcharge$Companion;->fromProto(Lcom/squareup/protos/client/bills/SurchargeLineItem;)Lcom/squareup/checkout/Surcharge;

    move-result-object p0

    return-object p0
.end method

.method public static final fromProto(Ljava/util/List;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/SurchargeLineItem;",
            ">;)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Surcharge;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/checkout/Surcharge;->Companion:Lcom/squareup/checkout/Surcharge$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/checkout/Surcharge$Companion;->fromProto(Ljava/util/List;)Ljava/util/Map;

    move-result-object p0

    return-object p0
.end method

.method public static final getAutoGratuity(Ljava/util/List;)Lcom/squareup/checkout/Surcharge$AutoGratuity;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/checkout/Surcharge;",
            ">;)",
            "Lcom/squareup/checkout/Surcharge$AutoGratuity;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/checkout/Surcharge;->Companion:Lcom/squareup/checkout/Surcharge$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/checkout/Surcharge$Companion;->getAutoGratuity(Ljava/util/List;)Lcom/squareup/checkout/Surcharge$AutoGratuity;

    move-result-object p0

    return-object p0
.end method

.method private final getCatalogSurcharge()Lcom/squareup/api/items/Surcharge;
    .locals 2

    .line 57
    iget-object v0, p0, Lcom/squareup/checkout/Surcharge;->surchargeProto:Lcom/squareup/protos/client/bills/SurchargeLineItem;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/SurchargeLineItem;->backing_details:Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;->surcharge:Lcom/squareup/api/items/Surcharge;

    const-string v1, "surchargeProto.backing_details.surcharge"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final getPercentageString()Ljava/lang/String;
    .locals 1

    .line 60
    invoke-direct {p0}, Lcom/squareup/checkout/Surcharge;->getCatalogSurcharge()Lcom/squareup/api/items/Surcharge;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/api/items/Surcharge;->percentage:Ljava/lang/String;

    return-object v0
.end method

.method public static final withPhase(Ljava/util/List;Lcom/squareup/calc/constants/CalculationPhase;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/checkout/Surcharge;",
            ">;",
            "Lcom/squareup/calc/constants/CalculationPhase;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/Surcharge;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/checkout/Surcharge;->Companion:Lcom/squareup/checkout/Surcharge$Companion;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/checkout/Surcharge$Companion;->withPhase(Ljava/util/List;Lcom/squareup/calc/constants/CalculationPhase;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public amount()Ljava/lang/Long;
    .locals 1

    .line 66
    invoke-direct {p0}, Lcom/squareup/checkout/Surcharge;->getCatalogSurcharge()Lcom/squareup/api/items/Surcharge;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/api/items/Surcharge;->amount:Lcom/squareup/protos/common/dinero/Money;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/common/dinero/Money;->cents:Ljava/lang/Long;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public appliedTaxes()Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Lcom/squareup/calc/order/Adjustment;",
            ">;"
        }
    .end annotation

    .line 79
    iget-object v0, p0, Lcom/squareup/checkout/Surcharge;->surchargeProto:Lcom/squareup/protos/client/bills/SurchargeLineItem;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/SurchargeLineItem;->fee:Ljava/util/List;

    const-string v1, "surchargeProto.fee"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 202
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 203
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/squareup/protos/client/bills/FeeLineItem;

    .line 80
    iget-object v3, v3, Lcom/squareup/protos/client/bills/FeeLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;->fee:Lcom/squareup/api/items/Fee;

    iget-object v3, v3, Lcom/squareup/api/items/Fee;->enabled:Ljava/lang/Boolean;

    const-string v4, "it.write_only_backing_details.fee.enabled"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 204
    :cond_1
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    const/16 v0, 0xa

    .line 205
    invoke-static {v1, v0}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v0

    invoke-static {v0}, Lkotlin/collections/MapsKt;->mapCapacity(I)I

    move-result v0

    const/16 v2, 0x10

    invoke-static {v0, v2}, Lkotlin/ranges/RangesKt;->coerceAtLeast(II)I

    move-result v0

    .line 206
    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2, v0}, Ljava/util/LinkedHashMap;-><init>(I)V

    check-cast v2, Ljava/util/Map;

    .line 207
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 208
    check-cast v1, Lcom/squareup/protos/client/bills/FeeLineItem;

    .line 81
    iget-object v3, v1, Lcom/squareup/protos/client/bills/FeeLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;->fee:Lcom/squareup/api/items/Fee;

    iget-object v3, v3, Lcom/squareup/api/items/Fee;->id:Ljava/lang/String;

    invoke-static {v1}, Lcom/squareup/checkout/Tax$Builder;->from(Lcom/squareup/protos/client/bills/FeeLineItem;)Lcom/squareup/checkout/Tax$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/checkout/Tax$Builder;->build()Lcom/squareup/checkout/Tax;

    move-result-object v1

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 82
    :cond_2
    invoke-static {v2}, Lkotlin/collections/MapsKt;->toMutableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final disclosureText(Lcom/squareup/util/Res;ILcom/squareup/CountryCode;)Ljava/lang/String;
    .locals 3

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "countryCode"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 146
    instance-of v0, p0, Lcom/squareup/checkout/Surcharge$AutoGratuity;

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    .line 147
    move-object v0, p0

    check-cast v0, Lcom/squareup/checkout/Surcharge$AutoGratuity;

    invoke-virtual {v0}, Lcom/squareup/checkout/Surcharge$AutoGratuity;->getMinimumSeatCount()I

    move-result v2

    if-ge p2, v2, :cond_0

    goto :goto_1

    .line 150
    :cond_0
    sget-object p2, Lcom/squareup/checkout/Surcharge$WhenMappings;->$EnumSwitchMapping$2:[I

    invoke-virtual {p3}, Lcom/squareup/CountryCode;->ordinal()I

    move-result p3

    aget p2, p2, p3

    const/4 p3, 0x1

    if-eq p2, p3, :cond_1

    const/4 p3, 0x2

    if-eq p2, p3, :cond_1

    .line 152
    sget p2, Lcom/squareup/checkout/R$string;->auto_gratuity_disclosure:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    goto :goto_0

    .line 151
    :cond_1
    sget p2, Lcom/squareup/checkout/R$string;->service_charge_disclosure:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 154
    :goto_0
    invoke-direct {p0}, Lcom/squareup/checkout/Surcharge;->getPercentageString()Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    const-string p3, "percent"

    invoke-virtual {p1, p3, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 155
    invoke-virtual {v0}, Lcom/squareup/checkout/Surcharge$AutoGratuity;->getMinimumSeatCount()I

    move-result p2

    const-string p3, "number"

    invoke-virtual {p1, p3, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 156
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 158
    :cond_2
    instance-of p1, p0, Lcom/squareup/checkout/Surcharge$CustomSurcharge;

    if-eqz p1, :cond_3

    :goto_1
    return-object v1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public final formattedName(Lcom/squareup/util/Res;ZLcom/squareup/CountryCode;)Ljava/lang/String;
    .locals 2

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "countryCode"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 120
    instance-of v0, p0, Lcom/squareup/checkout/Surcharge$AutoGratuity;

    const-string v1, "percent"

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/checkout/Surcharge$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p3}, Lcom/squareup/CountryCode;->ordinal()I

    move-result p3

    aget p3, v0, p3

    const/4 v0, 0x1

    if-eq p3, v0, :cond_1

    const/4 v0, 0x2

    if-eq p3, v0, :cond_1

    if-eqz p2, :cond_0

    .line 124
    sget p2, Lcom/squareup/checkout/R$string;->auto_gratuity_name_customer_facing:I

    goto :goto_0

    .line 126
    :cond_0
    sget p2, Lcom/squareup/checkout/R$string;->auto_gratuity_name_merchant_facing:I

    .line 122
    :goto_0
    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    goto :goto_1

    .line 121
    :cond_1
    sget p2, Lcom/squareup/checkout/R$string;->service_charge_name:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 130
    :goto_1
    invoke-direct {p0}, Lcom/squareup/checkout/Surcharge;->getPercentageString()Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p1, v1, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    goto :goto_2

    .line 132
    :cond_2
    instance-of p2, p0, Lcom/squareup/checkout/Surcharge$CustomSurcharge;

    if-eqz p2, :cond_4

    .line 134
    invoke-direct {p0}, Lcom/squareup/checkout/Surcharge;->getPercentageString()Ljava/lang/String;

    move-result-object p2

    const-string p3, "surcharge_name"

    if-eqz p2, :cond_3

    .line 135
    sget p2, Lcom/squareup/checkout/R$string;->custom_surcharge_percentage:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 136
    iget-object p2, p0, Lcom/squareup/checkout/Surcharge;->surchargeProto:Lcom/squareup/protos/client/bills/SurchargeLineItem;

    iget-object p2, p2, Lcom/squareup/protos/client/bills/SurchargeLineItem;->backing_details:Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;

    iget-object p2, p2, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;->surcharge:Lcom/squareup/api/items/Surcharge;

    iget-object p2, p2, Lcom/squareup/api/items/Surcharge;->name:Ljava/lang/String;

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p1, p3, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 137
    invoke-direct {p0}, Lcom/squareup/checkout/Surcharge;->getPercentageString()Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p1, v1, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    goto :goto_2

    .line 139
    :cond_3
    sget p2, Lcom/squareup/checkout/R$string;->custom_surcharge_amount:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 140
    iget-object p2, p0, Lcom/squareup/checkout/Surcharge;->surchargeProto:Lcom/squareup/protos/client/bills/SurchargeLineItem;

    iget-object p2, p2, Lcom/squareup/protos/client/bills/SurchargeLineItem;->backing_details:Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;

    iget-object p2, p2, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;->surcharge:Lcom/squareup/api/items/Surcharge;

    iget-object p2, p2, Lcom/squareup/api/items/Surcharge;->name:Ljava/lang/String;

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p1, p3, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 142
    :goto_2
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 134
    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public final getHistoricalAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/checkout/Surcharge;->historicalAmount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method protected final getSurchargeProto()Lcom/squareup/protos/client/bills/SurchargeLineItem;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/checkout/Surcharge;->surchargeProto:Lcom/squareup/protos/client/bills/SurchargeLineItem;

    return-object v0
.end method

.method public id()Ljava/lang/String;
    .locals 2

    .line 62
    invoke-direct {p0}, Lcom/squareup/checkout/Surcharge;->getCatalogSurcharge()Lcom/squareup/api/items/Surcharge;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/api/items/Surcharge;->id:Ljava/lang/String;

    const-string v1, "catalogSurcharge.id"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final isTaxable()Z
    .locals 1

    .line 44
    iget-boolean v0, p0, Lcom/squareup/checkout/Surcharge;->isTaxable:Z

    return v0
.end method

.method public percentage()Ljava/math/BigDecimal;
    .locals 2

    .line 64
    invoke-direct {p0}, Lcom/squareup/checkout/Surcharge;->getPercentageString()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v1, Lcom/squareup/util/Percentage;->Companion:Lcom/squareup/util/Percentage$Companion;

    invoke-virtual {v1, v0}, Lcom/squareup/util/Percentage$Companion;->fromString(Ljava/lang/String;)Lcom/squareup/util/Percentage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/util/Percentage;->bigDecimalRate()Ljava/math/BigDecimal;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public phase()Lcom/squareup/calc/constants/CalculationPhase;
    .locals 4

    .line 69
    invoke-direct {p0}, Lcom/squareup/checkout/Surcharge;->getCatalogSurcharge()Lcom/squareup/api/items/Surcharge;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/api/items/Surcharge;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    if-eqz v0, :cond_1

    sget-object v1, Lcom/squareup/checkout/Surcharge$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/api/items/CalculationPhase;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 71
    sget-object v0, Lcom/squareup/calc/constants/CalculationPhase;->SURCHARGE_TOTAL_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

    goto :goto_0

    .line 70
    :cond_0
    sget-object v0, Lcom/squareup/calc/constants/CalculationPhase;->SURCHARGE_PHASE:Lcom/squareup/calc/constants/CalculationPhase;

    :goto_0
    return-object v0

    .line 72
    :cond_1
    new-instance v1, Ljava/lang/IllegalStateException;

    .line 73
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unsupported calculation phase from com.squareup.api.items.Surcharge: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 72
    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1
.end method

.method public final toSurchargeLineItem(Lcom/squareup/protos/common/Money;Ljava/util/Map;)Lcom/squareup/protos/client/bills/SurchargeLineItem;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/Money;",
            ">;)",
            "Lcom/squareup/protos/client/bills/SurchargeLineItem;"
        }
    .end annotation

    const-string v0, "totalAmount"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "taxAmountById"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 162
    iget-object v0, p0, Lcom/squareup/checkout/Surcharge;->surchargeProto:Lcom/squareup/protos/client/bills/SurchargeLineItem;

    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/SurchargeLineItem;->newBuilder()Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;

    move-result-object v0

    .line 163
    new-instance v1, Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts$Builder;-><init>()V

    .line 164
    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts$Builder;->applied_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts$Builder;

    move-result-object p1

    .line 165
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts$Builder;->build()Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts;

    move-result-object p1

    .line 163
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;->amounts(Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts;)Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;

    move-result-object p1

    .line 167
    iget-object v0, p0, Lcom/squareup/checkout/Surcharge;->surchargeProto:Lcom/squareup/protos/client/bills/SurchargeLineItem;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/SurchargeLineItem;->fee:Ljava/util/List;

    const-string v1, "surchargeProto.fee"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 214
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 215
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 216
    check-cast v2, Lcom/squareup/protos/client/bills/FeeLineItem;

    .line 168
    invoke-virtual {v2}, Lcom/squareup/protos/client/bills/FeeLineItem;->newBuilder()Lcom/squareup/protos/client/bills/FeeLineItem$Builder;

    move-result-object v3

    .line 169
    new-instance v4, Lcom/squareup/protos/client/bills/FeeLineItem$Amounts$Builder;

    invoke-direct {v4}, Lcom/squareup/protos/client/bills/FeeLineItem$Amounts$Builder;-><init>()V

    .line 170
    iget-object v2, v2, Lcom/squareup/protos/client/bills/FeeLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;->fee:Lcom/squareup/api/items/Fee;

    iget-object v2, v2, Lcom/squareup/api/items/Fee;->id:Ljava/lang/String;

    invoke-interface {p2, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/common/Money;

    invoke-virtual {v4, v2}, Lcom/squareup/protos/client/bills/FeeLineItem$Amounts$Builder;->applied_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/FeeLineItem$Amounts$Builder;

    move-result-object v2

    .line 171
    invoke-virtual {v2}, Lcom/squareup/protos/client/bills/FeeLineItem$Amounts$Builder;->build()Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;

    move-result-object v2

    .line 169
    invoke-virtual {v3, v2}, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->amounts(Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;)Lcom/squareup/protos/client/bills/FeeLineItem$Builder;

    move-result-object v2

    .line 172
    invoke-virtual {v2}, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->build()Lcom/squareup/protos/client/bills/FeeLineItem;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 217
    :cond_0
    check-cast v1, Ljava/util/List;

    .line 167
    invoke-virtual {p1, v1}, Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;->fee(Ljava/util/List;)Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;

    move-result-object p1

    .line 174
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;->build()Lcom/squareup/protos/client/bills/SurchargeLineItem;

    move-result-object p1

    const-string p2, "surchargeProto.newBuilde\u2026    })\n          .build()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final withReappliedTax(Ljava/lang/String;)Lcom/squareup/checkout/Surcharge;
    .locals 3

    const-string v0, "removedTaxId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    iget-object v0, p0, Lcom/squareup/checkout/Surcharge;->disabledFees:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/FeeLineItem;

    if-eqz v0, :cond_2

    .line 108
    iget-object p1, p0, Lcom/squareup/checkout/Surcharge;->surchargeProto:Lcom/squareup/protos/client/bills/SurchargeLineItem;

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/SurchargeLineItem;->newBuilder()Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;

    move-result-object p1

    .line 109
    iget-object v1, p0, Lcom/squareup/checkout/Surcharge;->surchargeProto:Lcom/squareup/protos/client/bills/SurchargeLineItem;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/SurchargeLineItem;->fee:Ljava/util/List;

    const-string v2, "surchargeProto.fee"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/util/Collection;

    invoke-static {v1, v0}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;->fee(Ljava/util/List;)Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;

    move-result-object p1

    .line 110
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;->build()Lcom/squareup/protos/client/bills/SurchargeLineItem;

    move-result-object p1

    .line 113
    instance-of v0, p0, Lcom/squareup/checkout/Surcharge$AutoGratuity;

    const-string/jumbo v1, "updatedProto"

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/checkout/Surcharge$AutoGratuity;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/squareup/checkout/Surcharge;->disabledFees:Ljava/util/Map;

    invoke-direct {v0, p1, v1}, Lcom/squareup/checkout/Surcharge$AutoGratuity;-><init>(Lcom/squareup/protos/client/bills/SurchargeLineItem;Ljava/util/Map;)V

    check-cast v0, Lcom/squareup/checkout/Surcharge;

    goto :goto_0

    .line 114
    :cond_0
    instance-of v0, p0, Lcom/squareup/checkout/Surcharge$CustomSurcharge;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/squareup/checkout/Surcharge$CustomSurcharge;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/squareup/checkout/Surcharge;->disabledFees:Ljava/util/Map;

    invoke-direct {v0, p1, v1}, Lcom/squareup/checkout/Surcharge$CustomSurcharge;-><init>(Lcom/squareup/protos/client/bills/SurchargeLineItem;Ljava/util/Map;)V

    check-cast v0, Lcom/squareup/checkout/Surcharge;

    :goto_0
    return-object v0

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 106
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Tax id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " is not found in disabled fees: "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/squareup/checkout/Surcharge;->disabledFees:Ljava/util/Map;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 p1, 0x2e

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 105
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public final withoutTaxes(Ljava/util/Set;)Lcom/squareup/checkout/Surcharge;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/checkout/Surcharge;"
        }
    .end annotation

    const-string v0, "removedTaxIds"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    iget-object v0, p0, Lcom/squareup/checkout/Surcharge;->surchargeProto:Lcom/squareup/protos/client/bills/SurchargeLineItem;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/SurchargeLineItem;->fee:Ljava/util/List;

    const-string v1, "surchargeProto.fee"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 211
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 212
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/squareup/protos/client/bills/FeeLineItem;

    .line 86
    iget-object v4, v3, Lcom/squareup/protos/client/bills/FeeLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;

    iget-object v4, v4, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;->fee:Lcom/squareup/api/items/Fee;

    iget-object v4, v4, Lcom/squareup/api/items/Fee;->id:Ljava/lang/String;

    .line 87
    invoke-interface {p1, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 89
    iget-object v6, p0, Lcom/squareup/checkout/Surcharge;->disabledFees:Ljava/util/Map;

    const-string v7, "feeId"

    invoke-static {v4, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v7, "feeLineItem"

    invoke-static {v3, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v6, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    xor-int/lit8 v3, v5, 0x1

    if-eqz v3, :cond_0

    .line 91
    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 213
    :cond_2
    check-cast v1, Ljava/util/List;

    .line 94
    iget-object p1, p0, Lcom/squareup/checkout/Surcharge;->surchargeProto:Lcom/squareup/protos/client/bills/SurchargeLineItem;

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/SurchargeLineItem;->newBuilder()Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;

    move-result-object p1

    .line 95
    invoke-virtual {p1, v1}, Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;->fee(Ljava/util/List;)Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;

    move-result-object p1

    .line 96
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;->build()Lcom/squareup/protos/client/bills/SurchargeLineItem;

    move-result-object p1

    .line 99
    instance-of v0, p0, Lcom/squareup/checkout/Surcharge$AutoGratuity;

    const-string/jumbo v1, "updatedProto"

    if-eqz v0, :cond_3

    new-instance v0, Lcom/squareup/checkout/Surcharge$AutoGratuity;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/squareup/checkout/Surcharge;->disabledFees:Ljava/util/Map;

    invoke-direct {v0, p1, v1}, Lcom/squareup/checkout/Surcharge$AutoGratuity;-><init>(Lcom/squareup/protos/client/bills/SurchargeLineItem;Ljava/util/Map;)V

    check-cast v0, Lcom/squareup/checkout/Surcharge;

    goto :goto_1

    .line 100
    :cond_3
    instance-of v0, p0, Lcom/squareup/checkout/Surcharge$CustomSurcharge;

    if-eqz v0, :cond_4

    new-instance v0, Lcom/squareup/checkout/Surcharge$CustomSurcharge;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/squareup/checkout/Surcharge;->disabledFees:Ljava/util/Map;

    invoke-direct {v0, p1, v1}, Lcom/squareup/checkout/Surcharge$CustomSurcharge;-><init>(Lcom/squareup/protos/client/bills/SurchargeLineItem;Ljava/util/Map;)V

    check-cast v0, Lcom/squareup/checkout/Surcharge;

    :goto_1
    return-object v0

    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
