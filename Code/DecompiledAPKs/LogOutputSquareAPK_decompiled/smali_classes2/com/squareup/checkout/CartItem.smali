.class public Lcom/squareup/checkout/CartItem;
.super Ljava/lang/Object;
.source "CartItem.java"

# interfaces
.implements Lcom/squareup/calc/order/Item;
.implements Lcom/squareup/itemsorter/SortableItem;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkout/CartItem$Transform;,
        Lcom/squareup/checkout/CartItem$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/calc/order/Item;",
        "Lcom/squareup/itemsorter/SortableItem<",
        "Lcom/squareup/checkout/CartItem;",
        "Lcom/squareup/checkout/DiningOption;",
        ">;"
    }
.end annotation


# static fields
.field private static final GIFT_CARD_NAME_SEPARATOR:Ljava/lang/String; = " "


# instance fields
.field public final appliedDiscounts:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Discount;",
            ">;"
        }
    .end annotation
.end field

.field private final appliedModifiers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/OrderModifier;",
            ">;"
        }
    .end annotation
.end field

.field public final appliedTaxes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Tax;",
            ">;"
        }
    .end annotation
.end field

.field public final backingDetails:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

.field public final backingType:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

.field public final blacklistedDiscounts:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final category:Lcom/squareup/api/items/MenuCategory;

.field public final color:Ljava/lang/String;

.field public final courseId:Ljava/lang/String;

.field public final createdAt:Ljava/util/Date;

.field public final defaultTaxes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Tax;",
            ">;"
        }
    .end annotation
.end field

.field public final destination:Lcom/squareup/checkout/OrderDestination;

.field public final events:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Itemization$Event;",
            ">;"
        }
    .end annotation
.end field

.field public final featureDetails:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;

.field private final hasHiddenModifier:Z

.field public final idPair:Lcom/squareup/protos/client/IdPair;

.field public final isEcomAvailable:Z

.field private final isTaxedInTransactionsHistory:Ljava/lang/Boolean;

.field private final isVoided:Z

.field public final itemAbbreviation:Ljava/lang/String;

.field public final itemDescription:Ljava/lang/String;

.field public final itemId:Ljava/lang/String;

.field public final itemName:Ljava/lang/String;

.field public final itemOptions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;",
            ">;"
        }
    .end annotation
.end field

.field public final itemizedAdjustmentAmountsFromTransactionsHistoryById:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final lockConfiguration:Z

.field public final merchantCatalogObjectToken:Ljava/lang/String;

.field public final modifierLists:Ljava/util/SortedMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/SortedMap<",
            "Ljava/lang/Integer;",
            "Lcom/squareup/checkout/OrderModifierList;",
            ">;"
        }
    .end annotation
.end field

.field public final notes:Ljava/lang/String;

.field public final overridePrice:Lcom/squareup/protos/common/Money;

.field public final photoToken:Ljava/lang/String;

.field public final photoUrl:Ljava/lang/String;

.field public final pricingRuleAppliedDiscounts:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final quantity:Ljava/math/BigDecimal;

.field public final quantityEntryType:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

.field public final ruleBasedTaxes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Tax;",
            ">;"
        }
    .end annotation
.end field

.field private final selectedDiningOption:Lcom/squareup/checkout/DiningOption;

.field public final selectedModifiers:Ljava/util/SortedMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/SortedMap<",
            "Ljava/lang/Integer;",
            "Ljava/util/SortedMap<",
            "Ljava/lang/Integer;",
            "Lcom/squareup/checkout/OrderModifier;",
            ">;>;"
        }
    .end annotation
.end field

.field public final selectedVariation:Lcom/squareup/checkout/OrderVariation;

.field private final showVariationNameOverride:Z

.field private final skipModifierDetailScreen:Z

.field public final userEditedTaxIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final variablePrice:Lcom/squareup/protos/common/Money;

.field public final variations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/checkout/OrderVariation;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/squareup/checkout/CartItem$Builder;)V
    .locals 3

    .line 367
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 368
    invoke-static {p1}, Lcom/squareup/checkout/CartItem$Builder;->access$100(Lcom/squareup/checkout/CartItem$Builder;)Ljava/util/Map;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/calc/util/AdjustmentComparator;->sortToCalculationOrder(Ljava/util/Map;)Ljava/util/LinkedHashMap;

    move-result-object v0

    .line 369
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/checkout/CartItem;->appliedDiscounts:Ljava/util/Map;

    .line 370
    invoke-static {p1}, Lcom/squareup/checkout/CartItem$Builder;->access$200(Lcom/squareup/checkout/CartItem$Builder;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/checkout/CartItem;->pricingRuleAppliedDiscounts:Ljava/util/Set;

    .line 371
    invoke-static {p1}, Lcom/squareup/checkout/CartItem$Builder;->access$300(Lcom/squareup/checkout/CartItem$Builder;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/checkout/CartItem;->blacklistedDiscounts:Ljava/util/Set;

    .line 372
    invoke-static {p1}, Lcom/squareup/checkout/CartItem$Builder;->access$400(Lcom/squareup/checkout/CartItem$Builder;)Ljava/util/Map;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/calc/util/AdjustmentComparator;->sortToCalculationOrder(Ljava/util/Map;)Ljava/util/LinkedHashMap;

    move-result-object v0

    .line 373
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/checkout/CartItem;->appliedTaxes:Ljava/util/Map;

    .line 375
    invoke-static {p1}, Lcom/squareup/checkout/CartItem$Builder;->access$500(Lcom/squareup/checkout/CartItem$Builder;)Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/checkout/CartItem;->backingDetails:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    .line 376
    invoke-static {p1}, Lcom/squareup/checkout/CartItem$Builder;->access$600(Lcom/squareup/checkout/CartItem$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/checkout/CartItem;->color:Ljava/lang/String;

    .line 377
    invoke-static {p1}, Lcom/squareup/checkout/CartItem$Builder;->access$700(Lcom/squareup/checkout/CartItem$Builder;)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/checkout/CartItem;->createdAt:Ljava/util/Date;

    .line 381
    invoke-static {p1}, Lcom/squareup/checkout/CartItem$Builder;->access$800(Lcom/squareup/checkout/CartItem$Builder;)Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 382
    invoke-static {p1}, Lcom/squareup/checkout/CartItem$Builder;->access$800(Lcom/squareup/checkout/CartItem$Builder;)Ljava/util/Map;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/calc/util/AdjustmentComparator;->sortToCalculationOrder(Ljava/util/Map;)Ljava/util/LinkedHashMap;

    move-result-object v0

    .line 383
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/checkout/CartItem;->defaultTaxes:Ljava/util/Map;

    goto :goto_0

    .line 385
    :cond_0
    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->appliedTaxes:Ljava/util/Map;

    iput-object v0, p0, Lcom/squareup/checkout/CartItem;->defaultTaxes:Ljava/util/Map;

    .line 388
    :goto_0
    invoke-static {p1}, Lcom/squareup/checkout/CartItem$Builder;->access$900(Lcom/squareup/checkout/CartItem$Builder;)Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 389
    invoke-static {p1}, Lcom/squareup/checkout/CartItem$Builder;->access$900(Lcom/squareup/checkout/CartItem$Builder;)Ljava/util/Map;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/calc/util/AdjustmentComparator;->sortToCalculationOrder(Ljava/util/Map;)Ljava/util/LinkedHashMap;

    move-result-object v0

    .line 390
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/checkout/CartItem;->ruleBasedTaxes:Ljava/util/Map;

    goto :goto_1

    .line 392
    :cond_1
    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->appliedTaxes:Ljava/util/Map;

    iput-object v0, p0, Lcom/squareup/checkout/CartItem;->ruleBasedTaxes:Ljava/util/Map;

    .line 395
    :goto_1
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-static {p1}, Lcom/squareup/checkout/CartItem$Builder;->access$1000(Lcom/squareup/checkout/CartItem$Builder;)Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/LinkedHashSet;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/checkout/CartItem;->userEditedTaxIds:Ljava/util/Set;

    .line 397
    invoke-static {p1}, Lcom/squareup/checkout/CartItem$Builder;->access$1100(Lcom/squareup/checkout/CartItem$Builder;)Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/checkout/CartItem;->idPair:Lcom/squareup/protos/client/IdPair;

    .line 398
    invoke-static {p1}, Lcom/squareup/checkout/CartItem$Builder;->access$1200(Lcom/squareup/checkout/CartItem$Builder;)Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/checkout/CartItem;->backingType:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    .line 399
    invoke-static {p1}, Lcom/squareup/checkout/CartItem$Builder;->access$1300(Lcom/squareup/checkout/CartItem$Builder;)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/checkout/CartItem;->isTaxedInTransactionsHistory:Ljava/lang/Boolean;

    .line 401
    invoke-static {p1}, Lcom/squareup/checkout/CartItem$Builder;->access$1400(Lcom/squareup/checkout/CartItem$Builder;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/checkout/CartItem;->itemizedAdjustmentAmountsFromTransactionsHistoryById:Ljava/util/Map;

    .line 402
    invoke-static {p1}, Lcom/squareup/checkout/CartItem$Builder;->access$1500(Lcom/squareup/checkout/CartItem$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/checkout/CartItem;->itemAbbreviation:Ljava/lang/String;

    .line 403
    invoke-static {p1}, Lcom/squareup/checkout/CartItem$Builder;->access$1600(Lcom/squareup/checkout/CartItem$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/checkout/CartItem;->itemId:Ljava/lang/String;

    .line 404
    invoke-static {p1}, Lcom/squareup/checkout/CartItem$Builder;->access$1700(Lcom/squareup/checkout/CartItem$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/checkout/CartItem;->itemName:Ljava/lang/String;

    .line 405
    invoke-static {p1}, Lcom/squareup/checkout/CartItem$Builder;->access$1800(Lcom/squareup/checkout/CartItem$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/checkout/CartItem;->itemDescription:Ljava/lang/String;

    .line 406
    invoke-static {p1}, Lcom/squareup/checkout/CartItem$Builder;->access$1900(Lcom/squareup/checkout/CartItem$Builder;)Lcom/squareup/api/items/MenuCategory;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/checkout/CartItem;->category:Lcom/squareup/api/items/MenuCategory;

    .line 407
    invoke-static {p1}, Lcom/squareup/checkout/CartItem$Builder;->access$2000(Lcom/squareup/checkout/CartItem$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/checkout/CartItem;->lockConfiguration:Z

    .line 408
    invoke-static {p1}, Lcom/squareup/checkout/CartItem$Builder;->access$2100(Lcom/squareup/checkout/CartItem$Builder;)Ljava/util/SortedMap;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/checkout/CartItem;->modifierLists:Ljava/util/SortedMap;

    .line 409
    invoke-static {p1}, Lcom/squareup/checkout/CartItem$Builder;->access$2200(Lcom/squareup/checkout/CartItem$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/checkout/CartItem;->notes:Ljava/lang/String;

    .line 410
    invoke-static {p1}, Lcom/squareup/checkout/CartItem$Builder;->access$2300(Lcom/squareup/checkout/CartItem$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/checkout/CartItem;->photoToken:Ljava/lang/String;

    .line 411
    invoke-static {p1}, Lcom/squareup/checkout/CartItem$Builder;->access$2400(Lcom/squareup/checkout/CartItem$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/checkout/CartItem;->photoUrl:Ljava/lang/String;

    .line 412
    invoke-static {p1}, Lcom/squareup/checkout/CartItem$Builder;->access$2500(Lcom/squareup/checkout/CartItem$Builder;)Ljava/math/BigDecimal;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/checkout/CartItem;->quantity:Ljava/math/BigDecimal;

    .line 413
    invoke-static {p1}, Lcom/squareup/checkout/CartItem$Builder;->access$2600(Lcom/squareup/checkout/CartItem$Builder;)Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/checkout/CartItem;->quantityEntryType:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    .line 414
    invoke-static {p1}, Lcom/squareup/checkout/CartItem$Builder;->access$2700(Lcom/squareup/checkout/CartItem$Builder;)Lcom/squareup/checkout/DiningOption;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/checkout/CartItem;->selectedDiningOption:Lcom/squareup/checkout/DiningOption;

    .line 415
    invoke-static {p1}, Lcom/squareup/checkout/CartItem$Builder;->access$2800(Lcom/squareup/checkout/CartItem$Builder;)Ljava/util/SortedMap;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/checkout/CartItem;->selectedModifiers:Ljava/util/SortedMap;

    .line 416
    invoke-static {p1}, Lcom/squareup/checkout/CartItem$Builder;->access$2900(Lcom/squareup/checkout/CartItem$Builder;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/checkout/CartItem;->appliedModifiers:Ljava/util/Map;

    .line 417
    invoke-static {p1}, Lcom/squareup/checkout/CartItem$Builder;->access$3000(Lcom/squareup/checkout/CartItem$Builder;)Lcom/squareup/checkout/OrderVariation;

    move-result-object v0

    const-string v1, "selectedVariation"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkout/OrderVariation;

    iput-object v0, p0, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    .line 418
    invoke-static {p1}, Lcom/squareup/checkout/CartItem$Builder;->access$3100(Lcom/squareup/checkout/CartItem$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/checkout/CartItem;->showVariationNameOverride:Z

    .line 419
    invoke-static {p1}, Lcom/squareup/checkout/CartItem$Builder;->access$3200(Lcom/squareup/checkout/CartItem$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/checkout/CartItem;->skipModifierDetailScreen:Z

    .line 420
    invoke-static {p1}, Lcom/squareup/checkout/CartItem$Builder;->access$3300(Lcom/squareup/checkout/CartItem$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/checkout/CartItem;->hasHiddenModifier:Z

    .line 421
    invoke-static {p1}, Lcom/squareup/checkout/CartItem$Builder;->access$3400(Lcom/squareup/checkout/CartItem$Builder;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/checkout/CartItem;->variablePrice:Lcom/squareup/protos/common/Money;

    .line 422
    invoke-static {p1}, Lcom/squareup/checkout/CartItem$Builder;->access$3500(Lcom/squareup/checkout/CartItem$Builder;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/checkout/CartItem;->overridePrice:Lcom/squareup/protos/common/Money;

    .line 423
    invoke-static {p1}, Lcom/squareup/checkout/CartItem$Builder;->access$3600(Lcom/squareup/checkout/CartItem$Builder;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/checkout/CartItem;->featureDetails:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;

    .line 424
    invoke-static {p1}, Lcom/squareup/checkout/CartItem$Builder;->access$3700(Lcom/squareup/checkout/CartItem$Builder;)Lcom/squareup/checkout/OrderDestination;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/checkout/CartItem;->destination:Lcom/squareup/checkout/OrderDestination;

    .line 425
    invoke-static {p1}, Lcom/squareup/checkout/CartItem$Builder;->access$3800(Lcom/squareup/checkout/CartItem$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/checkout/CartItem;->courseId:Ljava/lang/String;

    .line 426
    invoke-static {p1}, Lcom/squareup/checkout/CartItem$Builder;->access$3900(Lcom/squareup/checkout/CartItem$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/checkout/CartItem;->isEcomAvailable:Z

    .line 427
    invoke-static {p1}, Lcom/squareup/checkout/CartItem$Builder;->access$4000(Lcom/squareup/checkout/CartItem$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/checkout/CartItem;->merchantCatalogObjectToken:Ljava/lang/String;

    .line 429
    invoke-static {p1}, Lcom/squareup/checkout/CartItem$Builder;->access$4100(Lcom/squareup/checkout/CartItem$Builder;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    .line 430
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_2

    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    .line 431
    invoke-static {p1}, Lcom/squareup/checkout/CartItem$Builder;->access$4100(Lcom/squareup/checkout/CartItem$Builder;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 429
    :goto_2
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/checkout/CartItem;->variations:Ljava/util/List;

    .line 433
    invoke-static {p1}, Lcom/squareup/checkout/CartItem$Builder;->access$4200(Lcom/squareup/checkout/CartItem$Builder;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/checkout/CartItem;->itemOptions:Ljava/util/List;

    .line 434
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p1}, Lcom/squareup/checkout/CartItem$Builder;->access$4300(Lcom/squareup/checkout/CartItem$Builder;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/checkout/CartItem;->events:Ljava/util/List;

    .line 435
    invoke-static {p1}, Lcom/squareup/checkout/CartItem$Builder;->access$4400(Lcom/squareup/checkout/CartItem$Builder;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/checkout/CartItem;->isVoided:Z

    .line 437
    iget-object p1, p0, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    invoke-virtual {p1}, Lcom/squareup/checkout/OrderVariation;->isVariablePriced()Z

    move-result p1

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-eqz p1, :cond_5

    .line 438
    iget-object p1, p0, Lcom/squareup/checkout/CartItem;->variablePrice:Lcom/squareup/protos/common/Money;

    if-eqz p1, :cond_3

    const/4 p1, 0x1

    goto :goto_3

    :cond_3
    const/4 p1, 0x0

    :goto_3
    const-string v2, "Must set variablePrice since variation is variable."

    invoke-static {p1, v2}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 439
    iget-object p1, p0, Lcom/squareup/checkout/CartItem;->overridePrice:Lcom/squareup/protos/common/Money;

    if-nez p1, :cond_4

    const/4 v0, 0x1

    :cond_4
    const-string p1, "Must not set overridePrice since variation is variable."

    invoke-static {v0, p1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    goto :goto_4

    .line 441
    :cond_5
    iget-object p1, p0, Lcom/squareup/checkout/CartItem;->variablePrice:Lcom/squareup/protos/common/Money;

    if-nez p1, :cond_6

    const/4 v0, 0x1

    :cond_6
    const-string p1, "Must not set variablePrice since variation is fixed."

    invoke-static {v0, p1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 444
    :goto_4
    iget-object p1, p0, Lcom/squareup/checkout/CartItem;->photoToken:Ljava/lang/String;

    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_7

    .line 445
    iget-object p1, p0, Lcom/squareup/checkout/CartItem;->photoUrl:Ljava/lang/String;

    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    xor-int/2addr p1, v1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Must have photoUrl with non-blank photoToken="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkout/CartItem;->photoToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    :cond_7
    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/checkout/CartItem$Builder;Lcom/squareup/checkout/CartItem$1;)V
    .locals 0

    .line 106
    invoke-direct {p0, p1}, Lcom/squareup/checkout/CartItem;-><init>(Lcom/squareup/checkout/CartItem$Builder;)V

    return-void
.end method

.method static synthetic access$4600(Lcom/squareup/checkout/CartItem;)Ljava/lang/Boolean;
    .locals 0

    .line 106
    iget-object p0, p0, Lcom/squareup/checkout/CartItem;->isTaxedInTransactionsHistory:Ljava/lang/Boolean;

    return-object p0
.end method

.method static synthetic access$4700(Lcom/squareup/checkout/CartItem;)Lcom/squareup/checkout/DiningOption;
    .locals 0

    .line 106
    iget-object p0, p0, Lcom/squareup/checkout/CartItem;->selectedDiningOption:Lcom/squareup/checkout/DiningOption;

    return-object p0
.end method

.method static synthetic access$4800(Lcom/squareup/checkout/CartItem;)Z
    .locals 0

    .line 106
    iget-boolean p0, p0, Lcom/squareup/checkout/CartItem;->showVariationNameOverride:Z

    return p0
.end method

.method static synthetic access$4900(Lcom/squareup/checkout/CartItem;)Z
    .locals 0

    .line 106
    iget-boolean p0, p0, Lcom/squareup/checkout/CartItem;->skipModifierDetailScreen:Z

    return p0
.end method

.method static synthetic access$5000(Lcom/squareup/checkout/CartItem;)Z
    .locals 0

    .line 106
    iget-boolean p0, p0, Lcom/squareup/checkout/CartItem;->hasHiddenModifier:Z

    return p0
.end method

.method static synthetic access$5100(Lcom/squareup/checkout/CartItem;)Z
    .locals 0

    .line 106
    iget-boolean p0, p0, Lcom/squareup/checkout/CartItem;->isVoided:Z

    return p0
.end method

.method public static fromBillHistory(Lcom/squareup/protos/client/bills/Bill;Lcom/squareup/util/Res;Lcom/squareup/checkout/OrderVariationNamer;ZLjava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/Bill;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/checkout/OrderVariationNamer;",
            "Z",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;"
        }
    .end annotation

    .line 343
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 346
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Cart$LineItems;->itemization:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 347
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Cart$LineItems;->itemization:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/bills/Itemization;

    .line 348
    new-instance v3, Lcom/squareup/checkout/CartItem$Builder;

    invoke-direct {v3}, Lcom/squareup/checkout/CartItem$Builder;-><init>()V

    .line 349
    invoke-virtual {v3, v2, p1, p2, p4}, Lcom/squareup/checkout/CartItem$Builder;->fromItemizationHistory(Lcom/squareup/protos/client/bills/Itemization;Lcom/squareup/util/Res;Lcom/squareup/checkout/OrderVariationNamer;Ljava/util/List;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v2

    .line 350
    invoke-virtual {v2}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object v2

    .line 348
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    if-eqz p3, :cond_1

    .line 355
    iget-object p3, p0, Lcom/squareup/protos/client/bills/Bill;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object p3, p3, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    if-eqz p3, :cond_1

    iget-object p3, p0, Lcom/squareup/protos/client/bills/Bill;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object p3, p3, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    iget-object p3, p3, Lcom/squareup/protos/client/bills/Cart$LineItems;->void_itemization:Ljava/util/List;

    if-eqz p3, :cond_1

    .line 356
    iget-object p0, p0, Lcom/squareup/protos/client/bills/Bill;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object p0, p0, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    iget-object p0, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->void_itemization:Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/squareup/protos/client/bills/Itemization;

    .line 357
    new-instance v1, Lcom/squareup/checkout/CartItem$Builder;

    invoke-direct {v1}, Lcom/squareup/checkout/CartItem$Builder;-><init>()V

    invoke-virtual {v1, p3, p1, p2, p4}, Lcom/squareup/checkout/CartItem$Builder;->fromItemizationHistory(Lcom/squareup/protos/client/bills/Itemization;Lcom/squareup/util/Res;Lcom/squareup/checkout/OrderVariationNamer;Ljava/util/List;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p3

    const/4 v1, 0x1

    .line 358
    invoke-virtual {p3, v1}, Lcom/squareup/checkout/CartItem$Builder;->isVoided(Z)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p3

    .line 359
    invoke-virtual {p3}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object p3

    .line 357
    invoke-interface {v0, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 364
    :cond_1
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static fromRefundedItemization(Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;Lcom/squareup/util/Res;Ljava/util/List;)Lcom/squareup/checkout/CartItem;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;",
            "Lcom/squareup/util/Res;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;",
            ">;)",
            "Lcom/squareup/checkout/CartItem;"
        }
    .end annotation

    .line 335
    new-instance v0, Lcom/squareup/checkout/CartItem$Builder;

    invoke-direct {v0}, Lcom/squareup/checkout/CartItem$Builder;-><init>()V

    iget-object p0, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    sget-object v1, Lcom/squareup/payment/OrderVariationNames;->INSTANCE:Lcom/squareup/payment/OrderVariationNames;

    .line 336
    invoke-virtual {v0, p0, p1, v1, p2}, Lcom/squareup/checkout/CartItem$Builder;->fromItemizationHistory(Lcom/squareup/protos/client/bills/Itemization;Lcom/squareup/util/Res;Lcom/squareup/checkout/OrderVariationNamer;Ljava/util/List;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p0

    .line 337
    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object p0

    return-object p0
.end method

.method private getAmounts(Lcom/squareup/calc/AdjustedItem;)Lcom/squareup/protos/client/bills/Itemization$Amounts;
    .locals 4

    .line 1135
    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem;->unitPriceOrNull()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 1136
    iget-object v1, v0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    .line 1137
    new-instance v2, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;-><init>()V

    .line 1138
    invoke-virtual {v2, v0}, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->item_variation_price_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/checkout/CartItem;->quantity:Ljava/math/BigDecimal;

    .line 1140
    invoke-static {v0, v3}, Lcom/squareup/quantity/SharedCalculationsKt;->itemVariationPriceTimesQuantityMoney(Lcom/squareup/protos/common/Money;Ljava/math/BigDecimal;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 1139
    invoke-virtual {v2, v0}, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->item_variation_price_times_quantity_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;

    move-result-object v0

    .line 1142
    invoke-virtual {p1}, Lcom/squareup/calc/AdjustedItem;->getTotalCollectedForAllDiscounts()J

    move-result-wide v2

    invoke-static {v2, v3, v1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v2

    .line 1141
    invoke-virtual {v0, v2}, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->discount_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;

    move-result-object v0

    .line 1143
    invoke-virtual {p1}, Lcom/squareup/calc/AdjustedItem;->getTotalCollectedForAllTaxes()J

    move-result-wide v2

    invoke-static {v2, v3, v1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->tax_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;

    move-result-object v0

    .line 1144
    invoke-virtual {p1}, Lcom/squareup/calc/AdjustedItem;->getAdjustedTotal()J

    move-result-wide v2

    invoke-static {v2, v3, v1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->total_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;

    move-result-object p1

    .line 1145
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$Amounts;

    move-result-object p1

    return-object p1
.end method

.method private getBackingDetails()Lcom/squareup/protos/client/bills/Itemization$BackingDetails;
    .locals 3

    .line 909
    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->backingDetails:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    if-nez v0, :cond_0

    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;-><init>()V

    goto :goto_0

    .line 911
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->newBuilder()Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;

    move-result-object v0

    .line 914
    :goto_0
    invoke-direct {p0}, Lcom/squareup/checkout/CartItem;->getSparseAvailableOptions()Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->available_options(Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;)Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;

    .line 918
    iget-object v1, p0, Lcom/squareup/checkout/CartItem;->backingType:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    sget-object v2, Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;->CUSTOM_ITEM_VARIATION:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    if-ne v1, v2, :cond_1

    .line 919
    invoke-direct {p0}, Lcom/squareup/checkout/CartItem;->getItem()Lcom/squareup/api/items/Item;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->item(Lcom/squareup/api/items/Item;)Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;

    .line 922
    :cond_1
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    move-result-object v0

    return-object v0
.end method

.method private getConfiguration(Lcom/squareup/calc/AdjustedItem;Z)Lcom/squareup/protos/client/bills/Itemization$Configuration;
    .locals 1

    .line 927
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;-><init>()V

    .line 928
    invoke-direct {p0, p1, p2}, Lcom/squareup/checkout/CartItem;->getSelectedOptions(Lcom/squareup/calc/AdjustedItem;Z)Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;->selected_options(Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;)Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/checkout/CartItem;->category:Lcom/squareup/api/items/MenuCategory;

    .line 929
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;->category(Lcom/squareup/api/items/MenuCategory;)Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/checkout/CartItem;->backingType:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    .line 930
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;->backing_type(Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;)Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;

    move-result-object p1

    .line 931
    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem;->unitPriceOrNull()Lcom/squareup/protos/common/Money;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;->item_variation_price_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;

    move-result-object p1

    .line 932
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$Configuration;

    move-result-object p1

    return-object p1
.end method

.method private getDiningOptionLineItem()Lcom/squareup/protos/client/bills/DiningOptionLineItem;
    .locals 2

    .line 1118
    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->selectedDiningOption:Lcom/squareup/checkout/DiningOption;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 1122
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/checkout/DiningOption;->getApplicationScope()Lcom/squareup/protos/client/bills/ApplicationScope;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1126
    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->selectedDiningOption:Lcom/squareup/checkout/DiningOption;

    invoke-virtual {v0}, Lcom/squareup/checkout/DiningOption;->buildDiningOptionLineItem()Lcom/squareup/protos/client/bills/DiningOptionLineItem;

    move-result-object v0

    return-object v0

    .line 1131
    :cond_1
    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->selectedDiningOption:Lcom/squareup/checkout/DiningOption;

    sget-object v1, Lcom/squareup/protos/client/bills/ApplicationScope;->ITEMIZATION_LEVEL:Lcom/squareup/protos/client/bills/ApplicationScope;

    invoke-virtual {v0, v1}, Lcom/squareup/checkout/DiningOption;->copy(Lcom/squareup/protos/client/bills/ApplicationScope;)Lcom/squareup/checkout/DiningOption;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/checkout/DiningOption;->buildDiningOptionLineItem()Lcom/squareup/protos/client/bills/DiningOptionLineItem;

    move-result-object v0

    return-object v0
.end method

.method private getFeatureDetailsWithDiscountBlacklist()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;
    .locals 4

    .line 937
    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->blacklistedDiscounts:Ljava/util/Set;

    if-eqz v0, :cond_0

    .line 938
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    .line 940
    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->featureDetails:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;

    return-object v0

    .line 943
    :cond_1
    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->featureDetails:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;

    if-nez v0, :cond_2

    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;-><init>()V

    goto :goto_1

    .line 945
    :cond_2
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->newBuilder()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;

    move-result-object v0

    .line 947
    :goto_1
    new-instance v1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState$Builder;-><init>()V

    .line 948
    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/squareup/checkout/CartItem;->blacklistedDiscounts:Ljava/util/Set;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 949
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState$Builder;->blacklisted_discount_ids(Ljava/util/List;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState$Builder;

    .line 950
    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;->pricing_engine_state(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;

    .line 952
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;

    move-result-object v0

    return-object v0
.end method

.method private getItem()Lcom/squareup/api/items/Item;
    .locals 4

    .line 956
    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->itemName:Ljava/lang/String;

    .line 957
    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem;->getItemType()Lcom/squareup/api/items/Item$Type;

    move-result-object v1

    .line 958
    sget-object v2, Lcom/squareup/api/items/Item$Type;->GIFT_CARD:Lcom/squareup/api/items/Item$Type;

    if-ne v1, v2, :cond_0

    .line 959
    iget-object v2, p0, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    .line 960
    invoke-virtual {v2}, Lcom/squareup/checkout/OrderVariation;->getGiftCardDetails()Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails;->pan_suffix:Ljava/lang/String;

    const-string v3, " "

    .line 959
    invoke-static {v0, v3, v2}, Lcom/squareup/util/Strings;->removeSuffix(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 963
    :cond_0
    new-instance v2, Lcom/squareup/api/items/Item$Builder;

    invoke-direct {v2}, Lcom/squareup/api/items/Item$Builder;-><init>()V

    .line 964
    invoke-virtual {v2, v0}, Lcom/squareup/api/items/Item$Builder;->name(Ljava/lang/String;)Lcom/squareup/api/items/Item$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/checkout/CartItem;->itemId:Ljava/lang/String;

    .line 965
    invoke-virtual {v0, v2}, Lcom/squareup/api/items/Item$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/Item$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/checkout/CartItem;->itemAbbreviation:Ljava/lang/String;

    .line 966
    invoke-virtual {v0, v2}, Lcom/squareup/api/items/Item$Builder;->abbreviation(Ljava/lang/String;)Lcom/squareup/api/items/Item$Builder;

    move-result-object v0

    iget-boolean v2, p0, Lcom/squareup/checkout/CartItem;->skipModifierDetailScreen:Z

    .line 967
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/api/items/Item$Builder;->skips_modifier_screen(Ljava/lang/Boolean;)Lcom/squareup/api/items/Item$Builder;

    move-result-object v0

    .line 968
    invoke-virtual {v0, v1}, Lcom/squareup/api/items/Item$Builder;->type(Lcom/squareup/api/items/Item$Type;)Lcom/squareup/api/items/Item$Builder;

    move-result-object v0

    .line 969
    invoke-virtual {v0}, Lcom/squareup/api/items/Item$Builder;->build()Lcom/squareup/api/items/Item;

    move-result-object v0

    return-object v0
.end method

.method private getItemVariationDetails()Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;
    .locals 3

    .line 1035
    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 1044
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem;->isCustomItem()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem;->isGiftCard()Z

    move-result v0

    if-nez v0, :cond_1

    return-object v1

    .line 1048
    :cond_1
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$Builder;-><init>()V

    new-instance v1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$BackingDetails$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$BackingDetails$Builder;-><init>()V

    iget-object v2, p0, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    .line 1050
    invoke-virtual {v2}, Lcom/squareup/checkout/OrderVariation;->getItemVariation()Lcom/squareup/api/items/ItemVariation;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$BackingDetails$Builder;->item_variation(Lcom/squareup/api/items/ItemVariation;)Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$BackingDetails$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    .line 1051
    invoke-virtual {v2}, Lcom/squareup/checkout/OrderVariation;->getQuantityUnit()Lcom/squareup/orders/model/Order$QuantityUnit;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$BackingDetails$Builder;->measurement_unit(Lcom/squareup/orders/model/Order$QuantityUnit;)Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$BackingDetails$Builder;

    move-result-object v1

    .line 1052
    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$BackingDetails$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$BackingDetails;

    move-result-object v1

    .line 1049
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$Builder;->write_only_backing_details(Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$BackingDetails;)Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    .line 1053
    invoke-virtual {v1}, Lcom/squareup/checkout/OrderVariation;->getDisplayDetails()Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$DisplayDetails;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$Builder;->read_only_display_details(Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$DisplayDetails;)Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$Builder;

    move-result-object v0

    .line 1054
    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem;->shouldShowVariationName()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$Builder;->display_variation_name(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    .line 1055
    invoke-virtual {v1}, Lcom/squareup/checkout/OrderVariation;->getGiftCardDetails()Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$Builder;->gift_card_details(Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails;)Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$Builder;

    move-result-object v0

    .line 1056
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;

    move-result-object v0

    return-object v0
.end method

.method private getItemizedAdjustments(Lcom/squareup/calc/AdjustedItem;Lcom/squareup/protos/common/CurrencyCode;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/calc/AdjustedItem;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/value/ItemizedAdjustment;",
            ">;"
        }
    .end annotation

    .line 883
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 884
    invoke-virtual {p1}, Lcom/squareup/calc/AdjustedItem;->getTotalCollectedPerAdjustment()Ljava/util/Map;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 885
    new-instance v2, Lcom/squareup/server/payment/value/ItemizedAdjustment;

    .line 886
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5, p2}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Lcom/squareup/server/payment/value/ItemizedAdjustment;-><init>(Ljava/lang/String;Lcom/squareup/protos/common/Money;)V

    .line 885
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method private getSelectedOptions(Lcom/squareup/calc/AdjustedItem;Z)Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;
    .locals 4

    .line 975
    invoke-direct {p0}, Lcom/squareup/checkout/CartItem;->getItemVariationDetails()Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;

    move-result-object v0

    .line 976
    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem;->getModifierOptionLineItems()Ljava/util/List;

    move-result-object v1

    .line 977
    invoke-virtual {p0, p1, p2}, Lcom/squareup/checkout/CartItem;->getDiscountLineItems(Lcom/squareup/calc/AdjustedItem;Z)Ljava/util/List;

    move-result-object p2

    .line 979
    invoke-virtual {p0, p1}, Lcom/squareup/checkout/CartItem;->getFeeLineItems(Lcom/squareup/calc/AdjustedItem;)Ljava/util/List;

    move-result-object p1

    .line 980
    invoke-direct {p0}, Lcom/squareup/checkout/CartItem;->getDiningOptionLineItem()Lcom/squareup/protos/client/bills/DiningOptionLineItem;

    move-result-object v2

    if-nez v0, :cond_0

    .line 983
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 984
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 985
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    if-nez v2, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 990
    :cond_0
    new-instance v3, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;

    invoke-direct {v3}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;-><init>()V

    .line 991
    invoke-virtual {v3, v0}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->item_variation_details(Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;)Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;

    move-result-object v0

    .line 992
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->modifier_option(Ljava/util/List;)Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;

    move-result-object v0

    .line 993
    invoke-virtual {v0, p2}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->discount(Ljava/util/List;)Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;

    move-result-object p2

    .line 994
    invoke-virtual {p2, p1}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->fee(Ljava/util/List;)Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;

    move-result-object p1

    .line 995
    invoke-virtual {p1, v2}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->dining_option(Lcom/squareup/protos/client/bills/DiningOptionLineItem;)Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;

    move-result-object p1

    .line 996
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    move-result-object p1

    return-object p1
.end method

.method private getSparseAvailableOptions()Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;
    .locals 7

    .line 1002
    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem;->isCustomItem()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 1006
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1008
    iget-object v1, p0, Lcom/squareup/checkout/CartItem;->selectedModifiers:Ljava/util/SortedMap;

    invoke-interface {v1}, Ljava/util/SortedMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 1011
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1012
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/SortedMap;

    invoke-interface {v4}, Ljava/util/SortedMap;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/checkout/OrderModifier;

    .line 1014
    invoke-virtual {v5}, Lcom/squareup/checkout/OrderModifier;->hasBackingDetails()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1015
    invoke-virtual {v5}, Lcom/squareup/checkout/OrderModifier;->getBackingDetails()Lcom/squareup/api/items/ItemModifierOption;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1019
    :cond_3
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 1020
    iget-object v4, p0, Lcom/squareup/checkout/CartItem;->modifierLists:Ljava/util/SortedMap;

    invoke-interface {v4, v2}, Ljava/util/SortedMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1021
    new-instance v4, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList$Builder;

    invoke-direct {v4}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList$Builder;-><init>()V

    .line 1022
    invoke-virtual {v4, v3}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList$Builder;->modifier_option(Ljava/util/List;)Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/checkout/CartItem;->modifierLists:Ljava/util/SortedMap;

    .line 1023
    invoke-interface {v4, v2}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/OrderModifierList;

    iget-object v2, v2, Lcom/squareup/checkout/OrderModifierList;->itemModifierList:Lcom/squareup/api/items/ItemModifierList;

    invoke-virtual {v3, v2}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList$Builder;->modifier_list(Lcom/squareup/api/items/ItemModifierList;)Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList$Builder;

    move-result-object v2

    .line 1024
    invoke-virtual {v2}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList;

    move-result-object v2

    .line 1021
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1028
    :cond_4
    new-instance v1, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$Builder;-><init>()V

    iget-object v2, p0, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    .line 1029
    invoke-virtual {v2}, Lcom/squareup/checkout/OrderVariation;->getItemVariation()Lcom/squareup/api/items/ItemVariation;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$Builder;->item_variation(Ljava/util/List;)Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$Builder;

    move-result-object v1

    .line 1030
    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$Builder;->modifier_list(Ljava/util/List;)Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$Builder;

    move-result-object v0

    .line 1031
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;

    move-result-object v0

    return-object v0
.end method

.method public static of(Ljava/util/List;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/Itemization;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;"
        }
    .end annotation

    .line 315
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 316
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/server/payment/Itemization;

    .line 317
    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2}, Ljava/util/LinkedHashMap;-><init>()V

    .line 318
    iget-object v3, v1, Lcom/squareup/server/payment/Itemization;->adjustments:Ljava/util/List;

    if-eqz v3, :cond_0

    .line 319
    iget-object v3, v1, Lcom/squareup/server/payment/Itemization;->adjustments:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/server/payment/value/ItemizedAdjustment;

    .line 320
    invoke-virtual {v4}, Lcom/squareup/server/payment/value/ItemizedAdjustment;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4}, Lcom/squareup/server/payment/value/ItemizedAdjustment;->getApplied()Lcom/squareup/protos/common/Money;

    move-result-object v4

    iget-object v4, v4, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-interface {v2, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 324
    :cond_0
    new-instance v3, Lcom/squareup/checkout/CartItem$Builder;

    invoke-direct {v3}, Lcom/squareup/checkout/CartItem$Builder;-><init>()V

    .line 325
    invoke-virtual {v3, v1}, Lcom/squareup/checkout/CartItem$Builder;->of(Lcom/squareup/server/payment/Itemization;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v1

    .line 326
    invoke-static {v1, v2}, Lcom/squareup/checkout/CartItem$Builder;->access$000(Lcom/squareup/checkout/CartItem$Builder;Ljava/util/Map;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v1

    .line 327
    invoke-virtual {v1}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object v1

    .line 328
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 330
    :cond_1
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public appliedDiscounts()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Discount;",
            ">;"
        }
    .end annotation

    .line 849
    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->appliedDiscounts:Ljava/util/Map;

    return-object v0
.end method

.method public appliedModifiers()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/OrderModifier;",
            ">;"
        }
    .end annotation

    .line 841
    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->appliedModifiers:Ljava/util/Map;

    return-object v0
.end method

.method public appliedTaxes()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Tax;",
            ">;"
        }
    .end annotation

    .line 845
    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->appliedTaxes:Ljava/util/Map;

    return-object v0
.end method

.method public baseAmount()J
    .locals 2

    .line 806
    invoke-static {p0}, Lcom/squareup/quantity/SharedCalculationsKt;->itemBaseAmount(Lcom/squareup/calc/order/Item;)J

    move-result-wide v0

    return-wide v0
.end method

.method public buildItemization(Lcom/squareup/calc/AdjustedItem;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/server/payment/Itemization;
    .locals 16

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    .line 853
    invoke-direct/range {p0 .. p2}, Lcom/squareup/checkout/CartItem;->getItemizedAdjustments(Lcom/squareup/calc/AdjustedItem;Lcom/squareup/protos/common/CurrencyCode;)Ljava/util/List;

    move-result-object v13

    .line 855
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 856
    iget-object v2, v0, Lcom/squareup/checkout/CartItem;->selectedModifiers:Ljava/util/SortedMap;

    invoke-interface {v2}, Ljava/util/SortedMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/SortedMap;

    .line 857
    invoke-interface {v3}, Ljava/util/SortedMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/checkout/OrderModifier;

    .line 858
    invoke-virtual {v4}, Lcom/squareup/checkout/OrderModifier;->getBasePriceTimesModifierQuantityOrNull()Lcom/squareup/protos/common/Money;

    move-result-object v5

    if-nez v5, :cond_1

    const-wide/16 v5, 0x0

    .line 860
    invoke-static {v5, v6, v1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v5

    .line 862
    :cond_1
    new-instance v6, Lcom/squareup/server/payment/ItemModifier;

    .line 863
    invoke-virtual {v4}, Lcom/squareup/checkout/OrderModifier;->getModifierId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4}, Lcom/squareup/checkout/OrderModifier;->getItemModifierName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v6, v7, v4, v5}, Lcom/squareup/server/payment/ItemModifier;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;)V

    .line 862
    invoke-interface {v14, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 867
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/calc/AdjustedItem;->getAdjustedTotal()J

    move-result-wide v2

    invoke-static {v2, v3, v1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v12

    .line 871
    iget-object v1, v0, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    invoke-virtual {v1}, Lcom/squareup/checkout/OrderVariation;->hasUserConfiguredName()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_3

    .line 872
    iget-object v1, v0, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    invoke-virtual {v1}, Lcom/squareup/checkout/OrderVariation;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v2}, Lcom/squareup/util/Strings;->valueOrDefault(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v6, v1

    goto :goto_1

    :cond_3
    move-object v6, v2

    .line 875
    :goto_1
    iget-object v1, v0, Lcom/squareup/checkout/CartItem;->itemId:Ljava/lang/String;

    iget-object v2, v0, Lcom/squareup/checkout/CartItem;->itemName:Ljava/lang/String;

    iget-object v3, v0, Lcom/squareup/checkout/CartItem;->photoToken:Ljava/lang/String;

    iget-object v4, v0, Lcom/squareup/checkout/CartItem;->notes:Ljava/lang/String;

    iget-object v5, v0, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    invoke-virtual {v5}, Lcom/squareup/checkout/OrderVariation;->getId()Ljava/lang/String;

    move-result-object v5

    iget-object v7, v0, Lcom/squareup/checkout/CartItem;->quantity:Ljava/math/BigDecimal;

    iget-object v8, v0, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    .line 876
    invoke-virtual {v8}, Lcom/squareup/checkout/OrderVariation;->getQuantityPrecision()I

    move-result v8

    iget-object v9, v0, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    .line 877
    invoke-virtual {v9}, Lcom/squareup/checkout/OrderVariation;->getUnitName()Ljava/lang/String;

    move-result-object v9

    iget-object v10, v0, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    invoke-virtual {v10}, Lcom/squareup/checkout/OrderVariation;->getUnitAbbreviation()Ljava/lang/String;

    move-result-object v10

    invoke-virtual/range {p0 .. p0}, Lcom/squareup/checkout/CartItem;->unitPriceOrNull()Lcom/squareup/protos/common/Money;

    move-result-object v11

    iget-object v15, v0, Lcom/squareup/checkout/CartItem;->color:Ljava/lang/String;

    .line 875
    invoke-static/range {v1 .. v15}, Lcom/squareup/server/payment/Itemization;->forRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;ILjava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)Lcom/squareup/server/payment/Itemization;

    move-result-object v1

    return-object v1
.end method

.method public buildUpon()Lcom/squareup/checkout/CartItem$Builder;
    .locals 2

    .line 450
    new-instance v0, Lcom/squareup/checkout/CartItem$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/checkout/CartItem$Builder;-><init>(Lcom/squareup/checkout/CartItem;Lcom/squareup/checkout/CartItem$1;)V

    return-object v0
.end method

.method public canBeComped()Z
    .locals 1

    .line 474
    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem;->isInteresting()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem;->isComped()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem;->isVoided()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public canBeUncomped()Z
    .locals 1

    .line 478
    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem;->isInteresting()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem;->isComped()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public canCoalesce(Lcom/squareup/checkout/CartItem;)Z
    .locals 1

    .line 664
    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem;->isGiftCard()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    invoke-virtual {v0}, Lcom/squareup/checkout/OrderVariation;->isUnitPriced()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/squareup/checkout/CartItem;->isSameLibraryItem(Lcom/squareup/checkout/CartItem;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public categoryId()Ljava/lang/String;
    .locals 1

    .line 542
    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->category:Lcom/squareup/api/items/MenuCategory;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/api/items/MenuCategory;->id:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 0

    .line 1164
    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method getCompDiscount()Lcom/squareup/checkout/Discount;
    .locals 3

    .line 482
    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->appliedDiscounts:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/Discount;

    .line 483
    invoke-virtual {v1}, Lcom/squareup/checkout/Discount;->isComp()Z

    move-result v2

    if-eqz v2, :cond_0

    return-object v1

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public getCompReason()Ljava/lang/String;
    .locals 1

    .line 491
    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem;->getCompDiscount()Lcom/squareup/checkout/Discount;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 492
    :cond_0
    iget-object v0, v0, Lcom/squareup/checkout/Discount;->name:Ljava/lang/String;

    :goto_0
    return-object v0
.end method

.method public getCompingEmployeeToken()Ljava/lang/String;
    .locals 5

    .line 510
    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem;->isComped()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 514
    :cond_0
    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->events:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/bills/Itemization$Event;

    .line 515
    iget-object v3, v2, Lcom/squareup/protos/client/bills/Itemization$Event;->event_type:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    sget-object v4, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->COMP:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    if-ne v3, v4, :cond_1

    .line 516
    iget-object v0, v2, Lcom/squareup/protos/client/bills/Itemization$Event;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    if-eqz v0, :cond_2

    iget-object v0, v2, Lcom/squareup/protos/client/bills/Itemization$Event;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/CreatorDetails;->employee:Lcom/squareup/protos/client/Employee;

    if-eqz v0, :cond_2

    iget-object v0, v2, Lcom/squareup/protos/client/bills/Itemization$Event;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/CreatorDetails;->employee:Lcom/squareup/protos/client/Employee;

    iget-object v1, v0, Lcom/squareup/protos/client/Employee;->employee_token:Ljava/lang/String;

    :cond_2
    return-object v1

    .line 522
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "comped item does not contain comp event"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getDiningOption(Lcom/squareup/checkout/DiningOption;)Lcom/squareup/checkout/DiningOption;
    .locals 1

    .line 636
    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem;->hasDiningOption()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/squareup/checkout/CartItem;->selectedDiningOption:Lcom/squareup/checkout/DiningOption;

    :cond_0
    return-object p1
.end method

.method public getDiscountLineItems(Lcom/squareup/calc/AdjustedItem;Z)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/calc/AdjustedItem;",
            "Z)",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/DiscountLineItem;",
            ">;"
        }
    .end annotation

    .line 1087
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1088
    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem;->unitPriceOrNull()Lcom/squareup/protos/common/Money;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    .line 1090
    invoke-direct {p0, p1, v1}, Lcom/squareup/checkout/CartItem;->getItemizedAdjustments(Lcom/squareup/calc/AdjustedItem;Lcom/squareup/protos/common/CurrencyCode;)Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/server/payment/value/ItemizedAdjustment;

    .line 1091
    iget-object v2, p0, Lcom/squareup/checkout/CartItem;->appliedDiscounts:Ljava/util/Map;

    invoke-virtual {v1}, Lcom/squareup/server/payment/value/ItemizedAdjustment;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/Discount;

    if-eqz v2, :cond_0

    .line 1094
    invoke-virtual {v1}, Lcom/squareup/server/payment/value/ItemizedAdjustment;->getApplied()Lcom/squareup/protos/common/Money;

    move-result-object v1

    iget-object v3, p0, Lcom/squareup/checkout/CartItem;->pricingRuleAppliedDiscounts:Ljava/util/Set;

    iget-object v4, v2, Lcom/squareup/checkout/Discount;->id:Ljava/lang/String;

    .line 1096
    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    .line 1093
    invoke-virtual {v2, v1, p2, v3}, Lcom/squareup/checkout/Discount;->buildDiscountLineItem(Lcom/squareup/protos/common/Money;ZZ)Lcom/squareup/protos/client/bills/DiscountLineItem;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    if-eqz p2, :cond_2

    .line 1102
    invoke-static {v0}, Lcom/squareup/checkout/Discounts;->toMergedDiscountLineItems(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    return-object p1

    :cond_2
    return-object v0
.end method

.method public getDuration()Lorg/threeten/bp/Duration;
    .locals 2

    .line 1114
    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->featureDetails:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->appointments_service_details:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;->service_duration:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Lorg/threeten/bp/Duration;->ofMillis(J)Lorg/threeten/bp/Duration;

    move-result-object v0

    return-object v0
.end method

.method public getFeeLineItems(Lcom/squareup/calc/AdjustedItem;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/calc/AdjustedItem;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/FeeLineItem;",
            ">;"
        }
    .end annotation

    .line 1074
    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem;->unitPriceOrNull()Lcom/squareup/protos/common/Money;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    .line 1075
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1076
    invoke-direct {p0, p1, v0}, Lcom/squareup/checkout/CartItem;->getItemizedAdjustments(Lcom/squareup/calc/AdjustedItem;Lcom/squareup/protos/common/CurrencyCode;)Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/payment/value/ItemizedAdjustment;

    .line 1077
    iget-object v2, p0, Lcom/squareup/checkout/CartItem;->appliedTaxes:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/squareup/server/payment/value/ItemizedAdjustment;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/Tax;

    if-eqz v2, :cond_0

    .line 1079
    invoke-virtual {v0}, Lcom/squareup/server/payment/value/ItemizedAdjustment;->getApplied()Lcom/squareup/protos/common/Money;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3, v3}, Lcom/squareup/checkout/Tax;->buildFeeLineItem(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/FeeLineItem;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public getGiftCardServerToken()Ljava/lang/String;
    .locals 1

    .line 834
    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/checkout/OrderVariation;->getGiftCardDetails()Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 835
    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    invoke-virtual {v0}, Lcom/squareup/checkout/OrderVariation;->getGiftCardDetails()Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails;->gift_card_server_id:Ljava/lang/String;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemType()Lcom/squareup/api/items/Item$Type;
    .locals 1

    .line 819
    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->backingDetails:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->item:Lcom/squareup/api/items/Item;

    if-eqz v0, :cond_0

    .line 820
    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->backingDetails:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->item:Lcom/squareup/api/items/Item;

    iget-object v0, v0, Lcom/squareup/api/items/Item;->type:Lcom/squareup/api/items/Item$Type;

    return-object v0

    .line 825
    :cond_0
    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/squareup/checkout/OrderVariation;->getGiftCardDetails()Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 826
    sget-object v0, Lcom/squareup/api/items/Item$Type;->GIFT_CARD:Lcom/squareup/api/items/Item$Type;

    return-object v0

    .line 830
    :cond_1
    sget-object v0, Lcom/squareup/api/items/Item$Type;->REGULAR:Lcom/squareup/api/items/Item$Type;

    return-object v0
.end method

.method public getItemizationProto(Lcom/squareup/calc/AdjustedItem;Z)Lcom/squareup/protos/client/bills/Itemization;
    .locals 2

    .line 893
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Itemization$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/checkout/CartItem;->idPair:Lcom/squareup/protos/client/IdPair;

    .line 894
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Itemization$Builder;->itemization_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/Itemization$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/checkout/CartItem;->createdAt:Ljava/util/Date;

    .line 895
    invoke-static {v1}, Lcom/squareup/checkout/util/ISO8601Dates;->tryBuildISO8601Date(Ljava/util/Date;)Lcom/squareup/protos/client/ISO8601Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Itemization$Builder;->created_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bills/Itemization$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/checkout/CartItem;->quantity:Ljava/math/BigDecimal;

    .line 896
    invoke-virtual {v1}, Ljava/math/BigDecimal;->toPlainString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Itemization$Builder;->quantity(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Itemization$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    .line 897
    invoke-virtual {v1}, Lcom/squareup/checkout/OrderVariation;->getQuantityUnitOverride()Lcom/squareup/orders/model/Order$QuantityUnit;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Itemization$Builder;->measurement_unit(Lcom/squareup/orders/model/Order$QuantityUnit;)Lcom/squareup/protos/client/bills/Itemization$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/checkout/CartItem;->quantityEntryType:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    .line 898
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Itemization$Builder;->quantity_entry_type(Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;)Lcom/squareup/protos/client/bills/Itemization$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/checkout/CartItem;->notes:Ljava/lang/String;

    .line 899
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Itemization$Builder;->custom_note(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Itemization$Builder;

    move-result-object v0

    .line 900
    invoke-direct {p0, p1, p2}, Lcom/squareup/checkout/CartItem;->getConfiguration(Lcom/squareup/calc/AdjustedItem;Z)Lcom/squareup/protos/client/bills/Itemization$Configuration;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/protos/client/bills/Itemization$Builder;->configuration(Lcom/squareup/protos/client/bills/Itemization$Configuration;)Lcom/squareup/protos/client/bills/Itemization$Builder;

    move-result-object p2

    .line 901
    invoke-direct {p0}, Lcom/squareup/checkout/CartItem;->getBackingDetails()Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/protos/client/bills/Itemization$Builder;->write_only_backing_details(Lcom/squareup/protos/client/bills/Itemization$BackingDetails;)Lcom/squareup/protos/client/bills/Itemization$Builder;

    move-result-object p2

    .line 902
    invoke-direct {p0, p1}, Lcom/squareup/checkout/CartItem;->getAmounts(Lcom/squareup/calc/AdjustedItem;)Lcom/squareup/protos/client/bills/Itemization$Amounts;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/protos/client/bills/Itemization$Builder;->amounts(Lcom/squareup/protos/client/bills/Itemization$Amounts;)Lcom/squareup/protos/client/bills/Itemization$Builder;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/checkout/CartItem;->events:Ljava/util/List;

    .line 903
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/bills/Itemization$Builder;->event(Ljava/util/List;)Lcom/squareup/protos/client/bills/Itemization$Builder;

    move-result-object p1

    .line 904
    invoke-direct {p0}, Lcom/squareup/checkout/CartItem;->getFeatureDetailsWithDiscountBlacklist()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/bills/Itemization$Builder;->feature_details(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;)Lcom/squareup/protos/client/bills/Itemization$Builder;

    move-result-object p1

    .line 905
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$Builder;->build()Lcom/squareup/protos/client/bills/Itemization;

    move-result-object p1

    return-object p1
.end method

.method protected getModifierOptionLineItems()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/ModifierOptionLineItem;",
            ">;"
        }
    .end annotation

    .line 1060
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1061
    iget-object v1, p0, Lcom/squareup/checkout/CartItem;->selectedModifiers:Ljava/util/SortedMap;

    invoke-interface {v1}, Ljava/util/SortedMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/SortedMap;

    .line 1062
    invoke-interface {v2}, Ljava/util/SortedMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/checkout/OrderModifier;

    .line 1064
    invoke-virtual {v3}, Lcom/squareup/checkout/OrderModifier;->hasBackingDetails()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1065
    iget-object v4, p0, Lcom/squareup/checkout/CartItem;->quantity:Ljava/math/BigDecimal;

    .line 1066
    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem;->unitPriceOrNull()Lcom/squareup/protos/common/Money;

    move-result-object v5

    iget-object v5, v5, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-virtual {v3, v4, v5}, Lcom/squareup/checkout/OrderModifier;->toModifierOptionLineItem(Ljava/math/BigDecimal;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem;

    move-result-object v3

    .line 1065
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method public getPricingRuleAppliedDiscounts()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1190
    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->pricingRuleAppliedDiscounts:Ljava/util/Set;

    return-object v0
.end method

.method public getSelectedDiningOption()Lcom/squareup/checkout/DiningOption;
    .locals 1

    .line 617
    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->selectedDiningOption:Lcom/squareup/checkout/DiningOption;

    return-object v0
.end method

.method public bridge synthetic getSelectedDiningOption()Lcom/squareup/itemsorter/SortableDiningOption;
    .locals 1

    .line 106
    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem;->getSelectedDiningOption()Lcom/squareup/checkout/DiningOption;

    move-result-object v0

    return-object v0
.end method

.method public getVoidReason()Ljava/lang/String;
    .locals 5

    .line 496
    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem;->isVoided()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 500
    :cond_0
    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->events:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/bills/Itemization$Event;

    .line 501
    iget-object v3, v2, Lcom/squareup/protos/client/bills/Itemization$Event;->event_type:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    sget-object v4, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->VOID:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    if-ne v3, v4, :cond_1

    .line 502
    iget-object v0, v2, Lcom/squareup/protos/client/bills/Itemization$Event;->reason:Ljava/lang/String;

    return-object v0

    :cond_2
    return-object v1
.end method

.method public getVoidingEmployeeToken()Ljava/lang/String;
    .locals 5

    .line 526
    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem;->isVoided()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 530
    :cond_0
    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->events:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/bills/Itemization$Event;

    .line 531
    iget-object v3, v2, Lcom/squareup/protos/client/bills/Itemization$Event;->event_type:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    sget-object v4, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->VOID:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    if-ne v3, v4, :cond_1

    .line 532
    iget-object v0, v2, Lcom/squareup/protos/client/bills/Itemization$Event;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    if-eqz v0, :cond_2

    iget-object v0, v2, Lcom/squareup/protos/client/bills/Itemization$Event;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/CreatorDetails;->employee:Lcom/squareup/protos/client/Employee;

    if-eqz v0, :cond_2

    iget-object v0, v2, Lcom/squareup/protos/client/bills/Itemization$Event;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/CreatorDetails;->employee:Lcom/squareup/protos/client/Employee;

    iget-object v1, v0, Lcom/squareup/protos/client/Employee;->employee_token:Ljava/lang/String;

    :cond_2
    return-object v1

    .line 538
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "voided item does not contain void event"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public hasAppliedAdditiveTaxes()Z
    .locals 3

    .line 1219
    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->appliedTaxes:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/Tax;

    .line 1220
    iget-object v1, v1, Lcom/squareup/checkout/Tax;->inclusionType:Lcom/squareup/api/items/Fee$InclusionType;

    sget-object v2, Lcom/squareup/api/items/Fee$InclusionType;->ADDITIVE:Lcom/squareup/api/items/Fee$InclusionType;

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public hasAppliedDiscounts()Z
    .locals 1

    .line 1228
    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->appliedDiscounts:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public hasDiningOption()Z
    .locals 1

    .line 640
    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->selectedDiningOption:Lcom/squareup/checkout/DiningOption;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasDiscountThatCanBeAppliedToOnlyOneItem()Z
    .locals 2

    .line 1181
    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->appliedDiscounts:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/Discount;

    .line 1182
    invoke-virtual {v1}, Lcom/squareup/checkout/Discount;->canOnlyBeAppliedToOneItem()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public hasHiddenModifier()Z
    .locals 1

    .line 587
    iget-boolean v0, p0, Lcom/squareup/checkout/CartItem;->hasHiddenModifier:Z

    return v0
.end method

.method public hasNonFreeSelectedModifier()Z
    .locals 3

    .line 564
    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->selectedModifiers:Ljava/util/SortedMap;

    invoke-interface {v0}, Ljava/util/SortedMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/SortedMap;

    .line 565
    invoke-interface {v1}, Ljava/util/SortedMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/OrderModifier;

    .line 566
    invoke-virtual {v2}, Lcom/squareup/checkout/OrderModifier;->isFreeModifier()Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v0, 0x1

    return v0

    :cond_2
    const/4 v0, 0x0

    return v0
.end method

.method public hasPerItemCapableDiscounts()Z
    .locals 2

    .line 1172
    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->appliedDiscounts:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/Discount;

    .line 1173
    invoke-virtual {v1}, Lcom/squareup/checkout/Discount;->canBeAppliedPerItem()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public hasSelectedModifiers()Z
    .locals 2

    .line 552
    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->selectedModifiers:Ljava/util/SortedMap;

    invoke-interface {v0}, Ljava/util/SortedMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/SortedMap;

    .line 553
    invoke-interface {v1}, Ljava/util/SortedMap;->size()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public hasUnitPrice()Z
    .locals 1

    .line 688
    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem;->unitPriceOrNull()Lcom/squareup/protos/common/Money;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final hashCode()I
    .locals 1

    .line 1157
    invoke-super {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public isAsExpensiveAs(Lcom/squareup/checkout/CartItem;)Z
    .locals 1

    .line 675
    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem;->unitPriceWithModifiers()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 676
    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->unitPriceWithModifiers()Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 674
    invoke-static {v0, p1}, Lcom/squareup/money/MoneyMath;->isEqual(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result p1

    return p1
.end method

.method public isComped()Z
    .locals 1

    .line 470
    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem;->getCompDiscount()Lcom/squareup/checkout/Discount;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isCustomItem()Z
    .locals 2

    .line 1211
    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->backingType:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    sget-object v1, Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;->CUSTOM_AMOUNT:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->backingType:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    sget-object v1, Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;->CUSTOM_ITEM_VARIATION:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public isGiftCard()Z
    .locals 2

    .line 815
    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem;->getItemType()Lcom/squareup/api/items/Item$Type;

    move-result-object v0

    sget-object v1, Lcom/squareup/api/items/Item$Type;->GIFT_CARD:Lcom/squareup/api/items/Item$Type;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isInteresting()Z
    .locals 5

    .line 607
    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->itemId:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->notes:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->photoUrl:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem;->baseAmount()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public isLessExpensiveThan(Lcom/squareup/checkout/CartItem;)Z
    .locals 1

    .line 669
    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem;->unitPriceWithModifiers()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 670
    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->unitPriceWithModifiers()Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 668
    invoke-static {v0, p1}, Lcom/squareup/money/MoneyMath;->greaterThanOrEqualTo(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    return p1
.end method

.method public isPriceOverridden()Z
    .locals 1

    .line 680
    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    invoke-virtual {v0}, Lcom/squareup/checkout/OrderVariation;->isFixedPriced()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->overridePrice:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSameLibraryItem(Lcom/squareup/checkout/CartItem;)Z
    .locals 2

    .line 650
    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->itemId:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/checkout/CartItem;->lockConfiguration:Z

    iget-boolean v1, p1, Lcom/squareup/checkout/CartItem;->lockConfiguration:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->itemId:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/checkout/CartItem;->itemId:Ljava/lang/String;

    .line 652
    invoke-static {v0, v1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->variablePrice:Lcom/squareup/protos/common/Money;

    iget-object v1, p1, Lcom/squareup/checkout/CartItem;->variablePrice:Lcom/squareup/protos/common/Money;

    .line 653
    invoke-static {v0, v1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->overridePrice:Lcom/squareup/protos/common/Money;

    iget-object v1, p1, Lcom/squareup/checkout/CartItem;->overridePrice:Lcom/squareup/protos/common/Money;

    .line 654
    invoke-static {v0, v1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    iget-object v1, p1, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    .line 655
    invoke-static {v0, v1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->selectedModifiers:Ljava/util/SortedMap;

    iget-object v1, p1, Lcom/squareup/checkout/CartItem;->selectedModifiers:Ljava/util/SortedMap;

    .line 656
    invoke-static {v0, v1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->selectedDiningOption:Lcom/squareup/checkout/DiningOption;

    iget-object v1, p1, Lcom/squareup/checkout/CartItem;->selectedDiningOption:Lcom/squareup/checkout/DiningOption;

    .line 657
    invoke-static {v0, v1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->appliedTaxes:Ljava/util/Map;

    iget-object v1, p1, Lcom/squareup/checkout/CartItem;->appliedTaxes:Ljava/util/Map;

    .line 658
    invoke-static {v0, v1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->appliedDiscounts:Ljava/util/Map;

    .line 659
    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/checkout/CartItem;->appliedDiscounts:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->notes:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/checkout/CartItem;->notes:Ljava/lang/String;

    .line 660
    invoke-static {v0, p1}, Lcom/squareup/util/Strings;->hasTextDifference(Ljava/lang/String;Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public isService()Z
    .locals 1

    .line 1109
    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->featureDetails:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->appointments_service_details:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isTaxed()Z
    .locals 3

    .line 454
    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->isTaxedInTransactionsHistory:Ljava/lang/Boolean;

    if-nez v0, :cond_2

    .line 455
    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->appliedTaxes:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/Tax;

    .line 456
    iget-object v1, v1, Lcom/squareup/checkout/Tax;->type:Lcom/squareup/api/items/Fee$AdjustmentType;

    sget-object v2, Lcom/squareup/api/items/Fee$AdjustmentType;->TAX:Lcom/squareup/api/items/Fee$AdjustmentType;

    invoke-virtual {v1, v2}, Lcom/squareup/api/items/Fee$AdjustmentType;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0

    .line 462
    :cond_2
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public isUncategorized()Z
    .locals 1

    .line 548
    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem;->categoryId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public isVoided()Z
    .locals 1

    .line 466
    iget-boolean v0, p0, Lcom/squareup/checkout/CartItem;->isVoided:Z

    return v0
.end method

.method public preSelectedModifierCountForSelectedModifiers()I
    .locals 4

    .line 1196
    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->selectedModifiers:Ljava/util/SortedMap;

    invoke-interface {v0}, Ljava/util/SortedMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/SortedMap;

    .line 1197
    invoke-interface {v2}, Ljava/util/SortedMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/checkout/OrderModifier;

    .line 1198
    invoke-virtual {v3}, Lcom/squareup/checkout/OrderModifier;->getOnByDefault()Z

    move-result v3

    if-eqz v3, :cond_1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return v1
.end method

.method public price()J
    .locals 2

    .line 775
    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem;->hasUnitPrice()Z

    move-result v0

    const-string v1, "Price not available"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 776
    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem;->unitPriceOrNull()Lcom/squareup/protos/common/Money;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public quantity()Ljava/math/BigDecimal;
    .locals 1

    .line 781
    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->quantity:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public setSelectedDiningOption(Lcom/squareup/checkout/DiningOption;)Lcom/squareup/checkout/CartItem;
    .locals 1

    .line 627
    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem;->buildUpon()Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/checkout/CartItem$Builder;->selectedDiningOption(Lcom/squareup/checkout/DiningOption;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic setSelectedDiningOption(Lcom/squareup/itemsorter/SortableDiningOption;)Lcom/squareup/itemsorter/SortableItem;
    .locals 0

    .line 106
    check-cast p1, Lcom/squareup/checkout/DiningOption;

    invoke-virtual {p0, p1}, Lcom/squareup/checkout/CartItem;->setSelectedDiningOption(Lcom/squareup/checkout/DiningOption;)Lcom/squareup/checkout/CartItem;

    move-result-object p1

    return-object p1
.end method

.method public shouldShowDiningOption(Ljava/lang/String;)Z
    .locals 1

    .line 644
    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->selectedDiningOption:Lcom/squareup/checkout/DiningOption;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/checkout/DiningOption;->getName()Ljava/lang/String;

    move-result-object v0

    .line 645
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public shouldShowVariationName()Z
    .locals 2

    .line 612
    iget-boolean v0, p0, Lcom/squareup/checkout/CartItem;->showVariationNameOverride:Z

    const/4 v1, 0x1

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->variations:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    .line 613
    invoke-virtual {v0}, Lcom/squareup/checkout/OrderVariation;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public shouldSkipModifierDetailScreen()Z
    .locals 1

    .line 579
    iget-boolean v0, p0, Lcom/squareup/checkout/CartItem;->skipModifierDetailScreen:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 1149
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OrderItem ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkout/CartItem;->idPair:Lcom/squareup/protos/client/IdPair;

    iget-object v1, v1, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v3, 0x4

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "){"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkout/CartItem;->quantity:Ljava/math/BigDecimal;

    .line 1150
    invoke-virtual {v1}, Ljava/math/BigDecimal;->toPlainString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " x \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkout/CartItem;->itemName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\" @ "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem;->unitPriceOrNull()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public total()Lcom/squareup/protos/common/Money;
    .locals 3

    .line 811
    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem;->baseAmount()J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem;->unitPriceOrNull()Lcom/squareup/protos/common/Money;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, v1, v2}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public unitPriceOrNull()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 703
    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem;->isPriceOverridden()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 704
    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->overridePrice:Lcom/squareup/protos/common/Money;

    goto :goto_0

    .line 705
    :cond_0
    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    invoke-virtual {v0}, Lcom/squareup/checkout/OrderVariation;->isVariablePriced()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 706
    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->variablePrice:Lcom/squareup/protos/common/Money;

    goto :goto_0

    .line 708
    :cond_1
    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    invoke-virtual {v0}, Lcom/squareup/checkout/OrderVariation;->getPrice()Lcom/squareup/protos/common/Money;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public unitPriceWithModifiers()Lcom/squareup/protos/common/Money;
    .locals 7

    .line 727
    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem;->unitPriceOrNull()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 729
    iget-object v1, p0, Lcom/squareup/checkout/CartItem;->selectedModifiers:Ljava/util/SortedMap;

    invoke-interface {v1}, Ljava/util/SortedMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const-wide/16 v2, 0x0

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/SortedMap;

    .line 730
    invoke-interface {v4}, Ljava/util/SortedMap;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/checkout/OrderModifier;

    .line 731
    invoke-virtual {v5}, Lcom/squareup/checkout/OrderModifier;->isFreeModifier()Z

    move-result v6

    if-nez v6, :cond_1

    .line 732
    invoke-virtual {v5}, Lcom/squareup/checkout/OrderModifier;->getBasePriceTimesModifierQuantityOrNull()Lcom/squareup/protos/common/Money;

    move-result-object v5

    iget-object v5, v5, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    add-long/2addr v2, v5

    goto :goto_0

    .line 736
    :cond_2
    iget-object v1, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    add-long/2addr v4, v2

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v4, v5, v0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public unitPriceWithModifiersOrNull()Lcom/squareup/protos/common/Money;
    .locals 8

    .line 745
    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem;->unitPriceOrNull()Lcom/squareup/protos/common/Money;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    const-wide/16 v2, 0x0

    .line 750
    iget-object v4, p0, Lcom/squareup/checkout/CartItem;->selectedModifiers:Ljava/util/SortedMap;

    invoke-interface {v4}, Ljava/util/SortedMap;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/SortedMap;

    .line 751
    invoke-interface {v5}, Ljava/util/SortedMap;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/checkout/OrderModifier;

    .line 752
    invoke-virtual {v6}, Lcom/squareup/checkout/OrderModifier;->getBasePriceTimesModifierQuantityOrNull()Lcom/squareup/protos/common/Money;

    move-result-object v6

    if-nez v6, :cond_2

    return-object v1

    .line 756
    :cond_2
    iget-object v6, v6, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    add-long/2addr v2, v6

    goto :goto_0

    .line 759
    :cond_3
    iget-object v1, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    add-long/2addr v4, v2

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v4, v5, v0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public usingExactlyRuleBasedTaxes()Z
    .locals 2

    .line 591
    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->appliedTaxes:Ljava/util/Map;

    iget-object v1, p0, Lcom/squareup/checkout/CartItem;->ruleBasedTaxes:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public withRuleBasedTaxes()Lcom/squareup/checkout/CartItem;
    .locals 2

    .line 595
    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem;->buildUpon()Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/checkout/CartItem;->ruleBasedTaxes:Ljava/util/Map;

    .line 596
    invoke-virtual {v0, v1}, Lcom/squareup/checkout/CartItem$Builder;->appliedTaxes(Ljava/util/Map;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v0

    .line 597
    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem$Builder;->clearUserEditedTaxIds()Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v0

    .line 598
    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object v0

    return-object v0
.end method
