.class public final Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealCheckoutAppletWorkflow.kt"

# interfaces
.implements Lcom/squareup/checkout/v2/CheckoutAppletWorkflow;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lkotlin/Unit;",
        "Lcom/squareup/checkout/v2/ui/CheckoutAppletWorkflowState;",
        "Lcom/squareup/checkout/v2/CheckoutAppletWorkflowOutput;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/checkout/v2/CheckoutAppletWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealCheckoutAppletWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealCheckoutAppletWorkflow.kt\ncom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow\n+ 2 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 3 Worker.kt\ncom/squareup/workflow/WorkerKt\n+ 4 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,118:1\n41#2:119\n56#2,2:120\n41#2:123\n56#2,2:124\n276#3:122\n276#3:126\n149#4,5:127\n*E\n*S KotlinDebug\n*F\n+ 1 RealCheckoutAppletWorkflow.kt\ncom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow\n*L\n51#1:119\n51#1,2:120\n52#1:123\n52#1,2:124\n51#1:122\n52#1:126\n65#1,5:127\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0018\u00002\u00020\u00012<\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012&\u0012$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n0\u0002B\u0017\u0008\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0002\u0010\u000fJ\u001c\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u00132\u0006\u0010\u0014\u001a\u00020\u0015H\u0002J\u001c\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u00132\u0006\u0010\r\u001a\u00020\u0017H\u0002J\u001f\u0010\u0018\u001a\u00020\u00042\u0006\u0010\u0019\u001a\u00020\u00032\u0008\u0010\u001a\u001a\u0004\u0018\u00010\u001bH\u0016\u00a2\u0006\u0002\u0010\u001cJS\u0010\u001d\u001a$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n2\u0006\u0010\u0019\u001a\u00020\u00032\u0006\u0010\u001e\u001a\u00020\u00042\u0012\u0010\u001f\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050 H\u0016\u00a2\u0006\u0002\u0010!J2\u0010\"\u001a$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n2\u0006\u0010\u001e\u001a\u00020\u0004H\u0002J2\u0010#\u001a$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n2\u0006\u0010\u001e\u001a\u00020\u0004H\u0002J2\u0010$\u001a$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n2\u0006\u0010\u001e\u001a\u00020\u0004H\u0002J\u0010\u0010%\u001a\u00020\u001b2\u0006\u0010\u001e\u001a\u00020\u0004H\u0016R\u0011\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006&"
    }
    d2 = {
        "Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow;",
        "Lcom/squareup/checkout/v2/CheckoutAppletWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "",
        "Lcom/squareup/checkout/v2/ui/CheckoutAppletWorkflowState;",
        "Lcom/squareup/checkout/v2/CheckoutAppletWorkflowOutput;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "transactionData",
        "Lcom/squareup/checkout/v2/data/transaction/TransactionData;",
        "device",
        "Lcom/squareup/util/Device;",
        "(Lcom/squareup/checkout/v2/data/transaction/TransactionData;Lcom/squareup/util/Device;)V",
        "getDevice",
        "()Lcom/squareup/util/Device;",
        "handleCartData",
        "Lcom/squareup/workflow/WorkflowAction;",
        "cartData",
        "Lcom/squareup/checkout/v2/data/transaction/model/CartData;",
        "handleDeviceInfo",
        "Lcom/squareup/util/DeviceScreenSizeInfo;",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/checkout/v2/ui/CheckoutAppletWorkflowState;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "(Lkotlin/Unit;Lcom/squareup/checkout/v2/ui/CheckoutAppletWorkflowState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;",
        "renderPhone",
        "renderPortrait",
        "renderTablet",
        "snapshotState",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final device:Lcom/squareup/util/Device;

.field private final transactionData:Lcom/squareup/checkout/v2/data/transaction/TransactionData;


# direct methods
.method public constructor <init>(Lcom/squareup/checkout/v2/data/transaction/TransactionData;Lcom/squareup/util/Device;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "transactionData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "device"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow;->transactionData:Lcom/squareup/checkout/v2/data/transaction/TransactionData;

    iput-object p2, p0, Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow;->device:Lcom/squareup/util/Device;

    return-void
.end method

.method public static final synthetic access$handleCartData(Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow;Lcom/squareup/checkout/v2/data/transaction/model/CartData;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 27
    invoke-direct {p0, p1}, Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow;->handleCartData(Lcom/squareup/checkout/v2/data/transaction/model/CartData;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$handleDeviceInfo(Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow;Lcom/squareup/util/DeviceScreenSizeInfo;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 27
    invoke-direct {p0, p1}, Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow;->handleDeviceInfo(Lcom/squareup/util/DeviceScreenSizeInfo;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method private final handleCartData(Lcom/squareup/checkout/v2/data/transaction/model/CartData;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkout/v2/data/transaction/model/CartData;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/checkout/v2/ui/CheckoutAppletWorkflowState;",
            "Lcom/squareup/checkout/v2/CheckoutAppletWorkflowOutput;",
            ">;"
        }
    .end annotation

    .line 95
    new-instance v0, Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow$handleCartData$1;

    invoke-direct {v0, p1}, Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow$handleCartData$1;-><init>(Lcom/squareup/checkout/v2/data/transaction/model/CartData;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x0

    const/4 v1, 0x1

    invoke-static {p0, p1, v0, v1, p1}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method private final handleDeviceInfo(Lcom/squareup/util/DeviceScreenSizeInfo;)Lcom/squareup/workflow/WorkflowAction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/DeviceScreenSizeInfo;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/checkout/v2/ui/CheckoutAppletWorkflowState;",
            "Lcom/squareup/checkout/v2/CheckoutAppletWorkflowOutput;",
            ">;"
        }
    .end annotation

    .line 80
    invoke-virtual {p1}, Lcom/squareup/util/DeviceScreenSizeInfo;->isPhone()Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow$handleDeviceInfo$1;->INSTANCE:Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow$handleDeviceInfo$1;

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, v2, p1, v1, v2}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 83
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/util/DeviceScreenSizeInfo;->isPortrait()Z

    move-result p1

    if-eqz p1, :cond_1

    sget-object p1, Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow$handleDeviceInfo$2;->INSTANCE:Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow$handleDeviceInfo$2;

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, v2, p1, v1, v2}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 86
    :cond_1
    sget-object p1, Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow$handleDeviceInfo$3;->INSTANCE:Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow$handleDeviceInfo$3;

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, v2, p1, v1, v2}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private final renderPhone(Lcom/squareup/checkout/v2/ui/CheckoutAppletWorkflowState;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkout/v2/ui/CheckoutAppletWorkflowState;",
            ")",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 65
    sget-object p1, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    sget-object v0, Lcom/squareup/checkout/v2/ui/CheckoutAppletWorkflowRendering;->INSTANCE:Lcom/squareup/checkout/v2/ui/CheckoutAppletWorkflowRendering;

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 128
    new-instance v1, Lcom/squareup/workflow/legacy/Screen;

    .line 129
    const-class v2, Lcom/squareup/checkout/v2/ui/CheckoutAppletWorkflowRendering;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-string v3, ""

    invoke-static {v2, v3}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 130
    sget-object v3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v3

    .line 128
    invoke-direct {v1, v2, v0, v3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 65
    invoke-static {p1, v1}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object p1

    invoke-static {p1}, Lkotlin/collections/MapsKt;->mapOf(Lkotlin/Pair;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method private final renderPortrait(Lcom/squareup/checkout/v2/ui/CheckoutAppletWorkflowState;)Ljava/util/Map;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkout/v2/ui/CheckoutAppletWorkflowState;",
            ")",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 75
    invoke-direct {p0, p1}, Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow;->renderPhone(Lcom/squareup/checkout/v2/ui/CheckoutAppletWorkflowState;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method private final renderTablet(Lcom/squareup/checkout/v2/ui/CheckoutAppletWorkflowState;)Ljava/util/Map;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkout/v2/ui/CheckoutAppletWorkflowState;",
            ")",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 70
    invoke-direct {p0, p1}, Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow;->renderPhone(Lcom/squareup/checkout/v2/ui/CheckoutAppletWorkflowState;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public final getDevice()Lcom/squareup/util/Device;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow;->device:Lcom/squareup/util/Device;

    return-object v0
.end method

.method public initialState(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/checkout/v2/ui/CheckoutAppletWorkflowState;
    .locals 0

    const-string p2, "props"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    new-instance p1, Lcom/squareup/checkout/v2/ui/CheckoutAppletWorkflowState;

    .line 41
    sget-object p2, Lcom/squareup/checkout/v2/ui/DeviceFormat$LoadingDevice;->INSTANCE:Lcom/squareup/checkout/v2/ui/DeviceFormat$LoadingDevice;

    check-cast p2, Lcom/squareup/checkout/v2/ui/DeviceFormat;

    .line 40
    invoke-direct {p1, p2}, Lcom/squareup/checkout/v2/ui/CheckoutAppletWorkflowState;-><init>(Lcom/squareup/checkout/v2/ui/DeviceFormat;)V

    return-object p1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 27
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow;->initialState(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/checkout/v2/ui/CheckoutAppletWorkflowState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 27
    check-cast p1, Lkotlin/Unit;

    check-cast p2, Lcom/squareup/checkout/v2/ui/CheckoutAppletWorkflowState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow;->render(Lkotlin/Unit;Lcom/squareup/checkout/v2/ui/CheckoutAppletWorkflowState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lkotlin/Unit;Lcom/squareup/checkout/v2/ui/CheckoutAppletWorkflowState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Unit;",
            "Lcom/squareup/checkout/v2/ui/CheckoutAppletWorkflowState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/checkout/v2/ui/CheckoutAppletWorkflowState;",
            "-",
            "Lcom/squareup/checkout/v2/CheckoutAppletWorkflowOutput;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "state"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "context"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    iget-object p1, p0, Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow;->transactionData:Lcom/squareup/checkout/v2/data/transaction/TransactionData;

    invoke-interface {p1}, Lcom/squareup/checkout/v2/data/transaction/TransactionData;->getCartData()Lio/reactivex/Observable;

    move-result-object p1

    .line 119
    sget-object v0, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object p1

    const-string v0, "this.toFlowable(BUFFER)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lorg/reactivestreams/Publisher;

    const-string v1, "null cannot be cast to non-null type org.reactivestreams.Publisher<T>"

    if-eqz p1, :cond_5

    .line 121
    invoke-static {p1}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 122
    const-class v2, Lcom/squareup/checkout/v2/data/transaction/model/CartData;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v2

    new-instance v3, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v3, v2, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v5, v3

    check-cast v5, Lcom/squareup/workflow/Worker;

    const/4 v6, 0x0

    .line 51
    new-instance p1, Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow$render$1;

    invoke-direct {p1, p0}, Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow$render$1;-><init>(Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow;)V

    move-object v7, p1

    check-cast v7, Lkotlin/jvm/functions/Function1;

    const/4 v8, 0x2

    const/4 v9, 0x0

    move-object v4, p3

    invoke-static/range {v4 .. v9}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 52
    iget-object p1, p0, Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow;->device:Lcom/squareup/util/Device;

    invoke-interface {p1}, Lcom/squareup/util/Device;->getScreenSize()Lio/reactivex/Observable;

    move-result-object p1

    .line 123
    sget-object v2, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {p1, v2}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lorg/reactivestreams/Publisher;

    if-eqz p1, :cond_4

    .line 125
    invoke-static {p1}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 126
    const-class v0, Lcom/squareup/util/DeviceScreenSizeInfo;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v0

    new-instance v1, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v1, v0, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v3, v1

    check-cast v3, Lcom/squareup/workflow/Worker;

    const/4 v4, 0x0

    .line 52
    new-instance p1, Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow$render$2;

    invoke-direct {p1, p0}, Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow$render$2;-><init>(Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow;)V

    move-object v5, p1

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x2

    const/4 v7, 0x0

    move-object v2, p3

    invoke-static/range {v2 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 54
    invoke-virtual {p2}, Lcom/squareup/checkout/v2/ui/CheckoutAppletWorkflowState;->getDeviceFormat()Lcom/squareup/checkout/v2/ui/DeviceFormat;

    move-result-object p1

    .line 55
    sget-object p3, Lcom/squareup/checkout/v2/ui/DeviceFormat$LoadingDevice;->INSTANCE:Lcom/squareup/checkout/v2/ui/DeviceFormat$LoadingDevice;

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_0

    invoke-static {}, Lkotlin/collections/MapsKt;->emptyMap()Ljava/util/Map;

    move-result-object p1

    goto :goto_0

    .line 56
    :cond_0
    sget-object p3, Lcom/squareup/checkout/v2/ui/DeviceFormat$Phone;->INSTANCE:Lcom/squareup/checkout/v2/ui/DeviceFormat$Phone;

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_1

    invoke-direct {p0, p2}, Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow;->renderPhone(Lcom/squareup/checkout/v2/ui/CheckoutAppletWorkflowState;)Ljava/util/Map;

    move-result-object p1

    goto :goto_0

    .line 57
    :cond_1
    sget-object p3, Lcom/squareup/checkout/v2/ui/DeviceFormat$Tablet;->INSTANCE:Lcom/squareup/checkout/v2/ui/DeviceFormat$Tablet;

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_2

    invoke-direct {p0, p2}, Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow;->renderTablet(Lcom/squareup/checkout/v2/ui/CheckoutAppletWorkflowState;)Ljava/util/Map;

    move-result-object p1

    goto :goto_0

    .line 58
    :cond_2
    sget-object p3, Lcom/squareup/checkout/v2/ui/DeviceFormat$Portrait;->INSTANCE:Lcom/squareup/checkout/v2/ui/DeviceFormat$Portrait;

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    invoke-direct {p0, p2}, Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow;->renderPortrait(Lcom/squareup/checkout/v2/ui/CheckoutAppletWorkflowState;)Ljava/util/Map;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 125
    :cond_4
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 121
    :cond_5
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/checkout/v2/ui/CheckoutAppletWorkflowState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    sget-object p1, Lcom/squareup/workflow/Snapshot;->EMPTY:Lcom/squareup/workflow/Snapshot;

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 27
    check-cast p1, Lcom/squareup/checkout/v2/ui/CheckoutAppletWorkflowState;

    invoke-virtual {p0, p1}, Lcom/squareup/checkout/v2/ui/RealCheckoutAppletWorkflow;->snapshotState(Lcom/squareup/checkout/v2/ui/CheckoutAppletWorkflowState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
