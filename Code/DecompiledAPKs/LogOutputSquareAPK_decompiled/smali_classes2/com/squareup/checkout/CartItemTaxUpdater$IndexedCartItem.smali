.class final Lcom/squareup/checkout/CartItemTaxUpdater$IndexedCartItem;
.super Ljava/lang/Object;
.source "CartItemTaxUpdater.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkout/CartItemTaxUpdater;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "IndexedCartItem"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0000\u0008\u0082\u0008\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000c\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\r\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0011\u001a\u00020\u0003H\u00d6\u0001J\t\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/checkout/CartItemTaxUpdater$IndexedCartItem;",
        "",
        "index",
        "",
        "item",
        "Lcom/squareup/checkout/CartItem;",
        "(ILcom/squareup/checkout/CartItem;)V",
        "getIndex",
        "()I",
        "getItem",
        "()Lcom/squareup/checkout/CartItem;",
        "component1",
        "component2",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "toString",
        "",
        "checkout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final index:I

.field private final item:Lcom/squareup/checkout/CartItem;


# direct methods
.method public constructor <init>(ILcom/squareup/checkout/CartItem;)V
    .locals 1

    const-string v0, "item"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/squareup/checkout/CartItemTaxUpdater$IndexedCartItem;->index:I

    iput-object p2, p0, Lcom/squareup/checkout/CartItemTaxUpdater$IndexedCartItem;->item:Lcom/squareup/checkout/CartItem;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/checkout/CartItemTaxUpdater$IndexedCartItem;ILcom/squareup/checkout/CartItem;ILjava/lang/Object;)Lcom/squareup/checkout/CartItemTaxUpdater$IndexedCartItem;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget p1, p0, Lcom/squareup/checkout/CartItemTaxUpdater$IndexedCartItem;->index:I

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/checkout/CartItemTaxUpdater$IndexedCartItem;->item:Lcom/squareup/checkout/CartItem;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/checkout/CartItemTaxUpdater$IndexedCartItem;->copy(ILcom/squareup/checkout/CartItem;)Lcom/squareup/checkout/CartItemTaxUpdater$IndexedCartItem;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/squareup/checkout/CartItemTaxUpdater$IndexedCartItem;->index:I

    return v0
.end method

.method public final component2()Lcom/squareup/checkout/CartItem;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkout/CartItemTaxUpdater$IndexedCartItem;->item:Lcom/squareup/checkout/CartItem;

    return-object v0
.end method

.method public final copy(ILcom/squareup/checkout/CartItem;)Lcom/squareup/checkout/CartItemTaxUpdater$IndexedCartItem;
    .locals 1

    const-string v0, "item"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/checkout/CartItemTaxUpdater$IndexedCartItem;

    invoke-direct {v0, p1, p2}, Lcom/squareup/checkout/CartItemTaxUpdater$IndexedCartItem;-><init>(ILcom/squareup/checkout/CartItem;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/checkout/CartItemTaxUpdater$IndexedCartItem;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/checkout/CartItemTaxUpdater$IndexedCartItem;

    iget v0, p0, Lcom/squareup/checkout/CartItemTaxUpdater$IndexedCartItem;->index:I

    iget v1, p1, Lcom/squareup/checkout/CartItemTaxUpdater$IndexedCartItem;->index:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/checkout/CartItemTaxUpdater$IndexedCartItem;->item:Lcom/squareup/checkout/CartItem;

    iget-object p1, p1, Lcom/squareup/checkout/CartItemTaxUpdater$IndexedCartItem;->item:Lcom/squareup/checkout/CartItem;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getIndex()I
    .locals 1

    .line 49
    iget v0, p0, Lcom/squareup/checkout/CartItemTaxUpdater$IndexedCartItem;->index:I

    return v0
.end method

.method public final getItem()Lcom/squareup/checkout/CartItem;
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/checkout/CartItemTaxUpdater$IndexedCartItem;->item:Lcom/squareup/checkout/CartItem;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lcom/squareup/checkout/CartItemTaxUpdater$IndexedCartItem;->index:I

    invoke-static {v0}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/checkout/CartItemTaxUpdater$IndexedCartItem;->item:Lcom/squareup/checkout/CartItem;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "IndexedCartItem(index="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/checkout/CartItemTaxUpdater$IndexedCartItem;->index:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", item="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkout/CartItemTaxUpdater$IndexedCartItem;->item:Lcom/squareup/checkout/CartItem;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
