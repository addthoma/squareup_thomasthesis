.class public final Lcom/squareup/bugreport/BatteryStatus;
.super Ljava/lang/Object;
.source "BatteryStatus.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/bugreport/BatteryStatus$Health;,
        Lcom/squareup/bugreport/BatteryStatus$Plugged;,
        Lcom/squareup/bugreport/BatteryStatus$Status;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008#\u0008\u0086\u0008\u0018\u00002\u00020\u0001:\u0003345B\u000f\u0008\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004BO\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\n\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\n\u0012\u0006\u0010\u0011\u001a\u00020\n\u0012\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u0013\u00a2\u0006\u0002\u0010\u0014J\t\u0010$\u001a\u00020\u0006H\u00c6\u0003J\t\u0010%\u001a\u00020\u0008H\u00c6\u0003J\t\u0010&\u001a\u00020\nH\u00c6\u0003J\t\u0010\'\u001a\u00020\nH\u00c6\u0003J\t\u0010(\u001a\u00020\rH\u00c6\u0003J\t\u0010)\u001a\u00020\u000fH\u00c6\u0003J\t\u0010*\u001a\u00020\nH\u00c6\u0003J\t\u0010+\u001a\u00020\nH\u00c6\u0003J\u000b\u0010,\u001a\u0004\u0018\u00010\u0013H\u00c6\u0003Je\u0010-\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0002\u0010\t\u001a\u00020\n2\u0008\u0008\u0002\u0010\u000b\u001a\u00020\n2\u0008\u0008\u0002\u0010\u000c\u001a\u00020\r2\u0008\u0008\u0002\u0010\u000e\u001a\u00020\u000f2\u0008\u0008\u0002\u0010\u0010\u001a\u00020\n2\u0008\u0008\u0002\u0010\u0011\u001a\u00020\n2\n\u0008\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u00c6\u0001J\u0013\u0010.\u001a\u00020\u00062\u0008\u0010/\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u00100\u001a\u00020\nH\u00d6\u0001J\u0006\u00101\u001a\u00020\u0013J\t\u00102\u001a\u00020\u0013H\u00d6\u0001R\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016R\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0018R\u0011\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u001aR\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u001cR\u0011\u0010\u000b\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001d\u0010\u0018R\u0011\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001e\u0010\u001fR\u0013\u0010\u0012\u001a\u0004\u0018\u00010\u0013\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008 \u0010!R\u0011\u0010\u0010\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\"\u0010\u0018R\u0011\u0010\u0011\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008#\u0010\u0018\u00a8\u00066"
    }
    d2 = {
        "Lcom/squareup/bugreport/BatteryStatus;",
        "",
        "intent",
        "Landroid/content/Intent;",
        "(Landroid/content/Intent;)V",
        "present",
        "",
        "health",
        "Lcom/squareup/bugreport/BatteryStatus$Health;",
        "level",
        "",
        "scale",
        "plugged",
        "Lcom/squareup/bugreport/BatteryStatus$Plugged;",
        "status",
        "Lcom/squareup/bugreport/BatteryStatus$Status;",
        "temperature",
        "voltage",
        "technology",
        "",
        "(ZLcom/squareup/bugreport/BatteryStatus$Health;IILcom/squareup/bugreport/BatteryStatus$Plugged;Lcom/squareup/bugreport/BatteryStatus$Status;IILjava/lang/String;)V",
        "getHealth",
        "()Lcom/squareup/bugreport/BatteryStatus$Health;",
        "getLevel",
        "()I",
        "getPlugged",
        "()Lcom/squareup/bugreport/BatteryStatus$Plugged;",
        "getPresent",
        "()Z",
        "getScale",
        "getStatus",
        "()Lcom/squareup/bugreport/BatteryStatus$Status;",
        "getTechnology",
        "()Ljava/lang/String;",
        "getTemperature",
        "getVoltage",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "component9",
        "copy",
        "equals",
        "other",
        "hashCode",
        "toReport",
        "toString",
        "Health",
        "Plugged",
        "Status",
        "bugreport_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final health:Lcom/squareup/bugreport/BatteryStatus$Health;

.field private final level:I

.field private final plugged:Lcom/squareup/bugreport/BatteryStatus$Plugged;

.field private final present:Z

.field private final scale:I

.field private final status:Lcom/squareup/bugreport/BatteryStatus$Status;

.field private final technology:Ljava/lang/String;

.field private final temperature:I

.field private final voltage:I


# direct methods
.method public constructor <init>(Landroid/content/Intent;)V
    .locals 12

    const-string v0, "intent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    sget-object v0, Lcom/squareup/bugreport/BatteryStatus$Health;->Companion:Lcom/squareup/bugreport/BatteryStatus$Health$Companion;

    const-string v1, "health"

    const/4 v2, 0x1

    .line 36
    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 35
    invoke-virtual {v0, v1}, Lcom/squareup/bugreport/BatteryStatus$Health$Companion;->fromBatteryManagerHealthExtra(I)Lcom/squareup/bugreport/BatteryStatus$Health;

    move-result-object v4

    .line 38
    sget-object v0, Lcom/squareup/bugreport/BatteryStatus$Plugged;->Companion:Lcom/squareup/bugreport/BatteryStatus$Plugged$Companion;

    const/4 v1, 0x0

    const-string v2, "plugged"

    .line 39
    invoke-virtual {p1, v2, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 38
    invoke-virtual {v0, v2}, Lcom/squareup/bugreport/BatteryStatus$Plugged$Companion;->fromBatteryManagerPluggedExtra(I)Lcom/squareup/bugreport/BatteryStatus$Plugged;

    move-result-object v7

    const-string v0, "level"

    .line 41
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    const-string v0, "scale"

    .line 42
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    .line 43
    sget-object v0, Lcom/squareup/bugreport/BatteryStatus$Status;->Companion:Lcom/squareup/bugreport/BatteryStatus$Status$Companion;

    const-string v2, "status"

    .line 44
    invoke-virtual {p1, v2, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 43
    invoke-virtual {v0, v2}, Lcom/squareup/bugreport/BatteryStatus$Status$Companion;->fromBatteryManagerStatusExtra(I)Lcom/squareup/bugreport/BatteryStatus$Status;

    move-result-object v8

    const-string v0, "temperature"

    .line 46
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v9

    const-string/jumbo v0, "voltage"

    .line 47
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v10

    const-string v0, "technology"

    .line 48
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const-string v0, "present"

    .line 49
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    move-object v2, p0

    .line 34
    invoke-direct/range {v2 .. v11}, Lcom/squareup/bugreport/BatteryStatus;-><init>(ZLcom/squareup/bugreport/BatteryStatus$Health;IILcom/squareup/bugreport/BatteryStatus$Plugged;Lcom/squareup/bugreport/BatteryStatus$Status;IILjava/lang/String;)V

    return-void
.end method

.method public constructor <init>(ZLcom/squareup/bugreport/BatteryStatus$Health;IILcom/squareup/bugreport/BatteryStatus$Plugged;Lcom/squareup/bugreport/BatteryStatus$Status;IILjava/lang/String;)V
    .locals 1

    const-string v0, "health"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "plugged"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "status"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/squareup/bugreport/BatteryStatus;->present:Z

    iput-object p2, p0, Lcom/squareup/bugreport/BatteryStatus;->health:Lcom/squareup/bugreport/BatteryStatus$Health;

    iput p3, p0, Lcom/squareup/bugreport/BatteryStatus;->level:I

    iput p4, p0, Lcom/squareup/bugreport/BatteryStatus;->scale:I

    iput-object p5, p0, Lcom/squareup/bugreport/BatteryStatus;->plugged:Lcom/squareup/bugreport/BatteryStatus$Plugged;

    iput-object p6, p0, Lcom/squareup/bugreport/BatteryStatus;->status:Lcom/squareup/bugreport/BatteryStatus$Status;

    iput p7, p0, Lcom/squareup/bugreport/BatteryStatus;->temperature:I

    iput p8, p0, Lcom/squareup/bugreport/BatteryStatus;->voltage:I

    iput-object p9, p0, Lcom/squareup/bugreport/BatteryStatus;->technology:Ljava/lang/String;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/bugreport/BatteryStatus;ZLcom/squareup/bugreport/BatteryStatus$Health;IILcom/squareup/bugreport/BatteryStatus$Plugged;Lcom/squareup/bugreport/BatteryStatus$Status;IILjava/lang/String;ILjava/lang/Object;)Lcom/squareup/bugreport/BatteryStatus;
    .locals 10

    move-object v0, p0

    move/from16 v1, p10

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-boolean v2, v0, Lcom/squareup/bugreport/BatteryStatus;->present:Z

    goto :goto_0

    :cond_0
    move v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/squareup/bugreport/BatteryStatus;->health:Lcom/squareup/bugreport/BatteryStatus$Health;

    goto :goto_1

    :cond_1
    move-object v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget v4, v0, Lcom/squareup/bugreport/BatteryStatus;->level:I

    goto :goto_2

    :cond_2
    move v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget v5, v0, Lcom/squareup/bugreport/BatteryStatus;->scale:I

    goto :goto_3

    :cond_3
    move v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-object v6, v0, Lcom/squareup/bugreport/BatteryStatus;->plugged:Lcom/squareup/bugreport/BatteryStatus$Plugged;

    goto :goto_4

    :cond_4
    move-object v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-object v7, v0, Lcom/squareup/bugreport/BatteryStatus;->status:Lcom/squareup/bugreport/BatteryStatus$Status;

    goto :goto_5

    :cond_5
    move-object/from16 v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget v8, v0, Lcom/squareup/bugreport/BatteryStatus;->temperature:I

    goto :goto_6

    :cond_6
    move/from16 v8, p7

    :goto_6
    and-int/lit16 v9, v1, 0x80

    if-eqz v9, :cond_7

    iget v9, v0, Lcom/squareup/bugreport/BatteryStatus;->voltage:I

    goto :goto_7

    :cond_7
    move/from16 v9, p8

    :goto_7
    and-int/lit16 v1, v1, 0x100

    if-eqz v1, :cond_8

    iget-object v1, v0, Lcom/squareup/bugreport/BatteryStatus;->technology:Ljava/lang/String;

    goto :goto_8

    :cond_8
    move-object/from16 v1, p9

    :goto_8
    move p1, v2

    move-object p2, v3

    move p3, v4

    move p4, v5

    move-object p5, v6

    move-object/from16 p6, v7

    move/from16 p7, v8

    move/from16 p8, v9

    move-object/from16 p9, v1

    invoke-virtual/range {p0 .. p9}, Lcom/squareup/bugreport/BatteryStatus;->copy(ZLcom/squareup/bugreport/BatteryStatus$Health;IILcom/squareup/bugreport/BatteryStatus$Plugged;Lcom/squareup/bugreport/BatteryStatus$Status;IILjava/lang/String;)Lcom/squareup/bugreport/BatteryStatus;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/bugreport/BatteryStatus;->present:Z

    return v0
.end method

.method public final component2()Lcom/squareup/bugreport/BatteryStatus$Health;
    .locals 1

    iget-object v0, p0, Lcom/squareup/bugreport/BatteryStatus;->health:Lcom/squareup/bugreport/BatteryStatus$Health;

    return-object v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/squareup/bugreport/BatteryStatus;->level:I

    return v0
.end method

.method public final component4()I
    .locals 1

    iget v0, p0, Lcom/squareup/bugreport/BatteryStatus;->scale:I

    return v0
.end method

.method public final component5()Lcom/squareup/bugreport/BatteryStatus$Plugged;
    .locals 1

    iget-object v0, p0, Lcom/squareup/bugreport/BatteryStatus;->plugged:Lcom/squareup/bugreport/BatteryStatus$Plugged;

    return-object v0
.end method

.method public final component6()Lcom/squareup/bugreport/BatteryStatus$Status;
    .locals 1

    iget-object v0, p0, Lcom/squareup/bugreport/BatteryStatus;->status:Lcom/squareup/bugreport/BatteryStatus$Status;

    return-object v0
.end method

.method public final component7()I
    .locals 1

    iget v0, p0, Lcom/squareup/bugreport/BatteryStatus;->temperature:I

    return v0
.end method

.method public final component8()I
    .locals 1

    iget v0, p0, Lcom/squareup/bugreport/BatteryStatus;->voltage:I

    return v0
.end method

.method public final component9()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/bugreport/BatteryStatus;->technology:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(ZLcom/squareup/bugreport/BatteryStatus$Health;IILcom/squareup/bugreport/BatteryStatus$Plugged;Lcom/squareup/bugreport/BatteryStatus$Status;IILjava/lang/String;)Lcom/squareup/bugreport/BatteryStatus;
    .locals 11

    const-string v0, "health"

    move-object v3, p2

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "plugged"

    move-object/from16 v6, p5

    invoke-static {v6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "status"

    move-object/from16 v7, p6

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/bugreport/BatteryStatus;

    move-object v1, v0

    move v2, p1

    move v4, p3

    move v5, p4

    move/from16 v8, p7

    move/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v1 .. v10}, Lcom/squareup/bugreport/BatteryStatus;-><init>(ZLcom/squareup/bugreport/BatteryStatus$Health;IILcom/squareup/bugreport/BatteryStatus$Plugged;Lcom/squareup/bugreport/BatteryStatus$Status;IILjava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/bugreport/BatteryStatus;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/bugreport/BatteryStatus;

    iget-boolean v0, p0, Lcom/squareup/bugreport/BatteryStatus;->present:Z

    iget-boolean v1, p1, Lcom/squareup/bugreport/BatteryStatus;->present:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/bugreport/BatteryStatus;->health:Lcom/squareup/bugreport/BatteryStatus$Health;

    iget-object v1, p1, Lcom/squareup/bugreport/BatteryStatus;->health:Lcom/squareup/bugreport/BatteryStatus$Health;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/bugreport/BatteryStatus;->level:I

    iget v1, p1, Lcom/squareup/bugreport/BatteryStatus;->level:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/bugreport/BatteryStatus;->scale:I

    iget v1, p1, Lcom/squareup/bugreport/BatteryStatus;->scale:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/bugreport/BatteryStatus;->plugged:Lcom/squareup/bugreport/BatteryStatus$Plugged;

    iget-object v1, p1, Lcom/squareup/bugreport/BatteryStatus;->plugged:Lcom/squareup/bugreport/BatteryStatus$Plugged;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/bugreport/BatteryStatus;->status:Lcom/squareup/bugreport/BatteryStatus$Status;

    iget-object v1, p1, Lcom/squareup/bugreport/BatteryStatus;->status:Lcom/squareup/bugreport/BatteryStatus$Status;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/bugreport/BatteryStatus;->temperature:I

    iget v1, p1, Lcom/squareup/bugreport/BatteryStatus;->temperature:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/bugreport/BatteryStatus;->voltage:I

    iget v1, p1, Lcom/squareup/bugreport/BatteryStatus;->voltage:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/bugreport/BatteryStatus;->technology:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/bugreport/BatteryStatus;->technology:Ljava/lang/String;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getHealth()Lcom/squareup/bugreport/BatteryStatus$Health;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/bugreport/BatteryStatus;->health:Lcom/squareup/bugreport/BatteryStatus$Health;

    return-object v0
.end method

.method public final getLevel()I
    .locals 1

    .line 25
    iget v0, p0, Lcom/squareup/bugreport/BatteryStatus;->level:I

    return v0
.end method

.method public final getPlugged()Lcom/squareup/bugreport/BatteryStatus$Plugged;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/bugreport/BatteryStatus;->plugged:Lcom/squareup/bugreport/BatteryStatus$Plugged;

    return-object v0
.end method

.method public final getPresent()Z
    .locals 1

    .line 23
    iget-boolean v0, p0, Lcom/squareup/bugreport/BatteryStatus;->present:Z

    return v0
.end method

.method public final getScale()I
    .locals 1

    .line 26
    iget v0, p0, Lcom/squareup/bugreport/BatteryStatus;->scale:I

    return v0
.end method

.method public final getStatus()Lcom/squareup/bugreport/BatteryStatus$Status;
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/bugreport/BatteryStatus;->status:Lcom/squareup/bugreport/BatteryStatus$Status;

    return-object v0
.end method

.method public final getTechnology()Ljava/lang/String;
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/squareup/bugreport/BatteryStatus;->technology:Ljava/lang/String;

    return-object v0
.end method

.method public final getTemperature()I
    .locals 1

    .line 29
    iget v0, p0, Lcom/squareup/bugreport/BatteryStatus;->temperature:I

    return v0
.end method

.method public final getVoltage()I
    .locals 1

    .line 30
    iget v0, p0, Lcom/squareup/bugreport/BatteryStatus;->voltage:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-boolean v0, p0, Lcom/squareup/bugreport/BatteryStatus;->present:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/bugreport/BatteryStatus;->health:Lcom/squareup/bugreport/BatteryStatus$Health;

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/bugreport/BatteryStatus;->level:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/bugreport/BatteryStatus;->scale:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/bugreport/BatteryStatus;->plugged:Lcom/squareup/bugreport/BatteryStatus$Plugged;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/bugreport/BatteryStatus;->status:Lcom/squareup/bugreport/BatteryStatus$Status;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_3
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/bugreport/BatteryStatus;->temperature:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/bugreport/BatteryStatus;->voltage:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/bugreport/BatteryStatus;->technology:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    return v0
.end method

.method public final toReport()Ljava/lang/String;
    .locals 2

    .line 111
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\nHealth:      "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    iget-object v1, p0, Lcom/squareup/bugreport/BatteryStatus;->health:Lcom/squareup/bugreport/BatteryStatus$Health;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "\nLevel:       "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 113
    iget v1, p0, Lcom/squareup/bugreport/BatteryStatus;->level:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " / "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/bugreport/BatteryStatus;->scale:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "\nPlugged:     "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    iget-object v1, p0, Lcom/squareup/bugreport/BatteryStatus;->plugged:Lcom/squareup/bugreport/BatteryStatus$Plugged;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "\nStatus:      "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 115
    iget-object v1, p0, Lcom/squareup/bugreport/BatteryStatus;->status:Lcom/squareup/bugreport/BatteryStatus$Status;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "\nTemperature: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 116
    iget v1, p0, Lcom/squareup/bugreport/BatteryStatus;->temperature:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "\nVoltage:     "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 117
    iget v1, p0, Lcom/squareup/bugreport/BatteryStatus;->voltage:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "\nTechnology:  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 118
    iget-object v1, p0, Lcom/squareup/bugreport/BatteryStatus;->technology:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\nPresent:     "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    iget-boolean v1, p0, Lcom/squareup/bugreport/BatteryStatus;->present:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BatteryStatus(present="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/bugreport/BatteryStatus;->present:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", health="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/bugreport/BatteryStatus;->health:Lcom/squareup/bugreport/BatteryStatus$Health;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", level="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/bugreport/BatteryStatus;->level:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", scale="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/bugreport/BatteryStatus;->scale:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", plugged="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/bugreport/BatteryStatus;->plugged:Lcom/squareup/bugreport/BatteryStatus$Plugged;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/bugreport/BatteryStatus;->status:Lcom/squareup/bugreport/BatteryStatus$Status;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", temperature="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/bugreport/BatteryStatus;->temperature:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", voltage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/bugreport/BatteryStatus;->voltage:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", technology="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/bugreport/BatteryStatus;->technology:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
