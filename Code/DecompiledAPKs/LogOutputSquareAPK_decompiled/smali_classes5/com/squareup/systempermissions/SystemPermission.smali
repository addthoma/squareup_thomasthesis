.class public final enum Lcom/squareup/systempermissions/SystemPermission;
.super Ljava/lang/Enum;
.source "SystemPermission.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/systempermissions/SystemPermission$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/systempermissions/SystemPermission;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSystemPermission.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SystemPermission.kt\ncom/squareup/systempermissions/SystemPermission\n+ 2 _Arrays.kt\nkotlin/collections/ArraysKt___ArraysKt\n*L\n1#1,74:1\n7476#2,2:75\n7736#2,4:77\n*E\n*S KotlinDebug\n*F\n+ 1 SystemPermission.kt\ncom/squareup/systempermissions/SystemPermission\n*L\n53#1,2:75\n53#1,4:77\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0007\u0008\u0086\u0001\u0018\u0000 \u00152\u0008\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u0015B\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008J\u000e\u0010\t\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\u000bJ\u0016\u0010\u000c\u001a\u00020\r2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000e\u001a\u00020\u000fR\u0010\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000j\u0002\u0008\u0010j\u0002\u0008\u0011j\u0002\u0008\u0012j\u0002\u0008\u0013j\u0002\u0008\u0014\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/systempermissions/SystemPermission;",
        "",
        "permissionName",
        "",
        "(Ljava/lang/String;ILjava/lang/String;)V",
        "hasPermission",
        "",
        "context",
        "Landroid/content/Context;",
        "hasSelectivelyDenied",
        "activity",
        "Landroid/app/Activity;",
        "requestPermission",
        "",
        "requestCode",
        "",
        "CONTACTS",
        "LOCATION",
        "MICROPHONE",
        "PHONE",
        "STORAGE",
        "Companion",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/systempermissions/SystemPermission;

.field public static final enum CONTACTS:Lcom/squareup/systempermissions/SystemPermission;

.field public static final Companion:Lcom/squareup/systempermissions/SystemPermission$Companion;

.field public static final enum LOCATION:Lcom/squareup/systempermissions/SystemPermission;

.field public static final enum MICROPHONE:Lcom/squareup/systempermissions/SystemPermission;

.field public static final enum PHONE:Lcom/squareup/systempermissions/SystemPermission;

.field public static final enum STORAGE:Lcom/squareup/systempermissions/SystemPermission;

.field private static final permissions:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/systempermissions/SystemPermission;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final permissionName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/systempermissions/SystemPermission;

    new-instance v1, Lcom/squareup/systempermissions/SystemPermission;

    const/4 v2, 0x0

    const-string v3, "CONTACTS"

    const-string v4, "android.permission.READ_CONTACTS"

    .line 21
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/systempermissions/SystemPermission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/systempermissions/SystemPermission;->CONTACTS:Lcom/squareup/systempermissions/SystemPermission;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/systempermissions/SystemPermission;

    const/4 v3, 0x1

    const-string v4, "LOCATION"

    const-string v5, "android.permission.ACCESS_FINE_LOCATION"

    .line 22
    invoke-direct {v1, v4, v3, v5}, Lcom/squareup/systempermissions/SystemPermission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/systempermissions/SystemPermission;->LOCATION:Lcom/squareup/systempermissions/SystemPermission;

    aput-object v1, v0, v3

    new-instance v1, Lcom/squareup/systempermissions/SystemPermission;

    const/4 v3, 0x2

    const-string v4, "MICROPHONE"

    const-string v5, "android.permission.RECORD_AUDIO"

    .line 23
    invoke-direct {v1, v4, v3, v5}, Lcom/squareup/systempermissions/SystemPermission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/systempermissions/SystemPermission;->MICROPHONE:Lcom/squareup/systempermissions/SystemPermission;

    aput-object v1, v0, v3

    new-instance v1, Lcom/squareup/systempermissions/SystemPermission;

    const/4 v3, 0x3

    const-string v4, "PHONE"

    const-string v5, "android.permission.READ_PHONE_STATE"

    .line 24
    invoke-direct {v1, v4, v3, v5}, Lcom/squareup/systempermissions/SystemPermission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/systempermissions/SystemPermission;->PHONE:Lcom/squareup/systempermissions/SystemPermission;

    aput-object v1, v0, v3

    new-instance v1, Lcom/squareup/systempermissions/SystemPermission;

    const/4 v3, 0x4

    const-string v4, "STORAGE"

    const-string v5, "android.permission.WRITE_EXTERNAL_STORAGE"

    .line 25
    invoke-direct {v1, v4, v3, v5}, Lcom/squareup/systempermissions/SystemPermission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/systempermissions/SystemPermission;->STORAGE:Lcom/squareup/systempermissions/SystemPermission;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/systempermissions/SystemPermission;->$VALUES:[Lcom/squareup/systempermissions/SystemPermission;

    new-instance v0, Lcom/squareup/systempermissions/SystemPermission$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/systempermissions/SystemPermission$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/systempermissions/SystemPermission;->Companion:Lcom/squareup/systempermissions/SystemPermission$Companion;

    .line 53
    invoke-static {}, Lcom/squareup/systempermissions/SystemPermission;->values()[Lcom/squareup/systempermissions/SystemPermission;

    move-result-object v0

    .line 75
    array-length v1, v0

    invoke-static {v1}, Lkotlin/collections/MapsKt;->mapCapacity(I)I

    move-result v1

    const/16 v3, 0x10

    invoke-static {v1, v3}, Lkotlin/ranges/RangesKt;->coerceAtLeast(II)I

    move-result v1

    .line 76
    new-instance v3, Ljava/util/LinkedHashMap;

    invoke-direct {v3, v1}, Ljava/util/LinkedHashMap;-><init>(I)V

    check-cast v3, Ljava/util/Map;

    .line 77
    array-length v1, v0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v4, v0, v2

    .line 53
    iget-object v5, v4, Lcom/squareup/systempermissions/SystemPermission;->permissionName:Ljava/lang/String;

    invoke-interface {v3, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 80
    :cond_0
    sput-object v3, Lcom/squareup/systempermissions/SystemPermission;->permissions:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/squareup/systempermissions/SystemPermission;->permissionName:Ljava/lang/String;

    return-void
.end method

.method public static final synthetic access$getPermissions$cp()Ljava/util/Map;
    .locals 1

    .line 18
    sget-object v0, Lcom/squareup/systempermissions/SystemPermission;->permissions:Ljava/util/Map;

    return-object v0
.end method

.method public static final allGrantedNames(Landroid/content/Context;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/systempermissions/SystemPermission;->Companion:Lcom/squareup/systempermissions/SystemPermission$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/systempermissions/SystemPermission$Companion;->allGrantedNames(Landroid/content/Context;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static final fromPermissionName(Ljava/lang/String;)Lcom/squareup/systempermissions/SystemPermission;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/systempermissions/SystemPermission;->Companion:Lcom/squareup/systempermissions/SystemPermission$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/systempermissions/SystemPermission$Companion;->fromPermissionName(Ljava/lang/String;)Lcom/squareup/systempermissions/SystemPermission;

    move-result-object p0

    return-object p0
.end method

.method public static final isGranted(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/systempermissions/SystemPermission;->Companion:Lcom/squareup/systempermissions/SystemPermission$Companion;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/systempermissions/SystemPermission$Companion;->isGranted(Landroid/content/Context;Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/systempermissions/SystemPermission;
    .locals 1

    const-class v0, Lcom/squareup/systempermissions/SystemPermission;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/systempermissions/SystemPermission;

    return-object p0
.end method

.method public static values()[Lcom/squareup/systempermissions/SystemPermission;
    .locals 1

    sget-object v0, Lcom/squareup/systempermissions/SystemPermission;->$VALUES:[Lcom/squareup/systempermissions/SystemPermission;

    invoke-virtual {v0}, [Lcom/squareup/systempermissions/SystemPermission;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/systempermissions/SystemPermission;

    return-object v0
.end method


# virtual methods
.method public final hasPermission(Landroid/content/Context;)Z
    .locals 2

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    sget-object v0, Lcom/squareup/systempermissions/SystemPermission;->Companion:Lcom/squareup/systempermissions/SystemPermission$Companion;

    iget-object v1, p0, Lcom/squareup/systempermissions/SystemPermission;->permissionName:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/systempermissions/SystemPermission$Companion;->isGranted(Landroid/content/Context;Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method public final hasSelectivelyDenied(Landroid/app/Activity;)Z
    .locals 1

    const-string v0, "activity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    iget-object v0, p0, Lcom/squareup/systempermissions/SystemPermission;->permissionName:Ljava/lang/String;

    invoke-static {p1, v0}, Landroidx/core/app/ActivityCompat;->shouldShowRequestPermissionRationale(Landroid/app/Activity;Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method public final requestPermission(Landroid/app/Activity;I)V
    .locals 3

    const-string v0, "activity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    .line 49
    iget-object v1, p0, Lcom/squareup/systempermissions/SystemPermission;->permissionName:Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {p1, v0, p2}, Landroidx/core/app/ActivityCompat;->requestPermissions(Landroid/app/Activity;[Ljava/lang/String;I)V

    return-void
.end method
