.class public abstract Lcom/squareup/systempermissions/RealSystemPermissionsCheckerModule;
.super Ljava/lang/Object;
.source "RealSystemPermissionsCheckerModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract providesSystemPermissionsChecker(Lcom/squareup/systempermissions/RealSystemPermissionsChecker;)Lcom/squareup/systempermissions/SystemPermissionsChecker;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
