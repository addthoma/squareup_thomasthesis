.class final Lcom/squareup/scales/NoConnectedScalesLayoutRunner$showRendering$2;
.super Lkotlin/jvm/internal/Lambda;
.source "NoConnectedScalesLayoutRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/scales/NoConnectedScalesLayoutRunner;->showRendering(Lcom/squareup/scales/NoConnectedScalesScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/mosaic/core/UiModelContext<",
        "Lkotlin/Unit;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNoConnectedScalesLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NoConnectedScalesLayoutRunner.kt\ncom/squareup/scales/NoConnectedScalesLayoutRunner$showRendering$2\n+ 2 BlueprintUiModel.kt\ncom/squareup/blueprint/mosaic/BlueprintUiModelKt\n+ 3 BlueprintDsl.kt\ncom/squareup/blueprint/BlueprintDslKt\n+ 4 BlueprintDsl.kt\ncom/squareup/blueprint/BlueprintDslKt$center$1\n*L\n1#1,114:1\n17#2,11:115\n26#2:126\n22#2,3:127\n28#2:159\n113#3,9:130\n119#3:139\n120#3:141\n141#3:142\n113#3,9:143\n119#3:152\n120#3,3:154\n142#3:157\n122#3:158\n113#4:140\n113#4:153\n*E\n*S KotlinDebug\n*F\n+ 1 NoConnectedScalesLayoutRunner.kt\ncom/squareup/scales/NoConnectedScalesLayoutRunner$showRendering$2\n*L\n63#1,11:115\n63#1:126\n63#1,3:127\n63#1:159\n63#1,9:130\n63#1:139\n63#1:141\n63#1:142\n63#1,9:143\n63#1:152\n63#1,3:154\n63#1:157\n63#1:158\n63#1:140\n63#1:153\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u0008\u0012\u0004\u0012\u00020\u00010\u0002H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/mosaic/core/UiModelContext;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/scales/NoConnectedScalesLayoutRunner;


# direct methods
.method constructor <init>(Lcom/squareup/scales/NoConnectedScalesLayoutRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/scales/NoConnectedScalesLayoutRunner$showRendering$2;->this$0:Lcom/squareup/scales/NoConnectedScalesLayoutRunner;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 36
    check-cast p1, Lcom/squareup/mosaic/core/UiModelContext;

    invoke-virtual {p0, p1}, Lcom/squareup/scales/NoConnectedScalesLayoutRunner$showRendering$2;->invoke(Lcom/squareup/mosaic/core/UiModelContext;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/mosaic/core/UiModelContext;)V
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mosaic/core/UiModelContext<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const-string v2, "$receiver"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 119
    new-instance v2, Lcom/squareup/blueprint/mosaic/BlueprintUiModel;

    .line 127
    invoke-interface/range {p1 .. p1}, Lcom/squareup/mosaic/core/UiModelContext;->createParams()Ljava/lang/Object;

    move-result-object v4

    const/4 v5, 0x1

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/16 v8, 0x8

    const/4 v9, 0x0

    move-object v3, v2

    .line 119
    invoke-direct/range {v3 .. v9}, Lcom/squareup/blueprint/mosaic/BlueprintUiModel;-><init>(Ljava/lang/Object;ZZLcom/squareup/blueprint/Block;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 64
    move-object v3, v2

    check-cast v3, Lcom/squareup/blueprint/BlueprintContext;

    sget-object v6, Lcom/squareup/blueprint/CenterBlock$Type;->VERTICALLY:Lcom/squareup/blueprint/CenterBlock$Type;

    .line 139
    invoke-interface {v3}, Lcom/squareup/blueprint/BlueprintContext;->createParams()Ljava/lang/Object;

    move-result-object v5

    .line 135
    new-instance v10, Lcom/squareup/blueprint/CenterBlock;

    const/4 v8, 0x4

    move-object v4, v10

    invoke-direct/range {v4 .. v9}, Lcom/squareup/blueprint/CenterBlock;-><init>(Ljava/lang/Object;Lcom/squareup/blueprint/CenterBlock$Type;Lcom/squareup/blueprint/Block;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 65
    move-object v4, v10

    check-cast v4, Lcom/squareup/blueprint/BlueprintContext;

    .line 142
    new-instance v5, Lcom/squareup/blueprint/VerticalBlock;

    invoke-interface {v4}, Lcom/squareup/blueprint/BlueprintContext;->createParams()Ljava/lang/Object;

    move-result-object v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x6

    const/16 v16, 0x0

    move-object v11, v5

    invoke-direct/range {v11 .. v16}, Lcom/squareup/blueprint/VerticalBlock;-><init>(Ljava/lang/Object;Ljava/util/List;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 66
    move-object v6, v5

    check-cast v6, Lcom/squareup/blueprint/BlueprintContext;

    sget-object v13, Lcom/squareup/blueprint/CenterBlock$Type;->HORIZONTALLY:Lcom/squareup/blueprint/CenterBlock$Type;

    .line 152
    invoke-interface {v6}, Lcom/squareup/blueprint/BlueprintContext;->createParams()Ljava/lang/Object;

    move-result-object v12

    .line 148
    new-instance v7, Lcom/squareup/blueprint/CenterBlock;

    const/4 v14, 0x0

    const/4 v15, 0x4

    move-object v11, v7

    invoke-direct/range {v11 .. v16}, Lcom/squareup/blueprint/CenterBlock;-><init>(Ljava/lang/Object;Lcom/squareup/blueprint/CenterBlock$Type;Lcom/squareup/blueprint/Block;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 67
    iget-object v8, v0, Lcom/squareup/scales/NoConnectedScalesLayoutRunner$showRendering$2;->this$0:Lcom/squareup/scales/NoConnectedScalesLayoutRunner;

    move-object v9, v7

    check-cast v9, Lcom/squareup/blueprint/BlueprintContext;

    invoke-static {v8, v9}, Lcom/squareup/scales/NoConnectedScalesLayoutRunner;->access$scaleImageModel(Lcom/squareup/scales/NoConnectedScalesLayoutRunner;Lcom/squareup/blueprint/BlueprintContext;)V

    .line 68
    check-cast v7, Lcom/squareup/blueprint/Block;

    .line 147
    invoke-interface {v6, v7}, Lcom/squareup/blueprint/BlueprintContext;->add(Lcom/squareup/blueprint/Block;)V

    .line 69
    iget-object v7, v0, Lcom/squareup/scales/NoConnectedScalesLayoutRunner$showRendering$2;->this$0:Lcom/squareup/scales/NoConnectedScalesLayoutRunner;

    move-object v8, v5

    check-cast v8, Lcom/squareup/blueprint/LinearBlock;

    invoke-static {v7, v8}, Lcom/squareup/scales/NoConnectedScalesLayoutRunner;->access$smallSpacing(Lcom/squareup/scales/NoConnectedScalesLayoutRunner;Lcom/squareup/blueprint/LinearBlock;)V

    .line 70
    iget-object v7, v0, Lcom/squareup/scales/NoConnectedScalesLayoutRunner$showRendering$2;->this$0:Lcom/squareup/scales/NoConnectedScalesLayoutRunner;

    invoke-static {v7, v6}, Lcom/squareup/scales/NoConnectedScalesLayoutRunner;->access$connectScaleLabelModel(Lcom/squareup/scales/NoConnectedScalesLayoutRunner;Lcom/squareup/blueprint/BlueprintContext;)V

    .line 71
    check-cast v5, Lcom/squareup/blueprint/Block;

    .line 142
    invoke-interface {v4, v5}, Lcom/squareup/blueprint/BlueprintContext;->add(Lcom/squareup/blueprint/Block;)V

    .line 72
    check-cast v10, Lcom/squareup/blueprint/Block;

    .line 134
    invoke-interface {v3, v10}, Lcom/squareup/blueprint/BlueprintContext;->add(Lcom/squareup/blueprint/Block;)V

    .line 125
    check-cast v2, Lcom/squareup/mosaic/core/UiModel;

    invoke-interface {v1, v2}, Lcom/squareup/mosaic/core/UiModelContext;->add(Lcom/squareup/mosaic/core/UiModel;)V

    return-void
.end method
