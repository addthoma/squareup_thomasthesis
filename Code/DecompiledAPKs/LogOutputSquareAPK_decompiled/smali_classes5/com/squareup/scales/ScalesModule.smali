.class public final Lcom/squareup/scales/ScalesModule;
.super Ljava/lang/Object;
.source "ScalesModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J(\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0007J@\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0015\u001a\u00020\u0016H\u0007\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/scales/ScalesModule;",
        "",
        "()V",
        "provideStarScaleDiscoverer",
        "Lcom/squareup/scales/StarScaleDiscoverer;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "application",
        "Landroid/app/Application;",
        "mainThread",
        "Lcom/squareup/thread/executor/MainThread;",
        "realScaleTracker",
        "Lcom/squareup/scales/RealScaleTracker;",
        "provideUsbScales",
        "Lcom/squareup/scales/SerialUsbScaleDiscoverer;",
        "usbDiscoverer",
        "Lcom/squareup/usb/UsbDiscoverer;",
        "manager",
        "Lcom/squareup/hardware/usb/UsbManager;",
        "usbScaleInterpreter",
        "Lcom/squareup/scales/UsbScaleInterpreter;",
        "analytics",
        "Lcom/squareup/scales/analytics/ScalesHardwareAnalytics;",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final provideStarScaleDiscoverer(Lcom/squareup/settings/server/Features;Landroid/app/Application;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/scales/RealScaleTracker;)Lcom/squareup/scales/StarScaleDiscoverer;
    .locals 1
    .annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    const-string v0, "features"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "application"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainThread"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "realScaleTracker"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    sget-object v0, Lcom/squareup/settings/server/Features$Feature;->CAN_USE_SCALES:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p1, v0}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/scales/RealStarScaleDiscoverer;

    invoke-direct {v0, p2, p3, p4, p1}, Lcom/squareup/scales/RealStarScaleDiscoverer;-><init>(Landroid/app/Application;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/scales/RealScaleTracker;Lcom/squareup/settings/server/Features;)V

    check-cast v0, Lcom/squareup/scales/StarScaleDiscoverer;

    goto :goto_0

    .line 31
    :cond_0
    new-instance p1, Lcom/squareup/scales/NoOpStarScaleDiscoverer;

    invoke-direct {p1}, Lcom/squareup/scales/NoOpStarScaleDiscoverer;-><init>()V

    move-object v0, p1

    check-cast v0, Lcom/squareup/scales/StarScaleDiscoverer;

    :goto_0
    return-object v0
.end method

.method public final provideUsbScales(Lcom/squareup/usb/UsbDiscoverer;Lcom/squareup/hardware/usb/UsbManager;Lcom/squareup/scales/RealScaleTracker;Lcom/squareup/scales/UsbScaleInterpreter;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/settings/server/Features;Lcom/squareup/scales/analytics/ScalesHardwareAnalytics;)Lcom/squareup/scales/SerialUsbScaleDiscoverer;
    .locals 8
    .annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    const-string/jumbo v0, "usbDiscoverer"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "manager"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "realScaleTracker"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "usbScaleInterpreter"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainThread"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    sget-object v0, Lcom/squareup/settings/server/Features$Feature;->CAN_USE_SCALES:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p6, v0}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p6

    if-eqz p6, :cond_0

    new-instance p6, Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;

    const-string/jumbo v0, "usb_scales"

    .line 50
    invoke-static {v0}, Lcom/squareup/thread/Threads;->backgroundThreadFactory(Ljava/lang/String;)Ljava/util/concurrent/ThreadFactory;

    move-result-object v0

    .line 49
    invoke-static {v0}, Lcom/squareup/thread/executor/Executors;->newCachedThreadPool(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    const-string v1, "Executors.newCachedThrea\u2026\"usb_scales\")\n          )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v6, v0

    check-cast v6, Ljava/util/concurrent/Executor;

    move-object v0, p6

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p7

    move-object v7, p5

    .line 44
    invoke-direct/range {v0 .. v7}, Lcom/squareup/scales/RealSerialUsbScaleDiscoverer;-><init>(Lcom/squareup/usb/UsbDiscoverer;Lcom/squareup/hardware/usb/UsbManager;Lcom/squareup/scales/RealScaleTracker;Lcom/squareup/scales/UsbScaleInterpreter;Lcom/squareup/scales/analytics/ScalesHardwareAnalytics;Ljava/util/concurrent/Executor;Lcom/squareup/thread/executor/MainThread;)V

    check-cast p6, Lcom/squareup/scales/SerialUsbScaleDiscoverer;

    goto :goto_0

    .line 55
    :cond_0
    new-instance p1, Lcom/squareup/scales/NoOpSerialUsbScaleDiscoverer;

    invoke-direct {p1}, Lcom/squareup/scales/NoOpSerialUsbScaleDiscoverer;-><init>()V

    move-object p6, p1

    check-cast p6, Lcom/squareup/scales/SerialUsbScaleDiscoverer;

    :goto_0
    return-object p6
.end method
