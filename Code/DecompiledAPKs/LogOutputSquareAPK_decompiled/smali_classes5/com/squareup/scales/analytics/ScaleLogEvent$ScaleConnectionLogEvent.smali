.class public final Lcom/squareup/scales/analytics/ScaleLogEvent$ScaleConnectionLogEvent;
.super Lcom/squareup/scales/analytics/ScaleLogEvent;
.source "ScalesHardwareAnalytics.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/scales/analytics/ScaleLogEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ScaleConnectionLogEvent"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/scales/analytics/ScaleLogEvent$ScaleConnectionLogEvent;",
        "Lcom/squareup/scales/analytics/ScaleLogEvent;",
        "eventValue",
        "Lcom/squareup/scales/analytics/ScaleEventValue;",
        "scale",
        "Lcom/squareup/scales/ScaleTracker$HardwareScale;",
        "(Lcom/squareup/scales/analytics/ScaleEventValue;Lcom/squareup/scales/ScaleTracker$HardwareScale;)V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/scales/analytics/ScaleEventValue;Lcom/squareup/scales/ScaleTracker$HardwareScale;)V
    .locals 1

    const-string v0, "eventValue"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 89
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/scales/analytics/ScaleLogEvent;-><init>(Lcom/squareup/scales/analytics/ScaleEventValue;Lcom/squareup/scales/ScaleTracker$HardwareScale;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method
