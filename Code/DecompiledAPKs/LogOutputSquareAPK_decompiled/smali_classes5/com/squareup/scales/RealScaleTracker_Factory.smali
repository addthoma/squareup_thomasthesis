.class public final Lcom/squareup/scales/RealScaleTracker_Factory;
.super Ljava/lang/Object;
.source "RealScaleTracker_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/scales/RealScaleTracker_Factory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/scales/RealScaleTracker;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/scales/RealScaleTracker_Factory;
    .locals 1

    .line 17
    invoke-static {}, Lcom/squareup/scales/RealScaleTracker_Factory$InstanceHolder;->access$000()Lcom/squareup/scales/RealScaleTracker_Factory;

    move-result-object v0

    return-object v0
.end method

.method public static newInstance()Lcom/squareup/scales/RealScaleTracker;
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/scales/RealScaleTracker;

    invoke-direct {v0}, Lcom/squareup/scales/RealScaleTracker;-><init>()V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/scales/RealScaleTracker;
    .locals 1

    .line 13
    invoke-static {}, Lcom/squareup/scales/RealScaleTracker_Factory;->newInstance()Lcom/squareup/scales/RealScaleTracker;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 6
    invoke-virtual {p0}, Lcom/squareup/scales/RealScaleTracker_Factory;->get()Lcom/squareup/scales/RealScaleTracker;

    move-result-object v0

    return-object v0
.end method
