.class public final Lcom/squareup/scales/UsbCable;
.super Ljava/lang/Object;
.source "UsbCable.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000J\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B=\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\u0002\u0010\u0010J\u0006\u0010\u0015\u001a\u00020\u0016J\u0006\u0010\u0017\u001a\u00020\u0016R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0013\u001a\u0004\u0018\u00010\u0014X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/scales/UsbCable;",
        "",
        "usbDevice",
        "Landroid/hardware/usb/UsbDevice;",
        "realScaleTracker",
        "Lcom/squareup/scales/RealScaleTracker;",
        "backgroundThreadExecutor",
        "Ljava/util/concurrent/Executor;",
        "manager",
        "Lcom/squareup/hardware/usb/UsbManager;",
        "mainThread",
        "Lcom/squareup/thread/executor/MainThread;",
        "usbScaleInterpreter",
        "Lcom/squareup/scales/UsbScaleInterpreter;",
        "analytics",
        "Lcom/squareup/scales/analytics/ScalesHardwareAnalytics;",
        "(Landroid/hardware/usb/UsbDevice;Lcom/squareup/scales/RealScaleTracker;Ljava/util/concurrent/Executor;Lcom/squareup/hardware/usb/UsbManager;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/scales/UsbScaleInterpreter;Lcom/squareup/scales/analytics/ScalesHardwareAnalytics;)V",
        "active",
        "",
        "usbScale",
        "Lcom/squareup/scales/UsbScale;",
        "onDisconnected",
        "",
        "onPluggedIntoPhone",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private active:Z

.field private final analytics:Lcom/squareup/scales/analytics/ScalesHardwareAnalytics;

.field private final backgroundThreadExecutor:Ljava/util/concurrent/Executor;

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private final manager:Lcom/squareup/hardware/usb/UsbManager;

.field private final realScaleTracker:Lcom/squareup/scales/RealScaleTracker;

.field private final usbDevice:Landroid/hardware/usb/UsbDevice;

.field private usbScale:Lcom/squareup/scales/UsbScale;

.field private final usbScaleInterpreter:Lcom/squareup/scales/UsbScaleInterpreter;


# direct methods
.method public constructor <init>(Landroid/hardware/usb/UsbDevice;Lcom/squareup/scales/RealScaleTracker;Ljava/util/concurrent/Executor;Lcom/squareup/hardware/usb/UsbManager;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/scales/UsbScaleInterpreter;Lcom/squareup/scales/analytics/ScalesHardwareAnalytics;)V
    .locals 1

    const-string/jumbo v0, "usbDevice"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "realScaleTracker"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "backgroundThreadExecutor"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "manager"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainThread"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "usbScaleInterpreter"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/scales/UsbCable;->usbDevice:Landroid/hardware/usb/UsbDevice;

    iput-object p2, p0, Lcom/squareup/scales/UsbCable;->realScaleTracker:Lcom/squareup/scales/RealScaleTracker;

    iput-object p3, p0, Lcom/squareup/scales/UsbCable;->backgroundThreadExecutor:Ljava/util/concurrent/Executor;

    iput-object p4, p0, Lcom/squareup/scales/UsbCable;->manager:Lcom/squareup/hardware/usb/UsbManager;

    iput-object p5, p0, Lcom/squareup/scales/UsbCable;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iput-object p6, p0, Lcom/squareup/scales/UsbCable;->usbScaleInterpreter:Lcom/squareup/scales/UsbScaleInterpreter;

    iput-object p7, p0, Lcom/squareup/scales/UsbCable;->analytics:Lcom/squareup/scales/analytics/ScalesHardwareAnalytics;

    const/4 p1, 0x1

    .line 26
    iput-boolean p1, p0, Lcom/squareup/scales/UsbCable;->active:Z

    return-void
.end method

.method public static final synthetic access$getActive$p(Lcom/squareup/scales/UsbCable;)Z
    .locals 0

    .line 16
    iget-boolean p0, p0, Lcom/squareup/scales/UsbCable;->active:Z

    return p0
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/scales/UsbCable;)Lcom/squareup/scales/analytics/ScalesHardwareAnalytics;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/squareup/scales/UsbCable;->analytics:Lcom/squareup/scales/analytics/ScalesHardwareAnalytics;

    return-object p0
.end method

.method public static final synthetic access$getMainThread$p(Lcom/squareup/scales/UsbCable;)Lcom/squareup/thread/executor/MainThread;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/squareup/scales/UsbCable;->mainThread:Lcom/squareup/thread/executor/MainThread;

    return-object p0
.end method

.method public static final synthetic access$getManager$p(Lcom/squareup/scales/UsbCable;)Lcom/squareup/hardware/usb/UsbManager;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/squareup/scales/UsbCable;->manager:Lcom/squareup/hardware/usb/UsbManager;

    return-object p0
.end method

.method public static final synthetic access$getRealScaleTracker$p(Lcom/squareup/scales/UsbCable;)Lcom/squareup/scales/RealScaleTracker;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/squareup/scales/UsbCable;->realScaleTracker:Lcom/squareup/scales/RealScaleTracker;

    return-object p0
.end method

.method public static final synthetic access$getUsbDevice$p(Lcom/squareup/scales/UsbCable;)Landroid/hardware/usb/UsbDevice;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/squareup/scales/UsbCable;->usbDevice:Landroid/hardware/usb/UsbDevice;

    return-object p0
.end method

.method public static final synthetic access$getUsbScale$p(Lcom/squareup/scales/UsbCable;)Lcom/squareup/scales/UsbScale;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/squareup/scales/UsbCable;->usbScale:Lcom/squareup/scales/UsbScale;

    return-object p0
.end method

.method public static final synthetic access$getUsbScaleInterpreter$p(Lcom/squareup/scales/UsbCable;)Lcom/squareup/scales/UsbScaleInterpreter;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/squareup/scales/UsbCable;->usbScaleInterpreter:Lcom/squareup/scales/UsbScaleInterpreter;

    return-object p0
.end method

.method public static final synthetic access$setActive$p(Lcom/squareup/scales/UsbCable;Z)V
    .locals 0

    .line 16
    iput-boolean p1, p0, Lcom/squareup/scales/UsbCable;->active:Z

    return-void
.end method

.method public static final synthetic access$setUsbScale$p(Lcom/squareup/scales/UsbCable;Lcom/squareup/scales/UsbScale;)V
    .locals 0

    .line 16
    iput-object p1, p0, Lcom/squareup/scales/UsbCable;->usbScale:Lcom/squareup/scales/UsbScale;

    return-void
.end method


# virtual methods
.method public final onDisconnected()V
    .locals 1

    const/4 v0, 0x0

    .line 78
    iput-boolean v0, p0, Lcom/squareup/scales/UsbCable;->active:Z

    .line 79
    iget-object v0, p0, Lcom/squareup/scales/UsbCable;->usbScale:Lcom/squareup/scales/UsbScale;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/scales/UsbScale;->onDisconnected()V

    :cond_0
    return-void
.end method

.method public final onPluggedIntoPhone()V
    .locals 2

    .line 32
    iget-object v0, p0, Lcom/squareup/scales/UsbCable;->backgroundThreadExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/squareup/scales/UsbCable$onPluggedIntoPhone$1;

    invoke-direct {v1, p0}, Lcom/squareup/scales/UsbCable$onPluggedIntoPhone$1;-><init>(Lcom/squareup/scales/UsbCable;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
