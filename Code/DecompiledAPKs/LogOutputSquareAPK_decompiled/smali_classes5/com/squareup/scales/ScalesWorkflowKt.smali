.class public final Lcom/squareup/scales/ScalesWorkflowKt;
.super Ljava/lang/Object;
.source "ScalesWorkflow.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000*(\u0010\u0000\"\u0008\u0012\u0004\u0012\u00020\u0002`\u00012\u001a\u0012\u0004\u0012\u00020\u0002\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0004j\u0002`\u00050\u0003\u00a8\u0006\u0006"
    }
    d2 = {
        "ScalesScreen",
        "Lcom/squareup/workflow/LayeredScreen;",
        "Lcom/squareup/container/PosLayering;",
        "",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation
