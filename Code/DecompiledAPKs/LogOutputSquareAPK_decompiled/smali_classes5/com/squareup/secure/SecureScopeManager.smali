.class public final Lcom/squareup/secure/SecureScopeManager;
.super Lmortar/Presenter;
.source "SecureScopeManager.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/secure/SecureScopeManager$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/Presenter<",
        "Landroid/app/Activity;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSecureScopeManager.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SecureScopeManager.kt\ncom/squareup/secure/SecureScopeManager\n+ 2 ArraysJVM.kt\nkotlin/collections/ArraysKt__ArraysJVMKt\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,130:1\n37#2,2:131\n37#2,2:133\n704#3:135\n777#3,2:136\n1360#3:138\n1429#3,3:139\n*E\n*S KotlinDebug\n*F\n+ 1 SecureScopeManager.kt\ncom/squareup/secure/SecureScopeManager\n*L\n56#1,2:131\n57#1,2:133\n104#1:135\n104#1,2:136\n105#1:138\n105#1,3:139\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010#\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0007\u0018\u0000 !2\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001!B\u000f\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u000e\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u000bJ\u0012\u0010\u0010\u001a\u00020\u00112\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u0002H\u0014J\u000e\u0010\u0013\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u000bJ\u0010\u0010\u0014\u001a\u00020\u000e2\u0006\u0010\u0015\u001a\u00020\u0016H\u0014J\u0012\u0010\u0017\u001a\u00020\u000e2\u0008\u0010\u0018\u001a\u0004\u0018\u00010\u0019H\u0016J\u0010\u0010\u001a\u001a\u00020\u000e2\u0006\u0010\u001b\u001a\u00020\u0019H\u0016J\u0016\u0010\u001c\u001a\u00020\u000e2\u000c\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\u001f0\u001eH\u0002J\u0008\u0010 \u001a\u00020\u000eH\u0002R\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0082\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0008R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/secure/SecureScopeManager;",
        "Lmortar/Presenter;",
        "Landroid/app/Activity;",
        "posContainer",
        "Lcom/squareup/ui/main/PosContainer;",
        "(Lcom/squareup/ui/main/PosContainer;)V",
        "isSecure",
        "",
        "Ljava/lang/Boolean;",
        "secureScreens",
        "",
        "",
        "secureSessions",
        "enterSecureSession",
        "",
        "key",
        "extractBundleService",
        "Lmortar/bundler/BundleService;",
        "activity",
        "leaveSecureSession",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onLoad",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onSave",
        "outState",
        "onScreensChanged",
        "topScreens",
        "",
        "Lcom/squareup/container/ContainerTreeKey;",
        "updateFlagSecure",
        "Companion",
        "secure_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/secure/SecureScopeManager$Companion;

.field private static final SCREENS:Ljava/lang/String; = "secure_screens"

.field private static final SESSIONS:Ljava/lang/String; = "secure_sessions"


# instance fields
.field private isSecure:Ljava/lang/Boolean;

.field private final posContainer:Lcom/squareup/ui/main/PosContainer;

.field private final secureScreens:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final secureSessions:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/secure/SecureScopeManager$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/secure/SecureScopeManager$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/secure/SecureScopeManager;->Companion:Lcom/squareup/secure/SecureScopeManager$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/main/PosContainer;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "posContainer"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-direct {p0}, Lmortar/Presenter;-><init>()V

    iput-object p1, p0, Lcom/squareup/secure/SecureScopeManager;->posContainer:Lcom/squareup/ui/main/PosContainer;

    .line 34
    new-instance p1, Ljava/util/LinkedHashSet;

    invoke-direct {p1}, Ljava/util/LinkedHashSet;-><init>()V

    check-cast p1, Ljava/util/Set;

    iput-object p1, p0, Lcom/squareup/secure/SecureScopeManager;->secureSessions:Ljava/util/Set;

    .line 35
    new-instance p1, Ljava/util/LinkedHashSet;

    invoke-direct {p1}, Ljava/util/LinkedHashSet;-><init>()V

    check-cast p1, Ljava/util/Set;

    iput-object p1, p0, Lcom/squareup/secure/SecureScopeManager;->secureScreens:Ljava/util/Set;

    return-void
.end method

.method public static final synthetic access$onScreensChanged(Lcom/squareup/secure/SecureScopeManager;Ljava/util/Collection;)V
    .locals 0

    .line 29
    invoke-direct {p0, p1}, Lcom/squareup/secure/SecureScopeManager;->onScreensChanged(Ljava/util/Collection;)V

    return-void
.end method

.method private final onScreensChanged(Ljava/util/Collection;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">;)V"
        }
    .end annotation

    .line 102
    iget-object v0, p0, Lcom/squareup/secure/SecureScopeManager;->secureScreens:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 103
    iget-object v0, p0, Lcom/squareup/secure/SecureScopeManager;->secureScreens:Ljava/util/Set;

    check-cast v0, Ljava/util/Collection;

    check-cast p1, Ljava/lang/Iterable;

    .line 135
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 136
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/squareup/container/ContainerTreeKey;

    .line 104
    instance-of v4, v3, Lcom/squareup/secure/FlagSecure;

    if-eqz v4, :cond_1

    check-cast v3, Lcom/squareup/secure/FlagSecure;

    invoke-interface {v3}, Lcom/squareup/secure/FlagSecure;->isSecure()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x1

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    :goto_1
    if-eqz v3, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 137
    :cond_2
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 138
    new-instance p1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v1, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {p1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p1, Ljava/util/Collection;

    .line 139
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 140
    check-cast v2, Lcom/squareup/container/ContainerTreeKey;

    .line 105
    invoke-virtual {v2}, Lcom/squareup/container/ContainerTreeKey;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 141
    :cond_3
    check-cast p1, Ljava/util/List;

    check-cast p1, Ljava/lang/Iterable;

    .line 103
    invoke-static {v0, p1}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    .line 107
    invoke-direct {p0}, Lcom/squareup/secure/SecureScopeManager;->updateFlagSecure()V

    return-void
.end method

.method private final updateFlagSecure()V
    .locals 4

    .line 111
    iget-object v0, p0, Lcom/squareup/secure/SecureScopeManager;->secureSessions:Ljava/util/Set;

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    const/4 v2, 0x0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/secure/SecureScopeManager;->secureScreens:Ljava/util/Set;

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/2addr v0, v1

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 113
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/squareup/secure/SecureScopeManager;->isSecure:Ljava/lang/Boolean;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {p0}, Lcom/squareup/secure/SecureScopeManager;->hasView()Z

    move-result v0

    if-nez v0, :cond_2

    goto :goto_1

    .line 114
    :cond_2
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/secure/SecureScopeManager;->isSecure:Ljava/lang/Boolean;

    .line 116
    iget-object v0, p0, Lcom/squareup/secure/SecureScopeManager;->isSecure:Ljava/lang/Boolean;

    if-nez v0, :cond_3

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_3
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const/16 v1, 0x2000

    const-string/jumbo v3, "view"

    if-eqz v0, :cond_4

    new-array v0, v2, [Ljava/lang/Object;

    const-string v2, "Setting FLAG_SECURE [ON]"

    .line 117
    invoke-static {v2, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 118
    invoke-virtual {p0}, Lcom/squareup/secure/SecureScopeManager;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    goto :goto_1

    :cond_4
    new-array v0, v2, [Ljava/lang/Object;

    const-string v2, "Setting FLAG_SECURE [OFF]"

    .line 120
    invoke-static {v2, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 121
    invoke-virtual {p0}, Lcom/squareup/secure/SecureScopeManager;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    :cond_5
    :goto_1
    return-void
.end method


# virtual methods
.method public final enterSecureSession(Ljava/lang/String;)V
    .locals 2

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 75
    invoke-static {v0, v1, v0}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread$default(Ljava/lang/String;ILjava/lang/Object;)V

    .line 77
    iget-object v0, p0, Lcom/squareup/secure/SecureScopeManager;->secureSessions:Ljava/util/Set;

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 78
    invoke-direct {p0}, Lcom/squareup/secure/SecureScopeManager;->updateFlagSecure()V

    return-void
.end method

.method protected extractBundleService(Landroid/app/Activity;)Lmortar/bundler/BundleService;
    .locals 1

    .line 39
    check-cast p1, Landroid/content/Context;

    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Landroid/content/Context;)Lmortar/bundler/BundleService;

    move-result-object p1

    const-string v0, "BundleService.getBundleService(activity)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public bridge synthetic extractBundleService(Ljava/lang/Object;)Lmortar/bundler/BundleService;
    .locals 0

    .line 29
    check-cast p1, Landroid/app/Activity;

    invoke-virtual {p0, p1}, Lcom/squareup/secure/SecureScopeManager;->extractBundleService(Landroid/app/Activity;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method public final leaveSecureSession(Ljava/lang/String;)V
    .locals 2

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 88
    invoke-static {v0, v1, v0}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread$default(Ljava/lang/String;ILjava/lang/Object;)V

    .line 90
    iget-object v0, p0, Lcom/squareup/secure/SecureScopeManager;->secureSessions:Ljava/util/Set;

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    .line 91
    invoke-direct {p0}, Lcom/squareup/secure/SecureScopeManager;->updateFlagSecure()V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    invoke-super {p0, p1}, Lmortar/Presenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 63
    iget-object v0, p0, Lcom/squareup/secure/SecureScopeManager;->posContainer:Lcom/squareup/ui/main/PosContainer;

    invoke-interface {v0}, Lcom/squareup/ui/main/PosContainer;->traversalByVisibleScreens()Lio/reactivex/Observable;

    move-result-object v0

    .line 64
    new-instance v1, Lcom/squareup/secure/SecureScopeManager$onEnterScope$1;

    move-object v2, p0

    check-cast v2, Lcom/squareup/secure/SecureScopeManager;

    invoke-direct {v1, v2}, Lcom/squareup/secure/SecureScopeManager$onEnterScope$1;-><init>(Lcom/squareup/secure/SecureScopeManager;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    new-instance v2, Lcom/squareup/secure/SecureScopeManager$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {v2, v1}, Lcom/squareup/secure/SecureScopeManager$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "posContainer.traversalBy\u2026cribe(::onScreensChanged)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 3

    .line 43
    invoke-super {p0, p1}, Lmortar/Presenter;->onLoad(Landroid/os/Bundle;)V

    if-eqz p1, :cond_1

    const-string v0, "secure_sessions"

    .line 45
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const-string v1, "it"

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/squareup/secure/SecureScopeManager;->secureSessions:Ljava/util/Set;

    check-cast v2, Ljava/util/Collection;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v2, v0}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    :cond_0
    const-string v0, "secure_screens"

    .line 46
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/squareup/secure/SecureScopeManager;->secureScreens:Ljava/util/Set;

    check-cast v0, Ljava/util/Collection;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p1}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    :cond_1
    const/4 p1, 0x0

    .line 50
    check-cast p1, Ljava/lang/Boolean;

    iput-object p1, p0, Lcom/squareup/secure/SecureScopeManager;->isSecure:Ljava/lang/Boolean;

    .line 51
    invoke-direct {p0}, Lcom/squareup/secure/SecureScopeManager;->updateFlagSecure()V

    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 4

    const-string v0, "outState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    invoke-super {p0, p1}, Lmortar/Presenter;->onSave(Landroid/os/Bundle;)V

    .line 56
    iget-object v0, p0, Lcom/squareup/secure/SecureScopeManager;->secureSessions:Ljava/util/Set;

    check-cast v0, Ljava/util/Collection;

    const/4 v1, 0x0

    new-array v2, v1, [Ljava/lang/String;

    .line 132
    invoke-interface {v0, v2}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    const-string v2, "null cannot be cast to non-null type kotlin.Array<T>"

    if-eqz v0, :cond_1

    check-cast v0, [Ljava/lang/String;

    const-string v3, "secure_sessions"

    .line 56
    invoke-virtual {p1, v3, v0}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 57
    iget-object v0, p0, Lcom/squareup/secure/SecureScopeManager;->secureScreens:Ljava/util/Set;

    check-cast v0, Ljava/util/Collection;

    new-array v1, v1, [Ljava/lang/String;

    .line 134
    invoke-interface {v0, v1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, [Ljava/lang/String;

    const-string v1, "secure_screens"

    .line 57
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    return-void

    .line 134
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 132
    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
