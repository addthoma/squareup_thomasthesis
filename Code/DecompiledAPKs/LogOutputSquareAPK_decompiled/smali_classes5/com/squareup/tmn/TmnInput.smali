.class public final Lcom/squareup/tmn/TmnInput;
.super Ljava/lang/Object;
.source "TmnInput.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000f\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\tH\u00c6\u0003J1\u0010\u0017\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\tH\u00c6\u0001J\u0013\u0010\u0018\u001a\u00020\u00192\u0008\u0010\u001a\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u001b\u001a\u00020\u001cH\u00d6\u0001J\t\u0010\u001d\u001a\u00020\u001eH\u00d6\u0001R\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012\u00a8\u0006\u001f"
    }
    d2 = {
        "Lcom/squareup/tmn/TmnInput;",
        "",
        "tmnTransactionType",
        "Lcom/squareup/tmn/TmnTransactionType;",
        "brandId",
        "Lcom/squareup/cardreader/lcr/CrsTmnBrandId;",
        "amountAuthorized",
        "",
        "action",
        "Lcom/squareup/tmn/Action;",
        "(Lcom/squareup/tmn/TmnTransactionType;Lcom/squareup/cardreader/lcr/CrsTmnBrandId;JLcom/squareup/tmn/Action;)V",
        "getAction",
        "()Lcom/squareup/tmn/Action;",
        "getAmountAuthorized",
        "()J",
        "getBrandId",
        "()Lcom/squareup/cardreader/lcr/CrsTmnBrandId;",
        "getTmnTransactionType",
        "()Lcom/squareup/tmn/TmnTransactionType;",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final action:Lcom/squareup/tmn/Action;

.field private final amountAuthorized:J

.field private final brandId:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

.field private final tmnTransactionType:Lcom/squareup/tmn/TmnTransactionType;


# direct methods
.method public constructor <init>(Lcom/squareup/tmn/TmnTransactionType;Lcom/squareup/cardreader/lcr/CrsTmnBrandId;JLcom/squareup/tmn/Action;)V
    .locals 1

    const-string/jumbo v0, "tmnTransactionType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "brandId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "action"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/tmn/TmnInput;->tmnTransactionType:Lcom/squareup/tmn/TmnTransactionType;

    iput-object p2, p0, Lcom/squareup/tmn/TmnInput;->brandId:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    iput-wide p3, p0, Lcom/squareup/tmn/TmnInput;->amountAuthorized:J

    iput-object p5, p0, Lcom/squareup/tmn/TmnInput;->action:Lcom/squareup/tmn/Action;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/tmn/TmnInput;Lcom/squareup/tmn/TmnTransactionType;Lcom/squareup/cardreader/lcr/CrsTmnBrandId;JLcom/squareup/tmn/Action;ILjava/lang/Object;)Lcom/squareup/tmn/TmnInput;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-object p1, p0, Lcom/squareup/tmn/TmnInput;->tmnTransactionType:Lcom/squareup/tmn/TmnTransactionType;

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-object p2, p0, Lcom/squareup/tmn/TmnInput;->brandId:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget-wide p3, p0, Lcom/squareup/tmn/TmnInput;->amountAuthorized:J

    :cond_2
    move-wide v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget-object p5, p0, Lcom/squareup/tmn/TmnInput;->action:Lcom/squareup/tmn/Action;

    :cond_3
    move-object v2, p5

    move-object p2, p0

    move-object p3, p1

    move-object p4, p7

    move-wide p5, v0

    move-object p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/squareup/tmn/TmnInput;->copy(Lcom/squareup/tmn/TmnTransactionType;Lcom/squareup/cardreader/lcr/CrsTmnBrandId;JLcom/squareup/tmn/Action;)Lcom/squareup/tmn/TmnInput;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/tmn/TmnTransactionType;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tmn/TmnInput;->tmnTransactionType:Lcom/squareup/tmn/TmnTransactionType;

    return-object v0
.end method

.method public final component2()Lcom/squareup/cardreader/lcr/CrsTmnBrandId;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tmn/TmnInput;->brandId:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    return-object v0
.end method

.method public final component3()J
    .locals 2

    iget-wide v0, p0, Lcom/squareup/tmn/TmnInput;->amountAuthorized:J

    return-wide v0
.end method

.method public final component4()Lcom/squareup/tmn/Action;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tmn/TmnInput;->action:Lcom/squareup/tmn/Action;

    return-object v0
.end method

.method public final copy(Lcom/squareup/tmn/TmnTransactionType;Lcom/squareup/cardreader/lcr/CrsTmnBrandId;JLcom/squareup/tmn/Action;)Lcom/squareup/tmn/TmnInput;
    .locals 7

    const-string/jumbo v0, "tmnTransactionType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "brandId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "action"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/tmn/TmnInput;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/squareup/tmn/TmnInput;-><init>(Lcom/squareup/tmn/TmnTransactionType;Lcom/squareup/cardreader/lcr/CrsTmnBrandId;JLcom/squareup/tmn/Action;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/tmn/TmnInput;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/tmn/TmnInput;

    iget-object v0, p0, Lcom/squareup/tmn/TmnInput;->tmnTransactionType:Lcom/squareup/tmn/TmnTransactionType;

    iget-object v1, p1, Lcom/squareup/tmn/TmnInput;->tmnTransactionType:Lcom/squareup/tmn/TmnTransactionType;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tmn/TmnInput;->brandId:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    iget-object v1, p1, Lcom/squareup/tmn/TmnInput;->brandId:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/squareup/tmn/TmnInput;->amountAuthorized:J

    iget-wide v2, p1, Lcom/squareup/tmn/TmnInput;->amountAuthorized:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-object v0, p0, Lcom/squareup/tmn/TmnInput;->action:Lcom/squareup/tmn/Action;

    iget-object p1, p1, Lcom/squareup/tmn/TmnInput;->action:Lcom/squareup/tmn/Action;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAction()Lcom/squareup/tmn/Action;
    .locals 1

    .line 9
    iget-object v0, p0, Lcom/squareup/tmn/TmnInput;->action:Lcom/squareup/tmn/Action;

    return-object v0
.end method

.method public final getAmountAuthorized()J
    .locals 2

    .line 8
    iget-wide v0, p0, Lcom/squareup/tmn/TmnInput;->amountAuthorized:J

    return-wide v0
.end method

.method public final getBrandId()Lcom/squareup/cardreader/lcr/CrsTmnBrandId;
    .locals 1

    .line 7
    iget-object v0, p0, Lcom/squareup/tmn/TmnInput;->brandId:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    return-object v0
.end method

.method public final getTmnTransactionType()Lcom/squareup/tmn/TmnTransactionType;
    .locals 1

    .line 6
    iget-object v0, p0, Lcom/squareup/tmn/TmnInput;->tmnTransactionType:Lcom/squareup/tmn/TmnTransactionType;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    iget-object v0, p0, Lcom/squareup/tmn/TmnInput;->tmnTransactionType:Lcom/squareup/tmn/TmnTransactionType;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/tmn/TmnInput;->brandId:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/squareup/tmn/TmnInput;->amountAuthorized:J

    invoke-static {v2, v3}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/tmn/TmnInput;->action:Lcom/squareup/tmn/Action;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TmnInput(tmnTransactionType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tmn/TmnInput;->tmnTransactionType:Lcom/squareup/tmn/TmnTransactionType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", brandId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tmn/TmnInput;->brandId:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", amountAuthorized="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/tmn/TmnInput;->amountAuthorized:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", action="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tmn/TmnInput;->action:Lcom/squareup/tmn/Action;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
