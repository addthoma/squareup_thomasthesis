.class public final synthetic Lcom/squareup/tmn/TmnTimings$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 3

    invoke-static {}, Lcom/squareup/tmn/What;->values()[Lcom/squareup/tmn/What;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/tmn/TmnTimings$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lcom/squareup/tmn/TmnTimings$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/tmn/What;->WORKFLOW_SEND_TO_SERVER:Lcom/squareup/tmn/What;

    invoke-virtual {v1}, Lcom/squareup/tmn/What;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/tmn/TmnTimings$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/tmn/What;->WORKFLOW_ON_SERVER_SUCCESS:Lcom/squareup/tmn/What;

    invoke-virtual {v1}, Lcom/squareup/tmn/What;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/tmn/TmnTimings$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/tmn/What;->WORKFLOW_ON_SERVER_FAILED:Lcom/squareup/tmn/What;

    invoke-virtual {v1}, Lcom/squareup/tmn/What;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/tmn/TmnTimings$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/tmn/What;->GATT_WRITE_CHARACTERISTIC:Lcom/squareup/tmn/What;

    invoke-virtual {v1}, Lcom/squareup/tmn/What;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/tmn/TmnTimings$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/tmn/What;->GATT_READ_CHARACTERISTIC:Lcom/squareup/tmn/What;

    invoke-virtual {v1}, Lcom/squareup/tmn/What;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/tmn/TmnTimings$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/tmn/What;->BLE_BACKEND_VECTOR_TO_LCR:Lcom/squareup/tmn/What;

    invoke-virtual {v1}, Lcom/squareup/tmn/What;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/tmn/TmnTimings$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/tmn/What;->BLE_BACKEND_BYTES_TO_LCR:Lcom/squareup/tmn/What;

    invoke-virtual {v1}, Lcom/squareup/tmn/What;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/tmn/TmnTimings$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/tmn/What;->GATT_CALLBACK_ON_CHARACTERISTIC_READ:Lcom/squareup/tmn/What;

    invoke-virtual {v1}, Lcom/squareup/tmn/What;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/tmn/TmnTimings$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/tmn/What;->GATT_CALLBACK_ON_CHARACTERISTIC_CHANGED:Lcom/squareup/tmn/What;

    invoke-virtual {v1}, Lcom/squareup/tmn/What;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/tmn/TmnTimings$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/tmn/What;->BLE_BACKEND_BYTES_FROM_LCR:Lcom/squareup/tmn/What;

    invoke-virtual {v1}, Lcom/squareup/tmn/What;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/tmn/TmnTimings$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/tmn/What;->BLE_BACKEND_BYTES_READ_ACK_VECTOR:Lcom/squareup/tmn/What;

    invoke-virtual {v1}, Lcom/squareup/tmn/What;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/tmn/TmnTimings$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/tmn/What;->PAYMENT_FEATURE_TMN_BYTES_TO_READER:Lcom/squareup/tmn/What;

    invoke-virtual {v1}, Lcom/squareup/tmn/What;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/tmn/TmnTimings$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/tmn/What;->PAYMENT_FEATURE_TMN_ACK_WRITE_NOTIFY:Lcom/squareup/tmn/What;

    invoke-virtual {v1}, Lcom/squareup/tmn/What;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/tmn/TmnTimings$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/tmn/What;->PAYMENT_FEATURE_TMN_START:Lcom/squareup/tmn/What;

    invoke-virtual {v1}, Lcom/squareup/tmn/What;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/tmn/TmnTimings$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/tmn/What;->PAYMENT_FEATURE_TMN_START_MIRYO:Lcom/squareup/tmn/What;

    invoke-virtual {v1}, Lcom/squareup/tmn/What;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/tmn/TmnTimings$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/tmn/What;->PAYMENT_FEATURE_ON_TMN_AUTH_REQUEST:Lcom/squareup/tmn/What;

    invoke-virtual {v1}, Lcom/squareup/tmn/What;->ordinal()I

    move-result v1

    const/16 v2, 0x10

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/tmn/TmnTimings$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/tmn/What;->PAYMENT_FEATURE_ON_TMN_DATA_TO_TMN:Lcom/squareup/tmn/What;

    invoke-virtual {v1}, Lcom/squareup/tmn/What;->ordinal()I

    move-result v1

    const/16 v2, 0x11

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/tmn/TmnTimings$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/tmn/What;->PAYMENT_FEATURE_ON_TMN_WRITE_NOTIFY:Lcom/squareup/tmn/What;

    invoke-virtual {v1}, Lcom/squareup/tmn/What;->ordinal()I

    move-result v1

    const/16 v2, 0x12

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/tmn/TmnTimings$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/tmn/What;->PAYMENT_FEATURE_ON_TMN_TRANSACTION_COMPLETE:Lcom/squareup/tmn/What;

    invoke-virtual {v1}, Lcom/squareup/tmn/What;->ordinal()I

    move-result v1

    const/16 v2, 0x13

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/tmn/TmnTimings$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/tmn/What;->WORKFLOW_STARTING_PAYMENT_ON_READER:Lcom/squareup/tmn/What;

    invoke-virtual {v1}, Lcom/squareup/tmn/What;->ordinal()I

    move-result v1

    const/16 v2, 0x14

    aput v2, v0, v1

    return-void
.end method
