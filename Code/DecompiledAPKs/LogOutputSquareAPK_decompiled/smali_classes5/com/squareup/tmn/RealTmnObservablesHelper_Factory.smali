.class public final Lcom/squareup/tmn/RealTmnObservablesHelper_Factory;
.super Ljava/lang/Object;
.source "RealTmnObservablesHelper_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/tmn/RealTmnObservablesHelper;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;)V"
        }
    .end annotation

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/squareup/tmn/RealTmnObservablesHelper_Factory;->arg0Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/tmn/RealTmnObservablesHelper_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;)",
            "Lcom/squareup/tmn/RealTmnObservablesHelper_Factory;"
        }
    .end annotation

    .line 26
    new-instance v0, Lcom/squareup/tmn/RealTmnObservablesHelper_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/tmn/RealTmnObservablesHelper_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/cardreader/CardReaderListeners;)Lcom/squareup/tmn/RealTmnObservablesHelper;
    .locals 1

    .line 30
    new-instance v0, Lcom/squareup/tmn/RealTmnObservablesHelper;

    invoke-direct {v0, p0}, Lcom/squareup/tmn/RealTmnObservablesHelper;-><init>(Lcom/squareup/cardreader/CardReaderListeners;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/tmn/RealTmnObservablesHelper;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/tmn/RealTmnObservablesHelper_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderListeners;

    invoke-static {v0}, Lcom/squareup/tmn/RealTmnObservablesHelper_Factory;->newInstance(Lcom/squareup/cardreader/CardReaderListeners;)Lcom/squareup/tmn/RealTmnObservablesHelper;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/tmn/RealTmnObservablesHelper_Factory;->get()Lcom/squareup/tmn/RealTmnObservablesHelper;

    move-result-object v0

    return-object v0
.end method
