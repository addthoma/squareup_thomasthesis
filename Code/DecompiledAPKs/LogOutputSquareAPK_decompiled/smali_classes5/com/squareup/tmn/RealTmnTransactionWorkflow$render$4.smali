.class final Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$4;
.super Lkotlin/jvm/internal/Lambda;
.source "RealTmnTransactionWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tmn/RealTmnTransactionWorkflow;->render(Lcom/squareup/tmn/TmnTransactionInput;Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;Lcom/squareup/workflow/RenderContext;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentStarterOutput;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;",
        "+",
        "Lcom/squareup/tmn/TmnTransactionOutput;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;",
        "Lcom/squareup/tmn/TmnTransactionOutput;",
        "it",
        "Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentStarterOutput;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;

.field final synthetic this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/tmn/RealTmnTransactionWorkflow;Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$4;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    iput-object p2, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$4;->$state:Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentStarterOutput;)Lcom/squareup/workflow/WorkflowAction;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentStarterOutput;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;",
            "Lcom/squareup/tmn/TmnTransactionOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 207
    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$4;->$state:Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;

    check-cast v0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForInitialTmnDataFromLcr;

    invoke-virtual {v0}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForInitialTmnDataFromLcr;->getPaymentInfo()Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentInfo;->getTransactionType()Lcom/squareup/tmn/TmnTransactionType;

    move-result-object v0

    sget-object v1, Lcom/squareup/tmn/TmnTransactionType;->Miryo:Lcom/squareup/tmn/TmnTransactionType;

    if-ne v0, v1, :cond_0

    .line 208
    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$4;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    invoke-virtual {v0}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->getMiryoWorkerDelayer()Lcom/squareup/tmn/MiryoWorkerDelayer;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/tmn/MiryoWorkerDelayer;->miryoStarted()V

    .line 211
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentStarterOutput;->getCardReader()Lcom/squareup/cardreader/CardReader;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 212
    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$4;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    iget-object v1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$4;->$state:Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;

    check-cast v1, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForInitialTmnDataFromLcr;

    invoke-static {v0, v1, p1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->access$handlePaymentStartSuccessful(Lcom/squareup/tmn/RealTmnTransactionWorkflow;Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForInitialTmnDataFromLcr;Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentStarterOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 214
    :cond_1
    iget-object p1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$4;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    .line 215
    sget-object v0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$Idle;->INSTANCE:Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$Idle;

    check-cast v0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;

    sget-object v1, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->CARDREADER_START_FAILED:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    .line 216
    new-instance v2, Lcom/squareup/tmn/TmnTransactionOutput$Failed$CardReaderError;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-direct {v2, v4, v3, v4}, Lcom/squareup/tmn/TmnTransactionOutput$Failed$CardReaderError;-><init>(Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v2, Lcom/squareup/tmn/TmnTransactionOutput;

    .line 214
    invoke-static {p1, v0, v1, v2}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->access$enterStateAndLog(Lcom/squareup/tmn/RealTmnTransactionWorkflow;Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;Lcom/squareup/tmn/TmnTransactionOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 95
    check-cast p1, Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentStarterOutput;

    invoke-virtual {p0, p1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$4;->invoke(Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentStarterOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
