.class final Lcom/squareup/tmn/RealTmnStarterWorkflow$render$3;
.super Lkotlin/jvm/internal/Lambda;
.source "RealTmnStarterWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tmn/RealTmnStarterWorkflow;->render(Lcom/squareup/tmn/TmnInput;Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState;Lcom/squareup/workflow/RenderContext;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/tmn/TmnTransactionOutput;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState;",
        "+",
        "Lcom/squareup/tmn/TmnOutput;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState;",
        "Lcom/squareup/tmn/TmnOutput;",
        "it",
        "Lcom/squareup/tmn/TmnTransactionOutput;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $input:Lcom/squareup/tmn/TmnInput;

.field final synthetic $state:Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState;

.field final synthetic this$0:Lcom/squareup/tmn/RealTmnStarterWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/tmn/RealTmnStarterWorkflow;Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState;Lcom/squareup/tmn/TmnInput;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tmn/RealTmnStarterWorkflow$render$3;->this$0:Lcom/squareup/tmn/RealTmnStarterWorkflow;

    iput-object p2, p0, Lcom/squareup/tmn/RealTmnStarterWorkflow$render$3;->$state:Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState;

    iput-object p3, p0, Lcom/squareup/tmn/RealTmnStarterWorkflow$render$3;->$input:Lcom/squareup/tmn/TmnInput;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/tmn/TmnTransactionOutput;)Lcom/squareup/workflow/WorkflowAction;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tmn/TmnTransactionOutput;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState;",
            "Lcom/squareup/tmn/TmnOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 118
    instance-of v0, p1, Lcom/squareup/tmn/TmnTransactionOutput$Completed;

    if-eqz v0, :cond_3

    .line 121
    check-cast p1, Lcom/squareup/tmn/TmnTransactionOutput$Completed;

    invoke-virtual {p1}, Lcom/squareup/tmn/TmnTransactionOutput$Completed;->getAfterWriteNotifyData()Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/tmn/RealTmnStarterWorkflow$render$3;->$state:Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState;

    check-cast v0, Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState$StartingPayment;

    invoke-virtual {v0}, Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState$StartingPayment;->getAfterWriteNotifyData()Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_2

    .line 124
    invoke-virtual {p1}, Lcom/squareup/tmn/TmnTransactionOutput$Completed;->getTmnTransactionResult()Lcom/squareup/cardreader/lcr/TmnTransactionResult;

    move-result-object v1

    sget-object v2, Lcom/squareup/cardreader/lcr/TmnTransactionResult;->TMN_RESULT_SUCCESS:Lcom/squareup/cardreader/lcr/TmnTransactionResult;

    if-eq v1, v2, :cond_2

    invoke-virtual {p1}, Lcom/squareup/tmn/TmnTransactionOutput$Completed;->getTmnTransactionResult()Lcom/squareup/cardreader/lcr/TmnTransactionResult;

    move-result-object v1

    sget-object v2, Lcom/squareup/cardreader/lcr/TmnTransactionResult;->TMN_RESULT_MIRYO_RESULT_FAILURE:Lcom/squareup/cardreader/lcr/TmnTransactionResult;

    if-ne v1, v2, :cond_1

    goto :goto_1

    .line 130
    :cond_1
    iget-object p1, p0, Lcom/squareup/tmn/RealTmnStarterWorkflow$render$3;->this$0:Lcom/squareup/tmn/RealTmnStarterWorkflow;

    .line 131
    new-instance v1, Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState$WaitingForStartingMiryo;

    invoke-direct {v1, v0}, Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState$WaitingForStartingMiryo;-><init>(Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;)V

    check-cast v1, Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState;

    sget-object v2, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->SHOULD_ENTER_MIRYO:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    .line 132
    new-instance v3, Lcom/squareup/tmn/TmnOutput$MiryoOutput$TmnErrorInMiryo;

    .line 133
    invoke-virtual {v0}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;->getBeforeBalance()I

    move-result v4

    invoke-virtual {v0}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;->getAmount()I

    move-result v0

    .line 132
    invoke-direct {v3, v4, v0}, Lcom/squareup/tmn/TmnOutput$MiryoOutput$TmnErrorInMiryo;-><init>(II)V

    check-cast v3, Lcom/squareup/tmn/TmnOutput;

    .line 130
    invoke-static {p1, v1, v2, v3}, Lcom/squareup/tmn/RealTmnStarterWorkflow;->access$enterStateAndLog(Lcom/squareup/tmn/RealTmnStarterWorkflow;Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState;Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;Lcom/squareup/tmn/TmnOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_3

    .line 126
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/squareup/tmn/RealTmnStarterWorkflow$render$3;->this$0:Lcom/squareup/tmn/RealTmnStarterWorkflow;

    .line 127
    new-instance v1, Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState$Idle;

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState$Idle;-><init>(ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v1, Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState;

    sget-object v2, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->TMN_TRANSACTION_COMPLETED:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    new-instance v3, Lcom/squareup/tmn/TmnOutput$Completed;

    invoke-virtual {p1}, Lcom/squareup/tmn/TmnTransactionOutput$Completed;->getTmnTransactionResult()Lcom/squareup/cardreader/lcr/TmnTransactionResult;

    move-result-object p1

    invoke-direct {v3, p1}, Lcom/squareup/tmn/TmnOutput$Completed;-><init>(Lcom/squareup/cardreader/lcr/TmnTransactionResult;)V

    check-cast v3, Lcom/squareup/tmn/TmnOutput;

    .line 126
    invoke-static {v0, v1, v2, v3}, Lcom/squareup/tmn/RealTmnStarterWorkflow;->access$enterStateAndLog(Lcom/squareup/tmn/RealTmnStarterWorkflow;Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState;Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;Lcom/squareup/tmn/TmnOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_3

    .line 138
    :cond_3
    instance-of v0, p1, Lcom/squareup/tmn/TmnTransactionOutput$Failed;

    if-eqz v0, :cond_5

    .line 139
    iget-object v0, p0, Lcom/squareup/tmn/RealTmnStarterWorkflow$render$3;->this$0:Lcom/squareup/tmn/RealTmnStarterWorkflow;

    .line 140
    iget-object v1, p0, Lcom/squareup/tmn/RealTmnStarterWorkflow$render$3;->$input:Lcom/squareup/tmn/TmnInput;

    invoke-virtual {v1}, Lcom/squareup/tmn/TmnInput;->getBrandId()Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    move-result-object v1

    .line 141
    check-cast p1, Lcom/squareup/tmn/TmnTransactionOutput$Failed;

    .line 146
    invoke-virtual {p1}, Lcom/squareup/tmn/TmnTransactionOutput$Failed;->getAfterWriteNotifyData()Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;

    move-result-object v2

    if-eqz v2, :cond_4

    goto :goto_2

    :cond_4
    iget-object v2, p0, Lcom/squareup/tmn/RealTmnStarterWorkflow$render$3;->$state:Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState;

    check-cast v2, Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState$StartingPayment;

    invoke-virtual {v2}, Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState$StartingPayment;->getAfterWriteNotifyData()Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;

    move-result-object v2

    .line 139
    :goto_2
    invoke-static {v0, v1, p1, v2}, Lcom/squareup/tmn/RealTmnStarterWorkflow;->access$handleStartingPaymentFailure(Lcom/squareup/tmn/RealTmnStarterWorkflow;Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Lcom/squareup/tmn/TmnTransactionOutput$Failed;Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_3

    .line 148
    :cond_5
    instance-of p1, p1, Lcom/squareup/tmn/TmnTransactionOutput$AfterWriteNotify;

    if-eqz p1, :cond_6

    .line 149
    iget-object p1, p0, Lcom/squareup/tmn/RealTmnStarterWorkflow$render$3;->this$0:Lcom/squareup/tmn/RealTmnStarterWorkflow;

    iget-object v0, p0, Lcom/squareup/tmn/RealTmnStarterWorkflow$render$3;->$state:Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState;

    sget-object v1, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->TMN_TRANSACTION_CARD_WRITE_STARTED:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    sget-object v2, Lcom/squareup/tmn/TmnOutput$CardWriteStarted;->INSTANCE:Lcom/squareup/tmn/TmnOutput$CardWriteStarted;

    check-cast v2, Lcom/squareup/tmn/TmnOutput;

    invoke-static {p1, v0, v1, v2}, Lcom/squareup/tmn/RealTmnStarterWorkflow;->access$enterStateAndLog(Lcom/squareup/tmn/RealTmnStarterWorkflow;Lcom/squareup/tmn/RealTmnStarterWorkflow$TmnState;Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;Lcom/squareup/tmn/TmnOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_3
    return-object p1

    :cond_6
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 56
    check-cast p1, Lcom/squareup/tmn/TmnTransactionOutput;

    invoke-virtual {p0, p1}, Lcom/squareup/tmn/RealTmnStarterWorkflow$render$3;->invoke(Lcom/squareup/tmn/TmnTransactionOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
