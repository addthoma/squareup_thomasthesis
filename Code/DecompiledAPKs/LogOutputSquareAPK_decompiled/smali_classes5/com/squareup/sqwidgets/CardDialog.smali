.class public Lcom/squareup/sqwidgets/CardDialog;
.super Ljava/lang/Object;
.source "CardDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/sqwidgets/CardDialog$DismissGlyph;
    }
.end annotation


# instance fields
.field private final DISMISS_DIALOG:Landroid/view/View$OnClickListener;

.field private final actionButton:Landroid/widget/Button;

.field private final contentView:Landroid/view/ViewGroup;

.field private final context:Landroid/content/Context;

.field private final dialog:Landroid/app/Dialog;

.field private final dismissButton:Lcom/squareup/glyph/SquareGlyphView;

.field private final statusBarOverlay:Landroid/view/View;

.field private final titleTextView:Landroid/widget/TextView;

.field private type:Lcom/squareup/sqwidgets/DialogType;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    .line 54
    invoke-direct {p0, p1, v0}, Lcom/squareup/sqwidgets/CardDialog;-><init>(Landroid/content/Context;Lcom/squareup/coordinators/CoordinatorProvider;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/squareup/coordinators/CoordinatorProvider;)V
    .locals 3

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    new-instance v0, Lcom/squareup/sqwidgets/CardDialog$1;

    invoke-direct {v0, p0}, Lcom/squareup/sqwidgets/CardDialog$1;-><init>(Lcom/squareup/sqwidgets/CardDialog;)V

    iput-object v0, p0, Lcom/squareup/sqwidgets/CardDialog;->DISMISS_DIALOG:Landroid/view/View$OnClickListener;

    .line 64
    iput-object p1, p0, Lcom/squareup/sqwidgets/CardDialog;->context:Landroid/content/Context;

    .line 65
    new-instance v0, Landroid/app/Dialog;

    invoke-direct {v0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/squareup/sqwidgets/CardDialog;->dialog:Landroid/app/Dialog;

    .line 67
    iget-object v0, p0, Lcom/squareup/sqwidgets/CardDialog;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/squareup/sqwidgets/R$layout;->card_dialog_layout:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 68
    iget-object v1, p0, Lcom/squareup/sqwidgets/CardDialog;->dialog:Landroid/app/Dialog;

    invoke-virtual {v1, v0}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 69
    iget-object v1, p0, Lcom/squareup/sqwidgets/CardDialog;->dialog:Landroid/app/Dialog;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 71
    sget v1, Lcom/squareup/sqwidgets/R$id;->status_bar_overlay:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/sqwidgets/CardDialog;->statusBarOverlay:Landroid/view/View;

    .line 73
    sget v1, Lcom/squareup/sqwidgets/R$id;->dismiss_button:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v1, p0, Lcom/squareup/sqwidgets/CardDialog;->dismissButton:Lcom/squareup/glyph/SquareGlyphView;

    .line 75
    sget v1, Lcom/squareup/sqwidgets/R$id;->title_text_view:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/squareup/sqwidgets/CardDialog;->titleTextView:Landroid/widget/TextView;

    .line 76
    sget v1, Lcom/squareup/sqwidgets/R$id;->content_view:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/squareup/sqwidgets/CardDialog;->contentView:Landroid/view/ViewGroup;

    .line 77
    sget v1, Lcom/squareup/sqwidgets/R$id;->action_button:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/squareup/sqwidgets/CardDialog;->actionButton:Landroid/widget/Button;

    .line 79
    invoke-static {p1}, Lcom/squareup/sqwidgets/DialogType;->forContext(Landroid/content/Context;)Lcom/squareup/sqwidgets/DialogType;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/sqwidgets/CardDialog;->type:Lcom/squareup/sqwidgets/DialogType;

    .line 81
    iget-object p1, p0, Lcom/squareup/sqwidgets/CardDialog;->statusBarOverlay:Landroid/view/View;

    iget-object v1, p0, Lcom/squareup/sqwidgets/CardDialog;->DISMISS_DIALOG:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    if-eqz p2, :cond_0

    .line 84
    invoke-static {v0, p2}, Lcom/squareup/coordinators/Coordinators;->bind(Landroid/view/View;Lcom/squareup/coordinators/CoordinatorProvider;)V

    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/squareup/sqwidgets/CardDialog;)Landroid/app/Dialog;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/squareup/sqwidgets/CardDialog;->dialog:Landroid/app/Dialog;

    return-object p0
.end method

.method static synthetic lambda$show$0(Ljava/lang/Runnable;Landroid/content/DialogInterface;)V
    .locals 0

    .line 159
    invoke-interface {p0}, Ljava/lang/Runnable;->run()V

    return-void
.end method


# virtual methods
.method public dismiss()V
    .locals 1

    .line 212
    iget-object v0, p0, Lcom/squareup/sqwidgets/CardDialog;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    return-void
.end method

.method public getBaseContext()Landroid/content/Context;
    .locals 1

    .line 208
    iget-object v0, p0, Lcom/squareup/sqwidgets/CardDialog;->context:Landroid/content/Context;

    return-object v0
.end method

.method public isDismissed()Z
    .locals 1

    .line 147
    iget-object v0, p0, Lcom/squareup/sqwidgets/CardDialog;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public setAction(ZLjava/lang/Runnable;)Lcom/squareup/sqwidgets/CardDialog;
    .locals 2

    .line 120
    iget-object v0, p0, Lcom/squareup/sqwidgets/CardDialog;->actionButton:Landroid/widget/Button;

    new-instance v1, Lcom/squareup/sqwidgets/CardDialog$2;

    invoke-direct {v1, p0, p2, p1}, Lcom/squareup/sqwidgets/CardDialog$2;-><init>(Lcom/squareup/sqwidgets/CardDialog;Ljava/lang/Runnable;Z)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object p0
.end method

.method public setActionEnabled(Z)Lcom/squareup/sqwidgets/CardDialog;
    .locals 1

    .line 132
    iget-object v0, p0, Lcom/squareup/sqwidgets/CardDialog;->actionButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    return-object p0
.end method

.method public setActionText(I)Lcom/squareup/sqwidgets/CardDialog;
    .locals 1

    .line 105
    iget-object v0, p0, Lcom/squareup/sqwidgets/CardDialog;->context:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/sqwidgets/CardDialog;->setActionText(Ljava/lang/String;)Lcom/squareup/sqwidgets/CardDialog;

    move-result-object p1

    return-object p1
.end method

.method public setActionText(Ljava/lang/String;)Lcom/squareup/sqwidgets/CardDialog;
    .locals 2

    .line 114
    iget-object v0, p0, Lcom/squareup/sqwidgets/CardDialog;->actionButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 115
    iget-object v0, p0, Lcom/squareup/sqwidgets/CardDialog;->actionButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    return-object p0
.end method

.method public setCanceledOnTouchOutside(Z)Lcom/squareup/sqwidgets/CardDialog;
    .locals 1

    .line 142
    iget-object v0, p0, Lcom/squareup/sqwidgets/CardDialog;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0, p1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    return-object p0
.end method

.method public setDismissGlyph(Lcom/squareup/sqwidgets/CardDialog$DismissGlyph;)Lcom/squareup/sqwidgets/CardDialog;
    .locals 1

    .line 109
    iget-object v0, p0, Lcom/squareup/sqwidgets/CardDialog;->dismissButton:Lcom/squareup/glyph/SquareGlyphView;

    invoke-static {p1}, Lcom/squareup/sqwidgets/CardDialog$DismissGlyph;->access$100(Lcom/squareup/sqwidgets/CardDialog$DismissGlyph;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    return-object p0
.end method

.method public setTitle(I)Lcom/squareup/sqwidgets/CardDialog;
    .locals 1

    .line 89
    iget-object v0, p0, Lcom/squareup/sqwidgets/CardDialog;->titleTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    return-object p0
.end method

.method public setTitle(Ljava/lang/String;)Lcom/squareup/sqwidgets/CardDialog;
    .locals 1

    .line 94
    iget-object v0, p0, Lcom/squareup/sqwidgets/CardDialog;->titleTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-object p0
.end method

.method public setType(Lcom/squareup/sqwidgets/DialogType;)Lcom/squareup/sqwidgets/CardDialog;
    .locals 0

    .line 137
    iput-object p1, p0, Lcom/squareup/sqwidgets/CardDialog;->type:Lcom/squareup/sqwidgets/DialogType;

    return-object p0
.end method

.method public setView(Landroid/view/View;)Lcom/squareup/sqwidgets/CardDialog;
    .locals 1

    .line 99
    iget-object v0, p0, Lcom/squareup/sqwidgets/CardDialog;->contentView:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 100
    iget-object v0, p0, Lcom/squareup/sqwidgets/CardDialog;->contentView:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-object p0
.end method

.method public show()Lcom/squareup/sqwidgets/CardDialog;
    .locals 1

    .line 151
    new-instance v0, Lcom/squareup/sqwidgets/CardDialog$3;

    invoke-direct {v0, p0}, Lcom/squareup/sqwidgets/CardDialog$3;-><init>(Lcom/squareup/sqwidgets/CardDialog;)V

    invoke-virtual {p0, v0}, Lcom/squareup/sqwidgets/CardDialog;->show(Ljava/lang/Runnable;)Lcom/squareup/sqwidgets/CardDialog;

    move-result-object v0

    return-object v0
.end method

.method public show(Ljava/lang/Runnable;)Lcom/squareup/sqwidgets/CardDialog;
    .locals 4

    .line 158
    iget-object v0, p0, Lcom/squareup/sqwidgets/CardDialog;->dismissButton:Lcom/squareup/glyph/SquareGlyphView;

    iget-object v1, p0, Lcom/squareup/sqwidgets/CardDialog;->DISMISS_DIALOG:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 159
    iget-object v0, p0, Lcom/squareup/sqwidgets/CardDialog;->dialog:Landroid/app/Dialog;

    new-instance v1, Lcom/squareup/sqwidgets/-$$Lambda$CardDialog$DsWwkS8jbDRQrdrfzfOcg4w56NA;

    invoke-direct {v1, p1}, Lcom/squareup/sqwidgets/-$$Lambda$CardDialog$DsWwkS8jbDRQrdrfzfOcg4w56NA;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 161
    iget-object p1, p0, Lcom/squareup/sqwidgets/CardDialog;->dialog:Landroid/app/Dialog;

    invoke-virtual {p1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object p1

    .line 162
    iget-object v0, p0, Lcom/squareup/sqwidgets/CardDialog;->type:Lcom/squareup/sqwidgets/DialogType;

    invoke-virtual {v0, p1}, Lcom/squareup/sqwidgets/DialogType;->applyTypeForWindow(Landroid/view/Window;)V

    .line 167
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 168
    invoke-virtual {p1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 170
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v1

    .line 171
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    .line 173
    sget-object v2, Lcom/squareup/sqwidgets/CardDialog$4;->$SwitchMap$com$squareup$sqwidgets$DialogType:[I

    iget-object v3, p0, Lcom/squareup/sqwidgets/CardDialog;->type:Lcom/squareup/sqwidgets/DialogType;

    invoke-virtual {v3}, Lcom/squareup/sqwidgets/DialogType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    const/4 v3, 0x1

    if-eq v2, v3, :cond_0

    const/4 v3, 0x2

    if-eq v2, v3, :cond_1

    .line 189
    iget-object v2, p0, Lcom/squareup/sqwidgets/CardDialog;->context:Landroid/content/Context;

    invoke-static {v2}, Lcom/squareup/sqwidgets/Settings;->statusBarSize(Landroid/content/Context;)I

    move-result v2

    sub-int/2addr v1, v2

    goto :goto_0

    .line 177
    :cond_0
    iget-object v2, p0, Lcom/squareup/sqwidgets/CardDialog;->context:Landroid/content/Context;

    invoke-static {v2}, Lcom/squareup/sqwidgets/Settings;->navBarSize(Landroid/content/Context;)I

    move-result v2

    add-int/2addr v1, v2

    .line 179
    iget-object v2, p0, Lcom/squareup/sqwidgets/CardDialog;->statusBarOverlay:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/sqwidgets/CardDialog;->context:Landroid/content/Context;

    invoke-static {v3}, Lcom/squareup/sqwidgets/Settings;->statusBarSize(Landroid/content/Context;)I

    move-result v3

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    :cond_1
    :goto_0
    if-ge v1, v0, :cond_2

    .line 194
    invoke-virtual {p1, v1, v1}, Landroid/view/Window;->setLayout(II)V

    goto :goto_1

    .line 196
    :cond_2
    invoke-virtual {p1, v0, v1}, Landroid/view/Window;->setLayout(II)V

    .line 199
    :goto_1
    iget-object p1, p0, Lcom/squareup/sqwidgets/CardDialog;->dialog:Landroid/app/Dialog;

    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    return-object p0
.end method
