.class public Lcom/squareup/sqwidgets/time/MarketDatePicker;
.super Lcom/squareup/marin/widgets/MarinDatePicker;
.source "MarketDatePicker.java"


# instance fields
.field private dayPickerInput:Landroid/widget/EditText;

.field private dayPickerPaint:Landroid/graphics/Paint;

.field private monthPickerInput:Landroid/widget/EditText;

.field private monthPickerPaint:Landroid/graphics/Paint;

.field private yearPickerInput:Landroid/widget/EditText;

.field private yearPickerPaint:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    .line 29
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/sqwidgets/time/MarketDatePicker;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 33
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/marin/widgets/MarinDatePicker;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 4

    .line 37
    invoke-super {p0}, Lcom/squareup/marin/widgets/MarinDatePicker;->onFinishInflate()V

    .line 38
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "numberpicker_input"

    const-string v2, "id"

    const-string v3, "android"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 39
    iget-object v1, p0, Lcom/squareup/sqwidgets/time/MarketDatePicker;->dayPicker:Landroid/widget/NumberPicker;

    invoke-static {v1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/squareup/sqwidgets/time/MarketDatePicker;->dayPickerInput:Landroid/widget/EditText;

    .line 40
    iget-object v1, p0, Lcom/squareup/sqwidgets/time/MarketDatePicker;->monthPicker:Landroid/widget/NumberPicker;

    invoke-static {v1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/squareup/sqwidgets/time/MarketDatePicker;->monthPickerInput:Landroid/widget/EditText;

    .line 41
    iget-object v1, p0, Lcom/squareup/sqwidgets/time/MarketDatePicker;->yearPicker:Landroid/widget/NumberPicker;

    invoke-static {v1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/squareup/sqwidgets/time/MarketDatePicker;->yearPickerInput:Landroid/widget/EditText;

    .line 44
    iget-object v0, p0, Lcom/squareup/sqwidgets/time/MarketDatePicker;->dayPickerInput:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 45
    iget-object v0, p0, Lcom/squareup/sqwidgets/time/MarketDatePicker;->monthPickerInput:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 46
    iget-object v0, p0, Lcom/squareup/sqwidgets/time/MarketDatePicker;->yearPickerInput:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 53
    :try_start_0
    const-class v0, Landroid/widget/NumberPicker;

    const-string v2, "mSelectorWheelPaint"

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    const/4 v2, 0x1

    .line 54
    invoke-virtual {v0, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 55
    iget-object v2, p0, Lcom/squareup/sqwidgets/time/MarketDatePicker;->dayPicker:Landroid/widget/NumberPicker;

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Paint;

    iput-object v2, p0, Lcom/squareup/sqwidgets/time/MarketDatePicker;->dayPickerPaint:Landroid/graphics/Paint;

    .line 56
    iget-object v2, p0, Lcom/squareup/sqwidgets/time/MarketDatePicker;->monthPicker:Landroid/widget/NumberPicker;

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Paint;

    iput-object v2, p0, Lcom/squareup/sqwidgets/time/MarketDatePicker;->monthPickerPaint:Landroid/graphics/Paint;

    .line 57
    iget-object v2, p0, Lcom/squareup/sqwidgets/time/MarketDatePicker;->yearPicker:Landroid/widget/NumberPicker;

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Paint;

    iput-object v0, p0, Lcom/squareup/sqwidgets/time/MarketDatePicker;->yearPickerPaint:Landroid/graphics/Paint;
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 62
    invoke-virtual {p0, v1}, Lcom/squareup/sqwidgets/time/MarketDatePicker;->setDividerColor(I)V

    .line 63
    invoke-virtual {p0}, Lcom/squareup/sqwidgets/time/MarketDatePicker;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/squareup/marketfont/MarketFont$Weight;->LIGHT:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-static {v0, v1}, Lcom/squareup/marketfont/MarketTypeface;->getTypeface(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;)Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/sqwidgets/time/MarketDatePicker;->setTypeface(Landroid/graphics/Typeface;)V

    const/4 v0, -0x1

    .line 64
    invoke-virtual {p0, v0}, Lcom/squareup/sqwidgets/time/MarketDatePicker;->setTextColor(I)V

    return-void

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0

    :catch_2
    move-exception v0

    .line 59
    :goto_0
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public setCellSize(II)V
    .locals 0

    .line 130
    invoke-virtual {p0, p1, p2}, Lcom/squareup/sqwidgets/time/MarketDatePicker;->setPickerSize(II)V

    return-void
.end method

.method public setDividerColor(I)V
    .locals 2

    .line 75
    :try_start_0
    const-class v0, Landroid/widget/NumberPicker;

    const-string v1, "mSelectionDivider"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    const/4 v1, 0x1

    .line 76
    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 77
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v1, p1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 78
    iget-object p1, p0, Lcom/squareup/sqwidgets/time/MarketDatePicker;->dayPicker:Landroid/widget/NumberPicker;

    invoke-virtual {v0, p1, v1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 79
    iget-object p1, p0, Lcom/squareup/sqwidgets/time/MarketDatePicker;->monthPicker:Landroid/widget/NumberPicker;

    invoke-virtual {v0, p1, v1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 80
    iget-object p1, p0, Lcom/squareup/sqwidgets/time/MarketDatePicker;->yearPicker:Landroid/widget/NumberPicker;

    invoke-virtual {v0, p1, v1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    goto :goto_0

    :catch_1
    move-exception p1

    goto :goto_0

    :catch_2
    move-exception p1

    goto :goto_0

    :catch_3
    move-exception p1

    .line 83
    :goto_0
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method public setTextColor(I)V
    .locals 1

    .line 107
    iget-object v0, p0, Lcom/squareup/sqwidgets/time/MarketDatePicker;->dayPickerInput:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setTextColor(I)V

    .line 108
    iget-object v0, p0, Lcom/squareup/sqwidgets/time/MarketDatePicker;->monthPickerInput:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setTextColor(I)V

    .line 109
    iget-object v0, p0, Lcom/squareup/sqwidgets/time/MarketDatePicker;->yearPickerInput:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setTextColor(I)V

    .line 110
    iget-object v0, p0, Lcom/squareup/sqwidgets/time/MarketDatePicker;->dayPickerPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 111
    iget-object v0, p0, Lcom/squareup/sqwidgets/time/MarketDatePicker;->monthPickerPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 112
    iget-object v0, p0, Lcom/squareup/sqwidgets/time/MarketDatePicker;->yearPickerPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    return-void
.end method

.method public setTextSize(I)V
    .locals 1

    .line 121
    iget-object v0, p0, Lcom/squareup/sqwidgets/time/MarketDatePicker;->dayPickerInput:Landroid/widget/EditText;

    int-to-float p1, p1

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setTextSize(F)V

    .line 122
    iget-object v0, p0, Lcom/squareup/sqwidgets/time/MarketDatePicker;->monthPickerInput:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setTextSize(F)V

    .line 123
    iget-object v0, p0, Lcom/squareup/sqwidgets/time/MarketDatePicker;->yearPickerInput:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setTextSize(F)V

    .line 124
    iget-object v0, p0, Lcom/squareup/sqwidgets/time/MarketDatePicker;->dayPickerPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 125
    iget-object v0, p0, Lcom/squareup/sqwidgets/time/MarketDatePicker;->monthPickerPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 126
    iget-object v0, p0, Lcom/squareup/sqwidgets/time/MarketDatePicker;->yearPickerPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setTextSize(F)V

    return-void
.end method

.method public setTypeface(Landroid/graphics/Typeface;)V
    .locals 1

    .line 93
    iget-object v0, p0, Lcom/squareup/sqwidgets/time/MarketDatePicker;->dayPickerInput:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setTypeface(Landroid/graphics/Typeface;)V

    .line 94
    iget-object v0, p0, Lcom/squareup/sqwidgets/time/MarketDatePicker;->monthPickerInput:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setTypeface(Landroid/graphics/Typeface;)V

    .line 95
    iget-object v0, p0, Lcom/squareup/sqwidgets/time/MarketDatePicker;->yearPickerInput:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setTypeface(Landroid/graphics/Typeface;)V

    .line 96
    iget-object v0, p0, Lcom/squareup/sqwidgets/time/MarketDatePicker;->dayPickerPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 97
    iget-object v0, p0, Lcom/squareup/sqwidgets/time/MarketDatePicker;->monthPickerPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 98
    iget-object v0, p0, Lcom/squareup/sqwidgets/time/MarketDatePicker;->yearPickerPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    return-void
.end method
