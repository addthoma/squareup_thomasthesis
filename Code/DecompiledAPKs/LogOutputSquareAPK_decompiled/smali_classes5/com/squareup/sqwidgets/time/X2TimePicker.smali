.class public final Lcom/squareup/sqwidgets/time/X2TimePicker;
.super Landroid/widget/RelativeLayout;
.source "X2TimePicker.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/sqwidgets/time/X2TimePicker$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nX2TimePicker.kt\nKotlin\n*S Kotlin\n*F\n+ 1 X2TimePicker.kt\ncom/squareup/sqwidgets/time/X2TimePicker\n+ 2 Views.kt\ncom/squareup/util/Views\n+ 3 _Sequences.kt\nkotlin/sequences/SequencesKt___SequencesKt\n*L\n1#1,127:1\n1163#2:128\n1104#3,2:129\n*E\n*S KotlinDebug\n*F\n+ 1 X2TimePicker.kt\ncom/squareup/sqwidgets/time/X2TimePicker\n*L\n65#1:128\n66#1,2:129\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000X\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010#\n\u0002\u0010\"\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0004\u0018\u0000 %2\u00020\u0001:\u0001%B%\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J0\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u0011\u001a\u00020\u00072\u0006\u0010\r\u001a\u00020\u00072\u0006\u0010\u001b\u001a\u00020\u00072\u0006\u0010\u001c\u001a\u00020\u0007H\u0002J\u0010\u0010\u001d\u001a\u00020\u00132\u0006\u0010\u001e\u001a\u00020\u001fH\u0002J\u0016\u0010 \u001a\u00020\u00182\u0006\u0010\u001e\u001a\u00020\u001f2\u0006\u0010\u0014\u001a\u00020\u000bJ\u0010\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020\u001aH\u0002J\u0008\u0010$\u001a\u00020\u0018H\u0014R\u000e\u0010\t\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\n\u001a\n \u000c*\u0004\u0018\u00010\u000b0\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R2\u0010\u000e\u001a&\u0012\u000c\u0012\n \u000c*\u0004\u0018\u00010\u00070\u0007 \u000c*\u0012\u0012\u000c\u0012\n \u000c*\u0004\u0018\u00010\u00070\u0007\u0018\u00010\u00100\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082.\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0014\u001a\u00020\u000b8F\u00a2\u0006\u0006\u001a\u0004\u0008\u0015\u0010\u0016\u00a8\u0006&"
    }
    d2 = {
        "Lcom/squareup/sqwidgets/time/X2TimePicker;",
        "Landroid/widget/RelativeLayout;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "defStyleAttr",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "cellStyleId",
        "currentTime",
        "Ljava/util/Calendar;",
        "kotlin.jvm.PlatformType",
        "overlayBackground",
        "overlayIds",
        "",
        "",
        "paddingSides",
        "pickerStrategy",
        "Lcom/squareup/sqwidgets/time/PickerStrategy;",
        "time",
        "getTime",
        "()Ljava/util/Calendar;",
        "formatOverlay",
        "",
        "overlay",
        "Landroid/view/View;",
        "width",
        "height",
        "getPickerStrategy",
        "timeFormatChooser",
        "Lcom/squareup/sqwidgets/time/TimeFormatChooser;",
        "initialize",
        "isOverlay",
        "",
        "view",
        "onAttachedToWindow",
        "Companion",
        "x2widgets_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/sqwidgets/time/X2TimePicker$Companion;

.field private static final INFINITE_SCROLL_PAGE_COUNT:I = 0x3e8

.field private static final NON_INIFINITE_SCROLL_PAGE_COUNT:I = 0x1


# instance fields
.field private final cellStyleId:I

.field private final currentTime:Ljava/util/Calendar;

.field private final overlayBackground:I

.field private final overlayIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final paddingSides:I

.field private pickerStrategy:Lcom/squareup/sqwidgets/time/PickerStrategy;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/sqwidgets/time/X2TimePicker$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/sqwidgets/time/X2TimePicker$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/sqwidgets/time/X2TimePicker;->Companion:Lcom/squareup/sqwidgets/time/X2TimePicker$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/sqwidgets/time/X2TimePicker;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/sqwidgets/time/X2TimePicker;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 26
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sqwidgets/time/X2TimePicker;->currentTime:Ljava/util/Calendar;

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Integer;

    .line 27
    sget v1, Lcom/squareup/sqwidgets/R$id;->overlay_top:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget v1, Lcom/squareup/sqwidgets/R$id;->overlay_middle:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v3, 0x1

    aput-object v1, v0, v3

    sget v1, Lcom/squareup/sqwidgets/R$id;->overlay_bottom:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v3, 0x2

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/squareup/util/SquareCollections;->asSet([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sqwidgets/time/X2TimePicker;->overlayIds:Ljava/util/Set;

    .line 36
    sget-object v0, Lcom/squareup/sqwidgets/R$styleable;->X2TimePicker:[I

    invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p2

    .line 38
    sget p3, Lcom/squareup/sqwidgets/R$styleable;->X2TimePicker_cellStyle:I

    sget v0, Lcom/squareup/sqwidgets/R$style;->Widget_Marin_TimePickerCell:I

    .line 37
    invoke-virtual {p2, p3, v0}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result p3

    iput p3, p0, Lcom/squareup/sqwidgets/time/X2TimePicker;->cellStyleId:I

    .line 40
    sget p3, Lcom/squareup/sqwidgets/R$styleable;->X2TimePicker_paddingSides:I

    invoke-virtual {p2, p3, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p3

    iput p3, p0, Lcom/squareup/sqwidgets/time/X2TimePicker;->paddingSides:I

    .line 42
    sget p3, Lcom/squareup/sqwidgets/R$styleable;->X2TimePicker_overlayBackground:I

    .line 43
    sget v0, Lcom/squareup/marin/R$color;->marin_white:I

    invoke-static {p1, v0}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p1

    .line 41
    invoke-virtual {p2, p3, p1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p1

    iput p1, p0, Lcom/squareup/sqwidgets/time/X2TimePicker;->overlayBackground:I

    .line 45
    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    .line 19
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 20
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/sqwidgets/time/X2TimePicker;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public static final synthetic access$getCellStyleId$p(Lcom/squareup/sqwidgets/time/X2TimePicker;)I
    .locals 0

    .line 17
    iget p0, p0, Lcom/squareup/sqwidgets/time/X2TimePicker;->cellStyleId:I

    return p0
.end method

.method public static final synthetic access$isOverlay(Lcom/squareup/sqwidgets/time/X2TimePicker;Landroid/view/View;)Z
    .locals 0

    .line 17
    invoke-direct {p0, p1}, Lcom/squareup/sqwidgets/time/X2TimePicker;->isOverlay(Landroid/view/View;)Z

    move-result p0

    return p0
.end method

.method private final formatOverlay(Landroid/view/View;IIII)V
    .locals 1

    .line 78
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, p4, p5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 80
    iput p2, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 81
    iput p2, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 79
    check-cast v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 84
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p2

    sget p4, Lcom/squareup/sqwidgets/R$id;->overlay_top:I

    if-eq p2, p4, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p2

    sget p4, Lcom/squareup/sqwidgets/R$id;->overlay_bottom:I

    if-ne p2, p4, :cond_1

    .line 85
    :cond_0
    invoke-virtual {p1, p3}, Landroid/view/View;->setBackgroundColor(I)V

    :cond_1
    return-void
.end method

.method private final getPickerStrategy(Lcom/squareup/sqwidgets/time/TimeFormatChooser;)Lcom/squareup/sqwidgets/time/PickerStrategy;
    .locals 4

    .line 90
    new-instance v0, Lcom/squareup/sqwidgets/time/X2TimePicker$getPickerStrategy$pickerFactory$1;

    invoke-direct {v0, p0}, Lcom/squareup/sqwidgets/time/X2TimePicker$getPickerStrategy$pickerFactory$1;-><init>(Lcom/squareup/sqwidgets/time/X2TimePicker;)V

    check-cast v0, Lkotlin/jvm/functions/Function3;

    .line 102
    invoke-interface {p1}, Lcom/squareup/sqwidgets/time/TimeFormatChooser;->is24HourFormat()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 103
    new-instance p1, Lcom/squareup/sqwidgets/time/TwentyFourHourPickerStrategy;

    .line 105
    sget v1, Lcom/squareup/sqwidgets/R$id;->hour_picker:I

    .line 106
    sget v2, Lcom/squareup/sqwidgets/R$id;->minute_picker:I

    .line 103
    invoke-direct {p1, v0, v1, v2}, Lcom/squareup/sqwidgets/time/TwentyFourHourPickerStrategy;-><init>(Lkotlin/jvm/functions/Function3;II)V

    check-cast p1, Lcom/squareup/sqwidgets/time/PickerStrategy;

    goto :goto_0

    .line 109
    :cond_0
    new-instance p1, Lcom/squareup/sqwidgets/time/TwelveHourPickerStrategy;

    .line 111
    sget v1, Lcom/squareup/sqwidgets/R$id;->hour_picker:I

    .line 112
    sget v2, Lcom/squareup/sqwidgets/R$id;->minute_picker:I

    .line 113
    sget v3, Lcom/squareup/sqwidgets/R$id;->am_pm_picker:I

    .line 109
    invoke-direct {p1, v0, v1, v2, v3}, Lcom/squareup/sqwidgets/time/TwelveHourPickerStrategy;-><init>(Lkotlin/jvm/functions/Function3;III)V

    check-cast p1, Lcom/squareup/sqwidgets/time/PickerStrategy;

    :goto_0
    return-object p1
.end method

.method private final isOverlay(Landroid/view/View;)Z
    .locals 1

    .line 69
    iget-object v0, p0, Lcom/squareup/sqwidgets/time/X2TimePicker;->overlayIds:Ljava/util/Set;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method


# virtual methods
.method public final getTime()Ljava/util/Calendar;
    .locals 2

    .line 32
    iget-object v0, p0, Lcom/squareup/sqwidgets/time/X2TimePicker;->pickerStrategy:Lcom/squareup/sqwidgets/time/PickerStrategy;

    if-nez v0, :cond_0

    const-string v1, "pickerStrategy"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/sqwidgets/time/PickerStrategy;->getTime()Ljava/util/Calendar;

    move-result-object v0

    return-object v0
.end method

.method public final initialize(Lcom/squareup/sqwidgets/time/TimeFormatChooser;Ljava/util/Calendar;)V
    .locals 7

    const-string/jumbo v0, "timeFormatChooser"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "time"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    invoke-virtual {p0}, Lcom/squareup/sqwidgets/time/X2TimePicker;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/squareup/sqwidgets/R$layout;->x2_time_picker_layout:I

    move-object v2, p0

    check-cast v2, Landroid/view/ViewGroup;

    invoke-static {v0, v1, v2}, Landroid/widget/RelativeLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 57
    iget-object v0, p0, Lcom/squareup/sqwidgets/time/X2TimePicker;->currentTime:Ljava/util/Calendar;

    const-string v1, "currentTime"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object p2

    invoke-virtual {v0, p2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 58
    invoke-direct {p0, p1}, Lcom/squareup/sqwidgets/time/X2TimePicker;->getPickerStrategy(Lcom/squareup/sqwidgets/time/TimeFormatChooser;)Lcom/squareup/sqwidgets/time/PickerStrategy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/sqwidgets/time/X2TimePicker;->pickerStrategy:Lcom/squareup/sqwidgets/time/PickerStrategy;

    .line 60
    sget p1, Lcom/squareup/sqwidgets/R$id;->hour_minute_divider:I

    invoke-virtual {p0, p1}, Lcom/squareup/sqwidgets/time/X2TimePicker;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/squareup/sqwidgets/time/X2TimePicker;->getContext()Landroid/content/Context;

    move-result-object p2

    iget v0, p0, Lcom/squareup/sqwidgets/time/X2TimePicker;->cellStyleId:I

    invoke-virtual {p1, p2, v0}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 63
    iget-object p1, p0, Lcom/squareup/sqwidgets/time/X2TimePicker;->pickerStrategy:Lcom/squareup/sqwidgets/time/PickerStrategy;

    const-string p2, "pickerStrategy"

    if-nez p1, :cond_0

    invoke-static {p2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Lcom/squareup/sqwidgets/time/PickerStrategy;->getHourPickerWidth()I

    move-result p1

    .line 64
    iget-object v0, p0, Lcom/squareup/sqwidgets/time/X2TimePicker;->pickerStrategy:Lcom/squareup/sqwidgets/time/PickerStrategy;

    if-nez v0, :cond_1

    invoke-static {p2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0}, Lcom/squareup/sqwidgets/time/PickerStrategy;->getHourPickerHeight()I

    move-result p2

    div-int/lit8 p2, p2, 0x3

    .line 65
    sget v0, Lcom/squareup/sqwidgets/R$id;->overlay:I

    invoke-virtual {p0, v0}, Lcom/squareup/sqwidgets/time/X2TimePicker;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById<ViewGroup>(R.id.overlay)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/ViewGroup;

    const/4 v1, 0x0

    .line 128
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    invoke-static {v1, v2}, Lkotlin/ranges/RangesKt;->until(II)Lkotlin/ranges/IntRange;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object v1

    new-instance v2, Lcom/squareup/sqwidgets/time/X2TimePicker$initialize$$inlined$getChildren$1;

    invoke-direct {v2, v0}, Lcom/squareup/sqwidgets/time/X2TimePicker$initialize$$inlined$getChildren$1;-><init>(Landroid/view/ViewGroup;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-static {v1, v2}, Lkotlin/sequences/SequencesKt;->map(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object v0

    .line 65
    new-instance v1, Lcom/squareup/sqwidgets/time/X2TimePicker$initialize$1;

    move-object v2, p0

    check-cast v2, Lcom/squareup/sqwidgets/time/X2TimePicker;

    invoke-direct {v1, v2}, Lcom/squareup/sqwidgets/time/X2TimePicker$initialize$1;-><init>(Lcom/squareup/sqwidgets/time/X2TimePicker;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lkotlin/sequences/SequencesKt;->filter(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object v0

    .line 129
    invoke-interface {v0}, Lkotlin/sequences/Sequence;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/view/View;

    .line 66
    iget v2, p0, Lcom/squareup/sqwidgets/time/X2TimePicker;->paddingSides:I

    iget v3, p0, Lcom/squareup/sqwidgets/time/X2TimePicker;->overlayBackground:I

    move-object v0, p0

    move v4, p1

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/sqwidgets/time/X2TimePicker;->formatOverlay(Landroid/view/View;IIII)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 3

    .line 118
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onAttachedToWindow()V

    .line 119
    iget-object v0, p0, Lcom/squareup/sqwidgets/time/X2TimePicker;->pickerStrategy:Lcom/squareup/sqwidgets/time/PickerStrategy;

    if-nez v0, :cond_0

    const-string v1, "pickerStrategy"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lcom/squareup/sqwidgets/time/X2TimePicker;->currentTime:Ljava/util/Calendar;

    const-string v2, "currentTime"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/squareup/sqwidgets/time/PickerStrategy;->setDisplayedTime(Ljava/util/Calendar;)V

    return-void
.end method
