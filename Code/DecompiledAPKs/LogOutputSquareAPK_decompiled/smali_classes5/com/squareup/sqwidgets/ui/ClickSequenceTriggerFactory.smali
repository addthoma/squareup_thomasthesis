.class public final Lcom/squareup/sqwidgets/ui/ClickSequenceTriggerFactory;
.super Ljava/lang/Object;
.source "ClickSequenceTrigger.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u0008\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J*\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\u00082\u0008\u0008\u0002\u0010\n\u001a\u00020\u000b2\u0008\u0008\u0002\u0010\u000c\u001a\u00020\rR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/sqwidgets/ui/ClickSequenceTriggerFactory;",
        "",
        "clock",
        "Lcom/squareup/util/Clock;",
        "(Lcom/squareup/util/Clock;)V",
        "create",
        "Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger;",
        "leftButtonNonAccessible",
        "Lcom/squareup/sqwidgets/ui/NonAccessibleTouchEventRelayButton;",
        "rightButtonNonAccessible",
        "maxTimeBetweenClicksMilli",
        "",
        "numberOfClicksToTrigger",
        "",
        "x2widgets_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final clock:Lcom/squareup/util/Clock;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Clock;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "clock"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/sqwidgets/ui/ClickSequenceTriggerFactory;->clock:Lcom/squareup/util/Clock;

    return-void
.end method

.method public static synthetic create$default(Lcom/squareup/sqwidgets/ui/ClickSequenceTriggerFactory;Lcom/squareup/sqwidgets/ui/NonAccessibleTouchEventRelayButton;Lcom/squareup/sqwidgets/ui/NonAccessibleTouchEventRelayButton;JIILjava/lang/Object;)Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger;
    .locals 6

    and-int/lit8 p7, p6, 0x4

    if-eqz p7, :cond_0

    const-wide/16 p3, 0x3e8

    :cond_0
    move-wide v3, p3

    and-int/lit8 p3, p6, 0x8

    if-eqz p3, :cond_1

    const/4 p5, 0x3

    const/4 v5, 0x3

    goto :goto_0

    :cond_1
    move v5, p5

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    .line 158
    invoke-virtual/range {v0 .. v5}, Lcom/squareup/sqwidgets/ui/ClickSequenceTriggerFactory;->create(Lcom/squareup/sqwidgets/ui/NonAccessibleTouchEventRelayButton;Lcom/squareup/sqwidgets/ui/NonAccessibleTouchEventRelayButton;JI)Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final create(Lcom/squareup/sqwidgets/ui/NonAccessibleTouchEventRelayButton;Lcom/squareup/sqwidgets/ui/NonAccessibleTouchEventRelayButton;JI)Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger;
    .locals 8

    const-string v0, "leftButtonNonAccessible"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rightButtonNonAccessible"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 159
    new-instance v0, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger;

    .line 160
    invoke-virtual {p1}, Lcom/squareup/sqwidgets/ui/NonAccessibleTouchEventRelayButton;->touchEvents()Lio/reactivex/Observable;

    move-result-object v2

    .line 161
    invoke-virtual {p2}, Lcom/squareup/sqwidgets/ui/NonAccessibleTouchEventRelayButton;->touchEvents()Lio/reactivex/Observable;

    move-result-object v3

    .line 162
    iget-object v4, p0, Lcom/squareup/sqwidgets/ui/ClickSequenceTriggerFactory;->clock:Lcom/squareup/util/Clock;

    move-object v1, v0

    move-wide v5, p3

    move v7, p5

    .line 159
    invoke-direct/range {v1 .. v7}, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger;-><init>(Lio/reactivex/Observable;Lio/reactivex/Observable;Lcom/squareup/util/Clock;JI)V

    return-object v0
.end method
