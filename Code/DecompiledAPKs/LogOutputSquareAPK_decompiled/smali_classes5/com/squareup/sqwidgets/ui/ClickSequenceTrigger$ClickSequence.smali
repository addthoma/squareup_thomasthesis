.class abstract Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence;
.super Ljava/lang/Object;
.source "ClickSequenceTrigger.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "ClickSequence"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence$ClickSequenceReset;,
        Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence$ClickSequenceInProgress;,
        Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence$ClickSequenceSuccess;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00082\u0018\u00002\u00020\u0001:\u0003\r\u000e\u000fB%\u0008\u0002\u0012\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\t\u0082\u0001\u0003\u0010\u0011\u0012\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence;",
        "",
        "leftClickCount",
        "",
        "rightClickCount",
        "previousClickTimestamp",
        "",
        "(IIJ)V",
        "getLeftClickCount",
        "()I",
        "getPreviousClickTimestamp",
        "()J",
        "getRightClickCount",
        "ClickSequenceInProgress",
        "ClickSequenceReset",
        "ClickSequenceSuccess",
        "Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence$ClickSequenceReset;",
        "Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence$ClickSequenceInProgress;",
        "Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence$ClickSequenceSuccess;",
        "x2widgets_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final leftClickCount:I

.field private final previousClickTimestamp:J

.field private final rightClickCount:I


# direct methods
.method private constructor <init>(IIJ)V
    .locals 0

    .line 132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence;->leftClickCount:I

    iput p2, p0, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence;->rightClickCount:I

    iput-wide p3, p0, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence;->previousClickTimestamp:J

    return-void
.end method

.method synthetic constructor <init>(IIJILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 1

    and-int/lit8 p6, p5, 0x1

    const/4 v0, 0x0

    if-eqz p6, :cond_0

    const/4 p1, 0x0

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    const/4 p2, 0x0

    :cond_1
    and-int/lit8 p5, p5, 0x4

    if-eqz p5, :cond_2

    const-wide/16 p3, 0x0

    .line 135
    :cond_2
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence;-><init>(IIJ)V

    return-void
.end method

.method public synthetic constructor <init>(IIJLkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 132
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence;-><init>(IIJ)V

    return-void
.end method


# virtual methods
.method public final getLeftClickCount()I
    .locals 1

    .line 133
    iget v0, p0, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence;->leftClickCount:I

    return v0
.end method

.method public final getPreviousClickTimestamp()J
    .locals 2

    .line 135
    iget-wide v0, p0, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence;->previousClickTimestamp:J

    return-wide v0
.end method

.method public final getRightClickCount()I
    .locals 1

    .line 134
    iget v0, p0, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence;->rightClickCount:I

    return v0
.end method
