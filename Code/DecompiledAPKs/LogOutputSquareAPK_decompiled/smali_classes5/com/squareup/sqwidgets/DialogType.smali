.class public final enum Lcom/squareup/sqwidgets/DialogType;
.super Ljava/lang/Enum;
.source "DialogType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/sqwidgets/DialogType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/sqwidgets/DialogType;

.field public static final enum REGULAR:Lcom/squareup/sqwidgets/DialogType;

.field public static final enum SYSTEM:Lcom/squareup/sqwidgets/DialogType;

.field public static final enum X2:Lcom/squareup/sqwidgets/DialogType;

.field public static final enum X2_SETTINGS:Lcom/squareup/sqwidgets/DialogType;


# instance fields
.field private windowType:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 18
    new-instance v0, Lcom/squareup/sqwidgets/DialogType;

    const/4 v1, 0x0

    const-string v2, "REGULAR"

    const/4 v3, -0x1

    invoke-direct {v0, v2, v1, v3}, Lcom/squareup/sqwidgets/DialogType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/sqwidgets/DialogType;->REGULAR:Lcom/squareup/sqwidgets/DialogType;

    .line 24
    new-instance v0, Lcom/squareup/sqwidgets/DialogType;

    const/4 v2, 0x1

    const-string v3, "SYSTEM"

    const/16 v4, 0x7d8

    invoke-direct {v0, v3, v2, v4}, Lcom/squareup/sqwidgets/DialogType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/sqwidgets/DialogType;->SYSTEM:Lcom/squareup/sqwidgets/DialogType;

    .line 36
    new-instance v0, Lcom/squareup/sqwidgets/DialogType;

    const/16 v3, 0x7de

    const/4 v4, 0x2

    const-string v5, "X2"

    invoke-direct {v0, v5, v4, v3}, Lcom/squareup/sqwidgets/DialogType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/sqwidgets/DialogType;->X2:Lcom/squareup/sqwidgets/DialogType;

    .line 44
    new-instance v0, Lcom/squareup/sqwidgets/DialogType;

    const/4 v5, 0x3

    const-string v6, "X2_SETTINGS"

    invoke-direct {v0, v6, v5, v3}, Lcom/squareup/sqwidgets/DialogType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/sqwidgets/DialogType;->X2_SETTINGS:Lcom/squareup/sqwidgets/DialogType;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/sqwidgets/DialogType;

    .line 13
    sget-object v3, Lcom/squareup/sqwidgets/DialogType;->REGULAR:Lcom/squareup/sqwidgets/DialogType;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/sqwidgets/DialogType;->SYSTEM:Lcom/squareup/sqwidgets/DialogType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sqwidgets/DialogType;->X2:Lcom/squareup/sqwidgets/DialogType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/sqwidgets/DialogType;->X2_SETTINGS:Lcom/squareup/sqwidgets/DialogType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/sqwidgets/DialogType;->$VALUES:[Lcom/squareup/sqwidgets/DialogType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 48
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 49
    iput p3, p0, Lcom/squareup/sqwidgets/DialogType;->windowType:I

    return-void
.end method

.method public static forContext(Landroid/content/Context;)Lcom/squareup/sqwidgets/DialogType;
    .locals 1

    .line 78
    invoke-static {}, Lcom/squareup/util/X2Build;->isSquareDevice()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    sget-object p0, Lcom/squareup/sqwidgets/DialogType;->X2:Lcom/squareup/sqwidgets/DialogType;

    return-object p0

    .line 82
    :cond_0
    instance-of p0, p0, Landroid/app/Service;

    if-eqz p0, :cond_1

    .line 83
    sget-object p0, Lcom/squareup/sqwidgets/DialogType;->SYSTEM:Lcom/squareup/sqwidgets/DialogType;

    return-object p0

    .line 86
    :cond_1
    sget-object p0, Lcom/squareup/sqwidgets/DialogType;->REGULAR:Lcom/squareup/sqwidgets/DialogType;

    return-object p0
.end method

.method public static forType(I)Lcom/squareup/sqwidgets/DialogType;
    .locals 5

    .line 94
    invoke-static {}, Lcom/squareup/sqwidgets/DialogType;->values()[Lcom/squareup/sqwidgets/DialogType;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 95
    iget v4, v3, Lcom/squareup/sqwidgets/DialogType;->windowType:I

    if-ne v4, p0, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 99
    :cond_1
    sget-object p0, Lcom/squareup/sqwidgets/DialogType;->REGULAR:Lcom/squareup/sqwidgets/DialogType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/sqwidgets/DialogType;
    .locals 1

    .line 13
    const-class v0, Lcom/squareup/sqwidgets/DialogType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/sqwidgets/DialogType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/sqwidgets/DialogType;
    .locals 1

    .line 13
    sget-object v0, Lcom/squareup/sqwidgets/DialogType;->$VALUES:[Lcom/squareup/sqwidgets/DialogType;

    invoke-virtual {v0}, [Lcom/squareup/sqwidgets/DialogType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/sqwidgets/DialogType;

    return-object v0
.end method


# virtual methods
.method public applyTypeForWindow(Landroid/view/Window;)V
    .locals 1

    .line 70
    sget-object v0, Lcom/squareup/sqwidgets/DialogType;->REGULAR:Lcom/squareup/sqwidgets/DialogType;

    if-eq p0, v0, :cond_0

    .line 71
    iget v0, p0, Lcom/squareup/sqwidgets/DialogType;->windowType:I

    invoke-virtual {p1, v0}, Landroid/view/Window;->setType(I)V

    :cond_0
    return-void
.end method
