.class Lcom/squareup/sqlbrite3/BriteDatabase$2;
.super Ljava/lang/Object;
.source "BriteDatabase.java"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sqlbrite3/BriteDatabase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/sqlbrite3/BriteDatabase;


# direct methods
.method constructor <init>(Lcom/squareup/sqlbrite3/BriteDatabase;)V
    .locals 0

    .line 106
    iput-object p1, p0, Lcom/squareup/sqlbrite3/BriteDatabase$2;->this$0:Lcom/squareup/sqlbrite3/BriteDatabase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public accept(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 108
    iget-object p1, p0, Lcom/squareup/sqlbrite3/BriteDatabase$2;->this$0:Lcom/squareup/sqlbrite3/BriteDatabase;

    iget-object p1, p1, Lcom/squareup/sqlbrite3/BriteDatabase;->transactions:Ljava/lang/ThreadLocal;

    invoke-virtual {p1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object p1

    if-nez p1, :cond_0

    return-void

    .line 109
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Cannot subscribe to observable query in a transaction."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
