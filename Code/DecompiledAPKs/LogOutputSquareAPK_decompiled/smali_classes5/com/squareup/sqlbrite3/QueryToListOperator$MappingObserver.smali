.class final Lcom/squareup/sqlbrite3/QueryToListOperator$MappingObserver;
.super Lio/reactivex/observers/DisposableObserver;
.source "QueryToListOperator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sqlbrite3/QueryToListOperator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "MappingObserver"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex/observers/DisposableObserver<",
        "Lcom/squareup/sqlbrite3/SqlBrite$Query;",
        ">;"
    }
.end annotation


# instance fields
.field private final downstream:Lio/reactivex/Observer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observer<",
            "-",
            "Ljava/util/List<",
            "TT;>;>;"
        }
    .end annotation
.end field

.field private final mapper:Lio/reactivex/functions/Function;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/functions/Function<",
            "Landroid/database/Cursor;",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lio/reactivex/Observer;Lio/reactivex/functions/Function;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observer<",
            "-",
            "Ljava/util/List<",
            "TT;>;>;",
            "Lio/reactivex/functions/Function<",
            "Landroid/database/Cursor;",
            "TT;>;)V"
        }
    .end annotation

    .line 43
    invoke-direct {p0}, Lio/reactivex/observers/DisposableObserver;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/squareup/sqlbrite3/QueryToListOperator$MappingObserver;->downstream:Lio/reactivex/Observer;

    .line 45
    iput-object p2, p0, Lcom/squareup/sqlbrite3/QueryToListOperator$MappingObserver;->mapper:Lio/reactivex/functions/Function;

    return-void
.end method


# virtual methods
.method public onComplete()V
    .locals 1

    .line 76
    invoke-virtual {p0}, Lcom/squareup/sqlbrite3/QueryToListOperator$MappingObserver;->isDisposed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/squareup/sqlbrite3/QueryToListOperator$MappingObserver;->downstream:Lio/reactivex/Observer;

    invoke-interface {v0}, Lio/reactivex/Observer;->onComplete()V

    :cond_0
    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    .line 82
    invoke-virtual {p0}, Lcom/squareup/sqlbrite3/QueryToListOperator$MappingObserver;->isDisposed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    invoke-static {p1}, Lio/reactivex/plugins/RxJavaPlugins;->onError(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 85
    :cond_0
    iget-object v0, p0, Lcom/squareup/sqlbrite3/QueryToListOperator$MappingObserver;->downstream:Lio/reactivex/Observer;

    invoke-interface {v0, p1}, Lio/reactivex/Observer;->onError(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method public onNext(Lcom/squareup/sqlbrite3/SqlBrite$Query;)V
    .locals 2

    .line 54
    :try_start_0
    invoke-virtual {p1}, Lcom/squareup/sqlbrite3/SqlBrite$Query;->run()Landroid/database/Cursor;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 55
    invoke-virtual {p0}, Lcom/squareup/sqlbrite3/QueryToListOperator$MappingObserver;->isDisposed()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    .line 58
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 60
    :goto_0
    :try_start_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 61
    iget-object v1, p0, Lcom/squareup/sqlbrite3/QueryToListOperator$MappingObserver;->mapper:Lio/reactivex/functions/Function;

    invoke-interface {v1, p1}, Lio/reactivex/functions/Function;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 64
    :cond_1
    :try_start_2
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 66
    invoke-virtual {p0}, Lcom/squareup/sqlbrite3/QueryToListOperator$MappingObserver;->isDisposed()Z

    move-result p1

    if-nez p1, :cond_3

    .line 67
    iget-object p1, p0, Lcom/squareup/sqlbrite3/QueryToListOperator$MappingObserver;->downstream:Lio/reactivex/Observer;

    invoke-interface {p1, v0}, Lio/reactivex/Observer;->onNext(Ljava/lang/Object;)V

    goto :goto_2

    :catchall_0
    move-exception v0

    .line 64
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_2
    :goto_1
    return-void

    :catchall_1
    move-exception p1

    .line 70
    invoke-static {p1}, Lio/reactivex/exceptions/Exceptions;->throwIfFatal(Ljava/lang/Throwable;)V

    .line 71
    invoke-virtual {p0, p1}, Lcom/squareup/sqlbrite3/QueryToListOperator$MappingObserver;->onError(Ljava/lang/Throwable;)V

    :cond_3
    :goto_2
    return-void
.end method

.method public bridge synthetic onNext(Ljava/lang/Object;)V
    .locals 0

    .line 39
    check-cast p1, Lcom/squareup/sqlbrite3/SqlBrite$Query;

    invoke-virtual {p0, p1}, Lcom/squareup/sqlbrite3/QueryToListOperator$MappingObserver;->onNext(Lcom/squareup/sqlbrite3/SqlBrite$Query;)V

    return-void
.end method

.method protected onStart()V
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/squareup/sqlbrite3/QueryToListOperator$MappingObserver;->downstream:Lio/reactivex/Observer;

    invoke-interface {v0, p0}, Lio/reactivex/Observer;->onSubscribe(Lio/reactivex/disposables/Disposable;)V

    return-void
.end method
