.class public abstract Lcom/squareup/sqlbrite3/SqlBrite$Query;
.super Ljava/lang/Object;
.source "SqlBrite.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sqlbrite3/SqlBrite;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Query"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static mapToList(Lio/reactivex/functions/Function;)Lio/reactivex/ObservableOperator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/functions/Function<",
            "Landroid/database/Cursor;",
            "TT;>;)",
            "Lio/reactivex/ObservableOperator<",
            "Ljava/util/List<",
            "TT;>;",
            "Lcom/squareup/sqlbrite3/SqlBrite$Query;",
            ">;"
        }
    .end annotation

    .line 189
    new-instance v0, Lcom/squareup/sqlbrite3/QueryToListOperator;

    invoke-direct {v0, p0}, Lcom/squareup/sqlbrite3/QueryToListOperator;-><init>(Lio/reactivex/functions/Function;)V

    return-object v0
.end method

.method public static mapToOne(Lio/reactivex/functions/Function;)Lio/reactivex/ObservableOperator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/functions/Function<",
            "Landroid/database/Cursor;",
            "TT;>;)",
            "Lio/reactivex/ObservableOperator<",
            "TT;",
            "Lcom/squareup/sqlbrite3/SqlBrite$Query;",
            ">;"
        }
    .end annotation

    .line 131
    new-instance v0, Lcom/squareup/sqlbrite3/QueryToOneOperator;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/sqlbrite3/QueryToOneOperator;-><init>(Lio/reactivex/functions/Function;Ljava/lang/Object;)V

    return-object v0
.end method

.method public static mapToOneOrDefault(Lio/reactivex/functions/Function;Ljava/lang/Object;)Lio/reactivex/ObservableOperator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/functions/Function<",
            "Landroid/database/Cursor;",
            "TT;>;TT;)",
            "Lio/reactivex/ObservableOperator<",
            "TT;",
            "Lcom/squareup/sqlbrite3/SqlBrite$Query;",
            ">;"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 152
    new-instance v0, Lcom/squareup/sqlbrite3/QueryToOneOperator;

    invoke-direct {v0, p0, p1}, Lcom/squareup/sqlbrite3/QueryToOneOperator;-><init>(Lio/reactivex/functions/Function;Ljava/lang/Object;)V

    return-object v0

    .line 151
    :cond_0
    new-instance p0, Ljava/lang/NullPointerException;

    const-string p1, "defaultValue == null"

    invoke-direct {p0, p1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static mapToOptional(Lio/reactivex/functions/Function;)Lio/reactivex/ObservableOperator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/functions/Function<",
            "Landroid/database/Cursor;",
            "TT;>;)",
            "Lio/reactivex/ObservableOperator<",
            "Ljava/util/Optional<",
            "TT;>;",
            "Lcom/squareup/sqlbrite3/SqlBrite$Query;",
            ">;"
        }
    .end annotation

    .line 171
    new-instance v0, Lcom/squareup/sqlbrite3/QueryToOptionalOperator;

    invoke-direct {v0, p0}, Lcom/squareup/sqlbrite3/QueryToOptionalOperator;-><init>(Lio/reactivex/functions/Function;)V

    return-object v0
.end method


# virtual methods
.method public final asRows(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/functions/Function<",
            "Landroid/database/Cursor;",
            "TT;>;)",
            "Lio/reactivex/Observable<",
            "TT;>;"
        }
    .end annotation

    .line 229
    new-instance v0, Lcom/squareup/sqlbrite3/SqlBrite$Query$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/sqlbrite3/SqlBrite$Query$1;-><init>(Lcom/squareup/sqlbrite3/SqlBrite$Query;Lio/reactivex/functions/Function;)V

    invoke-static {v0}, Lio/reactivex/Observable;->create(Lio/reactivex/ObservableOnSubscribe;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public abstract run()Landroid/database/Cursor;
.end method
