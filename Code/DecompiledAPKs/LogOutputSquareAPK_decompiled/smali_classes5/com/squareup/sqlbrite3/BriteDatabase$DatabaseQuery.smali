.class final Lcom/squareup/sqlbrite3/BriteDatabase$DatabaseQuery;
.super Lcom/squareup/sqlbrite3/SqlBrite$Query;
.source "BriteDatabase.java"

# interfaces
.implements Lio/reactivex/functions/Function;
.implements Lio/reactivex/functions/Predicate;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sqlbrite3/BriteDatabase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "DatabaseQuery"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/sqlbrite3/SqlBrite$Query;",
        "Lio/reactivex/functions/Function<",
        "Ljava/util/Set<",
        "Ljava/lang/String;",
        ">;",
        "Lcom/squareup/sqlbrite3/SqlBrite$Query;",
        ">;",
        "Lio/reactivex/functions/Predicate<",
        "Ljava/util/Set<",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final query:Landroidx/sqlite/db/SupportSQLiteQuery;

.field private final tables:Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Iterable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/squareup/sqlbrite3/BriteDatabase;


# direct methods
.method constructor <init>(Lcom/squareup/sqlbrite3/BriteDatabase;Ljava/lang/Iterable;Landroidx/sqlite/db/SupportSQLiteQuery;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable<",
            "Ljava/lang/String;",
            ">;",
            "Landroidx/sqlite/db/SupportSQLiteQuery;",
            ")V"
        }
    .end annotation

    .line 780
    iput-object p1, p0, Lcom/squareup/sqlbrite3/BriteDatabase$DatabaseQuery;->this$0:Lcom/squareup/sqlbrite3/BriteDatabase;

    invoke-direct {p0}, Lcom/squareup/sqlbrite3/SqlBrite$Query;-><init>()V

    .line 781
    iput-object p2, p0, Lcom/squareup/sqlbrite3/BriteDatabase$DatabaseQuery;->tables:Ljava/lang/Iterable;

    .line 782
    iput-object p3, p0, Lcom/squareup/sqlbrite3/BriteDatabase$DatabaseQuery;->query:Landroidx/sqlite/db/SupportSQLiteQuery;

    return-void
.end method


# virtual methods
.method public apply(Ljava/util/Set;)Lcom/squareup/sqlbrite3/SqlBrite$Query;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/sqlbrite3/SqlBrite$Query;"
        }
    .end annotation

    return-object p0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 775
    check-cast p1, Ljava/util/Set;

    invoke-virtual {p0, p1}, Lcom/squareup/sqlbrite3/BriteDatabase$DatabaseQuery;->apply(Ljava/util/Set;)Lcom/squareup/sqlbrite3/SqlBrite$Query;

    move-result-object p1

    return-object p1
.end method

.method public run()Landroid/database/Cursor;
    .locals 5

    .line 786
    iget-object v0, p0, Lcom/squareup/sqlbrite3/BriteDatabase$DatabaseQuery;->this$0:Lcom/squareup/sqlbrite3/BriteDatabase;

    iget-object v0, v0, Lcom/squareup/sqlbrite3/BriteDatabase;->transactions:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 790
    iget-object v0, p0, Lcom/squareup/sqlbrite3/BriteDatabase$DatabaseQuery;->this$0:Lcom/squareup/sqlbrite3/BriteDatabase;

    invoke-virtual {v0}, Lcom/squareup/sqlbrite3/BriteDatabase;->getReadableDatabase()Landroidx/sqlite/db/SupportSQLiteDatabase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/sqlbrite3/BriteDatabase$DatabaseQuery;->query:Landroidx/sqlite/db/SupportSQLiteQuery;

    invoke-interface {v0, v1}, Landroidx/sqlite/db/SupportSQLiteDatabase;->query(Landroidx/sqlite/db/SupportSQLiteQuery;)Landroid/database/Cursor;

    move-result-object v0

    .line 792
    iget-object v1, p0, Lcom/squareup/sqlbrite3/BriteDatabase$DatabaseQuery;->this$0:Lcom/squareup/sqlbrite3/BriteDatabase;

    iget-boolean v1, v1, Lcom/squareup/sqlbrite3/BriteDatabase;->logging:Z

    if-eqz v1, :cond_0

    .line 793
    iget-object v1, p0, Lcom/squareup/sqlbrite3/BriteDatabase$DatabaseQuery;->this$0:Lcom/squareup/sqlbrite3/BriteDatabase;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/squareup/sqlbrite3/BriteDatabase$DatabaseQuery;->tables:Ljava/lang/Iterable;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/squareup/sqlbrite3/BriteDatabase$DatabaseQuery;->query:Landroidx/sqlite/db/SupportSQLiteQuery;

    invoke-interface {v4}, Landroidx/sqlite/db/SupportSQLiteQuery;->getSql()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/squareup/sqlbrite3/BriteDatabase;->indentSql(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const-string v3, "QUERY\n  tables: %s\n  sql: %s"

    invoke-virtual {v1, v3, v2}, Lcom/squareup/sqlbrite3/BriteDatabase;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    return-object v0

    .line 787
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot execute observable query in a transaction."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public bridge synthetic test(Ljava/lang/Object;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 775
    check-cast p1, Ljava/util/Set;

    invoke-virtual {p0, p1}, Lcom/squareup/sqlbrite3/BriteDatabase$DatabaseQuery;->test(Ljava/util/Set;)Z

    move-result p1

    return p1
.end method

.method public test(Ljava/util/Set;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .line 808
    iget-object v0, p0, Lcom/squareup/sqlbrite3/BriteDatabase$DatabaseQuery;->tables:Ljava/lang/Iterable;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 809
    invoke-interface {p1, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 800
    iget-object v0, p0, Lcom/squareup/sqlbrite3/BriteDatabase$DatabaseQuery;->query:Landroidx/sqlite/db/SupportSQLiteQuery;

    invoke-interface {v0}, Landroidx/sqlite/db/SupportSQLiteQuery;->getSql()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
