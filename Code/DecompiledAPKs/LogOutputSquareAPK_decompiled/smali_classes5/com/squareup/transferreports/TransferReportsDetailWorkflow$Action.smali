.class public abstract Lcom/squareup/transferreports/TransferReportsDetailWorkflow$Action;
.super Ljava/lang/Object;
.source "TransferReportsDetailWorkflow.kt"

# interfaces
.implements Lcom/squareup/workflow/WorkflowAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/transferreports/TransferReportsDetailWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Action"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/transferreports/TransferReportsDetailWorkflow$Action$LoadBillEntries;,
        Lcom/squareup/transferreports/TransferReportsDetailWorkflow$Action$ShowTransferReportsDetail;,
        Lcom/squareup/transferreports/TransferReportsDetailWorkflow$Action$Finish;,
        Lcom/squareup/transferreports/TransferReportsDetailWorkflow$Action$ShowFeeDetailDialog;,
        Lcom/squareup/transferreports/TransferReportsDetailWorkflow$Action$CheckButton;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/transferreports/TransferReportsDetailState;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0005\u0007\u0008\t\n\u000bB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0004J\u0018\u0010\u0005\u001a\u00020\u0003*\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0006H\u0016\u0082\u0001\u0005\u000c\r\u000e\u000f\u0010\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/transferreports/TransferReportsDetailWorkflow$Action;",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/transferreports/TransferReportsDetailState;",
        "",
        "()V",
        "apply",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "CheckButton",
        "Finish",
        "LoadBillEntries",
        "ShowFeeDetailDialog",
        "ShowTransferReportsDetail",
        "Lcom/squareup/transferreports/TransferReportsDetailWorkflow$Action$LoadBillEntries;",
        "Lcom/squareup/transferreports/TransferReportsDetailWorkflow$Action$ShowTransferReportsDetail;",
        "Lcom/squareup/transferreports/TransferReportsDetailWorkflow$Action$Finish;",
        "Lcom/squareup/transferreports/TransferReportsDetailWorkflow$Action$ShowFeeDetailDialog;",
        "Lcom/squareup/transferreports/TransferReportsDetailWorkflow$Action$CheckButton;",
        "transfer-reports_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 160
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 160
    invoke-direct {p0}, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$Action;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;
    .locals 0

    .line 160
    invoke-virtual {p0, p1}, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$Action;->apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lkotlin/Unit;

    move-result-object p1

    return-object p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lkotlin/Unit;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/transferreports/TransferReportsDetailState;",
            ">;)",
            "Lkotlin/Unit;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Implement Updater.apply"
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 160
    invoke-static {p0, p1}, Lcom/squareup/workflow/WorkflowAction$DefaultImpls;->apply(Lcom/squareup/workflow/WorkflowAction;Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/Unit;

    return-object p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/transferreports/TransferReportsDetailState;",
            "-",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 162
    instance-of v0, p0, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$Action$LoadBillEntries;

    if-eqz v0, :cond_0

    .line 163
    new-instance v0, Lcom/squareup/transferreports/TransferReportsDetailState$LoadingBillEntries;

    move-object v1, p0

    check-cast v1, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$Action$LoadBillEntries;

    invoke-virtual {v1}, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$Action$LoadBillEntries;->getReportsSnapshot()Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/transferreports/TransferReportsDetailState$LoadingBillEntries;-><init>(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    goto :goto_0

    .line 165
    :cond_0
    instance-of v0, p0, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$Action$ShowTransferReportsDetail;

    if-eqz v0, :cond_1

    .line 166
    new-instance v0, Lcom/squareup/transferreports/TransferReportsDetailState$ShowingTransferReportsDetail;

    .line 167
    move-object v1, p0

    check-cast v1, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$Action$ShowTransferReportsDetail;

    invoke-virtual {v1}, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$Action$ShowTransferReportsDetail;->getReportsSnapshot()Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    move-result-object v1

    .line 168
    sget-object v2, Lcom/squareup/transferreports/ButtonType;->COLLECTED:Lcom/squareup/transferreports/ButtonType;

    .line 166
    invoke-direct {v0, v1, v2}, Lcom/squareup/transferreports/TransferReportsDetailState$ShowingTransferReportsDetail;-><init>(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;Lcom/squareup/transferreports/ButtonType;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    goto :goto_0

    .line 171
    :cond_1
    sget-object v0, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$Action$Finish;->INSTANCE:Lcom/squareup/transferreports/TransferReportsDetailWorkflow$Action$Finish;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    goto :goto_0

    .line 172
    :cond_2
    instance-of v0, p0, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$Action$ShowFeeDetailDialog;

    if-eqz v0, :cond_3

    .line 173
    new-instance v0, Lcom/squareup/transferreports/TransferReportsDetailState$ShowingFeeDetailDialog;

    move-object v1, p0

    check-cast v1, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$Action$ShowFeeDetailDialog;

    invoke-virtual {v1}, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$Action$ShowFeeDetailDialog;->getReportsSnapshot()Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/transferreports/TransferReportsDetailState$ShowingFeeDetailDialog;-><init>(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    goto :goto_0

    .line 175
    :cond_3
    instance-of v0, p0, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$Action$CheckButton;

    if-eqz v0, :cond_5

    .line 176
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Updater;->getNextState()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    check-cast v0, Lcom/squareup/transferreports/TransferReportsDetailState$ShowingTransferReportsDetail;

    .line 177
    move-object v1, p0

    check-cast v1, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$Action$CheckButton;

    invoke-virtual {v1}, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$Action$CheckButton;->getButtonType()Lcom/squareup/transferreports/ButtonType;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 176
    invoke-static {v0, v3, v1, v2, v3}, Lcom/squareup/transferreports/TransferReportsDetailState$ShowingTransferReportsDetail;->copy$default(Lcom/squareup/transferreports/TransferReportsDetailState$ShowingTransferReportsDetail;Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;Lcom/squareup/transferreports/ButtonType;ILjava/lang/Object;)Lcom/squareup/transferreports/TransferReportsDetailState$ShowingTransferReportsDetail;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_4
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.transferreports.TransferReportsDetailState.ShowingTransferReportsDetail"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_5
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
