.class public final Lcom/squareup/transferreports/TransferReportsViewFactory_Factory_Factory;
.super Ljava/lang/Object;
.source "TransferReportsViewFactory_Factory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/transferreports/TransferReportsViewFactory$Factory;",
        ">;"
    }
.end annotation


# instance fields
.field private final transferReportsDetailLayoutRunnerFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final transferReportsLayoutRunnerFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/transferreports/TransferReportsLayoutRunner$Factory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/transferreports/TransferReportsLayoutRunner$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$Factory;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/transferreports/TransferReportsViewFactory_Factory_Factory;->transferReportsLayoutRunnerFactoryProvider:Ljavax/inject/Provider;

    .line 24
    iput-object p2, p0, Lcom/squareup/transferreports/TransferReportsViewFactory_Factory_Factory;->transferReportsDetailLayoutRunnerFactoryProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/transferreports/TransferReportsViewFactory_Factory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/transferreports/TransferReportsLayoutRunner$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$Factory;",
            ">;)",
            "Lcom/squareup/transferreports/TransferReportsViewFactory_Factory_Factory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/transferreports/TransferReportsViewFactory_Factory_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/transferreports/TransferReportsViewFactory_Factory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/transferreports/TransferReportsLayoutRunner$Factory;Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$Factory;)Lcom/squareup/transferreports/TransferReportsViewFactory$Factory;
    .locals 1

    .line 41
    new-instance v0, Lcom/squareup/transferreports/TransferReportsViewFactory$Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/transferreports/TransferReportsViewFactory$Factory;-><init>(Lcom/squareup/transferreports/TransferReportsLayoutRunner$Factory;Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$Factory;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/transferreports/TransferReportsViewFactory$Factory;
    .locals 2

    .line 29
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsViewFactory_Factory_Factory;->transferReportsLayoutRunnerFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/transferreports/TransferReportsLayoutRunner$Factory;

    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsViewFactory_Factory_Factory;->transferReportsDetailLayoutRunnerFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$Factory;

    invoke-static {v0, v1}, Lcom/squareup/transferreports/TransferReportsViewFactory_Factory_Factory;->newInstance(Lcom/squareup/transferreports/TransferReportsLayoutRunner$Factory;Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$Factory;)Lcom/squareup/transferreports/TransferReportsViewFactory$Factory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/transferreports/TransferReportsViewFactory_Factory_Factory;->get()Lcom/squareup/transferreports/TransferReportsViewFactory$Factory;

    move-result-object v0

    return-object v0
.end method
