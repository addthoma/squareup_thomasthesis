.class public final synthetic Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I

.field public static final synthetic $EnumSwitchMapping$1:[I

.field public static final synthetic $EnumSwitchMapping$2:[I

.field public static final synthetic $EnumSwitchMapping$3:[I

.field public static final synthetic $EnumSwitchMapping$4:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 11

    invoke-static {}, Lcom/squareup/transferreports/TransferReportsLoader$State;->values()[Lcom/squareup/transferreports/TransferReportsLoader$State;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/transferreports/TransferReportsLoader$State;->LOADING:Lcom/squareup/transferreports/TransferReportsLoader$State;

    invoke-virtual {v1}, Lcom/squareup/transferreports/TransferReportsLoader$State;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/transferreports/TransferReportsLoader$State;->SUCCESS:Lcom/squareup/transferreports/TransferReportsLoader$State;

    invoke-virtual {v1}, Lcom/squareup/transferreports/TransferReportsLoader$State;->ordinal()I

    move-result v1

    const/4 v3, 0x2

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/transferreports/TransferReportsLoader$State;->FAILURE:Lcom/squareup/transferreports/TransferReportsLoader$State;

    invoke-virtual {v1}, Lcom/squareup/transferreports/TransferReportsLoader$State;->ordinal()I

    move-result v1

    const/4 v4, 0x3

    aput v4, v0, v1

    invoke-static {}, Lcom/squareup/transferreports/TransferReportsLoader$DepositType;->values()[Lcom/squareup/transferreports/TransferReportsLoader$DepositType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/transferreports/TransferReportsLoader$DepositType;->ACTIVE_SALES:Lcom/squareup/transferreports/TransferReportsLoader$DepositType;

    invoke-virtual {v1}, Lcom/squareup/transferreports/TransferReportsLoader$DepositType;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/transferreports/TransferReportsLoader$DepositType;->PENDING_DEPOSIT:Lcom/squareup/transferreports/TransferReportsLoader$DepositType;

    invoke-virtual {v1}, Lcom/squareup/transferreports/TransferReportsLoader$DepositType;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/transferreports/TransferReportsLoader$DepositType;->PENDING_WITHDRAWAL:Lcom/squareup/transferreports/TransferReportsLoader$DepositType;

    invoke-virtual {v1}, Lcom/squareup/transferreports/TransferReportsLoader$DepositType;->ordinal()I

    move-result v1

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/transferreports/TransferReportsLoader$DepositType;->WITHDRAWAL:Lcom/squareup/transferreports/TransferReportsLoader$DepositType;

    invoke-virtual {v1}, Lcom/squareup/transferreports/TransferReportsLoader$DepositType;->ordinal()I

    move-result v1

    const/4 v5, 0x4

    aput v5, v0, v1

    sget-object v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/transferreports/TransferReportsLoader$DepositType;->NEXT_BUSINESS_DAY_DEPOSIT:Lcom/squareup/transferreports/TransferReportsLoader$DepositType;

    invoke-virtual {v1}, Lcom/squareup/transferreports/TransferReportsLoader$DepositType;->ordinal()I

    move-result v1

    const/4 v6, 0x5

    aput v6, v0, v1

    sget-object v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/transferreports/TransferReportsLoader$DepositType;->SAME_DAY_DEPOSIT:Lcom/squareup/transferreports/TransferReportsLoader$DepositType;

    invoke-virtual {v1}, Lcom/squareup/transferreports/TransferReportsLoader$DepositType;->ordinal()I

    move-result v1

    const/4 v7, 0x6

    aput v7, v0, v1

    sget-object v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/transferreports/TransferReportsLoader$DepositType;->INSTANT_DEPOSIT:Lcom/squareup/transferreports/TransferReportsLoader$DepositType;

    invoke-virtual {v1}, Lcom/squareup/transferreports/TransferReportsLoader$DepositType;->ordinal()I

    move-result v1

    const/4 v8, 0x7

    aput v8, v0, v1

    sget-object v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/transferreports/TransferReportsLoader$DepositType;->NO_DEPOSIT:Lcom/squareup/transferreports/TransferReportsLoader$DepositType;

    invoke-virtual {v1}, Lcom/squareup/transferreports/TransferReportsLoader$DepositType;->ordinal()I

    move-result v1

    const/16 v9, 0x8

    aput v9, v0, v1

    sget-object v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/transferreports/TransferReportsLoader$DepositType;->DEBIT_CARD_PAY_IN:Lcom/squareup/transferreports/TransferReportsLoader$DepositType;

    invoke-virtual {v1}, Lcom/squareup/transferreports/TransferReportsLoader$DepositType;->ordinal()I

    move-result v1

    const/16 v10, 0x9

    aput v10, v0, v1

    invoke-static {}, Lcom/squareup/transferreports/TransferReportsLoader$DepositType;->values()[Lcom/squareup/transferreports/TransferReportsLoader$DepositType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/transferreports/TransferReportsLoader$DepositType;->ACTIVE_SALES:Lcom/squareup/transferreports/TransferReportsLoader$DepositType;

    invoke-virtual {v1}, Lcom/squareup/transferreports/TransferReportsLoader$DepositType;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/transferreports/TransferReportsLoader$DepositType;->PENDING_DEPOSIT:Lcom/squareup/transferreports/TransferReportsLoader$DepositType;

    invoke-virtual {v1}, Lcom/squareup/transferreports/TransferReportsLoader$DepositType;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/transferreports/TransferReportsLoader$DepositType;->PENDING_WITHDRAWAL:Lcom/squareup/transferreports/TransferReportsLoader$DepositType;

    invoke-virtual {v1}, Lcom/squareup/transferreports/TransferReportsLoader$DepositType;->ordinal()I

    move-result v1

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/transferreports/TransferReportsLoader$DepositType;->WITHDRAWAL:Lcom/squareup/transferreports/TransferReportsLoader$DepositType;

    invoke-virtual {v1}, Lcom/squareup/transferreports/TransferReportsLoader$DepositType;->ordinal()I

    move-result v1

    aput v5, v0, v1

    sget-object v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/transferreports/TransferReportsLoader$DepositType;->NO_DEPOSIT:Lcom/squareup/transferreports/TransferReportsLoader$DepositType;

    invoke-virtual {v1}, Lcom/squareup/transferreports/TransferReportsLoader$DepositType;->ordinal()I

    move-result v1

    aput v6, v0, v1

    sget-object v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/transferreports/TransferReportsLoader$DepositType;->NEXT_BUSINESS_DAY_DEPOSIT:Lcom/squareup/transferreports/TransferReportsLoader$DepositType;

    invoke-virtual {v1}, Lcom/squareup/transferreports/TransferReportsLoader$DepositType;->ordinal()I

    move-result v1

    aput v7, v0, v1

    sget-object v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/transferreports/TransferReportsLoader$DepositType;->SAME_DAY_DEPOSIT:Lcom/squareup/transferreports/TransferReportsLoader$DepositType;

    invoke-virtual {v1}, Lcom/squareup/transferreports/TransferReportsLoader$DepositType;->ordinal()I

    move-result v1

    aput v8, v0, v1

    sget-object v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/transferreports/TransferReportsLoader$DepositType;->INSTANT_DEPOSIT:Lcom/squareup/transferreports/TransferReportsLoader$DepositType;

    invoke-virtual {v1}, Lcom/squareup/transferreports/TransferReportsLoader$DepositType;->ordinal()I

    move-result v1

    aput v9, v0, v1

    sget-object v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/transferreports/TransferReportsLoader$DepositType;->DEBIT_CARD_PAY_IN:Lcom/squareup/transferreports/TransferReportsLoader$DepositType;

    invoke-virtual {v1}, Lcom/squareup/transferreports/TransferReportsLoader$DepositType;->ordinal()I

    move-result v1

    aput v10, v0, v1

    invoke-static {}, Lcom/squareup/transferreports/ButtonType;->values()[Lcom/squareup/transferreports/ButtonType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v1, Lcom/squareup/transferreports/ButtonType;->COLLECTED:Lcom/squareup/transferreports/ButtonType;

    invoke-virtual {v1}, Lcom/squareup/transferreports/ButtonType;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v1, Lcom/squareup/transferreports/ButtonType;->NET_TOTAL:Lcom/squareup/transferreports/ButtonType;

    invoke-virtual {v1}, Lcom/squareup/transferreports/ButtonType;->ordinal()I

    move-result v1

    aput v3, v0, v1

    invoke-static {}, Lcom/squareup/transferreports/ButtonType;->values()[Lcom/squareup/transferreports/ButtonType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v1, Lcom/squareup/transferreports/ButtonType;->COLLECTED:Lcom/squareup/transferreports/ButtonType;

    invoke-virtual {v1}, Lcom/squareup/transferreports/ButtonType;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v1, Lcom/squareup/transferreports/ButtonType;->NET_TOTAL:Lcom/squareup/transferreports/ButtonType;

    invoke-virtual {v1}, Lcom/squareup/transferreports/ButtonType;->ordinal()I

    move-result v1

    aput v3, v0, v1

    return-void
.end method
