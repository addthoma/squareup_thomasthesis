.class final Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$onSuccess$1;
.super Lkotlin/jvm/internal/Lambda;
.source "TransferReportsDetailLayoutRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->onSuccess(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/cycler/Update<",
        "Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u0008\u0012\u0004\u0012\u00020\u00030\u0002H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/cycler/Update;",
        "Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $showLoadMore:Z

.field final synthetic this$0:Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;


# direct methods
.method constructor <init>(Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;Z)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$onSuccess$1;->this$0:Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;

    iput-boolean p2, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$onSuccess$1;->$showLoadMore:Z

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 106
    check-cast p1, Lcom/squareup/cycler/Update;

    invoke-virtual {p0, p1}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$onSuccess$1;->invoke(Lcom/squareup/cycler/Update;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/cycler/Update;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/Update<",
            "Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 283
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$onSuccess$1;->this$0:Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;

    invoke-static {v0}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->access$getRows$p(Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->toList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    .line 284
    invoke-static {v0}, Lcom/squareup/cycler/DataSourceKt;->toDataSource(Ljava/util/List;)Lcom/squareup/cycler/DataSource;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/cycler/Update;->setData(Lcom/squareup/cycler/DataSource;)V

    .line 285
    iget-boolean v0, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$onSuccess$1;->$showLoadMore:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$LoadMore;->INSTANCE:Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$ReportsRow$LoadMore;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Lcom/squareup/cycler/Update;->setExtraItem(Ljava/lang/Object;)V

    return-void
.end method
