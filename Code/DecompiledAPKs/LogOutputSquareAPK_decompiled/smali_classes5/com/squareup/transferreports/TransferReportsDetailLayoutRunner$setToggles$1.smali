.class final Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$setToggles$1;
.super Ljava/lang/Object;
.source "TransferReportsDetailLayoutRunner.kt"

# interfaces
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->setToggles(Landroid/widget/RadioGroup;Lcom/squareup/transferreports/ButtonType;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u0006\u0010\u0005\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "<anonymous parameter 0>",
        "Landroid/widget/RadioGroup;",
        "kotlin.jvm.PlatformType",
        "checkedId",
        "",
        "onCheckedChanged"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $toggles:Landroid/widget/RadioGroup;

.field final synthetic this$0:Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;


# direct methods
.method constructor <init>(Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;Landroid/widget/RadioGroup;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$setToggles$1;->this$0:Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;

    iput-object p2, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$setToggles$1;->$toggles:Landroid/widget/RadioGroup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 0

    .line 581
    sget p1, Lcom/squareup/transferreports/R$id;->deposits_report_detail_collected:I

    if-ne p2, p1, :cond_0

    .line 582
    iget-object p1, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$setToggles$1;->this$0:Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;

    invoke-static {p1}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->access$getScreen$p(Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;)Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->getOnCheck()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    sget-object p2, Lcom/squareup/transferreports/ButtonType;->COLLECTED:Lcom/squareup/transferreports/ButtonType;

    invoke-interface {p1, p2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 584
    :cond_0
    sget p1, Lcom/squareup/transferreports/R$id;->deposits_report_detail_net_total:I

    if-ne p2, p1, :cond_1

    .line 585
    iget-object p1, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$setToggles$1;->this$0:Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;

    invoke-static {p1}, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;->access$getScreen$p(Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner;)Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/transferreports/TransferReportsDetailScreen$TransferReportsDetailContent;->getOnCheck()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    sget-object p2, Lcom/squareup/transferreports/ButtonType;->NET_TOTAL:Lcom/squareup/transferreports/ButtonType;

    invoke-interface {p1, p2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 589
    :goto_0
    iget-object p1, p0, Lcom/squareup/transferreports/TransferReportsDetailLayoutRunner$setToggles$1;->$toggles:Landroid/widget/RadioGroup;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    return-void

    .line 587
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Unexpected checkedId"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
