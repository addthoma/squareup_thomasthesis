.class public final Lcom/squareup/transferreports/TransferReportsDetailWorkflow$GetTransferDetailsWorker;
.super Lcom/squareup/workflow/rx2/PublisherWorker;
.source "TransferReportsDetailWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/transferreports/TransferReportsDetailWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "GetTransferDetailsWorker"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/rx2/PublisherWorker<",
        "Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0080\u0004\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0014\u0010\u0008\u001a\u00020\t2\n\u0010\n\u001a\u0006\u0012\u0002\u0008\u00030\u000bH\u0016J\u0010\u0010\u000c\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00020\rH\u0016R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/transferreports/TransferReportsDetailWorkflow$GetTransferDetailsWorker;",
        "Lcom/squareup/workflow/rx2/PublisherWorker;",
        "Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
        "props",
        "Lcom/squareup/transferreports/TransferReportsProps$TransferReportsDetailProps;",
        "(Lcom/squareup/transferreports/TransferReportsDetailWorkflow;Lcom/squareup/transferreports/TransferReportsProps$TransferReportsDetailProps;)V",
        "getProps",
        "()Lcom/squareup/transferreports/TransferReportsProps$TransferReportsDetailProps;",
        "doesSameWorkAs",
        "",
        "otherWorker",
        "Lcom/squareup/workflow/Worker;",
        "runPublisher",
        "Lorg/reactivestreams/Publisher;",
        "transfer-reports_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final props:Lcom/squareup/transferreports/TransferReportsProps$TransferReportsDetailProps;

.field final synthetic this$0:Lcom/squareup/transferreports/TransferReportsDetailWorkflow;


# direct methods
.method public constructor <init>(Lcom/squareup/transferreports/TransferReportsDetailWorkflow;Lcom/squareup/transferreports/TransferReportsProps$TransferReportsDetailProps;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/transferreports/TransferReportsProps$TransferReportsDetailProps;",
            ")V"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 132
    iput-object p1, p0, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$GetTransferDetailsWorker;->this$0:Lcom/squareup/transferreports/TransferReportsDetailWorkflow;

    .line 134
    invoke-direct {p0}, Lcom/squareup/workflow/rx2/PublisherWorker;-><init>()V

    iput-object p2, p0, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$GetTransferDetailsWorker;->props:Lcom/squareup/transferreports/TransferReportsProps$TransferReportsDetailProps;

    return-void
.end method


# virtual methods
.method public doesSameWorkAs(Lcom/squareup/workflow/Worker;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Worker<",
            "*>;)Z"
        }
    .end annotation

    const-string v0, "otherWorker"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 143
    instance-of v0, p1, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$GetTransferDetailsWorker;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$GetTransferDetailsWorker;

    iget-object p1, p1, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$GetTransferDetailsWorker;->props:Lcom/squareup/transferreports/TransferReportsProps$TransferReportsDetailProps;

    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$GetTransferDetailsWorker;->props:Lcom/squareup/transferreports/TransferReportsProps$TransferReportsDetailProps;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final getProps()Lcom/squareup/transferreports/TransferReportsProps$TransferReportsDetailProps;
    .locals 1

    .line 133
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$GetTransferDetailsWorker;->props:Lcom/squareup/transferreports/TransferReportsProps$TransferReportsDetailProps;

    return-object v0
.end method

.method public runPublisher()Lorg/reactivestreams/Publisher;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/reactivestreams/Publisher<",
            "+",
            "Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
            ">;"
        }
    .end annotation

    .line 136
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$GetTransferDetailsWorker;->this$0:Lcom/squareup/transferreports/TransferReportsDetailWorkflow;

    invoke-static {v0}, Lcom/squareup/transferreports/TransferReportsDetailWorkflow;->access$getTransferReportsLoader$p(Lcom/squareup/transferreports/TransferReportsDetailWorkflow;)Lcom/squareup/transferreports/TransferReportsLoader;

    move-result-object v0

    .line 137
    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$GetTransferDetailsWorker;->props:Lcom/squareup/transferreports/TransferReportsProps$TransferReportsDetailProps;

    invoke-virtual {v1}, Lcom/squareup/transferreports/TransferReportsProps$TransferReportsDetailProps;->getReportsSnapshot()Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$GetTransferDetailsWorker;->props:Lcom/squareup/transferreports/TransferReportsProps$TransferReportsDetailProps;

    invoke-virtual {v2}, Lcom/squareup/transferreports/TransferReportsProps$TransferReportsDetailProps;->getDepositType()Lcom/squareup/transferreports/TransferReportsLoader$DepositType;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$GetTransferDetailsWorker;->props:Lcom/squareup/transferreports/TransferReportsProps$TransferReportsDetailProps;

    invoke-virtual {v3}, Lcom/squareup/transferreports/TransferReportsProps$TransferReportsDetailProps;->getSettlementReport()Lcom/squareup/protos/client/settlements/SettlementReportWrapper;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$GetTransferDetailsWorker;->props:Lcom/squareup/transferreports/TransferReportsProps$TransferReportsDetailProps;

    invoke-virtual {v4}, Lcom/squareup/transferreports/TransferReportsProps$TransferReportsDetailProps;->getSettlementToken()Ljava/lang/String;

    move-result-object v4

    .line 136
    invoke-interface {v0, v1, v2, v3, v4}, Lcom/squareup/transferreports/TransferReportsLoader;->getTransferDetails(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;Lcom/squareup/transferreports/TransferReportsLoader$DepositType;Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v0

    .line 139
    invoke-virtual {v0}, Lio/reactivex/Single;->toFlowable()Lio/reactivex/Flowable;

    move-result-object v0

    const-string/jumbo v1, "transferReportsLoader.ge\u2026)\n          .toFlowable()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lorg/reactivestreams/Publisher;

    return-object v0
.end method
