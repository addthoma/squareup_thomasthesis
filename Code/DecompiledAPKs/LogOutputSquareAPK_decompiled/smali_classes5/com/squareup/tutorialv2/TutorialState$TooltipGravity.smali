.class public final enum Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;
.super Ljava/lang/Enum;
.source "TutorialState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tutorialv2/TutorialState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TooltipGravity"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\u0005\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0000j\u0002\u0008\u0004j\u0002\u0008\u0005\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;",
        "",
        "(Ljava/lang/String;I)V",
        "opposite",
        "TOOLTIP_BELOW_ANCHOR",
        "TOOLTIP_ABOVE_ANCHOR",
        "tutorial_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;

.field public static final enum TOOLTIP_ABOVE_ANCHOR:Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;

.field public static final enum TOOLTIP_BELOW_ANCHOR:Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;

    new-instance v1, Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;

    const/4 v2, 0x0

    const-string v3, "TOOLTIP_BELOW_ANCHOR"

    invoke-direct {v1, v3, v2}, Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;->TOOLTIP_BELOW_ANCHOR:Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;

    const/4 v2, 0x1

    const-string v3, "TOOLTIP_ABOVE_ANCHOR"

    invoke-direct {v1, v3, v2}, Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;->TOOLTIP_ABOVE_ANCHOR:Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;->$VALUES:[Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 156
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;
    .locals 1

    const-class v0, Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;

    return-object p0
.end method

.method public static values()[Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;
    .locals 1

    sget-object v0, Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;->$VALUES:[Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;

    invoke-virtual {v0}, [Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;

    return-object v0
.end method


# virtual methods
.method public final opposite()Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;
    .locals 2

    .line 167
    move-object v0, p0

    check-cast v0, Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;

    sget-object v1, Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;->TOOLTIP_ABOVE_ANCHOR:Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;

    if-ne v0, v1, :cond_0

    sget-object v1, Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;->TOOLTIP_BELOW_ANCHOR:Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;

    :cond_0
    return-object v1
.end method
