.class public final Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;
.super Ljava/lang/Object;
.source "TutorialV2DialogScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tutorialv2/TutorialV2DialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Prompt"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u001b\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B?\u0012\u0006\u0010\u0002\u001a\u00020\u0001\u0012\u0008\u0008\u0001\u0010\u0003\u001a\u00020\u0004\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0008\u0008\u0003\u0010\u0006\u001a\u00020\u0004\u0012\u0008\u0008\u0003\u0010\u0007\u001a\u00020\u0004\u0012\u0008\u0008\u0001\u0010\u0008\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\tJ\u000e\u0010\u0012\u001a\u00020\u0001H\u00c0\u0003\u00a2\u0006\u0002\u0008\u0013J\u000e\u0010\u0014\u001a\u00020\u0004H\u00c0\u0003\u00a2\u0006\u0002\u0008\u0015J\u000e\u0010\u0016\u001a\u00020\u0004H\u00c0\u0003\u00a2\u0006\u0002\u0008\u0017J\u000e\u0010\u0018\u001a\u00020\u0004H\u00c0\u0003\u00a2\u0006\u0002\u0008\u0019J\u000e\u0010\u001a\u001a\u00020\u0004H\u00c0\u0003\u00a2\u0006\u0002\u0008\u001bJ\u000e\u0010\u001c\u001a\u00020\u0004H\u00c0\u0003\u00a2\u0006\u0002\u0008\u001dJE\u0010\u001e\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00012\u0008\u0008\u0003\u0010\u0003\u001a\u00020\u00042\u0008\u0008\u0003\u0010\u0005\u001a\u00020\u00042\u0008\u0008\u0003\u0010\u0006\u001a\u00020\u00042\u0008\u0008\u0003\u0010\u0007\u001a\u00020\u00042\u0008\u0008\u0003\u0010\u0008\u001a\u00020\u0004H\u00c6\u0001J\u0013\u0010\u001f\u001a\u00020 2\u0008\u0010!\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\"\u001a\u00020\u0004H\u00d6\u0001J\t\u0010#\u001a\u00020$H\u00d6\u0001R\u0014\u0010\u0005\u001a\u00020\u0004X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0014\u0010\u0008\u001a\u00020\u0004X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\u000bR\u0014\u0010\u0002\u001a\u00020\u0001X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0014\u0010\u0006\u001a\u00020\u0004X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u000bR\u0014\u0010\u0007\u001a\u00020\u0004X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u000bR\u0014\u0010\u0003\u001a\u00020\u0004X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u000b\u00a8\u0006%"
    }
    d2 = {
        "Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;",
        "",
        "key",
        "titleId",
        "",
        "contentId",
        "primaryButtonId",
        "secondaryButtonId",
        "iconId",
        "(Ljava/lang/Object;IIIII)V",
        "getContentId$tutorial_release",
        "()I",
        "getIconId$tutorial_release",
        "getKey$tutorial_release",
        "()Ljava/lang/Object;",
        "getPrimaryButtonId$tutorial_release",
        "getSecondaryButtonId$tutorial_release",
        "getTitleId$tutorial_release",
        "component1",
        "component1$tutorial_release",
        "component2",
        "component2$tutorial_release",
        "component3",
        "component3$tutorial_release",
        "component4",
        "component4$tutorial_release",
        "component5",
        "component5$tutorial_release",
        "component6",
        "component6$tutorial_release",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "toString",
        "",
        "tutorial_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final contentId:I

.field private final iconId:I

.field private final key:Ljava/lang/Object;

.field private final primaryButtonId:I

.field private final secondaryButtonId:I

.field private final titleId:I


# direct methods
.method public constructor <init>(Ljava/lang/Object;IIIII)V
    .locals 1

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->key:Ljava/lang/Object;

    iput p2, p0, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->titleId:I

    iput p3, p0, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->contentId:I

    iput p4, p0, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->primaryButtonId:I

    iput p5, p0, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->secondaryButtonId:I

    iput p6, p0, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->iconId:I

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/Object;IIIIIILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 7

    and-int/lit8 p8, p7, 0x8

    if-eqz p8, :cond_0

    .line 39
    sget p4, Lcom/squareup/common/strings/R$string;->done:I

    :cond_0
    move v4, p4

    and-int/lit8 p4, p7, 0x10

    if-eqz p4, :cond_1

    const/4 p5, -0x1

    const/4 v5, -0x1

    goto :goto_0

    :cond_1
    move v5, p5

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v6, p6

    .line 41
    invoke-direct/range {v0 .. v6}, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;-><init>(Ljava/lang/Object;IIIII)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;Ljava/lang/Object;IIIIIILjava/lang/Object;)Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    iget-object p1, p0, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->key:Ljava/lang/Object;

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    iget p2, p0, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->titleId:I

    :cond_1
    move p8, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    iget p3, p0, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->contentId:I

    :cond_2
    move v0, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    iget p4, p0, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->primaryButtonId:I

    :cond_3
    move v1, p4

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    iget p5, p0, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->secondaryButtonId:I

    :cond_4
    move v2, p5

    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_5

    iget p6, p0, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->iconId:I

    :cond_5
    move v3, p6

    move-object p2, p0

    move-object p3, p1

    move p4, p8

    move p5, v0

    move p6, v1

    move p7, v2

    move p8, v3

    invoke-virtual/range {p2 .. p8}, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->copy(Ljava/lang/Object;IIIII)Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1$tutorial_release()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->key:Ljava/lang/Object;

    return-object v0
.end method

.method public final component2$tutorial_release()I
    .locals 1

    iget v0, p0, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->titleId:I

    return v0
.end method

.method public final component3$tutorial_release()I
    .locals 1

    iget v0, p0, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->contentId:I

    return v0
.end method

.method public final component4$tutorial_release()I
    .locals 1

    iget v0, p0, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->primaryButtonId:I

    return v0
.end method

.method public final component5$tutorial_release()I
    .locals 1

    iget v0, p0, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->secondaryButtonId:I

    return v0
.end method

.method public final component6$tutorial_release()I
    .locals 1

    iget v0, p0, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->iconId:I

    return v0
.end method

.method public final copy(Ljava/lang/Object;IIIII)Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;
    .locals 8

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;

    move-object v1, v0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;-><init>(Ljava/lang/Object;IIIII)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;

    iget-object v0, p0, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->key:Ljava/lang/Object;

    iget-object v1, p1, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->key:Ljava/lang/Object;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->titleId:I

    iget v1, p1, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->titleId:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->contentId:I

    iget v1, p1, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->contentId:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->primaryButtonId:I

    iget v1, p1, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->primaryButtonId:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->secondaryButtonId:I

    iget v1, p1, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->secondaryButtonId:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->iconId:I

    iget p1, p1, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->iconId:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getContentId$tutorial_release()I
    .locals 1

    .line 37
    iget v0, p0, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->contentId:I

    return v0
.end method

.method public final getIconId$tutorial_release()I
    .locals 1

    .line 43
    iget v0, p0, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->iconId:I

    return v0
.end method

.method public final getKey$tutorial_release()Ljava/lang/Object;
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->key:Ljava/lang/Object;

    return-object v0
.end method

.method public final getPrimaryButtonId$tutorial_release()I
    .locals 1

    .line 39
    iget v0, p0, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->primaryButtonId:I

    return v0
.end method

.method public final getSecondaryButtonId$tutorial_release()I
    .locals 1

    .line 41
    iget v0, p0, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->secondaryButtonId:I

    return v0
.end method

.method public final getTitleId$tutorial_release()I
    .locals 1

    .line 35
    iget v0, p0, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->titleId:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->key:Ljava/lang/Object;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->titleId:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->contentId:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->primaryButtonId:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->secondaryButtonId:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->iconId:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Prompt(key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->key:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", titleId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->titleId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", contentId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->contentId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", primaryButtonId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->primaryButtonId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", secondaryButtonId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->secondaryButtonId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", iconId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->iconId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
