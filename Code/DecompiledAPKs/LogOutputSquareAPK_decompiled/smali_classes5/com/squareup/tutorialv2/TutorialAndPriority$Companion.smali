.class public final Lcom/squareup/tutorialv2/TutorialAndPriority$Companion;
.super Ljava/lang/Object;
.source "TutorialAndPriority.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tutorialv2/TutorialAndPriority;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0005\u001a\u00020\u0006X\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008\"\u0004\u0008\t\u0010\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/tutorialv2/TutorialAndPriority$Companion;",
        "",
        "()V",
        "NO_TUTORIAL",
        "Lcom/squareup/tutorialv2/Tutorial;",
        "NO_TUTORIAL_PAIR",
        "Lcom/squareup/tutorialv2/TutorialAndPriority;",
        "getNO_TUTORIAL_PAIR$tutorial_release",
        "()Lcom/squareup/tutorialv2/TutorialAndPriority;",
        "setNO_TUTORIAL_PAIR$tutorial_release",
        "(Lcom/squareup/tutorialv2/TutorialAndPriority;)V",
        "tutorial_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 21
    invoke-direct {p0}, Lcom/squareup/tutorialv2/TutorialAndPriority$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final getNO_TUTORIAL_PAIR$tutorial_release()Lcom/squareup/tutorialv2/TutorialAndPriority;
    .locals 1

    .line 48
    invoke-static {}, Lcom/squareup/tutorialv2/TutorialAndPriority;->access$getNO_TUTORIAL_PAIR$cp()Lcom/squareup/tutorialv2/TutorialAndPriority;

    move-result-object v0

    return-object v0
.end method

.method public final setNO_TUTORIAL_PAIR$tutorial_release(Lcom/squareup/tutorialv2/TutorialAndPriority;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    invoke-static {p1}, Lcom/squareup/tutorialv2/TutorialAndPriority;->access$setNO_TUTORIAL_PAIR$cp(Lcom/squareup/tutorialv2/TutorialAndPriority;)V

    return-void
.end method
