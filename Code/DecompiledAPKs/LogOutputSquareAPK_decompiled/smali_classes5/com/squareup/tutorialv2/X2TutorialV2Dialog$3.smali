.class final Lcom/squareup/tutorialv2/X2TutorialV2Dialog$3;
.super Ljava/lang/Object;
.source "X2TutorialV2Dialog.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tutorialv2/X2TutorialV2Dialog;-><init>(Landroid/content/Context;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "run"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $core:Lcom/squareup/tutorialv2/TutorialCore;

.field final synthetic $prompt:Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;


# direct methods
.method constructor <init>(Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tutorialv2/X2TutorialV2Dialog$3;->$core:Lcom/squareup/tutorialv2/TutorialCore;

    iput-object p2, p0, Lcom/squareup/tutorialv2/X2TutorialV2Dialog$3;->$prompt:Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .line 40
    iget-object v0, p0, Lcom/squareup/tutorialv2/X2TutorialV2Dialog$3;->$core:Lcom/squareup/tutorialv2/TutorialCore;

    iget-object v1, p0, Lcom/squareup/tutorialv2/X2TutorialV2Dialog$3;->$prompt:Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;

    invoke-virtual {v1}, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->getKey$tutorial_release()Ljava/lang/Object;

    move-result-object v1

    const-string v2, "TutorialV2DialogScreen secondary tapped"

    invoke-interface {v0, v2, v1}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method
