.class public Lcom/squareup/tutorialv2/view/TutorialBannerView;
.super Landroidx/constraintlayout/widget/ConstraintLayout;
.source "TutorialBannerView.java"

# interfaces
.implements Lcom/squareup/tutorialv2/view/TutorialView;


# instance fields
.field private baseline:Landroid/view/View;

.field private closeButton:Lcom/squareup/glyph/SquareGlyphView;

.field private contentText:Landroid/widget/TextView;

.field private primaryButton:Lcom/squareup/marketfont/MarketButton;

.field private progressText:Landroid/widget/TextView;

.field private secondaryButton:Lcom/squareup/marketfont/MarketButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 37
    invoke-direct {p0, p1, p2}, Landroidx/constraintlayout/widget/ConstraintLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method static synthetic lambda$setOnInteractionListener$0(Lcom/squareup/tutorialv2/view/TutorialView$OnInteractionListener;)V
    .locals 0

    if-eqz p0, :cond_0

    .line 56
    invoke-interface {p0}, Lcom/squareup/tutorialv2/view/TutorialView$OnInteractionListener;->onDismissed()V

    :cond_0
    return-void
.end method

.method static synthetic lambda$setOnInteractionListener$1(Lcom/squareup/tutorialv2/view/TutorialView$OnInteractionListener;)V
    .locals 0

    .line 59
    invoke-interface {p0}, Lcom/squareup/tutorialv2/view/TutorialView$OnInteractionListener;->onTapped()V

    return-void
.end method

.method private resetBaselineVisibility()V
    .locals 5

    .line 125
    iget-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialBannerView;->progressText:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    const/4 v1, 0x1

    const/16 v2, 0x8

    const/4 v3, 0x0

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 126
    :goto_0
    iget-object v4, p0, Lcom/squareup/tutorialv2/view/TutorialBannerView;->primaryButton:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {v4}, Lcom/squareup/marketfont/MarketButton;->getVisibility()I

    move-result v4

    if-ne v4, v2, :cond_1

    iget-object v4, p0, Lcom/squareup/tutorialv2/view/TutorialBannerView;->secondaryButton:Lcom/squareup/marketfont/MarketButton;

    .line 127
    invoke-virtual {v4}, Lcom/squareup/marketfont/MarketButton;->getVisibility()I

    move-result v4

    if-ne v4, v2, :cond_1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    .line 130
    iget-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialBannerView;->baseline:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :cond_2
    const/4 v0, 0x4

    if-eqz v1, :cond_3

    .line 133
    iget-object v1, p0, Lcom/squareup/tutorialv2/view/TutorialBannerView;->baseline:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 134
    iget-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialBannerView;->baseline:Landroid/view/View;

    invoke-virtual {v0, v3, v3, v3, v3}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_2

    .line 137
    :cond_3
    iget-object v1, p0, Lcom/squareup/tutorialv2/view/TutorialBannerView;->baseline:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 138
    iget-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialBannerView;->baseline:Landroid/view/View;

    .line 139
    invoke-virtual {p0}, Lcom/squareup/tutorialv2/view/TutorialBannerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/noho/R$dimen;->noho_gap_20:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 138
    invoke-virtual {v0, v3, v1, v3, v3}, Landroid/view/View;->setPadding(IIII)V

    :goto_2
    return-void
.end method


# virtual methods
.method public hide()V
    .locals 1

    const/16 v0, 0x8

    .line 96
    invoke-virtual {p0, v0}, Lcom/squareup/tutorialv2/view/TutorialBannerView;->setVisibility(I)V

    return-void
.end method

.method public hidePrimaryButton()V
    .locals 2

    .line 107
    iget-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialBannerView;->primaryButton:Lcom/squareup/marketfont/MarketButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setVisibility(I)V

    .line 108
    invoke-direct {p0}, Lcom/squareup/tutorialv2/view/TutorialBannerView;->resetBaselineVisibility()V

    return-void
.end method

.method public hideSecondaryButton()V
    .locals 2

    .line 119
    iget-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialBannerView;->secondaryButton:Lcom/squareup/marketfont/MarketButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setVisibility(I)V

    .line 120
    invoke-direct {p0}, Lcom/squareup/tutorialv2/view/TutorialBannerView;->resetBaselineVisibility()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 41
    invoke-super {p0}, Landroidx/constraintlayout/widget/ConstraintLayout;->onFinishInflate()V

    .line 43
    sget v0, Lcom/squareup/common/tutorial/R$id;->tutorial2_content:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialBannerView;->contentText:Landroid/widget/TextView;

    .line 44
    sget v0, Lcom/squareup/common/tutorial/R$id;->tutorial2_progress_text:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialBannerView;->progressText:Landroid/widget/TextView;

    .line 45
    sget v0, Lcom/squareup/common/tutorial/R$id;->tutorial2_cancel:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialBannerView;->closeButton:Lcom/squareup/glyph/SquareGlyphView;

    .line 46
    sget v0, Lcom/squareup/common/tutorial/R$id;->tutorial2_primary_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialBannerView;->primaryButton:Lcom/squareup/marketfont/MarketButton;

    .line 47
    sget v0, Lcom/squareup/common/tutorial/R$id;->tutorial2_secondary_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialBannerView;->secondaryButton:Lcom/squareup/marketfont/MarketButton;

    .line 48
    sget v0, Lcom/squareup/common/tutorial/R$id;->tutorial2_baseline:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialBannerView;->baseline:Landroid/view/View;

    .line 50
    invoke-direct {p0}, Lcom/squareup/tutorialv2/view/TutorialBannerView;->resetBaselineVisibility()V

    return-void
.end method

.method public setCloseButtonColor(I)V
    .locals 2

    .line 70
    iget-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialBannerView;->closeButton:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {p0}, Lcom/squareup/tutorialv2/view/TutorialBannerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyphColor(I)V

    return-void
.end method

.method public setContentText(I)V
    .locals 1

    .line 78
    iget-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialBannerView;->contentText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method

.method public setContentText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialBannerView;->contentText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setOnInteractionListener(Lcom/squareup/tutorialv2/view/TutorialView$OnInteractionListener;)V
    .locals 2

    .line 54
    iget-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialBannerView;->closeButton:Lcom/squareup/glyph/SquareGlyphView;

    new-instance v1, Lcom/squareup/tutorialv2/view/-$$Lambda$TutorialBannerView$oYOGn_VTBXOtD7NZDYmG-SvmKXs;

    invoke-direct {v1, p1}, Lcom/squareup/tutorialv2/view/-$$Lambda$TutorialBannerView$oYOGn_VTBXOtD7NZDYmG-SvmKXs;-><init>(Lcom/squareup/tutorialv2/view/TutorialView$OnInteractionListener;)V

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounceRunnable(Ljava/lang/Runnable;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 59
    new-instance v0, Lcom/squareup/tutorialv2/view/-$$Lambda$TutorialBannerView$OFgsyp52IFh5xlFMCD1zY82fJb0;

    invoke-direct {v0, p1}, Lcom/squareup/tutorialv2/view/-$$Lambda$TutorialBannerView$OFgsyp52IFh5xlFMCD1zY82fJb0;-><init>(Lcom/squareup/tutorialv2/view/TutorialView$OnInteractionListener;)V

    invoke-static {v0}, Lcom/squareup/debounce/Debouncers;->debounceRunnable(Ljava/lang/Runnable;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/tutorialv2/view/TutorialBannerView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setPrimaryButton(ILandroid/view/View$OnClickListener;)V
    .locals 1

    .line 100
    iget-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialBannerView;->primaryButton:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketButton;->setText(I)V

    .line 101
    iget-object p1, p0, Lcom/squareup/tutorialv2/view/TutorialBannerView;->primaryButton:Lcom/squareup/marketfont/MarketButton;

    invoke-static {p2}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 102
    iget-object p1, p0, Lcom/squareup/tutorialv2/view/TutorialBannerView;->primaryButton:Lcom/squareup/marketfont/MarketButton;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lcom/squareup/marketfont/MarketButton;->setVisibility(I)V

    .line 103
    invoke-direct {p0}, Lcom/squareup/tutorialv2/view/TutorialBannerView;->resetBaselineVisibility()V

    return-void
.end method

.method public setSecondaryButton(ILandroid/view/View$OnClickListener;)V
    .locals 1

    .line 112
    iget-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialBannerView;->secondaryButton:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketButton;->setText(I)V

    .line 113
    iget-object p1, p0, Lcom/squareup/tutorialv2/view/TutorialBannerView;->secondaryButton:Lcom/squareup/marketfont/MarketButton;

    invoke-static {p2}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 114
    iget-object p1, p0, Lcom/squareup/tutorialv2/view/TutorialBannerView;->secondaryButton:Lcom/squareup/marketfont/MarketButton;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lcom/squareup/marketfont/MarketButton;->setVisibility(I)V

    .line 115
    invoke-direct {p0}, Lcom/squareup/tutorialv2/view/TutorialBannerView;->resetBaselineVisibility()V

    return-void
.end method

.method public setStepsText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 82
    iget-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialBannerView;->progressText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 83
    iget-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialBannerView;->progressText:Landroid/widget/TextView;

    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 84
    invoke-direct {p0}, Lcom/squareup/tutorialv2/view/TutorialBannerView;->resetBaselineVisibility()V

    return-void
.end method

.method public setViewBackgroundColor(I)V
    .locals 1

    .line 63
    invoke-virtual {p0}, Lcom/squareup/tutorialv2/view/TutorialBannerView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p1

    .line 64
    invoke-super {p0, p1}, Landroidx/constraintlayout/widget/ConstraintLayout;->setBackgroundColor(I)V

    .line 65
    iget-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialBannerView;->primaryButton:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketButton;->setTextColor(I)V

    .line 66
    iget-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialBannerView;->secondaryButton:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketButton;->setTextColor(I)V

    return-void
.end method

.method public show(Landroid/view/View;Lcom/squareup/tutorialv2/TutorialState$TooltipPosition;)V
    .locals 0

    const/4 p1, 0x0

    .line 92
    invoke-virtual {p0, p1}, Lcom/squareup/tutorialv2/view/TutorialBannerView;->setVisibility(I)V

    return-void
.end method
