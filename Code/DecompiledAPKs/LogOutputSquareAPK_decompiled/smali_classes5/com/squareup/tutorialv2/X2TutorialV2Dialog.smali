.class public final Lcom/squareup/tutorialv2/X2TutorialV2Dialog;
.super Lcom/squareup/sqwidgets/ConfirmDialog;
.source "X2TutorialV2Dialog.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/tutorialv2/X2TutorialV2Dialog;",
        "Lcom/squareup/sqwidgets/ConfirmDialog;",
        "context",
        "Landroid/content/Context;",
        "core",
        "Lcom/squareup/tutorialv2/TutorialCore;",
        "prompt",
        "Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;",
        "(Landroid/content/Context;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;)V",
        "tutorial_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "core"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "prompt"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0, p1}, Lcom/squareup/sqwidgets/ConfirmDialog;-><init>(Landroid/content/Context;)V

    .line 16
    invoke-virtual {p0}, Lcom/squareup/tutorialv2/X2TutorialV2Dialog;->showLogo()Lcom/squareup/sqwidgets/ConfirmDialog;

    .line 18
    invoke-virtual {p3}, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->getTitleId$tutorial_release()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/tutorialv2/X2TutorialV2Dialog;->setTitle(I)Lcom/squareup/sqwidgets/ConfirmDialog;

    .line 20
    invoke-virtual {p3}, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->getContentId$tutorial_release()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/tutorialv2/X2TutorialV2Dialog;->setMessage(I)Lcom/squareup/sqwidgets/ConfirmDialog;

    .line 22
    invoke-virtual {p3}, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->getSecondaryButtonId$tutorial_release()I

    move-result p1

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 25
    new-instance p1, Lcom/squareup/tutorialv2/X2TutorialV2Dialog$1;

    invoke-direct {p1, p2, p3}, Lcom/squareup/tutorialv2/X2TutorialV2Dialog$1;-><init>(Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;)V

    check-cast p1, Ljava/lang/Runnable;

    invoke-virtual {p0, p1}, Lcom/squareup/tutorialv2/X2TutorialV2Dialog;->setOnConfirmedBeforeDialogDismiss(Ljava/lang/Runnable;)Lcom/squareup/sqwidgets/ConfirmDialog;

    .line 28
    invoke-virtual {p3}, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->getPrimaryButtonId$tutorial_release()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/tutorialv2/X2TutorialV2Dialog;->setBlueConfirmButtonAndText(I)Lcom/squareup/sqwidgets/ConfirmDialog;

    .line 30
    invoke-virtual {p0}, Lcom/squareup/tutorialv2/X2TutorialV2Dialog;->hideCancelButton()Lcom/squareup/sqwidgets/ConfirmDialog;

    goto :goto_0

    .line 33
    :cond_0
    new-instance v0, Lcom/squareup/tutorialv2/X2TutorialV2Dialog$2;

    invoke-direct {v0, p2, p3}, Lcom/squareup/tutorialv2/X2TutorialV2Dialog$2;-><init>(Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;)V

    check-cast v0, Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/squareup/tutorialv2/X2TutorialV2Dialog;->setOnDismissedBeforeDialogDismiss(Ljava/lang/Runnable;)Lcom/squareup/sqwidgets/ConfirmDialog;

    .line 37
    invoke-virtual {p3}, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;->getPrimaryButtonId$tutorial_release()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/squareup/tutorialv2/X2TutorialV2Dialog;->setWhiteDismissButtonAndText(I)Lcom/squareup/sqwidgets/ConfirmDialog;

    .line 39
    new-instance v0, Lcom/squareup/tutorialv2/X2TutorialV2Dialog$3;

    invoke-direct {v0, p2, p3}, Lcom/squareup/tutorialv2/X2TutorialV2Dialog$3;-><init>(Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;)V

    check-cast v0, Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/squareup/tutorialv2/X2TutorialV2Dialog;->setOnConfirmedBeforeDialogDismiss(Ljava/lang/Runnable;)Lcom/squareup/sqwidgets/ConfirmDialog;

    .line 42
    invoke-virtual {p0, p1}, Lcom/squareup/tutorialv2/X2TutorialV2Dialog;->setBlueConfirmButtonAndText(I)Lcom/squareup/sqwidgets/ConfirmDialog;

    :goto_0
    return-void
.end method
