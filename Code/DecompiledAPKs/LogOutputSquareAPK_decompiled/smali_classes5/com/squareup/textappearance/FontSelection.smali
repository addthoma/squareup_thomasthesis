.class public final Lcom/squareup/textappearance/FontSelection;
.super Ljava/lang/Object;
.source "FontSelection.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/textappearance/FontSelection$Params;,
        Lcom/squareup/textappearance/FontSelection$ConcreteFontSpan;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001:\u0002\u0015\u0016B\u0019\u0008\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006B\u000f\u0008\u0012\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tB\u001d\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\u0005\u0012\u0006\u0010\r\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u000eJ\u0016\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0011\u001a\u00020\u0005J\u0016\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0014\u001a\u00020\u0013R\u000e\u0010\r\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/textappearance/FontSelection;",
        "",
        "context",
        "Landroid/content/Context;",
        "fontSelectionId",
        "",
        "(Landroid/content/Context;I)V",
        "all",
        "Lcom/squareup/textappearance/FontSelection$Params;",
        "(Lcom/squareup/textappearance/FontSelection$Params;)V",
        "fontFamily",
        "Lcom/squareup/textappearance/FontFamily;",
        "normalWeight",
        "boldWeight",
        "(Lcom/squareup/textappearance/FontFamily;II)V",
        "getFont",
        "Landroid/graphics/Typeface;",
        "style",
        "transform",
        "Landroid/text/Spannable;",
        "source",
        "ConcreteFontSpan",
        "Params",
        "textappearance_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final boldWeight:I

.field private final fontFamily:Lcom/squareup/textappearance/FontFamily;

.field private final normalWeight:I


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    new-instance v0, Lcom/squareup/textappearance/FontSelection$Params;

    invoke-direct {v0, p1, p2}, Lcom/squareup/textappearance/FontSelection$Params;-><init>(Landroid/content/Context;I)V

    .line 35
    invoke-direct {p0, v0}, Lcom/squareup/textappearance/FontSelection;-><init>(Lcom/squareup/textappearance/FontSelection$Params;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/textappearance/FontFamily;II)V
    .locals 1

    const-string v0, "fontFamily"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/textappearance/FontSelection;->fontFamily:Lcom/squareup/textappearance/FontFamily;

    iput p2, p0, Lcom/squareup/textappearance/FontSelection;->normalWeight:I

    iput p3, p0, Lcom/squareup/textappearance/FontSelection;->boldWeight:I

    return-void
.end method

.method private constructor <init>(Lcom/squareup/textappearance/FontSelection$Params;)V
    .locals 2

    .line 38
    invoke-virtual {p1}, Lcom/squareup/textappearance/FontSelection$Params;->getFontFamily()Lcom/squareup/textappearance/FontFamily;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/textappearance/FontSelection$Params;->getNormalWeight()I

    move-result v1

    invoke-virtual {p1}, Lcom/squareup/textappearance/FontSelection$Params;->getBoldWeight()I

    move-result p1

    invoke-direct {p0, v0, v1, p1}, Lcom/squareup/textappearance/FontSelection;-><init>(Lcom/squareup/textappearance/FontFamily;II)V

    return-void
.end method


# virtual methods
.method public final getFont(Landroid/content/Context;I)Landroid/graphics/Typeface;
    .locals 3

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    and-int/lit8 v0, p2, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    and-int/lit8 p2, p2, 0x2

    if-eqz p2, :cond_1

    const/4 v1, 0x1

    :cond_1
    if-eqz v0, :cond_2

    .line 59
    iget p2, p0, Lcom/squareup/textappearance/FontSelection;->boldWeight:I

    goto :goto_1

    :cond_2
    iget p2, p0, Lcom/squareup/textappearance/FontSelection;->normalWeight:I

    :goto_1
    if-eqz v1, :cond_3

    .line 60
    sget-object v0, Lcom/squareup/textappearance/Style;->ITALIC:Lcom/squareup/textappearance/Style;

    goto :goto_2

    :cond_3
    sget-object v0, Lcom/squareup/textappearance/Style;->NORMAL:Lcom/squareup/textappearance/Style;

    .line 61
    :goto_2
    iget-object v1, p0, Lcom/squareup/textappearance/FontSelection;->fontFamily:Lcom/squareup/textappearance/FontFamily;

    invoke-virtual {v1, p1, p2, v0}, Lcom/squareup/textappearance/FontFamily;->getFont(Landroid/content/Context;ILcom/squareup/textappearance/Style;)Landroid/graphics/Typeface;

    move-result-object p1

    return-object p1
.end method

.method public final transform(Landroid/content/Context;Landroid/text/Spannable;)Landroid/text/Spannable;
    .locals 10

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    new-instance v0, Landroid/text/SpannableStringBuilder;

    move-object v1, p2

    check-cast v1, Ljava/lang/CharSequence;

    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 72
    invoke-interface {p2}, Landroid/text/Spannable;->length()I

    move-result v1

    const-class v2, Landroid/text/style/StyleSpan;

    const/4 v3, 0x0

    invoke-interface {p2, v3, v1, v2}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Landroid/text/style/StyleSpan;

    .line 73
    array-length v2, v1

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v2, :cond_0

    aget-object v5, v1, v4

    .line 74
    invoke-interface {p2, v5}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v6

    .line 75
    invoke-interface {p2, v5}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v7

    .line 78
    invoke-virtual {v0, v5}, Landroid/text/SpannableStringBuilder;->removeSpan(Ljava/lang/Object;)V

    .line 79
    new-instance v8, Lcom/squareup/textappearance/FontSelection$ConcreteFontSpan;

    const-string v9, "span"

    .line 80
    invoke-static {v5, v9}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v5}, Landroid/text/style/StyleSpan;->getStyle()I

    move-result v5

    invoke-virtual {p0, p1, v5}, Lcom/squareup/textappearance/FontSelection;->getFont(Landroid/content/Context;I)Landroid/graphics/Typeface;

    move-result-object v5

    .line 79
    invoke-direct {v8, v5}, Lcom/squareup/textappearance/FontSelection$ConcreteFontSpan;-><init>(Landroid/graphics/Typeface;)V

    .line 82
    invoke-virtual {v0, v8, v6, v7, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 84
    :cond_0
    check-cast v0, Landroid/text/Spannable;

    return-object v0
.end method
