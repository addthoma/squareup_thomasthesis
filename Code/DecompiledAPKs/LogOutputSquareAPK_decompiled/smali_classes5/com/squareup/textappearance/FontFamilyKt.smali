.class public final Lcom/squareup/textappearance/FontFamilyKt;
.super Ljava/lang/Object;
.source "FontFamily.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nFontFamily.kt\nKotlin\n*S Kotlin\n*F\n+ 1 FontFamily.kt\ncom/squareup/textappearance/FontFamilyKt\n+ 2 _Arrays.kt\nkotlin/collections/ArraysKt___ArraysKt\n*L\n1#1,95:1\n925#2,2:96\n*E\n*S KotlinDebug\n*F\n+ 1 FontFamily.kt\ncom/squareup/textappearance/FontFamilyKt\n*L\n88#1,2:96\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0000\u001a\u000c\u0010\u0006\u001a\u00020\u0007*\u00020\u0008H\u0002\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0005\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "FONT_ATTRIBUTE",
        "",
        "FONT_FAMILY_TAG",
        "FONT_STYLE_ATTRIBUTE",
        "FONT_TAG",
        "FONT_WEIGHT_ATTRIBUTE",
        "toStyle",
        "Lcom/squareup/textappearance/Style;",
        "",
        "textappearance_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final FONT_ATTRIBUTE:Ljava/lang/String; = "font"

.field private static final FONT_FAMILY_TAG:Ljava/lang/String; = "font-family"

.field private static final FONT_STYLE_ATTRIBUTE:Ljava/lang/String; = "fontStyle"

.field private static final FONT_TAG:Ljava/lang/String; = "font"

.field private static final FONT_WEIGHT_ATTRIBUTE:Ljava/lang/String; = "fontWeight"


# direct methods
.method public static final synthetic access$toStyle(I)Lcom/squareup/textappearance/Style;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/textappearance/FontFamilyKt;->toStyle(I)Lcom/squareup/textappearance/Style;

    move-result-object p0

    return-object p0
.end method

.method private static final toStyle(I)Lcom/squareup/textappearance/Style;
    .locals 6

    .line 88
    invoke-static {}, Lcom/squareup/textappearance/Style;->values()[Lcom/squareup/textappearance/Style;

    move-result-object v0

    .line 96
    array-length v1, v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_2

    aget-object v4, v0, v3

    .line 88
    invoke-virtual {v4}, Lcom/squareup/textappearance/Style;->getAttributeValue()I

    move-result v5

    if-ne v5, p0, :cond_0

    const/4 v5, 0x1

    goto :goto_1

    :cond_0
    const/4 v5, 0x0

    :goto_1
    if-eqz v5, :cond_1

    return-object v4

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 97
    :cond_2
    new-instance p0, Ljava/util/NoSuchElementException;

    const-string v0, "Array contains no element matching the predicate."

    invoke-direct {p0, v0}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0
.end method
