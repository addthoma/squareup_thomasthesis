.class public Lcom/squareup/text/TabularSpan;
.super Landroid/text/style/ReplacementSpan;
.source "TabularSpan.java"


# static fields
.field private static final DEFAULT_DELIMITER_CHARACTERS:Ljava/lang/String; = ",."

.field private static final DEFAULT_NUMERAL_CHARACTERS:Ljava/lang/String; = "0123456789"


# instance fields
.field private final delimiters:Ljava/lang/String;

.field private final numerals:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 18
    invoke-direct {p0}, Landroid/text/style/ReplacementSpan;-><init>()V

    const-string v0, ",."

    .line 19
    iput-object v0, p0, Lcom/squareup/text/TabularSpan;->delimiters:Ljava/lang/String;

    const-string v0, "0123456789"

    .line 20
    iput-object v0, p0, Lcom/squareup/text/TabularSpan;->numerals:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 23
    invoke-direct {p0}, Landroid/text/style/ReplacementSpan;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/text/TabularSpan;->delimiters:Ljava/lang/String;

    .line 25
    iput-object p2, p0, Lcom/squareup/text/TabularSpan;->numerals:Ljava/lang/String;

    return-void
.end method

.method private static getMaxCharacterWidth(Landroid/graphics/Paint;Ljava/lang/CharSequence;)F
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 76
    :goto_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-ge v1, v2, :cond_0

    add-int/lit8 v2, v1, 0x1

    .line 77
    invoke-virtual {p0, p1, v1, v2}, Landroid/graphics/Paint;->measureText(Ljava/lang/CharSequence;II)F

    move-result v1

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    move v1, v2

    goto :goto_0

    :cond_0
    return v0
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;Ljava/lang/CharSequence;IIFIIILandroid/graphics/Paint;)V
    .locals 16

    move-object/from16 v0, p0

    move-object/from16 v8, p9

    .line 53
    invoke-interface/range {p2 .. p4}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v9

    .line 54
    iget-object v1, v0, Lcom/squareup/text/TabularSpan;->numerals:Ljava/lang/String;

    invoke-static {v8, v1}, Lcom/squareup/text/TabularSpan;->getMaxCharacterWidth(Landroid/graphics/Paint;Ljava/lang/CharSequence;)F

    move-result v10

    .line 55
    iget-object v1, v0, Lcom/squareup/text/TabularSpan;->delimiters:Ljava/lang/String;

    invoke-static {v8, v1}, Lcom/squareup/text/TabularSpan;->getMaxCharacterWidth(Landroid/graphics/Paint;Ljava/lang/CharSequence;)F

    move-result v11

    const/4 v1, 0x0

    move/from16 v12, p5

    const/4 v3, 0x0

    .line 57
    :goto_0
    invoke-interface {v9}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-ge v3, v1, :cond_2

    add-int/lit8 v13, v3, 0x1

    .line 58
    invoke-interface {v9, v3, v13}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    .line 59
    invoke-virtual {v8, v9, v3, v13}, Landroid/graphics/Paint;->measureText(Ljava/lang/CharSequence;II)F

    move-result v2

    .line 61
    iget-object v4, v0, Lcom/squareup/text/TabularSpan;->delimiters:Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v14, v11

    goto :goto_1

    .line 63
    :cond_0
    iget-object v4, v0, Lcom/squareup/text/TabularSpan;->numerals:Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    move v14, v10

    goto :goto_1

    :cond_1
    move v14, v2

    :goto_1
    sub-float v1, v14, v2

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    add-float v5, v12, v1

    move/from16 v15, p7

    int-to-float v6, v15

    move-object/from16 v1, p1

    move-object v2, v9

    move v4, v13

    move-object/from16 v7, p9

    .line 69
    invoke-virtual/range {v1 .. v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;)V

    add-float/2addr v12, v14

    move v3, v13

    goto :goto_0

    :cond_2
    return-void
.end method

.method public getSize(Landroid/graphics/Paint;Ljava/lang/CharSequence;IILandroid/graphics/Paint$FontMetricsInt;)I
    .locals 4

    if-eqz p5, :cond_0

    .line 30
    invoke-virtual {p1, p5}, Landroid/graphics/Paint;->getFontMetricsInt(Landroid/graphics/Paint$FontMetricsInt;)I

    .line 32
    :cond_0
    invoke-interface {p2, p3, p4}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object p2

    .line 33
    iget-object p3, p0, Lcom/squareup/text/TabularSpan;->numerals:Ljava/lang/String;

    invoke-static {p1, p3}, Lcom/squareup/text/TabularSpan;->getMaxCharacterWidth(Landroid/graphics/Paint;Ljava/lang/CharSequence;)F

    move-result p3

    .line 34
    iget-object p4, p0, Lcom/squareup/text/TabularSpan;->delimiters:Ljava/lang/String;

    invoke-static {p1, p4}, Lcom/squareup/text/TabularSpan;->getMaxCharacterWidth(Landroid/graphics/Paint;Ljava/lang/CharSequence;)F

    move-result p4

    const/4 p5, 0x0

    const/4 v0, 0x0

    const/4 p5, 0x0

    const/4 v1, 0x0

    .line 37
    :goto_0
    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-ge p5, v2, :cond_3

    add-int/lit8 v2, p5, 0x1

    .line 38
    invoke-interface {p2, p5, v2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object p5

    .line 39
    iget-object v3, p0, Lcom/squareup/text/TabularSpan;->delimiters:Ljava/lang/String;

    invoke-virtual {v3, p5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    add-float/2addr v1, p4

    goto :goto_1

    .line 41
    :cond_1
    iget-object v3, p0, Lcom/squareup/text/TabularSpan;->numerals:Ljava/lang/String;

    invoke-virtual {v3, p5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    add-float/2addr v1, p3

    goto :goto_1

    :cond_2
    const/4 v3, 0x1

    .line 44
    invoke-virtual {p1, p5, v0, v3}, Landroid/graphics/Paint;->measureText(Ljava/lang/CharSequence;II)F

    move-result p5

    add-float/2addr v1, p5

    :goto_1
    move p5, v2

    goto :goto_0

    :cond_3
    float-to-double p1, v1

    .line 47
    invoke-static {p1, p2}, Ljava/lang/Math;->ceil(D)D

    move-result-wide p1

    double-to-int p1, p1

    return p1
.end method
