.class public final Lcom/squareup/text/TextModule_ProvideCompactNumberFormatterFactory;
.super Ljava/lang/Object;
.source "TextModule_ProvideCompactNumberFormatterFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/text/Formatter<",
        "Ljava/lang/Number;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/text/TextModule_ProvideCompactNumberFormatterFactory;->localeProvider:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/squareup/text/TextModule_ProvideCompactNumberFormatterFactory;->resProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/text/TextModule_ProvideCompactNumberFormatterFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)",
            "Lcom/squareup/text/TextModule_ProvideCompactNumberFormatterFactory;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/text/TextModule_ProvideCompactNumberFormatterFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/text/TextModule_ProvideCompactNumberFormatterFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideCompactNumberFormatter(Ljavax/inject/Provider;Lcom/squareup/util/Res;)Lcom/squareup/text/Formatter;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/util/Res;",
            ")",
            "Lcom/squareup/text/Formatter<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation

    .line 41
    invoke-static {p0, p1}, Lcom/squareup/text/TextModule;->provideCompactNumberFormatter(Ljavax/inject/Provider;Lcom/squareup/util/Res;)Lcom/squareup/text/Formatter;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/text/Formatter;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/text/Formatter;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/text/Formatter<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation

    .line 31
    iget-object v0, p0, Lcom/squareup/text/TextModule_ProvideCompactNumberFormatterFactory;->localeProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/squareup/text/TextModule_ProvideCompactNumberFormatterFactory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Res;

    invoke-static {v0, v1}, Lcom/squareup/text/TextModule_ProvideCompactNumberFormatterFactory;->provideCompactNumberFormatter(Ljavax/inject/Provider;Lcom/squareup/util/Res;)Lcom/squareup/text/Formatter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/text/TextModule_ProvideCompactNumberFormatterFactory;->get()Lcom/squareup/text/Formatter;

    move-result-object v0

    return-object v0
.end method
