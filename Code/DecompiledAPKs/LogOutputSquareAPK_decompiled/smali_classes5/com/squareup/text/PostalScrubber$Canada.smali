.class Lcom/squareup/text/PostalScrubber$Canada;
.super Lcom/squareup/text/PostalScrubber;
.source "PostalScrubber.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/text/PostalScrubber;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Canada"
.end annotation


# static fields
.field private static final BAD_LETTERS:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection<",
            "Ljava/lang/Character;",
            ">;"
        }
    .end annotation
.end field

.field private static final MAX_LENGTH:I = 0x6

.field private static final SPACE_POSITION:I = 0x3


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Character;

    const/16 v1, 0x44

    .line 112
    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const/16 v1, 0x46

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const/16 v1, 0x49

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const/16 v1, 0x4f

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    const/4 v2, 0x3

    aput-object v1, v0, v2

    const/16 v1, 0x51

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    const/4 v2, 0x4

    aput-object v1, v0, v2

    const/16 v1, 0x55

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    const/4 v2, 0x5

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/squareup/text/PostalScrubber$Canada;->BAD_LETTERS:Ljava/util/Collection;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 111
    invoke-direct {p0}, Lcom/squareup/text/PostalScrubber;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/text/PostalScrubber$1;)V
    .locals 0

    .line 111
    invoke-direct {p0}, Lcom/squareup/text/PostalScrubber$Canada;-><init>()V

    return-void
.end method

.method private charOkay(CI)Z
    .locals 0

    .line 152
    rem-int/lit8 p2, p2, 0x2

    if-nez p2, :cond_1

    .line 153
    invoke-static {p1}, Ljava/lang/Character;->isLetter(C)Z

    move-result p2

    if-eqz p2, :cond_0

    sget-object p2, Lcom/squareup/text/PostalScrubber$Canada;->BAD_LETTERS:Ljava/util/Collection;

    invoke-static {p1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object p1

    invoke-interface {p2, p1}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1

    .line 156
    :cond_1
    invoke-static {p1}, Ljava/lang/Character;->isDigit(C)Z

    move-result p1

    return p1
.end method


# virtual methods
.method protected isValidFormattedPostalCode(Ljava/lang/String;)Z
    .locals 0

    .line 117
    invoke-static {p1}, Lcom/squareup/text/PostalCodes;->isCaPostalCode(Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method protected parsePostal(Ljava/lang/String;)Lcom/squareup/text/PostalScrubber$Result;
    .locals 11

    .line 121
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    .line 122
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 125
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    :goto_0
    const/4 v6, 0x1

    if-ge v3, v0, :cond_4

    .line 128
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v7

    .line 130
    invoke-direct {p0, v7, v4}, Lcom/squareup/text/PostalScrubber$Canada;->charOkay(CI)Z

    move-result v8

    const/16 v9, 0x20

    const/4 v10, 0x3

    if-eqz v8, :cond_1

    .line 131
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    if-ne v8, v10, :cond_0

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_0
    add-int/lit8 v4, v4, 0x1

    .line 133
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_1
    if-ne v3, v10, :cond_2

    if-ne v7, v9, :cond_2

    const/4 v7, 0x1

    goto :goto_1

    :cond_2
    const/4 v7, 0x0

    :goto_1
    xor-int/2addr v7, v6

    or-int/2addr v5, v7

    :goto_2
    const/4 v7, 0x6

    if-lt v4, v7, :cond_3

    goto :goto_3

    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 146
    :cond_4
    :goto_3
    new-instance p1, Lcom/squareup/text/PostalScrubber$Result;

    xor-int/lit8 v0, v5, 0x1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p1, v0, v1}, Lcom/squareup/text/PostalScrubber$Result;-><init>(ZLjava/lang/String;)V

    return-object p1
.end method
