.class public Lcom/squareup/tipping/FormattedTipOption;
.super Ljava/lang/Object;
.source "FormattedTipOption.java"


# instance fields
.field public final bottomLine:Ljava/lang/CharSequence;

.field public final topLine:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Ljava/lang/CharSequence;)V
    .locals 1

    const/4 v0, 0x0

    .line 9
    invoke-direct {p0, p1, v0}, Lcom/squareup/tipping/FormattedTipOption;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/squareup/tipping/FormattedTipOption;->topLine:Ljava/lang/CharSequence;

    .line 14
    iput-object p2, p0, Lcom/squareup/tipping/FormattedTipOption;->bottomLine:Ljava/lang/CharSequence;

    return-void
.end method
