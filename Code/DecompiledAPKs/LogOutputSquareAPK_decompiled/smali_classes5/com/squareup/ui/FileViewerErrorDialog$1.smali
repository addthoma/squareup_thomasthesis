.class synthetic Lcom/squareup/ui/FileViewerErrorDialog$1;
.super Ljava/lang/Object;
.source "FileViewerErrorDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/FileViewerErrorDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$ui$FileViewerErrorDialog$FileErrorType:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 48
    invoke-static {}, Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;->values()[Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/ui/FileViewerErrorDialog$1;->$SwitchMap$com$squareup$ui$FileViewerErrorDialog$FileErrorType:[I

    :try_start_0
    sget-object v0, Lcom/squareup/ui/FileViewerErrorDialog$1;->$SwitchMap$com$squareup$ui$FileViewerErrorDialog$FileErrorType:[I

    sget-object v1, Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;->NO_IMAGE_VIEWER:Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;

    invoke-virtual {v1}, Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :try_start_1
    sget-object v0, Lcom/squareup/ui/FileViewerErrorDialog$1;->$SwitchMap$com$squareup$ui$FileViewerErrorDialog$FileErrorType:[I

    sget-object v1, Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;->NO_PDF_VIEWER:Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;

    invoke-virtual {v1}, Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    :try_start_2
    sget-object v0, Lcom/squareup/ui/FileViewerErrorDialog$1;->$SwitchMap$com$squareup$ui$FileViewerErrorDialog$FileErrorType:[I

    sget-object v1, Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;->DEFAULT_NO_VIEWER:Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;

    invoke-virtual {v1}, Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    :try_start_3
    sget-object v0, Lcom/squareup/ui/FileViewerErrorDialog$1;->$SwitchMap$com$squareup$ui$FileViewerErrorDialog$FileErrorType:[I

    sget-object v1, Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;->LOAD_IMAGE_ERROR:Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;

    invoke-virtual {v1}, Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    :try_start_4
    sget-object v0, Lcom/squareup/ui/FileViewerErrorDialog$1;->$SwitchMap$com$squareup$ui$FileViewerErrorDialog$FileErrorType:[I

    sget-object v1, Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;->LOAD_PDF_ERROR:Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;

    invoke-virtual {v1}, Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :catch_4
    :try_start_5
    sget-object v0, Lcom/squareup/ui/FileViewerErrorDialog$1;->$SwitchMap$com$squareup$ui$FileViewerErrorDialog$FileErrorType:[I

    sget-object v1, Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;->LOAD_DEFAULT_ERROR:Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;

    invoke-virtual {v1}, Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_5

    :catch_5
    return-void
.end method
