.class Lcom/squareup/ui/NfcProcessor$DefaultSmartPaymentResultHandler;
.super Ljava/lang/Object;
.source "NfcProcessor.java"

# interfaces
.implements Lcom/squareup/ui/main/SmartPaymentResultHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/NfcProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DefaultSmartPaymentResultHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/NfcProcessor;


# direct methods
.method private constructor <init>(Lcom/squareup/ui/NfcProcessor;)V
    .locals 0

    .line 1043
    iput-object p1, p0, Lcom/squareup/ui/NfcProcessor$DefaultSmartPaymentResultHandler;->this$0:Lcom/squareup/ui/NfcProcessor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/ui/NfcProcessor;Lcom/squareup/ui/NfcProcessor$1;)V
    .locals 0

    .line 1043
    invoke-direct {p0, p1}, Lcom/squareup/ui/NfcProcessor$DefaultSmartPaymentResultHandler;-><init>(Lcom/squareup/ui/NfcProcessor;)V

    return-void
.end method


# virtual methods
.method public handleSmartPaymentResult(Lcom/squareup/ui/main/SmartPaymentResult;)V
    .locals 1

    .line 1045
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor$DefaultSmartPaymentResultHandler;->this$0:Lcom/squareup/ui/NfcProcessor;

    invoke-static {v0}, Lcom/squareup/ui/NfcProcessor;->access$600(Lcom/squareup/ui/NfcProcessor;)Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->navigateForSmartPaymentResult(Lcom/squareup/ui/main/SmartPaymentResult;)V

    return-void
.end method
