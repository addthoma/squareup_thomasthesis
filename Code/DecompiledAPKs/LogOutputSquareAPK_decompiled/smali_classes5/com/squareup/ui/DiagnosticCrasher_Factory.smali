.class public final Lcom/squareup/ui/DiagnosticCrasher_Factory;
.super Ljava/lang/Object;
.source "DiagnosticCrasher_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/DiagnosticCrasher;",
        ">;"
    }
.end annotation


# instance fields
.field private final calendarFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/DiagnosticCrasher$CalendarFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final dumperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/QueueDumper;",
            ">;"
        }
    .end annotation
.end field

.field private final gsonProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;"
        }
    .end annotation
.end field

.field private final remoteLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/logging/RemoteLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final rpcSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final userTokenProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/DiagnosticCrasher$CalendarFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/QueueDumper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/logging/RemoteLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;)V"
        }
    .end annotation

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/squareup/ui/DiagnosticCrasher_Factory;->userTokenProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p2, p0, Lcom/squareup/ui/DiagnosticCrasher_Factory;->calendarFactoryProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p3, p0, Lcom/squareup/ui/DiagnosticCrasher_Factory;->dumperProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p4, p0, Lcom/squareup/ui/DiagnosticCrasher_Factory;->gsonProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p5, p0, Lcom/squareup/ui/DiagnosticCrasher_Factory;->remoteLoggerProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p6, p0, Lcom/squareup/ui/DiagnosticCrasher_Factory;->rpcSchedulerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/DiagnosticCrasher_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/DiagnosticCrasher$CalendarFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/QueueDumper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/logging/RemoteLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;)",
            "Lcom/squareup/ui/DiagnosticCrasher_Factory;"
        }
    .end annotation

    .line 53
    new-instance v7, Lcom/squareup/ui/DiagnosticCrasher_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/DiagnosticCrasher_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Ljava/lang/String;Ljava/lang/Object;Ljavax/inject/Provider;Lcom/google/gson/Gson;Lcom/squareup/logging/RemoteLogger;Lrx/Scheduler;)Lcom/squareup/ui/DiagnosticCrasher;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/QueueDumper;",
            ">;",
            "Lcom/google/gson/Gson;",
            "Lcom/squareup/logging/RemoteLogger;",
            "Lrx/Scheduler;",
            ")",
            "Lcom/squareup/ui/DiagnosticCrasher;"
        }
    .end annotation

    .line 58
    new-instance v7, Lcom/squareup/ui/DiagnosticCrasher;

    move-object v2, p1

    check-cast v2, Lcom/squareup/ui/DiagnosticCrasher$CalendarFactory;

    move-object v0, v7

    move-object v1, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/DiagnosticCrasher;-><init>(Ljava/lang/String;Lcom/squareup/ui/DiagnosticCrasher$CalendarFactory;Ljavax/inject/Provider;Lcom/google/gson/Gson;Lcom/squareup/logging/RemoteLogger;Lrx/Scheduler;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/ui/DiagnosticCrasher;
    .locals 7

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/DiagnosticCrasher_Factory;->userTokenProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/lang/String;

    iget-object v0, p0, Lcom/squareup/ui/DiagnosticCrasher_Factory;->calendarFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/DiagnosticCrasher_Factory;->dumperProvider:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/squareup/ui/DiagnosticCrasher_Factory;->gsonProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/google/gson/Gson;

    iget-object v0, p0, Lcom/squareup/ui/DiagnosticCrasher_Factory;->remoteLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/logging/RemoteLogger;

    iget-object v0, p0, Lcom/squareup/ui/DiagnosticCrasher_Factory;->rpcSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lrx/Scheduler;

    invoke-static/range {v1 .. v6}, Lcom/squareup/ui/DiagnosticCrasher_Factory;->newInstance(Ljava/lang/String;Ljava/lang/Object;Ljavax/inject/Provider;Lcom/google/gson/Gson;Lcom/squareup/logging/RemoteLogger;Lrx/Scheduler;)Lcom/squareup/ui/DiagnosticCrasher;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ui/DiagnosticCrasher_Factory;->get()Lcom/squareup/ui/DiagnosticCrasher;

    move-result-object v0

    return-object v0
.end method
