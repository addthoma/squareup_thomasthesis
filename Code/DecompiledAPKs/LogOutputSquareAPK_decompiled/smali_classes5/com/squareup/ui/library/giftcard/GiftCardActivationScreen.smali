.class public final Lcom/squareup/ui/library/giftcard/GiftCardActivationScreen;
.super Lcom/squareup/ui/seller/InSellerScope;
.source "GiftCardActivationScreen.java"

# interfaces
.implements Lcom/squareup/ui/seller/EnablesCardSwipes;
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/spot/HasSpot;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/library/giftcard/GiftCardActivationScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/library/giftcard/GiftCardActivationScreen$Component;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/library/giftcard/GiftCardActivationScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final workingItem:Lcom/squareup/configure/item/WorkingItem;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 50
    sget-object v0, Lcom/squareup/ui/library/giftcard/-$$Lambda$GiftCardActivationScreen$lUHg7gS5PaNNl14pioYeoaVIoak;->INSTANCE:Lcom/squareup/ui/library/giftcard/-$$Lambda$GiftCardActivationScreen$lUHg7gS5PaNNl14pioYeoaVIoak;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/library/giftcard/GiftCardActivationScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/configure/item/WorkingItem;)V
    .locals 0

    .line 28
    invoke-direct {p0}, Lcom/squareup/ui/seller/InSellerScope;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationScreen;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/library/giftcard/GiftCardActivationScreen;
    .locals 1

    .line 53
    new-instance p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationScreen;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationScreen;-><init>(Lcom/squareup/configure/item/WorkingItem;)V

    return-object p0
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 41
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_GIFT_CARD_ACTIVATION:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 37
    sget-object p1, Lcom/squareup/container/spot/Spots;->BELOW:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 33
    sget v0, Lcom/squareup/orderentry/R$layout;->gift_card_activiation_view:I

    return v0
.end method
