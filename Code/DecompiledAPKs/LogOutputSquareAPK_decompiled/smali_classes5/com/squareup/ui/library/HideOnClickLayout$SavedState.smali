.class Lcom/squareup/ui/library/HideOnClickLayout$SavedState;
.super Landroid/view/View$BaseSavedState;
.source "HideOnClickLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/library/HideOnClickLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SavedState"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/library/HideOnClickLayout$SavedState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final topChildVisibility:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 81
    new-instance v0, Lcom/squareup/ui/library/HideOnClickLayout$SavedState$1;

    invoke-direct {v0}, Lcom/squareup/ui/library/HideOnClickLayout$SavedState$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/library/HideOnClickLayout$SavedState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    .line 72
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    .line 73
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/library/HideOnClickLayout$SavedState;->topChildVisibility:I

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/squareup/ui/library/HideOnClickLayout$1;)V
    .locals 0

    .line 63
    invoke-direct {p0, p1}, Lcom/squareup/ui/library/HideOnClickLayout$SavedState;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcelable;I)V
    .locals 0

    .line 67
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    .line 68
    iput p2, p0, Lcom/squareup/ui/library/HideOnClickLayout$SavedState;->topChildVisibility:I

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcelable;ILcom/squareup/ui/library/HideOnClickLayout$1;)V
    .locals 0

    .line 63
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/library/HideOnClickLayout$SavedState;-><init>(Landroid/os/Parcelable;I)V

    return-void
.end method

.method static synthetic access$400(Lcom/squareup/ui/library/HideOnClickLayout$SavedState;)I
    .locals 0

    .line 63
    iget p0, p0, Lcom/squareup/ui/library/HideOnClickLayout$SavedState;->topChildVisibility:I

    return p0
.end method


# virtual methods
.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 77
    invoke-super {p0, p1, p2}, Landroid/view/View$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    .line 78
    iget p2, p0, Lcom/squareup/ui/library/HideOnClickLayout$SavedState;->topChildVisibility:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
