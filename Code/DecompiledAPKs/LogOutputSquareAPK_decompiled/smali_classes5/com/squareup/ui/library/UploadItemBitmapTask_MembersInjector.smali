.class public final Lcom/squareup/ui/library/UploadItemBitmapTask_MembersInjector;
.super Ljava/lang/Object;
.source "UploadItemBitmapTask_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/library/UploadItemBitmapTask;",
        ">;"
    }
.end annotation


# instance fields
.field private final busProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;"
        }
    .end annotation
.end field

.field private final cogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field private final fileThreadExecutorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation
.end field

.field private final imageServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/ImageService;",
            ">;"
        }
    .end annotation
.end field

.field private final itemPhotosProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/ImageService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;)V"
        }
    .end annotation

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/squareup/ui/library/UploadItemBitmapTask_MembersInjector;->imageServiceProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p2, p0, Lcom/squareup/ui/library/UploadItemBitmapTask_MembersInjector;->busProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p3, p0, Lcom/squareup/ui/library/UploadItemBitmapTask_MembersInjector;->cogsProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p4, p0, Lcom/squareup/ui/library/UploadItemBitmapTask_MembersInjector;->itemPhotosProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p5, p0, Lcom/squareup/ui/library/UploadItemBitmapTask_MembersInjector;->fileThreadExecutorProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/ImageService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/library/UploadItemBitmapTask;",
            ">;"
        }
    .end annotation

    .line 48
    new-instance v6, Lcom/squareup/ui/library/UploadItemBitmapTask_MembersInjector;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/library/UploadItemBitmapTask_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static injectBus(Lcom/squareup/ui/library/UploadItemBitmapTask;Lcom/squareup/badbus/BadEventSink;)V
    .locals 0

    .line 66
    iput-object p1, p0, Lcom/squareup/ui/library/UploadItemBitmapTask;->bus:Lcom/squareup/badbus/BadEventSink;

    return-void
.end method

.method public static injectCogs(Lcom/squareup/ui/library/UploadItemBitmapTask;Lcom/squareup/cogs/Cogs;)V
    .locals 0

    .line 71
    iput-object p1, p0, Lcom/squareup/ui/library/UploadItemBitmapTask;->cogs:Lcom/squareup/cogs/Cogs;

    return-void
.end method

.method public static injectFileThreadExecutor(Lcom/squareup/ui/library/UploadItemBitmapTask;Ljava/util/concurrent/Executor;)V
    .locals 0
    .annotation runtime Lcom/squareup/thread/FileThread;
    .end annotation

    .line 83
    iput-object p1, p0, Lcom/squareup/ui/library/UploadItemBitmapTask;->fileThreadExecutor:Ljava/util/concurrent/Executor;

    return-void
.end method

.method public static injectImageService(Lcom/squareup/ui/library/UploadItemBitmapTask;Lcom/squareup/server/ImageService;)V
    .locals 0

    .line 61
    iput-object p1, p0, Lcom/squareup/ui/library/UploadItemBitmapTask;->imageService:Lcom/squareup/server/ImageService;

    return-void
.end method

.method public static injectItemPhotos(Lcom/squareup/ui/library/UploadItemBitmapTask;Lcom/squareup/ui/photo/ItemPhoto$Factory;)V
    .locals 0

    .line 76
    iput-object p1, p0, Lcom/squareup/ui/library/UploadItemBitmapTask;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/library/UploadItemBitmapTask;)V
    .locals 1

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/library/UploadItemBitmapTask_MembersInjector;->imageServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/ImageService;

    invoke-static {p1, v0}, Lcom/squareup/ui/library/UploadItemBitmapTask_MembersInjector;->injectImageService(Lcom/squareup/ui/library/UploadItemBitmapTask;Lcom/squareup/server/ImageService;)V

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/library/UploadItemBitmapTask_MembersInjector;->busProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/badbus/BadEventSink;

    invoke-static {p1, v0}, Lcom/squareup/ui/library/UploadItemBitmapTask_MembersInjector;->injectBus(Lcom/squareup/ui/library/UploadItemBitmapTask;Lcom/squareup/badbus/BadEventSink;)V

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/library/UploadItemBitmapTask_MembersInjector;->cogsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cogs/Cogs;

    invoke-static {p1, v0}, Lcom/squareup/ui/library/UploadItemBitmapTask_MembersInjector;->injectCogs(Lcom/squareup/ui/library/UploadItemBitmapTask;Lcom/squareup/cogs/Cogs;)V

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/library/UploadItemBitmapTask_MembersInjector;->itemPhotosProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/photo/ItemPhoto$Factory;

    invoke-static {p1, v0}, Lcom/squareup/ui/library/UploadItemBitmapTask_MembersInjector;->injectItemPhotos(Lcom/squareup/ui/library/UploadItemBitmapTask;Lcom/squareup/ui/photo/ItemPhoto$Factory;)V

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/library/UploadItemBitmapTask_MembersInjector;->fileThreadExecutorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    invoke-static {p1, v0}, Lcom/squareup/ui/library/UploadItemBitmapTask_MembersInjector;->injectFileThreadExecutor(Lcom/squareup/ui/library/UploadItemBitmapTask;Ljava/util/concurrent/Executor;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 14
    check-cast p1, Lcom/squareup/ui/library/UploadItemBitmapTask;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/library/UploadItemBitmapTask_MembersInjector;->injectMembers(Lcom/squareup/ui/library/UploadItemBitmapTask;)V

    return-void
.end method
