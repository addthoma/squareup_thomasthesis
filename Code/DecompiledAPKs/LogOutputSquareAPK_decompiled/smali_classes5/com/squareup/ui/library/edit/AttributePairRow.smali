.class public Lcom/squareup/ui/library/edit/AttributePairRow;
.super Landroid/widget/LinearLayout;
.source "AttributePairRow.java"


# instance fields
.field private key:Landroid/widget/TextView;

.field private value:Lcom/squareup/widgets/MessageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 18
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    sget p2, Lcom/squareup/widgets/pos/R$layout;->attribute_pair_row:I

    invoke-static {p1, p2, p0}, Lcom/squareup/ui/library/edit/AttributePairRow;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 20
    sget p1, Lcom/squareup/widgets/pos/R$id;->attribute_pair_key:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/ui/library/edit/AttributePairRow;->key:Landroid/widget/TextView;

    .line 21
    sget p1, Lcom/squareup/widgets/pos/R$id;->attribute_pair_value:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/MessageView;

    iput-object p1, p0, Lcom/squareup/ui/library/edit/AttributePairRow;->value:Lcom/squareup/widgets/MessageView;

    return-void
.end method


# virtual methods
.method public setKeyAndValue(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/ui/library/edit/AttributePairRow;->key:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 26
    iget-object p1, p0, Lcom/squareup/ui/library/edit/AttributePairRow;->value:Lcom/squareup/widgets/MessageView;

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
