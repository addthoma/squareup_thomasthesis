.class public final Lcom/squareup/ui/library/DiscountEntryPercentScreen;
.super Lcom/squareup/ui/seller/InSellerScope;
.source "DiscountEntryPercentScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/library/DiscountEntryPercentScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/library/DiscountEntryPercentScreen$Component;,
        Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;,
        Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/library/DiscountEntryPercentScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final authorizingEmployee:Lcom/squareup/protos/client/Employee;

.field private final workingDiscount:Lcom/squareup/configure/item/WorkingDiscount;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 249
    sget-object v0, Lcom/squareup/ui/library/-$$Lambda$DiscountEntryPercentScreen$lAvPln7gQWLQa8Grq8JJ7b_8mQw;->INSTANCE:Lcom/squareup/ui/library/-$$Lambda$DiscountEntryPercentScreen$lAvPln7gQWLQa8Grq8JJ7b_8mQw;

    .line 250
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/library/DiscountEntryPercentScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Lcom/squareup/configure/item/WorkingDiscount;Lcom/squareup/protos/client/Employee;)V
    .locals 0

    .line 49
    invoke-direct {p0}, Lcom/squareup/ui/seller/InSellerScope;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen;->workingDiscount:Lcom/squareup/configure/item/WorkingDiscount;

    .line 51
    iput-object p2, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen;->authorizingEmployee:Lcom/squareup/protos/client/Employee;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/library/DiscountEntryPercentScreen;)Lcom/squareup/configure/item/WorkingDiscount;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen;->workingDiscount:Lcom/squareup/configure/item/WorkingDiscount;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/library/DiscountEntryPercentScreen;)Lcom/squareup/protos/client/Employee;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen;->authorizingEmployee:Lcom/squareup/protos/client/Employee;

    return-object p0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/library/DiscountEntryPercentScreen;
    .locals 1

    .line 253
    new-instance p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen;

    const/4 v0, 0x0

    invoke-direct {p0, v0, v0}, Lcom/squareup/ui/library/DiscountEntryPercentScreen;-><init>(Lcom/squareup/configure/item/WorkingDiscount;Lcom/squareup/protos/client/Employee;)V

    return-object p0
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 55
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_DISCOUNT_ENTRY_PERCENT:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 257
    sget v0, Lcom/squareup/orderentry/R$layout;->discount_entry_percent_view:I

    return v0
.end method
