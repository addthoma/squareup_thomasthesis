.class Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter$MoneyEntryKeypadListener;
.super Lcom/squareup/padlock/MoneyKeypadListener;
.source "DiscountEntryMoneyScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MoneyEntryKeypadListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;)V
    .locals 3

    .line 157
    iput-object p1, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter$MoneyEntryKeypadListener;->this$0:Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;

    .line 158
    invoke-static {p1}, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->access$800(Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/library/DiscountEntryMoneyView;

    invoke-virtual {v0}, Lcom/squareup/ui/library/DiscountEntryMoneyView;->getKeypad()Lcom/squareup/padlock/Padlock;

    move-result-object v0

    invoke-static {p1}, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->access$900(Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;)Landroid/os/Vibrator;

    move-result-object p1

    sget-wide v1, Lcom/squareup/money/MaxMoneyScrubber;->MAX_MONEY:J

    invoke-direct {p0, v0, p1, v1, v2}, Lcom/squareup/padlock/MoneyKeypadListener;-><init>(Lcom/squareup/padlock/Padlock;Landroid/os/Vibrator;J)V

    return-void
.end method


# virtual methods
.method public getAmount()J
    .locals 2

    .line 162
    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter$MoneyEntryKeypadListener;->this$0:Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;

    invoke-static {v0}, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->access$1000(Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;)Lcom/squareup/ui/library/DiscountEntryMoneyScreen$MoneyWorkingDiscountState;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$MoneyWorkingDiscountState;->access$500(Lcom/squareup/ui/library/DiscountEntryMoneyScreen$MoneyWorkingDiscountState;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public onSubmitClicked()V
    .locals 1

    .line 172
    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter$MoneyEntryKeypadListener;->this$0:Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->onCommitSelected()Z

    return-void
.end method

.method public updateAmount(J)V
    .locals 2

    .line 166
    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter$MoneyEntryKeypadListener;->this$0:Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;

    invoke-static {v0}, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->access$1000(Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;)Lcom/squareup/ui/library/DiscountEntryMoneyScreen$MoneyWorkingDiscountState;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter$MoneyEntryKeypadListener;->this$0:Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;

    invoke-static {v1}, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->access$1100(Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;)Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v1

    invoke-static {p1, p2, v1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$MoneyWorkingDiscountState;->access$1200(Lcom/squareup/ui/library/DiscountEntryMoneyScreen$MoneyWorkingDiscountState;Lcom/squareup/protos/common/Money;)V

    .line 167
    iget-object p1, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter$MoneyEntryKeypadListener;->this$0:Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;

    invoke-static {p1}, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->access$1300(Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;)V

    .line 168
    invoke-virtual {p0}, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter$MoneyEntryKeypadListener;->updateKeyStates()V

    return-void
.end method
