.class final Lcom/squareup/ui/items/EditInventoryState$1;
.super Ljava/lang/Object;
.source "EditInventoryState.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditInventoryState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/squareup/ui/items/EditInventoryState;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/squareup/ui/items/EditInventoryState;
    .locals 4

    .line 152
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 153
    const-class v1, Ljava/math/BigDecimal;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readMap(Ljava/util/Map;Ljava/lang/ClassLoader;)V

    .line 154
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    .line 155
    const-class v2, Lcom/squareup/ui/items/EditInventoryState$InventoryAssignment;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Parcel;->readMap(Ljava/util/Map;Ljava/lang/ClassLoader;)V

    .line 157
    invoke-static {}, Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;->values()[Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;

    move-result-object v2

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result p1

    aget-object p1, v2, p1

    .line 158
    new-instance v2, Lcom/squareup/ui/items/EditInventoryState;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v1, p1, v3}, Lcom/squareup/ui/items/EditInventoryState;-><init>(Ljava/util/LinkedHashMap;Ljava/util/LinkedHashMap;Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;Lcom/squareup/ui/items/EditInventoryState$1;)V

    return-object v2
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 150
    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/EditInventoryState$1;->createFromParcel(Landroid/os/Parcel;)Lcom/squareup/ui/items/EditInventoryState;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/squareup/ui/items/EditInventoryState;
    .locals 0

    .line 163
    new-array p1, p1, [Lcom/squareup/ui/items/EditInventoryState;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 150
    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/EditInventoryState$1;->newArray(I)[Lcom/squareup/ui/items/EditInventoryState;

    move-result-object p1

    return-object p1
.end method
