.class Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter$LoadedData;
.super Ljava/lang/Object;
.source "ModifierSetAssignmentScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "LoadedData"
.end annotation


# instance fields
.field final categoryNameMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final libraryCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;


# direct methods
.method constructor <init>(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 142
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 143
    iput-object p1, p0, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter$LoadedData;->libraryCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    .line 144
    iput-object p2, p0, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter$LoadedData;->categoryNameMap:Ljava/util/Map;

    return-void
.end method
