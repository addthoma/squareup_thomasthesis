.class public final Lcom/squareup/ui/items/EditModifierSetScope_Module_ProvideModifierSetDataFactory;
.super Ljava/lang/Object;
.source "EditModifierSetScope_Module_ProvideModifierSetDataFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/EditModifierSetScope_Module_ProvideModifierSetDataFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/items/ModifierSetEditState;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/ui/items/EditModifierSetScope_Module_ProvideModifierSetDataFactory;
    .locals 1

    .line 22
    invoke-static {}, Lcom/squareup/ui/items/EditModifierSetScope_Module_ProvideModifierSetDataFactory$InstanceHolder;->access$000()Lcom/squareup/ui/items/EditModifierSetScope_Module_ProvideModifierSetDataFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideModifierSetData()Lcom/squareup/ui/items/ModifierSetEditState;
    .locals 2

    .line 26
    invoke-static {}, Lcom/squareup/ui/items/EditModifierSetScope$Module;->provideModifierSetData()Lcom/squareup/ui/items/ModifierSetEditState;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/ModifierSetEditState;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/items/ModifierSetEditState;
    .locals 1

    .line 18
    invoke-static {}, Lcom/squareup/ui/items/EditModifierSetScope_Module_ProvideModifierSetDataFactory;->provideModifierSetData()Lcom/squareup/ui/items/ModifierSetEditState;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditModifierSetScope_Module_ProvideModifierSetDataFactory;->get()Lcom/squareup/ui/items/ModifierSetEditState;

    move-result-object v0

    return-object v0
.end method
