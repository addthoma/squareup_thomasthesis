.class public interface abstract Lcom/squareup/ui/items/EditModifierSetScope$Component;
.super Ljava/lang/Object;
.source "EditModifierSetScope.java"


# annotations
.annotation runtime Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher$SharedScope;
.end annotation

.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/catalog/online/CatalogOnlineModule;,
        Lcom/squareup/ui/items/EditModifierSetScope$Module;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditModifierSetScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract editModifierSetMain()Lcom/squareup/ui/items/EditModifierSetMainScreen$Component;
.end method

.method public abstract modifierSetAssignment()Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Component;
.end method

.method public abstract scopeRunner()Lcom/squareup/ui/items/EditModifierSetScopeRunner;
.end method
