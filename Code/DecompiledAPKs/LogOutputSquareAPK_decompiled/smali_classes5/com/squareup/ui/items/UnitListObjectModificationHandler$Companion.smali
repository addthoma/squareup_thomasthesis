.class public final Lcom/squareup/ui/items/UnitListObjectModificationHandler$Companion;
.super Ljava/lang/Object;
.source "UnitListObjectModificationHandler.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/UnitListObjectModificationHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/ui/items/UnitListObjectModificationHandler$Companion;",
        "",
        "()V",
        "of",
        "Lcom/squareup/ui/items/UnitListObjectModificationHandler;",
        "res",
        "Lcom/squareup/util/Res;",
        "editUnitWorkflow",
        "Lcom/squareup/items/unit/EditUnitWorkflow;",
        "items-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 93
    invoke-direct {p0}, Lcom/squareup/ui/items/UnitListObjectModificationHandler$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final of(Lcom/squareup/util/Res;Lcom/squareup/items/unit/EditUnitWorkflow;)Lcom/squareup/ui/items/UnitListObjectModificationHandler;
    .locals 2

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "editUnitWorkflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 97
    new-instance v0, Lcom/squareup/ui/items/UnitListObjectModificationHandler;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1}, Lcom/squareup/ui/items/UnitListObjectModificationHandler;-><init>(Lcom/squareup/util/Res;Lcom/squareup/items/unit/EditUnitWorkflow;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method
