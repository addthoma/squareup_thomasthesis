.class public final Lcom/squareup/ui/items/DetailSearchableListScreen;
.super Ljava/lang/Object;
.source "DetailSearchableListScreen.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/V2Screen;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/DetailSearchableListScreen$Event;,
        Lcom/squareup/ui/items/DetailSearchableListScreen$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0008\u0018\u0000 \u00102\u00020\u0001:\u0002\u0010\u0011B!\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005\u00a2\u0006\u0002\u0010\u0008R\u000e\u0010\t\u001a\u00020\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001d\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u00058F\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000f\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/ui/items/DetailSearchableListScreen;",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        "state",
        "Lcom/squareup/ui/items/DetailSearchableListState$ShowList;",
        "eventHandler",
        "Lkotlin/Function1;",
        "Lcom/squareup/ui/items/DetailSearchableListScreen$Event;",
        "",
        "(Lcom/squareup/ui/items/DetailSearchableListState$ShowList;Lkotlin/jvm/functions/Function1;)V",
        "acceptingEvent",
        "",
        "onEvent",
        "getOnEvent",
        "()Lkotlin/jvm/functions/Function1;",
        "getState",
        "()Lcom/squareup/ui/items/DetailSearchableListState$ShowList;",
        "Companion",
        "Event",
        "items-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/items/DetailSearchableListScreen$Companion;

.field private static final KEY:Lcom/squareup/workflow/legacy/Screen$Key;


# instance fields
.field private acceptingEvent:Z

.field private final eventHandler:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/ui/items/DetailSearchableListScreen$Event;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final state:Lcom/squareup/ui/items/DetailSearchableListState$ShowList;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/squareup/ui/items/DetailSearchableListScreen$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/items/DetailSearchableListScreen$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/items/DetailSearchableListScreen;->Companion:Lcom/squareup/ui/items/DetailSearchableListScreen$Companion;

    .line 33
    const-class v0, Lcom/squareup/ui/items/DetailSearchableListScreen;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    const/4 v2, 0x1

    invoke-static {v0, v1, v2, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey$default(Lkotlin/reflect/KClass;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/items/DetailSearchableListScreen;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/items/DetailSearchableListState$ShowList;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/items/DetailSearchableListState$ShowList;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/ui/items/DetailSearchableListScreen$Event;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "eventHandler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListScreen;->state:Lcom/squareup/ui/items/DetailSearchableListState$ShowList;

    iput-object p2, p0, Lcom/squareup/ui/items/DetailSearchableListScreen;->eventHandler:Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x1

    .line 13
    iput-boolean p1, p0, Lcom/squareup/ui/items/DetailSearchableListScreen;->acceptingEvent:Z

    return-void
.end method

.method public static final synthetic access$getAcceptingEvent$p(Lcom/squareup/ui/items/DetailSearchableListScreen;)Z
    .locals 0

    .line 8
    iget-boolean p0, p0, Lcom/squareup/ui/items/DetailSearchableListScreen;->acceptingEvent:Z

    return p0
.end method

.method public static final synthetic access$getEventHandler$p(Lcom/squareup/ui/items/DetailSearchableListScreen;)Lkotlin/jvm/functions/Function1;
    .locals 0

    .line 8
    iget-object p0, p0, Lcom/squareup/ui/items/DetailSearchableListScreen;->eventHandler:Lkotlin/jvm/functions/Function1;

    return-object p0
.end method

.method public static final synthetic access$getKEY$cp()Lcom/squareup/workflow/legacy/Screen$Key;
    .locals 1

    .line 8
    sget-object v0, Lcom/squareup/ui/items/DetailSearchableListScreen;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-object v0
.end method

.method public static final synthetic access$setAcceptingEvent$p(Lcom/squareup/ui/items/DetailSearchableListScreen;Z)V
    .locals 0

    .line 8
    iput-boolean p1, p0, Lcom/squareup/ui/items/DetailSearchableListScreen;->acceptingEvent:Z

    return-void
.end method


# virtual methods
.method public final getOnEvent()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/ui/items/DetailSearchableListScreen$Event;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 15
    new-instance v0, Lcom/squareup/ui/items/DetailSearchableListScreen$onEvent$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/DetailSearchableListScreen$onEvent$1;-><init>(Lcom/squareup/ui/items/DetailSearchableListScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getState()Lcom/squareup/ui/items/DetailSearchableListState$ShowList;
    .locals 1

    .line 9
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListScreen;->state:Lcom/squareup/ui/items/DetailSearchableListState$ShowList;

    return-object v0
.end method
