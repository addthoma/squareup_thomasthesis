.class public final Lcom/squareup/ui/items/EditItemPhotoView_MembersInjector;
.super Ljava/lang/Object;
.source "EditItemPhotoView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/items/EditItemPhotoView;",
        ">;"
    }
.end annotation


# instance fields
.field private final photoFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemPhotoPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final toastFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/ToastFactory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemPhotoPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/ToastFactory;",
            ">;)V"
        }
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemPhotoView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p2, p0, Lcom/squareup/ui/items/EditItemPhotoView_MembersInjector;->photoFactoryProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p3, p0, Lcom/squareup/ui/items/EditItemPhotoView_MembersInjector;->toastFactoryProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemPhotoPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/ToastFactory;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/items/EditItemPhotoView;",
            ">;"
        }
    .end annotation

    .line 37
    new-instance v0, Lcom/squareup/ui/items/EditItemPhotoView_MembersInjector;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/items/EditItemPhotoView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectPhotoFactory(Lcom/squareup/ui/items/EditItemPhotoView;Lcom/squareup/ui/photo/ItemPhoto$Factory;)V
    .locals 0

    .line 54
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemPhotoView;->photoFactory:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/ui/items/EditItemPhotoView;Ljava/lang/Object;)V
    .locals 0

    .line 48
    check-cast p1, Lcom/squareup/ui/items/EditItemPhotoPresenter;

    iput-object p1, p0, Lcom/squareup/ui/items/EditItemPhotoView;->presenter:Lcom/squareup/ui/items/EditItemPhotoPresenter;

    return-void
.end method

.method public static injectToastFactory(Lcom/squareup/ui/items/EditItemPhotoView;Lcom/squareup/util/ToastFactory;)V
    .locals 0

    .line 59
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemPhotoView;->toastFactory:Lcom/squareup/util/ToastFactory;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/items/EditItemPhotoView;)V
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemPhotoView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/items/EditItemPhotoView_MembersInjector;->injectPresenter(Lcom/squareup/ui/items/EditItemPhotoView;Ljava/lang/Object;)V

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemPhotoView_MembersInjector;->photoFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/photo/ItemPhoto$Factory;

    invoke-static {p1, v0}, Lcom/squareup/ui/items/EditItemPhotoView_MembersInjector;->injectPhotoFactory(Lcom/squareup/ui/items/EditItemPhotoView;Lcom/squareup/ui/photo/ItemPhoto$Factory;)V

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemPhotoView_MembersInjector;->toastFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/ToastFactory;

    invoke-static {p1, v0}, Lcom/squareup/ui/items/EditItemPhotoView_MembersInjector;->injectToastFactory(Lcom/squareup/ui/items/EditItemPhotoView;Lcom/squareup/util/ToastFactory;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 10
    check-cast p1, Lcom/squareup/ui/items/EditItemPhotoView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/EditItemPhotoView_MembersInjector;->injectMembers(Lcom/squareup/ui/items/EditItemPhotoView;)V

    return-void
.end method
