.class Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder$2;
.super Lcom/squareup/debounce/DebouncedOnEditorActionListener;
.source "EditModifierSetRecyclerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;->bindStaticTopRowContent()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;)V
    .locals 0

    .line 120
    iput-object p1, p0, Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder$2;->this$0:Lcom/squareup/ui/items/EditModifierSetRecyclerAdapter$ViewHolder;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnEditorActionListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doOnEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 0

    const/4 p3, 0x6

    if-ne p3, p2, :cond_0

    .line 123
    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method
