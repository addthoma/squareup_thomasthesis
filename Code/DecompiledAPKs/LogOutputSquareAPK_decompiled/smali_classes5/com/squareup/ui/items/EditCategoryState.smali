.class public Lcom/squareup/ui/items/EditCategoryState;
.super Ljava/lang/Object;
.source "EditCategoryState.java"

# interfaces
.implements Lmortar/bundler/Bundler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/EditCategoryState$CategoryDataFromLibrary;,
        Lcom/squareup/ui/items/EditCategoryState$CategoryData;
    }
.end annotation


# static fields
.field private static final CATEGORY_BUILDER_KEY:Ljava/lang/String; = "edit_category_category_builder"

.field private static final CATEGORY_ORIGINAL_KEY:Ljava/lang/String; = "edit_category_category_original"

.field private static final IS_NEW_CATEGORY_KEY:Ljava/lang/String; = "edit_category_is_new_category"

.field private static final NEW_ITEMS_KEY:Ljava/lang/String; = "edit_category_new_items"

.field private static final REMOVED_ITEMS_KEY:Ljava/lang/String; = "edit_category_removed_items"


# instance fields
.field private categoryData:Lcom/squareup/ui/items/EditCategoryState$CategoryData;

.field public categoryDataFromLibrary:Lcom/squareup/ui/items/EditCategoryState$CategoryDataFromLibrary;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static loadCatalogItemCategoryData(Landroid/os/Bundle;Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemCategory;
    .locals 0

    .line 141
    invoke-virtual {p0, p1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object p0

    .line 140
    invoke-static {p0}, Lcom/squareup/shared/catalog/models/CatalogObjectType$Adapter;->objectFromByteArray([B)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object p0

    check-cast p0, Lcom/squareup/shared/catalog/models/CatalogItemCategory;

    return-object p0
.end method

.method private static loadCategoryAssignmentData(Landroid/os/Bundle;Ljava/lang/String;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 145
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 146
    invoke-virtual {p0, p1}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    return-object v0
.end method

.method private static saveCatalogItemCategoryData(Landroid/os/Bundle;Lcom/squareup/shared/catalog/models/CatalogItemCategory;Ljava/lang/String;)V
    .locals 0

    .line 152
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemCategory;->toByteArray()[B

    move-result-object p1

    invoke-virtual {p0, p2, p1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    return-void
.end method

.method private static saveCategoryAssignmentData(Landroid/os/Bundle;Ljava/util/Set;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 156
    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    .line 158
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v1, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    add-int/lit8 v3, v1, 0x1

    .line 159
    aput-object v2, v0, v1

    move v1, v3

    goto :goto_0

    .line 161
    :cond_0
    invoke-virtual {p0, p2, v0}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getAbbreviation()Ljava/lang/String;
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryState;->categoryData:Lcom/squareup/ui/items/EditCategoryState$CategoryData;

    invoke-static {v0}, Lcom/squareup/ui/items/EditCategoryState$CategoryData;->access$000(Lcom/squareup/ui/items/EditCategoryState$CategoryData;)Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;->getAbbreviation()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAddedItems()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 29
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryState;->categoryData:Lcom/squareup/ui/items/EditCategoryState$CategoryData;

    invoke-static {v0}, Lcom/squareup/ui/items/EditCategoryState$CategoryData;->access$100(Lcom/squareup/ui/items/EditCategoryState$CategoryData;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getCategory()Lcom/squareup/shared/catalog/models/CatalogItemCategory;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryState;->categoryData:Lcom/squareup/ui/items/EditCategoryState$CategoryData;

    invoke-static {v0}, Lcom/squareup/ui/items/EditCategoryState$CategoryData;->access$000(Lcom/squareup/ui/items/EditCategoryState$CategoryData;)Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItemCategory;

    move-result-object v0

    return-object v0
.end method

.method public getColor()Ljava/lang/String;
    .locals 1

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryState;->categoryData:Lcom/squareup/ui/items/EditCategoryState$CategoryData;

    invoke-static {v0}, Lcom/squareup/ui/items/EditCategoryState$CategoryData;->access$000(Lcom/squareup/ui/items/EditCategoryState$CategoryData;)Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;->getColor()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMortarBundleKey()Ljava/lang/String;
    .locals 1

    .line 110
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryState;->categoryData:Lcom/squareup/ui/items/EditCategoryState$CategoryData;

    invoke-static {v0}, Lcom/squareup/ui/items/EditCategoryState$CategoryData;->access$000(Lcom/squareup/ui/items/EditCategoryState$CategoryData;)Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRemovedItems()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 33
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryState;->categoryData:Lcom/squareup/ui/items/EditCategoryState$CategoryData;

    invoke-static {v0}, Lcom/squareup/ui/items/EditCategoryState$CategoryData;->access$200(Lcom/squareup/ui/items/EditCategoryState$CategoryData;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public hasCategoryChanged()Z
    .locals 2

    .line 37
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryState;->categoryData:Lcom/squareup/ui/items/EditCategoryState$CategoryData;

    if-eqz v0, :cond_1

    invoke-static {v0}, Lcom/squareup/ui/items/EditCategoryState$CategoryData;->access$000(Lcom/squareup/ui/items/EditCategoryState$CategoryData;)Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 40
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditCategoryState;->getCategory()Lcom/squareup/shared/catalog/models/CatalogItemCategory;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/items/EditCategoryState;->categoryData:Lcom/squareup/ui/items/EditCategoryState$CategoryData;

    invoke-static {v1}, Lcom/squareup/ui/items/EditCategoryState$CategoryData;->access$300(Lcom/squareup/ui/items/EditCategoryState$CategoryData;)Lcom/squareup/shared/catalog/models/CatalogItemCategory;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogItemCategory;->equals(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method public hasEditCategoryStateChanged()Z
    .locals 2

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryState;->categoryData:Lcom/squareup/ui/items/EditCategoryState$CategoryData;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 47
    :cond_0
    invoke-static {v0}, Lcom/squareup/ui/items/EditCategoryState$CategoryData;->access$100(Lcom/squareup/ui/items/EditCategoryState$CategoryData;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-gtz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryState;->categoryData:Lcom/squareup/ui/items/EditCategoryState$CategoryData;

    .line 48
    invoke-static {v0}, Lcom/squareup/ui/items/EditCategoryState$CategoryData;->access$200(Lcom/squareup/ui/items/EditCategoryState$CategoryData;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-gtz v0, :cond_1

    .line 49
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditCategoryState;->hasCategoryChanged()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v1, 0x1

    :cond_2
    return v1
.end method

.method public isDataLoaded()Z
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryState;->categoryData:Lcom/squareup/ui/items/EditCategoryState$CategoryData;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isNewCategory()Z
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryState;->categoryData:Lcom/squareup/ui/items/EditCategoryState$CategoryData;

    invoke-static {v0}, Lcom/squareup/ui/items/EditCategoryState$CategoryData;->access$400(Lcom/squareup/ui/items/EditCategoryState$CategoryData;)Z

    move-result v0

    return v0
.end method

.method public loadCategoryDataFromLibrary(Lcom/squareup/ui/items/EditCategoryState$CategoryDataFromLibrary;)V
    .locals 2

    .line 85
    iput-object p1, p0, Lcom/squareup/ui/items/EditCategoryState;->categoryDataFromLibrary:Lcom/squareup/ui/items/EditCategoryState$CategoryDataFromLibrary;

    .line 86
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditCategoryState;->isDataLoaded()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 89
    :cond_0
    new-instance v0, Lcom/squareup/ui/items/EditCategoryState$CategoryData;

    invoke-direct {v0}, Lcom/squareup/ui/items/EditCategoryState$CategoryData;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/items/EditCategoryState;->categoryData:Lcom/squareup/ui/items/EditCategoryState$CategoryData;

    .line 90
    iget-object p1, p1, Lcom/squareup/ui/items/EditCategoryState$CategoryDataFromLibrary;->category:Lcom/squareup/shared/catalog/models/CatalogItemCategory;

    if-nez p1, :cond_1

    .line 98
    new-instance p1, Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;

    invoke-direct {p1}, Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;-><init>()V

    const-string v0, ""

    .line 99
    invoke-virtual {p1, v0}, Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;->setName(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;

    move-result-object p1

    .line 100
    invoke-virtual {p1, v0}, Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;->setAbbreviation(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;

    move-result-object p1

    .line 101
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItemCategory;

    move-result-object p1

    goto :goto_0

    .line 103
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryState;->categoryData:Lcom/squareup/ui/items/EditCategoryState$CategoryData;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/ui/items/EditCategoryState$CategoryData;->access$402(Lcom/squareup/ui/items/EditCategoryState$CategoryData;Z)Z

    .line 105
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryState;->categoryData:Lcom/squareup/ui/items/EditCategoryState$CategoryData;

    new-instance v1, Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;

    invoke-direct {v1, p1}, Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;-><init>(Lcom/squareup/shared/catalog/models/CatalogItemCategory;)V

    invoke-static {v0, v1}, Lcom/squareup/ui/items/EditCategoryState$CategoryData;->access$002(Lcom/squareup/ui/items/EditCategoryState$CategoryData;Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;)Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;

    .line 106
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryState;->categoryData:Lcom/squareup/ui/items/EditCategoryState$CategoryData;

    invoke-static {v0, p1}, Lcom/squareup/ui/items/EditCategoryState$CategoryData;->access$302(Lcom/squareup/ui/items/EditCategoryState$CategoryData;Lcom/squareup/shared/catalog/models/CatalogItemCategory;)Lcom/squareup/shared/catalog/models/CatalogItemCategory;

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 168
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryState;->categoryDataFromLibrary:Lcom/squareup/ui/items/EditCategoryState$CategoryDataFromLibrary;

    if-eqz v0, :cond_0

    .line 169
    iget-object v0, v0, Lcom/squareup/ui/items/EditCategoryState$CategoryDataFromLibrary;->libraryCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->close()V

    :cond_0
    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 3

    if-nez p1, :cond_0

    return-void

    .line 117
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditCategoryState;->isDataLoaded()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "edit_category_category_builder"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 118
    new-instance v1, Lcom/squareup/ui/items/EditCategoryState$CategoryData;

    invoke-direct {v1}, Lcom/squareup/ui/items/EditCategoryState$CategoryData;-><init>()V

    iput-object v1, p0, Lcom/squareup/ui/items/EditCategoryState;->categoryData:Lcom/squareup/ui/items/EditCategoryState$CategoryData;

    .line 119
    iget-object v1, p0, Lcom/squareup/ui/items/EditCategoryState;->categoryData:Lcom/squareup/ui/items/EditCategoryState$CategoryData;

    new-instance v2, Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;

    .line 120
    invoke-static {p1, v0}, Lcom/squareup/ui/items/EditCategoryState;->loadCatalogItemCategoryData(Landroid/os/Bundle;Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemCategory;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;-><init>(Lcom/squareup/shared/catalog/models/CatalogItemCategory;)V

    .line 119
    invoke-static {v1, v2}, Lcom/squareup/ui/items/EditCategoryState$CategoryData;->access$002(Lcom/squareup/ui/items/EditCategoryState$CategoryData;Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;)Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;

    .line 121
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryState;->categoryData:Lcom/squareup/ui/items/EditCategoryState$CategoryData;

    const-string v1, "edit_category_category_original"

    invoke-static {p1, v1}, Lcom/squareup/ui/items/EditCategoryState;->loadCatalogItemCategoryData(Landroid/os/Bundle;Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemCategory;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/ui/items/EditCategoryState$CategoryData;->access$302(Lcom/squareup/ui/items/EditCategoryState$CategoryData;Lcom/squareup/shared/catalog/models/CatalogItemCategory;)Lcom/squareup/shared/catalog/models/CatalogItemCategory;

    .line 122
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryState;->categoryData:Lcom/squareup/ui/items/EditCategoryState$CategoryData;

    const-string v1, "edit_category_new_items"

    invoke-static {p1, v1}, Lcom/squareup/ui/items/EditCategoryState;->loadCategoryAssignmentData(Landroid/os/Bundle;Ljava/lang/String;)Ljava/util/Set;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/ui/items/EditCategoryState$CategoryData;->access$102(Lcom/squareup/ui/items/EditCategoryState$CategoryData;Ljava/util/Set;)Ljava/util/Set;

    .line 123
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryState;->categoryData:Lcom/squareup/ui/items/EditCategoryState$CategoryData;

    const-string v1, "edit_category_removed_items"

    invoke-static {p1, v1}, Lcom/squareup/ui/items/EditCategoryState;->loadCategoryAssignmentData(Landroid/os/Bundle;Ljava/lang/String;)Ljava/util/Set;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/ui/items/EditCategoryState$CategoryData;->access$202(Lcom/squareup/ui/items/EditCategoryState$CategoryData;Ljava/util/Set;)Ljava/util/Set;

    .line 124
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryState;->categoryData:Lcom/squareup/ui/items/EditCategoryState$CategoryData;

    const-string v1, "edit_category_is_new_category"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    invoke-static {v0, p1}, Lcom/squareup/ui/items/EditCategoryState$CategoryData;->access$402(Lcom/squareup/ui/items/EditCategoryState$CategoryData;Z)Z

    :cond_1
    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 129
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditCategoryState;->isDataLoaded()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 130
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryState;->categoryData:Lcom/squareup/ui/items/EditCategoryState$CategoryData;

    invoke-static {v0}, Lcom/squareup/ui/items/EditCategoryState$CategoryData;->access$300(Lcom/squareup/ui/items/EditCategoryState$CategoryData;)Lcom/squareup/shared/catalog/models/CatalogItemCategory;

    move-result-object v0

    const-string v1, "edit_category_category_original"

    invoke-static {p1, v0, v1}, Lcom/squareup/ui/items/EditCategoryState;->saveCatalogItemCategoryData(Landroid/os/Bundle;Lcom/squareup/shared/catalog/models/CatalogItemCategory;Ljava/lang/String;)V

    .line 131
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryState;->categoryData:Lcom/squareup/ui/items/EditCategoryState$CategoryData;

    invoke-static {v0}, Lcom/squareup/ui/items/EditCategoryState$CategoryData;->access$000(Lcom/squareup/ui/items/EditCategoryState$CategoryData;)Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItemCategory;

    move-result-object v0

    const-string v1, "edit_category_category_builder"

    invoke-static {p1, v0, v1}, Lcom/squareup/ui/items/EditCategoryState;->saveCatalogItemCategoryData(Landroid/os/Bundle;Lcom/squareup/shared/catalog/models/CatalogItemCategory;Ljava/lang/String;)V

    .line 133
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryState;->categoryData:Lcom/squareup/ui/items/EditCategoryState$CategoryData;

    invoke-static {v0}, Lcom/squareup/ui/items/EditCategoryState$CategoryData;->access$100(Lcom/squareup/ui/items/EditCategoryState$CategoryData;)Ljava/util/Set;

    move-result-object v0

    const-string v1, "edit_category_new_items"

    invoke-static {p1, v0, v1}, Lcom/squareup/ui/items/EditCategoryState;->saveCategoryAssignmentData(Landroid/os/Bundle;Ljava/util/Set;Ljava/lang/String;)V

    .line 134
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryState;->categoryData:Lcom/squareup/ui/items/EditCategoryState$CategoryData;

    invoke-static {v0}, Lcom/squareup/ui/items/EditCategoryState$CategoryData;->access$200(Lcom/squareup/ui/items/EditCategoryState$CategoryData;)Ljava/util/Set;

    move-result-object v0

    const-string v1, "edit_category_removed_items"

    invoke-static {p1, v0, v1}, Lcom/squareup/ui/items/EditCategoryState;->saveCategoryAssignmentData(Landroid/os/Bundle;Ljava/util/Set;Ljava/lang/String;)V

    .line 135
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryState;->categoryData:Lcom/squareup/ui/items/EditCategoryState$CategoryData;

    invoke-static {v0}, Lcom/squareup/ui/items/EditCategoryState$CategoryData;->access$400(Lcom/squareup/ui/items/EditCategoryState$CategoryData;)Z

    move-result v0

    const-string v1, "edit_category_is_new_category"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_0
    return-void
.end method

.method public setAbbreviation(Ljava/lang/String;)V
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryState;->categoryData:Lcom/squareup/ui/items/EditCategoryState$CategoryData;

    invoke-static {v0}, Lcom/squareup/ui/items/EditCategoryState$CategoryData;->access$000(Lcom/squareup/ui/items/EditCategoryState$CategoryData;)Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;->setAbbreviation(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;

    return-void
.end method

.method public setColor(Ljava/lang/String;)V
    .locals 1

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryState;->categoryData:Lcom/squareup/ui/items/EditCategoryState$CategoryData;

    invoke-static {v0}, Lcom/squareup/ui/items/EditCategoryState$CategoryData;->access$000(Lcom/squareup/ui/items/EditCategoryState$CategoryData;)Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;->setColor(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryState;->categoryData:Lcom/squareup/ui/items/EditCategoryState$CategoryData;

    invoke-static {v0}, Lcom/squareup/ui/items/EditCategoryState$CategoryData;->access$000(Lcom/squareup/ui/items/EditCategoryState$CategoryData;)Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;->setName(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;

    return-void
.end method
