.class final Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$skuValidationRunnable$1;
.super Lkotlin/jvm/internal/Lambda;
.source "EditItemVariationCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/EditItemVariationCoordinator;->configureInputFields(Landroid/view/View;Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $variationId:Ljava/lang/String;

.field final synthetic this$0:Lcom/squareup/ui/items/EditItemVariationCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/EditItemVariationCoordinator;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$skuValidationRunnable$1;->this$0:Lcom/squareup/ui/items/EditItemVariationCoordinator;

    iput-object p2, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$skuValidationRunnable$1;->$variationId:Ljava/lang/String;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 42
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$skuValidationRunnable$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 4

    .line 184
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$skuValidationRunnable$1;->this$0:Lcom/squareup/ui/items/EditItemVariationCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/items/EditItemVariationCoordinator;->access$getDuplicateSkuValidator$p(Lcom/squareup/ui/items/EditItemVariationCoordinator;)Lcom/squareup/ui/items/DuplicateSkuValidator;

    move-result-object v0

    .line 185
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$skuValidationRunnable$1;->$variationId:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$skuValidationRunnable$1;->this$0:Lcom/squareup/ui/items/EditItemVariationCoordinator;

    invoke-static {v2}, Lcom/squareup/ui/items/EditItemVariationCoordinator;->access$getNameEditText$p(Lcom/squareup/ui/items/EditItemVariationCoordinator;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$skuValidationRunnable$1;->this$0:Lcom/squareup/ui/items/EditItemVariationCoordinator;

    invoke-static {v3}, Lcom/squareup/ui/items/EditItemVariationCoordinator;->access$getSkuEditText$p(Lcom/squareup/ui/items/EditItemVariationCoordinator;)Landroid/widget/EditText;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 184
    invoke-virtual {v0, v1, v2, v3}, Lcom/squareup/ui/items/DuplicateSkuValidator;->validateSkuAndUpdateErrorsBarForSingleVariation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
