.class public interface abstract Lcom/squareup/ui/items/DetailSearchableListEditHandler;
.super Ljava/lang/Object;
.source "DetailSearchableListEditHandler.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001JF\u0010\u0002\u001a$\u0012\u0004\u0012\u00020\u0004\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0005j\u0002`\u00060\u0003j\u0008\u0012\u0004\u0012\u00020\u0004`\u00072\u0012\u0010\u0008\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b0\t2\u0006\u0010\u000c\u001a\u00020\rH&\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/ui/items/DetailSearchableListEditHandler;",
        "",
        "renderEdit",
        "",
        "Lcom/squareup/workflow/MainAndModal;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "Lcom/squareup/ui/items/DetailSearchableListState;",
        "Lcom/squareup/ui/items/DetailSearchableListResult;",
        "state",
        "Lcom/squareup/ui/items/DetailSearchableListState$Edit;",
        "items-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract renderEdit(Lcom/squareup/workflow/RenderContext;Lcom/squareup/ui/items/DetailSearchableListState$Edit;)Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/ui/items/DetailSearchableListState;",
            "-",
            "Lcom/squareup/ui/items/DetailSearchableListResult;",
            ">;",
            "Lcom/squareup/ui/items/DetailSearchableListState$Edit;",
            ")",
            "Ljava/util/Map<",
            "Lcom/squareup/workflow/MainAndModal;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation
.end method
