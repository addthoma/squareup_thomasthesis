.class public Lcom/squareup/ui/items/EditItemEventLogger;
.super Ljava/lang/Object;
.source "EditItemEventLogger.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditItemEventLogger.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditItemEventLogger.kt\ncom/squareup/ui/items/EditItemEventLogger\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,140:1\n1642#2,2:141\n1642#2,2:143\n1642#2,2:145\n1642#2,2:147\n1642#2:149\n704#2:150\n777#2,2:151\n1642#2,2:153\n1643#2:155\n*E\n*S KotlinDebug\n*F\n+ 1 EditItemEventLogger.kt\ncom/squareup/ui/items/EditItemEventLogger\n*L\n33#1,2:141\n43#1,2:143\n48#1,2:145\n120#1,2:147\n128#1:149\n128#1:150\n128#1,2:151\n128#1,2:153\n128#1:155\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0016\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J$\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0012\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u000b0\nH\u0016J8\u0010\u000c\u001a\u00020\u00062\u0006\u0010\r\u001a\u00020\u000b2\u0008\u0010\u000e\u001a\u0004\u0018\u00010\u000f2\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u000b2\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u000f2\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u000bH\u0016J$\u0010\u0013\u001a\u00020\u00062\u0006\u0010\u0014\u001a\u00020\u00152\u0012\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u000f0\nH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/ui/items/EditItemEventLogger;",
        "",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "(Lcom/squareup/analytics/Analytics;)V",
        "logItemOptionChanges",
        "",
        "changedItemOptions",
        "Lcom/squareup/ui/items/ItemOptionData$ChangedItemOptions;",
        "serverIdsByClientIds",
        "",
        "",
        "logMeasurementUnitAssignment",
        "variationId",
        "previousMeasurementUnit",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;",
        "previousMeasurementUnitToken",
        "newMeasurementUnit",
        "newMeasurementUnitToken",
        "logVariationChanges",
        "editItemVariationsState",
        "Lcom/squareup/catalog/EditItemVariationsState;",
        "measurementUnits",
        "edit-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/Analytics;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "analytics"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/EditItemEventLogger;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method


# virtual methods
.method public logItemOptionChanges(Lcom/squareup/ui/items/ItemOptionData$ChangedItemOptions;Ljava/util/Map;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/items/ItemOptionData$ChangedItemOptions;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const-string v0, "changedItemOptions"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "serverIdsByClientIds"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 120
    invoke-virtual {p1}, Lcom/squareup/ui/items/ItemOptionData$ChangedItemOptions;->getNewItemOptions()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 147
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;

    .line 121
    sget-object v2, Lcom/squareup/catalog/event/CatalogFeature;->OPTION_SET_CREATED:Lcom/squareup/catalog/event/CatalogFeature;

    .line 122
    iget-object v3, p0, Lcom/squareup/ui/items/EditItemEventLogger;->analytics:Lcom/squareup/analytics/Analytics;

    .line 123
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v8, 0xc

    const/4 v9, 0x0

    const-string v7, "Local"

    .line 121
    invoke-static/range {v2 .. v9}, Lcom/squareup/catalog/event/CatalogFeature;->log$default(Lcom/squareup/catalog/event/CatalogFeature;Lcom/squareup/analytics/Analytics;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V

    goto :goto_0

    .line 128
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/ui/items/ItemOptionData$ChangedItemOptions;->getEditedItemOptions()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 149
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;

    .line 129
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;->getAllValues()Ljava/util/List;

    move-result-object v1

    const-string v2, "editedItemOption.allValues"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Iterable;

    .line 150
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    check-cast v2, Ljava/util/Collection;

    .line 151
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v4, v3

    check-cast v4, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;

    .line 129
    invoke-interface {p2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v5

    const-string v6, "it"

    invoke-static {v4, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;->getUid()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v5, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 152
    :cond_3
    check-cast v2, Ljava/util/List;

    check-cast v2, Ljava/lang/Iterable;

    .line 153
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;

    .line 131
    sget-object v3, Lcom/squareup/catalog/event/CatalogFeature;->OPTION_ADDED_TO_OPTION_SET:Lcom/squareup/catalog/event/CatalogFeature;

    iget-object v4, p0, Lcom/squareup/ui/items/EditItemEventLogger;->analytics:Lcom/squareup/analytics/Analytics;

    const-string v5, "newValue"

    .line 132
    invoke-static {v2, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;->getUid()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v5, v2

    check-cast v5, Ljava/lang/String;

    .line 133
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;->getAllValues()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 134
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;->getId()Ljava/lang/String;

    move-result-object v7

    const-string v8, "Local"

    .line 131
    invoke-virtual/range {v3 .. v8}, Lcom/squareup/catalog/event/CatalogFeature;->log(Lcom/squareup/analytics/Analytics;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_4
    return-void
.end method

.method public logMeasurementUnitAssignment(Ljava/lang/String;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Ljava/lang/String;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Ljava/lang/String;)V
    .locals 13

    move-object v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p5

    const-string/jumbo v3, "variationId"

    move-object v12, p1

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    move-object v3, v1

    check-cast v3, Ljava/lang/CharSequence;

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-eqz v3, :cond_1

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-nez v3, :cond_0

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v3, 0x1

    :goto_1
    xor-int/2addr v3, v5

    .line 75
    move-object v6, v2

    check-cast v6, Ljava/lang/CharSequence;

    if-eqz v6, :cond_3

    invoke-interface {v6}, Ljava/lang/CharSequence;->length()I

    move-result v6

    if-nez v6, :cond_2

    goto :goto_2

    :cond_2
    const/4 v6, 0x0

    goto :goto_3

    :cond_3
    :goto_2
    const/4 v6, 0x1

    :goto_3
    xor-int/2addr v5, v6

    if-eqz v5, :cond_6

    if-eqz p4, :cond_5

    if-eqz v3, :cond_4

    const/4 v5, 0x2

    const/4 v6, 0x0

    .line 85
    invoke-static {v1, v2, v4, v5, v6}, Lkotlin/text/StringsKt;->equals$default(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 86
    sget-object v4, Lcom/squareup/catalog/event/CatalogFeature;->VARIATION_CHANGED_UNIT:Lcom/squareup/catalog/event/CatalogFeature;

    .line 87
    iget-object v5, v0, Lcom/squareup/ui/items/EditItemEventLogger;->analytics:Lcom/squareup/analytics/Analytics;

    .line 89
    invoke-virtual/range {p4 .. p4}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getPrecision()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    .line 90
    invoke-static/range {p4 .. p4}, Lcom/squareup/items/unit/EditUnitEventLoggerKt;->unitEventDetail(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    const/16 v10, 0x10

    const/4 v11, 0x0

    move-object v6, p1

    .line 86
    invoke-static/range {v4 .. v11}, Lcom/squareup/catalog/event/CatalogFeature;->log$default(Lcom/squareup/catalog/event/CatalogFeature;Lcom/squareup/analytics/Analytics;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V

    :cond_4
    if-nez v3, :cond_8

    .line 94
    sget-object v4, Lcom/squareup/catalog/event/CatalogFeature;->VARIATION_ADDED_UNIT:Lcom/squareup/catalog/event/CatalogFeature;

    .line 95
    iget-object v5, v0, Lcom/squareup/ui/items/EditItemEventLogger;->analytics:Lcom/squareup/analytics/Analytics;

    .line 97
    invoke-virtual/range {p4 .. p4}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getPrecision()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    .line 98
    invoke-static/range {p4 .. p4}, Lcom/squareup/items/unit/EditUnitEventLoggerKt;->unitEventDetail(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    const/16 v10, 0x10

    const/4 v11, 0x0

    move-object v6, p1

    .line 94
    invoke-static/range {v4 .. v11}, Lcom/squareup/catalog/event/CatalogFeature;->log$default(Lcom/squareup/catalog/event/CatalogFeature;Lcom/squareup/analytics/Analytics;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V

    goto :goto_4

    .line 80
    :cond_5
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Measurement unit token set, but no new measurement unit present."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    :cond_6
    if-eqz v3, :cond_8

    if-eqz p2, :cond_7

    .line 107
    sget-object v4, Lcom/squareup/catalog/event/CatalogFeature;->VARIATION_REMOVED_UNIT:Lcom/squareup/catalog/event/CatalogFeature;

    .line 108
    iget-object v5, v0, Lcom/squareup/ui/items/EditItemEventLogger;->analytics:Lcom/squareup/analytics/Analytics;

    .line 110
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getPrecision()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    .line 111
    invoke-static {p2}, Lcom/squareup/items/unit/EditUnitEventLoggerKt;->unitEventDetail(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    const/16 v10, 0x10

    const/4 v11, 0x0

    move-object v6, p1

    .line 107
    invoke-static/range {v4 .. v11}, Lcom/squareup/catalog/event/CatalogFeature;->log$default(Lcom/squareup/catalog/event/CatalogFeature;Lcom/squareup/analytics/Analytics;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V

    goto :goto_4

    .line 103
    :cond_7
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Measurement unit token set, but no current measurement unit present."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    :cond_8
    :goto_4
    return-void
.end method

.method public logVariationChanges(Lcom/squareup/catalog/EditItemVariationsState;Ljava/util/Map;)V
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/catalog/EditItemVariationsState;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;",
            ">;)V"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    const-string v3, "editItemVariationsState"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "measurementUnits"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    iget-object v3, v1, Lcom/squareup/catalog/EditItemVariationsState;->deleted:Ljava/util/List;

    const-string v4, "editItemVariationsState.deleted"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Ljava/lang/Iterable;

    .line 141
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    const/4 v5, 0x0

    const/4 v6, 0x1

    const-string/jumbo v7, "variation"

    const-string/jumbo v8, "variation.id"

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    .line 34
    sget-object v9, Lcom/squareup/catalog/event/CatalogFeature;->VARIATION_DELETED:Lcom/squareup/catalog/event/CatalogFeature;

    iget-object v10, v0, Lcom/squareup/ui/items/EditItemEventLogger;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {v4, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v9, v10, v7}, Lcom/squareup/catalog/event/CatalogFeature;->log(Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V

    .line 36
    invoke-virtual {v4}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getAllItemOptionValues()Ljava/util/List;

    move-result-object v7

    check-cast v7, Ljava/util/Collection;

    if-eqz v7, :cond_1

    invoke-interface {v7}, Ljava/util/Collection;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_2

    :cond_1
    const/4 v5, 0x1

    :cond_2
    if-nez v5, :cond_0

    .line 37
    sget-object v5, Lcom/squareup/catalog/event/CatalogFeature;->VARIATION_DELETED_WITH_OPTIONS:Lcom/squareup/catalog/event/CatalogFeature;

    .line 38
    iget-object v6, v0, Lcom/squareup/ui/items/EditItemEventLogger;->analytics:Lcom/squareup/analytics/Analytics;

    .line 39
    invoke-virtual {v4}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getMerchantCatalogObjectToken()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v7, "variation.merchantCatalogObjectToken"

    invoke-static {v4, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-virtual {v5, v6, v4}, Lcom/squareup/catalog/event/CatalogFeature;->log(Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V

    goto :goto_0

    .line 43
    :cond_3
    iget-object v3, v1, Lcom/squareup/catalog/EditItemVariationsState;->edited:Ljava/util/List;

    const-string v4, "editItemVariationsState.edited"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Ljava/lang/Iterable;

    .line 143
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    .line 44
    sget-object v9, Lcom/squareup/catalog/event/CatalogFeature;->VARIATION_EDITED:Lcom/squareup/catalog/event/CatalogFeature;

    .line 45
    iget-object v10, v0, Lcom/squareup/ui/items/EditItemEventLogger;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {v4, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-virtual {v9, v10, v4}, Lcom/squareup/catalog/event/CatalogFeature;->log(Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V

    goto :goto_1

    .line 48
    :cond_4
    iget-object v1, v1, Lcom/squareup/catalog/EditItemVariationsState;->created:Ljava/util/List;

    const-string v3, "editItemVariationsState.created"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Iterable;

    .line 145
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_5
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    .line 49
    sget-object v4, Lcom/squareup/catalog/event/CatalogFeature;->VARIATION_CREATED:Lcom/squareup/catalog/event/CatalogFeature;

    iget-object v9, v0, Lcom/squareup/ui/items/EditItemEventLogger;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {v3, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getId()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4, v9, v10}, Lcom/squareup/catalog/event/CatalogFeature;->log(Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V

    .line 50
    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getCatalogMeasurementUnitToken()Ljava/lang/String;

    move-result-object v4

    .line 51
    move-object v9, v4

    check-cast v9, Ljava/lang/CharSequence;

    if-eqz v9, :cond_7

    invoke-interface {v9}, Ljava/lang/CharSequence;->length()I

    move-result v9

    if-nez v9, :cond_6

    goto :goto_3

    :cond_6
    const/4 v9, 0x0

    goto :goto_4

    :cond_7
    :goto_3
    const/4 v9, 0x1

    :goto_4
    if-nez v9, :cond_9

    .line 52
    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    if-eqz v4, :cond_8

    .line 54
    invoke-static {v4}, Lcom/squareup/items/unit/EditUnitEventLoggerKt;->unitEventDetail(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)Ljava/lang/String;

    move-result-object v13

    .line 55
    sget-object v9, Lcom/squareup/catalog/event/CatalogFeature;->VARIATION_CREATED_WITH_UNIT:Lcom/squareup/catalog/event/CatalogFeature;

    iget-object v10, v0, Lcom/squareup/ui/items/EditItemEventLogger;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getId()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getPrecision()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    const/4 v14, 0x0

    const/16 v15, 0x10

    const/16 v16, 0x0

    invoke-static/range {v9 .. v16}, Lcom/squareup/catalog/event/CatalogFeature;->log$default(Lcom/squareup/catalog/event/CatalogFeature;Lcom/squareup/analytics/Analytics;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V

    goto :goto_5

    .line 53
    :cond_8
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "No measurement unit matching variation unit token"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    .line 58
    :cond_9
    :goto_5
    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getAllItemOptionValues()Ljava/util/List;

    move-result-object v4

    check-cast v4, Ljava/util/Collection;

    if-eqz v4, :cond_b

    invoke-interface {v4}, Ljava/util/Collection;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_a

    goto :goto_6

    :cond_a
    const/4 v4, 0x0

    goto :goto_7

    :cond_b
    :goto_6
    const/4 v4, 0x1

    :goto_7
    if-nez v4, :cond_5

    .line 59
    sget-object v4, Lcom/squareup/catalog/event/CatalogFeature;->VARIATION_CREATED_WITH_OPTIONS:Lcom/squareup/catalog/event/CatalogFeature;

    .line 60
    iget-object v9, v0, Lcom/squareup/ui/items/EditItemEventLogger;->analytics:Lcom/squareup/analytics/Analytics;

    .line 61
    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    invoke-virtual {v4, v9, v3}, Lcom/squareup/catalog/event/CatalogFeature;->log(Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_c
    return-void
.end method
