.class final Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$onEnterScope$2$3;
.super Ljava/lang/Object;
.source "DetailSearchableListWorkflowRunner.kt"

# interfaces
.implements Lcom/squareup/shared/catalog/CatalogCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$onEnterScope$2;->accept(Lcom/squareup/ui/items/DetailSearchableListResult;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/shared/catalog/CatalogCallback<",
        "Ljava/util/List<",
        "Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\u0010\u0000\u001a\u00020\u00012b\u0010\u0002\u001a^\u0012(\u0012&\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005 \u0006*\u0012\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005\u0018\u00010\u00070\u0004 \u0006*.\u0012(\u0012&\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005 \u0006*\u0012\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005\u0018\u00010\u00070\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "result",
        "Lcom/squareup/shared/catalog/CatalogResult;",
        "",
        "Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;",
        "kotlin.jvm.PlatformType",
        "",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $it:Lcom/squareup/ui/items/DetailSearchableListResult;

.field final synthetic this$0:Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$onEnterScope$2;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$onEnterScope$2;Lcom/squareup/ui/items/DetailSearchableListResult;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$onEnterScope$2$3;->this$0:Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$onEnterScope$2;

    iput-object p2, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$onEnterScope$2$3;->$it:Lcom/squareup/ui/items/DetailSearchableListResult;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/shared/catalog/CatalogResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/CatalogResult<",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;",
            ">;>;)V"
        }
    .end annotation

    .line 122
    invoke-interface {p1}, Lcom/squareup/shared/catalog/CatalogResult;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    .line 123
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$onEnterScope$2$3;->this$0:Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$onEnterScope$2;

    iget-object v0, v0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$onEnterScope$2;->this$0:Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;

    invoke-static {v0}, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->access$getItemsAppletScopeRunner$p(Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;)Lcom/squareup/ui/items/ItemsAppletScopeRunner;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$onEnterScope$2$3;->$it:Lcom/squareup/ui/items/DetailSearchableListResult;

    check-cast v1, Lcom/squareup/ui/items/DetailSearchableListResult$GoToHandleScannedBarcode;

    invoke-virtual {v1}, Lcom/squareup/ui/items/DetailSearchableListResult$GoToHandleScannedBarcode;->getBarcode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/ui/items/ItemsAppletScopeRunner;->onResultOfVariationsMatchingBarcode(Ljava/util/List;Ljava/lang/String;)V

    return-void
.end method
