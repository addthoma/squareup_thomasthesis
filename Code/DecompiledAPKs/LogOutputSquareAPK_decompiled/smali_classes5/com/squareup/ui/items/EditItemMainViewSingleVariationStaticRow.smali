.class public Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;
.super Lcom/squareup/widgets/ResponsiveLinearLayout;
.source "EditItemMainViewSingleVariationStaticRow.java"


# static fields
.field private static final SKU_SEARCH_DELAY_MS:J = 0xc8L


# instance fields
.field private defaultVariationContainer:Landroid/view/View;

.field moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field presenter:Lcom/squareup/ui/items/EditItemMainPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private priceEditScrubbingWatcher:Lcom/squareup/text/ScrubbingTextWatcher;

.field private priceEditText:Lcom/squareup/noho/NohoEditText;

.field private priceEditTextWatcher:Landroid/text/TextWatcher;

.field priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private priceSkuContainer:Landroid/view/View;

.field res:Lcom/squareup/util/Res;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private skuEditText:Lcom/squareup/noho/NohoEditText;

.field private final skuValidationRunnable:Ljava/lang/Runnable;

.field private stockCountRowForDefaultVariation:Lcom/squareup/ui/items/widgets/StockCountRow;

.field private subscriptions:Lrx/subscriptions/CompositeSubscription;

.field private unitTypeSelector:Lcom/squareup/noho/NohoRow;

.field private unitTypeSelectorHelpText:Lcom/squareup/widgets/MessageView;

.field private variationHeader:Lcom/squareup/marketfont/MarketTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 63
    invoke-direct {p0, p1, p2}, Lcom/squareup/widgets/ResponsiveLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 64
    new-instance p1, Lcom/squareup/ui/items/-$$Lambda$EditItemMainViewSingleVariationStaticRow$v5_tY8uNyUnsggXQashVTjWGm9c;

    invoke-direct {p1, p0}, Lcom/squareup/ui/items/-$$Lambda$EditItemMainViewSingleVariationStaticRow$v5_tY8uNyUnsggXQashVTjWGm9c;-><init>(Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;)V

    iput-object p1, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->skuValidationRunnable:Ljava/lang/Runnable;

    .line 69
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->getContext()Landroid/content/Context;

    move-result-object p1

    const-class p2, Lcom/squareup/ui/items/EditItemMainScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/EditItemMainScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/items/EditItemMainScreen$Component;->inject(Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;)Ljava/lang/Runnable;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->skuValidationRunnable:Ljava/lang/Runnable;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;)Lcom/squareup/noho/NohoEditText;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->priceEditText:Lcom/squareup/noho/NohoEditText;

    return-object p0
.end method

.method public static synthetic lambda$__xmk9Cqz0GRgj1xIpI-hf-vPbQ(Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->setScannedSku(Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic lambda$z5kO7lW088EsfjzyCp5wlED0xf4(Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->setSkuTextRed(Z)V

    return-void
.end method

.method private setAlwaysEditableContent(Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)V
    .locals 6

    .line 154
    iget-boolean v0, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->shouldShowInlineVariation:Z

    if-eqz v0, :cond_4

    .line 155
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->variationHeader:Lcom/squareup/marketfont/MarketTextView;

    sget v1, Lcom/squareup/edititem/R$string;->uppercase_price_and_inventory:I

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setText(I)V

    .line 156
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->defaultVariationContainer:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 157
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->skuEditText:Lcom/squareup/noho/NohoEditText;

    iget-object v1, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->defaultVariation:Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getSku()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditText;->setText(Ljava/lang/CharSequence;)V

    .line 159
    iget-object v0, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->defaultVariation:Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getCatalogMeasurementUnitToken()Ljava/lang/String;

    move-result-object v0

    .line 161
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 162
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->res:Lcom/squareup/util/Res;

    iget-object v2, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->measurementUnits:Ljava/util/Map;

    .line 163
    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    .line 162
    invoke-static {v1, v0}, Lcom/squareup/quantity/UnitDisplayData;->fromCatalogMeasurementUnit(Lcom/squareup/util/Res;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)Lcom/squareup/quantity/UnitDisplayData;

    move-result-object v0

    .line 164
    invoke-virtual {v0}, Lcom/squareup/quantity/UnitDisplayData;->getUnitAbbreviation()Ljava/lang/String;

    move-result-object v1

    .line 165
    iget-object v2, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->unitTypeSelector:Lcom/squareup/noho/NohoRow;

    iget-object v3, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/edititem/R$string;->edit_item_unit_type_value:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v3

    .line 166
    invoke-virtual {v0}, Lcom/squareup/quantity/UnitDisplayData;->getUnitName()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "unit_name"

    invoke-virtual {v3, v5, v4}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->res:Lcom/squareup/util/Res;

    .line 167
    invoke-virtual {v0, v4}, Lcom/squareup/quantity/UnitDisplayData;->getQuantityPrecisionHint(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v0

    const-string v4, "precision"

    invoke-virtual {v3, v4, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 168
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 165
    invoke-virtual {v2, v0}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 171
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->unitTypeSelector:Lcom/squareup/noho/NohoRow;

    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/edititem/R$string;->edit_item_unit_type_default:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    const-string v1, ""

    .line 174
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->priceEditScrubbingWatcher:Lcom/squareup/text/ScrubbingTextWatcher;

    if-eqz v0, :cond_1

    .line 175
    iget-object v2, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->priceEditText:Lcom/squareup/noho/NohoEditText;

    invoke-virtual {v2, v0}, Lcom/squareup/noho/NohoEditText;->removeSelectionWatcher(Lcom/squareup/text/SelectionWatcher;)V

    .line 176
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->priceEditText:Lcom/squareup/noho/NohoEditText;

    iget-object v2, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->priceEditScrubbingWatcher:Lcom/squareup/text/ScrubbingTextWatcher;

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 178
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iget-object v2, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->priceEditText:Lcom/squareup/noho/NohoEditText;

    sget-object v3, Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;->BLANK_ON_ZERO:Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;

    invoke-virtual {v0, v2, v3, v1}, Lcom/squareup/money/PriceLocaleHelper;->configure(Lcom/squareup/text/HasSelectableText;Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;Ljava/lang/String;)Lcom/squareup/text/ScrubbingTextWatcher;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->priceEditScrubbingWatcher:Lcom/squareup/text/ScrubbingTextWatcher;

    .line 180
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->priceEditTextWatcher:Landroid/text/TextWatcher;

    if-eqz v0, :cond_2

    .line 181
    iget-object v2, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->priceEditText:Lcom/squareup/noho/NohoEditText;

    invoke-virtual {v2, v0}, Lcom/squareup/noho/NohoEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 183
    :cond_2
    new-instance v0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow$2;

    invoke-direct {v0, p0, v1}, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow$2;-><init>(Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->priceEditTextWatcher:Landroid/text/TextWatcher;

    .line 191
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->priceEditText:Lcom/squareup/noho/NohoEditText;

    iget-object v2, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->priceEditTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 192
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iget-object v2, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->priceEditText:Lcom/squareup/noho/NohoEditText;

    invoke-virtual {v0, v2, v1}, Lcom/squareup/money/PriceLocaleHelper;->setHintToZeroMoney(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 193
    iget-object v0, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->defaultVariation:Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->isVariablePricing()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 194
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->priceEditText:Lcom/squareup/noho/NohoEditText;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoEditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 196
    :cond_3
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->priceEditText:Lcom/squareup/noho/NohoEditText;

    iget-object v2, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iget-object p1, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->defaultVariation:Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getPrice()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-virtual {v2, p1, v1}, Lcom/squareup/quantity/PerUnitFormatter;->format(Lcom/squareup/protos/common/Money;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoEditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 200
    :cond_4
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->variationHeader:Lcom/squareup/marketfont/MarketTextView;

    sget v0, Lcom/squareup/edititem/R$string;->uppercase_variations:I

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketTextView;->setText(I)V

    .line 201
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->defaultVariationContainer:Landroid/view/View;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    return-void
.end method

.method private setScannedSku(Ljava/lang/String;)V
    .locals 1

    .line 150
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->skuEditText:Lcom/squareup/noho/NohoEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoEditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private setSkuTextRed(Z)V
    .locals 2

    .line 144
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->skuEditText:Lcom/squareup/noho/NohoEditText;

    .line 145
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    if-eqz p1, :cond_0

    sget p1, Lcom/squareup/marin/R$color;->marin_red:I

    goto :goto_0

    :cond_0
    sget p1, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    :goto_0
    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    .line 144
    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoEditText;->setTextColor(I)V

    return-void
.end method

.method private showStockCountForDefaultVariation(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;ILjava/lang/String;)Lrx/Subscription;
    .locals 2

    .line 207
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->stockCountRowForDefaultVariation:Lcom/squareup/ui/items/widgets/StockCountRow;

    invoke-virtual {v0, v1, p1, p2, p3}, Lcom/squareup/ui/items/EditItemMainPresenter;->attachStockCountRowActionToStockCountRow(Lcom/squareup/ui/items/widgets/StockCountRow;Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;ILjava/lang/String;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method getAllSubscriptionsOnTheRow()Lrx/Subscription;
    .locals 1

    .line 135
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->subscriptions:Lrx/subscriptions/CompositeSubscription;

    return-object v0
.end method

.method public synthetic lambda$new$0$EditItemMainViewSingleVariationStaticRow()V
    .locals 2

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->priceSkuContainer:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->skuEditText:Lcom/squareup/noho/NohoEditText;

    invoke-virtual {v1}, Lcom/squareup/noho/NohoEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/items/EditItemMainPresenter;->defaultSkuChanged(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public synthetic lambda$onFinishInflate$1$EditItemMainViewSingleVariationStaticRow(Landroid/view/View;Z)V
    .locals 0

    .line 84
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/items/EditItemMainPresenter;->priceFieldFocusChanged(Z)V

    return-void
.end method

.method public synthetic lambda$onFinishInflate$2$EditItemMainViewSingleVariationStaticRow(Landroid/view/View;)V
    .locals 0

    .line 105
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemMainPresenter;->unitPriceTypeButtonClicked()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 139
    invoke-super {p0}, Lcom/squareup/widgets/ResponsiveLinearLayout;->onDetachedFromWindow()V

    .line 140
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->skuValidationRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->removeCallbacks(Ljava/lang/Runnable;)Z

    return-void
.end method

.method protected onFinishInflate()V
    .locals 4

    .line 73
    invoke-super {p0}, Lcom/squareup/widgets/ResponsiveLinearLayout;->onFinishInflate()V

    .line 75
    sget v0, Lcom/squareup/edititem/R$id;->variation_header:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->variationHeader:Lcom/squareup/marketfont/MarketTextView;

    .line 76
    sget v0, Lcom/squareup/edititem/R$id;->edit_item_main_view_default_variation_container:I

    .line 77
    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->defaultVariationContainer:Landroid/view/View;

    .line 78
    sget v0, Lcom/squareup/edititem/R$id;->edit_item_main_view_price_sku_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->priceSkuContainer:Landroid/view/View;

    .line 80
    sget v0, Lcom/squareup/edititem/R$id;->edit_item_main_view_stock_count_row:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/widgets/StockCountRow;

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->stockCountRowForDefaultVariation:Lcom/squareup/ui/items/widgets/StockCountRow;

    .line 82
    sget v0, Lcom/squareup/edititem/R$id;->edit_item_main_view_price_input_field:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoEditText;

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->priceEditText:Lcom/squareup/noho/NohoEditText;

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->priceEditText:Lcom/squareup/noho/NohoEditText;

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$EditItemMainViewSingleVariationStaticRow$85MeZt_wQDLUM-6YNhSYV317JK4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/-$$Lambda$EditItemMainViewSingleVariationStaticRow$85MeZt_wQDLUM-6YNhSYV317JK4;-><init>(Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;)V

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 86
    sget v0, Lcom/squareup/edititem/R$id;->edit_item_main_view_sku_input_field:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoEditText;

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->skuEditText:Lcom/squareup/noho/NohoEditText;

    .line 87
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->skuEditText:Lcom/squareup/noho/NohoEditText;

    new-instance v1, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow$1;-><init>(Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;)V

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 95
    sget v0, Lcom/squareup/edititem/R$id;->edit_item_main_view_unit_type:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoRow;

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->unitTypeSelector:Lcom/squareup/noho/NohoRow;

    .line 97
    sget v0, Lcom/squareup/edititem/R$id;->edit_item_main_view_unit_type_help_text:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->unitTypeSelectorHelpText:Lcom/squareup/widgets/MessageView;

    .line 98
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->unitTypeSelectorHelpText:Lcom/squareup/widgets/MessageView;

    new-instance v1, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/squareup/edititem/R$string;->edit_item_unit_type_selector_help_text:I

    const-string v3, "learn_more"

    .line 99
    invoke-virtual {v1, v2, v3}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/edititem/R$string;->unit_type_hint_url:I

    .line 100
    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/common/strings/R$string;->learn_more:I

    .line 101
    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/noho/R$color;->noho_text_help_link:I

    .line 102
    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->linkColor(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    .line 103
    invoke-virtual {v1}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object v1

    .line 98
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->unitTypeSelector:Lcom/squareup/noho/NohoRow;

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$EditItemMainViewSingleVariationStaticRow$VO7Kveb5h6cNjeZ0ubb0RUoD4uY;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/-$$Lambda$EditItemMainViewSingleVariationStaticRow$VO7Kveb5h6cNjeZ0ubb0RUoD4uY;-><init>(Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;)V

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method showContent(Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)V
    .locals 4

    .line 109
    new-instance v0, Lrx/subscriptions/CompositeSubscription;

    invoke-direct {v0}, Lrx/subscriptions/CompositeSubscription;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->subscriptions:Lrx/subscriptions/CompositeSubscription;

    .line 111
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->setAlwaysEditableContent(Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)V

    .line 113
    iget-boolean v0, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->shouldShowInlineVariation:Z

    if-eqz v0, :cond_1

    .line 114
    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->getDefaultVariationPrecision()I

    move-result v0

    .line 115
    iget-object v1, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->defaultVariation:Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 116
    invoke-virtual {p1, v1}, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->getMeasurementUnit(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;)Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 119
    iget-object v2, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->res:Lcom/squareup/util/Res;

    invoke-static {v2, v1}, Lcom/squareup/quantity/UnitDisplayData;->fromCatalogMeasurementUnit(Lcom/squareup/util/Res;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)Lcom/squareup/quantity/UnitDisplayData;

    move-result-object v1

    .line 121
    invoke-virtual {v1}, Lcom/squareup/quantity/UnitDisplayData;->getUnitAbbreviation()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    const-string v1, ""

    .line 123
    :goto_0
    iget-object v2, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->subscriptions:Lrx/subscriptions/CompositeSubscription;

    iget-object v3, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->defaultVariation:Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 124
    invoke-direct {p0, v3, v0, v1}, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->showStockCountForDefaultVariation(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;ILjava/lang/String;)Lrx/Subscription;

    move-result-object v0

    .line 123
    invoke-virtual {v2, v0}, Lrx/subscriptions/CompositeSubscription;->add(Lrx/Subscription;)V

    .line 126
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->subscriptions:Lrx/subscriptions/CompositeSubscription;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    iget-object p1, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->defaultVariation:Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 127
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/ui/items/EditItemMainPresenter;->shouldShowRedSkuForVariation(Ljava/lang/String;)Lrx/Observable;

    move-result-object p1

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$EditItemMainViewSingleVariationStaticRow$z5kO7lW088EsfjzyCp5wlED0xf4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/-$$Lambda$EditItemMainViewSingleVariationStaticRow$z5kO7lW088EsfjzyCp5wlED0xf4;-><init>(Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;)V

    .line 128
    invoke-virtual {p1, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    .line 126
    invoke-virtual {v0, p1}, Lrx/subscriptions/CompositeSubscription;->add(Lrx/Subscription;)V

    .line 130
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->subscriptions:Lrx/subscriptions/CompositeSubscription;

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemMainPresenter;->scannedSku()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$EditItemMainViewSingleVariationStaticRow$__xmk9Cqz0GRgj1xIpI-hf-vPbQ;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/-$$Lambda$EditItemMainViewSingleVariationStaticRow$__xmk9Cqz0GRgj1xIpI-hf-vPbQ;-><init>(Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/subscriptions/CompositeSubscription;->add(Lrx/Subscription;)V

    :cond_1
    return-void
.end method
