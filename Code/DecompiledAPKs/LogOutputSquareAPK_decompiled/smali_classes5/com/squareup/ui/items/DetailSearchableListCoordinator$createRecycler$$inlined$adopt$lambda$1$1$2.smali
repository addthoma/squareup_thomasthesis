.class public final Lcom/squareup/ui/items/DetailSearchableListCoordinator$createRecycler$$inlined$adopt$lambda$1$1$2;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "Views.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/DetailSearchableListCoordinator$createRecycler$$inlined$adopt$lambda$1$1;->invoke(ILjava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nViews.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Views.kt\ncom/squareup/util/Views$onClickDebounced$1\n+ 2 DetailSearchableListCoordinator.kt\ncom/squareup/ui/items/DetailSearchableListCoordinator\n*L\n1#1,1322:1\n212#2,2:1323\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000M\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0005*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006\u00b8\u0006\n"
    }
    d2 = {
        "com/squareup/util/Views$onClickDebounced$1",
        "Lcom/squareup/debounce/DebouncedOnClickListener;",
        "doClick",
        "",
        "view",
        "Landroid/view/View;",
        "public_release",
        "com/squareup/ui/items/DetailSearchableListCoordinator$$special$$inlined$onClickDebounced$2",
        "com/squareup/ui/items/DetailSearchableListCoordinator$$special$$inlined$bind$1$lambda$2",
        "com/squareup/ui/items/DetailSearchableListCoordinator$$special$$inlined$create$1$lambda$1$2",
        "com/squareup/ui/items/DetailSearchableListCoordinator$$special$$inlined$row$lambda$1$1$2"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $input$inlined:Lcom/squareup/ui/items/DetailSearchableListRow$CreateButtonRow;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/items/DetailSearchableListRow$CreateButtonRow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator$createRecycler$$inlined$adopt$lambda$1$1$2;->$input$inlined:Lcom/squareup/ui/items/DetailSearchableListRow$CreateButtonRow;

    .line 1103
    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1106
    check-cast p1, Lcom/squareup/noho/NohoButton;

    .line 1323
    iget-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator$createRecycler$$inlined$adopt$lambda$1$1$2;->$input$inlined:Lcom/squareup/ui/items/DetailSearchableListRow$CreateButtonRow;

    invoke-virtual {p1}, Lcom/squareup/ui/items/DetailSearchableListRow$CreateButtonRow;->getScreen()Lcom/squareup/ui/items/DetailSearchableListScreen;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/items/DetailSearchableListScreen;->getOnEvent()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    sget-object v0, Lcom/squareup/ui/items/DetailSearchableListScreen$Event$ConvertItemButtonPressed;->INSTANCE:Lcom/squareup/ui/items/DetailSearchableListScreen$Event$ConvertItemButtonPressed;

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
