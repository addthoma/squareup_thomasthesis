.class final Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow$render$1;
.super Lkotlin/jvm/internal/Lambda;
.source "AssignUnitToVariationWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow;->render(Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;Lcom/squareup/ui/items/unit/AssignUnitToVariationState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ui/items/unit/UnitSelectionScreen$Event;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/ui/items/unit/AssignUnitToVariationState;",
        "+",
        "Lcom/squareup/ui/items/unit/AssignUnitToVariationResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/items/unit/AssignUnitToVariationState;",
        "Lcom/squareup/ui/items/unit/AssignUnitToVariationResult;",
        "event",
        "Lcom/squareup/ui/items/unit/UnitSelectionScreen$Event;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $input:Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;

.field final synthetic $state:Lcom/squareup/ui/items/unit/AssignUnitToVariationState;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/unit/AssignUnitToVariationState;Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow$render$1;->$state:Lcom/squareup/ui/items/unit/AssignUnitToVariationState;

    iput-object p2, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow$render$1;->$input:Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/ui/items/unit/UnitSelectionScreen$Event;)Lcom/squareup/workflow/WorkflowAction;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/items/unit/UnitSelectionScreen$Event;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/items/unit/AssignUnitToVariationState;",
            "Lcom/squareup/ui/items/unit/AssignUnitToVariationResult;",
            ">;"
        }
    .end annotation

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 137
    instance-of v0, p1, Lcom/squareup/ui/items/unit/UnitSelectionScreen$Event$UnitSelected;

    const/4 v1, 0x2

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    .line 138
    iget-object v0, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow$render$1;->$state:Lcom/squareup/ui/items/unit/AssignUnitToVariationState;

    check-cast v0, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;

    invoke-virtual {v0}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;->getSelectedUnitId()Ljava/lang/String;

    move-result-object v0

    .line 139
    check-cast p1, Lcom/squareup/ui/items/unit/UnitSelectionScreen$Event$UnitSelected;

    invoke-virtual {p1}, Lcom/squareup/ui/items/unit/UnitSelectionScreen$Event$UnitSelected;->getSelectedUnitId()Ljava/lang/String;

    move-result-object v4

    .line 140
    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    if-eqz p1, :cond_0

    .line 141
    iget-object p1, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow$render$1;->$input:Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;

    invoke-virtual {p1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->getDoesVariationHaveStock()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 142
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 143
    new-instance v0, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$WarnInventoryNotUpdatedOnUnitChange;

    .line 144
    iget-object v3, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow$render$1;->$state:Lcom/squareup/ui/items/unit/AssignUnitToVariationState;

    check-cast v3, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;

    .line 143
    invoke-direct {v0, v3, v4}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$WarnInventoryNotUpdatedOnUnitChange;-><init>(Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;Ljava/lang/String;)V

    .line 142
    invoke-static {p1, v0, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1

    .line 148
    :cond_0
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    iget-object v0, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow$render$1;->$state:Lcom/squareup/ui/items/unit/AssignUnitToVariationState;

    move-object v3, v0

    check-cast v3, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0xe

    const/4 v9, 0x0

    invoke-static/range {v3 .. v9}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;->copy$default(Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;ILjava/lang/Object;)Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;

    move-result-object v0

    invoke-static {p1, v0, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1

    .line 152
    :cond_1
    instance-of v0, p1, Lcom/squareup/ui/items/unit/UnitSelectionScreen$Event$CreateUnitTapped;

    if-eqz v0, :cond_2

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    new-instance v0, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$CreateUnit;

    iget-object v3, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow$render$1;->$state:Lcom/squareup/ui/items/unit/AssignUnitToVariationState;

    check-cast v3, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;

    invoke-direct {v0, v3}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$CreateUnit;-><init>(Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;)V

    invoke-static {p1, v0, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 154
    :cond_2
    instance-of v0, p1, Lcom/squareup/ui/items/unit/UnitSelectionScreen$Event$DefaultStandardUnitSelected;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 155
    new-instance v3, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$AddDefaultStandardUnit;

    .line 156
    iget-object v4, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow$render$1;->$state:Lcom/squareup/ui/items/unit/AssignUnitToVariationState;

    check-cast v4, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;

    .line 157
    check-cast p1, Lcom/squareup/ui/items/unit/UnitSelectionScreen$Event$DefaultStandardUnitSelected;

    invoke-virtual {p1}, Lcom/squareup/ui/items/unit/UnitSelectionScreen$Event$DefaultStandardUnitSelected;->getDefaultStandardUnit()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object p1

    .line 155
    invoke-direct {v3, v4, p1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$AddDefaultStandardUnit;-><init>(Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;Lcom/squareup/protos/connect/v2/common/MeasurementUnit;)V

    .line 154
    invoke-static {v0, v3, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 161
    :cond_3
    instance-of v0, p1, Lcom/squareup/ui/items/unit/UnitSelectionScreen$Event$SaveButtonTapped;

    if-eqz v0, :cond_4

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 162
    new-instance v0, Lcom/squareup/ui/items/unit/AssignUnitToVariationResult;

    iget-object v1, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow$render$1;->$state:Lcom/squareup/ui/items/unit/AssignUnitToVariationState;

    check-cast v1, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;

    invoke-virtual {v1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;->getSelectedUnitId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow$render$1;->$state:Lcom/squareup/ui/items/unit/AssignUnitToVariationState;

    check-cast v2, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;

    invoke-virtual {v2}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;->getCreatedUnits()Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/ui/items/unit/AssignUnitToVariationResult;-><init>(Ljava/lang/String;Ljava/util/List;)V

    .line 161
    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    .line 165
    :cond_4
    instance-of p1, p1, Lcom/squareup/ui/items/unit/UnitSelectionScreen$Event$DiscardButtonTapped;

    if-eqz p1, :cond_6

    .line 166
    iget-object p1, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow$render$1;->$input:Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;

    invoke-virtual {p1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->getSelectedUnitId()Ljava/lang/String;

    move-result-object p1

    .line 167
    iget-object v0, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow$render$1;->$state:Lcom/squareup/ui/items/unit/AssignUnitToVariationState;

    check-cast v0, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;

    invoke-virtual {v0}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;->getSelectedUnitId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_5

    .line 168
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 169
    new-instance v0, Lcom/squareup/ui/items/unit/AssignUnitToVariationResult;

    iget-object v1, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow$render$1;->$state:Lcom/squareup/ui/items/unit/AssignUnitToVariationState;

    check-cast v1, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;

    invoke-virtual {v1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;->getSelectedUnitId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow$render$1;->$state:Lcom/squareup/ui/items/unit/AssignUnitToVariationState;

    check-cast v2, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;

    invoke-virtual {v2}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;->getCreatedUnits()Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/ui/items/unit/AssignUnitToVariationResult;-><init>(Ljava/lang/String;Ljava/util/List;)V

    .line 168
    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1

    .line 172
    :cond_5
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 173
    new-instance v0, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$WarnUnsavedUnitSelectionChange;

    iget-object v3, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow$render$1;->$state:Lcom/squareup/ui/items/unit/AssignUnitToVariationState;

    check-cast v3, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;

    invoke-direct {v0, v3}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$WarnUnsavedUnitSelectionChange;-><init>(Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;)V

    .line 172
    invoke-static {p1, v0, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1

    .line 167
    :cond_6
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 112
    check-cast p1, Lcom/squareup/ui/items/unit/UnitSelectionScreen$Event;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow$render$1;->invoke(Lcom/squareup/ui/items/unit/UnitSelectionScreen$Event;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
