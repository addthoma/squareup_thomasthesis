.class public Lcom/squareup/ui/items/EditItemReadOnlyDetailsView;
.super Lcom/squareup/widgets/ResponsiveLinearLayout;
.source "EditItemReadOnlyDetailsView.java"


# instance fields
.field private dashboardLink:Lcom/squareup/widgets/MessageView;

.field private readOnlyCategory:Lcom/squareup/ui/library/edit/AttributePairRow;

.field private readOnlyDescription:Lcom/squareup/ui/library/edit/AttributePairRow;

.field private readOnlyHeader:Landroid/widget/TextView;

.field private readOnlyName:Lcom/squareup/ui/library/edit/AttributePairRow;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 28
    invoke-direct {p0, p1, p2}, Lcom/squareup/widgets/ResponsiveLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    sget p2, Lcom/squareup/edititem/R$layout;->edit_item_read_only_details:I

    invoke-static {p1, p2, p0}, Lcom/squareup/ui/items/EditItemReadOnlyDetailsView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 30
    sget p1, Lcom/squareup/edititem/R$id;->read_only_details_header:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/ui/items/EditItemReadOnlyDetailsView;->readOnlyHeader:Landroid/widget/TextView;

    .line 31
    sget p1, Lcom/squareup/edititem/R$id;->item_editing_read_only_name:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/library/edit/AttributePairRow;

    iput-object p1, p0, Lcom/squareup/ui/items/EditItemReadOnlyDetailsView;->readOnlyName:Lcom/squareup/ui/library/edit/AttributePairRow;

    .line 32
    sget p1, Lcom/squareup/edititem/R$id;->item_editing_read_only_category:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/library/edit/AttributePairRow;

    iput-object p1, p0, Lcom/squareup/ui/items/EditItemReadOnlyDetailsView;->readOnlyCategory:Lcom/squareup/ui/library/edit/AttributePairRow;

    .line 33
    sget p1, Lcom/squareup/edititem/R$id;->item_editing_read_only_description:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/library/edit/AttributePairRow;

    iput-object p1, p0, Lcom/squareup/ui/items/EditItemReadOnlyDetailsView;->readOnlyDescription:Lcom/squareup/ui/library/edit/AttributePairRow;

    .line 34
    sget p1, Lcom/squareup/edititem/R$id;->dashboard_link:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/MessageView;

    iput-object p1, p0, Lcom/squareup/ui/items/EditItemReadOnlyDetailsView;->dashboardLink:Lcom/squareup/widgets/MessageView;

    const/4 p1, 0x1

    .line 35
    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/EditItemReadOnlyDetailsView;->setOrientation(I)V

    return-void
.end method


# virtual methods
.method public setContent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 39
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemReadOnlyDetailsView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 41
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemReadOnlyDetailsView;->readOnlyHeader:Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 42
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemReadOnlyDetailsView;->readOnlyName:Lcom/squareup/ui/library/edit/AttributePairRow;

    sget v1, Lcom/squareup/edititem/R$string;->item_editing_read_only_name:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1, p2}, Lcom/squareup/ui/library/edit/AttributePairRow;->setKeyAndValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemReadOnlyDetailsView;->readOnlyCategory:Lcom/squareup/ui/library/edit/AttributePairRow;

    sget p2, Lcom/squareup/edititem/R$string;->item_editing_read_only_category:I

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2, p3}, Lcom/squareup/ui/library/edit/AttributePairRow;->setKeyAndValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemReadOnlyDetailsView;->readOnlyDescription:Lcom/squareup/ui/library/edit/AttributePairRow;

    sget p2, Lcom/squareup/edititem/R$string;->item_editing_read_only_description:I

    .line 46
    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 45
    invoke-virtual {p1, p2, p4}, Lcom/squareup/ui/library/edit/AttributePairRow;->setKeyAndValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemReadOnlyDetailsView;->dashboardLink:Lcom/squareup/widgets/MessageView;

    new-instance p2, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemReadOnlyDetailsView;->getContext()Landroid/content/Context;

    move-result-object p3

    invoke-direct {p2, p3}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    sget p3, Lcom/squareup/edititem/R$string;->edit_item_details_dashboard:I

    const-string p4, "dashboard"

    .line 48
    invoke-virtual {p2, p3, p4}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p2

    sget p3, Lcom/squareup/common/strings/R$string;->dashboard_items_library_url:I

    .line 49
    invoke-virtual {p2, p3}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p2

    sget p3, Lcom/squareup/configure/item/R$string;->dashboard:I

    .line 50
    invoke-virtual {p2, p3}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p2

    .line 51
    invoke-virtual {p2}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object p2

    .line 47
    invoke-virtual {p1, p2}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
