.class public Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter$ViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
.source "EditItemMainViewRecyclerViewAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ViewHolder"
.end annotation


# instance fields
.field private final durationFormatter:Lcom/squareup/text/DurationFormatter;

.field private final moneyFormatter:Lcom/squareup/quantity/PerUnitFormatter;

.field private final presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

.field private final res:Lcom/squareup/util/Res;

.field private final row:Landroid/view/View;

.field private final subscriptionsOnViewHolder:Lrx/subscriptions/CompositeSubscription;


# direct methods
.method constructor <init>(Landroid/view/View;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/ui/items/EditItemMainPresenter;Lcom/squareup/util/Res;Lcom/squareup/text/DurationFormatter;)V
    .locals 1

    .line 91
    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 87
    new-instance v0, Lrx/subscriptions/CompositeSubscription;

    invoke-direct {v0}, Lrx/subscriptions/CompositeSubscription;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter$ViewHolder;->subscriptionsOnViewHolder:Lrx/subscriptions/CompositeSubscription;

    .line 92
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter$ViewHolder;->row:Landroid/view/View;

    .line 93
    iput-object p2, p0, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter$ViewHolder;->moneyFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    .line 94
    iput-object p3, p0, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter$ViewHolder;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    .line 95
    iput-object p4, p0, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter$ViewHolder;->res:Lcom/squareup/util/Res;

    .line 96
    iput-object p5, p0, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter$ViewHolder;->durationFormatter:Lcom/squareup/text/DurationFormatter;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter$ViewHolder;)Lrx/subscriptions/CompositeSubscription;
    .locals 0

    .line 80
    iget-object p0, p0, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter$ViewHolder;->subscriptionsOnViewHolder:Lrx/subscriptions/CompositeSubscription;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter$ViewHolder;)Landroid/view/View;
    .locals 0

    .line 80
    iget-object p0, p0, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter$ViewHolder;->row:Landroid/view/View;

    return-object p0
.end method

.method private bindItemVariation(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)V
    .locals 6

    .line 166
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter$ViewHolder;->row:Landroid/view/View;

    check-cast v0, Lcom/squareup/ui/items/widgets/DraggableItemVariationRow;

    .line 168
    invoke-virtual {p2, p1}, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->getMeasurementUnit(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;)Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 171
    iget-object v2, p0, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter$ViewHolder;->res:Lcom/squareup/util/Res;

    invoke-static {v2, v1}, Lcom/squareup/quantity/UnitDisplayData;->fromCatalogMeasurementUnit(Lcom/squareup/util/Res;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)Lcom/squareup/quantity/UnitDisplayData;

    move-result-object v2

    .line 173
    invoke-virtual {v2}, Lcom/squareup/quantity/UnitDisplayData;->getUnitAbbreviation()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_0
    const-string v2, ""

    .line 177
    :goto_0
    invoke-virtual {v0}, Lcom/squareup/ui/items/widgets/DraggableItemVariationRow;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/squareup/edititem/R$string;->edit_item_price_variable:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/squareup/quantity/ItemQuantities;->formatWithUnit(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 179
    iget-object v4, p0, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter$ViewHolder;->moneyFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    .line 180
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getPrice()Lcom/squareup/protos/common/Money;

    move-result-object v5

    invoke-virtual {v4, v5, v2}, Lcom/squareup/quantity/PerUnitFormatter;->format(Lcom/squareup/protos/common/Money;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v3}, Lcom/squareup/util/Strings;->valueOrDefault(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 183
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getSku()Ljava/lang/String;

    move-result-object v4

    iget-boolean v5, p2, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->shouldNameTakeExtraSpace:Z

    invoke-virtual {v0, v3, v4, v5}, Lcom/squareup/ui/items/widgets/DraggableItemVariationRow;->setPriceAndSkuText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)V

    .line 185
    invoke-virtual {p2, p1}, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->getPrecision(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;)I

    move-result p2

    .line 186
    iget-object v3, p0, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter$ViewHolder;->subscriptionsOnViewHolder:Lrx/subscriptions/CompositeSubscription;

    iget-object v4, p0, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter$ViewHolder;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    .line 187
    invoke-virtual {v0}, Lcom/squareup/ui/items/widgets/DraggableItemVariationRow;->getStockCountRow()Lcom/squareup/ui/items/widgets/StockCountRow;

    move-result-object v5

    invoke-virtual {v4, v5, p1, p2, v2}, Lcom/squareup/ui/items/EditItemMainPresenter;->attachStockCountRowActionToStockCountRow(Lcom/squareup/ui/items/widgets/StockCountRow;Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;ILjava/lang/String;)Lrx/Subscription;

    move-result-object p2

    .line 186
    invoke-virtual {v3, p2}, Lrx/subscriptions/CompositeSubscription;->add(Lrx/Subscription;)V

    .line 190
    invoke-virtual {v0}, Lcom/squareup/ui/items/widgets/DraggableItemVariationRow;->getDetailsContainer()Landroid/view/ViewGroup;

    move-result-object p2

    new-instance v2, Lcom/squareup/ui/items/-$$Lambda$EditItemMainViewRecyclerViewAdapter$ViewHolder$qB6BSaR7b9VIcQBDgygrJDn87Qk;

    invoke-direct {v2, p0, p1, v1}, Lcom/squareup/ui/items/-$$Lambda$EditItemMainViewRecyclerViewAdapter$ViewHolder$qB6BSaR7b9VIcQBDgygrJDn87Qk;-><init>(Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter$ViewHolder;Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V

    .line 191
    invoke-static {v2}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 193
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter$ViewHolder;->subscriptionsOnViewHolder:Lrx/subscriptions/CompositeSubscription;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter$ViewHolder;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    .line 194
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/ui/items/EditItemMainPresenter;->shouldShowRedSkuForVariation(Ljava/lang/String;)Lrx/Observable;

    move-result-object p1

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$sVuWn7mpBzE9i2_E6wTFYunQ4tY;

    invoke-direct {v1, v0}, Lcom/squareup/ui/items/-$$Lambda$sVuWn7mpBzE9i2_E6wTFYunQ4tY;-><init>(Lcom/squareup/ui/items/widgets/DraggableItemVariationRow;)V

    .line 195
    invoke-virtual {p1, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    .line 193
    invoke-virtual {p2, p1}, Lrx/subscriptions/CompositeSubscription;->add(Lrx/Subscription;)V

    return-void
.end method

.method private bindServiceVariation(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)V
    .locals 5

    .line 138
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter$ViewHolder;->row:Landroid/view/View;

    check-cast v0, Lcom/squareup/ui/items/widgets/DraggableServiceVariationRow;

    .line 140
    invoke-virtual {p2, p1}, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->getMeasurementUnit(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;)Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object p2

    if-eqz p2, :cond_0

    .line 143
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter$ViewHolder;->res:Lcom/squareup/util/Res;

    invoke-static {v1, p2}, Lcom/squareup/quantity/UnitDisplayData;->fromCatalogMeasurementUnit(Lcom/squareup/util/Res;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)Lcom/squareup/quantity/UnitDisplayData;

    move-result-object v1

    .line 145
    invoke-virtual {v1}, Lcom/squareup/quantity/UnitDisplayData;->getUnitAbbreviation()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    const-string v1, ""

    .line 149
    :goto_0
    invoke-virtual {v0}, Lcom/squareup/ui/items/widgets/DraggableServiceVariationRow;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/squareup/edititem/R$string;->edit_item_price_variable:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/squareup/quantity/ItemQuantities;->formatWithUnit(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 151
    iget-object v3, p0, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter$ViewHolder;->moneyFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    .line 152
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getPrice()Lcom/squareup/protos/common/Money;

    move-result-object v4

    invoke-virtual {v3, v4, v1}, Lcom/squareup/quantity/PerUnitFormatter;->format(Lcom/squareup/protos/common/Money;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v2}, Lcom/squareup/util/Strings;->valueOrDefault(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 154
    invoke-virtual {v0, v1}, Lcom/squareup/ui/items/widgets/DraggableServiceVariationRow;->setPrice(Ljava/lang/String;)V

    .line 156
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter$ViewHolder;->durationFormatter:Lcom/squareup/text/DurationFormatter;

    .line 157
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getDuration()J

    move-result-wide v2

    invoke-static {v2, v3}, Lorg/threeten/bp/Duration;->ofMillis(J)Lorg/threeten/bp/Duration;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/squareup/text/DurationFormatter;->format(Lorg/threeten/bp/Duration;Z)Ljava/lang/String;

    move-result-object v1

    .line 156
    invoke-virtual {v0, v1}, Lcom/squareup/ui/items/widgets/DraggableServiceVariationRow;->setDuration(Ljava/lang/String;)V

    .line 159
    invoke-virtual {v0}, Lcom/squareup/ui/items/widgets/DraggableServiceVariationRow;->getDetailsContainer()Landroid/view/ViewGroup;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$EditItemMainViewRecyclerViewAdapter$ViewHolder$qM5Vch3Z2WfQjGIjJiKdPjaiBd8;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/ui/items/-$$Lambda$EditItemMainViewRecyclerViewAdapter$ViewHolder$qM5Vch3Z2WfQjGIjJiKdPjaiBd8;-><init>(Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter$ViewHolder;Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V

    .line 160
    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method bindSingleVariationRowContent(Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)V
    .locals 1

    .line 105
    iget-boolean v0, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->isService:Z

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter$ViewHolder;->row:Landroid/view/View;

    check-cast v0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;

    .line 108
    invoke-virtual {v0, p1}, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->showContent(Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)V

    goto :goto_0

    .line 110
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter$ViewHolder;->row:Landroid/view/View;

    check-cast v0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;

    .line 112
    invoke-virtual {v0, p1}, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->showContent(Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)V

    .line 113
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter$ViewHolder;->subscriptionsOnViewHolder:Lrx/subscriptions/CompositeSubscription;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->getAllSubscriptionsOnTheRow()Lrx/Subscription;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/subscriptions/CompositeSubscription;->add(Lrx/Subscription;)V

    :goto_0
    return-void
.end method

.method bindStaticBottomRowContent(Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)V
    .locals 1

    .line 199
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter$ViewHolder;->row:Landroid/view/View;

    check-cast v0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;

    .line 200
    invoke-virtual {v0, p1}, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->showContent(Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)V

    .line 201
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter$ViewHolder;->subscriptionsOnViewHolder:Lrx/subscriptions/CompositeSubscription;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->getAllSubscriptionsOnTheRow()Lrx/Subscription;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/subscriptions/CompositeSubscription;->add(Lrx/Subscription;)V

    return-void
.end method

.method bindStaticTopRowContent(Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)V
    .locals 1

    .line 100
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter$ViewHolder;->row:Landroid/view/View;

    check-cast v0, Lcom/squareup/ui/items/EditItemMainViewStaticTopView;

    .line 101
    invoke-virtual {v0, p1}, Lcom/squareup/ui/items/EditItemMainViewStaticTopView;->showContent(Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)V

    return-void
.end method

.method bindVariation(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;ILcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)V
    .locals 1

    .line 119
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter$ViewHolder;->row:Landroid/view/View;

    check-cast v0, Lcom/squareup/ui/items/widgets/DraggableVariationRow;

    if-nez p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    .line 122
    :goto_0
    invoke-virtual {v0, p2}, Lcom/squareup/ui/items/widgets/DraggableVariationRow;->configureTopDivider(Z)V

    .line 124
    iget-boolean p2, p3, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->variationsAreDraggable:Z

    if-nez p2, :cond_1

    .line 125
    invoke-virtual {v0}, Lcom/squareup/ui/items/widgets/DraggableVariationRow;->hideDragHandle()V

    .line 127
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/ui/items/widgets/DraggableVariationRow;->setName(Ljava/lang/CharSequence;)V

    .line 129
    iget-boolean p2, p3, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->isService:Z

    if-eqz p2, :cond_2

    .line 130
    invoke-direct {p0, p1, p3}, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter$ViewHolder;->bindServiceVariation(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)V

    goto :goto_1

    .line 132
    :cond_2
    invoke-direct {p0, p1, p3}, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter$ViewHolder;->bindItemVariation(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)V

    :goto_1
    return-void
.end method

.method public synthetic lambda$bindItemVariation$1$EditItemMainViewRecyclerViewAdapter$ViewHolder(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Landroid/view/View;)V
    .locals 0

    .line 192
    iget-object p3, p0, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter$ViewHolder;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    invoke-virtual {p3, p1, p2}, Lcom/squareup/ui/items/EditItemMainPresenter;->existingVariationClicked(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V

    return-void
.end method

.method public synthetic lambda$bindServiceVariation$0$EditItemMainViewRecyclerViewAdapter$ViewHolder(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Landroid/view/View;)V
    .locals 0

    .line 161
    iget-object p3, p0, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter$ViewHolder;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    invoke-virtual {p3, p1, p2}, Lcom/squareup/ui/items/EditItemMainPresenter;->existingVariationClicked(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V

    return-void
.end method
