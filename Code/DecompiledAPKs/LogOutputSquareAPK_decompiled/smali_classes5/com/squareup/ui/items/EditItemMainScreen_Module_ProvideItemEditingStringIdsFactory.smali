.class public final Lcom/squareup/ui/items/EditItemMainScreen_Module_ProvideItemEditingStringIdsFactory;
.super Ljava/lang/Object;
.source "EditItemMainScreen_Module_ProvideItemEditingStringIdsFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/items/ItemEditingStringIds;",
        ">;"
    }
.end annotation


# instance fields
.field private final module:Lcom/squareup/ui/items/EditItemMainScreen$Module;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/items/EditItemMainScreen$Module;)V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemMainScreen_Module_ProvideItemEditingStringIdsFactory;->module:Lcom/squareup/ui/items/EditItemMainScreen$Module;

    return-void
.end method

.method public static create(Lcom/squareup/ui/items/EditItemMainScreen$Module;)Lcom/squareup/ui/items/EditItemMainScreen_Module_ProvideItemEditingStringIdsFactory;
    .locals 1

    .line 30
    new-instance v0, Lcom/squareup/ui/items/EditItemMainScreen_Module_ProvideItemEditingStringIdsFactory;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/EditItemMainScreen_Module_ProvideItemEditingStringIdsFactory;-><init>(Lcom/squareup/ui/items/EditItemMainScreen$Module;)V

    return-object v0
.end method

.method public static provideItemEditingStringIds(Lcom/squareup/ui/items/EditItemMainScreen$Module;)Lcom/squareup/ui/items/ItemEditingStringIds;
    .locals 1

    .line 35
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemMainScreen$Module;->provideItemEditingStringIds()Lcom/squareup/ui/items/ItemEditingStringIds;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/items/ItemEditingStringIds;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/items/ItemEditingStringIds;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainScreen_Module_ProvideItemEditingStringIdsFactory;->module:Lcom/squareup/ui/items/EditItemMainScreen$Module;

    invoke-static {v0}, Lcom/squareup/ui/items/EditItemMainScreen_Module_ProvideItemEditingStringIdsFactory;->provideItemEditingStringIds(Lcom/squareup/ui/items/EditItemMainScreen$Module;)Lcom/squareup/ui/items/ItemEditingStringIds;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemMainScreen_Module_ProvideItemEditingStringIdsFactory;->get()Lcom/squareup/ui/items/ItemEditingStringIds;

    move-result-object v0

    return-object v0
.end method
