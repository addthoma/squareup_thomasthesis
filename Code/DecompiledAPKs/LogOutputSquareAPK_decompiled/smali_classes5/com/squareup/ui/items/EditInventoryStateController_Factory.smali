.class public final Lcom/squareup/ui/items/EditInventoryStateController_Factory;
.super Ljava/lang/Object;
.source "EditInventoryStateController_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/items/EditInventoryStateController;",
        ">;"
    }
.end annotation


# instance fields
.field private final inventoryServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/inventory/InventoryService;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/inventory/InventoryService;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/ui/items/EditInventoryStateController_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/ui/items/EditInventoryStateController_Factory;->inventoryServiceProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/items/EditInventoryStateController_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/inventory/InventoryService;",
            ">;)",
            "Lcom/squareup/ui/items/EditInventoryStateController_Factory;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/ui/items/EditInventoryStateController_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/items/EditInventoryStateController_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/server/inventory/InventoryService;)Lcom/squareup/ui/items/EditInventoryStateController;
    .locals 1

    .line 41
    new-instance v0, Lcom/squareup/ui/items/EditInventoryStateController;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/items/EditInventoryStateController;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/server/inventory/InventoryService;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/items/EditInventoryStateController;
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/ui/items/EditInventoryStateController_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, p0, Lcom/squareup/ui/items/EditInventoryStateController_Factory;->inventoryServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/server/inventory/InventoryService;

    invoke-static {v0, v1}, Lcom/squareup/ui/items/EditInventoryStateController_Factory;->newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/server/inventory/InventoryService;)Lcom/squareup/ui/items/EditInventoryStateController;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditInventoryStateController_Factory;->get()Lcom/squareup/ui/items/EditInventoryStateController;

    move-result-object v0

    return-object v0
.end method
