.class public final synthetic Lcom/squareup/ui/items/-$$Lambda$EditItemState$8dhkDBiKSymR_K0lwAOGKuPtH4M;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lcom/squareup/shared/catalog/CatalogCallback;


# instance fields
.field private final synthetic f$0:Lcom/squareup/ui/items/EditItemState;

.field private final synthetic f$1:Ljava/util/List;

.field private final synthetic f$2:Lcom/squareup/cogs/Cogs;

.field private final synthetic f$3:Lcom/squareup/shared/catalog/sync/SyncCallback;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/ui/items/EditItemState;Ljava/util/List;Lcom/squareup/cogs/Cogs;Lcom/squareup/shared/catalog/sync/SyncCallback;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/-$$Lambda$EditItemState$8dhkDBiKSymR_K0lwAOGKuPtH4M;->f$0:Lcom/squareup/ui/items/EditItemState;

    iput-object p2, p0, Lcom/squareup/ui/items/-$$Lambda$EditItemState$8dhkDBiKSymR_K0lwAOGKuPtH4M;->f$1:Ljava/util/List;

    iput-object p3, p0, Lcom/squareup/ui/items/-$$Lambda$EditItemState$8dhkDBiKSymR_K0lwAOGKuPtH4M;->f$2:Lcom/squareup/cogs/Cogs;

    iput-object p4, p0, Lcom/squareup/ui/items/-$$Lambda$EditItemState$8dhkDBiKSymR_K0lwAOGKuPtH4M;->f$3:Lcom/squareup/shared/catalog/sync/SyncCallback;

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/shared/catalog/CatalogResult;)V
    .locals 4

    iget-object v0, p0, Lcom/squareup/ui/items/-$$Lambda$EditItemState$8dhkDBiKSymR_K0lwAOGKuPtH4M;->f$0:Lcom/squareup/ui/items/EditItemState;

    iget-object v1, p0, Lcom/squareup/ui/items/-$$Lambda$EditItemState$8dhkDBiKSymR_K0lwAOGKuPtH4M;->f$1:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/ui/items/-$$Lambda$EditItemState$8dhkDBiKSymR_K0lwAOGKuPtH4M;->f$2:Lcom/squareup/cogs/Cogs;

    iget-object v3, p0, Lcom/squareup/ui/items/-$$Lambda$EditItemState$8dhkDBiKSymR_K0lwAOGKuPtH4M;->f$3:Lcom/squareup/shared/catalog/sync/SyncCallback;

    invoke-virtual {v0, v1, v2, v3, p1}, Lcom/squareup/ui/items/EditItemState;->lambda$saveToCatalogStoreAndSync$1$EditItemState(Ljava/util/List;Lcom/squareup/cogs/Cogs;Lcom/squareup/shared/catalog/sync/SyncCallback;Lcom/squareup/shared/catalog/CatalogResult;)V

    return-void
.end method
