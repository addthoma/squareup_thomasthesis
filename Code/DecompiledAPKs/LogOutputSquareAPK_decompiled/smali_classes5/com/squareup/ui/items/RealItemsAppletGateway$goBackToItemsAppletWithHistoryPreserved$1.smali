.class final Lcom/squareup/ui/items/RealItemsAppletGateway$goBackToItemsAppletWithHistoryPreserved$1;
.super Ljava/lang/Object;
.source "RealItemsAppletGateway.kt"

# interfaces
.implements Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/RealItemsAppletGateway;->goBackToItemsAppletWithHistoryPreserved()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/container/Command;",
        "kotlin.jvm.PlatformType",
        "history",
        "Lflow/History;",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/items/RealItemsAppletGateway$goBackToItemsAppletWithHistoryPreserved$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/items/RealItemsAppletGateway$goBackToItemsAppletWithHistoryPreserved$1;

    invoke-direct {v0}, Lcom/squareup/ui/items/RealItemsAppletGateway$goBackToItemsAppletWithHistoryPreserved$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/items/RealItemsAppletGateway$goBackToItemsAppletWithHistoryPreserved$1;->INSTANCE:Lcom/squareup/ui/items/RealItemsAppletGateway$goBackToItemsAppletWithHistoryPreserved$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lflow/History;)Lcom/squareup/container/Command;
    .locals 2

    const-string v0, "history"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-virtual {p1}, Lflow/History;->buildUpon()Lflow/History$Builder;

    move-result-object p1

    .line 24
    :goto_0
    invoke-virtual {p1}, Lflow/History$Builder;->peek()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/ui/items/RealItemsAppletGatewayKt;->isInItemsAppletScope(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 25
    invoke-virtual {p1}, Lflow/History$Builder;->pop()Ljava/lang/Object;

    goto :goto_0

    :cond_0
    const-string v0, "historyBuilder"

    .line 28
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lflow/History$Builder;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 32
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    .line 33
    :goto_1
    invoke-virtual {p1}, Lflow/History$Builder;->peek()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/ui/items/RealItemsAppletGatewayKt;->isInItemsAppletScope(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 34
    invoke-virtual {p1}, Lflow/History$Builder;->pop()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 38
    :cond_1
    sget-object v1, Lcom/squareup/ui/items/ItemsAppletMasterScreen;->INSTANCE:Lcom/squareup/ui/items/ItemsAppletMasterScreen;

    invoke-virtual {p1, v1}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    .line 39
    :goto_2
    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 40
    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p1, v1}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    goto :goto_2

    .line 43
    :cond_2
    invoke-virtual {p1}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p1

    sget-object v0, Lflow/Direction;->REPLACE:Lflow/Direction;

    invoke-static {p1, v0}, Lcom/squareup/container/Command;->setHistory(Lflow/History;Lflow/Direction;)Lcom/squareup/container/Command;

    move-result-object p1

    return-object p1

    .line 29
    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "There must something in ItemsAppletPath in the history."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
