.class public Lcom/squareup/ui/items/EditInventoryStateController;
.super Ljava/lang/Object;
.source "EditInventoryStateController.java"

# interfaces
.implements Lmortar/bundler/Bundler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;,
        Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsDataFromResponse;
    }
.end annotation


# static fields
.field private static final EDIT_INVENTORY_STATE_KEY:Ljava/lang/String; = "edit_inventory_state"


# instance fields
.field private final compositeDisposable:Lio/reactivex/disposables/CompositeDisposable;

.field private editInventoryState:Lcom/squareup/ui/items/EditInventoryState;

.field private final editInventoryStateRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/ui/items/EditInventoryState;",
            ">;"
        }
    .end annotation
.end field

.field private final inventoryService:Lcom/squareup/server/inventory/InventoryService;

.field private final isInventoryApiDisallowed:Z

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/server/inventory/InventoryService;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    new-instance v0, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v0}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/items/EditInventoryStateController;->compositeDisposable:Lio/reactivex/disposables/CompositeDisposable;

    .line 63
    iput-object p1, p0, Lcom/squareup/ui/items/EditInventoryStateController;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 64
    iput-object p2, p0, Lcom/squareup/ui/items/EditInventoryStateController;->inventoryService:Lcom/squareup/server/inventory/InventoryService;

    .line 65
    invoke-static {}, Lcom/squareup/ui/items/EditInventoryState;->createEditInventoryState()Lcom/squareup/ui/items/EditInventoryState;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/items/EditInventoryStateController;->editInventoryState:Lcom/squareup/ui/items/EditInventoryState;

    .line 66
    iget-object p2, p0, Lcom/squareup/ui/items/EditInventoryStateController;->editInventoryState:Lcom/squareup/ui/items/EditInventoryState;

    invoke-static {p2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/items/EditInventoryStateController;->editInventoryStateRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 67
    invoke-virtual {p1}, Lcom/squareup/settings/server/AccountStatusSettings;->isInventoryApiDisallowed()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/ui/items/EditInventoryStateController;->isInventoryApiDisallowed:Z

    return-void
.end method


# virtual methods
.method buildBatchAdjustRequestForInventoryAssignments(Ljava/util/Map;J)Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;J)",
            "Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;"
        }
    .end annotation

    .line 210
    iget-object v0, p0, Lcom/squareup/ui/items/EditInventoryStateController;->editInventoryState:Lcom/squareup/ui/items/EditInventoryState;

    .line 211
    invoke-virtual {v0}, Lcom/squareup/ui/items/EditInventoryState;->getInventoryAssignments()Ljava/util/Map;

    move-result-object v0

    .line 212
    iget-object v1, p0, Lcom/squareup/ui/items/EditInventoryStateController;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/UserSettings;->getToken()Ljava/lang/String;

    move-result-object v1

    .line 213
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 215
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 216
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 217
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/ui/items/EditInventoryState$InventoryAssignment;

    .line 219
    iget-object v5, v3, Lcom/squareup/ui/items/EditInventoryState$InventoryAssignment;->count:Ljava/math/BigDecimal;

    if-eqz v5, :cond_0

    .line 220
    invoke-interface {p1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 221
    iget-object v5, v3, Lcom/squareup/ui/items/EditInventoryState$InventoryAssignment;->count:Ljava/math/BigDecimal;

    .line 222
    iget-object v3, v3, Lcom/squareup/ui/items/EditInventoryState$InventoryAssignment;->unitCost:Lcom/squareup/protos/common/Money;

    .line 224
    new-instance v6, Lcom/squareup/protos/client/InventoryAdjustment$Builder;

    invoke-direct {v6}, Lcom/squareup/protos/client/InventoryAdjustment$Builder;-><init>()V

    .line 225
    invoke-virtual {v6, v1}, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->unit_token(Ljava/lang/String;)Lcom/squareup/protos/client/InventoryAdjustment$Builder;

    move-result-object v6

    .line 226
    invoke-virtual {v6, v4}, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->variation_token(Ljava/lang/String;)Lcom/squareup/protos/client/InventoryAdjustment$Builder;

    move-result-object v4

    .line 227
    invoke-virtual {v5}, Ljava/math/BigDecimal;->toPlainString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->adjust_quantity_decimal(Ljava/lang/String;)Lcom/squareup/protos/client/InventoryAdjustment$Builder;

    move-result-object v4

    sget-object v5, Lcom/squareup/protos/client/InventoryAdjustmentReason;->RECEIVED:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    .line 228
    invoke-virtual {v4, v5}, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->reason(Lcom/squareup/protos/client/InventoryAdjustmentReason;)Lcom/squareup/protos/client/InventoryAdjustment$Builder;

    move-result-object v4

    .line 229
    invoke-virtual {v4, v3}, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->cost_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/InventoryAdjustment$Builder;

    move-result-object v3

    .line 230
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->catalog_version(Ljava/lang/Long;)Lcom/squareup/protos/client/InventoryAdjustment$Builder;

    move-result-object v3

    .line 231
    invoke-virtual {v3}, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->build()Lcom/squareup/protos/client/InventoryAdjustment;

    move-result-object v3

    .line 224
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 234
    :cond_1
    new-instance p1, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest$Builder;

    invoke-direct {p1}, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest$Builder;-><init>()V

    .line 235
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object p2

    invoke-virtual {p2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest$Builder;->idempotency_token(Ljava/lang/String;)Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest$Builder;

    move-result-object p1

    .line 236
    invoke-virtual {p1, v2}, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest$Builder;->adjustments(Ljava/util/List;)Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest$Builder;

    move-result-object p1

    .line 237
    invoke-virtual {p1}, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest$Builder;->build()Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;

    move-result-object p1

    return-object p1
.end method

.method buildInventoryStockCountsDataFromResponse(Lcom/squareup/protos/client/GetVariationInventoryResponse;Ljava/util/LinkedHashMap;)Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsDataFromResponse;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/GetVariationInventoryResponse;",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsDataFromResponse;"
        }
    .end annotation

    .line 255
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 256
    iget-object p1, p1, Lcom/squareup/protos/client/GetVariationInventoryResponse;->current_counts:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/InventoryCount;

    .line 257
    iget-object v2, v1, Lcom/squareup/protos/client/InventoryCount;->variation_token:Ljava/lang/String;

    invoke-virtual {p2, v2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 258
    iget-object v1, v1, Lcom/squareup/protos/client/InventoryCount;->current_quantity_decimal:Ljava/lang/String;

    invoke-static {v1}, Lcom/squareup/util/Numbers;->parseDecimal(Ljava/lang/String;)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 260
    :cond_0
    new-instance p1, Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsDataFromResponse;

    sget-object p2, Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;->SUCCESS:Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;

    invoke-direct {p1, v0, p2}, Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsDataFromResponse;-><init>(Ljava/util/LinkedHashMap;Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;)V

    return-object p1
.end method

.method doesVariationHaveStockCount(Ljava/lang/String;)Z
    .locals 1

    .line 205
    iget-object v0, p0, Lcom/squareup/ui/items/EditInventoryStateController;->editInventoryState:Lcom/squareup/ui/items/EditInventoryState;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/items/EditInventoryState;->getCurrentStockCountForVariation(Ljava/lang/String;)Ljava/math/BigDecimal;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method editInventoryState()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/items/EditInventoryState;",
            ">;"
        }
    .end annotation

    .line 100
    iget-object v0, p0, Lcom/squareup/ui/items/EditInventoryStateController;->editInventoryStateRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->asObservable()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getMortarBundleKey()Ljava/lang/String;
    .locals 1

    .line 75
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method hasPendingInventoryAssignments()Z
    .locals 2

    .line 191
    iget-object v0, p0, Lcom/squareup/ui/items/EditInventoryStateController;->editInventoryState:Lcom/squareup/ui/items/EditInventoryState;

    .line 192
    invoke-virtual {v0}, Lcom/squareup/ui/items/EditInventoryState;->getInventoryAssignments()Ljava/util/Map;

    move-result-object v0

    .line 193
    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/items/EditInventoryState$InventoryAssignment;

    .line 194
    iget-object v1, v1, Lcom/squareup/ui/items/EditInventoryState$InventoryAssignment;->count:Ljava/math/BigDecimal;

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public synthetic lambda$loadInventoryStockCountsData$2$EditInventoryStateController(Ljava/util/LinkedHashMap;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 138
    new-instance v0, Lcom/squareup/ui/items/-$$Lambda$EditInventoryStateController$0rasd5DKbRZk0qffJK7kxSqwHhU;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/items/-$$Lambda$EditInventoryStateController$0rasd5DKbRZk0qffJK7kxSqwHhU;-><init>(Lcom/squareup/ui/items/EditInventoryStateController;Ljava/util/LinkedHashMap;)V

    new-instance p1, Lcom/squareup/ui/items/-$$Lambda$EditInventoryStateController$wCDwm8Ij1s-DTRAc9MfJ9IRynOg;

    invoke-direct {p1, p0}, Lcom/squareup/ui/items/-$$Lambda$EditInventoryStateController$wCDwm8Ij1s-DTRAc9MfJ9IRynOg;-><init>(Lcom/squareup/ui/items/EditInventoryStateController;)V

    invoke-virtual {p2, v0, p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$null$0$EditInventoryStateController(Ljava/util/LinkedHashMap;Lcom/squareup/protos/client/GetVariationInventoryResponse;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 141
    invoke-virtual {p0, p2, p1}, Lcom/squareup/ui/items/EditInventoryStateController;->buildInventoryStockCountsDataFromResponse(Lcom/squareup/protos/client/GetVariationInventoryResponse;Ljava/util/LinkedHashMap;)Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsDataFromResponse;

    move-result-object p1

    .line 143
    iget-object p2, p0, Lcom/squareup/ui/items/EditInventoryStateController;->editInventoryState:Lcom/squareup/ui/items/EditInventoryState;

    invoke-virtual {p2, p1}, Lcom/squareup/ui/items/EditInventoryState;->updateInventoryStockCountsWithDataFromResponse(Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsDataFromResponse;)V

    .line 145
    iget-object p1, p0, Lcom/squareup/ui/items/EditInventoryStateController;->editInventoryStateRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object p2, p0, Lcom/squareup/ui/items/EditInventoryStateController;->editInventoryState:Lcom/squareup/ui/items/EditInventoryState;

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$null$1$EditInventoryStateController(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 148
    iget-object p1, p0, Lcom/squareup/ui/items/EditInventoryStateController;->editInventoryState:Lcom/squareup/ui/items/EditInventoryState;

    new-instance v0, Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsDataFromResponse;

    sget-object v1, Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;->ERROR:Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;

    const/4 v2, 0x0

    invoke-direct {v0, v2, v1}, Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsDataFromResponse;-><init>(Ljava/util/LinkedHashMap;Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;)V

    invoke-virtual {p1, v0}, Lcom/squareup/ui/items/EditInventoryState;->updateInventoryStockCountsWithDataFromResponse(Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsDataFromResponse;)V

    .line 150
    iget-object p1, p0, Lcom/squareup/ui/items/EditInventoryStateController;->editInventoryStateRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v0, p0, Lcom/squareup/ui/items/EditInventoryStateController;->editInventoryState:Lcom/squareup/ui/items/EditInventoryState;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method loadInventoryStockCountsData(Lcom/squareup/ui/items/EditItemState;)V
    .locals 3

    .line 104
    iget-boolean v0, p0, Lcom/squareup/ui/items/EditInventoryStateController;->isInventoryApiDisallowed:Z

    if-eqz v0, :cond_0

    .line 105
    iget-object p1, p0, Lcom/squareup/ui/items/EditInventoryStateController;->editInventoryState:Lcom/squareup/ui/items/EditInventoryState;

    sget-object v0, Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;->DISALLOWED:Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/items/EditInventoryState;->updateInventoryStockCountsLoadStatus(Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;)V

    .line 106
    iget-object p1, p0, Lcom/squareup/ui/items/EditInventoryStateController;->editInventoryStateRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v0, p0, Lcom/squareup/ui/items/EditInventoryStateController;->editInventoryState:Lcom/squareup/ui/items/EditInventoryState;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void

    :cond_0
    if-nez p1, :cond_1

    .line 111
    iget-object p1, p0, Lcom/squareup/ui/items/EditInventoryStateController;->editInventoryState:Lcom/squareup/ui/items/EditInventoryState;

    sget-object v0, Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;->ERROR:Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/items/EditInventoryState;->updateInventoryStockCountsLoadStatus(Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;)V

    .line 112
    iget-object p1, p0, Lcom/squareup/ui/items/EditInventoryStateController;->editInventoryStateRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v0, p0, Lcom/squareup/ui/items/EditInventoryStateController;->editInventoryState:Lcom/squareup/ui/items/EditInventoryState;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void

    .line 117
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState$ItemData;->getVariationIdsByMerchantCatalogTokens()Ljava/util/LinkedHashMap;

    move-result-object v0

    .line 120
    iget-boolean p1, p1, Lcom/squareup/ui/items/EditItemState;->isNewItem:Z

    if-nez p1, :cond_3

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    .line 126
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/items/EditInventoryStateController;->editInventoryState:Lcom/squareup/ui/items/EditInventoryState;

    sget-object v1, Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;->LOADING:Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;

    invoke-virtual {p1, v1}, Lcom/squareup/ui/items/EditInventoryState;->updateInventoryStockCountsLoadStatus(Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;)V

    .line 127
    iget-object p1, p0, Lcom/squareup/ui/items/EditInventoryStateController;->editInventoryStateRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v1, p0, Lcom/squareup/ui/items/EditInventoryStateController;->editInventoryState:Lcom/squareup/ui/items/EditInventoryState;

    invoke-virtual {p1, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 129
    new-instance p1, Lcom/squareup/protos/client/GetVariationInventoryRequest$Builder;

    invoke-direct {p1}, Lcom/squareup/protos/client/GetVariationInventoryRequest$Builder;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    .line 130
    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p1, v1}, Lcom/squareup/protos/client/GetVariationInventoryRequest$Builder;->variation_token(Ljava/util/List;)Lcom/squareup/protos/client/GetVariationInventoryRequest$Builder;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/ui/items/EditInventoryStateController;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 131
    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/UserSettings;->getToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/protos/client/GetVariationInventoryRequest$Builder;->unit_token(Ljava/lang/String;)Lcom/squareup/protos/client/GetVariationInventoryRequest$Builder;

    move-result-object p1

    sget-object v1, Lcom/squareup/protos/client/State;->IN_STOCK:Lcom/squareup/protos/client/State;

    .line 132
    invoke-virtual {p1, v1}, Lcom/squareup/protos/client/GetVariationInventoryRequest$Builder;->state(Lcom/squareup/protos/client/State;)Lcom/squareup/protos/client/GetVariationInventoryRequest$Builder;

    move-result-object p1

    .line 133
    invoke-virtual {p1}, Lcom/squareup/protos/client/GetVariationInventoryRequest$Builder;->build()Lcom/squareup/protos/client/GetVariationInventoryRequest;

    move-result-object p1

    .line 135
    iget-object v1, p0, Lcom/squareup/ui/items/EditInventoryStateController;->compositeDisposable:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v2, p0, Lcom/squareup/ui/items/EditInventoryStateController;->inventoryService:Lcom/squareup/server/inventory/InventoryService;

    invoke-interface {v2, p1}, Lcom/squareup/server/inventory/InventoryService;->get(Lcom/squareup/protos/client/GetVariationInventoryRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object p1

    .line 136
    invoke-virtual {p1}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    new-instance v2, Lcom/squareup/ui/items/-$$Lambda$EditInventoryStateController$zM7QeBY-a635UyC-JVLr5zhg8sA;

    invoke-direct {v2, p0, v0}, Lcom/squareup/ui/items/-$$Lambda$EditInventoryStateController$zM7QeBY-a635UyC-JVLr5zhg8sA;-><init>(Lcom/squareup/ui/items/EditInventoryStateController;Ljava/util/LinkedHashMap;)V

    .line 137
    invoke-virtual {p1, v2}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    .line 135
    invoke-virtual {v1, p1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void

    .line 121
    :cond_3
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/items/EditInventoryStateController;->editInventoryState:Lcom/squareup/ui/items/EditInventoryState;

    sget-object v0, Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;->SUCCESS:Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/items/EditInventoryState;->updateInventoryStockCountsLoadStatus(Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;)V

    .line 122
    iget-object p1, p0, Lcom/squareup/ui/items/EditInventoryStateController;->editInventoryStateRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v0, p0, Lcom/squareup/ui/items/EditInventoryStateController;->editInventoryState:Lcom/squareup/ui/items/EditInventoryState;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/items/EditInventoryStateController;->compositeDisposable:Lio/reactivex/disposables/CompositeDisposable;

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 3

    if-eqz p1, :cond_0

    const-string v0, "edit_inventory_state"

    .line 84
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/squareup/ui/items/EditInventoryStateController;->editInventoryState:Lcom/squareup/ui/items/EditInventoryState;

    .line 85
    invoke-virtual {v1}, Lcom/squareup/ui/items/EditInventoryState;->getInventoryStockCountsLoadStatus()Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;->NOT_REQUESTED:Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;

    if-ne v1, v2, :cond_0

    .line 86
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/EditInventoryState;

    iput-object p1, p0, Lcom/squareup/ui/items/EditInventoryStateController;->editInventoryState:Lcom/squareup/ui/items/EditInventoryState;

    :cond_0
    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 2

    if-eqz p1, :cond_0

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/items/EditInventoryStateController;->editInventoryState:Lcom/squareup/ui/items/EditInventoryState;

    const-string v1, "edit_inventory_state"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    return-void
.end method

.method removeInventoryAssignment(Ljava/lang/String;)V
    .locals 1

    .line 167
    iget-object v0, p0, Lcom/squareup/ui/items/EditInventoryStateController;->editInventoryState:Lcom/squareup/ui/items/EditInventoryState;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/items/EditInventoryState;->removeInventoryAssignment(Ljava/lang/String;)V

    .line 168
    iget-object p1, p0, Lcom/squareup/ui/items/EditInventoryStateController;->editInventoryStateRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v0, p0, Lcom/squareup/ui/items/EditInventoryStateController;->editInventoryState:Lcom/squareup/ui/items/EditInventoryState;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method saveInventoryAssignments(Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/BatchAdjustVariationInventoryResponse;",
            ">;>;"
        }
    .end annotation

    .line 183
    iget-boolean v0, p0, Lcom/squareup/ui/items/EditInventoryStateController;->isInventoryApiDisallowed:Z

    if-nez v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/squareup/ui/items/EditInventoryStateController;->inventoryService:Lcom/squareup/server/inventory/InventoryService;

    invoke-interface {v0, p1}, Lcom/squareup/server/inventory/InventoryService;->batchAdjust(Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1

    .line 184
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "The inventory API is disallowed."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method updateCreatedVariationIds(Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/api/items/MerchantCatalogObjectReference;",
            ">;)V"
        }
    .end annotation

    .line 173
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 174
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 176
    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/MerchantCatalogObjectReference;

    iget-object v3, v3, Lcom/squareup/api/items/MerchantCatalogObjectReference;->catalog_object_token:Ljava/lang/String;

    .line 175
    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 178
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/items/EditInventoryStateController;->editInventoryState:Lcom/squareup/ui/items/EditInventoryState;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/items/EditInventoryState;->updateVariationIdsForInventoryAssignments(Ljava/util/Map;)V

    return-void
.end method

.method updateInventoryAssignment(Ljava/lang/String;Ljava/math/BigDecimal;Lcom/squareup/protos/common/Money;)V
    .locals 1

    .line 162
    iget-object v0, p0, Lcom/squareup/ui/items/EditInventoryStateController;->editInventoryState:Lcom/squareup/ui/items/EditInventoryState;

    invoke-virtual {v0, p1, p2, p3}, Lcom/squareup/ui/items/EditInventoryState;->updateInventoryAssignment(Ljava/lang/String;Ljava/math/BigDecimal;Lcom/squareup/protos/common/Money;)V

    .line 163
    iget-object p1, p0, Lcom/squareup/ui/items/EditInventoryStateController;->editInventoryStateRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object p2, p0, Lcom/squareup/ui/items/EditInventoryStateController;->editInventoryState:Lcom/squareup/ui/items/EditInventoryState;

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method updateInventoryStockCountAfterAdjustment(Ljava/lang/String;Ljava/math/BigDecimal;)V
    .locals 1

    .line 156
    iget-object v0, p0, Lcom/squareup/ui/items/EditInventoryStateController;->editInventoryState:Lcom/squareup/ui/items/EditInventoryState;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/ui/items/EditInventoryState;->updateInventoryStockCount(Ljava/lang/String;Ljava/math/BigDecimal;)V

    .line 157
    iget-object p1, p0, Lcom/squareup/ui/items/EditInventoryStateController;->editInventoryStateRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object p2, p0, Lcom/squareup/ui/items/EditInventoryStateController;->editInventoryState:Lcom/squareup/ui/items/EditInventoryState;

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method
