.class public final synthetic Lcom/squareup/ui/items/-$$Lambda$DuplicateSkuValidator$szTr7mFtvqSEsUiPY1DQt-ACbN8;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lcom/squareup/shared/catalog/CatalogTask;


# instance fields
.field private final synthetic f$0:Lcom/squareup/ui/items/DuplicateSkuValidator;

.field private final synthetic f$1:Lcom/squareup/ui/items/DuplicateSkuValidator$DuplicateSkuValidationTarget;

.field private final synthetic f$2:Ljava/lang/String;

.field private final synthetic f$3:Ljava/lang/String;

.field private final synthetic f$4:Ljava/lang/String;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/ui/items/DuplicateSkuValidator;Lcom/squareup/ui/items/DuplicateSkuValidator$DuplicateSkuValidationTarget;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/-$$Lambda$DuplicateSkuValidator$szTr7mFtvqSEsUiPY1DQt-ACbN8;->f$0:Lcom/squareup/ui/items/DuplicateSkuValidator;

    iput-object p2, p0, Lcom/squareup/ui/items/-$$Lambda$DuplicateSkuValidator$szTr7mFtvqSEsUiPY1DQt-ACbN8;->f$1:Lcom/squareup/ui/items/DuplicateSkuValidator$DuplicateSkuValidationTarget;

    iput-object p3, p0, Lcom/squareup/ui/items/-$$Lambda$DuplicateSkuValidator$szTr7mFtvqSEsUiPY1DQt-ACbN8;->f$2:Ljava/lang/String;

    iput-object p4, p0, Lcom/squareup/ui/items/-$$Lambda$DuplicateSkuValidator$szTr7mFtvqSEsUiPY1DQt-ACbN8;->f$3:Ljava/lang/String;

    iput-object p5, p0, Lcom/squareup/ui/items/-$$Lambda$DuplicateSkuValidator$szTr7mFtvqSEsUiPY1DQt-ACbN8;->f$4:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final perform(Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/lang/Object;
    .locals 6

    iget-object v0, p0, Lcom/squareup/ui/items/-$$Lambda$DuplicateSkuValidator$szTr7mFtvqSEsUiPY1DQt-ACbN8;->f$0:Lcom/squareup/ui/items/DuplicateSkuValidator;

    iget-object v1, p0, Lcom/squareup/ui/items/-$$Lambda$DuplicateSkuValidator$szTr7mFtvqSEsUiPY1DQt-ACbN8;->f$1:Lcom/squareup/ui/items/DuplicateSkuValidator$DuplicateSkuValidationTarget;

    iget-object v2, p0, Lcom/squareup/ui/items/-$$Lambda$DuplicateSkuValidator$szTr7mFtvqSEsUiPY1DQt-ACbN8;->f$2:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/ui/items/-$$Lambda$DuplicateSkuValidator$szTr7mFtvqSEsUiPY1DQt-ACbN8;->f$3:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/ui/items/-$$Lambda$DuplicateSkuValidator$szTr7mFtvqSEsUiPY1DQt-ACbN8;->f$4:Ljava/lang/String;

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/ui/items/DuplicateSkuValidator;->lambda$validateSkuAndUpdateErrorsBar$0$DuplicateSkuValidator(Lcom/squareup/ui/items/DuplicateSkuValidator$DuplicateSkuValidationTarget;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$DuplicateSkuInfo;

    move-result-object p1

    return-object p1
.end method
