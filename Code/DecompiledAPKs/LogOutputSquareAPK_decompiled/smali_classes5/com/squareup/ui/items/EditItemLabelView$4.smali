.class Lcom/squareup/ui/items/EditItemLabelView$4;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "EditItemLabelView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/EditItemLabelView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/items/EditItemLabelView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/EditItemLabelView;)V
    .locals 0

    .line 125
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemLabelView$4;->this$0:Lcom/squareup/ui/items/EditItemLabelView;

    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 1

    .line 127
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemLabelView$4;->this$0:Lcom/squareup/ui/items/EditItemLabelView;

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemLabelView;->presenter:Lcom/squareup/ui/items/EditItemLabelPresenter;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/items/EditItemLabelPresenter;->labelTextChanged(Ljava/lang/String;)V

    return-void
.end method
