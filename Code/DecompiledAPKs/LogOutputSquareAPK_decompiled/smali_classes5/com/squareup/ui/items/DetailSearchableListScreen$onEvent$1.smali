.class final Lcom/squareup/ui/items/DetailSearchableListScreen$onEvent$1;
.super Lkotlin/jvm/internal/Lambda;
.source "DetailSearchableListScreen.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/DetailSearchableListScreen;->getOnEvent()Lkotlin/jvm/functions/Function1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ui/items/DetailSearchableListScreen$Event;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "event",
        "Lcom/squareup/ui/items/DetailSearchableListScreen$Event;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/items/DetailSearchableListScreen;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/DetailSearchableListScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListScreen$onEvent$1;->this$0:Lcom/squareup/ui/items/DetailSearchableListScreen;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 8
    check-cast p1, Lcom/squareup/ui/items/DetailSearchableListScreen$Event;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/DetailSearchableListScreen$onEvent$1;->invoke(Lcom/squareup/ui/items/DetailSearchableListScreen$Event;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/ui/items/DetailSearchableListScreen$Event;)V
    .locals 2

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListScreen$onEvent$1;->this$0:Lcom/squareup/ui/items/DetailSearchableListScreen;

    invoke-static {v0}, Lcom/squareup/ui/items/DetailSearchableListScreen;->access$getAcceptingEvent$p(Lcom/squareup/ui/items/DetailSearchableListScreen;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 17
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListScreen$onEvent$1;->this$0:Lcom/squareup/ui/items/DetailSearchableListScreen;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/ui/items/DetailSearchableListScreen;->access$setAcceptingEvent$p(Lcom/squareup/ui/items/DetailSearchableListScreen;Z)V

    .line 18
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListScreen$onEvent$1;->this$0:Lcom/squareup/ui/items/DetailSearchableListScreen;

    invoke-static {v0}, Lcom/squareup/ui/items/DetailSearchableListScreen;->access$getEventHandler$p(Lcom/squareup/ui/items/DetailSearchableListScreen;)Lkotlin/jvm/functions/Function1;

    move-result-object v0

    invoke-interface {v0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method
