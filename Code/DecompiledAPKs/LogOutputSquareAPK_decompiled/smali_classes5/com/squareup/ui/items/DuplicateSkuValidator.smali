.class public Lcom/squareup/ui/items/DuplicateSkuValidator;
.super Ljava/lang/Object;
.source "DuplicateSkuValidator.java"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/DuplicateSkuValidator$DuplicateSkuValidationTarget;,
        Lcom/squareup/ui/items/DuplicateSkuValidator$SharedScope;
    }
.end annotation


# static fields
.field private static final NOT_SPECIFIED:Ljava/lang/String;


# instance fields
.field private final cogs:Lcom/squareup/cogs/Cogs;

.field private final editItemState:Lcom/squareup/ui/items/EditItemState;

.field private final errorsBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

.field private isInScope:Ljava/lang/Boolean;

.field private final locale:Ljava/util/Locale;

.field private final res:Lcom/squareup/util/Res;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final subscriptions:Lrx/subscriptions/CompositeSubscription;

.field private final variationIdsWithRedSkuRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/cogs/Cogs;Lcom/squareup/ui/items/EditItemState;Ljava/util/Locale;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/items/DuplicateSkuValidator;->variationIdsWithRedSkuRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 62
    new-instance v0, Lrx/subscriptions/CompositeSubscription;

    invoke-direct {v0}, Lrx/subscriptions/CompositeSubscription;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/items/DuplicateSkuValidator;->subscriptions:Lrx/subscriptions/CompositeSubscription;

    const/4 v0, 0x0

    .line 64
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/items/DuplicateSkuValidator;->isInScope:Ljava/lang/Boolean;

    .line 68
    iput-object p1, p0, Lcom/squareup/ui/items/DuplicateSkuValidator;->errorsBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    .line 69
    iput-object p2, p0, Lcom/squareup/ui/items/DuplicateSkuValidator;->cogs:Lcom/squareup/cogs/Cogs;

    .line 70
    iput-object p3, p0, Lcom/squareup/ui/items/DuplicateSkuValidator;->editItemState:Lcom/squareup/ui/items/EditItemState;

    .line 71
    iput-object p4, p0, Lcom/squareup/ui/items/DuplicateSkuValidator;->locale:Ljava/util/Locale;

    .line 72
    iput-object p5, p0, Lcom/squareup/ui/items/DuplicateSkuValidator;->res:Lcom/squareup/util/Res;

    .line 73
    iput-object p6, p0, Lcom/squareup/ui/items/DuplicateSkuValidator;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    return-void
.end method

.method private findDuplicateSkuInfoWithCogs(Lcom/squareup/shared/catalog/Catalog$Local;Lcom/squareup/ui/items/DuplicateSkuValidator$DuplicateSkuValidationTarget;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$DuplicateSkuInfo;
    .locals 10

    .line 302
    const-class v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader;

    .line 303
    invoke-interface {p1, v0}, Lcom/squareup/shared/catalog/Catalog$Local;->getSyntheticTableReader(Ljava/lang/Class;)Lcom/squareup/shared/catalog/synthetictables/SyntheticTableReader;

    move-result-object p1

    move-object v0, p1

    check-cast v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader;

    .line 304
    iget-object p1, p0, Lcom/squareup/ui/items/DuplicateSkuValidator;->editItemState:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->getId()Ljava/lang/String;

    move-result-object p1

    .line 305
    iget-object v1, p0, Lcom/squareup/ui/items/DuplicateSkuValidator;->editItemState:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->getName()Ljava/lang/String;

    move-result-object v3

    .line 306
    iget-object v1, p0, Lcom/squareup/ui/items/DuplicateSkuValidator;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    const/4 v2, 0x1

    new-array v4, v2, [Lcom/squareup/api/items/Item$Type;

    sget-object v5, Lcom/squareup/api/items/Item$Type;->GIFT_CARD:Lcom/squareup/api/items/Item$Type;

    const/4 v6, 0x0

    aput-object v5, v4, v6

    invoke-virtual {v1, v4}, Lcom/squareup/settings/server/AccountStatusSettings;->supportedCatalogItemTypesExcluding([Lcom/squareup/api/items/Item$Type;)Ljava/util/List;

    move-result-object v4

    .line 307
    new-instance v5, Ljava/util/LinkedHashMap;

    invoke-direct {v5}, Ljava/util/LinkedHashMap;-><init>()V

    .line 308
    new-instance v6, Ljava/util/LinkedHashMap;

    invoke-direct {v6}, Ljava/util/LinkedHashMap;-><init>()V

    .line 309
    iget-object v1, p0, Lcom/squareup/ui/items/DuplicateSkuValidator;->editItemState:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 310
    invoke-virtual {v7}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v5, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 311
    invoke-virtual {v7}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getId()Ljava/lang/String;

    move-result-object v8

    .line 312
    invoke-virtual {v7}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getSku()Ljava/lang/String;

    move-result-object v7

    iget-object v9, p0, Lcom/squareup/ui/items/DuplicateSkuValidator;->locale:Ljava/util/Locale;

    invoke-virtual {v7, v9}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v7

    .line 311
    invoke-interface {v6, v8, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 315
    :cond_0
    sget-object v1, Lcom/squareup/ui/items/DuplicateSkuValidator$1;->$SwitchMap$com$squareup$ui$items$DuplicateSkuValidator$DuplicateSkuValidationTarget:[I

    invoke-virtual {p2}, Lcom/squareup/ui/items/DuplicateSkuValidator$DuplicateSkuValidationTarget;->ordinal()I

    move-result v7

    aget v1, v1, v7

    if-eq v1, v2, :cond_2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 323
    iget-object p2, p0, Lcom/squareup/ui/items/DuplicateSkuValidator;->locale:Ljava/util/Locale;

    invoke-virtual {p5, p2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 324
    invoke-interface {v5, p3, p4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 325
    invoke-interface {v6, p3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v2, p1

    .line 327
    invoke-virtual/range {v0 .. v6}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader;->findDuplicateSkuInfoForNormalizedSku(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/Map;Ljava/util/Map;)Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$DuplicateSkuInfo;

    move-result-object p1

    return-object p1

    .line 331
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "Unexpected DuplicateSkuValidationTarget: "

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    move-object v1, p1

    move-object v2, v3

    move-object v3, v4

    move-object v4, v5

    move-object v5, v6

    .line 317
    invoke-virtual/range {v0 .. v5}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader;->findDuplicateSkuInfoForItem(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/Map;Ljava/util/Map;)Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$DuplicateSkuInfo;

    move-result-object p1

    return-object p1
.end method

.method private getAllVariationIdsWithDuplicateSkuInItem(Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$DuplicateSkuInfo;Ljava/lang/String;)Ljava/util/Set;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$DuplicateSkuInfo;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 240
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    .line 241
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$DuplicateSkuInfo;->getAllDuplicateNormalizedSkus()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 243
    invoke-virtual {p1, v2}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$DuplicateSkuInfo;->getSkuLookupInfosByItemIdsForNormalizedSku(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v2

    .line 244
    invoke-interface {v2, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 245
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;

    .line 246
    iget-object v4, v3, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;->itemId:Ljava/lang/String;

    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 247
    iget-object v3, v3, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;->variationId:Ljava/lang/String;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method private getAllVariationSkusByUpperCaseSkus()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 256
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 257
    iget-object v1, p0, Lcom/squareup/ui/items/DuplicateSkuValidator;->editItemState:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 258
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getSku()Ljava/lang/String;

    move-result-object v2

    .line 259
    iget-object v3, p0, Lcom/squareup/ui/items/DuplicateSkuValidator;->locale:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method private getItemDescription(Ljava/util/List;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 210
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    const-string v2, "item_name"

    const/4 v3, 0x1

    if-ne v0, v3, :cond_1

    .line 211
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;

    .line 212
    iget-boolean v0, p1, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;->isSingleVariation:Z

    if-eqz v0, :cond_0

    .line 213
    iget-object v0, p0, Lcom/squareup/ui/items/DuplicateSkuValidator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/edititem/R$string;->duplicate_sku_warning_item_description_only_one_variation:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;->itemName:Ljava/lang/String;

    iget-object v1, p0, Lcom/squareup/ui/items/DuplicateSkuValidator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/edititem/R$string;->duplicate_sku_warning_unnamed:I

    .line 215
    invoke-interface {v1, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 214
    invoke-static {p1, v1}, Lcom/squareup/util/Strings;->valueOrDefault(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 216
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 217
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 219
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/DuplicateSkuValidator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/edititem/R$string;->duplicate_sku_warning_item_description_one_of_variations:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;->itemName:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/ui/items/DuplicateSkuValidator;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/edititem/R$string;->duplicate_sku_warning_unnamed:I

    .line 221
    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 220
    invoke-static {v1, v3}, Lcom/squareup/util/Strings;->valueOrDefault(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;->variationName:Ljava/lang/String;

    iget-object v1, p0, Lcom/squareup/ui/items/DuplicateSkuValidator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/edititem/R$string;->duplicate_sku_warning_unnamed:I

    .line 223
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 222
    invoke-static {p1, v1}, Lcom/squareup/util/Strings;->valueOrDefault(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string/jumbo v1, "variation_name"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 224
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 225
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 228
    :cond_1
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;

    .line 229
    iget-object v1, p0, Lcom/squareup/ui/items/DuplicateSkuValidator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/edititem/R$string;->duplicate_sku_warning_item_description_multiple_variations:I

    invoke-interface {v1, v3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    iget-object v0, v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;->itemName:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/ui/items/DuplicateSkuValidator;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/edititem/R$string;->duplicate_sku_warning_unnamed:I

    .line 231
    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 230
    invoke-static {v0, v3}, Lcom/squareup/util/Strings;->valueOrDefault(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 232
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    const-string v1, "count"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 233
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 234
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private produceDuplicateSkuMessage(Ljava/lang/String;Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$DuplicateSkuInfo;Ljava/util/Map;)Ljava/lang/String;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$DuplicateSkuInfo;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 111
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$DuplicateSkuInfo;->getAllDuplicateNormalizedSkus()Ljava/util/List;

    move-result-object v0

    .line 112
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-eqz v1, :cond_3

    const/4 v2, 0x1

    const/4 v3, 0x0

    const-string v4, "SKU"

    if-eq v1, v2, :cond_2

    const-string p1, "second_duplicate_sku"

    const-string p2, "first_duplicate_sku"

    const/4 v5, 0x2

    if-eq v1, v5, :cond_1

    const/4 v6, 0x3

    if-eq v1, v6, :cond_0

    .line 139
    iget-object v1, p0, Lcom/squareup/ui/items/DuplicateSkuValidator;->res:Lcom/squareup/util/Res;

    sget v6, Lcom/squareup/edititem/R$string;->duplicate_sku_warning_more_then_three_skus:I

    invoke-interface {v1, v6}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 141
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {p3, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3, v4}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    .line 140
    invoke-virtual {v1, p2, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 143
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    invoke-static {p3, v4}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/lang/CharSequence;

    .line 142
    invoke-virtual {p2, p1, p3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 144
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p2

    sub-int/2addr p2, v5

    const-string p3, "count"

    invoke-virtual {p1, p3, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 145
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 146
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 129
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/items/DuplicateSkuValidator;->res:Lcom/squareup/util/Res;

    sget v6, Lcom/squareup/edititem/R$string;->duplicate_sku_warning_three_skus:I

    invoke-interface {v1, v6}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 131
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {p3, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3, v4}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    .line 130
    invoke-virtual {v1, p2, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 133
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1, v4}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    .line 132
    invoke-virtual {p2, p1, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 135
    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    invoke-interface {p3, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    invoke-static {p2, v4}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    const-string/jumbo p3, "third_duplicate_sku"

    .line 134
    invoke-virtual {p1, p3, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 136
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 137
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 121
    :cond_1
    iget-object v1, p0, Lcom/squareup/ui/items/DuplicateSkuValidator;->res:Lcom/squareup/util/Res;

    sget v5, Lcom/squareup/edititem/R$string;->duplicate_sku_warning_two_skus:I

    invoke-interface {v1, v5}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 123
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {p3, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3, v4}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    .line 122
    invoke-virtual {v1, p2, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 125
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    invoke-static {p3, v4}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/lang/CharSequence;

    .line 124
    invoke-virtual {p2, p1, p3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 126
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 127
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 117
    :cond_2
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    invoke-static {p3, v4}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/lang/String;

    .line 119
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 118
    invoke-direct {p0, p1, v0, p3, p2}, Lcom/squareup/ui/items/DuplicateSkuValidator;->produceDuplicateSkuMessageForSingleDuplicateSku(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$DuplicateSkuInfo;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_3
    const/4 p1, 0x0

    return-object p1
.end method

.method private produceDuplicateSkuMessageForSingleDuplicateSku(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$DuplicateSkuInfo;)Ljava/lang/String;
    .locals 6

    .line 153
    invoke-virtual {p4, p2}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$DuplicateSkuInfo;->getSkuLookupInfosByItemIdsForNormalizedSku(Ljava/lang/String;)Ljava/util/Map;

    move-result-object p2

    .line 157
    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Ljava/util/List;

    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result p4

    const/4 v0, 0x1

    if-ne p4, v0, :cond_0

    .line 158
    invoke-interface {p2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 164
    :cond_0
    new-instance p1, Ljava/util/ArrayList;

    .line 165
    invoke-interface {p2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p4

    invoke-direct {p1, p4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 167
    invoke-interface {p2}, Ljava/util/Map;->size()I

    move-result p2

    const/4 p4, 0x0

    const-string v1, "duplicate_sku"

    if-eqz p2, :cond_3

    if-eq p2, v0, :cond_3

    const-string v2, "second_item"

    const-string v3, "first_item"

    const/4 v4, 0x2

    if-eq p2, v4, :cond_2

    const/4 v5, 0x3

    if-eq p2, v5, :cond_1

    .line 197
    iget-object p2, p0, Lcom/squareup/ui/items/DuplicateSkuValidator;->res:Lcom/squareup/util/Res;

    sget v5, Lcom/squareup/edititem/R$string;->duplicate_sku_warning_one_sku_more_than_three_item:I

    invoke-interface {p2, v5}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 198
    invoke-virtual {p2, v1, p3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 200
    invoke-interface {p1, p4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/util/List;

    invoke-direct {p0, p3}, Lcom/squareup/ui/items/DuplicateSkuValidator;->getItemDescription(Ljava/util/List;)Ljava/lang/String;

    move-result-object p3

    .line 199
    invoke-virtual {p2, v3, p3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 202
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/util/List;

    invoke-direct {p0, p3}, Lcom/squareup/ui/items/DuplicateSkuValidator;->getItemDescription(Ljava/util/List;)Ljava/lang/String;

    move-result-object p3

    .line 201
    invoke-virtual {p2, v2, p3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 203
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    sub-int/2addr p1, v4

    const-string p3, "count"

    invoke-virtual {p2, p3, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 204
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 205
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 186
    :cond_1
    iget-object p2, p0, Lcom/squareup/ui/items/DuplicateSkuValidator;->res:Lcom/squareup/util/Res;

    sget v5, Lcom/squareup/edititem/R$string;->duplicate_sku_warning_one_sku_three_item:I

    invoke-interface {p2, v5}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 187
    invoke-virtual {p2, v1, p3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 189
    invoke-interface {p1, p4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/util/List;

    invoke-direct {p0, p3}, Lcom/squareup/ui/items/DuplicateSkuValidator;->getItemDescription(Ljava/util/List;)Ljava/lang/String;

    move-result-object p3

    .line 188
    invoke-virtual {p2, v3, p3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 191
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/util/List;

    invoke-direct {p0, p3}, Lcom/squareup/ui/items/DuplicateSkuValidator;->getItemDescription(Ljava/util/List;)Ljava/lang/String;

    move-result-object p3

    .line 190
    invoke-virtual {p2, v2, p3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 193
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    invoke-direct {p0, p1}, Lcom/squareup/ui/items/DuplicateSkuValidator;->getItemDescription(Ljava/util/List;)Ljava/lang/String;

    move-result-object p1

    const-string/jumbo p3, "third_item"

    .line 192
    invoke-virtual {p2, p3, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 194
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 195
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 177
    :cond_2
    iget-object p2, p0, Lcom/squareup/ui/items/DuplicateSkuValidator;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/edititem/R$string;->duplicate_sku_warning_one_sku_two_item:I

    invoke-interface {p2, v4}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 178
    invoke-virtual {p2, v1, p3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 180
    invoke-interface {p1, p4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/util/List;

    invoke-direct {p0, p3}, Lcom/squareup/ui/items/DuplicateSkuValidator;->getItemDescription(Ljava/util/List;)Ljava/lang/String;

    move-result-object p3

    .line 179
    invoke-virtual {p2, v3, p3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 182
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    invoke-direct {p0, p1}, Lcom/squareup/ui/items/DuplicateSkuValidator;->getItemDescription(Ljava/util/List;)Ljava/lang/String;

    move-result-object p1

    .line 181
    invoke-virtual {p2, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 183
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 184
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 170
    :cond_3
    iget-object p2, p0, Lcom/squareup/ui/items/DuplicateSkuValidator;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/edititem/R$string;->duplicate_sku_warning_one_sku_one_item:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 171
    invoke-virtual {p2, v1, p3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 173
    invoke-interface {p1, p4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    invoke-direct {p0, p1}, Lcom/squareup/ui/items/DuplicateSkuValidator;->getItemDescription(Ljava/util/List;)Ljava/lang/String;

    move-result-object p1

    const-string p3, "item"

    .line 172
    invoke-virtual {p2, p3, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 174
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 175
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public synthetic lambda$validateSkuAndUpdateErrorsBar$0$DuplicateSkuValidator(Lcom/squareup/ui/items/DuplicateSkuValidator$DuplicateSkuValidationTarget;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$DuplicateSkuInfo;
    .locals 6

    move-object v0, p0

    move-object v1, p5

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    .line 273
    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/items/DuplicateSkuValidator;->findDuplicateSkuInfoWithCogs(Lcom/squareup/shared/catalog/Catalog$Local;Lcom/squareup/ui/items/DuplicateSkuValidator$DuplicateSkuValidationTarget;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$DuplicateSkuInfo;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$validateSkuAndUpdateErrorsBar$1$DuplicateSkuValidator(Ljava/util/Map;Lcom/squareup/ui/items/DuplicateSkuValidator$DuplicateSkuValidationTarget;Ljava/lang/String;Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$DuplicateSkuInfo;)V
    .locals 3

    .line 276
    iget-object v0, p0, Lcom/squareup/ui/items/DuplicateSkuValidator;->editItemState:Lcom/squareup/ui/items/EditItemState;

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemState;->itemId:Ljava/lang/String;

    .line 278
    invoke-direct {p0, v0, p4, p1}, Lcom/squareup/ui/items/DuplicateSkuValidator;->produceDuplicateSkuMessage(Ljava/lang/String;Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$DuplicateSkuInfo;Ljava/util/Map;)Ljava/lang/String;

    move-result-object p1

    .line 279
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 280
    sget-object v1, Lcom/squareup/ui/items/DuplicateSkuValidator$1;->$SwitchMap$com$squareup$ui$items$DuplicateSkuValidator$DuplicateSkuValidationTarget:[I

    invoke-virtual {p2}, Lcom/squareup/ui/items/DuplicateSkuValidator$DuplicateSkuValidationTarget;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    const/4 p4, 0x2

    if-ne v1, p4, :cond_0

    .line 282
    iget-object p2, p0, Lcom/squareup/ui/items/DuplicateSkuValidator;->errorsBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    invoke-virtual {p2, p3, p1}, Lcom/squareup/ui/ErrorsBarPresenter;->addError(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    iget-object p1, p0, Lcom/squareup/ui/items/DuplicateSkuValidator;->variationIdsWithRedSkuRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-static {p3}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    goto :goto_0

    .line 291
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "Unexpected DuplicateSkuValidationTarget: "

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 286
    :cond_1
    iget-object p2, p0, Lcom/squareup/ui/items/DuplicateSkuValidator;->errorsBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    invoke-virtual {p2, v0, p1}, Lcom/squareup/ui/ErrorsBarPresenter;->addError(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    iget-object p1, p0, Lcom/squareup/ui/items/DuplicateSkuValidator;->variationIdsWithRedSkuRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 288
    invoke-direct {p0, p4, v0}, Lcom/squareup/ui/items/DuplicateSkuValidator;->getAllVariationIdsWithDuplicateSkuInItem(Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$DuplicateSkuInfo;Ljava/lang/String;)Ljava/util/Set;

    move-result-object p2

    .line 287
    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    const/4 p1, 0x1

    .line 336
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/items/DuplicateSkuValidator;->isInScope:Ljava/lang/Boolean;

    return-void
.end method

.method public onExitScope()V
    .locals 1

    const/4 v0, 0x0

    .line 340
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/items/DuplicateSkuValidator;->isInScope:Ljava/lang/Boolean;

    .line 341
    iget-object v0, p0, Lcom/squareup/ui/items/DuplicateSkuValidator;->subscriptions:Lrx/subscriptions/CompositeSubscription;

    invoke-virtual {v0}, Lrx/subscriptions/CompositeSubscription;->unsubscribe()V

    return-void
.end method

.method validateSkuAndUpdateErrorsBar(Lcom/squareup/ui/items/DuplicateSkuValidator$DuplicateSkuValidationTarget;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/items/DuplicateSkuValidator$DuplicateSkuValidationTarget;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 267
    sget-object v0, Lcom/squareup/ui/items/DuplicateSkuValidator;->NOT_SPECIFIED:Ljava/lang/String;

    invoke-static {p2, v0}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/ui/items/DuplicateSkuValidator;->NOT_SPECIFIED:Ljava/lang/String;

    .line 268
    invoke-static {p3, v0}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/ui/items/DuplicateSkuValidator;->NOT_SPECIFIED:Ljava/lang/String;

    .line 269
    invoke-static {p5, v0}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 270
    :goto_0
    sget-object v3, Lcom/squareup/ui/items/DuplicateSkuValidator$DuplicateSkuValidationTarget;->FOR_VARIATION:Lcom/squareup/ui/items/DuplicateSkuValidator$DuplicateSkuValidationTarget;

    if-eq p1, v3, :cond_2

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :cond_2
    :goto_1
    invoke-static {v1}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    .line 272
    iget-object v0, p0, Lcom/squareup/ui/items/DuplicateSkuValidator;->subscriptions:Lrx/subscriptions/CompositeSubscription;

    iget-object v1, p0, Lcom/squareup/ui/items/DuplicateSkuValidator;->cogs:Lcom/squareup/cogs/Cogs;

    new-instance v8, Lcom/squareup/ui/items/-$$Lambda$DuplicateSkuValidator$szTr7mFtvqSEsUiPY1DQt-ACbN8;

    move-object v2, v8

    move-object v3, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    move-object v7, p5

    invoke-direct/range {v2 .. v7}, Lcom/squareup/ui/items/-$$Lambda$DuplicateSkuValidator$szTr7mFtvqSEsUiPY1DQt-ACbN8;-><init>(Lcom/squareup/ui/items/DuplicateSkuValidator;Lcom/squareup/ui/items/DuplicateSkuValidator$DuplicateSkuValidationTarget;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    invoke-interface {v1, v8}, Lcom/squareup/cogs/Cogs;->asSingle(Lcom/squareup/shared/catalog/CatalogTask;)Lrx/Single;

    move-result-object p3

    new-instance p5, Lcom/squareup/ui/items/-$$Lambda$DuplicateSkuValidator$4JKAPuvsSqVMsab28L0Y8u7GMgs;

    invoke-direct {p5, p0, p4, p1, p2}, Lcom/squareup/ui/items/-$$Lambda$DuplicateSkuValidator$4JKAPuvsSqVMsab28L0Y8u7GMgs;-><init>(Lcom/squareup/ui/items/DuplicateSkuValidator;Ljava/util/Map;Lcom/squareup/ui/items/DuplicateSkuValidator$DuplicateSkuValidationTarget;Ljava/lang/String;)V

    .line 275
    invoke-virtual {p3, p5}, Lrx/Single;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    .line 272
    invoke-virtual {v0, p1}, Lrx/subscriptions/CompositeSubscription;->add(Lrx/Subscription;)V

    return-void
.end method

.method validateSkuAndUpdateErrorsBarForAllVariations()V
    .locals 8

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/items/DuplicateSkuValidator;->isInScope:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/DuplicateSkuValidator;->errorsBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/ErrorsBarPresenter;->clearErrors()V

    .line 98
    iget-object v0, p0, Lcom/squareup/ui/items/DuplicateSkuValidator;->variationIdsWithRedSkuRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 100
    sget-object v3, Lcom/squareup/ui/items/DuplicateSkuValidator$DuplicateSkuValidationTarget;->FOR_ITEM:Lcom/squareup/ui/items/DuplicateSkuValidator$DuplicateSkuValidationTarget;

    sget-object v5, Lcom/squareup/ui/items/DuplicateSkuValidator;->NOT_SPECIFIED:Ljava/lang/String;

    .line 101
    invoke-direct {p0}, Lcom/squareup/ui/items/DuplicateSkuValidator;->getAllVariationSkusByUpperCaseSkus()Ljava/util/Map;

    move-result-object v6

    sget-object v7, Lcom/squareup/ui/items/DuplicateSkuValidator;->NOT_SPECIFIED:Ljava/lang/String;

    move-object v2, p0

    move-object v4, v5

    .line 100
    invoke-virtual/range {v2 .. v7}, Lcom/squareup/ui/items/DuplicateSkuValidator;->validateSkuAndUpdateErrorsBar(Lcom/squareup/ui/items/DuplicateSkuValidator$DuplicateSkuValidationTarget;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;)V

    return-void
.end method

.method validateSkuAndUpdateErrorsBarForSingleVariation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/items/DuplicateSkuValidator;->isInScope:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 83
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/DuplicateSkuValidator;->errorsBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/ErrorsBarPresenter;->clearErrors()V

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/items/DuplicateSkuValidator;->variationIdsWithRedSkuRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 86
    sget-object v3, Lcom/squareup/ui/items/DuplicateSkuValidator$DuplicateSkuValidationTarget;->FOR_VARIATION:Lcom/squareup/ui/items/DuplicateSkuValidator$DuplicateSkuValidationTarget;

    iget-object v0, p0, Lcom/squareup/ui/items/DuplicateSkuValidator;->locale:Ljava/util/Locale;

    .line 87
    invoke-virtual {p3, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p3}, Ljava/util/Collections;->singletonMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v6

    move-object v2, p0

    move-object v4, p1

    move-object v5, p2

    move-object v7, p3

    .line 86
    invoke-virtual/range {v2 .. v7}, Lcom/squareup/ui/items/DuplicateSkuValidator;->validateSkuAndUpdateErrorsBar(Lcom/squareup/ui/items/DuplicateSkuValidator$DuplicateSkuValidationTarget;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;)V

    return-void
.end method

.method variationIdsWithRedSku()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/items/DuplicateSkuValidator;->variationIdsWithRedSkuRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->asObservable()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method
