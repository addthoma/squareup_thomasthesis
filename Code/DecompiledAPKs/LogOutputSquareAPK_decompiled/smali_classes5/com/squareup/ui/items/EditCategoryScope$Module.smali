.class public Lcom/squareup/ui/items/EditCategoryScope$Module;
.super Ljava/lang/Object;
.source "EditCategoryScope.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditCategoryScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Module"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideEditCategoryState()Lcom/squareup/ui/items/EditCategoryState;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 53
    new-instance v0, Lcom/squareup/ui/items/EditCategoryState;

    invoke-direct {v0}, Lcom/squareup/ui/items/EditCategoryState;-><init>()V

    return-object v0
.end method
