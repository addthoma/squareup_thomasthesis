.class public abstract Lcom/squareup/ui/items/DetailSearchableListState;
.super Ljava/lang/Object;
.source "DetailSearchableListState.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/DetailSearchableListState$ShowList;,
        Lcom/squareup/ui/items/DetailSearchableListState$Create;,
        Lcom/squareup/ui/items/DetailSearchableListState$PreCreate;,
        Lcom/squareup/ui/items/DetailSearchableListState$Edit;,
        Lcom/squareup/ui/items/DetailSearchableListState$PreEdit;,
        Lcom/squareup/ui/items/DetailSearchableListState$ListData;,
        Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;,
        Lcom/squareup/ui/items/DetailSearchableListState$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000^\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u0000 #2\u00020\u0001:\u0008#$%&\'()*B\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010\u001c\u001a\u00020\u001dJ\u0014\u0010\u001e\u001a\u00020\u001f*\u00020 2\u0006\u0010!\u001a\u00020\tH\u0002J\u0014\u0010\"\u001a\u00020\u001f*\u00020 2\u0006\u0010\u0002\u001a\u00020\u0003H\u0002R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006R\u0019\u0010\u0007\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010\u00088F\u00a2\u0006\u0006\u001a\u0004\u0008\n\u0010\u000bR\u0011\u0010\u000c\u001a\u00020\r8F\u00a2\u0006\u0006\u001a\u0004\u0008\u000e\u0010\u000fR\u0011\u0010\u0010\u001a\u00020\u00118F\u00a2\u0006\u0006\u001a\u0004\u0008\u0012\u0010\u0013R\u0011\u0010\u0014\u001a\u00020\r8F\u00a2\u0006\u0006\u001a\u0004\u0008\u0015\u0010\u000fR\u0011\u0010\u0016\u001a\u00020\r8F\u00a2\u0006\u0006\u001a\u0004\u0008\u0017\u0010\u000fR\u0011\u0010\u0018\u001a\u00020\u00198F\u00a2\u0006\u0006\u001a\u0004\u0008\u001a\u0010\u001b\u0082\u0001\u0005+,-./\u00a8\u00060"
    }
    d2 = {
        "Lcom/squareup/ui/items/DetailSearchableListState;",
        "",
        "listData",
        "Lcom/squareup/ui/items/DetailSearchableListState$ListData;",
        "(Lcom/squareup/ui/items/DetailSearchableListState$ListData;)V",
        "getListData",
        "()Lcom/squareup/ui/items/DetailSearchableListState$ListData;",
        "listDataSource",
        "Lcom/squareup/cycler/DataSource;",
        "Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;",
        "getListDataSource",
        "()Lcom/squareup/cycler/DataSource;",
        "possiblyShowSetUpItemGridButton",
        "",
        "getPossiblyShowSetUpItemGridButton",
        "()Z",
        "searchTerm",
        "",
        "getSearchTerm",
        "()Ljava/lang/String;",
        "shouldShowCovertItemButton",
        "getShouldShowCovertItemButton",
        "showCreateFromSearchButton",
        "getShowCreateFromSearchButton",
        "textBag",
        "Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;",
        "getTextBag",
        "()Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;",
        "toSnapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "writeDetailSearchableObject",
        "",
        "Lokio/BufferedSink;",
        "obj",
        "writeListData",
        "Companion",
        "Create",
        "DetailSearchableObject",
        "Edit",
        "ListData",
        "PreCreate",
        "PreEdit",
        "ShowList",
        "Lcom/squareup/ui/items/DetailSearchableListState$ShowList;",
        "Lcom/squareup/ui/items/DetailSearchableListState$Create;",
        "Lcom/squareup/ui/items/DetailSearchableListState$PreCreate;",
        "Lcom/squareup/ui/items/DetailSearchableListState$Edit;",
        "Lcom/squareup/ui/items/DetailSearchableListState$PreEdit;",
        "items-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/items/DetailSearchableListState$Companion;


# instance fields
.field private final listData:Lcom/squareup/ui/items/DetailSearchableListState$ListData;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/items/DetailSearchableListState$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/items/DetailSearchableListState$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/items/DetailSearchableListState;->Companion:Lcom/squareup/ui/items/DetailSearchableListState$Companion;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/ui/items/DetailSearchableListState$ListData;)V
    .locals 0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListState;->listData:Lcom/squareup/ui/items/DetailSearchableListState$ListData;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/ui/items/DetailSearchableListState$ListData;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 23
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/DetailSearchableListState;-><init>(Lcom/squareup/ui/items/DetailSearchableListState$ListData;)V

    return-void
.end method

.method public static final synthetic access$writeDetailSearchableObject(Lcom/squareup/ui/items/DetailSearchableListState;Lokio/BufferedSink;Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;)V
    .locals 0

    .line 23
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/items/DetailSearchableListState;->writeDetailSearchableObject(Lokio/BufferedSink;Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;)V

    return-void
.end method

.method public static final synthetic access$writeListData(Lcom/squareup/ui/items/DetailSearchableListState;Lokio/BufferedSink;Lcom/squareup/ui/items/DetailSearchableListState$ListData;)V
    .locals 0

    .line 23
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/items/DetailSearchableListState;->writeListData(Lokio/BufferedSink;Lcom/squareup/ui/items/DetailSearchableListState$ListData;)V

    return-void
.end method

.method private final writeDetailSearchableObject(Lokio/BufferedSink;Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;)V
    .locals 2

    .line 94
    instance-of v0, p2, Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$MeasurementUnit;

    if-eqz v0, :cond_0

    .line 95
    check-cast p2, Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$MeasurementUnit;

    .line 96
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "typedObj::class.java.name"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 97
    sget-object v0, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper;->Companion:Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;

    invoke-virtual {p2}, Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$MeasurementUnit;->getMeasurementUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;->writeCatalogMeasurementUnit(Lokio/BufferedSink;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V

    :cond_0
    return-void
.end method

.method private final writeListData(Lokio/BufferedSink;Lcom/squareup/ui/items/DetailSearchableListState$ListData;)V
    .locals 2

    .line 103
    invoke-virtual {p2}, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->getTextBag()Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->getSearchHintId()I

    move-result v0

    invoke-interface {p1, v0}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    .line 104
    invoke-virtual {p2}, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->getTextBag()Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->getTitleId()I

    move-result v0

    invoke-interface {p1, v0}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    .line 105
    invoke-virtual {p2}, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->getTextBag()Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->getCreateButtonTextId()I

    move-result v0

    invoke-interface {p1, v0}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    .line 106
    invoke-virtual {p2}, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->getTextBag()Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->getEmptyViewTitleId()I

    move-result v0

    invoke-interface {p1, v0}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    .line 107
    invoke-virtual {p2}, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->getTextBag()Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->getEmptyViewSubtitleLearnMoreLinkId()I

    move-result v0

    invoke-interface {p1, v0}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    .line 108
    invoke-virtual {p2}, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->getTextBag()Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->getObjectNumberLimitHelpTextId()Ljava/lang/Integer;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/items/DetailSearchableListState$writeListData$1;->INSTANCE:Lcom/squareup/ui/items/DetailSearchableListState$writeListData$1;

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-static {p1, v0, v1}, Lcom/squareup/workflow/SnapshotKt;->writeNullable(Lokio/BufferedSink;Ljava/lang/Object;Lkotlin/jvm/functions/Function2;)Lokio/BufferedSink;

    .line 109
    invoke-virtual {p2}, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->getSearchTerm()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 110
    invoke-virtual {p2}, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->getShowCreateFromSearchButton()Z

    move-result v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeBooleanAsInt(Lokio/BufferedSink;Z)Lokio/BufferedSink;

    .line 111
    invoke-virtual {p2}, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->getSupportSetUpItemGridButton()Z

    move-result v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeBooleanAsInt(Lokio/BufferedSink;Z)Lokio/BufferedSink;

    .line 112
    invoke-virtual {p2}, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->getShouldDisableCreateButton()Z

    move-result v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeBooleanAsInt(Lokio/BufferedSink;Z)Lokio/BufferedSink;

    .line 113
    invoke-virtual {p2}, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->getShouldShowCovertItemButton()Z

    move-result p2

    invoke-static {p1, p2}, Lcom/squareup/workflow/SnapshotKt;->writeBooleanAsInt(Lokio/BufferedSink;Z)Lokio/BufferedSink;

    return-void
.end method


# virtual methods
.method public getListData()Lcom/squareup/ui/items/DetailSearchableListState$ListData;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListState;->listData:Lcom/squareup/ui/items/DetailSearchableListState$ListData;

    return-object v0
.end method

.method public final getListDataSource()Lcom/squareup/cycler/DataSource;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/cycler/DataSource<",
            "Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;",
            ">;"
        }
    .end annotation

    .line 28
    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListState;->getListData()Lcom/squareup/ui/items/DetailSearchableListState$ListData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->getListDataSource()Lcom/squareup/cycler/DataSource;

    move-result-object v0

    return-object v0
.end method

.method public final getPossiblyShowSetUpItemGridButton()Z
    .locals 1

    .line 36
    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListState;->getListData()Lcom/squareup/ui/items/DetailSearchableListState$ListData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->getSupportSetUpItemGridButton()Z

    move-result v0

    return v0
.end method

.method public final getSearchTerm()Ljava/lang/String;
    .locals 1

    .line 32
    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListState;->getListData()Lcom/squareup/ui/items/DetailSearchableListState$ListData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->getSearchTerm()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getShouldShowCovertItemButton()Z
    .locals 1

    .line 38
    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListState;->getListData()Lcom/squareup/ui/items/DetailSearchableListState$ListData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->getShouldShowCovertItemButton()Z

    move-result v0

    return v0
.end method

.method public final getShowCreateFromSearchButton()Z
    .locals 1

    .line 34
    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListState;->getListData()Lcom/squareup/ui/items/DetailSearchableListState$ListData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->getShowCreateFromSearchButton()Z

    move-result v0

    return v0
.end method

.method public final getTextBag()Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;
    .locals 1

    .line 30
    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListState;->getListData()Lcom/squareup/ui/items/DetailSearchableListState$ListData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->getTextBag()Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

    move-result-object v0

    return-object v0
.end method

.method public final toSnapshot()Lcom/squareup/workflow/Snapshot;
    .locals 2

    .line 78
    sget-object v0, Lcom/squareup/workflow/Snapshot;->Companion:Lcom/squareup/workflow/Snapshot$Companion;

    new-instance v1, Lcom/squareup/ui/items/DetailSearchableListState$toSnapshot$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/DetailSearchableListState$toSnapshot$1;-><init>(Lcom/squareup/ui/items/DetailSearchableListState;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/Snapshot$Companion;->write(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Snapshot;

    move-result-object v0

    return-object v0
.end method
