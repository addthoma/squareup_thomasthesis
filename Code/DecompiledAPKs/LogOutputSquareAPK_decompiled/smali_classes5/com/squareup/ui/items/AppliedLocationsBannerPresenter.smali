.class public Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;
.super Lmortar/ViewPresenter;
.source "AppliedLocationsBannerPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/items/AppliedLocationsBanner;",
        ">;"
    }
.end annotation


# static fields
.field public static final USE_GREY_BACKGROUND:Z = true


# instance fields
.field private appliedLocationCountDisposable:Lio/reactivex/disposables/Disposable;

.field private final appliedLocationCountFetcher:Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;

.field private final isNewItem:Z

.field private final res:Lcom/squareup/util/Res;

.field private final shouldDisable:Z

.field private final strings:Lcom/squareup/ui/items/ItemEditingStringIds;

.field private final useGreyBackground:Z


# direct methods
.method public constructor <init>(Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;Lcom/squareup/util/Res;Lcom/squareup/ui/items/ItemEditingStringIds;ZZZ)V
    .locals 0

    .line 30
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;->appliedLocationCountFetcher:Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;

    .line 32
    iput-object p2, p0, Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;->res:Lcom/squareup/util/Res;

    .line 33
    iput-object p3, p0, Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;->strings:Lcom/squareup/ui/items/ItemEditingStringIds;

    .line 34
    iput-boolean p4, p0, Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;->isNewItem:Z

    .line 35
    iput-boolean p5, p0, Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;->useGreyBackground:Z

    .line 36
    iput-boolean p6, p0, Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;->shouldDisable:Z

    return-void
.end method

.method private createBannerText(I)Ljava/lang/String;
    .locals 3

    const/4 v0, 0x1

    if-gt p1, v0, :cond_0

    .line 90
    iget-object p1, p0, Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;->res:Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;->strings:Lcom/squareup/ui/items/ItemEditingStringIds;

    iget v0, v0, Lcom/squareup/ui/items/ItemEditingStringIds;->bannerNotSharedResId:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    const/4 v1, 0x2

    if-ne p1, v1, :cond_1

    .line 92
    iget-object p1, p0, Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;->res:Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;->strings:Lcom/squareup/ui/items/ItemEditingStringIds;

    iget v0, v0, Lcom/squareup/ui/items/ItemEditingStringIds;->bannerOneOtherResId:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 94
    :cond_1
    iget-object v1, p0, Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;->res:Lcom/squareup/util/Res;

    iget-object v2, p0, Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;->strings:Lcom/squareup/ui/items/ItemEditingStringIds;

    iget v2, v2, Lcom/squareup/ui/items/ItemEditingStringIds;->bannerSharedResId:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    sub-int/2addr p1, v0

    const-string v0, "num_locations"

    .line 95
    invoke-virtual {v1, v0, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 96
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 97
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private isMultiUnitMerchant()Z
    .locals 3

    .line 82
    iget-boolean v0, p0, Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;->shouldDisable:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 85
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;->appliedLocationCountFetcher:Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;

    invoke-interface {v0}, Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;->getActiveLocationCount()I

    move-result v0

    const/4 v2, 0x1

    if-le v0, v2, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method


# virtual methods
.method public synthetic lambda$onLoad$0$AppliedLocationsBannerPresenter(Lcom/squareup/catalog/online/CatalogAppliedLocationCount;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 45
    invoke-virtual {p0}, Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 46
    instance-of v0, p1, Lcom/squareup/catalog/online/CatalogAppliedLocationCount$Error;

    if-eqz v0, :cond_0

    .line 48
    invoke-virtual {p0}, Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/AppliedLocationsBanner;

    iget-object v0, p0, Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;->res:Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;->strings:Lcom/squareup/ui/items/ItemEditingStringIds;

    iget v1, v1, Lcom/squareup/ui/items/ItemEditingStringIds;->bannerErrorResId:I

    .line 49
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 48
    invoke-virtual {p1, v0}, Lcom/squareup/ui/items/AppliedLocationsBanner;->showItemChangesUnavailableBanner(Ljava/lang/String;)V

    goto :goto_0

    .line 50
    :cond_0
    instance-of v0, p1, Lcom/squareup/catalog/online/CatalogAppliedLocationCount$Data;

    if-eqz v0, :cond_2

    .line 51
    check-cast p1, Lcom/squareup/catalog/online/CatalogAppliedLocationCount$Data;

    .line 53
    invoke-virtual {p1}, Lcom/squareup/catalog/online/CatalogAppliedLocationCount$Data;->getActiveAtCurrentLocation()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 55
    invoke-virtual {p1}, Lcom/squareup/catalog/online/CatalogAppliedLocationCount$Data;->getAppliedLocationCount()I

    move-result p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;->createBannerText(I)Ljava/lang/String;

    move-result-object p1

    .line 56
    invoke-virtual {p0}, Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/AppliedLocationsBanner;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/items/AppliedLocationsBanner;->updateBannerText(Ljava/lang/String;)V

    goto :goto_0

    .line 58
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/AppliedLocationsBanner;

    iget-object v0, p0, Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;->res:Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;->strings:Lcom/squareup/ui/items/ItemEditingStringIds;

    iget v1, v1, Lcom/squareup/ui/items/ItemEditingStringIds;->bannerInactiveResId:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/items/AppliedLocationsBanner;->updateBannerText(Ljava/lang/String;)V

    goto :goto_0

    .line 61
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown appliedLocationCount "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    :goto_0
    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 40
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 41
    iget-boolean p1, p0, Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;->isNewItem:Z

    if-nez p1, :cond_0

    invoke-direct {p0}, Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;->isMultiUnitMerchant()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 42
    iget-object p1, p0, Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;->appliedLocationCountFetcher:Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;

    .line 43
    invoke-interface {p1}, Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;->getAppliedLocationCount()Lio/reactivex/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/items/-$$Lambda$AppliedLocationsBannerPresenter$6J-M_2fZ_EBj9-xrTS-ExxkYgyo;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/-$$Lambda$AppliedLocationsBannerPresenter$6J-M_2fZ_EBj9-xrTS-ExxkYgyo;-><init>(Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;)V

    .line 44
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;->appliedLocationCountDisposable:Lio/reactivex/disposables/Disposable;

    :cond_0
    return-void
.end method

.method protected onSave(Landroid/os/Bundle;)V
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;->appliedLocationCountDisposable:Lio/reactivex/disposables/Disposable;

    if-eqz v0, :cond_0

    .line 71
    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->dispose()V

    const/4 v0, 0x0

    .line 72
    iput-object v0, p0, Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;->appliedLocationCountDisposable:Lio/reactivex/disposables/Disposable;

    .line 74
    :cond_0
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onSave(Landroid/os/Bundle;)V

    return-void
.end method

.method useGreyBackground()Z
    .locals 1

    .line 78
    iget-boolean v0, p0, Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;->useGreyBackground:Z

    return v0
.end method
