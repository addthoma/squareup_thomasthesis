.class Lcom/squareup/ui/items/ItemWithVariation;
.super Ljava/lang/Object;
.source "ItemWithVariation.java"

# interfaces
.implements Lcom/squareup/ui/items/EditDiscountHydratedProduct;


# instance fields
.field final isVariablePricing:Z

.field private item:Lcom/squareup/shared/catalog/models/CatalogItem;

.field private multiVariationPriceRange:Landroidx/core/util/Pair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/core/util/Pair<",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private singleVariationPrice:Lcom/squareup/protos/common/Money;


# direct methods
.method constructor <init>(Lcom/squareup/shared/catalog/models/CatalogItem;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/models/CatalogItem;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/ui/items/ItemWithVariation;->item:Lcom/squareup/shared/catalog/models/CatalogItem;

    .line 24
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p1

    if-lez p1, :cond_3

    .line 27
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    .line 28
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getPrice()Lcom/squareup/protos/common/Money;

    move-result-object v0

    if-nez v0, :cond_0

    .line 29
    iput-boolean v1, p0, Lcom/squareup/ui/items/ItemWithVariation;->isVariablePricing:Z

    return-void

    :cond_1
    const/4 p1, 0x0

    .line 33
    iput-boolean p1, p0, Lcom/squareup/ui/items/ItemWithVariation;->isVariablePricing:Z

    .line 34
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v1, :cond_2

    .line 35
    invoke-interface {p2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getPrice()Lcom/squareup/protos/common/Money;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/items/ItemWithVariation;->singleVariationPrice:Lcom/squareup/protos/common/Money;

    goto :goto_0

    .line 37
    :cond_2
    sget-object v0, Lcom/squareup/ui/items/-$$Lambda$ItemWithVariation$KxMFWGehWBcL6j3ZxJQwGybE-_g;->INSTANCE:Lcom/squareup/ui/items/-$$Lambda$ItemWithVariation$KxMFWGehWBcL6j3ZxJQwGybE-_g;

    invoke-static {p2, v0}, Lcom/squareup/util/SquareCollections;->sortedCopy(Ljava/util/List;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object v0

    .line 39
    new-instance v2, Landroidx/core/util/Pair;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getPrice()Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 40
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    sub-int/2addr p2, v1

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getPrice()Lcom/squareup/protos/common/Money;

    move-result-object p2

    invoke-direct {v2, p1, p2}, Landroidx/core/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v2, p0, Lcom/squareup/ui/items/ItemWithVariation;->multiVariationPriceRange:Landroidx/core/util/Pair;

    :goto_0
    return-void

    .line 25
    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Expected at least 1 variation per item"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method static synthetic lambda$new$0(Lcom/squareup/shared/catalog/models/CatalogItemVariation;Lcom/squareup/shared/catalog/models/CatalogItemVariation;)I
    .locals 2

    .line 38
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getPrice()Lcom/squareup/protos/common/Money;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getPrice()Lcom/squareup/protos/common/Money;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide p0

    sub-long/2addr v0, p0

    long-to-int p0, v0

    return p0
.end method


# virtual methods
.method public getColor()Ljava/lang/String;
    .locals 1

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/items/ItemWithVariation;->item:Lcom/squareup/shared/catalog/models/CatalogItem;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItem;->getColor()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMultiVariationPriceRange()Landroidx/core/util/Pair;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroidx/core/util/Pair<",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/items/ItemWithVariation;->multiVariationPriceRange:Landroidx/core/util/Pair;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/items/ItemWithVariation;->item:Lcom/squareup/shared/catalog/models/CatalogItem;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItem;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSingleVariationPrice()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/items/ItemWithVariation;->singleVariationPrice:Lcom/squareup/protos/common/Money;

    return-object v0
.end method
