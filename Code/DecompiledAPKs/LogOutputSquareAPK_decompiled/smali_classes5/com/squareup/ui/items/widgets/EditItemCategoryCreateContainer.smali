.class public Lcom/squareup/ui/items/widgets/EditItemCategoryCreateContainer;
.super Landroid/widget/FrameLayout;
.source "EditItemCategoryCreateContainer.java"

# interfaces
.implements Lcom/squareup/marin/widgets/HorizontalRule;


# instance fields
.field private final gutterHalf:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 15
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 16
    invoke-virtual {p0}, Lcom/squareup/ui/items/widgets/EditItemCategoryCreateContainer;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/squareup/marin/R$dimen;->marin_gutter_half:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/items/widgets/EditItemCategoryCreateContainer;->gutterHalf:I

    return-void
.end method


# virtual methods
.method public drawSelf()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public getRulePosition()I
    .locals 3

    .line 24
    invoke-virtual {p0}, Lcom/squareup/ui/items/widgets/EditItemCategoryCreateContainer;->getBottom()I

    move-result v0

    invoke-virtual {p0}, Lcom/squareup/ui/items/widgets/EditItemCategoryCreateContainer;->getBottom()I

    move-result v1

    iget v2, p0, Lcom/squareup/ui/items/widgets/EditItemCategoryCreateContainer;->gutterHalf:I

    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method
