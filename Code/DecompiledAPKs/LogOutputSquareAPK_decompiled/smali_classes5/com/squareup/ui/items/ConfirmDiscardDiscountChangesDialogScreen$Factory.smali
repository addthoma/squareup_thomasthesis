.class public Lcom/squareup/ui/items/ConfirmDiscardDiscountChangesDialogScreen$Factory;
.super Ljava/lang/Object;
.source "ConfirmDiscardDiscountChangesDialogScreen.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/ConfirmDiscardDiscountChangesDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 17
    const-class v0, Lcom/squareup/ui/items/EditDiscountScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/EditDiscountScope$Component;

    .line 18
    invoke-interface {v0}, Lcom/squareup/ui/items/EditDiscountScope$Component;->scopeRunner()Lcom/squareup/ui/items/EditDiscountScopeRunner;

    move-result-object v0

    .line 19
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$SvdFHGvw74APZiWZMOpoU0mzX60;

    invoke-direct {v1, v0}, Lcom/squareup/ui/items/-$$Lambda$SvdFHGvw74APZiWZMOpoU0mzX60;-><init>(Lcom/squareup/ui/items/EditDiscountScopeRunner;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/items/-$$Lambda$gIj5UDr7J53nrWZURbNrcJUQtuc;

    invoke-direct {v2, v0}, Lcom/squareup/ui/items/-$$Lambda$gIj5UDr7J53nrWZURbNrcJUQtuc;-><init>(Lcom/squareup/ui/items/EditDiscountScopeRunner;)V

    invoke-static {p1, v1, v2}, Lcom/squareup/ui/ConfirmDiscardChangesAlertDialogFactory;->createConfirmDiscardChangesAlertDialog(Landroid/content/Context;Ljava/lang/Runnable;Ljava/lang/Runnable;)Landroid/app/AlertDialog;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
