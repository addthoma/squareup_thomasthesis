.class public final Lcom/squareup/ui/items/DetailSearchableListWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "DetailSearchableListWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/ui/items/DetailSearchableListInput;",
        "Lcom/squareup/ui/items/DetailSearchableListState;",
        "Lcom/squareup/ui/items/DetailSearchableListResult;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDetailSearchableListWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DetailSearchableListWorkflow.kt\ncom/squareup/ui/items/DetailSearchableListWorkflow\n+ 2 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n+ 3 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 4 Worker.kt\ncom/squareup/workflow/WorkerKt\n*L\n1#1,267:1\n149#2,5:268\n149#2,5:273\n149#2,5:278\n149#2,5:283\n149#2,5:288\n41#3:293\n56#3,2:294\n276#4:296\n*E\n*S KotlinDebug\n*F\n+ 1 DetailSearchableListWorkflow.kt\ncom/squareup/ui/items/DetailSearchableListWorkflow\n*L\n161#1,5:268\n176#1,5:273\n192#1,5:278\n203#1,5:283\n223#1,5:288\n234#1:293\n234#1,2:294\n234#1:296\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0005\u0018\u00002<\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012&\u0012$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t0\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJ\u001a\u0010\u000f\u001a\u00020\u00032\u0006\u0010\u0010\u001a\u00020\u00022\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u0016JA\u0010\u0013\u001a\u00020\u00142\u0012\u0010\u0015\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u00162\u0006\u0010\u0017\u001a\u00020\u00032\u000c\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u001a0\u00192\u0008\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0002\u00a2\u0006\u0002\u0010\u001dJN\u0010\u001e\u001a$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t2\u0006\u0010\u0010\u001a\u00020\u00022\u0006\u0010\u001f\u001a\u00020\u00032\u0012\u0010\u0015\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0016H\u0016J\u0010\u0010 \u001a\u00020\u00122\u0006\u0010\u001f\u001a\u00020\u0003H\u0016R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/ui/items/DetailSearchableListWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/ui/items/DetailSearchableListInput;",
        "Lcom/squareup/ui/items/DetailSearchableListState;",
        "Lcom/squareup/ui/items/DetailSearchableListResult;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "modificationHandlerFactory",
        "Lcom/squareup/ui/items/DetailSearchableListModificationHandlerFactory;",
        "behaviorFactory",
        "Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory;",
        "(Lcom/squareup/ui/items/DetailSearchableListModificationHandlerFactory;Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory;)V",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "loadList",
        "",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "destinationState",
        "objList",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/ui/items/DetailSearchableListData;",
        "objectNumberLimit",
        "",
        "(Lcom/squareup/workflow/RenderContext;Lcom/squareup/ui/items/DetailSearchableListState;Lio/reactivex/Observable;Ljava/lang/Integer;)V",
        "render",
        "state",
        "snapshotState",
        "items-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final behaviorFactory:Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory;

.field private final modificationHandlerFactory:Lcom/squareup/ui/items/DetailSearchableListModificationHandlerFactory;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/items/DetailSearchableListModificationHandlerFactory;Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "modificationHandlerFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "behaviorFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 119
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflow;->modificationHandlerFactory:Lcom/squareup/ui/items/DetailSearchableListModificationHandlerFactory;

    iput-object p2, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflow;->behaviorFactory:Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory;

    return-void
.end method

.method private final loadList(Lcom/squareup/workflow/RenderContext;Lcom/squareup/ui/items/DetailSearchableListState;Lio/reactivex/Observable;Ljava/lang/Integer;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/ui/items/DetailSearchableListState;",
            "-",
            "Lcom/squareup/ui/items/DetailSearchableListResult;",
            ">;",
            "Lcom/squareup/ui/items/DetailSearchableListState;",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/items/DetailSearchableListData;",
            ">;",
            "Ljava/lang/Integer;",
            ")V"
        }
    .end annotation

    .line 293
    sget-object v0, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {p3, v0}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object p3

    const-string/jumbo v0, "this.toFlowable(BUFFER)"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p3, Lorg/reactivestreams/Publisher;

    if-eqz p3, :cond_0

    .line 295
    invoke-static {p3}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p3

    .line 296
    const-class v0, Lcom/squareup/ui/items/DetailSearchableListData;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v0

    new-instance v1, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v1, v0, p3}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v1, Lcom/squareup/workflow/Worker;

    .line 235
    invoke-virtual {p2}, Lcom/squareup/ui/items/DetailSearchableListState;->getSearchTerm()Ljava/lang/String;

    move-result-object p3

    .line 236
    new-instance v0, Lcom/squareup/ui/items/DetailSearchableListWorkflow$loadList$1;

    invoke-direct {v0, p2, p4}, Lcom/squareup/ui/items/DetailSearchableListWorkflow$loadList$1;-><init>(Lcom/squareup/ui/items/DetailSearchableListState;Ljava/lang/Integer;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 234
    invoke-interface {p1, v1, p3, v0}, Lcom/squareup/workflow/RenderContext;->runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    return-void

    .line 295
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type org.reactivestreams.Publisher<T>"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public initialState(Lcom/squareup/ui/items/DetailSearchableListInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/ui/items/DetailSearchableListState;
    .locals 13

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 252
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflow;->behaviorFactory:Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory;

    invoke-virtual {p1}, Lcom/squareup/ui/items/DetailSearchableListInput;->getConfiguration()Lcom/squareup/ui/items/DetailSearchableListConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/items/DetailSearchableListConfiguration;->getDataType()Lcom/squareup/ui/items/DetailSearchableListDataType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory;->getBehaviorFor(Lcom/squareup/ui/items/DetailSearchableListDataType;)Lcom/squareup/ui/items/DetailSearchableListBehavior;

    move-result-object v0

    if-eqz p2, :cond_0

    .line 254
    sget-object v1, Lcom/squareup/ui/items/DetailSearchableListState;->Companion:Lcom/squareup/ui/items/DetailSearchableListState$Companion;

    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {v1, p2}, Lcom/squareup/ui/items/DetailSearchableListState$Companion;->fromSnapshot(Lokio/ByteString;)Lcom/squareup/ui/items/DetailSearchableListState;

    move-result-object p2

    if-eqz p2, :cond_0

    goto :goto_1

    .line 255
    :cond_0
    new-instance p2, Lcom/squareup/ui/items/DetailSearchableListState$ShowList;

    .line 256
    new-instance v12, Lcom/squareup/ui/items/DetailSearchableListState$ListData;

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 257
    invoke-interface {v0}, Lcom/squareup/ui/items/DetailSearchableListBehavior;->getTextBag()Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

    move-result-object v4

    const/4 v5, 0x0

    .line 258
    invoke-interface {v0}, Lcom/squareup/ui/items/DetailSearchableListBehavior;->getShouldShowCreateFromSearchButton()Z

    move-result v6

    .line 259
    invoke-virtual {p1}, Lcom/squareup/ui/items/DetailSearchableListInput;->getSupportSetUpItemGridButton()Z

    move-result v7

    const/4 v8, 0x0

    .line 260
    invoke-virtual {p1}, Lcom/squareup/ui/items/DetailSearchableListInput;->getConfiguration()Lcom/squareup/ui/items/DetailSearchableListConfiguration;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/items/DetailSearchableListConfiguration;->getDataType()Lcom/squareup/ui/items/DetailSearchableListDataType;

    move-result-object p1

    sget-object v0, Lcom/squareup/ui/items/DetailSearchableListDataType;->Services:Lcom/squareup/ui/items/DetailSearchableListDataType;

    if-ne p1, v0, :cond_1

    const/4 p1, 0x1

    const/4 v9, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    const/4 v9, 0x0

    :goto_0
    const/16 v10, 0x4b

    const/4 v11, 0x0

    move-object v1, v12

    .line 256
    invoke-direct/range {v1 .. v11}, Lcom/squareup/ui/items/DetailSearchableListState$ListData;-><init>(Lcom/squareup/cycler/DataSource;Ljava/lang/Integer;Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;Ljava/lang/String;ZZZZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 255
    invoke-direct {p2, v12}, Lcom/squareup/ui/items/DetailSearchableListState$ShowList;-><init>(Lcom/squareup/ui/items/DetailSearchableListState$ListData;)V

    check-cast p2, Lcom/squareup/ui/items/DetailSearchableListState;

    :goto_1
    return-object p2
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 115
    check-cast p1, Lcom/squareup/ui/items/DetailSearchableListInput;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/items/DetailSearchableListWorkflow;->initialState(Lcom/squareup/ui/items/DetailSearchableListInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/ui/items/DetailSearchableListState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 115
    check-cast p1, Lcom/squareup/ui/items/DetailSearchableListInput;

    check-cast p2, Lcom/squareup/ui/items/DetailSearchableListState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/items/DetailSearchableListWorkflow;->render(Lcom/squareup/ui/items/DetailSearchableListInput;Lcom/squareup/ui/items/DetailSearchableListState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/ui/items/DetailSearchableListInput;Lcom/squareup/ui/items/DetailSearchableListState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/items/DetailSearchableListInput;",
            "Lcom/squareup/ui/items/DetailSearchableListState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/ui/items/DetailSearchableListState;",
            "-",
            "Lcom/squareup/ui/items/DetailSearchableListResult;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    const-string v4, "props"

    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "state"

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "context"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 131
    iget-object v4, v0, Lcom/squareup/ui/items/DetailSearchableListWorkflow;->behaviorFactory:Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory;

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/ui/items/DetailSearchableListInput;->getConfiguration()Lcom/squareup/ui/items/DetailSearchableListConfiguration;

    move-result-object v5

    invoke-virtual {v5}, Lcom/squareup/ui/items/DetailSearchableListConfiguration;->getDataType()Lcom/squareup/ui/items/DetailSearchableListDataType;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/squareup/ui/items/DetailSearchableListBehaviorFactory;->getBehaviorFor(Lcom/squareup/ui/items/DetailSearchableListDataType;)Lcom/squareup/ui/items/DetailSearchableListBehavior;

    move-result-object v4

    .line 133
    instance-of v5, v2, Lcom/squareup/ui/items/DetailSearchableListState$ShowList;

    const-string v6, ""

    if-eqz v5, :cond_0

    .line 136
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/ui/items/DetailSearchableListState;->getSearchTerm()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/squareup/ui/items/DetailSearchableListBehavior;->list(Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object v5

    .line 137
    invoke-interface {v4}, Lcom/squareup/ui/items/DetailSearchableListBehavior;->getObjectNumberLimit()Ljava/lang/Integer;

    move-result-object v7

    .line 134
    invoke-direct {v0, v3, v2, v5, v7}, Lcom/squareup/ui/items/DetailSearchableListWorkflow;->loadList(Lcom/squareup/workflow/RenderContext;Lcom/squareup/ui/items/DetailSearchableListState;Lio/reactivex/Observable;Ljava/lang/Integer;)V

    .line 139
    new-instance v5, Lcom/squareup/ui/items/DetailSearchableListScreen;

    move-object v7, v2

    check-cast v7, Lcom/squareup/ui/items/DetailSearchableListState$ShowList;

    new-instance v8, Lcom/squareup/ui/items/DetailSearchableListWorkflow$render$1;

    invoke-direct {v8, v4, v2, v1}, Lcom/squareup/ui/items/DetailSearchableListWorkflow$render$1;-><init>(Lcom/squareup/ui/items/DetailSearchableListBehavior;Lcom/squareup/ui/items/DetailSearchableListState;Lcom/squareup/ui/items/DetailSearchableListInput;)V

    check-cast v8, Lkotlin/jvm/functions/Function1;

    invoke-interface {v3, v8}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v1

    invoke-direct {v5, v7, v1}, Lcom/squareup/ui/items/DetailSearchableListScreen;-><init>(Lcom/squareup/ui/items/DetailSearchableListState$ShowList;Lkotlin/jvm/functions/Function1;)V

    check-cast v5, Lcom/squareup/workflow/legacy/V2Screen;

    .line 269
    new-instance v1, Lcom/squareup/workflow/legacy/Screen;

    .line 270
    const-class v2, Lcom/squareup/ui/items/DetailSearchableListScreen;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v2, v6}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 271
    sget-object v3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v3

    .line 269
    invoke-direct {v1, v2, v5, v3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 162
    sget-object v2, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {v1, v2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object v1

    goto/16 :goto_0

    .line 165
    :cond_0
    instance-of v5, v2, Lcom/squareup/ui/items/DetailSearchableListState$Create;

    if-eqz v5, :cond_1

    .line 166
    iget-object v5, v0, Lcom/squareup/ui/items/DetailSearchableListWorkflow;->modificationHandlerFactory:Lcom/squareup/ui/items/DetailSearchableListModificationHandlerFactory;

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/ui/items/DetailSearchableListInput;->getConfiguration()Lcom/squareup/ui/items/DetailSearchableListConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/items/DetailSearchableListConfiguration;->getDataType()Lcom/squareup/ui/items/DetailSearchableListDataType;

    move-result-object v1

    invoke-virtual {v5, v1}, Lcom/squareup/ui/items/DetailSearchableListModificationHandlerFactory;->createHandler(Lcom/squareup/ui/items/DetailSearchableListDataType;)Lcom/squareup/ui/items/DetailSearchableListCreateHandler;

    move-result-object v1

    .line 167
    move-object v5, v2

    check-cast v5, Lcom/squareup/ui/items/DetailSearchableListState$Create;

    invoke-interface {v1, v3, v5}, Lcom/squareup/ui/items/DetailSearchableListCreateHandler;->renderCreate(Lcom/squareup/workflow/RenderContext;Lcom/squareup/ui/items/DetailSearchableListState$Create;)Ljava/util/Map;

    move-result-object v1

    .line 168
    sget-object v7, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 169
    new-instance v5, Lcom/squareup/ui/items/DetailSearchableListScreen;

    .line 170
    new-instance v8, Lcom/squareup/ui/items/DetailSearchableListState$ShowList;

    .line 171
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/ui/items/DetailSearchableListState;->getListData()Lcom/squareup/ui/items/DetailSearchableListState$ListData;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    .line 172
    invoke-interface {v4}, Lcom/squareup/ui/items/DetailSearchableListBehavior;->getShouldShowCreateFromSearchButton()Z

    move-result v14

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0xef

    const/16 v19, 0x0

    .line 171
    invoke-static/range {v9 .. v19}, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->copy$default(Lcom/squareup/ui/items/DetailSearchableListState$ListData;Lcom/squareup/cycler/DataSource;Ljava/lang/Integer;Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;Ljava/lang/String;ZZZZILjava/lang/Object;)Lcom/squareup/ui/items/DetailSearchableListState$ListData;

    move-result-object v2

    .line 170
    invoke-direct {v8, v2}, Lcom/squareup/ui/items/DetailSearchableListState$ShowList;-><init>(Lcom/squareup/ui/items/DetailSearchableListState$ListData;)V

    .line 175
    sget-object v2, Lcom/squareup/ui/items/DetailSearchableListWorkflow$render$2;->INSTANCE:Lcom/squareup/ui/items/DetailSearchableListWorkflow$render$2;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-interface {v3, v2}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v2

    .line 169
    invoke-direct {v5, v8, v2}, Lcom/squareup/ui/items/DetailSearchableListScreen;-><init>(Lcom/squareup/ui/items/DetailSearchableListState$ShowList;Lkotlin/jvm/functions/Function1;)V

    check-cast v5, Lcom/squareup/workflow/legacy/V2Screen;

    .line 274
    new-instance v8, Lcom/squareup/workflow/legacy/Screen;

    .line 275
    const-class v2, Lcom/squareup/ui/items/DetailSearchableListScreen;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v2, v6}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 276
    sget-object v3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v3

    .line 274
    invoke-direct {v8, v2, v5, v3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    const/4 v9, 0x0

    .line 177
    sget-object v2, Lcom/squareup/workflow/MainAndModal;->MAIN:Lcom/squareup/workflow/MainAndModal;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v10, v2

    check-cast v10, Lcom/squareup/workflow/legacy/Screen;

    .line 178
    sget-object v2, Lcom/squareup/workflow/MainAndModal;->MODAL:Lcom/squareup/workflow/MainAndModal;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/workflow/legacy/Screen;

    const/16 v13, 0xa

    const/4 v14, 0x0

    .line 168
    invoke-static/range {v7 .. v14}, Lcom/squareup/container/PosLayering$Companion;->fullStack$default(Lcom/squareup/container/PosLayering$Companion;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;ILjava/lang/Object;)Ljava/util/Map;

    move-result-object v1

    goto/16 :goto_0

    .line 182
    :cond_1
    instance-of v5, v2, Lcom/squareup/ui/items/DetailSearchableListState$PreCreate;

    if-eqz v5, :cond_2

    .line 185
    new-instance v1, Lcom/squareup/ui/items/DetailSearchableListState$Create;

    invoke-virtual/range {p2 .. p2}, Lcom/squareup/ui/items/DetailSearchableListState;->getListData()Lcom/squareup/ui/items/DetailSearchableListState$ListData;

    move-result-object v5

    invoke-direct {v1, v5}, Lcom/squareup/ui/items/DetailSearchableListState$Create;-><init>(Lcom/squareup/ui/items/DetailSearchableListState$ListData;)V

    check-cast v1, Lcom/squareup/ui/items/DetailSearchableListState;

    .line 186
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/ui/items/DetailSearchableListState;->getSearchTerm()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/squareup/ui/items/DetailSearchableListBehavior;->list(Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object v5

    .line 187
    invoke-interface {v4}, Lcom/squareup/ui/items/DetailSearchableListBehavior;->getObjectNumberLimit()Ljava/lang/Integer;

    move-result-object v4

    .line 184
    invoke-direct {v0, v3, v1, v5, v4}, Lcom/squareup/ui/items/DetailSearchableListWorkflow;->loadList(Lcom/squareup/workflow/RenderContext;Lcom/squareup/ui/items/DetailSearchableListState;Lio/reactivex/Observable;Ljava/lang/Integer;)V

    .line 189
    new-instance v1, Lcom/squareup/ui/items/DetailSearchableListScreen;

    .line 190
    new-instance v4, Lcom/squareup/ui/items/DetailSearchableListState$ShowList;

    invoke-virtual/range {p2 .. p2}, Lcom/squareup/ui/items/DetailSearchableListState;->getListData()Lcom/squareup/ui/items/DetailSearchableListState$ListData;

    move-result-object v2

    invoke-direct {v4, v2}, Lcom/squareup/ui/items/DetailSearchableListState$ShowList;-><init>(Lcom/squareup/ui/items/DetailSearchableListState$ListData;)V

    .line 191
    sget-object v2, Lcom/squareup/ui/items/DetailSearchableListWorkflow$render$3;->INSTANCE:Lcom/squareup/ui/items/DetailSearchableListWorkflow$render$3;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-interface {v3, v2}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v2

    .line 189
    invoke-direct {v1, v4, v2}, Lcom/squareup/ui/items/DetailSearchableListScreen;-><init>(Lcom/squareup/ui/items/DetailSearchableListState$ShowList;Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 279
    new-instance v2, Lcom/squareup/workflow/legacy/Screen;

    .line 280
    const-class v3, Lcom/squareup/ui/items/DetailSearchableListScreen;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    invoke-static {v3, v6}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v3

    .line 281
    sget-object v4, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v4}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v4

    .line 279
    invoke-direct {v2, v3, v1, v4}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 193
    sget-object v1, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {v2, v1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object v1

    goto/16 :goto_0

    .line 196
    :cond_2
    instance-of v5, v2, Lcom/squareup/ui/items/DetailSearchableListState$Edit;

    if-eqz v5, :cond_3

    .line 197
    iget-object v4, v0, Lcom/squareup/ui/items/DetailSearchableListWorkflow;->modificationHandlerFactory:Lcom/squareup/ui/items/DetailSearchableListModificationHandlerFactory;

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/ui/items/DetailSearchableListInput;->getConfiguration()Lcom/squareup/ui/items/DetailSearchableListConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/items/DetailSearchableListConfiguration;->getDataType()Lcom/squareup/ui/items/DetailSearchableListDataType;

    move-result-object v1

    invoke-virtual {v4, v1}, Lcom/squareup/ui/items/DetailSearchableListModificationHandlerFactory;->editHandler(Lcom/squareup/ui/items/DetailSearchableListDataType;)Lcom/squareup/ui/items/DetailSearchableListEditHandler;

    move-result-object v1

    .line 198
    move-object v4, v2

    check-cast v4, Lcom/squareup/ui/items/DetailSearchableListState$Edit;

    invoke-interface {v1, v3, v4}, Lcom/squareup/ui/items/DetailSearchableListEditHandler;->renderEdit(Lcom/squareup/workflow/RenderContext;Lcom/squareup/ui/items/DetailSearchableListState$Edit;)Ljava/util/Map;

    move-result-object v1

    .line 199
    sget-object v7, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 200
    new-instance v4, Lcom/squareup/ui/items/DetailSearchableListScreen;

    .line 201
    new-instance v5, Lcom/squareup/ui/items/DetailSearchableListState$ShowList;

    invoke-virtual/range {p2 .. p2}, Lcom/squareup/ui/items/DetailSearchableListState;->getListData()Lcom/squareup/ui/items/DetailSearchableListState$ListData;

    move-result-object v2

    invoke-direct {v5, v2}, Lcom/squareup/ui/items/DetailSearchableListState$ShowList;-><init>(Lcom/squareup/ui/items/DetailSearchableListState$ListData;)V

    .line 202
    sget-object v2, Lcom/squareup/ui/items/DetailSearchableListWorkflow$render$4;->INSTANCE:Lcom/squareup/ui/items/DetailSearchableListWorkflow$render$4;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-interface {v3, v2}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v2

    .line 200
    invoke-direct {v4, v5, v2}, Lcom/squareup/ui/items/DetailSearchableListScreen;-><init>(Lcom/squareup/ui/items/DetailSearchableListState$ShowList;Lkotlin/jvm/functions/Function1;)V

    check-cast v4, Lcom/squareup/workflow/legacy/V2Screen;

    .line 284
    new-instance v8, Lcom/squareup/workflow/legacy/Screen;

    .line 285
    const-class v2, Lcom/squareup/ui/items/DetailSearchableListScreen;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v2, v6}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 286
    sget-object v3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v3

    .line 284
    invoke-direct {v8, v2, v4, v3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    const/4 v9, 0x0

    .line 204
    sget-object v2, Lcom/squareup/workflow/MainAndModal;->MAIN:Lcom/squareup/workflow/MainAndModal;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v10, v2

    check-cast v10, Lcom/squareup/workflow/legacy/Screen;

    const/4 v11, 0x0

    .line 205
    sget-object v2, Lcom/squareup/workflow/MainAndModal;->MODAL:Lcom/squareup/workflow/MainAndModal;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/workflow/legacy/Screen;

    const/16 v13, 0xa

    const/4 v14, 0x0

    .line 199
    invoke-static/range {v7 .. v14}, Lcom/squareup/container/PosLayering$Companion;->fullStack$default(Lcom/squareup/container/PosLayering$Companion;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;ILjava/lang/Object;)Ljava/util/Map;

    move-result-object v1

    goto :goto_0

    .line 209
    :cond_3
    instance-of v1, v2, Lcom/squareup/ui/items/DetailSearchableListState$PreEdit;

    if-eqz v1, :cond_4

    .line 213
    new-instance v1, Lcom/squareup/ui/items/DetailSearchableListState$Edit;

    .line 214
    move-object v5, v2

    check-cast v5, Lcom/squareup/ui/items/DetailSearchableListState$PreEdit;

    invoke-virtual {v5}, Lcom/squareup/ui/items/DetailSearchableListState$PreEdit;->getSelectedObject()Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;

    move-result-object v5

    .line 215
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/ui/items/DetailSearchableListState;->getListData()Lcom/squareup/ui/items/DetailSearchableListState$ListData;

    move-result-object v7

    .line 213
    invoke-direct {v1, v7, v5}, Lcom/squareup/ui/items/DetailSearchableListState$Edit;-><init>(Lcom/squareup/ui/items/DetailSearchableListState$ListData;Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;)V

    check-cast v1, Lcom/squareup/ui/items/DetailSearchableListState;

    .line 217
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/ui/items/DetailSearchableListState;->getSearchTerm()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/squareup/ui/items/DetailSearchableListBehavior;->list(Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object v5

    .line 218
    invoke-interface {v4}, Lcom/squareup/ui/items/DetailSearchableListBehavior;->getObjectNumberLimit()Ljava/lang/Integer;

    move-result-object v4

    .line 211
    invoke-direct {v0, v3, v1, v5, v4}, Lcom/squareup/ui/items/DetailSearchableListWorkflow;->loadList(Lcom/squareup/workflow/RenderContext;Lcom/squareup/ui/items/DetailSearchableListState;Lio/reactivex/Observable;Ljava/lang/Integer;)V

    .line 220
    new-instance v1, Lcom/squareup/ui/items/DetailSearchableListScreen;

    .line 221
    new-instance v4, Lcom/squareup/ui/items/DetailSearchableListState$ShowList;

    invoke-virtual/range {p2 .. p2}, Lcom/squareup/ui/items/DetailSearchableListState;->getListData()Lcom/squareup/ui/items/DetailSearchableListState$ListData;

    move-result-object v2

    invoke-direct {v4, v2}, Lcom/squareup/ui/items/DetailSearchableListState$ShowList;-><init>(Lcom/squareup/ui/items/DetailSearchableListState$ListData;)V

    .line 222
    sget-object v2, Lcom/squareup/ui/items/DetailSearchableListWorkflow$render$5;->INSTANCE:Lcom/squareup/ui/items/DetailSearchableListWorkflow$render$5;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-interface {v3, v2}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v2

    .line 220
    invoke-direct {v1, v4, v2}, Lcom/squareup/ui/items/DetailSearchableListScreen;-><init>(Lcom/squareup/ui/items/DetailSearchableListState$ShowList;Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 289
    new-instance v2, Lcom/squareup/workflow/legacy/Screen;

    .line 290
    const-class v3, Lcom/squareup/ui/items/DetailSearchableListScreen;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    invoke-static {v3, v6}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v3

    .line 291
    sget-object v4, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v4}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v4

    .line 289
    invoke-direct {v2, v3, v1, v4}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 224
    sget-object v1, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {v2, v1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_4
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1
.end method

.method public snapshotState(Lcom/squareup/ui/items/DetailSearchableListState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 265
    invoke-virtual {p1}, Lcom/squareup/ui/items/DetailSearchableListState;->toSnapshot()Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 115
    check-cast p1, Lcom/squareup/ui/items/DetailSearchableListState;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/DetailSearchableListWorkflow;->snapshotState(Lcom/squareup/ui/items/DetailSearchableListState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
