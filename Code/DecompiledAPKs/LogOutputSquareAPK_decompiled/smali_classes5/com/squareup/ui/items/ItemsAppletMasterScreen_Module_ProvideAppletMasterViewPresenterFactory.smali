.class public final Lcom/squareup/ui/items/ItemsAppletMasterScreen_Module_ProvideAppletMasterViewPresenterFactory;
.super Ljava/lang/Object;
.source "ItemsAppletMasterScreen_Module_ProvideAppletMasterViewPresenterFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/applet/AppletMasterViewPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final actionBarNavigationHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/ActionBarNavigationHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final appletSelectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletSelection;",
            ">;"
        }
    .end annotation
.end field

.field private final catalogIntegrationControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/ActionBarNavigationHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletSelection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ">;)V"
        }
    .end annotation

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/squareup/ui/items/ItemsAppletMasterScreen_Module_ProvideAppletMasterViewPresenterFactory;->actionBarNavigationHelperProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p2, p0, Lcom/squareup/ui/items/ItemsAppletMasterScreen_Module_ProvideAppletMasterViewPresenterFactory;->resProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p3, p0, Lcom/squareup/ui/items/ItemsAppletMasterScreen_Module_ProvideAppletMasterViewPresenterFactory;->appletSelectionProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p4, p0, Lcom/squareup/ui/items/ItemsAppletMasterScreen_Module_ProvideAppletMasterViewPresenterFactory;->catalogIntegrationControllerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/items/ItemsAppletMasterScreen_Module_ProvideAppletMasterViewPresenterFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/ActionBarNavigationHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletSelection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ">;)",
            "Lcom/squareup/ui/items/ItemsAppletMasterScreen_Module_ProvideAppletMasterViewPresenterFactory;"
        }
    .end annotation

    .line 49
    new-instance v0, Lcom/squareup/ui/items/ItemsAppletMasterScreen_Module_ProvideAppletMasterViewPresenterFactory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/items/ItemsAppletMasterScreen_Module_ProvideAppletMasterViewPresenterFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideAppletMasterViewPresenter(Lcom/squareup/applet/ActionBarNavigationHelper;Lcom/squareup/util/Res;Lcom/squareup/applet/AppletSelection;Lcom/squareup/catalogapi/CatalogIntegrationController;)Lcom/squareup/applet/AppletMasterViewPresenter;
    .locals 0

    .line 55
    invoke-static {p0, p1, p2, p3}, Lcom/squareup/ui/items/ItemsAppletMasterScreen$Module;->provideAppletMasterViewPresenter(Lcom/squareup/applet/ActionBarNavigationHelper;Lcom/squareup/util/Res;Lcom/squareup/applet/AppletSelection;Lcom/squareup/catalogapi/CatalogIntegrationController;)Lcom/squareup/applet/AppletMasterViewPresenter;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/applet/AppletMasterViewPresenter;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/applet/AppletMasterViewPresenter;
    .locals 4

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/items/ItemsAppletMasterScreen_Module_ProvideAppletMasterViewPresenterFactory;->actionBarNavigationHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/applet/ActionBarNavigationHelper;

    iget-object v1, p0, Lcom/squareup/ui/items/ItemsAppletMasterScreen_Module_ProvideAppletMasterViewPresenterFactory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v2, p0, Lcom/squareup/ui/items/ItemsAppletMasterScreen_Module_ProvideAppletMasterViewPresenterFactory;->appletSelectionProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/applet/AppletSelection;

    iget-object v3, p0, Lcom/squareup/ui/items/ItemsAppletMasterScreen_Module_ProvideAppletMasterViewPresenterFactory;->catalogIntegrationControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/catalogapi/CatalogIntegrationController;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/ui/items/ItemsAppletMasterScreen_Module_ProvideAppletMasterViewPresenterFactory;->provideAppletMasterViewPresenter(Lcom/squareup/applet/ActionBarNavigationHelper;Lcom/squareup/util/Res;Lcom/squareup/applet/AppletSelection;Lcom/squareup/catalogapi/CatalogIntegrationController;)Lcom/squareup/applet/AppletMasterViewPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/ui/items/ItemsAppletMasterScreen_Module_ProvideAppletMasterViewPresenterFactory;->get()Lcom/squareup/applet/AppletMasterViewPresenter;

    move-result-object v0

    return-object v0
.end method
