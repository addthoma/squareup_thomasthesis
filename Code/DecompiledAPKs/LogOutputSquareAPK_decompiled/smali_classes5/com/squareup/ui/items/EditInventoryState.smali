.class public Lcom/squareup/ui/items/EditInventoryState;
.super Ljava/lang/Object;
.source "EditInventoryState.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/EditInventoryState$InventoryAssignment;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/items/EditInventoryState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private inventoryAssignments:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Lcom/squareup/ui/items/EditInventoryState$InventoryAssignment;",
            ">;"
        }
    .end annotation
.end field

.field inventoryStockCounts:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Ljava/math/BigDecimal;",
            ">;"
        }
    .end annotation
.end field

.field private inventoryStockCountsLoadStatus:Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 150
    new-instance v0, Lcom/squareup/ui/items/EditInventoryState$1;

    invoke-direct {v0}, Lcom/squareup/ui/items/EditInventoryState$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/items/EditInventoryState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/util/LinkedHashMap;Ljava/util/LinkedHashMap;Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Ljava/math/BigDecimal;",
            ">;",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Lcom/squareup/ui/items/EditInventoryState$InventoryAssignment;",
            ">;",
            "Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;",
            ")V"
        }
    .end annotation

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p1, p0, Lcom/squareup/ui/items/EditInventoryState;->inventoryStockCounts:Ljava/util/LinkedHashMap;

    .line 55
    iput-object p2, p0, Lcom/squareup/ui/items/EditInventoryState;->inventoryAssignments:Ljava/util/LinkedHashMap;

    .line 56
    iput-object p3, p0, Lcom/squareup/ui/items/EditInventoryState;->inventoryStockCountsLoadStatus:Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;

    return-void
.end method

.method synthetic constructor <init>(Ljava/util/LinkedHashMap;Ljava/util/LinkedHashMap;Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;Lcom/squareup/ui/items/EditInventoryState$1;)V
    .locals 0

    .line 42
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/items/EditInventoryState;-><init>(Ljava/util/LinkedHashMap;Ljava/util/LinkedHashMap;Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;)V

    return-void
.end method

.method static createEditInventoryState()Lcom/squareup/ui/items/EditInventoryState;
    .locals 4

    .line 48
    new-instance v0, Lcom/squareup/ui/items/EditInventoryState;

    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2}, Ljava/util/LinkedHashMap;-><init>()V

    sget-object v3, Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;->NOT_REQUESTED:Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/ui/items/EditInventoryState;-><init>(Ljava/util/LinkedHashMap;Ljava/util/LinkedHashMap;Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;)V

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method getCurrentStockCountForVariation(Ljava/lang/String;)Ljava/math/BigDecimal;
    .locals 2

    .line 123
    iget-object v0, p0, Lcom/squareup/ui/items/EditInventoryState;->inventoryStockCounts:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/math/BigDecimal;

    .line 124
    iget-object v1, p0, Lcom/squareup/ui/items/EditInventoryState;->inventoryAssignments:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/items/EditInventoryState$InventoryAssignment;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    .line 125
    :cond_0
    iget-object v1, v1, Lcom/squareup/ui/items/EditInventoryState$InventoryAssignment;->count:Ljava/math/BigDecimal;

    .line 128
    :goto_0
    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/EditInventoryState;->hasServerStockCountForVariation(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    move-object v0, v1

    :goto_1
    return-object v0
.end method

.method getCurrentUnitCostForVariation(Ljava/lang/String;)Lcom/squareup/protos/common/Money;
    .locals 1

    .line 134
    iget-object v0, p0, Lcom/squareup/ui/items/EditInventoryState;->inventoryAssignments:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/EditInventoryState$InventoryAssignment;

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 135
    :cond_0
    iget-object p1, p1, Lcom/squareup/ui/items/EditInventoryState$InventoryAssignment;->unitCost:Lcom/squareup/protos/common/Money;

    :goto_0
    return-object p1
.end method

.method getInventoryAssignments()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/ui/items/EditInventoryState$InventoryAssignment;",
            ">;"
        }
    .end annotation

    .line 143
    iget-object v0, p0, Lcom/squareup/ui/items/EditInventoryState;->inventoryAssignments:Ljava/util/LinkedHashMap;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method getInventoryStockCountsLoadStatus()Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;
    .locals 1

    .line 139
    iget-object v0, p0, Lcom/squareup/ui/items/EditInventoryState;->inventoryStockCountsLoadStatus:Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;

    return-object v0
.end method

.method hasServerStockCountForVariation(Ljava/lang/String;)Z
    .locals 1

    .line 147
    iget-object v0, p0, Lcom/squareup/ui/items/EditInventoryState;->inventoryStockCounts:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method removeInventoryAssignment(Ljava/lang/String;)V
    .locals 1

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/items/EditInventoryState;->inventoryAssignments:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method updateInventoryAssignment(Ljava/lang/String;Ljava/math/BigDecimal;Lcom/squareup/protos/common/Money;)V
    .locals 2

    if-nez p2, :cond_1

    if-nez p3, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    const-string v1, "A unitCost assignment is not allowed when no count is specified"

    .line 77
    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    if-nez p2, :cond_2

    if-nez p3, :cond_2

    .line 80
    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/EditInventoryState;->removeInventoryAssignment(Ljava/lang/String;)V

    return-void

    .line 84
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/items/EditInventoryState;->inventoryAssignments:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/EditInventoryState$InventoryAssignment;

    if-eqz v0, :cond_3

    .line 86
    iput-object p2, v0, Lcom/squareup/ui/items/EditInventoryState$InventoryAssignment;->count:Ljava/math/BigDecimal;

    .line 87
    iput-object p3, v0, Lcom/squareup/ui/items/EditInventoryState$InventoryAssignment;->unitCost:Lcom/squareup/protos/common/Money;

    goto :goto_2

    .line 89
    :cond_3
    iget-object v0, p0, Lcom/squareup/ui/items/EditInventoryState;->inventoryAssignments:Ljava/util/LinkedHashMap;

    new-instance v1, Lcom/squareup/ui/items/EditInventoryState$InventoryAssignment;

    invoke-direct {v1, p2, p3}, Lcom/squareup/ui/items/EditInventoryState$InventoryAssignment;-><init>(Ljava/math/BigDecimal;Lcom/squareup/protos/common/Money;)V

    invoke-virtual {v0, p1, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_2
    return-void
.end method

.method updateInventoryStockCount(Ljava/lang/String;Ljava/math/BigDecimal;)V
    .locals 2

    .line 94
    iget-object v0, p0, Lcom/squareup/ui/items/EditInventoryState;->inventoryStockCounts:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/squareup/ui/items/EditInventoryState;->inventoryStockCounts:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    sget-object p1, Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;->SUCCESS:Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;

    iput-object p1, p0, Lcom/squareup/ui/items/EditInventoryState;->inventoryStockCountsLoadStatus:Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;

    return-void

    .line 95
    :cond_0
    new-instance p2, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "The stock count of variation with id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " cannot be updated before it gets an existing stock count."

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method updateInventoryStockCountsLoadStatus(Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;)V
    .locals 0

    .line 67
    iput-object p1, p0, Lcom/squareup/ui/items/EditInventoryState;->inventoryStockCountsLoadStatus:Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;

    return-void
.end method

.method updateInventoryStockCountsWithDataFromResponse(Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsDataFromResponse;)V
    .locals 2

    .line 60
    iget-object v0, p1, Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsDataFromResponse;->status:Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;

    iput-object v0, p0, Lcom/squareup/ui/items/EditInventoryState;->inventoryStockCountsLoadStatus:Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;

    .line 61
    iget-object v0, p1, Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsDataFromResponse;->status:Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;

    sget-object v1, Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;->SUCCESS:Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;

    if-ne v0, v1, :cond_0

    .line 62
    iget-object p1, p1, Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsDataFromResponse;->inventoryStockCounts:Ljava/util/LinkedHashMap;

    iput-object p1, p0, Lcom/squareup/ui/items/EditInventoryState;->inventoryStockCounts:Ljava/util/LinkedHashMap;

    :cond_0
    return-void
.end method

.method updateVariationIdsForInventoryAssignments(Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 114
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 115
    iget-object v2, p0, Lcom/squareup/ui/items/EditInventoryState;->inventoryAssignments:Ljava/util/LinkedHashMap;

    invoke-virtual {v2, v1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/items/EditInventoryState$InventoryAssignment;

    if-eqz v2, :cond_0

    .line 117
    iget-object v3, p0, Lcom/squareup/ui/items/EditInventoryState;->inventoryAssignments:Ljava/util/LinkedHashMap;

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v3, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 172
    iget-object p2, p0, Lcom/squareup/ui/items/EditInventoryState;->inventoryStockCounts:Ljava/util/LinkedHashMap;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 173
    iget-object p2, p0, Lcom/squareup/ui/items/EditInventoryState;->inventoryAssignments:Ljava/util/LinkedHashMap;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 174
    iget-object p2, p0, Lcom/squareup/ui/items/EditInventoryState;->inventoryStockCountsLoadStatus:Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;

    invoke-virtual {p2}, Lcom/squareup/ui/items/EditInventoryStateController$InventoryStockCountsLoadStatus;->ordinal()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
