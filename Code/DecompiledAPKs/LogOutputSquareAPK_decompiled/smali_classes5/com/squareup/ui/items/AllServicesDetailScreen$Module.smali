.class public abstract Lcom/squareup/ui/items/AllServicesDetailScreen$Module;
.super Ljava/lang/Object;
.source "AllServicesDetailScreen.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/AllServicesDetailScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401
    name = "Module"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/items/AllServicesDetailScreen;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/items/AllServicesDetailScreen;)V
    .locals 0

    .line 127
    iput-object p1, p0, Lcom/squareup/ui/items/AllServicesDetailScreen$Module;->this$0:Lcom/squareup/ui/items/AllServicesDetailScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideAllElementDetailPresenter(Lcom/squareup/ui/items/AllServicesDetailScreen$AllServicesDetailPresenter;)Lcom/squareup/ui/items/AllItemsOrServicesDetailPresenter;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
