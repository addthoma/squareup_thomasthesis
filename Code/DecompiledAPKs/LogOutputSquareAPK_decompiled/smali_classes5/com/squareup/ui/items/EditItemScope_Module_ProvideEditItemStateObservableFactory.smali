.class public final Lcom/squareup/ui/items/EditItemScope_Module_ProvideEditItemStateObservableFactory;
.super Ljava/lang/Object;
.source "EditItemScope_Module_ProvideEditItemStateObservableFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lrx/Single<",
        "Lcom/squareup/ui/items/EditItemState;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final scopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemScopeRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemScopeRunner;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemScope_Module_ProvideEditItemStateObservableFactory;->scopeRunnerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/ui/items/EditItemScope_Module_ProvideEditItemStateObservableFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemScopeRunner;",
            ">;)",
            "Lcom/squareup/ui/items/EditItemScope_Module_ProvideEditItemStateObservableFactory;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/ui/items/EditItemScope_Module_ProvideEditItemStateObservableFactory;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/EditItemScope_Module_ProvideEditItemStateObservableFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideEditItemStateObservable(Lcom/squareup/ui/items/EditItemScopeRunner;)Lrx/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/items/EditItemScopeRunner;",
            ")",
            "Lrx/Single<",
            "Lcom/squareup/ui/items/EditItemState;",
            ">;"
        }
    .end annotation

    .line 37
    invoke-static {p0}, Lcom/squareup/ui/items/EditItemScope$Module;->provideEditItemStateObservable(Lcom/squareup/ui/items/EditItemScopeRunner;)Lrx/Single;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lrx/Single;

    return-object p0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemScope_Module_ProvideEditItemStateObservableFactory;->get()Lrx/Single;

    move-result-object v0

    return-object v0
.end method

.method public get()Lrx/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Single<",
            "Lcom/squareup/ui/items/EditItemState;",
            ">;"
        }
    .end annotation

    .line 27
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScope_Module_ProvideEditItemStateObservableFactory;->scopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/EditItemScopeRunner;

    invoke-static {v0}, Lcom/squareup/ui/items/EditItemScope_Module_ProvideEditItemStateObservableFactory;->provideEditItemStateObservable(Lcom/squareup/ui/items/EditItemScopeRunner;)Lrx/Single;

    move-result-object v0

    return-object v0
.end method
