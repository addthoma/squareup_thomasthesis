.class public Lcom/squareup/ui/items/CategoriesListScreen$Presenter;
.super Lcom/squareup/ui/items/DetailSearchableListPresenter;
.source "CategoriesListScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/CategoriesListScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/items/DetailSearchableListPresenter<",
        "Lcom/squareup/ui/items/CategoriesListView;",
        ">;"
    }
.end annotation


# instance fields
.field private final badBus:Lcom/squareup/badbus/BadBus;

.field private final flow:Lflow/Flow;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/badbus/BadBus;Lcom/squareup/cogs/Cogs;Lcom/squareup/util/Device;Lcom/squareup/analytics/Analytics;Lcom/squareup/permissions/PermissionGatekeeper;Lflow/Flow;Lcom/squareup/ui/items/ItemsAppletScopeRunner;Lcom/squareup/ui/items/EditItemGateway;Lcom/squareup/orderentry/OrderEntryAppletGateway;)V
    .locals 11
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v10, p0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    move-object v3, p4

    move-object/from16 v4, p7

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p8

    move-object/from16 v8, p9

    move-object/from16 v9, p10

    .line 52
    invoke-direct/range {v0 .. v9}, Lcom/squareup/ui/items/DetailSearchableListPresenter;-><init>(Lcom/squareup/util/Res;Lcom/squareup/cogs/Cogs;Lcom/squareup/util/Device;Lflow/Flow;Lcom/squareup/analytics/Analytics;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/items/ItemsAppletScopeRunner;Lcom/squareup/ui/items/EditItemGateway;Lcom/squareup/orderentry/OrderEntryAppletGateway;)V

    move-object v0, p2

    .line 54
    iput-object v0, v10, Lcom/squareup/ui/items/CategoriesListScreen$Presenter;->badBus:Lcom/squareup/badbus/BadBus;

    move-object/from16 v0, p7

    .line 55
    iput-object v0, v10, Lcom/squareup/ui/items/CategoriesListScreen$Presenter;->flow:Lflow/Flow;

    return-void
.end method

.method public static synthetic lambda$38S5mWlA23E_v_mFBStgTMpNoHM(Lcom/squareup/ui/items/CategoriesListScreen$Presenter;Lcom/squareup/cogs/CatalogUpdateEvent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/items/CategoriesListScreen$Presenter;->onCogsUpdate(Lcom/squareup/cogs/CatalogUpdateEvent;)V

    return-void
.end method

.method private onCogsUpdate(Lcom/squareup/cogs/CatalogUpdateEvent;)V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 96
    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_CATEGORY:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-virtual {p1, v0}, Lcom/squareup/cogs/CatalogUpdateEvent;->hasOneOf([Lcom/squareup/shared/catalog/models/CatalogObjectType;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 97
    invoke-virtual {p0}, Lcom/squareup/ui/items/CategoriesListScreen$Presenter;->forceRefresh()V

    :cond_0
    return-void
.end method


# virtual methods
.method protected getActionBarTitle()Ljava/lang/CharSequence;
    .locals 2

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/items/CategoriesListScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/registerlib/R$string;->item_library_categories:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getButtonCount()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method getButtonText(I)Ljava/lang/String;
    .locals 1

    .line 80
    iget-object p1, p0, Lcom/squareup/ui/items/CategoriesListScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/itemsapplet/R$string;->items_applet_create_category:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method getCatalogObjectType()Lcom/squareup/shared/catalog/models/CatalogObjectType;
    .locals 1

    .line 88
    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_CATEGORY:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    return-object v0
.end method

.method getItemTypes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method onButtonClicked(I)V
    .locals 1

    .line 75
    iget-object p1, p0, Lcom/squareup/ui/items/CategoriesListScreen$Presenter;->itemsAppletScopeRunner:Lcom/squareup/ui/items/ItemsAppletScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/items/ItemsAppletScopeRunner;->startNewCategoryFlow()V

    .line 76
    iget-object p1, p0, Lcom/squareup/ui/items/CategoriesListScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_CREATE_CATEGORY:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 59
    invoke-super {p0, p1}, Lcom/squareup/ui/items/DetailSearchableListPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/items/CategoriesListScreen$Presenter;->badBus:Lcom/squareup/badbus/BadBus;

    const-class v1, Lcom/squareup/cogs/CatalogUpdateEvent;

    invoke-virtual {v0, v1}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$CategoriesListScreen$Presenter$38S5mWlA23E_v_mFBStgTMpNoHM;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/-$$Lambda$CategoriesListScreen$Presenter$38S5mWlA23E_v_mFBStgTMpNoHM;-><init>(Lcom/squareup/ui/items/CategoriesListScreen$Presenter;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method onRowClicked(I)V
    .locals 3

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/items/CategoriesListScreen$Presenter;->itemCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->moveToPosition(I)Z

    .line 69
    iget-object p1, p0, Lcom/squareup/ui/items/CategoriesListScreen$Presenter;->itemCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getLibraryEntry()Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object p1

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/items/CategoriesListScreen$Presenter;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/items/CategoryDetailScreen;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getObjectId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, v2, p1}, Lcom/squareup/ui/items/CategoryDetailScreen;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    .line 71
    iget-object p1, p0, Lcom/squareup/ui/items/CategoriesListScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_EDIT_CATEGORY:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    return-void
.end method
