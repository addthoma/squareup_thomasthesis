.class public final Lcom/squareup/ui/items/EditModifierSetMainScreen;
.super Lcom/squareup/ui/items/InEditModifierSetScope;
.source "EditModifierSetMainScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/items/EditModifierSetMainScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/EditModifierSetMainScreen$Component;,
        Lcom/squareup/ui/items/EditModifierSetMainScreen$Module;,
        Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/items/EditModifierSetMainScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 348
    sget-object v0, Lcom/squareup/ui/items/-$$Lambda$EditModifierSetMainScreen$nHtiGjHmYzcz1g6pIZ2092Hf754;->INSTANCE:Lcom/squareup/ui/items/-$$Lambda$EditModifierSetMainScreen$nHtiGjHmYzcz1g6pIZ2092Hf754;

    .line 349
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/items/EditModifierSetMainScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 55
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/InEditModifierSetScope;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/items/EditModifierSetMainScreen;
    .locals 1

    .line 350
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p0

    .line 351
    new-instance v0, Lcom/squareup/ui/items/EditModifierSetMainScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/EditModifierSetMainScreen;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 344
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/items/InEditModifierSetScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 345
    iget-object p2, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen;->modifierSetId:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 59
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->ITEMS_APPLET_EDIT_MODIFIER_SET:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 355
    sget v0, Lcom/squareup/itemsapplet/R$layout;->edit_modifier_set_main_view:I

    return v0
.end method
