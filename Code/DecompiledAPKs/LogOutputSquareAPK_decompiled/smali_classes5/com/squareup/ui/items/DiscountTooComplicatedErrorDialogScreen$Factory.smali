.class public Lcom/squareup/ui/items/DiscountTooComplicatedErrorDialogScreen$Factory;
.super Ljava/lang/Object;
.source "DiscountTooComplicatedErrorDialogScreen.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/DiscountTooComplicatedErrorDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 20
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 22
    sget v1, Lcom/squareup/itemsapplet/R$string;->items_applet_discount_too_complicated_dialog_title:I

    .line 23
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/squareup/itemsapplet/R$string;->items_applet_discount_too_complicated_dialog_message:I

    .line 24
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget v3, Lcom/squareup/itemsapplet/R$string;->items_applet_discount_too_complicated_dialog_button:I

    .line 25
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 22
    invoke-static {v1, v2, v0}, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->noRetry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/register/widgets/FailureAlertDialogFactory;

    move-result-object v0

    .line 26
    invoke-virtual {v0, p1}, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->createFailureAlertDialog(Landroid/content/Context;)Landroid/app/AlertDialog;

    move-result-object p1

    .line 27
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
