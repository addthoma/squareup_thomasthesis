.class public final Lcom/squareup/ui/items/EditVariationRunner$editServiceVariationScreenData$$inlined$combineLatest$1;
.super Ljava/lang/Object;
.source "RxKotlin.kt"

# interfaces
.implements Lio/reactivex/functions/Function3;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/EditVariationRunner;->editServiceVariationScreenData()Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "T3:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function3<",
        "TT1;TT2;TT3;TR;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRxKotlin.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RxKotlin.kt\ncom/squareup/util/rx2/Observables$combineLatest$4\n+ 2 EditVariationRunner.kt\ncom/squareup/ui/items/EditVariationRunner\n*L\n1#1,1655:1\n260#2,33:1656\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0010\u0000\n\u0002\u0008\u0007\n\u0002\u0008\u0007\n\u0002\u0008\u0007\n\u0002\u0008\u0007\n\u0002\u0008\u0007\n\u0002\u0008\u0007\n\u0002\u0008\u0008\u0010\u0000\u001a\u0002H\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0004*\u00020\u0003\"\u0008\u0008\u0002\u0010\u0005*\u00020\u0003\"\u0008\u0008\u0003\u0010\u0001*\u00020\u00032\u0006\u0010\u0006\u001a\u0002H\u00022\u0006\u0010\u0007\u001a\u0002H\u00042\u0006\u0010\u0008\u001a\u0002H\u0005H\n\u00a2\u0006\u0004\u0008\t\u0010\n\u00a8\u0006\u000b"
    }
    d2 = {
        "<anonymous>",
        "R",
        "T1",
        "",
        "T2",
        "T3",
        "t1",
        "t2",
        "t3",
        "apply",
        "(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;",
        "com/squareup/util/rx2/Observables$combineLatest$4"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/items/EditVariationRunner;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/items/EditVariationRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/items/EditVariationRunner$editServiceVariationScreenData$$inlined$combineLatest$1;->this$0:Lcom/squareup/ui/items/EditVariationRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 25
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT1;TT2;TT3;)TR;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    const-string v4, "t1"

    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "t2"

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "t3"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    check-cast v3, Lkotlin/Unit;

    check-cast v2, Ljava/util/List;

    check-cast v1, Lcom/squareup/appointmentsapi/BusinessInfo;

    .line 1656
    iget-object v3, v0, Lcom/squareup/ui/items/EditVariationRunner$editServiceVariationScreenData$$inlined$combineLatest$1;->this$0:Lcom/squareup/ui/items/EditVariationRunner;

    invoke-virtual {v3}, Lcom/squareup/ui/items/EditVariationRunner;->getEditItemVariationState()Lcom/squareup/ui/items/EditVariationState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/ui/items/EditVariationState;->getVariationEditingState()Lcom/squareup/ui/items/ItemVariationEditingState;

    move-result-object v3

    .line 1657
    iget-object v4, v0, Lcom/squareup/ui/items/EditVariationRunner$editServiceVariationScreenData$$inlined$combineLatest$1;->this$0:Lcom/squareup/ui/items/EditVariationRunner;

    invoke-virtual {v4}, Lcom/squareup/ui/items/EditVariationRunner;->getEditItemVariationState()Lcom/squareup/ui/items/EditVariationState;

    move-result-object v4

    invoke-virtual {v4}, Lcom/squareup/ui/items/EditVariationState;->getVariationInEditing()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 1658
    invoke-virtual {v4}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getId()Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    :cond_0
    const/4 v5, 0x0

    :goto_0
    move-object v8, v5

    .line 1659
    iget-object v5, v0, Lcom/squareup/ui/items/EditVariationRunner$editServiceVariationScreenData$$inlined$combineLatest$1;->this$0:Lcom/squareup/ui/items/EditVariationRunner;

    invoke-virtual {v5}, Lcom/squareup/ui/items/EditVariationRunner;->getEditItemVariationState()Lcom/squareup/ui/items/EditVariationState;

    move-result-object v5

    invoke-virtual {v5}, Lcom/squareup/ui/items/EditVariationState;->getMeasurementUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v12

    .line 1662
    sget-object v5, Lcom/squareup/ui/items/ItemVariationEditingState;->NOT_STARTED:Lcom/squareup/ui/items/ItemVariationEditingState;

    const/4 v6, 0x1

    const/4 v7, 0x0

    if-eq v3, v5, :cond_1

    const/4 v5, 0x1

    goto :goto_1

    :cond_1
    const/4 v5, 0x0

    :goto_1
    const-string v9, "Should not be trying to update the service variation subject when variation editing is not started."

    .line 1661
    invoke-static {v5, v9}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 1667
    iget-object v5, v0, Lcom/squareup/ui/items/EditVariationRunner$editServiceVariationScreenData$$inlined$combineLatest$1;->this$0:Lcom/squareup/ui/items/EditVariationRunner;

    invoke-static {v5}, Lcom/squareup/ui/items/EditVariationRunner;->access$getIntermissionHelper$p(Lcom/squareup/ui/items/EditVariationRunner;)Lcom/squareup/intermission/IntermissionHelper;

    move-result-object v5

    if-nez v4, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    invoke-interface {v5, v4}, Lcom/squareup/intermission/IntermissionHelper;->getGapTimeInfo(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;)Lcom/squareup/intermission/GapTimeInfo;

    move-result-object v18

    .line 1669
    new-instance v5, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;

    .line 1670
    sget-object v9, Lcom/squareup/ui/items/ItemVariationEditingState;->CREATING_NEW_VARIATION:Lcom/squareup/ui/items/ItemVariationEditingState;

    if-ne v3, v9, :cond_3

    const/4 v7, 0x1

    :cond_3
    if-nez v8, :cond_4

    .line 1671
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 1672
    :cond_4
    invoke-virtual {v4}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getName()Ljava/lang/String;

    move-result-object v9

    .line 1673
    invoke-virtual {v4}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getPrice()Lcom/squareup/protos/common/Money;

    move-result-object v10

    .line 1674
    iget-object v3, v0, Lcom/squareup/ui/items/EditVariationRunner$editServiceVariationScreenData$$inlined$combineLatest$1;->this$0:Lcom/squareup/ui/items/EditVariationRunner;

    invoke-static {v3}, Lcom/squareup/ui/items/EditVariationRunner;->access$getEditItemState$p(Lcom/squareup/ui/items/EditVariationRunner;)Lcom/squareup/ui/items/EditItemState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/ui/items/EditItemState;->hasInclusiveTaxesApplied()Z

    move-result v11

    .line 1675
    invoke-virtual {v4}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->isVariablePricing()Z

    move-result v13

    .line 1676
    invoke-virtual {v4}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getDuration()J

    move-result-wide v14

    .line 1678
    invoke-virtual {v1}, Lcom/squareup/appointmentsapi/BusinessInfo;->getNoShowProtectionEnabled()Z

    move-result v16

    .line 1679
    invoke-virtual {v4}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getNoShowFee()Lcom/squareup/protos/common/Money;

    move-result-object v17

    .line 1680
    invoke-virtual {v4}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getTransitionTime()J

    move-result-wide v19

    .line 1681
    invoke-virtual {v4}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getAvailableOnOnlineBookingSite()Z

    move-result v21

    .line 1682
    iget-object v1, v0, Lcom/squareup/ui/items/EditVariationRunner$editServiceVariationScreenData$$inlined$combineLatest$1;->this$0:Lcom/squareup/ui/items/EditVariationRunner;

    invoke-static {v1}, Lcom/squareup/ui/items/EditVariationRunner;->access$getEmployeeTextHelper$p(Lcom/squareup/ui/items/EditVariationRunner;)Lcom/squareup/ui/items/EmployeeTextHelper;

    move-result-object v1

    .line 1683
    invoke-virtual {v4}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getEmployeeTokens()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    .line 1682
    invoke-virtual {v1, v3, v2}, Lcom/squareup/ui/items/EmployeeTextHelper;->format(II)Ljava/lang/String;

    move-result-object v22

    .line 1685
    invoke-virtual {v4}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getPriceDescription()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v23, v1

    const-string/jumbo v2, "variationInEditing.priceDescription"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1686
    iget-object v1, v0, Lcom/squareup/ui/items/EditVariationRunner$editServiceVariationScreenData$$inlined$combineLatest$1;->this$0:Lcom/squareup/ui/items/EditVariationRunner;

    invoke-static {v1}, Lcom/squareup/ui/items/EditVariationRunner;->access$isUnitPricedServiceCreationEnabled(Lcom/squareup/ui/items/EditVariationRunner;)Z

    move-result v24

    move-object v6, v5

    .line 1669
    invoke-direct/range {v6 .. v24}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;-><init>(ZLjava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;ZLcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;ZJZLcom/squareup/protos/common/Money;Lcom/squareup/intermission/GapTimeInfo;JZLjava/lang/String;Ljava/lang/String;Z)V

    return-object v5
.end method
