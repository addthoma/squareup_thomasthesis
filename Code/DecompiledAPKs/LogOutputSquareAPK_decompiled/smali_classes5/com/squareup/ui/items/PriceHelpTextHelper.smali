.class public Lcom/squareup/ui/items/PriceHelpTextHelper;
.super Ljava/lang/Object;
.source "PriceHelpTextHelper.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static buildPriceHelpTextForItem(ZZLcom/squareup/protos/common/Money;Landroid/content/res/Resources;Lcom/squareup/quantity/PerUnitFormatter;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    if-eqz p0, :cond_0

    .line 22
    invoke-static {p1, p2, p3, p4, p5}, Lcom/squareup/ui/items/PriceHelpTextHelper;->buildPriceHelpTextForVariation(ZLcom/squareup/protos/common/Money;Landroid/content/res/Resources;Lcom/squareup/quantity/PerUnitFormatter;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_0
    if-eqz p1, :cond_1

    .line 26
    sget p0, Lcom/squareup/edititem/R$string;->tax_included_in_multiple_prices:I

    .line 27
    invoke-virtual {p3, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method static buildPriceHelpTextForVariation(ZLcom/squareup/protos/common/Money;Landroid/content/res/Resources;Lcom/squareup/quantity/PerUnitFormatter;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 39
    sget v0, Lcom/squareup/edititem/R$string;->edit_item_price_message:I

    .line 40
    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-nez p0, :cond_0

    return-object v0

    :cond_0
    const-string p0, " "

    if-nez p1, :cond_1

    .line 45
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    sget p3, Lcom/squareup/edititem/R$string;->tax_included_in_one_variable_price:I

    invoke-virtual {p2, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 50
    :cond_1
    invoke-virtual {p3, p1, p4}, Lcom/squareup/quantity/PerUnitFormatter;->format(Lcom/squareup/protos/common/Money;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 51
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    sget p4, Lcom/squareup/edititem/R$string;->tax_included_in_help_text:I

    invoke-static {p2, p4}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    const-string p4, "money"

    .line 52
    invoke-virtual {p2, p4, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 53
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 54
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method
