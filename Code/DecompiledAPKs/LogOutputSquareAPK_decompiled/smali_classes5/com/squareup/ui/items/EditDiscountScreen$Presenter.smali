.class Lcom/squareup/ui/items/EditDiscountScreen$Presenter;
.super Lcom/squareup/ui/items/BaseEditObjectViewPresenter;
.source "EditDiscountScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditDiscountScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/items/BaseEditObjectViewPresenter<",
        "Lcom/squareup/ui/items/EditDiscountView;",
        ">;"
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final appliedLocationCountFetcher:Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;

.field private final catalogLocalizer:Lcom/squareup/shared/i18n/Localizer;

.field private final cogs:Lcom/squareup/cogs/Cogs;

.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private currentName:Ljava/lang/String;

.field private final discountBundleFactory:Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;

.field private final favoritesTileItemSelectionEvents:Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final flow:Lflow/Flow;

.field private final libraryDeleter:Lcom/squareup/ui/items/LibraryDeleter;

.field private requestInitialFocus:Z

.field private final res:Lcom/squareup/util/Res;

.field private screen:Lcom/squareup/ui/items/EditDiscountScreen;

.field private screenModel:Lcom/squareup/ui/items/EditDiscountScreenModel;

.field private final screenModelBuilder:Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;

.field private final tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

.field private final timeZone:Ljava/util/TimeZone;

.field private tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/cogs/Cogs;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/ui/items/LibraryDeleter;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;Lflow/Flow;Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;Lcom/squareup/util/Clock;Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;Lcom/squareup/settings/server/Features;Lcom/squareup/shared/i18n/Localizer;Lcom/squareup/tutorialv2/TutorialCore;)V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    move-object v1, p5

    move-object v2, p8

    move-object v3, p7

    .line 145
    invoke-direct {p0, p8, p7, p5}, Lcom/squareup/ui/items/BaseEditObjectViewPresenter;-><init>(Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/analytics/Analytics;)V

    move-object v3, p1

    .line 146
    iput-object v3, v0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->res:Lcom/squareup/util/Res;

    move-object v3, p2

    .line 147
    iput-object v3, v0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->cogs:Lcom/squareup/cogs/Cogs;

    move-object v3, p3

    .line 148
    iput-object v3, v0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    move-object v3, p4

    .line 149
    iput-object v3, v0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->libraryDeleter:Lcom/squareup/ui/items/LibraryDeleter;

    .line 150
    iput-object v1, v0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    move-object v1, p6

    .line 151
    iput-object v1, v0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    .line 152
    iput-object v2, v0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->appliedLocationCountFetcher:Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;

    move-object v1, p9

    .line 153
    iput-object v1, v0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->flow:Lflow/Flow;

    move-object v1, p10

    .line 154
    iput-object v1, v0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->favoritesTileItemSelectionEvents:Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;

    .line 155
    invoke-interface {p11}, Lcom/squareup/util/Clock;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->timeZone:Ljava/util/TimeZone;

    move-object/from16 v1, p12

    .line 156
    iput-object v1, v0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->screenModelBuilder:Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;

    move-object/from16 v1, p13

    .line 157
    iput-object v1, v0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->discountBundleFactory:Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;

    move-object/from16 v1, p14

    .line 158
    iput-object v1, v0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    move-object/from16 v1, p15

    .line 159
    iput-object v1, v0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->catalogLocalizer:Lcom/squareup/shared/i18n/Localizer;

    move-object/from16 v1, p16

    .line 160
    iput-object v1, v0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    return-void
.end method

.method private createCatalogDiscountFromView()Lcom/squareup/shared/catalog/models/CatalogDiscount;
    .locals 10

    .line 375
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/EditDiscountView;

    .line 376
    invoke-virtual {v0}, Lcom/squareup/ui/items/EditDiscountView;->getName()Ljava/lang/String;

    move-result-object v2

    .line 381
    invoke-virtual {v0}, Lcom/squareup/ui/items/EditDiscountView;->isPercentage()Z

    move-result v1

    const/4 v3, 0x0

    if-eqz v1, :cond_2

    .line 383
    invoke-virtual {v0}, Lcom/squareup/ui/items/EditDiscountView;->getPercentageText()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v3}, Lcom/squareup/util/Numbers;->parseFormattedPercentage(Ljava/lang/String;Lcom/squareup/util/Percentage;)Lcom/squareup/util/Percentage;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 384
    sget-object v4, Lcom/squareup/util/Percentage;->ZERO:Lcom/squareup/util/Percentage;

    invoke-virtual {v1, v4}, Lcom/squareup/util/Percentage;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move-object v1, v3

    :cond_0
    if-nez v1, :cond_1

    .line 388
    sget-object v1, Lcom/squareup/api/items/Discount$DiscountType;->VARIABLE_PERCENTAGE:Lcom/squareup/api/items/Discount$DiscountType;

    .line 390
    sget-object v4, Lcom/squareup/util/Percentage;->ZERO:Lcom/squareup/util/Percentage;

    goto :goto_0

    .line 392
    :cond_1
    sget-object v4, Lcom/squareup/api/items/Discount$DiscountType;->FIXED:Lcom/squareup/api/items/Discount$DiscountType;

    move-object v9, v4

    move-object v4, v1

    move-object v1, v9

    :goto_0
    move-object v5, v3

    move-object v9, v4

    move-object v4, v1

    move-object v1, v9

    goto :goto_2

    .line 397
    :cond_2
    invoke-virtual {v0}, Lcom/squareup/ui/items/EditDiscountView;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v1

    const-wide/16 v4, 0x0

    if-eqz v1, :cond_3

    .line 398
    iget-object v6, v1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v8, v6, v4

    if-nez v8, :cond_3

    move-object v1, v3

    :cond_3
    if-nez v1, :cond_4

    .line 402
    sget-object v1, Lcom/squareup/api/items/Discount$DiscountType;->VARIABLE_AMOUNT:Lcom/squareup/api/items/Discount$DiscountType;

    .line 404
    iget-object v6, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v4, v5, v6}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v4

    move-object v5, v4

    move-object v4, v1

    goto :goto_1

    .line 406
    :cond_4
    sget-object v4, Lcom/squareup/api/items/Discount$DiscountType;->FIXED:Lcom/squareup/api/items/Discount$DiscountType;

    move-object v5, v1

    :goto_1
    move-object v1, v3

    .line 409
    :goto_2
    invoke-virtual {v0}, Lcom/squareup/ui/items/EditDiscountView;->getDoNotModifyTaxBasis()Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object v0, Lcom/squareup/api/items/Discount$ModifyTaxBasis;->DO_NOT_MODIFY_TAX_BASIS:Lcom/squareup/api/items/Discount$ModifyTaxBasis;

    goto :goto_3

    :cond_5
    sget-object v0, Lcom/squareup/api/items/Discount$ModifyTaxBasis;->MODIFY_TAX_BASIS:Lcom/squareup/api/items/Discount$ModifyTaxBasis;

    :goto_3
    move-object v6, v0

    .line 412
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->screenModel:Lcom/squareup/ui/items/EditDiscountScreenModel;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditDiscountScreenModel;->getDiscount()Lcom/squareup/shared/catalog/models/CatalogDiscount;

    move-result-object v0

    if-nez v1, :cond_6

    move-object v7, v3

    goto :goto_4

    .line 413
    :cond_6
    invoke-virtual {v1}, Lcom/squareup/util/Percentage;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v7, v1

    :goto_4
    move-object v1, v0

    move-object v3, v4

    move-object v4, v7

    .line 412
    invoke-virtual/range {v1 .. v6}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->updateOrSame(Ljava/lang/String;Lcom/squareup/api/items/Discount$DiscountType;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/api/items/Discount$ModifyTaxBasis;)Lcom/squareup/shared/catalog/models/CatalogDiscount;

    move-result-object v0

    return-object v0
.end method

.method private fetchAllCatalogDiscountedProducts(Lcom/squareup/shared/catalog/Catalog$Local;Lcom/squareup/shared/catalog/utils/InflatedPricingRule;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/Catalog$Local;",
            "Lcom/squareup/shared/catalog/utils/InflatedPricingRule;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/models/CatalogObject;",
            ">;"
        }
    .end annotation

    .line 233
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->getFlattenedMatchProductsWithoutExcludes()Ljava/util/Set;

    move-result-object p2

    .line 234
    invoke-interface {p2}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 235
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    return-object p1

    .line 237
    :cond_0
    invoke-direct {p0, p2}, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->mapToDefaultIds(Ljava/util/Set;)Ljava/util/Set;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/squareup/shared/catalog/Catalog$Local;->findObjectsByIds(Ljava/util/Set;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method private fetchAllCatalogProducts(Lcom/squareup/shared/catalog/Catalog$Local;Lcom/squareup/shared/catalog/utils/InflatedPricingRule;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/Catalog$Local;",
            "Lcom/squareup/shared/catalog/utils/InflatedPricingRule;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/models/CatalogObject;",
            ">;"
        }
    .end annotation

    .line 224
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->getFlattenedExcludeProducts()Ljava/util/Set;

    move-result-object p2

    .line 225
    invoke-interface {p2}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 226
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    return-object p1

    .line 228
    :cond_0
    invoke-direct {p0, p2}, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->mapToDefaultIds(Ljava/util/Set;)Ljava/util/Set;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/squareup/shared/catalog/Catalog$Local;->findObjectsByIds(Ljava/util/Set;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method private fetchExistingDiscount()Lrx/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Single<",
            "Lcom/squareup/ui/items/EditDiscountScreenModel;",
            ">;"
        }
    .end annotation

    .line 187
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->cogs:Lcom/squareup/cogs/Cogs;

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$EditDiscountScreen$Presenter$eC9yUWHxWUYFs2-4SJ01xpF4Nbk;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/-$$Lambda$EditDiscountScreen$Presenter$eC9yUWHxWUYFs2-4SJ01xpF4Nbk;-><init>(Lcom/squareup/ui/items/EditDiscountScreen$Presenter;)V

    invoke-interface {v0, v1}, Lcom/squareup/cogs/Cogs;->asSingle(Lcom/squareup/shared/catalog/CatalogTask;)Lrx/Single;

    move-result-object v0

    return-object v0
.end method

.method private fetchFirstPricingRule(Lcom/squareup/shared/catalog/Catalog$Local;Lcom/squareup/shared/catalog/models/CatalogDiscount;)Lcom/squareup/shared/catalog/utils/InflatedPricingRule;
    .locals 3

    .line 213
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->discountBundleFactory:Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;

    .line 214
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->getId()Ljava/lang/String;

    move-result-object p2

    iget-object v1, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->timeZone:Ljava/util/TimeZone;

    iget-object v2, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->catalogLocalizer:Lcom/squareup/shared/i18n/Localizer;

    invoke-virtual {v0, p2, v1, p1, v2}, Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;->forDiscountSynchronous(Ljava/lang/String;Ljava/util/TimeZone;Lcom/squareup/shared/catalog/Catalog$Local;Lcom/squareup/shared/i18n/Localizer;)Lcom/squareup/shared/catalog/utils/DiscountBundle;

    move-result-object p1

    .line 216
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/utils/DiscountBundle;->getRules()Ljava/util/Map;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Collection;->isEmpty()Z

    move-result p2

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 219
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/utils/DiscountBundle;->getRules()Ljava/util/Map;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;

    return-object p1
.end method

.method private finish()V
    .locals 2

    .line 469
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    .line 470
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v1, "Edit Discount Dismissed"

    invoke-interface {v0, v1}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    return-void
.end method

.method private hydrateProduct(Lcom/squareup/shared/catalog/Catalog$Local;Lcom/squareup/shared/catalog/models/CatalogObject;)Lcom/squareup/ui/items/EditDiscountHydratedProduct;
    .locals 2

    .line 260
    instance-of v0, p2, Lcom/squareup/shared/catalog/models/CatalogItemCategory;

    if-eqz v0, :cond_0

    .line 261
    check-cast p2, Lcom/squareup/shared/catalog/models/CatalogItemCategory;

    .line 262
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItemCategory;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/shared/catalog/Catalog$Local;->findCategoryItems(Ljava/lang/String;)Ljava/util/List;

    move-result-object p1

    .line 263
    new-instance v0, Lcom/squareup/ui/items/CategoryWithItemCount;

    invoke-direct {v0, p2, p1}, Lcom/squareup/ui/items/CategoryWithItemCount;-><init>(Lcom/squareup/shared/catalog/models/CatalogItemCategory;Ljava/util/List;)V

    return-object v0

    .line 264
    :cond_0
    instance-of v0, p2, Lcom/squareup/shared/catalog/models/CatalogItem;

    if-eqz v0, :cond_1

    .line 265
    check-cast p2, Lcom/squareup/shared/catalog/models/CatalogItem;

    .line 266
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItem;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/shared/catalog/Catalog$Local;->findItemVariations(Ljava/lang/String;)Ljava/util/List;

    move-result-object p1

    .line 267
    new-instance v0, Lcom/squareup/ui/items/ItemWithVariation;

    invoke-direct {v0, p2, p1}, Lcom/squareup/ui/items/ItemWithVariation;-><init>(Lcom/squareup/shared/catalog/models/CatalogItem;Ljava/util/List;)V

    return-object v0

    .line 268
    :cond_1
    instance-of v0, p2, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    if-eqz v0, :cond_2

    .line 269
    check-cast p2, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    .line 270
    const-class v0, Lcom/squareup/shared/catalog/models/CatalogItem;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getItemId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lcom/squareup/shared/catalog/Catalog$Local;->findById(Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/models/CatalogItem;

    .line 271
    new-instance v0, Lcom/squareup/ui/items/VariationWithParentItem;

    invoke-direct {v0, p2, p1}, Lcom/squareup/ui/items/VariationWithParentItem;-><init>(Lcom/squareup/shared/catalog/models/CatalogItemVariation;Lcom/squareup/shared/catalog/models/CatalogItem;)V

    return-object v0

    .line 273
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Unsupported type"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private hydrateProducts(Lcom/squareup/shared/catalog/Catalog$Local;Ljava/util/Map;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/Catalog$Local;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/models/CatalogObject;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/ui/items/EditDiscountHydratedProduct;",
            ">;"
        }
    .end annotation

    .line 250
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 251
    invoke-interface {p2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/models/CatalogObject;

    .line 252
    invoke-direct {p0, p1, v1}, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->hydrateProduct(Lcom/squareup/shared/catalog/Catalog$Local;Lcom/squareup/shared/catalog/models/CatalogObject;)Lcom/squareup/ui/items/EditDiscountHydratedProduct;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 254
    :cond_0
    sget-object p1, Lcom/squareup/ui/items/EditDiscountHydratedProduct;->COMPARATOR:Ljava/util/Comparator;

    invoke-static {v0, p1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-object v0
.end method

.method public static synthetic lambda$mGFP_voF1XsIQ_wa7_eL5zjWxRQ(Lcom/squareup/ui/items/EditDiscountScreen$Presenter;)V
    .locals 0

    invoke-direct {p0}, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->finish()V

    return-void
.end method

.method private mapToDefaultIds(Ljava/util/Set;)Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;)",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 241
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    .line 242
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/api/sync/ObjectId;

    .line 243
    iget-object v1, v1, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method private maybeRequestInitialFocus()V
    .locals 1

    .line 364
    iget-boolean v0, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->requestInitialFocus:Z

    if-eqz v0, :cond_0

    .line 365
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/EditDiscountView;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditDiscountView;->requestInitialFocus()V

    const/4 v0, 0x0

    .line 366
    iput-boolean v0, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->requestInitialFocus:Z

    :cond_0
    return-void
.end method

.method private showDiscountLoaded()V
    .locals 8

    .line 278
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 282
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_PRICING_ENGINE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 283
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->screenModel:Lcom/squareup/ui/items/EditDiscountScreenModel;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditDiscountScreenModel;->getProductRuleRows()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->screenModel:Lcom/squareup/ui/items/EditDiscountScreenModel;

    .line 284
    invoke-virtual {v0}, Lcom/squareup/ui/items/EditDiscountScreenModel;->getDiscountedProductRuleRows()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 285
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/EditDiscountView;

    iget-object v1, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->screenModel:Lcom/squareup/ui/items/EditDiscountScreenModel;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditDiscountScreenModel;->getProductsTitle()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->screenModel:Lcom/squareup/ui/items/EditDiscountScreenModel;

    .line 286
    invoke-virtual {v2}, Lcom/squareup/ui/items/EditDiscountScreenModel;->getDiscountedProductsTitle()Ljava/lang/String;

    move-result-object v2

    .line 285
    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/items/EditDiscountView;->updateHeaders(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/EditDiscountView;

    iget-object v1, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->screenModel:Lcom/squareup/ui/items/EditDiscountScreenModel;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditDiscountScreenModel;->getProductRuleRows()Ljava/util/List;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/items/EditDiscountScreen$RuleType;->PRODUCT:Lcom/squareup/ui/items/EditDiscountScreen$RuleType;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/items/EditDiscountView;->updateDiscountRule(Ljava/util/List;Lcom/squareup/ui/items/EditDiscountScreen$RuleType;)V

    .line 288
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/EditDiscountView;

    iget-object v1, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->screenModel:Lcom/squareup/ui/items/EditDiscountScreenModel;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditDiscountScreenModel;->getDiscountedProductRuleRows()Ljava/util/List;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/items/EditDiscountScreen$RuleType;->DISCOUNTED_PRODUCT:Lcom/squareup/ui/items/EditDiscountScreen$RuleType;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/items/EditDiscountView;->updateDiscountRule(Ljava/util/List;Lcom/squareup/ui/items/EditDiscountScreen$RuleType;)V

    goto :goto_0

    .line 290
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->screenModel:Lcom/squareup/ui/items/EditDiscountScreenModel;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditDiscountScreenModel;->getDiscountedProductRuleRows()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 291
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->screenModel:Lcom/squareup/ui/items/EditDiscountScreenModel;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditDiscountScreenModel;->getDiscountedProductsTitle()Ljava/lang/String;

    move-result-object v0

    .line 292
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/items/EditDiscountView;

    iget-object v2, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->screenModel:Lcom/squareup/ui/items/EditDiscountScreenModel;

    invoke-virtual {v2}, Lcom/squareup/ui/items/EditDiscountScreenModel;->getProductsTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/squareup/ui/items/EditDiscountView;->updateHeaders(Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/items/EditDiscountView;

    iget-object v2, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->screenModel:Lcom/squareup/ui/items/EditDiscountScreenModel;

    invoke-virtual {v2}, Lcom/squareup/ui/items/EditDiscountScreenModel;->getDiscountedProductRuleRows()Ljava/util/List;

    move-result-object v2

    sget-object v3, Lcom/squareup/ui/items/EditDiscountScreen$RuleType;->PRODUCT:Lcom/squareup/ui/items/EditDiscountScreen$RuleType;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/ui/items/EditDiscountView;->updateDiscountRule(Ljava/util/List;Lcom/squareup/ui/items/EditDiscountScreen$RuleType;)V

    if-eqz v0, :cond_2

    .line 297
    new-instance v0, Lcom/squareup/ui/items/RuleRowItem;

    iget-object v1, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/edititem/R$string;->edit_discount_evenly_across_items:I

    .line 298
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Lcom/squareup/ui/items/RuleRowItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/items/EditDiscountView;

    .line 300
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sget-object v2, Lcom/squareup/ui/items/EditDiscountScreen$RuleType;->DISCOUNTED_PRODUCT:Lcom/squareup/ui/items/EditDiscountScreen$RuleType;

    .line 299
    invoke-virtual {v1, v0, v2}, Lcom/squareup/ui/items/EditDiscountView;->updateDiscountRule(Ljava/util/List;Lcom/squareup/ui/items/EditDiscountScreen$RuleType;)V

    .line 303
    :cond_2
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/EditDiscountView;

    iget-object v1, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->screenModel:Lcom/squareup/ui/items/EditDiscountScreenModel;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditDiscountScreenModel;->getScheduleRuleRows()Ljava/util/List;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/items/EditDiscountScreen$RuleType;->SCHEDULE:Lcom/squareup/ui/items/EditDiscountScreen$RuleType;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/items/EditDiscountView;->updateDiscountRule(Ljava/util/List;Lcom/squareup/ui/items/EditDiscountScreen$RuleType;)V

    .line 304
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/EditDiscountView;

    iget-object v1, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->screenModel:Lcom/squareup/ui/items/EditDiscountScreenModel;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditDiscountScreenModel;->getDateRangeRuleRows()Ljava/util/List;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/items/EditDiscountScreen$RuleType;->DATE_RANGE:Lcom/squareup/ui/items/EditDiscountScreen$RuleType;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/items/EditDiscountView;->updateDiscountRule(Ljava/util/List;Lcom/squareup/ui/items/EditDiscountScreen$RuleType;)V

    .line 307
    :cond_3
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->screenModel:Lcom/squareup/ui/items/EditDiscountScreenModel;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditDiscountScreenModel;->getDiscount()Lcom/squareup/shared/catalog/models/CatalogDiscount;

    move-result-object v0

    .line 308
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->getName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->currentName:Ljava/lang/String;

    .line 309
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->isPercentageDiscount()Z

    move-result v6

    .line 312
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->getDiscountType()Lcom/squareup/api/items/Discount$DiscountType;

    move-result-object v1

    sget-object v2, Lcom/squareup/api/items/Discount$DiscountType;->FIXED:Lcom/squareup/api/items/Discount$DiscountType;

    const/4 v3, 0x0

    if-ne v1, v2, :cond_6

    if-eqz v6, :cond_5

    .line 314
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->getPercentage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v3}, Lcom/squareup/util/Numbers;->parsePercentage(Ljava/lang/String;Lcom/squareup/util/Percentage;)Lcom/squareup/util/Percentage;

    move-result-object v1

    .line 316
    sget-object v2, Lcom/squareup/util/Percentage;->ZERO:Lcom/squareup/util/Percentage;

    invoke-virtual {v1, v2}, Lcom/squareup/util/Percentage;->compareTo(Lcom/squareup/util/Percentage;)I

    move-result v2

    if-ltz v2, :cond_6

    sget-object v2, Lcom/squareup/util/Percentage;->ONE_HUNDRED:Lcom/squareup/util/Percentage;

    invoke-virtual {v1, v2}, Lcom/squareup/util/Percentage;->compareTo(Lcom/squareup/util/Percentage;)I

    move-result v2

    if-lez v2, :cond_4

    goto :goto_1

    :cond_4
    move-object v4, v1

    move-object v5, v3

    goto :goto_2

    .line 320
    :cond_5
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v1

    move-object v5, v1

    move-object v4, v3

    goto :goto_2

    :cond_6
    :goto_1
    move-object v4, v3

    move-object v5, v4

    .line 323
    :goto_2
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->getModifyTaxBasis()Lcom/squareup/api/items/Discount$ModifyTaxBasis;

    move-result-object v0

    sget-object v1, Lcom/squareup/api/items/Discount$ModifyTaxBasis;->DO_NOT_MODIFY_TAX_BASIS:Lcom/squareup/api/items/Discount$ModifyTaxBasis;

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/Discount$ModifyTaxBasis;->equals(Ljava/lang/Object;)Z

    move-result v7

    .line 324
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->updatePrimaryButtonState()V

    .line 325
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/items/EditDiscountView;

    iget-object v3, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->currentName:Ljava/lang/String;

    invoke-virtual/range {v2 .. v7}, Lcom/squareup/ui/items/EditDiscountView;->showDiscountLoaded(Ljava/lang/String;Lcom/squareup/util/Percentage;Lcom/squareup/protos/common/Money;ZZ)V

    .line 327
    invoke-direct {p0}, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->maybeRequestInitialFocus()V

    return-void
.end method


# virtual methods
.method public delete()V
    .locals 3

    .line 474
    sget-object v0, Lcom/squareup/catalog/event/CatalogFeature;->DISCOUNT_DELETED:Lcom/squareup/catalog/event/CatalogFeature;

    iget-object v1, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    iget-object v2, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->screen:Lcom/squareup/ui/items/EditDiscountScreen;

    iget-object v2, v2, Lcom/squareup/ui/items/EditDiscountScreen;->id:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/catalog/event/CatalogFeature;->log(Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V

    .line 476
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->screenModel:Lcom/squareup/ui/items/EditDiscountScreenModel;

    const-string v1, "model"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 477
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->libraryDeleter:Lcom/squareup/ui/items/LibraryDeleter;

    iget-object v1, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->screenModel:Lcom/squareup/ui/items/EditDiscountScreenModel;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditDiscountScreenModel;->getDiscount()Lcom/squareup/shared/catalog/models/CatalogDiscount;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/items/-$$Lambda$EditDiscountScreen$Presenter$mGFP_voF1XsIQ_wa7_eL5zjWxRQ;

    invoke-direct {v2, p0}, Lcom/squareup/ui/items/-$$Lambda$EditDiscountScreen$Presenter$mGFP_voF1XsIQ_wa7_eL5zjWxRQ;-><init>(Lcom/squareup/ui/items/EditDiscountScreen$Presenter;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/items/LibraryDeleter;->delete(Lcom/squareup/shared/catalog/models/CatalogObject;Ljava/lang/Runnable;)V

    return-void
.end method

.method public getCurrentName()Ljava/lang/String;
    .locals 1

    .line 335
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->currentName:Ljava/lang/String;

    return-object v0
.end method

.method public isNewObject()Z
    .locals 2

    .line 331
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->screen:Lcom/squareup/ui/items/EditDiscountScreen;

    iget-object v0, v0, Lcom/squareup/ui/items/EditDiscountScreen;->id:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public synthetic lambda$fetchExistingDiscount$1$EditDiscountScreen$Presenter(Lcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/ui/items/EditDiscountScreenModel;
    .locals 4

    .line 188
    const-class v0, Lcom/squareup/shared/catalog/models/CatalogDiscount;

    iget-object v1, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->screen:Lcom/squareup/ui/items/EditDiscountScreen;

    iget-object v1, v1, Lcom/squareup/ui/items/EditDiscountScreen;->id:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lcom/squareup/shared/catalog/Catalog$Local;->findById(Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogDiscount;

    .line 189
    iget-object v1, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->screenModelBuilder:Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;

    invoke-virtual {v1, v0}, Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;->setDiscount(Lcom/squareup/shared/catalog/models/CatalogDiscount;)Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;

    .line 190
    iget-object v1, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->USE_PRICING_ENGINE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 191
    iget-object p1, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->screenModelBuilder:Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;->build()Lcom/squareup/ui/items/EditDiscountScreenModel;

    move-result-object p1

    return-object p1

    .line 193
    :cond_0
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->fetchFirstPricingRule(Lcom/squareup/shared/catalog/Catalog$Local;Lcom/squareup/shared/catalog/models/CatalogDiscount;)Lcom/squareup/shared/catalog/utils/InflatedPricingRule;

    move-result-object v1

    if-nez v1, :cond_1

    .line 195
    iget-object p1, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->screenModelBuilder:Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;->build()Lcom/squareup/ui/items/EditDiscountScreenModel;

    move-result-object p1

    return-object p1

    .line 197
    :cond_1
    sget-object v2, Lcom/squareup/catalog/event/CatalogFeature;->AUTOMATIC_DISCOUNT_VIEWED:Lcom/squareup/catalog/event/CatalogFeature;

    iget-object v3, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Lcom/squareup/catalog/event/CatalogFeature;->log(Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V

    .line 198
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->screenModelBuilder:Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;->setPricingRule(Lcom/squareup/shared/catalog/utils/InflatedPricingRule;)Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;

    .line 199
    invoke-direct {p0, p1, v1}, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->fetchAllCatalogProducts(Lcom/squareup/shared/catalog/Catalog$Local;Lcom/squareup/shared/catalog/utils/InflatedPricingRule;)Ljava/util/Map;

    move-result-object v0

    .line 200
    invoke-direct {p0, p1, v1}, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->fetchAllCatalogDiscountedProducts(Lcom/squareup/shared/catalog/Catalog$Local;Lcom/squareup/shared/catalog/utils/InflatedPricingRule;)Ljava/util/Map;

    move-result-object v1

    .line 203
    iget-object v2, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->screenModelBuilder:Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;

    .line 204
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->hydrateProducts(Lcom/squareup/shared/catalog/Catalog$Local;Ljava/util/Map;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;->setPricingRuleProducts(Ljava/util/List;)Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;

    move-result-object v0

    .line 205
    invoke-direct {p0, p1, v1}, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->hydrateProducts(Lcom/squareup/shared/catalog/Catalog$Local;Ljava/util/Map;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;->setPricingRuleDiscountedProducts(Ljava/util/List;)Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;

    move-result-object p1

    .line 206
    invoke-virtual {p1}, Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;->build()Lcom/squareup/ui/items/EditDiscountScreenModel;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onEnterScope$0$EditDiscountScreen$Presenter(Lcom/squareup/ui/items/EditDiscountScreenModel;)V
    .locals 0

    .line 180
    iput-object p1, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->screenModel:Lcom/squareup/ui/items/EditDiscountScreenModel;

    .line 181
    invoke-direct {p0}, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->showDiscountLoaded()V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 164
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/EditDiscountScreen;

    iput-object v0, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->screen:Lcom/squareup/ui/items/EditDiscountScreen;

    .line 165
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->useMultiUnitEditingUI()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->appliedLocationCountFetcher:Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 167
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->appliedLocationCountFetcher:Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;

    iget-object v1, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->screen:Lcom/squareup/ui/items/EditDiscountScreen;

    iget-object v1, v1, Lcom/squareup/ui/items/EditDiscountScreen;->id:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;->fetchAppliedLocationCount(Ljava/lang/String;)V

    .line 170
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->isNewObject()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 171
    new-instance p1, Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;

    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogDiscount$Builder;

    invoke-direct {v0}, Lcom/squareup/shared/catalog/models/CatalogDiscount$Builder;-><init>()V

    const-string v1, ""

    .line 172
    invoke-virtual {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogDiscount$Builder;->setName(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogDiscount$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/api/items/Discount$DiscountType;->VARIABLE_PERCENTAGE:Lcom/squareup/api/items/Discount$DiscountType;

    .line 173
    invoke-virtual {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogDiscount$Builder;->setDiscountType(Lcom/squareup/api/items/Discount$DiscountType;)Lcom/squareup/shared/catalog/models/CatalogDiscount$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/util/Percentage;->ZERO:Lcom/squareup/util/Percentage;

    .line 174
    invoke-virtual {v1}, Lcom/squareup/util/Percentage;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogDiscount$Builder;->setPercentage(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogDiscount$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/api/items/Discount$ModifyTaxBasis;->MODIFY_TAX_BASIS:Lcom/squareup/api/items/Discount$ModifyTaxBasis;

    .line 175
    invoke-virtual {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogDiscount$Builder;->setModifyTaxBasis(Lcom/squareup/api/items/Discount$ModifyTaxBasis;)Lcom/squareup/shared/catalog/models/CatalogDiscount$Builder;

    move-result-object v0

    .line 176
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogDiscount$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogDiscount;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;-><init>(Lcom/squareup/shared/catalog/models/CatalogDiscount;)V

    .line 177
    invoke-virtual {p1}, Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;->build()Lcom/squareup/ui/items/EditDiscountScreenModel;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->screenModel:Lcom/squareup/ui/items/EditDiscountScreenModel;

    goto :goto_0

    .line 179
    :cond_1
    invoke-direct {p0}, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->fetchExistingDiscount()Lrx/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$EditDiscountScreen$Presenter$BnxIWy5hvVS90UG1AcqzrQe7B0s;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/-$$Lambda$EditDiscountScreen$Presenter$BnxIWy5hvVS90UG1AcqzrQe7B0s;-><init>(Lcom/squareup/ui/items/EditDiscountScreen$Presenter;)V

    invoke-virtual {v0, v1}, Lrx/Single;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    :goto_0
    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 4

    .line 339
    invoke-super {p0, p1}, Lcom/squareup/ui/items/BaseEditObjectViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 341
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$T1y4-alN7GvKciTysVXOixNoQAs;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/-$$Lambda$T1y4-alN7GvKciTysVXOixNoQAs;-><init>(Lcom/squareup/ui/items/EditDiscountScreen$Presenter;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    .line 342
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v2, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 343
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->isNewObject()Z

    move-result v3

    if-eqz v3, :cond_0

    sget v3, Lcom/squareup/registerlib/R$string;->create_discount:I

    goto :goto_0

    :cond_0
    sget v3, Lcom/squareup/edititem/R$string;->edit_discount:I

    :goto_0
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 342
    invoke-virtual {v0, v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 346
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    .line 347
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    iget-object v2, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/strings/R$string;->save:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonText(Ljava/lang/CharSequence;)V

    .line 348
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    new-instance v2, Lcom/squareup/ui/items/-$$Lambda$Yx95jxA9OoasOZd6hHZZLLqMvOE;

    invoke-direct {v2, p0}, Lcom/squareup/ui/items/-$$Lambda$Yx95jxA9OoasOZd6hHZZLLqMvOE;-><init>(Lcom/squareup/ui/items/EditDiscountScreen$Presenter;)V

    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->showPrimaryButton(Ljava/lang/Runnable;)V

    if-nez p1, :cond_1

    const/4 v1, 0x1

    .line 350
    :cond_1
    iput-boolean v1, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->requestInitialFocus:Z

    .line 351
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/EditDiscountView;

    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;->isTextTileMode()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/items/EditDiscountView;->setTextTile(Z)V

    .line 352
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->isNewObject()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 353
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/EditDiscountView;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditDiscountView;->showNewDiscount()V

    .line 354
    invoke-direct {p0}, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->maybeRequestInitialFocus()V

    goto :goto_1

    .line 355
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->screenModel:Lcom/squareup/ui/items/EditDiscountScreenModel;

    if-eqz p1, :cond_3

    .line 356
    invoke-direct {p0}, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->showDiscountLoaded()V

    goto :goto_1

    .line 358
    :cond_3
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/EditDiscountView;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditDiscountView;->showDiscountLoading()V

    .line 360
    :goto_1
    iget-object p1, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v0, "Edit Discount Shown"

    invoke-interface {p1, v0}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    return-void
.end method

.method onNameChanged(Ljava/lang/String;)V
    .locals 0

    .line 495
    iput-object p1, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->currentName:Ljava/lang/String;

    .line 496
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->updatePrimaryButtonState()V

    return-void
.end method

.method saveDiscount()V
    .locals 7

    .line 417
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->screenModel:Lcom/squareup/ui/items/EditDiscountScreenModel;

    if-nez v0, :cond_0

    .line 418
    invoke-direct {p0}, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->finish()V

    return-void

    .line 422
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->createCatalogDiscountFromView()Lcom/squareup/shared/catalog/models/CatalogDiscount;

    move-result-object v0

    .line 424
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 425
    new-instance v0, Lcom/squareup/widgets/warning/WarningIds;

    sget v1, Lcom/squareup/edititem/R$string;->discount_name_required_warning_title:I

    sget v2, Lcom/squareup/edititem/R$string;->discount_name_required_warning_message:I

    invoke-direct {v0, v1, v2}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    .line 427
    iget-object v1, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->flow:Lflow/Flow;

    new-instance v2, Lcom/squareup/register/widgets/WarningDialogScreen;

    invoke-direct {v2, v0}, Lcom/squareup/register/widgets/WarningDialogScreen;-><init>(Lcom/squareup/widgets/warning/Warning;)V

    invoke-virtual {v1, v2}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void

    .line 431
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/items/EditDiscountView;

    .line 432
    invoke-virtual {v1}, Lcom/squareup/ui/items/EditDiscountView;->getPercentageText()Ljava/lang/String;

    .line 433
    invoke-virtual {v1}, Lcom/squareup/ui/items/EditDiscountView;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v2

    .line 435
    invoke-virtual {v1}, Lcom/squareup/ui/items/EditDiscountView;->isPercentage()Z

    move-result v3

    const/4 v4, 0x1

    if-eqz v3, :cond_2

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditDiscountView;->getPercentageText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_3

    .line 436
    :cond_2
    invoke-virtual {v1}, Lcom/squareup/ui/items/EditDiscountView;->isPercentage()Z

    move-result v1

    if-nez v1, :cond_4

    if-eqz v2, :cond_3

    iget-object v1, v2, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    const-wide/16 v5, 0x0

    cmp-long v3, v1, v5

    if-nez v3, :cond_4

    :cond_3
    const/4 v1, 0x1

    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    .line 435
    :goto_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 438
    iget-object v2, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->screenModel:Lcom/squareup/ui/items/EditDiscountScreenModel;

    invoke-virtual {v2}, Lcom/squareup/ui/items/EditDiscountScreenModel;->getPricingRule()Lcom/squareup/shared/catalog/utils/InflatedPricingRule;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 439
    new-instance v0, Lcom/squareup/widgets/warning/WarningIds;

    sget v1, Lcom/squareup/edititem/R$string;->discount_variable_auto_apply_warning_title:I

    sget v2, Lcom/squareup/edititem/R$string;->discount_variable_auto_apply_warning_text:I

    invoke-direct {v0, v1, v2}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    .line 441
    iget-object v1, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->flow:Lflow/Flow;

    new-instance v2, Lcom/squareup/register/widgets/WarningDialogScreen;

    invoke-direct {v2, v0}, Lcom/squareup/register/widgets/WarningDialogScreen;-><init>(Lcom/squareup/widgets/warning/Warning;)V

    invoke-virtual {v1, v2}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void

    .line 445
    :cond_5
    iget-object v1, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->screenModel:Lcom/squareup/ui/items/EditDiscountScreenModel;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditDiscountScreenModel;->getDiscount()Lcom/squareup/shared/catalog/models/CatalogDiscount;

    move-result-object v1

    .line 446
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->isNewObject()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 447
    sget-object v2, Lcom/squareup/catalog/event/CatalogFeature;->DISCOUNT_CREATED:Lcom/squareup/catalog/event/CatalogFeature;

    iget-object v3, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v3, v5}, Lcom/squareup/catalog/event/CatalogFeature;->log(Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V

    goto :goto_1

    .line 449
    :cond_6
    sget-object v2, Lcom/squareup/catalog/event/CatalogFeature;->DISCOUNT_EDITED:Lcom/squareup/catalog/event/CatalogFeature;

    iget-object v3, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v3, v5}, Lcom/squareup/catalog/event/CatalogFeature;->log(Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V

    .line 452
    :goto_1
    sget-object v2, Lsquareup/items/merchant/CatalogObjectType;->DISCOUNT:Lsquareup/items/merchant/CatalogObjectType;

    invoke-virtual {v2}, Lsquareup/items/merchant/CatalogObjectType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2, v4}, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->logEditCatalogObjectEvent(Ljava/lang/String;Z)V

    .line 454
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->isNewObject()Z

    move-result v2

    if-nez v2, :cond_7

    invoke-virtual {v1, v0}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 455
    :cond_7
    iget-object v2, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->cogs:Lcom/squareup/cogs/Cogs;

    invoke-static {}, Lcom/squareup/cogs/CogsTasks;->write()Lcom/squareup/cogs/WriteBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/squareup/cogs/WriteBuilder;->update(Lcom/squareup/shared/catalog/models/CatalogObject;)Lcom/squareup/cogs/WriteBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->cogs:Lcom/squareup/cogs/Cogs;

    invoke-static {v3}, Lcom/squareup/shared/catalog/CatalogTasks;->syncWhenFinished(Lcom/squareup/shared/catalog/Catalog;)Lcom/squareup/shared/catalog/CatalogCallback;

    move-result-object v3

    invoke-interface {v2, v0, v3}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    .line 458
    :cond_8
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->isNewObject()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 459
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->favoritesTileItemSelectionEvents:Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;

    sget-object v2, Lcom/squareup/shared/catalog/models/CatalogObjectType;->DISCOUNT:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 460
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->getId()Ljava/lang/String;

    move-result-object v1

    .line 459
    invoke-virtual {v0, v2, v1}, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;->createCatalogItem(Lcom/squareup/shared/catalog/models/CatalogObjectType;Ljava/lang/String;)V

    .line 465
    :cond_9
    invoke-direct {p0}, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->finish()V

    return-void
.end method

.method showConfirmDiscardDialogOrFinish()V
    .locals 2

    .line 481
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->screenModel:Lcom/squareup/ui/items/EditDiscountScreenModel;

    if-nez v0, :cond_0

    .line 482
    invoke-direct {p0}, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->finish()V

    return-void

    .line 486
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->createCatalogDiscountFromView()Lcom/squareup/shared/catalog/models/CatalogDiscount;

    move-result-object v0

    .line 487
    iget-object v1, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->screenModel:Lcom/squareup/ui/items/EditDiscountScreenModel;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditDiscountScreenModel;->getDiscount()Lcom/squareup/shared/catalog/models/CatalogDiscount;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 488
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/items/ConfirmDiscardDiscountChangesDialogScreen;

    invoke-direct {v1}, Lcom/squareup/ui/items/ConfirmDiscardDiscountChangesDialogScreen;-><init>()V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 490
    :cond_1
    invoke-direct {p0}, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->finish()V

    :goto_0
    return-void
.end method
