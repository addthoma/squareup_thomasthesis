.class public Lcom/squareup/ui/items/EditItemCategorySelectionScreen;
.super Lcom/squareup/ui/items/InEditItemScope;
.source "EditItemCategorySelectionScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/FullSheet;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Component;,
        Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/items/EditItemCategorySelectionScreen;",
            ">;"
        }
    .end annotation
.end field

.field static final SELECTED_NONE_INDEX:I = -0x1


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 189
    sget-object v0, Lcom/squareup/ui/items/-$$Lambda$EditItemCategorySelectionScreen$Mi8TDjDA3kXdupzWZcvGELG-bOo;->INSTANCE:Lcom/squareup/ui/items/-$$Lambda$EditItemCategorySelectionScreen$Mi8TDjDA3kXdupzWZcvGELG-bOo;

    .line 190
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/items/EditItemCategorySelectionScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Lcom/squareup/ui/items/EditItemScope;)V
    .locals 0

    .line 37
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/InEditItemScope;-><init>(Lcom/squareup/ui/items/EditItemScope;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/items/EditItemCategorySelectionScreen;
    .locals 1

    .line 191
    const-class v0, Lcom/squareup/ui/items/EditItemScope;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/items/EditItemScope;

    .line 192
    new-instance v0, Lcom/squareup/ui/items/EditItemCategorySelectionScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/EditItemCategorySelectionScreen;-><init>(Lcom/squareup/ui/items/EditItemScope;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 186
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemCategorySelectionScreen;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method getSecondaryButton()Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method getUpButtonText(Lcom/squareup/util/Res;)Ljava/lang/CharSequence;
    .locals 1

    .line 45
    sget v0, Lcom/squareup/edititem/R$string;->edit_item_select_category_title:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 196
    sget v0, Lcom/squareup/edititem/R$layout;->edit_item_category_selection_view:I

    return v0
.end method
