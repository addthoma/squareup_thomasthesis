.class public Lcom/squareup/ui/items/ConfirmDiscardCategoryChangesDialogScreen$Factory;
.super Ljava/lang/Object;
.source "ConfirmDiscardCategoryChangesDialogScreen.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/ConfirmDiscardCategoryChangesDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 22
    const-class v0, Lcom/squareup/ui/items/ItemsAppletScope$Component;

    .line 23
    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->componentInParent(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/ItemsAppletScope$Component;

    .line 24
    invoke-interface {v0}, Lcom/squareup/ui/items/ItemsAppletScope$Component;->scopeRunner()Lcom/squareup/ui/items/ItemsAppletScopeRunner;

    move-result-object v0

    .line 25
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$5wXrK6cT-lu608Qvz8r_Wqfa8dU;

    invoke-direct {v1, v0}, Lcom/squareup/ui/items/-$$Lambda$5wXrK6cT-lu608Qvz8r_Wqfa8dU;-><init>(Lcom/squareup/ui/items/ItemsAppletScopeRunner;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/items/-$$Lambda$xK49smD_xMNU_yKIRde8SHeQL-0;

    invoke-direct {v2, v0}, Lcom/squareup/ui/items/-$$Lambda$xK49smD_xMNU_yKIRde8SHeQL-0;-><init>(Lcom/squareup/ui/items/ItemsAppletScopeRunner;)V

    invoke-static {p1, v1, v2}, Lcom/squareup/ui/ConfirmDiscardChangesAlertDialogFactory;->createConfirmDiscardChangesAlertDialog(Landroid/content/Context;Ljava/lang/Runnable;Ljava/lang/Runnable;)Landroid/app/AlertDialog;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
