.class public Lcom/squareup/ui/items/EditCategoryListRow;
.super Landroid/widget/LinearLayout;
.source "EditCategoryListRow.java"


# instance fields
.field private deleteButton:Landroid/view/View;

.field private nameView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 24
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 45
    sget v0, Lcom/squareup/itemsapplet/R$id;->category_row_delete:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/items/EditCategoryListRow;->deleteButton:Landroid/view/View;

    .line 46
    sget v0, Lcom/squareup/itemsapplet/R$id;->category_row_name:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/items/EditCategoryListRow;->nameView:Landroid/widget/TextView;

    return-void
.end method

.method public static inflate(Landroid/view/ViewGroup;)Lcom/squareup/ui/items/EditCategoryListRow;
    .locals 1

    .line 17
    sget v0, Lcom/squareup/itemsapplet/R$layout;->category_edit_list_row:I

    invoke-static {v0, p0}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/items/EditCategoryListRow;

    return-object p0
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 0

    .line 28
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 29
    invoke-direct {p0}, Lcom/squareup/ui/items/EditCategoryListRow;->bindViews()V

    return-void
.end method

.method public setCategoryName(Ljava/lang/String;)V
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryListRow;->nameView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setDeleteClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryListRow;->deleteButton:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setNameClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryListRow;->nameView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
