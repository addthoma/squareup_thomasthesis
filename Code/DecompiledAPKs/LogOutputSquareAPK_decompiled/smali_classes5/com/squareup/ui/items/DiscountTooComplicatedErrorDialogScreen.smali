.class public Lcom/squareup/ui/items/DiscountTooComplicatedErrorDialogScreen;
.super Lcom/squareup/ui/items/InItemsAppletScope;
.source "DiscountTooComplicatedErrorDialogScreen.java"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/items/DiscountTooComplicatedErrorDialogScreen$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/DiscountTooComplicatedErrorDialogScreen$Factory;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/items/DiscountTooComplicatedErrorDialogScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 31
    sget-object v0, Lcom/squareup/ui/items/-$$Lambda$DiscountTooComplicatedErrorDialogScreen$rU4vyu-Vy-uTsixTN0tLdQeb7XY;->INSTANCE:Lcom/squareup/ui/items/-$$Lambda$DiscountTooComplicatedErrorDialogScreen$rU4vyu-Vy-uTsixTN0tLdQeb7XY;

    .line 32
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/items/DiscountTooComplicatedErrorDialogScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Lcom/squareup/ui/items/InItemsAppletScope;-><init>()V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/items/DiscountTooComplicatedErrorDialogScreen;
    .locals 0

    .line 32
    new-instance p0, Lcom/squareup/ui/items/DiscountTooComplicatedErrorDialogScreen;

    invoke-direct {p0}, Lcom/squareup/ui/items/DiscountTooComplicatedErrorDialogScreen;-><init>()V

    return-object p0
.end method
