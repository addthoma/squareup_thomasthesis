.class public Lcom/squareup/ui/items/EditDiscountScreenModel;
.super Ljava/lang/Object;
.source "EditDiscountScreenModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;
    }
.end annotation


# instance fields
.field private discount:Lcom/squareup/shared/catalog/models/CatalogDiscount;

.field private localizer:Lcom/squareup/shared/i18n/Localizer;

.field private moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private pricingRule:Lcom/squareup/shared/catalog/utils/InflatedPricingRule;

.field private pricingRuleDiscountedProducts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/items/EditDiscountHydratedProduct;",
            ">;"
        }
    .end annotation
.end field

.field private pricingRuleProducts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/items/EditDiscountHydratedProduct;",
            ">;"
        }
    .end annotation
.end field

.field private res:Lcom/squareup/util/Res;

.field private shortDateFormat:Ljava/text/DateFormat;

.field private timeFormat:Ljava/text/DateFormat;


# direct methods
.method private constructor <init>(Lcom/squareup/shared/catalog/models/CatalogDiscount;Lcom/squareup/shared/catalog/utils/InflatedPricingRule;Ljava/util/List;Ljava/util/List;Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Ljava/text/DateFormat;Lcom/squareup/shared/i18n/Localizer;Lcom/squareup/util/Res;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/models/CatalogDiscount;",
            "Lcom/squareup/shared/catalog/utils/InflatedPricingRule;",
            "Ljava/util/List<",
            "Lcom/squareup/ui/items/EditDiscountHydratedProduct;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/ui/items/EditDiscountHydratedProduct;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Ljava/text/DateFormat;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/shared/i18n/Localizer;",
            "Lcom/squareup/util/Res;",
            ")V"
        }
    .end annotation

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Lcom/squareup/ui/items/EditDiscountScreenModel;->discount:Lcom/squareup/shared/catalog/models/CatalogDiscount;

    .line 50
    iput-object p2, p0, Lcom/squareup/ui/items/EditDiscountScreenModel;->pricingRule:Lcom/squareup/shared/catalog/utils/InflatedPricingRule;

    .line 51
    iput-object p3, p0, Lcom/squareup/ui/items/EditDiscountScreenModel;->pricingRuleProducts:Ljava/util/List;

    .line 52
    iput-object p4, p0, Lcom/squareup/ui/items/EditDiscountScreenModel;->pricingRuleDiscountedProducts:Ljava/util/List;

    .line 53
    iput-object p5, p0, Lcom/squareup/ui/items/EditDiscountScreenModel;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 54
    iput-object p6, p0, Lcom/squareup/ui/items/EditDiscountScreenModel;->shortDateFormat:Ljava/text/DateFormat;

    .line 55
    iput-object p7, p0, Lcom/squareup/ui/items/EditDiscountScreenModel;->timeFormat:Ljava/text/DateFormat;

    .line 56
    iput-object p8, p0, Lcom/squareup/ui/items/EditDiscountScreenModel;->localizer:Lcom/squareup/shared/i18n/Localizer;

    .line 57
    iput-object p9, p0, Lcom/squareup/ui/items/EditDiscountScreenModel;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/shared/catalog/models/CatalogDiscount;Lcom/squareup/shared/catalog/utils/InflatedPricingRule;Ljava/util/List;Ljava/util/List;Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Ljava/text/DateFormat;Lcom/squareup/shared/i18n/Localizer;Lcom/squareup/util/Res;Lcom/squareup/ui/items/EditDiscountScreenModel$1;)V
    .locals 0

    .line 28
    invoke-direct/range {p0 .. p9}, Lcom/squareup/ui/items/EditDiscountScreenModel;-><init>(Lcom/squareup/shared/catalog/models/CatalogDiscount;Lcom/squareup/shared/catalog/utils/InflatedPricingRule;Ljava/util/List;Ljava/util/List;Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Ljava/text/DateFormat;Lcom/squareup/shared/i18n/Localizer;Lcom/squareup/util/Res;)V

    return-void
.end method

.method private getProductRuleRows(Ljava/util/List;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/items/EditDiscountHydratedProduct;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/ui/items/RuleRowItem;",
            ">;"
        }
    .end annotation

    .line 77
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-nez p1, :cond_0

    return-object v0

    .line 83
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/items/EditDiscountHydratedProduct;

    .line 85
    instance-of v2, v1, Lcom/squareup/ui/items/CategoryWithItemCount;

    if-eqz v2, :cond_1

    .line 86
    check-cast v1, Lcom/squareup/ui/items/CategoryWithItemCount;

    invoke-direct {p0, v1}, Lcom/squareup/ui/items/EditDiscountScreenModel;->parseCategoryWithItemCount(Lcom/squareup/ui/items/CategoryWithItemCount;)Lcom/squareup/ui/items/RuleRowItem;

    move-result-object v1

    goto :goto_1

    .line 87
    :cond_1
    instance-of v2, v1, Lcom/squareup/ui/items/ItemWithVariation;

    if-eqz v2, :cond_2

    .line 88
    check-cast v1, Lcom/squareup/ui/items/ItemWithVariation;

    invoke-direct {p0, v1}, Lcom/squareup/ui/items/EditDiscountScreenModel;->parseItemWithVariation(Lcom/squareup/ui/items/ItemWithVariation;)Lcom/squareup/ui/items/RuleRowItem;

    move-result-object v1

    goto :goto_1

    .line 89
    :cond_2
    instance-of v2, v1, Lcom/squareup/ui/items/VariationWithParentItem;

    if-eqz v2, :cond_3

    .line 90
    check-cast v1, Lcom/squareup/ui/items/VariationWithParentItem;

    invoke-direct {p0, v1}, Lcom/squareup/ui/items/EditDiscountScreenModel;->parseVariationWithParentItem(Lcom/squareup/ui/items/VariationWithParentItem;)Lcom/squareup/ui/items/RuleRowItem;

    move-result-object v1

    .line 95
    :goto_1
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 92
    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected EditDiscountHydratedProduct of type "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 93
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_4
    return-object v0
.end method

.method static synthetic lambda$getScheduleRuleRows$0(Ljava/util/TimeZone;Lcom/squareup/shared/catalog/models/CatalogTimePeriod;Lcom/squareup/shared/catalog/models/CatalogTimePeriod;)I
    .locals 0

    .line 108
    invoke-virtual {p1, p0}, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->getSchedule(Ljava/util/TimeZone;)Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;

    move-result-object p1

    invoke-virtual {p2, p0}, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->getSchedule(Ljava/util/TimeZone;)Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;

    move-result-object p0

    invoke-virtual {p1, p0}, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;->compareTo(Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;)I

    move-result p0

    return p0
.end method

.method private parseCategoryWithItemCount(Lcom/squareup/ui/items/CategoryWithItemCount;)Lcom/squareup/ui/items/RuleRowItem;
    .locals 4

    .line 175
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountScreenModel;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/edititem/R$string;->category_label_with_name:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 176
    invoke-virtual {p1}, Lcom/squareup/ui/items/CategoryWithItemCount;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "category_name"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 177
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 179
    invoke-virtual {p1}, Lcom/squareup/ui/items/CategoryWithItemCount;->getItemCount()I

    move-result v1

    if-eqz v1, :cond_1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    .line 187
    iget-object v1, p0, Lcom/squareup/ui/items/EditDiscountScreenModel;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/edititem/R$string;->edit_discount_item_plural:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 188
    invoke-virtual {p1}, Lcom/squareup/ui/items/CategoryWithItemCount;->getItemCount()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "number"

    invoke-virtual {v1, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 189
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_0

    .line 184
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/items/EditDiscountScreenModel;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/edititem/R$string;->edit_discount_item_single:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 181
    :cond_1
    iget-object v1, p0, Lcom/squareup/ui/items/EditDiscountScreenModel;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/edititem/R$string;->edit_discount_item_zero:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 191
    :goto_0
    new-instance v2, Lcom/squareup/ui/items/RuleRowItem;

    invoke-virtual {p1}, Lcom/squareup/ui/items/CategoryWithItemCount;->getColor()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/items/EditDiscountScreenModel;->parseColor(Ljava/lang/String;)I

    move-result p1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, p1, v0, v1}, Lcom/squareup/ui/items/RuleRowItem;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method

.method private parseColor(Ljava/lang/String;)I
    .locals 2

    .line 195
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountScreenModel;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/widgets/pos/R$color;->edit_item_gray:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getColor(I)I

    move-result v0

    invoke-static {p1, v0}, Lcom/squareup/util/Colors;->parseHex(Ljava/lang/String;I)I

    move-result p1

    return p1
.end method

.method private parseItemWithVariation(Lcom/squareup/ui/items/ItemWithVariation;)Lcom/squareup/ui/items/RuleRowItem;
    .locals 4

    .line 160
    iget-boolean v0, p1, Lcom/squareup/ui/items/ItemWithVariation;->isVariablePricing:Z

    if-eqz v0, :cond_0

    .line 161
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountScreenModel;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/edititem/R$string;->edit_item_price_variable:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 163
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/ui/items/ItemWithVariation;->getSingleVariationPrice()Lcom/squareup/protos/common/Money;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 164
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountScreenModel;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p1}, Lcom/squareup/ui/items/ItemWithVariation;->getSingleVariationPrice()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 166
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/ui/items/ItemWithVariation;->getMultiVariationPriceRange()Landroidx/core/util/Pair;

    move-result-object v0

    .line 167
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/squareup/ui/items/EditDiscountScreenModel;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v3, v0, Landroidx/core/util/Pair;->first:Ljava/lang/Object;

    invoke-interface {v2, v3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/ui/items/EditDiscountScreenModel;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v0, v0, Landroidx/core/util/Pair;->second:Ljava/lang/Object;

    .line 169
    invoke-interface {v2, v0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 171
    :goto_0
    new-instance v1, Lcom/squareup/ui/items/RuleRowItem;

    invoke-virtual {p1}, Lcom/squareup/ui/items/ItemWithVariation;->getColor()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/squareup/ui/items/EditDiscountScreenModel;->parseColor(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p1}, Lcom/squareup/ui/items/ItemWithVariation;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, p1, v0}, Lcom/squareup/ui/items/RuleRowItem;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method

.method private parseVariationWithParentItem(Lcom/squareup/ui/items/VariationWithParentItem;)Lcom/squareup/ui/items/RuleRowItem;
    .locals 4

    .line 154
    new-instance v0, Lcom/squareup/ui/items/RuleRowItem;

    invoke-virtual {p1}, Lcom/squareup/ui/items/VariationWithParentItem;->getParentItemColor()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/squareup/ui/items/EditDiscountScreenModel;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p1}, Lcom/squareup/ui/items/VariationWithParentItem;->getName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/items/EditDiscountScreenModel;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 155
    invoke-virtual {p1}, Lcom/squareup/ui/items/VariationWithParentItem;->getPrice()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-interface {v3, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, v2, p1}, Lcom/squareup/ui/items/RuleRowItem;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public getDateRangeRuleRows()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/ui/items/RuleRowItem;",
            ">;"
        }
    .end annotation

    .line 126
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 127
    iget-object v1, p0, Lcom/squareup/ui/items/EditDiscountScreenModel;->pricingRule:Lcom/squareup/shared/catalog/utils/InflatedPricingRule;

    if-eqz v1, :cond_1

    .line 128
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->startsAt()Ljava/util/Date;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/squareup/ui/items/EditDiscountScreenModel;->pricingRule:Lcom/squareup/shared/catalog/utils/InflatedPricingRule;

    .line 129
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->endsAt()Ljava/util/Date;

    move-result-object v1

    if-nez v1, :cond_0

    goto :goto_0

    .line 132
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/squareup/ui/items/EditDiscountScreenModel;->shortDateFormat:Ljava/text/DateFormat;

    iget-object v3, p0, Lcom/squareup/ui/items/EditDiscountScreenModel;->pricingRule:Lcom/squareup/shared/catalog/utils/InflatedPricingRule;

    .line 133
    invoke-virtual {v3}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->startsAt()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/squareup/ui/items/EditDiscountScreenModel;->timeFormat:Ljava/text/DateFormat;

    iget-object v4, p0, Lcom/squareup/ui/items/EditDiscountScreenModel;->pricingRule:Lcom/squareup/shared/catalog/utils/InflatedPricingRule;

    .line 134
    invoke-virtual {v4}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->startsAt()Ljava/util/Date;

    move-result-object v4

    .line 133
    invoke-virtual {v3, v4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 135
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/squareup/ui/items/EditDiscountScreenModel;->shortDateFormat:Ljava/text/DateFormat;

    iget-object v5, p0, Lcom/squareup/ui/items/EditDiscountScreenModel;->pricingRule:Lcom/squareup/shared/catalog/utils/InflatedPricingRule;

    .line 136
    invoke-virtual {v5}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->endsAt()Ljava/util/Date;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/ui/items/EditDiscountScreenModel;->timeFormat:Ljava/text/DateFormat;

    iget-object v4, p0, Lcom/squareup/ui/items/EditDiscountScreenModel;->pricingRule:Lcom/squareup/shared/catalog/utils/InflatedPricingRule;

    .line 137
    invoke-virtual {v4}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->endsAt()Ljava/util/Date;

    move-result-object v4

    .line 136
    invoke-virtual {v2, v4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 138
    new-instance v3, Lcom/squareup/ui/items/RuleRowItem;

    iget-object v4, p0, Lcom/squareup/ui/items/EditDiscountScreenModel;->res:Lcom/squareup/util/Res;

    sget v5, Lcom/squareup/edititem/R$string;->discount_rule_type_date_range_starts:I

    .line 139
    invoke-interface {v4, v5}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v1}, Lcom/squareup/ui/items/RuleRowItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 140
    new-instance v1, Lcom/squareup/ui/items/RuleRowItem;

    iget-object v3, p0, Lcom/squareup/ui/items/EditDiscountScreenModel;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/edititem/R$string;->discount_rule_type_date_range_ends:I

    .line 141
    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3, v2}, Lcom/squareup/ui/items/RuleRowItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_0
    return-object v0
.end method

.method public getDiscount()Lcom/squareup/shared/catalog/models/CatalogDiscount;
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountScreenModel;->discount:Lcom/squareup/shared/catalog/models/CatalogDiscount;

    return-object v0
.end method

.method public getDiscountedProductRuleRows()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/ui/items/RuleRowItem;",
            ">;"
        }
    .end annotation

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountScreenModel;->pricingRule:Lcom/squareup/shared/catalog/utils/InflatedPricingRule;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->matchesAllProducts()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/ui/items/RuleRowItem;

    iget-object v1, p0, Lcom/squareup/ui/items/EditDiscountScreenModel;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/edititem/R$string;->edit_discount_all_items:I

    .line 72
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Lcom/squareup/ui/items/RuleRowItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountScreenModel;->pricingRuleDiscountedProducts:Ljava/util/List;

    .line 73
    invoke-direct {p0, v0}, Lcom/squareup/ui/items/EditDiscountScreenModel;->getProductRuleRows(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public getDiscountedProductsTitle()Ljava/lang/String;
    .locals 2

    .line 150
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountScreenModel;->pricingRule:Lcom/squareup/shared/catalog/utils/InflatedPricingRule;

    iget-object v1, p0, Lcom/squareup/ui/items/EditDiscountScreenModel;->localizer:Lcom/squareup/shared/i18n/Localizer;

    invoke-virtual {v0, v1}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->applyDescription(Lcom/squareup/shared/i18n/Localizer;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPricingRule()Lcom/squareup/shared/catalog/utils/InflatedPricingRule;
    .locals 1

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountScreenModel;->pricingRule:Lcom/squareup/shared/catalog/utils/InflatedPricingRule;

    return-object v0
.end method

.method public getProductRuleRows()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/ui/items/RuleRowItem;",
            ">;"
        }
    .end annotation

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountScreenModel;->pricingRuleProducts:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/squareup/ui/items/EditDiscountScreenModel;->getProductRuleRows(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getProductsTitle()Ljava/lang/String;
    .locals 2

    .line 146
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountScreenModel;->pricingRule:Lcom/squareup/shared/catalog/utils/InflatedPricingRule;

    iget-object v1, p0, Lcom/squareup/ui/items/EditDiscountScreenModel;->localizer:Lcom/squareup/shared/i18n/Localizer;

    invoke-virtual {v0, v1}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->matchDescription(Lcom/squareup/shared/i18n/Localizer;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getScheduleRuleRows()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/ui/items/RuleRowItem;",
            ">;"
        }
    .end annotation

    .line 101
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 102
    iget-object v1, p0, Lcom/squareup/ui/items/EditDiscountScreenModel;->pricingRule:Lcom/squareup/shared/catalog/utils/InflatedPricingRule;

    if-nez v1, :cond_0

    return-object v0

    .line 105
    :cond_0
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v1

    .line 106
    iget-object v2, p0, Lcom/squareup/ui/items/EditDiscountScreenModel;->pricingRule:Lcom/squareup/shared/catalog/utils/InflatedPricingRule;

    .line 107
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/utils/InflatedPricingRule;->getPeriods()Ljava/util/Collection;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    new-instance v3, Lcom/squareup/ui/items/-$$Lambda$EditDiscountScreenModel$rdYFnRvczjutQosTy3HmLtuvDpI;

    invoke-direct {v3, v1}, Lcom/squareup/ui/items/-$$Lambda$EditDiscountScreenModel$rdYFnRvczjutQosTy3HmLtuvDpI;-><init>(Ljava/util/TimeZone;)V

    invoke-static {v2, v3}, Lcom/squareup/util/SquareCollections;->sortedCopy(Ljava/util/List;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object v1

    .line 111
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const-string v2, ""

    move-object v3, v2

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;

    .line 113
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->getSchedule(Ljava/util/TimeZone;)Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;

    move-result-object v5

    iget-object v6, p0, Lcom/squareup/ui/items/EditDiscountScreenModel;->localizer:Lcom/squareup/shared/i18n/Localizer;

    invoke-virtual {v5, v6}, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;->daysOfTheWeek(Lcom/squareup/shared/i18n/Localizer;)Ljava/lang/String;

    move-result-object v5

    .line 115
    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    move-object v3, v2

    goto :goto_1

    :cond_1
    move-object v3, v5

    .line 117
    :goto_1
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->getSchedule(Ljava/util/TimeZone;)Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;

    move-result-object v4

    iget-object v6, p0, Lcom/squareup/ui/items/EditDiscountScreenModel;->localizer:Lcom/squareup/shared/i18n/Localizer;

    invoke-virtual {v4, v6}, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;->timeRange(Lcom/squareup/shared/i18n/Localizer;)Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v6, "to"

    const-string v7, "-"

    .line 118
    invoke-virtual {v4, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    .line 119
    new-instance v6, Lcom/squareup/ui/items/RuleRowItem;

    invoke-direct {v6, v3, v4}, Lcom/squareup/ui/items/RuleRowItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v3, v5

    goto :goto_0

    :cond_2
    return-object v0
.end method
