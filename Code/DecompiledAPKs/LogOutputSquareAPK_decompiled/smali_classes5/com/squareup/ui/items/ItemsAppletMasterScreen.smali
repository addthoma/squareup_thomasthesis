.class public final Lcom/squareup/ui/items/ItemsAppletMasterScreen;
.super Lcom/squareup/ui/items/InItemsAppletScope;
.source "ItemsAppletMasterScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/spot/HasSpot;


# annotations
.annotation runtime Lcom/squareup/container/layer/Master;
    applet = Lcom/squareup/ui/items/ItemsApplet;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/items/ItemsAppletMasterScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/ItemsAppletMasterScreen$Component;,
        Lcom/squareup/ui/items/ItemsAppletMasterScreen$ItemsAppletSectionsListPresenter;,
        Lcom/squareup/ui/items/ItemsAppletMasterScreen$Module;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/items/ItemsAppletMasterScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/items/ItemsAppletMasterScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 37
    new-instance v0, Lcom/squareup/ui/items/ItemsAppletMasterScreen;

    invoke-direct {v0}, Lcom/squareup/ui/items/ItemsAppletMasterScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/items/ItemsAppletMasterScreen;->INSTANCE:Lcom/squareup/ui/items/ItemsAppletMasterScreen;

    .line 111
    sget-object v0, Lcom/squareup/ui/items/ItemsAppletMasterScreen;->INSTANCE:Lcom/squareup/ui/items/ItemsAppletMasterScreen;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/items/ItemsAppletMasterScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 39
    invoke-direct {p0}, Lcom/squareup/ui/items/InItemsAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 43
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->ITEMS_APPLET_MASTER_SCREEN:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 51
    sget-object p1, Lcom/squareup/container/spot/Spots;->HERE:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 47
    sget v0, Lcom/squareup/pos/container/R$layout;->applet_master_view:I

    return v0
.end method
