.class public final Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow_MembersInjector;
.super Ljava/lang/Object;
.source "EditItemMainViewSingleVariationStaticRow_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;",
        ">;"
    }
.end annotation


# instance fields
.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final perUnitFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemMainPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final priceLocaleHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/PriceLocaleHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemMainPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/PriceLocaleHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)V"
        }
    .end annotation

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p2, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow_MembersInjector;->priceLocaleHelperProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p3, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow_MembersInjector;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p4, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow_MembersInjector;->perUnitFormatterProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p5, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow_MembersInjector;->resProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemMainPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/PriceLocaleHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;",
            ">;"
        }
    .end annotation

    .line 49
    new-instance v6, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow_MembersInjector;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static injectMoneyFormatter(Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;Lcom/squareup/text/Formatter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    .line 75
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->moneyFormatter:Lcom/squareup/text/Formatter;

    return-void
.end method

.method public static injectPerUnitFormatter(Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;Lcom/squareup/quantity/PerUnitFormatter;)V
    .locals 0

    .line 81
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;Ljava/lang/Object;)V
    .locals 0

    .line 63
    check-cast p1, Lcom/squareup/ui/items/EditItemMainPresenter;

    iput-object p1, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    return-void
.end method

.method public static injectPriceLocaleHelper(Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;Lcom/squareup/money/PriceLocaleHelper;)V
    .locals 0

    .line 69
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    return-void
.end method

.method public static injectRes(Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;Lcom/squareup/util/Res;)V
    .locals 0

    .line 86
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->res:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;)V
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow_MembersInjector;->injectPresenter(Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;Ljava/lang/Object;)V

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow_MembersInjector;->priceLocaleHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/money/PriceLocaleHelper;

    invoke-static {p1, v0}, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow_MembersInjector;->injectPriceLocaleHelper(Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;Lcom/squareup/money/PriceLocaleHelper;)V

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow_MembersInjector;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/text/Formatter;

    invoke-static {p1, v0}, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow_MembersInjector;->injectMoneyFormatter(Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;Lcom/squareup/text/Formatter;)V

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow_MembersInjector;->perUnitFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/quantity/PerUnitFormatter;

    invoke-static {p1, v0}, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow_MembersInjector;->injectPerUnitFormatter(Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;Lcom/squareup/quantity/PerUnitFormatter;)V

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow_MembersInjector;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    invoke-static {p1, v0}, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow_MembersInjector;->injectRes(Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;Lcom/squareup/util/Res;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 13
    check-cast p1, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow_MembersInjector;->injectMembers(Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;)V

    return-void
.end method
