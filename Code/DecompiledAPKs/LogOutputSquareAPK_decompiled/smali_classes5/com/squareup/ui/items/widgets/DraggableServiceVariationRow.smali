.class public Lcom/squareup/ui/items/widgets/DraggableServiceVariationRow;
.super Lcom/squareup/ui/items/widgets/DraggableVariationRow;
.source "DraggableServiceVariationRow.java"


# instance fields
.field private durationRow:Lcom/squareup/marketfont/MarketTextView;

.field private priceRow:Lcom/squareup/marketfont/MarketTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 13
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/items/widgets/DraggableVariationRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    .line 17
    invoke-super {p0}, Lcom/squareup/ui/items/widgets/DraggableVariationRow;->onFinishInflate()V

    .line 19
    sget v0, Lcom/squareup/edititem/R$id;->draggable_service_variation_row_duration:I

    invoke-virtual {p0, v0}, Lcom/squareup/ui/items/widgets/DraggableServiceVariationRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/items/widgets/DraggableServiceVariationRow;->durationRow:Lcom/squareup/marketfont/MarketTextView;

    .line 20
    sget v0, Lcom/squareup/edititem/R$id;->draggable_service_variation_row_price:I

    invoke-virtual {p0, v0}, Lcom/squareup/ui/items/widgets/DraggableServiceVariationRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/items/widgets/DraggableServiceVariationRow;->priceRow:Lcom/squareup/marketfont/MarketTextView;

    return-void
.end method

.method public setDuration(Ljava/lang/String;)V
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/ui/items/widgets/DraggableServiceVariationRow;->durationRow:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setPrice(Ljava/lang/String;)V
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/ui/items/widgets/DraggableServiceVariationRow;->priceRow:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
