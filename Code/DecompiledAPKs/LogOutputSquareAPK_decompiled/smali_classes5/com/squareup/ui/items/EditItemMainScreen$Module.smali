.class public Lcom/squareup/ui/items/EditItemMainScreen$Module;
.super Ljava/lang/Object;
.source "EditItemMainScreen.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditItemMainScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Module"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/items/EditItemMainScreen;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/items/EditItemMainScreen;)V
    .locals 0

    .line 79
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemMainScreen$Module;->this$0:Lcom/squareup/ui/items/EditItemMainScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method provideAppliedLocationsBannerPresenter(Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;Lcom/squareup/util/Res;Lcom/squareup/ui/items/ItemEditingStringIds;Lcom/squareup/ui/items/EditItemMainPresenter;Lcom/squareup/catalogapi/CatalogIntegrationController;)Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;
    .locals 12
    .annotation runtime Ldagger/Provides;
    .end annotation

    move-object v0, p0

    .line 90
    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainScreen$Module;->this$0:Lcom/squareup/ui/items/EditItemMainScreen;

    iget-object v1, v1, Lcom/squareup/ui/items/EditItemMainScreen;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    iget-object v1, v1, Lcom/squareup/ui/items/EditItemScope;->type:Lcom/squareup/api/items/Item$Type;

    sget-object v2, Lcom/squareup/api/items/Item$Type;->REGULAR:Lcom/squareup/api/items/Item$Type;

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-ne v1, v2, :cond_0

    const/4 v10, 0x1

    goto :goto_0

    :cond_0
    const/4 v10, 0x0

    .line 91
    :goto_0
    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainScreen$Module;->this$0:Lcom/squareup/ui/items/EditItemMainScreen;

    iget-object v1, v1, Lcom/squareup/ui/items/EditItemMainScreen;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    iget-object v1, v1, Lcom/squareup/ui/items/EditItemScope;->type:Lcom/squareup/api/items/Item$Type;

    sget-object v2, Lcom/squareup/api/items/Item$Type;->APPOINTMENTS_SERVICE:Lcom/squareup/api/items/Item$Type;

    if-ne v1, v2, :cond_1

    .line 93
    invoke-interface/range {p5 .. p5}, Lcom/squareup/catalogapi/CatalogIntegrationController;->shouldShowNewFeature()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v11, 0x1

    goto :goto_1

    :cond_1
    const/4 v11, 0x0

    .line 94
    :goto_1
    new-instance v1, Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;

    .line 95
    invoke-virtual/range {p4 .. p4}, Lcom/squareup/ui/items/EditItemMainPresenter;->isNewObject()Z

    move-result v9

    move-object v5, v1

    move-object v6, p1

    move-object v7, p2

    move-object v8, p3

    invoke-direct/range {v5 .. v11}, Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;-><init>(Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;Lcom/squareup/util/Res;Lcom/squareup/ui/items/ItemEditingStringIds;ZZZ)V

    return-object v1
.end method

.method provideAssignUnitLayoutConfiguration()Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitLayoutConfiguration;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 100
    sget-object v0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitLayoutConfiguration;->FULL_SHEET:Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitLayoutConfiguration;

    return-object v0
.end method

.method provideItemEditingStringIds()Lcom/squareup/ui/items/ItemEditingStringIds;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainScreen$Module;->this$0:Lcom/squareup/ui/items/EditItemMainScreen;

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemMainScreen;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemScope;->type:Lcom/squareup/api/items/Item$Type;

    invoke-static {v0}, Lcom/squareup/ui/items/ItemEditingStringIds;->getStringsForCatalogItemType(Lcom/squareup/api/items/Item$Type;)Lcom/squareup/ui/items/ItemEditingStringIds;

    move-result-object v0

    return-object v0
.end method
