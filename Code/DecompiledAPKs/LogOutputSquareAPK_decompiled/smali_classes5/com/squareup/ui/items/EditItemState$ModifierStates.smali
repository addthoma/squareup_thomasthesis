.class final Lcom/squareup/ui/items/EditItemState$ModifierStates;
.super Ljava/lang/Object;
.source "EditItemState.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditItemState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "ModifierStates"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/items/EditItemState$ModifierStates;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private dirty:Z

.field itemId:Ljava/lang/String;

.field private final table:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Lcom/squareup/ui/items/EditItemState$ModifierState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1339
    new-instance v0, Lcom/squareup/ui/items/EditItemState$ModifierStates$1;

    invoke-direct {v0}, Lcom/squareup/ui/items/EditItemState$ModifierStates$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/items/EditItemState$ModifierStates;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .line 1250
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 1248
    iput-boolean v0, p0, Lcom/squareup/ui/items/EditItemState$ModifierStates;->dirty:Z

    .line 1251
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemState$ModifierStates;->table:Ljava/util/LinkedHashMap;

    return-void
.end method

.method private constructor <init>(Ljava/util/LinkedHashMap;Ljava/lang/String;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Lcom/squareup/ui/items/EditItemState$ModifierState;",
            ">;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .line 1256
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 1248
    iput-boolean v0, p0, Lcom/squareup/ui/items/EditItemState$ModifierStates;->dirty:Z

    .line 1257
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemState$ModifierStates;->table:Ljava/util/LinkedHashMap;

    .line 1258
    iput-object p2, p0, Lcom/squareup/ui/items/EditItemState$ModifierStates;->itemId:Ljava/lang/String;

    .line 1259
    iput-boolean p3, p0, Lcom/squareup/ui/items/EditItemState$ModifierStates;->dirty:Z

    return-void
.end method

.method synthetic constructor <init>(Ljava/util/LinkedHashMap;Ljava/lang/String;ZLcom/squareup/ui/items/EditItemState$1;)V
    .locals 0

    .line 1245
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/items/EditItemState$ModifierStates;-><init>(Ljava/util/LinkedHashMap;Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/ui/items/EditItemState$ModifierStates;)Ljava/util/List;
    .locals 0

    .line 1245
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemState$ModifierStates;->getNewlyDisabledMemberships()Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/ui/items/EditItemState$ModifierStates;)Ljava/util/List;
    .locals 0

    .line 1245
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemState$ModifierStates;->getNewlyEnabledMemberships()Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method private getNewlyDisabledMemberships()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;",
            ">;"
        }
    .end annotation

    .line 1326
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1327
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemState$ModifierStates;->table:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/items/EditItemState$ModifierState;

    .line 1328
    iget-boolean v3, v2, Lcom/squareup/ui/items/EditItemState$ModifierState;->enabled:Z

    if-nez v3, :cond_0

    invoke-static {v2}, Lcom/squareup/ui/items/EditItemState$ModifierState;->access$1300(Lcom/squareup/ui/items/EditItemState$ModifierState;)Z

    move-result v3

    if-nez v3, :cond_1

    goto :goto_0

    .line 1329
    :cond_1
    new-instance v3, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership$Builder;

    iget-object v2, v2, Lcom/squareup/ui/items/EditItemState$ModifierState;->membership:[B

    .line 1331
    invoke-static {v2}, Lcom/squareup/shared/catalog/models/CatalogObjectType$Adapter;->objectFromByteArray([B)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;

    invoke-direct {v3, v2}, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership$Builder;-><init>(Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;)V

    const/4 v2, 0x0

    .line 1332
    invoke-virtual {v3, v2}, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership$Builder;->setEnabled(Z)Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership$Builder;

    move-result-object v2

    .line 1333
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;

    move-result-object v2

    .line 1334
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method private getNewlyEnabledMemberships()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;",
            ">;"
        }
    .end annotation

    .line 1307
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1308
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemState$ModifierStates;->table:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/items/EditItemState$ModifierState;

    .line 1309
    iget-boolean v3, v2, Lcom/squareup/ui/items/EditItemState$ModifierState;->enabled:Z

    if-eqz v3, :cond_0

    invoke-static {v2}, Lcom/squareup/ui/items/EditItemState$ModifierState;->access$1300(Lcom/squareup/ui/items/EditItemState$ModifierState;)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_0

    .line 1311
    :cond_1
    iget-object v3, v2, Lcom/squareup/ui/items/EditItemState$ModifierState;->membership:[B

    if-eqz v3, :cond_2

    .line 1312
    new-instance v3, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership$Builder;

    iget-object v2, v2, Lcom/squareup/ui/items/EditItemState$ModifierState;->membership:[B

    .line 1313
    invoke-static {v2}, Lcom/squareup/shared/catalog/models/CatalogObjectType$Adapter;->objectFromByteArray([B)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;

    invoke-direct {v3, v2}, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership$Builder;-><init>(Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;)V

    const/4 v2, 0x1

    .line 1314
    invoke-virtual {v3, v2}, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership$Builder;->setEnabled(Z)Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership$Builder;

    move-result-object v2

    .line 1315
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;

    move-result-object v2

    goto :goto_1

    .line 1317
    :cond_2
    new-instance v3, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership$Builder;

    iget-object v4, p0, Lcom/squareup/ui/items/EditItemState$ModifierStates;->itemId:Ljava/lang/String;

    iget-object v2, v2, Lcom/squareup/ui/items/EditItemState$ModifierState;->id:Ljava/lang/String;

    invoke-direct {v3, v4, v2}, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership$Builder;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;

    move-result-object v2

    .line 1320
    :goto_1
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method getStates()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/ui/items/EditItemState$ModifierState;",
            ">;"
        }
    .end annotation

    .line 1269
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemState$ModifierStates;->table:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method isDirty()Z
    .locals 1

    .line 1279
    iget-boolean v0, p0, Lcom/squareup/ui/items/EditItemState$ModifierStates;->dirty:Z

    return v0
.end method

.method load(Ljava/util/List;Ljava/util/Map;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/Related<",
            "Lcom/squareup/shared/catalog/models/CatalogItemModifierList;",
            "Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;",
            ">;>;",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/catalog/models/CatalogItemModifierList;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;",
            ">;>;)V"
        }
    .end annotation

    .line 1285
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState$ModifierStates;->table:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    .line 1287
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/Related;

    .line 1288
    iget-object v1, v0, Lcom/squareup/shared/catalog/Related;->object:Lcom/squareup/shared/catalog/models/CatalogObject;

    check-cast v1, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;

    .line 1289
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;->getId()Ljava/lang/String;

    move-result-object v9

    .line 1290
    iget-object v2, v0, Lcom/squareup/shared/catalog/Related;->relation:Lcom/squareup/shared/catalog/models/CatalogObject;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    goto :goto_1

    :cond_0
    iget-object v2, v0, Lcom/squareup/shared/catalog/Related;->relation:Lcom/squareup/shared/catalog/models/CatalogObject;

    check-cast v2, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;

    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;->toByteArray()[B

    move-result-object v2

    :goto_1
    move-object v6, v2

    .line 1292
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 1293
    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    if-eqz v2, :cond_1

    .line 1295
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;

    .line 1296
    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1299
    :cond_1
    iget-boolean v2, v0, Lcom/squareup/shared/catalog/Related;->related:Z

    if-eqz v2, :cond_2

    iget-object v0, v0, Lcom/squareup/shared/catalog/Related;->relation:Lcom/squareup/shared/catalog/models/CatalogObject;

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    const/4 v5, 0x1

    goto :goto_3

    :cond_2
    const/4 v0, 0x0

    const/4 v5, 0x0

    .line 1300
    :goto_3
    new-instance v0, Lcom/squareup/ui/items/EditItemState$ModifierState;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;->getName()Ljava/lang/String;

    move-result-object v4

    const/4 v8, 0x0

    move-object v2, v0

    move-object v3, v9

    invoke-direct/range {v2 .. v8}, Lcom/squareup/ui/items/EditItemState$ModifierState;-><init>(Ljava/lang/String;Ljava/lang/String;Z[BLjava/util/List;Lcom/squareup/ui/items/EditItemState$1;)V

    .line 1302
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemState$ModifierStates;->table:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, v9, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_3
    return-void
.end method

.method reset(Ljava/lang/String;)V
    .locals 1

    .line 1273
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState$ModifierStates;->table:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    .line 1274
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemState$ModifierStates;->itemId:Ljava/lang/String;

    const/4 p1, 0x0

    .line 1275
    iput-boolean p1, p0, Lcom/squareup/ui/items/EditItemState$ModifierStates;->dirty:Z

    return-void
.end method

.method setApplied(Ljava/lang/String;Z)V
    .locals 1

    .line 1263
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState$ModifierStates;->table:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/EditItemState$ModifierState;

    const/4 v0, 0x1

    .line 1264
    iput-boolean v0, p0, Lcom/squareup/ui/items/EditItemState$ModifierStates;->dirty:Z

    .line 1265
    iput-boolean p2, p1, Lcom/squareup/ui/items/EditItemState$ModifierState;->enabled:Z

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 1358
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemState$ModifierStates;->table:Ljava/util/LinkedHashMap;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 1359
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemState$ModifierStates;->itemId:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1360
    iget-boolean p2, p0, Lcom/squareup/ui/items/EditItemState$ModifierStates;->dirty:Z

    int-to-byte p2, p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeByte(B)V

    return-void
.end method
