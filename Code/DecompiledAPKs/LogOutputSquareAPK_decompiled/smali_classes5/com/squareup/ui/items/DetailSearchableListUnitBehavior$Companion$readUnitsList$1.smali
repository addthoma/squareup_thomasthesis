.class final Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion$readUnitsList$1;
.super Ljava/lang/Object;
.source "DetailSearchableListUnitBehavior.kt"

# interfaces
.implements Lcom/squareup/shared/catalog/CatalogTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion;->readUnitsList(Lcom/squareup/cogs/Cogs;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/shared/catalog/CatalogTask<",
        "Ljava/util/List<",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a&\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002 \u0003*\u0012\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00040\u00012\u000e\u0010\u0005\u001a\n \u0003*\u0004\u0018\u00010\u00060\u0006H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;",
        "kotlin.jvm.PlatformType",
        "",
        "local",
        "Lcom/squareup/shared/catalog/Catalog$Local;",
        "perform"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion$readUnitsList$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion$readUnitsList$1;

    invoke-direct {v0}, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion$readUnitsList$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion$readUnitsList$1;->INSTANCE:Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion$readUnitsList$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic perform(Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/lang/Object;
    .locals 0

    .line 51
    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion$readUnitsList$1;->perform(Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public final perform(Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/Catalog$Local;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;",
            ">;"
        }
    .end annotation

    .line 97
    const-class v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    invoke-interface {p1, v0}, Lcom/squareup/shared/catalog/Catalog$Local;->readAllCatalogConnectV2Objects(Ljava/lang/Class;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method
