.class public Lcom/squareup/ui/items/EditItemConfirmGlobalPriceDialogScreen$Factory;
.super Ljava/lang/Object;
.source "EditItemConfirmGlobalPriceDialogScreen.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditItemConfirmGlobalPriceDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$create$0(Lcom/squareup/ui/items/EditItemScopeRunner;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 38
    sget-object p1, Lcom/squareup/ui/items/EditItemScopeRunner$VariationSavePath;->SAVE_VARIATIONS_VIA_ITEM_V3_GLOBALLY:Lcom/squareup/ui/items/EditItemScopeRunner$VariationSavePath;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/EditItemScopeRunner;->saveChanges(Lcom/squareup/ui/items/EditItemScopeRunner$VariationSavePath;)V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 30
    const-class v0, Lcom/squareup/ui/items/EditItemScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/EditItemScope$Component;

    .line 31
    invoke-interface {v0}, Lcom/squareup/ui/items/EditItemScope$Component;->scopeRunner()Lcom/squareup/ui/items/EditItemScopeRunner;

    move-result-object v0

    .line 32
    invoke-static {p1}, Lcom/squareup/container/ContainerTreeKey;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/items/EditItemConfirmGlobalPriceDialogScreen;

    .line 34
    new-instance v2, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v2, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget p1, Lcom/squareup/edititem/R$string;->item_editing_confirm_price_update_dialog_title:I

    .line 35
    invoke-virtual {v2, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 36
    invoke-static {v1}, Lcom/squareup/ui/items/EditItemConfirmGlobalPriceDialogScreen;->access$000(Lcom/squareup/ui/items/EditItemConfirmGlobalPriceDialogScreen;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v1, Lcom/squareup/common/strings/R$string;->confirm:I

    new-instance v2, Lcom/squareup/ui/items/-$$Lambda$EditItemConfirmGlobalPriceDialogScreen$Factory$9wmI5DEtkoOXIAoXkZwQkwng-HU;

    invoke-direct {v2, v0}, Lcom/squareup/ui/items/-$$Lambda$EditItemConfirmGlobalPriceDialogScreen$Factory$9wmI5DEtkoOXIAoXkZwQkwng-HU;-><init>(Lcom/squareup/ui/items/EditItemScopeRunner;)V

    .line 37
    invoke-virtual {p1, v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/common/strings/R$string;->dismiss:I

    .line 39
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 40
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 42
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
