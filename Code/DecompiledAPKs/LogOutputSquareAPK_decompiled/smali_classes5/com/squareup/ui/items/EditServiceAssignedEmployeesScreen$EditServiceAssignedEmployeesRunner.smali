.class public interface abstract Lcom/squareup/ui/items/EditServiceAssignedEmployeesScreen$EditServiceAssignedEmployeesRunner;
.super Ljava/lang/Object;
.source "EditServiceAssignedEmployeesScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditServiceAssignedEmployeesScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "EditServiceAssignedEmployeesRunner"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\"\n\u0002\u0010\u000e\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H&J\u000e\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H&J\u0016\u0010\u0007\u001a\u00020\u00032\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\tH&\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/ui/items/EditServiceAssignedEmployeesScreen$EditServiceAssignedEmployeesRunner;",
        "",
        "assignedEmployeesFinish",
        "",
        "getStaffItemState",
        "Lrx/Observable;",
        "Lcom/squareup/ui/items/EditItemScopeRunner$StaffItemState;",
        "setEmployeeAssigned",
        "employeeIds",
        "",
        "",
        "edit-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract assignedEmployeesFinish()V
.end method

.method public abstract getStaffItemState()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/items/EditItemScopeRunner$StaffItemState;",
            ">;"
        }
    .end annotation
.end method

.method public abstract setEmployeeAssigned(Ljava/util/Set;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method
