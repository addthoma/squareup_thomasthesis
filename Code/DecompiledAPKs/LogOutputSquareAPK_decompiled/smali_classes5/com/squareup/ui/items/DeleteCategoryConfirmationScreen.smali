.class public final Lcom/squareup/ui/items/DeleteCategoryConfirmationScreen;
.super Lcom/squareup/ui/items/InItemsAppletScope;
.source "DeleteCategoryConfirmationScreen.java"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/items/DeleteCategoryConfirmationScreen$Factory;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/items/DeleteCategoryConfirmationScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/DeleteCategoryConfirmationScreen$Component;,
        Lcom/squareup/ui/items/DeleteCategoryConfirmationScreen$Factory;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/items/DeleteCategoryConfirmationScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final categoryId:Ljava/lang/String;

.field private final deleteMessage:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 74
    sget-object v0, Lcom/squareup/ui/items/-$$Lambda$DeleteCategoryConfirmationScreen$iQTrUt0SxpdCYCqQKUfePg7IfvE;->INSTANCE:Lcom/squareup/ui/items/-$$Lambda$DeleteCategoryConfirmationScreen$iQTrUt0SxpdCYCqQKUfePg7IfvE;

    .line 75
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/items/DeleteCategoryConfirmationScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .line 28
    invoke-direct {p0}, Lcom/squareup/ui/items/InItemsAppletScope;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/squareup/ui/items/DeleteCategoryConfirmationScreen;->categoryId:Ljava/lang/String;

    .line 30
    iput p2, p0, Lcom/squareup/ui/items/DeleteCategoryConfirmationScreen;->deleteMessage:I

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/items/DeleteCategoryConfirmationScreen;)I
    .locals 0

    .line 23
    iget p0, p0, Lcom/squareup/ui/items/DeleteCategoryConfirmationScreen;->deleteMessage:I

    return p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/items/DeleteCategoryConfirmationScreen;)Ljava/lang/String;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/squareup/ui/items/DeleteCategoryConfirmationScreen;->categoryId:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/items/DeleteCategoryConfirmationScreen;
    .locals 2

    .line 76
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 77
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result p0

    .line 78
    new-instance v1, Lcom/squareup/ui/items/DeleteCategoryConfirmationScreen;

    invoke-direct {v1, v0, p0}, Lcom/squareup/ui/items/DeleteCategoryConfirmationScreen;-><init>(Ljava/lang/String;I)V

    return-object v1
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 69
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/items/InItemsAppletScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 70
    iget-object p2, p0, Lcom/squareup/ui/items/DeleteCategoryConfirmationScreen;->categoryId:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 71
    iget p2, p0, Lcom/squareup/ui/items/DeleteCategoryConfirmationScreen;->deleteMessage:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
