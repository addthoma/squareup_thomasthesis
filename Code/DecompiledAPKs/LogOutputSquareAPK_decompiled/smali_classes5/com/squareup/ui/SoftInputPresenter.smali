.class public Lcom/squareup/ui/SoftInputPresenter;
.super Lmortar/Presenter;
.source "SoftInputPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/SoftInputPresenter$SharedScope;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/Presenter<",
        "Landroid/app/Activity;",
        ">;"
    }
.end annotation


# instance fields
.field private final showTabletUi:Z


# direct methods
.method constructor <init>(Lcom/squareup/util/Device;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 36
    invoke-direct {p0}, Lmortar/Presenter;-><init>()V

    .line 37
    invoke-interface {p1}, Lcom/squareup/util/Device;->isTablet()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/ui/SoftInputPresenter;->showTabletUi:Z

    return-void
.end method


# virtual methods
.method protected extractBundleService(Landroid/app/Activity;)Lmortar/bundler/BundleService;
    .locals 0

    .line 41
    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Landroid/content/Context;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic extractBundleService(Ljava/lang/Object;)Lmortar/bundler/BundleService;
    .locals 0

    .line 27
    check-cast p1, Landroid/app/Activity;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/SoftInputPresenter;->extractBundleService(Landroid/app/Activity;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method public prepKeyboardForScreen(Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/workflow/SoftInputMode;)V
    .locals 1

    .line 45
    invoke-virtual {p0}, Lcom/squareup/ui/SoftInputPresenter;->hasView()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 47
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/SoftInputPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 48
    invoke-static {v0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 51
    iget-boolean v0, p0, Lcom/squareup/ui/SoftInputPresenter;->showTabletUi:Z

    if-eqz v0, :cond_1

    .line 53
    sget-object p2, Lcom/squareup/workflow/SoftInputMode;->PAN:Lcom/squareup/workflow/SoftInputMode;

    goto :goto_0

    .line 54
    :cond_1
    instance-of v0, p1, Lcom/squareup/container/HasSoftInputModeForPhone;

    if-eqz v0, :cond_2

    .line 56
    check-cast p1, Lcom/squareup/container/HasSoftInputModeForPhone;

    invoke-interface {p1}, Lcom/squareup/container/HasSoftInputModeForPhone;->getSoftInputMode()Lcom/squareup/workflow/SoftInputMode;

    move-result-object p2

    .line 62
    :cond_2
    :goto_0
    invoke-virtual {p0, p2}, Lcom/squareup/ui/SoftInputPresenter;->setSoftInputMode(Lcom/squareup/workflow/SoftInputMode;)V

    return-void
.end method

.method public setSoftInputMode(Lcom/squareup/workflow/SoftInputMode;)V
    .locals 1

    .line 66
    invoke-virtual {p0}, Lcom/squareup/ui/SoftInputPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 67
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/SoftInputPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/workflow/SoftInputMode;->getFlags()I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/view/Window;->setSoftInputMode(I)V

    return-void
.end method
