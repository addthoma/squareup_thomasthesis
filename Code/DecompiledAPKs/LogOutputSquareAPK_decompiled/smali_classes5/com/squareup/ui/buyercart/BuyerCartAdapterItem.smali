.class public abstract Lcom/squareup/ui/buyercart/BuyerCartAdapterItem;
.super Ljava/lang/Object;
.source "BuyerCartAdapterItem.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranCartFooter;,
        Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranCartHeader;,
        Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranSectionHeader;,
        Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem;,
        Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BuyerCartHeader;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0005\t\n\u000b\u000c\rB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0012\u0010\u0003\u001a\u00020\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006R\u0012\u0010\u0007\u001a\u00020\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\u0006\u0082\u0001\u0005\u000e\u000f\u0010\u0011\u0012\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/ui/buyercart/BuyerCartAdapterItem;",
        "",
        "()V",
        "id",
        "",
        "getId",
        "()I",
        "layoutId",
        "getLayoutId",
        "AbstractBuyerCartItem",
        "BranCartFooter",
        "BranCartHeader",
        "BranSectionHeader",
        "BuyerCartHeader",
        "Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranCartFooter;",
        "Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranCartHeader;",
        "Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranSectionHeader;",
        "Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem;",
        "Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BuyerCartHeader;",
        "buyer-cart_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 9
    invoke-direct {p0}, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getId()I
.end method

.method public abstract getLayoutId()I
.end method
