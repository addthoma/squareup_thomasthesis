.class public final Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranCartFooter;
.super Lcom/squareup/ui/buyercart/BuyerCartAdapterItem;
.source "BuyerCartAdapterItem.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyercart/BuyerCartAdapterItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BranCartFooter"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\n\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\r\u001a\u00020\u0003H\u00c0\u0003\u00a2\u0006\u0002\u0008\u000eJ\u0013\u0010\u000f\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\u0010\u001a\u00020\u00112\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u00d6\u0003J\t\u0010\u0014\u001a\u00020\u0006H\u00d6\u0001J\t\u0010\u0015\u001a\u00020\u0003H\u00d6\u0001R\u0014\u0010\u0005\u001a\u00020\u0006X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0014\u0010\t\u001a\u00020\u00068WX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\n\u0010\u0008R\u0014\u0010\u0002\u001a\u00020\u0003X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000c\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranCartFooter;",
        "Lcom/squareup/ui/buyercart/BuyerCartAdapterItem;",
        "total",
        "",
        "(Ljava/lang/String;)V",
        "id",
        "",
        "getId",
        "()I",
        "layoutId",
        "getLayoutId",
        "getTotal$buyer_cart_release",
        "()Ljava/lang/String;",
        "component1",
        "component1$buyer_cart_release",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "buyer-cart_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final id:I

.field private final total:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    const-string/jumbo v0, "total"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 20
    invoke-direct {p0, v0}, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranCartFooter;->total:Ljava/lang/String;

    .line 24
    sget p1, Lcom/squareup/ui/buyercart/R$layout;->bran_cart_list_footer:I

    iput p1, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranCartFooter;->id:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranCartFooter;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranCartFooter;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranCartFooter;->total:Ljava/lang/String;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranCartFooter;->copy(Ljava/lang/String;)Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranCartFooter;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1$buyer_cart_release()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranCartFooter;->total:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;)Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranCartFooter;
    .locals 1

    const-string/jumbo v0, "total"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranCartFooter;

    invoke-direct {v0, p1}, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranCartFooter;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranCartFooter;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranCartFooter;

    iget-object v0, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranCartFooter;->total:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranCartFooter;->total:Ljava/lang/String;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getId()I
    .locals 1

    .line 24
    iget v0, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranCartFooter;->id:I

    return v0
.end method

.method public getLayoutId()I
    .locals 1

    .line 22
    sget v0, Lcom/squareup/ui/buyercart/R$layout;->bran_cart_list_footer:I

    return v0
.end method

.method public final getTotal$buyer_cart_release()Ljava/lang/String;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranCartFooter;->total:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranCartFooter;->total:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BranCartFooter(total="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranCartFooter;->total:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
