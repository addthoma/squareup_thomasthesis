.class public final Lcom/squareup/ui/buyercart/BuyerCartViewHolder$BranCartHeaderViewHolder;
.super Lcom/squareup/ui/buyercart/BuyerCartViewHolder;
.source "BuyerCartViewHolder.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyercart/BuyerCartViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BranCartHeaderViewHolder"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH\u0016R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\t\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u0008\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/ui/buyercart/BuyerCartViewHolder$BranCartHeaderViewHolder;",
        "Lcom/squareup/ui/buyercart/BuyerCartViewHolder;",
        "itemView",
        "Landroid/view/View;",
        "(Landroid/view/View;)V",
        "title",
        "Landroid/widget/TextView;",
        "getTitle",
        "()Landroid/widget/TextView;",
        "total",
        "getTotal",
        "bind",
        "",
        "item",
        "Lcom/squareup/ui/buyercart/BuyerCartAdapterItem;",
        "buyer-cart_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final title:Landroid/widget/TextView;

.field private final total:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    const-string v0, "itemView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyercart/BuyerCartViewHolder;-><init>(Landroid/view/View;)V

    .line 28
    sget v0, Lcom/squareup/ui/buyercart/R$id;->total:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "itemView.findViewById(R.id.total)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/buyercart/BuyerCartViewHolder$BranCartHeaderViewHolder;->total:Landroid/widget/TextView;

    .line 29
    sget v0, Lcom/squareup/ui/buyercart/R$id;->cart_title:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "itemView.findViewById(R.id.cart_title)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/ui/buyercart/BuyerCartViewHolder$BranCartHeaderViewHolder;->title:Landroid/widget/TextView;

    return-void
.end method


# virtual methods
.method public bind(Lcom/squareup/ui/buyercart/BuyerCartAdapterItem;)V
    .locals 5

    const-string v0, "item"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    check-cast p1, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranCartHeader;

    .line 33
    iget-object v0, p0, Lcom/squareup/ui/buyercart/BuyerCartViewHolder$BranCartHeaderViewHolder;->total:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranCartHeader;->getTotal$buyer_cart_release()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/buyercart/BuyerCartViewHolder$BranCartHeaderViewHolder;->itemView:Landroid/view/View;

    const-string v1, "itemView"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 36
    sget v1, Lcom/squareup/ui/buyercart/R$color;->title_color_refund:I

    invoke-static {v0, v1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    .line 37
    sget v2, Lcom/squareup/ui/buyercart/R$color;->title_color_purchase:I

    invoke-static {v0, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 38
    sget v3, Lcom/squareup/ui/buyercart/R$color;->title_color_default:I

    invoke-static {v0, v3}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    .line 39
    iget-object v3, p1, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranCartHeader;->transactionType:Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranCartHeader$TransactionType;

    sget-object v4, Lcom/squareup/ui/buyercart/BuyerCartViewHolder$BranCartHeaderViewHolder$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v3}, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranCartHeader$TransactionType;->ordinal()I

    move-result v3

    aget v3, v4, v3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_2

    const/4 v1, 0x3

    if-eq v3, v1, :cond_0

    goto :goto_1

    .line 51
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/buyercart/BuyerCartViewHolder$BranCartHeaderViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 52
    iget-object v1, p0, Lcom/squareup/ui/buyercart/BuyerCartViewHolder$BranCartHeaderViewHolder;->total:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/buyercart/BuyerCartViewHolder$BranCartHeaderViewHolder;->title:Landroid/widget/TextView;

    .line 54
    invoke-virtual {p1}, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$BranCartHeader;->isOffline$buyer_cart_release()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 55
    sget p1, Lcom/squareup/ui/buyercart/R$string;->buyer_cart_title_offline:I

    goto :goto_0

    .line 57
    :cond_1
    sget p1, Lcom/squareup/ui/buyercart/R$string;->buyer_cart_title:I

    .line 53
    :goto_0
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 46
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/buyercart/BuyerCartViewHolder$BranCartHeaderViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 47
    iget-object p1, p0, Lcom/squareup/ui/buyercart/BuyerCartViewHolder$BranCartHeaderViewHolder;->total:Landroid/widget/TextView;

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 48
    iget-object p1, p0, Lcom/squareup/ui/buyercart/BuyerCartViewHolder$BranCartHeaderViewHolder;->title:Landroid/widget/TextView;

    sget v0, Lcom/squareup/ui/buyercart/R$string;->buyer_cart_title_exchange_even:I

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 41
    :cond_3
    iget-object p1, p0, Lcom/squareup/ui/buyercart/BuyerCartViewHolder$BranCartHeaderViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 42
    iget-object p1, p0, Lcom/squareup/ui/buyercart/BuyerCartViewHolder$BranCartHeaderViewHolder;->total:Landroid/widget/TextView;

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 43
    iget-object p1, p0, Lcom/squareup/ui/buyercart/BuyerCartViewHolder$BranCartHeaderViewHolder;->title:Landroid/widget/TextView;

    sget v0, Lcom/squareup/ui/buyercart/R$string;->buyer_cart_title_refund:I

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(I)V

    :goto_1
    return-void
.end method

.method public final getTitle()Landroid/widget/TextView;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/ui/buyercart/BuyerCartViewHolder$BranCartHeaderViewHolder;->title:Landroid/widget/TextView;

    return-object v0
.end method

.method public final getTotal()Landroid/widget/TextView;
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/ui/buyercart/BuyerCartViewHolder$BranCartHeaderViewHolder;->total:Landroid/widget/TextView;

    return-object v0
.end method
