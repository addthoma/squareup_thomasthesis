.class Lcom/squareup/ui/LinkSpan$Builder$1;
.super Lcom/squareup/ui/LinkSpan;
.source "LinkSpan.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/LinkSpan$Builder;->clickableSpan()Lcom/squareup/ui/LinkSpan;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/LinkSpan$Builder;


# direct methods
.method constructor <init>(Lcom/squareup/ui/LinkSpan$Builder;I)V
    .locals 0

    .line 154
    iput-object p1, p0, Lcom/squareup/ui/LinkSpan$Builder$1;->this$0:Lcom/squareup/ui/LinkSpan$Builder;

    invoke-direct {p0, p2}, Lcom/squareup/ui/LinkSpan;-><init>(I)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1

    .line 156
    iget-object v0, p0, Lcom/squareup/ui/LinkSpan$Builder$1;->this$0:Lcom/squareup/ui/LinkSpan$Builder;

    invoke-static {v0}, Lcom/squareup/ui/LinkSpan$Builder;->access$000(Lcom/squareup/ui/LinkSpan$Builder;)Ljava/lang/Runnable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 157
    iget-object v0, p0, Lcom/squareup/ui/LinkSpan$Builder$1;->this$0:Lcom/squareup/ui/LinkSpan$Builder;

    invoke-static {v0}, Lcom/squareup/ui/LinkSpan$Builder;->access$000(Lcom/squareup/ui/LinkSpan$Builder;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 159
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const-class v0, Lcom/squareup/ui/LinkSpan$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->componentInParent(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/LinkSpan$Component;

    .line 160
    invoke-interface {p1}, Lcom/squareup/ui/LinkSpan$Component;->browserLauncher()Lcom/squareup/util/BrowserLauncher;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/LinkSpan$Builder$1;->this$0:Lcom/squareup/ui/LinkSpan$Builder;

    .line 161
    invoke-static {v0}, Lcom/squareup/ui/LinkSpan$Builder;->access$100(Lcom/squareup/ui/LinkSpan$Builder;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/util/BrowserLauncher;->launchBrowser(Ljava/lang/String;)V

    return-void
.end method
