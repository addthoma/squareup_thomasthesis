.class public final Lcom/squareup/ui/instantdeposits/InstantDepositsResultScreen;
.super Lcom/squareup/ui/activity/InActivityAppletScope;
.source "InstantDepositsResultScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/MaybePersistent;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/instantdeposits/InstantDepositsResultScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/instantdeposits/InstantDepositsResultScreen$Component;
    }
.end annotation


# instance fields
.field public final depositAmount:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/common/Money;)V
    .locals 0

    .line 21
    invoke-direct {p0}, Lcom/squareup/ui/activity/InActivityAppletScope;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultScreen;->depositAmount:Lcom/squareup/protos/common/Money;

    return-void
.end method


# virtual methods
.method public isPersistent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public screenLayout()I
    .locals 1

    .line 36
    sget v0, Lcom/squareup/billhistoryui/R$layout;->instant_deposits_result_view:I

    return v0
.end method
