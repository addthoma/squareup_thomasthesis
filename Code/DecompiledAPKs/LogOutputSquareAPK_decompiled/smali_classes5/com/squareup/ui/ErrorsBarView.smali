.class public Lcom/squareup/ui/ErrorsBarView;
.super Landroid/widget/ScrollView;
.source "ErrorsBarView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/ErrorsBarView$Component;
    }
.end annotation


# static fields
.field private static final UNSET:I = -0x1


# instance fields
.field private final marinGapMultilinePadding:I

.field presenter:Lcom/squareup/ui/ErrorsBarPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final rowContainer:Landroid/widget/LinearLayout;

.field private final rowHeight:I

.field private final rowLeftPadding:I

.field private final rowRightPadding:I

.field private final shakeAnim:Landroid/view/animation/Animation;

.field private final textColor:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    .line 42
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/ui/ErrorsBarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 43
    invoke-virtual {p0}, Lcom/squareup/ui/ErrorsBarView;->getContext()Landroid/content/Context;

    move-result-object p1

    const-class p2, Lcom/squareup/ui/ErrorsBarView$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ErrorsBarView$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/ErrorsBarView$Component;->inject(Lcom/squareup/ui/ErrorsBarView;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .line 47
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 49
    invoke-virtual {p0}, Lcom/squareup/ui/ErrorsBarView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/marin/R$dimen;->marin_gap_multiline_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/squareup/ui/ErrorsBarView;->marinGapMultilinePadding:I

    .line 52
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/squareup/ui/ErrorsBarView;->rowContainer:Landroid/widget/LinearLayout;

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/ErrorsBarView;->rowContainer:Landroid/widget/LinearLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/ErrorsBarView;->rowContainer:Landroid/widget/LinearLayout;

    new-instance v1, Landroid/animation/LayoutTransition;

    invoke-direct {v1}, Landroid/animation/LayoutTransition;-><init>()V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 56
    sget-object v0, Lcom/squareup/widgets/pos/R$styleable;->ErrorsBarView:[I

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 59
    sget p2, Lcom/squareup/widgets/pos/R$styleable;->ErrorsBarView_android_showDividers:I

    invoke-virtual {p1, p2, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result p2

    if-eqz p2, :cond_0

    .line 62
    iget-object p3, p0, Lcom/squareup/ui/ErrorsBarView;->rowContainer:Landroid/widget/LinearLayout;

    invoke-virtual {p3, p2}, Landroid/widget/LinearLayout;->setShowDividers(I)V

    .line 63
    iget-object p2, p0, Lcom/squareup/ui/ErrorsBarView;->rowContainer:Landroid/widget/LinearLayout;

    sget p3, Lcom/squareup/widgets/pos/R$styleable;->ErrorsBarView_android_divider:I

    invoke-virtual {p1, p3}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p3

    invoke-virtual {p2, p3}, Landroid/widget/LinearLayout;->setDividerDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 66
    :cond_0
    sget p2, Lcom/squareup/widgets/pos/R$styleable;->ErrorsBarView_android_textColor:I

    const/4 p3, -0x1

    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p2

    if-ne p2, p3, :cond_1

    .line 68
    invoke-virtual {p0}, Lcom/squareup/ui/ErrorsBarView;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget v0, Lcom/squareup/marin/R$color;->marin_red:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result p2

    .line 70
    :cond_1
    iput p2, p0, Lcom/squareup/ui/ErrorsBarView;->textColor:I

    .line 72
    sget p2, Lcom/squareup/widgets/pos/R$styleable;->ErrorsBarView_errorRowHeight:I

    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p2

    if-ne p2, p3, :cond_2

    .line 74
    invoke-virtual {p0}, Lcom/squareup/ui/ErrorsBarView;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget p3, Lcom/squareup/marin/R$dimen;->marin_min_height:I

    invoke-virtual {p2, p3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    .line 76
    :cond_2
    iput p2, p0, Lcom/squareup/ui/ErrorsBarView;->rowHeight:I

    .line 78
    sget p2, Lcom/squareup/widgets/pos/R$styleable;->ErrorsBarView_android_paddingLeft:I

    invoke-virtual {p1, p2, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p2

    iput p2, p0, Lcom/squareup/ui/ErrorsBarView;->rowLeftPadding:I

    .line 79
    sget p2, Lcom/squareup/widgets/pos/R$styleable;->ErrorsBarView_android_paddingRight:I

    invoke-virtual {p1, p2, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p2

    iput p2, p0, Lcom/squareup/ui/ErrorsBarView;->rowRightPadding:I

    .line 81
    invoke-virtual {p0, v1, v1, v1, v1}, Lcom/squareup/ui/ErrorsBarView;->setPadding(IIII)V

    .line 82
    iget-object p2, p0, Lcom/squareup/ui/ErrorsBarView;->rowContainer:Landroid/widget/LinearLayout;

    invoke-virtual {p2, v1, v1, v1, v1}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 84
    new-instance p2, Lcom/squareup/register/widgets/validation/ShakeAnimation;

    invoke-direct {p2}, Lcom/squareup/register/widgets/validation/ShakeAnimation;-><init>()V

    iput-object p2, p0, Lcom/squareup/ui/ErrorsBarView;->shakeAnim:Landroid/view/animation/Animation;

    .line 86
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    .line 87
    iget-object p1, p0, Lcom/squareup/ui/ErrorsBarView;->rowContainer:Landroid/widget/LinearLayout;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/ErrorsBarView;->addView(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public addRow(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 101
    invoke-virtual {p0, p1}, Lcom/squareup/ui/ErrorsBarView;->removeRow(Ljava/lang/String;)Z

    .line 102
    new-instance v0, Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p0}, Lcom/squareup/ui/ErrorsBarView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/marketfont/MarketTextView;-><init>(Landroid/content/Context;)V

    .line 103
    iget v1, p0, Lcom/squareup/ui/ErrorsBarView;->textColor:I

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setTextColor(I)V

    .line 104
    invoke-virtual {v0, p2}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 105
    iget p2, p0, Lcom/squareup/ui/ErrorsBarView;->rowHeight:I

    invoke-virtual {v0, p2}, Lcom/squareup/marketfont/MarketTextView;->setMinHeight(I)V

    .line 106
    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setTag(Ljava/lang/Object;)V

    const/16 p1, 0x11

    .line 107
    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setGravity(I)V

    .line 108
    sget-object p1, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 109
    iget p1, p0, Lcom/squareup/ui/ErrorsBarView;->rowLeftPadding:I

    if-nez p1, :cond_0

    iget p1, p0, Lcom/squareup/ui/ErrorsBarView;->rowRightPadding:I

    if-eqz p1, :cond_1

    .line 110
    :cond_0
    iget p1, p0, Lcom/squareup/ui/ErrorsBarView;->rowLeftPadding:I

    iget p2, p0, Lcom/squareup/ui/ErrorsBarView;->marinGapMultilinePadding:I

    iget v1, p0, Lcom/squareup/ui/ErrorsBarView;->rowRightPadding:I

    invoke-virtual {v0, p1, p2, v1, p2}, Lcom/squareup/marketfont/MarketTextView;->setPadding(IIII)V

    .line 114
    :cond_1
    new-instance p1, Lcom/squareup/register/widgets/validation/ShakeAnimationListener;

    const/4 p2, 0x1

    invoke-direct {p1, v0, p2}, Lcom/squareup/register/widgets/validation/ShakeAnimationListener;-><init>(Landroid/widget/TextView;Z)V

    .line 115
    iget-object p2, p0, Lcom/squareup/ui/ErrorsBarView;->shakeAnim:Landroid/view/animation/Animation;

    invoke-virtual {p2, p1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 116
    iget-object p1, p0, Lcom/squareup/ui/ErrorsBarView;->rowContainer:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 96
    iget-object v0, p0, Lcom/squareup/ui/ErrorsBarView;->presenter:Lcom/squareup/ui/ErrorsBarPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/ErrorsBarPresenter;->dropView(Ljava/lang/Object;)V

    .line 97
    invoke-super {p0}, Landroid/widget/ScrollView;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 91
    invoke-super {p0}, Landroid/widget/ScrollView;->onFinishInflate()V

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/ErrorsBarView;->presenter:Lcom/squareup/ui/ErrorsBarPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/ErrorsBarPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public removeRow(Ljava/lang/String;)Z
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 120
    :goto_0
    iget-object v2, p0, Lcom/squareup/ui/ErrorsBarView;->rowContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 121
    iget-object v2, p0, Lcom/squareup/ui/ErrorsBarView;->rowContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 122
    iget-object p1, p0, Lcom/squareup/ui/ErrorsBarView;->rowContainer:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v1}, Landroid/widget/LinearLayout;->removeViewAt(I)V

    const/4 p1, 0x1

    return p1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return v0
.end method

.method public shakeErrors()V
    .locals 1

    .line 130
    iget-object v0, p0, Lcom/squareup/ui/ErrorsBarView;->shakeAnim:Landroid/view/animation/Animation;

    invoke-virtual {p0, v0}, Lcom/squareup/ui/ErrorsBarView;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method
