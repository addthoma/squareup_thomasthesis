.class public final Lcom/squareup/ui/SuffixPlugin$watcher$1;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "SuffixPlugin.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/SuffixPlugin;-><init>(Landroid/content/Context;Ljava/lang/String;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001f\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J*\u0010\u0002\u001a\u00020\u00032\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\u00072\u0006\u0010\t\u001a\u00020\u0007H\u0016\u00a8\u0006\n"
    }
    d2 = {
        "com/squareup/ui/SuffixPlugin$watcher$1",
        "Lcom/squareup/text/EmptyTextWatcher;",
        "beforeTextChanged",
        "",
        "s",
        "",
        "start",
        "",
        "count",
        "after",
        "widgets-pos_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/SuffixPlugin;


# direct methods
.method constructor <init>(Lcom/squareup/ui/SuffixPlugin;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 46
    iput-object p1, p0, Lcom/squareup/ui/SuffixPlugin$watcher$1;->this$0:Lcom/squareup/ui/SuffixPlugin;

    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .line 54
    iget-object p1, p0, Lcom/squareup/ui/SuffixPlugin$watcher$1;->this$0:Lcom/squareup/ui/SuffixPlugin;

    invoke-static {p1}, Lcom/squareup/ui/SuffixPlugin;->access$getEditText$p(Lcom/squareup/ui/SuffixPlugin;)Lcom/squareup/noho/NohoEditRow;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/noho/NohoEditRow;->invalidateDrawingInfo()V

    return-void
.end method
