.class public Lcom/squareup/ui/cart/CartFeesModel$Session;
.super Ljava/lang/Object;
.source "CartFeesModel.java"

# interfaces
.implements Lmortar/bundler/Bundler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/cart/CartFeesModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Session"
.end annotation


# instance fields
.field private editFeesState:Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;

.field private final editFeesStateKey:Lcom/squareup/BundleKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final stateKey:Lcom/squareup/BundleKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/ui/cart/CartFeesModel$TaxRowsFlags;",
            ">;"
        }
    .end annotation
.end field

.field private taxRowsFlags:Lcom/squareup/ui/cart/CartFeesModel$TaxRowsFlags;

.field private final transaction:Lcom/squareup/payment/Transaction;

.field private final voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/BundleKey;Lcom/squareup/BundleKey;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Res;Lcom/squareup/tickets/voidcomp/VoidCompSettings;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/ui/cart/CartFeesModel$TaxRowsFlags;",
            ">;",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;",
            ">;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Lcom/squareup/ui/cart/CartFeesModel$Session;->stateKey:Lcom/squareup/BundleKey;

    .line 58
    iput-object p2, p0, Lcom/squareup/ui/cart/CartFeesModel$Session;->editFeesStateKey:Lcom/squareup/BundleKey;

    .line 59
    iput-object p3, p0, Lcom/squareup/ui/cart/CartFeesModel$Session;->transaction:Lcom/squareup/payment/Transaction;

    .line 60
    iput-object p4, p0, Lcom/squareup/ui/cart/CartFeesModel$Session;->res:Lcom/squareup/util/Res;

    .line 61
    iput-object p5, p0, Lcom/squareup/ui/cart/CartFeesModel$Session;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    return-void
.end method


# virtual methods
.method public executeDiscountDeletes()V
    .locals 3

    .line 191
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 192
    iget-object v1, p0, Lcom/squareup/ui/cart/CartFeesModel$Session;->editFeesState:Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;

    invoke-static {v1}, Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;->access$200(Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/Adjustment;

    .line 193
    iget-object v2, v2, Lcom/squareup/checkout/Adjustment;->id:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 195
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/cart/CartFeesModel$Session;->transaction:Lcom/squareup/payment/Transaction;

    sget-object v2, Lcom/squareup/payment/OrderEntryEvents$ChangeSource;->MANUAL:Lcom/squareup/payment/OrderEntryEvents$ChangeSource;

    invoke-virtual {v1, v0, v2}, Lcom/squareup/payment/Transaction;->removeDiscountsFromAllItems(Ljava/lang/Iterable;Lcom/squareup/payment/OrderEntryEvents$ChangeSource;)V

    return-void
.end method

.method public executeTaxDeletes()V
    .locals 2

    .line 278
    iget-object v0, p0, Lcom/squareup/ui/cart/CartFeesModel$Session;->transaction:Lcom/squareup/payment/Transaction;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartFeesModel$Session;->editFeesState:Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;

    invoke-static {v1}, Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;->access$200(Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;)Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Transaction;->removeTaxesFromAllItemsAndSurcharges(Ljava/util/Map;)V

    return-void
.end method

.method public getMortarBundleKey()Ljava/lang/String;
    .locals 1

    .line 65
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hasCustomizedTaxes()Z
    .locals 1

    .line 260
    iget-object v0, p0, Lcom/squareup/ui/cart/CartFeesModel$Session;->taxRowsFlags:Lcom/squareup/ui/cart/CartFeesModel$TaxRowsFlags;

    iget-boolean v0, v0, Lcom/squareup/ui/cart/CartFeesModel$TaxRowsFlags;->notExactlyRuleBasedTaxes:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/cart/CartFeesModel$Session;->editFeesState:Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;

    if-eqz v0, :cond_0

    .line 261
    invoke-static {v0}, Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;->access$200(Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 1

    if-eqz p1, :cond_1

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/cart/CartFeesModel$Session;->taxRowsFlags:Lcom/squareup/ui/cart/CartFeesModel$TaxRowsFlags;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/cart/CartFeesModel$Session;->stateKey:Lcom/squareup/BundleKey;

    invoke-virtual {v0, p1}, Lcom/squareup/BundleKey;->get(Landroid/os/Bundle;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/cart/CartFeesModel$TaxRowsFlags;

    iput-object v0, p0, Lcom/squareup/ui/cart/CartFeesModel$Session;->taxRowsFlags:Lcom/squareup/ui/cart/CartFeesModel$TaxRowsFlags;

    .line 74
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/cart/CartFeesModel$Session;->editFeesState:Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/cart/CartFeesModel$Session;->editFeesStateKey:Lcom/squareup/BundleKey;

    invoke-virtual {v0, p1}, Lcom/squareup/BundleKey;->get(Landroid/os/Bundle;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;

    iput-object p1, p0, Lcom/squareup/ui/cart/CartFeesModel$Session;->editFeesState:Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;

    .line 76
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/cart/CartFeesModel$Session;->taxRowsFlags:Lcom/squareup/ui/cart/CartFeesModel$TaxRowsFlags;

    if-nez p1, :cond_2

    new-instance p1, Lcom/squareup/ui/cart/CartFeesModel$TaxRowsFlags;

    invoke-direct {p1}, Lcom/squareup/ui/cart/CartFeesModel$TaxRowsFlags;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/cart/CartFeesModel$Session;->taxRowsFlags:Lcom/squareup/ui/cart/CartFeesModel$TaxRowsFlags;

    :cond_2
    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/cart/CartFeesModel$Session;->stateKey:Lcom/squareup/BundleKey;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartFeesModel$Session;->taxRowsFlags:Lcom/squareup/ui/cart/CartFeesModel$TaxRowsFlags;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/BundleKey;->put(Landroid/os/Bundle;Ljava/lang/Object;)V

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/cart/CartFeesModel$Session;->editFeesStateKey:Lcom/squareup/BundleKey;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartFeesModel$Session;->editFeesState:Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/BundleKey;->put(Landroid/os/Bundle;Ljava/lang/Object;)V

    return-void
.end method

.method public prepareDiscountRows()V
    .locals 13

    .line 99
    new-instance v0, Lcom/squareup/ui/cart/CartFeesModel$TaxRowsFlags;

    invoke-direct {v0}, Lcom/squareup/ui/cart/CartFeesModel$TaxRowsFlags;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/cart/CartFeesModel$Session;->taxRowsFlags:Lcom/squareup/ui/cart/CartFeesModel$TaxRowsFlags;

    .line 100
    iget-object v0, p0, Lcom/squareup/ui/cart/CartFeesModel$Session;->taxRowsFlags:Lcom/squareup/ui/cart/CartFeesModel$TaxRowsFlags;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartFeesModel$Session;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getCartRowCount()I

    move-result v1

    iput v1, v0, Lcom/squareup/ui/cart/CartFeesModel$TaxRowsFlags;->cartRowCount:I

    .line 102
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 103
    new-instance v1, Lcom/squareup/ui/cart/CartFeesModel$Session$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/cart/CartFeesModel$Session$1;-><init>(Lcom/squareup/ui/cart/CartFeesModel$Session;)V

    .line 113
    new-instance v2, Lcom/squareup/ui/cart/CartFeesModel$Session$2;

    invoke-direct {v2, p0}, Lcom/squareup/ui/cart/CartFeesModel$Session$2;-><init>(Lcom/squareup/ui/cart/CartFeesModel$Session;)V

    .line 125
    iget-object v3, p0, Lcom/squareup/ui/cart/CartFeesModel$Session;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->getAddedCouponsAndCartScopeDiscounts()Ljava/util/Map;

    move-result-object v3

    .line 126
    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    .line 125
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/checkout/Discount;

    .line 127
    iget-object v5, v4, Lcom/squareup/checkout/Discount;->id:Ljava/lang/String;

    const/4 v6, 0x0

    .line 128
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v1, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-wide/16 v6, 0x0

    .line 131
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v2, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    invoke-interface {v0, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 137
    :cond_0
    iget-object v3, p0, Lcom/squareup/ui/cart/CartFeesModel$Session;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->getNotVoidedItems()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    const/4 v5, 0x1

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/checkout/CartItem;

    .line 138
    iget-object v6, p0, Lcom/squareup/ui/cart/CartFeesModel$Session;->transaction:Lcom/squareup/payment/Transaction;

    .line 139
    invoke-virtual {v6, v4}, Lcom/squareup/payment/Transaction;->getOrderAdjustmentAmountsByIdForItemization(Lcom/squareup/checkout/CartItem;)Ljava/util/Map;

    move-result-object v6

    .line 140
    iget-object v4, v4, Lcom/squareup/checkout/CartItem;->appliedDiscounts:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/squareup/checkout/Discount;

    .line 141
    iget-object v8, p0, Lcom/squareup/ui/cart/CartFeesModel$Session;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    invoke-virtual {v8}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->isCompEnabled()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-virtual {v7}, Lcom/squareup/checkout/Discount;->isComp()Z

    move-result v8

    if-eqz v8, :cond_2

    goto :goto_1

    .line 142
    :cond_2
    iget-object v8, v7, Lcom/squareup/checkout/Discount;->id:Ljava/lang/String;

    .line 146
    invoke-interface {v1, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    add-int/2addr v9, v5

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v1, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    invoke-interface {v2, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Long;

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    invoke-interface {v6, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Long;

    invoke-virtual {v11}, Ljava/lang/Long;->longValue()J

    move-result-wide v11

    add-long/2addr v9, v11

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-interface {v2, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    invoke-interface {v0, v8, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 152
    :cond_3
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 153
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 154
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 155
    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/squareup/checkout/Discount;

    .line 156
    new-instance v8, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;

    invoke-direct {v8, v7}, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;-><init>(Lcom/squareup/checkout/Adjustment;)V

    .line 157
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iput v4, v8, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->count:I

    .line 158
    iget-object v4, v7, Lcom/squareup/checkout/Discount;->name:Ljava/lang/String;

    iput-object v4, v8, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->name:Ljava/lang/String;

    .line 159
    invoke-interface {v2, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    iput-wide v9, v8, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->appliedAmount:J

    .line 162
    iget v4, v8, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->count:I

    if-nez v4, :cond_4

    .line 163
    iget-object v4, p0, Lcom/squareup/ui/cart/CartFeesModel$Session;->res:Lcom/squareup/util/Res;

    sget v6, Lcom/squareup/orderentry/R$string;->cart_items_none:I

    invoke-interface {v4, v6}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_3

    .line 164
    :cond_4
    iget v4, v8, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->count:I

    iget-object v6, p0, Lcom/squareup/ui/cart/CartFeesModel$Session;->taxRowsFlags:Lcom/squareup/ui/cart/CartFeesModel$TaxRowsFlags;

    iget v6, v6, Lcom/squareup/ui/cart/CartFeesModel$TaxRowsFlags;->cartRowCount:I

    if-ne v4, v6, :cond_5

    .line 165
    iget-object v4, p0, Lcom/squareup/ui/cart/CartFeesModel$Session;->res:Lcom/squareup/util/Res;

    sget v6, Lcom/squareup/orderentry/R$string;->cart_items_all:I

    invoke-interface {v4, v6}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_3

    .line 166
    :cond_5
    iget v4, v8, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->count:I

    if-ne v4, v5, :cond_6

    .line 167
    iget-object v4, p0, Lcom/squareup/ui/cart/CartFeesModel$Session;->res:Lcom/squareup/util/Res;

    sget v6, Lcom/squareup/orderentry/R$string;->cart_items_one:I

    invoke-interface {v4, v6}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_3

    .line 169
    :cond_6
    iget-object v4, p0, Lcom/squareup/ui/cart/CartFeesModel$Session;->res:Lcom/squareup/util/Res;

    sget v6, Lcom/squareup/orderentry/R$string;->cart_items_some:I

    invoke-interface {v4, v6}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v4

    .line 170
    invoke-static {}, Ljava/text/NumberFormat;->getIntegerInstance()Ljava/text/NumberFormat;

    move-result-object v6

    iget v9, v8, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->count:I

    int-to-long v9, v9

    invoke-virtual {v6, v9, v10}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v6

    const-string v9, "number"

    invoke-virtual {v4, v9, v6}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v4

    .line 171
    invoke-virtual {v4}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v4

    .line 172
    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    .line 174
    :goto_3
    iget-object v6, p0, Lcom/squareup/ui/cart/CartFeesModel$Session;->res:Lcom/squareup/util/Res;

    sget v9, Lcom/squareup/billhistoryui/R$string;->parenthesis:I

    invoke-interface {v6, v9}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v6

    const-string v9, "content"

    .line 175
    invoke-virtual {v6, v9, v4}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v4

    .line 176
    invoke-virtual {v4}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v4

    .line 177
    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v8, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->applicableQuantity:Ljava/lang/String;

    .line 179
    iget v4, v8, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->count:I

    if-nez v4, :cond_7

    invoke-virtual {v7}, Lcom/squareup/checkout/Discount;->canOnlyBeAppliedToOneItem()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 180
    iget-object v4, p0, Lcom/squareup/ui/cart/CartFeesModel$Session;->res:Lcom/squareup/util/Res;

    sget v6, Lcom/squareup/orderentry/R$string;->discount_warning_no_matching_item:I

    invoke-interface {v4, v6}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v8, v4}, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->access$002(Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;Ljava/lang/String;)Ljava/lang/String;

    .line 183
    :cond_7
    invoke-interface {v3, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 186
    :cond_8
    invoke-static {}, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->getComparator()Ljava/util/Comparator;

    move-result-object v0

    invoke-static {v3, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 187
    new-instance v0, Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;

    const/4 v1, 0x0

    invoke-direct {v0, v3, v1}, Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;-><init>(Ljava/util/List;Lcom/squareup/ui/cart/CartFeesModel$1;)V

    iput-object v0, p0, Lcom/squareup/ui/cart/CartFeesModel$Session;->editFeesState:Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;

    return-void
.end method

.method public prepareTaxRows()V
    .locals 12

    .line 203
    new-instance v0, Lcom/squareup/ui/cart/CartFeesModel$TaxRowsFlags;

    invoke-direct {v0}, Lcom/squareup/ui/cart/CartFeesModel$TaxRowsFlags;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/cart/CartFeesModel$Session;->taxRowsFlags:Lcom/squareup/ui/cart/CartFeesModel$TaxRowsFlags;

    .line 204
    iget-object v0, p0, Lcom/squareup/ui/cart/CartFeesModel$Session;->taxRowsFlags:Lcom/squareup/ui/cart/CartFeesModel$TaxRowsFlags;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartFeesModel$Session;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getCartRowCount()I

    move-result v1

    iput v1, v0, Lcom/squareup/ui/cart/CartFeesModel$TaxRowsFlags;->cartRowCount:I

    .line 206
    new-instance v0, Lcom/squareup/ui/cart/CartFeesModel$Session$3;

    invoke-direct {v0, p0}, Lcom/squareup/ui/cart/CartFeesModel$Session$3;-><init>(Lcom/squareup/ui/cart/CartFeesModel$Session;)V

    .line 218
    iget-object v1, p0, Lcom/squareup/ui/cart/CartFeesModel$Session;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getNotVoidedItems()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/CartItem;

    .line 219
    iget-object v4, p0, Lcom/squareup/ui/cart/CartFeesModel$Session;->transaction:Lcom/squareup/payment/Transaction;

    .line 220
    invoke-virtual {v4, v2}, Lcom/squareup/payment/Transaction;->getOrderAdjustmentAmountsByIdForItemization(Lcom/squareup/checkout/CartItem;)Ljava/util/Map;

    move-result-object v4

    .line 221
    iget-object v5, v2, Lcom/squareup/checkout/CartItem;->appliedTaxes:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/checkout/Tax;

    .line 225
    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;

    iget v8, v7, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->count:I

    add-int/2addr v8, v3

    iput v8, v7, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->count:I

    .line 226
    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;

    iget-wide v8, v7, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->appliedAmount:J

    iget-object v6, v6, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    invoke-interface {v4, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    add-long/2addr v8, v10

    iput-wide v8, v7, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->appliedAmount:J

    goto :goto_1

    .line 228
    :cond_0
    iget-object v4, p0, Lcom/squareup/ui/cart/CartFeesModel$Session;->taxRowsFlags:Lcom/squareup/ui/cart/CartFeesModel$TaxRowsFlags;

    iget-boolean v5, v4, Lcom/squareup/ui/cart/CartFeesModel$TaxRowsFlags;->notExactlyRuleBasedTaxes:Z

    invoke-virtual {v2}, Lcom/squareup/checkout/CartItem;->usingExactlyRuleBasedTaxes()Z

    move-result v2

    xor-int/2addr v2, v3

    or-int/2addr v2, v5

    iput-boolean v2, v4, Lcom/squareup/ui/cart/CartFeesModel$TaxRowsFlags;->notExactlyRuleBasedTaxes:Z

    goto :goto_0

    .line 231
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 232
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 233
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/checkout/Tax;

    .line 234
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;

    .line 237
    iget v5, v2, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->count:I

    iget-object v6, p0, Lcom/squareup/ui/cart/CartFeesModel$Session;->taxRowsFlags:Lcom/squareup/ui/cart/CartFeesModel$TaxRowsFlags;

    iget v6, v6, Lcom/squareup/ui/cart/CartFeesModel$TaxRowsFlags;->cartRowCount:I

    if-ne v5, v6, :cond_2

    .line 238
    iget-object v5, p0, Lcom/squareup/ui/cart/CartFeesModel$Session;->res:Lcom/squareup/util/Res;

    sget v6, Lcom/squareup/orderentry/R$string;->cart_items_all:I

    invoke-interface {v5, v6}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_3

    .line 239
    :cond_2
    iget v5, v2, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->count:I

    if-ne v5, v3, :cond_3

    .line 240
    iget-object v5, p0, Lcom/squareup/ui/cart/CartFeesModel$Session;->res:Lcom/squareup/util/Res;

    sget v6, Lcom/squareup/orderentry/R$string;->cart_items_one:I

    invoke-interface {v5, v6}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_3

    .line 242
    :cond_3
    iget-object v5, p0, Lcom/squareup/ui/cart/CartFeesModel$Session;->res:Lcom/squareup/util/Res;

    sget v6, Lcom/squareup/orderentry/R$string;->cart_items_some:I

    invoke-interface {v5, v6}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v5

    .line 243
    invoke-static {}, Ljava/text/NumberFormat;->getIntegerInstance()Ljava/text/NumberFormat;

    move-result-object v6

    iget v7, v2, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->count:I

    int-to-long v7, v7

    invoke-virtual {v6, v7, v8}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v6

    const-string v7, "number"

    invoke-virtual {v5, v7, v6}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v5

    .line 244
    invoke-virtual {v5}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v5

    .line 245
    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    .line 247
    :goto_3
    iget-object v4, v4, Lcom/squareup/checkout/Tax;->name:Ljava/lang/String;

    iput-object v4, v2, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->name:Ljava/lang/String;

    .line 248
    iget-object v4, p0, Lcom/squareup/ui/cart/CartFeesModel$Session;->res:Lcom/squareup/util/Res;

    sget v6, Lcom/squareup/billhistoryui/R$string;->parenthesis:I

    invoke-interface {v4, v6}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v4

    const-string v6, "content"

    .line 249
    invoke-virtual {v4, v6, v5}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v4

    .line 250
    invoke-virtual {v4}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v4

    .line 251
    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->applicableQuantity:Ljava/lang/String;

    .line 252
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 255
    :cond_4
    invoke-static {}, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->getComparator()Ljava/util/Comparator;

    move-result-object v0

    invoke-static {v1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 256
    new-instance v0, Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;-><init>(Ljava/util/List;Lcom/squareup/ui/cart/CartFeesModel$1;)V

    iput-object v0, p0, Lcom/squareup/ui/cart/CartFeesModel$Session;->editFeesState:Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;

    return-void
.end method

.method public requireEditFeesState()Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;
    .locals 2

    .line 88
    iget-object v0, p0, Lcom/squareup/ui/cart/CartFeesModel$Session;->editFeesState:Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;

    if-eqz v0, :cond_0

    return-object v0

    .line 89
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "EditFeesState required but not prepared."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public resetTaxes()V
    .locals 5

    .line 265
    iget-object v0, p0, Lcom/squareup/ui/cart/CartFeesModel$Session;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getCartRowCount()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_2

    .line 267
    iget-object v3, p0, Lcom/squareup/ui/cart/CartFeesModel$Session;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v3, v2}, Lcom/squareup/payment/Transaction;->getOrderItem(I)Lcom/squareup/checkout/CartItem;

    move-result-object v3

    .line 268
    invoke-virtual {v3}, Lcom/squareup/checkout/CartItem;->usingExactlyRuleBasedTaxes()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, v3, Lcom/squareup/checkout/CartItem;->userEditedTaxIds:Ljava/util/Set;

    .line 269
    invoke-interface {v4}, Ljava/util/Set;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1

    .line 270
    :cond_0
    iget-object v4, p0, Lcom/squareup/ui/cart/CartFeesModel$Session;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v3}, Lcom/squareup/checkout/CartItem;->withRuleBasedTaxes()Lcom/squareup/checkout/CartItem;

    move-result-object v3

    invoke-virtual {v4, v2, v3, v1}, Lcom/squareup/payment/Transaction;->replaceItem(ILcom/squareup/checkout/CartItem;Z)V

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 273
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/cart/CartFeesModel$Session;->transaction:Lcom/squareup/payment/Transaction;

    sget-object v1, Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;->EDIT:Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Transaction;->postTaxChanged(Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;)V

    .line 274
    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartFeesModel$Session;->prepareTaxRows()V

    return-void
.end method
