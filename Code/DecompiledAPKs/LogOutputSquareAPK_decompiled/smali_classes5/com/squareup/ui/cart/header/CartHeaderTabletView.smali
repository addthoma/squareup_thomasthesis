.class public Lcom/squareup/ui/cart/header/CartHeaderTabletView;
.super Lcom/squareup/ui/cart/header/CartHeaderBaseView;
.source "CartHeaderTabletView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/cart/header/CartHeaderTabletView$VisualState;
    }
.end annotation


# instance fields
.field private final flyoutDirect:Landroid/animation/Animator;

.field private final flyoutInverse:Landroid/animation/Animator;

.field presenter:Lcom/squareup/ui/cart/header/CartHeaderTabletPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private visualState:Lcom/squareup/ui/cart/header/CartHeaderTabletView$VisualState;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 32
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/cart/header/CartHeaderBaseView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    sget-object p2, Lcom/squareup/ui/cart/header/CartHeaderTabletView$VisualState;->NO_SALE:Lcom/squareup/ui/cart/header/CartHeaderTabletView$VisualState;

    iput-object p2, p0, Lcom/squareup/ui/cart/header/CartHeaderTabletView;->visualState:Lcom/squareup/ui/cart/header/CartHeaderTabletView$VisualState;

    .line 33
    const-class p2, Lcom/squareup/orderentry/OrderEntryScreen$TabletComponent;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/orderentry/OrderEntryScreen$TabletComponent;

    invoke-interface {p2, p0}, Lcom/squareup/orderentry/OrderEntryScreen$TabletComponent;->inject(Lcom/squareup/ui/cart/header/CartHeaderTabletView;)V

    .line 35
    sget p2, Lcom/squareup/orderentry/R$animator;->cart_header_flyout_direct:I

    invoke-static {p1, p2}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/cart/header/CartHeaderTabletView;->flyoutDirect:Landroid/animation/Animator;

    .line 36
    sget p2, Lcom/squareup/orderentry/R$animator;->cart_header_flyout_inverse:I

    invoke-static {p1, p2}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/cart/header/CartHeaderTabletView;->flyoutInverse:Landroid/animation/Animator;

    .line 39
    invoke-virtual {p0}, Lcom/squareup/ui/cart/header/CartHeaderTabletView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/squareup/orderentry/R$string;->cart_menu_drop_down_button_content_description:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 38
    invoke-virtual {p0, p1}, Lcom/squareup/ui/cart/header/CartHeaderTabletView;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method animateFlyout(Ljava/lang/String;)V
    .locals 1

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/cart/header/CartHeaderTabletView;->flyout:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 104
    iget-object p1, p0, Lcom/squareup/ui/cart/header/CartHeaderTabletView;->flyoutDirect:Landroid/animation/Animator;

    invoke-virtual {p1}, Landroid/animation/Animator;->end()V

    .line 105
    iget-object p1, p0, Lcom/squareup/ui/cart/header/CartHeaderTabletView;->flyoutDirect:Landroid/animation/Animator;

    iget-object v0, p0, Lcom/squareup/ui/cart/header/CartHeaderTabletView;->flyout:Landroid/widget/TextView;

    invoke-virtual {p1, v0}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    .line 106
    iget-object p1, p0, Lcom/squareup/ui/cart/header/CartHeaderTabletView;->flyoutDirect:Landroid/animation/Animator;

    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    .line 109
    sget-object p1, Lcom/squareup/ui/cart/header/CartHeaderTabletView$1;->$SwitchMap$com$squareup$ui$cart$header$CartHeaderTabletView$VisualState:[I

    iget-object v0, p0, Lcom/squareup/ui/cart/header/CartHeaderTabletView;->visualState:Lcom/squareup/ui/cart/header/CartHeaderTabletView$VisualState;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/header/CartHeaderTabletView$VisualState;->ordinal()I

    move-result v0

    aget p1, p1, v0

    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 115
    iget-object p1, p0, Lcom/squareup/ui/cart/header/CartHeaderTabletView;->currentSaleContainer:Landroid/view/View;

    goto :goto_0

    .line 119
    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1

    .line 111
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/cart/header/CartHeaderTabletView;->noSale:Landroid/view/View;

    .line 122
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/cart/header/CartHeaderTabletView;->flyoutInverse:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->end()V

    .line 123
    iget-object v0, p0, Lcom/squareup/ui/cart/header/CartHeaderTabletView;->flyoutInverse:Landroid/animation/Animator;

    invoke-virtual {v0, p1}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    .line 124
    iget-object p1, p0, Lcom/squareup/ui/cart/header/CartHeaderTabletView;->flyoutInverse:Landroid/animation/Animator;

    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 43
    invoke-super {p0}, Lcom/squareup/ui/cart/header/CartHeaderBaseView;->onAttachedToWindow()V

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/cart/header/CartHeaderTabletView;->presenter:Lcom/squareup/ui/cart/header/CartHeaderTabletPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/cart/header/CartHeaderTabletPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 97
    iget-object v0, p0, Lcom/squareup/ui/cart/header/CartHeaderTabletView;->presenter:Lcom/squareup/ui/cart/header/CartHeaderTabletPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/cart/header/CartHeaderTabletPresenter;->dropView(Ljava/lang/Object;)V

    .line 98
    invoke-super {p0}, Lcom/squareup/ui/cart/header/CartHeaderBaseView;->onDetachedFromWindow()V

    return-void
.end method

.method protected showCurrentSale(ZLjava/lang/String;)V
    .locals 3

    if-eqz p2, :cond_0

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/cart/header/CartHeaderTabletView;->currentSaleLabel:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 52
    :cond_0
    iget-object p2, p0, Lcom/squareup/ui/cart/header/CartHeaderTabletView;->visualState:Lcom/squareup/ui/cart/header/CartHeaderTabletView$VisualState;

    sget-object v0, Lcom/squareup/ui/cart/header/CartHeaderTabletView$VisualState;->CURRENT_SALE:Lcom/squareup/ui/cart/header/CartHeaderTabletView$VisualState;

    if-eq p2, v0, :cond_2

    .line 53
    sget-object p2, Lcom/squareup/ui/cart/header/CartHeaderTabletView$VisualState;->CURRENT_SALE:Lcom/squareup/ui/cart/header/CartHeaderTabletView$VisualState;

    iput-object p2, p0, Lcom/squareup/ui/cart/header/CartHeaderTabletView;->visualState:Lcom/squareup/ui/cart/header/CartHeaderTabletView$VisualState;

    .line 55
    iget-object p2, p0, Lcom/squareup/ui/cart/header/CartHeaderTabletView;->flyoutDirect:Landroid/animation/Animator;

    invoke-virtual {p2}, Landroid/animation/Animator;->end()V

    .line 56
    iget-object p2, p0, Lcom/squareup/ui/cart/header/CartHeaderTabletView;->flyoutInverse:Landroid/animation/Animator;

    invoke-virtual {p2}, Landroid/animation/Animator;->end()V

    const/4 p2, 0x4

    if-eqz p1, :cond_1

    .line 59
    iget-object p1, p0, Lcom/squareup/ui/cart/header/CartHeaderTabletView;->noSale:Landroid/view/View;

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    .line 60
    iget-object p1, p0, Lcom/squareup/ui/cart/header/CartHeaderTabletView;->currentSaleContainer:Landroid/view/View;

    const/16 p2, 0x96

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    goto :goto_0

    .line 62
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/cart/header/CartHeaderTabletView;->currentSaleContainer:Landroid/view/View;

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 63
    iget-object p1, p0, Lcom/squareup/ui/cart/header/CartHeaderTabletView;->currentSaleContainer:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/view/View;->setY(F)V

    .line 64
    iget-object p1, p0, Lcom/squareup/ui/cart/header/CartHeaderTabletView;->currentSaleContainer:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 65
    iget-object p1, p0, Lcom/squareup/ui/cart/header/CartHeaderTabletView;->noSale:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 66
    iget-object p1, p0, Lcom/squareup/ui/cart/header/CartHeaderTabletView;->noSale:Landroid/view/View;

    invoke-virtual {p1, v1}, Landroid/view/View;->setY(F)V

    .line 67
    iget-object p1, p0, Lcom/squareup/ui/cart/header/CartHeaderTabletView;->noSale:Landroid/view/View;

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    :goto_0
    return-void
.end method

.method protected showNoSale(Z)V
    .locals 4

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/cart/header/CartHeaderTabletView;->visualState:Lcom/squareup/ui/cart/header/CartHeaderTabletView$VisualState;

    sget-object v1, Lcom/squareup/ui/cart/header/CartHeaderTabletView$VisualState;->NO_SALE:Lcom/squareup/ui/cart/header/CartHeaderTabletView$VisualState;

    if-eq v0, v1, :cond_1

    .line 74
    sget-object v0, Lcom/squareup/ui/cart/header/CartHeaderTabletView$VisualState;->NO_SALE:Lcom/squareup/ui/cart/header/CartHeaderTabletView$VisualState;

    iput-object v0, p0, Lcom/squareup/ui/cart/header/CartHeaderTabletView;->visualState:Lcom/squareup/ui/cart/header/CartHeaderTabletView$VisualState;

    const/4 v0, 0x0

    .line 77
    invoke-virtual {p0, v0}, Lcom/squareup/ui/cart/header/CartHeaderTabletView;->setSaleQuantity(I)V

    .line 79
    iget-object v1, p0, Lcom/squareup/ui/cart/header/CartHeaderTabletView;->flyoutDirect:Landroid/animation/Animator;

    invoke-virtual {v1}, Landroid/animation/Animator;->end()V

    .line 80
    iget-object v1, p0, Lcom/squareup/ui/cart/header/CartHeaderTabletView;->flyoutInverse:Landroid/animation/Animator;

    invoke-virtual {v1}, Landroid/animation/Animator;->end()V

    const/4 v1, 0x4

    if-eqz p1, :cond_0

    .line 83
    iget-object p1, p0, Lcom/squareup/ui/cart/header/CartHeaderTabletView;->currentSaleContainer:Landroid/view/View;

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 84
    iget-object p1, p0, Lcom/squareup/ui/cart/header/CartHeaderTabletView;->noSale:Landroid/view/View;

    const/16 v0, 0x96

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    goto :goto_0

    .line 86
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/cart/header/CartHeaderTabletView;->currentSaleContainer:Landroid/view/View;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {p1, v2}, Landroid/view/View;->setAlpha(F)V

    .line 87
    iget-object p1, p0, Lcom/squareup/ui/cart/header/CartHeaderTabletView;->currentSaleContainer:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Landroid/view/View;->setY(F)V

    .line 88
    iget-object p1, p0, Lcom/squareup/ui/cart/header/CartHeaderTabletView;->currentSaleContainer:Landroid/view/View;

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 89
    iget-object p1, p0, Lcom/squareup/ui/cart/header/CartHeaderTabletView;->noSale:Landroid/view/View;

    invoke-virtual {p1, v2}, Landroid/view/View;->setAlpha(F)V

    .line 90
    iget-object p1, p0, Lcom/squareup/ui/cart/header/CartHeaderTabletView;->noSale:Landroid/view/View;

    invoke-virtual {p1, v3}, Landroid/view/View;->setY(F)V

    .line 91
    iget-object p1, p0, Lcom/squareup/ui/cart/header/CartHeaderTabletView;->noSale:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    :goto_0
    return-void
.end method
