.class public final Lcom/squareup/ui/cart/CartEntryModule_ProvideSplitTicketCartEntryViewModelFactoryFactory;
.super Ljava/lang/Object;
.source "CartEntryModule_ProvideSplitTicketCartEntryViewModelFactoryFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/cart/CartEntryViewModelFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final applicationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final durationFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/DurationFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final perUnitFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final voidCompSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/DurationFormatter;",
            ">;)V"
        }
    .end annotation

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/squareup/ui/cart/CartEntryModule_ProvideSplitTicketCartEntryViewModelFactoryFactory;->applicationProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p2, p0, Lcom/squareup/ui/cart/CartEntryModule_ProvideSplitTicketCartEntryViewModelFactoryFactory;->perUnitFormatterProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p3, p0, Lcom/squareup/ui/cart/CartEntryModule_ProvideSplitTicketCartEntryViewModelFactoryFactory;->voidCompSettingsProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p4, p0, Lcom/squareup/ui/cart/CartEntryModule_ProvideSplitTicketCartEntryViewModelFactoryFactory;->resProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p5, p0, Lcom/squareup/ui/cart/CartEntryModule_ProvideSplitTicketCartEntryViewModelFactoryFactory;->durationFormatterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/cart/CartEntryModule_ProvideSplitTicketCartEntryViewModelFactoryFactory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/DurationFormatter;",
            ">;)",
            "Lcom/squareup/ui/cart/CartEntryModule_ProvideSplitTicketCartEntryViewModelFactoryFactory;"
        }
    .end annotation

    .line 54
    new-instance v6, Lcom/squareup/ui/cart/CartEntryModule_ProvideSplitTicketCartEntryViewModelFactoryFactory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/cart/CartEntryModule_ProvideSplitTicketCartEntryViewModelFactoryFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static provideSplitTicketCartEntryViewModelFactory(Landroid/app/Application;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/util/Res;Lcom/squareup/text/DurationFormatter;)Lcom/squareup/ui/cart/CartEntryViewModelFactory;
    .locals 0

    .line 60
    invoke-static {p0, p1, p2, p3, p4}, Lcom/squareup/ui/cart/CartEntryModule;->provideSplitTicketCartEntryViewModelFactory(Landroid/app/Application;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/util/Res;Lcom/squareup/text/DurationFormatter;)Lcom/squareup/ui/cart/CartEntryViewModelFactory;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/cart/CartEntryViewModelFactory;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/cart/CartEntryViewModelFactory;
    .locals 5

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryModule_ProvideSplitTicketCartEntryViewModelFactoryFactory;->applicationProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartEntryModule_ProvideSplitTicketCartEntryViewModelFactoryFactory;->perUnitFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v2, p0, Lcom/squareup/ui/cart/CartEntryModule_ProvideSplitTicketCartEntryViewModelFactoryFactory;->voidCompSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    iget-object v3, p0, Lcom/squareup/ui/cart/CartEntryModule_ProvideSplitTicketCartEntryViewModelFactoryFactory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/util/Res;

    iget-object v4, p0, Lcom/squareup/ui/cart/CartEntryModule_ProvideSplitTicketCartEntryViewModelFactoryFactory;->durationFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/text/DurationFormatter;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/ui/cart/CartEntryModule_ProvideSplitTicketCartEntryViewModelFactoryFactory;->provideSplitTicketCartEntryViewModelFactory(Landroid/app/Application;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/util/Res;Lcom/squareup/text/DurationFormatter;)Lcom/squareup/ui/cart/CartEntryViewModelFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartEntryModule_ProvideSplitTicketCartEntryViewModelFactoryFactory;->get()Lcom/squareup/ui/cart/CartEntryViewModelFactory;

    move-result-object v0

    return-object v0
.end method
