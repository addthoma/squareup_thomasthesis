.class public Lcom/squareup/ui/cart/CartScreenFinisher$Tablet;
.super Ljava/lang/Object;
.source "CartScreenFinisher.java"

# interfaces
.implements Lcom/squareup/ui/cart/CartScreenFinisher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/cart/CartScreenFinisher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Tablet"
.end annotation


# instance fields
.field private final cartDropDownPresenter:Lcom/squareup/orderentry/CartDropDownPresenter;


# direct methods
.method public constructor <init>(Lcom/squareup/orderentry/CartDropDownPresenter;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/squareup/ui/cart/CartScreenFinisher$Tablet;->cartDropDownPresenter:Lcom/squareup/orderentry/CartDropDownPresenter;

    return-void
.end method


# virtual methods
.method public finishCartScreen()V
    .locals 2

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/cart/CartScreenFinisher$Tablet;->cartDropDownPresenter:Lcom/squareup/orderentry/CartDropDownPresenter;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/orderentry/CartDropDownPresenter;->hideCart(Z)V

    return-void
.end method
