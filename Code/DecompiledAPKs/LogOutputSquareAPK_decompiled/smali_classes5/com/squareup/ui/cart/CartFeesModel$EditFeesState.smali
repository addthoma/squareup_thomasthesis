.class public Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;
.super Ljava/lang/Object;
.source "CartFeesModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/cart/CartFeesModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "EditFeesState"
.end annotation


# instance fields
.field private deletedFees:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Adjustment;",
            ">;"
        }
    .end annotation
.end field

.field private feeRows:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;",
            ">;"
        }
    .end annotation
.end field

.field private lastDeletedFee:Lcom/squareup/checkout/Adjustment;

.field private lastRestoredFee:Lcom/squareup/checkout/Adjustment;

.field private previousFeeRows:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;",
            ">;)V"
        }
    .end annotation

    .line 300
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 296
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;->deletedFees:Ljava/util/Map;

    .line 301
    iput-object p1, p0, Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;->feeRows:Ljava/util/List;

    return-void
.end method

.method synthetic constructor <init>(Ljava/util/List;Lcom/squareup/ui/cart/CartFeesModel$1;)V
    .locals 0

    .line 293
    invoke-direct {p0, p1}, Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;-><init>(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$200(Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;)Ljava/util/Map;
    .locals 0

    .line 293
    iget-object p0, p0, Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;->deletedFees:Ljava/util/Map;

    return-object p0
.end method


# virtual methods
.method public clearLastRestoredFee()V
    .locals 1

    const/4 v0, 0x0

    .line 333
    iput-object v0, p0, Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;->lastRestoredFee:Lcom/squareup/checkout/Adjustment;

    return-void
.end method

.method public deleteFee(Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;)V
    .locals 2

    .line 309
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;->feeRows:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 310
    iget-object v1, p0, Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;->feeRows:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    return-void

    .line 312
    :cond_0
    iget-object v1, p1, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->fee:Lcom/squareup/checkout/Adjustment;

    iput-object v1, p0, Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;->lastDeletedFee:Lcom/squareup/checkout/Adjustment;

    const/4 v1, 0x0

    .line 313
    iput-object v1, p0, Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;->lastRestoredFee:Lcom/squareup/checkout/Adjustment;

    .line 315
    iput-object v0, p0, Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;->previousFeeRows:Ljava/util/List;

    .line 316
    iget-object v0, p0, Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;->deletedFees:Ljava/util/Map;

    iget-object v1, p1, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->fee:Lcom/squareup/checkout/Adjustment;

    iget-object v1, v1, Lcom/squareup/checkout/Adjustment;->id:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->fee:Lcom/squareup/checkout/Adjustment;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public getFeeRows()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;",
            ">;"
        }
    .end annotation

    .line 305
    iget-object v0, p0, Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;->feeRows:Ljava/util/List;

    return-object v0
.end method

.method public getLastRestoredFee()Lcom/squareup/checkout/Adjustment;
    .locals 1

    .line 329
    iget-object v0, p0, Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;->lastRestoredFee:Lcom/squareup/checkout/Adjustment;

    return-object v0
.end method

.method public undoDeleteFee()V
    .locals 3

    .line 320
    iget-object v0, p0, Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;->previousFeeRows:Ljava/util/List;

    iput-object v0, p0, Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;->feeRows:Ljava/util/List;

    const/4 v0, 0x0

    .line 321
    iput-object v0, p0, Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;->previousFeeRows:Ljava/util/List;

    .line 323
    iget-object v1, p0, Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;->deletedFees:Ljava/util/Map;

    iget-object v2, p0, Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;->lastDeletedFee:Lcom/squareup/checkout/Adjustment;

    iget-object v2, v2, Lcom/squareup/checkout/Adjustment;->id:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 324
    iget-object v1, p0, Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;->lastDeletedFee:Lcom/squareup/checkout/Adjustment;

    iput-object v1, p0, Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;->lastRestoredFee:Lcom/squareup/checkout/Adjustment;

    .line 325
    iput-object v0, p0, Lcom/squareup/ui/cart/CartFeesModel$EditFeesState;->lastDeletedFee:Lcom/squareup/checkout/Adjustment;

    return-void
.end method
