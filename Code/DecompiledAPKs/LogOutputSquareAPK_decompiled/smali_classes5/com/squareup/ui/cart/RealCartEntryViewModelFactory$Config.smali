.class public final Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;
.super Ljava/lang/Object;
.source "RealCartEntryViewModelFactory.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Config"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0007\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008!\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001:\u0001-BU\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0005\u0012\u0006\u0010\u0008\u001a\u00020\u0005\u0012\u0006\u0010\t\u001a\u00020\u0005\u0012\u0006\u0010\n\u001a\u00020\u0005\u0012\u0006\u0010\u000b\u001a\u00020\u0005\u0012\u0006\u0010\u000c\u001a\u00020\u0005\u0012\u0006\u0010\r\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u000eJ\t\u0010\u001b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001c\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001d\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001e\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001f\u001a\u00020\u0005H\u00c6\u0003J\t\u0010 \u001a\u00020\u0005H\u00c6\u0003J\t\u0010!\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\"\u001a\u00020\u0005H\u00c6\u0003J\t\u0010#\u001a\u00020\u0005H\u00c6\u0003J\t\u0010$\u001a\u00020\u0005H\u00c6\u0003Jm\u0010%\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u00052\u0008\u0008\u0002\u0010\t\u001a\u00020\u00052\u0008\u0008\u0002\u0010\n\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u000c\u001a\u00020\u00052\u0008\u0008\u0002\u0010\r\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010&\u001a\u00020\'2\u0008\u0010(\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010)\u001a\u00020*H\u00d6\u0001J\t\u0010+\u001a\u00020,H\u00d6\u0001R\u0011\u0010\u000b\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0011\u0010\t\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0010R\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0010R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0010R\u0011\u0010\r\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0010R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016R\u0011\u0010\u0007\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0010R\u0011\u0010\u0008\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0010R\u0011\u0010\n\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u0010R\u0011\u0010\u000c\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u0010\u00a8\u0006."
    }
    d2 = {
        "Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;",
        "",
        "subLabelFontSize",
        "",
        "items",
        "Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;",
        "itemAmount",
        "subtotal",
        "taxes",
        "discount",
        "tip",
        "autoGratuity",
        "total",
        "loyaltyTitle",
        "(FLcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;)V",
        "getAutoGratuity",
        "()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;",
        "getDiscount",
        "getItemAmount",
        "getItems",
        "getLoyaltyTitle",
        "getSubLabelFontSize",
        "()F",
        "getSubtotal",
        "getTaxes",
        "getTip",
        "getTotal",
        "component1",
        "component10",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "component9",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "Style",
        "checkout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final autoGratuity:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

.field private final discount:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

.field private final itemAmount:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

.field private final items:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

.field private final loyaltyTitle:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

.field private final subLabelFontSize:F

.field private final subtotal:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

.field private final taxes:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

.field private final tip:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

.field private final total:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;


# direct methods
.method public constructor <init>(FLcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;)V
    .locals 1

    const-string v0, "items"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "itemAmount"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "subtotal"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "taxes"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "discount"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tip"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "autoGratuity"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "total"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loyaltyTitle"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->subLabelFontSize:F

    iput-object p2, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->items:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    iput-object p3, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->itemAmount:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    iput-object p4, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->subtotal:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    iput-object p5, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->taxes:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    iput-object p6, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->discount:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    iput-object p7, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->tip:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    iput-object p8, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->autoGratuity:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    iput-object p9, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->total:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    iput-object p10, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->loyaltyTitle:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;FLcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;ILjava/lang/Object;)Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;
    .locals 11

    move-object v0, p0

    move/from16 v1, p11

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget v2, v0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->subLabelFontSize:F

    goto :goto_0

    :cond_0
    move v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->items:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    goto :goto_1

    :cond_1
    move-object v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->itemAmount:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    goto :goto_2

    :cond_2
    move-object v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->subtotal:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    goto :goto_3

    :cond_3
    move-object v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-object v6, v0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->taxes:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    goto :goto_4

    :cond_4
    move-object/from16 v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-object v7, v0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->discount:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    goto :goto_5

    :cond_5
    move-object/from16 v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-object v8, v0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->tip:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    goto :goto_6

    :cond_6
    move-object/from16 v8, p7

    :goto_6
    and-int/lit16 v9, v1, 0x80

    if-eqz v9, :cond_7

    iget-object v9, v0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->autoGratuity:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    goto :goto_7

    :cond_7
    move-object/from16 v9, p8

    :goto_7
    and-int/lit16 v10, v1, 0x100

    if-eqz v10, :cond_8

    iget-object v10, v0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->total:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    goto :goto_8

    :cond_8
    move-object/from16 v10, p9

    :goto_8
    and-int/lit16 v1, v1, 0x200

    if-eqz v1, :cond_9

    iget-object v1, v0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->loyaltyTitle:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    goto :goto_9

    :cond_9
    move-object/from16 v1, p10

    :goto_9
    move p1, v2

    move-object p2, v3

    move-object p3, v4

    move-object p4, v5

    move-object/from16 p5, v6

    move-object/from16 p6, v7

    move-object/from16 p7, v8

    move-object/from16 p8, v9

    move-object/from16 p9, v10

    move-object/from16 p10, v1

    invoke-virtual/range {p0 .. p10}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->copy(FLcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;)Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()F
    .locals 1

    iget v0, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->subLabelFontSize:F

    return v0
.end method

.method public final component10()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->loyaltyTitle:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    return-object v0
.end method

.method public final component2()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->items:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    return-object v0
.end method

.method public final component3()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->itemAmount:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    return-object v0
.end method

.method public final component4()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->subtotal:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    return-object v0
.end method

.method public final component5()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->taxes:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    return-object v0
.end method

.method public final component6()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->discount:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    return-object v0
.end method

.method public final component7()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->tip:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    return-object v0
.end method

.method public final component8()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->autoGratuity:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    return-object v0
.end method

.method public final component9()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->total:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    return-object v0
.end method

.method public final copy(FLcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;)Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;
    .locals 12

    const-string v0, "items"

    move-object v3, p2

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "itemAmount"

    move-object v4, p3

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "subtotal"

    move-object/from16 v5, p4

    invoke-static {v5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "taxes"

    move-object/from16 v6, p5

    invoke-static {v6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "discount"

    move-object/from16 v7, p6

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tip"

    move-object/from16 v8, p7

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "autoGratuity"

    move-object/from16 v9, p8

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "total"

    move-object/from16 v10, p9

    invoke-static {v10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loyaltyTitle"

    move-object/from16 v11, p10

    invoke-static {v11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    move-object v1, v0

    move v2, p1

    invoke-direct/range {v1 .. v11}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;-><init>(FLcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    iget v0, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->subLabelFontSize:F

    iget v1, p1, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->subLabelFontSize:F

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->items:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    iget-object v1, p1, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->items:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->itemAmount:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    iget-object v1, p1, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->itemAmount:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->subtotal:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    iget-object v1, p1, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->subtotal:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->taxes:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    iget-object v1, p1, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->taxes:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->discount:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    iget-object v1, p1, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->discount:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->tip:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    iget-object v1, p1, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->tip:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->autoGratuity:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    iget-object v1, p1, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->autoGratuity:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->total:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    iget-object v1, p1, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->total:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->loyaltyTitle:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    iget-object p1, p1, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->loyaltyTitle:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAutoGratuity()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;
    .locals 1

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->autoGratuity:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    return-object v0
.end method

.method public final getDiscount()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;
    .locals 1

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->discount:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    return-object v0
.end method

.method public final getItemAmount()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->itemAmount:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    return-object v0
.end method

.method public final getItems()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;
    .locals 1

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->items:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    return-object v0
.end method

.method public final getLoyaltyTitle()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;
    .locals 1

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->loyaltyTitle:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    return-object v0
.end method

.method public final getSubLabelFontSize()F
    .locals 1

    .line 51
    iget v0, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->subLabelFontSize:F

    return v0
.end method

.method public final getSubtotal()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->subtotal:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    return-object v0
.end method

.method public final getTaxes()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->taxes:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    return-object v0
.end method

.method public final getTip()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->tip:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    return-object v0
.end method

.method public final getTotal()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->total:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget v0, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->subLabelFontSize:F

    invoke-static {v0}, L$r8$java8methods$utility$Float$hashCode$IF;->hashCode(F)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->items:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->itemAmount:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->subtotal:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->taxes:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->discount:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->tip:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->autoGratuity:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->total:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->loyaltyTitle:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_8
    add-int/2addr v0, v2

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Config(subLabelFontSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->subLabelFontSize:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", items="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->items:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", itemAmount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->itemAmount:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", subtotal="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->subtotal:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", taxes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->taxes:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", discount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->discount:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", tip="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->tip:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", autoGratuity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->autoGratuity:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", total="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->total:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", loyaltyTitle="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->loyaltyTitle:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
