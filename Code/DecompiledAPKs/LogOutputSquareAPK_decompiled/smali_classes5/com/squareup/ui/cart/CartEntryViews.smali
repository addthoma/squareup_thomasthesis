.class public final Lcom/squareup/ui/cart/CartEntryViews;
.super Ljava/lang/Object;
.source "CartEntryViews.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0011\u0018\u00002\u00020\u0001BW\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0008\u0010\u0005\u001a\u0004\u0018\u00010\u0004\u0012\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0008\u0010\u0008\u001a\u0004\u0018\u00010\u0004\u0012\u0008\u0010\t\u001a\u0004\u0018\u00010\u0004\u0012\u0008\u0010\n\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u000bR\u0017\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0017\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\rR\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0013\u0010\t\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0010R\u0017\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\rR\u0013\u0010\u0008\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0010R\u0013\u0010\n\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0010\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/ui/cart/CartEntryViews;",
        "",
        "orderItemViews",
        "",
        "Lcom/squareup/ui/cart/CartEntryView;",
        "subTotalView",
        "discountViews",
        "taxViews",
        "tipView",
        "surchargeView",
        "totalView",
        "(Ljava/util/List;Lcom/squareup/ui/cart/CartEntryView;Ljava/util/List;Ljava/util/List;Lcom/squareup/ui/cart/CartEntryView;Lcom/squareup/ui/cart/CartEntryView;Lcom/squareup/ui/cart/CartEntryView;)V",
        "getDiscountViews",
        "()Ljava/util/List;",
        "getOrderItemViews",
        "getSubTotalView",
        "()Lcom/squareup/ui/cart/CartEntryView;",
        "getSurchargeView",
        "getTaxViews",
        "getTipView",
        "getTotalView",
        "checkout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final discountViews:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/cart/CartEntryView;",
            ">;"
        }
    .end annotation
.end field

.field private final orderItemViews:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/cart/CartEntryView;",
            ">;"
        }
    .end annotation
.end field

.field private final subTotalView:Lcom/squareup/ui/cart/CartEntryView;

.field private final surchargeView:Lcom/squareup/ui/cart/CartEntryView;

.field private final taxViews:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/cart/CartEntryView;",
            ">;"
        }
    .end annotation
.end field

.field private final tipView:Lcom/squareup/ui/cart/CartEntryView;

.field private final totalView:Lcom/squareup/ui/cart/CartEntryView;


# direct methods
.method public constructor <init>(Ljava/util/List;Lcom/squareup/ui/cart/CartEntryView;Ljava/util/List;Ljava/util/List;Lcom/squareup/ui/cart/CartEntryView;Lcom/squareup/ui/cart/CartEntryView;Lcom/squareup/ui/cart/CartEntryView;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/ui/cart/CartEntryView;",
            ">;",
            "Lcom/squareup/ui/cart/CartEntryView;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/ui/cart/CartEntryView;",
            ">;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/ui/cart/CartEntryView;",
            ">;",
            "Lcom/squareup/ui/cart/CartEntryView;",
            "Lcom/squareup/ui/cart/CartEntryView;",
            "Lcom/squareup/ui/cart/CartEntryView;",
            ")V"
        }
    .end annotation

    const-string v0, "orderItemViews"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "discountViews"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "taxViews"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/cart/CartEntryViews;->orderItemViews:Ljava/util/List;

    iput-object p2, p0, Lcom/squareup/ui/cart/CartEntryViews;->subTotalView:Lcom/squareup/ui/cart/CartEntryView;

    iput-object p3, p0, Lcom/squareup/ui/cart/CartEntryViews;->discountViews:Ljava/util/List;

    iput-object p4, p0, Lcom/squareup/ui/cart/CartEntryViews;->taxViews:Ljava/util/List;

    iput-object p5, p0, Lcom/squareup/ui/cart/CartEntryViews;->tipView:Lcom/squareup/ui/cart/CartEntryView;

    iput-object p6, p0, Lcom/squareup/ui/cart/CartEntryViews;->surchargeView:Lcom/squareup/ui/cart/CartEntryView;

    iput-object p7, p0, Lcom/squareup/ui/cart/CartEntryViews;->totalView:Lcom/squareup/ui/cart/CartEntryView;

    return-void
.end method


# virtual methods
.method public final getDiscountViews()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/ui/cart/CartEntryView;",
            ">;"
        }
    .end annotation

    .line 7
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViews;->discountViews:Ljava/util/List;

    return-object v0
.end method

.method public final getOrderItemViews()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/ui/cart/CartEntryView;",
            ">;"
        }
    .end annotation

    .line 5
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViews;->orderItemViews:Ljava/util/List;

    return-object v0
.end method

.method public final getSubTotalView()Lcom/squareup/ui/cart/CartEntryView;
    .locals 1

    .line 6
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViews;->subTotalView:Lcom/squareup/ui/cart/CartEntryView;

    return-object v0
.end method

.method public final getSurchargeView()Lcom/squareup/ui/cart/CartEntryView;
    .locals 1

    .line 10
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViews;->surchargeView:Lcom/squareup/ui/cart/CartEntryView;

    return-object v0
.end method

.method public final getTaxViews()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/ui/cart/CartEntryView;",
            ">;"
        }
    .end annotation

    .line 8
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViews;->taxViews:Ljava/util/List;

    return-object v0
.end method

.method public final getTipView()Lcom/squareup/ui/cart/CartEntryView;
    .locals 1

    .line 9
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViews;->tipView:Lcom/squareup/ui/cart/CartEntryView;

    return-object v0
.end method

.method public final getTotalView()Lcom/squareup/ui/cart/CartEntryView;
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/squareup/ui/cart/CartEntryViews;->totalView:Lcom/squareup/ui/cart/CartEntryView;

    return-object v0
.end method
