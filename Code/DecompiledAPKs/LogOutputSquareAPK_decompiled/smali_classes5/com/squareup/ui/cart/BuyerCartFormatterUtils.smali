.class public final Lcom/squareup/ui/cart/BuyerCartFormatterUtils;
.super Ljava/lang/Object;
.source "BuyerCartFormatterUtils.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBuyerCartFormatterUtils.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BuyerCartFormatterUtils.kt\ncom/squareup/ui/cart/BuyerCartFormatterUtils\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,142:1\n704#2:143\n777#2,2:144\n1360#2:146\n1429#2,3:147\n1360#2:150\n1429#2,3:151\n1360#2:154\n1429#2,3:155\n*E\n*S KotlinDebug\n*F\n+ 1 BuyerCartFormatterUtils.kt\ncom/squareup/ui/cart/BuyerCartFormatterUtils\n*L\n64#1:143\n64#1,2:144\n65#1:146\n65#1,3:147\n91#1:150\n91#1,3:151\n131#1:154\n131#1,3:155\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u001a(\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\t\u001a*\u0010\n\u001a\u00020\u000b*\u00020\u00022\u0006\u0010\u000c\u001a\u00020\r2\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u0007H\u0002\u001a\u001a\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000f*\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u0007H\u0002\u001a\u001a\u0010\u0011\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\t\u001a \u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00010\u000f*\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\t\u001a\n\u0010\u0013\u001a\u00020\u0014*\u00020\u0002\u001a\n\u0010\u0015\u001a\u00020\u0014*\u00020\u0002\u00a8\u0006\u0016"
    }
    d2 = {
        "getReturnDiscountsDisplayItem",
        "Lcom/squareup/comms/protos/seller/DisplayItem;",
        "Lcom/squareup/checkout/ReturnCart;",
        "percentageFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/util/Percentage;",
        "perUnitFormatter",
        "Lcom/squareup/quantity/PerUnitFormatter;",
        "res",
        "Lcom/squareup/util/Res;",
        "getReturnDisplayDiscount",
        "Lcom/squareup/comms/protos/seller/Discount;",
        "returnDiscount",
        "Lcom/squareup/checkout/ReturnDiscount;",
        "getReturnDisplayTaxes",
        "",
        "Lcom/squareup/comms/protos/seller/Tax;",
        "getReturnTaxesDisplayItem",
        "getReturnTipDisplayItems",
        "shouldShowReturnDiscountRow",
        "",
        "shouldShowReturnTaxRow",
        "transaction_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final getReturnDiscountsDisplayItem(Lcom/squareup/checkout/ReturnCart;Lcom/squareup/text/Formatter;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/util/Res;)Lcom/squareup/comms/protos/seller/DisplayItem;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkout/ReturnCart;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            "Lcom/squareup/util/Res;",
            ")",
            "Lcom/squareup/comms/protos/seller/DisplayItem;"
        }
    .end annotation

    const-string v0, "$this$getReturnDiscountsDisplayItem"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "percentageFormatter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "perUnitFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    invoke-virtual {p0}, Lcom/squareup/checkout/ReturnCart;->getReturnDiscountsAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/money/MoneyMath;->negate(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x2

    .line 87
    invoke-static {p2, v0, v1, v2, v1}, Lcom/squareup/quantity/PerUnitFormatter;->format$default(Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/protos/common/Money;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 88
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 89
    sget v1, Lcom/squareup/transaction/R$string;->cart_discounts:I

    invoke-interface {p3, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p3

    .line 90
    invoke-virtual {p0}, Lcom/squareup/checkout/ReturnCart;->getReturnDiscounts()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 150
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v1, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 151
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 152
    check-cast v3, Lcom/squareup/checkout/ReturnDiscount;

    .line 91
    invoke-static {p0, v3, p1, p2}, Lcom/squareup/ui/cart/BuyerCartFormatterUtils;->getReturnDisplayDiscount(Lcom/squareup/checkout/ReturnCart;Lcom/squareup/checkout/ReturnDiscount;Lcom/squareup/text/Formatter;Lcom/squareup/quantity/PerUnitFormatter;)Lcom/squareup/comms/protos/seller/Discount;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 153
    :cond_0
    check-cast v2, Ljava/util/List;

    .line 93
    new-instance p0, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    invoke-direct {p0}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;-><init>()V

    .line 94
    invoke-virtual {p0, p3}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->name(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object p0

    .line 95
    invoke-virtual {p0, v0}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->price(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object p0

    const/4 p1, 0x1

    .line 96
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p0, p2}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->quantity(Ljava/lang/Integer;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object p0

    .line 97
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->is_discount_row(Ljava/lang/Boolean;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object p0

    .line 98
    sget-object p1, Lcom/squareup/ui/cart/BuyerCartFormatter;->Companion:Lcom/squareup/ui/cart/BuyerCartFormatter$Companion;

    invoke-virtual {p1}, Lcom/squareup/ui/cart/BuyerCartFormatter$Companion;->getRETURN_DISCOUNT_CLIENT_ID()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->client_id(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object p0

    .line 99
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->taxes(Ljava/util/List;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object p0

    .line 100
    invoke-virtual {p0, v2}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->discounts(Ljava/util/List;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object p0

    const/4 p1, 0x0

    .line 101
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->is_section_header_row(Ljava/lang/Boolean;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object p0

    .line 102
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->build()Lcom/squareup/comms/protos/seller/DisplayItem;

    move-result-object p0

    return-object p0
.end method

.method private static final getReturnDisplayDiscount(Lcom/squareup/checkout/ReturnCart;Lcom/squareup/checkout/ReturnDiscount;Lcom/squareup/text/Formatter;Lcom/squareup/quantity/PerUnitFormatter;)Lcom/squareup/comms/protos/seller/Discount;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkout/ReturnCart;",
            "Lcom/squareup/checkout/ReturnDiscount;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ")",
            "Lcom/squareup/comms/protos/seller/Discount;"
        }
    .end annotation

    .line 114
    invoke-virtual {p1}, Lcom/squareup/checkout/ReturnDiscount;->getDiscount()Lcom/squareup/checkout/Discount;

    move-result-object v0

    .line 115
    iget-object v1, v0, Lcom/squareup/checkout/Discount;->percentage:Lcom/squareup/util/Percentage;

    const/4 v2, 0x2

    if-eqz v1, :cond_0

    .line 116
    iget-object p0, v0, Lcom/squareup/checkout/Discount;->percentage:Lcom/squareup/util/Percentage;

    invoke-interface {p2, p0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 118
    :cond_0
    new-instance p2, Lcom/squareup/protos/common/Money;

    invoke-virtual {p1}, Lcom/squareup/checkout/ReturnDiscount;->getAppliedAmount()J

    move-result-wide v3

    neg-long v3, v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {p0}, Lcom/squareup/checkout/ReturnCart;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object p0

    invoke-direct {p2, p1, p0}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    const/4 p0, 0x0

    invoke-static {p3, p2, p0, v2, p0}, Lcom/squareup/quantity/PerUnitFormatter;->format$default(Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/protos/common/Money;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    .line 120
    :goto_0
    sget-object p1, Lkotlin/jvm/internal/StringCompanionObject;->INSTANCE:Lkotlin/jvm/internal/StringCompanionObject;

    new-array p1, v2, [Ljava/lang/Object;

    const/4 p2, 0x0

    .line 122
    iget-object p3, v0, Lcom/squareup/checkout/Discount;->name:Ljava/lang/String;

    aput-object p3, p1, p2

    const/4 p2, 0x1

    aput-object p0, p1, p2

    .line 120
    array-length p0, p1

    invoke-static {p1, p0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p0

    const-string p1, "%s (%s)"

    invoke-static {p1, p0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string p0, "java.lang.String.format(format, *args)"

    invoke-static {v3, p0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 125
    new-instance p0, Lcom/squareup/comms/protos/seller/Discount;

    iget-object v2, v0, Lcom/squareup/checkout/Discount;->id:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/comms/protos/seller/Discount;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object p0
.end method

.method private static final getReturnDisplayTaxes(Lcom/squareup/checkout/ReturnCart;Lcom/squareup/quantity/PerUnitFormatter;)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkout/ReturnCart;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/comms/protos/seller/Tax;",
            ">;"
        }
    .end annotation

    .line 63
    invoke-virtual {p0}, Lcom/squareup/checkout/ReturnCart;->getReturnTaxes()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 143
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 144
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/squareup/checkout/ReturnTax;

    .line 64
    invoke-virtual {v3}, Lcom/squareup/checkout/ReturnTax;->getTax()Lcom/squareup/checkout/Tax;

    move-result-object v3

    iget-object v3, v3, Lcom/squareup/checkout/Tax;->inclusionType:Lcom/squareup/api/items/Fee$InclusionType;

    sget-object v4, Lcom/squareup/api/items/Fee$InclusionType;->ADDITIVE:Lcom/squareup/api/items/Fee$InclusionType;

    if-ne v3, v4, :cond_1

    const/4 v3, 0x1

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    :goto_1
    if-eqz v3, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 145
    :cond_2
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 146
    new-instance v0, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v1, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 147
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 148
    check-cast v2, Lcom/squareup/checkout/ReturnTax;

    .line 66
    new-instance v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v2}, Lcom/squareup/checkout/ReturnTax;->getAppliedAmount()J

    move-result-wide v4

    neg-long v4, v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {p0}, Lcom/squareup/checkout/ReturnCart;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    .line 67
    new-instance v4, Lcom/squareup/comms/protos/seller/Tax;

    invoke-virtual {v2}, Lcom/squareup/checkout/ReturnTax;->getTax()Lcom/squareup/checkout/Tax;

    move-result-object v2

    iget-object v7, v2, Lcom/squareup/checkout/Tax;->name:Ljava/lang/String;

    const-string v2, "returnTax.tax.name"

    invoke-static {v7, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x2

    const/4 v5, 0x0

    invoke-static {p1, v3, v5, v2, v5}, Lcom/squareup/quantity/PerUnitFormatter;->format$default(Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/protos/common/Money;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x4

    const/4 v11, 0x0

    move-object v6, v4

    invoke-direct/range {v6 .. v11}, Lcom/squareup/comms/protos/seller/Tax;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-interface {v0, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 149
    :cond_3
    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public static final getReturnTaxesDisplayItem(Lcom/squareup/checkout/ReturnCart;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/util/Res;)Lcom/squareup/comms/protos/seller/DisplayItem;
    .locals 3

    const-string v0, "$this$getReturnTaxesDisplayItem"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "perUnitFormatter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-virtual {p0}, Lcom/squareup/checkout/ReturnCart;->getAdditiveReturnTaxesAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/money/MoneyMath;->negate(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-static {p1, v0, v1, v2, v1}, Lcom/squareup/quantity/PerUnitFormatter;->format$default(Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/protos/common/Money;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 39
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 40
    invoke-static {p0, p1}, Lcom/squareup/ui/cart/BuyerCartFormatterUtils;->getReturnDisplayTaxes(Lcom/squareup/checkout/ReturnCart;Lcom/squareup/quantity/PerUnitFormatter;)Ljava/util/List;

    move-result-object p0

    .line 42
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result p1

    const/4 v1, 0x1

    if-le p1, v1, :cond_0

    sget p1, Lcom/squareup/transaction/R$string;->cart_taxes_title:I

    goto :goto_0

    :cond_0
    sget p1, Lcom/squareup/transaction/R$string;->cart_tax_row:I

    :goto_0
    invoke-interface {p2, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 44
    new-instance p2, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    invoke-direct {p2}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;-><init>()V

    .line 45
    invoke-virtual {p2, p1}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->name(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object p1

    .line 46
    invoke-virtual {p1, v0}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->price(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object p1

    .line 47
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->quantity(Ljava/lang/Integer;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object p1

    const/4 p2, 0x0

    .line 48
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->is_discount_row(Ljava/lang/Boolean;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object p1

    .line 49
    sget-object v0, Lcom/squareup/ui/cart/BuyerCartFormatter;->Companion:Lcom/squareup/ui/cart/BuyerCartFormatter$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/BuyerCartFormatter$Companion;->getRETURN_TAX_CLIENT_ID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->client_id(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object p1

    .line 50
    invoke-virtual {p1, p0}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->taxes(Ljava/util/List;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object p0

    .line 51
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->discounts(Ljava/util/List;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object p0

    .line 52
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->is_section_header_row(Ljava/lang/Boolean;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object p0

    .line 53
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->build()Lcom/squareup/comms/protos/seller/DisplayItem;

    move-result-object p0

    return-object p0
.end method

.method public static final getReturnTipDisplayItems(Lcom/squareup/checkout/ReturnCart;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/util/Res;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkout/ReturnCart;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            "Lcom/squareup/util/Res;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/comms/protos/seller/DisplayItem;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$getReturnTipDisplayItems"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "perUnitFormatter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 131
    invoke-virtual {p0}, Lcom/squareup/checkout/ReturnCart;->getReturnTips()Ljava/util/List;

    move-result-object p0

    check-cast p0, Ljava/lang/Iterable;

    .line 154
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p0, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 155
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 156
    check-cast v1, Lcom/squareup/checkout/ReturnTip;

    .line 132
    new-instance v2, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    invoke-direct {v2}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;-><init>()V

    .line 133
    sget v3, Lcom/squareup/transaction/R$string;->tip:I

    invoke-interface {p2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->name(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v2

    .line 134
    invoke-virtual {v1}, Lcom/squareup/checkout/ReturnTip;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/money/MoneyMath;->negate(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-static {p1, v1, v4, v3, v4}, Lcom/squareup/quantity/PerUnitFormatter;->format$default(Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/protos/common/Money;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->price(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v1

    const/4 v2, 0x0

    .line 135
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->is_discount_row(Ljava/lang/Boolean;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v1

    const/4 v3, 0x1

    .line 136
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->quantity(Ljava/lang/Integer;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v1

    .line 137
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->discounts(Ljava/util/List;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v1

    .line 138
    sget-object v3, Lcom/squareup/ui/cart/BuyerCartFormatter;->Companion:Lcom/squareup/ui/cart/BuyerCartFormatter$Companion;

    invoke-virtual {v3}, Lcom/squareup/ui/cart/BuyerCartFormatter$Companion;->getRETURN_TIP_CLIENT_ID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->client_id(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v1

    .line 139
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->is_section_header_row(Ljava/lang/Boolean;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v1

    .line 140
    invoke-virtual {v1}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->build()Lcom/squareup/comms/protos/seller/DisplayItem;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 157
    :cond_0
    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public static final shouldShowReturnDiscountRow(Lcom/squareup/checkout/ReturnCart;)Z
    .locals 1

    const-string v0, "$this$shouldShowReturnDiscountRow"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    invoke-virtual {p0}, Lcom/squareup/checkout/ReturnCart;->getReturnDiscounts()Ljava/util/List;

    move-result-object p0

    check-cast p0, Ljava/util/Collection;

    invoke-interface {p0}, Ljava/util/Collection;->isEmpty()Z

    move-result p0

    xor-int/lit8 p0, p0, 0x1

    return p0
.end method

.method public static final shouldShowReturnTaxRow(Lcom/squareup/checkout/ReturnCart;)Z
    .locals 2

    const-string v0, "$this$shouldShowReturnTaxRow"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-virtual {p0}, Lcom/squareup/checkout/ReturnCart;->getAdditiveReturnTaxes()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    if-eqz v0, :cond_0

    .line 27
    invoke-virtual {p0}, Lcom/squareup/checkout/ReturnCart;->getAdditiveReturnTaxesAmount()Lcom/squareup/protos/common/Money;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/money/MoneyMath;->isZero(Lcom/squareup/protos/common/Money;)Z

    move-result p0

    if-nez p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method
