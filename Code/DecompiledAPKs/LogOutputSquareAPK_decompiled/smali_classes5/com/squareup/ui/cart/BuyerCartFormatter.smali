.class public Lcom/squareup/ui/cart/BuyerCartFormatter;
.super Ljava/lang/Object;
.source "BuyerCartFormatter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/cart/BuyerCartFormatter$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBuyerCartFormatter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BuyerCartFormatter.kt\ncom/squareup/ui/cart/BuyerCartFormatter\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,611:1\n704#2:612\n777#2,2:613\n1360#2:615\n1429#2,3:616\n1642#2:619\n1529#2,3:620\n1643#2:623\n704#2:624\n777#2,2:625\n1360#2:627\n1429#2,3:628\n704#2:631\n777#2,2:632\n1360#2:634\n1429#2,3:635\n704#2:638\n777#2,2:639\n1360#2:641\n1429#2,3:642\n1360#2:645\n1429#2,3:646\n1642#2:649\n1642#2,2:650\n1643#2:652\n704#2:653\n777#2,2:654\n1360#2:656\n1429#2,3:657\n*E\n*S KotlinDebug\n*F\n+ 1 BuyerCartFormatter.kt\ncom/squareup/ui/cart/BuyerCartFormatter\n*L\n119#1:612\n119#1,2:613\n120#1:615\n120#1,3:616\n233#1:619\n233#1,3:620\n233#1:623\n247#1:624\n247#1,2:625\n248#1:627\n248#1,3:628\n253#1:631\n253#1,2:632\n254#1:634\n254#1,3:635\n272#1:638\n272#1,2:639\n273#1:641\n273#1,3:642\n376#1:645\n376#1,3:646\n484#1:649\n484#1,2:650\n484#1:652\n527#1:653\n527#1,2:654\n528#1:656\n528#1,3:657\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00ac\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0013\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0016\u0018\u0000 J2\u00020\u0001:\u0001JBG\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u000e\u0008\u0001\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000b\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u00a2\u0006\u0002\u0010\u0011J\u001e\u0010\u0012\u001a\u00020\u00132\u000c\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00160\u00152\u0006\u0010\u0017\u001a\u00020\u0018H\u0002J\u0008\u0010\u0019\u001a\u00020\u0016H\u0002J\u0016\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u001c0\u001b2\u0006\u0010\u001d\u001a\u00020\u001eH\u0002J\u0012\u0010\u001f\u001a\u0004\u0018\u00010 2\u0008\u0010!\u001a\u0004\u0018\u00010\"J\u0016\u0010#\u001a\u0008\u0012\u0004\u0012\u00020%0$2\u0006\u0010\u001d\u001a\u00020\u001eH\u0002J*\u0010&\u001a\u0008\u0012\u0004\u0012\u00020%0$2\u000c\u0010\'\u001a\u0008\u0012\u0004\u0012\u00020(0\u001b2\u000c\u0010)\u001a\u0008\u0012\u0004\u0012\u00020*0\u001bH\u0002J\u0010\u0010+\u001a\u00020\u00162\u0006\u0010\u001d\u001a\u00020\u001eH\u0002J\u0010\u0010,\u001a\u00020*2\u0006\u0010-\u001a\u00020.H\u0002J\u0010\u0010/\u001a\u00020\u00162\u0006\u0010\u001d\u001a\u00020\u001eH\u0002J(\u00100\u001a\u0002012\u000c\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00160\u001b2\u0008\u0010!\u001a\u0004\u0018\u00010\"2\u0006\u00102\u001a\u000203H\u0016J\u0010\u00104\u001a\u0002052\u0006\u00106\u001a\u00020(H\u0002J\u0010\u00107\u001a\u0002052\u0006\u00106\u001a\u00020(H\u0002J\u0016\u00108\u001a\u0008\u0012\u0004\u0012\u0002050\u001b2\u0006\u0010\u001d\u001a\u00020\u001eH\u0002J\u0010\u00109\u001a\u0002052\u0006\u00106\u001a\u00020(H\u0002J\u0016\u0010:\u001a\u0008\u0012\u0004\u0012\u0002050\u001b2\u0006\u0010\u001d\u001a\u00020\u001eH\u0002J\u000e\u0010;\u001a\u0008\u0012\u0004\u0012\u00020\u00160\u001bH\u0016J\n\u0010<\u001a\u0004\u0018\u00010%H\u0002J\u0016\u0010=\u001a\u0008\u0012\u0004\u0012\u00020\u00160\u001b2\u0006\u0010\u001d\u001a\u00020\u001eH\u0002J$\u0010>\u001a\u0008\u0012\u0004\u0012\u0002050\u001b2\u0006\u0010?\u001a\u00020*2\u000c\u0010@\u001a\u0008\u0012\u0004\u0012\u00020%0$H\u0002J\u0012\u0010A\u001a\u0004\u0018\u00010%2\u0006\u0010?\u001a\u00020*H\u0002J\u0016\u0010B\u001a\u0008\u0012\u0004\u0012\u00020%0$2\u0006\u0010\u0017\u001a\u00020\u0018H\u0002J$\u0010C\u001a\u0008\u0012\u0004\u0012\u00020\u00160\u001b2\u0006\u0010\u0017\u001a\u00020\u00182\u000c\u0010@\u001a\u0008\u0012\u0004\u0012\u00020%0$H\u0002J\u0010\u0010D\u001a\n E*\u0004\u0018\u00010*0*H\u0002J\u0010\u0010F\u001a\u00020\u00162\u0006\u0010\u001d\u001a\u00020\u001eH\u0002J\u0012\u0010G\u001a\u0004\u0018\u00010%2\u0006\u0010?\u001a\u00020*H\u0002J\u000c\u0010H\u001a\u00020I*\u00020*H\u0002R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006K"
    }
    d2 = {
        "Lcom/squareup/ui/cart/BuyerCartFormatter;",
        "",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "transaction",
        "Lcom/squareup/payment/Transaction;",
        "res",
        "Lcom/squareup/util/Res;",
        "perUnitFormatter",
        "Lcom/squareup/quantity/PerUnitFormatter;",
        "percentageFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/util/Percentage;",
        "offlineModeMonitor",
        "Lcom/squareup/payment/OfflineModeMonitor;",
        "countryCode",
        "Lcom/squareup/CountryCode;",
        "(Lcom/squareup/settings/server/Features;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/CountryCode;)V",
        "addReturnRows",
        "",
        "displayItems",
        "",
        "Lcom/squareup/comms/protos/seller/DisplayItem;",
        "returnCart",
        "Lcom/squareup/checkout/ReturnCart;",
        "autoGratuityDisplayItem",
        "getCalculatedAdditiveTaxes",
        "",
        "Lcom/squareup/comms/protos/seller/Tax;",
        "order",
        "Lcom/squareup/payment/Order;",
        "getCartChangedBanner",
        "Lcom/squareup/comms/protos/seller/DisplayBanner;",
        "event",
        "Lcom/squareup/payment/OrderEntryEvents$CartChanged;",
        "getCartLevelDiscountIds",
        "",
        "",
        "getCartLevelDiscounts",
        "discounts",
        "Lcom/squareup/checkout/Discount;",
        "cartItems",
        "Lcom/squareup/checkout/CartItem;",
        "getCompRow",
        "getDiscountOrderItem",
        "changeType",
        "Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;",
        "getDiscountRow",
        "getDisplayCart",
        "Lcom/squareup/comms/protos/seller/DisplayCart;",
        "isCardStillInsertedFromPreviousTransaction",
        "",
        "getDisplayComp",
        "Lcom/squareup/comms/protos/seller/Discount;",
        "discount",
        "getDisplayCompReason",
        "getDisplayComps",
        "getDisplayDiscount",
        "getDisplayDiscounts",
        "getDisplayItems",
        "getFirstDiscountName",
        "getItemsRows",
        "getPerItemProtoDiscounts",
        "item",
        "cartLevelDiscountIds",
        "getPerUnitPriceAndQuantity",
        "getReturnCartLevelDiscountIds",
        "getReturnItems",
        "getTaxOrderItem",
        "kotlin.jvm.PlatformType",
        "getTaxRow",
        "getVariationAndModifiers",
        "getDisplayQuantity",
        "",
        "Companion",
        "transaction_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final AUTO_GRAT_CLIENT_ID:Ljava/lang/String;

.field private static final COMP_CLIENT_ID:Ljava/lang/String;

.field public static final Companion:Lcom/squareup/ui/cart/BuyerCartFormatter$Companion;

.field private static final DISCOUNT_CLIENT_ID:Ljava/lang/String;

.field private static final PURCHASE_HEADER_CLIENT_ID:Ljava/lang/String;

.field private static final RETURN_DISCOUNT_CLIENT_ID:Ljava/lang/String;

.field private static final RETURN_HEADER_CLIENT_ID:Ljava/lang/String;

.field private static final RETURN_TAX_CLIENT_ID:Ljava/lang/String;

.field private static final RETURN_TIP_CLIENT_ID:Ljava/lang/String;

.field private static final TAX_CLIENT_ID:Ljava/lang/String;


# instance fields
.field private final countryCode:Lcom/squareup/CountryCode;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

.field private final perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

.field private final percentageFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/cart/BuyerCartFormatter$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/cart/BuyerCartFormatter$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/cart/BuyerCartFormatter;->Companion:Lcom/squareup/ui/cart/BuyerCartFormatter$Companion;

    .line 591
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    .line 592
    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "UUID.randomUUID()\n        .toString()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/ui/cart/BuyerCartFormatter;->DISCOUNT_CLIENT_ID:Ljava/lang/String;

    .line 593
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    .line 594
    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/ui/cart/BuyerCartFormatter;->COMP_CLIENT_ID:Ljava/lang/String;

    .line 595
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    .line 596
    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/ui/cart/BuyerCartFormatter;->TAX_CLIENT_ID:Ljava/lang/String;

    .line 597
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    .line 598
    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/ui/cart/BuyerCartFormatter;->AUTO_GRAT_CLIENT_ID:Ljava/lang/String;

    .line 599
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    .line 600
    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/ui/cart/BuyerCartFormatter;->PURCHASE_HEADER_CLIENT_ID:Ljava/lang/String;

    .line 601
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    .line 602
    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/ui/cart/BuyerCartFormatter;->RETURN_DISCOUNT_CLIENT_ID:Ljava/lang/String;

    .line 603
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    .line 604
    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/ui/cart/BuyerCartFormatter;->RETURN_TAX_CLIENT_ID:Ljava/lang/String;

    .line 605
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    .line 606
    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/ui/cart/BuyerCartFormatter;->RETURN_TIP_CLIENT_ID:Ljava/lang/String;

    .line 607
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    .line 608
    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/ui/cart/BuyerCartFormatter;->RETURN_HEADER_CLIENT_ID:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/settings/server/Features;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/CountryCode;)V
    .locals 1
    .param p5    # Lcom/squareup/text/Formatter;
        .annotation runtime Lcom/squareup/text/ForPercentage;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            "Lcom/squareup/CountryCode;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "features"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "transaction"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "perUnitFormatter"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "percentageFormatter"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "offlineModeMonitor"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "countryCode"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->features:Lcom/squareup/settings/server/Features;

    iput-object p2, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->transaction:Lcom/squareup/payment/Transaction;

    iput-object p3, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->res:Lcom/squareup/util/Res;

    iput-object p4, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iput-object p5, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->percentageFormatter:Lcom/squareup/text/Formatter;

    iput-object p6, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

    iput-object p7, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->countryCode:Lcom/squareup/CountryCode;

    return-void
.end method

.method public static final synthetic access$getAUTO_GRAT_CLIENT_ID$cp()Ljava/lang/String;
    .locals 1

    .line 44
    sget-object v0, Lcom/squareup/ui/cart/BuyerCartFormatter;->AUTO_GRAT_CLIENT_ID:Ljava/lang/String;

    return-object v0
.end method

.method public static final synthetic access$getCOMP_CLIENT_ID$cp()Ljava/lang/String;
    .locals 1

    .line 44
    sget-object v0, Lcom/squareup/ui/cart/BuyerCartFormatter;->COMP_CLIENT_ID:Ljava/lang/String;

    return-object v0
.end method

.method public static final synthetic access$getDISCOUNT_CLIENT_ID$cp()Ljava/lang/String;
    .locals 1

    .line 44
    sget-object v0, Lcom/squareup/ui/cart/BuyerCartFormatter;->DISCOUNT_CLIENT_ID:Ljava/lang/String;

    return-object v0
.end method

.method public static final synthetic access$getPURCHASE_HEADER_CLIENT_ID$cp()Ljava/lang/String;
    .locals 1

    .line 44
    sget-object v0, Lcom/squareup/ui/cart/BuyerCartFormatter;->PURCHASE_HEADER_CLIENT_ID:Ljava/lang/String;

    return-object v0
.end method

.method public static final synthetic access$getRETURN_DISCOUNT_CLIENT_ID$cp()Ljava/lang/String;
    .locals 1

    .line 44
    sget-object v0, Lcom/squareup/ui/cart/BuyerCartFormatter;->RETURN_DISCOUNT_CLIENT_ID:Ljava/lang/String;

    return-object v0
.end method

.method public static final synthetic access$getRETURN_HEADER_CLIENT_ID$cp()Ljava/lang/String;
    .locals 1

    .line 44
    sget-object v0, Lcom/squareup/ui/cart/BuyerCartFormatter;->RETURN_HEADER_CLIENT_ID:Ljava/lang/String;

    return-object v0
.end method

.method public static final synthetic access$getRETURN_TAX_CLIENT_ID$cp()Ljava/lang/String;
    .locals 1

    .line 44
    sget-object v0, Lcom/squareup/ui/cart/BuyerCartFormatter;->RETURN_TAX_CLIENT_ID:Ljava/lang/String;

    return-object v0
.end method

.method public static final synthetic access$getRETURN_TIP_CLIENT_ID$cp()Ljava/lang/String;
    .locals 1

    .line 44
    sget-object v0, Lcom/squareup/ui/cart/BuyerCartFormatter;->RETURN_TIP_CLIENT_ID:Ljava/lang/String;

    return-object v0
.end method

.method public static final synthetic access$getTAX_CLIENT_ID$cp()Ljava/lang/String;
    .locals 1

    .line 44
    sget-object v0, Lcom/squareup/ui/cart/BuyerCartFormatter;->TAX_CLIENT_ID:Ljava/lang/String;

    return-object v0
.end method

.method private final addReturnRows(Ljava/util/List;Lcom/squareup/checkout/ReturnCart;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/comms/protos/seller/DisplayItem;",
            ">;",
            "Lcom/squareup/checkout/ReturnCart;",
            ")V"
        }
    .end annotation

    .line 413
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const-string v3, "emptyList()"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    if-nez v0, :cond_0

    .line 417
    new-instance v0, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    invoke-direct {v0}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;-><init>()V

    .line 418
    iget-object v7, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->res:Lcom/squareup/util/Res;

    sget v8, Lcom/squareup/transaction/R$string;->buyer_cart_section_header_purchase:I

    invoke-interface {v7, v8}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->name(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v0

    .line 419
    invoke-virtual {v0, v6}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->quantity(Ljava/lang/Integer;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v0

    .line 420
    sget-object v7, Lcom/squareup/ui/cart/BuyerCartFormatter;->PURCHASE_HEADER_CLIENT_ID:Ljava/lang/String;

    invoke-virtual {v0, v7}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->client_id(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v0

    .line 421
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v7

    invoke-static {v7, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v7}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->taxes(Ljava/util/List;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v0

    .line 422
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v7

    invoke-static {v7, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v7}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->discounts(Ljava/util/List;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v0

    .line 423
    invoke-virtual {v0, v5}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->is_discount_row(Ljava/lang/Boolean;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v0

    .line 424
    invoke-virtual {v0, v2}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->is_section_header_row(Ljava/lang/Boolean;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v0

    .line 425
    invoke-virtual {v0}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->build()Lcom/squareup/comms/protos/seller/DisplayItem;

    move-result-object v0

    .line 428
    invoke-interface {p1, v4, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 432
    :cond_0
    check-cast p1, Ljava/util/Collection;

    new-instance v0, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    invoke-direct {v0}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;-><init>()V

    .line 433
    iget-object v4, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->res:Lcom/squareup/util/Res;

    sget v7, Lcom/squareup/transaction/R$string;->buyer_cart_section_header_return:I

    invoke-interface {v4, v7}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->name(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v0

    .line 434
    invoke-virtual {v0, v6}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->quantity(Ljava/lang/Integer;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v0

    .line 435
    sget-object v4, Lcom/squareup/ui/cart/BuyerCartFormatter;->RETURN_HEADER_CLIENT_ID:Ljava/lang/String;

    invoke-virtual {v0, v4}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->client_id(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v0

    .line 436
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v4

    invoke-static {v4, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->taxes(Ljava/util/List;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v0

    .line 437
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v4

    invoke-static {v4, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->discounts(Ljava/util/List;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v0

    .line 438
    invoke-virtual {v0, v5}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->is_discount_row(Ljava/lang/Boolean;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v0

    .line 439
    invoke-virtual {v0, v2}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->is_section_header_row(Ljava/lang/Boolean;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v0

    .line 440
    invoke-virtual {v0}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->build()Lcom/squareup/comms/protos/seller/DisplayItem;

    move-result-object v0

    .line 432
    invoke-interface {p1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 442
    invoke-direct {p0, p2}, Lcom/squareup/ui/cart/BuyerCartFormatter;->getReturnCartLevelDiscountIds(Lcom/squareup/checkout/ReturnCart;)Ljava/util/Set;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lcom/squareup/ui/cart/BuyerCartFormatter;->getReturnItems(Lcom/squareup/checkout/ReturnCart;Ljava/util/Set;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {p1, v0}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    .line 445
    invoke-static {p2}, Lcom/squareup/ui/cart/BuyerCartFormatterUtils;->shouldShowReturnDiscountRow(Lcom/squareup/checkout/ReturnCart;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 447
    iget-object v0, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->percentageFormatter:Lcom/squareup/text/Formatter;

    iget-object v2, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v3, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->res:Lcom/squareup/util/Res;

    .line 446
    invoke-static {p2, v0, v2, v3}, Lcom/squareup/ui/cart/BuyerCartFormatterUtils;->getReturnDiscountsDisplayItem(Lcom/squareup/checkout/ReturnCart;Lcom/squareup/text/Formatter;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/util/Res;)Lcom/squareup/comms/protos/seller/DisplayItem;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 452
    :cond_1
    invoke-virtual {p2}, Lcom/squareup/checkout/ReturnCart;->getReturnTips()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/2addr v0, v1

    if-eqz v0, :cond_2

    .line 453
    iget-object v0, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v1, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->res:Lcom/squareup/util/Res;

    invoke-static {p2, v0, v1}, Lcom/squareup/ui/cart/BuyerCartFormatterUtils;->getReturnTipDisplayItems(Lcom/squareup/checkout/ReturnCart;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/util/Res;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {p1, v0}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    .line 457
    :cond_2
    invoke-static {p2}, Lcom/squareup/ui/cart/BuyerCartFormatterUtils;->shouldShowReturnTaxRow(Lcom/squareup/checkout/ReturnCart;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 458
    iget-object v0, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v1, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->res:Lcom/squareup/util/Res;

    invoke-static {p2, v0, v1}, Lcom/squareup/ui/cart/BuyerCartFormatterUtils;->getReturnTaxesDisplayItem(Lcom/squareup/checkout/ReturnCart;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/util/Res;)Lcom/squareup/comms/protos/seller/DisplayItem;

    move-result-object p2

    invoke-interface {p1, p2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_3
    return-void
.end method

.method private final autoGratuityDisplayItem()Lcom/squareup/comms/protos/seller/DisplayItem;
    .locals 7

    .line 386
    iget-object v0, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getAutoGratuity()Lcom/squareup/checkout/Surcharge$AutoGratuity;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    const-string/jumbo v1, "transaction.autoGratuity!!"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 387
    iget-object v1, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->res:Lcom/squareup/util/Res;

    iget-object v2, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->countryCode:Lcom/squareup/CountryCode;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v3, v2}, Lcom/squareup/checkout/Surcharge$AutoGratuity;->formattedName(Lcom/squareup/util/Res;ZLcom/squareup/CountryCode;)Ljava/lang/String;

    move-result-object v1

    .line 388
    iget-object v2, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v4, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v4}, Lcom/squareup/payment/Transaction;->getAutoGratuityAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v4

    const/4 v5, 0x2

    const/4 v6, 0x0

    invoke-static {v2, v4, v6, v5, v6}, Lcom/squareup/quantity/PerUnitFormatter;->format$default(Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/protos/common/Money;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 389
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 391
    iget-object v4, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->res:Lcom/squareup/util/Res;

    .line 392
    iget-object v5, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v5}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v5

    invoke-virtual {v5}, Lcom/squareup/payment/Order;->getSeatingHandler()Lcom/squareup/payment/OrderSeatingHandler;

    move-result-object v5

    invoke-virtual {v5}, Lcom/squareup/payment/OrderSeatingHandler;->getEnabledSeats()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    .line 393
    iget-object v6, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->countryCode:Lcom/squareup/CountryCode;

    .line 390
    invoke-virtual {v0, v4, v5, v6}, Lcom/squareup/checkout/Surcharge$AutoGratuity;->disclosureText(Lcom/squareup/util/Res;ILcom/squareup/CountryCode;)Ljava/lang/String;

    move-result-object v0

    .line 396
    new-instance v4, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    invoke-direct {v4}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;-><init>()V

    .line 397
    invoke-virtual {v4, v1}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->name(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v1

    .line 398
    invoke-virtual {v1, v2}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->price(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v1

    .line 399
    invoke-virtual {v1, v0}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->description(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v0

    .line 400
    sget-object v1, Lcom/squareup/ui/cart/BuyerCartFormatter;->AUTO_GRAT_CLIENT_ID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->client_id(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v0

    .line 401
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    const-string v2, "emptyList()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->taxes(Ljava/util/List;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v0

    const/4 v1, 0x0

    .line 402
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->is_discount_row(Ljava/lang/Boolean;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v0

    .line 403
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->quantity(Ljava/lang/Integer;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v0

    .line 404
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v3

    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->discounts(Ljava/util/List;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v0

    .line 405
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->is_section_header_row(Ljava/lang/Boolean;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v0

    .line 406
    invoke-virtual {v0}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->build()Lcom/squareup/comms/protos/seller/DisplayItem;

    move-result-object v0

    return-object v0
.end method

.method private final getCalculatedAdditiveTaxes(Lcom/squareup/payment/Order;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/Order;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/comms/protos/seller/Tax;",
            ">;"
        }
    .end annotation

    .line 375
    invoke-virtual {p1}, Lcom/squareup/payment/Order;->getAdditiveTaxOrderAdjustments()Ljava/util/List;

    move-result-object p1

    const-string v0, "order.additiveTaxOrderAdjustments"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Iterable;

    .line 645
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 646
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 647
    check-cast v1, Lcom/squareup/payment/OrderAdjustment;

    .line 377
    new-instance v8, Lcom/squareup/comms/protos/seller/Tax;

    iget-object v3, v1, Lcom/squareup/payment/OrderAdjustment;->name:Ljava/lang/String;

    const-string v2, "tax.name"

    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v1, v1, Lcom/squareup/payment/OrderAdjustment;->appliedMoney:Lcom/squareup/protos/common/Money;

    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-static {v2, v1, v5, v4, v5}, Lcom/squareup/quantity/PerUnitFormatter;->format$default(Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/protos/common/Money;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v2, v8

    invoke-direct/range {v2 .. v7}, Lcom/squareup/comms/protos/seller/Tax;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-interface {v0, v8}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 648
    :cond_0
    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method private final getCartLevelDiscountIds(Lcom/squareup/payment/Order;)Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/Order;",
            ")",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 214
    invoke-virtual {p1}, Lcom/squareup/payment/Order;->getAllAppliedDiscountsInDisplayOrder()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->toList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    .line 215
    invoke-virtual {p1}, Lcom/squareup/payment/Order;->getNotVoidedItems()Ljava/util/List;

    move-result-object p1

    const-string v1, "order.notVoidedItems"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 213
    invoke-direct {p0, v0, p1}, Lcom/squareup/ui/cart/BuyerCartFormatter;->getCartLevelDiscounts(Ljava/util/List;Ljava/util/List;)Ljava/util/Set;

    move-result-object p1

    return-object p1
.end method

.method private final getCartLevelDiscounts(Ljava/util/List;Ljava/util/List;)Ljava/util/Set;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/Discount;",
            ">;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/checkout/CartItem;",
            ">;)",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 231
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 233
    check-cast p1, Ljava/lang/Iterable;

    .line 619
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/Discount;

    .line 235
    move-object v2, p2

    check-cast v2, Ljava/lang/Iterable;

    .line 620
    instance-of v3, v2, Ljava/util/Collection;

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-eqz v3, :cond_1

    move-object v3, v2

    check-cast v3, Ljava/util/Collection;

    invoke-interface {v3}, Ljava/util/Collection;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_3

    .line 621
    :cond_1
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/checkout/CartItem;

    .line 237
    invoke-virtual {v3}, Lcom/squareup/checkout/CartItem;->isInteresting()Z

    move-result v6

    if-eqz v6, :cond_4

    iget-object v3, v3, Lcom/squareup/checkout/CartItem;->appliedDiscounts:Ljava/util/Map;

    const-string v6, "item.appliedDiscounts"

    invoke-static {v3, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v6, v1, Lcom/squareup/checkout/Discount;->id:Ljava/lang/String;

    invoke-interface {v3, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_3
    const/4 v3, 0x0

    goto :goto_2

    :cond_4
    :goto_1
    const/4 v3, 0x1

    :goto_2
    if-nez v3, :cond_2

    const/4 v5, 0x0

    :cond_5
    :goto_3
    if-eqz v5, :cond_0

    .line 240
    iget-object v1, v1, Lcom/squareup/checkout/Discount;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 242
    :cond_6
    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method private final getCompRow(Lcom/squareup/payment/Order;)Lcom/squareup/comms/protos/seller/DisplayItem;
    .locals 5

    .line 166
    new-instance v0, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    invoke-direct {v0}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;-><init>()V

    .line 167
    iget-object v1, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/checkout/R$string;->cart_comps:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->name(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v0

    .line 168
    iget-object v1, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    invoke-virtual {p1}, Lcom/squareup/payment/Order;->getAllComps()Lcom/squareup/protos/common/Money;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    invoke-static {v1, v2, v3, v4, v3}, Lcom/squareup/quantity/PerUnitFormatter;->format$default(Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/protos/common/Money;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->price(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v0

    .line 169
    invoke-virtual {v0, v3}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->description(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v0

    .line 170
    sget-object v1, Lcom/squareup/ui/cart/BuyerCartFormatter;->COMP_CLIENT_ID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->client_id(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v0

    .line 171
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    const-string v2, "emptyList()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->taxes(Ljava/util/List;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v0

    const/4 v1, 0x0

    .line 172
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->is_discount_row(Ljava/lang/Boolean;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v0

    const/4 v2, 0x1

    .line 173
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->quantity(Ljava/lang/Integer;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v0

    .line 174
    invoke-direct {p0, p1}, Lcom/squareup/ui/cart/BuyerCartFormatter;->getDisplayComps(Lcom/squareup/payment/Order;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->discounts(Ljava/util/List;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object p1

    .line 175
    invoke-virtual {p1, v1}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->is_section_header_row(Ljava/lang/Boolean;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object p1

    .line 176
    invoke-virtual {p1}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->build()Lcom/squareup/comms/protos/seller/DisplayItem;

    move-result-object p1

    return-object p1
.end method

.method private final getDiscountOrderItem(Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;)Lcom/squareup/checkout/CartItem;
    .locals 2

    .line 363
    iget-object v0, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasExactlyOneDiscount()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;->ADD:Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;

    if-ne p1, v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/ui/cart/BuyerCartFormatter;->getFirstDiscountName()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 364
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/transaction/R$string;->cart_discounts:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 367
    :goto_0
    new-instance v0, Lcom/squareup/checkout/CartItem$Builder;

    invoke-direct {v0}, Lcom/squareup/checkout/CartItem$Builder;-><init>()V

    .line 368
    invoke-virtual {v0, p1}, Lcom/squareup/checkout/CartItem$Builder;->itemName(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p1

    .line 369
    new-instance v0, Lcom/squareup/protos/client/IdPair$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/IdPair$Builder;-><init>()V

    sget-object v1, Lcom/squareup/ui/cart/BuyerCartFormatter;->DISCOUNT_CLIENT_ID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/IdPair$Builder;->client_id(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/protos/client/IdPair$Builder;->build()Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/checkout/CartItem$Builder;->idPair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p1

    .line 370
    iget-object v0, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    const-string/jumbo v1, "transaction.order"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getNegativeAllDiscounts()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/checkout/CartItem$Builder;->variablePrice(Lcom/squareup/protos/common/Money;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p1

    .line 371
    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object p1

    const-string v0, "CartItem.Builder()\n     \u2026scounts)\n        .build()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final getDiscountRow(Lcom/squareup/payment/Order;)Lcom/squareup/comms/protos/seller/DisplayItem;
    .locals 5

    .line 149
    iget-object v0, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    invoke-virtual {p1}, Lcom/squareup/payment/Order;->getNegativeAllNonCompDiscounts()Lcom/squareup/protos/common/Money;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-static {v0, v1, v2, v3, v2}, Lcom/squareup/quantity/PerUnitFormatter;->format$default(Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/protos/common/Money;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 150
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 152
    new-instance v1, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    invoke-direct {v1}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;-><init>()V

    .line 153
    iget-object v3, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/transaction/R$string;->cart_discounts:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->name(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v1

    .line 154
    invoke-virtual {v1, v0}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->price(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v0

    .line 155
    invoke-virtual {v0, v2}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->description(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v0

    .line 156
    sget-object v1, Lcom/squareup/ui/cart/BuyerCartFormatter;->DISCOUNT_CLIENT_ID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->client_id(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v0

    .line 157
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    const-string v2, "emptyList()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->taxes(Ljava/util/List;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v0

    const/4 v1, 0x1

    .line 158
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->is_discount_row(Ljava/lang/Boolean;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v0

    .line 159
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->quantity(Ljava/lang/Integer;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v0

    .line 160
    invoke-direct {p0, p1}, Lcom/squareup/ui/cart/BuyerCartFormatter;->getDisplayDiscounts(Lcom/squareup/payment/Order;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->discounts(Ljava/util/List;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 161
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->is_section_header_row(Ljava/lang/Boolean;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object p1

    .line 162
    invoke-virtual {p1}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->build()Lcom/squareup/comms/protos/seller/DisplayItem;

    move-result-object p1

    return-object p1
.end method

.method private final getDisplayComp(Lcom/squareup/checkout/Discount;)Lcom/squareup/comms/protos/seller/Discount;
    .locals 8

    .line 580
    iget-object v0, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/checkout/Discount;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order;->getCompAmountFromDiscount(Ljava/lang/String;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/money/MoneyMath;->negate(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 581
    sget-object v1, Lkotlin/jvm/internal/StringCompanionObject;->INSTANCE:Lkotlin/jvm/internal/StringCompanionObject;

    const/4 v1, 0x2

    new-array v2, v1, [Ljava/lang/Object;

    .line 583
    iget-object v3, p1, Lcom/squareup/checkout/Discount;->name:Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    .line 584
    iget-object v3, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    const/4 v4, 0x0

    invoke-static {v3, v0, v4, v1, v4}, Lcom/squareup/quantity/PerUnitFormatter;->format$default(Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/protos/common/Money;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    aput-object v0, v2, v1

    .line 581
    array-length v0, v2

    invoke-static {v2, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    const-string v1, "%s (%s)"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v0, "java.lang.String.format(format, *args)"

    invoke-static {v4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 586
    new-instance v0, Lcom/squareup/comms/protos/seller/Discount;

    iget-object v3, p1, Lcom/squareup/checkout/Discount;->id:Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v2, v0

    invoke-direct/range {v2 .. v7}, Lcom/squareup/comms/protos/seller/Discount;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method

.method private final getDisplayCompReason(Lcom/squareup/checkout/Discount;)Lcom/squareup/comms/protos/seller/Discount;
    .locals 7

    .line 572
    iget-object v0, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/checkout/R$string;->comped_with_reason:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 573
    iget-object v1, p1, Lcom/squareup/checkout/Discount;->name:Ljava/lang/String;

    check-cast v1, Ljava/lang/CharSequence;

    const-string v2, "reason"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 574
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 575
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 576
    new-instance v0, Lcom/squareup/comms/protos/seller/Discount;

    iget-object v2, p1, Lcom/squareup/checkout/Discount;->id:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/comms/protos/seller/Discount;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method

.method private final getDisplayComps(Lcom/squareup/payment/Order;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/Order;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/comms/protos/seller/Discount;",
            ">;"
        }
    .end annotation

    .line 252
    invoke-virtual {p1}, Lcom/squareup/payment/Order;->getAllAppliedDiscountsInDisplayOrder()Ljava/util/Map;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 631
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 632
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/checkout/Discount;

    .line 253
    invoke-virtual {v2}, Lcom/squareup/checkout/Discount;->isComp()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 633
    :cond_1
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 634
    new-instance p1, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {p1, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p1, Ljava/util/Collection;

    .line 635
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 636
    check-cast v1, Lcom/squareup/checkout/Discount;

    const-string v2, "discount"

    .line 254
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/squareup/ui/cart/BuyerCartFormatter;->getDisplayComp(Lcom/squareup/checkout/Discount;)Lcom/squareup/comms/protos/seller/Discount;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 637
    :cond_2
    check-cast p1, Ljava/util/List;

    return-object p1
.end method

.method private final getDisplayDiscount(Lcom/squareup/checkout/Discount;)Lcom/squareup/comms/protos/seller/Discount;
    .locals 8

    .line 563
    iget-object v0, p1, Lcom/squareup/checkout/Discount;->percentage:Lcom/squareup/util/Percentage;

    const/4 v1, 0x2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->percentageFormatter:Lcom/squareup/text/Formatter;

    iget-object v2, p1, Lcom/squareup/checkout/Discount;->percentage:Lcom/squareup/util/Percentage;

    invoke-interface {v0, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 564
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v2, p1, Lcom/squareup/checkout/Discount;->amount:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x0

    invoke-static {v0, v2, v3, v1, v3}, Lcom/squareup/quantity/PerUnitFormatter;->format$default(Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/protos/common/Money;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 567
    :goto_0
    sget-object v2, Lkotlin/jvm/internal/StringCompanionObject;->INSTANCE:Lkotlin/jvm/internal/StringCompanionObject;

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p1, Lcom/squareup/checkout/Discount;->name:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v2

    array-length v0, v1

    invoke-static {v1, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    const-string v1, "%s (%s)"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v0, "java.lang.String.format(format, *args)"

    invoke-static {v4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 568
    new-instance v0, Lcom/squareup/comms/protos/seller/Discount;

    iget-object v3, p1, Lcom/squareup/checkout/Discount;->id:Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v2, v0

    invoke-direct/range {v2 .. v7}, Lcom/squareup/comms/protos/seller/Discount;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method

.method private final getDisplayDiscounts(Lcom/squareup/payment/Order;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/Order;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/comms/protos/seller/Discount;",
            ">;"
        }
    .end annotation

    .line 246
    invoke-virtual {p1}, Lcom/squareup/payment/Order;->getAllAppliedDiscountsInDisplayOrder()Ljava/util/Map;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 624
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 625
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/checkout/Discount;

    .line 247
    invoke-virtual {v2}, Lcom/squareup/checkout/Discount;->isComp()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 626
    :cond_1
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 627
    new-instance p1, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {p1, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p1, Ljava/util/Collection;

    .line 628
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 629
    check-cast v1, Lcom/squareup/checkout/Discount;

    const-string v2, "discount"

    .line 248
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/squareup/ui/cart/BuyerCartFormatter;->getDisplayDiscount(Lcom/squareup/checkout/Discount;)Lcom/squareup/comms/protos/seller/Discount;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 630
    :cond_2
    check-cast p1, Ljava/util/List;

    return-object p1
.end method

.method private final getDisplayQuantity(Lcom/squareup/checkout/CartItem;)I
    .locals 2

    .line 144
    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    const-string v1, "selectedVariation"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/checkout/OrderVariation;->isUnitPriced()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    .line 145
    :cond_0
    iget-object p1, p1, Lcom/squareup/checkout/CartItem;->quantity:Ljava/math/BigDecimal;

    invoke-virtual {p1}, Ljava/math/BigDecimal;->intValueExact()I

    move-result p1

    :goto_0
    return p1
.end method

.method private final getFirstDiscountName()Ljava/lang/String;
    .locals 3

    .line 258
    iget-object v0, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    const-string/jumbo v1, "transaction.order"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getNotVoidedItems()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/CartItem;

    .line 259
    iget-object v1, v1, Lcom/squareup/checkout/CartItem;->appliedDiscounts:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkout/Discount;

    .line 260
    iget-object v0, v0, Lcom/squareup/checkout/Discount;->name:Ljava/lang/String;

    return-object v0

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method private final getItemsRows(Lcom/squareup/payment/Order;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/Order;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/comms/protos/seller/DisplayItem;",
            ">;"
        }
    .end annotation

    .line 117
    invoke-virtual {p1}, Lcom/squareup/payment/Order;->getNotVoidedItems()Ljava/util/List;

    move-result-object v0

    const-string v1, "order.notVoidedItems"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 118
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->reversed(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 612
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 613
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const-string v3, "item"

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v4, v2

    check-cast v4, Lcom/squareup/checkout/CartItem;

    .line 119
    invoke-static {v4, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/squareup/checkout/CartItem;->isInteresting()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 614
    :cond_1
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 615
    new-instance v0, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v1, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 616
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 617
    check-cast v2, Lcom/squareup/checkout/CartItem;

    .line 122
    iget-object v4, v2, Lcom/squareup/checkout/CartItem;->itemName:Ljava/lang/String;

    check-cast v4, Ljava/lang/CharSequence;

    invoke-static {v4}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 123
    iget-object v4, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->res:Lcom/squareup/util/Res;

    sget v5, Lcom/squareup/common/strings/R$string;->default_itemization_name:I

    invoke-interface {v4, v5}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    .line 125
    :cond_2
    iget-object v4, v2, Lcom/squareup/checkout/CartItem;->itemName:Ljava/lang/String;

    .line 128
    :goto_2
    new-instance v5, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    invoke-direct {v5}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;-><init>()V

    .line 129
    invoke-virtual {v5, v4}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->name(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v4

    .line 130
    iget-object v5, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    invoke-virtual {v2}, Lcom/squareup/checkout/CartItem;->total()Lcom/squareup/protos/common/Money;

    move-result-object v6

    const/4 v7, 0x2

    const/4 v8, 0x0

    invoke-static {v5, v6, v8, v7, v8}, Lcom/squareup/quantity/PerUnitFormatter;->format$default(Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/protos/common/Money;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->price(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v4

    .line 131
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v2}, Lcom/squareup/ui/cart/BuyerCartFormatter;->getVariationAndModifiers(Lcom/squareup/checkout/CartItem;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->description(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v4

    .line 132
    iget-object v5, v2, Lcom/squareup/checkout/CartItem;->idPair:Lcom/squareup/protos/client/IdPair;

    iget-object v5, v5, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->client_id(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v4

    .line 133
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v5

    const-string v6, "emptyList()"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->taxes(Ljava/util/List;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v4

    const/4 v5, 0x0

    .line 134
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->is_discount_row(Ljava/lang/Boolean;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v4

    .line 135
    invoke-direct {p0, v2}, Lcom/squareup/ui/cart/BuyerCartFormatter;->getDisplayQuantity(Lcom/squareup/checkout/CartItem;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->quantity(Ljava/lang/Integer;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v4

    .line 136
    invoke-direct {p0, v2}, Lcom/squareup/ui/cart/BuyerCartFormatter;->getPerUnitPriceAndQuantity(Lcom/squareup/checkout/CartItem;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->per_unit_price_and_quantity(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v4

    .line 137
    invoke-direct {p0, p1}, Lcom/squareup/ui/cart/BuyerCartFormatter;->getCartLevelDiscountIds(Lcom/squareup/payment/Order;)Ljava/util/Set;

    move-result-object v6

    invoke-direct {p0, v2, v6}, Lcom/squareup/ui/cart/BuyerCartFormatter;->getPerItemProtoDiscounts(Lcom/squareup/checkout/CartItem;Ljava/util/Set;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v4, v2}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->discounts(Ljava/util/List;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v2

    .line 138
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->is_section_header_row(Ljava/lang/Boolean;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v2

    .line 139
    invoke-virtual {v2}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->build()Lcom/squareup/comms/protos/seller/DisplayItem;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 618
    :cond_3
    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method private final getPerItemProtoDiscounts(Lcom/squareup/checkout/CartItem;Ljava/util/Set;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkout/CartItem;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/comms/protos/seller/Discount;",
            ">;"
        }
    .end annotation

    .line 526
    iget-object p1, p1, Lcom/squareup/checkout/CartItem;->appliedDiscounts:Ljava/util/Map;

    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 653
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 654
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/checkout/Discount;

    .line 527
    invoke-virtual {v2}, Lcom/squareup/checkout/Discount;->canBeAppliedPerItem()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v2, v2, Lcom/squareup/checkout/Discount;->id:Ljava/lang/String;

    invoke-interface {p2, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    if-eqz v2, :cond_0

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 655
    :cond_2
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 656
    new-instance p1, Ljava/util/ArrayList;

    const/16 p2, 0xa

    invoke-static {v0, p2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result p2

    invoke-direct {p1, p2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p1, Ljava/util/Collection;

    .line 657
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_2
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 658
    check-cast v0, Lcom/squareup/checkout/Discount;

    .line 530
    invoke-virtual {v0}, Lcom/squareup/checkout/Discount;->isComp()Z

    move-result v1

    const-string v2, "discount"

    if-eqz v1, :cond_3

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/squareup/ui/cart/BuyerCartFormatter;->getDisplayCompReason(Lcom/squareup/checkout/Discount;)Lcom/squareup/comms/protos/seller/Discount;

    move-result-object v0

    goto :goto_3

    .line 531
    :cond_3
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/squareup/ui/cart/BuyerCartFormatter;->getDisplayDiscount(Lcom/squareup/checkout/Discount;)Lcom/squareup/comms/protos/seller/Discount;

    move-result-object v0

    .line 532
    :goto_3
    invoke-interface {p1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 659
    :cond_4
    check-cast p1, Ljava/util/List;

    return-object p1
.end method

.method private final getPerUnitPriceAndQuantity(Lcom/squareup/checkout/CartItem;)Ljava/lang/String;
    .locals 4

    .line 542
    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    const-string v1, "selectedVariation"

    .line 544
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/checkout/OrderVariation;->isUnitPriced()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 547
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    .line 548
    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->unitPriceOrNull()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/quantity/PerUnitFormatter;->money(Lcom/squareup/protos/common/Money;)Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object v1

    .line 549
    invoke-virtual {v1}, Lcom/squareup/quantity/PerUnitFormatter;->inParentheses()Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object v1

    .line 550
    iget-object v2, p1, Lcom/squareup/checkout/CartItem;->quantity:Ljava/math/BigDecimal;

    invoke-virtual {v0}, Lcom/squareup/checkout/OrderVariation;->getQuantityPrecision()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/squareup/quantity/PerUnitFormatter;->quantityAndPrecision(Ljava/math/BigDecimal;I)Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object v1

    .line 551
    invoke-virtual {v0}, Lcom/squareup/checkout/OrderVariation;->getUnitAbbreviation()Ljava/lang/String;

    move-result-object v0

    const-string v2, "selectedVariation.unitAbbreviation"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Lcom/squareup/quantity/PerUnitFormatter;->unit(Ljava/lang/String;)Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object v0

    .line 552
    invoke-virtual {v0}, Lcom/squareup/quantity/PerUnitFormatter;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 553
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 554
    iget-object p1, p1, Lcom/squareup/checkout/CartItem;->quantityEntryType:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    iget-object v1, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->res:Lcom/squareup/util/Res;

    invoke-static {p1, v1}, Lcom/squareup/quantity/ItemQuantities;->toQuantityEntrySuffix(Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;Lcom/squareup/util/Res;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 556
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private final getReturnCartLevelDiscountIds(Lcom/squareup/checkout/ReturnCart;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkout/ReturnCart;",
            ")",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 223
    invoke-virtual {p1}, Lcom/squareup/checkout/ReturnCart;->getReturnDiscountsAsDiscounts()Ljava/util/List;

    move-result-object v0

    .line 224
    invoke-virtual {p1}, Lcom/squareup/checkout/ReturnCart;->getReturnItemsAsCartItems()Ljava/util/List;

    move-result-object p1

    .line 222
    invoke-direct {p0, v0, p1}, Lcom/squareup/ui/cart/BuyerCartFormatter;->getCartLevelDiscounts(Ljava/util/List;Ljava/util/List;)Ljava/util/Set;

    move-result-object p1

    return-object p1
.end method

.method private final getReturnItems(Lcom/squareup/checkout/ReturnCart;Ljava/util/Set;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkout/ReturnCart;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/comms/protos/seller/DisplayItem;",
            ">;"
        }
    .end annotation

    .line 271
    invoke-virtual {p1}, Lcom/squareup/checkout/ReturnCart;->getReturnItemsAsCartItems()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 638
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 639
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/checkout/CartItem;

    .line 272
    invoke-virtual {v2}, Lcom/squareup/checkout/CartItem;->isInteresting()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 640
    :cond_1
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 641
    new-instance p1, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {p1, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p1, Ljava/util/Collection;

    .line 642
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 643
    check-cast v1, Lcom/squareup/checkout/CartItem;

    .line 275
    iget-object v2, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    invoke-virtual {v1}, Lcom/squareup/checkout/CartItem;->total()Lcom/squareup/protos/common/Money;

    move-result-object v3

    invoke-static {v3}, Lcom/squareup/money/MoneyMath;->negate(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v3

    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-static {v2, v3, v5, v4, v5}, Lcom/squareup/quantity/PerUnitFormatter;->format$default(Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/protos/common/Money;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 276
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 278
    iget-object v3, v1, Lcom/squareup/checkout/CartItem;->itemName:Ljava/lang/String;

    check-cast v3, Ljava/lang/CharSequence;

    invoke-static {v3}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 279
    iget-object v3, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/common/strings/R$string;->default_itemization_name:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    .line 280
    :cond_2
    iget-object v3, v1, Lcom/squareup/checkout/CartItem;->itemName:Ljava/lang/String;

    .line 283
    :goto_2
    invoke-direct {p0, v1, p2}, Lcom/squareup/ui/cart/BuyerCartFormatter;->getPerItemProtoDiscounts(Lcom/squareup/checkout/CartItem;Ljava/util/Set;)Ljava/util/List;

    move-result-object v4

    .line 285
    new-instance v5, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    invoke-direct {v5}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;-><init>()V

    .line 286
    invoke-virtual {v5, v3}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->name(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v3

    .line 287
    invoke-virtual {v3, v2}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->price(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v2

    .line 288
    invoke-direct {p0, v1}, Lcom/squareup/ui/cart/BuyerCartFormatter;->getVariationAndModifiers(Lcom/squareup/checkout/CartItem;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->description(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v2

    .line 289
    iget-object v3, v1, Lcom/squareup/checkout/CartItem;->idPair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, v3, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->client_id(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v2

    .line 290
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v3

    const-string v5, "emptyList()"

    invoke-static {v3, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->taxes(Ljava/util/List;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v2

    const/4 v3, 0x0

    .line 291
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->is_discount_row(Ljava/lang/Boolean;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v2

    .line 292
    invoke-direct {p0, v1}, Lcom/squareup/ui/cart/BuyerCartFormatter;->getDisplayQuantity(Lcom/squareup/checkout/CartItem;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->quantity(Ljava/lang/Integer;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v2

    .line 293
    invoke-direct {p0, v1}, Lcom/squareup/ui/cart/BuyerCartFormatter;->getPerUnitPriceAndQuantity(Lcom/squareup/checkout/CartItem;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->per_unit_price_and_quantity(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v1

    .line 294
    invoke-virtual {v1, v4}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->discounts(Ljava/util/List;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v1

    .line 295
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->is_section_header_row(Ljava/lang/Boolean;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v1

    .line 296
    invoke-virtual {v1}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->build()Lcom/squareup/comms/protos/seller/DisplayItem;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 644
    :cond_3
    check-cast p1, Ljava/util/List;

    return-object p1
.end method

.method private final getTaxOrderItem()Lcom/squareup/checkout/CartItem;
    .locals 3

    .line 203
    new-instance v0, Lcom/squareup/checkout/CartItem$Builder;

    invoke-direct {v0}, Lcom/squareup/checkout/CartItem$Builder;-><init>()V

    .line 204
    iget-object v1, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/transaction/R$string;->cart_taxes_title:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/checkout/CartItem$Builder;->itemName(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v0

    .line 205
    new-instance v1, Lcom/squareup/protos/client/IdPair$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/IdPair$Builder;-><init>()V

    sget-object v2, Lcom/squareup/ui/cart/BuyerCartFormatter;->TAX_CLIENT_ID:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/IdPair$Builder;->client_id(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair$Builder;->build()Lcom/squareup/protos/client/IdPair;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/checkout/CartItem$Builder;->idPair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v0

    .line 206
    iget-object v1, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getAllTaxes()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/checkout/CartItem$Builder;->variablePrice(Lcom/squareup/protos/common/Money;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v0

    .line 207
    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object v0

    return-object v0
.end method

.method private final getTaxRow(Lcom/squareup/payment/Order;)Lcom/squareup/comms/protos/seller/DisplayItem;
    .locals 5

    .line 180
    iget-object v0, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    invoke-virtual {p1}, Lcom/squareup/payment/Order;->getAdditionalTaxes()Lcom/squareup/protos/common/Money;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-static {v0, v1, v2, v3, v2}, Lcom/squareup/quantity/PerUnitFormatter;->format$default(Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/protos/common/Money;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 181
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 182
    invoke-direct {p0, p1}, Lcom/squareup/ui/cart/BuyerCartFormatter;->getCalculatedAdditiveTaxes(Lcom/squareup/payment/Order;)Ljava/util/List;

    move-result-object p1

    .line 184
    iget-object v1, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->res:Lcom/squareup/util/Res;

    .line 186
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_0

    sget v3, Lcom/squareup/transaction/R$string;->cart_taxes_title:I

    goto :goto_0

    .line 187
    :cond_0
    sget v3, Lcom/squareup/transaction/R$string;->cart_tax_row:I

    .line 184
    :goto_0
    invoke-interface {v1, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 190
    new-instance v3, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    invoke-direct {v3}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;-><init>()V

    .line 191
    invoke-virtual {v3, v1}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->name(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v1

    .line 192
    invoke-virtual {v1, v0}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->price(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v0

    .line 193
    invoke-virtual {v0, v2}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->description(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v0

    .line 194
    sget-object v1, Lcom/squareup/ui/cart/BuyerCartFormatter;->TAX_CLIENT_ID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->client_id(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object v0

    .line 195
    invoke-virtual {v0, p1}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->taxes(Ljava/util/List;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 196
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->is_discount_row(Ljava/lang/Boolean;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object p1

    .line 197
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->quantity(Ljava/lang/Integer;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object p1

    .line 198
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    const-string v2, "emptyList()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->discounts(Ljava/util/List;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object p1

    .line 199
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->is_section_header_row(Ljava/lang/Boolean;)Lcom/squareup/comms/protos/seller/DisplayItem$Builder;

    move-result-object p1

    .line 200
    invoke-virtual {p1}, Lcom/squareup/comms/protos/seller/DisplayItem$Builder;->build()Lcom/squareup/comms/protos/seller/DisplayItem;

    move-result-object p1

    return-object p1
.end method

.method private final getVariationAndModifiers(Lcom/squareup/checkout/CartItem;)Ljava/lang/String;
    .locals 17

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    .line 463
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 465
    new-instance v3, Lcom/squareup/ui/cart/BuyerCartFormatter$getVariationAndModifiers$1;

    invoke-direct {v3, v2}, Lcom/squareup/ui/cart/BuyerCartFormatter$getVariationAndModifiers$1;-><init>(Ljava/lang/StringBuilder;)V

    .line 476
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/checkout/CartItem;->shouldShowVariationName()Z

    move-result v4

    const/4 v5, 0x0

    const-string v6, "item.selectedVariation"

    if-eqz v4, :cond_0

    .line 477
    iget-object v4, v1, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    invoke-static {v4, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/squareup/checkout/OrderVariation;->getDisplayName()Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v3, v4, v5}, Lcom/squareup/ui/cart/BuyerCartFormatter$getVariationAndModifiers$1;->invoke(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 481
    :cond_0
    new-instance v4, Lcom/squareup/protos/common/Money;

    const-wide/16 v7, 0x0

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/checkout/CartItem;->total()Lcom/squareup/protos/common/Money;

    move-result-object v10

    iget-object v10, v10, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-direct {v4, v9, v10}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    .line 484
    iget-object v9, v1, Lcom/squareup/checkout/CartItem;->selectedModifiers:Ljava/util/SortedMap;

    invoke-interface {v9}, Ljava/util/SortedMap;->values()Ljava/util/Collection;

    move-result-object v9

    const-string v10, "item.selectedModifiers.values"

    invoke-static {v9, v10}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v9, Ljava/lang/Iterable;

    .line 649
    invoke-interface {v9}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    const/4 v11, 0x1

    const-string v12, "item.selectedVariation.unitAbbreviation"

    if-eqz v10, :cond_5

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/util/SortedMap;

    .line 485
    invoke-interface {v10}, Ljava/util/SortedMap;->values()Ljava/util/Collection;

    move-result-object v10

    const-string v13, "modifierList.values"

    invoke-static {v10, v13}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v10, Ljava/lang/Iterable;

    .line 650
    invoke-interface {v10}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_2
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_1

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/squareup/checkout/OrderModifier;

    .line 486
    invoke-virtual {v13}, Lcom/squareup/checkout/OrderModifier;->isFreeModifier()Z

    move-result v14

    xor-int/2addr v14, v11

    .line 487
    invoke-virtual {v13}, Lcom/squareup/checkout/OrderModifier;->getBasePriceTimesModifierQuantityOrNull()Lcom/squareup/protos/common/Money;

    move-result-object v15

    .line 490
    invoke-virtual {v13}, Lcom/squareup/checkout/OrderModifier;->getHideFromCustomer()Z

    move-result v16

    if-nez v16, :cond_4

    .line 491
    move-object/from16 v16, v5

    check-cast v16, Ljava/lang/CharSequence;

    if-eqz v14, :cond_3

    .line 493
    iget-object v14, v0, Lcom/squareup/ui/cart/BuyerCartFormatter;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    .line 494
    invoke-virtual {v14, v15}, Lcom/squareup/quantity/PerUnitFormatter;->money(Lcom/squareup/protos/common/Money;)Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object v14

    .line 495
    iget-object v15, v1, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    invoke-static {v15, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v15}, Lcom/squareup/checkout/OrderVariation;->getUnitAbbreviation()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15, v12}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v14, v15}, Lcom/squareup/quantity/PerUnitFormatter;->unit(Ljava/lang/String;)Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object v14

    .line 496
    invoke-virtual {v14}, Lcom/squareup/quantity/PerUnitFormatter;->inParentheses()Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object v14

    .line 497
    invoke-virtual {v14}, Lcom/squareup/quantity/PerUnitFormatter;->format()Ljava/lang/CharSequence;

    move-result-object v16

    :cond_3
    move-object/from16 v14, v16

    .line 500
    iget-object v15, v0, Lcom/squareup/ui/cart/BuyerCartFormatter;->res:Lcom/squareup/util/Res;

    invoke-virtual {v13, v15}, Lcom/squareup/checkout/OrderModifier;->getDisplayName(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v13

    check-cast v13, Ljava/lang/CharSequence;

    invoke-virtual {v3, v13, v14}, Lcom/squareup/ui/cart/BuyerCartFormatter$getVariationAndModifiers$1;->invoke(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_4
    if-eqz v14, :cond_2

    .line 503
    invoke-static {v4, v15}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v4

    const-string v13, "MoneyMath.sum(hiddenModifierCost, modifierPrice)"

    invoke-static {v4, v13}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 509
    :cond_5
    iget-object v9, v4, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    cmp-long v13, v9, v7

    if-lez v13, :cond_6

    .line 510
    iget-object v7, v0, Lcom/squareup/ui/cart/BuyerCartFormatter;->res:Lcom/squareup/util/Res;

    sget v8, Lcom/squareup/transaction/R$string;->additional:I

    invoke-interface {v7, v8}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 511
    iget-object v8, v0, Lcom/squareup/ui/cart/BuyerCartFormatter;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    .line 512
    invoke-virtual {v8, v4}, Lcom/squareup/quantity/PerUnitFormatter;->money(Lcom/squareup/protos/common/Money;)Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object v4

    .line 513
    iget-object v1, v1, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    invoke-static {v1, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/squareup/checkout/OrderVariation;->getUnitAbbreviation()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v12}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Lcom/squareup/quantity/PerUnitFormatter;->unit(Ljava/lang/String;)Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object v1

    .line 514
    invoke-virtual {v1}, Lcom/squareup/quantity/PerUnitFormatter;->inParentheses()Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object v1

    .line 515
    invoke-virtual {v1}, Lcom/squareup/quantity/PerUnitFormatter;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 516
    check-cast v7, Ljava/lang/CharSequence;

    invoke-virtual {v3, v7, v1}, Lcom/squareup/ui/cart/BuyerCartFormatter$getVariationAndModifiers$1;->invoke(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 519
    :cond_6
    move-object v1, v2

    check-cast v1, Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-lez v1, :cond_7

    goto :goto_1

    :cond_7
    const/4 v11, 0x0

    :goto_1
    if-eqz v11, :cond_8

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    :cond_8
    return-object v5
.end method


# virtual methods
.method public final getCartChangedBanner(Lcom/squareup/payment/OrderEntryEvents$CartChanged;)Lcom/squareup/comms/protos/seller/DisplayBanner;
    .locals 6

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 301
    iget-object v1, p1, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->status:Lcom/squareup/payment/OrderEntryEvents$ItemStatus;

    goto :goto_0

    :cond_0
    move-object v1, v0

    :goto_0
    if-nez v1, :cond_1

    return-object v0

    .line 303
    :cond_1
    iget-object p1, p1, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->status:Lcom/squareup/payment/OrderEntryEvents$ItemStatus;

    .line 310
    iget-object v1, p1, Lcom/squareup/payment/OrderEntryEvents$ItemStatus;->itemType:Lcom/squareup/payment/OrderEntryEvents$ItemType;

    sget-object v2, Lcom/squareup/payment/OrderEntryEvents$ItemType;->DISCOUNT:Lcom/squareup/payment/OrderEntryEvents$ItemType;

    const/4 v3, 0x1

    if-ne v1, v2, :cond_2

    .line 311
    iget-object v1, p1, Lcom/squareup/payment/OrderEntryEvents$ItemStatus;->changeType:Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;

    const-string v2, "status.changeType"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/squareup/ui/cart/BuyerCartFormatter;->getDiscountOrderItem(Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;)Lcom/squareup/checkout/CartItem;

    move-result-object v1

    :goto_1
    const/4 v2, 0x1

    goto :goto_4

    .line 315
    :cond_2
    iget-object v1, p1, Lcom/squareup/payment/OrderEntryEvents$ItemStatus;->itemType:Lcom/squareup/payment/OrderEntryEvents$ItemType;

    sget-object v2, Lcom/squareup/payment/OrderEntryEvents$ItemType;->TAX:Lcom/squareup/payment/OrderEntryEvents$ItemType;

    if-ne v1, v2, :cond_3

    .line 316
    invoke-direct {p0}, Lcom/squareup/ui/cart/BuyerCartFormatter;->getTaxOrderItem()Lcom/squareup/checkout/CartItem;

    move-result-object v1

    const-string v2, "getTaxOrderItem()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 320
    :cond_3
    iget-object v1, p1, Lcom/squareup/payment/OrderEntryEvents$ItemStatus;->item:Lcom/squareup/checkout/CartItem;

    iget-object v1, v1, Lcom/squareup/checkout/CartItem;->itemName:Ljava/lang/String;

    check-cast v1, Ljava/lang/CharSequence;

    if-eqz v1, :cond_5

    invoke-static {v1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    goto :goto_2

    :cond_4
    const/4 v1, 0x0

    goto :goto_3

    :cond_5
    :goto_2
    const/4 v1, 0x1

    :goto_3
    if-eqz v1, :cond_6

    .line 321
    iget-object v1, p1, Lcom/squareup/payment/OrderEntryEvents$ItemStatus;->item:Lcom/squareup/checkout/CartItem;

    invoke-virtual {v1}, Lcom/squareup/checkout/CartItem;->buildUpon()Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v1

    .line 322
    iget-object v2, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/common/strings/R$string;->default_itemization_name:I

    invoke-interface {v2, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/checkout/CartItem$Builder;->itemName(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v1

    .line 323
    invoke-virtual {v1}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object v1

    const-string v2, "status.item.buildUpon()\n\u2026me))\n            .build()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 325
    iget-object v2, v1, Lcom/squareup/checkout/CartItem;->quantity:Ljava/math/BigDecimal;

    invoke-virtual {v2}, Ljava/math/BigDecimal;->intValueExact()I

    move-result v2

    goto :goto_4

    .line 329
    :cond_6
    iget-object v1, p1, Lcom/squareup/payment/OrderEntryEvents$ItemStatus;->item:Lcom/squareup/checkout/CartItem;

    const-string v2, "status.item"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 330
    invoke-direct {p0, v1}, Lcom/squareup/ui/cart/BuyerCartFormatter;->getDisplayQuantity(Lcom/squareup/checkout/CartItem;)I

    move-result v2

    .line 334
    :goto_4
    iget-object p1, p1, Lcom/squareup/payment/OrderEntryEvents$ItemStatus;->changeType:Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;

    if-nez p1, :cond_7

    goto :goto_5

    :cond_7
    sget-object v4, Lcom/squareup/ui/cart/BuyerCartFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;->ordinal()I

    move-result p1

    aget p1, v4, p1

    const-string v4, "name"

    const/4 v5, 0x2

    if-eq p1, v3, :cond_a

    if-eq p1, v5, :cond_9

    const/4 v3, 0x3

    if-eq p1, v3, :cond_8

    :goto_5
    return-object v0

    .line 342
    :cond_8
    iget-object p1, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/transaction/R$string;->buyer_banner_item_edited:I

    invoke-interface {p1, v3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 343
    iget-object v3, v1, Lcom/squareup/checkout/CartItem;->itemName:Ljava/lang/String;

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {p1, v4, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 344
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 345
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_6

    .line 340
    :cond_9
    iget-object p1, v1, Lcom/squareup/checkout/CartItem;->itemName:Ljava/lang/String;

    goto :goto_6

    .line 335
    :cond_a
    iget-object p1, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/transaction/R$string;->buyer_banner_item_removed:I

    invoke-interface {p1, v3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 336
    iget-object v3, v1, Lcom/squareup/checkout/CartItem;->itemName:Ljava/lang/String;

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {p1, v4, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 337
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 338
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 350
    :goto_6
    iget-object v3, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    invoke-virtual {v1}, Lcom/squareup/checkout/CartItem;->total()Lcom/squareup/protos/common/Money;

    move-result-object v4

    invoke-static {v3, v4, v0, v5, v0}, Lcom/squareup/quantity/PerUnitFormatter;->format$default(Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/protos/common/Money;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 351
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 353
    new-instance v3, Lcom/squareup/comms/protos/seller/DisplayBanner$Builder;

    invoke-direct {v3}, Lcom/squareup/comms/protos/seller/DisplayBanner$Builder;-><init>()V

    .line 354
    invoke-virtual {v3, p1}, Lcom/squareup/comms/protos/seller/DisplayBanner$Builder;->name(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayBanner$Builder;

    move-result-object p1

    .line 355
    invoke-virtual {p1, v0}, Lcom/squareup/comms/protos/seller/DisplayBanner$Builder;->price(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayBanner$Builder;

    move-result-object p1

    .line 356
    iget-object v0, v1, Lcom/squareup/checkout/CartItem;->idPair:Lcom/squareup/protos/client/IdPair;

    iget-object v0, v0, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/comms/protos/seller/DisplayBanner$Builder;->item_client_id(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayBanner$Builder;

    move-result-object p1

    .line 357
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/comms/protos/seller/DisplayBanner$Builder;->quantity(Ljava/lang/Integer;)Lcom/squareup/comms/protos/seller/DisplayBanner$Builder;

    move-result-object p1

    .line 358
    invoke-virtual {p1}, Lcom/squareup/comms/protos/seller/DisplayBanner$Builder;->build()Lcom/squareup/comms/protos/seller/DisplayBanner;

    move-result-object p1

    return-object p1
.end method

.method public getDisplayCart(Ljava/util/List;Lcom/squareup/payment/OrderEntryEvents$CartChanged;Z)Lcom/squareup/comms/protos/seller/DisplayCart;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/comms/protos/seller/DisplayItem;",
            ">;",
            "Lcom/squareup/payment/OrderEntryEvents$CartChanged;",
            "Z)",
            "Lcom/squareup/comms/protos/seller/DisplayCart;"
        }
    .end annotation

    const-string v0, "displayItems"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    invoke-virtual {p0, p2}, Lcom/squareup/ui/cart/BuyerCartFormatter;->getCartChangedBanner(Lcom/squareup/payment/OrderEntryEvents$CartChanged;)Lcom/squareup/comms/protos/seller/DisplayBanner;

    move-result-object p2

    .line 66
    new-instance v0, Lcom/squareup/comms/protos/seller/DisplayCart$Builder;

    invoke-direct {v0}, Lcom/squareup/comms/protos/seller/DisplayCart$Builder;-><init>()V

    .line 67
    invoke-virtual {v0, p1}, Lcom/squareup/comms/protos/seller/DisplayCart$Builder;->items(Ljava/util/List;)Lcom/squareup/comms/protos/seller/DisplayCart$Builder;

    move-result-object p1

    .line 68
    invoke-virtual {p1, p2}, Lcom/squareup/comms/protos/seller/DisplayCart$Builder;->banner(Lcom/squareup/comms/protos/seller/DisplayBanner;)Lcom/squareup/comms/protos/seller/DisplayCart$Builder;

    move-result-object p1

    .line 69
    iget-object p2, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p2}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object p2

    const-string/jumbo v0, "transaction.order"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/squareup/payment/Order;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object p2

    iget-object p2, p2, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p1, p2}, Lcom/squareup/comms/protos/seller/DisplayCart$Builder;->total_amount(Ljava/lang/Long;)Lcom/squareup/comms/protos/seller/DisplayCart$Builder;

    move-result-object p1

    .line 70
    iget-object p2, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

    invoke-interface {p2}, Lcom/squareup/payment/OfflineModeMonitor;->isInOfflineMode()Z

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/comms/protos/seller/DisplayCart$Builder;->offline_mode(Z)Lcom/squareup/comms/protos/seller/DisplayCart$Builder;

    move-result-object p1

    .line 71
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/comms/protos/seller/DisplayCart$Builder;->is_card_still_inserted(Ljava/lang/Boolean;)Lcom/squareup/comms/protos/seller/DisplayCart$Builder;

    move-result-object p1

    .line 72
    iget-object p2, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p2}, Lcom/squareup/payment/Transaction;->hasReturn()Z

    move-result p2

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/comms/protos/seller/DisplayCart$Builder;->has_return(Ljava/lang/Boolean;)Lcom/squareup/comms/protos/seller/DisplayCart$Builder;

    move-result-object p1

    .line 73
    iget-object p2, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->features:Lcom/squareup/settings/server/Features;

    sget-object p3, Lcom/squareup/settings/server/Features$Feature;->BRAN_CART_SCROLL_LOGGING:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p2, p3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p2

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/comms/protos/seller/DisplayCart$Builder;->enable_bran_cart_scroll_logging(Ljava/lang/Boolean;)Lcom/squareup/comms/protos/seller/DisplayCart$Builder;

    move-result-object p1

    .line 74
    invoke-virtual {p1}, Lcom/squareup/comms/protos/seller/DisplayCart$Builder;->build()Lcom/squareup/comms/protos/seller/DisplayCart;

    move-result-object p1

    return-object p1
.end method

.method public getDisplayItems()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/comms/protos/seller/DisplayItem;",
            ">;"
        }
    .end annotation

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    .line 82
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/List;

    .line 84
    move-object v2, v1

    check-cast v2, Ljava/util/Collection;

    const-string v3, "order"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/squareup/ui/cart/BuyerCartFormatter;->getItemsRows(Lcom/squareup/payment/Order;)Ljava/util/List;

    move-result-object v3

    check-cast v3, Ljava/lang/Iterable;

    invoke-static {v2, v3}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    .line 86
    iget-object v3, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->shouldShowDiscountRow()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 87
    invoke-direct {p0, v0}, Lcom/squareup/ui/cart/BuyerCartFormatter;->getDiscountRow(Lcom/squareup/payment/Order;)Lcom/squareup/comms/protos/seller/DisplayItem;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 90
    :cond_0
    iget-object v3, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->shouldShowCompRow()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 91
    invoke-direct {p0, v0}, Lcom/squareup/ui/cart/BuyerCartFormatter;->getCompRow(Lcom/squareup/payment/Order;)Lcom/squareup/comms/protos/seller/DisplayItem;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 94
    :cond_1
    iget-object v3, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->shouldShowPreTaxAutoGratuityRow()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 95
    invoke-direct {p0}, Lcom/squareup/ui/cart/BuyerCartFormatter;->autoGratuityDisplayItem()Lcom/squareup/comms/protos/seller/DisplayItem;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 98
    :cond_2
    iget-object v3, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->shouldShowTaxRow()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 99
    iget-object v3, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->hasInterestingTaxState()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getAdditionalTaxesAmount()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v7, v3, v5

    if-lez v7, :cond_3

    .line 102
    invoke-direct {p0, v0}, Lcom/squareup/ui/cart/BuyerCartFormatter;->getTaxRow(Lcom/squareup/payment/Order;)Lcom/squareup/comms/protos/seller/DisplayItem;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 105
    :cond_3
    iget-object v3, p0, Lcom/squareup/ui/cart/BuyerCartFormatter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->shouldShowPostTaxAutoGratuityRow()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 106
    invoke-direct {p0}, Lcom/squareup/ui/cart/BuyerCartFormatter;->autoGratuityDisplayItem()Lcom/squareup/comms/protos/seller/DisplayItem;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 109
    :cond_4
    invoke-virtual {v0}, Lcom/squareup/payment/Order;->hasReturn()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 110
    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getReturnCart()Lcom/squareup/checkout/ReturnCart;

    move-result-object v0

    if-nez v0, :cond_5

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_5
    const-string v2, "order.returnCart!!"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/squareup/ui/cart/BuyerCartFormatter;->addReturnRows(Ljava/util/List;Lcom/squareup/checkout/ReturnCart;)V

    :cond_6
    return-object v1
.end method
