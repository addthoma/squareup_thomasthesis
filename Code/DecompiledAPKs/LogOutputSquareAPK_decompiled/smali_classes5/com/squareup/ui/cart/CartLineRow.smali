.class public Lcom/squareup/ui/cart/CartLineRow;
.super Landroid/view/View;
.source "CartLineRow.java"


# instance fields
.field private linePaint:Landroid/graphics/Paint;

.field private lineStroke:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 24
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 25
    invoke-direct {p0}, Lcom/squareup/ui/cart/CartLineRow;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 19
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    invoke-direct {p0}, Lcom/squareup/ui/cart/CartLineRow;->init()V

    return-void
.end method

.method private init()V
    .locals 4

    .line 37
    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartLineRow;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 38
    sget v1, Lcom/squareup/marin/R$dimen;->marin_cart_item_gutter:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 39
    sget v2, Lcom/squareup/marin/R$dimen;->marin_gap_multiline_padding:I

    .line 40
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 41
    sget v3, Lcom/squareup/orderentry/R$dimen;->cart_line_height:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, p0, Lcom/squareup/ui/cart/CartLineRow;->lineStroke:I

    .line 42
    iget v3, p0, Lcom/squareup/ui/cart/CartLineRow;->lineStroke:I

    add-int/2addr v3, v2

    add-int/2addr v3, v2

    invoke-virtual {p0, v3}, Lcom/squareup/ui/cart/CartLineRow;->setMinimumHeight(I)V

    .line 43
    invoke-virtual {p0, v1, v2, v1, v2}, Lcom/squareup/ui/cart/CartLineRow;->setPadding(IIII)V

    .line 45
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/squareup/ui/cart/CartLineRow;->linePaint:Landroid/graphics/Paint;

    .line 46
    iget-object v1, p0, Lcom/squareup/ui/cart/CartLineRow;->linePaint:Landroid/graphics/Paint;

    sget v2, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/cart/CartLineRow;->linePaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/squareup/ui/cart/CartLineRow;->lineStroke:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 8

    .line 29
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 30
    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartLineRow;->getHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    .line 31
    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartLineRow;->getPaddingLeft()I

    move-result v1

    int-to-float v3, v1

    int-to-float v6, v0

    .line 32
    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartLineRow;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartLineRow;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    int-to-float v5, v0

    iget-object v7, p0, Lcom/squareup/ui/cart/CartLineRow;->linePaint:Landroid/graphics/Paint;

    move-object v2, p1

    move v4, v6

    .line 31
    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    return-void
.end method
