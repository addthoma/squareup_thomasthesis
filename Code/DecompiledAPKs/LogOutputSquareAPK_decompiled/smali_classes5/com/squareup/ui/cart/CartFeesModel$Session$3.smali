.class Lcom/squareup/ui/cart/CartFeesModel$Session$3;
.super Ljava/util/HashMap;
.source "CartFeesModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/cart/CartFeesModel$Session;->prepareTaxRows()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap<",
        "Lcom/squareup/checkout/Tax;",
        "Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/cart/CartFeesModel$Session;


# direct methods
.method constructor <init>(Lcom/squareup/ui/cart/CartFeesModel$Session;)V
    .locals 0

    .line 206
    iput-object p1, p0, Lcom/squareup/ui/cart/CartFeesModel$Session$3;->this$0:Lcom/squareup/ui/cart/CartFeesModel$Session;

    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    return-void
.end method


# virtual methods
.method public get(Ljava/lang/Object;)Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;
    .locals 1

    .line 208
    invoke-super {p0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;

    if-nez v0, :cond_0

    .line 210
    check-cast p1, Lcom/squareup/checkout/Tax;

    .line 211
    new-instance v0, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;

    invoke-direct {v0, p1}, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;-><init>(Lcom/squareup/checkout/Adjustment;)V

    .line 212
    invoke-virtual {p0, p1, v0}, Lcom/squareup/ui/cart/CartFeesModel$Session$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0
.end method

.method public bridge synthetic get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 206
    invoke-virtual {p0, p1}, Lcom/squareup/ui/cart/CartFeesModel$Session$3;->get(Ljava/lang/Object;)Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;

    move-result-object p1

    return-object p1
.end method
