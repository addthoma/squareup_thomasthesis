.class public Lcom/squareup/ui/cart/CartRecyclerViewPresenter;
.super Lmortar/ViewPresenter;
.source "CartRecyclerViewPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/cart/CartRecyclerView;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final badBus:Lcom/squareup/badbus/BadBus;

.field private final cartFees:Lcom/squareup/ui/cart/CartFeesModel$Session;

.field private final cartScreenRunner:Lcom/squareup/ui/cart/CartScreenRunner;

.field private discountRowIndex:I

.field private final features:Lcom/squareup/settings/server/Features;

.field private final flow:Lflow/Flow;

.field private initialized:Z

.field private final isTablet:Z

.field private final loyaltyCalculator:Lcom/squareup/loyalty/LoyaltyCalculator;

.field private loyaltyPointsRowSubtitle:Ljava/lang/Integer;

.field private final loyaltyPointsRowSubtitleRenderer:Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer;

.field private final loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

.field private final openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

.field private final orderEntryPages:Lcom/squareup/orderentry/pages/OrderEntryPages;

.field private final permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

.field private final pointsTermsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

.field private final res:Lcom/squareup/util/Res;

.field private final transaction:Lcom/squareup/payment/Transaction;

.field private visibleFooterRowCount:I


# direct methods
.method constructor <init>(Lcom/squareup/badbus/BadBus;Lflow/Flow;Lcom/squareup/payment/Transaction;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/analytics/Analytics;Lcom/squareup/orderentry/pages/OrderEntryPages;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/ui/cart/CartFeesModel$Session;Lcom/squareup/ui/cart/CartScreenRunner;Lcom/squareup/settings/server/Features;Lcom/squareup/util/Device;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/loyalty/LoyaltyCalculator;Lcom/squareup/loyalty/PointsTermsFormatter;Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer;Lcom/squareup/util/Res;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    .line 98
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    const/4 v1, 0x0

    .line 87
    iput v1, v0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->visibleFooterRowCount:I

    const/4 v1, -0x1

    .line 88
    iput v1, v0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->discountRowIndex:I

    const/4 v1, 0x0

    .line 89
    iput-object v1, v0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->loyaltyPointsRowSubtitle:Ljava/lang/Integer;

    move-object v1, p1

    .line 99
    iput-object v1, v0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->badBus:Lcom/squareup/badbus/BadBus;

    move-object v1, p2

    .line 100
    iput-object v1, v0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->flow:Lflow/Flow;

    move-object v1, p3

    .line 101
    iput-object v1, v0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->transaction:Lcom/squareup/payment/Transaction;

    move-object v1, p4

    .line 102
    iput-object v1, v0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    move-object v1, p5

    .line 103
    iput-object v1, v0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    move-object v1, p6

    .line 104
    iput-object v1, v0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->orderEntryPages:Lcom/squareup/orderentry/pages/OrderEntryPages;

    move-object v1, p7

    .line 105
    iput-object v1, v0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    move-object v1, p8

    .line 106
    iput-object v1, v0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->cartFees:Lcom/squareup/ui/cart/CartFeesModel$Session;

    move-object v1, p9

    .line 107
    iput-object v1, v0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->cartScreenRunner:Lcom/squareup/ui/cart/CartScreenRunner;

    move-object v1, p10

    .line 108
    iput-object v1, v0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->features:Lcom/squareup/settings/server/Features;

    .line 109
    invoke-interface {p11}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v1

    iput-boolean v1, v0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->isTablet:Z

    move-object v1, p12

    .line 110
    iput-object v1, v0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    move-object v1, p13

    .line 111
    iput-object v1, v0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    move-object/from16 v1, p14

    .line 112
    iput-object v1, v0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->loyaltyCalculator:Lcom/squareup/loyalty/LoyaltyCalculator;

    move-object/from16 v1, p15

    .line 113
    iput-object v1, v0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->pointsTermsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    move-object/from16 v1, p16

    .line 114
    iput-object v1, v0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->loyaltyPointsRowSubtitleRenderer:Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer;

    move-object/from16 v1, p17

    .line 115
    iput-object v1, v0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/cart/CartRecyclerViewPresenter;)Lcom/squareup/payment/Transaction;
    .locals 0

    .line 66
    iget-object p0, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->transaction:Lcom/squareup/payment/Transaction;

    return-object p0
.end method

.method private containsMoreThanJustEmptyCustomAmount(Ljava/util/List;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/cart/CartAdapterItem;",
            ">;)Z"
        }
    .end annotation

    .line 493
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/cart/CartAdapterItem;

    .line 494
    invoke-interface {v0}, Lcom/squareup/ui/cart/CartAdapterItem;->getType()Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->STANDARD:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    if-ne v0, v1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method private currentPageIsKeypad()Z
    .locals 2

    .line 511
    iget-object v0, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->orderEntryPages:Lcom/squareup/orderentry/pages/OrderEntryPages;

    invoke-virtual {v0}, Lcom/squareup/orderentry/pages/OrderEntryPages;->getCurrentPage()Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    move-result-object v0

    sget-object v1, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->KEYPAD:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isOpenTicketsEnabled()Z
    .locals 1

    .line 515
    iget-object v0, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsEnabled()Z

    move-result v0

    return v0
.end method

.method private shouldAnimateChanges()Z
    .locals 2

    .line 527
    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/cart/CartRecyclerView;

    .line 528
    iget-boolean v1, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->isTablet:Z

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/ui/cart/CartRecyclerView;->isPortrait()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private showLoyaltyPointsRow()Z
    .locals 1

    .line 458
    iget-object v0, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    invoke-interface {v0}, Lcom/squareup/loyalty/LoyaltySettings;->isLoyaltyProgramActive()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    invoke-interface {v0}, Lcom/squareup/loyalty/LoyaltySettings;->isLoyaltyScreensEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private snapToLastRowOrItem()V
    .locals 1

    .line 519
    invoke-direct {p0}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->isOpenTicketsEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/cart/CartRecyclerView;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/CartRecyclerView;->isPortrait()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 522
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/cart/CartRecyclerView;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/CartRecyclerView;->snapToLastItem()V

    goto :goto_1

    .line 520
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->snapToLastRow()V

    :goto_1
    return-void
.end method

.method private updateAutoGratuityPostTaxRow(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/cart/CartAdapterItem;",
            ">;)V"
        }
    .end annotation

    .line 397
    iget-object v0, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->shouldShowPostTaxAutoGratuityRow()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 398
    invoke-direct {p0, p1}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->updateAutoGratuityRow(Ljava/util/List;)V

    :cond_0
    return-void
.end method

.method private updateAutoGratuityPreTaxRow(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/cart/CartAdapterItem;",
            ">;)V"
        }
    .end annotation

    .line 391
    iget-object v0, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->shouldShowPreTaxAutoGratuityRow()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 392
    invoke-direct {p0, p1}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->updateAutoGratuityRow(Ljava/util/List;)V

    :cond_0
    return-void
.end method

.method private updateAutoGratuityRow(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/cart/CartAdapterItem;",
            ">;)V"
        }
    .end annotation

    .line 405
    iget-object v0, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getAutoGratuity()Lcom/squareup/checkout/Surcharge$AutoGratuity;

    move-result-object v0

    .line 406
    iget-object v1, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->res:Lcom/squareup/util/Res;

    iget-object v2, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 407
    invoke-virtual {v2}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/settings/server/UserSettings;->getCountryCode()Lcom/squareup/CountryCode;

    move-result-object v2

    const/4 v3, 0x0

    .line 406
    invoke-virtual {v0, v1, v3, v2}, Lcom/squareup/checkout/Surcharge$AutoGratuity;->formattedName(Lcom/squareup/util/Res;ZLcom/squareup/CountryCode;)Ljava/lang/String;

    move-result-object v0

    .line 408
    new-instance v1, Lcom/squareup/ui/cart/CartAdapterItem$SurchargeRow;

    iget-object v2, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v2}, Lcom/squareup/payment/Transaction;->getAutoGratuityAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/squareup/ui/cart/CartAdapterItem$SurchargeRow;-><init>(Ljava/lang/String;Lcom/squareup/protos/common/Money;)V

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 409
    iget p1, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->visibleFooterRowCount:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->visibleFooterRowCount:I

    return-void
.end method

.method private updateCart()V
    .locals 1

    const/4 v0, 0x0

    .line 502
    invoke-direct {p0, v0, v0}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->updateCart(ZZ)V

    return-void
.end method

.method private updateCart(ZZ)V
    .locals 1

    .line 506
    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/cart/CartRecyclerView;

    .line 507
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->getCartAdapterItems(ZZ)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/cart/CartRecyclerView;->setCartItems(Ljava/util/List;)V

    return-void
.end method

.method private updateCompRow(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/cart/CartAdapterItem;",
            ">;)V"
        }
    .end annotation

    .line 363
    iget-object v0, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->shouldShowCompRow()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 364
    new-instance v0, Lcom/squareup/ui/cart/CartAdapterItem$CompRow;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getAllComps()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/cart/CartAdapterItem$CompRow;-><init>(Lcom/squareup/protos/common/Money;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 365
    iget p1, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->visibleFooterRowCount:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->visibleFooterRowCount:I

    :cond_0
    return-void
.end method

.method private updateDiningOptions(Lcom/squareup/payment/OrderEntryEvents$CartChanged;)V
    .locals 2

    .line 320
    iget-boolean v0, p1, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->diningOptionChanged:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 321
    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/cart/CartRecyclerView;

    .line 322
    iget-object v1, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->hasDiningOption()Z

    move-result v1

    .line 323
    invoke-virtual {v0, v1}, Lcom/squareup/ui/cart/CartRecyclerView;->updateDiningOptionVisibility(Z)V

    .line 327
    invoke-virtual {p1}, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->everythingChanged()Z

    move-result p1

    if-eqz p1, :cond_0

    if-eqz v1, :cond_0

    .line 328
    invoke-virtual {v0}, Lcom/squareup/ui/cart/CartRecyclerView;->resetDiningOptionsTranslationY()V

    :cond_0
    return-void
.end method

.method private updateDiscountRow(Ljava/util/List;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/cart/CartAdapterItem;",
            ">;Z)V"
        }
    .end annotation

    .line 352
    iget-object v0, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->shouldShowDiscountRow()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 353
    new-instance v0, Lcom/squareup/ui/cart/CartAdapterItem$DiscountRow;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getAllDiscounts()Lcom/squareup/protos/common/Money;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 354
    invoke-virtual {v2}, Lcom/squareup/payment/Transaction;->hasUnappliedDiscountsThatCanBeAppliedToOnlyOneItem()Z

    move-result v2

    invoke-direct {v0, v1, v2, p2}, Lcom/squareup/ui/cart/CartAdapterItem$DiscountRow;-><init>(Lcom/squareup/protos/common/Money;ZZ)V

    .line 353
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 355
    iget p2, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->visibleFooterRowCount:I

    add-int/lit8 p2, p2, 0x1

    iput p2, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->visibleFooterRowCount:I

    .line 356
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    add-int/lit8 p1, p1, -0x1

    iput p1, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->discountRowIndex:I

    goto :goto_0

    :cond_0
    const/4 p1, -0x1

    .line 358
    iput p1, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->discountRowIndex:I

    :goto_0
    return-void
.end method

.method private updateEmptyCustomAmountRow(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/cart/CartAdapterItem;",
            ">;)V"
        }
    .end annotation

    .line 341
    iget-boolean v0, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->isTablet:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->currentPageIsKeypad()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 345
    new-instance v0, Lcom/squareup/ui/cart/CartAdapterItem$EmptyCustomAmountRow;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getEmptyKeypadItem()Lcom/squareup/checkout/CartItem;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/cart/CartAdapterItem$EmptyCustomAmountRow;-><init>(Lcom/squareup/checkout/CartItem;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 346
    iget p1, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->visibleFooterRowCount:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->visibleFooterRowCount:I

    :cond_0
    return-void
.end method

.method private updateLoyaltyPointsRow(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/cart/CartAdapterItem;",
            ">;)V"
        }
    .end annotation

    .line 438
    invoke-direct {p0}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->showLoyaltyPointsRow()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->containsMoreThanJustEmptyCustomAmount(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 440
    iget-object v0, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->loyaltyCalculator:Lcom/squareup/loyalty/LoyaltyCalculator;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0, v1}, Lcom/squareup/loyalty/LoyaltyCalculator;->calculateCartPoints(Lcom/squareup/payment/Transaction;)Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 445
    new-instance v1, Lcom/squareup/ui/cart/CartAdapterItem$TicketLineRow;

    invoke-direct {v1}, Lcom/squareup/ui/cart/CartAdapterItem$TicketLineRow;-><init>()V

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 446
    iget v1, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->visibleFooterRowCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->visibleFooterRowCount:I

    .line 448
    iget-object v1, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->pointsTermsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sget v2, Lcom/squareup/orderentry/R$string;->loyalty_cart_row_value:I

    invoke-virtual {v1, v0, v2}, Lcom/squareup/loyalty/PointsTermsFormatter;->points(II)Ljava/lang/String;

    move-result-object v0

    .line 451
    new-instance v1, Lcom/squareup/ui/cart/CartAdapterItem$LoyaltyPointsRow;

    iget-object v2, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->loyaltyPointsRowSubtitle:Ljava/lang/Integer;

    invoke-direct {v1, v0, v2}, Lcom/squareup/ui/cart/CartAdapterItem$LoyaltyPointsRow;-><init>(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 452
    iget p1, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->visibleFooterRowCount:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->visibleFooterRowCount:I

    :cond_0
    return-void
.end method

.method private updateTaxRow(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/cart/CartAdapterItem;",
            ">;)V"
        }
    .end annotation

    .line 370
    iget-object v0, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->shouldShowTaxRow()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 373
    iget-object v0, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->shouldShowTaxOff()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 374
    sget v0, Lcom/squareup/orderentry/R$string;->cart_tax_row_off:I

    goto :goto_0

    .line 375
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getAdditionalTaxes()Lcom/squareup/protos/common/Money;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-lez v4, :cond_1

    .line 376
    sget v0, Lcom/squareup/transaction/R$string;->cart_tax_row:I

    goto :goto_0

    .line 377
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasInclusiveTaxes()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 378
    sget v0, Lcom/squareup/common/strings/R$string;->cart_tax_row_included:I

    goto :goto_0

    .line 380
    :cond_2
    sget v0, Lcom/squareup/transaction/R$string;->cart_tax_row:I

    .line 383
    :goto_0
    new-instance v1, Lcom/squareup/ui/cart/CartAdapterItem$TaxRow;

    iget-object v2, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 384
    invoke-virtual {v2}, Lcom/squareup/payment/Transaction;->hasInterestingTaxState()Z

    move-result v2

    iget-object v3, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->getAdditionalTaxes()Lcom/squareup/protos/common/Money;

    move-result-object v3

    invoke-direct {v1, v2, v3, v0}, Lcom/squareup/ui/cart/CartAdapterItem$TaxRow;-><init>(ZLcom/squareup/protos/common/Money;I)V

    .line 383
    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 386
    iget p1, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->visibleFooterRowCount:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->visibleFooterRowCount:I

    :cond_3
    return-void
.end method

.method private updateTicketLine(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/cart/CartAdapterItem;",
            ">;)V"
        }
    .end annotation

    .line 426
    iget-object v0, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasTicket()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasLockedAndNonLockedItems()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 427
    iget-object v0, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getIndexOfLastLockedItem()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 428
    iget-object v1, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->hasOpenTicketNote()Z

    move-result v1

    if-eqz v1, :cond_0

    add-int/lit8 v0, v0, 0x1

    .line 432
    :cond_0
    new-instance v1, Lcom/squareup/ui/cart/CartAdapterItem$TicketLineRow;

    invoke-direct {v1}, Lcom/squareup/ui/cart/CartAdapterItem$TicketLineRow;-><init>()V

    invoke-interface {p1, v0, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 433
    iget p1, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->visibleFooterRowCount:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->visibleFooterRowCount:I

    :cond_1
    return-void
.end method

.method private updateTicketRow(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/cart/CartAdapterItem;",
            ">;)V"
        }
    .end annotation

    .line 335
    iget-object v0, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasTicket()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasOpenTicketNote()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 336
    new-instance v0, Lcom/squareup/ui/cart/CartAdapterItem$TicketNoteRow;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getOpenTicketNote()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/cart/CartAdapterItem$TicketNoteRow;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method private updateTotalRow(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/cart/CartAdapterItem;",
            ">;)V"
        }
    .end annotation

    .line 417
    invoke-direct {p0}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->isOpenTicketsEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_0

    .line 419
    new-instance v0, Lcom/squareup/ui/cart/CartAdapterItem$TotalRow;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/cart/CartAdapterItem$TotalRow;-><init>(Lcom/squareup/protos/common/Money;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 420
    iget p1, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->visibleFooterRowCount:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->visibleFooterRowCount:I

    :cond_0
    return-void
.end method

.method private updateUnappliedCouponRow(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/cart/CartAdapterItem;",
            ">;)V"
        }
    .end annotation

    .line 468
    iget-object v0, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getUnappliedDiscountsThatCanBeAppliedToOnlyOneItem()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/Discount;

    const/4 v2, 0x0

    .line 474
    invoke-static {v1, v2}, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponPropsKt;->toAddEligibleItemForCouponProps(Lcom/squareup/checkout/Discount;Z)Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 477
    new-instance v3, Lcom/squareup/ui/cart/CartAdapterItem$UnappliedCouponRow;

    iget-object v4, v1, Lcom/squareup/checkout/Discount;->name:Ljava/lang/String;

    .line 479
    invoke-virtual {v1}, Lcom/squareup/checkout/Discount;->getCouponToken()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-direct {v3, v4, v1, v2}, Lcom/squareup/ui/cart/CartAdapterItem$UnappliedCouponRow;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;)V

    .line 477
    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method getCartAdapterItems(ZZ)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ)",
            "Ljava/util/List<",
            "Lcom/squareup/ui/cart/CartAdapterItem;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 263
    iput v0, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->visibleFooterRowCount:I

    .line 264
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 267
    invoke-direct {p0, v1}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->updateTicketRow(Ljava/util/List;)V

    .line 270
    iget-object v2, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_X2_FRONT_OF_TRANSACTION_CHECKIN:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v2, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->DISCOUNT_APPLY_MULTIPLE_COUPONS:Lcom/squareup/settings/server/Features$Feature;

    .line 271
    invoke-interface {v2, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 272
    :cond_0
    invoke-direct {p0, v1}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->updateUnappliedCouponRow(Ljava/util/List;)V

    .line 276
    :cond_1
    iget-object v2, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v2}, Lcom/squareup/payment/Transaction;->getItems()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    .line 277
    iget-object v3, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->getDiningOptionName()Ljava/lang/String;

    move-result-object v3

    const/4 v10, 0x0

    :goto_0
    if-ge v10, v2, :cond_6

    add-int/lit8 v4, v2, -0x1

    const/4 v5, 0x1

    if-ne v10, v4, :cond_2

    const/4 v4, 0x1

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    :goto_1
    add-int/lit8 v6, v2, -0x2

    if-ne v10, v6, :cond_3

    const/4 v6, 0x1

    goto :goto_2

    :cond_3
    const/4 v6, 0x0

    :goto_2
    if-eqz v4, :cond_4

    .line 282
    iget-object v4, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v4}, Lcom/squareup/payment/Transaction;->hasEmptyKeypadItem()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 283
    invoke-direct {p0, v1}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->updateEmptyCustomAmountRow(Ljava/util/List;)V

    goto :goto_4

    :cond_4
    if-eqz p1, :cond_5

    if-eqz v6, :cond_5

    const/4 v8, 0x1

    goto :goto_3

    :cond_5
    const/4 v8, 0x0

    .line 288
    :goto_3
    new-instance v11, Lcom/squareup/ui/cart/CartAdapterItem$CartRowItem;

    iget-object v4, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 289
    invoke-virtual {v4, v10}, Lcom/squareup/payment/Transaction;->getOrderItem(I)Lcom/squareup/checkout/CartItem;

    move-result-object v5

    move-object v4, v11

    move v6, v10

    move-object v7, v3

    move v9, p2

    invoke-direct/range {v4 .. v9}, Lcom/squareup/ui/cart/CartAdapterItem$CartRowItem;-><init>(Lcom/squareup/checkout/CartItem;ILjava/lang/String;ZZ)V

    .line 288
    invoke-interface {v1, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_4
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 295
    :cond_6
    invoke-direct {p0, v1, p2}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->updateDiscountRow(Ljava/util/List;Z)V

    .line 296
    invoke-direct {p0, v1}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->updateCompRow(Ljava/util/List;)V

    .line 297
    invoke-direct {p0, v1}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->updateAutoGratuityPreTaxRow(Ljava/util/List;)V

    .line 298
    invoke-direct {p0, v1}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->updateTaxRow(Ljava/util/List;)V

    .line 299
    invoke-direct {p0, v1}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->updateAutoGratuityPostTaxRow(Ljava/util/List;)V

    .line 300
    invoke-direct {p0, v1}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->updateTotalRow(Ljava/util/List;)V

    .line 301
    invoke-direct {p0, v1}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->updateTicketLine(Ljava/util/List;)V

    .line 302
    invoke-direct {p0, v1}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->updateLoyaltyPointsRow(Ljava/util/List;)V

    .line 304
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method getFooterRowCount()I
    .locals 1

    .line 413
    iget v0, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->visibleFooterRowCount:I

    return v0
.end method

.method getItemCount()I
    .locals 1

    .line 238
    iget-object v0, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getCartRowCount()I

    move-result v0

    return v0
.end method

.method public isLoaded()Z
    .locals 1

    .line 315
    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->hasView()Z

    move-result v0

    return v0
.end method

.method public synthetic lambda$onEnterScope$0$CartRecyclerViewPresenter(Lcom/squareup/tickets/TicketEdited;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 119
    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->onTicketEdited()V

    return-void
.end method

.method public synthetic lambda$onEnterScope$1$CartRecyclerViewPresenter(Lcom/squareup/orderentry/pages/OrderEntryPageKey;)V
    .locals 1

    .line 126
    sget-object v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->KEYPAD:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-nez p1, :cond_1

    .line 129
    iget-object v0, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->commitKeypadItem()Z

    .line 132
    :cond_1
    iget-boolean v0, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->initialized:Z

    if-eqz v0, :cond_2

    .line 134
    invoke-direct {p0}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->updateCart()V

    if-eqz p1, :cond_2

    .line 135
    iget-object p1, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->hasItems()Z

    move-result p1

    if-eqz p1, :cond_2

    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/cart/CartRecyclerView;

    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartRecyclerView;->isPortrait()Z

    move-result p1

    if-nez p1, :cond_2

    .line 138
    invoke-direct {p0}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->snapToLastRowOrItem()V

    :cond_2
    return-void
.end method

.method public synthetic lambda$onEnterScope$2$CartRecyclerViewPresenter(Ljava/lang/Integer;)V
    .locals 1

    .line 151
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 152
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->loyaltyPointsRowSubtitle:Ljava/lang/Integer;

    invoke-static {p1, v0}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 153
    iput-object p1, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->loyaltyPointsRowSubtitle:Ljava/lang/Integer;

    .line 154
    invoke-direct {p0}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->updateCart()V

    :cond_1
    return-void
.end method

.method maybeRemoveFromCart(I)V
    .locals 3

    .line 242
    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/cart/CartRecyclerView;

    if-eqz v0, :cond_1

    .line 244
    invoke-virtual {v0, p1}, Lcom/squareup/ui/cart/CartRecyclerView;->getCartItem(I)Lcom/squareup/ui/cart/CartAdapterItem;

    move-result-object p1

    .line 245
    instance-of v0, p1, Lcom/squareup/ui/cart/CartAdapterItem$CartRowItem;

    if-eqz v0, :cond_1

    .line 246
    check-cast p1, Lcom/squareup/ui/cart/CartAdapterItem$CartRowItem;

    iget p1, p1, Lcom/squareup/ui/cart/CartAdapterItem$CartRowItem;->orderItemIndex:I

    .line 247
    iget-object v0, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0, p1}, Lcom/squareup/payment/Transaction;->getOrderItem(I)Lcom/squareup/checkout/CartItem;

    move-result-object v0

    .line 248
    iget-boolean v0, v0, Lcom/squareup/checkout/CartItem;->lockConfiguration:Z

    if-eqz v0, :cond_0

    .line 249
    iget-object v0, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v1, Lcom/squareup/permissions/Permission;->OPEN_TICKET_VOID:Lcom/squareup/permissions/Permission;

    new-instance v2, Lcom/squareup/ui/cart/CartRecyclerViewPresenter$1;

    invoke-direct {v2, p0, p1}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter$1;-><init>(Lcom/squareup/ui/cart/CartRecyclerViewPresenter;I)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    goto :goto_0

    .line 255
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0, p1}, Lcom/squareup/payment/Transaction;->removeItem(I)V

    :cond_1
    :goto_0
    return-void
.end method

.method onCartChanged(Lcom/squareup/payment/OrderEntryEvents$CartChanged;)V
    .locals 3

    .line 174
    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/cart/CartRecyclerView;

    if-nez v0, :cond_0

    return-void

    .line 179
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->updateDiningOptions(Lcom/squareup/payment/OrderEntryEvents$CartChanged;)V

    .line 180
    invoke-virtual {p1}, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->onlyKeypadCommitted()Z

    move-result v1

    invoke-virtual {p1}, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->onlyDiscountChanged()Z

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->updateCart(ZZ)V

    .line 182
    invoke-direct {p0}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->shouldAnimateChanges()Z

    move-result v1

    if-nez v1, :cond_1

    return-void

    .line 186
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->onlyItemChanged()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p1, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->status:Lcom/squareup/payment/OrderEntryEvents$ItemStatus;

    if-eqz v1, :cond_2

    iget-object v1, p1, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->status:Lcom/squareup/payment/OrderEntryEvents$ItemStatus;

    iget-object v1, v1, Lcom/squareup/payment/OrderEntryEvents$ItemStatus;->changeType:Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;

    sget-object v2, Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;->DELETE:Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;

    if-ne v1, v2, :cond_2

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    .line 187
    :goto_0
    iget-boolean v2, p1, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->animate:Z

    if-eqz v2, :cond_5

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->hasItems()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 189
    invoke-direct {p0}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->isOpenTicketsEnabled()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 190
    invoke-virtual {v0}, Lcom/squareup/ui/cart/CartRecyclerView;->smoothScrollToLastRow()V

    goto :goto_1

    .line 192
    :cond_3
    invoke-virtual {p1}, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->onlyDiscountChanged()Z

    move-result p1

    if-eqz p1, :cond_4

    iget-object p1, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 193
    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->hasNonCompDiscounts()Z

    move-result p1

    if-eqz p1, :cond_4

    iget p1, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->discountRowIndex:I

    const/4 v1, -0x1

    if-eq p1, v1, :cond_4

    .line 195
    invoke-virtual {v0, p1}, Lcom/squareup/ui/cart/CartRecyclerView;->smoothScrollToPosition(I)V

    goto :goto_1

    .line 197
    :cond_4
    invoke-virtual {v0}, Lcom/squareup/ui/cart/CartRecyclerView;->smoothScrollToLastItem()V

    goto :goto_1

    .line 200
    :cond_5
    iget-boolean p1, p1, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->viaTicket:Z

    if-eqz p1, :cond_6

    .line 202
    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->snapToLastRow()V

    :cond_6
    :goto_1
    return-void
.end method

.method onDiscountRowClicked(I)V
    .locals 1

    .line 220
    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/cart/CartRecyclerView;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/cart/CartRecyclerView;->recoverPreviousSwipe(I)V

    .line 222
    iget-object p1, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->cartFees:Lcom/squareup/ui/cart/CartFeesModel$Session;

    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartFeesModel$Session;->prepareDiscountRows()V

    .line 223
    iget-object p1, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->cartScreenRunner:Lcom/squareup/ui/cart/CartScreenRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartScreenRunner;->goToCartDiscounts()V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 119
    iget-object v0, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->badBus:Lcom/squareup/badbus/BadBus;

    const-class v1, Lcom/squareup/tickets/TicketEdited;

    invoke-virtual {v0, v1}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/cart/-$$Lambda$CartRecyclerViewPresenter$gVZIP_zGXxoQHcdWgAVnDm0hJGY;

    invoke-direct {v1, p0}, Lcom/squareup/ui/cart/-$$Lambda$CartRecyclerViewPresenter$gVZIP_zGXxoQHcdWgAVnDm0hJGY;-><init>(Lcom/squareup/ui/cart/CartRecyclerViewPresenter;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 120
    iget-object v0, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->badBus:Lcom/squareup/badbus/BadBus;

    const-class v1, Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    invoke-virtual {v0, v1}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/cart/-$$Lambda$JlFY0zdc6nrIlMDGr69Wv25HWfg;

    invoke-direct {v1, p0}, Lcom/squareup/ui/cart/-$$Lambda$JlFY0zdc6nrIlMDGr69Wv25HWfg;-><init>(Lcom/squareup/ui/cart/CartRecyclerViewPresenter;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 123
    iget-boolean v0, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->isTablet:Z

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->orderEntryPages:Lcom/squareup/orderentry/pages/OrderEntryPages;

    .line 125
    invoke-virtual {v0}, Lcom/squareup/orderentry/pages/OrderEntryPages;->observeCurrentPage()Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/cart/-$$Lambda$CartRecyclerViewPresenter$U-5X1ZKoT9M7S11CjQyE1kL0DIs;

    invoke-direct {v1, p0}, Lcom/squareup/ui/cart/-$$Lambda$CartRecyclerViewPresenter$U-5X1ZKoT9M7S11CjQyE1kL0DIs;-><init>(Lcom/squareup/ui/cart/CartRecyclerViewPresenter;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 124
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    .line 144
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->showLoyaltyPointsRow()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 148
    iget-object v0, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->loyaltyPointsRowSubtitleRenderer:Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 149
    invoke-virtual {v0, v1}, Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer;->getSubtitle(Lcom/squareup/payment/Transaction;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/cart/-$$Lambda$CartRecyclerViewPresenter$pBd9bz1xKaIrAU0BS8D6o2RVkLc;

    invoke-direct {v1, p0}, Lcom/squareup/ui/cart/-$$Lambda$CartRecyclerViewPresenter$pBd9bz1xKaIrAU0BS8D6o2RVkLc;-><init>(Lcom/squareup/ui/cart/CartRecyclerViewPresenter;)V

    .line 150
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 148
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    :cond_1
    return-void
.end method

.method onItemClicked(Lcom/squareup/checkout/CartItem;II)V
    .locals 2

    .line 207
    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->isVoided()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    const-string v1, "Voided items should not be clickable"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 209
    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/cart/CartRecyclerView;

    invoke-virtual {v0, p2}, Lcom/squareup/ui/cart/CartRecyclerView;->recoverPreviousSwipe(I)V

    .line 211
    iget-object p2, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {p2}, Lcom/squareup/settings/server/AccountStatusSettings;->isSkipModifierDetailScreenEnabled()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 212
    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->shouldSkipModifierDetailScreen()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 213
    iget-object p1, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object p2, Lcom/squareup/analytics/RegisterActionName;->ADVANCED_MODIFIER_SKIP_ITEM_MODAL_OPEN_ITEM_IN_CART:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {p1, p2}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 216
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->flow:Lflow/Flow;

    new-instance p2, Lcom/squareup/configure/item/ConfigureItemDetailScreen;

    sget-object v0, Lcom/squareup/ui/seller/SellerScope;->INSTANCE:Lcom/squareup/ui/seller/SellerScope;

    invoke-direct {p2, v0, p3}, Lcom/squareup/configure/item/ConfigureItemDetailScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;I)V

    invoke-virtual {p1, p2}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 0

    const/4 p1, 0x1

    .line 161
    iput-boolean p1, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->initialized:Z

    .line 162
    invoke-direct {p0}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->updateCart()V

    .line 164
    iget-boolean p1, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->isTablet:Z

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->hasItems()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 165
    invoke-direct {p0}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->snapToLastRowOrItem()V

    :cond_0
    return-void
.end method

.method onLoyaltyRowClicked(I)V
    .locals 1

    .line 462
    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/cart/CartRecyclerView;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/cart/CartRecyclerView;->recoverPreviousSwipe(I)V

    .line 464
    iget-object p1, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->cartScreenRunner:Lcom/squareup/ui/cart/CartScreenRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartScreenRunner;->goToLoyaltyRedeemPointsScreen()V

    return-void
.end method

.method onTaxRowClicked(I)V
    .locals 1

    .line 227
    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/cart/CartRecyclerView;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/cart/CartRecyclerView;->recoverPreviousSwipe(I)V

    .line 229
    iget-object p1, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->cartFees:Lcom/squareup/ui/cart/CartFeesModel$Session;

    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartFeesModel$Session;->prepareTaxRows()V

    .line 230
    iget-object p1, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->cartScreenRunner:Lcom/squareup/ui/cart/CartScreenRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartScreenRunner;->goToCartTaxes()V

    return-void
.end method

.method onTicketEdited()V
    .locals 0

    .line 170
    invoke-direct {p0}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->updateCart()V

    return-void
.end method

.method onUnappliedCouponRowClicked(ILcom/squareup/ui/cart/CartAdapterItem$UnappliedCouponRow;)V
    .locals 1

    .line 487
    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/cart/CartRecyclerView;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/cart/CartRecyclerView;->recoverPreviousSwipe(I)V

    .line 489
    iget-object p1, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->cartScreenRunner:Lcom/squareup/ui/cart/CartScreenRunner;

    iget-object p2, p2, Lcom/squareup/ui/cart/CartAdapterItem$UnappliedCouponRow;->props:Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/cart/CartScreenRunner;->goToAddEligibleItemWorkflow(Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;)V

    return-void
.end method

.method showDiningOption()Z
    .locals 2

    .line 234
    iget-object v0, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->DINING_OPTIONS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasDiningOption()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public snapToLastRow()V
    .locals 1

    .line 308
    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/cart/CartRecyclerView;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/CartRecyclerView;->snapToLastRow()V

    return-void
.end method
