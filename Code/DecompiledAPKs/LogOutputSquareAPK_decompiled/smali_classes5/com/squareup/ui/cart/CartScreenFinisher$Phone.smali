.class public Lcom/squareup/ui/cart/CartScreenFinisher$Phone;
.super Ljava/lang/Object;
.source "CartScreenFinisher.java"

# interfaces
.implements Lcom/squareup/ui/cart/CartScreenFinisher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/cart/CartScreenFinisher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Phone"
.end annotation


# instance fields
.field private final flow:Lflow/Flow;


# direct methods
.method public constructor <init>(Lflow/Flow;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/ui/cart/CartScreenFinisher$Phone;->flow:Lflow/Flow;

    return-void
.end method


# virtual methods
.method public finishCartScreen()V
    .locals 2

    .line 26
    iget-object v0, p0, Lcom/squareup/ui/cart/CartScreenFinisher$Phone;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/cart/CartContainerScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method
