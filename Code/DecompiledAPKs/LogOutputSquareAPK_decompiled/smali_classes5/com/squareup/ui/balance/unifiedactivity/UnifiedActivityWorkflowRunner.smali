.class public final Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivityWorkflowRunner;
.super Lcom/squareup/container/WorkflowV2Runner;
.source "UnifiedActivityWorkflowRunner.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivityWorkflowRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/container/WorkflowV2Runner<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 \u000f2\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u000fB\u001f\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\u0010\u0010\u000c\u001a\u00020\u00022\u0006\u0010\r\u001a\u00020\u000eH\u0014R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u00020\u0006X\u0094\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000b\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivityWorkflowRunner;",
        "Lcom/squareup/container/WorkflowV2Runner;",
        "",
        "container",
        "Lcom/squareup/ui/main/PosContainer;",
        "workflow",
        "Lcom/squareup/balance/activity/ui/list/BalanceActivityWorkflow;",
        "viewFactory",
        "Lcom/squareup/balance/activity/ui/BalanceActivityViewFactory;",
        "(Lcom/squareup/ui/main/PosContainer;Lcom/squareup/balance/activity/ui/list/BalanceActivityWorkflow;Lcom/squareup/balance/activity/ui/BalanceActivityViewFactory;)V",
        "getWorkflow",
        "()Lcom/squareup/balance/activity/ui/list/BalanceActivityWorkflow;",
        "onEnterScope",
        "newScope",
        "Lmortar/MortarScope;",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivityWorkflowRunner$Companion;

.field private static final NAME:Ljava/lang/String;


# instance fields
.field private final container:Lcom/squareup/ui/main/PosContainer;

.field private final workflow:Lcom/squareup/balance/activity/ui/list/BalanceActivityWorkflow;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivityWorkflowRunner$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivityWorkflowRunner$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivityWorkflowRunner;->Companion:Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivityWorkflowRunner$Companion;

    .line 51
    const-class v0, Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivityWorkflowRunner;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "UnifiedActivityWorkflowRunner::class.java.name"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivityWorkflowRunner;->NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/main/PosContainer;Lcom/squareup/balance/activity/ui/list/BalanceActivityWorkflow;Lcom/squareup/balance/activity/ui/BalanceActivityViewFactory;)V
    .locals 9
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "container"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "viewFactory"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    sget-object v2, Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivityWorkflowRunner;->NAME:Ljava/lang/String;

    .line 21
    invoke-interface {p1}, Lcom/squareup/ui/main/PosContainer;->nextHistory()Lio/reactivex/Observable;

    move-result-object v3

    .line 22
    const-class v0, Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivitySection;

    invoke-interface {p3, v0}, Lcom/squareup/balance/activity/ui/BalanceActivityViewFactory;->create(Ljava/lang/Class;)Lcom/squareup/workflow/WorkflowViewFactory;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x18

    const/4 v8, 0x0

    move-object v1, p0

    .line 19
    invoke-direct/range {v1 .. v8}, Lcom/squareup/container/WorkflowV2Runner;-><init>(Ljava/lang/String;Lio/reactivex/Observable;Lcom/squareup/workflow/WorkflowViewFactory;ZLkotlinx/coroutines/CoroutineDispatcher;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivityWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    iput-object p2, p0, Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivityWorkflowRunner;->workflow:Lcom/squareup/balance/activity/ui/list/BalanceActivityWorkflow;

    return-void
.end method

.method public static final synthetic access$ensureWorkflow(Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivityWorkflowRunner;)V
    .locals 0

    .line 15
    invoke-virtual {p0}, Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivityWorkflowRunner;->ensureWorkflow()V

    return-void
.end method

.method public static final synthetic access$getContainer$p(Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivityWorkflowRunner;)Lcom/squareup/ui/main/PosContainer;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivityWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    return-object p0
.end method

.method public static final synthetic access$getNAME$cp()Ljava/lang/String;
    .locals 1

    .line 15
    sget-object v0, Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivityWorkflowRunner;->NAME:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected getWorkflow()Lcom/squareup/balance/activity/ui/list/BalanceActivityWorkflow;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivityWorkflowRunner;->workflow:Lcom/squareup/balance/activity/ui/list/BalanceActivityWorkflow;

    return-object v0
.end method

.method public bridge synthetic getWorkflow()Lcom/squareup/workflow/Workflow;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivityWorkflowRunner;->getWorkflow()Lcom/squareup/balance/activity/ui/list/BalanceActivityWorkflow;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/Workflow;

    return-object v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    const-string v0, "newScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-super {p0, p1}, Lcom/squareup/container/WorkflowV2Runner;->onEnterScope(Lmortar/MortarScope;)V

    .line 32
    invoke-virtual {p0}, Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivityWorkflowRunner;->onUpdateScreens()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivityWorkflowRunner$onEnterScope$1;

    iget-object v2, p0, Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivityWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    invoke-direct {v1, v2}, Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivityWorkflowRunner$onEnterScope$1;-><init>(Lcom/squareup/ui/main/PosContainer;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 34
    invoke-virtual {p0}, Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivityWorkflowRunner;->onResult()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivityWorkflowRunner$onEnterScope$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivityWorkflowRunner$onEnterScope$2;-><init>(Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivityWorkflowRunner;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method
