.class public final Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator_Factory;
.super Ljava/lang/Object;
.source "BalanceTransactionsCoordinator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$Runner;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/DayAndDateFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/recycler/RecyclerFactory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/DayAndDateFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/recycler/RecyclerFactory;",
            ">;)V"
        }
    .end annotation

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 35
    iput-object p2, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 36
    iput-object p3, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 37
    iput-object p4, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 38
    iput-object p5, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator_Factory;->arg4Provider:Ljavax/inject/Provider;

    .line 39
    iput-object p6, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator_Factory;->arg5Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/DayAndDateFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/recycler/RecyclerFactory;",
            ">;)",
            "Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator_Factory;"
        }
    .end annotation

    .line 51
    new-instance v7, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$Runner;Lcom/squareup/util/Device;Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Lcom/squareup/text/DayAndDateFormatter;Lcom/squareup/recycler/RecyclerFactory;)Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$Runner;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/text/DayAndDateFormatter;",
            "Lcom/squareup/recycler/RecyclerFactory;",
            ")",
            "Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;"
        }
    .end annotation

    .line 57
    new-instance v7, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;-><init>(Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$Runner;Lcom/squareup/util/Device;Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Lcom/squareup/text/DayAndDateFormatter;Lcom/squareup/recycler/RecyclerFactory;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;
    .locals 7

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$Runner;

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/util/Device;

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/text/Formatter;

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Ljava/text/DateFormat;

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/text/DayAndDateFormatter;

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator_Factory;->arg5Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/recycler/RecyclerFactory;

    invoke-static/range {v1 .. v6}, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator_Factory;->newInstance(Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$Runner;Lcom/squareup/util/Device;Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Lcom/squareup/text/DayAndDateFormatter;Lcom/squareup/recycler/RecyclerFactory;)Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator_Factory;->get()Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;

    move-result-object v0

    return-object v0
.end method
