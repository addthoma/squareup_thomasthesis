.class public final Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData;
.super Ljava/lang/Object;
.source "TransferToBankScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ScreenData"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;
    }
.end annotation


# instance fields
.field public final formattedFee:Ljava/lang/CharSequence;

.field public final formattedFeeTotalAmount:Ljava/lang/CharSequence;

.field public final formattedMaxDeposit:Ljava/lang/CharSequence;

.field public final instantDepositClickable:Z

.field public final isFeeFixedAmount:Z

.field public final maxDeposit:Lcom/squareup/protos/common/Money;

.field public final showInstantDeposit:Z

.field public final transferSpeed:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;


# direct methods
.method private constructor <init>(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;)V
    .locals 1

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    invoke-static {p1}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;->access$000(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData;->showInstantDeposit:Z

    .line 57
    invoke-static {p1}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;->access$100(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData;->transferSpeed:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;

    .line 58
    invoke-static {p1}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;->access$200(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData;->instantDepositClickable:Z

    .line 59
    invoke-static {p1}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;->access$300(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData;->isFeeFixedAmount:Z

    .line 60
    invoke-static {p1}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;->access$400(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData;->maxDeposit:Lcom/squareup/protos/common/Money;

    .line 61
    invoke-static {p1}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;->access$500(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData;->formattedMaxDeposit:Ljava/lang/CharSequence;

    .line 62
    invoke-static {p1}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;->access$600(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData;->formattedFee:Ljava/lang/CharSequence;

    .line 63
    invoke-static {p1}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;->access$700(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;)Ljava/lang/CharSequence;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData;->formattedFeeTotalAmount:Ljava/lang/CharSequence;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$1;)V
    .locals 0

    .line 45
    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData;-><init>(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;)V

    return-void
.end method
