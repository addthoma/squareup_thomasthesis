.class public final Lcom/squareup/ui/balance/bizbanking/capital/CapitalBootstrapScreen;
.super Lcom/squareup/container/BootstrapTreeKey;
.source "CapitalBootstrapScreen.kt"

# interfaces
.implements Lcom/squareup/container/layer/InSection;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u00012\u00020\u0002B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0016J\u0008\u0010\u0012\u001a\u00020\u0013H\u0016R\u0016\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0018\u0010\n\u001a\u0006\u0012\u0002\u0008\u00030\u000b8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\r\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/ui/balance/bizbanking/capital/CapitalBootstrapScreen;",
        "Lcom/squareup/container/BootstrapTreeKey;",
        "Lcom/squareup/container/layer/InSection;",
        "device",
        "Lcom/squareup/util/Device;",
        "(Lcom/squareup/util/Device;)V",
        "directionOverride",
        "Lflow/Direction;",
        "getDirectionOverride",
        "()Lflow/Direction;",
        "section",
        "Ljava/lang/Class;",
        "getSection",
        "()Ljava/lang/Class;",
        "doRegister",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "getParentKey",
        "Lcom/squareup/ui/balance/bizbanking/capital/CapitalScope;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final directionOverride:Lflow/Direction;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Device;)V
    .locals 1

    const-string v0, "device"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Lcom/squareup/container/BootstrapTreeKey;-><init>()V

    .line 18
    invoke-interface {p1}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    sget-object p1, Lflow/Direction;->REPLACE:Lflow/Direction;

    :goto_0
    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/capital/CapitalBootstrapScreen;->directionOverride:Lflow/Direction;

    return-void
.end method


# virtual methods
.method public doRegister(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-static {p1}, Lcom/squareup/ui/balance/bizbanking/capital/CapitalBootstrapScreenKt;->access$getCapitalFlexLoanWorkflowRunner(Lmortar/MortarScope;)Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowRunner;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowRunner;->startWorkflow()V

    return-void
.end method

.method public getDirectionOverride()Lflow/Direction;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/capital/CapitalBootstrapScreen;->directionOverride:Lflow/Direction;

    return-object v0
.end method

.method public getParentKey()Lcom/squareup/ui/balance/bizbanking/capital/CapitalScope;
    .locals 1

    .line 20
    sget-object v0, Lcom/squareup/ui/balance/bizbanking/capital/CapitalScope;->INSTANCE:Lcom/squareup/ui/balance/bizbanking/capital/CapitalScope;

    return-object v0
.end method

.method public bridge synthetic getParentKey()Ljava/lang/Object;
    .locals 1

    .line 16
    invoke-virtual {p0}, Lcom/squareup/ui/balance/bizbanking/capital/CapitalBootstrapScreen;->getParentKey()Lcom/squareup/ui/balance/bizbanking/capital/CapitalScope;

    move-result-object v0

    return-object v0
.end method

.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 26
    const-class v0, Lcom/squareup/capital/flexloan/CapitalFlexLoanSection;

    return-object v0
.end method
