.class public final Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$$special$$inlined$pathCreator$1;
.super Lcom/squareup/container/ContainerTreeKey$PathCreator;
.source "Container.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen;-><clinit>()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/container/ContainerTreeKey$PathCreator<",
        "Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nContainer.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Container.kt\ncom/squareup/container/ContainerKt$pathCreator$1\n+ 2 BalanceTransactionsScreen.kt\ncom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen\n*L\n1#1,56:1\n39#2:57\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000/\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0011\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u0008\u0012\u0004\u0012\u00028\u00000\u0001J\u0015\u0010\u0002\u001a\u00028\u00002\u0006\u0010\u0003\u001a\u00020\u0004H\u0014\u00a2\u0006\u0002\u0010\u0005J\u001d\u0010\u0006\u001a\n\u0012\u0006\u0012\u0004\u0018\u00018\u00000\u00072\u0006\u0010\u0008\u001a\u00020\tH\u0016\u00a2\u0006\u0002\u0010\n\u00a8\u0006\u000b\u00b8\u0006\u0000"
    }
    d2 = {
        "com/squareup/container/ContainerKt$pathCreator$1",
        "Lcom/squareup/container/ContainerTreeKey$PathCreator;",
        "doCreateFromParcel",
        "source",
        "Landroid/os/Parcel;",
        "(Landroid/os/Parcel;)Lcom/squareup/container/ContainerTreeKey;",
        "newArray",
        "",
        "size",
        "",
        "(I)[Lcom/squareup/container/ContainerTreeKey;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 24
    invoke-direct {p0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;-><init>()V

    return-void
.end method


# virtual methods
.method protected doCreateFromParcel(Landroid/os/Parcel;)Lcom/squareup/container/ContainerTreeKey;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Parcel;",
            ")",
            "Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen;"
        }
    .end annotation

    const-string v0, "source"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    sget-object p1, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen;->INSTANCE:Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen;

    check-cast p1, Lcom/squareup/container/ContainerTreeKey;

    return-object p1
.end method

.method public newArray(I)[Lcom/squareup/container/ContainerTreeKey;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)[",
            "Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen;"
        }
    .end annotation

    .line 25
    new-array p1, p1, [Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 24
    invoke-virtual {p0, p1}, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$$special$$inlined$pathCreator$1;->newArray(I)[Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    return-object p1
.end method
