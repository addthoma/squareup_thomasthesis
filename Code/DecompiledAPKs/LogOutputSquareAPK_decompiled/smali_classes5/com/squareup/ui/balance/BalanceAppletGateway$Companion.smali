.class public final Lcom/squareup/ui/balance/BalanceAppletGateway$Companion;
.super Ljava/lang/Object;
.source "BalanceAppletGateway.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/balance/BalanceAppletGateway;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/squareup/ui/balance/BalanceAppletGateway$Companion;",
        "",
        "()V",
        "DEEP_LINK",
        "",
        "INTENT_SCREEN_EXTRA",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$INSTANCE:Lcom/squareup/ui/balance/BalanceAppletGateway$Companion;

.field public static final DEEP_LINK:Ljava/lang/String; = "square-register://deposits"

.field public static final INTENT_SCREEN_EXTRA:Ljava/lang/String; = "BALANCE"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 31
    new-instance v0, Lcom/squareup/ui/balance/BalanceAppletGateway$Companion;

    invoke-direct {v0}, Lcom/squareup/ui/balance/BalanceAppletGateway$Companion;-><init>()V

    sput-object v0, Lcom/squareup/ui/balance/BalanceAppletGateway$Companion;->$$INSTANCE:Lcom/squareup/ui/balance/BalanceAppletGateway$Companion;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
