.class public abstract Lcom/squareup/ui/balance/addmoney/AddMoneyModule;
.super Ljava/lang/Object;
.source "AddMoneyModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\'\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0015\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H!\u00a2\u0006\u0002\u0008\u0007J\u0015\u0010\u0008\u001a\u00020\t2\u0006\u0010\u0005\u001a\u00020\nH!\u00a2\u0006\u0002\u0008\u000bJ\u0015\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH!\u00a2\u0006\u0002\u0008\u0010\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/ui/balance/addmoney/AddMoneyModule;",
        "",
        "()V",
        "bindsEnterAmountWorkflow",
        "Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountWorkflow;",
        "enterAmountWorkflow",
        "Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow;",
        "bindsEnterAmountWorkflow$impl_wiring_release",
        "bindsPendingSourceWorkflow",
        "Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceWorkflow;",
        "Lcom/squareup/ui/balance/addmoney/pendingsource/RealPendingSourceWorkflow;",
        "bindsPendingSourceWorkflow$impl_wiring_release",
        "bindsSubmitAmountWorkflow",
        "Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountWorkflow;",
        "submitAmountWorkflow",
        "Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow;",
        "bindsSubmitAmountWorkflow$impl_wiring_release",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract bindsEnterAmountWorkflow$impl_wiring_release(Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow;)Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindsPendingSourceWorkflow$impl_wiring_release(Lcom/squareup/ui/balance/addmoney/pendingsource/RealPendingSourceWorkflow;)Lcom/squareup/ui/balance/addmoney/pendingsource/PendingSourceWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindsSubmitAmountWorkflow$impl_wiring_release(Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow;)Lcom/squareup/ui/balance/addmoney/submitamount/SubmitAmountWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
