.class public final Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator_Factory;
.super Ljava/lang/Object;
.source "TransferResultCoordinator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;",
        ">;"
    }
.end annotation


# instance fields
.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$Runner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator_Factory;->runnerProvider:Ljavax/inject/Provider;

    .line 21
    iput-object p2, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;)",
            "Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator_Factory;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$Runner;Lcom/squareup/text/Formatter;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$Runner;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)",
            "Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;"
        }
    .end annotation

    .line 37
    new-instance v0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;-><init>(Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$Runner;Lcom/squareup/text/Formatter;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;
    .locals 2

    .line 26
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator_Factory;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$Runner;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/text/Formatter;

    invoke-static {v0, v1}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator_Factory;->newInstance(Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$Runner;Lcom/squareup/text/Formatter;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator_Factory;->get()Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;

    move-result-object v0

    return-object v0
.end method
