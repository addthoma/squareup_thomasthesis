.class public interface abstract Lcom/squareup/ui/balance/BalanceAppletGateway;
.super Ljava/lang/Object;
.source "BalanceAppletGateway.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/balance/BalanceAppletGateway$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0003\u0008f\u0018\u0000 \u00082\u00020\u0001:\u0001\u0008J\u0008\u0010\u0002\u001a\u00020\u0003H&J\u000e\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H&J\u0008\u0010\u0007\u001a\u00020\u0006H&\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/ui/balance/BalanceAppletGateway;",
        "",
        "activateInitialScreen",
        "",
        "hasPermissionToActivate",
        "Lio/reactivex/Observable;",
        "",
        "isVisible",
        "Companion",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/balance/BalanceAppletGateway$Companion;

.field public static final DEEP_LINK:Ljava/lang/String; = "square-register://deposits"

.field public static final INTENT_SCREEN_EXTRA:Ljava/lang/String; = "BALANCE"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/squareup/ui/balance/BalanceAppletGateway$Companion;->$$INSTANCE:Lcom/squareup/ui/balance/BalanceAppletGateway$Companion;

    sput-object v0, Lcom/squareup/ui/balance/BalanceAppletGateway;->Companion:Lcom/squareup/ui/balance/BalanceAppletGateway$Companion;

    return-void
.end method


# virtual methods
.method public abstract activateInitialScreen()V
.end method

.method public abstract hasPermissionToActivate()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract isVisible()Z
.end method
