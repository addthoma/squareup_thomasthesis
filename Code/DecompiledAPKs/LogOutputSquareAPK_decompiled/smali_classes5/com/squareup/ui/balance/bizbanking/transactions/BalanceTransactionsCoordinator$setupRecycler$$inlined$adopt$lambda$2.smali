.class public final Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$setupRecycler$$inlined$adopt$lambda$2;
.super Lkotlin/jvm/internal/Lambda;
.source "BinderRowSpec.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function3;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;->setupRecycler()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function3<",
        "Ljava/lang/Integer;",
        "TS;TV;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBinderRowSpec.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BinderRowSpec.kt\ncom/squareup/cycler/BinderRowSpec$bind$1\n+ 2 BalanceTransactionsCoordinator.kt\ncom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator\n*L\n1#1,56:1\n156#2,21:57\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00de\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0006\n\u0002\u0008\u0006\n\u0002\u0008\u0006\n\u0002\u0008\u0006\n\u0002\u0008\u0006\n\u0002\u0008\u0007\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0004*\u0002H\u0002\"\u0008\u0008\u0002\u0010\u0005*\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u0002H\u00042\u0006\u0010\n\u001a\u0002H\u0005H\n\u00a2\u0006\u0004\u0008\u000b\u0010\u000c\u00a8\u0006\u000f"
    }
    d2 = {
        "<anonymous>",
        "",
        "I",
        "",
        "S",
        "V",
        "Landroid/view/View;",
        "<anonymous parameter 0>",
        "",
        "item",
        "view",
        "invoke",
        "(ILjava/lang/Object;Landroid/view/View;)V",
        "com/squareup/cycler/BinderRowSpec$bind$1",
        "com/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$$special$$inlined$bind$2",
        "com/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$$special$$inlined$nohoRow$lambda$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $strikeThroughSpan$inlined:Landroid/text/style/StrikethroughSpan;

.field final synthetic this$0:Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;Landroid/text/style/StrikethroughSpan;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$setupRecycler$$inlined$adopt$lambda$2;->this$0:Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;

    iput-object p2, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$setupRecycler$$inlined$adopt$lambda$2;->$strikeThroughSpan$inlined:Landroid/text/style/StrikethroughSpan;

    const/4 p1, 0x3

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 14
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    check-cast p3, Landroid/view/View;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$setupRecycler$$inlined$adopt$lambda$2;->invoke(ILjava/lang/Object;Landroid/view/View;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(ILjava/lang/Object;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITS;TV;)V"
        }
    .end annotation

    const-string p1, "item"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo p1, "view"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    check-cast p3, Lcom/squareup/noho/NohoRow;

    check-cast p2, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$TransactionRow$CardTransactionRow;

    .line 57
    invoke-virtual {p2}, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$TransactionRow$CardTransactionRow;->getCardActivityRow()Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$CardActivityRow;

    move-result-object p1

    .line 59
    iget-object p2, p1, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$CardActivityRow;->name:Ljava/lang/String;

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p3, p2}, Lcom/squareup/noho/NohoRow;->setLabel(Ljava/lang/CharSequence;)V

    .line 61
    iget-boolean p2, p1, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$CardActivityRow;->isPending:Z

    if-eqz p2, :cond_0

    .line 62
    sget p2, Lcom/squareup/balance/applet/impl/R$style;->TransactionDescriptionItalic:I

    invoke-virtual {p3, p2}, Lcom/squareup/noho/NohoRow;->setDescriptionAppearanceId(I)V

    .line 63
    sget p2, Lcom/squareup/balance/applet/impl/R$string;->card_activity_pending:I

    invoke-virtual {p3, p2}, Lcom/squareup/noho/NohoRow;->setDescriptionId(I)V

    goto :goto_0

    .line 65
    :cond_0
    iget-boolean p2, p1, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$CardActivityRow;->isDeclined:Z

    if-eqz p2, :cond_1

    .line 66
    sget p2, Lcom/squareup/balance/applet/impl/R$style;->TransactionDescriptionItalic:I

    invoke-virtual {p3, p2}, Lcom/squareup/noho/NohoRow;->setDescriptionAppearanceId(I)V

    .line 67
    sget p2, Lcom/squareup/balance/applet/impl/R$string;->card_activity_declined:I

    invoke-virtual {p3, p2}, Lcom/squareup/noho/NohoRow;->setDescriptionId(I)V

    goto :goto_0

    .line 70
    :cond_1
    sget p2, Lcom/squareup/balance/applet/impl/R$style;->TransactionDescriptionNormal:I

    invoke-virtual {p3, p2}, Lcom/squareup/noho/NohoRow;->setDescriptionAppearanceId(I)V

    .line 71
    iget-object p2, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$setupRecycler$$inlined$adopt$lambda$2;->this$0:Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;

    invoke-static {p2}, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;->access$getTimeFormatter$p(Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;)Ljava/text/DateFormat;

    move-result-object p2

    iget-object v0, p1, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$CardActivityRow;->date:Ljava/util/Date;

    invoke-virtual {p2, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p3, p2}, Lcom/squareup/noho/NohoRow;->setDescription(Ljava/lang/CharSequence;)V

    .line 74
    :goto_0
    iget-object p2, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$setupRecycler$$inlined$adopt$lambda$2;->this$0:Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;

    invoke-static {p2}, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;->access$getMoneyFormatter$p(Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;)Lcom/squareup/text/Formatter;

    move-result-object p2

    iget-object v0, p1, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$CardActivityRow;->money:Lcom/squareup/protos/common/Money;

    invoke-interface {p2, v0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    .line 75
    iget-boolean v0, p1, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$CardActivityRow;->isDeclined:Z

    if-eqz v0, :cond_2

    const-string/jumbo v0, "value"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$setupRecycler$$inlined$adopt$lambda$2;->$strikeThroughSpan$inlined:Landroid/text/style/StrikethroughSpan;

    check-cast v0, Landroid/text/style/CharacterStyle;

    invoke-static {p2, v0}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    :cond_2
    invoke-virtual {p3, p2}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 76
    new-instance p2, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$setupRecycler$$inlined$adopt$lambda$2$1;

    invoke-direct {p2, p1, p0}, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$setupRecycler$$inlined$adopt$lambda$2$1;-><init>(Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$CardActivityRow;Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$setupRecycler$$inlined$adopt$lambda$2;)V

    check-cast p2, Landroid/view/View$OnClickListener;

    invoke-static {p2}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p1

    check-cast p1, Landroid/view/View$OnClickListener;

    invoke-virtual {p3, p1}, Lcom/squareup/noho/NohoRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
