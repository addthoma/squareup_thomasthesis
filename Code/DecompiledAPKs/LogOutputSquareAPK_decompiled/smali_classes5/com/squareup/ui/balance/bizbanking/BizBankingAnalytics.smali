.class public final Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;
.super Ljava/lang/Object;
.source "BizBankingAnalytics.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0008\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\n\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010\u0005\u001a\u00020\u0006J\u0006\u0010\u0007\u001a\u00020\u0006J\u0006\u0010\u0008\u001a\u00020\u0006J\u0006\u0010\t\u001a\u00020\u0006J\u0006\u0010\n\u001a\u00020\u0006J\u0006\u0010\u000b\u001a\u00020\u0006J\u0006\u0010\u000c\u001a\u00020\u0006J\u0016\u0010\r\u001a\u00020\u00062\u000e\u0010\u000e\u001a\n\u0012\u0004\u0012\u00020\u0010\u0018\u00010\u000fJ\u0006\u0010\u0011\u001a\u00020\u0006J\u0010\u0010\u0012\u001a\u00020\u00062\u0006\u0010\u0013\u001a\u00020\u0014H\u0002J\u0006\u0010\u0015\u001a\u00020\u0006J\u0010\u0010\u0016\u001a\u00020\u00062\u0006\u0010\u0013\u001a\u00020\u0014H\u0002J\u0006\u0010\u0017\u001a\u00020\u0006J\u0006\u0010\u0018\u001a\u00020\u0006J\u0006\u0010\u0019\u001a\u00020\u0006J\u0006\u0010\u001a\u001a\u00020\u0006J\u0006\u0010\u001b\u001a\u00020\u0006J\u0006\u0010\u001c\u001a\u00020\u0006J\u001e\u0010\u001d\u001a\u00020\u00062\u0006\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020\u001f2\u0006\u0010!\u001a\u00020\"J\u0006\u0010#\u001a\u00020\u0006J\u0006\u0010$\u001a\u00020\u0006J\u000e\u0010%\u001a\u00020\u00062\u0006\u0010&\u001a\u00020\'J\u0006\u0010(\u001a\u00020\u0006J\u0006\u0010)\u001a\u00020\u0006J\u0006\u0010*\u001a\u00020\u0006R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006+"
    }
    d2 = {
        "Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;",
        "",
        "bizBankingAnalyticsLogger",
        "Lcom/squareup/ui/balance/bizbanking/BizBankingAnalyticsLogger;",
        "(Lcom/squareup/ui/balance/bizbanking/BizBankingAnalyticsLogger;)V",
        "logActiveSalesClick",
        "",
        "logBackFromTransferConfirmationClick",
        "logBackFromTransferDetailsClick",
        "logBackFromTransferResultsClick",
        "logBalanceHeaderView",
        "logBalanceLoadFailure",
        "logBankAccountSettingsClick",
        "logCardActivityResult",
        "newTransactions",
        "",
        "Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$CardActivityRow;",
        "logCardSpendTransactionDetailsClick",
        "logClick",
        "description",
        "",
        "logDepositSettingsClick",
        "logImpression",
        "logManageSquareCardClick",
        "logPendingDepositClick",
        "logShowBalanceClick",
        "logStartTransferClick",
        "logTransferConfirmationClick",
        "logTransferReportsClick",
        "logTransferRequested",
        "requested",
        "Lcom/squareup/protos/common/Money;",
        "maxAvailable",
        "isInstant",
        "",
        "logTransferResultsDoneClick",
        "logViewCardActivityClick",
        "maybeLogTransferResults",
        "requestState",
        "Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;",
        "onFreezeLearnMore",
        "onFreezeVerify",
        "onViewedSuspendedDeposits",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final bizBankingAnalyticsLogger:Lcom/squareup/ui/balance/bizbanking/BizBankingAnalyticsLogger;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/balance/bizbanking/BizBankingAnalyticsLogger;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "bizBankingAnalyticsLogger"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;->bizBankingAnalyticsLogger:Lcom/squareup/ui/balance/bizbanking/BizBankingAnalyticsLogger;

    return-void
.end method

.method private final logClick(Ljava/lang/String;)V
    .locals 1

    .line 162
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;->bizBankingAnalyticsLogger:Lcom/squareup/ui/balance/bizbanking/BizBankingAnalyticsLogger;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/balance/bizbanking/BizBankingAnalyticsLogger;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method private final logImpression(Ljava/lang/String;)V
    .locals 1

    .line 166
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;->bizBankingAnalyticsLogger:Lcom/squareup/ui/balance/bizbanking/BizBankingAnalyticsLogger;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/balance/bizbanking/BizBankingAnalyticsLogger;->logImpression(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final logActiveSalesClick()V
    .locals 1

    const-string v0, "Deposits: Active Sales"

    .line 100
    invoke-direct {p0, v0}, Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public final logBackFromTransferConfirmationClick()V
    .locals 1

    const-string v0, "Bank Transfer Confirmation: Back"

    .line 56
    invoke-direct {p0, v0}, Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public final logBackFromTransferDetailsClick()V
    .locals 1

    const-string v0, "Bank Transfer: Exit"

    .line 52
    invoke-direct {p0, v0}, Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public final logBackFromTransferResultsClick()V
    .locals 1

    const-string v0, "Bank Transfer Result: Exit"

    .line 84
    invoke-direct {p0, v0}, Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public final logBalanceHeaderView()V
    .locals 1

    const-string v0, "Balance: Balance"

    .line 123
    invoke-direct {p0, v0}, Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;->logImpression(Ljava/lang/String;)V

    return-void
.end method

.method public final logBalanceLoadFailure()V
    .locals 1

    const-string v0, "Balance: Unable To Load Balance"

    .line 127
    invoke-direct {p0, v0}, Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;->logImpression(Ljava/lang/String;)V

    return-void
.end method

.method public final logBankAccountSettingsClick()V
    .locals 1

    const-string v0, "Bank Transfer Confirmation: Change Bank Account"

    .line 76
    invoke-direct {p0, v0}, Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public final logCardActivityResult(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsScreen$CardActivityRow;",
            ">;)V"
        }
    .end annotation

    if-nez p1, :cond_0

    const-string p1, "Card Spend: Cannot Load Transactions"

    goto :goto_0

    .line 133
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_1

    const-string p1, "Card Spend: No Transactions"

    goto :goto_0

    :cond_1
    const-string p1, "Card Spend: Transactions"

    .line 136
    :goto_0
    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;->logImpression(Ljava/lang/String;)V

    return-void
.end method

.method public final logCardSpendTransactionDetailsClick()V
    .locals 1

    const-string v0, "Card Spend: Transaction Details"

    .line 88
    invoke-direct {p0, v0}, Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public final logDepositSettingsClick()V
    .locals 1

    const-string v0, "Bank Transfer Confirmation: Change Debit Card"

    .line 72
    invoke-direct {p0, v0}, Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public final logManageSquareCardClick()V
    .locals 1

    const-string v0, "Deposits: Square Card"

    .line 92
    invoke-direct {p0, v0}, Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public final logPendingDepositClick()V
    .locals 1

    const-string v0, "Deposits: Pending Deposit"

    .line 104
    invoke-direct {p0, v0}, Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public final logShowBalanceClick()V
    .locals 1

    const-string v0, "Balance: Balance"

    .line 48
    invoke-direct {p0, v0}, Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public final logStartTransferClick()V
    .locals 1

    const-string v0, "Balance: Transfer to Bank"

    .line 60
    invoke-direct {p0, v0}, Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public final logTransferConfirmationClick()V
    .locals 1

    const-string v0, "Bank Transfer Confirmation: Transfer"

    .line 68
    invoke-direct {p0, v0}, Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public final logTransferReportsClick()V
    .locals 1

    const-string v0, "Deposits: Deposit Reports"

    .line 96
    invoke-direct {p0, v0}, Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public final logTransferRequested(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Z)V
    .locals 1

    const-string v0, "requested"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "maxAvailable"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 112
    invoke-static {p1, p2}, Lcom/squareup/money/MoneyMath;->isEqual(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result p1

    if-eqz p3, :cond_0

    if-eqz p1, :cond_0

    const-string p1, "Bank Transfer: Continue With Instant Max Amount"

    goto :goto_0

    :cond_0
    if-eqz p3, :cond_1

    if-nez p1, :cond_1

    const-string p1, "Bank Transfer: Continue With Instant Partial Amount"

    goto :goto_0

    :cond_1
    if-nez p3, :cond_2

    if-eqz p1, :cond_2

    const-string p1, "Bank Transfer: Continue With Regular Max Amount"

    goto :goto_0

    :cond_2
    const-string p1, "Bank Transfer: Continue With Regular Partial Amount"

    .line 119
    :goto_0
    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public final logTransferResultsDoneClick()V
    .locals 1

    const-string v0, "Bank Transfer Result: Done"

    .line 80
    invoke-direct {p0, v0}, Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public final logViewCardActivityClick()V
    .locals 1

    const-string v0, "Balance: Card Spend"

    .line 64
    invoke-direct {p0, v0}, Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public final maybeLogTransferResults(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;)V
    .locals 1

    const-string v0, "requestState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 140
    sget-object v0, Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_3

    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    const/4 v0, 0x3

    if-eq p1, v0, :cond_1

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    return-void

    .line 144
    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_1
    const-string p1, "Bank Transfer Result: Transfer in Progress"

    goto :goto_0

    :cond_2
    const-string p1, "Bank Transfer Result: Transfer Error"

    goto :goto_0

    :cond_3
    const-string p1, "Bank Transfer Result: Successful"

    .line 146
    :goto_0
    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;->logImpression(Ljava/lang/String;)V

    return-void
.end method

.method public final onFreezeLearnMore()V
    .locals 1

    const-string v0, "Account Freeze Deposits Suspended Overview: Learn More"

    .line 154
    invoke-direct {p0, v0}, Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public final onFreezeVerify()V
    .locals 1

    const-string v0, "Account Freeze Deposits Suspended Overview: Verify Account"

    .line 158
    invoke-direct {p0, v0}, Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;->logClick(Ljava/lang/String;)V

    return-void
.end method

.method public final onViewedSuspendedDeposits()V
    .locals 2

    .line 150
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/BizBankingAnalytics;->bizBankingAnalyticsLogger:Lcom/squareup/ui/balance/bizbanking/BizBankingAnalyticsLogger;

    const-string v1, "Account Freeze Deposits Suspended Overview"

    invoke-virtual {v0, v1}, Lcom/squareup/ui/balance/bizbanking/BizBankingAnalyticsLogger;->logView(Ljava/lang/String;)V

    return-void
.end method
