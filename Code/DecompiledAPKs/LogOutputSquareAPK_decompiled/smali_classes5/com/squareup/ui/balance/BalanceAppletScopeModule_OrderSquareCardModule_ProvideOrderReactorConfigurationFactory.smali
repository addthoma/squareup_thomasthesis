.class public final Lcom/squareup/ui/balance/BalanceAppletScopeModule_OrderSquareCardModule_ProvideOrderReactorConfigurationFactory;
.super Ljava/lang/Object;
.source "BalanceAppletScopeModule_OrderSquareCardModule_ProvideOrderReactorConfigurationFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/balance/BalanceAppletScopeModule_OrderSquareCardModule_ProvideOrderReactorConfigurationFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/mailorder/OrderReactor$Configuration;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/ui/balance/BalanceAppletScopeModule_OrderSquareCardModule_ProvideOrderReactorConfigurationFactory;
    .locals 1

    .line 24
    invoke-static {}, Lcom/squareup/ui/balance/BalanceAppletScopeModule_OrderSquareCardModule_ProvideOrderReactorConfigurationFactory$InstanceHolder;->access$000()Lcom/squareup/ui/balance/BalanceAppletScopeModule_OrderSquareCardModule_ProvideOrderReactorConfigurationFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideOrderReactorConfiguration()Lcom/squareup/mailorder/OrderReactor$Configuration;
    .locals 2

    .line 28
    invoke-static {}, Lcom/squareup/ui/balance/BalanceAppletScopeModule$OrderSquareCardModule;->provideOrderReactorConfiguration()Lcom/squareup/mailorder/OrderReactor$Configuration;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/mailorder/OrderReactor$Configuration;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/mailorder/OrderReactor$Configuration;
    .locals 1

    .line 19
    invoke-static {}, Lcom/squareup/ui/balance/BalanceAppletScopeModule_OrderSquareCardModule_ProvideOrderReactorConfigurationFactory;->provideOrderReactorConfiguration()Lcom/squareup/mailorder/OrderReactor$Configuration;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ui/balance/BalanceAppletScopeModule_OrderSquareCardModule_ProvideOrderReactorConfigurationFactory;->get()Lcom/squareup/mailorder/OrderReactor$Configuration;

    move-result-object v0

    return-object v0
.end method
