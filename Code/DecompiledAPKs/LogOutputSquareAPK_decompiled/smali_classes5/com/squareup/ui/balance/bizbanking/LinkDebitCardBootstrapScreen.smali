.class public final Lcom/squareup/ui/balance/bizbanking/LinkDebitCardBootstrapScreen;
.super Lcom/squareup/container/BootstrapTreeKey;
.source "LinkDebitCardBootstrapScreen.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0016J\u0008\u0010\t\u001a\u00020\nH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/ui/balance/bizbanking/LinkDebitCardBootstrapScreen;",
        "Lcom/squareup/container/BootstrapTreeKey;",
        "linkDebitCardWorkflowStartArg",
        "Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;",
        "(Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;)V",
        "doRegister",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "getParentKey",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final linkDebitCardWorkflowStartArg:Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;


# direct methods
.method public constructor <init>(Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;)V
    .locals 1

    const-string v0, "linkDebitCardWorkflowStartArg"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0}, Lcom/squareup/container/BootstrapTreeKey;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/LinkDebitCardBootstrapScreen;->linkDebitCardWorkflowStartArg:Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;

    return-void
.end method


# virtual methods
.method public doRegister(Lmortar/MortarScope;)V
    .locals 2

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    sget-object v0, Lcom/squareup/debitcard/LinkDebitCardWorkflowRunner;->Companion:Lcom/squareup/debitcard/LinkDebitCardWorkflowRunner$Companion;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/LinkDebitCardBootstrapScreen;->linkDebitCardWorkflowStartArg:Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/debitcard/LinkDebitCardWorkflowRunner$Companion;->startNewWorkflow(Lmortar/MortarScope;Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;)V

    return-void
.end method

.method public getParentKey()Ljava/lang/Object;
    .locals 3

    .line 15
    new-instance v0, Lcom/squareup/debitcard/LinkDebitCardScope;

    sget-object v1, Lcom/squareup/ui/balance/BalanceAppletScope;->INSTANCE:Lcom/squareup/ui/balance/BalanceAppletScope;

    const-string v2, "BalanceAppletScope.INSTANCE"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-direct {v0, v1}, Lcom/squareup/debitcard/LinkDebitCardScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-object v0
.end method
