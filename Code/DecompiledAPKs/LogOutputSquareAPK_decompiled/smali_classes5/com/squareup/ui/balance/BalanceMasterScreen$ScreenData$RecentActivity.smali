.class public Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;
.super Ljava/lang/Object;
.source "BalanceMasterScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RecentActivity"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;
    }
.end annotation


# instance fields
.field private final activityType:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;

.field private final balanceActivity:Lcom/squareup/balance/activity/data/BalanceActivity;

.field private final cardActivity:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bizbank/CardActivityEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final depositActivity:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;

.field private final title:I


# direct methods
.method private constructor <init>(Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;ILjava/util/List;Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;Lcom/squareup/balance/activity/data/BalanceActivity;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;",
            "I",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bizbank/CardActivityEvent;",
            ">;",
            "Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;",
            "Lcom/squareup/balance/activity/data/BalanceActivity;",
            ")V"
        }
    .end annotation

    .line 117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 118
    iput-object p1, p0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;->activityType:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;

    .line 119
    iput p2, p0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;->title:I

    .line 120
    iput-object p3, p0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;->cardActivity:Ljava/util/List;

    .line 121
    iput-object p4, p0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;->depositActivity:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;

    .line 122
    iput-object p5, p0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;->balanceActivity:Lcom/squareup/balance/activity/data/BalanceActivity;

    return-void
.end method

.method private static asCardActivity(Ljava/util/List;)Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bizbank/CardActivityEvent;",
            ">;)",
            "Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;"
        }
    .end annotation

    .line 190
    new-instance v6, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;

    sget-object v1, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;->CARD:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;

    sget v2, Lcom/squareup/balance/applet/impl/R$string;->recent_activity_balance_title_uppercase:I

    new-instance v0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity$Builder;-><init>()V

    .line 193
    invoke-virtual {v0}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity$Builder;->build()Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;

    move-result-object v4

    const/4 v5, 0x0

    move-object v0, v6

    move-object v3, p0

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;-><init>(Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;ILjava/util/List;Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;Lcom/squareup/balance/activity/data/BalanceActivity;)V

    return-object v6
.end method

.method private static asDepositActivity(Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;)Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;
    .locals 7

    .line 207
    new-instance v6, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;

    sget-object v1, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;->DEPOSIT:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;

    sget v2, Lcom/squareup/balance/applet/impl/R$string;->recent_activity_deposits_title_uppercase:I

    .line 209
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v3

    const/4 v5, 0x0

    move-object v0, v6

    move-object v4, p0

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;-><init>(Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;ILjava/util/List;Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;Lcom/squareup/balance/activity/data/BalanceActivity;)V

    return-object v6
.end method

.method private static asNone()Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;
    .locals 7

    .line 185
    new-instance v6, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;

    sget-object v1, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;->NONE:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v3

    new-instance v0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity$Builder;-><init>()V

    .line 186
    invoke-virtual {v0}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity$Builder;->build()Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;

    move-result-object v4

    const/4 v2, -0x1

    const/4 v5, 0x0

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;-><init>(Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;ILjava/util/List;Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;Lcom/squareup/balance/activity/data/BalanceActivity;)V

    return-object v6
.end method

.method private static asUnifiedActivity(Lcom/squareup/balance/activity/data/BalanceActivity;)Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;
    .locals 7

    .line 198
    new-instance v6, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;

    sget-object v1, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;->UNIFIED_ACTIVITY:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;

    sget v2, Lcom/squareup/balance/applet/impl/R$string;->recent_activity_unified_activity_title:I

    .line 200
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v3

    new-instance v0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity$Builder;-><init>()V

    .line 201
    invoke-virtual {v0}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity$Builder;->build()Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;

    move-result-object v4

    move-object v0, v6

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;-><init>(Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;ILjava/util/List;Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;Lcom/squareup/balance/activity/data/BalanceActivity;)V

    return-object v6
.end method

.method public static recentActivity(Ljava/util/List;Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;Lcom/squareup/balance/activity/data/BalanceActivity;)Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bizbank/CardActivityEvent;",
            ">;",
            "Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;",
            "Lcom/squareup/balance/activity/data/BalanceActivity;",
            ")",
            "Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;"
        }
    .end annotation

    if-nez p0, :cond_0

    if-nez p1, :cond_0

    if-nez p2, :cond_0

    .line 172
    invoke-static {}, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;->asNone()Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;

    move-result-object p0

    return-object p0

    :cond_0
    if-eqz p2, :cond_1

    .line 176
    invoke-static {p2}, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;->asUnifiedActivity(Lcom/squareup/balance/activity/data/BalanceActivity;)Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;

    move-result-object p0

    return-object p0

    :cond_1
    if-eqz p1, :cond_2

    .line 178
    invoke-static {p1}, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;->asDepositActivity(Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;)Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;

    move-result-object p0

    return-object p0

    .line 180
    :cond_2
    invoke-static {p0}, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;->asCardActivity(Ljava/util/List;)Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public getCardActivityEvents()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bizbank/CardActivityEvent;",
            ">;"
        }
    .end annotation

    .line 126
    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;->activityType:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;

    sget-object v1, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;->CARD:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Requesting card activity when type is not CARD. Type is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;->activityType:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;

    .line 127
    invoke-virtual {v2}, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 126
    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 128
    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;->cardActivity:Ljava/util/List;

    return-object v0
.end method

.method public getDepositActivity()Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;
    .locals 3

    .line 132
    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;->activityType:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;

    sget-object v1, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;->DEPOSIT:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Requesting deposit activity events when type is not DEPOSIT. Type is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;->activityType:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;

    .line 134
    invoke-virtual {v2}, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 132
    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 135
    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;->depositActivity:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;

    return-object v0
.end method

.method public getUnifiedActivity()Lcom/squareup/balance/activity/data/BalanceActivity;
    .locals 5

    .line 139
    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;->activityType:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;

    sget-object v1, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;->UNIFIED_ACTIVITY:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Requesting balance activity when type is not UNIFIED_ACTIVITY. Type is "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;->activityType:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;

    .line 141
    invoke-virtual {v4}, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 139
    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 142
    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;->balanceActivity:Lcom/squareup/balance/activity/data/BalanceActivity;

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    const-string v0, "Balance Activity should not be null for UNIFIED_ACTIVITY"

    invoke-static {v2, v0}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 144
    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;->balanceActivity:Lcom/squareup/balance/activity/data/BalanceActivity;

    return-object v0
.end method

.method public hasRecentActivity()Z
    .locals 1

    .line 160
    invoke-virtual {p0}, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;->hasRecentCardActivity()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;->hasRecentDepositActivity()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;->hasUnifiedActivity()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public hasRecentCardActivity()Z
    .locals 2

    .line 148
    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;->activityType:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;

    sget-object v1, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;->CARD:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasRecentDepositActivity()Z
    .locals 2

    .line 152
    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;->activityType:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;

    sget-object v1, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;->DEPOSIT:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasUnifiedActivity()Z
    .locals 2

    .line 156
    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;->activityType:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;

    sget-object v1, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;->UNIFIED_ACTIVITY:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public title()I
    .locals 1

    .line 164
    iget v0, p0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;->title:I

    return v0
.end method
