.class public interface abstract Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$Runner;
.super Ljava/lang/Object;
.source "TransferToBankScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation


# virtual methods
.method public abstract onBackFromTransferToBank()V
.end method

.method public abstract onInstantClicked()V
.end method

.method public abstract onNextClickedFromTransferToBank(Lcom/squareup/protos/common/Money;Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;)V
.end method

.method public abstract onStandardClicked()V
.end method

.method public abstract transferToBankScreenData()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData;",
            ">;"
        }
    .end annotation
.end method

.method public abstract updateFee(Lcom/squareup/protos/common/Money;)V
.end method
