.class final Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator$AmountTextWatcher;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "EnterAmountCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "AmountTextWatcher"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0082\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0012\u0010\u0005\u001a\u00020\u00062\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0008H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator$AmountTextWatcher;",
        "Lcom/squareup/text/EmptyTextWatcher;",
        "screen",
        "Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;",
        "(Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;)V",
        "afterTextChanged",
        "",
        "editable",
        "Landroid/text/Editable;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final screen:Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;

.field final synthetic this$0:Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;",
            ")V"
        }
    .end annotation

    const-string v0, "screen"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 197
    iput-object p1, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator$AmountTextWatcher;->this$0:Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;

    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    iput-object p2, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator$AmountTextWatcher;->screen:Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 1

    .line 199
    iget-object p1, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator$AmountTextWatcher;->this$0:Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;

    iget-object v0, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator$AmountTextWatcher;->screen:Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;

    invoke-static {p1, v0}, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;->access$notifyBalanceUpdated(Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;)V

    return-void
.end method
