.class public final Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;
.super Ljava/lang/Object;
.source "EnterAmountProps.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B\u001f\u0012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0007J\u000b\u0010\r\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\t\u0010\u000e\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u000f\u001a\u00020\u0005H\u00c6\u0003J)\u0010\u0010\u001a\u00020\u00002\n\u0008\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0005H\u00c6\u0001J\t\u0010\u0011\u001a\u00020\u0012H\u00d6\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u00d6\u0003J\t\u0010\u0017\u001a\u00020\u0012H\u00d6\u0001J\t\u0010\u0018\u001a\u00020\u0019H\u00d6\u0001J\u0019\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u0012H\u00d6\u0001R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\u000b\u00a8\u0006\u001f"
    }
    d2 = {
        "Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;",
        "Landroid/os/Parcelable;",
        "cardInfo",
        "Lcom/squareup/protos/client/deposits/CardInfo;",
        "minAllowedAmount",
        "Lcom/squareup/protos/common/Money;",
        "maxAllowedAmount",
        "(Lcom/squareup/protos/client/deposits/CardInfo;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V",
        "getCardInfo",
        "()Lcom/squareup/protos/client/deposits/CardInfo;",
        "getMaxAllowedAmount",
        "()Lcom/squareup/protos/common/Money;",
        "getMinAllowedAmount",
        "component1",
        "component2",
        "component3",
        "copy",
        "describeContents",
        "",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final cardInfo:Lcom/squareup/protos/client/deposits/CardInfo;

.field private final maxAllowedAmount:Lcom/squareup/protos/common/Money;

.field private final minAllowedAmount:Lcom/squareup/protos/common/Money;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps$Creator;

    invoke-direct {v0}, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps$Creator;-><init>()V

    sput-object v0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/deposits/CardInfo;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V
    .locals 1

    const-string v0, "minAllowedAmount"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "maxAllowedAmount"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;->cardInfo:Lcom/squareup/protos/client/deposits/CardInfo;

    iput-object p2, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;->minAllowedAmount:Lcom/squareup/protos/common/Money;

    iput-object p3, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;->maxAllowedAmount:Lcom/squareup/protos/common/Money;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;Lcom/squareup/protos/client/deposits/CardInfo;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;ILjava/lang/Object;)Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;->cardInfo:Lcom/squareup/protos/client/deposits/CardInfo;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;->minAllowedAmount:Lcom/squareup/protos/common/Money;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;->maxAllowedAmount:Lcom/squareup/protos/common/Money;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;->copy(Lcom/squareup/protos/client/deposits/CardInfo;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/protos/client/deposits/CardInfo;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;->cardInfo:Lcom/squareup/protos/client/deposits/CardInfo;

    return-object v0
.end method

.method public final component2()Lcom/squareup/protos/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;->minAllowedAmount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final component3()Lcom/squareup/protos/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;->maxAllowedAmount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final copy(Lcom/squareup/protos/client/deposits/CardInfo;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;
    .locals 1

    const-string v0, "minAllowedAmount"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "maxAllowedAmount"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;-><init>(Lcom/squareup/protos/client/deposits/CardInfo;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;

    iget-object v0, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;->cardInfo:Lcom/squareup/protos/client/deposits/CardInfo;

    iget-object v1, p1, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;->cardInfo:Lcom/squareup/protos/client/deposits/CardInfo;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;->minAllowedAmount:Lcom/squareup/protos/common/Money;

    iget-object v1, p1, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;->minAllowedAmount:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;->maxAllowedAmount:Lcom/squareup/protos/common/Money;

    iget-object p1, p1, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;->maxAllowedAmount:Lcom/squareup/protos/common/Money;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCardInfo()Lcom/squareup/protos/client/deposits/CardInfo;
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;->cardInfo:Lcom/squareup/protos/client/deposits/CardInfo;

    return-object v0
.end method

.method public final getMaxAllowedAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;->maxAllowedAmount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final getMinAllowedAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;->minAllowedAmount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;->cardInfo:Lcom/squareup/protos/client/deposits/CardInfo;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;->minAllowedAmount:Lcom/squareup/protos/common/Money;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;->maxAllowedAmount:Lcom/squareup/protos/common/Money;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "EnterAmountProps(cardInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;->cardInfo:Lcom/squareup/protos/client/deposits/CardInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", minAllowedAmount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;->minAllowedAmount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", maxAllowedAmount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;->maxAllowedAmount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;->cardInfo:Lcom/squareup/protos/client/deposits/CardInfo;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    iget-object p2, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;->minAllowedAmount:Lcom/squareup/protos/common/Money;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    iget-object p2, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountProps;->maxAllowedAmount:Lcom/squareup/protos/common/Money;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    return-void
.end method
