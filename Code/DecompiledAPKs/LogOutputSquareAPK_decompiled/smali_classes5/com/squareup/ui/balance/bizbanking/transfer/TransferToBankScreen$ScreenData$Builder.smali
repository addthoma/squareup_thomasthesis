.class public Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;
.super Ljava/lang/Object;
.source "TransferToBankScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private formattedFee:Ljava/lang/CharSequence;

.field private formattedFeeTotalAmount:Ljava/lang/CharSequence;

.field private formattedMaxDeposit:Ljava/lang/CharSequence;

.field private instantDepositClickable:Z

.field private isFeeFixedAmount:Z

.field private maxDeposit:Lcom/squareup/protos/common/Money;

.field private showInstantDeposit:Z

.field private transferSpeed:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;)Z
    .locals 0

    .line 66
    iget-boolean p0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;->showInstantDeposit:Z

    return p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;
    .locals 0

    .line 66
    iget-object p0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;->transferSpeed:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;)Z
    .locals 0

    .line 66
    iget-boolean p0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;->instantDepositClickable:Z

    return p0
.end method

.method static synthetic access$300(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;)Z
    .locals 0

    .line 66
    iget-boolean p0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;->isFeeFixedAmount:Z

    return p0
.end method

.method static synthetic access$400(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;)Lcom/squareup/protos/common/Money;
    .locals 0

    .line 66
    iget-object p0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;->maxDeposit:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;)Ljava/lang/CharSequence;
    .locals 0

    .line 66
    iget-object p0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;->formattedMaxDeposit:Ljava/lang/CharSequence;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;)Ljava/lang/CharSequence;
    .locals 0

    .line 66
    iget-object p0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;->formattedFee:Ljava/lang/CharSequence;

    return-object p0
.end method

.method static synthetic access$700(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;)Ljava/lang/CharSequence;
    .locals 0

    .line 66
    iget-object p0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;->formattedFeeTotalAmount:Ljava/lang/CharSequence;

    return-object p0
.end method


# virtual methods
.method public build()Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData;
    .locals 2

    .line 117
    new-instance v0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData;-><init>(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$1;)V

    return-object v0
.end method

.method public formattedFee(Ljava/lang/CharSequence;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;
    .locals 0

    .line 107
    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;->formattedFee:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public formattedFeeTotalAmount(Ljava/lang/CharSequence;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;
    .locals 0

    .line 112
    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;->formattedFeeTotalAmount:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public formattedMaxDeposit(Ljava/lang/CharSequence;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;
    .locals 0

    .line 102
    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;->formattedMaxDeposit:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public instantDepositClickable(Z)Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;
    .locals 0

    .line 87
    iput-boolean p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;->instantDepositClickable:Z

    return-object p0
.end method

.method public isFeeFixedAmount(Z)Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;
    .locals 0

    .line 92
    iput-boolean p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;->isFeeFixedAmount:Z

    return-object p0
.end method

.method public maxDeposit(Lcom/squareup/protos/common/Money;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;
    .locals 0

    .line 97
    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;->maxDeposit:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public showInstantDeposit(Z)Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;
    .locals 0

    .line 77
    iput-boolean p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;->showInstantDeposit:Z

    return-object p0
.end method

.method public transferSpeed(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;
    .locals 0

    .line 82
    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$ScreenData$Builder;->transferSpeed:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;

    return-object p0
.end method
