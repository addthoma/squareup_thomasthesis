.class public Lcom/squareup/ui/XableAutoCompleteEditText;
.super Lcom/squareup/ui/BaseXableEditText;
.source "XableAutoCompleteEditText.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/BaseXableEditText<",
        "Lcom/squareup/widgets/SelectableAutoCompleteEditText;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 15
    sget v0, Lcom/squareup/widgets/pos/R$layout;->xable_auto_complete_edit_text:I

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/ui/BaseXableEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method public setAutoCompleteAdapter(Landroid/widget/ArrayAdapter;)V
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/ui/XableAutoCompleteEditText;->editText:Landroid/widget/EditText;

    check-cast v0, Lcom/squareup/widgets/SelectableAutoCompleteEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/SelectableAutoCompleteEditText;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method
