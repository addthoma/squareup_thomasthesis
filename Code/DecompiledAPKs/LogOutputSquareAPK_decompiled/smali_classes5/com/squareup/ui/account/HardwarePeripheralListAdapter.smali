.class public abstract Lcom/squareup/ui/account/HardwarePeripheralListAdapter;
.super Landroid/widget/BaseAdapter;
.source "HardwarePeripheralListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/account/HardwarePeripheralListAdapter$RowType;,
        Lcom/squareup/ui/account/HardwarePeripheralListAdapter$StringHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/widget/BaseAdapter;"
    }
.end annotation


# static fields
.field public static final HEADER_ROW:I = 0x0

.field public static final ITEM_ROW:I = 0x1

.field public static final MESSAGE_ROW:I = 0x2

.field public static final NONE_FOUND_ROW:I = 0x3

.field public static final UNSUPPORTED_HEADER_ROW:I = 0x4

.field public static final UNSUPPORTED_ITEM_ROW:I = 0x5


# direct methods
.method protected constructor <init>()V
    .locals 0

    .line 62
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method protected buildAndBindEmptyRow(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    if-nez p1, :cond_0

    .line 96
    sget p1, Lcom/squareup/settingsapplet/R$layout;->hardware_peripheral_empty_row:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    .line 99
    :cond_0
    sget p2, Lcom/squareup/settingsapplet/R$id;->title:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/squareup/ui/account/HardwarePeripheralListAdapter;->getStrings()Lcom/squareup/ui/account/HardwarePeripheralListAdapter$StringHolder;

    move-result-object v0

    iget v0, v0, Lcom/squareup/ui/account/HardwarePeripheralListAdapter$StringHolder;->emptyRow:I

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(I)V

    return-object p1
.end method

.method protected buildAndBindHeaderRow(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    if-nez p1, :cond_0

    .line 111
    new-instance p1, Lcom/squareup/widgets/list/SectionHeaderRow;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-virtual {p0}, Lcom/squareup/ui/account/HardwarePeripheralListAdapter;->getStrings()Lcom/squareup/ui/account/HardwarePeripheralListAdapter$StringHolder;

    move-result-object v0

    iget v0, v0, Lcom/squareup/ui/account/HardwarePeripheralListAdapter$StringHolder;->availableSection:I

    invoke-direct {p1, p2, v0}, Lcom/squareup/widgets/list/SectionHeaderRow;-><init>(Landroid/content/Context;I)V

    :cond_0
    return-object p1
.end method

.method protected buildAndBindHelpRow(Landroid/widget/LinearLayout;Landroid/view/ViewGroup;)Landroid/widget/LinearLayout;
    .locals 3

    if-nez p1, :cond_0

    .line 125
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    sget v0, Lcom/squareup/settingsapplet/R$layout;->hardware_settings_message:I

    const/4 v1, 0x0

    .line 126
    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    .line 128
    sget v0, Lcom/squareup/settingsapplet/R$id;->message_text:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    .line 129
    new-instance v1, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-direct {v1, p2}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    .line 130
    invoke-virtual {p0}, Lcom/squareup/ui/account/HardwarePeripheralListAdapter;->getStrings()Lcom/squareup/ui/account/HardwarePeripheralListAdapter$StringHolder;

    move-result-object p2

    iget p2, p2, Lcom/squareup/ui/account/HardwarePeripheralListAdapter$StringHolder;->itemHelpMessage:I

    const-string v2, "support_center"

    invoke-virtual {v1, p2, v2}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p2

    sget v1, Lcom/squareup/cardreader/ui/R$string;->supported_hardware_url:I

    .line 131
    invoke-virtual {p2, v1}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p2

    sget v1, Lcom/squareup/cardreader/ui/R$string;->support_center:I

    .line 132
    invoke-virtual {p2, v1}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p2

    .line 133
    invoke-virtual {p2}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object p2

    .line 129
    invoke-virtual {v0, p2}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-object p1
.end method

.method protected abstract buildAndBindItemRow(Lcom/squareup/ui/account/view/SmartLineRow;Ljava/lang/Object;Landroid/view/ViewGroup;)Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/account/view/SmartLineRow;",
            "TT;",
            "Landroid/view/ViewGroup;",
            ")",
            "Landroid/view/View;"
        }
    .end annotation
.end method

.method protected buildAndBindUnsupportedHeaderRow(Lcom/squareup/widgets/list/SectionHeaderRow;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    if-nez p1, :cond_0

    .line 118
    new-instance p1, Lcom/squareup/widgets/list/SectionHeaderRow;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-virtual {p0}, Lcom/squareup/ui/account/HardwarePeripheralListAdapter;->getStrings()Lcom/squareup/ui/account/HardwarePeripheralListAdapter$StringHolder;

    move-result-object v0

    iget v0, v0, Lcom/squareup/ui/account/HardwarePeripheralListAdapter$StringHolder;->unsupportedSection:I

    invoke-direct {p1, p2, v0}, Lcom/squareup/widgets/list/SectionHeaderRow;-><init>(Landroid/content/Context;I)V

    :cond_0
    return-object p1
.end method

.method protected abstract buildAndBindUnsupportedItemRow(Lcom/squareup/ui/account/view/SmartLineRow;Ljava/lang/Object;Landroid/view/ViewGroup;)Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/account/view/SmartLineRow;",
            "TT;",
            "Landroid/view/ViewGroup;",
            ")",
            "Landroid/view/View;"
        }
    .end annotation
.end method

.method public abstract getAvailableCount()I
.end method

.method public getCount()I
    .locals 3

    .line 66
    invoke-virtual {p0}, Lcom/squareup/ui/account/HardwarePeripheralListAdapter;->getUnsupportedCount()I

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    .line 70
    invoke-virtual {p0}, Lcom/squareup/ui/account/HardwarePeripheralListAdapter;->getAvailableCount()I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/2addr v0, v1

    :goto_0
    add-int/2addr v0, v1

    return v0

    .line 77
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/account/HardwarePeripheralListAdapter;->getAvailableCount()I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/2addr v0, v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/squareup/ui/account/HardwarePeripheralListAdapter;->getUnsupportedCount()I

    move-result v2

    add-int/2addr v0, v2

    goto :goto_0
.end method

.method public abstract getItem(I)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 6

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return p1

    .line 144
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/account/HardwarePeripheralListAdapter;->getCount()I

    move-result v0

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    const/4 v2, 0x2

    if-ne p1, v0, :cond_1

    return v2

    .line 146
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/ui/account/HardwarePeripheralListAdapter;->getUnsupportedCount()I

    move-result v0

    .line 147
    invoke-virtual {p0}, Lcom/squareup/ui/account/HardwarePeripheralListAdapter;->getAvailableCount()I

    move-result v3

    const/4 v4, 0x3

    if-nez v0, :cond_3

    if-nez v3, :cond_2

    return v4

    :cond_2
    return v1

    :cond_3
    const/4 v0, 0x4

    const/4 v5, 0x5

    if-nez v3, :cond_6

    if-ne p1, v1, :cond_4

    return v4

    :cond_4
    if-ne p1, v2, :cond_5

    return v0

    :cond_5
    return v5

    :cond_6
    add-int/lit8 v4, v3, 0x1

    if-ge p1, v4, :cond_7

    return v1

    :cond_7
    add-int/2addr v3, v2

    if-ge p1, v3, :cond_8

    return v0

    :cond_8
    return v5
.end method

.method protected abstract getStrings()Lcom/squareup/ui/account/HardwarePeripheralListAdapter$StringHolder;
.end method

.method public abstract getUnsupportedCount()I
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .line 174
    invoke-virtual {p0, p1}, Lcom/squareup/ui/account/HardwarePeripheralListAdapter;->getItemViewType(I)I

    move-result v0

    if-eqz v0, :cond_5

    const/4 v1, 0x1

    if-eq v0, v1, :cond_4

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 188
    check-cast p2, Lcom/squareup/ui/account/view/SmartLineRow;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/account/HardwarePeripheralListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {p0, p2, p1, p3}, Lcom/squareup/ui/account/HardwarePeripheralListAdapter;->buildAndBindUnsupportedItemRow(Lcom/squareup/ui/account/view/SmartLineRow;Ljava/lang/Object;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    return-object p1

    .line 191
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "Unknown type: "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 186
    :cond_1
    check-cast p2, Lcom/squareup/widgets/list/SectionHeaderRow;

    invoke-virtual {p0, p2, p3}, Lcom/squareup/ui/account/HardwarePeripheralListAdapter;->buildAndBindUnsupportedHeaderRow(Lcom/squareup/widgets/list/SectionHeaderRow;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    return-object p1

    .line 184
    :cond_2
    invoke-virtual {p0, p2, p3}, Lcom/squareup/ui/account/HardwarePeripheralListAdapter;->buildAndBindEmptyRow(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    return-object p1

    .line 180
    :cond_3
    check-cast p2, Landroid/widget/LinearLayout;

    invoke-virtual {p0, p2, p3}, Lcom/squareup/ui/account/HardwarePeripheralListAdapter;->buildAndBindHelpRow(Landroid/widget/LinearLayout;Landroid/view/ViewGroup;)Landroid/widget/LinearLayout;

    move-result-object p1

    return-object p1

    .line 182
    :cond_4
    check-cast p2, Lcom/squareup/ui/account/view/SmartLineRow;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/account/HardwarePeripheralListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {p0, p2, p1, p3}, Lcom/squareup/ui/account/HardwarePeripheralListAdapter;->buildAndBindItemRow(Lcom/squareup/ui/account/view/SmartLineRow;Ljava/lang/Object;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    return-object p1

    .line 178
    :cond_5
    invoke-virtual {p0, p2, p3}, Lcom/squareup/ui/account/HardwarePeripheralListAdapter;->buildAndBindHeaderRow(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x6

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1

    .line 139
    invoke-virtual {p0, p1}, Lcom/squareup/ui/account/HardwarePeripheralListAdapter;->getItemViewType(I)I

    move-result p1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
