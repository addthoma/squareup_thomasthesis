.class public final Lcom/squareup/ui/PaymentActivity$StartPaymentActivity;
.super Landroid/app/Activity;
.source "PaymentActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/PaymentActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StartPaymentActivity"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 278
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private startPaymentActivity()V
    .locals 2

    .line 292
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/squareup/ui/PaymentActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x6000000

    .line 293
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 297
    invoke-virtual {p0}, Lcom/squareup/ui/PaymentActivity$StartPaymentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/content/Intent;)Landroid/content/Intent;

    .line 299
    invoke-virtual {p0, v0}, Lcom/squareup/ui/PaymentActivity$StartPaymentActivity;->startActivity(Landroid/content/Intent;)V

    const/4 v0, 0x0

    .line 303
    invoke-virtual {p0, v0, v0}, Lcom/squareup/ui/PaymentActivity$StartPaymentActivity;->overridePendingTransition(II)V

    .line 307
    invoke-virtual {p0}, Lcom/squareup/ui/PaymentActivity$StartPaymentActivity;->finish()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0

    .line 280
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 281
    invoke-direct {p0}, Lcom/squareup/ui/PaymentActivity$StartPaymentActivity;->startPaymentActivity()V

    return-void
.end method

.method protected onResume()V
    .locals 0

    .line 285
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 288
    invoke-direct {p0}, Lcom/squareup/ui/PaymentActivity$StartPaymentActivity;->startPaymentActivity()V

    return-void
.end method
