.class public Lcom/squareup/ui/BaseXableEditText;
.super Landroid/widget/FrameLayout;
.source "BaseXableEditText.java"

# interfaces
.implements Lcom/squareup/widgets/EmailSuggestionHandler$SuggestionListener;
.implements Lcom/squareup/text/HasSelectableText;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/BaseXableEditText$ShowButtonTextWatcher;,
        Lcom/squareup/ui/BaseXableEditText$ShowButtonFocusListener;,
        Lcom/squareup/ui/BaseXableEditText$SavedState;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Landroid/widget/EditText;",
        ":",
        "Lcom/squareup/text/HasSelectableText;",
        ">",
        "Landroid/widget/FrameLayout;",
        "Lcom/squareup/widgets/EmailSuggestionHandler$SuggestionListener;",
        "Lcom/squareup/text/HasSelectableText;"
    }
.end annotation


# static fields
.field private static final REQUEST_FOCUS_JUST_BEFORE_LONG_PRESS:J


# instance fields
.field private final dismissOnClick:Z

.field protected final editText:Landroid/widget/EditText;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final fadeDurationMs:I

.field private final hideXButtonAction:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Landroid/view/View;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final requestFocusRunnable:Ljava/lang/Runnable;

.field private final showButtonFocusListener:Lcom/squareup/ui/BaseXableEditText$ShowButtonFocusListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/ui/BaseXableEditText<",
            "TT;>.ShowButtonFocus",
            "Listener;"
        }
    .end annotation
.end field

.field private final showButtonTextWatcher:Lcom/squareup/ui/BaseXableEditText$ShowButtonTextWatcher;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/ui/BaseXableEditText<",
            "TT;>.ShowButtonTextWatcher;"
        }
    .end annotation
.end field

.field private final showXOnFocus:Z

.field private final showXOnNonEmptyText:Z

.field private final xButton:Lcom/squareup/glyph/SquareGlyphView;

.field private final xButtonWidthMinusMargin:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 58
    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v0

    int-to-double v0, v0

    const-wide/high16 v2, 0x3fe8000000000000L    # 0.75

    mul-double v0, v0, v2

    double-to-long v0, v0

    sput-wide v0, Lcom/squareup/ui/BaseXableEditText;->REQUEST_FOCUS_JUST_BEFORE_LONG_PRESS:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .line 78
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 71
    new-instance v0, Lcom/squareup/ui/BaseXableEditText$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/BaseXableEditText$1;-><init>(Lcom/squareup/ui/BaseXableEditText;)V

    iput-object v0, p0, Lcom/squareup/ui/BaseXableEditText;->requestFocusRunnable:Ljava/lang/Runnable;

    .line 80
    invoke-static {p1, p3, p0}, Lcom/squareup/ui/BaseXableEditText;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 81
    sget p3, Lcom/squareup/widgets/pos/R$id;->xable_text_field:I

    invoke-static {p0, p3}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/EditText;

    iput-object p3, p0, Lcom/squareup/ui/BaseXableEditText;->editText:Landroid/widget/EditText;

    .line 82
    sget p3, Lcom/squareup/widgets/pos/R$id;->x_button:I

    invoke-static {p0, p3}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Lcom/squareup/glyph/SquareGlyphView;

    iput-object p3, p0, Lcom/squareup/ui/BaseXableEditText;->xButton:Lcom/squareup/glyph/SquareGlyphView;

    .line 84
    invoke-virtual {p0}, Lcom/squareup/ui/BaseXableEditText;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    const/high16 v0, 0x10e0000

    .line 85
    invoke-virtual {p3, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/squareup/ui/BaseXableEditText;->fadeDurationMs:I

    .line 87
    sget-object v0, Lcom/squareup/widgets/pos/R$styleable;->XableEditText:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 89
    sget p2, Lcom/squareup/widgets/pos/R$styleable;->XableEditText_dismissOnClick:I

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p2

    iput-boolean p2, p0, Lcom/squareup/ui/BaseXableEditText;->dismissOnClick:Z

    .line 90
    sget p2, Lcom/squareup/widgets/pos/R$styleable;->XableEditText_showXOnFocus:I

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p2

    iput-boolean p2, p0, Lcom/squareup/ui/BaseXableEditText;->showXOnFocus:Z

    .line 91
    sget p2, Lcom/squareup/widgets/pos/R$styleable;->XableEditText_showXOnNonEmptyText:I

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p2

    iput-boolean p2, p0, Lcom/squareup/ui/BaseXableEditText;->showXOnNonEmptyText:Z

    .line 93
    new-instance p2, Lcom/squareup/ui/BaseXableEditText$ShowButtonFocusListener;

    const/4 v1, 0x0

    invoke-direct {p2, p0, v1}, Lcom/squareup/ui/BaseXableEditText$ShowButtonFocusListener;-><init>(Lcom/squareup/ui/BaseXableEditText;Lcom/squareup/ui/BaseXableEditText$1;)V

    iput-object p2, p0, Lcom/squareup/ui/BaseXableEditText;->showButtonFocusListener:Lcom/squareup/ui/BaseXableEditText$ShowButtonFocusListener;

    .line 94
    new-instance p2, Lcom/squareup/ui/BaseXableEditText$ShowButtonTextWatcher;

    invoke-direct {p2, p0, v1}, Lcom/squareup/ui/BaseXableEditText$ShowButtonTextWatcher;-><init>(Lcom/squareup/ui/BaseXableEditText;Lcom/squareup/ui/BaseXableEditText$1;)V

    iput-object p2, p0, Lcom/squareup/ui/BaseXableEditText;->showButtonTextWatcher:Lcom/squareup/ui/BaseXableEditText$ShowButtonTextWatcher;

    .line 95
    sget-object p2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 96
    invoke-virtual {p2, p3}, Lcom/squareup/glyph/GlyphTypeface$Glyph;->initGlyph(Landroid/content/res/Resources;)V

    .line 97
    sget v2, Lcom/squareup/marin/R$dimen;->marin_gap_medium:I

    invoke-virtual {p3, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 98
    invoke-virtual {p2}, Lcom/squareup/glyph/GlyphTypeface$Glyph;->getFixedGlyphWidth()I

    move-result p2

    add-int/2addr v2, p2

    iput v2, p0, Lcom/squareup/ui/BaseXableEditText;->xButtonWidthMinusMargin:I

    .line 100
    new-instance p2, Lcom/squareup/ui/-$$Lambda$BaseXableEditText$scAScivRInPq2yHcEuCxIiaEk48;

    invoke-direct {p2, p0}, Lcom/squareup/ui/-$$Lambda$BaseXableEditText$scAScivRInPq2yHcEuCxIiaEk48;-><init>(Lcom/squareup/ui/BaseXableEditText;)V

    iput-object p2, p0, Lcom/squareup/ui/BaseXableEditText;->hideXButtonAction:Lkotlin/jvm/functions/Function1;

    .line 106
    sget p2, Lcom/squareup/widgets/pos/R$styleable;->XableEditText_xSelector:I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p2

    if-eqz p2, :cond_0

    .line 109
    iget-object v2, p0, Lcom/squareup/ui/BaseXableEditText;->xButton:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v2}, Lcom/squareup/glyph/SquareGlyphView;->getPaddingLeft()I

    move-result v2

    .line 110
    iget-object v3, p0, Lcom/squareup/ui/BaseXableEditText;->xButton:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v3}, Lcom/squareup/glyph/SquareGlyphView;->getPaddingRight()I

    move-result v3

    .line 111
    iget-object v4, p0, Lcom/squareup/ui/BaseXableEditText;->xButton:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v4, p2}, Lcom/squareup/glyph/SquareGlyphView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 112
    iget-object p2, p0, Lcom/squareup/ui/BaseXableEditText;->xButton:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {p2, v2, v0, v3, v0}, Lcom/squareup/glyph/SquareGlyphView;->setPadding(IIII)V

    .line 115
    :cond_0
    sget p2, Lcom/squareup/widgets/pos/R$styleable;->XableEditText_android_text:I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {p0, p2}, Lcom/squareup/ui/BaseXableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 116
    sget p2, Lcom/squareup/widgets/pos/R$styleable;->XableEditText_android_hint:I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p2}, Lcom/squareup/ui/BaseXableEditText;->setHint(Ljava/lang/String;)V

    .line 118
    sget p2, Lcom/squareup/widgets/pos/R$styleable;->XableEditText_android_imeOptions:I

    const/4 v2, 0x1

    invoke-virtual {p1, p2, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result p2

    .line 119
    iget-object v3, p0, Lcom/squareup/ui/BaseXableEditText;->editText:Landroid/widget/EditText;

    invoke-virtual {v3, p2}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 121
    sget p2, Lcom/squareup/widgets/pos/R$styleable;->XableEditText_android_inputType:I

    invoke-virtual {p1, p2, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result p2

    .line 122
    iget-object v3, p0, Lcom/squareup/ui/BaseXableEditText;->editText:Landroid/widget/EditText;

    invoke-virtual {v3, p2}, Landroid/widget/EditText;->setInputType(I)V

    const/16 v3, 0x80

    and-int/2addr p2, v3

    if-ne p2, v3, :cond_1

    .line 124
    iget-object p2, p0, Lcom/squareup/ui/BaseXableEditText;->editText:Landroid/widget/EditText;

    sget-object v3, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {p2, v3}, Landroid/widget/EditText;->setTypeface(Landroid/graphics/Typeface;)V

    .line 125
    iget-object p2, p0, Lcom/squareup/ui/BaseXableEditText;->editText:Landroid/widget/EditText;

    invoke-virtual {p2, v2}, Landroid/widget/EditText;->setHorizontalFadingEdgeEnabled(Z)V

    .line 128
    :cond_1
    sget p2, Lcom/squareup/widgets/pos/R$styleable;->XableEditText_hideBackground:I

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p2

    if-eqz p2, :cond_2

    .line 129
    iget-object p2, p0, Lcom/squareup/ui/BaseXableEditText;->editText:Landroid/widget/EditText;

    invoke-virtual {p2, v1}, Landroid/widget/EditText;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 132
    :cond_2
    sget p2, Lcom/squareup/widgets/pos/R$styleable;->XableEditText_android_maxLength:I

    const/4 v3, -0x1

    invoke-virtual {p1, p2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result p2

    if-eq p2, v3, :cond_3

    new-array v2, v2, [Landroid/text/InputFilter;

    .line 135
    new-instance v4, Landroid/text/InputFilter$LengthFilter;

    invoke-direct {v4, p2}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v4, v2, v0

    .line 136
    iget-object p2, p0, Lcom/squareup/ui/BaseXableEditText;->editText:Landroid/widget/EditText;

    invoke-virtual {p2, v2}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 139
    :cond_3
    sget p2, Lcom/squareup/widgets/pos/R$styleable;->XableEditText_glyph:I

    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->ALL_GLYPHS:Ljava/util/List;

    invoke-static {p1, p2, v0, v1}, Lcom/squareup/util/Views;->getEnum(Landroid/content/res/TypedArray;ILjava/util/List;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object p2

    check-cast p2, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    if-eqz p2, :cond_4

    .line 140
    invoke-virtual {p0}, Lcom/squareup/ui/BaseXableEditText;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_4

    .line 141
    new-instance v0, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    invoke-direct {v0, p3}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;-><init>(Landroid/content/res/Resources;)V

    .line 142
    invoke-virtual {v0, p2}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->glyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    move-result-object p2

    .line 143
    sget v0, Lcom/squareup/widgets/pos/R$styleable;->XableEditText_sq_glyphColor:I

    sget v2, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    .line 144
    invoke-virtual {p3, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result p3

    .line 143
    invoke-virtual {p1, v0, p3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p3

    .line 145
    invoke-virtual {p2, p3}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->color(I)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    .line 146
    iget-object p3, p0, Lcom/squareup/ui/BaseXableEditText;->editText:Landroid/widget/EditText;

    invoke-virtual {p2}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->build()Lcom/squareup/glyph/SquareGlyphDrawable;

    move-result-object p2

    invoke-virtual {p3, p2, v1, v1, v1}, Landroid/widget/EditText;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 149
    sget p2, Lcom/squareup/widgets/pos/R$styleable;->XableEditText_glyphSpacing:I

    invoke-virtual {p1, p2, v3}, Landroid/content/res/TypedArray;->getLayoutDimension(II)I

    move-result p2

    if-eq p2, v3, :cond_4

    .line 151
    iget-object p3, p0, Lcom/squareup/ui/BaseXableEditText;->editText:Landroid/widget/EditText;

    invoke-virtual {p3, p2}, Landroid/widget/EditText;->setCompoundDrawablePadding(I)V

    .line 154
    :cond_4
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    .line 156
    iget-object p1, p0, Lcom/squareup/ui/BaseXableEditText;->editText:Landroid/widget/EditText;

    iget-object p2, p0, Lcom/squareup/ui/BaseXableEditText;->showButtonFocusListener:Lcom/squareup/ui/BaseXableEditText$ShowButtonFocusListener;

    invoke-virtual {p1, p2}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 157
    iget-object p1, p0, Lcom/squareup/ui/BaseXableEditText;->editText:Landroid/widget/EditText;

    iget-object p2, p0, Lcom/squareup/ui/BaseXableEditText;->showButtonTextWatcher:Lcom/squareup/ui/BaseXableEditText$ShowButtonTextWatcher;

    invoke-virtual {p1, p2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 159
    iget-object p1, p0, Lcom/squareup/ui/BaseXableEditText;->xButton:Lcom/squareup/glyph/SquareGlyphView;

    new-instance p2, Lcom/squareup/ui/BaseXableEditText$2;

    invoke-direct {p2, p0}, Lcom/squareup/ui/BaseXableEditText$2;-><init>(Lcom/squareup/ui/BaseXableEditText;)V

    invoke-virtual {p1, p2}, Lcom/squareup/glyph/SquareGlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method static synthetic access$200(Lcom/squareup/ui/BaseXableEditText;)Z
    .locals 0

    .line 54
    iget-boolean p0, p0, Lcom/squareup/ui/BaseXableEditText;->dismissOnClick:Z

    return p0
.end method

.method static synthetic access$800(Lcom/squareup/ui/BaseXableEditText;ZZ)V
    .locals 0

    .line 54
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/BaseXableEditText;->updateButtonVisible(ZZ)V

    return-void
.end method

.method private updateButtonVisible(ZZ)V
    .locals 1

    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    :cond_0
    if-eqz p1, :cond_1

    .line 322
    iget-boolean p1, p0, Lcom/squareup/ui/BaseXableEditText;->showXOnFocus:Z

    if-nez p1, :cond_2

    :cond_1
    if-eqz p2, :cond_3

    iget-boolean p1, p0, Lcom/squareup/ui/BaseXableEditText;->showXOnNonEmptyText:Z

    if-eqz p1, :cond_3

    .line 323
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/BaseXableEditText;->xButton:Lcom/squareup/glyph/SquareGlyphView;

    iget p2, p0, Lcom/squareup/ui/BaseXableEditText;->fadeDurationMs:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    .line 324
    iget-object p1, p0, Lcom/squareup/ui/BaseXableEditText;->editText:Landroid/widget/EditText;

    iget p2, p0, Lcom/squareup/ui/BaseXableEditText;->xButtonWidthMinusMargin:I

    const/4 v0, 0x0

    invoke-virtual {p1, v0, v0, p2, v0}, Landroid/widget/EditText;->setPadding(IIII)V

    goto :goto_0

    .line 326
    :cond_3
    iget-object p1, p0, Lcom/squareup/ui/BaseXableEditText;->xButton:Lcom/squareup/glyph/SquareGlyphView;

    iget p2, p0, Lcom/squareup/ui/BaseXableEditText;->fadeDurationMs:I

    iget-object v0, p0, Lcom/squareup/ui/BaseXableEditText;->hideXButtonAction:Lkotlin/jvm/functions/Function1;

    invoke-static {p1, p2, v0}, Lcom/squareup/util/Views;->fadeOut(Landroid/view/View;ILkotlin/jvm/functions/Function1;)V

    :goto_0
    return-void
.end method


# virtual methods
.method public addSelectionWatcher(Lcom/squareup/text/SelectionWatcher;)V
    .locals 1

    .line 171
    iget-object v0, p0, Lcom/squareup/ui/BaseXableEditText;->editText:Landroid/widget/EditText;

    check-cast v0, Lcom/squareup/text/HasSelectableText;

    invoke-interface {v0, p1}, Lcom/squareup/text/HasSelectableText;->addSelectionWatcher(Lcom/squareup/text/SelectionWatcher;)V

    return-void
.end method

.method public addTextChangedListener(Landroid/text/TextWatcher;)V
    .locals 1

    .line 179
    iget-object v0, p0, Lcom/squareup/ui/BaseXableEditText;->editText:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method public append(Ljava/lang/String;)V
    .locals 1

    .line 301
    iget-object v0, p0, Lcom/squareup/ui/BaseXableEditText;->editText:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->append(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public disableActionsExceptPaste()V
    .locals 1

    .line 331
    iget-object v0, p0, Lcom/squareup/ui/BaseXableEditText;->editText:Landroid/widget/EditText;

    invoke-static {v0}, Lcom/squareup/util/EditTexts;->disableActionsExceptPaste(Landroid/widget/EditText;)V

    return-void
.end method

.method public getEditText()Landroid/widget/EditText;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .line 274
    iget-object v0, p0, Lcom/squareup/ui/BaseXableEditText;->editText:Landroid/widget/EditText;

    return-object v0
.end method

.method public getSelectionEnd()I
    .locals 1

    .line 191
    iget-object v0, p0, Lcom/squareup/ui/BaseXableEditText;->editText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v0

    return v0
.end method

.method public getSelectionStart()I
    .locals 1

    .line 187
    iget-object v0, p0, Lcom/squareup/ui/BaseXableEditText;->editText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v0

    return v0
.end method

.method public getText()Landroid/text/Editable;
    .locals 1

    .line 199
    iget-object v0, p0, Lcom/squareup/ui/BaseXableEditText;->editText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getText()Ljava/lang/CharSequence;
    .locals 1

    .line 54
    invoke-virtual {p0}, Lcom/squareup/ui/BaseXableEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$new$0$BaseXableEditText(Landroid/view/View;)Lkotlin/Unit;
    .locals 1

    .line 101
    iget-object p1, p0, Lcom/squareup/ui/BaseXableEditText;->xButton:Lcom/squareup/glyph/SquareGlyphView;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 102
    iget-object p1, p0, Lcom/squareup/ui/BaseXableEditText;->editText:Landroid/widget/EditText;

    const/4 v0, 0x0

    invoke-virtual {p1, v0, v0, v0, v0}, Landroid/widget/EditText;->setPadding(IIII)V

    .line 103
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public synthetic lambda$onRestoreInstanceState$1$BaseXableEditText()V
    .locals 1

    .line 248
    iget-object v0, p0, Lcom/squareup/ui/BaseXableEditText;->editText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 269
    iget-object v0, p0, Lcom/squareup/ui/BaseXableEditText;->requestFocusRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/squareup/ui/BaseXableEditText;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 270
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .line 257
    iget-object v0, p0, Lcom/squareup/ui/BaseXableEditText;->editText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->hasFocus()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/BaseXableEditText;->editText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 258
    iget-object v0, p0, Lcom/squareup/ui/BaseXableEditText;->editText:Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/squareup/util/Views;->pointInViewRaw(Landroid/view/View;FF)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 259
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v1

    if-nez v1, :cond_0

    .line 260
    iget-object v0, p0, Lcom/squareup/ui/BaseXableEditText;->requestFocusRunnable:Ljava/lang/Runnable;

    sget-wide v1, Lcom/squareup/ui/BaseXableEditText;->REQUEST_FOCUS_JUST_BEFORE_LONG_PRESS:J

    invoke-virtual {p0, v0, v1, v2}, Lcom/squareup/ui/BaseXableEditText;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :cond_0
    if-nez v0, :cond_1

    .line 262
    iget-object v0, p0, Lcom/squareup/ui/BaseXableEditText;->requestFocusRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/squareup/ui/BaseXableEditText;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 265
    :cond_1
    :goto_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .line 243
    check-cast p1, Lcom/squareup/ui/BaseXableEditText$SavedState;

    .line 244
    invoke-virtual {p1}, Lcom/squareup/ui/BaseXableEditText$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 246
    invoke-static {p1}, Lcom/squareup/ui/BaseXableEditText$SavedState;->access$500(Lcom/squareup/ui/BaseXableEditText$SavedState;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/ui/BaseXableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 247
    invoke-static {p1}, Lcom/squareup/ui/BaseXableEditText$SavedState;->access$600(Lcom/squareup/ui/BaseXableEditText$SavedState;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 248
    new-instance p1, Lcom/squareup/ui/-$$Lambda$BaseXableEditText$JSbALQEDIhffH_dzVceofoMdShk;

    invoke-direct {p1, p0}, Lcom/squareup/ui/-$$Lambda$BaseXableEditText$JSbALQEDIhffH_dzVceofoMdShk;-><init>(Lcom/squareup/ui/BaseXableEditText;)V

    invoke-virtual {p0, p1}, Lcom/squareup/ui/BaseXableEditText;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 5

    .line 239
    new-instance v0, Lcom/squareup/ui/BaseXableEditText$SavedState;

    invoke-super {p0}, Landroid/widget/FrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-virtual {p0}, Lcom/squareup/ui/BaseXableEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/BaseXableEditText;->editText:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->isFocused()Z

    move-result v3

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/ui/BaseXableEditText$SavedState;-><init>(Landroid/os/Parcelable;Ljava/lang/String;ZLcom/squareup/ui/BaseXableEditText$1;)V

    return-object v0
.end method

.method public postRestartInput()V
    .locals 0

    return-void
.end method

.method public removeSelectionWatcher(Lcom/squareup/text/SelectionWatcher;)V
    .locals 1

    .line 175
    iget-object v0, p0, Lcom/squareup/ui/BaseXableEditText;->editText:Landroid/widget/EditText;

    check-cast v0, Lcom/squareup/text/HasSelectableText;

    invoke-interface {v0, p1}, Lcom/squareup/text/HasSelectableText;->removeSelectionWatcher(Lcom/squareup/text/SelectionWatcher;)V

    return-void
.end method

.method public removeTextChangedListener(Landroid/text/TextWatcher;)V
    .locals 1

    .line 183
    iget-object v0, p0, Lcom/squareup/ui/BaseXableEditText;->editText:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method public requestFocus(ILandroid/graphics/Rect;)Z
    .locals 1

    .line 310
    iget-object v0, p0, Lcom/squareup/ui/BaseXableEditText;->editText:Landroid/widget/EditText;

    invoke-virtual {v0, p1, p2}, Landroid/widget/EditText;->requestFocus(ILandroid/graphics/Rect;)Z

    move-result p1

    return p1
.end method

.method public setEnabled(Z)V
    .locals 1

    .line 234
    iget-object v0, p0, Lcom/squareup/ui/BaseXableEditText;->editText:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 235
    iget-object v0, p0, Lcom/squareup/ui/BaseXableEditText;->xButton:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setEnabled(Z)V

    return-void
.end method

.method public setHint(Ljava/lang/String;)V
    .locals 1

    .line 278
    iget-object v0, p0, Lcom/squareup/ui/BaseXableEditText;->editText:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setImeOptions(I)V
    .locals 1

    .line 318
    iget-object v0, p0, Lcom/squareup/ui/BaseXableEditText;->editText:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setImeOptions(I)V

    return-void
.end method

.method public setKeyListener(Landroid/text/method/KeyListener;)V
    .locals 0

    return-void
.end method

.method public setNextFocusView(Landroid/view/View;)V
    .locals 1

    .line 286
    new-instance v0, Lcom/squareup/ui/BaseXableEditText$3;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/BaseXableEditText$3;-><init>(Lcom/squareup/ui/BaseXableEditText;Landroid/view/View;)V

    invoke-virtual {p0, v0}, Lcom/squareup/ui/BaseXableEditText;->setOnEditorActionListener(Lcom/squareup/debounce/DebouncedOnEditorActionListener;)V

    return-void
.end method

.method public setOnButtonClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .line 226
    iget-object v0, p0, Lcom/squareup/ui/BaseXableEditText;->xButton:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0

    .line 219
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setOnEditorActionListener(Lcom/squareup/debounce/DebouncedOnEditorActionListener;)V
    .locals 1

    .line 282
    iget-object v0, p0, Lcom/squareup/ui/BaseXableEditText;->editText:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    return-void
.end method

.method public setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V
    .locals 1

    .line 230
    iget-object v0, p0, Lcom/squareup/ui/BaseXableEditText;->showButtonFocusListener:Lcom/squareup/ui/BaseXableEditText$ShowButtonFocusListener;

    invoke-static {v0, p1}, Lcom/squareup/ui/BaseXableEditText$ShowButtonFocusListener;->access$300(Lcom/squareup/ui/BaseXableEditText$ShowButtonFocusListener;Landroid/view/View$OnFocusChangeListener;)V

    return-void
.end method

.method public setSelectAllOnFocus(Z)V
    .locals 1

    .line 314
    iget-object v0, p0, Lcom/squareup/ui/BaseXableEditText;->editText:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setSelectAllOnFocus(Z)V

    return-void
.end method

.method public setSelection(II)V
    .locals 1

    .line 195
    iget-object v0, p0, Lcom/squareup/ui/BaseXableEditText;->editText:Landroid/widget/EditText;

    invoke-virtual {v0, p1, p2}, Landroid/widget/EditText;->setSelection(II)V

    return-void
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 203
    iget-object v0, p0, Lcom/squareup/ui/BaseXableEditText;->editText:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 205
    iget-object p1, p0, Lcom/squareup/ui/BaseXableEditText;->editText:Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result p1

    .line 206
    iget-object v0, p0, Lcom/squareup/ui/BaseXableEditText;->editText:Landroid/widget/EditText;

    invoke-virtual {v0, p1, p1}, Landroid/widget/EditText;->setSelection(II)V

    return-void
.end method
