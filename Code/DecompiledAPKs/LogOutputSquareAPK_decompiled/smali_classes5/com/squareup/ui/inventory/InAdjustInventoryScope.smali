.class public abstract Lcom/squareup/ui/inventory/InAdjustInventoryScope;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "InAdjustInventoryScope.java"


# instance fields
.field public final adjustInventoryScope:Lcom/squareup/ui/inventory/AdjustInventoryScope;


# direct methods
.method protected constructor <init>(Lcom/squareup/ui/inventory/AdjustInventoryScope;)V
    .locals 0

    .line 9
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    .line 10
    iput-object p1, p0, Lcom/squareup/ui/inventory/InAdjustInventoryScope;->adjustInventoryScope:Lcom/squareup/ui/inventory/AdjustInventoryScope;

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 18
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->ITEMS_APPLET_EDIT_ITEM:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public final getParentKey()Ljava/lang/Object;
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/squareup/ui/inventory/InAdjustInventoryScope;->adjustInventoryScope:Lcom/squareup/ui/inventory/AdjustInventoryScope;

    return-object v0
.end method
