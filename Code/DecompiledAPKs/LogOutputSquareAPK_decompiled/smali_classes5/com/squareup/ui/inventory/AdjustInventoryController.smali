.class public Lcom/squareup/ui/inventory/AdjustInventoryController;
.super Ljava/lang/Object;
.source "AdjustInventoryController.java"

# interfaces
.implements Lmortar/bundler/Bundler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/inventory/AdjustInventoryController$InventoryAdjustmentSaveStatus;,
        Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;,
        Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventoryErrorDialogScreenData;,
        Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventorySpecifyNumberScreenData;,
        Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventorySelectReasonScreenData;
    }
.end annotation


# static fields
.field private static final ADJUST_INVENTORY_STATE_KEY:Ljava/lang/String; = "adjust_inventory_state"

.field private static final stockAdjustmentReasons:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final adjustInventoryHost:Lcom/squareup/ui/inventory/AdjustInventoryHost;

.field private adjustInventoryScope:Lcom/squareup/ui/inventory/AdjustInventoryScope;

.field private adjustInventoryState:Lcom/squareup/ui/inventory/AdjustInventoryState;

.field private final adjustInventoryStateRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/ui/inventory/AdjustInventoryState;",
            ">;"
        }
    .end annotation
.end field

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private catalogVersion:Ljava/lang/Long;

.field private final cogs:Lcom/squareup/cogs/Cogs;

.field private final compositeDisposable:Lio/reactivex/disposables/CompositeDisposable;

.field private final flow:Lflow/Flow;

.field private final inventoryAdjustmentSaveStatusRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/ui/inventory/AdjustInventoryController$InventoryAdjustmentSaveStatus;",
            ">;"
        }
    .end annotation
.end field

.field private final inventoryService:Lcom/squareup/server/inventory/InventoryService;

.field private final mainScheduler:Lio/reactivex/Scheduler;
    .annotation runtime Lcom/squareup/thread/Main;
    .end annotation
.end field

.field private final priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;

    .line 66
    new-instance v1, Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;

    sget v2, Lcom/squareup/adjustinventory/R$string;->adjust_stock_reason_receive:I

    sget-object v3, Lcom/squareup/protos/client/InventoryAdjustmentReason;->RECEIVED:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;-><init>(ILcom/squareup/protos/client/InventoryAdjustmentReason;Z)V

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;

    sget v2, Lcom/squareup/adjustinventory/R$string;->adjust_stock_reason_recount:I

    sget-object v3, Lcom/squareup/protos/client/InventoryAdjustmentReason;->DO_NOT_USE_REASON:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    const/4 v5, 0x1

    invoke-direct {v1, v2, v3, v5}, Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;-><init>(ILcom/squareup/protos/client/InventoryAdjustmentReason;Z)V

    aput-object v1, v0, v5

    new-instance v1, Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;

    sget v2, Lcom/squareup/adjustinventory/R$string;->adjust_stock_reason_damage:I

    sget-object v3, Lcom/squareup/protos/client/InventoryAdjustmentReason;->DAMAGED:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;-><init>(ILcom/squareup/protos/client/InventoryAdjustmentReason;Z)V

    const/4 v2, 0x2

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;

    sget v2, Lcom/squareup/adjustinventory/R$string;->adjust_stock_reason_theft:I

    sget-object v3, Lcom/squareup/protos/client/InventoryAdjustmentReason;->THEFT:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;-><init>(ILcom/squareup/protos/client/InventoryAdjustmentReason;Z)V

    const/4 v2, 0x3

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;

    sget v2, Lcom/squareup/adjustinventory/R$string;->adjust_stock_reason_loss:I

    sget-object v3, Lcom/squareup/protos/client/InventoryAdjustmentReason;->LOST:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;-><init>(ILcom/squareup/protos/client/InventoryAdjustmentReason;Z)V

    const/4 v2, 0x4

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;

    sget v2, Lcom/squareup/adjustinventory/R$string;->adjust_stock_reason_return:I

    sget-object v3, Lcom/squareup/protos/client/InventoryAdjustmentReason;->RETURNED:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;-><init>(ILcom/squareup/protos/client/InventoryAdjustmentReason;Z)V

    const/4 v2, 0x5

    aput-object v1, v0, v2

    .line 67
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 66
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/inventory/AdjustInventoryController;->stockAdjustmentReasons:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/server/inventory/InventoryService;Lio/reactivex/Scheduler;Lflow/Flow;Lcom/squareup/analytics/Analytics;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/ui/inventory/AdjustInventoryHost;Lcom/squareup/cogs/Cogs;)V
    .locals 1
    .param p3    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    new-instance v0, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v0}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->compositeDisposable:Lio/reactivex/disposables/CompositeDisposable;

    .line 101
    iput-object p1, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 102
    iput-object p2, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->inventoryService:Lcom/squareup/server/inventory/InventoryService;

    .line 103
    iput-object p3, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->mainScheduler:Lio/reactivex/Scheduler;

    .line 104
    iput-object p4, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->flow:Lflow/Flow;

    .line 105
    iput-object p5, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->analytics:Lcom/squareup/analytics/Analytics;

    .line 106
    iput-object p6, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    .line 107
    iput-object p7, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryHost:Lcom/squareup/ui/inventory/AdjustInventoryHost;

    .line 108
    iput-object p8, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->cogs:Lcom/squareup/cogs/Cogs;

    .line 109
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryStateRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 110
    sget-object p1, Lcom/squareup/ui/inventory/AdjustInventoryController$InventoryAdjustmentSaveStatus;->NOT_REQUESTED:Lcom/squareup/ui/inventory/AdjustInventoryController$InventoryAdjustmentSaveStatus;

    invoke-static {p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->inventoryAdjustmentSaveStatusRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 111
    invoke-static {}, Lcom/squareup/ui/inventory/AdjustInventoryState;->createAdjustInventoryState()Lcom/squareup/ui/inventory/AdjustInventoryState;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryState:Lcom/squareup/ui/inventory/AdjustInventoryState;

    return-void
.end method

.method private buildAdjustVariationInventoryRequest()Lcom/squareup/protos/client/AdjustVariationInventoryRequest;
    .locals 8

    .line 233
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryState:Lcom/squareup/ui/inventory/AdjustInventoryState;

    invoke-virtual {v0}, Lcom/squareup/ui/inventory/AdjustInventoryState;->getUserInputCount()Ljava/math/BigDecimal;

    move-result-object v4

    .line 234
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryState:Lcom/squareup/ui/inventory/AdjustInventoryState;

    invoke-virtual {v0}, Lcom/squareup/ui/inventory/AdjustInventoryState;->getUserInputUnitCost()Lcom/squareup/protos/common/Money;

    move-result-object v5

    .line 235
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryScope:Lcom/squareup/ui/inventory/AdjustInventoryScope;

    iget-object v2, v0, Lcom/squareup/ui/inventory/AdjustInventoryScope;->variationMerchantCatalogToken:Ljava/lang/String;

    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryState:Lcom/squareup/ui/inventory/AdjustInventoryState;

    .line 237
    invoke-virtual {v0}, Lcom/squareup/ui/inventory/AdjustInventoryState;->getReason()Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;

    move-result-object v3

    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->catalogVersion:Ljava/lang/Long;

    .line 238
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    move-object v1, p0

    .line 235
    invoke-virtual/range {v1 .. v7}, Lcom/squareup/ui/inventory/AdjustInventoryController;->buildAdjustVariationInventoryRequest(Ljava/lang/String;Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;Ljava/math/BigDecimal;Lcom/squareup/protos/common/Money;J)Lcom/squareup/protos/client/AdjustVariationInventoryRequest;

    move-result-object v0

    return-object v0
.end method

.method private formatQuantity(Ljava/math/BigDecimal;)Ljava/lang/String;
    .locals 0

    if-eqz p1, :cond_0

    .line 420
    invoke-virtual {p1}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method private isUnitCostEnabledForInventoryAdjustment(Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;)Z
    .locals 2

    .line 378
    iget-boolean v0, p1, Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;->isRecount:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 381
    :cond_0
    iget-object p1, p1, Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;->inventoryAdjustmentReason:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    .line 382
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->isInventoryPlusEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/client/InventoryAdjustmentReason;->RECEIVED:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    if-eq p1, v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/InventoryAdjustmentReason;->RETURNED:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    if-ne p1, v0, :cond_2

    :cond_1
    const/4 v1, 0x1

    :cond_2
    return v1
.end method

.method private sendAdjustmentRequest(Lcom/squareup/protos/client/AdjustVariationInventoryRequest;)V
    .locals 3

    .line 310
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->inventoryAdjustmentSaveStatusRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v1, Lcom/squareup/ui/inventory/AdjustInventoryController$InventoryAdjustmentSaveStatus;->SAVING:Lcom/squareup/ui/inventory/AdjustInventoryController$InventoryAdjustmentSaveStatus;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 312
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->compositeDisposable:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->inventoryService:Lcom/squareup/server/inventory/InventoryService;

    invoke-interface {v1, p1}, Lcom/squareup/server/inventory/InventoryService;->adjust(Lcom/squareup/protos/client/AdjustVariationInventoryRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object v1

    .line 313
    invoke-virtual {v1}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->mainScheduler:Lio/reactivex/Scheduler;

    .line 314
    invoke-virtual {v1, v2}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/inventory/-$$Lambda$AdjustInventoryController$JA5L2YzpTaLc_aIM1vGmI36ADK8;

    invoke-direct {v2, p0, p1}, Lcom/squareup/ui/inventory/-$$Lambda$AdjustInventoryController$JA5L2YzpTaLc_aIM1vGmI36ADK8;-><init>(Lcom/squareup/ui/inventory/AdjustInventoryController;Lcom/squareup/protos/client/AdjustVariationInventoryRequest;)V

    .line 315
    invoke-virtual {v1, v2}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    .line 312
    invoke-virtual {v0, p1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method private startAdjustInventoryFlow(ZLjava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;Lcom/squareup/protos/common/Money;ILjava/lang/String;)V
    .locals 8

    if-eqz p1, :cond_0

    .line 201
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryState:Lcom/squareup/ui/inventory/AdjustInventoryState;

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move v4, p6

    move-object v5, p7

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/ui/inventory/AdjustInventoryState;->startAdjustInventory(Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;ILjava/lang/String;)V

    goto :goto_0

    .line 204
    :cond_0
    new-instance v3, Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;

    sget p1, Lcom/squareup/adjustinventory/R$string;->adjust_stock_reason_receive:I

    sget-object p3, Lcom/squareup/protos/client/InventoryAdjustmentReason;->RECEIVED:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    const/4 v0, 0x0

    invoke-direct {v3, p1, p3, v0}, Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;-><init>(ILcom/squareup/protos/client/InventoryAdjustmentReason;Z)V

    .line 206
    iget-object v1, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryState:Lcom/squareup/ui/inventory/AdjustInventoryState;

    move-object v2, p2

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    move-object v7, p7

    invoke-virtual/range {v1 .. v7}, Lcom/squareup/ui/inventory/AdjustInventoryState;->receiveInitialStock(Ljava/lang/String;Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;Ljava/math/BigDecimal;Lcom/squareup/protos/common/Money;ILjava/lang/String;)V

    .line 209
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryStateRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object p2, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryState:Lcom/squareup/ui/inventory/AdjustInventoryState;

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method adjustInventoryErrorDialogScreenData()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventoryErrorDialogScreenData;",
            ">;"
        }
    .end annotation

    .line 474
    invoke-virtual {p0}, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryState()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/inventory/-$$Lambda$mQ7EloKYxVcjqp5JA2wXdW1bGy8;

    invoke-direct {v1, p0}, Lcom/squareup/ui/inventory/-$$Lambda$mQ7EloKYxVcjqp5JA2wXdW1bGy8;-><init>(Lcom/squareup/ui/inventory/AdjustInventoryController;)V

    .line 475
    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public adjustInventoryReasonRowClicked(Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;)V
    .locals 1

    .line 213
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryState:Lcom/squareup/ui/inventory/AdjustInventoryState;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/inventory/AdjustInventoryState;->selectReasonToAdjustInventory(Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;)V

    .line 214
    iget-object p1, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryStateRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryState:Lcom/squareup/ui/inventory/AdjustInventoryState;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method adjustInventorySelectReasonScreenData()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventorySelectReasonScreenData;",
            ">;"
        }
    .end annotation

    .line 451
    new-instance v0, Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventorySelectReasonScreenData;

    sget-object v1, Lcom/squareup/ui/inventory/AdjustInventoryController;->stockAdjustmentReasons:Ljava/util/List;

    invoke-direct {v0, v1}, Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventorySelectReasonScreenData;-><init>(Ljava/util/List;)V

    invoke-static {v0}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method adjustInventorySpecifyNumberScreenData()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventorySpecifyNumberScreenData;",
            ">;"
        }
    .end annotation

    .line 455
    invoke-virtual {p0}, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryState()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/inventory/-$$Lambda$AdjustInventoryController$v3p3ZNhIBV5CFPGcZk6WrT2Qu80;

    invoke-direct {v1, p0}, Lcom/squareup/ui/inventory/-$$Lambda$AdjustInventoryController$v3p3ZNhIBV5CFPGcZk6WrT2Qu80;-><init>(Lcom/squareup/ui/inventory/AdjustInventoryController;)V

    .line 456
    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method adjustInventoryState()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/inventory/AdjustInventoryState;",
            ">;"
        }
    .end annotation

    .line 479
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryStateRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->asObservable()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method applySuccessfulInventoryAdjustment(Ljava/lang/String;Ljava/math/BigDecimal;Lcom/squareup/protos/client/AdjustVariationInventoryRequest;)V
    .locals 5

    .line 344
    iget-object v0, p3, Lcom/squareup/protos/client/AdjustVariationInventoryRequest;->count:Lcom/squareup/protos/client/InventoryCount;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 349
    iget-object p2, p3, Lcom/squareup/protos/client/AdjustVariationInventoryRequest;->count:Lcom/squareup/protos/client/InventoryCount;

    iget-object p2, p2, Lcom/squareup/protos/client/InventoryCount;->current_quantity_decimal:Ljava/lang/String;

    invoke-static {p2}, Lcom/squareup/util/Numbers;->parseDecimal(Ljava/lang/String;)Ljava/math/BigDecimal;

    move-result-object p2

    move-object v2, p2

    goto :goto_2

    .line 352
    :cond_1
    iget-object v2, p3, Lcom/squareup/protos/client/AdjustVariationInventoryRequest;->adjustment:Lcom/squareup/protos/client/InventoryAdjustment;

    iget-object v2, v2, Lcom/squareup/protos/client/InventoryAdjustment;->adjust_quantity_decimal:Ljava/lang/String;

    invoke-static {v2}, Lcom/squareup/util/Numbers;->parseDecimal(Ljava/lang/String;)Ljava/math/BigDecimal;

    move-result-object v2

    .line 353
    sget-object v3, Lcom/squareup/ui/inventory/AdjustInventoryController$1;->$SwitchMap$com$squareup$protos$client$InventoryAdjustmentReason:[I

    iget-object v4, p3, Lcom/squareup/protos/client/AdjustVariationInventoryRequest;->adjustment:Lcom/squareup/protos/client/InventoryAdjustment;

    iget-object v4, v4, Lcom/squareup/protos/client/InventoryAdjustment;->reason:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    invoke-virtual {v4}, Lcom/squareup/protos/client/InventoryAdjustmentReason;->ordinal()I

    move-result v4

    aget v3, v3, v4

    if-eq v3, v1, :cond_5

    const/4 v1, 0x2

    if-eq v3, v1, :cond_5

    const/4 v1, 0x3

    if-eq v3, v1, :cond_5

    const/4 v1, 0x4

    if-eq v3, v1, :cond_3

    const/4 v1, 0x5

    if-ne v3, v1, :cond_2

    goto :goto_1

    .line 366
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Illegal Reason: "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p3, p3, Lcom/squareup/protos/client/AdjustVariationInventoryRequest;->adjustment:Lcom/squareup/protos/client/InventoryAdjustment;

    iget-object p3, p3, Lcom/squareup/protos/client/InventoryAdjustment;->reason:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    :goto_1
    if-nez p2, :cond_4

    move-object p2, v2

    goto :goto_2

    .line 363
    :cond_4
    invoke-virtual {p2, v2}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object p2

    goto :goto_2

    .line 357
    :cond_5
    invoke-virtual {p2, v2}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object p2

    .line 370
    :goto_2
    iget-object v1, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryHost:Lcom/squareup/ui/inventory/AdjustInventoryHost;

    if-eqz v0, :cond_6

    const/4 p3, 0x0

    goto :goto_3

    :cond_6
    iget-object p3, p3, Lcom/squareup/protos/client/AdjustVariationInventoryRequest;->adjustment:Lcom/squareup/protos/client/InventoryAdjustment;

    iget-object p3, p3, Lcom/squareup/protos/client/InventoryAdjustment;->reason:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    :goto_3
    invoke-interface {v1, p1, p3, v2, p2}, Lcom/squareup/ui/inventory/AdjustInventoryHost;->updateInventoryStockCountAfterAdjustment(Ljava/lang/String;Lcom/squareup/protos/client/InventoryAdjustmentReason;Ljava/math/BigDecimal;Ljava/math/BigDecimal;)V

    return-void
.end method

.method buildAdjustInventoryErrorDialogScreenData(Lcom/squareup/ui/inventory/AdjustInventoryState;)Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventoryErrorDialogScreenData;
    .locals 3

    .line 425
    invoke-virtual {p1}, Lcom/squareup/ui/inventory/AdjustInventoryState;->getReason()Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;

    move-result-object v0

    .line 426
    invoke-virtual {p1}, Lcom/squareup/ui/inventory/AdjustInventoryState;->getRequest()Lcom/squareup/protos/client/AdjustVariationInventoryRequest;

    move-result-object p1

    .line 428
    iget-boolean v1, v0, Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;->isRecount:Z

    if-eqz v1, :cond_0

    .line 429
    new-instance v0, Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventoryErrorDialogScreenData;

    sget v1, Lcom/squareup/adjustinventory/R$string;->adjust_stock_error_title_recount_stock:I

    sget v2, Lcom/squareup/adjustinventory/R$string;->adjust_stock_error_message_recount_stock:I

    invoke-direct {v0, v1, v2, p1}, Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventoryErrorDialogScreenData;-><init>(IILcom/squareup/protos/client/AdjustVariationInventoryRequest;)V

    return-object v0

    .line 434
    :cond_0
    sget-object v1, Lcom/squareup/ui/inventory/AdjustInventoryController$1;->$SwitchMap$com$squareup$protos$client$InventoryAdjustmentReason:[I

    iget-object v2, v0, Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;->inventoryAdjustmentReason:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    invoke-virtual {v2}, Lcom/squareup/protos/client/InventoryAdjustmentReason;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x1

    if-eq v1, v2, :cond_3

    const/4 v2, 0x2

    if-eq v1, v2, :cond_3

    const/4 v2, 0x3

    if-eq v1, v2, :cond_3

    const/4 v2, 0x4

    if-eq v1, v2, :cond_2

    const/4 v2, 0x5

    if-ne v1, v2, :cond_1

    goto :goto_0

    .line 446
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Illegal Reason: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, v0, Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;->inventoryAdjustmentReason:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 443
    :cond_2
    :goto_0
    new-instance v0, Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventoryErrorDialogScreenData;

    sget v1, Lcom/squareup/adjustinventory/R$string;->adjust_stock_error_title_add_stock:I

    sget v2, Lcom/squareup/adjustinventory/R$string;->adjust_stock_error_message_add_stock:I

    invoke-direct {v0, v1, v2, p1}, Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventoryErrorDialogScreenData;-><init>(IILcom/squareup/protos/client/AdjustVariationInventoryRequest;)V

    return-object v0

    .line 438
    :cond_3
    new-instance v0, Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventoryErrorDialogScreenData;

    sget v1, Lcom/squareup/adjustinventory/R$string;->adjust_stock_error_title_remove_stock:I

    sget v2, Lcom/squareup/adjustinventory/R$string;->adjust_stock_error_message_remove_stock:I

    invoke-direct {v0, v1, v2, p1}, Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventoryErrorDialogScreenData;-><init>(IILcom/squareup/protos/client/AdjustVariationInventoryRequest;)V

    return-object v0
.end method

.method buildAdjustVariationInventoryRequest(Ljava/lang/String;Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;Ljava/math/BigDecimal;Lcom/squareup/protos/common/Money;J)Lcom/squareup/protos/client/AdjustVariationInventoryRequest;
    .locals 2

    .line 389
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getToken()Ljava/lang/String;

    move-result-object v0

    .line 391
    iget-boolean v1, p2, Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;->isRecount:Z

    if-eqz v1, :cond_0

    .line 392
    new-instance p2, Lcom/squareup/protos/client/InventoryCount$Builder;

    invoke-direct {p2}, Lcom/squareup/protos/client/InventoryCount$Builder;-><init>()V

    .line 393
    invoke-virtual {p2, v0}, Lcom/squareup/protos/client/InventoryCount$Builder;->unit_token(Ljava/lang/String;)Lcom/squareup/protos/client/InventoryCount$Builder;

    move-result-object p2

    .line 394
    invoke-virtual {p2, p1}, Lcom/squareup/protos/client/InventoryCount$Builder;->variation_token(Ljava/lang/String;)Lcom/squareup/protos/client/InventoryCount$Builder;

    move-result-object p1

    .line 395
    invoke-direct {p0, p3}, Lcom/squareup/ui/inventory/AdjustInventoryController;->formatQuantity(Ljava/math/BigDecimal;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/InventoryCount$Builder;->current_quantity_decimal(Ljava/lang/String;)Lcom/squareup/protos/client/InventoryCount$Builder;

    move-result-object p1

    sget-object p2, Lcom/squareup/protos/client/State;->IN_STOCK:Lcom/squareup/protos/client/State;

    .line 396
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/InventoryCount$Builder;->state(Lcom/squareup/protos/client/State;)Lcom/squareup/protos/client/InventoryCount$Builder;

    move-result-object p1

    .line 397
    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/InventoryCount$Builder;->catalog_version(Ljava/lang/Long;)Lcom/squareup/protos/client/InventoryCount$Builder;

    move-result-object p1

    .line 398
    invoke-virtual {p1}, Lcom/squareup/protos/client/InventoryCount$Builder;->build()Lcom/squareup/protos/client/InventoryCount;

    move-result-object p1

    .line 399
    new-instance p2, Lcom/squareup/protos/client/AdjustVariationInventoryRequest$Builder;

    invoke-direct {p2}, Lcom/squareup/protos/client/AdjustVariationInventoryRequest$Builder;-><init>()V

    .line 400
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object p3

    invoke-virtual {p3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/squareup/protos/client/AdjustVariationInventoryRequest$Builder;->idempotency_token(Ljava/lang/String;)Lcom/squareup/protos/client/AdjustVariationInventoryRequest$Builder;

    move-result-object p2

    .line 401
    invoke-virtual {p2, p1}, Lcom/squareup/protos/client/AdjustVariationInventoryRequest$Builder;->count(Lcom/squareup/protos/client/InventoryCount;)Lcom/squareup/protos/client/AdjustVariationInventoryRequest$Builder;

    move-result-object p1

    .line 402
    invoke-virtual {p1}, Lcom/squareup/protos/client/AdjustVariationInventoryRequest$Builder;->build()Lcom/squareup/protos/client/AdjustVariationInventoryRequest;

    move-result-object p1

    return-object p1

    .line 404
    :cond_0
    new-instance v1, Lcom/squareup/protos/client/InventoryAdjustment$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/InventoryAdjustment$Builder;-><init>()V

    .line 405
    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->unit_token(Ljava/lang/String;)Lcom/squareup/protos/client/InventoryAdjustment$Builder;

    move-result-object v0

    .line 406
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->variation_token(Ljava/lang/String;)Lcom/squareup/protos/client/InventoryAdjustment$Builder;

    move-result-object p1

    .line 407
    invoke-direct {p0, p3}, Lcom/squareup/ui/inventory/AdjustInventoryController;->formatQuantity(Ljava/math/BigDecimal;)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p3}, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->adjust_quantity_decimal(Ljava/lang/String;)Lcom/squareup/protos/client/InventoryAdjustment$Builder;

    move-result-object p1

    iget-object p2, p2, Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;->inventoryAdjustmentReason:Lcom/squareup/protos/client/InventoryAdjustmentReason;

    .line 408
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->reason(Lcom/squareup/protos/client/InventoryAdjustmentReason;)Lcom/squareup/protos/client/InventoryAdjustment$Builder;

    move-result-object p1

    .line 409
    invoke-virtual {p1, p4}, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->cost_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/InventoryAdjustment$Builder;

    move-result-object p1

    .line 410
    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->catalog_version(Ljava/lang/Long;)Lcom/squareup/protos/client/InventoryAdjustment$Builder;

    move-result-object p1

    .line 411
    invoke-virtual {p1}, Lcom/squareup/protos/client/InventoryAdjustment$Builder;->build()Lcom/squareup/protos/client/InventoryAdjustment;

    move-result-object p1

    .line 412
    new-instance p2, Lcom/squareup/protos/client/AdjustVariationInventoryRequest$Builder;

    invoke-direct {p2}, Lcom/squareup/protos/client/AdjustVariationInventoryRequest$Builder;-><init>()V

    .line 413
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object p3

    invoke-virtual {p3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/squareup/protos/client/AdjustVariationInventoryRequest$Builder;->idempotency_token(Ljava/lang/String;)Lcom/squareup/protos/client/AdjustVariationInventoryRequest$Builder;

    move-result-object p2

    .line 414
    invoke-virtual {p2, p1}, Lcom/squareup/protos/client/AdjustVariationInventoryRequest$Builder;->adjustment(Lcom/squareup/protos/client/InventoryAdjustment;)Lcom/squareup/protos/client/AdjustVariationInventoryRequest$Builder;

    move-result-object p1

    .line 415
    invoke-virtual {p1}, Lcom/squareup/protos/client/AdjustVariationInventoryRequest$Builder;->build()Lcom/squareup/protos/client/AdjustVariationInventoryRequest;

    move-result-object p1

    return-object p1
.end method

.method public getMortarBundleKey()Ljava/lang/String;
    .locals 1

    .line 171
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method inventoryAdjustmentSaveStatus()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/inventory/AdjustInventoryController$InventoryAdjustmentSaveStatus;",
            ">;"
        }
    .end annotation

    .line 470
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->inventoryAdjustmentSaveStatusRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->asObservable()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$adjustInventorySpecifyNumberScreenData$5$AdjustInventoryController(Lcom/squareup/ui/inventory/AdjustInventoryState;)Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventorySpecifyNumberScreenData;
    .locals 10

    .line 457
    invoke-virtual {p1}, Lcom/squareup/ui/inventory/AdjustInventoryState;->getPhase()Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;->RECEIVING_INITIAL_STOCK:Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 458
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/ui/inventory/AdjustInventoryState;->getReason()Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;

    move-result-object v3

    .line 459
    new-instance v0, Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventorySpecifyNumberScreenData;

    .line 460
    invoke-direct {p0, v3}, Lcom/squareup/ui/inventory/AdjustInventoryController;->isUnitCostEnabledForInventoryAdjustment(Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;)Z

    move-result v4

    .line 461
    invoke-virtual {p1}, Lcom/squareup/ui/inventory/AdjustInventoryState;->getServerCount()Ljava/math/BigDecimal;

    move-result-object v5

    .line 462
    invoke-virtual {p1}, Lcom/squareup/ui/inventory/AdjustInventoryState;->getUserInputCount()Ljava/math/BigDecimal;

    move-result-object v6

    .line 463
    invoke-virtual {p1}, Lcom/squareup/ui/inventory/AdjustInventoryState;->getUserInputUnitCost()Lcom/squareup/protos/common/Money;

    move-result-object v7

    .line 464
    invoke-virtual {p1}, Lcom/squareup/ui/inventory/AdjustInventoryState;->getPrecision()I

    move-result v8

    .line 465
    invoke-virtual {p1}, Lcom/squareup/ui/inventory/AdjustInventoryState;->getUnitAbbreviation()Ljava/lang/String;

    move-result-object v9

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventorySpecifyNumberScreenData;-><init>(ZLcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;ZLjava/math/BigDecimal;Ljava/math/BigDecimal;Lcom/squareup/protos/common/Money;ILjava/lang/String;)V

    return-object v0
.end method

.method public synthetic lambda$null$2$AdjustInventoryController(Lcom/squareup/protos/client/AdjustVariationInventoryRequest;Lcom/squareup/protos/client/AdjustVariationInventoryResponse;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 319
    iget-object p2, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->analytics:Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryScope:Lcom/squareup/ui/inventory/AdjustInventoryScope;

    iget-object v0, v0, Lcom/squareup/ui/inventory/AdjustInventoryScope;->variationMerchantCatalogToken:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/squareup/log/inventory/InventoryStockActionEvent;->adjustInventoryStockCount(Lcom/squareup/protos/client/AdjustVariationInventoryRequest;Ljava/lang/String;)Lcom/squareup/log/inventory/InventoryStockActionEvent;

    move-result-object v0

    invoke-interface {p2, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    .line 322
    iget-object p2, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->inventoryAdjustmentSaveStatusRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v0, Lcom/squareup/ui/inventory/AdjustInventoryController$InventoryAdjustmentSaveStatus;->NOT_REQUESTED:Lcom/squareup/ui/inventory/AdjustInventoryController$InventoryAdjustmentSaveStatus;

    invoke-virtual {p2, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 325
    iget-object p2, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryState:Lcom/squareup/ui/inventory/AdjustInventoryState;

    invoke-virtual {p2}, Lcom/squareup/ui/inventory/AdjustInventoryState;->getVariationId()Ljava/lang/String;

    move-result-object p2

    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryState:Lcom/squareup/ui/inventory/AdjustInventoryState;

    .line 326
    invoke-virtual {v0}, Lcom/squareup/ui/inventory/AdjustInventoryState;->getServerCount()Ljava/math/BigDecimal;

    move-result-object v0

    .line 325
    invoke-virtual {p0, p2, v0, p1}, Lcom/squareup/ui/inventory/AdjustInventoryController;->applySuccessfulInventoryAdjustment(Ljava/lang/String;Ljava/math/BigDecimal;Lcom/squareup/protos/client/AdjustVariationInventoryRequest;)V

    .line 327
    iget-object p1, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryState:Lcom/squareup/ui/inventory/AdjustInventoryState;

    invoke-virtual {p1}, Lcom/squareup/ui/inventory/AdjustInventoryState;->finishAdjustInventory()V

    .line 328
    iget-object p1, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryStateRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object p2, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryState:Lcom/squareup/ui/inventory/AdjustInventoryState;

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$null$3$AdjustInventoryController(Lcom/squareup/protos/client/AdjustVariationInventoryRequest;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 332
    iget-object p2, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->inventoryAdjustmentSaveStatusRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v0, Lcom/squareup/ui/inventory/AdjustInventoryController$InventoryAdjustmentSaveStatus;->ERROR:Lcom/squareup/ui/inventory/AdjustInventoryController$InventoryAdjustmentSaveStatus;

    invoke-virtual {p2, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 333
    iget-object p2, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryState:Lcom/squareup/ui/inventory/AdjustInventoryState;

    invoke-virtual {p2, p1}, Lcom/squareup/ui/inventory/AdjustInventoryState;->failToAdjustInventory(Lcom/squareup/protos/client/AdjustVariationInventoryRequest;)V

    .line 334
    iget-object p1, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryStateRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object p2, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryState:Lcom/squareup/ui/inventory/AdjustInventoryState;

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$onEnterScope$0$AdjustInventoryController(Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;)V
    .locals 5

    .line 133
    sget-object v0, Lcom/squareup/ui/inventory/AdjustInventoryController$1;->$SwitchMap$com$squareup$ui$inventory$AdjustInventoryState$AdjustInventoryPhase:[I

    invoke-virtual {p1}, Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x3

    const/4 v1, 0x2

    const/4 v2, 0x1

    if-eq p1, v2, :cond_4

    if-eq p1, v1, :cond_3

    if-eq p1, v0, :cond_2

    const/4 v0, 0x4

    if-eq p1, v0, :cond_1

    const/4 v0, 0x5

    if-ne p1, v0, :cond_0

    .line 158
    iget-object p1, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberScreen;

    iget-object v1, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryScope:Lcom/squareup/ui/inventory/AdjustInventoryScope;

    invoke-direct {v0, v1}, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberScreen;-><init>(Lcom/squareup/ui/inventory/AdjustInventoryScope;)V

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 161
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "com.squareup.ui.inventory.AdjustInventoryState.AdjustInventoryPhase can never be "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryState:Lcom/squareup/ui/inventory/AdjustInventoryState;

    .line 163
    invoke-virtual {v1}, Lcom/squareup/ui/inventory/AdjustInventoryState;->getPhase()Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 153
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/ui/inventory/AdjustInventoryErrorDialogScreen;

    iget-object v1, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryScope:Lcom/squareup/ui/inventory/AdjustInventoryScope;

    invoke-direct {v0, v1}, Lcom/squareup/ui/inventory/AdjustInventoryErrorDialogScreen;-><init>(Lcom/squareup/ui/inventory/AdjustInventoryScope;)V

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 148
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberScreen;

    iget-object v1, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryScope:Lcom/squareup/ui/inventory/AdjustInventoryScope;

    invoke-direct {v0, v1}, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberScreen;-><init>(Lcom/squareup/ui/inventory/AdjustInventoryScope;)V

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 143
    :cond_3
    iget-object p1, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/ui/inventory/AdjustInventorySelectReasonScreen;

    iget-object v1, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryScope:Lcom/squareup/ui/inventory/AdjustInventoryScope;

    invoke-direct {v0, v1}, Lcom/squareup/ui/inventory/AdjustInventorySelectReasonScreen;-><init>(Lcom/squareup/ui/inventory/AdjustInventoryScope;)V

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 137
    :cond_4
    iget-object p1, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->flow:Lflow/Flow;

    new-array v0, v0, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberScreen;

    aput-object v4, v0, v3

    const-class v3, Lcom/squareup/ui/inventory/AdjustInventorySelectReasonScreen;

    aput-object v3, v0, v2

    const-class v2, Lcom/squareup/ui/inventory/AdjustInventoryErrorDialogScreen;

    aput-object v2, v0, v1

    invoke-static {p1, v0}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    :goto_0
    return-void
.end method

.method public synthetic lambda$saveInventoryAdjustment$1$AdjustInventoryController(Ljava/lang/Long;)V
    .locals 0

    .line 222
    iput-object p1, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->catalogVersion:Ljava/lang/Long;

    .line 223
    invoke-direct {p0}, Lcom/squareup/ui/inventory/AdjustInventoryController;->buildAdjustVariationInventoryRequest()Lcom/squareup/protos/client/AdjustVariationInventoryRequest;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/inventory/AdjustInventoryController;->sendAdjustmentRequest(Lcom/squareup/protos/client/AdjustVariationInventoryRequest;)V

    return-void
.end method

.method public synthetic lambda$sendAdjustmentRequest$4$AdjustInventoryController(Lcom/squareup/protos/client/AdjustVariationInventoryRequest;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 316
    new-instance v0, Lcom/squareup/ui/inventory/-$$Lambda$AdjustInventoryController$e3Q0IlvUmdeQ0AxyZ8fW85awuWc;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/inventory/-$$Lambda$AdjustInventoryController$e3Q0IlvUmdeQ0AxyZ8fW85awuWc;-><init>(Lcom/squareup/ui/inventory/AdjustInventoryController;Lcom/squareup/protos/client/AdjustVariationInventoryRequest;)V

    new-instance v1, Lcom/squareup/ui/inventory/-$$Lambda$AdjustInventoryController$Jark7n7gVHeKUOqbMf29pOmVncs;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/inventory/-$$Lambda$AdjustInventoryController$Jark7n7gVHeKUOqbMf29pOmVncs;-><init>(Lcom/squareup/ui/inventory/AdjustInventoryController;Lcom/squareup/protos/client/AdjustVariationInventoryRequest;)V

    invoke-virtual {p2, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public onBackInAdjustInventorySelectReasonScreen()V
    .locals 2

    .line 255
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryState:Lcom/squareup/ui/inventory/AdjustInventoryState;

    invoke-virtual {v0}, Lcom/squareup/ui/inventory/AdjustInventoryState;->finishAdjustInventory()V

    .line 256
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryStateRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v1, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryState:Lcom/squareup/ui/inventory/AdjustInventoryState;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public onBackInAdjustInventorySpecifyNumberScreen()V
    .locals 2

    .line 260
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryState:Lcom/squareup/ui/inventory/AdjustInventoryState;

    invoke-virtual {v0}, Lcom/squareup/ui/inventory/AdjustInventoryState;->getPhase()Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;->RECEIVING_INITIAL_STOCK:Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 262
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryState:Lcom/squareup/ui/inventory/AdjustInventoryState;

    invoke-virtual {v0}, Lcom/squareup/ui/inventory/AdjustInventoryState;->finishAdjustInventory()V

    goto :goto_1

    .line 264
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryState:Lcom/squareup/ui/inventory/AdjustInventoryState;

    invoke-virtual {v0}, Lcom/squareup/ui/inventory/AdjustInventoryState;->reselectReason()V

    .line 266
    :goto_1
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryStateRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v1, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryState:Lcom/squareup/ui/inventory/AdjustInventoryState;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public onDismissInErrorDialog()V
    .locals 2

    .line 270
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryState:Lcom/squareup/ui/inventory/AdjustInventoryState;

    invoke-virtual {v0}, Lcom/squareup/ui/inventory/AdjustInventoryState;->retryOrDismissAdjustment()V

    .line 271
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryStateRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v1, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryState:Lcom/squareup/ui/inventory/AdjustInventoryState;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 272
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->inventoryAdjustmentSaveStatusRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v1, Lcom/squareup/ui/inventory/AdjustInventoryController$InventoryAdjustmentSaveStatus;->NOT_REQUESTED:Lcom/squareup/ui/inventory/AdjustInventoryController$InventoryAdjustmentSaveStatus;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public onDoneReceivingInitialStock()V
    .locals 4

    .line 242
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryHost:Lcom/squareup/ui/inventory/AdjustInventoryHost;

    invoke-interface {v0}, Lcom/squareup/ui/inventory/AdjustInventoryHost;->shouldDeferInitialStockAdjustment()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 243
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryState:Lcom/squareup/ui/inventory/AdjustInventoryState;

    invoke-virtual {v0}, Lcom/squareup/ui/inventory/AdjustInventoryState;->getVariationId()Ljava/lang/String;

    move-result-object v0

    .line 244
    iget-object v1, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryHost:Lcom/squareup/ui/inventory/AdjustInventoryHost;

    iget-object v2, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryState:Lcom/squareup/ui/inventory/AdjustInventoryState;

    .line 245
    invoke-virtual {v2}, Lcom/squareup/ui/inventory/AdjustInventoryState;->getUserInputCount()Ljava/math/BigDecimal;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryState:Lcom/squareup/ui/inventory/AdjustInventoryState;

    invoke-virtual {v3}, Lcom/squareup/ui/inventory/AdjustInventoryState;->getUserInputUnitCost()Lcom/squareup/protos/common/Money;

    move-result-object v3

    .line 244
    invoke-interface {v1, v0, v2, v3}, Lcom/squareup/ui/inventory/AdjustInventoryHost;->deferInitialStockAdjustment(Ljava/lang/String;Ljava/math/BigDecimal;Lcom/squareup/protos/common/Money;)V

    .line 247
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryState:Lcom/squareup/ui/inventory/AdjustInventoryState;

    invoke-virtual {v0}, Lcom/squareup/ui/inventory/AdjustInventoryState;->finishAdjustInventory()V

    .line 248
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryStateRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v1, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryState:Lcom/squareup/ui/inventory/AdjustInventoryState;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    goto :goto_0

    .line 250
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/inventory/AdjustInventoryController;->saveInventoryAdjustment()V

    :goto_0
    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 9

    .line 115
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/inventory/AdjustInventoryScope;

    iput-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryScope:Lcom/squareup/ui/inventory/AdjustInventoryScope;

    .line 117
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryScope:Lcom/squareup/ui/inventory/AdjustInventoryScope;

    iget-boolean v2, v0, Lcom/squareup/ui/inventory/AdjustInventoryScope;->hasServerStockCount:Z

    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryScope:Lcom/squareup/ui/inventory/AdjustInventoryScope;

    iget-object v3, v0, Lcom/squareup/ui/inventory/AdjustInventoryScope;->variationId:Ljava/lang/String;

    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryScope:Lcom/squareup/ui/inventory/AdjustInventoryScope;

    iget-object v4, v0, Lcom/squareup/ui/inventory/AdjustInventoryScope;->variationMerchantCatalogToken:Ljava/lang/String;

    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryScope:Lcom/squareup/ui/inventory/AdjustInventoryScope;

    iget-object v5, v0, Lcom/squareup/ui/inventory/AdjustInventoryScope;->serverCount:Ljava/math/BigDecimal;

    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryScope:Lcom/squareup/ui/inventory/AdjustInventoryScope;

    iget-object v6, v0, Lcom/squareup/ui/inventory/AdjustInventoryScope;->currentCost:Lcom/squareup/protos/common/Money;

    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryScope:Lcom/squareup/ui/inventory/AdjustInventoryScope;

    iget v7, v0, Lcom/squareup/ui/inventory/AdjustInventoryScope;->precision:I

    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryScope:Lcom/squareup/ui/inventory/AdjustInventoryScope;

    iget-object v8, v0, Lcom/squareup/ui/inventory/AdjustInventoryScope;->unitAbbreviation:Ljava/lang/String;

    move-object v1, p0

    invoke-direct/range {v1 .. v8}, Lcom/squareup/ui/inventory/AdjustInventoryController;->startAdjustInventoryFlow(ZLjava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;Lcom/squareup/protos/common/Money;ILjava/lang/String;)V

    .line 128
    invoke-virtual {p0}, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryState()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/inventory/-$$Lambda$CKnpGWM2tKsbQ6XvqAK_ROuHWz8;->INSTANCE:Lcom/squareup/ui/inventory/-$$Lambda$CKnpGWM2tKsbQ6XvqAK_ROuHWz8;

    .line 129
    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 131
    invoke-virtual {v0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/inventory/-$$Lambda$AdjustInventoryController$uzwOdFYjlrtEuwdyCsDtD-9Uckg;

    invoke-direct {v1, p0}, Lcom/squareup/ui/inventory/-$$Lambda$AdjustInventoryController$uzwOdFYjlrtEuwdyCsDtD-9Uckg;-><init>(Lcom/squareup/ui/inventory/AdjustInventoryController;)V

    .line 132
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 128
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    .line 167
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->compositeDisposable:Lio/reactivex/disposables/CompositeDisposable;

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 3

    if-eqz p1, :cond_0

    const-string v0, "adjust_inventory_state"

    .line 178
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryState:Lcom/squareup/ui/inventory/AdjustInventoryState;

    .line 179
    invoke-virtual {v1}, Lcom/squareup/ui/inventory/AdjustInventoryState;->getPhase()Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;->FINISHED:Lcom/squareup/ui/inventory/AdjustInventoryState$AdjustInventoryPhase;

    if-ne v1, v2, :cond_0

    .line 180
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/inventory/AdjustInventoryState;

    iput-object p1, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryState:Lcom/squareup/ui/inventory/AdjustInventoryState;

    .line 181
    iget-object p1, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryStateRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryState:Lcom/squareup/ui/inventory/AdjustInventoryState;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public onRetryInErrorDialog(Lcom/squareup/protos/client/AdjustVariationInventoryRequest;)V
    .locals 2

    .line 276
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryState:Lcom/squareup/ui/inventory/AdjustInventoryState;

    invoke-virtual {v0}, Lcom/squareup/ui/inventory/AdjustInventoryState;->retryOrDismissAdjustment()V

    .line 277
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryStateRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v1, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryState:Lcom/squareup/ui/inventory/AdjustInventoryState;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 278
    invoke-direct {p0, p1}, Lcom/squareup/ui/inventory/AdjustInventoryController;->sendAdjustmentRequest(Lcom/squareup/protos/client/AdjustVariationInventoryRequest;)V

    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 2

    if-eqz p1, :cond_0

    .line 187
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryState:Lcom/squareup/ui/inventory/AdjustInventoryState;

    const-string v1, "adjust_inventory_state"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    return-void
.end method

.method public saveInventoryAdjustment()V
    .locals 3

    .line 219
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->catalogVersion:Ljava/lang/Long;

    if-nez v0, :cond_0

    .line 220
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->compositeDisposable:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->cogs:Lcom/squareup/cogs/Cogs;

    sget-object v2, Lcom/squareup/ui/inventory/-$$Lambda$aXB2dx93XQiJjkA_uqhUjdR7YZA;->INSTANCE:Lcom/squareup/ui/inventory/-$$Lambda$aXB2dx93XQiJjkA_uqhUjdR7YZA;

    .line 221
    invoke-interface {v1, v2}, Lcom/squareup/cogs/Cogs;->asSingle(Lcom/squareup/shared/catalog/CatalogTask;)Lrx/Single;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/inventory/-$$Lambda$AdjustInventoryController$i5ZT5Zw1d5DNckbpQtIab_V0GKg;

    invoke-direct {v2, p0}, Lcom/squareup/ui/inventory/-$$Lambda$AdjustInventoryController$i5ZT5Zw1d5DNckbpQtIab_V0GKg;-><init>(Lcom/squareup/ui/inventory/AdjustInventoryController;)V

    invoke-virtual {v1, v2}, Lrx/Single;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v1

    .line 220
    invoke-static {v1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Disposable(Lrx/Subscription;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    goto :goto_0

    .line 228
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/inventory/AdjustInventoryController;->buildAdjustVariationInventoryRequest()Lcom/squareup/protos/client/AdjustVariationInventoryRequest;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/ui/inventory/AdjustInventoryController;->sendAdjustmentRequest(Lcom/squareup/protos/client/AdjustVariationInventoryRequest;)V

    :goto_0
    return-void
.end method

.method shouldIncreaseStockCount(Lcom/squareup/protos/client/InventoryAdjustmentReason;)Z
    .locals 3

    .line 296
    sget-object v0, Lcom/squareup/ui/inventory/AdjustInventoryController$1;->$SwitchMap$com$squareup$protos$client$InventoryAdjustmentReason:[I

    invoke-virtual {p1}, Lcom/squareup/protos/client/InventoryAdjustmentReason;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v2, 0x2

    if-eq v0, v2, :cond_2

    const/4 v2, 0x3

    if-eq v0, v2, :cond_2

    const/4 v2, 0x4

    if-eq v0, v2, :cond_1

    const/4 v2, 0x5

    if-ne v0, v2, :cond_0

    goto :goto_0

    .line 305
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Illegal Reason: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    return v1

    :cond_2
    const/4 p1, 0x0

    return p1
.end method

.method stockCountUpdated(Ljava/math/BigDecimal;)V
    .locals 1

    .line 282
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryState:Lcom/squareup/ui/inventory/AdjustInventoryState;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/inventory/AdjustInventoryState;->updateUserInputCount(Ljava/math/BigDecimal;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 283
    iget-object p1, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryStateRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryState:Lcom/squareup/ui/inventory/AdjustInventoryState;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method unitCostTextChanged(Ljava/lang/String;)V
    .locals 2

    .line 288
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iget-object v1, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryState:Lcom/squareup/ui/inventory/AdjustInventoryState;

    .line 289
    invoke-virtual {v1}, Lcom/squareup/ui/inventory/AdjustInventoryState;->getUnitAbbreviation()Ljava/lang/String;

    move-result-object v1

    .line 288
    invoke-virtual {v0, p1, v1}, Lcom/squareup/money/PriceLocaleHelper;->extractMoney(Ljava/lang/CharSequence;Ljava/lang/String;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 290
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryState:Lcom/squareup/ui/inventory/AdjustInventoryState;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/inventory/AdjustInventoryState;->updateUserInputUnitCost(Lcom/squareup/protos/common/Money;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 291
    iget-object p1, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryStateRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryState:Lcom/squareup/ui/inventory/AdjustInventoryState;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method
