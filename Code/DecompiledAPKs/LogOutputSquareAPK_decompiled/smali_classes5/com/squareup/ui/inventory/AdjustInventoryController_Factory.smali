.class public final Lcom/squareup/ui/inventory/AdjustInventoryController_Factory;
.super Ljava/lang/Object;
.source "AdjustInventoryController_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/inventory/AdjustInventoryController;",
        ">;"
    }
.end annotation


# instance fields
.field private final adjustInventoryHostProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/inventory/AdjustInventoryHost;",
            ">;"
        }
    .end annotation
.end field

.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final cogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final inventoryServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/inventory/InventoryService;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final priceLocaleHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/PriceLocaleHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/inventory/InventoryService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/PriceLocaleHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/inventory/AdjustInventoryHost;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;)V"
        }
    .end annotation

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/squareup/ui/inventory/AdjustInventoryController_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p2, p0, Lcom/squareup/ui/inventory/AdjustInventoryController_Factory;->inventoryServiceProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p3, p0, Lcom/squareup/ui/inventory/AdjustInventoryController_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p4, p0, Lcom/squareup/ui/inventory/AdjustInventoryController_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p5, p0, Lcom/squareup/ui/inventory/AdjustInventoryController_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p6, p0, Lcom/squareup/ui/inventory/AdjustInventoryController_Factory;->priceLocaleHelperProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p7, p0, Lcom/squareup/ui/inventory/AdjustInventoryController_Factory;->adjustInventoryHostProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p8, p0, Lcom/squareup/ui/inventory/AdjustInventoryController_Factory;->cogsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/inventory/AdjustInventoryController_Factory;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/inventory/InventoryService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/PriceLocaleHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/inventory/AdjustInventoryHost;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;)",
            "Lcom/squareup/ui/inventory/AdjustInventoryController_Factory;"
        }
    .end annotation

    .line 61
    new-instance v9, Lcom/squareup/ui/inventory/AdjustInventoryController_Factory;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/ui/inventory/AdjustInventoryController_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v9
.end method

.method public static newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/server/inventory/InventoryService;Lio/reactivex/Scheduler;Lflow/Flow;Lcom/squareup/analytics/Analytics;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/ui/inventory/AdjustInventoryHost;Lcom/squareup/cogs/Cogs;)Lcom/squareup/ui/inventory/AdjustInventoryController;
    .locals 10

    .line 67
    new-instance v9, Lcom/squareup/ui/inventory/AdjustInventoryController;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/ui/inventory/AdjustInventoryController;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/server/inventory/InventoryService;Lio/reactivex/Scheduler;Lflow/Flow;Lcom/squareup/analytics/Analytics;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/ui/inventory/AdjustInventoryHost;Lcom/squareup/cogs/Cogs;)V

    return-object v9
.end method


# virtual methods
.method public get()Lcom/squareup/ui/inventory/AdjustInventoryController;
    .locals 9

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController_Factory;->inventoryServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/server/inventory/InventoryService;

    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lio/reactivex/Scheduler;

    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lflow/Flow;

    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController_Factory;->priceLocaleHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/money/PriceLocaleHelper;

    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController_Factory;->adjustInventoryHostProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/ui/inventory/AdjustInventoryHost;

    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventoryController_Factory;->cogsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/cogs/Cogs;

    invoke-static/range {v1 .. v8}, Lcom/squareup/ui/inventory/AdjustInventoryController_Factory;->newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/server/inventory/InventoryService;Lio/reactivex/Scheduler;Lflow/Flow;Lcom/squareup/analytics/Analytics;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/ui/inventory/AdjustInventoryHost;Lcom/squareup/cogs/Cogs;)Lcom/squareup/ui/inventory/AdjustInventoryController;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/ui/inventory/AdjustInventoryController_Factory;->get()Lcom/squareup/ui/inventory/AdjustInventoryController;

    move-result-object v0

    return-object v0
.end method
