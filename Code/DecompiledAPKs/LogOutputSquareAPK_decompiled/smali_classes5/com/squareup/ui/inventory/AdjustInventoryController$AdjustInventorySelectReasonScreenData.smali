.class Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventorySelectReasonScreenData;
.super Ljava/lang/Object;
.source "AdjustInventoryController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/inventory/AdjustInventoryController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "AdjustInventorySelectReasonScreenData"
.end annotation


# instance fields
.field final reasons:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;",
            ">;)V"
        }
    .end annotation

    .line 485
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 486
    iput-object p1, p0, Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventorySelectReasonScreenData;->reasons:Ljava/util/List;

    return-void
.end method
