.class public final Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator_Factory;
.super Ljava/lang/Object;
.source "AdjustInventorySpecifyNumberCoordinator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;",
        ">;"
    }
.end annotation


# instance fields
.field private final adjustInventoryControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/inventory/AdjustInventoryController;",
            ">;"
        }
    .end annotation
.end field

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final priceLocaleHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/PriceLocaleHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialCoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/PriceLocaleHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/inventory/AdjustInventoryController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;)V"
        }
    .end annotation

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator_Factory;->resProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p2, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator_Factory;->priceLocaleHelperProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p3, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator_Factory;->adjustInventoryControllerProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p4, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p5, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator_Factory;->localeProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p6, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/PriceLocaleHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/inventory/AdjustInventoryController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;)",
            "Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator_Factory;"
        }
    .end annotation

    .line 52
    new-instance v7, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/util/Res;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/ui/inventory/AdjustInventoryController;Lcom/squareup/quantity/PerUnitFormatter;Ljavax/inject/Provider;Lcom/squareup/tutorialv2/TutorialCore;)Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/money/PriceLocaleHelper;",
            "Lcom/squareup/ui/inventory/AdjustInventoryController;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ")",
            "Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;"
        }
    .end annotation

    .line 58
    new-instance v7, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;-><init>(Lcom/squareup/util/Res;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/ui/inventory/AdjustInventoryController;Lcom/squareup/quantity/PerUnitFormatter;Ljavax/inject/Provider;Lcom/squareup/tutorialv2/TutorialCore;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;
    .locals 7

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator_Factory;->priceLocaleHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/money/PriceLocaleHelper;

    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator_Factory;->adjustInventoryControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/ui/inventory/AdjustInventoryController;

    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v5, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator_Factory;->localeProvider:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/tutorialv2/TutorialCore;

    invoke-static/range {v1 .. v6}, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator_Factory;->newInstance(Lcom/squareup/util/Res;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/ui/inventory/AdjustInventoryController;Lcom/squareup/quantity/PerUnitFormatter;Ljavax/inject/Provider;Lcom/squareup/tutorialv2/TutorialCore;)Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator_Factory;->get()Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;

    move-result-object v0

    return-object v0
.end method
