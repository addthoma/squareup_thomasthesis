.class public final synthetic Lcom/squareup/ui/inventory/-$$Lambda$AdjustInventoryErrorDialogScreen$Factory$qwlRDNhJQHXmLuVJ_TDd0EiKPv8;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lio/reactivex/functions/Function;


# instance fields
.field private final synthetic f$0:Landroid/content/res/Resources;

.field private final synthetic f$1:Landroid/content/Context;

.field private final synthetic f$2:Lcom/squareup/ui/inventory/AdjustInventoryController;


# direct methods
.method public synthetic constructor <init>(Landroid/content/res/Resources;Landroid/content/Context;Lcom/squareup/ui/inventory/AdjustInventoryController;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/inventory/-$$Lambda$AdjustInventoryErrorDialogScreen$Factory$qwlRDNhJQHXmLuVJ_TDd0EiKPv8;->f$0:Landroid/content/res/Resources;

    iput-object p2, p0, Lcom/squareup/ui/inventory/-$$Lambda$AdjustInventoryErrorDialogScreen$Factory$qwlRDNhJQHXmLuVJ_TDd0EiKPv8;->f$1:Landroid/content/Context;

    iput-object p3, p0, Lcom/squareup/ui/inventory/-$$Lambda$AdjustInventoryErrorDialogScreen$Factory$qwlRDNhJQHXmLuVJ_TDd0EiKPv8;->f$2:Lcom/squareup/ui/inventory/AdjustInventoryController;

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    iget-object v0, p0, Lcom/squareup/ui/inventory/-$$Lambda$AdjustInventoryErrorDialogScreen$Factory$qwlRDNhJQHXmLuVJ_TDd0EiKPv8;->f$0:Landroid/content/res/Resources;

    iget-object v1, p0, Lcom/squareup/ui/inventory/-$$Lambda$AdjustInventoryErrorDialogScreen$Factory$qwlRDNhJQHXmLuVJ_TDd0EiKPv8;->f$1:Landroid/content/Context;

    iget-object v2, p0, Lcom/squareup/ui/inventory/-$$Lambda$AdjustInventoryErrorDialogScreen$Factory$qwlRDNhJQHXmLuVJ_TDd0EiKPv8;->f$2:Lcom/squareup/ui/inventory/AdjustInventoryController;

    check-cast p1, Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventoryErrorDialogScreenData;

    invoke-static {v0, v1, v2, p1}, Lcom/squareup/ui/inventory/AdjustInventoryErrorDialogScreen$Factory;->lambda$create$1(Landroid/content/res/Resources;Landroid/content/Context;Lcom/squareup/ui/inventory/AdjustInventoryController;Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventoryErrorDialogScreenData;)Landroid/app/Dialog;

    move-result-object p1

    return-object p1
.end method
