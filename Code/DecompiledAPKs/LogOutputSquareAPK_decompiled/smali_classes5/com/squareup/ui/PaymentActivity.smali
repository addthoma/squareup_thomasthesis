.class public Lcom/squareup/ui/PaymentActivity;
.super Lcom/squareup/ui/SquareActivity;
.source "PaymentActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/PaymentActivity$StartPaymentActivity;,
        Lcom/squareup/ui/PaymentActivity$LoggedInComponent;,
        Lcom/squareup/ui/PaymentActivity$Component;
    }
.end annotation


# static fields
.field private static final REQUEST_CODE:I = 0x2


# instance fields
.field authenticator:Lcom/squareup/account/LegacyAuthenticator;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field loggedOutStarter:Lcom/squareup/loggedout/LoggedOutStarter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 114
    sget-object v0, Lcom/squareup/ui/SquareActivity$Preconditions;->NO_AUTH_NEEDED:Lcom/squareup/ui/SquareActivity$Preconditions;

    invoke-direct {p0, v0}, Lcom/squareup/ui/SquareActivity;-><init>(Lcom/squareup/ui/SquareActivity$Preconditions;)V

    return-void
.end method

.method public static createPendingIntent(Landroid/content/Context;)Landroid/app/PendingIntent;
    .locals 1

    .line 83
    invoke-static {p0}, Lcom/squareup/ui/PaymentActivity;->doCreateIntent(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/PendingIntent;->cancel()V

    .line 84
    invoke-static {p0}, Lcom/squareup/ui/PaymentActivity;->doCreateIntent(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object p0

    return-object p0
.end method

.method public static createPendingIntentForDeepLink(Landroid/content/Context;Landroid/net/Uri;)Landroid/app/PendingIntent;
    .locals 1

    .line 98
    invoke-static {p0, p1}, Lcom/squareup/ui/PaymentActivity;->doCreateIntentForDeepLink(Landroid/content/Context;Landroid/net/Uri;)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/PendingIntent;->cancel()V

    .line 99
    invoke-static {p0, p1}, Lcom/squareup/ui/PaymentActivity;->doCreateIntentForDeepLink(Landroid/content/Context;Landroid/net/Uri;)Landroid/app/PendingIntent;

    move-result-object p0

    return-object p0
.end method

.method private static doCreateIntent(Landroid/content/Context;)Landroid/app/PendingIntent;
    .locals 3

    .line 88
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/squareup/ui/PaymentActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "android.intent.action.MAIN"

    .line 89
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "android.intent.category.LAUNCHER"

    .line 90
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    const/4 v1, 0x2

    const/high16 v2, 0x8000000

    .line 92
    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object p0

    return-object p0
.end method

.method private static doCreateIntentForDeepLink(Landroid/content/Context;Landroid/net/Uri;)Landroid/app/PendingIntent;
    .locals 2

    .line 103
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/squareup/ui/PaymentActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 104
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const-string p1, "android.intent.action.VIEW"

    .line 105
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const/4 p1, 0x2

    const/high16 v1, 0x8000000

    .line 107
    invoke-static {p0, p1, v0, v1}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object p0

    return-object p0
.end method

.method public static exitWithResult(Landroid/app/Activity;I)V
    .locals 2

    .line 183
    invoke-static {p0}, Lcom/squareup/util/TaskLock;->isInTaskLockMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 187
    :cond_0
    invoke-virtual {p0, p1}, Landroid/app/Activity;->setResult(I)V

    .line 194
    new-instance p1, Landroid/content/Intent;

    const-class v0, Lcom/squareup/ui/PaymentActivity;

    invoke-direct {p1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v0, 0x6000000

    .line 195
    invoke-virtual {p1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const/4 v0, 0x1

    const-string v1, "com.squareup.EXIT_APP"

    .line 197
    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 198
    invoke-virtual {p0, p1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 200
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method private finishIfExiting()Z
    .locals 4

    .line 257
    invoke-virtual {p0}, Lcom/squareup/ui/PaymentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.squareup.EXIT_APP"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    new-array v3, v0, [Ljava/lang/Object;

    aput-object v1, v3, v2

    const-string v1, "PaymentActivity finishing because %s = true."

    .line 258
    invoke-static {v1, v3}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 259
    invoke-virtual {p0}, Lcom/squareup/ui/PaymentActivity;->useDefaultExitAnimation()V

    .line 260
    invoke-virtual {p0}, Lcom/squareup/ui/PaymentActivity;->finish()V

    return v0

    :cond_0
    return v2
.end method

.method private isWronglyStarted()Z
    .locals 5

    .line 161
    invoke-virtual {p0}, Lcom/squareup/ui/PaymentActivity;->isTaskRoot()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_1

    .line 162
    invoke-virtual {p0}, Lcom/squareup/ui/PaymentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 163
    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v4, "android.intent.action.MAIN"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    const-string v4, "android.intent.category.LAUNCHER"

    .line 164
    invoke-virtual {v0, v4}, Landroid/content/Intent;->hasCategory(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz v2, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method public static reset(Landroid/content/Context;)V
    .locals 2

    .line 210
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/squareup/ui/PaymentActivity$StartPaymentActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x10000000

    .line 211
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 212
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 214
    invoke-static {p0}, Lcom/squareup/util/Contexts;->getActivity(Landroid/content/Context;)Landroid/app/Activity;

    move-result-object p0

    if-eqz p0, :cond_0

    const/4 v0, 0x0

    const v1, 0x10a0001

    .line 217
    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->overridePendingTransition(II)V

    .line 219
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :cond_0
    return-void
.end method

.method private startNextActivity()V
    .locals 3

    .line 225
    iget-object v0, p0, Lcom/squareup/ui/PaymentActivity;->authenticator:Lcom/squareup/account/LegacyAuthenticator;

    invoke-interface {v0}, Lcom/squareup/account/LegacyAuthenticator;->isLoggedIn()Z

    move-result v0

    if-nez v0, :cond_0

    .line 226
    iget-object v0, p0, Lcom/squareup/ui/PaymentActivity;->loggedOutStarter:Lcom/squareup/loggedout/LoggedOutStarter;

    invoke-interface {v0, p0}, Lcom/squareup/loggedout/LoggedOutStarter;->startLoggedOutActivity(Landroid/app/Activity;)V

    goto :goto_1

    .line 228
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/squareup/ui/main/MainActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 231
    invoke-virtual {p0}, Lcom/squareup/ui/PaymentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 232
    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 233
    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 234
    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/content/Intent;)Landroid/content/Intent;

    const/high16 v1, 0x2000000

    .line 235
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 237
    invoke-virtual {p0, v0}, Lcom/squareup/ui/PaymentActivity;->startActivity(Landroid/content/Intent;)V

    .line 241
    const-class v0, Lcom/squareup/ui/PaymentActivity$LoggedInComponent;

    invoke-static {v0}, Lcom/squareup/MortarLoggedIn;->getLoggedInComponent(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/PaymentActivity$LoggedInComponent;

    .line 242
    invoke-interface {v0}, Lcom/squareup/ui/PaymentActivity$LoggedInComponent;->jailKeeper()Lcom/squareup/jailkeeper/JailKeeper;

    move-result-object v0

    .line 243
    invoke-interface {v0}, Lcom/squareup/jailkeeper/JailKeeper;->getState()Lcom/squareup/jailkeeper/JailKeeper$State;

    move-result-object v1

    sget-object v2, Lcom/squareup/jailkeeper/JailKeeper$State;->FAILED:Lcom/squareup/jailkeeper/JailKeeper$State;

    if-eq v1, v2, :cond_2

    invoke-interface {v0}, Lcom/squareup/jailkeeper/JailKeeper;->getState()Lcom/squareup/jailkeeper/JailKeeper$State;

    move-result-object v0

    sget-object v1, Lcom/squareup/jailkeeper/JailKeeper$State;->SYNCING:Lcom/squareup/jailkeeper/JailKeeper$State;

    if-ne v0, v1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 251
    sget v1, Lcom/squareup/widgets/R$anim;->slide_out_to_left:I

    invoke-virtual {p0, v0, v1}, Lcom/squareup/ui/PaymentActivity;->overridePendingTransition(II)V

    goto :goto_1

    .line 246
    :cond_2
    :goto_0
    sget v0, Lcom/squareup/widgets/R$anim;->slide_in_from_right:I

    sget v1, Lcom/squareup/widgets/R$anim;->slide_out_to_left:I

    invoke-virtual {p0, v0, v1}, Lcom/squareup/ui/PaymentActivity;->overridePendingTransition(II)V

    :goto_1
    return-void
.end method


# virtual methods
.method protected doCreate(Landroid/os/Bundle;)V
    .locals 0

    .line 128
    invoke-super {p0, p1}, Lcom/squareup/ui/SquareActivity;->doCreate(Landroid/os/Bundle;)V

    .line 130
    invoke-direct {p0}, Lcom/squareup/ui/PaymentActivity;->isWronglyStarted()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 131
    invoke-virtual {p0}, Lcom/squareup/ui/PaymentActivity;->finish()V

    :cond_0
    return-void
.end method

.method protected inject(Lmortar/MortarScope;)V
    .locals 1

    .line 118
    const-class v0, Lcom/squareup/ui/PaymentActivity$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/PaymentActivity$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/PaymentActivity$Component;->inject(Lcom/squareup/ui/PaymentActivity;)V

    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0

    .line 124
    invoke-virtual {p0, p1}, Lcom/squareup/ui/PaymentActivity;->setIntent(Landroid/content/Intent;)V

    return-void
.end method

.method protected onResume()V
    .locals 3

    .line 170
    invoke-super {p0}, Lcom/squareup/ui/SquareActivity;->onResume()V

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 172
    invoke-virtual {p0}, Lcom/squareup/ui/PaymentActivity;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "PaymentActivity.onResume(), getCallingActivity() = %s"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 176
    invoke-direct {p0}, Lcom/squareup/ui/PaymentActivity;->finishIfExiting()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 178
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/PaymentActivity;->startNextActivity()V

    return-void
.end method
