.class public final Lcom/squareup/ui/activity/ActivityDeepLinkHandler;
.super Ljava/lang/Object;
.source "ActivityDeepLinkHandler.kt"

# interfaces
.implements Lcom/squareup/deeplinks/DeepLinkHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/activity/ActivityDeepLinkHandler$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nActivityDeepLinkHandler.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ActivityDeepLinkHandler.kt\ncom/squareup/ui/activity/ActivityDeepLinkHandler\n*L\n1#1,25:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 \t2\u00020\u0001:\u0001\tB\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/ui/activity/ActivityDeepLinkHandler;",
        "Lcom/squareup/deeplinks/DeepLinkHandler;",
        "activityApplet",
        "Lcom/squareup/ui/activity/ActivityApplet;",
        "(Lcom/squareup/ui/activity/ActivityApplet;)V",
        "handleExternal",
        "Lcom/squareup/deeplinks/DeepLinkResult;",
        "uri",
        "Landroid/net/Uri;",
        "Companion",
        "bill-history-ui_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final ACTIVITY:Ljava/lang/String; = "activity"

.field public static final Companion:Lcom/squareup/ui/activity/ActivityDeepLinkHandler$Companion;


# instance fields
.field private final activityApplet:Lcom/squareup/ui/activity/ActivityApplet;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/activity/ActivityDeepLinkHandler$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/activity/ActivityDeepLinkHandler$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/activity/ActivityDeepLinkHandler;->Companion:Lcom/squareup/ui/activity/ActivityDeepLinkHandler$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/activity/ActivityApplet;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "activityApplet"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/activity/ActivityDeepLinkHandler;->activityApplet:Lcom/squareup/ui/activity/ActivityApplet;

    return-void
.end method


# virtual methods
.method public handleExternal(Landroid/net/Uri;)Lcom/squareup/deeplinks/DeepLinkResult;
    .locals 2

    const-string/jumbo v0, "uri"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 14
    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object p1

    const-string v1, "activity"

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    move-object p1, v0

    check-cast p1, Ljava/lang/CharSequence;

    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result p1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    if-nez p1, :cond_2

    const-string p1, "/"

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 15
    :cond_2
    new-instance p1, Lcom/squareup/deeplinks/DeepLinkResult;

    iget-object v0, p0, Lcom/squareup/ui/activity/ActivityDeepLinkHandler;->activityApplet:Lcom/squareup/ui/activity/ActivityApplet;

    check-cast v0, Lcom/squareup/ui/main/HistoryFactory;

    invoke-direct {p1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/ui/main/HistoryFactory;)V

    goto :goto_2

    .line 17
    :cond_3
    new-instance p1, Lcom/squareup/deeplinks/DeepLinkResult;

    sget-object v0, Lcom/squareup/deeplinks/DeepLinkStatus;->UNRECOGNIZED:Lcom/squareup/deeplinks/DeepLinkStatus;

    invoke-direct {p1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/deeplinks/DeepLinkStatus;)V

    :goto_2
    return-object p1
.end method
