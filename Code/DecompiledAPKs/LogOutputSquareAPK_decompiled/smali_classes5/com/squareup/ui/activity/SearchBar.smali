.class public interface abstract Lcom/squareup/ui/activity/SearchBar;
.super Ljava/lang/Object;
.source "SearchBar.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000J\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\r\n\u0002\u0008\u0003\u0008`\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0008\u0010\u0006\u001a\u00020\u0003H&J\n\u0010\u0007\u001a\u0004\u0018\u00010\u0008H&J\u0010\u0010\t\u001a\u00020\u00032\u0006\u0010\n\u001a\u00020\u000bH&J\u0008\u0010\u000c\u001a\u00020\u000bH&J\u0008\u0010\r\u001a\u00020\u000bH&J\u0010\u0010\u000e\u001a\u00020\u00032\u0006\u0010\u000f\u001a\u00020\u0010H&J\u0010\u0010\u0011\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0012H&J\u0012\u0010\u0013\u001a\u00020\u00032\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0014H&J\u0010\u0010\u0015\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0016H&J\u0010\u0010\u0017\u001a\u00020\u00032\u0006\u0010\u0018\u001a\u00020\u0019H&J\u0010\u0010\u001a\u001a\u00020\u00032\u0006\u0010\u001b\u001a\u00020\u000bH&\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/ui/activity/SearchBar;",
        "",
        "addTextChangedListener",
        "",
        "listener",
        "Landroid/text/TextWatcher;",
        "clearFocus",
        "getText",
        "Landroid/text/Editable;",
        "isEnabled",
        "enabled",
        "",
        "isFocused",
        "requestFocus",
        "setHint",
        "hint",
        "",
        "setOnClickListener",
        "Landroid/view/View$OnClickListener;",
        "setOnEditorActionListener",
        "Lcom/squareup/debounce/DebouncedOnEditorActionListener;",
        "setOnFocusChangeListener",
        "Landroid/view/View$OnFocusChangeListener;",
        "setText",
        "text",
        "",
        "setVisibleOrGone",
        "visible",
        "bill-history-ui_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract addTextChangedListener(Landroid/text/TextWatcher;)V
.end method

.method public abstract clearFocus()V
.end method

.method public abstract getText()Landroid/text/Editable;
.end method

.method public abstract isEnabled(Z)V
.end method

.method public abstract isFocused()Z
.end method

.method public abstract requestFocus()Z
.end method

.method public abstract setHint(Ljava/lang/String;)V
.end method

.method public abstract setOnClickListener(Landroid/view/View$OnClickListener;)V
.end method

.method public abstract setOnEditorActionListener(Lcom/squareup/debounce/DebouncedOnEditorActionListener;)V
.end method

.method public abstract setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V
.end method

.method public abstract setText(Ljava/lang/CharSequence;)V
.end method

.method public abstract setVisibleOrGone(Z)V
.end method
