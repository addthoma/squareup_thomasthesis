.class public final Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;
.super Lmortar/ViewPresenter;
.source "LegacyBillHistoryDetailPresenter.java"

# interfaces
.implements Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/activity/BillHistoryDetailView;",
        ">;",
        "Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;"
    }
.end annotation


# instance fields
.field private final billHistoryRunner:Lcom/squareup/ui/activity/BillHistoryRunner;

.field private final cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

.field private final currentBill:Lcom/squareup/activity/CurrentBill;

.field private final device:Lcom/squareup/util/Device;

.field private final expiryCalculator:Lcom/squareup/activity/ExpiryCalculator;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final flow:Lflow/Flow;

.field private final legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final settleTipsPresenter:Lcom/squareup/ui/activity/SettleTipsSectionPresenter;


# direct methods
.method constructor <init>(Lflow/Flow;Lcom/squareup/activity/CurrentBill;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/settings/server/Features;Lcom/squareup/activity/LegacyTransactionsHistory;Lcom/squareup/util/Device;Lcom/squareup/ui/activity/SettleTipsSectionPresenter;Lcom/squareup/activity/ExpiryCalculator;Lcom/squareup/ui/activity/BillHistoryRunner;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflow/Flow;",
            "Lcom/squareup/activity/CurrentBill;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/cardreader/CardReaderHub;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/activity/LegacyTransactionsHistory;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/ui/activity/SettleTipsSectionPresenter;",
            "Lcom/squareup/activity/ExpiryCalculator;",
            "Lcom/squareup/ui/activity/BillHistoryRunner;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 75
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 76
    iput-object p1, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->flow:Lflow/Flow;

    .line 77
    iput-object p2, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->currentBill:Lcom/squareup/activity/CurrentBill;

    .line 78
    iput-object p3, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 79
    iput-object p4, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->res:Lcom/squareup/util/Res;

    .line 80
    iput-object p5, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 81
    iput-object p6, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    .line 82
    iput-object p7, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->features:Lcom/squareup/settings/server/Features;

    .line 83
    iput-object p8, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    .line 84
    iput-object p9, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->device:Lcom/squareup/util/Device;

    .line 85
    iput-object p10, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->settleTipsPresenter:Lcom/squareup/ui/activity/SettleTipsSectionPresenter;

    .line 86
    iput-object p11, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->expiryCalculator:Lcom/squareup/activity/ExpiryCalculator;

    .line 87
    iput-object p12, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->billHistoryRunner:Lcom/squareup/ui/activity/BillHistoryRunner;

    return-void
.end method

.method private getBill()Lcom/squareup/billhistory/model/BillHistory;
    .locals 2

    .line 319
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->currentBill:Lcom/squareup/activity/CurrentBill;

    invoke-virtual {v0}, Lcom/squareup/activity/CurrentBill;->isSet()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 320
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    iget-object v1, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->currentBill:Lcom/squareup/activity/CurrentBill;

    invoke-virtual {v1}, Lcom/squareup/activity/CurrentBill;->getId()Lcom/squareup/billhistory/model/BillHistoryId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/activity/LegacyTransactionsHistory;->getBill(Lcom/squareup/billhistory/model/BillHistoryId;)Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method private getEmptyViewMessageResId()I
    .locals 6

    .line 212
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderHub;->getCardReaders()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/CardReader;

    .line 213
    invoke-interface {v3}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/squareup/cardreader/CardReaderInfo;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v4

    sget-object v5, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R6:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    if-ne v4, v5, :cond_1

    iget-object v4, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v5, Lcom/squareup/settings/server/Features$Feature;->USE_R6:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v4, v5}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v4

    if-nez v4, :cond_1

    goto :goto_0

    .line 216
    :cond_1
    invoke-interface {v3}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/squareup/cardreader/CardReaderInfo;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v4

    sget-object v5, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R12:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    if-ne v4, v5, :cond_2

    iget-object v4, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v5, Lcom/squareup/settings/server/Features$Feature;->USE_R12:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v4, v5}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v4

    if-nez v4, :cond_2

    goto :goto_0

    .line 220
    :cond_2
    invoke-interface {v3}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/squareup/cardreader/CardReaderInfo;->supportsSwipes()Z

    move-result v4

    const/4 v5, 0x1

    if-eqz v4, :cond_3

    const/4 v1, 0x1

    .line 223
    :cond_3
    invoke-interface {v3}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/cardreader/CardReaderInfo;->supportsDips()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_4
    if-eqz v1, :cond_5

    if-eqz v2, :cond_5

    .line 229
    sget v0, Lcom/squareup/billhistoryui/R$string;->bill_history_empty_view_with_swipe_and_dip_card_readers:I

    return v0

    :cond_5
    if-eqz v1, :cond_6

    .line 231
    sget v0, Lcom/squareup/billhistoryui/R$string;->bill_history_empty_view_with_swipe_card_reader:I

    return v0

    :cond_6
    if-eqz v2, :cond_7

    .line 233
    sget v0, Lcom/squareup/billhistoryui/R$string;->bill_history_empty_view_with_dip_card_reader:I

    return v0

    .line 235
    :cond_7
    sget v0, Lcom/squareup/billhistoryui/R$string;->bill_history_empty_view_without_card_reader:I

    return v0
.end method

.method private handleLoadedState(Lcom/squareup/ui/activity/BillHistoryDetailView;)V
    .locals 2

    .line 180
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    invoke-virtual {v0}, Lcom/squareup/activity/LegacyTransactionsHistory;->getBills()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ENHANCED_TRANSACTION_SEARCH:Lcom/squareup/settings/server/Features$Feature;

    .line 181
    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 183
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->showEmptyView(Lcom/squareup/ui/activity/BillHistoryDetailView;)V

    goto :goto_0

    .line 186
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->showNoPayments(Lcom/squareup/ui/activity/BillHistoryDetailView;)V

    :goto_0
    return-void
.end method

.method private showBill(Lcom/squareup/ui/activity/BillHistoryDetailView;)V
    .locals 4

    .line 240
    invoke-virtual {p1}, Lcom/squareup/ui/activity/BillHistoryDetailView;->showContent()V

    .line 242
    invoke-direct {p0}, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->getBill()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->currentBill:Lcom/squareup/activity/CurrentBill;

    .line 244
    invoke-virtual {v2}, Lcom/squareup/activity/CurrentBill;->getId()Lcom/squareup/billhistory/model/BillHistoryId;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "getBill() returned null, cannot update UI. Bill ID: %s"

    .line 243
    invoke-static {v2, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 242
    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/billhistory/model/BillHistory;

    .line 246
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->updateTitle(Lcom/squareup/ui/activity/BillHistoryDetailView;Lcom/squareup/billhistory/model/BillHistory;)V

    .line 247
    iget-object v1, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->updateStatus(Lcom/squareup/ui/activity/BillHistoryDetailView;Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/settings/server/AccountStatusSettings;)V

    .line 248
    invoke-direct {p0, v0}, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->updateSettleTips(Lcom/squareup/billhistory/model/BillHistory;)V

    .line 250
    invoke-virtual {p1}, Lcom/squareup/ui/activity/BillHistoryDetailView;->billHistoryView()Lcom/squareup/ui/activity/billhistory/BillHistoryView;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->show(Lcom/squareup/billhistory/model/BillHistory;)V

    return-void
.end method

.method private showEmptyView(Lcom/squareup/ui/activity/BillHistoryDetailView;)V
    .locals 1

    .line 202
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/PaymentSettings;->eligibleForSquareCardProcessing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 203
    invoke-direct {p0}, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->getEmptyViewMessageResId()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/activity/BillHistoryDetailView;->showEmptyView(I)V

    :cond_0
    return-void
.end method

.method private showError(Lcom/squareup/ui/activity/BillHistoryDetailView;Lcom/squareup/activity/LoaderError;)V
    .locals 1

    .line 191
    iget-object v0, p2, Lcom/squareup/activity/LoaderError;->title:Ljava/lang/String;

    iget-object p2, p2, Lcom/squareup/activity/LoaderError;->message:Ljava/lang/String;

    invoke-virtual {p1, v0, p2}, Lcom/squareup/ui/activity/BillHistoryDetailView;->showMessage(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    return-void
.end method

.method private showLoading(Lcom/squareup/ui/activity/BillHistoryDetailView;)V
    .locals 0

    .line 149
    invoke-virtual {p1}, Lcom/squareup/ui/activity/BillHistoryDetailView;->showLoading()V

    return-void
.end method

.method private showNoPayments(Lcom/squareup/ui/activity/BillHistoryDetailView;)V
    .locals 3

    .line 195
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/billhistoryui/R$string;->activity_applet_no_payments_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/billhistoryui/R$string;->activity_applet_no_payments_message:I

    .line 196
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 195
    invoke-virtual {p1, v0, v1}, Lcom/squareup/ui/activity/BillHistoryDetailView;->showMessage(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 197
    invoke-virtual {p1}, Lcom/squareup/ui/activity/BillHistoryDetailView;->setSupportCenterButton()V

    return-void
.end method

.method private updateSettleTips(Lcom/squareup/billhistory/model/BillHistory;)V
    .locals 1

    .line 272
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->settleTipsPresenter:Lcom/squareup/ui/activity/SettleTipsSectionPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->setBill(Lcom/squareup/billhistory/model/BillHistory;)V

    return-void
.end method

.method private updateStatus(Lcom/squareup/ui/activity/BillHistoryDetailView;Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/settings/server/AccountStatusSettings;)V
    .locals 3

    .line 277
    invoke-virtual {p1}, Lcom/squareup/ui/activity/BillHistoryDetailView;->clearStatusLink()V

    .line 279
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->expiryCalculator:Lcom/squareup/activity/ExpiryCalculator;

    invoke-virtual {v0, p2}, Lcom/squareup/activity/ExpiryCalculator;->isStoredPaymentExpiringSoon(Lcom/squareup/billhistory/model/BillHistory;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 280
    iget-object p2, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/billhistoryui/R$string;->payment_expiring:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 282
    invoke-virtual {p3}, Lcom/squareup/settings/server/AccountStatusSettings;->getStoreAndForwardSettings()Lcom/squareup/settings/server/StoreAndForwardSettings;

    move-result-object p3

    invoke-virtual {p3}, Lcom/squareup/settings/server/StoreAndForwardSettings;->getPaymentExpirationHours()I

    move-result p3

    .line 283
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/billhistoryui/R$string;->payment_expiring_message:I

    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v2, "hours"

    .line 284
    invoke-virtual {v0, v2, p3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p3

    .line 285
    invoke-virtual {p3}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p3

    .line 286
    invoke-virtual {p1, p2, p3, v1}, Lcom/squareup/ui/activity/BillHistoryDetailView;->showStatus(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)V

    goto/16 :goto_1

    .line 287
    :cond_0
    iget-object p3, p2, Lcom/squareup/billhistory/model/BillHistory;->errorTitle:Ljava/lang/String;

    invoke-static {p3}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p3

    if-eqz p3, :cond_6

    iget-object p3, p2, Lcom/squareup/billhistory/model/BillHistory;->errorMessage:Ljava/lang/String;

    invoke-static {p3}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p3

    if-nez p3, :cond_1

    goto :goto_0

    .line 292
    :cond_1
    iget-object p3, p2, Lcom/squareup/billhistory/model/BillHistory;->paymentState:Lcom/squareup/protos/client/bills/Tender$State;

    sget-object v0, Lcom/squareup/protos/client/bills/Tender$State;->LOST:Lcom/squareup/protos/client/bills/Tender$State;

    if-ne p3, v0, :cond_3

    .line 293
    invoke-virtual {p1}, Lcom/squareup/ui/activity/BillHistoryDetailView;->getContext()Landroid/content/Context;

    move-result-object p3

    .line 294
    sget v0, Lcom/squareup/billhistoryui/R$string;->receipt_detail_card_declined_message:I

    .line 295
    invoke-virtual {p2}, Lcom/squareup/billhistory/model/BillHistory;->hasNonLostTender()Z

    move-result p2

    if-eqz p2, :cond_2

    .line 296
    sget v0, Lcom/squareup/billhistoryui/R$string;->receipt_detail_card_declined_message_partial:I

    .line 298
    :cond_2
    iget-object p2, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->res:Lcom/squareup/util/Res;

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/billhistoryui/R$string;->receipt_detail_card_declined_message_learn_more:I

    .line 301
    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    .line 302
    invoke-static {p3, v2}, Lcom/squareup/marketfont/MarketSpanKt;->marketSpanFor(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;)Lcom/squareup/fonts/FontSpan;

    move-result-object p3

    .line 300
    invoke-static {v0, p3}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object p3

    const-string v0, "receipt_detail_card_declined_message_learn_more"

    .line 299
    invoke-virtual {p2, v0, p3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 303
    invoke-virtual {p2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p2

    .line 304
    iget-object p3, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/billhistoryui/R$string;->receipt_detail_card_declined_title:I

    invoke-interface {p3, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p3

    .line 305
    invoke-virtual {p1, p3, p2, v1}, Lcom/squareup/ui/activity/BillHistoryDetailView;->showStatus(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)V

    .line 306
    sget p2, Lcom/squareup/billhistoryui/R$string;->offline_mode_url:I

    invoke-virtual {p1, p2}, Lcom/squareup/ui/activity/BillHistoryDetailView;->linkStatusToSupportUrl(I)V

    goto :goto_1

    .line 307
    :cond_3
    iget-object p3, p2, Lcom/squareup/billhistory/model/BillHistory;->storeAndForward:Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    sget-object v0, Lcom/squareup/transactionhistory/pending/StoreAndForwardState;->FORWARDED:Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    const/4 v1, 0x0

    if-ne p3, v0, :cond_4

    .line 308
    sget p2, Lcom/squareup/billhistoryui/R$string;->receipt_detail_payment_forwarded_title:I

    sget p3, Lcom/squareup/billhistoryui/R$string;->receipt_detail_payment_forwarded_message:I

    invoke-virtual {p1, p2, p3, v1}, Lcom/squareup/ui/activity/BillHistoryDetailView;->showStatus(IIZ)V

    goto :goto_1

    .line 310
    :cond_4
    iget-boolean p2, p2, Lcom/squareup/billhistory/model/BillHistory;->pending:Z

    if-eqz p2, :cond_5

    .line 311
    sget p2, Lcom/squareup/billhistoryui/R$string;->receipt_detail_payment_pending_title:I

    sget p3, Lcom/squareup/billhistoryui/R$string;->receipt_detail_payment_pending_message:I

    invoke-virtual {p1, p2, p3, v1}, Lcom/squareup/ui/activity/BillHistoryDetailView;->showStatus(IIZ)V

    goto :goto_1

    .line 314
    :cond_5
    invoke-virtual {p1}, Lcom/squareup/ui/activity/BillHistoryDetailView;->hideStatus()V

    goto :goto_1

    .line 288
    :cond_6
    :goto_0
    iget-object p3, p2, Lcom/squareup/billhistory/model/BillHistory;->errorTitle:Ljava/lang/String;

    iget-object v0, p2, Lcom/squareup/billhistory/model/BillHistory;->errorMessage:Ljava/lang/String;

    invoke-virtual {p1, p3, v0, v1}, Lcom/squareup/ui/activity/BillHistoryDetailView;->showStatus(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)V

    .line 289
    invoke-virtual {p2}, Lcom/squareup/billhistory/model/BillHistory;->isStoreAndForward()Z

    move-result p2

    if-eqz p2, :cond_7

    .line 290
    sget p2, Lcom/squareup/billhistoryui/R$string;->offline_mode_url:I

    invoke-virtual {p1, p2}, Lcom/squareup/ui/activity/BillHistoryDetailView;->linkStatusToSupportUrl(I)V

    :cond_7
    :goto_1
    return-void
.end method

.method private updateTitle(Lcom/squareup/ui/activity/BillHistoryDetailView;Lcom/squareup/billhistory/model/BillHistory;)V
    .locals 3

    .line 254
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->res:Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-static {p2, v0, v1}, Lcom/squareup/billhistory/Bills;->formatTitleOf(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;)Ljava/lang/CharSequence;

    move-result-object p2

    .line 256
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 257
    iget-object v1, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {v1}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 258
    invoke-virtual {v0, p2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p2

    const/4 v1, 0x1

    .line 259
    invoke-virtual {p2, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p2

    iget-object v1, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->flow:Lflow/Flow;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/activity/-$$Lambda$sZX4UsMUUPBhpIptxMDlKtBJT0w;

    invoke-direct {v2, v1}, Lcom/squareup/ui/activity/-$$Lambda$sZX4UsMUUPBhpIptxMDlKtBJT0w;-><init>(Lflow/Flow;)V

    .line 260
    invoke-virtual {p2, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    goto :goto_0

    .line 261
    :cond_0
    invoke-static {p2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 263
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hideUpButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    .line 265
    invoke-virtual {v0, v1, p2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 266
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hideUpButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 268
    :goto_0
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/activity/BillHistoryDetailView;->setActionBarConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method private updateView(Lcom/squareup/ui/activity/BillHistoryDetailView;)V
    .locals 2

    .line 153
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->currentBill:Lcom/squareup/activity/CurrentBill;

    .line 154
    invoke-virtual {v0}, Lcom/squareup/activity/CurrentBill;->isSet()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    iget-object v1, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->currentBill:Lcom/squareup/activity/CurrentBill;

    invoke-virtual {v1}, Lcom/squareup/activity/CurrentBill;->getId()Lcom/squareup/billhistory/model/BillHistoryId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/activity/LegacyTransactionsHistory;->hasBillWithId(Lcom/squareup/billhistory/model/BillHistoryId;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 156
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {v1}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result v1

    if-eqz v1, :cond_2

    if-eqz v0, :cond_1

    .line 157
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    invoke-virtual {v0}, Lcom/squareup/activity/LegacyTransactionsHistory;->getBills()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 160
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->showBill(Lcom/squareup/ui/activity/BillHistoryDetailView;)V

    goto :goto_1

    .line 162
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->flow:Lflow/Flow;

    invoke-virtual {p1}, Lflow/Flow;->goBack()Z

    goto :goto_1

    :cond_2
    if-eqz v0, :cond_3

    .line 167
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->showBill(Lcom/squareup/ui/activity/BillHistoryDetailView;)V

    goto :goto_1

    .line 168
    :cond_3
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    invoke-virtual {v0}, Lcom/squareup/activity/LegacyTransactionsHistory;->getState()Lcom/squareup/activity/LoaderState;

    move-result-object v0

    sget-object v1, Lcom/squareup/activity/LoaderState;->LOADING:Lcom/squareup/activity/LoaderState;

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ENHANCED_TRANSACTION_SEARCH:Lcom/squareup/settings/server/Features$Feature;

    .line 169
    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 170
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->showLoading(Lcom/squareup/ui/activity/BillHistoryDetailView;)V

    goto :goto_1

    .line 171
    :cond_4
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    invoke-virtual {v0}, Lcom/squareup/activity/LegacyTransactionsHistory;->getState()Lcom/squareup/activity/LoaderState;

    move-result-object v0

    sget-object v1, Lcom/squareup/activity/LoaderState;->LOADED:Lcom/squareup/activity/LoaderState;

    if-ne v0, v1, :cond_5

    .line 172
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->handleLoadedState(Lcom/squareup/ui/activity/BillHistoryDetailView;)V

    goto :goto_1

    .line 173
    :cond_5
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    invoke-virtual {v0}, Lcom/squareup/activity/LegacyTransactionsHistory;->getState()Lcom/squareup/activity/LoaderState;

    move-result-object v0

    sget-object v1, Lcom/squareup/activity/LoaderState;->FAILED:Lcom/squareup/activity/LoaderState;

    if-ne v0, v1, :cond_6

    .line 174
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    invoke-virtual {v0}, Lcom/squareup/activity/LegacyTransactionsHistory;->getLastError()Lcom/squareup/activity/LoaderError;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->showError(Lcom/squareup/ui/activity/BillHistoryDetailView;Lcom/squareup/activity/LoaderError;)V

    :cond_6
    :goto_1
    return-void
.end method


# virtual methods
.method public dropView(Lcom/squareup/ui/activity/BillHistoryDetailView;)V
    .locals 1

    .line 138
    invoke-virtual {p0}, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {v0, p0}, Lcom/squareup/cardreader/CardReaderHub;->removeCardReaderAttachListener(Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;)V

    .line 141
    :cond_0
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->dropView(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic dropView(Ljava/lang/Object;)V
    .locals 0

    .line 53
    check-cast p1, Lcom/squareup/ui/activity/BillHistoryDetailView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->dropView(Lcom/squareup/ui/activity/BillHistoryDetailView;)V

    return-void
.end method

.method public synthetic lambda$null$0$LegacyBillHistoryDetailPresenter(Lcom/squareup/ui/activity/BillHistoryDetailView;Lkotlin/Unit;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 96
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->updateView(Lcom/squareup/ui/activity/BillHistoryDetailView;)V

    return-void
.end method

.method public synthetic lambda$null$10$LegacyBillHistoryDetailPresenter(Lcom/squareup/ui/activity/BillHistoryDetailView;Lkotlin/Unit;)V
    .locals 0

    .line 127
    invoke-virtual {p1}, Lcom/squareup/ui/activity/BillHistoryDetailView;->billHistoryView()Lcom/squareup/ui/activity/billhistory/BillHistoryView;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->temporarilyDisablePrintGiftReceiptButton()V

    .line 128
    iget-object p1, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->billHistoryRunner:Lcom/squareup/ui/activity/BillHistoryRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/activity/BillHistoryRunner;->startPrintGiftReceiptFlow()V

    return-void
.end method

.method public synthetic lambda$null$2$LegacyBillHistoryDetailPresenter(Lcom/squareup/ui/activity/BillHistoryDetailView;Lkotlin/Unit;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 100
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->updateView(Lcom/squareup/ui/activity/BillHistoryDetailView;)V

    return-void
.end method

.method public synthetic lambda$null$4$LegacyBillHistoryDetailPresenter(Lkotlin/Unit;)V
    .locals 2

    .line 105
    invoke-direct {p0}, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->getBill()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/billhistory/model/BillHistory;->isAwaitingMerchantTip()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 106
    iget-object p1, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/ui/activity/AwaitingTipRefundDialogScreen;

    invoke-direct {v0}, Lcom/squareup/ui/activity/AwaitingTipRefundDialogScreen;-><init>()V

    sget-object v1, Lflow/Direction;->FORWARD:Lflow/Direction;

    invoke-static {p1, v0, v1}, Lcom/squareup/container/Flows;->goToDialogScreen(Lflow/Flow;Lcom/squareup/container/ContainerTreeKey;Lflow/Direction;)V

    goto :goto_0

    .line 108
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->billHistoryRunner:Lcom/squareup/ui/activity/BillHistoryRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/activity/BillHistoryRunner;->continueToRefund()V

    :goto_0
    return-void
.end method

.method public synthetic lambda$null$6$LegacyBillHistoryDetailPresenter(Lkotlin/Unit;)V
    .locals 0

    .line 114
    iget-object p1, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->billHistoryRunner:Lcom/squareup/ui/activity/BillHistoryRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/activity/BillHistoryRunner;->showFirstIssueReceiptScreen()V

    return-void
.end method

.method public synthetic lambda$null$8$LegacyBillHistoryDetailPresenter(Lcom/squareup/ui/activity/BillHistoryDetailView;Lkotlin/Unit;)V
    .locals 0

    .line 119
    invoke-virtual {p1}, Lcom/squareup/ui/activity/BillHistoryDetailView;->billHistoryView()Lcom/squareup/ui/activity/billhistory/BillHistoryView;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->temporarilyDisableReprintTicketButton()V

    .line 120
    iget-object p1, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->billHistoryRunner:Lcom/squareup/ui/activity/BillHistoryRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/activity/BillHistoryRunner;->reprintTicket()V

    return-void
.end method

.method public synthetic lambda$onLoad$1$LegacyBillHistoryDetailPresenter(Lcom/squareup/ui/activity/BillHistoryDetailView;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->currentBill:Lcom/squareup/activity/CurrentBill;

    invoke-virtual {v0}, Lcom/squareup/activity/CurrentBill;->onChanged()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/activity/-$$Lambda$LegacyBillHistoryDetailPresenter$Iz8K8NRuXjNajmZQDEfAapFEKvw;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/activity/-$$Lambda$LegacyBillHistoryDetailPresenter$Iz8K8NRuXjNajmZQDEfAapFEKvw;-><init>(Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;Lcom/squareup/ui/activity/BillHistoryDetailView;)V

    .line 96
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$11$LegacyBillHistoryDetailPresenter(Lcom/squareup/ui/activity/BillHistoryDetailView;)Lrx/Subscription;
    .locals 2

    .line 125
    invoke-virtual {p1}, Lcom/squareup/ui/activity/BillHistoryDetailView;->billHistoryView()Lcom/squareup/ui/activity/billhistory/BillHistoryView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->onPrintGiftReceiptButtonClicked()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/activity/-$$Lambda$LegacyBillHistoryDetailPresenter$lQMnCjOqm7uQ5DL4ympM9BF4IsA;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/activity/-$$Lambda$LegacyBillHistoryDetailPresenter$lQMnCjOqm7uQ5DL4ympM9BF4IsA;-><init>(Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;Lcom/squareup/ui/activity/BillHistoryDetailView;)V

    .line 126
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$3$LegacyBillHistoryDetailPresenter(Lcom/squareup/ui/activity/BillHistoryDetailView;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 99
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->legacyTransactionsHistory:Lcom/squareup/activity/LegacyTransactionsHistory;

    invoke-virtual {v0}, Lcom/squareup/activity/LegacyTransactionsHistory;->onChanged()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/activity/-$$Lambda$LegacyBillHistoryDetailPresenter$u_VbyycUtDa7RZDHcsbWs08qVG0;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/activity/-$$Lambda$LegacyBillHistoryDetailPresenter$u_VbyycUtDa7RZDHcsbWs08qVG0;-><init>(Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;Lcom/squareup/ui/activity/BillHistoryDetailView;)V

    .line 100
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$5$LegacyBillHistoryDetailPresenter(Lcom/squareup/ui/activity/BillHistoryDetailView;)Lrx/Subscription;
    .locals 1

    .line 103
    invoke-virtual {p1}, Lcom/squareup/ui/activity/BillHistoryDetailView;->billHistoryView()Lcom/squareup/ui/activity/billhistory/BillHistoryView;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->onRefundButtonClicked()Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/activity/-$$Lambda$LegacyBillHistoryDetailPresenter$t-Aob-rgef7CiTBG9tI_8vA4NaM;

    invoke-direct {v0, p0}, Lcom/squareup/ui/activity/-$$Lambda$LegacyBillHistoryDetailPresenter$t-Aob-rgef7CiTBG9tI_8vA4NaM;-><init>(Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;)V

    .line 104
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$7$LegacyBillHistoryDetailPresenter(Lcom/squareup/ui/activity/BillHistoryDetailView;)Lrx/Subscription;
    .locals 1

    .line 113
    invoke-virtual {p1}, Lcom/squareup/ui/activity/BillHistoryDetailView;->billHistoryView()Lcom/squareup/ui/activity/billhistory/BillHistoryView;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->onReceiptButtonClicked()Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/activity/-$$Lambda$LegacyBillHistoryDetailPresenter$1RUsb_04h958jwRKWkA0IQDc5JU;

    invoke-direct {v0, p0}, Lcom/squareup/ui/activity/-$$Lambda$LegacyBillHistoryDetailPresenter$1RUsb_04h958jwRKWkA0IQDc5JU;-><init>(Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;)V

    .line 114
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$9$LegacyBillHistoryDetailPresenter(Lcom/squareup/ui/activity/BillHistoryDetailView;)Lrx/Subscription;
    .locals 2

    .line 117
    invoke-virtual {p1}, Lcom/squareup/ui/activity/BillHistoryDetailView;->billHistoryView()Lcom/squareup/ui/activity/billhistory/BillHistoryView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->onReprintTicketButtonClicked()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/activity/-$$Lambda$LegacyBillHistoryDetailPresenter$nagZM1Z5KJB_lNazxA_rpplAags;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/activity/-$$Lambda$LegacyBillHistoryDetailPresenter$nagZM1Z5KJB_lNazxA_rpplAags;-><init>(Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;Lcom/squareup/ui/activity/BillHistoryDetailView;)V

    .line 118
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public onCardReaderAdded(Lcom/squareup/cardreader/CardReader;)V
    .locals 2

    .line 326
    invoke-virtual {p0}, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/activity/BillHistoryDetailView;

    .line 327
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ENHANCED_TRANSACTION_SEARCH:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 328
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->updateView(Lcom/squareup/ui/activity/BillHistoryDetailView;)V

    :cond_0
    return-void
.end method

.method public onCardReaderRemoved(Lcom/squareup/cardreader/CardReader;)V
    .locals 2

    .line 333
    invoke-virtual {p0}, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/activity/BillHistoryDetailView;

    .line 334
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ENHANCED_TRANSACTION_SEARCH:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 335
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->updateView(Lcom/squareup/ui/activity/BillHistoryDetailView;)V

    :cond_0
    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 91
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 92
    invoke-virtual {p0}, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/activity/BillHistoryDetailView;

    .line 94
    new-instance v0, Lcom/squareup/ui/activity/-$$Lambda$LegacyBillHistoryDetailPresenter$U9pPPjNr-4JHYJb-T8FfT1_A9XI;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/activity/-$$Lambda$LegacyBillHistoryDetailPresenter$U9pPPjNr-4JHYJb-T8FfT1_A9XI;-><init>(Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;Lcom/squareup/ui/activity/BillHistoryDetailView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 98
    new-instance v0, Lcom/squareup/ui/activity/-$$Lambda$LegacyBillHistoryDetailPresenter$-FHbAxEKL6rtkuWTOojEhTDGtdY;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/activity/-$$Lambda$LegacyBillHistoryDetailPresenter$-FHbAxEKL6rtkuWTOojEhTDGtdY;-><init>(Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;Lcom/squareup/ui/activity/BillHistoryDetailView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 102
    new-instance v0, Lcom/squareup/ui/activity/-$$Lambda$LegacyBillHistoryDetailPresenter$PTSunMg6J5Kca4C5tOnvtw7dqoU;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/activity/-$$Lambda$LegacyBillHistoryDetailPresenter$PTSunMg6J5Kca4C5tOnvtw7dqoU;-><init>(Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;Lcom/squareup/ui/activity/BillHistoryDetailView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 112
    new-instance v0, Lcom/squareup/ui/activity/-$$Lambda$LegacyBillHistoryDetailPresenter$OF6uvK2amwPozfDD89pMySTxvJU;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/activity/-$$Lambda$LegacyBillHistoryDetailPresenter$OF6uvK2amwPozfDD89pMySTxvJU;-><init>(Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;Lcom/squareup/ui/activity/BillHistoryDetailView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 116
    new-instance v0, Lcom/squareup/ui/activity/-$$Lambda$LegacyBillHistoryDetailPresenter$S2JDaFK_MiR4SMMWQLCiyTzvP1Q;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/activity/-$$Lambda$LegacyBillHistoryDetailPresenter$S2JDaFK_MiR4SMMWQLCiyTzvP1Q;-><init>(Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;Lcom/squareup/ui/activity/BillHistoryDetailView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 124
    new-instance v0, Lcom/squareup/ui/activity/-$$Lambda$LegacyBillHistoryDetailPresenter$Dh6aG17rcMEPpsB2ixiyE6xQBtU;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/activity/-$$Lambda$LegacyBillHistoryDetailPresenter$Dh6aG17rcMEPpsB2ixiyE6xQBtU;-><init>(Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;Lcom/squareup/ui/activity/BillHistoryDetailView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    const/4 v0, 0x0

    .line 131
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->updateTitle(Lcom/squareup/ui/activity/BillHistoryDetailView;Lcom/squareup/billhistory/model/BillHistory;)V

    .line 132
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->updateView(Lcom/squareup/ui/activity/BillHistoryDetailView;)V

    .line 134
    iget-object p1, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {p1, p0}, Lcom/squareup/cardreader/CardReaderHub;->addCardReaderAttachListener(Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;)V

    return-void
.end method

.method onSupportCenterClicked(Landroid/content/Context;)V
    .locals 1

    .line 145
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getSupportSettings()Lcom/squareup/settings/server/SupportUrlSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/SupportUrlSettings;->getHelpCenterUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/util/RegisterIntents;->launchBrowser(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method
