.class final Lcom/squareup/ui/activity/ReaderCancelingResultSingleEmitter;
.super Ljava/lang/Object;
.source "RealCardPresentRefund.kt"

# interfaces
.implements Lcom/squareup/ui/activity/CardPresentRefundEventHandler;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0003\n\u0002\u0008\u0003\u0008\u0002\u0018\u00002\u00020\u0001B\u001b\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0002\u0010\u0007J\u0008\u0010\n\u001a\u00020\u000bH\u0002J\u0010\u0010\u000c\u001a\u00020\u000b2\u0006\u0010\r\u001a\u00020\u000eH\u0016J\u0010\u0010\u000f\u001a\u00020\u000b2\u0006\u0010\u0010\u001a\u00020\u0006H\u0016R\u0014\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/ui/activity/ReaderCancelingResultSingleEmitter;",
        "Lcom/squareup/ui/activity/CardPresentRefundEventHandler;",
        "nfcProcessor",
        "Lcom/squareup/ui/NfcProcessor;",
        "emitter",
        "Lio/reactivex/SingleEmitter;",
        "Lcom/squareup/ui/activity/ReaderResult;",
        "(Lcom/squareup/ui/NfcProcessor;Lio/reactivex/SingleEmitter;)V",
        "stopped",
        "Ljava/util/concurrent/atomic/AtomicBoolean;",
        "finallyFinishWithReaders",
        "",
        "onReaderError",
        "throwable",
        "",
        "onReaderResult",
        "refundResult",
        "android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final emitter:Lio/reactivex/SingleEmitter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/SingleEmitter<",
            "Lcom/squareup/ui/activity/ReaderResult;",
            ">;"
        }
    .end annotation
.end field

.field private final nfcProcessor:Lcom/squareup/ui/NfcProcessor;

.field private final stopped:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/NfcProcessor;Lio/reactivex/SingleEmitter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/NfcProcessor;",
            "Lio/reactivex/SingleEmitter<",
            "Lcom/squareup/ui/activity/ReaderResult;",
            ">;)V"
        }
    .end annotation

    const-string v0, "nfcProcessor"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "emitter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/activity/ReaderCancelingResultSingleEmitter;->nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    iput-object p2, p0, Lcom/squareup/ui/activity/ReaderCancelingResultSingleEmitter;->emitter:Lio/reactivex/SingleEmitter;

    .line 60
    new-instance p1, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 p2, 0x0

    invoke-direct {p1, p2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object p1, p0, Lcom/squareup/ui/activity/ReaderCancelingResultSingleEmitter;->stopped:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 64
    iget-object p1, p0, Lcom/squareup/ui/activity/ReaderCancelingResultSingleEmitter;->emitter:Lio/reactivex/SingleEmitter;

    new-instance p2, Lcom/squareup/ui/activity/ReaderCancelingResultSingleEmitter$1;

    move-object v0, p0

    check-cast v0, Lcom/squareup/ui/activity/ReaderCancelingResultSingleEmitter;

    invoke-direct {p2, v0}, Lcom/squareup/ui/activity/ReaderCancelingResultSingleEmitter$1;-><init>(Lcom/squareup/ui/activity/ReaderCancelingResultSingleEmitter;)V

    check-cast p2, Lkotlin/jvm/functions/Function0;

    new-instance v0, Lcom/squareup/ui/activity/RealCardPresentRefundKt$sam$io_reactivex_functions_Cancellable$0;

    invoke-direct {v0, p2}, Lcom/squareup/ui/activity/RealCardPresentRefundKt$sam$io_reactivex_functions_Cancellable$0;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v0, Lio/reactivex/functions/Cancellable;

    invoke-interface {p1, v0}, Lio/reactivex/SingleEmitter;->setCancellable(Lio/reactivex/functions/Cancellable;)V

    return-void
.end method

.method public static final synthetic access$finallyFinishWithReaders(Lcom/squareup/ui/activity/ReaderCancelingResultSingleEmitter;)V
    .locals 0

    .line 55
    invoke-direct {p0}, Lcom/squareup/ui/activity/ReaderCancelingResultSingleEmitter;->finallyFinishWithReaders()V

    return-void
.end method

.method private final finallyFinishWithReaders()V
    .locals 2

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/activity/ReaderCancelingResultSingleEmitter;->stopped:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/activity/ReaderCancelingResultSingleEmitter;->nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    invoke-virtual {v0}, Lcom/squareup/ui/NfcProcessor;->cancelPaymentOnAllContactlessReaders()V

    :cond_0
    return-void
.end method


# virtual methods
.method public onReaderError(Ljava/lang/Throwable;)V
    .locals 1

    const-string/jumbo v0, "throwable"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    invoke-direct {p0}, Lcom/squareup/ui/activity/ReaderCancelingResultSingleEmitter;->finallyFinishWithReaders()V

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/activity/ReaderCancelingResultSingleEmitter;->emitter:Lio/reactivex/SingleEmitter;

    invoke-interface {v0, p1}, Lio/reactivex/SingleEmitter;->onError(Ljava/lang/Throwable;)V

    return-void
.end method

.method public onReaderResult(Lcom/squareup/ui/activity/ReaderResult;)V
    .locals 1

    const-string v0, "refundResult"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    invoke-direct {p0}, Lcom/squareup/ui/activity/ReaderCancelingResultSingleEmitter;->finallyFinishWithReaders()V

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/activity/ReaderCancelingResultSingleEmitter;->emitter:Lio/reactivex/SingleEmitter;

    invoke-interface {v0, p1}, Lio/reactivex/SingleEmitter;->onSuccess(Ljava/lang/Object;)V

    return-void
.end method
