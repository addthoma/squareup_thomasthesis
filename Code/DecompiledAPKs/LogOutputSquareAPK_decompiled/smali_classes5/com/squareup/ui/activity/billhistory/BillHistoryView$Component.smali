.class public interface abstract Lcom/squareup/ui/activity/billhistory/BillHistoryView$Component;
.super Ljava/lang/Object;
.source "BillHistoryView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/activity/billhistory/BillHistoryView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract inject(Lcom/squareup/ui/activity/billhistory/BillHistoryItemsSection;)V
.end method

.method public abstract inject(Lcom/squareup/ui/activity/billhistory/BillHistoryTenderSection;)V
.end method

.method public abstract inject(Lcom/squareup/ui/activity/billhistory/BillHistoryView;)V
.end method

.method public abstract inject(Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;)V
.end method
