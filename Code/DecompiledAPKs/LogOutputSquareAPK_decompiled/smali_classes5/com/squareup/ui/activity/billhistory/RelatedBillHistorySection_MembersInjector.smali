.class public final Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection_MembersInjector;
.super Ljava/lang/Object;
.source "RelatedBillHistorySection_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;",
        ">;"
    }
.end annotation


# instance fields
.field private final billHistoryRowFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final employeesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/Employees;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final resourcesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/Employees;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection_MembersInjector;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p2, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection_MembersInjector;->resourcesProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p3, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection_MembersInjector;->employeesProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p4, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection_MembersInjector;->localeProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p5, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection_MembersInjector;->billHistoryRowFactoryProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p6, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection_MembersInjector;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/Employees;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;",
            ">;"
        }
    .end annotation

    .line 53
    new-instance v7, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection_MembersInjector;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static injectBillHistoryRowFactory(Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;Ljava/lang/Object;)V
    .locals 0

    .line 90
    check-cast p1, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;

    iput-object p1, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->billHistoryRowFactory:Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;

    return-void
.end method

.method public static injectEmployees(Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;Lcom/squareup/permissions/Employees;)V
    .locals 0

    .line 78
    iput-object p1, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->employees:Lcom/squareup/permissions/Employees;

    return-void
.end method

.method public static injectFeatures(Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;Lcom/squareup/settings/server/Features;)V
    .locals 0

    .line 95
    iput-object p1, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method public static injectLocaleProvider(Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)V"
        }
    .end annotation

    .line 84
    iput-object p1, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->localeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static injectMoneyFormatter(Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;Lcom/squareup/text/Formatter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    .line 68
    iput-object p1, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->moneyFormatter:Lcom/squareup/text/Formatter;

    return-void
.end method

.method public static injectResources(Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;Lcom/squareup/util/Res;)V
    .locals 0

    .line 73
    iput-object p1, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->resources:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;)V
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection_MembersInjector;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/text/Formatter;

    invoke-static {p1, v0}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection_MembersInjector;->injectMoneyFormatter(Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;Lcom/squareup/text/Formatter;)V

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection_MembersInjector;->resourcesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    invoke-static {p1, v0}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection_MembersInjector;->injectResources(Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;Lcom/squareup/util/Res;)V

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection_MembersInjector;->employeesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/permissions/Employees;

    invoke-static {p1, v0}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection_MembersInjector;->injectEmployees(Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;Lcom/squareup/permissions/Employees;)V

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection_MembersInjector;->localeProvider:Ljavax/inject/Provider;

    invoke-static {p1, v0}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection_MembersInjector;->injectLocaleProvider(Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;Ljavax/inject/Provider;)V

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection_MembersInjector;->billHistoryRowFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection_MembersInjector;->injectBillHistoryRowFactory(Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;Ljava/lang/Object;)V

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection_MembersInjector;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/Features;

    invoke-static {p1, v0}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection_MembersInjector;->injectFeatures(Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;Lcom/squareup/settings/server/Features;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 14
    check-cast p1, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection_MembersInjector;->injectMembers(Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;)V

    return-void
.end method
