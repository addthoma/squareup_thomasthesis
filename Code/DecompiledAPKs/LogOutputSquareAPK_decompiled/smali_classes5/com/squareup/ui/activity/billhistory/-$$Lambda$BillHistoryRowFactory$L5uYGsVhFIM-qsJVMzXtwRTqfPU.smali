.class public final synthetic Lcom/squareup/ui/activity/billhistory/-$$Lambda$BillHistoryRowFactory$L5uYGsVhFIM-qsJVMzXtwRTqfPU;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# instance fields
.field private final synthetic f$0:Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;

.field private final synthetic f$1:Lcom/squareup/checkout/CartItem;

.field private final synthetic f$2:Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;Lcom/squareup/checkout/CartItem;Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/activity/billhistory/-$$Lambda$BillHistoryRowFactory$L5uYGsVhFIM-qsJVMzXtwRTqfPU;->f$0:Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;

    iput-object p2, p0, Lcom/squareup/ui/activity/billhistory/-$$Lambda$BillHistoryRowFactory$L5uYGsVhFIM-qsJVMzXtwRTqfPU;->f$1:Lcom/squareup/checkout/CartItem;

    iput-object p3, p0, Lcom/squareup/ui/activity/billhistory/-$$Lambda$BillHistoryRowFactory$L5uYGsVhFIM-qsJVMzXtwRTqfPU;->f$2:Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;

    return-void
.end method


# virtual methods
.method public final invoke()Ljava/lang/Object;
    .locals 3

    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/-$$Lambda$BillHistoryRowFactory$L5uYGsVhFIM-qsJVMzXtwRTqfPU;->f$0:Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;

    iget-object v1, p0, Lcom/squareup/ui/activity/billhistory/-$$Lambda$BillHistoryRowFactory$L5uYGsVhFIM-qsJVMzXtwRTqfPU;->f$1:Lcom/squareup/checkout/CartItem;

    iget-object v2, p0, Lcom/squareup/ui/activity/billhistory/-$$Lambda$BillHistoryRowFactory$L5uYGsVhFIM-qsJVMzXtwRTqfPU;->f$2:Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->lambda$createItemizationRows$0$BillHistoryRowFactory(Lcom/squareup/checkout/CartItem;Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method
