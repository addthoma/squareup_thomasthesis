.class public Lcom/squareup/ui/activity/RefundPinDialog$RefundPinDialogModule;
.super Ljava/lang/Object;
.source "RefundPinDialog.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/activity/RefundPinDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RefundPinDialogModule"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method pinListener(Lcom/squareup/ui/activity/IssueRefundScopeRunner;)Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 47
    invoke-virtual {p1}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->getPinListener()Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;

    move-result-object p1

    return-object p1
.end method

.method pinPresenter(Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;Lcom/squareup/util/Res;Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;)Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 37
    new-instance v0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;

    invoke-static {p2}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object p2

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;-><init>(Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;Lio/reactivex/Observable;Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;)V

    return-object v0
.end method

.method starGroupPresenter()Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 42
    new-instance v0, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;

    invoke-direct {v0}, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;-><init>()V

    return-object v0
.end method
