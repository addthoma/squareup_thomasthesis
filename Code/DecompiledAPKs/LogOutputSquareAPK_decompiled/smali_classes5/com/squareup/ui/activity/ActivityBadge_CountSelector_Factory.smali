.class public final Lcom/squareup/ui/activity/ActivityBadge_CountSelector_Factory;
.super Ljava/lang/Object;
.source "ActivityBadge_CountSelector_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/activity/ActivityBadge$CountSelector;",
        ">;"
    }
.end annotation


# instance fields
.field private final connectivityMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final pendingTransactionsStoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/pending/PendingTransactionsStore;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/pending/PendingTransactionsStore;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/ui/activity/ActivityBadge_CountSelector_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/squareup/ui/activity/ActivityBadge_CountSelector_Factory;->pendingTransactionsStoreProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/activity/ActivityBadge_CountSelector_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/pending/PendingTransactionsStore;",
            ">;)",
            "Lcom/squareup/ui/activity/ActivityBadge_CountSelector_Factory;"
        }
    .end annotation

    .line 37
    new-instance v0, Lcom/squareup/ui/activity/ActivityBadge_CountSelector_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/activity/ActivityBadge_CountSelector_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/payment/pending/PendingTransactionsStore;)Lcom/squareup/ui/activity/ActivityBadge$CountSelector;
    .locals 1

    .line 42
    new-instance v0, Lcom/squareup/ui/activity/ActivityBadge$CountSelector;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/activity/ActivityBadge$CountSelector;-><init>(Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/payment/pending/PendingTransactionsStore;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/activity/ActivityBadge$CountSelector;
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/squareup/ui/activity/ActivityBadge_CountSelector_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/connectivity/ConnectivityMonitor;

    iget-object v1, p0, Lcom/squareup/ui/activity/ActivityBadge_CountSelector_Factory;->pendingTransactionsStoreProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/payment/pending/PendingTransactionsStore;

    invoke-static {v0, v1}, Lcom/squareup/ui/activity/ActivityBadge_CountSelector_Factory;->newInstance(Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/payment/pending/PendingTransactionsStore;)Lcom/squareup/ui/activity/ActivityBadge$CountSelector;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/ui/activity/ActivityBadge_CountSelector_Factory;->get()Lcom/squareup/ui/activity/ActivityBadge$CountSelector;

    move-result-object v0

    return-object v0
.end method
