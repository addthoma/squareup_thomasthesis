.class Lcom/squareup/ui/activity/SelectRefundTenderPresenter;
.super Lcom/squareup/activity/AbstractActivityCardPresenter;
.source "SelectRefundTenderPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/activity/AbstractActivityCardPresenter<",
        "Lcom/squareup/ui/activity/SelectRefundTenderView;",
        ">;"
    }
.end annotation


# instance fields
.field private final clock:Lcom/squareup/util/Clock;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method constructor <init>(Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;Lcom/squareup/util/Clock;Lcom/squareup/util/Res;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 24
    invoke-direct {p0, p1}, Lcom/squareup/activity/AbstractActivityCardPresenter;-><init>(Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;)V

    .line 25
    iput-object p2, p0, Lcom/squareup/ui/activity/SelectRefundTenderPresenter;->clock:Lcom/squareup/util/Clock;

    .line 26
    iput-object p3, p0, Lcom/squareup/ui/activity/SelectRefundTenderPresenter;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method private updateView(Lcom/squareup/ui/activity/SelectRefundTenderView;)V
    .locals 9

    .line 48
    invoke-virtual {p1}, Lcom/squareup/ui/activity/SelectRefundTenderView;->clearTenders()V

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/activity/SelectRefundTenderPresenter;->runner:Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;

    invoke-interface {v0}, Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;->getBill()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v0

    .line 50
    invoke-virtual {v0}, Lcom/squareup/billhistory/model/BillHistory;->getTenders()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/billhistory/model/TenderHistory;

    .line 51
    invoke-virtual {v0, v2}, Lcom/squareup/billhistory/model/BillHistory;->refundableAmount(Lcom/squareup/billhistory/model/TenderHistory;)Lcom/squareup/protos/common/Money;

    move-result-object v3

    .line 52
    iget-object v4, v3, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v8, v4, v6

    if-lez v8, :cond_0

    iget-object v4, p0, Lcom/squareup/ui/activity/SelectRefundTenderPresenter;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v4}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/squareup/billhistory/model/TenderHistory;->isPastRefundDate(J)Z

    move-result v4

    if-nez v4, :cond_0

    .line 53
    iget-object v4, v2, Lcom/squareup/billhistory/model/TenderHistory;->id:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/squareup/billhistory/model/TenderHistory;->getBrandedTenderGlyphGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v5

    iget-object v6, p0, Lcom/squareup/ui/activity/SelectRefundTenderPresenter;->res:Lcom/squareup/util/Res;

    .line 54
    invoke-virtual {v2, v6}, Lcom/squareup/billhistory/model/TenderHistory;->getDescription(Lcom/squareup/util/Res;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 53
    invoke-virtual {p1, v4, v5, v2, v3}, Lcom/squareup/ui/activity/SelectRefundTenderView;->addTender(Ljava/lang/String;Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Lcom/squareup/protos/common/Money;)V

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method protected getActionBarText()Ljava/lang/String;
    .locals 2

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/activity/SelectRefundTenderPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/activity/R$string;->refund_issue:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$null$0$SelectRefundTenderPresenter(Lcom/squareup/ui/activity/SelectRefundTenderView;Lkotlin/Unit;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 38
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/SelectRefundTenderPresenter;->updateView(Lcom/squareup/ui/activity/SelectRefundTenderView;)V

    return-void
.end method

.method public synthetic lambda$onLoad$1$SelectRefundTenderPresenter(Lcom/squareup/ui/activity/SelectRefundTenderView;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 37
    iget-object v0, p0, Lcom/squareup/ui/activity/SelectRefundTenderPresenter;->runner:Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;

    invoke-interface {v0}, Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;->onTransactionsHistoryChanged()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/activity/-$$Lambda$SelectRefundTenderPresenter$Ub_kZVF6qAe--hhG2a9x83ppzUE;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/activity/-$$Lambda$SelectRefundTenderPresenter$Ub_kZVF6qAe--hhG2a9x83ppzUE;-><init>(Lcom/squareup/ui/activity/SelectRefundTenderPresenter;Lcom/squareup/ui/activity/SelectRefundTenderView;)V

    .line 38
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 30
    invoke-super {p0, p1}, Lcom/squareup/activity/AbstractActivityCardPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 31
    invoke-virtual {p0}, Lcom/squareup/ui/activity/SelectRefundTenderPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/activity/SelectRefundTenderView;

    .line 33
    invoke-virtual {p0}, Lcom/squareup/ui/activity/SelectRefundTenderPresenter;->createDefaultActionBarConfigBuilder()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 34
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/activity/SelectRefundTenderView;->setActionBarConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 36
    new-instance v0, Lcom/squareup/ui/activity/-$$Lambda$SelectRefundTenderPresenter$6e-3S6yCRsFdy30mFifkJHWMjuI;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/activity/-$$Lambda$SelectRefundTenderPresenter$6e-3S6yCRsFdy30mFifkJHWMjuI;-><init>(Lcom/squareup/ui/activity/SelectRefundTenderPresenter;Lcom/squareup/ui/activity/SelectRefundTenderView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 40
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/SelectRefundTenderPresenter;->updateView(Lcom/squareup/ui/activity/SelectRefundTenderView;)V

    return-void
.end method

.method tenderSelected(Ljava/lang/String;)V
    .locals 1

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/activity/SelectRefundTenderPresenter;->runner:Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;

    invoke-interface {v0, p1}, Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;->showStartRefundScreen(Ljava/lang/String;)V

    return-void
.end method
