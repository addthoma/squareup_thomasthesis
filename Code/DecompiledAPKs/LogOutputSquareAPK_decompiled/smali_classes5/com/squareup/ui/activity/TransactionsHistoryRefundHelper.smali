.class public abstract Lcom/squareup/ui/activity/TransactionsHistoryRefundHelper;
.super Ljava/lang/Object;
.source "TransactionsHistoryRefundHelper.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008&\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H\u0016J\u0008\u0010\u0005\u001a\u00020\u0004H\u0016J\u0018\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\u00072\u0006\u0010\t\u001a\u00020\nH&\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/ui/activity/TransactionsHistoryRefundHelper;",
        "",
        "()V",
        "canceledRefundFlow",
        "",
        "finishedRefundFlow",
        "ingestRefundsResponse",
        "Lcom/squareup/billhistory/model/BillHistory;",
        "billHistory",
        "issueRefundsResponse",
        "Lcom/squareup/protos/client/bills/IssueRefundsResponse;",
        "activity_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public canceledRefundFlow()V
    .locals 0

    return-void
.end method

.method public finishedRefundFlow()V
    .locals 0

    return-void
.end method

.method public abstract ingestRefundsResponse(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/protos/client/bills/IssueRefundsResponse;)Lcom/squareup/billhistory/model/BillHistory;
.end method
