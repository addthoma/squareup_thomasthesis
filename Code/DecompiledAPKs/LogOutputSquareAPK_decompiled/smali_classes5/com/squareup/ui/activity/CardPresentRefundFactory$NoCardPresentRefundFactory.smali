.class public final Lcom/squareup/ui/activity/CardPresentRefundFactory$NoCardPresentRefundFactory;
.super Ljava/lang/Object;
.source "CardPresentRefund.kt"

# interfaces
.implements Lcom/squareup/ui/activity/CardPresentRefundFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/activity/CardPresentRefundFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NoCardPresentRefundFactory"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCardPresentRefund.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CardPresentRefund.kt\ncom/squareup/ui/activity/CardPresentRefundFactory$NoCardPresentRefundFactory\n+ 2 Objects.kt\ncom/squareup/util/Objects\n*L\n1#1,68:1\n151#2,2:69\n*E\n*S KotlinDebug\n*F\n+ 1 CardPresentRefund.kt\ncom/squareup/ui/activity/CardPresentRefundFactory$NoCardPresentRefundFactory\n*L\n66#1,2:69\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u0011\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0096\u0001\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/ui/activity/CardPresentRefundFactory$NoCardPresentRefundFactory;",
        "Lcom/squareup/ui/activity/CardPresentRefundFactory;",
        "()V",
        "cardPresentRefund",
        "Lcom/squareup/ui/activity/CardPresentRefund;",
        "scope",
        "Lmortar/MortarScope;",
        "activity_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final synthetic $$delegate_0:Lcom/squareup/ui/activity/CardPresentRefundFactory;


# direct methods
.method public constructor <init>()V
    .locals 5
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 69
    move-object v1, v0

    check-cast v1, Ljava/lang/String;

    .line 70
    const-class v2, Lcom/squareup/ui/activity/CardPresentRefundFactory;

    const/4 v3, 0x0

    const/4 v4, 0x4

    invoke-static {v2, v1, v3, v4, v0}, Lcom/squareup/util/Objects;->allMethodsThrowUnsupportedOperation$default(Ljava/lang/Class;Ljava/lang/String;ZILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/CardPresentRefundFactory;

    iput-object v0, p0, Lcom/squareup/ui/activity/CardPresentRefundFactory$NoCardPresentRefundFactory;->$$delegate_0:Lcom/squareup/ui/activity/CardPresentRefundFactory;

    return-void
.end method


# virtual methods
.method public cardPresentRefund(Lmortar/MortarScope;)Lcom/squareup/ui/activity/CardPresentRefund;
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/ui/activity/CardPresentRefundFactory$NoCardPresentRefundFactory;->$$delegate_0:Lcom/squareup/ui/activity/CardPresentRefundFactory;

    invoke-interface {v0, p1}, Lcom/squareup/ui/activity/CardPresentRefundFactory;->cardPresentRefund(Lmortar/MortarScope;)Lcom/squareup/ui/activity/CardPresentRefund;

    move-result-object p1

    return-object p1
.end method
