.class public Lcom/squareup/ui/activity/TipOptionsView;
.super Landroid/widget/LinearLayout;
.source "TipOptionsView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/activity/TipOptionsView$OnTipOptionSelectedListener;
    }
.end annotation


# instance fields
.field private final buttonHorizontalPadding:I

.field private final buttonMinWidth:I

.field private gravity:I

.field private final onClickListener:Landroid/view/View$OnClickListener;

.field private onTipOptionSelectedListener:Lcom/squareup/ui/activity/TipOptionsView$OnTipOptionSelectedListener;

.field private textColors:Landroid/content/res/ColorStateList;

.field private tipOptionAmounts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private tipOptionCustom:Lcom/squareup/marketfont/MarketTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .line 61
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 62
    sget p2, Lcom/squareup/billhistoryui/R$layout;->activity_applet_tip_options_view:I

    invoke-static {p1, p2, p0}, Lcom/squareup/ui/activity/TipOptionsView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 63
    invoke-direct {p0}, Lcom/squareup/ui/activity/TipOptionsView;->bindViews()V

    .line 65
    new-instance p1, Lcom/squareup/ui/activity/TipOptionsView$1;

    invoke-direct {p1, p0}, Lcom/squareup/ui/activity/TipOptionsView$1;-><init>(Lcom/squareup/ui/activity/TipOptionsView;)V

    iput-object p1, p0, Lcom/squareup/ui/activity/TipOptionsView;->onClickListener:Landroid/view/View$OnClickListener;

    .line 75
    iget-object p1, p0, Lcom/squareup/ui/activity/TipOptionsView;->tipOptionCustom:Lcom/squareup/marketfont/MarketTextView;

    iget-object p2, p0, Lcom/squareup/ui/activity/TipOptionsView;->onClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, p2}, Lcom/squareup/marketfont/MarketTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 77
    invoke-virtual {p0}, Lcom/squareup/ui/activity/TipOptionsView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/squareup/marin/R$dimen;->marin_gap_mini:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/activity/TipOptionsView;->buttonHorizontalPadding:I

    .line 79
    invoke-virtual {p0}, Lcom/squareup/ui/activity/TipOptionsView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/squareup/marin/R$dimen;->marin_min_width:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/activity/TipOptionsView;->buttonMinWidth:I

    const/4 p1, 0x0

    .line 81
    iput p1, p0, Lcom/squareup/ui/activity/TipOptionsView;->gravity:I

    const/4 p2, 0x5

    .line 82
    invoke-direct {p0, p2}, Lcom/squareup/ui/activity/TipOptionsView;->getPaddingForGravity(I)I

    move-result p2

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/activity/TipOptionsView;->tipOptionCustom:Lcom/squareup/marketfont/MarketTextView;

    iget v1, p0, Lcom/squareup/ui/activity/TipOptionsView;->buttonHorizontalPadding:I

    invoke-virtual {v0, v1, p1, p2, p1}, Lcom/squareup/marketfont/MarketTextView;->setPadding(IIII)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/activity/TipOptionsView;)Lcom/squareup/marketfont/MarketTextView;
    .locals 0

    .line 43
    iget-object p0, p0, Lcom/squareup/ui/activity/TipOptionsView;->tipOptionCustom:Lcom/squareup/marketfont/MarketTextView;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/activity/TipOptionsView;)Ljava/util/List;
    .locals 0

    .line 43
    iget-object p0, p0, Lcom/squareup/ui/activity/TipOptionsView;->tipOptionAmounts:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/ui/activity/TipOptionsView;Ljava/lang/Long;)V
    .locals 0

    .line 43
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/TipOptionsView;->fireTipOptionSelected(Ljava/lang/Long;)V

    return-void
.end method

.method private addTipOption(Ljava/lang/String;I)V
    .locals 3

    .line 140
    new-instance v0, Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p0}, Lcom/squareup/ui/activity/TipOptionsView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/marketfont/MarketTextView;-><init>(Landroid/content/Context;)V

    const/16 v1, 0x11

    .line 141
    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setGravity(I)V

    .line 142
    invoke-virtual {p0}, Lcom/squareup/ui/activity/TipOptionsView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/squareup/marin/R$style;->TextAppearance_Marin_Medium_Blue:I

    invoke-virtual {v0, v1, v2}, Lcom/squareup/marketfont/MarketTextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 144
    sget v1, Lcom/squareup/marin/R$drawable;->marin_selector_clear_ultra_light_gray_pressed:I

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setBackgroundResource(I)V

    .line 146
    iget-object v1, p0, Lcom/squareup/ui/activity/TipOptionsView;->onClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 147
    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 148
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setTag(Ljava/lang/Object;)V

    .line 149
    sget-object p1, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 150
    iget p1, p0, Lcom/squareup/ui/activity/TipOptionsView;->buttonMinWidth:I

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setMinimumWidth(I)V

    .line 153
    iget p1, p0, Lcom/squareup/ui/activity/TipOptionsView;->buttonHorizontalPadding:I

    .line 154
    invoke-virtual {p0}, Lcom/squareup/ui/activity/TipOptionsView;->getChildCount()I

    move-result p2

    const/4 v1, 0x1

    if-ne p2, v1, :cond_0

    const/4 p1, 0x3

    .line 156
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/TipOptionsView;->getPaddingForGravity(I)I

    move-result p1

    .line 158
    :cond_0
    iget p2, p0, Lcom/squareup/ui/activity/TipOptionsView;->buttonHorizontalPadding:I

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v2, p2, v2}, Lcom/squareup/marketfont/MarketTextView;->setPadding(IIII)V

    .line 160
    iget-object p1, p0, Lcom/squareup/ui/activity/TipOptionsView;->textColors:Landroid/content/res/ColorStateList;

    if-eqz p1, :cond_1

    .line 161
    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 164
    :cond_1
    new-instance p1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 p2, -0x2

    const/4 v2, -0x1

    invoke-direct {p1, p2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/4 p2, 0x5

    .line 166
    iput p2, p1, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 169
    invoke-virtual {p0}, Lcom/squareup/ui/activity/TipOptionsView;->getChildCount()I

    move-result p2

    if-lt p2, v1, :cond_2

    .line 173
    invoke-virtual {p0}, Lcom/squareup/ui/activity/TipOptionsView;->getChildCount()I

    move-result p2

    sub-int/2addr p2, v1

    .line 175
    invoke-virtual {p0, v0, p2, p1}, Lcom/squareup/ui/activity/TipOptionsView;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    return-void

    .line 170
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-class v0, Lcom/squareup/ui/activity/TipOptionsView;

    .line 171
    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " should always have at least 1 child view."

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private bindViews()V
    .locals 1

    .line 198
    sget v0, Lcom/squareup/billhistoryui/R$id;->tip_option_custom:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/activity/TipOptionsView;->tipOptionCustom:Lcom/squareup/marketfont/MarketTextView;

    return-void
.end method

.method private clearTipOptions()V
    .locals 2

    .line 132
    invoke-virtual {p0}, Lcom/squareup/ui/activity/TipOptionsView;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/squareup/ui/activity/TipOptionsView;->removeViews(II)V

    return-void
.end method

.method private fireTipOptionSelected(Ljava/lang/Long;)V
    .locals 1

    .line 179
    iget-object v0, p0, Lcom/squareup/ui/activity/TipOptionsView;->onTipOptionSelectedListener:Lcom/squareup/ui/activity/TipOptionsView$OnTipOptionSelectedListener;

    if-eqz v0, :cond_0

    .line 180
    invoke-interface {v0, p1}, Lcom/squareup/ui/activity/TipOptionsView$OnTipOptionSelectedListener;->onTipOptionSelected(Ljava/lang/Long;)V

    :cond_0
    return-void
.end method

.method private getPaddingForGravity(I)I
    .locals 2

    .line 193
    iget v0, p0, Lcom/squareup/ui/activity/TipOptionsView;->gravity:I

    and-int/2addr v0, p1

    const/4 v1, 0x0

    if-ne v0, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    goto :goto_1

    .line 194
    :cond_1
    iget v1, p0, Lcom/squareup/ui/activity/TipOptionsView;->buttonHorizontalPadding:I

    :goto_1
    return v1
.end method

.method private setLeftPadding(Landroid/view/View;I)V
    .locals 3

    .line 185
    invoke-virtual {p1}, Landroid/view/View;->getPaddingTop()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getPaddingRight()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getPaddingBottom()I

    move-result v2

    invoke-virtual {p1, p2, v0, v1, v2}, Landroid/view/View;->setPadding(IIII)V

    return-void
.end method

.method private setRightPadding(Landroid/view/View;I)V
    .locals 3

    .line 189
    invoke-virtual {p1}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getPaddingBottom()I

    move-result v2

    invoke-virtual {p1, v0, v1, p2, v2}, Landroid/view/View;->setPadding(IIII)V

    return-void
.end method


# virtual methods
.method public setHorizontalGravity(I)V
    .locals 1

    and-int/lit8 p1, p1, 0x7

    .line 117
    iget v0, p0, Lcom/squareup/ui/activity/TipOptionsView;->gravity:I

    if-ne v0, p1, :cond_0

    return-void

    .line 120
    :cond_0
    iput p1, p0, Lcom/squareup/ui/activity/TipOptionsView;->gravity:I

    .line 123
    invoke-virtual {p0}, Lcom/squareup/ui/activity/TipOptionsView;->getChildCount()I

    move-result p1

    const/4 v0, 0x1

    if-le p1, v0, :cond_1

    const/4 p1, 0x0

    .line 125
    invoke-virtual {p0, p1}, Lcom/squareup/ui/activity/TipOptionsView;->getChildAt(I)Landroid/view/View;

    move-result-object p1

    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/squareup/ui/activity/TipOptionsView;->getPaddingForGravity(I)I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/activity/TipOptionsView;->setLeftPadding(Landroid/view/View;I)V

    .line 127
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/activity/TipOptionsView;->tipOptionCustom:Lcom/squareup/marketfont/MarketTextView;

    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/squareup/ui/activity/TipOptionsView;->getPaddingForGravity(I)I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/activity/TipOptionsView;->setRightPadding(Landroid/view/View;I)V

    return-void
.end method

.method public setOnTipOptionSelectedListener(Lcom/squareup/ui/activity/TipOptionsView$OnTipOptionSelectedListener;)V
    .locals 0

    .line 96
    iput-object p1, p0, Lcom/squareup/ui/activity/TipOptionsView;->onTipOptionSelectedListener:Lcom/squareup/ui/activity/TipOptionsView$OnTipOptionSelectedListener;

    return-void
.end method

.method public setTextColor(Landroid/content/res/ColorStateList;)V
    .locals 2

    .line 87
    iput-object p1, p0, Lcom/squareup/ui/activity/TipOptionsView;->textColors:Landroid/content/res/ColorStateList;

    const/4 v0, 0x0

    .line 89
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/ui/activity/TipOptionsView;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 90
    invoke-virtual {p0, v0}, Lcom/squareup/ui/activity/TipOptionsView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 91
    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public setTipOptions(Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .line 100
    iput-object p2, p0, Lcom/squareup/ui/activity/TipOptionsView;->tipOptionAmounts:Ljava/util/List;

    .line 102
    invoke-direct {p0}, Lcom/squareup/ui/activity/TipOptionsView;->clearTipOptions()V

    const/4 p2, 0x0

    .line 103
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge p2, v0, :cond_0

    .line 104
    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0, p2}, Lcom/squareup/ui/activity/TipOptionsView;->addTipOption(Ljava/lang/String;I)V

    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
