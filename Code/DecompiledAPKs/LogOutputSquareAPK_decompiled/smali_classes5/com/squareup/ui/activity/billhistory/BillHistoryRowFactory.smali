.class Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;
.super Ljava/lang/Object;
.source "BillHistoryRowFactory.java"


# instance fields
.field private final countryCode:Lcom/squareup/CountryCode;

.field private final entryRowFactory:Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow$Factory;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

.field private final perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

.field private final percentageFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation
.end field

.field private final photoFactory:Lcom/squareup/ui/photo/ItemPhoto$Factory;

.field private final res:Lcom/squareup/util/Res;

.field private final taxFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation
.end field

.field private final voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;


# direct methods
.method constructor <init>(Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/util/Res;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow$Factory;Lcom/squareup/CountryCode;)V
    .locals 0
    .param p3    # Lcom/squareup/text/Formatter;
        .annotation runtime Lcom/squareup/money/ForTaxPercentage;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            "Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow$Factory;",
            "Lcom/squareup/CountryCode;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->photoFactory:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    .line 62
    iput-object p2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->percentageFormatter:Lcom/squareup/text/Formatter;

    .line 63
    iput-object p3, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->taxFormatter:Lcom/squareup/text/Formatter;

    .line 64
    iput-object p4, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 65
    iput-object p5, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    .line 66
    iput-object p6, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    .line 67
    iput-object p7, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->res:Lcom/squareup/util/Res;

    .line 68
    iput-object p8, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    .line 69
    iput-object p9, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->entryRowFactory:Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow$Factory;

    .line 70
    iput-object p10, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->countryCode:Lcom/squareup/CountryCode;

    return-void
.end method

.method private buildAttributedCompReason(Lcom/squareup/checkout/CartItem;Lcom/squareup/permissions/Employee;)Ljava/lang/String;
    .locals 2

    .line 332
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/billhistoryui/R$string;->comp_reason_attributed:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 333
    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->getCompReason()Ljava/lang/String;

    move-result-object p1

    const-string v1, "reason"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object v0, p2, Lcom/squareup/permissions/Employee;->firstName:Ljava/lang/String;

    const-string v1, "first_name"

    .line 334
    invoke-virtual {p1, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object p2, p2, Lcom/squareup/permissions/Employee;->lastName:Ljava/lang/String;

    const-string v0, "last_name"

    .line 335
    invoke-virtual {p1, v0, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 336
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 337
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private buildAttributedVoidReason(Lcom/squareup/checkout/CartItem;Lcom/squareup/permissions/Employee;)Ljava/lang/String;
    .locals 2

    .line 354
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/billhistoryui/R$string;->void_reason_attributed:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 355
    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->getVoidReason()Ljava/lang/String;

    move-result-object p1

    const-string v1, "reason"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object v0, p2, Lcom/squareup/permissions/Employee;->firstName:Ljava/lang/String;

    const-string v1, "first_name"

    .line 356
    invoke-virtual {p1, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object p2, p2, Lcom/squareup/permissions/Employee;->lastName:Ljava/lang/String;

    const-string v0, "last_name"

    .line 357
    invoke-virtual {p1, v0, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 358
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 359
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private buildDefaultCompReason(Lcom/squareup/checkout/CartItem;)Ljava/lang/String;
    .locals 2

    .line 325
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/billhistoryui/R$string;->comp_reason_default:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 326
    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->getCompReason()Ljava/lang/String;

    move-result-object p1

    const-string v1, "reason"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 327
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 328
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private buildDefaultVoidReason(Lcom/squareup/checkout/CartItem;)Ljava/lang/String;
    .locals 2

    .line 347
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/billhistoryui/R$string;->void_reason_default:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 348
    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->getVoidReason()Ljava/lang/String;

    move-result-object p1

    const-string v1, "reason"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 349
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 350
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private compReason(Lcom/squareup/checkout/CartItem;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkout/CartItem;",
            ")",
            "Lio/reactivex/Single<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 319
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->getCompingEmployeeToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->maybeOneEmployeeByToken(Ljava/lang/String;)Lio/reactivex/Maybe;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/activity/billhistory/-$$Lambda$BillHistoryRowFactory$dTc9zHLeMYoyqE4Br0VPZiMbqM4;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/activity/billhistory/-$$Lambda$BillHistoryRowFactory$dTc9zHLeMYoyqE4Br0VPZiMbqM4;-><init>(Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;Lcom/squareup/checkout/CartItem;)V

    .line 320
    invoke-virtual {v0, v1}, Lio/reactivex/Maybe;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Maybe;

    move-result-object v0

    .line 321
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->buildDefaultCompReason(Lcom/squareup/checkout/CartItem;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lio/reactivex/Maybe;->toSingle(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method private voidReason(Lcom/squareup/checkout/CartItem;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkout/CartItem;",
            ")",
            "Lio/reactivex/Single<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 341
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->getVoidingEmployeeToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->maybeOneEmployeeByToken(Ljava/lang/String;)Lio/reactivex/Maybe;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/activity/billhistory/-$$Lambda$BillHistoryRowFactory$YQnnr2rBqTg7Jf6RTrHBvTBlNoY;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/activity/billhistory/-$$Lambda$BillHistoryRowFactory$YQnnr2rBqTg7Jf6RTrHBvTBlNoY;-><init>(Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;Lcom/squareup/checkout/CartItem;)V

    .line 342
    invoke-virtual {v0, v1}, Lio/reactivex/Maybe;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Maybe;

    move-result-object v0

    .line 343
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->buildDefaultVoidReason(Lcom/squareup/checkout/CartItem;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lio/reactivex/Maybe;->toSingle(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method createAdjustmentRow(Landroid/content/Context;Lcom/squareup/payment/OrderAdjustment;)Landroid/view/View;
    .locals 6

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->entryRowFactory:Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow$Factory;

    iget-object v2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->res:Lcom/squareup/util/Res;

    iget-object v3, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v5, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->countryCode:Lcom/squareup/CountryCode;

    const/4 v4, 0x0

    move-object v1, p1

    .line 103
    invoke-virtual/range {v0 .. v5}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow$Factory;->create(Landroid/content/Context;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;ZLcom/squareup/CountryCode;)Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;

    move-result-object p1

    .line 104
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->percentageFormatter:Lcom/squareup/text/Formatter;

    iget-object v1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->taxFormatter:Lcom/squareup/text/Formatter;

    iget-object v2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p1, p2, v0, v1, v2}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showAdjustment(Lcom/squareup/payment/OrderAdjustment;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;)V

    return-object p1
.end method

.method createDiscountLineItem(Landroid/content/Context;Lcom/squareup/protos/client/bills/DiscountLineItem;ZZ)Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;
    .locals 6

    .line 222
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->entryRowFactory:Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow$Factory;

    iget-object v2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->res:Lcom/squareup/util/Res;

    iget-object v3, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v5, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->countryCode:Lcom/squareup/CountryCode;

    move-object v1, p1

    move v4, p3

    .line 223
    invoke-virtual/range {v0 .. v5}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow$Factory;->create(Landroid/content/Context;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;ZLcom/squareup/CountryCode;)Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;

    move-result-object p1

    .line 224
    iget-object p3, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->percentageFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p1, p2, p3, v0, p4}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showDiscountLineItem(Lcom/squareup/protos/client/bills/DiscountLineItem;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Z)V

    return-object p1
.end method

.method createFeeLineItem(Landroid/content/Context;Lcom/squareup/protos/client/bills/FeeLineItem;ZZ)Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;
    .locals 6

    .line 232
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->entryRowFactory:Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow$Factory;

    iget-object v2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->res:Lcom/squareup/util/Res;

    iget-object v3, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v5, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->countryCode:Lcom/squareup/CountryCode;

    move-object v1, p1

    move v4, p3

    .line 233
    invoke-virtual/range {v0 .. v5}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow$Factory;->create(Landroid/content/Context;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;ZLcom/squareup/CountryCode;)Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;

    move-result-object p1

    .line 234
    iget-object p3, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->percentageFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p1, p2, p3, v0, p4}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showFeeLineItem(Lcom/squareup/protos/client/bills/FeeLineItem;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Z)V

    return-object p1
.end method

.method createGiftCardRows(Landroid/content/Context;Lcom/squareup/checkout/CartItem;Lcom/squareup/debounce/DebouncedOnClickListener;Z)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/squareup/checkout/CartItem;",
            "Lcom/squareup/debounce/DebouncedOnClickListener;",
            "Z)",
            "Ljava/util/List<",
            "Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;",
            ">;"
        }
    .end annotation

    .line 254
    invoke-virtual {p2}, Lcom/squareup/checkout/CartItem;->isGiftCard()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 257
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 259
    iget-object v1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->entryRowFactory:Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow$Factory;

    iget-object v3, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->res:Lcom/squareup/util/Res;

    iget-object v4, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v6, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->countryCode:Lcom/squareup/CountryCode;

    move-object v2, p1

    move v5, p4

    .line 260
    invoke-virtual/range {v1 .. v6}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow$Factory;->create(Landroid/content/Context;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;ZLcom/squareup/CountryCode;)Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;

    move-result-object v1

    .line 261
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 262
    invoke-virtual {v1, p2}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showTitleWithQuantityAndSubtitle(Lcom/squareup/checkout/CartItem;)V

    .line 263
    iget-object v2, p2, Lcom/squareup/checkout/CartItem;->color:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->setColorStripBackgroundColor(Ljava/lang/String;)V

    .line 264
    iget-object v2, p2, Lcom/squareup/checkout/CartItem;->color:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showGiftCardImage(Ljava/lang/String;)V

    .line 265
    invoke-virtual {v1}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->hideSubtitle()V

    .line 266
    invoke-virtual {v1, p3}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->enableCheckGiftCardBalance(Landroid/view/View$OnClickListener;)V

    .line 268
    invoke-virtual {p2}, Lcom/squareup/checkout/CartItem;->isVoided()Z

    move-result p3

    if-eqz p3, :cond_0

    .line 269
    iget-object v2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->entryRowFactory:Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow$Factory;

    iget-object v4, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->res:Lcom/squareup/util/Res;

    iget-object v5, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v7, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->countryCode:Lcom/squareup/CountryCode;

    move-object v3, p1

    move v6, p4

    .line 270
    invoke-virtual/range {v2 .. v7}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow$Factory;->create(Landroid/content/Context;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;ZLcom/squareup/CountryCode;)Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;

    move-result-object p1

    .line 271
    new-instance p3, Lcom/squareup/ui/activity/billhistory/-$$Lambda$BillHistoryRowFactory$_4CVg6TBmcrKcyZJ7NFn2D3rdQg;

    invoke-direct {p3, p0, p2, p1}, Lcom/squareup/ui/activity/billhistory/-$$Lambda$BillHistoryRowFactory$_4CVg6TBmcrKcyZJ7NFn2D3rdQg;-><init>(Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;Lcom/squareup/checkout/CartItem;Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;)V

    invoke-static {v1, p3}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 274
    invoke-virtual {v1}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->hideAmount()V

    .line 275
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 276
    :cond_0
    iget-object p3, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    invoke-virtual {p3}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->isCompEnabled()Z

    move-result p3

    if-eqz p3, :cond_1

    invoke-virtual {p2}, Lcom/squareup/checkout/CartItem;->isComped()Z

    move-result p3

    if-eqz p3, :cond_1

    .line 277
    iget-object v2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->entryRowFactory:Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow$Factory;

    iget-object v4, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->res:Lcom/squareup/util/Res;

    iget-object v5, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v7, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->countryCode:Lcom/squareup/CountryCode;

    move-object v3, p1

    move v6, p4

    .line 278
    invoke-virtual/range {v2 .. v7}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow$Factory;->create(Landroid/content/Context;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;ZLcom/squareup/CountryCode;)Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;

    move-result-object p1

    .line 279
    new-instance p3, Lcom/squareup/ui/activity/billhistory/-$$Lambda$BillHistoryRowFactory$QV6LRGQi5I7gb49hMW3L97WDTt8;

    invoke-direct {p3, p0, p2, p1}, Lcom/squareup/ui/activity/billhistory/-$$Lambda$BillHistoryRowFactory$QV6LRGQi5I7gb49hMW3L97WDTt8;-><init>(Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;Lcom/squareup/checkout/CartItem;Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;)V

    invoke-static {v1, p3}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 282
    invoke-virtual {p2}, Lcom/squareup/checkout/CartItem;->total()Lcom/squareup/protos/common/Money;

    move-result-object p2

    iget-object p3, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {v1, p2, p3}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showAmount(Lcom/squareup/protos/common/Money;Lcom/squareup/text/Formatter;)V

    .line 283
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 285
    :cond_1
    invoke-virtual {p2}, Lcom/squareup/checkout/CartItem;->total()Lcom/squareup/protos/common/Money;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {v1, p1, p2}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showAmount(Lcom/squareup/protos/common/Money;Lcom/squareup/text/Formatter;)V

    :goto_0
    return-object v0

    .line 255
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Item is not a gift card."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method createItemizationRows(Landroid/content/Context;Lcom/squareup/checkout/CartItem;ZLjava/lang/String;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/squareup/checkout/CartItem;",
            "Z",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;",
            ">;"
        }
    .end annotation

    .line 147
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 148
    iget-object v1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->entryRowFactory:Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow$Factory;

    iget-object v3, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->res:Lcom/squareup/util/Res;

    iget-object v4, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v6, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->countryCode:Lcom/squareup/CountryCode;

    move-object v2, p1

    move v5, p3

    .line 149
    invoke-virtual/range {v1 .. v6}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow$Factory;->create(Landroid/content/Context;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;ZLcom/squareup/CountryCode;)Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;

    move-result-object v1

    .line 150
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 152
    iget-object v2, p2, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    if-eqz v2, :cond_0

    iget-object v2, p2, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    invoke-virtual {v2}, Lcom/squareup/checkout/OrderVariation;->isUnitPriced()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 153
    invoke-virtual {v1, p2}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showTitleWithSubtitle(Lcom/squareup/checkout/CartItem;)V

    goto :goto_0

    .line 155
    :cond_0
    invoke-virtual {v1, p2}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showTitleWithQuantityAndSubtitle(Lcom/squareup/checkout/CartItem;)V

    .line 158
    :goto_0
    iget-object v2, p2, Lcom/squareup/checkout/CartItem;->color:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->setColorStripBackgroundColor(Ljava/lang/String;)V

    .line 160
    invoke-virtual {p2}, Lcom/squareup/checkout/CartItem;->isGiftCard()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 161
    iget-object v2, p2, Lcom/squareup/checkout/CartItem;->color:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showGiftCardImage(Ljava/lang/String;)V

    goto :goto_1

    .line 162
    :cond_1
    invoke-virtual {p2}, Lcom/squareup/checkout/CartItem;->isCustomItem()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 163
    invoke-virtual {v1}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showCustomAmountImage()V

    goto :goto_1

    .line 165
    :cond_2
    iget-object v2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->photoFactory:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    invoke-virtual {v1, p2, v2}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showItemImage(Lcom/squareup/checkout/CartItem;Lcom/squareup/ui/photo/ItemPhoto$Factory;)V

    .line 168
    :goto_1
    invoke-virtual {p2}, Lcom/squareup/checkout/CartItem;->isVoided()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 169
    iget-object v3, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->entryRowFactory:Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow$Factory;

    iget-object v5, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->res:Lcom/squareup/util/Res;

    iget-object v6, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v8, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->countryCode:Lcom/squareup/CountryCode;

    move-object v4, p1

    move v7, p3

    .line 170
    invoke-virtual/range {v3 .. v8}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow$Factory;->create(Landroid/content/Context;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;ZLcom/squareup/CountryCode;)Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;

    move-result-object p1

    .line 171
    new-instance p3, Lcom/squareup/ui/activity/billhistory/-$$Lambda$BillHistoryRowFactory$L5uYGsVhFIM-qsJVMzXtwRTqfPU;

    invoke-direct {p3, p0, p2, p1}, Lcom/squareup/ui/activity/billhistory/-$$Lambda$BillHistoryRowFactory$L5uYGsVhFIM-qsJVMzXtwRTqfPU;-><init>(Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;Lcom/squareup/checkout/CartItem;Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;)V

    invoke-static {v1, p3}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 174
    invoke-virtual {v1}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->hideAmount()V

    .line 175
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 176
    :cond_3
    iget-object v2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    invoke-virtual {v2}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->isCompEnabled()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {p2}, Lcom/squareup/checkout/CartItem;->isComped()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 177
    iget-object v3, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->entryRowFactory:Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow$Factory;

    iget-object v5, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->res:Lcom/squareup/util/Res;

    iget-object v6, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v8, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->countryCode:Lcom/squareup/CountryCode;

    move-object v4, p1

    move v7, p3

    .line 178
    invoke-virtual/range {v3 .. v8}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow$Factory;->create(Landroid/content/Context;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;ZLcom/squareup/CountryCode;)Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;

    move-result-object p1

    .line 179
    new-instance p3, Lcom/squareup/ui/activity/billhistory/-$$Lambda$BillHistoryRowFactory$ETFUCgDGrwbQ3CQk7Zwk-s-L8BQ;

    invoke-direct {p3, p0, p2, p1}, Lcom/squareup/ui/activity/billhistory/-$$Lambda$BillHistoryRowFactory$ETFUCgDGrwbQ3CQk7Zwk-s-L8BQ;-><init>(Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;Lcom/squareup/checkout/CartItem;Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;)V

    invoke-static {v1, p3}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 182
    invoke-virtual {p2}, Lcom/squareup/checkout/CartItem;->total()Lcom/squareup/protos/common/Money;

    move-result-object p2

    iget-object p3, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {v1, p2, p3, p4}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showAmountForItem(Lcom/squareup/protos/common/Money;Lcom/squareup/text/Formatter;Ljava/lang/String;)V

    .line 183
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 185
    :cond_4
    invoke-virtual {p2}, Lcom/squareup/checkout/CartItem;->total()Lcom/squareup/protos/common/Money;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {v1, p1, p2, p4}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showAmountForItem(Lcom/squareup/protos/common/Money;Lcom/squareup/text/Formatter;Ljava/lang/String;)V

    :goto_2
    return-object v0
.end method

.method createReceiptRow(Landroid/content/Context;ZLjava/lang/String;)Landroid/view/View;
    .locals 2

    if-nez p3, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 304
    :cond_0
    new-instance v0, Lcom/squareup/ui/account/view/LineRow$Builder;

    invoke-direct {v0, p1}, Lcom/squareup/ui/account/view/LineRow$Builder;-><init>(Landroid/content/Context;)V

    sget-object p1, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    .line 305
    invoke-virtual {v0, p1}, Lcom/squareup/ui/account/view/LineRow$Builder;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object p1

    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->RECEIPT:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 306
    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/LineRow$Builder;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/billhistoryui/R$string;->receipt_detail_id:I

    .line 307
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v1, "receipt_number"

    .line 308
    invoke-virtual {v0, v1, p3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p3

    .line 309
    invoke-virtual {p3}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p3

    .line 307
    invoke-virtual {p1, p3}, Lcom/squareup/ui/account/view/LineRow$Builder;->setTitle(Ljava/lang/CharSequence;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object p1

    if-eqz p2, :cond_1

    .line 312
    iget-object p2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->res:Lcom/squareup/util/Res;

    sget p3, Lcom/squareup/billhistoryui/R$string;->receipt_detail_not_sent:I

    invoke-interface {p2, p3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/LineRow$Builder;->setNote(Ljava/lang/CharSequence;)Lcom/squareup/ui/account/view/LineRow$Builder;

    .line 315
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/ui/account/view/LineRow$Builder;->build()Lcom/squareup/ui/account/view/LineRow;

    move-result-object p1

    return-object p1
.end method

.method createRefundItemization(Landroid/content/Context;Lcom/squareup/checkout/CartItem;ZLjava/lang/String;)Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;
    .locals 6

    .line 193
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->entryRowFactory:Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow$Factory;

    iget-object v2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->res:Lcom/squareup/util/Res;

    iget-object v3, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v5, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->countryCode:Lcom/squareup/CountryCode;

    move-object v1, p1

    move v4, p3

    .line 194
    invoke-virtual/range {v0 .. v5}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow$Factory;->create(Landroid/content/Context;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;ZLcom/squareup/CountryCode;)Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;

    move-result-object p1

    .line 196
    iget-object p3, p2, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    if-eqz p3, :cond_0

    iget-object p3, p2, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    invoke-virtual {p3}, Lcom/squareup/checkout/OrderVariation;->isUnitPriced()Z

    move-result p3

    if-eqz p3, :cond_0

    .line 197
    invoke-virtual {p1, p2}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showTitleWithSubtitle(Lcom/squareup/checkout/CartItem;)V

    goto :goto_0

    .line 199
    :cond_0
    invoke-virtual {p1, p2}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showTitleWithQuantityAndSubtitle(Lcom/squareup/checkout/CartItem;)V

    .line 202
    :goto_0
    iget-object p3, p2, Lcom/squareup/checkout/CartItem;->color:Ljava/lang/String;

    invoke-virtual {p1, p3}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->setColorStripBackgroundColor(Ljava/lang/String;)V

    .line 204
    invoke-virtual {p2}, Lcom/squareup/checkout/CartItem;->isGiftCard()Z

    move-result p3

    if-eqz p3, :cond_1

    .line 205
    iget-object p3, p2, Lcom/squareup/checkout/CartItem;->color:Ljava/lang/String;

    invoke-virtual {p1, p3}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showGiftCardImage(Ljava/lang/String;)V

    goto :goto_1

    .line 206
    :cond_1
    invoke-virtual {p2}, Lcom/squareup/checkout/CartItem;->isCustomItem()Z

    move-result p3

    if-eqz p3, :cond_2

    .line 207
    invoke-virtual {p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showCustomAmountImage()V

    goto :goto_1

    .line 209
    :cond_2
    iget-object p3, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->photoFactory:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    invoke-virtual {p1, p2, p3}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showItemImage(Lcom/squareup/checkout/CartItem;Lcom/squareup/ui/photo/ItemPhoto$Factory;)V

    .line 212
    :goto_1
    invoke-virtual {p2}, Lcom/squareup/checkout/CartItem;->baseAmount()J

    move-result-wide v0

    invoke-virtual {p2}, Lcom/squareup/checkout/CartItem;->unitPriceOrNull()Lcom/squareup/protos/common/Money;

    move-result-object p2

    iget-object p2, p2, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, v1, p2}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p2

    .line 213
    invoke-static {p2}, Lcom/squareup/money/MoneyMath;->negate(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p2

    .line 215
    iget-object p3, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p1, p2, p3, p4}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showAmountForItem(Lcom/squareup/protos/common/Money;Lcom/squareup/text/Formatter;Ljava/lang/String;)V

    return-object p1
.end method

.method createReturnSurchargeRow(Lcom/squareup/checkout/Surcharge;Lcom/squareup/util/Res;ZLandroid/content/Context;)Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;
    .locals 6

    .line 241
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->entryRowFactory:Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow$Factory;

    iget-object v3, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v5, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->countryCode:Lcom/squareup/CountryCode;

    move-object v1, p4

    move-object v2, p2

    move v4, p3

    .line 242
    invoke-virtual/range {v0 .. v5}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow$Factory;->create(Landroid/content/Context;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;ZLcom/squareup/CountryCode;)Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;

    move-result-object p2

    .line 243
    iget-object p3, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    const/4 p4, 0x1

    invoke-virtual {p2, p1, p3, p4}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showSurcharge(Lcom/squareup/checkout/Surcharge;Lcom/squareup/text/Formatter;Z)V

    return-object p2
.end method

.method createSubtotalRow(Landroid/content/Context;Lcom/squareup/protos/common/Money;)Landroid/view/View;
    .locals 6

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->entryRowFactory:Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow$Factory;

    iget-object v2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->res:Lcom/squareup/util/Res;

    iget-object v3, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v5, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->countryCode:Lcom/squareup/CountryCode;

    const/4 v4, 0x0

    move-object v1, p1

    .line 82
    invoke-virtual/range {v0 .. v5}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow$Factory;->create(Landroid/content/Context;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;ZLcom/squareup/CountryCode;)Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;

    move-result-object p1

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p1, p2, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showSubtotal(Lcom/squareup/protos/common/Money;Lcom/squareup/text/Formatter;)V

    return-object p1
.end method

.method createSurchargeRow(Landroid/content/Context;Lcom/squareup/checkout/Surcharge;)Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;
    .locals 6

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->entryRowFactory:Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow$Factory;

    iget-object v2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->res:Lcom/squareup/util/Res;

    iget-object v3, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v5, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->countryCode:Lcom/squareup/CountryCode;

    const/4 v4, 0x0

    move-object v1, p1

    .line 96
    invoke-virtual/range {v0 .. v5}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow$Factory;->create(Landroid/content/Context;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;ZLcom/squareup/CountryCode;)Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;

    move-result-object p1

    .line 97
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showSurcharge(Lcom/squareup/checkout/Surcharge;Lcom/squareup/text/Formatter;Z)V

    return-object p1
.end method

.method createSwedishRoundingAdjustmentRow(Landroid/content/Context;Lcom/squareup/payment/OrderAdjustment;)Landroid/view/View;
    .locals 6

    .line 120
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->entryRowFactory:Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow$Factory;

    iget-object v2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->res:Lcom/squareup/util/Res;

    iget-object v3, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v5, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->countryCode:Lcom/squareup/CountryCode;

    const/4 v4, 0x0

    move-object v1, p1

    .line 121
    invoke-virtual/range {v0 .. v5}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow$Factory;->create(Landroid/content/Context;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;ZLcom/squareup/CountryCode;)Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;

    move-result-object p1

    .line 122
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p1, p2, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showSwedishRounding(Lcom/squareup/payment/OrderAdjustment;Lcom/squareup/text/Formatter;)V

    return-object p1
.end method

.method createSwedishRoundingAdjustmentRow(Landroid/content/Context;Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;ZZ)Landroid/view/View;
    .locals 6

    .line 129
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->entryRowFactory:Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow$Factory;

    iget-object v2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->res:Lcom/squareup/util/Res;

    iget-object v3, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v5, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->countryCode:Lcom/squareup/CountryCode;

    move-object v1, p1

    move v4, p3

    .line 130
    invoke-virtual/range {v0 .. v5}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow$Factory;->create(Landroid/content/Context;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;ZLcom/squareup/CountryCode;)Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;

    move-result-object p1

    if-eqz p4, :cond_0

    .line 131
    iget-object p2, p2, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->amounts:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts;

    iget-object p2, p2, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts;->applied_money:Lcom/squareup/protos/common/Money;

    .line 132
    invoke-static {p2}, Lcom/squareup/money/MoneyMath;->negate(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p2

    goto :goto_0

    :cond_0
    iget-object p2, p2, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->amounts:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts;

    iget-object p2, p2, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts;->applied_money:Lcom/squareup/protos/common/Money;

    .line 134
    :goto_0
    iget-object p3, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p1, p2, p3}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showSwedishRounding(Lcom/squareup/protos/common/Money;Lcom/squareup/text/Formatter;)V

    return-object p1
.end method

.method createTaxBreakdownRow(Landroid/content/Context;Ljava/lang/String;Lcom/squareup/util/TaxBreakdown$IndividualTaxBreakdown;)Lcom/squareup/ui/activity/billhistory/BillHistoryTaxBreakdownRow;
    .locals 2

    .line 297
    new-instance v0, Lcom/squareup/ui/activity/billhistory/BillHistoryTaxBreakdownRow;

    iget-object v1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-direct {v0, p1, v1, p2, p3}, Lcom/squareup/ui/activity/billhistory/BillHistoryTaxBreakdownRow;-><init>(Landroid/content/Context;Lcom/squareup/text/Formatter;Ljava/lang/String;Lcom/squareup/util/TaxBreakdown$IndividualTaxBreakdown;)V

    return-object v0
.end method

.method createTaxMultipleAdjustmentRow(Landroid/content/Context;)Landroid/view/View;
    .locals 6

    .line 113
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->entryRowFactory:Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow$Factory;

    iget-object v2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->res:Lcom/squareup/util/Res;

    iget-object v3, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v5, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->countryCode:Lcom/squareup/CountryCode;

    const/4 v4, 0x0

    move-object v1, p1

    .line 114
    invoke-virtual/range {v0 .. v5}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow$Factory;->create(Landroid/content/Context;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;ZLcom/squareup/CountryCode;)Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;

    move-result-object p1

    .line 115
    invoke-virtual {p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showTaxMultipleAdjustment()V

    return-object p1
.end method

.method createTipRow(Landroid/content/Context;Lcom/squareup/protos/common/Money;Z)Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;
    .locals 6

    .line 88
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->entryRowFactory:Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow$Factory;

    iget-object v2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->res:Lcom/squareup/util/Res;

    iget-object v3, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v5, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->countryCode:Lcom/squareup/CountryCode;

    const/4 v4, 0x0

    move-object v1, p1

    .line 89
    invoke-virtual/range {v0 .. v5}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow$Factory;->create(Landroid/content/Context;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;ZLcom/squareup/CountryCode;)Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;

    move-result-object p1

    .line 90
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p1, p2, v0, p3}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showTip(Lcom/squareup/protos/common/Money;Lcom/squareup/text/Formatter;Z)V

    return-object p1
.end method

.method createTotalRow(Landroid/content/Context;Lcom/squareup/protos/common/Money;)Landroid/view/View;
    .locals 6

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->entryRowFactory:Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow$Factory;

    iget-object v2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->res:Lcom/squareup/util/Res;

    iget-object v3, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v5, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->countryCode:Lcom/squareup/CountryCode;

    const/4 v4, 0x0

    move-object v1, p1

    .line 75
    invoke-virtual/range {v0 .. v5}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow$Factory;->create(Landroid/content/Context;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;ZLcom/squareup/CountryCode;)Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;

    move-result-object p1

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p1, p2, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showTotal(Lcom/squareup/protos/common/Money;Lcom/squareup/text/Formatter;)V

    return-object p1
.end method

.method public synthetic lambda$compReason$4$BillHistoryRowFactory(Lcom/squareup/checkout/CartItem;Lcom/squareup/permissions/Employee;)Ljava/lang/String;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 320
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->buildAttributedCompReason(Lcom/squareup/checkout/CartItem;Lcom/squareup/permissions/Employee;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$createGiftCardRows$2$BillHistoryRowFactory(Lcom/squareup/checkout/CartItem;Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;)Lio/reactivex/disposables/Disposable;
    .locals 1

    .line 272
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->voidReason(Lcom/squareup/checkout/CartItem;)Lio/reactivex/Single;

    move-result-object p1

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v0, Lcom/squareup/ui/activity/billhistory/-$$Lambda$cjzMyVLyckDdN1fD8VtNMmKDJhk;

    invoke-direct {v0, p2}, Lcom/squareup/ui/activity/billhistory/-$$Lambda$cjzMyVLyckDdN1fD8VtNMmKDJhk;-><init>(Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;)V

    .line 273
    invoke-virtual {p1, v0}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$createGiftCardRows$3$BillHistoryRowFactory(Lcom/squareup/checkout/CartItem;Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;)Lio/reactivex/disposables/Disposable;
    .locals 1

    .line 280
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->compReason(Lcom/squareup/checkout/CartItem;)Lio/reactivex/Single;

    move-result-object p1

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v0, Lcom/squareup/ui/activity/billhistory/-$$Lambda$cjzMyVLyckDdN1fD8VtNMmKDJhk;

    invoke-direct {v0, p2}, Lcom/squareup/ui/activity/billhistory/-$$Lambda$cjzMyVLyckDdN1fD8VtNMmKDJhk;-><init>(Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;)V

    .line 281
    invoke-virtual {p1, v0}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$createItemizationRows$0$BillHistoryRowFactory(Lcom/squareup/checkout/CartItem;Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;)Lio/reactivex/disposables/Disposable;
    .locals 1

    .line 172
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->voidReason(Lcom/squareup/checkout/CartItem;)Lio/reactivex/Single;

    move-result-object p1

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v0, Lcom/squareup/ui/activity/billhistory/-$$Lambda$cjzMyVLyckDdN1fD8VtNMmKDJhk;

    invoke-direct {v0, p2}, Lcom/squareup/ui/activity/billhistory/-$$Lambda$cjzMyVLyckDdN1fD8VtNMmKDJhk;-><init>(Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;)V

    .line 173
    invoke-virtual {p1, v0}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$createItemizationRows$1$BillHistoryRowFactory(Lcom/squareup/checkout/CartItem;Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;)Lio/reactivex/disposables/Disposable;
    .locals 1

    .line 180
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->compReason(Lcom/squareup/checkout/CartItem;)Lio/reactivex/Single;

    move-result-object p1

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v0, Lcom/squareup/ui/activity/billhistory/-$$Lambda$cjzMyVLyckDdN1fD8VtNMmKDJhk;

    invoke-direct {v0, p2}, Lcom/squareup/ui/activity/billhistory/-$$Lambda$cjzMyVLyckDdN1fD8VtNMmKDJhk;-><init>(Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;)V

    .line 181
    invoke-virtual {p1, v0}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$voidReason$5$BillHistoryRowFactory(Lcom/squareup/checkout/CartItem;Lcom/squareup/permissions/Employee;)Ljava/lang/String;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 342
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->buildAttributedVoidReason(Lcom/squareup/checkout/CartItem;Lcom/squareup/permissions/Employee;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
