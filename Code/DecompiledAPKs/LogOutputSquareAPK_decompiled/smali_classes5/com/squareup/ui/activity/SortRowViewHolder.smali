.class public Lcom/squareup/ui/activity/SortRowViewHolder;
.super Lcom/squareup/ui/activity/BulkSettleView$BulkSettleViewHolder;
.source "SortRowViewHolder.java"


# instance fields
.field private checkableGroup:Lcom/squareup/widgets/CheckableGroup;

.field private final presenter:Lcom/squareup/ui/activity/BulkSettlePresenter;


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;Lcom/squareup/ui/activity/BulkSettlePresenter;)V
    .locals 1

    .line 31
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/BulkSettleView$BulkSettleViewHolder;-><init>(Landroid/view/View;)V

    .line 32
    sget v0, Lcom/squareup/billhistoryui/R$id;->bulk_settle_sort_options:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/CheckableGroup;

    iput-object p1, p0, Lcom/squareup/ui/activity/SortRowViewHolder;->checkableGroup:Lcom/squareup/widgets/CheckableGroup;

    .line 34
    iput-object p2, p0, Lcom/squareup/ui/activity/SortRowViewHolder;->presenter:Lcom/squareup/ui/activity/BulkSettlePresenter;

    .line 36
    iget-object p1, p0, Lcom/squareup/ui/activity/SortRowViewHolder;->checkableGroup:Lcom/squareup/widgets/CheckableGroup;

    new-instance v0, Lcom/squareup/ui/activity/-$$Lambda$SortRowViewHolder$EsFuGlwLtmhnzTBhjwhKNdfi4mM;

    invoke-direct {v0, p2}, Lcom/squareup/ui/activity/-$$Lambda$SortRowViewHolder$EsFuGlwLtmhnzTBhjwhKNdfi4mM;-><init>(Lcom/squareup/ui/activity/BulkSettlePresenter;)V

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/CheckableGroup;->setOnCheckedChangeListener(Lcom/squareup/widgets/CheckableGroup$OnCheckedChangeListener;)V

    return-void
.end method

.method private getIdForSortField(Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;)I
    .locals 3

    .line 64
    sget-object v0, Lcom/squareup/ui/activity/SortRowViewHolder$1;->$SwitchMap$com$squareup$ui$activity$TendersAwaitingTipLoader$SortField:[I

    invoke-virtual {p1}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 70
    sget p1, Lcom/squareup/billhistoryui/R$id;->bulk_settle_sort_employee:I

    return p1

    .line 72
    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown sort field: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 68
    :cond_1
    sget p1, Lcom/squareup/billhistoryui/R$id;->bulk_settle_sort_amount:I

    return p1

    .line 66
    :cond_2
    sget p1, Lcom/squareup/billhistoryui/R$id;->bulk_settle_sort_time:I

    return p1
.end method

.method public static inflate(Lcom/squareup/ui/activity/BulkSettlePresenter;Landroid/content/Context;)Lcom/squareup/ui/activity/SortRowViewHolder;
    .locals 2

    .line 21
    sget v0, Lcom/squareup/billhistoryui/R$layout;->activity_applet_bulk_settle_sort_row:I

    const/4 v1, 0x0

    .line 22
    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    .line 23
    new-instance v0, Lcom/squareup/ui/activity/SortRowViewHolder;

    invoke-direct {v0, p1, p0}, Lcom/squareup/ui/activity/SortRowViewHolder;-><init>(Landroid/view/ViewGroup;Lcom/squareup/ui/activity/BulkSettlePresenter;)V

    return-object v0
.end method

.method static synthetic lambda$new$0(Lcom/squareup/ui/activity/BulkSettlePresenter;Lcom/squareup/widgets/CheckableGroup;II)V
    .locals 0

    .line 37
    sget p1, Lcom/squareup/billhistoryui/R$id;->bulk_settle_sort_time:I

    if-ne p2, p1, :cond_0

    .line 38
    sget-object p1, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;->TIME:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/activity/BulkSettlePresenter;->setSortField(Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;)V

    goto :goto_0

    .line 39
    :cond_0
    sget p1, Lcom/squareup/billhistoryui/R$id;->bulk_settle_sort_amount:I

    if-ne p2, p1, :cond_1

    .line 40
    sget-object p1, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;->AMOUNT:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/activity/BulkSettlePresenter;->setSortField(Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;)V

    goto :goto_0

    .line 41
    :cond_1
    sget p1, Lcom/squareup/billhistoryui/R$id;->bulk_settle_sort_employee:I

    if-ne p2, p1, :cond_2

    .line 42
    sget-object p1, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;->EMPLOYEE:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/activity/BulkSettlePresenter;->setSortField(Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;)V

    :goto_0
    return-void

    .line 44
    :cond_2
    new-instance p0, Ljava/lang/AssertionError;

    const-string p1, "Unknown option selected."

    invoke-direct {p0, p1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p0
.end method


# virtual methods
.method onBind(I)V
    .locals 0

    .line 50
    iget-object p1, p0, Lcom/squareup/ui/activity/SortRowViewHolder;->presenter:Lcom/squareup/ui/activity/BulkSettlePresenter;

    invoke-virtual {p1, p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->bindSortRow(Lcom/squareup/ui/activity/SortRowViewHolder;)V

    return-void
.end method

.method public setSortField(Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;)V
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/activity/SortRowViewHolder;->checkableGroup:Lcom/squareup/widgets/CheckableGroup;

    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/SortRowViewHolder;->getIdForSortField(Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;)I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/CheckableGroup;->check(I)V

    return-void
.end method

.method public setSortFieldVisible(Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;Z)V
    .locals 1

    .line 58
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/SortRowViewHolder;->getIdForSortField(Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;)I

    move-result p1

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/activity/SortRowViewHolder;->checkableGroup:Lcom/squareup/widgets/CheckableGroup;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/CheckableGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    .line 60
    invoke-static {p1, p2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method
