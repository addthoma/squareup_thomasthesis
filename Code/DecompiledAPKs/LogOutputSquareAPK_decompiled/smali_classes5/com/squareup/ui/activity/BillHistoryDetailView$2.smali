.class Lcom/squareup/ui/activity/BillHistoryDetailView$2;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "BillHistoryDetailView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/activity/BillHistoryDetailView;->linkStatusToSupportUrl(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/activity/BillHistoryDetailView;

.field final synthetic val$urlResId:I


# direct methods
.method constructor <init>(Lcom/squareup/ui/activity/BillHistoryDetailView;I)V
    .locals 0

    .line 134
    iput-object p1, p0, Lcom/squareup/ui/activity/BillHistoryDetailView$2;->this$0:Lcom/squareup/ui/activity/BillHistoryDetailView;

    iput p2, p0, Lcom/squareup/ui/activity/BillHistoryDetailView$2;->val$urlResId:I

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    .line 136
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    .line 137
    iget v0, p0, Lcom/squareup/ui/activity/BillHistoryDetailView$2;->val$urlResId:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/util/RegisterIntents;->launchBrowser(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method
