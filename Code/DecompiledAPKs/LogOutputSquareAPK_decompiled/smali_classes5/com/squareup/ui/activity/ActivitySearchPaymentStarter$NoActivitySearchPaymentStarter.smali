.class public Lcom/squareup/ui/activity/ActivitySearchPaymentStarter$NoActivitySearchPaymentStarter;
.super Ljava/lang/Object;
.source "ActivitySearchPaymentStarter.java"

# interfaces
.implements Lcom/squareup/ui/activity/ActivitySearchPaymentStarter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/activity/ActivitySearchPaymentStarter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NoActivitySearchPaymentStarter"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public disableContactlessField()V
    .locals 1

    .line 57
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public enableContactlessField()V
    .locals 1

    .line 53
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public register(Lmortar/MortarScope;Lcom/squareup/ui/activity/ActivitySearchPaymentStarter$SearchListener;Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;)V
    .locals 0

    .line 49
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1
.end method
