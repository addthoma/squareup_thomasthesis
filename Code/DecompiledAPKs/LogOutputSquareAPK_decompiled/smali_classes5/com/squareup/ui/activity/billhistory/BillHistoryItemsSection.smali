.class public Lcom/squareup/ui/activity/billhistory/BillHistoryItemsSection;
.super Landroid/widget/LinearLayout;
.source "BillHistoryItemsSection.java"


# instance fields
.field private final itemsContainer:Landroid/widget/LinearLayout;

.field localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;)V"
        }
    .end annotation

    .line 32
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x1

    .line 33
    invoke-virtual {p0, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryItemsSection;->setOrientation(I)V

    .line 35
    const-class v0, Lcom/squareup/ui/activity/billhistory/BillHistoryView$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/billhistory/BillHistoryView$Component;

    invoke-interface {v0, p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryView$Component;->inject(Lcom/squareup/ui/activity/billhistory/BillHistoryItemsSection;)V

    .line 36
    sget v0, Lcom/squareup/billhistoryui/R$layout;->bill_history_items_section:I

    invoke-static {p1, v0, p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryItemsSection;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 38
    sget p1, Lcom/squareup/billhistoryui/R$id;->items_title:I

    invoke-virtual {p0, p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryItemsSection;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    const/4 v0, 0x0

    .line 39
    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/checkout/CartItem;

    invoke-virtual {p2}, Lcom/squareup/checkout/CartItem;->getSelectedDiningOption()Lcom/squareup/checkout/DiningOption;

    move-result-object p2

    invoke-direct {p0, p2}, Lcom/squareup/ui/activity/billhistory/BillHistoryItemsSection;->createTitle(Lcom/squareup/checkout/DiningOption;)Ljava/lang/CharSequence;

    move-result-object p2

    .line 40
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 42
    sget p1, Lcom/squareup/billhistoryui/R$id;->receipt_detail_items_container:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryItemsSection;->itemsContainer:Landroid/widget/LinearLayout;

    return-void
.end method

.method private createTitle(Lcom/squareup/checkout/DiningOption;)Ljava/lang/CharSequence;
    .locals 4

    .line 50
    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryItemsSection;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 51
    sget v1, Lcom/squareup/billhistoryui/R$string;->uppercase_items_and_services:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 52
    invoke-virtual {p1}, Lcom/squareup/checkout/DiningOption;->getName()Ljava/lang/String;

    move-result-object p1

    iget-object v2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryItemsSection;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Locale;

    invoke-virtual {p1, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    .line 54
    sget v2, Lcom/squareup/billhistoryui/R$string;->receipt_detail_items_dining_option:I

    invoke-static {v0, v2}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 55
    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryItemsSection;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-static {v2, v3}, Lcom/squareup/marketfont/MarketSpanKt;->marketSpanFor(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;)Lcom/squareup/fonts/FontSpan;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object v1

    const-string v2, "items"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 59
    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryItemsSection;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-static {v1, v2}, Lcom/squareup/marketfont/MarketSpanKt;->marketSpanFor(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;)Lcom/squareup/fonts/FontSpan;

    move-result-object v1

    .line 58
    invoke-static {p1, v1}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object p1

    const-string v1, "dining_option"

    .line 56
    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 60
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public addView(Landroid/view/View;)V
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryItemsSection;->itemsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-void
.end method
