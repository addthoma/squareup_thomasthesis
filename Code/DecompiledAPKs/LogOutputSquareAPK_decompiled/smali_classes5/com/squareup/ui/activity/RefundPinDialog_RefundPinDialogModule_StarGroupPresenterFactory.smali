.class public final Lcom/squareup/ui/activity/RefundPinDialog_RefundPinDialogModule_StarGroupPresenterFactory;
.super Ljava/lang/Object;
.source "RefundPinDialog_RefundPinDialogModule_StarGroupPresenterFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final module:Lcom/squareup/ui/activity/RefundPinDialog$RefundPinDialogModule;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/activity/RefundPinDialog$RefundPinDialogModule;)V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/ui/activity/RefundPinDialog_RefundPinDialogModule_StarGroupPresenterFactory;->module:Lcom/squareup/ui/activity/RefundPinDialog$RefundPinDialogModule;

    return-void
.end method

.method public static create(Lcom/squareup/ui/activity/RefundPinDialog$RefundPinDialogModule;)Lcom/squareup/ui/activity/RefundPinDialog_RefundPinDialogModule_StarGroupPresenterFactory;
    .locals 1

    .line 31
    new-instance v0, Lcom/squareup/ui/activity/RefundPinDialog_RefundPinDialogModule_StarGroupPresenterFactory;

    invoke-direct {v0, p0}, Lcom/squareup/ui/activity/RefundPinDialog_RefundPinDialogModule_StarGroupPresenterFactory;-><init>(Lcom/squareup/ui/activity/RefundPinDialog$RefundPinDialogModule;)V

    return-object v0
.end method

.method public static starGroupPresenter(Lcom/squareup/ui/activity/RefundPinDialog$RefundPinDialogModule;)Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;
    .locals 1

    .line 36
    invoke-virtual {p0}, Lcom/squareup/ui/activity/RefundPinDialog$RefundPinDialogModule;->starGroupPresenter()Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/ui/activity/RefundPinDialog_RefundPinDialogModule_StarGroupPresenterFactory;->module:Lcom/squareup/ui/activity/RefundPinDialog$RefundPinDialogModule;

    invoke-static {v0}, Lcom/squareup/ui/activity/RefundPinDialog_RefundPinDialogModule_StarGroupPresenterFactory;->starGroupPresenter(Lcom/squareup/ui/activity/RefundPinDialog$RefundPinDialogModule;)Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ui/activity/RefundPinDialog_RefundPinDialogModule_StarGroupPresenterFactory;->get()Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;

    move-result-object v0

    return-object v0
.end method
