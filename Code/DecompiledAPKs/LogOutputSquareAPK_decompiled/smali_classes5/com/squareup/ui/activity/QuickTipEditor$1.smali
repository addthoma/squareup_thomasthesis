.class Lcom/squareup/ui/activity/QuickTipEditor$1;
.super Lcom/squareup/debounce/DebouncedTextWatcher;
.source "QuickTipEditor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/activity/QuickTipEditor;->onAttachedToWindow()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/activity/QuickTipEditor;


# direct methods
.method constructor <init>(Lcom/squareup/ui/activity/QuickTipEditor;)V
    .locals 0

    .line 74
    iput-object p1, p0, Lcom/squareup/ui/activity/QuickTipEditor$1;->this$0:Lcom/squareup/ui/activity/QuickTipEditor;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public doAfterTextChanged(Landroid/text/Editable;)V
    .locals 1

    .line 76
    iget-object p1, p0, Lcom/squareup/ui/activity/QuickTipEditor$1;->this$0:Lcom/squareup/ui/activity/QuickTipEditor;

    iget-object p1, p1, Lcom/squareup/ui/activity/QuickTipEditor;->presenter:Lcom/squareup/ui/activity/QuickTipEditorPresenter;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->onTipAmountChanged(Z)V

    return-void
.end method
