.class public Lcom/squareup/ui/activity/TransactionsHistoryListView$LegacyTransactionsHistoryAdapter;
.super Landroid/widget/BaseAdapter;
.source "TransactionsHistoryListView.java"

# interfaces
.implements Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/activity/TransactionsHistoryListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LegacyTransactionsHistoryAdapter"
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field private final legacyPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

.field private final useEnhancedTransactionSearchUi:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;Z)V
    .locals 0

    .line 97
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 98
    iput-object p1, p0, Lcom/squareup/ui/activity/TransactionsHistoryListView$LegacyTransactionsHistoryAdapter;->context:Landroid/content/Context;

    .line 99
    iput-object p2, p0, Lcom/squareup/ui/activity/TransactionsHistoryListView$LegacyTransactionsHistoryAdapter;->legacyPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    .line 100
    iput-boolean p3, p0, Lcom/squareup/ui/activity/TransactionsHistoryListView$LegacyTransactionsHistoryAdapter;->useEnhancedTransactionSearchUi:Z

    return-void
.end method

.method private bindLoadMoreRow(Lcom/squareup/ui/LoadMoreRow;)V
    .locals 1

    .line 337
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryListView$LegacyTransactionsHistoryAdapter;->legacyPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->getLoadMoreState()Lcom/squareup/ui/LoadMoreState;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/LoadMoreRow;->setLoadMore(Lcom/squareup/ui/LoadMoreState;)V

    .line 338
    iget-object p1, p0, Lcom/squareup/ui/activity/TransactionsHistoryListView$LegacyTransactionsHistoryAdapter;->legacyPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->loadMoreIfNecessary()V

    return-void
.end method

.method private bindShowAllRow(Lcom/squareup/ui/account/view/SmartLineRow;)V
    .locals 1

    .line 332
    sget v0, Lcom/squareup/billhistoryui/R$string;->activity_applet_load_all_activity:I

    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleText(I)V

    .line 333
    sget-object v0, Lcom/squareup/marin/widgets/ChevronVisibility;->GONE:Lcom/squareup/marin/widgets/ChevronVisibility;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setChevronVisibility(Lcom/squareup/marin/widgets/ChevronVisibility;)V

    return-void
.end method

.method private buildBillRow(Landroid/view/ViewGroup;)Lcom/squareup/ui/account/view/SmartLineRow;
    .locals 1

    .line 316
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryListView$LegacyTransactionsHistoryAdapter;->legacyPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->isDisplayedAsSidebar()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 317
    sget v0, Lcom/squareup/billhistoryui/R$layout;->transactions_history_sidebar_list_row:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/account/view/SmartLineRow;

    return-object p1

    .line 320
    :cond_0
    sget v0, Lcom/squareup/billhistoryui/R$layout;->transactions_history_list_row:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/account/view/SmartLineRow;

    return-object p1
.end method

.method private buildDeprecatedBillRow(Landroid/view/ViewGroup;)Lcom/squareup/ui/account/view/SmartLineRow;
    .locals 1

    .line 308
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryListView$LegacyTransactionsHistoryAdapter;->legacyPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->isDisplayedAsSidebar()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 309
    sget v0, Lcom/squareup/pos/container/R$layout;->applet_sidebar_row:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/account/view/SmartLineRow;

    return-object p1

    .line 311
    :cond_0
    sget v0, Lcom/squareup/billhistoryui/R$layout;->activity_applet_list_row:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/account/view/SmartLineRow;

    return-object p1
.end method

.method private buildDeprecatedGroupingHeader(Landroid/view/ViewGroup;)Landroid/widget/TextView;
    .locals 1

    .line 294
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryListView$LegacyTransactionsHistoryAdapter;->legacyPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->isDisplayedAsSidebar()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 295
    sget v0, Lcom/squareup/pos/container/R$layout;->applet_header_sidebar:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    return-object p1

    .line 299
    :cond_0
    sget v0, Lcom/squareup/billhistoryui/R$layout;->activity_applet_list_header_row:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    return-object p1
.end method

.method private buildGroupingHeader(Landroid/view/ViewGroup;)Landroidx/constraintlayout/widget/ConstraintLayout;
    .locals 1

    .line 304
    sget v0, Lcom/squareup/billhistoryui/R$layout;->transactions_history_list_header_row:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/constraintlayout/widget/ConstraintLayout;

    return-object p1
.end method

.method private buildHeaderPaddingRow(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .line 286
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryListView$LegacyTransactionsHistoryAdapter;->legacyPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->isDisplayedAsSidebar()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 287
    new-instance p1, Landroid/view/View;

    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryListView$LegacyTransactionsHistoryAdapter;->context:Landroid/content/Context;

    invoke-direct {p1, v0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    return-object p1

    .line 289
    :cond_0
    sget v0, Lcom/squareup/billhistoryui/R$layout;->activity_applet_list_header_padding_row:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method private buildInstantDepositRow(Landroid/view/ViewGroup;)Lcom/squareup/ui/activity/InstantDepositRowView;
    .locals 1

    .line 278
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryListView$LegacyTransactionsHistoryAdapter;->legacyPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->isDisplayedAsSidebar()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 279
    sget v0, Lcom/squareup/billhistoryui/R$layout;->activity_applet_instant_deposit_row_sidebar:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/activity/InstantDepositRowView;

    return-object p1

    .line 281
    :cond_0
    sget v0, Lcom/squareup/billhistoryui/R$layout;->activity_applet_instant_deposit_row_list:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/activity/InstantDepositRowView;

    return-object p1
.end method

.method private buildShowAllRow(Landroid/view/ViewGroup;)Lcom/squareup/ui/account/view/SmartLineRow;
    .locals 1

    .line 324
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryListView$LegacyTransactionsHistoryAdapter;->legacyPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->isDisplayedAsSidebar()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 325
    sget v0, Lcom/squareup/pos/container/R$layout;->applet_sidebar_row:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/account/view/SmartLineRow;

    return-object p1

    .line 327
    :cond_0
    sget v0, Lcom/squareup/billhistoryui/R$layout;->activity_applet_list_row:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/account/view/SmartLineRow;

    return-object p1
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method bindBillRow(ILcom/squareup/ui/account/view/SmartLineRow;)V
    .locals 5

    .line 218
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryListView$LegacyTransactionsHistoryAdapter;->legacyPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->isRowSelected(I)Z

    move-result v0

    .line 219
    invoke-virtual {p2, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setActivated(Z)V

    .line 220
    iget-object v1, p0, Lcom/squareup/ui/activity/TransactionsHistoryListView$LegacyTransactionsHistoryAdapter;->legacyPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    invoke-virtual {v1, p1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->getBillRowTitle(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleText(Ljava/lang/CharSequence;)V

    .line 225
    iget-object v1, p0, Lcom/squareup/ui/activity/TransactionsHistoryListView$LegacyTransactionsHistoryAdapter;->legacyPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    invoke-virtual {v1, p1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->getBillDateText(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueText(Ljava/lang/CharSequence;)V

    const/4 v1, 0x1

    .line 226
    invoke-virtual {p2, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueVisible(Z)V

    .line 230
    iget-object v2, p0, Lcom/squareup/ui/activity/TransactionsHistoryListView$LegacyTransactionsHistoryAdapter;->legacyPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    invoke-virtual {v2, p1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->getBillVectorImageResId(I)I

    move-result v2

    if-lez v2, :cond_0

    .line 232
    invoke-virtual {p2, v2}, Lcom/squareup/ui/account/view/SmartLineRow;->setVectorImage(I)V

    .line 233
    invoke-virtual {p2, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setBadgedIconBlockVisible(Z)V

    goto :goto_0

    .line 235
    :cond_0
    iget-object v2, p0, Lcom/squareup/ui/activity/TransactionsHistoryListView$LegacyTransactionsHistoryAdapter;->legacyPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    invoke-virtual {v2, p1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->getBillIcon(I)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 237
    invoke-virtual {p2, v2}, Lcom/squareup/ui/account/view/SmartLineRow;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 238
    invoke-virtual {p2, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setBadgedIconBlockVisible(Z)V

    goto :goto_0

    :cond_1
    const/4 v2, 0x4

    .line 240
    invoke-virtual {p2, v2}, Lcom/squareup/ui/account/view/SmartLineRow;->setBadgedIconBlockVisibility(I)V

    .line 243
    :goto_0
    iget-object v2, p0, Lcom/squareup/ui/activity/TransactionsHistoryListView$LegacyTransactionsHistoryAdapter;->legacyPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    invoke-virtual {v2}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->isDisplayedAsSidebar()Z

    move-result v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/squareup/marin/widgets/ChevronVisibility;->VISIBLE:Lcom/squareup/marin/widgets/ChevronVisibility;

    goto :goto_1

    :cond_2
    sget-object v2, Lcom/squareup/marin/widgets/ChevronVisibility;->GONE:Lcom/squareup/marin/widgets/ChevronVisibility;

    .line 246
    :goto_1
    invoke-virtual {p2, v2}, Lcom/squareup/ui/account/view/SmartLineRow;->setChevronVisibility(Lcom/squareup/marin/widgets/ChevronVisibility;)V

    .line 248
    iget-object v2, p0, Lcom/squareup/ui/activity/TransactionsHistoryListView$LegacyTransactionsHistoryAdapter;->legacyPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    invoke-virtual {v2, p1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->getBillBadge(I)Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;

    move-result-object v2

    const/4 v3, 0x0

    if-eqz v2, :cond_3

    const/4 v4, 0x1

    goto :goto_2

    :cond_3
    const/4 v4, 0x0

    .line 250
    :goto_2
    invoke-virtual {p2, v4}, Lcom/squareup/ui/account/view/SmartLineRow;->setBadgeVisible(Z)V

    if-eqz v4, :cond_4

    .line 252
    iget v2, v2, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$Badge;->resId:I

    invoke-virtual {p2, v2}, Lcom/squareup/ui/account/view/SmartLineRow;->setBadgeImage(I)V

    .line 255
    :cond_4
    iget-object v2, p0, Lcom/squareup/ui/activity/TransactionsHistoryListView$LegacyTransactionsHistoryAdapter;->legacyPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    invoke-virtual {v2, p1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->getBillRowSubtitle(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    goto :goto_3

    :cond_5
    const/4 v1, 0x0

    .line 257
    :goto_3
    invoke-virtual {p2, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleVisible(Z)V

    if-eqz v1, :cond_7

    .line 259
    iget-boolean v1, p0, Lcom/squareup/ui/activity/TransactionsHistoryListView$LegacyTransactionsHistoryAdapter;->useEnhancedTransactionSearchUi:Z

    if-eqz v1, :cond_6

    .line 260
    sget v1, Lcom/squareup/billhistoryui/R$dimen;->transactions_history_list_row_title_bottom_padding:I

    invoke-virtual {p2, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleBottomPadding(I)V

    .line 262
    :cond_6
    invoke-virtual {p2, v2}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 266
    :cond_7
    iget-object v1, p0, Lcom/squareup/ui/activity/TransactionsHistoryListView$LegacyTransactionsHistoryAdapter;->legacyPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    invoke-virtual {v1, p1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->doesBillHaveError(I)Z

    move-result v1

    if-eqz v1, :cond_8

    if-nez v0, :cond_8

    .line 267
    invoke-virtual {p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setErrorColors()V

    goto :goto_4

    .line 269
    :cond_8
    invoke-virtual {p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setDefaultColors()V

    :goto_4
    if-nez v0, :cond_9

    .line 273
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryListView$LegacyTransactionsHistoryAdapter;->legacyPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->getBillRowTitleColor(I)I

    move-result p1

    invoke-virtual {p2, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleColor(I)V

    :cond_9
    return-void
.end method

.method public getCount()I
    .locals 1

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryListView$LegacyTransactionsHistoryAdapter;->legacyPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->getRowCount()I

    move-result v0

    return v0
.end method

.method public getHeaderId(I)J
    .locals 2

    .line 190
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryListView$LegacyTransactionsHistoryAdapter;->legacyPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->getHeaderIndexForRow(I)I

    move-result p1

    int-to-long v0, p1

    return-wide v0
.end method

.method public getHeaderView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .line 194
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryListView$LegacyTransactionsHistoryAdapter;->legacyPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->isHeaderOmitted(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 195
    new-instance p1, Landroid/view/View;

    iget-object p2, p0, Lcom/squareup/ui/activity/TransactionsHistoryListView$LegacyTransactionsHistoryAdapter;->context:Landroid/content/Context;

    invoke-direct {p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    return-object p1

    .line 198
    :cond_0
    iget-boolean v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryListView$LegacyTransactionsHistoryAdapter;->useEnhancedTransactionSearchUi:Z

    if-eqz v0, :cond_2

    .line 199
    instance-of v0, p2, Landroidx/constraintlayout/widget/ConstraintLayout;

    if-eqz v0, :cond_1

    check-cast p2, Landroidx/constraintlayout/widget/ConstraintLayout;

    goto :goto_0

    .line 201
    :cond_1
    invoke-direct {p0, p3}, Lcom/squareup/ui/activity/TransactionsHistoryListView$LegacyTransactionsHistoryAdapter;->buildGroupingHeader(Landroid/view/ViewGroup;)Landroidx/constraintlayout/widget/ConstraintLayout;

    move-result-object p2

    .line 203
    :goto_0
    sget p3, Lcom/squareup/billhistoryui/R$id;->header_text_view:I

    invoke-virtual {p2, p3}, Landroidx/constraintlayout/widget/ConstraintLayout;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/TextView;

    .line 204
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryListView$LegacyTransactionsHistoryAdapter;->legacyPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->getHeaderText(I)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p3, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-object p2

    .line 209
    :cond_2
    instance-of v0, p2, Landroid/widget/TextView;

    if-eqz v0, :cond_3

    check-cast p2, Landroid/widget/TextView;

    goto :goto_1

    .line 211
    :cond_3
    invoke-direct {p0, p3}, Lcom/squareup/ui/activity/TransactionsHistoryListView$LegacyTransactionsHistoryAdapter;->buildDeprecatedGroupingHeader(Landroid/view/ViewGroup;)Landroid/widget/TextView;

    move-result-object p2

    .line 212
    :goto_1
    iget-object p3, p0, Lcom/squareup/ui/activity/TransactionsHistoryListView$LegacyTransactionsHistoryAdapter;->legacyPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    invoke-virtual {p3, p1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->getHeaderText(I)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-object p2
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .line 116
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryListView$LegacyTransactionsHistoryAdapter;->legacyPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->getRowBillForUiTests(I)Lcom/squareup/billhistory/model/BillHistory;

    move-result-object p1

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .line 178
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryListView$LegacyTransactionsHistoryAdapter;->legacyPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->getRowType(I)Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$RowType;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$RowType;->ordinal()I

    move-result p1

    return p1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .line 128
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryListView$LegacyTransactionsHistoryAdapter;->legacyPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->getRowType(I)Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$RowType;

    move-result-object v0

    .line 129
    sget-object v1, Lcom/squareup/ui/activity/TransactionsHistoryListView$1;->$SwitchMap$com$squareup$ui$activity$LegacyTransactionsHistoryListPresenter$RowType:[I

    invoke-virtual {v0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$RowType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x1

    if-eq v1, v2, :cond_9

    const/4 v2, 0x2

    if-eq v1, v2, :cond_7

    const/4 v2, 0x3

    if-eq v1, v2, :cond_4

    const/4 p1, 0x4

    if-eq v1, p1, :cond_2

    const/4 p1, 0x5

    if-ne v1, p1, :cond_1

    .line 166
    instance-of p1, p2, Lcom/squareup/ui/LoadMoreRow;

    if-eqz p1, :cond_0

    check-cast p2, Lcom/squareup/ui/LoadMoreRow;

    goto :goto_0

    :cond_0
    new-instance p2, Lcom/squareup/ui/LoadMoreRow;

    iget-object p1, p0, Lcom/squareup/ui/activity/TransactionsHistoryListView$LegacyTransactionsHistoryAdapter;->context:Landroid/content/Context;

    invoke-direct {p2, p1}, Lcom/squareup/ui/LoadMoreRow;-><init>(Landroid/content/Context;)V

    .line 169
    :goto_0
    invoke-direct {p0, p2}, Lcom/squareup/ui/activity/TransactionsHistoryListView$LegacyTransactionsHistoryAdapter;->bindLoadMoreRow(Lcom/squareup/ui/LoadMoreRow;)V

    return-object p2

    .line 173
    :cond_1
    new-instance p1, Ljava/lang/AssertionError;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "Did not specify how to build views for ViewType "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1

    .line 159
    :cond_2
    instance-of p1, p2, Lcom/squareup/ui/account/view/SmartLineRow;

    if-eqz p1, :cond_3

    check-cast p2, Lcom/squareup/ui/account/view/SmartLineRow;

    goto :goto_1

    .line 161
    :cond_3
    invoke-direct {p0, p3}, Lcom/squareup/ui/activity/TransactionsHistoryListView$LegacyTransactionsHistoryAdapter;->buildShowAllRow(Landroid/view/ViewGroup;)Lcom/squareup/ui/account/view/SmartLineRow;

    move-result-object p2

    .line 162
    :goto_1
    invoke-direct {p0, p2}, Lcom/squareup/ui/activity/TransactionsHistoryListView$LegacyTransactionsHistoryAdapter;->bindShowAllRow(Lcom/squareup/ui/account/view/SmartLineRow;)V

    return-object p2

    .line 147
    :cond_4
    instance-of v0, p2, Lcom/squareup/ui/account/view/SmartLineRow;

    if-eqz v0, :cond_5

    .line 148
    check-cast p2, Lcom/squareup/ui/account/view/SmartLineRow;

    goto :goto_2

    .line 150
    :cond_5
    iget-boolean p2, p0, Lcom/squareup/ui/activity/TransactionsHistoryListView$LegacyTransactionsHistoryAdapter;->useEnhancedTransactionSearchUi:Z

    if-eqz p2, :cond_6

    .line 151
    invoke-direct {p0, p3}, Lcom/squareup/ui/activity/TransactionsHistoryListView$LegacyTransactionsHistoryAdapter;->buildBillRow(Landroid/view/ViewGroup;)Lcom/squareup/ui/account/view/SmartLineRow;

    move-result-object p2

    goto :goto_2

    .line 152
    :cond_6
    invoke-direct {p0, p3}, Lcom/squareup/ui/activity/TransactionsHistoryListView$LegacyTransactionsHistoryAdapter;->buildDeprecatedBillRow(Landroid/view/ViewGroup;)Lcom/squareup/ui/account/view/SmartLineRow;

    move-result-object p2

    .line 155
    :goto_2
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/activity/TransactionsHistoryListView$LegacyTransactionsHistoryAdapter;->bindBillRow(ILcom/squareup/ui/account/view/SmartLineRow;)V

    return-object p2

    :cond_7
    if-eqz p2, :cond_8

    goto :goto_3

    .line 142
    :cond_8
    invoke-direct {p0, p3}, Lcom/squareup/ui/activity/TransactionsHistoryListView$LegacyTransactionsHistoryAdapter;->buildHeaderPaddingRow(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    :goto_3
    return-object p2

    .line 132
    :cond_9
    instance-of p1, p2, Lcom/squareup/ui/activity/InstantDepositRowView;

    if-eqz p1, :cond_a

    check-cast p2, Lcom/squareup/ui/activity/InstantDepositRowView;

    goto :goto_4

    .line 135
    :cond_a
    invoke-direct {p0, p3}, Lcom/squareup/ui/activity/TransactionsHistoryListView$LegacyTransactionsHistoryAdapter;->buildInstantDepositRow(Landroid/view/ViewGroup;)Lcom/squareup/ui/activity/InstantDepositRowView;

    move-result-object p2

    .line 136
    :goto_4
    invoke-virtual {p2}, Lcom/squareup/ui/activity/InstantDepositRowView;->updateView()V

    return-object p2
.end method

.method public getViewTypeCount()I
    .locals 1

    .line 182
    invoke-static {}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$RowType;->values()[Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$RowType;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .line 186
    invoke-virtual {p0}, Lcom/squareup/ui/activity/TransactionsHistoryListView$LegacyTransactionsHistoryAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isEnabled(I)Z
    .locals 1

    .line 108
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryListView$LegacyTransactionsHistoryAdapter;->legacyPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->isRowClickable(I)Z

    move-result p1

    return p1
.end method
