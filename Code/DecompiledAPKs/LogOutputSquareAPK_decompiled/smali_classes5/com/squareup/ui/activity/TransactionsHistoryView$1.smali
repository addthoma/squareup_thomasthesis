.class Lcom/squareup/ui/activity/TransactionsHistoryView$1;
.super Lcom/squareup/debounce/DebouncedOnEditorActionListener;
.source "TransactionsHistoryView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/activity/TransactionsHistoryView;->onAttachedToWindow()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/activity/TransactionsHistoryView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/activity/TransactionsHistoryView;)V
    .locals 0

    .line 92
    iput-object p1, p0, Lcom/squareup/ui/activity/TransactionsHistoryView$1;->this$0:Lcom/squareup/ui/activity/TransactionsHistoryView;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnEditorActionListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doOnEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 1

    const/4 v0, 0x3

    if-eq p2, v0, :cond_1

    .line 96
    invoke-static {p3}, Lcom/squareup/text/KeyEvents;->isKeyboardEnterPress(Landroid/view/KeyEvent;)Z

    move-result p2

    if-eqz p2, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    .line 97
    :cond_1
    :goto_0
    iget-object p2, p0, Lcom/squareup/ui/activity/TransactionsHistoryView$1;->this$0:Lcom/squareup/ui/activity/TransactionsHistoryView;

    iget-object p2, p2, Lcom/squareup/ui/activity/TransactionsHistoryView;->legacyPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->onSearchAction(Ljava/lang/String;)V

    const/4 p1, 0x1

    return p1
.end method
