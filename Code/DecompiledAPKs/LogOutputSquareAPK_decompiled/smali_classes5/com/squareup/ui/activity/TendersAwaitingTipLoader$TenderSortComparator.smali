.class Lcom/squareup/ui/activity/TendersAwaitingTipLoader$TenderSortComparator;
.super Ljava/lang/Object;
.source "TendersAwaitingTipLoader.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/activity/TendersAwaitingTipLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TenderSortComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator<",
        "Lcom/squareup/ui/activity/TenderEditState;",
        ">;"
    }
.end annotation


# static fields
.field private static final UNKNOWN:Ljava/lang/String; = "Unknown"


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/activity/TendersAwaitingTipLoader;


# direct methods
.method private constructor <init>(Lcom/squareup/ui/activity/TendersAwaitingTipLoader;)V
    .locals 0

    .line 286
    iput-object p1, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$TenderSortComparator;->this$0:Lcom/squareup/ui/activity/TendersAwaitingTipLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/ui/activity/TendersAwaitingTipLoader;Lcom/squareup/ui/activity/TendersAwaitingTipLoader$1;)V
    .locals 0

    .line 286
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$TenderSortComparator;-><init>(Lcom/squareup/ui/activity/TendersAwaitingTipLoader;)V

    return-void
.end method

.method private compare(Lcom/squareup/billhistory/model/TenderHistory;Lcom/squareup/billhistory/model/TenderHistory;)I
    .locals 3

    .line 295
    sget-object v0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$2;->$SwitchMap$com$squareup$ui$activity$TendersAwaitingTipLoader$SortField:[I

    iget-object v1, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$TenderSortComparator;->this$0:Lcom/squareup/ui/activity/TendersAwaitingTipLoader;

    invoke-static {v1}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->access$300(Lcom/squareup/ui/activity/TendersAwaitingTipLoader;)Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_a

    const/4 v2, 0x2

    if-eq v0, v2, :cond_9

    const/4 v2, 0x3

    if-ne v0, v2, :cond_8

    .line 301
    iget-object v0, p1, Lcom/squareup/billhistory/model/TenderHistory;->employeeToken:Ljava/lang/String;

    iget-object v2, p2, Lcom/squareup/billhistory/model/TenderHistory;->employeeToken:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    return v2

    .line 305
    :cond_0
    iget-object p1, p1, Lcom/squareup/billhistory/model/TenderHistory;->employeeToken:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$TenderSortComparator;->employeeName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 306
    iget-object p2, p2, Lcom/squareup/billhistory/model/TenderHistory;->employeeToken:Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$TenderSortComparator;->employeeName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    if-nez p1, :cond_1

    if-eqz p2, :cond_2

    :cond_1
    const-string v0, "Unknown"

    if-ne p1, v0, :cond_3

    if-ne p2, v0, :cond_3

    :cond_2
    return v2

    :cond_3
    if-nez p1, :cond_4

    return v1

    :cond_4
    const/4 v2, -0x1

    if-nez p2, :cond_5

    return v2

    :cond_5
    if-ne p1, v0, :cond_6

    return v1

    :cond_6
    if-ne p2, v0, :cond_7

    return v2

    .line 325
    :cond_7
    invoke-virtual {p1, p2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result p1

    return p1

    .line 327
    :cond_8
    new-instance p1, Ljava/lang/AssertionError;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Unknown sort field: "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$TenderSortComparator;->this$0:Lcom/squareup/ui/activity/TendersAwaitingTipLoader;

    invoke-static {v0}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->access$300(Lcom/squareup/ui/activity/TendersAwaitingTipLoader;)Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1

    .line 299
    :cond_9
    sget-object v0, Lcom/squareup/money/MoneyMath;->COMPARATOR:Ljava/util/Comparator;

    iget-object p2, p2, Lcom/squareup/billhistory/model/TenderHistory;->amount:Lcom/squareup/protos/common/Money;

    iget-object p1, p1, Lcom/squareup/billhistory/model/TenderHistory;->amount:Lcom/squareup/protos/common/Money;

    invoke-interface {v0, p2, p1}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result p1

    return p1

    .line 297
    :cond_a
    iget-object p2, p2, Lcom/squareup/billhistory/model/TenderHistory;->timestamp:Ljava/util/Date;

    iget-object p1, p1, Lcom/squareup/billhistory/model/TenderHistory;->timestamp:Ljava/util/Date;

    invoke-virtual {p2, p1}, Ljava/util/Date;->compareTo(Ljava/util/Date;)I

    move-result p1

    return p1
.end method

.method private employeeName(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 333
    iget-object v0, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$TenderSortComparator;->this$0:Lcom/squareup/ui/activity/TendersAwaitingTipLoader;

    .line 334
    invoke-static {v0}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->access$400(Lcom/squareup/ui/activity/TendersAwaitingTipLoader;)Lcom/squareup/permissions/Employees;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/permissions/Employees;->maybeOneEmployeeByToken(Ljava/lang/String;)Lio/reactivex/Maybe;

    move-result-object p1

    invoke-virtual {p1}, Lio/reactivex/Maybe;->toObservable()Lio/reactivex/Observable;

    move-result-object p1

    sget-object v0, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {p1, v0}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object p1

    sget-object v0, Lcom/squareup/ui/activity/-$$Lambda$TendersAwaitingTipLoader$TenderSortComparator$Hp26ieA_xZilBrdBnifHR5hRbg8;->INSTANCE:Lcom/squareup/ui/activity/-$$Lambda$TendersAwaitingTipLoader$TenderSortComparator$Hp26ieA_xZilBrdBnifHR5hRbg8;

    .line 336
    invoke-virtual {p1, v0}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    const-string v0, ""

    .line 342
    invoke-virtual {p1, v0}, Lrx/Observable;->defaultIfEmpty(Ljava/lang/Object;)Lrx/Observable;

    move-result-object p1

    .line 333
    invoke-static {p1}, Lcom/squareup/util/rx/RxBlockingSupport;->getValueOrThrow(Lrx/Observable;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 344
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    :cond_0
    return-object p1
.end method

.method static synthetic lambda$employeeName$0(Lcom/squareup/permissions/Employee;)Ljava/lang/String;
    .locals 3

    .line 337
    iget-object v0, p0, Lcom/squareup/permissions/Employee;->firstName:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    const-string v1, ""

    if-eqz v0, :cond_0

    move-object v0, v1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/permissions/Employee;->firstName:Ljava/lang/String;

    .line 338
    :goto_0
    iget-object v2, p0, Lcom/squareup/permissions/Employee;->lastName:Ljava/lang/String;

    invoke-static {v2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lcom/squareup/permissions/Employee;->lastName:Ljava/lang/String;

    .line 339
    :goto_1
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 340
    invoke-static {p0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string p0, "Unknown"

    :cond_2
    return-object p0
.end method


# virtual methods
.method public compare(Lcom/squareup/ui/activity/TenderEditState;Lcom/squareup/ui/activity/TenderEditState;)I
    .locals 0

    .line 291
    iget-object p1, p1, Lcom/squareup/ui/activity/TenderEditState;->tenderHistory:Lcom/squareup/billhistory/model/TenderHistory;

    iget-object p2, p2, Lcom/squareup/ui/activity/TenderEditState;->tenderHistory:Lcom/squareup/billhistory/model/TenderHistory;

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$TenderSortComparator;->compare(Lcom/squareup/billhistory/model/TenderHistory;Lcom/squareup/billhistory/model/TenderHistory;)I

    move-result p1

    return p1
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    .line 286
    check-cast p1, Lcom/squareup/ui/activity/TenderEditState;

    check-cast p2, Lcom/squareup/ui/activity/TenderEditState;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$TenderSortComparator;->compare(Lcom/squareup/ui/activity/TenderEditState;Lcom/squareup/ui/activity/TenderEditState;)I

    move-result p1

    return p1
.end method
