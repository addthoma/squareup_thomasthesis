.class public final Lcom/squareup/ui/activity/TenderEditState;
.super Ljava/lang/Object;
.source "TenderEditState.java"


# instance fields
.field public tenderHistory:Lcom/squareup/billhistory/model/TenderHistory;

.field public wasQuickTipUsed:Z


# direct methods
.method public constructor <init>(Lcom/squareup/billhistory/model/TenderHistory;)V
    .locals 1

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 11
    iput-boolean v0, p0, Lcom/squareup/ui/activity/TenderEditState;->wasQuickTipUsed:Z

    .line 12
    iput-object p1, p0, Lcom/squareup/ui/activity/TenderEditState;->tenderHistory:Lcom/squareup/billhistory/model/TenderHistory;

    return-void
.end method
