.class Lcom/squareup/ui/activity/billhistory/BillHistoryTaxBreakdownSection;
.super Landroid/widget/LinearLayout;
.source "BillHistoryTaxBreakdownSection.java"


# instance fields
.field private final container:Landroid/widget/LinearLayout;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 15
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x1

    .line 16
    invoke-virtual {p0, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryTaxBreakdownSection;->setOrientation(I)V

    .line 18
    sget v0, Lcom/squareup/billhistoryui/R$layout;->bill_history_tax_breakdown_section:I

    invoke-static {p1, v0, p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryTaxBreakdownSection;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 20
    sget p1, Lcom/squareup/billhistoryui/R$id;->tender_section_tax_breakdown_container:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTaxBreakdownSection;->container:Landroid/widget/LinearLayout;

    return-void
.end method


# virtual methods
.method public addView(Landroid/view/View;)V
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryTaxBreakdownSection;->container:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-void
.end method
