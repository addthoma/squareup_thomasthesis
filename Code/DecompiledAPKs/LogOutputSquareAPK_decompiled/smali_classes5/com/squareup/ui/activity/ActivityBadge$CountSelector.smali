.class Lcom/squareup/ui/activity/ActivityBadge$CountSelector;
.super Ljava/lang/Object;
.source "ActivityBadge.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/activity/ActivityBadge;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "CountSelector"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/activity/ActivityBadge$CountSelector$CountType;
    }
.end annotation


# instance fields
.field private final currentlySelectedCount:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/payment/pending/PendingTransactionsStore;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    invoke-interface {p1}, Lcom/squareup/connectivity/ConnectivityMonitor;->internetState()Lio/reactivex/Observable;

    move-result-object p1

    .line 49
    invoke-interface {p2}, Lcom/squareup/payment/pending/PendingTransactionsStore;->processingOfflineTransactions()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/activity/-$$Lambda$ActivityBadge$CountSelector$Jf8pRy4uYvf8L-F6BdsGXFX99oQ;->INSTANCE:Lcom/squareup/ui/activity/-$$Lambda$ActivityBadge$CountSelector$Jf8pRy4uYvf8L-F6BdsGXFX99oQ;

    .line 47
    invoke-static {p1, v0, v1}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/activity/-$$Lambda$ActivityBadge$CountSelector$j0bXwCDn1UIgBNjF9ZqUW2VlCLg;

    invoke-direct {v0, p2}, Lcom/squareup/ui/activity/-$$Lambda$ActivityBadge$CountSelector$j0bXwCDn1UIgBNjF9ZqUW2VlCLg;-><init>(Lcom/squareup/payment/pending/PendingTransactionsStore;)V

    .line 56
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    .line 66
    invoke-virtual {p1}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/activity/ActivityBadge$CountSelector;->currentlySelectedCount:Lio/reactivex/Observable;

    return-void
.end method

.method static synthetic lambda$new$0(Lcom/squareup/connectivity/InternetState;Ljava/lang/Boolean;)Lcom/squareup/ui/activity/ActivityBadge$CountSelector$CountType;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 51
    sget-object v0, Lcom/squareup/connectivity/InternetState;->NOT_CONNECTED:Lcom/squareup/connectivity/InternetState;

    if-ne p0, v0, :cond_0

    .line 52
    sget-object p0, Lcom/squareup/ui/activity/ActivityBadge$CountSelector$CountType;->RAW:Lcom/squareup/ui/activity/ActivityBadge$CountSelector$CountType;

    return-object p0

    .line 54
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-eqz p0, :cond_1

    sget-object p0, Lcom/squareup/ui/activity/ActivityBadge$CountSelector$CountType;->RAW:Lcom/squareup/ui/activity/ActivityBadge$CountSelector$CountType;

    goto :goto_0

    :cond_1
    sget-object p0, Lcom/squareup/ui/activity/ActivityBadge$CountSelector$CountType;->RIPENED:Lcom/squareup/ui/activity/ActivityBadge$CountSelector$CountType;

    :goto_0
    return-object p0
.end method

.method static synthetic lambda$new$1(Lcom/squareup/payment/pending/PendingTransactionsStore;Lcom/squareup/ui/activity/ActivityBadge$CountSelector$CountType;)Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 57
    sget-object v0, Lcom/squareup/ui/activity/ActivityBadge$1;->$SwitchMap$com$squareup$ui$activity$ActivityBadge$CountSelector$CountType:[I

    invoke-virtual {p1}, Lcom/squareup/ui/activity/ActivityBadge$CountSelector$CountType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 61
    invoke-interface {p0}, Lcom/squareup/payment/pending/PendingTransactionsStore;->smoothedAllPendingTransactionsCount()Lio/reactivex/Observable;

    move-result-object p0

    return-object p0

    .line 63
    :cond_0
    new-instance p0, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unrecognized count type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 59
    :cond_1
    invoke-interface {p0}, Lcom/squareup/payment/pending/PendingTransactionsStore;->smoothedRipenedAllPendingTransactionsCount()Lio/reactivex/Observable;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method count()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/activity/ActivityBadge$CountSelector;->currentlySelectedCount:Lio/reactivex/Observable;

    return-object v0
.end method
