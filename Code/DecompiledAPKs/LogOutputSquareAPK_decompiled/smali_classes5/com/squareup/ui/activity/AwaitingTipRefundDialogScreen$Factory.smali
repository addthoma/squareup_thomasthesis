.class public Lcom/squareup/ui/activity/AwaitingTipRefundDialogScreen$Factory;
.super Ljava/lang/Object;
.source "AwaitingTipRefundDialogScreen.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/activity/AwaitingTipRefundDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$create$0(Lcom/squareup/ui/activity/BillHistoryRunner;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 44
    invoke-virtual {p0}, Lcom/squareup/ui/activity/BillHistoryRunner;->continueToRefund()V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 39
    const-class v0, Lcom/squareup/ui/activity/AwaitingTipRefundDialogScreen$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/AwaitingTipRefundDialogScreen$Component;

    .line 40
    invoke-interface {v0}, Lcom/squareup/ui/activity/AwaitingTipRefundDialogScreen$Component;->billHistoryController()Lcom/squareup/ui/activity/BillHistoryRunner;

    move-result-object v0

    .line 42
    new-instance v1, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v1, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget p1, Lcom/squareup/activity/R$string;->refund:I

    new-instance v2, Lcom/squareup/ui/activity/-$$Lambda$AwaitingTipRefundDialogScreen$Factory$jpyw3VZIGwjRhEq9R101qfNGRp0;

    invoke-direct {v2, v0}, Lcom/squareup/ui/activity/-$$Lambda$AwaitingTipRefundDialogScreen$Factory$jpyw3VZIGwjRhEq9R101qfNGRp0;-><init>(Lcom/squareup/ui/activity/BillHistoryRunner;)V

    .line 43
    invoke-virtual {v1, p1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/common/strings/R$string;->cancel:I

    .line 45
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/billhistoryui/R$string;->awaiting_tip:I

    .line 46
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/billhistoryui/R$string;->refund_tenders_awaiting_tip_message:I

    .line 47
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 48
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 42
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
