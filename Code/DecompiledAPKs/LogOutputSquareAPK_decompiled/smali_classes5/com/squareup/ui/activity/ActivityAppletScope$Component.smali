.class public interface abstract Lcom/squareup/ui/activity/ActivityAppletScope$Component;
.super Ljava/lang/Object;
.source "ActivityAppletScope.java"

# interfaces
.implements Lcom/squareup/ui/activity/IssueRefundScope$ParentComponent;
.implements Lcom/squareup/debitcard/LinkDebitCardScope$ParentComponent;
.implements Lcom/squareup/instantdeposit/PriceChangeDialog$ParentComponent;


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/ui/activity/ActivityAppletScope$Module;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/activity/ActivityAppletScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract abstractActivityCardPresenterController()Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;
.end method

.method public abstract activityAppletScopeRunner()Lcom/squareup/ui/activity/ActivityAppletScopeRunner;
.end method

.method public abstract awaitingTipRefundPopupScreen()Lcom/squareup/ui/activity/AwaitingTipRefundDialogScreen$Component;
.end method

.method public abstract billHistoryDetailScreen()Lcom/squareup/ui/activity/BillHistoryDetailScreen$Component;
.end method

.method public abstract bulkSettleScope()Lcom/squareup/ui/activity/BulkSettleScope$Component;
.end method

.method public abstract instantDepositResultScreen()Lcom/squareup/ui/instantdeposits/InstantDepositsResultScreen$Component;
.end method

.method public abstract issueReceiptScreen()Lcom/squareup/ui/activity/IssueReceiptScreen$Component;
.end method

.method public abstract legacyTransactionsHistory()Lcom/squareup/activity/LegacyTransactionsHistory;
.end method

.method public abstract salesHistoryScreen()Lcom/squareup/ui/activity/TransactionsHistoryScreen$Component;
.end method

.method public abstract selectGiftReceiptTenderScreen()Lcom/squareup/ui/activity/SelectGiftReceiptTenderScreen$Component;
.end method

.method public abstract selectReceiptTenderScreen()Lcom/squareup/ui/activity/SelectReceiptTenderScreen$Component;
.end method

.method public abstract selectRefundTenderScreen()Lcom/squareup/ui/activity/SelectRefundTenderScreen$Component;
.end method

.method public abstract tipSettlementFailedDialogScreen()Lcom/squareup/ui/activity/TipSettlementFailedDialogScreen$Component;
.end method
