.class public interface abstract Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2$Runner;
.super Ljava/lang/Object;
.source "ConversationDetailCoordinatorV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation


# virtual methods
.method public abstract closeConversationDetailScreen()V
.end method

.method public abstract showAddCouponToConversation()V
.end method

.method public abstract showContactFromConversation(Lcom/squareup/protos/client/rolodex/Contact;)V
.end method
