.class final Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$sendResult$1;
.super Lkotlin/jvm/internal/Lambda;
.source "ChooseCustomerScopeRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->sendResult(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lflow/History$Builder;",
        "Lflow/History$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "Lflow/History$Builder;",
        "builder",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $maybeCreateResult:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;


# direct methods
.method constructor <init>(Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$sendResult$1;->$maybeCreateResult:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lflow/History$Builder;)Lflow/History$Builder;
    .locals 1

    const-string v0, "builder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 333
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$sendResult$1;->$maybeCreateResult:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;->getHistoryFunc()Lkotlin/jvm/functions/Function1;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/History$Builder;

    :cond_0
    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 65
    check-cast p1, Lflow/History$Builder;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$sendResult$1;->invoke(Lflow/History$Builder;)Lflow/History$Builder;

    move-result-object p1

    return-object p1
.end method
