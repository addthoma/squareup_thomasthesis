.class public interface abstract Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor;
.super Ljava/lang/Object;
.source "CofDippedCardInfoProcessor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$NoOpCofDippedCardInfoProcessor;,
        Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult;
    }
.end annotation


# virtual methods
.method public abstract cancel()V
.end method

.method public abstract onCardInserted()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end method

.method public abstract onDipResult()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult;",
            ">;"
        }
    .end annotation
.end method

.method public abstract processDip()V
.end method

.method public abstract register(Lmortar/MortarScope;)V
.end method
