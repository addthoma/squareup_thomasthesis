.class public interface abstract Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen$Runner;
.super Ljava/lang/Object;
.source "ViewLoyaltyBalanceScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen$Runner$AdjustmentType;
    }
.end annotation


# virtual methods
.method public abstract canGoNegative()Z
.end method

.method public abstract closeViewLoyaltyBalanceScreen()V
.end method

.method public abstract getLoyaltyStatusPoints()I
.end method

.method public abstract goToAdjustPointsScreen(Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen$Runner$AdjustmentType;)V
.end method
