.class public final synthetic Lcom/squareup/ui/crm/v2/profile/-$$Lambda$7WQc9-6lEnNse3z_55A0SS0MZvI;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter$SetContactFieldFunc;


# static fields
.field public static final synthetic INSTANCE:Lcom/squareup/ui/crm/v2/profile/-$$Lambda$7WQc9-6lEnNse3z_55A0SS0MZvI;


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$7WQc9-6lEnNse3z_55A0SS0MZvI;

    invoke-direct {v0}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$7WQc9-6lEnNse3z_55A0SS0MZvI;-><init>()V

    sput-object v0, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$7WQc9-6lEnNse3z_55A0SS0MZvI;->INSTANCE:Lcom/squareup/ui/crm/v2/profile/-$$Lambda$7WQc9-6lEnNse3z_55A0SS0MZvI;

    return-void
.end method

.method private synthetic constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/protos/client/rolodex/Contact;Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Contact;
    .locals 0

    invoke-static {p1, p2}, Lcom/squareup/crm/util/RolodexContactHelper;->withNote(Lcom/squareup/protos/client/rolodex/Contact;Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p1

    return-object p1
.end method
