.class Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
.source "AllCouponsAndRewardsCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ViewHolder"
.end annotation


# instance fields
.field private coupon:Lcom/squareup/protos/client/coupons/Coupon;

.field private final row:Lcom/squareup/ui/account/view/SmartLineRow;

.field private subscription:Lrx/Subscription;

.field final synthetic this$1:Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter;Lcom/squareup/ui/account/view/SmartLineRow;)V
    .locals 4

    .line 97
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;->this$1:Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter;

    .line 98
    invoke-direct {p0, p2}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 99
    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;->row:Lcom/squareup/ui/account/view/SmartLineRow;

    .line 101
    invoke-virtual {p2}, Lcom/squareup/ui/account/view/SmartLineRow;->getStartGlyphView()Lcom/squareup/glyph/SquareGlyphView;

    move-result-object v0

    .line 103
    invoke-virtual {p2}, Lcom/squareup/ui/account/view/SmartLineRow;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/crm/R$drawable;->crm_payment_giftcard:I

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroidx/core/content/res/ResourcesCompat;->getDrawable(Landroid/content/res/Resources;ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 102
    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    const/4 v0, 0x0

    .line 107
    invoke-virtual {p2, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setBadgedIconBlockVisibility(I)V

    .line 109
    invoke-virtual {p2}, Lcom/squareup/ui/account/view/SmartLineRow;->getEndGlyph()Lcom/squareup/glyph/SquareGlyphView;

    move-result-object v0

    .line 111
    invoke-virtual {p2}, Lcom/squareup/ui/account/view/SmartLineRow;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget v1, Lcom/squareup/crm/R$drawable;->crm_action_arrow:I

    invoke-static {p2, v1, v3}, Landroidx/core/content/res/ResourcesCompat;->getDrawable(Landroid/content/res/Resources;ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object p2

    .line 110
    invoke-virtual {v0, p2}, Lcom/squareup/glyph/SquareGlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 114
    iget-object p2, p1, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter;->this$0:Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;

    invoke-static {p2}, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;->access$000(Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;)Lcom/squareup/checkout/HoldsCoupons;

    move-result-object p2

    if-eqz p2, :cond_0

    .line 115
    iget-object p2, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;->row:Lcom/squareup/ui/account/view/SmartLineRow;

    new-instance v0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder$1;-><init>(Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter;)V

    invoke-virtual {p2, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method

.method static synthetic access$100(Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;)Lcom/squareup/protos/client/coupons/Coupon;
    .locals 0

    .line 92
    iget-object p0, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;->coupon:Lcom/squareup/protos/client/coupons/Coupon;

    return-object p0
.end method


# virtual methods
.method bindCoupon(Lcom/squareup/protos/client/coupons/Coupon;)V
    .locals 2

    .line 129
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;->coupon:Lcom/squareup/protos/client/coupons/Coupon;

    if-ne v0, p1, :cond_0

    return-void

    .line 133
    :cond_0
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;->coupon:Lcom/squareup/protos/client/coupons/Coupon;

    .line 135
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;->subscription:Lrx/Subscription;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lrx/Subscription;->isUnsubscribed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 136
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;->this$1:Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter;

    invoke-static {v0}, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter;->access$200(Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter;)Lrx/subscriptions/CompositeSubscription;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;->subscription:Lrx/Subscription;

    invoke-virtual {v0, v1}, Lrx/subscriptions/CompositeSubscription;->remove(Lrx/Subscription;)V

    .line 137
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;->subscription:Lrx/Subscription;

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    .line 140
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;->row:Lcom/squareup/ui/account/view/SmartLineRow;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;->this$1:Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter;

    iget-object v1, v1, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter;->this$0:Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;

    invoke-static {v1}, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;->access$300(Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;)Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;->formatName(Lcom/squareup/protos/client/coupons/Coupon;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleText(Ljava/lang/CharSequence;)V

    .line 142
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;->this$1:Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter;

    iget-object v0, v0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter;->this$0:Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;->access$400(Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;)Lcom/squareup/util/Res;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;->this$1:Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter;

    iget-object v1, v1, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter;->this$0:Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;

    invoke-static {v1}, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;->access$500(Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;)Ljava/util/Locale;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/squareup/crm/DateTimeFormatHelper;->formatCouponDetails(Lcom/squareup/util/Res;Lcom/squareup/protos/client/coupons/Coupon;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 144
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 145
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;->row:Lcom/squareup/ui/account/view/SmartLineRow;

    invoke-virtual {v1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 146
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;->row:Lcom/squareup/ui/account/view/SmartLineRow;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleVisible(Z)V

    goto :goto_0

    .line 148
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;->row:Lcom/squareup/ui/account/view/SmartLineRow;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleVisible(Z)V

    .line 152
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;->this$1:Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter;

    iget-object v0, v0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter;->this$0:Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;->access$000(Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;)Lcom/squareup/checkout/HoldsCoupons;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 153
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;->this$1:Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter;

    iget-object v0, v0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter;->this$0:Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;->access$000(Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;)Lcom/squareup/checkout/HoldsCoupons;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/checkout/HoldsCoupons;->discountsChanged()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v0, v1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v0

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    .line 154
    invoke-virtual {v0, v1}, Lrx/Observable;->startWith(Ljava/lang/Object;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder$ErBH-vIE34b1tAgpx1DvRcdY9GI;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder$ErBH-vIE34b1tAgpx1DvRcdY9GI;-><init>(Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;Lcom/squareup/protos/client/coupons/Coupon;)V

    .line 155
    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    .line 156
    invoke-virtual {p1}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder$ACMinVv7j9ybXcA8AYQQcRVUx-w;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder$ACMinVv7j9ybXcA8AYQQcRVUx-w;-><init>(Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;)V

    .line 157
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;->subscription:Lrx/Subscription;

    .line 165
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;->this$1:Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter;

    invoke-static {p1}, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter;->access$200(Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter;)Lrx/subscriptions/CompositeSubscription;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;->subscription:Lrx/Subscription;

    invoke-virtual {p1, v0}, Lrx/subscriptions/CompositeSubscription;->add(Lrx/Subscription;)V

    :cond_3
    return-void
.end method

.method public synthetic lambda$bindCoupon$0$AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder(Lcom/squareup/protos/client/coupons/Coupon;Lkotlin/Unit;)Ljava/lang/Boolean;
    .locals 0

    .line 155
    iget-object p2, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;->this$1:Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter;

    iget-object p2, p2, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter;->this$0:Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;

    invoke-static {p2}, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;->access$000(Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;)Lcom/squareup/checkout/HoldsCoupons;

    move-result-object p2

    iget-object p1, p1, Lcom/squareup/protos/client/coupons/Coupon;->coupon_id:Ljava/lang/String;

    invoke-interface {p2, p1}, Lcom/squareup/checkout/HoldsCoupons;->isCouponAddedToCart(Ljava/lang/String;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$bindCoupon$1$AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder(Ljava/lang/Boolean;)V
    .locals 1

    .line 158
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 159
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;->row:Lcom/squareup/ui/account/view/SmartLineRow;

    sget v0, Lcom/squareup/marin/R$color;->marin_medium_gray:I

    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleColor(I)V

    goto :goto_0

    .line 161
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter$ViewHolder;->row:Lcom/squareup/ui/account/view/SmartLineRow;

    sget v0, Lcom/squareup/marin/R$color;->marin_black:I

    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleColor(I)V

    :goto_0
    return-void
.end method
