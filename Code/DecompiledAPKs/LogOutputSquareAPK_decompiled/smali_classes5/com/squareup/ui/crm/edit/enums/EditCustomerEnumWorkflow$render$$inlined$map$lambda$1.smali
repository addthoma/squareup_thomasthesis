.class final Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow$render$$inlined$map$lambda$1;
.super Lkotlin/jvm/internal/Lambda;
.source "EditCustomerEnumWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow;->render(Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumProps;Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRendering;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Boolean;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "checked",
        "",
        "invoke",
        "com/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow$render$1$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context$inlined:Lcom/squareup/workflow/RenderContext;

.field final synthetic $it:Ljava/lang/String;

.field final synthetic $state$inlined:Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;

.field final synthetic this$0:Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow;


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow;Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;Lcom/squareup/workflow/RenderContext;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow$render$$inlined$map$lambda$1;->$it:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow$render$$inlined$map$lambda$1;->this$0:Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow;

    iput-object p3, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow$render$$inlined$map$lambda$1;->$state$inlined:Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;

    iput-object p4, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow$render$$inlined$map$lambda$1;->$context$inlined:Lcom/squareup/workflow/RenderContext;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 12
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow$render$$inlined$map$lambda$1;->invoke(Z)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Z)V
    .locals 4

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow$render$$inlined$map$lambda$1;->$context$inlined:Lcom/squareup/workflow/RenderContext;

    invoke-interface {v0}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow$render$$inlined$map$lambda$1;->this$0:Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow;

    iget-object v2, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow$render$$inlined$map$lambda$1;->$state$inlined:Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;

    iget-object v3, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow$render$$inlined$map$lambda$1;->$it:Ljava/lang/String;

    invoke-static {v1, v2, v3, p1}, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow;->access$updateCheckedAction(Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow;Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;Ljava/lang/String;Z)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    return-void
.end method
