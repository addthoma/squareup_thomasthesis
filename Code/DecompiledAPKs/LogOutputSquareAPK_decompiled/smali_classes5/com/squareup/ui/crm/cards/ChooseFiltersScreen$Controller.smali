.class public interface abstract Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Controller;
.super Ljava/lang/Object;
.source "ChooseFiltersScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/ChooseFiltersScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Controller"
.end annotation


# virtual methods
.method public abstract clearModifiedFilters()V
.end method

.method public abstract closeChooseFiltersScreen()V
.end method

.method public abstract commitChooseFiltersScreen()V
.end method

.method public abstract modifiedFilters()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract originalFilters()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract showCreateFilterScreen()V
.end method

.method public abstract showSaveFiltersScreen()V
.end method

.method public abstract showUpdateFilter(I)V
.end method
