.class Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "ResolveDuplicatesScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/crm/cards/ResolveDuplicatesCardView;",
        ">;"
    }
.end annotation


# instance fields
.field private final controller:Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen$Controller;

.field private final mergeProposalLoader:Lcom/squareup/crm/MergeProposalLoader;

.field private final onMergeClicked:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen$Controller;Lcom/squareup/util/Res;Lcom/squareup/crm/MergeProposalLoader;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 84
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 81
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen$Presenter;->onMergeClicked:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 85
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen$Presenter;->controller:Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen$Controller;

    .line 86
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 87
    iput-object p3, p0, Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen$Presenter;->mergeProposalLoader:Lcom/squareup/crm/MergeProposalLoader;

    return-void
.end method

.method static synthetic lambda$null$2(Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Integer;)Ljava/lang/Boolean;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 115
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-nez p0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p0

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p1

    if-le p0, p1, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$null$4(Lkotlin/Unit;Ljava/lang/Integer;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 125
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p0

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p1

    sub-int/2addr p0, p1

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public synthetic lambda$onLoad$0$ResolveDuplicatesScreen$Presenter()V
    .locals 1

    .line 101
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen$Presenter;->mergeProposalLoader:Lcom/squareup/crm/MergeProposalLoader;

    invoke-virtual {v0}, Lcom/squareup/crm/MergeProposalLoader;->refresh()V

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen$Presenter;->controller:Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen$Controller;

    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen$Controller;->closeResolveDuplicatesScreen()V

    return-void
.end method

.method public synthetic lambda$onLoad$1$ResolveDuplicatesScreen$Presenter()V
    .locals 2

    .line 107
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen$Presenter;->onMergeClicked:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$onLoad$3$ResolveDuplicatesScreen$Presenter(Lcom/squareup/ui/crm/cards/ResolveDuplicatesCardView;Lcom/squareup/marin/widgets/MarinActionBar;)Lio/reactivex/disposables/Disposable;
    .locals 3

    .line 112
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/ResolveDuplicatesCardView;->mergeProposals()Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;->isBusy()Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen$Presenter;->mergeProposalLoader:Lcom/squareup/crm/MergeProposalLoader;

    .line 113
    invoke-virtual {v1}, Lcom/squareup/crm/MergeProposalLoader;->totalDuplicateCount()Lio/reactivex/Observable;

    move-result-object v1

    .line 114
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/ResolveDuplicatesCardView;->mergeProposals()Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;->uncheckedCount()Lio/reactivex/Observable;

    move-result-object p1

    sget-object v2, Lcom/squareup/ui/crm/cards/-$$Lambda$ResolveDuplicatesScreen$Presenter$8JOKfAK2p8A-zaKteRjOHjTdRKc;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$ResolveDuplicatesScreen$Presenter$8JOKfAK2p8A-zaKteRjOHjTdRKc;

    .line 111
    invoke-static {v0, v1, p1, v2}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function3;)Lio/reactivex/Observable;

    move-result-object p1

    const/4 v0, 0x1

    .line 116
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/rx2/Rx2TransformersKt;->distinctUntilChangedWithFirstValueToSkip(Ljava/lang/Object;)Lio/reactivex/ObservableTransformer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object p1

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$SxET--V_t5eSk3B_3IRcq3lnTMA;

    invoke-direct {v0, p2}, Lcom/squareup/ui/crm/cards/-$$Lambda$SxET--V_t5eSk3B_3IRcq3lnTMA;-><init>(Lcom/squareup/marin/widgets/MarinActionBar;)V

    .line 117
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$5$ResolveDuplicatesScreen$Presenter(Lcom/squareup/ui/crm/cards/ResolveDuplicatesCardView;)Lio/reactivex/disposables/Disposable;
    .locals 3

    .line 121
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen$Presenter;->onMergeClicked:Lcom/jakewharton/rxrelay2/PublishRelay;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen$Presenter;->mergeProposalLoader:Lcom/squareup/crm/MergeProposalLoader;

    .line 123
    invoke-virtual {v1}, Lcom/squareup/crm/MergeProposalLoader;->totalDuplicateCount()Lio/reactivex/Observable;

    move-result-object v1

    .line 124
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/ResolveDuplicatesCardView;->mergeProposals()Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;->uncheckedCount()Lio/reactivex/Observable;

    move-result-object p1

    sget-object v2, Lcom/squareup/ui/crm/cards/-$$Lambda$ResolveDuplicatesScreen$Presenter$oCKsZPH3njBHg_P1qAN5JflX3nQ;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$ResolveDuplicatesScreen$Presenter$oCKsZPH3njBHg_P1qAN5JflX3nQ;

    .line 122
    invoke-virtual {v0, v1, p1, v2}, Lcom/jakewharton/rxrelay2/PublishRelay;->withLatestFrom(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function3;)Lio/reactivex/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen$Presenter;->controller:Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen$Controller;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$QjzVW86Vdz7Gdmx0501clZmKeP0;

    invoke-direct {v1, v0}, Lcom/squareup/ui/crm/cards/-$$Lambda$QjzVW86Vdz7Gdmx0501clZmKeP0;-><init>(Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen$Controller;)V

    .line 126
    invoke-virtual {p1, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    .line 91
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onEnterScope(Lmortar/MortarScope;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 4

    .line 95
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 96
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/ResolveDuplicatesCardView;

    .line 97
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/ResolveDuplicatesCardView;->actionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    .line 99
    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/crm/applet/R$string;->crm_resolve_duplicates_title:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 100
    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$ResolveDuplicatesScreen$Presenter$MQcE1TU0lqrTMfojODaGw9pFUmM;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$ResolveDuplicatesScreen$Presenter$MQcE1TU0lqrTMfojODaGw9pFUmM;-><init>(Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen$Presenter;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    .line 105
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/crmviewcustomer/R$string;->crm_merge_label:I

    .line 106
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 105
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonText(Ljava/lang/CharSequence;)V

    .line 107
    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$ResolveDuplicatesScreen$Presenter$USF0nuxNRTlp_DX3onTcXbndypM;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$ResolveDuplicatesScreen$Presenter$USF0nuxNRTlp_DX3onTcXbndypM;-><init>(Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen$Presenter;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->showPrimaryButton(Ljava/lang/Runnable;)V

    .line 110
    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$ResolveDuplicatesScreen$Presenter$b6yzeFQ_z3R4s7xND21IQxtvavY;

    invoke-direct {v1, p0, p1, v0}, Lcom/squareup/ui/crm/cards/-$$Lambda$ResolveDuplicatesScreen$Presenter$b6yzeFQ_z3R4s7xND21IQxtvavY;-><init>(Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen$Presenter;Lcom/squareup/ui/crm/cards/ResolveDuplicatesCardView;Lcom/squareup/marin/widgets/MarinActionBar;)V

    invoke-static {p1, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 120
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$ResolveDuplicatesScreen$Presenter$ivHU1kHxIvsjakHkdfCXhChTFPE;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$ResolveDuplicatesScreen$Presenter$ivHU1kHxIvsjakHkdfCXhChTFPE;-><init>(Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen$Presenter;Lcom/squareup/ui/crm/cards/ResolveDuplicatesCardView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
