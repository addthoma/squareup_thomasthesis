.class public Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingScreen$ComponentFactory;
.super Ljava/lang/Object;
.source "ReviewCustomerForMergingScreen.java"

# interfaces
.implements Lcom/squareup/ui/WithComponent$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ComponentFactory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Lmortar/MortarScope;Lcom/squareup/container/ContainerTreeKey;)Ljava/lang/Object;
    .locals 0

    .line 85
    const-class p2, Lcom/squareup/ui/crm/flow/CrmScope$SharedCustomerProfileComponent;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->componentInParent(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/flow/CrmScope$SharedCustomerProfileComponent;

    .line 86
    invoke-interface {p1}, Lcom/squareup/ui/crm/flow/CrmScope$SharedCustomerProfileComponent;->reviewCustomerForMergingScreen()Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingScreen$Component;

    move-result-object p1

    return-object p1
.end method
