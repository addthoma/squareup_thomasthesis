.class public Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2;
.super Lcom/squareup/ui/crm/flow/InUpdateCustomerScope;
.source "ChooseEnumAttributeScreenV2.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardOverSheetScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Component;,
        Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Presenter;,
        Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Runner;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 149
    sget-object v0, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ChooseEnumAttributeScreenV2$WNvJqQibagUymHcJeYUs4v-pyt4;->INSTANCE:Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ChooseEnumAttributeScreenV2$WNvJqQibagUymHcJeYUs4v-pyt4;

    .line 150
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/crm/flow/UpdateCustomerScope;)V
    .locals 0

    .line 35
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/flow/InUpdateCustomerScope;-><init>(Lcom/squareup/ui/crm/flow/UpdateCustomerScope;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2;
    .locals 1

    .line 151
    const-class v0, Lcom/squareup/ui/crm/flow/UpdateCustomerScope;

    .line 152
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/crm/flow/UpdateCustomerScope;

    .line 153
    new-instance v0, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2;-><init>(Lcom/squareup/ui/crm/flow/UpdateCustomerScope;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 145
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/crm/flow/InUpdateCustomerScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 146
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2;->updateCustomerScope:Lcom/squareup/ui/crm/flow/UpdateCustomerScope;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public screenLayout()I
    .locals 1

    .line 39
    sget v0, Lcom/squareup/crmupdatecustomer/R$layout;->crm_v2_choose_enum_attribute_view:I

    return v0
.end method
