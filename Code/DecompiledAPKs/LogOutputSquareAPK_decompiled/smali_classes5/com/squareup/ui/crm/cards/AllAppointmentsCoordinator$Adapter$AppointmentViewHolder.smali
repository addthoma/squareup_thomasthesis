.class Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Adapter$AppointmentViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
.source "AllAppointmentsCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Adapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "AppointmentViewHolder"
.end annotation


# instance fields
.field final row:Lcom/squareup/ui/crm/rows/AppointmentRow;

.field final synthetic this$1:Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Adapter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Adapter;Landroid/view/View;)V
    .locals 0

    .line 130
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Adapter$AppointmentViewHolder;->this$1:Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Adapter;

    .line 131
    invoke-direct {p0, p2}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 132
    check-cast p2, Lcom/squareup/ui/crm/rows/AppointmentRow;

    iput-object p2, p0, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Adapter$AppointmentViewHolder;->row:Lcom/squareup/ui/crm/rows/AppointmentRow;

    return-void
.end method


# virtual methods
.method public bind(I)V
    .locals 2

    .line 136
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Adapter$AppointmentViewHolder;->this$1:Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Adapter;

    iget-object v0, v0, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Adapter;->this$0:Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator;->access$100(Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel$CrmAppointmentRowViewModel;

    .line 137
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Adapter$AppointmentViewHolder;->row:Lcom/squareup/ui/crm/rows/AppointmentRow;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/crm/rows/AppointmentRow;->setViewData(Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel$CrmAppointmentRowViewModel;)V

    .line 138
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Adapter$AppointmentViewHolder;->row:Lcom/squareup/ui/crm/rows/AppointmentRow;

    new-instance v1, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Adapter$AppointmentViewHolder$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Adapter$AppointmentViewHolder$1;-><init>(Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Adapter$AppointmentViewHolder;Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel$CrmAppointmentRowViewModel;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/rows/AppointmentRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
