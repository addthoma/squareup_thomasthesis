.class public final Lcom/squareup/ui/crm/edit/EditCustomerProps;
.super Ljava/lang/Object;
.source "EditCustomerProps.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/edit/EditCustomerProps$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000f\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\tH\u00c6\u0003J1\u0010\u0017\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\tH\u00c6\u0001J\t\u0010\u0018\u001a\u00020\u0019H\u00d6\u0001J\u0013\u0010\u001a\u001a\u00020\u001b2\u0008\u0010\u001c\u001a\u0004\u0018\u00010\u001dH\u00d6\u0003J\t\u0010\u001e\u001a\u00020\u0019H\u00d6\u0001J\t\u0010\u001f\u001a\u00020 H\u00d6\u0001J\u0019\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020$2\u0006\u0010%\u001a\u00020\u0019H\u00d6\u0001R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012\u00a8\u0006&"
    }
    d2 = {
        "Lcom/squareup/ui/crm/edit/EditCustomerProps;",
        "Landroid/os/Parcelable;",
        "type",
        "Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;",
        "contactValidationType",
        "Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;",
        "contact",
        "Lcom/squareup/protos/client/rolodex/Contact;",
        "updateCustomerResultKey",
        "Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;",
        "(Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;)V",
        "getContact",
        "()Lcom/squareup/protos/client/rolodex/Contact;",
        "getContactValidationType",
        "()Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;",
        "getType",
        "()Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;",
        "getUpdateCustomerResultKey",
        "()Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "describeContents",
        "",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final contact:Lcom/squareup/protos/client/rolodex/Contact;

.field private final contactValidationType:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;

.field private final type:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;

.field private final updateCustomerResultKey:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/crm/edit/EditCustomerProps$Creator;

    invoke-direct {v0}, Lcom/squareup/ui/crm/edit/EditCustomerProps$Creator;-><init>()V

    sput-object v0, Lcom/squareup/ui/crm/edit/EditCustomerProps;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;)V
    .locals 1

    const-string/jumbo v0, "type"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "contactValidationType"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "contact"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "updateCustomerResultKey"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/crm/edit/EditCustomerProps;->type:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;

    iput-object p2, p0, Lcom/squareup/ui/crm/edit/EditCustomerProps;->contactValidationType:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;

    iput-object p3, p0, Lcom/squareup/ui/crm/edit/EditCustomerProps;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    iput-object p4, p0, Lcom/squareup/ui/crm/edit/EditCustomerProps;->updateCustomerResultKey:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/crm/edit/EditCustomerProps;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;ILjava/lang/Object;)Lcom/squareup/ui/crm/edit/EditCustomerProps;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/crm/edit/EditCustomerProps;->type:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/squareup/ui/crm/edit/EditCustomerProps;->contactValidationType:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-object p3, p0, Lcom/squareup/ui/crm/edit/EditCustomerProps;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lcom/squareup/ui/crm/edit/EditCustomerProps;->updateCustomerResultKey:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/ui/crm/edit/EditCustomerProps;->copy(Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;)Lcom/squareup/ui/crm/edit/EditCustomerProps;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/crm/edit/EditCustomerProps;->type:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;

    return-object v0
.end method

.method public final component2()Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/crm/edit/EditCustomerProps;->contactValidationType:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;

    return-object v0
.end method

.method public final component3()Lcom/squareup/protos/client/rolodex/Contact;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/crm/edit/EditCustomerProps;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    return-object v0
.end method

.method public final component4()Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/crm/edit/EditCustomerProps;->updateCustomerResultKey:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;

    return-object v0
.end method

.method public final copy(Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;)Lcom/squareup/ui/crm/edit/EditCustomerProps;
    .locals 1

    const-string/jumbo v0, "type"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "contactValidationType"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "contact"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "updateCustomerResultKey"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/crm/edit/EditCustomerProps;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/ui/crm/edit/EditCustomerProps;-><init>(Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/crm/edit/EditCustomerProps;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/crm/edit/EditCustomerProps;

    iget-object v0, p0, Lcom/squareup/ui/crm/edit/EditCustomerProps;->type:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;

    iget-object v1, p1, Lcom/squareup/ui/crm/edit/EditCustomerProps;->type:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/crm/edit/EditCustomerProps;->contactValidationType:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;

    iget-object v1, p1, Lcom/squareup/ui/crm/edit/EditCustomerProps;->contactValidationType:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/crm/edit/EditCustomerProps;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    iget-object v1, p1, Lcom/squareup/ui/crm/edit/EditCustomerProps;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/crm/edit/EditCustomerProps;->updateCustomerResultKey:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;

    iget-object p1, p1, Lcom/squareup/ui/crm/edit/EditCustomerProps;->updateCustomerResultKey:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getContact()Lcom/squareup/protos/client/rolodex/Contact;
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/squareup/ui/crm/edit/EditCustomerProps;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    return-object v0
.end method

.method public final getContactValidationType()Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;
    .locals 1

    .line 13
    iget-object v0, p0, Lcom/squareup/ui/crm/edit/EditCustomerProps;->contactValidationType:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;

    return-object v0
.end method

.method public final getType()Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;
    .locals 1

    .line 12
    iget-object v0, p0, Lcom/squareup/ui/crm/edit/EditCustomerProps;->type:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;

    return-object v0
.end method

.method public final getUpdateCustomerResultKey()Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/squareup/ui/crm/edit/EditCustomerProps;->updateCustomerResultKey:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/ui/crm/edit/EditCustomerProps;->type:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/crm/edit/EditCustomerProps;->contactValidationType:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/crm/edit/EditCustomerProps;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/crm/edit/EditCustomerProps;->updateCustomerResultKey:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "EditCustomerProps(type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/crm/edit/EditCustomerProps;->type:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", contactValidationType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/crm/edit/EditCustomerProps;->contactValidationType:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", contact="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/crm/edit/EditCustomerProps;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", updateCustomerResultKey="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/crm/edit/EditCustomerProps;->updateCustomerResultKey:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/ui/crm/edit/EditCustomerProps;->type:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;

    invoke-virtual {p2}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/ui/crm/edit/EditCustomerProps;->contactValidationType:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;

    invoke-virtual {p2}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/ui/crm/edit/EditCustomerProps;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    iget-object p2, p0, Lcom/squareup/ui/crm/edit/EditCustomerProps;->updateCustomerResultKey:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;

    invoke-virtual {p2}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
