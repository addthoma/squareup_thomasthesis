.class final Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$sendResult$2;
.super Lkotlin/jvm/internal/Lambda;
.source "ChooseCustomerScopeRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->sendResult(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lflow/History$Builder;",
        "Lflow/History$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "Lflow/History$Builder;",
        "builder",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$sendResult$2;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$sendResult$2;

    invoke-direct {v0}, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$sendResult$2;-><init>()V

    sput-object v0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$sendResult$2;->INSTANCE:Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$sendResult$2;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lflow/History$Builder;)Lflow/History$Builder;
    .locals 3

    const-string v0, "builder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Class;

    .line 334
    const-class v1, Lcom/squareup/ui/crm/ChooseCustomerScreen;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {p1, v0}, Lcom/squareup/container/Histories;->popWhile(Lflow/History$Builder;[Ljava/lang/Class;)Lflow/History$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 65
    check-cast p1, Lflow/History$Builder;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$sendResult$2;->invoke(Lflow/History$Builder;)Lflow/History$Builder;

    move-result-object p1

    return-object p1
.end method
