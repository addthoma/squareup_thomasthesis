.class public final Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;
.super Ljava/lang/Object;
.source "AddCustomerToSaleRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountStatusSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final addCardOnFileFlowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;",
            ">;"
        }
    .end annotation
.end field

.field private final adjustPointsFlowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;",
            ">;"
        }
    .end annotation
.end field

.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final appointmentLinkingHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/appointmentsapi/AppointmentLinkingHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final billHistoryFlowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/flow/BillHistoryFlow;",
            ">;"
        }
    .end annotation
.end field

.field private final cameraHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/camerahelper/CameraHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final chooseCustomerFlowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/ChooseCustomerFlow;",
            ">;"
        }
    .end annotation
.end field

.field private final contactLoaderForSearchProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexContactLoader;",
            ">;"
        }
    .end annotation
.end field

.field private final employeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final eventLoaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexEventLoader;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ClientInvoiceServiceHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final invoicesCustomerLoaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/InvoicesCustomerLoader;",
            ">;"
        }
    .end annotation
.end field

.field private final loyaltyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltyServiceHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final loyaltySettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ">;"
        }
    .end annotation
.end field

.field private final mainContainerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final manageCouponFlowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;",
            ">;"
        }
    .end annotation
.end field

.field private final mergeCustomersFlowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/flow/MergeCustomersFlow;",
            ">;"
        }
    .end annotation
.end field

.field private final onboardingDiverterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingDiverter;",
            ">;"
        }
    .end annotation
.end field

.field private final permissionGatekeeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;"
        }
    .end annotation
.end field

.field private final phoneHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/PhoneNumberHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final profileAttachmentsVisibilityProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/ProfileAttachmentsVisibility;",
            ">;"
        }
    .end annotation
.end field

.field private final recentContactLoaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexRecentContactLoader;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final retrofitQueueProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;"
        }
    .end annotation
.end field

.field private final rolodexProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final updateCustomerFlowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;",
            ">;"
        }
    .end annotation
.end field

.field private final viewCustomerConfigurationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/viewcustomerconfiguration/api/ViewCustomerConfiguration;",
            ">;"
        }
    .end annotation
.end field

.field private final x2SellerScreenRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexContactLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexRecentContactLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltyServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexEventLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/PhoneNumberHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingDiverter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/flow/BillHistoryFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/viewcustomerconfiguration/api/ViewCustomerConfiguration;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/InvoicesCustomerLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ClientInvoiceServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/flow/MergeCustomersFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/appointmentsapi/AppointmentLinkingHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/ProfileAttachmentsVisibility;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/camerahelper/CameraHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/ChooseCustomerFlow;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 135
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->contactLoaderForSearchProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 136
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->recentContactLoaderProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 137
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->rolodexProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 138
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->loyaltyProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 139
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->eventLoaderProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 140
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->featuresProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 141
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->analyticsProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 142
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 143
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->resProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 144
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->phoneHelperProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 145
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->retrofitQueueProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 146
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 147
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 148
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 149
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->onboardingDiverterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 150
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->x2SellerScreenRunnerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 151
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->billHistoryFlowProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p18

    .line 152
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->addCardOnFileFlowProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p19

    .line 153
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->manageCouponFlowProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p20

    .line 154
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->viewCustomerConfigurationProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p21

    .line 155
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->invoicesCustomerLoaderProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p22

    .line 156
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->invoiceServiceProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p23

    .line 157
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->mergeCustomersFlowProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p24

    .line 158
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->appointmentLinkingHandlerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p25

    .line 159
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->mainContainerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p26

    .line 160
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->updateCustomerFlowProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p27

    .line 161
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->adjustPointsFlowProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p28

    .line 162
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->loyaltySettingsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p29

    .line 163
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->profileAttachmentsVisibilityProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p30

    .line 164
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->cameraHelperProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p31

    .line 165
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->chooseCustomerFlowProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;
    .locals 33
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexContactLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexRecentContactLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltyServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexEventLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/PhoneNumberHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingDiverter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/flow/BillHistoryFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/viewcustomerconfiguration/api/ViewCustomerConfiguration;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/InvoicesCustomerLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ClientInvoiceServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/flow/MergeCustomersFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/appointmentsapi/AppointmentLinkingHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/ProfileAttachmentsVisibility;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/camerahelper/CameraHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/ChooseCustomerFlow;",
            ">;)",
            "Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    move-object/from16 v25, p24

    move-object/from16 v26, p25

    move-object/from16 v27, p26

    move-object/from16 v28, p27

    move-object/from16 v29, p28

    move-object/from16 v30, p29

    move-object/from16 v31, p30

    .line 202
    new-instance v32, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;

    move-object/from16 v0, v32

    invoke-direct/range {v0 .. v31}, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v32
.end method

.method public static newInstance(Lcom/squareup/crm/RolodexContactLoader;Lcom/squareup/crm/RolodexRecentContactLoader;Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/loyalty/LoyaltyServiceHelper;Lcom/squareup/crm/RolodexEventLoader;Lcom/squareup/settings/server/Features;Lcom/squareup/analytics/Analytics;Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/onboarding/OnboardingDiverter;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/ui/crm/flow/BillHistoryFlow;Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;Lcom/squareup/crm/viewcustomerconfiguration/api/ViewCustomerConfiguration;Lcom/squareup/invoices/InvoicesCustomerLoader;Lcom/squareup/invoices/ClientInvoiceServiceHelper;Lcom/squareup/ui/crm/flow/MergeCustomersFlow;Lcom/squareup/appointmentsapi/AppointmentLinkingHandler;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/ui/crm/ProfileAttachmentsVisibility;Lcom/squareup/camerahelper/CameraHelper;Lcom/squareup/ui/crm/ChooseCustomerFlow;)Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;
    .locals 33

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    move-object/from16 v25, p24

    move-object/from16 v26, p25

    move-object/from16 v27, p26

    move-object/from16 v28, p27

    move-object/from16 v29, p28

    move-object/from16 v30, p29

    move-object/from16 v31, p30

    .line 221
    new-instance v32, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;

    move-object/from16 v0, v32

    invoke-direct/range {v0 .. v31}, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;-><init>(Lcom/squareup/crm/RolodexContactLoader;Lcom/squareup/crm/RolodexRecentContactLoader;Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/loyalty/LoyaltyServiceHelper;Lcom/squareup/crm/RolodexEventLoader;Lcom/squareup/settings/server/Features;Lcom/squareup/analytics/Analytics;Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/onboarding/OnboardingDiverter;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/ui/crm/flow/BillHistoryFlow;Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;Lcom/squareup/crm/viewcustomerconfiguration/api/ViewCustomerConfiguration;Lcom/squareup/invoices/InvoicesCustomerLoader;Lcom/squareup/invoices/ClientInvoiceServiceHelper;Lcom/squareup/ui/crm/flow/MergeCustomersFlow;Lcom/squareup/appointmentsapi/AppointmentLinkingHandler;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/ui/crm/ProfileAttachmentsVisibility;Lcom/squareup/camerahelper/CameraHelper;Lcom/squareup/ui/crm/ChooseCustomerFlow;)V

    return-object v32
.end method


# virtual methods
.method public get()Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;
    .locals 33

    move-object/from16 v0, p0

    .line 170
    iget-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->contactLoaderForSearchProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/crm/RolodexContactLoader;

    iget-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->recentContactLoaderProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/crm/RolodexRecentContactLoader;

    iget-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->rolodexProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/crm/RolodexServiceHelper;

    iget-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->loyaltyProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/loyalty/LoyaltyServiceHelper;

    iget-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->eventLoaderProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/crm/RolodexEventLoader;

    iget-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/settings/server/Features;

    iget-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/analytics/Analytics;

    iget-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lflow/Flow;

    iget-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/util/Res;

    iget-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->phoneHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/text/PhoneNumberHelper;

    iget-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->retrofitQueueProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/queue/retrofit/RetrofitQueue;

    iget-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/permissions/EmployeeManagement;

    iget-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/permissions/PermissionGatekeeper;

    iget-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->onboardingDiverterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/onboarding/OnboardingDiverter;

    iget-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->x2SellerScreenRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    iget-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->billHistoryFlowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lcom/squareup/ui/crm/flow/BillHistoryFlow;

    iget-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->addCardOnFileFlowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v19, v1

    check-cast v19, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;

    iget-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->manageCouponFlowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v20, v1

    check-cast v20, Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;

    iget-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->viewCustomerConfigurationProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v21, v1

    check-cast v21, Lcom/squareup/crm/viewcustomerconfiguration/api/ViewCustomerConfiguration;

    iget-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->invoicesCustomerLoaderProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v22, v1

    check-cast v22, Lcom/squareup/invoices/InvoicesCustomerLoader;

    iget-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->invoiceServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v23, v1

    check-cast v23, Lcom/squareup/invoices/ClientInvoiceServiceHelper;

    iget-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->mergeCustomersFlowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v24, v1

    check-cast v24, Lcom/squareup/ui/crm/flow/MergeCustomersFlow;

    iget-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->appointmentLinkingHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v25, v1

    check-cast v25, Lcom/squareup/appointmentsapi/AppointmentLinkingHandler;

    iget-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->mainContainerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v26, v1

    check-cast v26, Lcom/squareup/ui/main/PosContainer;

    iget-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->updateCustomerFlowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v27, v1

    check-cast v27, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;

    iget-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->adjustPointsFlowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v28, v1

    check-cast v28, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;

    iget-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->loyaltySettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v29, v1

    check-cast v29, Lcom/squareup/loyalty/LoyaltySettings;

    iget-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->profileAttachmentsVisibilityProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v30, v1

    check-cast v30, Lcom/squareup/ui/crm/ProfileAttachmentsVisibility;

    iget-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->cameraHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v31, v1

    check-cast v31, Lcom/squareup/camerahelper/CameraHelper;

    iget-object v1, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->chooseCustomerFlowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v32, v1

    check-cast v32, Lcom/squareup/ui/crm/ChooseCustomerFlow;

    invoke-static/range {v2 .. v32}, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->newInstance(Lcom/squareup/crm/RolodexContactLoader;Lcom/squareup/crm/RolodexRecentContactLoader;Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/loyalty/LoyaltyServiceHelper;Lcom/squareup/crm/RolodexEventLoader;Lcom/squareup/settings/server/Features;Lcom/squareup/analytics/Analytics;Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/onboarding/OnboardingDiverter;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/ui/crm/flow/BillHistoryFlow;Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;Lcom/squareup/crm/viewcustomerconfiguration/api/ViewCustomerConfiguration;Lcom/squareup/invoices/InvoicesCustomerLoader;Lcom/squareup/invoices/ClientInvoiceServiceHelper;Lcom/squareup/ui/crm/flow/MergeCustomersFlow;Lcom/squareup/appointmentsapi/AppointmentLinkingHandler;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/ui/crm/ProfileAttachmentsVisibility;Lcom/squareup/camerahelper/CameraHelper;Lcom/squareup/ui/crm/ChooseCustomerFlow;)Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 35
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner_Factory;->get()Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;

    move-result-object v0

    return-object v0
.end method
