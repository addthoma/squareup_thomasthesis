.class public final Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2_Presenter_Factory;
.super Ljava/lang/Object;
.source "ChooseCustomerToSaveScreenV2_Presenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Runner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2_Presenter_Factory;->runnerProvider:Ljavax/inject/Provider;

    .line 24
    iput-object p2, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2_Presenter_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)",
            "Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2_Presenter_Factory;"
        }
    .end annotation

    .line 34
    new-instance v0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2_Presenter_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2_Presenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Runner;Lcom/squareup/util/Res;)Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Presenter;
    .locals 1

    .line 39
    new-instance v0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Presenter;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Presenter;-><init>(Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Runner;Lcom/squareup/util/Res;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Presenter;
    .locals 2

    .line 29
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2_Presenter_Factory;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Runner;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Res;

    invoke-static {v0, v1}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2_Presenter_Factory;->newInstance(Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Runner;Lcom/squareup/util/Res;)Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Presenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2_Presenter_Factory;->get()Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Presenter;

    move-result-object v0

    return-object v0
.end method
