.class public Lcom/squareup/ui/crm/rows/CustomerUnitRow$ViewData;
.super Ljava/lang/Object;
.source "CustomerUnitRow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/rows/CustomerUnitRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ViewData"
.end annotation


# instance fields
.field public final circleColor:I

.field public final contactInfo:Ljava/lang/String;

.field public final highlighted:Z

.field public final inMultiSelectMode:Z

.field public final index:I

.field public final initialCircleItalic:Z

.field public final initialCircleString:Ljava/lang/String;

.field public final multiSelected:Z

.field public final name:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;ZIZZZ)V
    .locals 0

    .line 149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 150
    iput-object p1, p0, Lcom/squareup/ui/crm/rows/CustomerUnitRow$ViewData;->name:Ljava/lang/String;

    .line 151
    iput-object p2, p0, Lcom/squareup/ui/crm/rows/CustomerUnitRow$ViewData;->contactInfo:Ljava/lang/String;

    .line 152
    iput p3, p0, Lcom/squareup/ui/crm/rows/CustomerUnitRow$ViewData;->index:I

    .line 153
    iput-object p4, p0, Lcom/squareup/ui/crm/rows/CustomerUnitRow$ViewData;->initialCircleString:Ljava/lang/String;

    .line 154
    iput-boolean p5, p0, Lcom/squareup/ui/crm/rows/CustomerUnitRow$ViewData;->initialCircleItalic:Z

    .line 155
    iput p6, p0, Lcom/squareup/ui/crm/rows/CustomerUnitRow$ViewData;->circleColor:I

    .line 156
    iput-boolean p7, p0, Lcom/squareup/ui/crm/rows/CustomerUnitRow$ViewData;->highlighted:Z

    .line 157
    iput-boolean p8, p0, Lcom/squareup/ui/crm/rows/CustomerUnitRow$ViewData;->inMultiSelectMode:Z

    .line 158
    iput-boolean p9, p0, Lcom/squareup/ui/crm/rows/CustomerUnitRow$ViewData;->multiSelected:Z

    return-void
.end method
