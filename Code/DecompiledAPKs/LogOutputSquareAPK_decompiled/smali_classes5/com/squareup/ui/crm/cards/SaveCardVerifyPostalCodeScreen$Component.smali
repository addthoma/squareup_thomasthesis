.class public interface abstract Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Component;
.super Ljava/lang/Object;
.source "SaveCardVerifyPostalCodeScreen.java"

# interfaces
.implements Lcom/squareup/ui/ErrorsBarView$Component;


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Module;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract inject(Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;)V
.end method
