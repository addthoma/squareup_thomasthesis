.class Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$HeaderViewHolder;
.super Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$AbstractViewHolder;
.source "ViewGroupsListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "HeaderViewHolder"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter;Landroid/view/ViewGroup;)V
    .locals 1

    .line 45
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$HeaderViewHolder;->this$0:Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter;

    .line 46
    sget v0, Lcom/squareup/crm/R$layout;->crm_v2_list_header_row:I

    invoke-direct {p0, p1, v0, p2}, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$AbstractViewHolder;-><init>(Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter;ILandroid/view/ViewGroup;)V

    return-void
.end method


# virtual methods
.method public bind(I)V
    .locals 2

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$HeaderViewHolder;->itemView:Landroid/view/View;

    sget v1, Lcom/squareup/crm/R$id;->crm_list_header_row_text:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    .line 51
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$HeaderViewHolder;->this$0:Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter;

    invoke-static {v1}, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter;->access$000(Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$Item;

    iget-object p1, p1, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$Item;->name:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
