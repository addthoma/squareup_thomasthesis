.class public final Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter_Factory;
.super Ljava/lang/Object;
.source "MergeProposalListPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final loaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/MergeProposalLoader;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final rolodexProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/MergeProposalLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter_Factory;->loaderProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter_Factory;->rolodexProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/MergeProposalLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            ">;)",
            "Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter_Factory;"
        }
    .end annotation

    .line 40
    new-instance v0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/crm/MergeProposalLoader;Lio/reactivex/Scheduler;Lcom/squareup/crm/RolodexServiceHelper;)Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;
    .locals 1

    .line 45
    new-instance v0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;-><init>(Lcom/squareup/crm/MergeProposalLoader;Lio/reactivex/Scheduler;Lcom/squareup/crm/RolodexServiceHelper;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;
    .locals 3

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter_Factory;->loaderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/crm/MergeProposalLoader;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/reactivex/Scheduler;

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter_Factory;->rolodexProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/crm/RolodexServiceHelper;

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter_Factory;->newInstance(Lcom/squareup/crm/MergeProposalLoader;Lio/reactivex/Scheduler;Lcom/squareup/crm/RolodexServiceHelper;)Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter_Factory;->get()Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;

    move-result-object v0

    return-object v0
.end method
