.class Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentPresenter;
.super Lmortar/ViewPresenter;
.source "SingleTextFilterContentPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentView;",
        ">;"
    }
.end annotation


# instance fields
.field private final filter:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;"
        }
    .end annotation
.end field

.field private final filterHelper:Lcom/squareup/crm/filters/SingleTextFilterHelper;


# direct methods
.method constructor <init>(Lcom/squareup/crm/filters/SingleTextFilterHelper;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 25
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 22
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentPresenter;->filter:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 26
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentPresenter;->filterHelper:Lcom/squareup/crm/filters/SingleTextFilterHelper;

    return-void
.end method

.method private rebind(Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentView;)V
    .locals 2

    .line 52
    invoke-static {p1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetachNow(Landroid/view/View;)V

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentPresenter;->filter:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/Filter;

    .line 55
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentPresenter;->filterHelper:Lcom/squareup/crm/filters/SingleTextFilterHelper;

    invoke-virtual {v1, v0}, Lcom/squareup/crm/filters/SingleTextFilterHelper;->getHint(Lcom/squareup/protos/client/rolodex/Filter;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentView;->showHint(Ljava/lang/String;)V

    .line 56
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentPresenter;->filterHelper:Lcom/squareup/crm/filters/SingleTextFilterHelper;

    invoke-virtual {v1, v0}, Lcom/squareup/crm/filters/SingleTextFilterHelper;->getText(Lcom/squareup/protos/client/rolodex/Filter;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentView;->showText(Ljava/lang/String;)V

    .line 57
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentPresenter;->filterHelper:Lcom/squareup/crm/filters/SingleTextFilterHelper;

    invoke-virtual {v1, v0}, Lcom/squareup/crm/filters/SingleTextFilterHelper;->getTextInputType(Lcom/squareup/protos/client/rolodex/Filter;)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentView;->setInputType(I)V

    .line 59
    new-instance v0, Lcom/squareup/ui/crm/cards/filters/-$$Lambda$SingleTextFilterContentPresenter$iIN9QX9fY0N5z_F-GKGrE3ObQng;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/cards/filters/-$$Lambda$SingleTextFilterContentPresenter$iIN9QX9fY0N5z_F-GKGrE3ObQng;-><init>(Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentPresenter;Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method


# virtual methods
.method filter()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;"
        }
    .end annotation

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentPresenter;->filter:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method isValid()Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentPresenter;->filter:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentPresenter;->filterHelper:Lcom/squareup/crm/filters/SingleTextFilterHelper;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/crm/cards/filters/-$$Lambda$0NKgRzEzMKjSE234pEC_AoXAtAg;

    invoke-direct {v2, v1}, Lcom/squareup/ui/crm/cards/filters/-$$Lambda$0NKgRzEzMKjSE234pEC_AoXAtAg;-><init>(Lcom/squareup/crm/filters/SingleTextFilterHelper;)V

    .line 47
    invoke-virtual {v0, v2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 48
    invoke-virtual {v0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$null$0$SingleTextFilterContentPresenter(Ljava/lang/String;)V
    .locals 3

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentPresenter;->filter:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentPresenter;->filterHelper:Lcom/squareup/crm/filters/SingleTextFilterHelper;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/rolodex/Filter;

    invoke-virtual {v1, v2, p1}, Lcom/squareup/crm/filters/SingleTextFilterHelper;->setText(Lcom/squareup/protos/client/rolodex/Filter;Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Filter;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$rebind$1$SingleTextFilterContentPresenter(Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentView;)Lrx/Subscription;
    .locals 1

    .line 60
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentView;->onTextChanged()Lrx/Observable;

    move-result-object p1

    sget-object v0, Lcom/squareup/ui/crm/cards/filters/-$$Lambda$6rauPI2WqeSTthaJGOytxTiesGI;->INSTANCE:Lcom/squareup/ui/crm/cards/filters/-$$Lambda$6rauPI2WqeSTthaJGOytxTiesGI;

    .line 61
    invoke-virtual {p1, v0}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    .line 62
    invoke-virtual {p1}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/cards/filters/-$$Lambda$SingleTextFilterContentPresenter$rlM7AYR6pW4no3JWhZRJdU55RsE;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/filters/-$$Lambda$SingleTextFilterContentPresenter$rlM7AYR6pW4no3JWhZRJdU55RsE;-><init>(Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentPresenter;)V

    .line 63
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 0

    .line 30
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 31
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentView;

    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentPresenter;->rebind(Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentView;)V

    return-void
.end method

.method setFilter(Lcom/squareup/protos/client/rolodex/Filter;)V
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentPresenter;->filter:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 36
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentPresenter;->hasView()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 37
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentView;

    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentPresenter;->rebind(Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentView;)V

    :cond_0
    return-void
.end method
