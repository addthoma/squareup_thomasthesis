.class public final enum Lcom/squareup/ui/crm/v2/MultiSelectMode;
.super Ljava/lang/Enum;
.source "MultiSelectMode.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/crm/v2/MultiSelectMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/crm/v2/MultiSelectMode;

.field public static final enum ADD_TO_ANOTHER_MANUAL_GROUP:Lcom/squareup/ui/crm/v2/MultiSelectMode;

.field public static final enum ADD_TO_MANUAL_GROUP:Lcom/squareup/ui/crm/v2/MultiSelectMode;

.field public static final enum ADD_TO_NEWLY_CREATED_GROUP:Lcom/squareup/ui/crm/v2/MultiSelectMode;

.field public static final enum BULK_DELETE:Lcom/squareup/ui/crm/v2/MultiSelectMode;

.field public static final enum MERGE:Lcom/squareup/ui/crm/v2/MultiSelectMode;

.field public static final enum NONE:Lcom/squareup/ui/crm/v2/MultiSelectMode;

.field public static final enum REMOVE_FROM_MANUAL_GROUP:Lcom/squareup/ui/crm/v2/MultiSelectMode;

.field public static final UNLIMITED:I = -0x1


# instance fields
.field public analyticsName:Ljava/lang/String;

.field public blue:Z

.field public commitLabel:I

.field public maxSelectionAmount:I

.field public minSelectionAmount:I


# direct methods
.method static constructor <clinit>()V
    .locals 15

    .line 7
    new-instance v6, Lcom/squareup/ui/crm/v2/MultiSelectMode;

    const-string v1, "NONE"

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, ""

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/crm/v2/MultiSelectMode;-><init>(Ljava/lang/String;IIZLjava/lang/String;)V

    sput-object v6, Lcom/squareup/ui/crm/v2/MultiSelectMode;->NONE:Lcom/squareup/ui/crm/v2/MultiSelectMode;

    .line 8
    new-instance v0, Lcom/squareup/ui/crm/v2/MultiSelectMode;

    sget v10, Lcom/squareup/crmviewcustomer/R$string;->crm_merge_customers_label:I

    const-string v8, "MERGE"

    const/4 v9, 0x1

    const/4 v11, 0x1

    const-string v12, "Merge Customers"

    const/4 v13, 0x2

    const/16 v14, 0xa

    move-object v7, v0

    invoke-direct/range {v7 .. v14}, Lcom/squareup/ui/crm/v2/MultiSelectMode;-><init>(Ljava/lang/String;IIZLjava/lang/String;II)V

    sput-object v0, Lcom/squareup/ui/crm/v2/MultiSelectMode;->MERGE:Lcom/squareup/ui/crm/v2/MultiSelectMode;

    .line 10
    new-instance v0, Lcom/squareup/ui/crm/v2/MultiSelectMode;

    sget v4, Lcom/squareup/crm/applet/R$string;->crm_select_customers_delete_label:I

    const-string v2, "BULK_DELETE"

    const/4 v3, 0x2

    const/4 v5, 0x0

    const-string v6, "Bulk Delete"

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/crm/v2/MultiSelectMode;-><init>(Ljava/lang/String;IIZLjava/lang/String;)V

    sput-object v0, Lcom/squareup/ui/crm/v2/MultiSelectMode;->BULK_DELETE:Lcom/squareup/ui/crm/v2/MultiSelectMode;

    .line 11
    new-instance v0, Lcom/squareup/ui/crm/v2/MultiSelectMode;

    sget v10, Lcom/squareup/crm/applet/R$string;->crm_add_to_manual_group:I

    const-string v8, "ADD_TO_MANUAL_GROUP"

    const/4 v9, 0x3

    const-string v12, "Add To Group"

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/ui/crm/v2/MultiSelectMode;-><init>(Ljava/lang/String;IIZLjava/lang/String;)V

    sput-object v0, Lcom/squareup/ui/crm/v2/MultiSelectMode;->ADD_TO_MANUAL_GROUP:Lcom/squareup/ui/crm/v2/MultiSelectMode;

    .line 12
    new-instance v0, Lcom/squareup/ui/crm/v2/MultiSelectMode;

    sget v4, Lcom/squareup/crm/applet/R$string;->crm_select_customers_add_to_group_label:I

    const-string v2, "ADD_TO_NEWLY_CREATED_GROUP"

    const/4 v3, 0x4

    const/4 v5, 0x1

    const-string v6, "Add To Group"

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/crm/v2/MultiSelectMode;-><init>(Ljava/lang/String;IIZLjava/lang/String;)V

    sput-object v0, Lcom/squareup/ui/crm/v2/MultiSelectMode;->ADD_TO_NEWLY_CREATED_GROUP:Lcom/squareup/ui/crm/v2/MultiSelectMode;

    .line 14
    new-instance v0, Lcom/squareup/ui/crm/v2/MultiSelectMode;

    sget v10, Lcom/squareup/crm/applet/R$string;->crm_add_to_manual_group_another:I

    const-string v8, "ADD_TO_ANOTHER_MANUAL_GROUP"

    const/4 v9, 0x5

    const-string v12, "Add To Group"

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/ui/crm/v2/MultiSelectMode;-><init>(Ljava/lang/String;IIZLjava/lang/String;)V

    sput-object v0, Lcom/squareup/ui/crm/v2/MultiSelectMode;->ADD_TO_ANOTHER_MANUAL_GROUP:Lcom/squareup/ui/crm/v2/MultiSelectMode;

    .line 15
    new-instance v0, Lcom/squareup/ui/crm/v2/MultiSelectMode;

    sget v4, Lcom/squareup/crm/applet/R$string;->crm_remove_from_group:I

    const-string v2, "REMOVE_FROM_MANUAL_GROUP"

    const/4 v3, 0x6

    const/4 v5, 0x0

    const-string v6, "Remove From Group"

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/crm/v2/MultiSelectMode;-><init>(Ljava/lang/String;IIZLjava/lang/String;)V

    sput-object v0, Lcom/squareup/ui/crm/v2/MultiSelectMode;->REMOVE_FROM_MANUAL_GROUP:Lcom/squareup/ui/crm/v2/MultiSelectMode;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/squareup/ui/crm/v2/MultiSelectMode;

    .line 6
    sget-object v1, Lcom/squareup/ui/crm/v2/MultiSelectMode;->NONE:Lcom/squareup/ui/crm/v2/MultiSelectMode;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/crm/v2/MultiSelectMode;->MERGE:Lcom/squareup/ui/crm/v2/MultiSelectMode;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/crm/v2/MultiSelectMode;->BULK_DELETE:Lcom/squareup/ui/crm/v2/MultiSelectMode;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/crm/v2/MultiSelectMode;->ADD_TO_MANUAL_GROUP:Lcom/squareup/ui/crm/v2/MultiSelectMode;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/crm/v2/MultiSelectMode;->ADD_TO_NEWLY_CREATED_GROUP:Lcom/squareup/ui/crm/v2/MultiSelectMode;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/crm/v2/MultiSelectMode;->ADD_TO_ANOTHER_MANUAL_GROUP:Lcom/squareup/ui/crm/v2/MultiSelectMode;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/crm/v2/MultiSelectMode;->REMOVE_FROM_MANUAL_GROUP:Lcom/squareup/ui/crm/v2/MultiSelectMode;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/ui/crm/v2/MultiSelectMode;->$VALUES:[Lcom/squareup/ui/crm/v2/MultiSelectMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIZLjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 17
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 18
    iput p3, p0, Lcom/squareup/ui/crm/v2/MultiSelectMode;->commitLabel:I

    .line 19
    iput-boolean p4, p0, Lcom/squareup/ui/crm/v2/MultiSelectMode;->blue:Z

    const/4 p1, 0x1

    .line 20
    iput p1, p0, Lcom/squareup/ui/crm/v2/MultiSelectMode;->minSelectionAmount:I

    const/4 p1, -0x1

    .line 21
    iput p1, p0, Lcom/squareup/ui/crm/v2/MultiSelectMode;->maxSelectionAmount:I

    .line 22
    iput-object p5, p0, Lcom/squareup/ui/crm/v2/MultiSelectMode;->analyticsName:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIZLjava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ",
            "Ljava/lang/String;",
            "II)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 27
    iput p3, p0, Lcom/squareup/ui/crm/v2/MultiSelectMode;->commitLabel:I

    .line 28
    iput-boolean p4, p0, Lcom/squareup/ui/crm/v2/MultiSelectMode;->blue:Z

    .line 29
    iput p6, p0, Lcom/squareup/ui/crm/v2/MultiSelectMode;->minSelectionAmount:I

    .line 30
    iput p7, p0, Lcom/squareup/ui/crm/v2/MultiSelectMode;->maxSelectionAmount:I

    .line 31
    iput-object p5, p0, Lcom/squareup/ui/crm/v2/MultiSelectMode;->analyticsName:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/crm/v2/MultiSelectMode;
    .locals 1

    .line 6
    const-class v0, Lcom/squareup/ui/crm/v2/MultiSelectMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/crm/v2/MultiSelectMode;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/crm/v2/MultiSelectMode;
    .locals 1

    .line 6
    sget-object v0, Lcom/squareup/ui/crm/v2/MultiSelectMode;->$VALUES:[Lcom/squareup/ui/crm/v2/MultiSelectMode;

    invoke-virtual {v0}, [Lcom/squareup/ui/crm/v2/MultiSelectMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/crm/v2/MultiSelectMode;

    return-object v0
.end method
