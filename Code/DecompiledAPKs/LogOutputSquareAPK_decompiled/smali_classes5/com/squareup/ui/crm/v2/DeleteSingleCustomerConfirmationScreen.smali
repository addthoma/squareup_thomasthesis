.class public Lcom/squareup/ui/crm/v2/DeleteSingleCustomerConfirmationScreen;
.super Lcom/squareup/ui/crm/flow/InCrmScope;
.source "DeleteSingleCustomerConfirmationScreen.java"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/crm/v2/DeleteSingleCustomerConfirmationScreen$Factory;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/crm/v2/DeleteSingleCustomerConfirmationScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/v2/DeleteSingleCustomerConfirmationScreen$Factory;,
        Lcom/squareup/ui/crm/v2/DeleteSingleCustomerConfirmationScreen$Runner;,
        Lcom/squareup/ui/crm/v2/DeleteSingleCustomerConfirmationScreen$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/crm/v2/DeleteSingleCustomerConfirmationScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 68
    sget-object v0, Lcom/squareup/ui/crm/v2/-$$Lambda$DeleteSingleCustomerConfirmationScreen$SSURxldHdSiVEcw5Vaa7xJQqohw;->INSTANCE:Lcom/squareup/ui/crm/v2/-$$Lambda$DeleteSingleCustomerConfirmationScreen$SSURxldHdSiVEcw5Vaa7xJQqohw;

    .line 69
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/v2/DeleteSingleCustomerConfirmationScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 0

    .line 25
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/flow/InCrmScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/crm/v2/DeleteSingleCustomerConfirmationScreen;
    .locals 1

    .line 70
    const-class v0, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/main/RegisterTreeKey;

    .line 71
    new-instance v0, Lcom/squareup/ui/crm/v2/DeleteSingleCustomerConfirmationScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/DeleteSingleCustomerConfirmationScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 64
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/crm/flow/InCrmScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/DeleteSingleCustomerConfirmationScreen;->crmPath:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
