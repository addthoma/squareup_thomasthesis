.class public final Lcom/squareup/ui/crm/cards/UpdateGroup2View_MembersInjector;
.super Ljava/lang/Object;
.source "UpdateGroup2View_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/crm/cards/UpdateGroup2View;",
        ">;"
    }
.end annotation


# instance fields
.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2View_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2View_MembersInjector;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/crm/cards/UpdateGroup2View;",
            ">;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/ui/crm/cards/UpdateGroup2View_MembersInjector;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/cards/UpdateGroup2View_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectFeatures(Lcom/squareup/ui/crm/cards/UpdateGroup2View;Lcom/squareup/settings/server/Features;)V
    .locals 0

    .line 46
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2View;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/ui/crm/cards/UpdateGroup2View;Ljava/lang/Object;)V
    .locals 0

    .line 41
    check-cast p1, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2View;->presenter:Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/crm/cards/UpdateGroup2View;)V
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2View_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/crm/cards/UpdateGroup2View_MembersInjector;->injectPresenter(Lcom/squareup/ui/crm/cards/UpdateGroup2View;Ljava/lang/Object;)V

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2View_MembersInjector;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/Features;

    invoke-static {p1, v0}, Lcom/squareup/ui/crm/cards/UpdateGroup2View_MembersInjector;->injectFeatures(Lcom/squareup/ui/crm/cards/UpdateGroup2View;Lcom/squareup/settings/server/Features;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 9
    check-cast p1, Lcom/squareup/ui/crm/cards/UpdateGroup2View;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/cards/UpdateGroup2View_MembersInjector;->injectMembers(Lcom/squareup/ui/crm/cards/UpdateGroup2View;)V

    return-void
.end method
