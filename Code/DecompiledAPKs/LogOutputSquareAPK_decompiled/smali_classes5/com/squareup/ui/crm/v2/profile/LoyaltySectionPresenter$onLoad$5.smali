.class final Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter$onLoad$5;
.super Lkotlin/jvm/internal/Lambda;
.source "LoyaltySectionPresenter.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->onLoad(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lkotlin/Unit;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0003*\u0004\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0004\u0008\u0004\u0010\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "kotlin.jvm.PlatformType",
        "invoke",
        "(Lkotlin/Unit;)V"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter$onLoad$5;->this$0:Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 42
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter$onLoad$5;->invoke(Lkotlin/Unit;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lkotlin/Unit;)V
    .locals 0

    .line 104
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter$onLoad$5;->this$0:Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;

    invoke-static {p1}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->access$getRunner$p(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;)Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->seeAllRewardTiersClicked()V

    return-void
.end method
