.class public final Lcom/squareup/ui/crm/edit/RealEditCustomerFlow;
.super Ljava/lang/Object;
.source "RealEditCustomerFlow.kt"

# interfaces
.implements Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0007\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\u0005H\u0016J:\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00132\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u00152\u0006\u0010\u0016\u001a\u00020\u0017H\u0016J\u000e\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0018H\u0016R\u001c\u0010\u0003\u001a\u0010\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00050\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/ui/crm/edit/RealEditCustomerFlow;",
        "Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;",
        "()V",
        "results",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;",
        "kotlin.jvm.PlatformType",
        "emitResult",
        "",
        "result",
        "getFirstScreen",
        "Lcom/squareup/container/ContainerTreeKey;",
        "parentKey",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "updateCustomerResultKey",
        "Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;",
        "type",
        "Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;",
        "contactValidationType",
        "Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;",
        "crmScopeType",
        "Lcom/squareup/ui/crm/flow/CrmScopeType;",
        "baseContact",
        "Lcom/squareup/protos/client/rolodex/Contact;",
        "Lio/reactivex/Observable;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final results:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    const-string v1, "PublishRelay.create<Result>()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerFlow;->results:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-void
.end method


# virtual methods
.method public emitResult(Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;)V
    .locals 1

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerFlow;->results:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public getFirstScreen(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;Lcom/squareup/ui/crm/flow/CrmScopeType;Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/container/ContainerTreeKey;
    .locals 0

    const-string p5, "parentKey"

    invoke-static {p1, p5}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo p1, "updateCustomerResultKey"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo p1, "type"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "contactValidationType"

    invoke-static {p4, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "baseContact"

    invoke-static {p6, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    new-instance p1, Lcom/squareup/ui/crm/edit/EditCustomerBootstrapScreen;

    .line 30
    new-instance p5, Lcom/squareup/ui/crm/edit/EditCustomerProps;

    invoke-direct {p5, p3, p4, p6, p2}, Lcom/squareup/ui/crm/edit/EditCustomerProps;-><init>(Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;)V

    .line 36
    move-object p2, p0

    check-cast p2, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;

    .line 29
    invoke-direct {p1, p5, p2}, Lcom/squareup/ui/crm/edit/EditCustomerBootstrapScreen;-><init>(Lcom/squareup/ui/crm/edit/EditCustomerProps;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;)V

    check-cast p1, Lcom/squareup/container/ContainerTreeKey;

    return-object p1
.end method

.method public results()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;",
            ">;"
        }
    .end annotation

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerFlow;->results:Lcom/jakewharton/rxrelay2/PublishRelay;

    check-cast v0, Lio/reactivex/Observable;

    return-object v0
.end method
