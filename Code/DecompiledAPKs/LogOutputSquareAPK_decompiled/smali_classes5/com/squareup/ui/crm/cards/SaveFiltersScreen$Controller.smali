.class public interface abstract Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Controller;
.super Ljava/lang/Object;
.source "SaveFiltersScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/SaveFiltersScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Controller"
.end annotation


# virtual methods
.method public abstract closeSaveFiltersScreen()V
.end method

.method public abstract closeSaveFiltersScreen(Lcom/squareup/protos/client/rolodex/GroupV2;)V
.end method

.method public abstract getFiltersForSaveFiltersScreen()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;"
        }
    .end annotation
.end method
