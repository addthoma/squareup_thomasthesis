.class final Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$toEditRow$addressRendering$2;
.super Lkotlin/jvm/internal/Lambda;
.source "RealEditCustomerWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;->toEditRow(Lcom/squareup/crm/model/ContactAttribute;Lcom/squareup/workflow/RenderContext;)Lkotlin/Pair;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/address/Address;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/ui/crm/edit/EditCustomerState;",
        "+",
        "Lcom/squareup/ui/crm/edit/EditCustomerOutput;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/crm/edit/EditCustomerState;",
        "Lcom/squareup/ui/crm/edit/EditCustomerOutput;",
        "newValue",
        "Lcom/squareup/address/Address;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $this_toEditRow:Lcom/squareup/crm/model/ContactAttribute;

.field final synthetic this$0:Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;Lcom/squareup/crm/model/ContactAttribute;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$toEditRow$addressRendering$2;->this$0:Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;

    iput-object p2, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$toEditRow$addressRendering$2;->$this_toEditRow:Lcom/squareup/crm/model/ContactAttribute;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/address/Address;)Lcom/squareup/workflow/WorkflowAction;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/address/Address;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/crm/edit/EditCustomerState;",
            "Lcom/squareup/ui/crm/edit/EditCustomerOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "newValue"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 364
    iget-object v0, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$toEditRow$addressRendering$2;->$this_toEditRow:Lcom/squareup/crm/model/ContactAttribute;

    move-object v1, v0

    check-cast v1, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomAddressAttribute;

    .line 365
    iget-object v0, p1, Lcom/squareup/address/Address;->country:Lcom/squareup/CountryCode;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$toEditRow$addressRendering$2;->this$0:Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;

    invoke-static {v0}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;->access$getCountryCodeProvider$p(Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    const-string v2, "countryCodeProvider.get()"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/CountryCode;

    :goto_0
    invoke-virtual {p1, v0}, Lcom/squareup/address/Address;->toGlobalAddress(Lcom/squareup/CountryCode;)Lcom/squareup/protos/common/location/GlobalAddress;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x1e

    const/4 v8, 0x0

    .line 364
    invoke-static/range {v1 .. v8}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomAddressAttribute;->copy$default(Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomAddressAttribute;Lcom/squareup/protos/common/location/GlobalAddress;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;ILjava/lang/Object;)Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomAddressAttribute;

    move-result-object p1

    .line 367
    iget-object v0, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$toEditRow$addressRendering$2;->this$0:Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;

    check-cast p1, Lcom/squareup/crm/model/ContactAttribute;

    invoke-static {v0, p1}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;->access$updateAttributeAction(Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;Lcom/squareup/crm/model/ContactAttribute;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 85
    check-cast p1, Lcom/squareup/address/Address;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$toEditRow$addressRendering$2;->invoke(Lcom/squareup/address/Address;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
