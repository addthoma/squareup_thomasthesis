.class public Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;
.super Landroid/widget/LinearLayout;
.source "ChooseCustomer2ViewV2.java"


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private createNewButton:Landroid/widget/Button;

.field device:Lcom/squareup/util/Device;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field locale:Ljava/util/Locale;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private lookup:Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;

.field private final onRecentCustomerClickListener:Landroid/view/View$OnClickListener;

.field private final onSearchCustomerClickListener:Landroid/view/View$OnClickListener;

.field phoneNumberHelper:Lcom/squareup/text/PhoneNumberHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field presenter:Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private recentContacts:Landroid/widget/LinearLayout;

.field private recentProgressBar:Landroid/widget/ProgressBar;

.field private recentTitle:Landroid/view/View;

.field res:Lcom/squareup/util/Res;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private searchContacts:Landroid/widget/LinearLayout;

.field private searchMessage:Lcom/squareup/widgets/MessageView;

.field private searchProgressBar:Landroid/widget/ProgressBar;

.field private final shortAnimTimeMs:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 58
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 59
    const-class p2, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Component;->inject(Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;)V

    .line 61
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->res:Lcom/squareup/util/Res;

    const/high16 p2, 0x10e0000

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getInteger(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->shortAnimTimeMs:I

    .line 63
    new-instance p1, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2$1;

    invoke-direct {p1, p0}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2$1;-><init>(Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;)V

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->onSearchCustomerClickListener:Landroid/view/View$OnClickListener;

    .line 69
    new-instance p1, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2$2;

    invoke-direct {p1, p0}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2$2;-><init>(Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;)V

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->onRecentCustomerClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method private bindViews()V
    .locals 2

    .line 188
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 189
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhone()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 191
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    sget v1, Lcom/squareup/crmviewcustomer/R$id;->non_stable_action_bar:I

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/ActionBarView;->setId(I)V

    .line 193
    :cond_0
    sget v0, Lcom/squareup/crmviewcustomer/R$id;->crm_customer_lookup:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->lookup:Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;

    .line 195
    sget v0, Lcom/squareup/crmviewcustomer/R$id;->crm_search_create_new_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->createNewButton:Landroid/widget/Button;

    .line 197
    sget v0, Lcom/squareup/crmviewcustomer/R$id;->crm_search_contacts:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->searchContacts:Landroid/widget/LinearLayout;

    .line 198
    sget v0, Lcom/squareup/crmviewcustomer/R$id;->crm_search_progress_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->searchProgressBar:Landroid/widget/ProgressBar;

    .line 199
    sget v0, Lcom/squareup/crmviewcustomer/R$id;->crm_search_message:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->searchMessage:Lcom/squareup/widgets/MessageView;

    .line 201
    sget v0, Lcom/squareup/crmviewcustomer/R$id;->crm_recent_title:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->recentTitle:Landroid/view/View;

    .line 202
    sget v0, Lcom/squareup/crmviewcustomer/R$id;->crm_recent_contacts:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->recentContacts:Landroid/widget/LinearLayout;

    .line 203
    sget v0, Lcom/squareup/crmviewcustomer/R$id;->crm_recent_progress_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->recentProgressBar:Landroid/widget/ProgressBar;

    return-void
.end method

.method private newCustomerRow(Lcom/squareup/protos/client/rolodex/Contact;IZ)Lcom/squareup/ui/crm/rows/CustomerUnitRow;
    .locals 9

    .line 207
    new-instance v0, Lcom/squareup/ui/crm/rows/CustomerUnitRow;

    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/crm/rows/CustomerUnitRow;-><init>(Landroid/content/Context;)V

    .line 208
    iget-object v4, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->res:Lcom/squareup/util/Res;

    iget-object v5, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->phoneNumberHelper:Lcom/squareup/text/PhoneNumberHelper;

    iget-object v6, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->locale:Ljava/util/Locale;

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v2, p1

    move v3, p2

    invoke-static/range {v2 .. v8}, Lcom/squareup/ui/crm/rows/CustomerUnitRow;->getViewDataFromContact(Lcom/squareup/protos/client/rolodex/Contact;ILcom/squareup/util/Res;Lcom/squareup/text/PhoneNumberHelper;Ljava/util/Locale;ZZ)Lcom/squareup/ui/crm/rows/CustomerUnitRow$ViewData;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/ui/crm/rows/CustomerUnitRow;->setViewData(Lcom/squareup/ui/crm/rows/CustomerUnitRow$ViewData;)V

    .line 210
    invoke-virtual {v0, p1}, Lcom/squareup/ui/crm/rows/CustomerUnitRow;->setTag(Ljava/lang/Object;)V

    if-eqz p3, :cond_0

    .line 211
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->onRecentCustomerClickListener:Landroid/view/View$OnClickListener;

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->onSearchCustomerClickListener:Landroid/view/View$OnClickListener;

    :goto_0
    invoke-virtual {v0, p1}, Lcom/squareup/ui/crm/rows/CustomerUnitRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v0
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 1

    .line 88
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 89
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->presenter:Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 93
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->presenter:Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;->dropView(Ljava/lang/Object;)V

    .line 94
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 77
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 78
    invoke-direct {p0}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->bindViews()V

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->createNewButton:Landroid/widget/Button;

    new-instance v1, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2$3;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2$3;-><init>(Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method onSearchClicked()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 106
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->lookup:Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;->onSearchClicked()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method searchTerm()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 98
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->lookup:Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;->searchTerm()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method setActionBarConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V
    .locals 1

    .line 180
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method setRecentContacts(Ljava/util/List;Z)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;Z)V"
        }
    .end annotation

    .line 136
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->recentContacts:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 139
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/rolodex/Contact;

    .line 140
    iget-object v3, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->recentContacts:Landroid/widget/LinearLayout;

    add-int/lit8 v4, v1, 0x1

    const/4 v5, 0x1

    invoke-direct {p0, v2, v1, v5}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->newCustomerRow(Lcom/squareup/protos/client/rolodex/Contact;IZ)Lcom/squareup/ui/crm/rows/CustomerUnitRow;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    move v1, v4

    goto :goto_0

    :cond_0
    if-eqz p2, :cond_1

    .line 144
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->recentContacts:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object p1

    const/4 p2, 0x4

    .line 145
    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    .line 148
    iget p2, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->shortAnimTimeMs:I

    invoke-static {p1, p2, p2}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;II)V

    :cond_1
    return-void
.end method

.method setSearchContacts(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;>;)V"
        }
    .end annotation

    .line 161
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->searchContacts:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 164
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v0, 0x0

    const/4 v1, 0x0

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 165
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/rolodex/Contact;

    .line 166
    iget-object v4, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->searchContacts:Landroid/widget/LinearLayout;

    add-int/lit8 v5, v1, 0x1

    invoke-direct {p0, v3, v1, v0}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->newCustomerRow(Lcom/squareup/protos/client/rolodex/Contact;IZ)Lcom/squareup/ui/crm/rows/CustomerUnitRow;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    move v1, v5

    goto :goto_0

    :cond_1
    return-void
.end method

.method setSearchMessage(Ljava/lang/String;)V
    .locals 1

    .line 172
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->searchMessage:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method setSearchTerm(Ljava/lang/String;)V
    .locals 1

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->lookup:Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;->setSearchTerm(Ljava/lang/String;)V

    return-void
.end method

.method showCreateNewButton(Ljava/lang/CharSequence;)V
    .locals 1

    .line 184
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->createNewButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method showRecentList(Z)V
    .locals 1

    if-eqz p1, :cond_0

    .line 127
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->recentTitle:Landroid/view/View;

    iget v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->shortAnimTimeMs:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    .line 128
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->recentContacts:Landroid/widget/LinearLayout;

    iget v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->shortAnimTimeMs:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    goto :goto_0

    .line 130
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->recentTitle:Landroid/view/View;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 131
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->recentContacts:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method showRecentProgress(Z)V
    .locals 1

    if-eqz p1, :cond_0

    .line 119
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->recentProgressBar:Landroid/widget/ProgressBar;

    iget v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->shortAnimTimeMs:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    goto :goto_0

    .line 121
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->recentProgressBar:Landroid/widget/ProgressBar;

    iget v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->shortAnimTimeMs:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->fadeOutToGone(Landroid/view/View;I)V

    :goto_0
    return-void
.end method

.method showSearchContacts(Z)V
    .locals 1

    if-eqz p1, :cond_0

    .line 154
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->searchContacts:Landroid/widget/LinearLayout;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 156
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->searchContacts:Landroid/widget/LinearLayout;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method showSearchMessage(Z)V
    .locals 1

    .line 176
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->searchMessage:Lcom/squareup/widgets/MessageView;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method showSearchProgress(Z)V
    .locals 1

    if-eqz p1, :cond_0

    .line 111
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->searchProgressBar:Landroid/widget/ProgressBar;

    iget v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->shortAnimTimeMs:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    goto :goto_0

    .line 113
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->searchProgressBar:Landroid/widget/ProgressBar;

    iget v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->shortAnimTimeMs:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->fadeOutToGone(Landroid/view/View;I)V

    :goto_0
    return-void
.end method
