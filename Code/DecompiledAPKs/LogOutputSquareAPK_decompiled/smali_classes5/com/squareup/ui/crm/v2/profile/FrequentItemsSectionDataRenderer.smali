.class public Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionDataRenderer;
.super Ljava/lang/Object;
.source "FrequentItemsSectionDataRenderer.java"


# static fields
.field public static final SHOW_VIEW_ALL_SIZE:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x3

    .line 14
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionDataRenderer;->SHOW_VIEW_ALL_SIZE:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public generateViewData(Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionView$ViewData;
    .locals 3

    .line 21
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Contact;->frequent_items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sget-object v1, Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionDataRenderer;->SHOW_VIEW_ALL_SIZE:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x0

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 22
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Contact;->frequent_items:Ljava/util/List;

    sget-object v1, Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionDataRenderer;->SHOW_VIEW_ALL_SIZE:Ljava/lang/Integer;

    .line 23
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {p1, v2, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p1

    goto :goto_1

    :cond_1
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Contact;->frequent_items:Ljava/util/List;

    .line 26
    :goto_1
    new-instance v1, Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionView$ViewData;

    invoke-direct {v1, v0, p1}, Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionView$ViewData;-><init>(ZLjava/util/List;)V

    return-object v1
.end method
