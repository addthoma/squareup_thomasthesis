.class public Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;
.super Lcom/squareup/coordinators/Coordinator;
.source "ConversationDetailCoordinatorV2.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2$Runner;
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private final billHistoryFlow:Lcom/squareup/ui/crm/flow/BillHistoryFlow;

.field private final contact:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;"
        }
    .end annotation
.end field

.field private conversationView:Lcom/squareup/ui/crm/ConversationView;

.field private final customerConversationTokenHolder:Lcom/squareup/ui/crm/applet/CustomerConversationTokenHolder;

.field private final device:Lcom/squareup/util/Device;

.field private final res:Lcom/squareup/util/Res;

.field private final rolodex:Lcom/squareup/crm/RolodexServiceHelper;

.field private final runner:Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2$Runner;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2$Runner;Lcom/squareup/ui/crm/applet/CustomerConversationTokenHolder;Lcom/squareup/util/Device;Lcom/squareup/ui/crm/flow/BillHistoryFlow;Lcom/squareup/util/Res;Lcom/squareup/crm/RolodexServiceHelper;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 54
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    .line 33
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;->contact:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 55
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;->runner:Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2$Runner;

    .line 56
    iput-object p2, p0, Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;->customerConversationTokenHolder:Lcom/squareup/ui/crm/applet/CustomerConversationTokenHolder;

    .line 57
    iput-object p3, p0, Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;->device:Lcom/squareup/util/Device;

    .line 58
    iput-object p4, p0, Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;->billHistoryFlow:Lcom/squareup/ui/crm/flow/BillHistoryFlow;

    .line 59
    iput-object p5, p0, Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;->res:Lcom/squareup/util/Res;

    .line 60
    iput-object p6, p0, Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    return-void
.end method

.method private bindViews(Landroid/view/View;)V
    .locals 1

    .line 149
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_conversation:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/ConversationView;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;->conversationView:Lcom/squareup/ui/crm/ConversationView;

    .line 150
    invoke-static {p1}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    return-void
.end method

.method static synthetic lambda$null$0(Lcom/squareup/protos/client/dialogue/Conversation;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 77
    iget-object v0, p0, Lcom/squareup/protos/client/dialogue/Conversation;->contact:Lcom/squareup/protos/client/dialogue/Contact;

    if-eqz v0, :cond_0

    iget-object p0, p0, Lcom/squareup/protos/client/dialogue/Conversation;->contact:Lcom/squareup/protos/client/dialogue/Contact;

    iget-object p0, p0, Lcom/squareup/protos/client/dialogue/Contact;->name:Ljava/lang/String;

    invoke-static {p0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$null$1(Lcom/squareup/protos/client/dialogue/Conversation;)Ljava/lang/String;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 78
    iget-object p0, p0, Lcom/squareup/protos/client/dialogue/Conversation;->contact:Lcom/squareup/protos/client/dialogue/Contact;

    iget-object p0, p0, Lcom/squareup/protos/client/dialogue/Contact;->name:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic lambda$null$10(Ljava/lang/String;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 115
    invoke-static {p0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p0

    xor-int/lit8 p0, p0, 0x1

    return p0
.end method

.method static synthetic lambda$null$12(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    return-void
.end method

.method static synthetic lambda$null$18(Ljava/lang/Boolean;Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/Boolean;
    .locals 0

    .line 139
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-nez p0, :cond_0

    if-eqz p1, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$null$9(Lcom/squareup/protos/client/dialogue/Conversation;)Lcom/squareup/util/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 111
    iget-object v0, p0, Lcom/squareup/protos/client/dialogue/Conversation;->contact:Lcom/squareup/protos/client/dialogue/Contact;

    if-eqz v0, :cond_0

    iget-object p0, p0, Lcom/squareup/protos/client/dialogue/Conversation;->contact:Lcom/squareup/protos/client/dialogue/Contact;

    iget-object p0, p0, Lcom/squareup/protos/client/dialogue/Contact;->contact_token:Ljava/lang/String;

    .line 112
    invoke-static {p0}, Lcom/squareup/util/Optional;->ofNullable(Ljava/lang/Object;)Lcom/squareup/util/Optional;

    move-result-object p0

    goto :goto_0

    .line 113
    :cond_0
    invoke-static {}, Lcom/squareup/util/Optional;->empty()Lcom/squareup/util/Optional;

    move-result-object p0

    :goto_0
    return-object p0
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 5

    .line 64
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 66
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;->bindViews(Landroid/view/View;)V

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result v0

    const-string v1, ""

    .line 69
    invoke-static {v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v1

    .line 71
    iget-object v2, p0, Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;->conversationView:Lcom/squareup/ui/crm/ConversationView;

    iget-object v3, p0, Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;->customerConversationTokenHolder:Lcom/squareup/ui/crm/applet/CustomerConversationTokenHolder;

    invoke-virtual {v3}, Lcom/squareup/ui/crm/applet/CustomerConversationTokenHolder;->getConversationToken()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/ui/crm/ConversationView;->setConversationToken(Ljava/lang/String;)V

    .line 74
    new-instance v2, Lcom/squareup/ui/crm/v2/-$$Lambda$ConversationDetailCoordinatorV2$2kba23MS74uIprP7xhq0s9CidGY;

    invoke-direct {v2, p0, v1}, Lcom/squareup/ui/crm/v2/-$$Lambda$ConversationDetailCoordinatorV2$2kba23MS74uIprP7xhq0s9CidGY;-><init>(Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;Lcom/jakewharton/rxrelay2/BehaviorRelay;)V

    invoke-static {p1, v2}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 81
    iget-object v2, p0, Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    iget-object v3, p0, Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/crm/applet/R$string;->crm_conversation_view_profile:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar;->setSecondaryButtonTextNoGlyph(Ljava/lang/CharSequence;)V

    .line 84
    new-instance v2, Lcom/squareup/ui/crm/v2/-$$Lambda$ConversationDetailCoordinatorV2$BR0lEn_PrkpB9RxvO5_kFW1XAoI;

    invoke-direct {v2, p0, v1, v0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ConversationDetailCoordinatorV2$BR0lEn_PrkpB9RxvO5_kFW1XAoI;-><init>(Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;Lcom/jakewharton/rxrelay2/BehaviorRelay;Z)V

    invoke-static {p1, v2}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;->runner:Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2$Runner;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$zAjVVgg3jzpUZtxoKP9rknG7adI;

    invoke-direct {v1, v0}, Lcom/squareup/ui/crm/v2/-$$Lambda$zAjVVgg3jzpUZtxoKP9rknG7adI;-><init>(Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2$Runner;)V

    invoke-static {p1, v1}, Lcom/squareup/workflow/ui/HandlesBack$Helper;->setBackHandler(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 98
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ConversationDetailCoordinatorV2$e0m20FtHO-BVYWebgclo-uNGWuI;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ConversationDetailCoordinatorV2$e0m20FtHO-BVYWebgclo-uNGWuI;-><init>(Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 103
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ConversationDetailCoordinatorV2$AgJ4pCgpAa3OBXqwPBsBIgZmyns;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ConversationDetailCoordinatorV2$AgJ4pCgpAa3OBXqwPBsBIgZmyns;-><init>(Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 109
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ConversationDetailCoordinatorV2$aKdg68fhE3KubtUQ7bw5eJLorbg;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ConversationDetailCoordinatorV2$aKdg68fhE3KubtUQ7bw5eJLorbg;-><init>(Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 128
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ConversationDetailCoordinatorV2$szzE6gaX0F8r5q02DDd58LZFyGg;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ConversationDetailCoordinatorV2$szzE6gaX0F8r5q02DDd58LZFyGg;-><init>(Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 135
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ConversationDetailCoordinatorV2$G3NoJ_wuSSpoZ3PJyno5dAcGfXQ;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ConversationDetailCoordinatorV2$G3NoJ_wuSSpoZ3PJyno5dAcGfXQ;-><init>(Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public detach(Landroid/view/View;)V
    .locals 0

    .line 145
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->detach(Landroid/view/View;)V

    return-void
.end method

.method public synthetic lambda$attach$14$ConversationDetailCoordinatorV2()Lio/reactivex/disposables/Disposable;
    .locals 3

    .line 110
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;->conversationView:Lcom/squareup/ui/crm/ConversationView;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/ConversationView;->conversation()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ConversationDetailCoordinatorV2$r_nUhb8gF88aHbDZlU0cMudKpBU;->INSTANCE:Lcom/squareup/ui/crm/v2/-$$Lambda$ConversationDetailCoordinatorV2$r_nUhb8gF88aHbDZlU0cMudKpBU;

    .line 111
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 114
    invoke-static {}, Lcom/squareup/util/OptionalExtensionsKt;->mapIfPresentObservable()Lio/reactivex/ObservableTransformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ConversationDetailCoordinatorV2$Lkzttsvlngt1BWQT5uSBk9LIQPk;->INSTANCE:Lcom/squareup/ui/crm/v2/-$$Lambda$ConversationDetailCoordinatorV2$Lkzttsvlngt1BWQT5uSBk9LIQPk;

    .line 115
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    .line 116
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/crm/v2/-$$Lambda$CrYuZtsGWOcaM4w57baEYcqdgyE;

    invoke-direct {v2, v1}, Lcom/squareup/ui/crm/v2/-$$Lambda$CrYuZtsGWOcaM4w57baEYcqdgyE;-><init>(Lcom/squareup/crm/RolodexServiceHelper;)V

    .line 117
    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->flatMapSingle(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ConversationDetailCoordinatorV2$OBMg62_QhJlPTtFI8PF-4ynWEu8;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ConversationDetailCoordinatorV2$OBMg62_QhJlPTtFI8PF-4ynWEu8;-><init>(Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;)V

    .line 118
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attach$17$ConversationDetailCoordinatorV2()Lrx/Subscription;
    .locals 2

    .line 129
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;->contact:Lcom/jakewharton/rxrelay/PublishRelay;

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ConversationDetailCoordinatorV2$jTW43oY4BmMs5VwsAMOHUJjLD_I;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ConversationDetailCoordinatorV2$jTW43oY4BmMs5VwsAMOHUJjLD_I;-><init>(Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;)V

    .line 130
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attach$2$ConversationDetailCoordinatorV2(Lcom/jakewharton/rxrelay2/BehaviorRelay;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 75
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;->conversationView:Lcom/squareup/ui/crm/ConversationView;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/ConversationView;->conversation()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ConversationDetailCoordinatorV2$C9p00-mVfdCB4R02ZUzm-u3YoUc;->INSTANCE:Lcom/squareup/ui/crm/v2/-$$Lambda$ConversationDetailCoordinatorV2$C9p00-mVfdCB4R02ZUzm-u3YoUc;

    .line 76
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ConversationDetailCoordinatorV2$Cdqa7Inu8LmKFAzNafXU4JAWSN0;->INSTANCE:Lcom/squareup/ui/crm/v2/-$$Lambda$ConversationDetailCoordinatorV2$Cdqa7Inu8LmKFAzNafXU4JAWSN0;

    .line 78
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 79
    invoke-virtual {v0, p1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$attach$20$ConversationDetailCoordinatorV2()Lrx/Subscription;
    .locals 3

    .line 136
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;->conversationView:Lcom/squareup/ui/crm/ConversationView;

    .line 137
    invoke-virtual {v0}, Lcom/squareup/ui/crm/ConversationView;->busy()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;->contact:Lcom/jakewharton/rxrelay/PublishRelay;

    sget-object v2, Lcom/squareup/ui/crm/v2/-$$Lambda$ConversationDetailCoordinatorV2$21pppKlVN2yHheULRGfCE7YPAAI;->INSTANCE:Lcom/squareup/ui/crm/v2/-$$Lambda$ConversationDetailCoordinatorV2$21pppKlVN2yHheULRGfCE7YPAAI;

    .line 136
    invoke-static {v0, v1, v2}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    .line 140
    invoke-virtual {v0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ConversationDetailCoordinatorV2$glbQcVhNosz5C6iTnBt1QdBOVYc;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ConversationDetailCoordinatorV2$glbQcVhNosz5C6iTnBt1QdBOVYc;-><init>(Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;)V

    .line 141
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attach$4$ConversationDetailCoordinatorV2(Lcom/jakewharton/rxrelay2/BehaviorRelay;Z)Lio/reactivex/disposables/Disposable;
    .locals 1

    .line 85
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ConversationDetailCoordinatorV2$_fiSN9hC-9z8pzNMWIYoUskPVxo;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/crm/v2/-$$Lambda$ConversationDetailCoordinatorV2$_fiSN9hC-9z8pzNMWIYoUskPVxo;-><init>(Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;Z)V

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$attach$6$ConversationDetailCoordinatorV2()Lrx/Subscription;
    .locals 2

    .line 99
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;->conversationView:Lcom/squareup/ui/crm/ConversationView;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/ConversationView;->onAddCouponClicked()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ConversationDetailCoordinatorV2$MAjUcRJQ-2co101D07iC4On0xq4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ConversationDetailCoordinatorV2$MAjUcRJQ-2co101D07iC4On0xq4;-><init>(Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;)V

    .line 100
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attach$8$ConversationDetailCoordinatorV2()Lrx/Subscription;
    .locals 2

    .line 104
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;->conversationView:Lcom/squareup/ui/crm/ConversationView;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/ConversationView;->onBillHistoryClicked()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ConversationDetailCoordinatorV2$0daXUkiihMeq8zdBbganOSh7zhM;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ConversationDetailCoordinatorV2$0daXUkiihMeq8zdBbganOSh7zhM;-><init>(Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;)V

    .line 105
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$null$11$ConversationDetailCoordinatorV2(Lcom/squareup/protos/client/rolodex/GetContactResponse;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 120
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;->contact:Lcom/jakewharton/rxrelay/PublishRelay;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/GetContactResponse;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$null$13$ConversationDetailCoordinatorV2(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 118
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ConversationDetailCoordinatorV2$KWE1holucYneT3sPr8ZR9n62aMM;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ConversationDetailCoordinatorV2$KWE1holucYneT3sPr8ZR9n62aMM;-><init>(Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;)V

    sget-object v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ConversationDetailCoordinatorV2$CUFZQOyucnm_HFDrNsSJzI1p0kk;->INSTANCE:Lcom/squareup/ui/crm/v2/-$$Lambda$ConversationDetailCoordinatorV2$CUFZQOyucnm_HFDrNsSJzI1p0kk;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$null$15$ConversationDetailCoordinatorV2(Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 1

    .line 132
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;->runner:Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2$Runner;

    invoke-interface {v0, p1}, Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2$Runner;->showContactFromConversation(Lcom/squareup/protos/client/rolodex/Contact;)V

    return-void
.end method

.method public synthetic lambda$null$16$ConversationDetailCoordinatorV2(Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 2

    .line 131
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ConversationDetailCoordinatorV2$YwuXsCeDCZ2m56-0bfLFamYNHkw;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/v2/-$$Lambda$ConversationDetailCoordinatorV2$YwuXsCeDCZ2m56-0bfLFamYNHkw;-><init>(Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;Lcom/squareup/protos/client/rolodex/Contact;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->showSecondaryButton(Ljava/lang/Runnable;)V

    return-void
.end method

.method public synthetic lambda$null$19$ConversationDetailCoordinatorV2(Ljava/lang/Boolean;)V
    .locals 1

    .line 141
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setSecondaryButtonEnabled(Z)V

    return-void
.end method

.method public synthetic lambda$null$3$ConversationDetailCoordinatorV2(ZLjava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 87
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)V

    .line 88
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    iget-object p2, p0, Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;->runner:Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2$Runner;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$zAjVVgg3jzpUZtxoKP9rknG7adI;

    invoke-direct {v0, p2}, Lcom/squareup/ui/crm/v2/-$$Lambda$zAjVVgg3jzpUZtxoKP9rknG7adI;-><init>(Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2$Runner;)V

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 90
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinActionBar;->setTitleNoUpButton(Ljava/lang/CharSequence;)V

    .line 91
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar;->hideUpButton()V

    :goto_0
    return-void
.end method

.method public synthetic lambda$null$5$ConversationDetailCoordinatorV2(Lkotlin/Unit;)V
    .locals 0

    .line 100
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;->runner:Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2$Runner;

    invoke-interface {p1}, Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2$Runner;->showAddCouponToConversation()V

    return-void
.end method

.method public synthetic lambda$null$7$ConversationDetailCoordinatorV2(Lcom/squareup/billhistory/model/BillHistory;)V
    .locals 3

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;->billHistoryFlow:Lcom/squareup/ui/crm/flow/BillHistoryFlow;

    sget-object v1, Lcom/squareup/ui/crm/applet/ConversationDetailScope;->INSTANCE:Lcom/squareup/ui/crm/applet/ConversationDetailScope;

    const/4 v2, 0x1

    .line 106
    invoke-virtual {v0, v1, v2, p1}, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->showFirstScreen(Lcom/squareup/ui/main/RegisterTreeKey;ZLcom/squareup/billhistory/model/BillHistory;)V

    return-void
.end method
