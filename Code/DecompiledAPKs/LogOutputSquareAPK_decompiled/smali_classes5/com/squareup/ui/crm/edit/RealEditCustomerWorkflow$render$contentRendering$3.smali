.class final Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$render$contentRendering$3;
.super Lkotlin/jvm/internal/Lambda;
.source "RealEditCustomerWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;->render(Lcom/squareup/ui/crm/edit/EditCustomerProps;Lcom/squareup/ui/crm/edit/EditCustomerState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context:Lcom/squareup/workflow/RenderContext;

.field final synthetic $props:Lcom/squareup/ui/crm/edit/EditCustomerProps;

.field final synthetic $state:Lcom/squareup/ui/crm/edit/EditCustomerState;

.field final synthetic this$0:Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;Lcom/squareup/workflow/RenderContext;Lcom/squareup/ui/crm/edit/EditCustomerProps;Lcom/squareup/ui/crm/edit/EditCustomerState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$render$contentRendering$3;->this$0:Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;

    iput-object p2, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$render$contentRendering$3;->$context:Lcom/squareup/workflow/RenderContext;

    iput-object p3, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$render$contentRendering$3;->$props:Lcom/squareup/ui/crm/edit/EditCustomerProps;

    iput-object p4, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$render$contentRendering$3;->$state:Lcom/squareup/ui/crm/edit/EditCustomerState;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 85
    invoke-virtual {p0}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$render$contentRendering$3;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 4

    .line 136
    iget-object v0, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$render$contentRendering$3;->$context:Lcom/squareup/workflow/RenderContext;

    invoke-interface {v0}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object v0

    .line 137
    iget-object v1, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$render$contentRendering$3;->this$0:Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;

    iget-object v2, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$render$contentRendering$3;->$props:Lcom/squareup/ui/crm/edit/EditCustomerProps;

    invoke-virtual {v2}, Lcom/squareup/ui/crm/edit/EditCustomerProps;->getContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$render$contentRendering$3;->$state:Lcom/squareup/ui/crm/edit/EditCustomerState;

    invoke-virtual {v3}, Lcom/squareup/ui/crm/edit/EditCustomerState;->getData()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/squareup/crm/util/RolodexAttributeHelper;->withContactAttributes(Lcom/squareup/protos/client/rolodex/Contact;Ljava/util/Collection;)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;->access$saveAction(Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    .line 136
    invoke-interface {v0, v1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    return-void
.end method
