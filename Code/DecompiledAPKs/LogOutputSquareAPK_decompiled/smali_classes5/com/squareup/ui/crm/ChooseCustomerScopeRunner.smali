.class public final Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;
.super Ljava/lang/Object;
.source "ChooseCustomerScopeRunner.kt"

# interfaces
.implements Lmortar/bundler/Bundler;
.implements Lcom/squareup/ui/crm/ChooseCustomerScreen$Runner;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/ui/crm/ChooseCustomerScope;
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nChooseCustomerScopeRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ChooseCustomerScopeRunner.kt\ncom/squareup/ui/crm/ChooseCustomerScopeRunner\n*L\n1#1,367:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00c4\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0008\u0007\u0018\u00002\u00020\u00012\u00020\u0002Be\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0008\u0008\u0001\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u0012\u0008\u0008\u0001\u0010\u0015\u001a\u00020\u0016\u0012\u0008\u0008\u0001\u0010\u0017\u001a\u00020\u0016\u00a2\u0006\u0002\u0010\u0018J\u0008\u0010,\u001a\u00020$H\u0016J\u0008\u0010-\u001a\u00020$H\u0002J\u0008\u0010.\u001a\u00020(H\u0016J\u0008\u0010/\u001a\u000200H\u0002J\u0018\u00101\u001a\u00020(2\u0006\u00102\u001a\u0002032\u0006\u00104\u001a\u00020$H\u0002J$\u00105\u001a\u00020(2\u0012\u00106\u001a\u000e\u0012\u0004\u0012\u000208\u0012\u0004\u0012\u000203072\u0006\u00109\u001a\u00020$H\u0002JD\u0010:\u001a>\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020  !*\n\u0012\u0004\u0012\u00020 \u0018\u00010\u001f0\u001f !*\u001e\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020  !*\n\u0012\u0004\u0012\u00020 \u0018\u00010\u001f0\u001f\u0018\u00010\u001e0\u001eH\u0016J\u0010\u0010;\u001a\u00020+2\u0006\u00102\u001a\u000203H\u0002J\u0008\u0010<\u001a\u00020+H\u0016J,\u0010=\u001a&\u0012\u000c\u0012\n !*\u0004\u0018\u00010>0> !*\u0012\u0012\u000c\u0012\n !*\u0004\u0018\u00010>0>\u0018\u00010\u001a0\u001aH\u0016J,\u0010?\u001a&\u0012\u000c\u0012\n !*\u0004\u0018\u00010&0& !*\u0012\u0012\u000c\u0012\n !*\u0004\u0018\u00010&0&\u0018\u00010\u001a0\u001aH\u0016J\u0010\u0010@\u001a\n !*\u0004\u0018\u00010+0+H\u0016J\u0008\u0010A\u001a\u00020+H\u0016J\u0010\u0010B\u001a\u00020(2\u0006\u0010C\u001a\u00020DH\u0016J\u0008\u0010E\u001a\u00020(H\u0016J\u0012\u0010F\u001a\u00020(2\u0008\u0010G\u001a\u0004\u0018\u00010HH\u0016J\u0010\u0010I\u001a\u00020(2\u0006\u0010J\u001a\u00020$H\u0016J\u0010\u0010K\u001a\u00020(2\u0006\u0010L\u001a\u00020HH\u0016J\"\u0010M\u001a\u00020(2\u0006\u00102\u001a\u0002032\u0008\u0010N\u001a\u0004\u0018\u00010O2\u0006\u00104\u001a\u00020$H\u0002J\u0010\u0010P\u001a\u00020(2\u0006\u0010Q\u001a\u00020&H\u0016J\u0010\u0010R\u001a\u00020(2\u0006\u0010S\u001a\u00020+H\u0016J\u0008\u0010T\u001a\u00020(H\u0016R\u0014\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u00160\u001aX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u001cX\u0082.\u00a2\u0006\u0002\n\u0000RJ\u0010\u001d\u001a>\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020  !*\n\u0012\u0004\u0012\u00020 \u0018\u00010\u001f0\u001f !*\u001e\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020  !*\n\u0012\u0004\u0012\u00020 \u0018\u00010\u001f0\u001f\u0018\u00010\u001e0\u001eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R2\u0010\"\u001a&\u0012\u000c\u0012\n !*\u0004\u0018\u00010$0$ !*\u0012\u0012\u000c\u0012\n !*\u0004\u0018\u00010$0$\u0018\u00010#0#X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R2\u0010%\u001a&\u0012\u000c\u0012\n !*\u0004\u0018\u00010&0& !*\u0012\u0012\u000c\u0012\n !*\u0004\u0018\u00010&0&\u0018\u00010#0#X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000RJ\u0010\'\u001a>\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020( !*\n\u0012\u0004\u0012\u00020(\u0018\u00010\u001a0\u001a !*\u001e\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020( !*\n\u0012\u0004\u0012\u00020(\u0018\u00010\u001a0\u001a\u0018\u00010#0#X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010)\u001a\u00020&X\u0082\u000e\u00a2\u0006\u0002\n\u0000R2\u0010*\u001a&\u0012\u000c\u0012\n !*\u0004\u0018\u00010+0+ !*\u0012\u0012\u000c\u0012\n !*\u0004\u0018\u00010+0+\u0018\u00010\u001e0\u001eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006U"
    }
    d2 = {
        "Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;",
        "Lmortar/bundler/Bundler;",
        "Lcom/squareup/ui/crm/ChooseCustomerScreen$Runner;",
        "flow",
        "Lflow/Flow;",
        "res",
        "Lcom/squareup/util/Res;",
        "locale",
        "Ljava/util/Locale;",
        "phoneNumberHelper",
        "Lcom/squareup/text/PhoneNumberHelper;",
        "chooseCustomerFlow",
        "Lcom/squareup/ui/crm/ChooseCustomerFlow;",
        "updateCustomerFlow",
        "Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;",
        "mainThreadEnforcer",
        "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
        "permissionGatekeeper",
        "Lcom/squareup/permissions/PermissionGatekeeper;",
        "x2SellerScreenRunner",
        "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
        "rolodexContactLoader",
        "Lcom/squareup/crm/RolodexContactLoader;",
        "recentRolodexContactLoader",
        "(Lflow/Flow;Lcom/squareup/util/Res;Ljava/util/Locale;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/ui/crm/ChooseCustomerFlow;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/crm/RolodexContactLoader;Lcom/squareup/crm/RolodexContactLoader;)V",
        "activeRolodexContactLoader",
        "Lrx/Observable;",
        "chooseCustomerScope",
        "Lcom/squareup/ui/crm/ChooseCustomerScope;",
        "customerListItems",
        "Lcom/jakewharton/rxrelay/BehaviorRelay;",
        "",
        "Lcom/squareup/ui/crm/BaseCustomerListItem;",
        "kotlin.jvm.PlatformType",
        "isNearEndOfList",
        "Lcom/jakewharton/rxrelay/PublishRelay;",
        "",
        "overrideScrollPosition",
        "",
        "requestNewPageObservables",
        "",
        "scrollPosition",
        "searchTerm",
        "",
        "actionBarGlyphIsX",
        "canCreateCustomer",
        "cancelChooseCustomer",
        "createCreateCustomerRow",
        "Lcom/squareup/ui/crm/BaseCustomerListItem$CreateCustomerItem;",
        "customerClicked",
        "contact",
        "Lcom/squareup/protos/client/rolodex/Contact;",
        "isRecentContacts",
        "generateItems",
        "results",
        "Lcom/squareup/datafetch/Rx1AbstractLoader$Results;",
        "Lcom/squareup/crm/RolodexContactLoader$Input;",
        "displayErrorItemAtEnd",
        "getCustomerListItems",
        "getHeaderValue",
        "getMortarBundleKey",
        "getScreenLoadingState",
        "Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;",
        "getScrollPosition",
        "getSearchTerm",
        "getTitle",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "onLoad",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onNearEndOfListChanged",
        "nearEndOfList",
        "onSave",
        "outState",
        "sendResult",
        "maybeCreateResult",
        "Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;",
        "setContactListScrollPosition",
        "position",
        "setSearchTerm",
        "value",
        "showCreateCustomerScreen",
        "crm-choose-customer_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private activeRolodexContactLoader:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lcom/squareup/crm/RolodexContactLoader;",
            ">;"
        }
    .end annotation
.end field

.field private final chooseCustomerFlow:Lcom/squareup/ui/crm/ChooseCustomerFlow;

.field private chooseCustomerScope:Lcom/squareup/ui/crm/ChooseCustomerScope;

.field private final customerListItems:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/util/List<",
            "Lcom/squareup/ui/crm/BaseCustomerListItem;",
            ">;>;"
        }
    .end annotation
.end field

.field private final flow:Lflow/Flow;

.field private final isNearEndOfList:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final locale:Ljava/util/Locale;

.field private final mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

.field private final overrideScrollPosition:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

.field private final phoneNumberHelper:Lcom/squareup/text/PhoneNumberHelper;

.field private final recentRolodexContactLoader:Lcom/squareup/crm/RolodexContactLoader;

.field private final requestNewPageObservables:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;>;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final rolodexContactLoader:Lcom/squareup/crm/RolodexContactLoader;

.field private scrollPosition:I

.field private searchTerm:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final updateCustomerFlow:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;

.field private final x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;


# direct methods
.method public constructor <init>(Lflow/Flow;Lcom/squareup/util/Res;Ljava/util/Locale;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/ui/crm/ChooseCustomerFlow;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/crm/RolodexContactLoader;Lcom/squareup/crm/RolodexContactLoader;)V
    .locals 1
    .param p7    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p10    # Lcom/squareup/crm/RolodexContactLoader;
        .annotation runtime Lcom/squareup/ui/crm/ChooseCustomerScope$ChooseCustomerRolodexContactLoader;
        .end annotation
    .end param
    .param p11    # Lcom/squareup/crm/RolodexContactLoader;
        .annotation runtime Lcom/squareup/ui/crm/ChooseCustomerScope$ChooseCustomerRecentRolodexContactLoader;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "flow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locale"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "phoneNumberHelper"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "chooseCustomerFlow"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "updateCustomerFlow"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainThreadEnforcer"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "permissionGatekeeper"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "x2SellerScreenRunner"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rolodexContactLoader"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "recentRolodexContactLoader"

    invoke-static {p11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->flow:Lflow/Flow;

    iput-object p2, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->res:Lcom/squareup/util/Res;

    iput-object p3, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->locale:Ljava/util/Locale;

    iput-object p4, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->phoneNumberHelper:Lcom/squareup/text/PhoneNumberHelper;

    iput-object p5, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->chooseCustomerFlow:Lcom/squareup/ui/crm/ChooseCustomerFlow;

    iput-object p6, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->updateCustomerFlow:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;

    iput-object p7, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    iput-object p8, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    iput-object p9, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    iput-object p10, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->rolodexContactLoader:Lcom/squareup/crm/RolodexContactLoader;

    iput-object p11, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->recentRolodexContactLoader:Lcom/squareup/crm/RolodexContactLoader;

    const-string p1, ""

    .line 82
    invoke-static {p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->searchTerm:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 88
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->overrideScrollPosition:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 90
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->requestNewPageObservables:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 91
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->isNearEndOfList:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 93
    new-instance p1, Lcom/squareup/ui/crm/BaseCustomerListItem$LoadingItem;

    invoke-direct {p1}, Lcom/squareup/ui/crm/BaseCustomerListItem$LoadingItem;-><init>()V

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    .line 92
    invoke-static {p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->customerListItems:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-void
.end method

.method public static final synthetic access$customerClicked(Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;Lcom/squareup/protos/client/rolodex/Contact;Z)V
    .locals 0

    .line 65
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->customerClicked(Lcom/squareup/protos/client/rolodex/Contact;Z)V

    return-void
.end method

.method public static final synthetic access$generateItems(Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;Lcom/squareup/datafetch/Rx1AbstractLoader$Results;Z)V
    .locals 0

    .line 65
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->generateItems(Lcom/squareup/datafetch/Rx1AbstractLoader$Results;Z)V

    return-void
.end method

.method public static final synthetic access$getChooseCustomerScope$p(Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;)Lcom/squareup/ui/crm/ChooseCustomerScope;
    .locals 1

    .line 65
    iget-object p0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->chooseCustomerScope:Lcom/squareup/ui/crm/ChooseCustomerScope;

    if-nez p0, :cond_0

    const-string v0, "chooseCustomerScope"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getFlow$p(Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;)Lflow/Flow;
    .locals 0

    .line 65
    iget-object p0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->flow:Lflow/Flow;

    return-object p0
.end method

.method public static final synthetic access$getMainThreadEnforcer$p(Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;)Lcom/squareup/thread/enforcer/ThreadEnforcer;
    .locals 0

    .line 65
    iget-object p0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    return-object p0
.end method

.method public static final synthetic access$getRecentRolodexContactLoader$p(Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;)Lcom/squareup/crm/RolodexContactLoader;
    .locals 0

    .line 65
    iget-object p0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->recentRolodexContactLoader:Lcom/squareup/crm/RolodexContactLoader;

    return-object p0
.end method

.method public static final synthetic access$getRolodexContactLoader$p(Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;)Lcom/squareup/crm/RolodexContactLoader;
    .locals 0

    .line 65
    iget-object p0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->rolodexContactLoader:Lcom/squareup/crm/RolodexContactLoader;

    return-object p0
.end method

.method public static final synthetic access$getSearchTerm$p(Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;)Lcom/jakewharton/rxrelay/BehaviorRelay;
    .locals 0

    .line 65
    iget-object p0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->searchTerm:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$getUpdateCustomerFlow$p(Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;)Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;
    .locals 0

    .line 65
    iget-object p0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->updateCustomerFlow:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;

    return-object p0
.end method

.method public static final synthetic access$sendResult(Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;Z)V
    .locals 0

    .line 65
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->sendResult(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;Z)V

    return-void
.end method

.method public static final synthetic access$setChooseCustomerScope$p(Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;Lcom/squareup/ui/crm/ChooseCustomerScope;)V
    .locals 0

    .line 65
    iput-object p1, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->chooseCustomerScope:Lcom/squareup/ui/crm/ChooseCustomerScope;

    return-void
.end method

.method public static final synthetic access$setSearchTerm$p(Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;Lcom/jakewharton/rxrelay/BehaviorRelay;)V
    .locals 0

    .line 65
    iput-object p1, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->searchTerm:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-void
.end method

.method private final canCreateCustomer()Z
    .locals 2

    .line 297
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->chooseCustomerScope:Lcom/squareup/ui/crm/ChooseCustomerScope;

    if-nez v0, :cond_0

    const-string v1, "chooseCustomerScope"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object v0, v0, Lcom/squareup/ui/crm/ChooseCustomerScope;->createNewCustomerValidationType:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private final createCreateCustomerRow()Lcom/squareup/ui/crm/BaseCustomerListItem$CreateCustomerItem;
    .locals 4

    .line 346
    new-instance v0, Lcom/squareup/ui/crm/BaseCustomerListItem$CreateCustomerItem;

    .line 347
    iget-object v1, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->searchTerm:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const-string v2, "searchTerm"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    const-string v3, "searchTerm.value"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 348
    iget-object v1, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/crm/R$string;->crm_create_new_customer_label:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 350
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/crmchoosecustomer/R$string;->crm_create_new_customer_label_format:I

    invoke-interface {v1, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v1}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 351
    iget-object v3, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->searchTerm:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    const-string/jumbo v3, "term"

    invoke-virtual {v1, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 352
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 353
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 355
    :goto_0
    new-instance v2, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$createCreateCustomerRow$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$createCreateCustomerRow$1;-><init>(Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    .line 346
    invoke-direct {v0, v1, v2}, Lcom/squareup/ui/crm/BaseCustomerListItem$CreateCustomerItem;-><init>(Ljava/lang/String;Lkotlin/jvm/functions/Function0;)V

    return-object v0
.end method

.method private final customerClicked(Lcom/squareup/protos/client/rolodex/Contact;Z)V
    .locals 1

    const/4 v0, 0x0

    .line 321
    invoke-direct {p0, p1, v0, p2}, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->sendResult(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;Z)V

    return-void
.end method

.method private final generateItems(Lcom/squareup/datafetch/Rx1AbstractLoader$Results;Z)V
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/datafetch/Rx1AbstractLoader$Results<",
            "Lcom/squareup/crm/RolodexContactLoader$Input;",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;Z)V"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    .line 194
    iget-object v2, v1, Lcom/squareup/datafetch/Rx1AbstractLoader$Results;->input:Ljava/lang/Object;

    check-cast v2, Lcom/squareup/crm/RolodexContactLoader$Input;

    iget-object v2, v2, Lcom/squareup/crm/RolodexContactLoader$Input;->sortType:Lcom/squareup/protos/client/rolodex/ListContactsSortType;

    sget-object v3, Lcom/squareup/protos/client/rolodex/ListContactsSortType;->CREATED_AT_DESCENDING:Lcom/squareup/protos/client/rolodex/ListContactsSortType;

    if-ne v2, v3, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 195
    :goto_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 200
    iget-object v6, v0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->searchTerm:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const-string v7, "searchTerm"

    invoke-static {v6, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v6}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v6

    const-string v8, "searchTerm.value"

    invoke-static {v6, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v6, Ljava/lang/CharSequence;

    invoke-static {v6}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-direct/range {p0 .. p0}, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->canCreateCustomer()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 202
    invoke-direct/range {p0 .. p0}, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->createCreateCustomerRow()Lcom/squareup/ui/crm/BaseCustomerListItem$CreateCustomerItem;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    if-eqz v2, :cond_2

    .line 207
    new-instance v6, Lcom/squareup/ui/crm/BaseCustomerListItem$HeaderItem;

    iget-object v9, v0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->res:Lcom/squareup/util/Res;

    sget v10, Lcom/squareup/crmchoosecustomer/R$string;->crm_recent_header_uppercase:I

    invoke-interface {v9, v10}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v6, v9}, Lcom/squareup/ui/crm/BaseCustomerListItem$HeaderItem;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 213
    :cond_2
    iget-object v6, v1, Lcom/squareup/datafetch/Rx1AbstractLoader$Results;->items:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    const-string v9, ""

    const/4 v10, 0x0

    :cond_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/List;

    .line 214
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_3

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/squareup/protos/client/rolodex/Contact;

    if-nez v2, :cond_4

    const-string v13, "contact"

    .line 216
    invoke-static {v12, v13}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v12}, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->getHeaderValue(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;

    move-result-object v13

    .line 217
    invoke-virtual {v9, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_4

    .line 219
    new-instance v9, Lcom/squareup/ui/crm/BaseCustomerListItem$HeaderItem;

    invoke-direct {v9, v13}, Lcom/squareup/ui/crm/BaseCustomerListItem$HeaderItem;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v9, v13

    :cond_4
    add-int/lit8 v10, v10, 0x1

    .line 226
    new-instance v15, Lcom/squareup/ui/crm/BaseCustomerListItem$ContactItem;

    .line 228
    iget-object v14, v0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->res:Lcom/squareup/util/Res;

    iget-object v13, v0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->phoneNumberHelper:Lcom/squareup/text/PhoneNumberHelper;

    iget-object v5, v0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->locale:Ljava/util/Locale;

    const/16 v18, 0x0

    const/16 v19, 0x0

    move-object/from16 v16, v13

    move-object v13, v12

    move-object/from16 v17, v14

    move v14, v10

    move-object v4, v15

    move-object/from16 v15, v17

    move-object/from16 v17, v5

    .line 227
    invoke-static/range {v13 .. v19}, Lcom/squareup/ui/crm/rows/CustomerUnitRow;->getViewDataFromContact(Lcom/squareup/protos/client/rolodex/Contact;ILcom/squareup/util/Res;Lcom/squareup/text/PhoneNumberHelper;Ljava/util/Locale;ZZ)Lcom/squareup/ui/crm/rows/CustomerUnitRow$ViewData;

    move-result-object v5

    const-string v13, "getViewDataFromContact(\n\u2026, false\n                )"

    invoke-static {v5, v13}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 230
    new-instance v13, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$generateItems$1;

    invoke-direct {v13, v0, v12, v2}, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$generateItems$1;-><init>(Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;Lcom/squareup/protos/client/rolodex/Contact;Z)V

    check-cast v13, Lkotlin/jvm/functions/Function0;

    .line 226
    invoke-direct {v4, v5, v13}, Lcom/squareup/ui/crm/BaseCustomerListItem$ContactItem;-><init>(Lcom/squareup/ui/crm/rows/CustomerUnitRow$ViewData;Lkotlin/jvm/functions/Function0;)V

    .line 225
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 236
    :cond_5
    iget-boolean v4, v1, Lcom/squareup/datafetch/Rx1AbstractLoader$Results;->hasMore:Z

    if-eqz v4, :cond_7

    if-nez v2, :cond_7

    if-eqz p2, :cond_6

    .line 240
    new-instance v2, Lcom/squareup/ui/crm/BaseCustomerListItem$ErrorItem;

    invoke-direct {v2}, Lcom/squareup/ui/crm/BaseCustomerListItem$ErrorItem;-><init>()V

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 242
    :cond_6
    new-instance v2, Lcom/squareup/ui/crm/BaseCustomerListItem$LoadingItem;

    invoke-direct {v2}, Lcom/squareup/ui/crm/BaseCustomerListItem$LoadingItem;-><init>()V

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 248
    :goto_2
    iget-object v2, v0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->requestNewPageObservables:Lcom/jakewharton/rxrelay/PublishRelay;

    iget-object v4, v0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->isNearEndOfList:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 249
    invoke-virtual {v4}, Lcom/jakewharton/rxrelay/PublishRelay;->distinctUntilChanged()Lrx/Observable;

    move-result-object v4

    .line 250
    sget-object v5, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$generateItems$2;->INSTANCE:Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$generateItems$2;

    check-cast v5, Lrx/functions/Func1;

    invoke-virtual {v4, v5}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v4

    .line 251
    sget-object v5, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$generateItems$3;->INSTANCE:Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$generateItems$3;

    check-cast v5, Lrx/functions/Func1;

    invoke-virtual {v4, v5}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v4

    const/4 v5, 0x1

    .line 252
    invoke-virtual {v4, v5}, Lrx/Observable;->take(I)Lrx/Observable;

    move-result-object v4

    .line 248
    invoke-virtual {v2, v4}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    .line 255
    :cond_7
    iget-object v2, v0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->searchTerm:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-static {v2, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Ljava/lang/CharSequence;

    invoke-static {v2}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_8

    invoke-direct/range {p0 .. p0}, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->canCreateCustomer()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 256
    invoke-direct/range {p0 .. p0}, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->createCreateCustomerRow()Lcom/squareup/ui/crm/BaseCustomerListItem$CreateCustomerItem;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 259
    :cond_8
    iget-object v2, v0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->customerListItems:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v2, v3}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 261
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/datafetch/Rx1AbstractLoader$Results;->isFirstPage()Z

    move-result v1

    if-eqz v1, :cond_9

    const/4 v1, 0x0

    .line 263
    iput v1, v0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->scrollPosition:I

    .line 264
    iget-object v2, v0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->overrideScrollPosition:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    :cond_9
    return-void
.end method

.method private final getHeaderValue(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;
    .locals 3

    .line 359
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->locale:Ljava/util/Locale;

    invoke-static {p1, v0}, Lcom/squareup/ui/crm/rows/CustomerUnitRow;->getInitials(Lcom/squareup/protos/client/rolodex/Contact;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "initials"

    .line 360
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    check-cast v0, Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 361
    iget-object p1, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/crm/R$string;->crm_contact_default_display_header_label:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 363
    :cond_1
    invoke-virtual {p1, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    const-string v0, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_1
    return-object p1
.end method

.method private final sendResult(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;Z)V
    .locals 9

    .line 328
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->chooseCustomerFlow:Lcom/squareup/ui/crm/ChooseCustomerFlow;

    .line 329
    new-instance v8, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;

    .line 330
    iget-object v1, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->chooseCustomerScope:Lcom/squareup/ui/crm/ChooseCustomerScope;

    const-string v2, "chooseCustomerScope"

    if-nez v1, :cond_0

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object v3, v1, Lcom/squareup/ui/crm/ChooseCustomerScope;->chooseCustomerResultKey:Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;

    const-string v1, "chooseCustomerScope.chooseCustomerResultKey"

    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 333
    new-instance v1, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$sendResult$1;

    invoke-direct {v1, p2}, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$sendResult$1;-><init>(Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;)V

    move-object v5, v1

    check-cast v5, Lkotlin/jvm/functions/Function1;

    .line 334
    sget-object p2, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$sendResult$2;->INSTANCE:Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$sendResult$2;

    move-object v6, p2

    check-cast v6, Lkotlin/jvm/functions/Function1;

    .line 335
    iget-object p2, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->chooseCustomerScope:Lcom/squareup/ui/crm/ChooseCustomerScope;

    if-nez p2, :cond_1

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    move-object v7, p2

    check-cast v7, Lcom/squareup/ui/main/RegisterTreeKey;

    move-object v1, v8

    move-object v2, v3

    move-object v3, p1

    move v4, p3

    .line 329
    invoke-direct/range {v1 .. v7}, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;-><init>(Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;Lcom/squareup/protos/client/rolodex/Contact;ZLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lcom/squareup/ui/main/RegisterTreeKey;)V

    check-cast v8, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result;

    .line 328
    invoke-virtual {v0, v8}, Lcom/squareup/ui/crm/ChooseCustomerFlow;->sendResult$crm_choose_customer_release(Lcom/squareup/ui/crm/ChooseCustomerFlow$Result;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic actionBarGlyphIsX()Ljava/lang/Boolean;
    .locals 1

    .line 65
    invoke-virtual {p0}, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->actionBarGlyphIsX()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public actionBarGlyphIsX()Z
    .locals 2

    .line 270
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->chooseCustomerScope:Lcom/squareup/ui/crm/ChooseCustomerScope;

    if-nez v0, :cond_0

    const-string v1, "chooseCustomerScope"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-boolean v0, v0, Lcom/squareup/ui/crm/ChooseCustomerScope;->isFirstCardScreen:Z

    return v0
.end method

.method public cancelChooseCustomer()V
    .locals 4

    .line 342
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/crm/ChooseCustomerScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    .line 343
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->chooseCustomerFlow:Lcom/squareup/ui/crm/ChooseCustomerFlow;

    new-instance v1, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$Cancelled;

    iget-object v2, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->chooseCustomerScope:Lcom/squareup/ui/crm/ChooseCustomerScope;

    if-nez v2, :cond_0

    const-string v3, "chooseCustomerScope"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object v2, v2, Lcom/squareup/ui/crm/ChooseCustomerScope;->chooseCustomerResultKey:Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;

    const-string v3, "chooseCustomerScope.chooseCustomerResultKey"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$Cancelled;-><init>(Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;)V

    check-cast v1, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/ChooseCustomerFlow;->sendResult$crm_choose_customer_release(Lcom/squareup/ui/crm/ChooseCustomerFlow$Result;)V

    return-void
.end method

.method public getCustomerListItems()Lcom/jakewharton/rxrelay/BehaviorRelay;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/util/List<",
            "Lcom/squareup/ui/crm/BaseCustomerListItem;",
            ">;>;"
        }
    .end annotation

    .line 282
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->customerListItems:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object v0
.end method

.method public bridge synthetic getCustomerListItems()Lrx/Observable;
    .locals 1

    .line 65
    invoke-virtual {p0}, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->getCustomerListItems()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    check-cast v0, Lrx/Observable;

    return-object v0
.end method

.method public getMortarBundleKey()Ljava/lang/String;
    .locals 2

    .line 98
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "javaClass.name"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public getScreenLoadingState()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;",
            ">;"
        }
    .end annotation

    .line 285
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->activeRolodexContactLoader:Lrx/Observable;

    if-nez v0, :cond_0

    const-string v1, "activeRolodexContactLoader"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget-object v1, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$getScreenLoadingState$1;->INSTANCE:Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$getScreenLoadingState$1;

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lrx/Observable;->switchMap(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getScrollPosition()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 295
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->overrideScrollPosition:Lcom/jakewharton/rxrelay/PublishRelay;

    iget v1, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->scrollPosition:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->startWith(Ljava/lang/Object;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getSearchTerm()Ljava/lang/String;
    .locals 2

    .line 272
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->searchTerm:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const-string v1, "searchTerm"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 3

    .line 268
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->res:Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->chooseCustomerScope:Lcom/squareup/ui/crm/ChooseCustomerScope;

    if-nez v1, :cond_0

    const-string v2, "chooseCustomerScope"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget v1, v1, Lcom/squareup/ui/crm/ChooseCustomerScope;->titleResId:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 4

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    const-string v1, "RegisterTreeKey.get(scope)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/ui/crm/ChooseCustomerScope;

    iput-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->chooseCustomerScope:Lcom/squareup/ui/crm/ChooseCustomerScope;

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->rolodexContactLoader:Lcom/squareup/crm/RolodexContactLoader;

    check-cast v0, Lmortar/Scoped;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 107
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->chooseCustomerScope:Lcom/squareup/ui/crm/ChooseCustomerScope;

    const-string v1, "chooseCustomerScope"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-boolean v0, v0, Lcom/squareup/ui/crm/ChooseCustomerScope;->sortByRecentContacts:Z

    if-eqz v0, :cond_1

    .line 108
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->recentRolodexContactLoader:Lcom/squareup/crm/RolodexContactLoader;

    check-cast v0, Lmortar/Scoped;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 109
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->recentRolodexContactLoader:Lcom/squareup/crm/RolodexContactLoader;

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/crm/RolodexContactLoader;->setDefaultPageSize(Ljava/lang/Integer;)V

    .line 110
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->recentRolodexContactLoader:Lcom/squareup/crm/RolodexContactLoader;

    sget-object v2, Lcom/squareup/protos/client/rolodex/ListContactsSortType;->CREATED_AT_DESCENDING:Lcom/squareup/protos/client/rolodex/ListContactsSortType;

    invoke-virtual {v0, v2}, Lcom/squareup/crm/RolodexContactLoader;->setSortType(Lcom/squareup/protos/client/rolodex/ListContactsSortType;)V

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->rolodexContactLoader:Lcom/squareup/crm/RolodexContactLoader;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/squareup/crm/RolodexContactLoader;->setRestrictToSearchingOnly(Z)V

    .line 115
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->enteringSaveCustomer()Z

    .line 118
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->rolodexContactLoader:Lcom/squareup/crm/RolodexContactLoader;

    sget-object v2, Lcom/squareup/protos/client/rolodex/ListContactsSortType;->DISPLAY_NAME_ASCENDING:Lcom/squareup/protos/client/rolodex/ListContactsSortType;

    invoke-virtual {v0, v2}, Lcom/squareup/crm/RolodexContactLoader;->setSortType(Lcom/squareup/protos/client/rolodex/ListContactsSortType;)V

    .line 119
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->rolodexContactLoader:Lcom/squareup/crm/RolodexContactLoader;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Lcom/squareup/crm/RolodexContactLoader;->setSearchDelayMs(J)V

    .line 121
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->requestNewPageObservables:Lcom/jakewharton/rxrelay/PublishRelay;

    check-cast v0, Lrx/Observable;

    invoke-static {v0}, Lrx/Observable;->switchOnNext(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    const-string v2, "switchOnNext<Unit>(requestNewPageObservables)"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 122
    new-instance v2, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$onEnterScope$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$onEnterScope$1;-><init>(Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v2}, Lcom/squareup/util/ObservablesKt;->subscribeWith(Lrx/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lrx/Subscription;

    .line 127
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->chooseCustomerScope:Lcom/squareup/ui/crm/ChooseCustomerScope;

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    iget-boolean v0, v0, Lcom/squareup/ui/crm/ChooseCustomerScope;->sortByRecentContacts:Z

    if-eqz v0, :cond_3

    .line 129
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->searchTerm:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 130
    new-instance v1, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$onEnterScope$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$onEnterScope$2;-><init>(Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;)V

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 131
    new-instance v1, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$onEnterScope$3;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$onEnterScope$3;-><init>(Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;)V

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lrx/Observable;->switchMap(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    const-string v1, "searchTerm\n          .ma\u2026            )\n          }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 142
    :cond_3
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->rolodexContactLoader:Lcom/squareup/crm/RolodexContactLoader;

    invoke-static {v0}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object v0

    const-string v1, "Observable.just(rolodexContactLoader)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 127
    :goto_0
    iput-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->activeRolodexContactLoader:Lrx/Observable;

    .line 145
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->activeRolodexContactLoader:Lrx/Observable;

    if-nez v0, :cond_4

    const-string v1, "activeRolodexContactLoader"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    sget-object v1, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$onEnterScope$4;->INSTANCE:Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$onEnterScope$4;

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lrx/Observable;->switchMap(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    const-string v1, "activeRolodexContactLoad\u2026          toPair())\n    }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 156
    new-instance v1, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$onEnterScope$5;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$onEnterScope$5;-><init>(Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/ObservablesKt;->subscribeWith(Lrx/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lrx/Subscription;

    .line 161
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->updateCustomerFlow:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;

    invoke-interface {v0}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;->results()Lio/reactivex/Observable;

    move-result-object v0

    .line 162
    sget-object v1, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$onEnterScope$6;->INSTANCE:Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$onEnterScope$6;

    check-cast v1, Lio/reactivex/functions/Predicate;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    .line 163
    sget-object v1, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$onEnterScope$7;->INSTANCE:Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$onEnterScope$7;

    check-cast v1, Lio/reactivex/functions/Predicate;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    const-string/jumbo v1, "updateCustomerFlow.resul\u2026ter { it.type == CREATE }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 164
    new-instance v1, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$onEnterScope$8;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$onEnterScope$8;-><init>(Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    .line 180
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->searchTerm:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const-string v1, "searchTerm"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    const-string v0, "scrollPosition"

    .line 181
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->scrollPosition:I

    .line 182
    iget-object p1, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->overrideScrollPosition:Lcom/jakewharton/rxrelay/PublishRelay;

    iget v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->scrollPosition:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic onNearEndOfListChanged(Ljava/lang/Boolean;)V
    .locals 0

    .line 65
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->onNearEndOfListChanged(Z)V

    return-void
.end method

.method public onNearEndOfListChanged(Z)V
    .locals 1

    .line 288
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->isNearEndOfList:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "outState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 186
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->searchTerm:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const-string v1, "searchTerm"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    iget v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->scrollPosition:I

    const-string v1, "scrollPosition"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method

.method public setContactListScrollPosition(I)V
    .locals 0

    .line 292
    iput p1, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->scrollPosition:I

    return-void
.end method

.method public setSearchTerm(Ljava/lang/String;)V
    .locals 4

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 277
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->searchTerm:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const-string v1, "searchTerm"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "searchTerm.value"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->chooseCustomerScope:Lcom/squareup/ui/crm/ChooseCustomerScope;

    if-nez v0, :cond_0

    const-string v2, "chooseCustomerScope"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-boolean v0, v0, Lcom/squareup/ui/crm/ChooseCustomerScope;->sortByRecentContacts:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 278
    :goto_0
    iget-object v2, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->rolodexContactLoader:Lcom/squareup/crm/RolodexContactLoader;

    new-instance v3, Lcom/squareup/crm/RolodexContactLoader$SearchTerm;

    xor-int/2addr v0, v1

    invoke-direct {v3, p1, v0}, Lcom/squareup/crm/RolodexContactLoader$SearchTerm;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v2, v3}, Lcom/squareup/crm/RolodexContactLoader;->setSearchTerm(Lcom/squareup/crm/RolodexContactLoader$SearchTerm;)V

    .line 279
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->searchTerm:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public showCreateCustomerScreen()V
    .locals 3

    .line 300
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    .line 301
    sget-object v1, Lcom/squareup/permissions/Permission;->MANAGE_CUSTOMERS:Lcom/squareup/permissions/Permission;

    new-instance v2, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$showCreateCustomerScreen$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$showCreateCustomerScreen$1;-><init>(Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;)V

    check-cast v2, Lcom/squareup/permissions/PermissionGatekeeper$When;

    .line 300
    invoke-virtual {v0, v1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    return-void
.end method
