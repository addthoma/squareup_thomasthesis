.class public Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;
.super Ljava/lang/Object;
.source "AdjustPointsFlow.java"

# interfaces
.implements Lmortar/bundler/Bundler;
.implements Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen$Runner;
.implements Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$Runner;


# instance fields
.field private final INACTIVE_SEED:Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;

.field private final adjustPointsLoadingState:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$AdjustPointsLoadingState;",
            ">;"
        }
    .end annotation
.end field

.field private final adjustReason:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason;",
            ">;"
        }
    .end annotation
.end field

.field private final adjustmentAmount:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private adjustmentType:Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen$Runner$AdjustmentType;

.field private crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final flow:Lflow/Flow;

.field private final loyaltyService:Lcom/squareup/loyalty/LoyaltyServiceHelper;

.field private final result:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final saveAdjustmentAmount:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private seed:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lflow/Flow;Lcom/squareup/loyalty/LoyaltyServiceHelper;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    new-instance v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse$Builder;-><init>()V

    .line 46
    invoke-virtual {v0}, Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse$Builder;->build()Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;->INACTIVE_SEED:Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;->INACTIVE_SEED:Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;

    .line 48
    invoke-static {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;->seed:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 50
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;->result:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 53
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;->adjustmentAmount:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 54
    sget-object v0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$AdjustPointsLoadingState;->NOT_SAVED_YET:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$AdjustPointsLoadingState;

    .line 55
    invoke-static {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;->adjustPointsLoadingState:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 58
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;->saveAdjustmentAmount:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 60
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;->adjustReason:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 66
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;->flow:Lflow/Flow;

    .line 67
    iput-object p2, p0, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;->loyaltyService:Lcom/squareup/loyalty/LoyaltyServiceHelper;

    .line 68
    iput-object p3, p0, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method private complete()V
    .locals 2

    const/4 v0, 0x0

    .line 123
    iput-object v0, p0, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;->adjustmentType:Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen$Runner$AdjustmentType;

    .line 124
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;->adjustmentAmount:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 125
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;->adjustPointsLoadingState:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v1, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$AdjustPointsLoadingState;->NOT_SAVED_YET:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$AdjustPointsLoadingState;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 126
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;->seed:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;->INACTIVE_SEED:Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public adjustPointsLoadingState()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$AdjustPointsLoadingState;",
            ">;"
        }
    .end annotation

    .line 156
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;->adjustPointsLoadingState:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object v0
.end method

.method public canGoNegative()Z
    .locals 2

    .line 205
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_CAN_ADJUST_POINTS_NEGATIVE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method public closeAdjustPointsScreen()V
    .locals 2

    .line 183
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;->adjustmentAmount:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 185
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public closeViewLoyaltyBalanceScreen()V
    .locals 2

    .line 142
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    .line 143
    invoke-direct {p0}, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;->complete()V

    return-void
.end method

.method public currentAdjustment()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 164
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;->adjustmentAmount:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v1, Lcom/squareup/ui/crm/v2/flow/-$$Lambda$jF9L_j5KQhV90V2aHbjVb602odQ;->INSTANCE:Lcom/squareup/ui/crm/v2/flow/-$$Lambda$jF9L_j5KQhV90V2aHbjVb602odQ;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public enterAdjustment(I)V
    .locals 2

    .line 168
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;->getAdjustmentType()Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen$Runner$AdjustmentType;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen$Runner$AdjustmentType;->ADD_POINTS:Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen$Runner$AdjustmentType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, -0x1

    :goto_0
    mul-int p1, p1, v0

    .line 171
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;->canGoNegative()Z

    move-result v0

    if-nez v0, :cond_1

    .line 173
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;->getLoyaltyStatusPoints()I

    move-result v0

    add-int v1, v0, p1

    if-gez v1, :cond_1

    neg-int p1, v0

    .line 178
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;->adjustmentAmount:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public getAdjustmentType()Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen$Runner$AdjustmentType;
    .locals 1

    .line 152
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;->adjustmentType:Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen$Runner$AdjustmentType;

    return-object v0
.end method

.method public getLoyaltyStatusPoints()I
    .locals 1

    .line 138
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;->seed:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;

    invoke-static {v0}, Lcom/squareup/loyalty/LoyaltyServiceHelper;->getPointsFromGetLoyaltyStatus(Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getMortarBundleKey()Ljava/lang/String;
    .locals 1

    .line 108
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public goToAdjustPointsScreen(Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen$Runner$AdjustmentType;)V
    .locals 2

    .line 147
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;->adjustmentType:Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen$Runner$AdjustmentType;

    .line 148
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    invoke-direct {v0, v1}, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$null$0$AdjustPointsFlow()V
    .locals 2

    .line 91
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;->adjustPointsLoadingState:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v1, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$AdjustPointsLoadingState;->LOADING:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$AdjustPointsLoadingState;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$null$2$AdjustPointsFlow(Lcom/squareup/protos/client/loyalty/AdjustPunchesResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 97
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;->flow:Lflow/Flow;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Class;

    const-class v1, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-class v1, Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {p1, v0}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    .line 98
    invoke-direct {p0}, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;->complete()V

    .line 99
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;->result:Lcom/jakewharton/rxrelay/PublishRelay;

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$null$3$AdjustPointsFlow(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 102
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;->adjustPointsLoadingState:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v0, Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$AdjustPointsLoadingState;->ERROR:Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$AdjustPointsLoadingState;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$onEnterScope$1$AdjustPointsFlow(Lkotlin/Triple;)Lrx/Observable;
    .locals 5

    .line 79
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;->getLoyaltyStatusPoints()I

    move-result v0

    .line 80
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;->adjustmentAmount:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/2addr v1, v0

    .line 81
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;->canGoNegative()Z

    move-result v2

    const/4 v3, 0x0

    if-nez v2, :cond_1

    if-ltz v1, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    const-string v4, "Balance cannot go negative!"

    .line 82
    invoke-static {v2, v4}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 85
    :cond_1
    iget-object v2, p0, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;->loyaltyService:Lcom/squareup/loyalty/LoyaltyServiceHelper;

    .line 86
    invoke-virtual {p1}, Lkotlin/Triple;->getSecond()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;

    iget-object v4, v4, Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;->loyalty_status:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId;

    iget-object v3, v3, Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId;->customer_identifier:Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;

    .line 89
    invoke-virtual {p1}, Lkotlin/Triple;->getThird()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason;

    .line 85
    invoke-virtual {v2, v3, v0, v1, p1}, Lcom/squareup/loyalty/LoyaltyServiceHelper;->adjustPunches(Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;IILcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason;)Lio/reactivex/Single;

    move-result-object p1

    invoke-static {p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Single(Lio/reactivex/SingleSource;)Lrx/Single;

    move-result-object p1

    .line 90
    invoke-virtual {p1}, Lrx/Single;->toObservable()Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/v2/flow/-$$Lambda$AdjustPointsFlow$wB7Ec849pLD8oSy6j81Vk7C90Ho;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/flow/-$$Lambda$AdjustPointsFlow$wB7Ec849pLD8oSy6j81Vk7C90Ho;-><init>(Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;)V

    .line 91
    invoke-virtual {p1, v0}, Lrx/Observable;->doOnSubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onEnterScope$4$AdjustPointsFlow(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2

    .line 95
    new-instance v0, Lcom/squareup/ui/crm/v2/flow/-$$Lambda$AdjustPointsFlow$AJ8RNJfDyr3tAIDWgN33lkHvJVw;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/flow/-$$Lambda$AdjustPointsFlow$AJ8RNJfDyr3tAIDWgN33lkHvJVw;-><init>(Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;)V

    new-instance v1, Lcom/squareup/ui/crm/v2/flow/-$$Lambda$AdjustPointsFlow$SYh4VgkRPGVYM8KOEJ4TFpCgNDM;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/flow/-$$Lambda$AdjustPointsFlow$SYh4VgkRPGVYM8KOEJ4TFpCgNDM;-><init>(Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;)V

    invoke-virtual {p1, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 4

    .line 72
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/flow/CrmScope;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;->saveAdjustmentAmount:Lcom/jakewharton/rxrelay/PublishRelay;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;->seed:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;->adjustReason:Lcom/jakewharton/rxrelay/PublishRelay;

    sget-object v3, Lcom/squareup/ui/crm/v2/flow/-$$Lambda$K42IHCDC4Q9dpIAQmfvbo2bdDoE;->INSTANCE:Lcom/squareup/ui/crm/v2/flow/-$$Lambda$K42IHCDC4Q9dpIAQmfvbo2bdDoE;

    .line 76
    invoke-virtual {v0, v1, v2, v3}, Lcom/jakewharton/rxrelay/PublishRelay;->withLatestFrom(Lrx/Observable;Lrx/Observable;Lrx/functions/Func3;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/flow/-$$Lambda$AdjustPointsFlow$ema2HBT1YS_dIJFCoWGd-Bsm864;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/flow/-$$Lambda$AdjustPointsFlow$ema2HBT1YS_dIJFCoWGd-Bsm864;-><init>(Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;)V

    .line 77
    invoke-virtual {v0, v1}, Lrx/Observable;->switchMap(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/flow/-$$Lambda$AdjustPointsFlow$XKb-iQXiLFw3re8_KKas0vqUpUw;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/flow/-$$Lambda$AdjustPointsFlow$XKb-iQXiLFw3re8_KKas0vqUpUw;-><init>(Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;)V

    .line 94
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 74
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 0

    if-nez p1, :cond_0

    :cond_0
    return-void
.end method

.method public onResult()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 134
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;->result:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method public saveAdjustmentAmount()V
    .locals 2

    .line 160
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;->saveAdjustmentAmount:Lcom/jakewharton/rxrelay/PublishRelay;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;->adjustmentAmount:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public setAdjustReason(Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason;)V
    .locals 1

    .line 197
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;->adjustReason:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public shouldShowSmsDisclaimer()Z
    .locals 2

    .line 201
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_ADJUST_POINTS_SMS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;->adjustmentType:Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen$Runner$AdjustmentType;

    sget-object v1, Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen$Runner$AdjustmentType;->REMOVE_POINTS:Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen$Runner$AdjustmentType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public showFirstScreen(Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;)V
    .locals 2

    .line 115
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;->seed:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 116
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;->adjustmentAmount:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 118
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    invoke-direct {v0, v1}, Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method
