.class public Lcom/squareup/ui/crm/rows/ChooseCustomerRow;
.super Landroid/widget/LinearLayout;
.source "ChooseCustomerRow.java"


# instance fields
.field private displayName:Landroid/widget/TextView;

.field private piiNameScrubber:Lcom/squareup/accessibility/AccessibilityScrubber;

.field private statusLine:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    .line 27
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/crm/rows/ChooseCustomerRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 31
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    .line 35
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 37
    sget v0, Lcom/squareup/crm/R$id;->crm_customer_name_pii_wrapper:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/accessibility/AccessibilityScrubber;

    iput-object v0, p0, Lcom/squareup/ui/crm/rows/ChooseCustomerRow;->piiNameScrubber:Lcom/squareup/accessibility/AccessibilityScrubber;

    .line 38
    sget v0, Lcom/squareup/crm/R$id;->crm_customer_display_name:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/crm/rows/ChooseCustomerRow;->displayName:Landroid/widget/TextView;

    .line 39
    sget v0, Lcom/squareup/crm/R$id;->crm_customer_status_line:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/crm/rows/ChooseCustomerRow;->statusLine:Landroid/widget/TextView;

    return-void
.end method

.method public setPiiNameScrubberContentDescription(I)V
    .locals 3

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/ChooseCustomerRow;->piiNameScrubber:Lcom/squareup/accessibility/AccessibilityScrubber;

    sget v1, Lcom/squareup/crm/R$string;->crm_customer_name_row_content_description:I

    .line 44
    invoke-static {p0, v1}, Lcom/squareup/phrase/Phrase;->from(Landroid/view/View;I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    const-string v2, "index"

    .line 45
    invoke-virtual {v1, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 46
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 43
    invoke-virtual {v0, p1}, Lcom/squareup/accessibility/AccessibilityScrubber;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public showDisplayName(Ljava/lang/String;)V
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/ChooseCustomerRow;->displayName:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public showStatusLine(Ljava/lang/String;)V
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/ChooseCustomerRow;->statusLine:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
