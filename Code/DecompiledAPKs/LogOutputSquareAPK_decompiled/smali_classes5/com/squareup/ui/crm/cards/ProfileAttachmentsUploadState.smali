.class public Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadState;
.super Ljava/lang/Object;
.source "ProfileAttachmentsUploadState.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadState$UploadState;
    }
.end annotation


# instance fields
.field private final uploadState:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadState$UploadState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    sget-object v0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadState$UploadState;->NOT_STARTED:Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadState$UploadState;

    invoke-static {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadState;->uploadState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-void
.end method


# virtual methods
.method public failedUpload()V
    .locals 2

    .line 28
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadState;->uploadState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadState$UploadState;->FAILURE:Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadState$UploadState;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public startUpload()V
    .locals 2

    .line 24
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadState;->uploadState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadState$UploadState;->IN_PROGRESS:Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadState$UploadState;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public successfulUpload()V
    .locals 2

    .line 32
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadState;->uploadState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadState$UploadState;->SUCCESS:Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadState$UploadState;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public uploadState()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadState$UploadState;",
            ">;"
        }
    .end annotation

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadState;->uploadState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object v0
.end method
