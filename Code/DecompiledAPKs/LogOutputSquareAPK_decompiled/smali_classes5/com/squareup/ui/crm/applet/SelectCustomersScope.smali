.class public Lcom/squareup/ui/crm/applet/SelectCustomersScope;
.super Lcom/squareup/ui/crm/applet/InCustomersAppletScope;
.source "SelectCustomersScope.java"


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/crm/applet/SelectCustomersScope$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/applet/SelectCustomersScope$Component;,
        Lcom/squareup/ui/crm/applet/SelectCustomersScope$Module;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/crm/applet/SelectCustomersScope;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/crm/applet/SelectCustomersScope;

.field public static final OTHER_INSTANCE:Lcom/squareup/ui/crm/applet/SelectCustomersScope;


# instance fields
.field private final isOtherInstance:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 30
    new-instance v0, Lcom/squareup/ui/crm/applet/SelectCustomersScope;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/crm/applet/SelectCustomersScope;-><init>(Z)V

    sput-object v0, Lcom/squareup/ui/crm/applet/SelectCustomersScope;->INSTANCE:Lcom/squareup/ui/crm/applet/SelectCustomersScope;

    .line 35
    new-instance v0, Lcom/squareup/ui/crm/applet/SelectCustomersScope;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/squareup/ui/crm/applet/SelectCustomersScope;-><init>(Z)V

    sput-object v0, Lcom/squareup/ui/crm/applet/SelectCustomersScope;->OTHER_INSTANCE:Lcom/squareup/ui/crm/applet/SelectCustomersScope;

    .line 103
    sget-object v0, Lcom/squareup/ui/crm/applet/-$$Lambda$SelectCustomersScope$NaUJncE19JzGPEpm60kGxSK6zac;->INSTANCE:Lcom/squareup/ui/crm/applet/-$$Lambda$SelectCustomersScope$NaUJncE19JzGPEpm60kGxSK6zac;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/applet/SelectCustomersScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 0

    .line 90
    invoke-direct {p0}, Lcom/squareup/ui/crm/applet/InCustomersAppletScope;-><init>()V

    .line 91
    iput-boolean p1, p0, Lcom/squareup/ui/crm/applet/SelectCustomersScope;->isOtherInstance:Z

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/crm/applet/SelectCustomersScope;
    .locals 1

    .line 104
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result p0

    const/4 v0, 0x1

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 105
    :goto_0
    new-instance p0, Lcom/squareup/ui/crm/applet/SelectCustomersScope;

    invoke-direct {p0, v0}, Lcom/squareup/ui/crm/applet/SelectCustomersScope;-><init>(Z)V

    return-object p0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 99
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/crm/applet/InCustomersAppletScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 100
    iget-boolean p2, p0, Lcom/squareup/ui/crm/applet/SelectCustomersScope;->isOtherInstance:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .line 95
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Lcom/squareup/ui/crm/applet/InCustomersAppletScope;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/crm/applet/SelectCustomersScope;->isOtherInstance:Z

    if-eqz v1, :cond_0

    const-string v1, "_OTHER"

    goto :goto_0

    :cond_0
    const-string v1, ""

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
