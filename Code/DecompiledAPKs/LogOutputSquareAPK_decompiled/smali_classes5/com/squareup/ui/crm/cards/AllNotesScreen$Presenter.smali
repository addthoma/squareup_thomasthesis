.class Lcom/squareup/ui/crm/cards/AllNotesScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "AllNotesScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/AllNotesScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/crm/cards/AllNotesView;",
        ">;"
    }
.end annotation


# instance fields
.field private final dateFormatter:Ljava/text/DateFormat;

.field private final locale:Ljava/util/Locale;

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/ui/crm/cards/AllNotesScreen$Runner;

.field private final timeFormatter:Ljava/text/DateFormat;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/AllNotesScreen$Runner;Lcom/squareup/util/Res;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljava/util/Locale;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 73
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 74
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/AllNotesScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/AllNotesScreen$Runner;

    .line 75
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/AllNotesScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 76
    iput-object p3, p0, Lcom/squareup/ui/crm/cards/AllNotesScreen$Presenter;->dateFormatter:Ljava/text/DateFormat;

    .line 77
    iput-object p4, p0, Lcom/squareup/ui/crm/cards/AllNotesScreen$Presenter;->timeFormatter:Ljava/text/DateFormat;

    .line 78
    iput-object p5, p0, Lcom/squareup/ui/crm/cards/AllNotesScreen$Presenter;->locale:Ljava/util/Locale;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/crm/cards/AllNotesScreen$Presenter;)Lcom/squareup/ui/crm/cards/AllNotesScreen$Runner;
    .locals 0

    .line 63
    iget-object p0, p0, Lcom/squareup/ui/crm/cards/AllNotesScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/AllNotesScreen$Runner;

    return-object p0
.end method

.method private bindRow(Lcom/squareup/ui/crm/rows/NoteRow;Lcom/squareup/protos/client/rolodex/Note;)V
    .locals 2

    .line 102
    iget-object v0, p2, Lcom/squareup/protos/client/rolodex/Note;->body:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/rows/NoteRow;->showNote(Ljava/lang/String;)V

    .line 103
    iget-object v0, p2, Lcom/squareup/protos/client/rolodex/Note;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Note;->occurred_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/crm/cards/AllNotesScreen$Presenter;->formatCreatorTimestamp(Lcom/squareup/protos/client/CreatorDetails;Lcom/squareup/protos/common/time/DateTime;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/rows/NoteRow;->showCreatorTimestamp(Ljava/lang/String;)V

    .line 104
    iget-object v0, p2, Lcom/squareup/protos/client/rolodex/Note;->reminder:Lcom/squareup/protos/client/rolodex/Reminder;

    if-eqz v0, :cond_0

    .line 105
    invoke-virtual {p1}, Lcom/squareup/ui/crm/rows/NoteRow;->showReminder()V

    .line 107
    :cond_0
    new-instance v0, Lcom/squareup/ui/crm/cards/AllNotesScreen$Presenter$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/crm/cards/AllNotesScreen$Presenter$1;-><init>(Lcom/squareup/ui/crm/cards/AllNotesScreen$Presenter;Lcom/squareup/protos/client/rolodex/Note;)V

    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/rows/NoteRow;->setNoteOnClick(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private formatCreatorTimestamp(Lcom/squareup/protos/client/CreatorDetails;Lcom/squareup/protos/common/time/DateTime;)Ljava/lang/String;
    .locals 4

    .line 116
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AllNotesScreen$Presenter;->locale:Ljava/util/Locale;

    invoke-static {p2, v0}, Lcom/squareup/util/ProtoTimes;->asDate(Lcom/squareup/protos/common/time/DateTime;Ljava/util/Locale;)Ljava/util/Date;

    move-result-object p2

    const-string/jumbo v0, "time"

    const-string v1, "date"

    if-eqz p1, :cond_0

    .line 117
    iget-object v2, p1, Lcom/squareup/protos/client/CreatorDetails;->employee:Lcom/squareup/protos/client/Employee;

    if-eqz v2, :cond_0

    iget-object v2, p1, Lcom/squareup/protos/client/CreatorDetails;->employee:Lcom/squareup/protos/client/Employee;

    iget-object v2, v2, Lcom/squareup/protos/client/Employee;->read_only_full_name:Ljava/lang/String;

    .line 119
    invoke-static {v2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 120
    iget-object v2, p0, Lcom/squareup/ui/crm/cards/AllNotesScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/crmscreens/R$string;->crm_note_creator_timestamp:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    iget-object p1, p1, Lcom/squareup/protos/client/CreatorDetails;->employee:Lcom/squareup/protos/client/Employee;

    iget-object p1, p1, Lcom/squareup/protos/client/Employee;->read_only_full_name:Ljava/lang/String;

    const-string v3, "full_name"

    .line 121
    invoke-virtual {v2, v3, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/AllNotesScreen$Presenter;->dateFormatter:Ljava/text/DateFormat;

    .line 122
    invoke-virtual {v2, p2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/AllNotesScreen$Presenter;->timeFormatter:Ljava/text/DateFormat;

    .line 123
    invoke-virtual {v1, p2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, v0, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 124
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 126
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/AllNotesScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/crm/R$string;->date_format:I

    invoke-interface {p1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/AllNotesScreen$Presenter;->dateFormatter:Ljava/text/DateFormat;

    .line 127
    invoke-virtual {v2, p2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/AllNotesScreen$Presenter;->timeFormatter:Ljava/text/DateFormat;

    .line 128
    invoke-virtual {v1, p2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, v0, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 129
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method protected onLoad(Landroid/os/Bundle;)V
    .locals 4

    .line 82
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 83
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/AllNotesScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/AllNotesView;

    .line 85
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/AllNotesScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/crmscreens/R$string;->crm_all_notes_title:I

    .line 86
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/AllNotesScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/AllNotesScreen$Runner;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/crm/cards/-$$Lambda$aV-u43rgSzQElERmABtTi2B5uBI;

    invoke-direct {v2, v1}, Lcom/squareup/ui/crm/cards/-$$Lambda$aV-u43rgSzQElERmABtTi2B5uBI;-><init>(Lcom/squareup/ui/crm/cards/AllNotesScreen$Runner;)V

    .line 87
    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 88
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hidePrimaryButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 89
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    .line 85
    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/cards/AllNotesView;->setActionBarConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 91
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AllNotesScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/AllNotesScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/AllNotesScreen$Runner;->getContactForAllNotesScreen()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/squareup/ui/crm/cards/AllNotesScreen$Presenter;->setContact(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/ui/crm/cards/AllNotesView;)V

    return-void
.end method

.method setContact(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/ui/crm/cards/AllNotesView;)V
    .locals 2

    .line 95
    invoke-virtual {p2}, Lcom/squareup/ui/crm/cards/AllNotesView;->clearRows()V

    .line 96
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Contact;->note:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/Note;

    .line 97
    invoke-virtual {p2}, Lcom/squareup/ui/crm/cards/AllNotesView;->addRow()Lcom/squareup/ui/crm/rows/NoteRow;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/squareup/ui/crm/cards/AllNotesScreen$Presenter;->bindRow(Lcom/squareup/ui/crm/rows/NoteRow;Lcom/squareup/protos/client/rolodex/Note;)V

    goto :goto_0

    :cond_0
    return-void
.end method
