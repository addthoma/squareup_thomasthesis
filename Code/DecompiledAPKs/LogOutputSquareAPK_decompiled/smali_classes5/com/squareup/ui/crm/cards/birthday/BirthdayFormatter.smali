.class public Lcom/squareup/ui/crm/cards/birthday/BirthdayFormatter;
.super Ljava/lang/Object;
.source "BirthdayFormatter.java"


# instance fields
.field private final formatMonthDay:Ljava/text/DateFormat;

.field private final formatYearMonthDay:Ljava/text/DateFormat;


# direct methods
.method public constructor <init>(Ljava/text/DateFormat;Ljava/text/DateFormat;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayFormatter;->formatMonthDay:Ljava/text/DateFormat;

    .line 21
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayFormatter;->formatYearMonthDay:Ljava/text/DateFormat;

    return-void
.end method


# virtual methods
.method public format(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/lang/String;
    .locals 2

    if-eqz p1, :cond_1

    .line 27
    :try_start_0
    iget-object v0, p1, Lcom/squareup/protos/common/time/YearMonthDay;->month_of_year:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/squareup/protos/common/time/YearMonthDay;->day_of_month:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 28
    iget-object v0, p1, Lcom/squareup/protos/common/time/YearMonthDay;->year:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 29
    iget-object v0, p1, Lcom/squareup/protos/common/time/YearMonthDay;->year:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p1, Lcom/squareup/protos/common/time/YearMonthDay;->month_of_year:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    iget-object p1, p1, Lcom/squareup/protos/common/time/YearMonthDay;->day_of_month:Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-static {v0, v1, p1}, Lcom/squareup/util/Times;->asDate(III)Ljava/util/Date;

    move-result-object p1

    .line 30
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayFormatter;->formatYearMonthDay:Ljava/text/DateFormat;

    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    const/16 v0, 0x7d0

    .line 33
    iget-object v1, p1, Lcom/squareup/protos/common/time/YearMonthDay;->month_of_year:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    iget-object p1, p1, Lcom/squareup/protos/common/time/YearMonthDay;->day_of_month:Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-static {v0, v1, p1}, Lcom/squareup/util/Times;->asDate(III)Ljava/util/Date;

    move-result-object p1

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayFormatter;->formatMonthDay:Ljava/text/DateFormat;

    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method
