.class public Lcom/squareup/ui/crm/v2/profile/InvoicesSectionDataRenderer;
.super Ljava/lang/Object;
.source "InvoicesSectionDataRenderer.java"


# instance fields
.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/InvoicesSectionDataRenderer;->res:Lcom/squareup/util/Res;

    .line 27
    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/InvoicesSectionDataRenderer;->moneyFormatter:Lcom/squareup/text/Formatter;

    return-void
.end method

.method private getTitle(Lcom/squareup/protos/client/invoice/MetricType;)Ljava/lang/String;
    .locals 1

    .line 79
    sget-object v0, Lcom/squareup/ui/crm/v2/profile/InvoicesSectionDataRenderer$1;->$SwitchMap$com$squareup$protos$client$invoice$MetricType:[I

    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/MetricType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_3

    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    const/4 v0, 0x3

    if-eq p1, v0, :cond_1

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 87
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/InvoicesSectionDataRenderer;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/common/invoices/R$string;->invoice_display_state_unpaid:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 89
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Could not recognize invoice metric type."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 85
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/InvoicesSectionDataRenderer;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/common/invoices/R$string;->invoice_display_state_draft:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 83
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/InvoicesSectionDataRenderer;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/common/invoices/R$string;->invoice_display_state_paid:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 81
    :cond_3
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/InvoicesSectionDataRenderer;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/crmviewcustomer/R$string;->outstanding:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private showCta(Ljava/util/List;Z)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;",
            ">;Z)Z"
        }
    .end annotation

    const/4 v0, 0x0

    if-eqz p1, :cond_2

    if-nez p2, :cond_0

    goto :goto_0

    .line 68
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;

    .line 69
    iget-object p2, p2, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;->value:Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;

    iget-object p2, p2, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;->money_value:Lcom/squareup/protos/common/Money;

    iget-object p2, p2, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long p2, v1, v3

    if-lez p2, :cond_1

    const/4 v0, 0x1

    :cond_2
    :goto_0
    return v0
.end method

.method private toSummaryLines(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;",
            ">;"
        }
    .end annotation

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 46
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;

    .line 47
    iget-object v2, v1, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;->value:Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;

    .line 48
    sget-object v3, Lcom/squareup/ui/crm/v2/profile/InvoicesSectionDataRenderer$1;->$SwitchMap$com$squareup$protos$client$invoice$GetMetricsResponse$Metric$ValueType:[I

    iget-object v4, v2, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;->value_type:Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$ValueType;

    invoke-virtual {v4}, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$ValueType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 50
    iget-object v2, v2, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;->money_value:Lcom/squareup/protos/common/Money;

    .line 51
    iget-object v1, v1, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;->metric_type:Lcom/squareup/protos/client/invoice/MetricType;

    invoke-direct {p0, v1}, Lcom/squareup/ui/crm/v2/profile/InvoicesSectionDataRenderer;->getTitle(Lcom/squareup/protos/client/invoice/MetricType;)Ljava/lang/String;

    move-result-object v1

    .line 52
    new-instance v3, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;

    iget-object v4, p0, Lcom/squareup/ui/crm/v2/profile/InvoicesSectionDataRenderer;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 53
    invoke-interface {v4, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v4, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;->TEXT:Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;

    invoke-direct {v3, v1, v2, v4}, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;)V

    .line 52
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 56
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Could not recognize invoice metric value type."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    return-object v0
.end method


# virtual methods
.method public generateViewData(Lcom/squareup/protos/client/invoice/GetMetricsResponse;Z)Lcom/squareup/ui/crm/v2/profile/SimpleSectionView$ViewData;
    .locals 8

    .line 33
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/GetMetricsResponse;->metric:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/squareup/ui/crm/v2/profile/InvoicesSectionDataRenderer;->toSummaryLines(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 36
    new-instance v7, Lcom/squareup/ui/crm/v2/profile/SimpleSectionView$ViewData;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/InvoicesSectionDataRenderer;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/crmviewcustomer/R$string;->crm_invoices_uppercase:I

    .line 37
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v4, Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;->DISABLED:Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;

    .line 40
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/GetMetricsResponse;->metric:Ljava/util/List;

    .line 41
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/crm/v2/profile/InvoicesSectionDataRenderer;->showCta(Ljava/util/List;Z)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/InvoicesSectionDataRenderer;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/crmviewcustomer/R$string;->crm_invoice_details_all:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    move-object v6, p1

    const/4 v3, 0x0

    move-object v1, v7

    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/crm/v2/profile/SimpleSectionView$ViewData;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;Ljava/util/List;Ljava/lang/String;)V

    return-object v7
.end method
