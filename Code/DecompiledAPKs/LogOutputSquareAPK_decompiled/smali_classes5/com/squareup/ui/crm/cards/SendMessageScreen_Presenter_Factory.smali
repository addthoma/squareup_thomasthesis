.class public final Lcom/squareup/ui/crm/cards/SendMessageScreen_Presenter_Factory;
.super Ljava/lang/Object;
.source "SendMessageScreen_Presenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/coupon/AddCouponState;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/cards/SendMessageScreen$Runner;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/DialogueServiceHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final arg6Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final arg7Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/coupon/AddCouponState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/cards/SendMessageScreen$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/DialogueServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen_Presenter_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 42
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen_Presenter_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 43
    iput-object p3, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen_Presenter_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 44
    iput-object p4, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen_Presenter_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 45
    iput-object p5, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen_Presenter_Factory;->arg4Provider:Ljavax/inject/Provider;

    .line 46
    iput-object p6, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen_Presenter_Factory;->arg5Provider:Ljavax/inject/Provider;

    .line 47
    iput-object p7, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen_Presenter_Factory;->arg6Provider:Ljavax/inject/Provider;

    .line 48
    iput-object p8, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen_Presenter_Factory;->arg7Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/crm/cards/SendMessageScreen_Presenter_Factory;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/coupon/AddCouponState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/cards/SendMessageScreen$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/DialogueServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/ui/crm/cards/SendMessageScreen_Presenter_Factory;"
        }
    .end annotation

    .line 61
    new-instance v9, Lcom/squareup/ui/crm/cards/SendMessageScreen_Presenter_Factory;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/ui/crm/cards/SendMessageScreen_Presenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v9
.end method

.method public static newInstance(Lcom/squareup/ui/crm/coupon/AddCouponState;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/crm/cards/SendMessageScreen$Runner;Lcom/squareup/crm/DialogueServiceHelper;Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;)Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/crm/coupon/AddCouponState;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/ui/crm/cards/SendMessageScreen$Runner;",
            "Lcom/squareup/crm/DialogueServiceHelper;",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/settings/server/Features;",
            ")",
            "Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;"
        }
    .end annotation

    .line 67
    new-instance v9, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;-><init>(Lcom/squareup/ui/crm/coupon/AddCouponState;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/crm/cards/SendMessageScreen$Runner;Lcom/squareup/crm/DialogueServiceHelper;Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;)V

    return-object v9
.end method


# virtual methods
.method public get()Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;
    .locals 9

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen_Presenter_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/crm/coupon/AddCouponState;

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen_Presenter_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen_Presenter_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/ui/crm/cards/SendMessageScreen$Runner;

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen_Presenter_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/crm/DialogueServiceHelper;

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen_Presenter_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/ui/ErrorsBarPresenter;

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen_Presenter_Factory;->arg5Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/text/Formatter;

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen_Presenter_Factory;->arg6Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageScreen_Presenter_Factory;->arg7Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/settings/server/Features;

    invoke-static/range {v1 .. v8}, Lcom/squareup/ui/crm/cards/SendMessageScreen_Presenter_Factory;->newInstance(Lcom/squareup/ui/crm/coupon/AddCouponState;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/crm/cards/SendMessageScreen$Runner;Lcom/squareup/crm/DialogueServiceHelper;Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;)Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/SendMessageScreen_Presenter_Factory;->get()Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;

    move-result-object v0

    return-object v0
.end method
