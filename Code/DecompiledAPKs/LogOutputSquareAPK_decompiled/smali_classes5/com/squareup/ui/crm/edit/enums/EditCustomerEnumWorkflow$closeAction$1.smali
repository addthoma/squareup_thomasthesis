.class final Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow$closeAction$1;
.super Lkotlin/jvm/internal/Lambda;
.source "EditCustomerEnumWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow;->closeAction(Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumProps;)Lcom/squareup/workflow/WorkflowAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/WorkflowAction$Updater<",
        "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;",
        "-",
        "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumOutput;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditCustomerEnumWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditCustomerEnumWorkflow.kt\ncom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow$closeAction$1\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,68:1\n704#2:69\n777#2,2:70\n*E\n*S KotlinDebug\n*F\n+ 1 EditCustomerEnumWorkflow.kt\ncom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow$closeAction$1\n*L\n65#1:69\n65#1,2:70\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0002H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;",
        "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumOutput;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $props:Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumProps;

.field final synthetic $state:Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumProps;Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow$closeAction$1;->$props:Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumProps;

    iput-object p2, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow$closeAction$1;->$state:Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 12
    check-cast p1, Lcom/squareup/workflow/WorkflowAction$Updater;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow$closeAction$1;->invoke(Lcom/squareup/workflow/WorkflowAction$Updater;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;",
            "-",
            "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow$closeAction$1;->$props:Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumProps;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumProps;->getOptions()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 69
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 70
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Ljava/lang/String;

    .line 65
    iget-object v4, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow$closeAction$1;->$state:Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;

    invoke-virtual {v4}, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;->getSelected()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 71
    :cond_1
    check-cast v1, Ljava/util/List;

    .line 65
    new-instance v0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumOutput;

    invoke-direct {v0, v1}, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumOutput;-><init>(Ljava/util/List;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    return-void
.end method
