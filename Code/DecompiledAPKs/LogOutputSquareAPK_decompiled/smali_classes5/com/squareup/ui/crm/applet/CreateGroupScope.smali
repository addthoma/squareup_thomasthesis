.class public Lcom/squareup/ui/crm/applet/CreateGroupScope;
.super Lcom/squareup/ui/crm/applet/InCustomersAppletScope;
.source "CreateGroupScope.java"


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/crm/applet/CreateGroupScope$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/applet/CreateGroupScope$Component;,
        Lcom/squareup/ui/crm/applet/CreateGroupScope$Module;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/crm/applet/CreateGroupScope;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/crm/applet/CreateGroupScope;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 13
    new-instance v0, Lcom/squareup/ui/crm/applet/CreateGroupScope;

    invoke-direct {v0}, Lcom/squareup/ui/crm/applet/CreateGroupScope;-><init>()V

    sput-object v0, Lcom/squareup/ui/crm/applet/CreateGroupScope;->INSTANCE:Lcom/squareup/ui/crm/applet/CreateGroupScope;

    .line 31
    sget-object v0, Lcom/squareup/ui/crm/applet/CreateGroupScope;->INSTANCE:Lcom/squareup/ui/crm/applet/CreateGroupScope;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/applet/CreateGroupScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 28
    invoke-direct {p0}, Lcom/squareup/ui/crm/applet/InCustomersAppletScope;-><init>()V

    return-void
.end method
