.class public interface abstract Lcom/squareup/ui/crm/cards/DeleteCustomersConfirmationScreen$Controller;
.super Ljava/lang/Object;
.source "DeleteCustomersConfirmationScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/DeleteCustomersConfirmationScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Controller"
.end annotation


# virtual methods
.method public abstract cancelDeleteCustomersConfirmationScreen()V
.end method

.method public abstract deleteCustomers(Lcom/squareup/protos/client/rolodex/ContactSet;)V
.end method

.method public abstract getContactSet()Lcom/squareup/protos/client/rolodex/ContactSet;
.end method

.method public abstract getDeleteCustomersConfirmationText()Ljava/lang/String;
.end method
