.class public final synthetic Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$Presenter$0hbrjNO0wHWyd9osH8hxQnBP9jQ;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lrx/functions/Action1;


# instance fields
.field private final synthetic f$0:Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Presenter;

.field private final synthetic f$1:Lcom/squareup/ui/crm/rows/CheckableRow;

.field private final synthetic f$2:Ljava/util/concurrent/atomic/AtomicReference;

.field private final synthetic f$3:Lcom/squareup/marin/widgets/MarinActionBar;

.field private final synthetic f$4:Lcom/squareup/protos/client/rolodex/Group;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Presenter;Lcom/squareup/ui/crm/rows/CheckableRow;Ljava/util/concurrent/atomic/AtomicReference;Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/protos/client/rolodex/Group;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$Presenter$0hbrjNO0wHWyd9osH8hxQnBP9jQ;->f$0:Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Presenter;

    iput-object p2, p0, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$Presenter$0hbrjNO0wHWyd9osH8hxQnBP9jQ;->f$1:Lcom/squareup/ui/crm/rows/CheckableRow;

    iput-object p3, p0, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$Presenter$0hbrjNO0wHWyd9osH8hxQnBP9jQ;->f$2:Ljava/util/concurrent/atomic/AtomicReference;

    iput-object p4, p0, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$Presenter$0hbrjNO0wHWyd9osH8hxQnBP9jQ;->f$3:Lcom/squareup/marin/widgets/MarinActionBar;

    iput-object p5, p0, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$Presenter$0hbrjNO0wHWyd9osH8hxQnBP9jQ;->f$4:Lcom/squareup/protos/client/rolodex/Group;

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Object;)V
    .locals 6

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$Presenter$0hbrjNO0wHWyd9osH8hxQnBP9jQ;->f$0:Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Presenter;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$Presenter$0hbrjNO0wHWyd9osH8hxQnBP9jQ;->f$1:Lcom/squareup/ui/crm/rows/CheckableRow;

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$Presenter$0hbrjNO0wHWyd9osH8hxQnBP9jQ;->f$2:Ljava/util/concurrent/atomic/AtomicReference;

    iget-object v3, p0, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$Presenter$0hbrjNO0wHWyd9osH8hxQnBP9jQ;->f$3:Lcom/squareup/marin/widgets/MarinActionBar;

    iget-object v4, p0, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$Presenter$0hbrjNO0wHWyd9osH8hxQnBP9jQ;->f$4:Lcom/squareup/protos/client/rolodex/Group;

    move-object v5, p1

    check-cast v5, Lkotlin/Unit;

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Presenter;->lambda$null$0$AddCustomersToGroupScreen$Presenter(Lcom/squareup/ui/crm/rows/CheckableRow;Ljava/util/concurrent/atomic/AtomicReference;Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/protos/client/rolodex/Group;Lkotlin/Unit;)V

    return-void
.end method
