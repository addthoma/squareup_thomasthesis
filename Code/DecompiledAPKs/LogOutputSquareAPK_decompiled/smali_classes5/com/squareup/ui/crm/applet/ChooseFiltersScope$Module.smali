.class public abstract Lcom/squareup/ui/crm/applet/ChooseFiltersScope$Module;
.super Ljava/lang/Object;
.source "ChooseFiltersScope.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/applet/ChooseFiltersScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Module"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract chooseFiltersCardScreen(Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;)Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Controller;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract saveFiltersCardScreen(Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;)Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Controller;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
