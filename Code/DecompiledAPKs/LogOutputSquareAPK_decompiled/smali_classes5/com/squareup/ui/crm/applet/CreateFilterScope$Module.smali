.class public abstract Lcom/squareup/ui/crm/applet/CreateFilterScope$Module;
.super Ljava/lang/Object;
.source "CreateFilterScope.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/applet/CreateFilterScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Module"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract chooseFilter(Lcom/squareup/ui/crm/flow/CreateFilterFlow;)Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Controller;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract editFilter(Lcom/squareup/ui/crm/flow/CreateFilterFlow;)Lcom/squareup/ui/crm/cards/EditFilterScreen$Controller;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
