.class public Lcom/squareup/ui/crm/v2/CrmDynamicEvent;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "CrmDynamicEvent.java"


# static fields
.field private static final ALL_CUSTOMERS_PREFIX:Ljava/lang/String; = "Contacts List"

.field private static final FILTERS_PREFIX:Ljava/lang/String; = "Contacts List:Select Filter"

.field public static final FILTERS_SELECT_FILTER_VALUE_SUFFIX:Ljava/lang/String; = "Select Filter Value"

.field private static final MANUAL_GROUP_PREFIX:Ljava/lang/String; = "Selected Manual Group"

.field public static final MENU_ACTION_ADD_TO_GROUP_SUFFIX:Ljava/lang/String; = "Menu:Add to Group"

.field public static final MENU_ACTION_BULK_DELETE_SUFFIX:Ljava/lang/String; = "Menu:Bulk Delete"

.field public static final MENU_ACTION_CLOSE:Ljava/lang/String; = "Close"

.field public static final MENU_ACTION_CREATE_CUSTOMER_SUFFIX:Ljava/lang/String; = "Menu:Create Customer"

.field public static final MENU_ACTION_DESELECT_ALL:Ljava/lang/String; = "Deselect All"

.field public static final MENU_ACTION_DONE:Ljava/lang/String; = "Done"

.field public static final MENU_ACTION_EDIT_GROUP_SUFFIX:Ljava/lang/String; = "Menu:Edit Group"

.field public static final MENU_ACTION_FILTER_CUSTOMERS_SUFFIX:Ljava/lang/String; = "Menu:Filter Customers"

.field public static final MENU_ACTION_MERGE_CUSTOMERS_SUFFIX:Ljava/lang/String; = "Menu:Merge Customers"

.field public static final MENU_ACTION_REMOVE_FROM_GROUP_SUFFIX:Ljava/lang/String; = "Menu:Remove From Group"

.field public static final MENU_ACTION_RESOLVE_DUPLICATES_SUFFIX:Ljava/lang/String; = "Menu:Resolve Duplicates"

.field public static final MENU_ACTION_SELECT_ALL:Ljava/lang/String; = "Select All"

.field public static final MENU_ACTION_VIEW_FEEDBACK_SUFFIX:Ljava/lang/String; = "Menu:View Feedback"

.field public static final MENU_ACTION_VIEW_GROUPS_SUFFIX:Ljava/lang/String; = "Menu:View Groups"

.field private static final SMART_GROUP_PREFIX:Ljava/lang/String; = "Selected Smart Group"


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 1

    .line 52
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    invoke-direct {p0, v0, p1}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    return-void
.end method

.method private static create(Ljava/lang/String;[Ljava/lang/String;)Lcom/squareup/ui/crm/v2/CrmDynamicEvent;
    .locals 4

    .line 56
    new-instance v0, Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Directory:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 57
    array-length p0, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, p0, :cond_0

    aget-object v2, p1, v1

    const-string v3, ":"

    .line 58
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 60
    :cond_0
    new-instance p0, Lcom/squareup/ui/crm/v2/CrmDynamicEvent;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/v2/CrmDynamicEvent;-><init>(Ljava/lang/String;)V

    return-object p0
.end method

.method public static varargs createAllCustomersEvent([Ljava/lang/String;)Lcom/squareup/ui/crm/v2/CrmDynamicEvent;
    .locals 1

    const-string v0, "Contacts List"

    .line 64
    invoke-static {v0, p0}, Lcom/squareup/ui/crm/v2/CrmDynamicEvent;->create(Ljava/lang/String;[Ljava/lang/String;)Lcom/squareup/ui/crm/v2/CrmDynamicEvent;

    move-result-object p0

    return-object p0
.end method

.method public static createEditContactEvent(Ljava/lang/String;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;Ljava/lang/String;)Lcom/squareup/ui/crm/v2/CrmDynamicEvent;
    .locals 2

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    .line 82
    sget-object v1, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;->CREATE:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;

    if-ne p1, v1, :cond_0

    const-string p1, "Create Customer"

    goto :goto_0

    :cond_0
    const-string p1, "Edit Customer"

    :goto_0
    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 p1, 0x1

    aput-object p2, v0, p1

    invoke-static {p0, v0}, Lcom/squareup/ui/crm/v2/CrmDynamicEvent;->create(Ljava/lang/String;[Ljava/lang/String;)Lcom/squareup/ui/crm/v2/CrmDynamicEvent;

    move-result-object p0

    return-object p0
.end method

.method public static varargs createFilterEvent([Ljava/lang/String;)Lcom/squareup/ui/crm/v2/CrmDynamicEvent;
    .locals 1

    const-string v0, "Contacts List:Select Filter"

    .line 76
    invoke-static {v0, p0}, Lcom/squareup/ui/crm/v2/CrmDynamicEvent;->create(Ljava/lang/String;[Ljava/lang/String;)Lcom/squareup/ui/crm/v2/CrmDynamicEvent;

    move-result-object p0

    return-object p0
.end method

.method public static varargs createManualGroupEvent([Ljava/lang/String;)Lcom/squareup/ui/crm/v2/CrmDynamicEvent;
    .locals 1

    const-string v0, "Selected Manual Group"

    .line 68
    invoke-static {v0, p0}, Lcom/squareup/ui/crm/v2/CrmDynamicEvent;->create(Ljava/lang/String;[Ljava/lang/String;)Lcom/squareup/ui/crm/v2/CrmDynamicEvent;

    move-result-object p0

    return-object p0
.end method

.method public static varargs createMultiSelectEvent(Ljava/lang/String;[Ljava/lang/String;)Lcom/squareup/ui/crm/v2/CrmDynamicEvent;
    .locals 0

    .line 89
    invoke-static {p0, p1}, Lcom/squareup/ui/crm/v2/CrmDynamicEvent;->create(Ljava/lang/String;[Ljava/lang/String;)Lcom/squareup/ui/crm/v2/CrmDynamicEvent;

    move-result-object p0

    return-object p0
.end method

.method public static varargs createSmartGroupEvent([Ljava/lang/String;)Lcom/squareup/ui/crm/v2/CrmDynamicEvent;
    .locals 1

    const-string v0, "Selected Smart Group"

    .line 72
    invoke-static {v0, p0}, Lcom/squareup/ui/crm/v2/CrmDynamicEvent;->create(Ljava/lang/String;[Ljava/lang/String;)Lcom/squareup/ui/crm/v2/CrmDynamicEvent;

    move-result-object p0

    return-object p0
.end method

.method public static getUpdateCustomerAnalyticsPathType(Lcom/squareup/ui/crm/flow/CrmScopeType;)Ljava/lang/String;
    .locals 1

    .line 94
    sget-object v0, Lcom/squareup/ui/crm/v2/CrmDynamicEvent$1;->$SwitchMap$com$squareup$ui$crm$flow$CrmScopeType:[I

    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/CrmScopeType;->ordinal()I

    move-result p0

    aget p0, v0, p0

    packed-switch p0, :pswitch_data_0

    .line 128
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "Invalid path type"

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0

    :pswitch_0
    const-string p0, "Add to Estimate"

    return-object p0

    :pswitch_1
    const-string p0, "Customers Applet"

    return-object p0

    :pswitch_2
    const-string p0, "Add To Sale Post Transaction"

    return-object p0

    :pswitch_3
    const-string p0, "Add To Sale In Cart"

    return-object p0

    :pswitch_4
    const-string p0, "Add To Invoice"

    return-object p0

    :pswitch_5
    const-string p0, "Add to Appointment"

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_5
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
