.class Lcom/squareup/ui/crm/cards/ConversationCardScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "ConversationCardScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/ConversationCardScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/crm/cards/ConversationCardView;",
        ">;"
    }
.end annotation


# instance fields
.field private final runner:Lcom/squareup/ui/crm/cards/ConversationCardScreen$Runner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/ConversationCardScreen$Runner;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 68
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 69
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/ConversationCardScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/ConversationCardScreen$Runner;

    return-void
.end method

.method static synthetic lambda$null$0(Lcom/squareup/ui/crm/cards/ConversationCardView;Lcom/squareup/protos/client/dialogue/Conversation;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 86
    iget-object v0, p1, Lcom/squareup/protos/client/dialogue/Conversation;->contact:Lcom/squareup/protos/client/dialogue/Contact;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/squareup/protos/client/dialogue/Conversation;->contact:Lcom/squareup/protos/client/dialogue/Contact;

    iget-object v0, v0, Lcom/squareup/protos/client/dialogue/Contact;->name:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 87
    iget-object p1, p1, Lcom/squareup/protos/client/dialogue/Conversation;->contact:Lcom/squareup/protos/client/dialogue/Contact;

    iget-object p1, p1, Lcom/squareup/protos/client/dialogue/Contact;->name:Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/cards/ConversationCardView;->setActionBarUpButtonTextBackArrow(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method static synthetic lambda$null$2(Lcom/squareup/ui/crm/cards/ConversationCardView;Ljava/lang/Boolean;)V
    .locals 0

    .line 94
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/cards/ConversationCardView;->setActionBarUpButtonEnabled(Z)V

    return-void
.end method

.method static synthetic lambda$onLoad$1(Lcom/squareup/ui/crm/cards/ConversationCardView;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 84
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/ConversationCardView;->conversationView()Lcom/squareup/ui/crm/ConversationView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/crm/ConversationView;->conversation()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$ConversationCardScreen$Presenter$3Pu8aqMFMnh7VDqHuHzSA4yFzGQ;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$ConversationCardScreen$Presenter$3Pu8aqMFMnh7VDqHuHzSA4yFzGQ;-><init>(Lcom/squareup/ui/crm/cards/ConversationCardView;)V

    .line 85
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$onLoad$3(Lcom/squareup/ui/crm/cards/ConversationCardView;)Lrx/Subscription;
    .locals 2

    .line 92
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/ConversationCardView;->conversationView()Lcom/squareup/ui/crm/ConversationView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/crm/ConversationView;->busy()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$ConversationCardScreen$Presenter$A5w1h9-ecoHCsFQm8Yn7cy33r7M;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$ConversationCardScreen$Presenter$A5w1h9-ecoHCsFQm8Yn7cy33r7M;-><init>(Lcom/squareup/ui/crm/cards/ConversationCardView;)V

    .line 93
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public synthetic lambda$null$4$ConversationCardScreen$Presenter(Lkotlin/Unit;)V
    .locals 0

    .line 100
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/ConversationCardScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/ConversationCardScreen$Runner;

    invoke-interface {p1}, Lcom/squareup/ui/crm/cards/ConversationCardScreen$Runner;->showAddCouponScreen()V

    return-void
.end method

.method public synthetic lambda$onLoad$5$ConversationCardScreen$Presenter(Lcom/squareup/ui/crm/cards/ConversationCardView;)Lrx/Subscription;
    .locals 1

    .line 99
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/ConversationCardView;->conversationView()Lcom/squareup/ui/crm/ConversationView;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/crm/ConversationView;->onAddCouponClicked()Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$ConversationCardScreen$Presenter$0AkQwnOJeDp3bzdDxoSunp1dAI8;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$ConversationCardScreen$Presenter$0AkQwnOJeDp3bzdDxoSunp1dAI8;-><init>(Lcom/squareup/ui/crm/cards/ConversationCardScreen$Presenter;)V

    .line 100
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$6$ConversationCardScreen$Presenter(Lcom/squareup/ui/crm/cards/ConversationCardView;)Lrx/Subscription;
    .locals 2

    .line 104
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/ConversationCardView;->conversationView()Lcom/squareup/ui/crm/ConversationView;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/crm/ConversationView;->onBillHistoryClicked()Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ConversationCardScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/ConversationCardScreen$Runner;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$x0pBgBA-ZjO4ylj43sD8c-7eNuQ;

    invoke-direct {v1, v0}, Lcom/squareup/ui/crm/cards/-$$Lambda$x0pBgBA-ZjO4ylj43sD8c-7eNuQ;-><init>(Lcom/squareup/ui/crm/cards/ConversationCardScreen$Runner;)V

    .line 105
    invoke-virtual {p1, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 3

    .line 73
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 74
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/ConversationCardScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/ConversationCardView;

    .line 76
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v2, 0x0

    .line 77
    invoke-virtual {v0, v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphNoText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/ConversationCardScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/ConversationCardScreen$Runner;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/crm/cards/-$$Lambda$qt4oj1TUN2tf6N__OwafGNOwM6s;

    invoke-direct {v2, v1}, Lcom/squareup/ui/crm/cards/-$$Lambda$qt4oj1TUN2tf6N__OwafGNOwM6s;-><init>(Lcom/squareup/ui/crm/cards/ConversationCardScreen$Runner;)V

    .line 78
    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 79
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    .line 76
    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/cards/ConversationCardView;->setActionBarConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ConversationCardScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/ConversationCardScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/ConversationCardScreen$Runner;->getConversationTokenForConversationScreen()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/cards/ConversationCardView;->setConversationToken(Ljava/lang/String;)V

    .line 83
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$ConversationCardScreen$Presenter$nZAoRhrCRZxuJ5eUZaZar7ZCd8c;

    invoke-direct {v0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$ConversationCardScreen$Presenter$nZAoRhrCRZxuJ5eUZaZar7ZCd8c;-><init>(Lcom/squareup/ui/crm/cards/ConversationCardView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 91
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$ConversationCardScreen$Presenter$bSvXmVci1HDvbMlfCR1DJT3SB-o;

    invoke-direct {v0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$ConversationCardScreen$Presenter$bSvXmVci1HDvbMlfCR1DJT3SB-o;-><init>(Lcom/squareup/ui/crm/cards/ConversationCardView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 98
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$ConversationCardScreen$Presenter$Zk_0dPCgiQPe_vPyYsPLlg5oIBA;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$ConversationCardScreen$Presenter$Zk_0dPCgiQPe_vPyYsPLlg5oIBA;-><init>(Lcom/squareup/ui/crm/cards/ConversationCardScreen$Presenter;Lcom/squareup/ui/crm/cards/ConversationCardView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 103
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$ConversationCardScreen$Presenter$w2nGIchfITvKQvVMNM_JZSp3Bz4;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$ConversationCardScreen$Presenter$w2nGIchfITvKQvVMNM_JZSp3Bz4;-><init>(Lcom/squareup/ui/crm/cards/ConversationCardScreen$Presenter;Lcom/squareup/ui/crm/cards/ConversationCardView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
