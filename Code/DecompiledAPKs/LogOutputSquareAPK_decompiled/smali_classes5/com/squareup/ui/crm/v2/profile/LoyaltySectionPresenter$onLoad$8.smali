.class final Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter$onLoad$8;
.super Ljava/lang/Object;
.source "LoyaltySectionPresenter.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->onLoad(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/ObservableSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLoyaltySectionPresenter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LoyaltySectionPresenter.kt\ncom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter$onLoad$8\n*L\n1#1,325:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00030\u00020\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Observable;",
        "",
        "Lcom/squareup/loyalty/ui/RewardWrapper;",
        "data",
        "Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter$onLoad$8;->this$0:Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;)Lio/reactivex/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;",
            ")",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/loyalty/ui/RewardWrapper;",
            ">;>;"
        }
    .end annotation

    const-string v0, "data"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 141
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter$onLoad$8;->this$0:Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;

    invoke-static {v0, p1}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->access$spendablePoints(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;)Ljava/lang/Integer;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 142
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    .line 143
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter$onLoad$8;->this$0:Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;

    invoke-static {v0}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->access$getRewardAdapterHelper$p(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;)Lcom/squareup/loyalty/ui/RewardAdapterHelper;

    move-result-object v0

    .line 144
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-static {p1}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object p1

    const-string v1, "rx.Observable.just(spendablePoints)"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 145
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter$onLoad$8;->this$0:Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;

    invoke-static {v1}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->access$getRunner$p(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;)Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->getHoldsCoupons()Lcom/squareup/checkout/HoldsCoupons;

    move-result-object v1

    .line 146
    new-instance v2, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter$onLoad$8$1$1;

    iget-object v3, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter$onLoad$8;->this$0:Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;

    invoke-static {v3}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->access$getRunner$p(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;)Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter$onLoad$8$1$1;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 143
    invoke-virtual {v0, p1, v1, v2}, Lcom/squareup/loyalty/ui/RewardAdapterHelper;->createRewardWrapperList(Lrx/Observable;Lcom/squareup/checkout/HoldsCoupons;Lkotlin/jvm/functions/Function1;)Lrx/Observable;

    move-result-object p1

    .line 148
    invoke-static {p1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_0

    .line 150
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object p1

    const-string v0, "Observable.just(emptyList())"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 42
    check-cast p1, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter$onLoad$8;->apply(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method
