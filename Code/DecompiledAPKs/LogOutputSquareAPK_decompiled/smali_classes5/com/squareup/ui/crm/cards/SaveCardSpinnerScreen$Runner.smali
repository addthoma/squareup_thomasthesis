.class public interface abstract Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Runner;
.super Ljava/lang/Object;
.source "SaveCardSpinnerScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation


# virtual methods
.method public abstract closeSaveCardSpinner()V
.end method

.method public abstract finishSaveCardFlow(Lcom/squareup/protos/client/rolodex/Contact;)V
.end method

.method public abstract getContactForSaveCardScreen()Lcom/squareup/protos/client/rolodex/Contact;
.end method

.method public abstract getPathType()Lcom/squareup/ui/crm/flow/CrmScopeType;
.end method

.method public abstract getStateForSaveCardScreens()Lcom/squareup/ui/crm/flow/SaveCardSharedState;
.end method
