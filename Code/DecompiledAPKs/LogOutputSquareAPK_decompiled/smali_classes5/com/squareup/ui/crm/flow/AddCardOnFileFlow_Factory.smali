.class public final Lcom/squareup/ui/crm/flow/AddCardOnFileFlow_Factory;
.super Ljava/lang/Object;
.source "AddCardOnFileFlow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;",
        ">;"
    }
.end annotation


# instance fields
.field private final busProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;",
            ">;"
        }
    .end annotation
.end field

.field private final cofDippedCardInfoProcessorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final giftCardsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/GiftCards;",
            ">;"
        }
    .end annotation
.end field

.field private final mainContainerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final x2SellerScreenRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/GiftCards;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p2, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow_Factory;->x2SellerScreenRunnerProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p3, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow_Factory;->mainContainerProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p4, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow_Factory;->busProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p5, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow_Factory;->giftCardsProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p6, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow_Factory;->cofDippedCardInfoProcessorProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p7, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow_Factory;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/crm/flow/AddCardOnFileFlow_Factory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/GiftCards;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/ui/crm/flow/AddCardOnFileFlow_Factory;"
        }
    .end annotation

    .line 63
    new-instance v8, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow_Factory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static newInstance(Lflow/Flow;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;Lcom/squareup/giftcard/GiftCards;Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor;Lcom/squareup/settings/server/Features;)Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;
    .locals 9

    .line 70
    new-instance v8, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;-><init>(Lflow/Flow;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;Lcom/squareup/giftcard/GiftCards;Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor;Lcom/squareup/settings/server/Features;)V

    return-object v8
.end method


# virtual methods
.method public get()Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;
    .locals 8

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lflow/Flow;

    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow_Factory;->x2SellerScreenRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow_Factory;->mainContainerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/ui/main/PosContainer;

    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow_Factory;->busProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;

    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow_Factory;->giftCardsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/giftcard/GiftCards;

    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow_Factory;->cofDippedCardInfoProcessorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor;

    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/settings/server/Features;

    invoke-static/range {v1 .. v7}, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow_Factory;->newInstance(Lflow/Flow;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;Lcom/squareup/giftcard/GiftCards;Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor;Lcom/squareup/settings/server/Features;)Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow_Factory;->get()Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;

    move-result-object v0

    return-object v0
.end method
