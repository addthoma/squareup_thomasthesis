.class public Lcom/squareup/ui/crm/flow/CreateFilterFlow;
.super Ljava/lang/Object;
.source "CreateFilterFlow.java"

# interfaces
.implements Lmortar/bundler/Bundler;
.implements Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Controller;
.implements Lcom/squareup/ui/crm/cards/EditFilterScreen$Controller;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/flow/CreateFilterFlow$SharedScope;
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private currentFilters:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;"
        }
    .end annotation
.end field

.field private filter:Lcom/squareup/protos/client/rolodex/Filter;

.field private final flow:Lflow/Flow;

.field private final onResult:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lkotlin/Pair<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            "Lrx/functions/Func1<",
            "Lflow/History;",
            "Lflow/History;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private parentKey:Lcom/squareup/ui/main/RegisterTreeKey;


# direct methods
.method public constructor <init>(Lflow/Flow;Lcom/squareup/analytics/Analytics;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/CreateFilterFlow;->onResult:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 67
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/CreateFilterFlow;->flow:Lflow/Flow;

    .line 68
    iput-object p2, p0, Lcom/squareup/ui/crm/flow/CreateFilterFlow;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method

.method static synthetic lambda$commitEditFilterCard$0(Lflow/History;)Lflow/History;
    .locals 1

    .line 128
    invoke-virtual {p0}, Lflow/History;->buildUpon()Lflow/History$Builder;

    move-result-object p0

    .line 129
    const-class v0, Lcom/squareup/ui/crm/cards/EditFilterScreen;

    invoke-static {p0, v0}, Lcom/squareup/container/Histories;->popLastScreen(Lflow/History$Builder;Ljava/lang/Class;)Lflow/History$Builder;

    .line 130
    const-class v0, Lcom/squareup/ui/crm/cards/ChooseFilterScreen;

    invoke-static {p0, v0}, Lcom/squareup/container/Histories;->popLastScreen(Lflow/History$Builder;Ljava/lang/Class;)Lflow/History$Builder;

    .line 131
    invoke-virtual {p0}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public closeChooseFilterScreen()V
    .locals 2

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/CreateFilterFlow;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/crm/cards/ChooseFilterScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    .line 96
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/CreateFilterFlow;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_FILTER_CLOSE:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    return-void
.end method

.method public commitEditFilterCard()V
    .locals 4

    .line 126
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/CreateFilterFlow;->onResult:Lcom/jakewharton/rxrelay/PublishRelay;

    new-instance v1, Lkotlin/Pair;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/CreateFilterFlow;->filter:Lcom/squareup/protos/client/rolodex/Filter;

    sget-object v3, Lcom/squareup/ui/crm/flow/-$$Lambda$CreateFilterFlow$x40MbR_1CyKMJLCowAz_Y6WFOyc;->INSTANCE:Lcom/squareup/ui/crm/flow/-$$Lambda$CreateFilterFlow$x40MbR_1CyKMJLCowAz_Y6WFOyc;

    invoke-direct {v1, v2, v3}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    .line 133
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/CreateFilterFlow;->filter:Lcom/squareup/protos/client/rolodex/Filter;

    if-eqz v0, :cond_0

    .line 134
    iget-object v1, p0, Lcom/squareup/ui/crm/flow/CreateFilterFlow;->analytics:Lcom/squareup/analytics/Analytics;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/Filter;->display_name:Ljava/lang/String;

    aput-object v0, v2, v3

    const/4 v0, 0x1

    const-string v3, "Select Filter Value"

    aput-object v3, v2, v0

    invoke-static {v2}, Lcom/squareup/ui/crm/v2/CrmDynamicEvent;->createFilterEvent([Ljava/lang/String;)Lcom/squareup/ui/crm/v2/CrmDynamicEvent;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    :cond_0
    return-void
.end method

.method public dismissEditFilterCard()V
    .locals 2

    .line 122
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/CreateFilterFlow;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/crm/cards/EditFilterScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public getCurrentFilters()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;"
        }
    .end annotation

    .line 106
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/CreateFilterFlow;->currentFilters:Ljava/util/List;

    return-object v0
.end method

.method public getFilter()Lcom/squareup/protos/client/rolodex/Filter;
    .locals 1

    .line 114
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/CreateFilterFlow;->filter:Lcom/squareup/protos/client/rolodex/Filter;

    return-object v0
.end method

.method public getMortarBundleKey()Ljava/lang/String;
    .locals 1

    .line 72
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isFilterBeingCreated()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    :cond_0
    const-string v0, "parentKey"

    .line 83
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/RegisterTreeKey;

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/CreateFilterFlow;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    .line 84
    sget-object v0, Lcom/squareup/protos/client/rolodex/Filter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-string v1, "currentFilters"

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Protos;->loadProtos(Lcom/squareup/wire/ProtoAdapter;Landroid/os/Bundle;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/CreateFilterFlow;->currentFilters:Ljava/util/List;

    .line 85
    sget-object v0, Lcom/squareup/protos/client/rolodex/Filter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-string v1, "filter"

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Protos;->loadProto(Lcom/squareup/wire/ProtoAdapter;Landroid/os/Bundle;Ljava/lang/String;)Lcom/squareup/wire/Message;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/rolodex/Filter;

    iput-object p1, p0, Lcom/squareup/ui/crm/flow/CreateFilterFlow;->filter:Lcom/squareup/protos/client/rolodex/Filter;

    return-void
.end method

.method public onResult()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Pair<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            "Lrx/functions/Func1<",
            "Lflow/History;",
            "Lflow/History;",
            ">;>;>;"
        }
    .end annotation

    .line 147
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/CreateFilterFlow;->onResult:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 89
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/CreateFilterFlow;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    const-string v1, "parentKey"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 90
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/CreateFilterFlow;->currentFilters:Ljava/util/List;

    invoke-static {v0}, Lcom/squareup/util/ProtosPure;->encodeOrNull(Ljava/util/List;)[B

    move-result-object v0

    const-string v1, "currentFilters"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 91
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/CreateFilterFlow;->filter:Lcom/squareup/protos/client/rolodex/Filter;

    invoke-static {v0}, Lcom/squareup/util/ProtosPure;->encodeOrNull(Lcom/squareup/wire/Message;)[B

    move-result-object v0

    const-string v1, "filter"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    return-void
.end method

.method public setFilter(Lcom/squareup/protos/client/rolodex/Filter;)V
    .locals 0

    .line 118
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/CreateFilterFlow;->filter:Lcom/squareup/protos/client/rolodex/Filter;

    return-void
.end method

.method public showEditFilterScreen(Lcom/squareup/protos/client/rolodex/Filter;)V
    .locals 3

    .line 100
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/CreateFilterFlow;->filter:Lcom/squareup/protos/client/rolodex/Filter;

    .line 101
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/CreateFilterFlow;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/crm/cards/EditFilterScreen;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/CreateFilterFlow;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-direct {v1, v2}, Lcom/squareup/ui/crm/cards/EditFilterScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/CreateFilterFlow;->analytics:Lcom/squareup/analytics/Analytics;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter;->display_name:Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v1}, Lcom/squareup/ui/crm/v2/CrmDynamicEvent;->createFilterEvent([Ljava/lang/String;)Lcom/squareup/ui/crm/v2/CrmDynamicEvent;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public showFirstScreen(Lcom/squareup/ui/main/RegisterTreeKey;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/main/RegisterTreeKey;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;)V"
        }
    .end annotation

    .line 140
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/CreateFilterFlow;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    .line 141
    iput-object p2, p0, Lcom/squareup/ui/crm/flow/CreateFilterFlow;->currentFilters:Ljava/util/List;

    const/4 p2, 0x0

    .line 142
    iput-object p2, p0, Lcom/squareup/ui/crm/flow/CreateFilterFlow;->filter:Lcom/squareup/protos/client/rolodex/Filter;

    .line 143
    iget-object p2, p0, Lcom/squareup/ui/crm/flow/CreateFilterFlow;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/ui/crm/cards/ChooseFilterScreen;

    invoke-direct {v0, p1}, Lcom/squareup/ui/crm/cards/ChooseFilterScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {p2, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method
