.class public Lcom/squareup/ui/crm/flow/BillHistoryFlow;
.super Ljava/lang/Object;
.source "BillHistoryFlow.java"

# interfaces
.implements Lmortar/bundler/Bundler;
.implements Lcom/squareup/ui/crm/cards/BillHistoryScreen$Runner;
.implements Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/flow/BillHistoryFlow$OnIssueRefundAccessGranted;,
        Lcom/squareup/ui/crm/flow/BillHistoryFlow$BillHistoryOrServerError;,
        Lcom/squareup/ui/crm/flow/BillHistoryFlow$SharedScope;
    }
.end annotation


# instance fields
.field private bill:Lcom/squareup/billhistory/model/BillHistory;

.field private final billService:Lcom/squareup/server/bills/BillListServiceHelper;

.field private final flow:Lflow/Flow;

.field private final hudToaster:Lcom/squareup/hudtoaster/HudToaster;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

.field private parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

.field private paymentToken:Ljava/lang/String;

.field private final permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

.field private final refundBillHistoryWrapper:Lcom/squareup/ui/crm/flow/RefundBillHistoryWrapper;

.field private final res:Lcom/squareup/util/Res;

.field private tender:Lcom/squareup/billhistory/model/TenderHistory;

.field private title:Ljava/lang/String;

.field private final voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;


# direct methods
.method constructor <init>(Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/server/bills/BillListServiceHelper;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/text/Formatter;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/ui/crm/flow/RefundBillHistoryWrapper;Lcom/squareup/permissions/PermissionGatekeeper;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflow/Flow;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/server/bills/BillListServiceHelper;",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/print/OrderPrintingDispatcher;",
            "Lcom/squareup/hudtoaster/HudToaster;",
            "Lcom/squareup/ui/crm/flow/RefundBillHistoryWrapper;",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->flow:Lflow/Flow;

    .line 81
    iput-object p2, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->res:Lcom/squareup/util/Res;

    .line 82
    iput-object p3, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->billService:Lcom/squareup/server/bills/BillListServiceHelper;

    .line 83
    iput-object p4, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    .line 84
    iput-object p5, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 85
    iput-object p6, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    .line 86
    iput-object p7, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    .line 87
    iput-object p8, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->refundBillHistoryWrapper:Lcom/squareup/ui/crm/flow/RefundBillHistoryWrapper;

    .line 88
    iput-object p9, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/ui/crm/flow/BillHistoryFlow;)Lcom/squareup/billhistory/model/BillHistory;
    .locals 0

    .line 52
    iget-object p0, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->bill:Lcom/squareup/billhistory/model/BillHistory;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/ui/crm/flow/BillHistoryFlow;)Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 0

    .line 52
    iget-object p0, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/ui/crm/flow/BillHistoryFlow;)Lflow/Flow;
    .locals 0

    .line 52
    iget-object p0, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->flow:Lflow/Flow;

    return-object p0
.end method

.method public static synthetic lambda$5MImqD70cNvaMncSNvTbzu_F2T0(Lcom/squareup/ui/crm/flow/BillHistoryFlow;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/ui/crm/flow/BillHistoryFlow$BillHistoryOrServerError;
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->toBillHistoryOrServerError(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/ui/crm/flow/BillHistoryFlow$BillHistoryOrServerError;

    move-result-object p0

    return-object p0
.end method

.method private selectFirstTenderIfSingleTender()V
    .locals 2

    .line 310
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->bill:Lcom/squareup/billhistory/model/BillHistory;

    invoke-virtual {v0}, Lcom/squareup/billhistory/model/BillHistory;->isSplitTender()Z

    move-result v0

    if-nez v0, :cond_0

    .line 311
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->bill:Lcom/squareup/billhistory/model/BillHistory;

    invoke-virtual {v0}, Lcom/squareup/billhistory/model/BillHistory;->getTenders()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/billhistory/model/TenderHistory;

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->tender:Lcom/squareup/billhistory/model/TenderHistory;

    :cond_0
    return-void
.end method

.method private setFirstTenderAsPaymentForSingleTender()V
    .locals 2

    .line 321
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->bill:Lcom/squareup/billhistory/model/BillHistory;

    invoke-virtual {v0}, Lcom/squareup/billhistory/model/BillHistory;->isSplitTender()Z

    move-result v0

    if-nez v0, :cond_0

    .line 323
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->bill:Lcom/squareup/billhistory/model/BillHistory;

    invoke-virtual {v0}, Lcom/squareup/billhistory/model/BillHistory;->getTenders()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/billhistory/model/TenderHistory;

    invoke-virtual {v0}, Lcom/squareup/billhistory/model/TenderHistory;->getId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->paymentToken:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method private toBillHistoryOrServerError(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/ui/crm/flow/BillHistoryFlow$BillHistoryOrServerError;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/bills/GetBillByTenderServerIdResponse;",
            ">;)",
            "Lcom/squareup/ui/crm/flow/BillHistoryFlow$BillHistoryOrServerError;"
        }
    .end annotation

    .line 329
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    .line 330
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    .line 332
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/bills/GetBillByTenderServerIdResponse;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/GetBillByTenderServerIdResponse;->bill:Lcom/squareup/protos/client/bills/Bill;

    iget-object v0, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->res:Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    invoke-virtual {v1}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->isVoidCompAllowed()Z

    move-result v1

    invoke-static {p1, v0, v1}, Lcom/squareup/billhistory/Bills;->toBill(Lcom/squareup/protos/client/bills/Bill;Lcom/squareup/util/Res;Z)Lcom/squareup/billhistory/model/BillHistory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->bill:Lcom/squareup/billhistory/model/BillHistory;

    .line 333
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->bill:Lcom/squareup/billhistory/model/BillHistory;

    invoke-static {p1}, Lcom/squareup/ui/crm/flow/BillHistoryFlow$BillHistoryOrServerError;->withBillHistory(Lcom/squareup/billhistory/model/BillHistory;)Lcom/squareup/ui/crm/flow/BillHistoryFlow$BillHistoryOrServerError;

    move-result-object p1

    return-object p1

    .line 335
    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    .line 337
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->getOkayResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/bills/GetBillByTenderServerIdResponse;

    if-eqz p1, :cond_1

    .line 339
    iget-object v1, p1, Lcom/squareup/protos/client/bills/GetBillByTenderServerIdResponse;->status:Lcom/squareup/protos/client/Status;

    if-eqz v1, :cond_1

    iget-object v1, p1, Lcom/squareup/protos/client/bills/GetBillByTenderServerIdResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v1, v1, Lcom/squareup/protos/client/Status;->localized_title:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 340
    iget-object v0, p1, Lcom/squareup/protos/client/bills/GetBillByTenderServerIdResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v0, v0, Lcom/squareup/protos/client/Status;->localized_title:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/GetBillByTenderServerIdResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object p1, p1, Lcom/squareup/protos/client/Status;->localized_description:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/squareup/ui/crm/flow/BillHistoryFlow$BillHistoryOrServerError;->withError(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/ui/crm/flow/BillHistoryFlow$BillHistoryOrServerError;

    move-result-object p1

    return-object p1

    .line 343
    :cond_1
    invoke-virtual {v0}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getReceived()Lcom/squareup/receiving/ReceivedResponse;

    move-result-object p1

    instance-of p1, p1, Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;

    if-eqz p1, :cond_2

    .line 344
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/common/strings/R$string;->network_error_title:I

    .line 345
    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/common/strings/R$string;->network_error_message:I

    .line 346
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 344
    invoke-static {p1, v0}, Lcom/squareup/ui/crm/flow/BillHistoryFlow$BillHistoryOrServerError;->withError(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/ui/crm/flow/BillHistoryFlow$BillHistoryOrServerError;

    move-result-object p1

    return-object p1

    .line 348
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/common/strings/R$string;->server_error_title:I

    .line 349
    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/common/strings/R$string;->server_error_message:I

    .line 350
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 348
    invoke-static {p1, v0}, Lcom/squareup/ui/crm/flow/BillHistoryFlow$BillHistoryOrServerError;->withError(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/ui/crm/flow/BillHistoryFlow$BillHistoryOrServerError;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public billHistoryOrServerError()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/crm/flow/BillHistoryFlow$BillHistoryOrServerError;",
            ">;"
        }
    .end annotation

    .line 191
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->bill:Lcom/squareup/billhistory/model/BillHistory;

    if-eqz v0, :cond_0

    .line 192
    invoke-static {v0}, Lcom/squareup/ui/crm/flow/BillHistoryFlow$BillHistoryOrServerError;->withBillHistory(Lcom/squareup/billhistory/model/BillHistory;)Lcom/squareup/ui/crm/flow/BillHistoryFlow$BillHistoryOrServerError;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->billService:Lcom/squareup/server/bills/BillListServiceHelper;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->paymentToken:Ljava/lang/String;

    .line 193
    invoke-virtual {v0, v1}, Lcom/squareup/server/bills/BillListServiceHelper;->getBill(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$BillHistoryFlow$5MImqD70cNvaMncSNvTbzu_F2T0;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$BillHistoryFlow$5MImqD70cNvaMncSNvTbzu_F2T0;-><init>(Lcom/squareup/ui/crm/flow/BillHistoryFlow;)V

    .line 194
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    .line 195
    invoke-virtual {v0}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public closeBillHistoryCard()V
    .locals 2

    const/4 v0, 0x0

    .line 199
    iput-object v0, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->title:Ljava/lang/String;

    .line 200
    iput-object v0, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->paymentToken:Ljava/lang/String;

    .line 201
    iput-object v0, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->bill:Lcom/squareup/billhistory/model/BillHistory;

    .line 202
    iput-object v0, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->tender:Lcom/squareup/billhistory/model/TenderHistory;

    .line 203
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/crm/cards/BillHistoryScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public closeBillHistoryCardAndShowWarning(Lcom/squareup/widgets/warning/Warning;)V
    .locals 2

    .line 235
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->closeBillHistoryCard()V

    .line 236
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/register/widgets/WarningDialogScreen;

    invoke-direct {v1, p1}, Lcom/squareup/register/widgets/WarningDialogScreen;-><init>(Lcom/squareup/widgets/warning/Warning;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public closeIssueReceiptScreen()V
    .locals 2

    const/4 v0, 0x0

    .line 153
    iput-object v0, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->tender:Lcom/squareup/billhistory/model/TenderHistory;

    .line 154
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/activity/IssueReceiptScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public closeIssueRefundScreen()V
    .locals 2

    const/4 v0, 0x0

    .line 170
    iput-object v0, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->tender:Lcom/squareup/billhistory/model/TenderHistory;

    .line 171
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/activity/InIssueRefundScope;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public closeIssueRefundScreen(Lcom/squareup/billhistory/model/BillHistory;)V
    .locals 0

    .line 164
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->bill:Lcom/squareup/billhistory/model/BillHistory;

    const/4 p1, 0x0

    .line 165
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->tender:Lcom/squareup/billhistory/model/TenderHistory;

    .line 166
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->closeIssueRefundScreen()V

    return-void
.end method

.method public getBill()Lcom/squareup/billhistory/model/BillHistory;
    .locals 1

    .line 118
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->bill:Lcom/squareup/billhistory/model/BillHistory;

    return-object v0
.end method

.method public getMortarBundleKey()Ljava/lang/String;
    .locals 1

    .line 92
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPaymentIdForReceipt()Ljava/lang/String;
    .locals 2

    .line 134
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->bill:Lcom/squareup/billhistory/model/BillHistory;

    iget-object v0, v0, Lcom/squareup/billhistory/model/BillHistory;->id:Lcom/squareup/billhistory/model/BillHistoryId;

    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->getTender()Lcom/squareup/billhistory/model/TenderHistory;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/billhistory/model/BillHistoryId;->forTender(Lcom/squareup/billhistory/model/TenderHistory;)Lcom/squareup/billhistory/model/BillHistoryId;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/billhistory/model/BillHistoryId;->getPaymentIdForReceipt()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTender()Lcom/squareup/billhistory/model/TenderHistory;
    .locals 1

    .line 122
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->tender:Lcom/squareup/billhistory/model/TenderHistory;

    return-object v0
.end method

.method public getTitleForBillHistoryCard()Ljava/lang/String;
    .locals 1

    .line 187
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->title:Ljava/lang/String;

    return-object v0
.end method

.method public goBack()V
    .locals 1

    .line 179
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method public goBackBecauseBillIdChanged()V
    .locals 1

    .line 175
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public synthetic lambda$onEnterScope$0$BillHistoryFlow(Lcom/squareup/billhistory/model/BillHistory;)V
    .locals 0

    .line 97
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->bill:Lcom/squareup/billhistory/model/BillHistory;

    return-void
.end method

.method public onBillIdChanged()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 126
    invoke-static {}, Lio/reactivex/Observable;->empty()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 96
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->refundBillHistoryWrapper:Lcom/squareup/ui/crm/flow/RefundBillHistoryWrapper;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/flow/RefundBillHistoryWrapper;->getObservable()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$BillHistoryFlow$YOaDaUUHli0aMY0EXJfk2uqGzDk;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$BillHistoryFlow$YOaDaUUHli0aMY0EXJfk2uqGzDk;-><init>(Lcom/squareup/ui/crm/flow/BillHistoryFlow;)V

    .line 97
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 96
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    :cond_0
    const-string v0, "parentKey"

    .line 106
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/RegisterTreeKey;

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    const-string/jumbo v0, "title"

    .line 107
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->title:Ljava/lang/String;

    const-string v0, "paymentToken"

    .line 108
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->paymentToken:Ljava/lang/String;

    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    const-string v1, "parentKey"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 113
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->title:Ljava/lang/String;

    const-string/jumbo v1, "title"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->paymentToken:Ljava/lang/String;

    const-string v1, "paymentToken"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onTransactionsHistoryChanged()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 130
    invoke-static {}, Lio/reactivex/Observable;->empty()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public printGiftReceiptForSelectedTender(Ljava/lang/String;)V
    .locals 4

    .line 138
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_PRINTER_CONNECTED:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/billhistoryui/R$string;->printing_hud:I

    .line 139
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    .line 138
    invoke-interface {v0, v1, v2, v3}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 140
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->getBill()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/billhistory/model/BillHistory;->getTender(Ljava/lang/String;)Lcom/squareup/billhistory/model/TenderHistory;

    move-result-object p1

    .line 141
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    .line 142
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->getBill()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v1

    sget-object v2, Lcom/squareup/print/payload/ReceiptPayload$RenderType;->GIFT_RECEIPT:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    .line 141
    invoke-virtual {v0, v1, p1, v2}, Lcom/squareup/print/OrderPrintingDispatcher;->printReceipt(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/billhistory/model/TenderHistory;Lcom/squareup/print/payload/ReceiptPayload$RenderType;)V

    return-void
.end method

.method public reprintTicket()V
    .locals 4

    .line 229
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_PRINTER_CONNECTED:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/billhistoryui/R$string;->printing_hud:I

    .line 230
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    .line 229
    invoke-interface {v0, v1, v2, v3}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 231
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->getBill()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/print/OrderPrintingDispatcher;->reprintTicket(Lcom/squareup/billhistory/model/BillHistory;)V

    return-void
.end method

.method public showFirstIssueReceiptScreen()V
    .locals 4

    .line 211
    invoke-direct {p0}, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->selectFirstTenderIfSingleTender()V

    .line 212
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->flow:Lflow/Flow;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->tender:Lcom/squareup/billhistory/model/TenderHistory;

    if-nez v1, :cond_0

    new-instance v1, Lcom/squareup/ui/activity/SelectReceiptTenderScreen;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-direct {v1, v2}, Lcom/squareup/ui/activity/SelectReceiptTenderScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    goto :goto_0

    :cond_0
    new-instance v1, Lcom/squareup/ui/activity/IssueReceiptScreen;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/squareup/ui/activity/IssueReceiptScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Z)V

    :goto_0
    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showFirstIssueRefundScreen()V
    .locals 4

    .line 207
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v1, Lcom/squareup/permissions/Permission;->ISSUE_REFUNDS:Lcom/squareup/permissions/Permission;

    new-instance v2, Lcom/squareup/ui/crm/flow/BillHistoryFlow$OnIssueRefundAccessGranted;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/squareup/ui/crm/flow/BillHistoryFlow$OnIssueRefundAccessGranted;-><init>(Lcom/squareup/ui/crm/flow/BillHistoryFlow;Lcom/squareup/ui/crm/flow/BillHistoryFlow$1;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    return-void
.end method

.method public showFirstScreen(Lcom/squareup/ui/main/RegisterTreeKey;ZLcom/squareup/billhistory/model/BillHistory;)V
    .locals 2

    .line 240
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    .line 241
    iput-object p3, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->bill:Lcom/squareup/billhistory/model/BillHistory;

    const/4 p3, 0x0

    .line 242
    iput-object p3, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->tender:Lcom/squareup/billhistory/model/TenderHistory;

    .line 243
    iget-object p3, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->bill:Lcom/squareup/billhistory/model/BillHistory;

    iget-object v0, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->res:Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-static {p3, v0, v1}, Lcom/squareup/billhistory/Bills;->formatTitleOf(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;)Ljava/lang/CharSequence;

    move-result-object p3

    invoke-interface {p3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p3

    iput-object p3, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->title:Ljava/lang/String;

    .line 244
    invoke-direct {p0}, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->setFirstTenderAsPaymentForSingleTender()V

    .line 245
    iget-object p3, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/ui/crm/cards/BillHistoryScreen;

    invoke-direct {v0, p1, p2}, Lcom/squareup/ui/crm/cards/BillHistoryScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Z)V

    invoke-virtual {p3, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showFirstScreen(Lcom/squareup/ui/main/RegisterTreeKey;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 250
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    const/4 v0, 0x0

    .line 251
    iput-object v0, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->bill:Lcom/squareup/billhistory/model/BillHistory;

    .line 252
    iput-object v0, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->tender:Lcom/squareup/billhistory/model/TenderHistory;

    .line 253
    iput-object p3, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->title:Ljava/lang/String;

    const-string p3, "paymentToken"

    .line 254
    invoke-static {p4, p3}, Lcom/squareup/util/Preconditions;->nonBlank(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object p3

    check-cast p3, Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->paymentToken:Ljava/lang/String;

    .line 255
    iget-object p3, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->flow:Lflow/Flow;

    new-instance p4, Lcom/squareup/ui/crm/cards/BillHistoryScreen;

    invoke-direct {p4, p1, p2}, Lcom/squareup/ui/crm/cards/BillHistoryScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Z)V

    invoke-virtual {p3, p4}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showIssueReceiptScreen(Ljava/lang/String;)V
    .locals 2

    .line 146
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->bill:Lcom/squareup/billhistory/model/BillHistory;

    invoke-virtual {v0, p1}, Lcom/squareup/billhistory/model/BillHistory;->getTender(Ljava/lang/String;)Lcom/squareup/billhistory/model/TenderHistory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->tender:Lcom/squareup/billhistory/model/TenderHistory;

    .line 147
    new-instance p1, Lcom/squareup/ui/activity/IssueReceiptScreen;

    iget-object v0, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    const/4 v1, 0x0

    invoke-direct {p1, v0, v1}, Lcom/squareup/ui/activity/IssueReceiptScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Z)V

    .line 148
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->flow:Lflow/Flow;

    sget-object v1, Lflow/Direction;->FORWARD:Lflow/Direction;

    invoke-static {v0, p1, v1}, Lcom/squareup/container/Flows;->replaceTop(Lflow/Flow;Lcom/squareup/container/ContainerTreeKey;Lflow/Direction;)V

    const/4 v0, 0x1

    .line 149
    iput-boolean v0, p1, Lcom/squareup/ui/activity/IssueReceiptScreen;->isFirstScreen:Z

    return-void
.end method

.method public showSelectBuyerLanguageScreen()V
    .locals 1

    .line 183
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public showStartRefundScreen(Ljava/lang/String;)V
    .locals 1

    .line 158
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->bill:Lcom/squareup/billhistory/model/BillHistory;

    invoke-virtual {v0, p1}, Lcom/squareup/billhistory/model/BillHistory;->getTender(Ljava/lang/String;)Lcom/squareup/billhistory/model/TenderHistory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->tender:Lcom/squareup/billhistory/model/TenderHistory;

    .line 160
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1
.end method

.method public startPrintGiftReceiptFlow()V
    .locals 3

    .line 218
    invoke-direct {p0}, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->selectFirstTenderIfSingleTender()V

    .line 219
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->paymentToken:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 220
    invoke-virtual {p0, v0}, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->printGiftReceiptForSelectedTender(Ljava/lang/String;)V

    goto :goto_0

    .line 221
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->tender:Lcom/squareup/billhistory/model/TenderHistory;

    if-nez v0, :cond_1

    .line 222
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/activity/SelectGiftReceiptTenderScreen;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-direct {v1, v2}, Lcom/squareup/ui/activity/SelectGiftReceiptTenderScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 224
    :cond_1
    iget-object v0, v0, Lcom/squareup/billhistory/model/TenderHistory;->id:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->printGiftReceiptForSelectedTender(Ljava/lang/String;)V

    :goto_0
    return-void
.end method
