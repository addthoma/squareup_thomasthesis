.class public Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "SelectLoyaltyPhoneCoordinator.java"


# instance fields
.field private final phoneHelper:Lcom/squareup/text/PhoneNumberHelper;

.field private final pointsTermsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneScreen$Runner;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneScreen$Runner;Lcom/squareup/util/Res;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/loyalty/PointsTermsFormatter;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 41
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneCoordinator;->runner:Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneScreen$Runner;

    .line 43
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneCoordinator;->res:Lcom/squareup/util/Res;

    .line 44
    iput-object p3, p0, Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneCoordinator;->phoneHelper:Lcom/squareup/text/PhoneNumberHelper;

    .line 45
    iput-object p4, p0, Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneCoordinator;->pointsTermsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    return-void
.end method

.method static synthetic lambda$null$1(Landroid/widget/ProgressBar;Ljava/lang/Boolean;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 63
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method static synthetic lambda$null$3(Ljava/lang/Throwable;)Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 76
    instance-of v0, p0, Lretrofit/RetrofitError;

    if-eqz v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 79
    :cond_0
    new-instance v0, Lio/reactivex/exceptions/OnErrorNotImplementedException;

    invoke-direct {v0, p0}, Lio/reactivex/exceptions/OnErrorNotImplementedException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method static synthetic lambda$null$7(Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 p1, 0x1

    .line 113
    invoke-virtual {p0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    return-void
.end method


# virtual methods
.method addLoyaltyPhoneRow(Landroid/view/ViewGroup;)Lcom/squareup/ui/crm/rows/SelectableLoyaltyPhoneRow;
    .locals 1

    .line 117
    sget v0, Lcom/squareup/crm/R$layout;->crm_selectable_loyalty_phone_row:I

    .line 118
    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/rows/SelectableLoyaltyPhoneRow;

    .line 119
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-object v0
.end method

.method public attach(Landroid/view/View;)V
    .locals 5

    .line 49
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 51
    sget v0, Lcom/squareup/crmviewcustomer/R$id;->non_stable_action_bar:I

    .line 52
    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    .line 53
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    .line 54
    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/crm/applet/R$string;->crm_select_loyalty_phone:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 55
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneCoordinator;->runner:Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneScreen$Runner;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/crm/cards/-$$Lambda$myFjQPbv-Am1Qh_3ij6TLEGdaHI;

    invoke-direct {v2, v1}, Lcom/squareup/ui/crm/cards/-$$Lambda$myFjQPbv-Am1Qh_3ij6TLEGdaHI;-><init>(Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneScreen$Runner;)V

    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    .line 56
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/crm/applet/R$string;->crm_select_loyalty_phone_primary_button_label:I

    .line 57
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 56
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonText(Ljava/lang/CharSequence;)V

    const/4 v1, 0x0

    .line 58
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    .line 59
    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$SelectLoyaltyPhoneCoordinator$nBrICpH0-4Zu1ffVq7oadFmY6KU;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$SelectLoyaltyPhoneCoordinator$nBrICpH0-4Zu1ffVq7oadFmY6KU;-><init>(Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneCoordinator;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->showPrimaryButton(Ljava/lang/Runnable;)V

    .line 61
    sget v1, Lcom/squareup/crm/applet/R$id;->crm_progress_bar:I

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    .line 62
    new-instance v2, Lcom/squareup/ui/crm/cards/-$$Lambda$SelectLoyaltyPhoneCoordinator$q8Hkq7Q32LMvUsAGE6FUyynJQ0U;

    invoke-direct {v2, p0, v1}, Lcom/squareup/ui/crm/cards/-$$Lambda$SelectLoyaltyPhoneCoordinator$q8Hkq7Q32LMvUsAGE6FUyynJQ0U;-><init>(Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneCoordinator;Landroid/widget/ProgressBar;)V

    invoke-static {p1, v2}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 65
    sget v1, Lcom/squareup/crm/applet/R$id;->crm_message:I

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/widgets/MessageView;

    .line 66
    sget v2, Lcom/squareup/crm/applet/R$string;->crm_select_loyalty_phone_primary_message:I

    invoke-virtual {v1, v2}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 68
    sget v2, Lcom/squareup/crm/applet/R$id;->crm_select_loyalty_hint:I

    invoke-static {p1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 69
    iget-object v3, p0, Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneCoordinator;->pointsTermsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    sget v4, Lcom/squareup/crm/applet/R$string;->crm_select_loyalty_phone_secondary_message:I

    invoke-virtual {v3, v4}, Lcom/squareup/loyalty/PointsTermsFormatter;->plural(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    sget v2, Lcom/squareup/crm/applet/R$id;->crm_contact_list:I

    invoke-static {p1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 73
    new-instance v3, Lcom/squareup/ui/crm/cards/-$$Lambda$SelectLoyaltyPhoneCoordinator$zUrxmU5Ef_wq8Wxeoj607aK8tTI;

    invoke-direct {v3, p0, v2, v1}, Lcom/squareup/ui/crm/cards/-$$Lambda$SelectLoyaltyPhoneCoordinator$zUrxmU5Ef_wq8Wxeoj607aK8tTI;-><init>(Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneCoordinator;Landroid/widget/LinearLayout;Lcom/squareup/widgets/MessageView;)V

    invoke-static {p1, v3}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 111
    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$SelectLoyaltyPhoneCoordinator$Fmz2awxuFaXu30PFngkTQ9HcuW0;

    invoke-direct {v1, p0, v0}, Lcom/squareup/ui/crm/cards/-$$Lambda$SelectLoyaltyPhoneCoordinator$Fmz2awxuFaXu30PFngkTQ9HcuW0;-><init>(Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneCoordinator;Lcom/squareup/marin/widgets/MarinActionBar;)V

    invoke-static {p1, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public synthetic lambda$attach$0$SelectLoyaltyPhoneCoordinator()V
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneCoordinator;->runner:Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneScreen$Runner;->showMergeCustomersScreen()V

    return-void
.end method

.method public synthetic lambda$attach$2$SelectLoyaltyPhoneCoordinator(Landroid/widget/ProgressBar;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneCoordinator;->runner:Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneScreen$Runner;->selectLoyaltyPhoneScreenBusy()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$SelectLoyaltyPhoneCoordinator$3HdLvNcIyGE9acvrqDbTwRuHlxc;

    invoke-direct {v1, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$SelectLoyaltyPhoneCoordinator$3HdLvNcIyGE9acvrqDbTwRuHlxc;-><init>(Landroid/widget/ProgressBar;)V

    .line 63
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$attach$6$SelectLoyaltyPhoneCoordinator(Landroid/widget/LinearLayout;Lcom/squareup/widgets/MessageView;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneCoordinator;->runner:Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneScreen$Runner;->getLoyaltyAccounts()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/cards/-$$Lambda$SelectLoyaltyPhoneCoordinator$FJ9gARr14e27_BfTV3W5xHxbjw0;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$SelectLoyaltyPhoneCoordinator$FJ9gARr14e27_BfTV3W5xHxbjw0;

    .line 75
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$SelectLoyaltyPhoneCoordinator$i7ZkBO3OC3F6A6fiili0Qup_ln4;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/ui/crm/cards/-$$Lambda$SelectLoyaltyPhoneCoordinator$i7ZkBO3OC3F6A6fiili0Qup_ln4;-><init>(Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneCoordinator;Landroid/widget/LinearLayout;Lcom/squareup/widgets/MessageView;)V

    .line 82
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$attach$8$SelectLoyaltyPhoneCoordinator(Lcom/squareup/marin/widgets/MarinActionBar;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneCoordinator;->runner:Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneScreen$Runner;->getTargetLoyaltyAccountMapping()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$SelectLoyaltyPhoneCoordinator$KZ9Hf5xV7UDvj3Wsu96qPERJdio;

    invoke-direct {v1, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$SelectLoyaltyPhoneCoordinator$KZ9Hf5xV7UDvj3Wsu96qPERJdio;-><init>(Lcom/squareup/marin/widgets/MarinActionBar;)V

    .line 113
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$4$SelectLoyaltyPhoneCoordinator(Landroid/widget/LinearLayout;Landroid/view/View;)V
    .locals 1

    .line 100
    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->indexOfChild(Landroid/view/View;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneCoordinator;->selectRow(Landroid/view/ViewGroup;I)V

    .line 101
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneCoordinator;->runner:Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneScreen$Runner;

    check-cast p2, Lcom/squareup/ui/crm/rows/SelectableLoyaltyPhoneRow;

    .line 102
    invoke-virtual {p2}, Lcom/squareup/ui/crm/rows/SelectableLoyaltyPhoneRow;->getLoyaltyAccountMappingToken()Ljava/lang/String;

    move-result-object v0

    .line 103
    invoke-virtual {p2}, Lcom/squareup/ui/crm/rows/SelectableLoyaltyPhoneRow;->getPhoneNumber()Landroid/widget/TextView;

    move-result-object p2

    .line 104
    invoke-virtual {p2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object p2

    .line 105
    invoke-interface {p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p2

    .line 101
    invoke-interface {p1, v0, p2}, Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneScreen$Runner;->setTargetLoyaltyAccountMapping(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$null$5$SelectLoyaltyPhoneCoordinator(Landroid/widget/LinearLayout;Lcom/squareup/widgets/MessageView;Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 83
    invoke-virtual {p1}, Landroid/widget/LinearLayout;->removeAllViews()V

    if-nez p3, :cond_0

    .line 85
    sget p1, Lcom/squareup/crm/R$string;->crm_failed_to_load_customers:I

    invoke-virtual {p2, p1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    return-void

    .line 89
    :cond_0
    iget-object p2, p3, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;->loyaltyAccounts:Ljava/util/Map;

    invoke-interface {p2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p2

    const/4 p3, 0x1

    :cond_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/loyalty/LoyaltyAccount;

    .line 90
    iget-object v1, v0, Lcom/squareup/protos/client/loyalty/LoyaltyAccount;->mappings:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    .line 91
    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneCoordinator;->addLoyaltyPhoneRow(Landroid/view/ViewGroup;)Lcom/squareup/ui/crm/rows/SelectableLoyaltyPhoneRow;

    move-result-object v3

    .line 92
    iget-object v4, v0, Lcom/squareup/protos/client/loyalty/LoyaltyAccount;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/squareup/protos/client/loyalty/LoyaltyAccount;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    iget-object v4, v4, Lcom/squareup/protos/client/rolodex/Contact;->display_name:Ljava/lang/String;

    goto :goto_1

    :cond_2
    const-string v4, ""

    .line 95
    :goto_1
    invoke-virtual {v3, v4}, Lcom/squareup/ui/crm/rows/SelectableLoyaltyPhoneRow;->setContactName(Ljava/lang/String;)V

    .line 96
    iget-object v4, p0, Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneCoordinator;->phoneHelper:Lcom/squareup/text/PhoneNumberHelper;

    iget-object v5, v2, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->raw_id:Ljava/lang/String;

    invoke-interface {v4, v5}, Lcom/squareup/text/PhoneNumberHelper;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/squareup/ui/crm/rows/SelectableLoyaltyPhoneRow;->setPhoneNumber(Ljava/lang/String;)V

    add-int/lit8 v4, p3, 0x1

    .line 97
    invoke-virtual {v3, p3}, Lcom/squareup/ui/crm/rows/SelectableLoyaltyPhoneRow;->setPiiNameScrubberContentDescription(I)V

    .line 98
    iget-object p3, v2, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->loyalty_account_mapping_token:Ljava/lang/String;

    invoke-virtual {v3, p3}, Lcom/squareup/ui/crm/rows/SelectableLoyaltyPhoneRow;->setLoyaltyAccountMappingToken(Ljava/lang/String;)V

    .line 99
    new-instance p3, Lcom/squareup/ui/crm/cards/-$$Lambda$SelectLoyaltyPhoneCoordinator$NoiOqlaSlbwDncghaBjWDz8Nf2M;

    invoke-direct {p3, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$SelectLoyaltyPhoneCoordinator$NoiOqlaSlbwDncghaBjWDz8Nf2M;-><init>(Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneCoordinator;Landroid/widget/LinearLayout;)V

    invoke-virtual {v3, p3}, Lcom/squareup/ui/crm/rows/SelectableLoyaltyPhoneRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move p3, v4

    goto :goto_0

    :cond_3
    return-void
.end method

.method selectRow(Landroid/view/ViewGroup;I)V
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 124
    :goto_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 126
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/crm/rows/SelectableLoyaltyPhoneRow;

    if-ne v1, p2, :cond_0

    .line 128
    invoke-virtual {v2}, Lcom/squareup/ui/crm/rows/SelectableLoyaltyPhoneRow;->getChecked()Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    invoke-virtual {v2, v3}, Lcom/squareup/ui/crm/rows/SelectableLoyaltyPhoneRow;->setChecked(Z)V

    goto :goto_1

    .line 130
    :cond_0
    invoke-virtual {v2, v0}, Lcom/squareup/ui/crm/rows/SelectableLoyaltyPhoneRow;->setChecked(Z)V

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method
