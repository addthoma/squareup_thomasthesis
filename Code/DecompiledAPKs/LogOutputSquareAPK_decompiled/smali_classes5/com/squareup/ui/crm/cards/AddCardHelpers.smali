.class public Lcom/squareup/ui/crm/cards/AddCardHelpers;
.super Ljava/lang/Object;
.source "AddCardHelpers.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getEntryMethod(Lcom/squareup/Card;Lcom/squareup/payment/BillPayment;)Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;
    .locals 0

    if-nez p1, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 16
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/payment/BillPayment;->hasEmvTenderInFlight()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 17
    sget-object p0, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->EMV:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    return-object p0

    .line 19
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/Card;->isManual()Z

    move-result p0

    if-eqz p0, :cond_2

    sget-object p0, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->KEYED:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    goto :goto_0

    :cond_2
    sget-object p0, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->SWIPED:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    :goto_0
    return-object p0
.end method
