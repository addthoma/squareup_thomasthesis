.class public final Lcom/squareup/ui/crm/cards/loyalty/ConfirmVoidCouponDialogScreen;
.super Lcom/squareup/ui/crm/flow/InCrmScope;
.source "ConfirmVoidCouponDialogScreen.java"

# interfaces
.implements Lcom/squareup/container/MaybePersistent;


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/crm/cards/loyalty/ConfirmVoidCouponDialogScreen$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/cards/loyalty/ConfirmVoidCouponDialogScreen$Factory;
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/ui/crm/flow/CrmScope;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 24
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/flow/InCrmScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method


# virtual methods
.method public isPersistent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
