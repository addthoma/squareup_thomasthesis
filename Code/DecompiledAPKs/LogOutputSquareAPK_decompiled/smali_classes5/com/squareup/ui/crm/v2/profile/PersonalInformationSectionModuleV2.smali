.class public Lcom/squareup/ui/crm/v2/profile/PersonalInformationSectionModuleV2;
.super Ljava/lang/Object;
.source "PersonalInformationSectionModuleV2.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static providesEmailAppAvailable(Landroid/app/Application;)Z
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    const-string v0, "mailto:"

    .line 27
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/squareup/util/Intents;->hasLinkableAppToUri(Landroid/app/Application;Landroid/net/Uri;)Z

    move-result p0

    return p0
.end method

.method public static providesMapAppAvailable(Landroid/app/Application;)Z
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    const-string v0, "geo:0,0?q="

    .line 37
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/squareup/util/Intents;->hasLinkableAppToUri(Landroid/app/Application;Landroid/net/Uri;)Z

    move-result p0

    return p0
.end method

.method public static providesPhoneAppAvailable(Landroid/app/Application;)Z
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    const-string v0, "tel:"

    .line 32
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/squareup/util/Intents;->hasLinkableAppToUri(Landroid/app/Application;Landroid/net/Uri;)Z

    move-result p0

    return p0
.end method
