.class public final synthetic Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$Presenter$7KzYSICt2Bkim-3pXRHK5-xvU28;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# instance fields
.field private final synthetic f$0:Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Presenter;

.field private final synthetic f$1:Lcom/squareup/ui/crm/cards/AddCustomersToGroupView;

.field private final synthetic f$2:Ljava/lang/String;

.field private final synthetic f$3:Lcom/squareup/marin/widgets/MarinActionBar;

.field private final synthetic f$4:Ljava/util/concurrent/atomic/AtomicReference;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Presenter;Lcom/squareup/ui/crm/cards/AddCustomersToGroupView;Ljava/lang/String;Lcom/squareup/marin/widgets/MarinActionBar;Ljava/util/concurrent/atomic/AtomicReference;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$Presenter$7KzYSICt2Bkim-3pXRHK5-xvU28;->f$0:Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Presenter;

    iput-object p2, p0, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$Presenter$7KzYSICt2Bkim-3pXRHK5-xvU28;->f$1:Lcom/squareup/ui/crm/cards/AddCustomersToGroupView;

    iput-object p3, p0, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$Presenter$7KzYSICt2Bkim-3pXRHK5-xvU28;->f$2:Ljava/lang/String;

    iput-object p4, p0, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$Presenter$7KzYSICt2Bkim-3pXRHK5-xvU28;->f$3:Lcom/squareup/marin/widgets/MarinActionBar;

    iput-object p5, p0, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$Presenter$7KzYSICt2Bkim-3pXRHK5-xvU28;->f$4:Ljava/util/concurrent/atomic/AtomicReference;

    return-void
.end method


# virtual methods
.method public final invoke()Ljava/lang/Object;
    .locals 5

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$Presenter$7KzYSICt2Bkim-3pXRHK5-xvU28;->f$0:Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Presenter;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$Presenter$7KzYSICt2Bkim-3pXRHK5-xvU28;->f$1:Lcom/squareup/ui/crm/cards/AddCustomersToGroupView;

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$Presenter$7KzYSICt2Bkim-3pXRHK5-xvU28;->f$2:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$Presenter$7KzYSICt2Bkim-3pXRHK5-xvU28;->f$3:Lcom/squareup/marin/widgets/MarinActionBar;

    iget-object v4, p0, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$Presenter$7KzYSICt2Bkim-3pXRHK5-xvU28;->f$4:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Presenter;->lambda$onLoad$3$AddCustomersToGroupScreen$Presenter(Lcom/squareup/ui/crm/cards/AddCustomersToGroupView;Ljava/lang/String;Lcom/squareup/marin/widgets/MarinActionBar;Ljava/util/concurrent/atomic/AtomicReference;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method
