.class public Lcom/squareup/ui/crm/flow/SaveCardSharedState;
.super Ljava/lang/Object;
.source "SaveCardSharedState.java"


# instance fields
.field private card:Lcom/squareup/Card;

.field private cardData:Lcom/squareup/protos/client/bills/CardData;

.field public complete:Z

.field public email:Ljava/lang/String;

.field private entryMethod:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

.field public failureMessage:Ljava/lang/CharSequence;

.field public firstName:Ljava/lang/String;

.field public lastName:Ljava/lang/String;

.field private serverCardInfo:Lcom/squareup/protos/client/instruments/InstrumentSummary;

.field public success:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public clearCard()V
    .locals 1

    const/4 v0, 0x0

    .line 58
    iput-object v0, p0, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->card:Lcom/squareup/Card;

    .line 59
    iput-object v0, p0, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->cardData:Lcom/squareup/protos/client/bills/CardData;

    .line 60
    iput-object v0, p0, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->entryMethod:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    .line 61
    iput-object v0, p0, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->serverCardInfo:Lcom/squareup/protos/client/instruments/InstrumentSummary;

    return-void
.end method

.method public getCard()Lcom/squareup/Card;
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->card:Lcom/squareup/Card;

    return-object v0
.end method

.method public getCardData()Lcom/squareup/protos/client/bills/CardData;
    .locals 1

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->cardData:Lcom/squareup/protos/client/bills/CardData;

    return-object v0
.end method

.method public getEntryMethod()Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->entryMethod:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    return-object v0
.end method

.method public getServerCardInfo()Lcom/squareup/protos/client/instruments/InstrumentSummary;
    .locals 1

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->serverCardInfo:Lcom/squareup/protos/client/instruments/InstrumentSummary;

    return-object v0
.end method

.method public setCard(Lcom/squareup/Card;Lcom/squareup/protos/client/bills/CardData;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)V
    .locals 1

    const-string v0, "card should not be null"

    .line 49
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "cardData should not be null"

    .line 50
    invoke-static {p2, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "entryMethod should not be null"

    .line 51
    invoke-static {p3, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 52
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->card:Lcom/squareup/Card;

    .line 53
    iput-object p2, p0, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->cardData:Lcom/squareup/protos/client/bills/CardData;

    .line 54
    iput-object p3, p0, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->entryMethod:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    return-void
.end method

.method public setServerCardInfo(Lcom/squareup/protos/client/instruments/InstrumentSummary;)V
    .locals 0

    .line 81
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->serverCardInfo:Lcom/squareup/protos/client/instruments/InstrumentSummary;

    return-void
.end method
