.class public interface abstract Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneConflictDialogScreen$Runner;
.super Ljava/lang/Object;
.source "UpdateLoyaltyPhoneConflictDialogScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneConflictDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation


# virtual methods
.method public abstract cancelUpdateLoyaltyPhoneConflictScreen()V
.end method

.method public abstract getConflictingPhoneNumber()Ljava/lang/String;
.end method

.method public abstract showConflictingContact()V
.end method

.method public abstract showLoyaltyMergeProposal()V
.end method
