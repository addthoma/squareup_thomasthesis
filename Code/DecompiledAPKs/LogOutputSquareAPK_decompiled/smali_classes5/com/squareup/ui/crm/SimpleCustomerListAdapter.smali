.class public final Lcom/squareup/ui/crm/SimpleCustomerListAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "SimpleCustomerListAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/squareup/ui/crm/BaseCustomerListViewHolder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0004\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\u000c\u001a\u00020\rH\u0016J\u0010\u0010\u000e\u001a\u00020\r2\u0006\u0010\u000f\u001a\u00020\rH\u0016J\"\u0010\u0010\u001a\n \u0012*\u0004\u0018\u00010\u00110\u00112\u0006\u0010\u0013\u001a\u00020\u00142\u0008\u0008\u0001\u0010\u0015\u001a\u00020\rH\u0002J\u0018\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00022\u0006\u0010\u000f\u001a\u00020\rH\u0016J\u0018\u0010\u0019\u001a\u00020\u00022\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u001a\u001a\u00020\rH\u0016R0\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00052\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0008\u0010\t\"\u0004\u0008\n\u0010\u000b\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/ui/crm/SimpleCustomerListAdapter;",
        "Landroidx/recyclerview/widget/RecyclerView$Adapter;",
        "Lcom/squareup/ui/crm/BaseCustomerListViewHolder;",
        "()V",
        "value",
        "",
        "Lcom/squareup/ui/crm/BaseCustomerListItem;",
        "items",
        "getItems",
        "()Ljava/util/List;",
        "setItems",
        "(Ljava/util/List;)V",
        "getItemCount",
        "",
        "getItemViewType",
        "position",
        "inflate",
        "Landroid/view/View;",
        "kotlin.jvm.PlatformType",
        "parent",
        "Landroid/view/ViewGroup;",
        "res",
        "onBindViewHolder",
        "",
        "holder",
        "onCreateViewHolder",
        "viewType",
        "crm-choose-customer_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/ui/crm/BaseCustomerListItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 20
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    .line 22
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/SimpleCustomerListAdapter;->items:Ljava/util/List;

    return-void
.end method

.method private final inflate(Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 2

    .line 47
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p2, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public getItemCount()I
    .locals 1

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/crm/SimpleCustomerListAdapter;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/crm/SimpleCustomerListAdapter;->items:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/BaseCustomerListItem;

    invoke-virtual {p1}, Lcom/squareup/ui/crm/BaseCustomerListItem;->getOrdinal()I

    move-result p1

    return p1
.end method

.method public final getItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/ui/crm/BaseCustomerListItem;",
            ">;"
        }
    .end annotation

    .line 22
    iget-object v0, p0, Lcom/squareup/ui/crm/SimpleCustomerListAdapter;->items:Ljava/util/List;

    return-object v0
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .line 20
    check-cast p1, Lcom/squareup/ui/crm/BaseCustomerListViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/crm/SimpleCustomerListAdapter;->onBindViewHolder(Lcom/squareup/ui/crm/BaseCustomerListViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/squareup/ui/crm/BaseCustomerListViewHolder;I)V
    .locals 1

    const-string v0, "holder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/crm/SimpleCustomerListAdapter;->items:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/crm/BaseCustomerListItem;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/BaseCustomerListViewHolder;->bind(Lcom/squareup/ui/crm/BaseCustomerListItem;)V

    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    .line 20
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/crm/SimpleCustomerListAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/crm/BaseCustomerListViewHolder;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/crm/BaseCustomerListViewHolder;
    .locals 1

    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    const/4 v0, 0x1

    if-eq p2, v0, :cond_3

    const/4 v0, 0x2

    if-eq p2, v0, :cond_2

    const/4 v0, 0x3

    if-eq p2, v0, :cond_1

    const/4 v0, 0x4

    if-ne p2, v0, :cond_0

    .line 41
    new-instance p2, Lcom/squareup/ui/crm/BaseCustomerListViewHolder$ErrorViewHolder;

    sget v0, Lcom/squareup/crmchoosecustomer/R$layout;->crm_customer_list_error_row:I

    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/crm/SimpleCustomerListAdapter;->inflate(Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object p1

    const-string v0, "inflate(parent, R.layout\u2026_customer_list_error_row)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p2, p1}, Lcom/squareup/ui/crm/BaseCustomerListViewHolder$ErrorViewHolder;-><init>(Landroid/view/View;)V

    check-cast p2, Lcom/squareup/ui/crm/BaseCustomerListViewHolder;

    goto :goto_0

    .line 43
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Unexpected row type"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 38
    :cond_1
    new-instance p2, Lcom/squareup/ui/crm/BaseCustomerListViewHolder$CreateCustomerViewHolder;

    .line 39
    sget v0, Lcom/squareup/crmchoosecustomer/R$layout;->crm_choose_customer_list_create_customer_row:I

    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/crm/SimpleCustomerListAdapter;->inflate(Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object p1

    const-string v0, "inflate(parent, R.layout\u2026list_create_customer_row)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-direct {p2, p1}, Lcom/squareup/ui/crm/BaseCustomerListViewHolder$CreateCustomerViewHolder;-><init>(Landroid/view/View;)V

    check-cast p2, Lcom/squareup/ui/crm/BaseCustomerListViewHolder;

    goto :goto_0

    .line 36
    :cond_2
    new-instance p2, Lcom/squareup/ui/crm/BaseCustomerListViewHolder$LoadingViewHolder;

    sget v0, Lcom/squareup/crmchoosecustomer/R$layout;->crm_customer_list_loading_row:I

    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/crm/SimpleCustomerListAdapter;->inflate(Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object p1

    const-string v0, "inflate(parent, R.layout\u2026ustomer_list_loading_row)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p2, p1}, Lcom/squareup/ui/crm/BaseCustomerListViewHolder$LoadingViewHolder;-><init>(Landroid/view/View;)V

    check-cast p2, Lcom/squareup/ui/crm/BaseCustomerListViewHolder;

    goto :goto_0

    .line 34
    :cond_3
    new-instance p2, Lcom/squareup/ui/crm/BaseCustomerListViewHolder$ContactViewHolder;

    new-instance v0, Lcom/squareup/ui/crm/rows/CustomerUnitRow;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/ui/crm/rows/CustomerUnitRow;-><init>(Landroid/content/Context;)V

    invoke-direct {p2, v0}, Lcom/squareup/ui/crm/BaseCustomerListViewHolder$ContactViewHolder;-><init>(Lcom/squareup/ui/crm/rows/CustomerUnitRow;)V

    check-cast p2, Lcom/squareup/ui/crm/BaseCustomerListViewHolder;

    goto :goto_0

    .line 32
    :cond_4
    new-instance p2, Lcom/squareup/ui/crm/BaseCustomerListViewHolder$HeaderViewHolder;

    sget v0, Lcom/squareup/crmchoosecustomer/R$layout;->crm_customer_list_header_row:I

    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/crm/SimpleCustomerListAdapter;->inflate(Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object p1

    const-string v0, "inflate(parent, R.layout\u2026customer_list_header_row)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p2, p1}, Lcom/squareup/ui/crm/BaseCustomerListViewHolder$HeaderViewHolder;-><init>(Landroid/view/View;)V

    check-cast p2, Lcom/squareup/ui/crm/BaseCustomerListViewHolder;

    :goto_0
    return-object p2
.end method

.method public final setItems(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/ui/crm/BaseCustomerListItem;",
            ">;)V"
        }
    .end annotation

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    iput-object p1, p0, Lcom/squareup/ui/crm/SimpleCustomerListAdapter;->items:Ljava/util/List;

    .line 25
    invoke-virtual {p0}, Lcom/squareup/ui/crm/SimpleCustomerListAdapter;->notifyDataSetChanged()V

    return-void
.end method
