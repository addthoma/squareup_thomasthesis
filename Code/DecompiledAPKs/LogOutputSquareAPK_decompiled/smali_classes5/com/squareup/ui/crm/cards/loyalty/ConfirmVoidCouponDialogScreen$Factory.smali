.class public final Lcom/squareup/ui/crm/cards/loyalty/ConfirmVoidCouponDialogScreen$Factory;
.super Ljava/lang/Object;
.source "ConfirmVoidCouponDialogScreen.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/loyalty/ConfirmVoidCouponDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$create$0(Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$Runner;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 41
    invoke-interface {p0}, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$Runner;->confirmVoid()V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 33
    const-class v0, Lcom/squareup/ui/crm/flow/CrmScope$SharedCustomerProfileComponent;

    .line 34
    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/flow/CrmScope$SharedCustomerProfileComponent;

    .line 35
    invoke-interface {v0}, Lcom/squareup/ui/crm/flow/CrmScope$SharedCustomerProfileComponent;->manageCouponsAndRewardsScreen()Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$Component;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$Component;->runner()Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$Runner;

    move-result-object v0

    .line 37
    new-instance v1, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v1, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget p1, Lcom/squareup/crmviewcustomer/R$string;->crm_coupons_and_rewards_void_confirm_title:I

    .line 38
    invoke-virtual {v1, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 39
    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$Runner;->getConfirmCopy()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v1, Lcom/squareup/crmscreens/R$string;->crm_coupons_and_rewards_void_action:I

    new-instance v2, Lcom/squareup/ui/crm/cards/loyalty/-$$Lambda$ConfirmVoidCouponDialogScreen$Factory$PJzGwZiYUgzdrc_SV-2oTgXH71A;

    invoke-direct {v2, v0}, Lcom/squareup/ui/crm/cards/loyalty/-$$Lambda$ConfirmVoidCouponDialogScreen$Factory$PJzGwZiYUgzdrc_SV-2oTgXH71A;-><init>(Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$Runner;)V

    .line 40
    invoke-virtual {p1, v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/marin/R$drawable;->marin_selector_red:I

    .line 42
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButtonBackground(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/marin/R$color;->marin_white:I

    .line 43
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButtonTextColor(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    const/4 v0, 0x1

    .line 44
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setCancelable(Z)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/common/strings/R$string;->cancel:I

    .line 45
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 46
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 37
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
