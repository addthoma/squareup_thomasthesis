.class public Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$CouponItem;
.super Ljava/lang/Object;
.source "ManageCouponsAndRewardsScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CouponItem"
.end annotation


# instance fields
.field public checked:Z

.field public coupon:Lcom/squareup/protos/client/coupons/Coupon;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/client/coupons/Coupon;Z)V
    .locals 0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$CouponItem;->coupon:Lcom/squareup/protos/client/coupons/Coupon;

    .line 46
    iput-boolean p2, p0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$CouponItem;->checked:Z

    return-void
.end method
