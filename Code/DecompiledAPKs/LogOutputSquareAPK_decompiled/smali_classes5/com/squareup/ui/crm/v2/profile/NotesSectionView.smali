.class public Lcom/squareup/ui/crm/v2/profile/NotesSectionView;
.super Landroid/widget/LinearLayout;
.source "NotesSectionView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/v2/profile/NotesSectionView$ViewData;
    }
.end annotation


# instance fields
.field private final allNotes:Landroid/view/View;

.field private final noteClicked:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lcom/squareup/protos/client/rolodex/Note;",
            ">;"
        }
    .end annotation
.end field

.field private final onAllNotesClicked:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final rows:Landroid/widget/LinearLayout;

.field private final sectionHeader:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 35
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/NotesSectionView;->noteClicked:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 37
    sget p2, Lcom/squareup/crmviewcustomer/R$layout;->crm_notes_section_view_v2:I

    invoke-static {p1, p2, p0}, Lcom/squareup/ui/crm/v2/profile/NotesSectionView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    const/4 p1, 0x1

    .line 38
    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/v2/profile/NotesSectionView;->setOrientation(I)V

    .line 40
    sget p1, Lcom/squareup/crmviewcustomer/R$id;->crm_section_header:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/NotesSectionView;->sectionHeader:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    .line 42
    sget p1, Lcom/squareup/crmviewcustomer/R$id;->crm_notes_rows:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/NotesSectionView;->rows:Landroid/widget/LinearLayout;

    .line 43
    sget p1, Lcom/squareup/crmviewcustomer/R$id;->crm_notes_all_notes:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/NotesSectionView;->allNotes:Landroid/view/View;

    .line 45
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/NotesSectionView;->sectionHeader:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/profile/NotesSectionView;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget v0, Lcom/squareup/crmviewcustomer/R$string;->crm_notes_section_header_uppercase:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;->setTitle(Ljava/lang/String;)V

    .line 46
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/NotesSectionView;->sectionHeader:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/profile/NotesSectionView;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget v0, Lcom/squareup/crmviewcustomer/R$string;->crm_add_note_label:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    sget-object v0, Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;->ENABLED:Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;

    invoke-virtual {p1, p2, v0}, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;->setActionButton(Ljava/lang/String;Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;)V

    .line 49
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/NotesSectionView;->allNotes:Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/rx2/Rx2Views;->debouncedOnClicked(Landroid/view/View;)Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/NotesSectionView;->onAllNotesClicked:Lio/reactivex/Observable;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/crm/v2/profile/NotesSectionView;)Lcom/jakewharton/rxrelay2/PublishRelay;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/squareup/ui/crm/v2/profile/NotesSectionView;->noteClicked:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object p0
.end method

.method private bindRow(Lcom/squareup/ui/account/view/SmartLineRow;Lcom/squareup/protos/client/rolodex/Note;Z)V
    .locals 1

    .line 100
    iget-object v0, p2, Lcom/squareup/protos/client/rolodex/Note;->body:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleText(Ljava/lang/CharSequence;)V

    .line 101
    new-instance v0, Lcom/squareup/ui/crm/v2/profile/NotesSectionView$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/crm/v2/profile/NotesSectionView$1;-><init>(Lcom/squareup/ui/crm/v2/profile/NotesSectionView;Lcom/squareup/protos/client/rolodex/Note;)V

    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    if-eqz p3, :cond_0

    .line 108
    iget-object p3, p2, Lcom/squareup/protos/client/rolodex/Note;->reminder:Lcom/squareup/protos/client/rolodex/Reminder;

    if-eqz p3, :cond_0

    iget-object p2, p2, Lcom/squareup/protos/client/rolodex/Note;->reminder:Lcom/squareup/protos/client/rolodex/Reminder;

    iget-object p2, p2, Lcom/squareup/protos/client/rolodex/Reminder;->scheduled_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz p2, :cond_0

    .line 109
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/profile/NotesSectionView;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget p3, Lcom/squareup/crmscreens/R$string;->crm_reminder:I

    invoke-virtual {p2, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueText(Ljava/lang/CharSequence;)V

    const/4 p2, 0x1

    .line 110
    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueVisible(Z)V

    .line 111
    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setPreserveValueText(Z)V

    :cond_0
    return-void
.end method


# virtual methods
.method public onAddNoteClicked()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/NotesSectionView;->sectionHeader:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;->onActionClicked()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public onAllNotesClicked()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/NotesSectionView;->onAllNotesClicked:Lio/reactivex/Observable;

    return-object v0
.end method

.method public onNoteClicked()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/protos/client/rolodex/Note;",
            ">;"
        }
    .end annotation

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/NotesSectionView;->noteClicked:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object v0
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .line 54
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not implemented. Set saveEnabled to false."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setViewData(Lcom/squareup/ui/crm/v2/profile/NotesSectionView$ViewData;)V
    .locals 4

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/NotesSectionView;->rows:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 75
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/NotesSectionView;->allNotes:Landroid/view/View;

    iget-boolean v1, p1, Lcom/squareup/ui/crm/v2/profile/NotesSectionView$ViewData;->showAll:Z

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/NotesSectionView;->sectionHeader:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    iget-object v1, p1, Lcom/squareup/ui/crm/v2/profile/NotesSectionView$ViewData;->notes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;->setDividerVisible(Z)V

    .line 78
    iget-object v0, p1, Lcom/squareup/ui/crm/v2/profile/NotesSectionView$ViewData;->notes:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/rolodex/Note;

    .line 79
    sget v2, Lcom/squareup/crmviewcustomer/R$layout;->crm_v2_profile_smartlinerow:I

    iget-object v3, p0, Lcom/squareup/ui/crm/v2/profile/NotesSectionView;->rows:Landroid/widget/LinearLayout;

    invoke-static {v2, v3}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/account/view/SmartLineRow;

    .line 80
    iget-object v3, p0, Lcom/squareup/ui/crm/v2/profile/NotesSectionView;->rows:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 81
    iget-boolean v3, p1, Lcom/squareup/ui/crm/v2/profile/NotesSectionView$ViewData;->showReminders:Z

    invoke-direct {p0, v2, v1, v3}, Lcom/squareup/ui/crm/v2/profile/NotesSectionView;->bindRow(Lcom/squareup/ui/account/view/SmartLineRow;Lcom/squareup/protos/client/rolodex/Note;Z)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method setVisible(Z)V
    .locals 0

    .line 58
    invoke-static {p0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method
