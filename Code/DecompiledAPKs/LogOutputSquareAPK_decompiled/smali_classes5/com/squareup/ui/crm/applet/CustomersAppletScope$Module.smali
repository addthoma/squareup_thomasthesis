.class public abstract Lcom/squareup/ui/crm/applet/CustomersAppletScope$Module;
.super Ljava/lang/Object;
.source "CustomersAppletScope.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/applet/CustomersAppletScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Module"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract allCustomersMasterController(Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;)Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract createManualGroupRunner(Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;)Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract createMessageListScreenV2Runer(Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;)Lcom/squareup/ui/crm/v2/MessageListScreenV2$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract mergingDuplicatesDialogScreenController(Lcom/squareup/ui/crm/flow/ResolveDuplicatesFlow;)Lcom/squareup/ui/crm/cards/MergingDuplicatesScreen$Controller;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract noCustomerSelectedDetailCoordinatorEvents(Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;)Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailCoordinator$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract resolveDuplicatesScreenController(Lcom/squareup/ui/crm/flow/ResolveDuplicatesFlow;)Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen$Controller;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract viewGroupMasterCoordinator(Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;)Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract viewGroupsListCoordinatorRunner(Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;)Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
