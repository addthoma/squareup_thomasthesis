.class final enum Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;
.super Ljava/lang/Enum;
.source "ChooseGroupsScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "VisualState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;

.field public static final enum SHOWING_ERROR_MESSAGE:Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;

.field public static final enum SHOWING_NO_GROUPS_MESSAGE:Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;

.field public static final enum SHOWING_SOME_GROUPS:Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 84
    new-instance v0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;

    const/4 v1, 0x0

    const-string v2, "SHOWING_SOME_GROUPS"

    invoke-direct {v0, v2, v1}, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;->SHOWING_SOME_GROUPS:Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;

    .line 85
    new-instance v0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;

    const/4 v2, 0x1

    const-string v3, "SHOWING_NO_GROUPS_MESSAGE"

    invoke-direct {v0, v3, v2}, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;->SHOWING_NO_GROUPS_MESSAGE:Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;

    .line 86
    new-instance v0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;

    const/4 v3, 0x2

    const-string v4, "SHOWING_ERROR_MESSAGE"

    invoke-direct {v0, v4, v3}, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;->SHOWING_ERROR_MESSAGE:Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;

    .line 83
    sget-object v4, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;->SHOWING_SOME_GROUPS:Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;->SHOWING_NO_GROUPS_MESSAGE:Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;->SHOWING_ERROR_MESSAGE:Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;->$VALUES:[Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 83
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;
    .locals 1

    .line 83
    const-class v0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;
    .locals 1

    .line 83
    sget-object v0, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;->$VALUES:[Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;

    invoke-virtual {v0}, [Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter$VisualState;

    return-object v0
.end method
