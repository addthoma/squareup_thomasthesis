.class public interface abstract Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$Runner;
.super Ljava/lang/Object;
.source "UpdateCustomerScreenV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation


# virtual methods
.method public abstract X2displayCustomerDetailsOnBran(Lcom/squareup/x2/customers/CustomerInfoWithState$State;)V
.end method

.method public abstract addressBookClicked()V
.end method

.method public abstract analyticsPathName()Ljava/lang/String;
.end method

.method public abstract closeUpdateCustomerScreen()V
.end method

.method public abstract contactChangedByDialogScreen()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getContact()Lcom/squareup/protos/client/rolodex/Contact;
.end method

.method public abstract getScreenData()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$ScreenData;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getTitle()Ljava/lang/String;
.end method

.method public abstract getType()Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;
.end method

.method public abstract onContactUpdated(Lcom/squareup/protos/client/rolodex/Contact;)V
.end method

.method public abstract saveContact()V
.end method

.method public abstract shouldShowAddressBookButton()Z
.end method

.method public abstract showChooseCustomDateAttributeScreen(Lcom/squareup/ui/crm/rows/EditDateAttributeRow$DateAttribute;)V
.end method

.method public abstract showChooseEnumAttributeScreen(Lcom/squareup/ui/crm/rows/EnumAttribute;)V
.end method

.method public abstract showChooseGroupsScreen()V
.end method
