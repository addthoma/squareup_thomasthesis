.class Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "BirthdayPicker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;)V
    .locals 0

    .line 69
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker$1;->this$0:Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 0

    .line 71
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker$1;->this$0:Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;

    invoke-static {p1}, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->access$000(Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;)Lcom/squareup/marin/widgets/MarinCheckBox;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinCheckBox;->toggle()V

    .line 72
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker$1;->this$0:Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;

    invoke-static {p1}, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->access$000(Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;)Lcom/squareup/marin/widgets/MarinCheckBox;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinCheckBox;->isChecked()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 73
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker$1;->this$0:Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;

    invoke-static {p1}, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->access$100(Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;)Landroid/animation/Animator;

    move-result-object p1

    invoke-virtual {p1}, Landroid/animation/Animator;->cancel()V

    .line 74
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker$1;->this$0:Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;

    invoke-static {p1}, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->access$200(Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;)Landroid/animation/Animator;

    move-result-object p1

    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    goto :goto_0

    .line 76
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker$1;->this$0:Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;

    invoke-static {p1}, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->access$200(Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;)Landroid/animation/Animator;

    move-result-object p1

    invoke-virtual {p1}, Landroid/animation/Animator;->cancel()V

    .line 77
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker$1;->this$0:Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;

    invoke-static {p1}, Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;->access$100(Lcom/squareup/ui/crm/cards/birthday/BirthdayPicker;)Landroid/animation/Animator;

    move-result-object p1

    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    :goto_0
    return-void
.end method
