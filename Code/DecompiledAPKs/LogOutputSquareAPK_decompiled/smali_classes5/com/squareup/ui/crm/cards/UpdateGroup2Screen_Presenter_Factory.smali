.class public final Lcom/squareup/ui/crm/cards/UpdateGroup2Screen_Presenter_Factory;
.super Ljava/lang/Object;
.source "UpdateGroup2Screen_Presenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final controllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Controller;",
            ">;"
        }
    .end annotation
.end field

.field private final errorBarProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final filterHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/filters/FilterHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final rolodexProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Controller;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/filters/FilterHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            ">;)V"
        }
    .end annotation

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen_Presenter_Factory;->controllerProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen_Presenter_Factory;->errorBarProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p3, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p4, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen_Presenter_Factory;->filterHelperProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p5, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen_Presenter_Factory;->rolodexProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/crm/cards/UpdateGroup2Screen_Presenter_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Controller;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/filters/FilterHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            ">;)",
            "Lcom/squareup/ui/crm/cards/UpdateGroup2Screen_Presenter_Factory;"
        }
    .end annotation

    .line 50
    new-instance v6, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen_Presenter_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen_Presenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Controller;Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/util/Res;Lcom/squareup/crm/filters/FilterHelper;Lcom/squareup/crm/RolodexServiceHelper;)Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;
    .locals 7

    .line 56
    new-instance v6, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;-><init>(Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Controller;Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/util/Res;Lcom/squareup/crm/filters/FilterHelper;Lcom/squareup/crm/RolodexServiceHelper;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;
    .locals 5

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen_Presenter_Factory;->controllerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Controller;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen_Presenter_Factory;->errorBarProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/ErrorsBarPresenter;

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/util/Res;

    iget-object v3, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen_Presenter_Factory;->filterHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/crm/filters/FilterHelper;

    iget-object v4, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen_Presenter_Factory;->rolodexProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/crm/RolodexServiceHelper;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen_Presenter_Factory;->newInstance(Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Controller;Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/util/Res;Lcom/squareup/crm/filters/FilterHelper;Lcom/squareup/crm/RolodexServiceHelper;)Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen_Presenter_Factory;->get()Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;

    move-result-object v0

    return-object v0
.end method
