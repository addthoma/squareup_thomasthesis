.class public final Lcom/squareup/ui/crm/flow/UpdateCustomerModule_BindUpdateCustomerFlowFactory;
.super Ljava/lang/Object;
.source "UpdateCustomerModule_BindUpdateCustomerFlowFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;",
        ">;"
    }
.end annotation


# instance fields
.field private final editFlowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/edit/RealEditCustomerFlow;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final updateFlowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/flow/RealUpdateCustomerFlow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/flow/RealUpdateCustomerFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/edit/RealEditCustomerFlow;",
            ">;)V"
        }
    .end annotation

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/UpdateCustomerModule_BindUpdateCustomerFlowFactory;->featuresProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p2, p0, Lcom/squareup/ui/crm/flow/UpdateCustomerModule_BindUpdateCustomerFlowFactory;->updateFlowProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p3, p0, Lcom/squareup/ui/crm/flow/UpdateCustomerModule_BindUpdateCustomerFlowFactory;->editFlowProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static bindUpdateCustomerFlow(Lcom/squareup/settings/server/Features;Lcom/squareup/ui/crm/flow/RealUpdateCustomerFlow;Lcom/squareup/ui/crm/edit/RealEditCustomerFlow;)Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;
    .locals 0

    .line 47
    invoke-static {p0, p1, p2}, Lcom/squareup/ui/crm/flow/UpdateCustomerModule;->bindUpdateCustomerFlow(Lcom/squareup/settings/server/Features;Lcom/squareup/ui/crm/flow/RealUpdateCustomerFlow;Lcom/squareup/ui/crm/edit/RealEditCustomerFlow;)Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;

    return-object p0
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/crm/flow/UpdateCustomerModule_BindUpdateCustomerFlowFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/flow/RealUpdateCustomerFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/edit/RealEditCustomerFlow;",
            ">;)",
            "Lcom/squareup/ui/crm/flow/UpdateCustomerModule_BindUpdateCustomerFlowFactory;"
        }
    .end annotation

    .line 42
    new-instance v0, Lcom/squareup/ui/crm/flow/UpdateCustomerModule_BindUpdateCustomerFlowFactory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/crm/flow/UpdateCustomerModule_BindUpdateCustomerFlowFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;
    .locals 3

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/UpdateCustomerModule_BindUpdateCustomerFlowFactory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/Features;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/UpdateCustomerModule_BindUpdateCustomerFlowFactory;->updateFlowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/crm/flow/RealUpdateCustomerFlow;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/UpdateCustomerModule_BindUpdateCustomerFlowFactory;->editFlowProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/crm/edit/RealEditCustomerFlow;

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/crm/flow/UpdateCustomerModule_BindUpdateCustomerFlowFactory;->bindUpdateCustomerFlow(Lcom/squareup/settings/server/Features;Lcom/squareup/ui/crm/flow/RealUpdateCustomerFlow;Lcom/squareup/ui/crm/edit/RealEditCustomerFlow;)Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/UpdateCustomerModule_BindUpdateCustomerFlowFactory;->get()Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;

    move-result-object v0

    return-object v0
.end method
