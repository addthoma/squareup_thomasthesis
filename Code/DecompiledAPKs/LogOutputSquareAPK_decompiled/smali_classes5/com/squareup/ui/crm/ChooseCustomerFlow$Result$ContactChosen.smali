.class public final Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;
.super Lcom/squareup/ui/crm/ChooseCustomerFlow$Result;
.source "ChooseCustomerFlow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/ChooseCustomerFlow$Result;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ContactChosen"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0015\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001BM\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0012\u0010\u0008\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\n0\t\u0012\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\n0\t\u0012\u0006\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJ\t\u0010\u001a\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001b\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001c\u001a\u00020\u0007H\u00c6\u0003J\u0015\u0010\u001d\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\n0\tH\u00c6\u0003J\u0015\u0010\u001e\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\n0\tH\u00c6\u0003J\t\u0010\u001f\u001a\u00020\rH\u00c6\u0003J]\u0010 \u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0014\u0008\u0002\u0010\u0008\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\n0\t2\u0014\u0008\u0002\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\n0\t2\u0008\u0008\u0002\u0010\u000c\u001a\u00020\rH\u00c6\u0001J\u0013\u0010!\u001a\u00020\u00072\u0008\u0010\"\u001a\u0004\u0018\u00010#H\u00d6\u0003J\t\u0010$\u001a\u00020%H\u00d6\u0001J\t\u0010&\u001a\u00020\'H\u00d6\u0001R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u001d\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\n0\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016R\u001d\u0010\u0008\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\n0\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0016R\u0011\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019\u00a8\u0006("
    }
    d2 = {
        "Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;",
        "Lcom/squareup/ui/crm/ChooseCustomerFlow$Result;",
        "chooseCustomerResultKey",
        "Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;",
        "contact",
        "Lcom/squareup/protos/client/rolodex/Contact;",
        "contactChosenFromRecent",
        "",
        "historyFuncForConfirmation",
        "Lkotlin/Function1;",
        "Lflow/History$Builder;",
        "historyFuncForBackout",
        "parentKeyForChild",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "(Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;Lcom/squareup/protos/client/rolodex/Contact;ZLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lcom/squareup/ui/main/RegisterTreeKey;)V",
        "getChooseCustomerResultKey",
        "()Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;",
        "getContact",
        "()Lcom/squareup/protos/client/rolodex/Contact;",
        "getContactChosenFromRecent",
        "()Z",
        "getHistoryFuncForBackout",
        "()Lkotlin/jvm/functions/Function1;",
        "getHistoryFuncForConfirmation",
        "getParentKeyForChild",
        "()Lcom/squareup/ui/main/RegisterTreeKey;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "crm-choose-customer_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final chooseCustomerResultKey:Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;

.field private final contact:Lcom/squareup/protos/client/rolodex/Contact;

.field private final contactChosenFromRecent:Z

.field private final historyFuncForBackout:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lflow/History$Builder;",
            "Lflow/History$Builder;",
            ">;"
        }
    .end annotation
.end field

.field private final historyFuncForConfirmation:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lflow/History$Builder;",
            "Lflow/History$Builder;",
            ">;"
        }
    .end annotation
.end field

.field private final parentKeyForChild:Lcom/squareup/ui/main/RegisterTreeKey;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;Lcom/squareup/protos/client/rolodex/Contact;ZLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            "Z",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lflow/History$Builder;",
            "Lflow/History$Builder;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lflow/History$Builder;",
            "Lflow/History$Builder;",
            ">;",
            "Lcom/squareup/ui/main/RegisterTreeKey;",
            ")V"
        }
    .end annotation

    const-string v0, "chooseCustomerResultKey"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "contact"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "historyFuncForConfirmation"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "historyFuncForBackout"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parentKeyForChild"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 65
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result;-><init>(Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->chooseCustomerResultKey:Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;

    iput-object p2, p0, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    iput-boolean p3, p0, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->contactChosenFromRecent:Z

    iput-object p4, p0, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->historyFuncForConfirmation:Lkotlin/jvm/functions/Function1;

    iput-object p5, p0, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->historyFuncForBackout:Lkotlin/jvm/functions/Function1;

    iput-object p6, p0, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->parentKeyForChild:Lcom/squareup/ui/main/RegisterTreeKey;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;Lcom/squareup/protos/client/rolodex/Contact;ZLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lcom/squareup/ui/main/RegisterTreeKey;ILjava/lang/Object;)Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->getChooseCustomerResultKey()Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;

    move-result-object p1

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    iget-object p2, p0, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    :cond_1
    move-object p8, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    iget-boolean p3, p0, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->contactChosenFromRecent:Z

    :cond_2
    move v0, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->historyFuncForConfirmation:Lkotlin/jvm/functions/Function1;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->historyFuncForBackout:Lkotlin/jvm/functions/Function1;

    :cond_4
    move-object v2, p5

    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_5

    iget-object p6, p0, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->parentKeyForChild:Lcom/squareup/ui/main/RegisterTreeKey;

    :cond_5
    move-object v3, p6

    move-object p2, p0

    move-object p3, p1

    move-object p4, p8

    move p5, v0

    move-object p6, v1

    move-object p7, v2

    move-object p8, v3

    invoke-virtual/range {p2 .. p8}, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->copy(Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;Lcom/squareup/protos/client/rolodex/Contact;ZLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lcom/squareup/ui/main/RegisterTreeKey;)Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->getChooseCustomerResultKey()Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;

    move-result-object v0

    return-object v0
.end method

.method public final component2()Lcom/squareup/protos/client/rolodex/Contact;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    return-object v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->contactChosenFromRecent:Z

    return v0
.end method

.method public final component4()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lflow/History$Builder;",
            "Lflow/History$Builder;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->historyFuncForConfirmation:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final component5()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lflow/History$Builder;",
            "Lflow/History$Builder;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->historyFuncForBackout:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final component6()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->parentKeyForChild:Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method

.method public final copy(Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;Lcom/squareup/protos/client/rolodex/Contact;ZLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lcom/squareup/ui/main/RegisterTreeKey;)Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            "Z",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lflow/History$Builder;",
            "Lflow/History$Builder;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lflow/History$Builder;",
            "Lflow/History$Builder;",
            ">;",
            "Lcom/squareup/ui/main/RegisterTreeKey;",
            ")",
            "Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;"
        }
    .end annotation

    const-string v0, "chooseCustomerResultKey"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "contact"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "historyFuncForConfirmation"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "historyFuncForBackout"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parentKeyForChild"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;-><init>(Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;Lcom/squareup/protos/client/rolodex/Contact;ZLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;

    invoke-virtual {p0}, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->getChooseCustomerResultKey()Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->getChooseCustomerResultKey()Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    iget-object v1, p1, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->contactChosenFromRecent:Z

    iget-boolean v1, p1, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->contactChosenFromRecent:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->historyFuncForConfirmation:Lkotlin/jvm/functions/Function1;

    iget-object v1, p1, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->historyFuncForConfirmation:Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->historyFuncForBackout:Lkotlin/jvm/functions/Function1;

    iget-object v1, p1, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->historyFuncForBackout:Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->parentKeyForChild:Lcom/squareup/ui/main/RegisterTreeKey;

    iget-object p1, p1, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->parentKeyForChild:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getChooseCustomerResultKey()Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->chooseCustomerResultKey:Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;

    return-object v0
.end method

.method public final getContact()Lcom/squareup/protos/client/rolodex/Contact;
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    return-object v0
.end method

.method public final getContactChosenFromRecent()Z
    .locals 1

    .line 43
    iget-boolean v0, p0, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->contactChosenFromRecent:Z

    return v0
.end method

.method public final getHistoryFuncForBackout()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lflow/History$Builder;",
            "Lflow/History$Builder;",
            ">;"
        }
    .end annotation

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->historyFuncForBackout:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getHistoryFuncForConfirmation()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lflow/History$Builder;",
            "Lflow/History$Builder;",
            ">;"
        }
    .end annotation

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->historyFuncForConfirmation:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getParentKeyForChild()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 1

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->parentKeyForChild:Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->getChooseCustomerResultKey()Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->contactChosenFromRecent:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->historyFuncForConfirmation:Lkotlin/jvm/functions/Function1;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->historyFuncForBackout:Lkotlin/jvm/functions/Function1;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_4
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->parentKeyForChild:Lcom/squareup/ui/main/RegisterTreeKey;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_5
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ContactChosen(chooseCustomerResultKey="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->getChooseCustomerResultKey()Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", contact="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", contactChosenFromRecent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->contactChosenFromRecent:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", historyFuncForConfirmation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->historyFuncForConfirmation:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", historyFuncForBackout="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->historyFuncForBackout:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", parentKeyForChild="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->parentKeyForChild:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
