.class public final Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner_Factory;
.super Ljava/lang/Object;
.source "CustomersAppletScopeRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final chooseFiltersFlowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;",
            ">;"
        }
    .end annotation
.end field

.field private final contactLoaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexContactLoader;",
            ">;"
        }
    .end annotation
.end field

.field private final conversationLoaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/ConversationLoader;",
            ">;"
        }
    .end annotation
.end field

.field private final customerConversationTokenHolderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/applet/CustomerConversationTokenHolder;",
            ">;"
        }
    .end annotation
.end field

.field private final customersAppletProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/applet/CustomersApplet;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final groupLoaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexGroupLoader;",
            ">;"
        }
    .end annotation
.end field

.field private final mergeProposalLoaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/MergeProposalLoader;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final resolveDuplicatesFlowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/flow/ResolveDuplicatesFlow;",
            ">;"
        }
    .end annotation
.end field

.field private final rightPaneDataRendererProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/RightPaneDataRenderer;",
            ">;"
        }
    .end annotation
.end field

.field private final rolodexProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final selectCustomersFlowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/flow/SelectCustomersFlow;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionDiscountAdapterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TransactionDiscountAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field

.field private final updateCustomerFlowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;",
            ">;"
        }
    .end annotation
.end field

.field private final updateGroup2FlowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final x2ScreenRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TransactionDiscountAdapter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/flow/ResolveDuplicatesFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/flow/SelectCustomersFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexContactLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/ConversationLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/MergeProposalLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexGroupLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/applet/CustomersApplet;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/applet/CustomerConversationTokenHolder;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/RightPaneDataRenderer;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 88
    iput-object v1, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 89
    iput-object v1, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner_Factory;->resProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 90
    iput-object v1, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner_Factory;->transactionProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 91
    iput-object v1, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner_Factory;->transactionDiscountAdapterProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 92
    iput-object v1, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner_Factory;->updateGroup2FlowProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 93
    iput-object v1, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner_Factory;->resolveDuplicatesFlowProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 94
    iput-object v1, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner_Factory;->chooseFiltersFlowProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 95
    iput-object v1, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner_Factory;->selectCustomersFlowProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 96
    iput-object v1, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner_Factory;->updateCustomerFlowProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 97
    iput-object v1, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner_Factory;->contactLoaderProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 98
    iput-object v1, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner_Factory;->conversationLoaderProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 99
    iput-object v1, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner_Factory;->mergeProposalLoaderProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 100
    iput-object v1, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner_Factory;->groupLoaderProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 101
    iput-object v1, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner_Factory;->customersAppletProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 102
    iput-object v1, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner_Factory;->x2ScreenRunnerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 103
    iput-object v1, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner_Factory;->customerConversationTokenHolderProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 104
    iput-object v1, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner_Factory;->deviceProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p18

    .line 105
    iput-object v1, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner_Factory;->rolodexProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p19

    .line 106
    iput-object v1, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner_Factory;->rightPaneDataRendererProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner_Factory;
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TransactionDiscountAdapter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/flow/ResolveDuplicatesFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/flow/SelectCustomersFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexContactLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/ConversationLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/MergeProposalLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexGroupLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/applet/CustomersApplet;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/applet/CustomerConversationTokenHolder;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/RightPaneDataRenderer;",
            ">;)",
            "Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    .line 131
    new-instance v20, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner_Factory;

    move-object/from16 v0, v20

    invoke-direct/range {v0 .. v19}, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v20
.end method

.method public static newInstance(Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/payment/Transaction;Lcom/squareup/payment/TransactionDiscountAdapter;Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;Lcom/squareup/ui/crm/flow/ResolveDuplicatesFlow;Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;Lcom/squareup/ui/crm/flow/SelectCustomersFlow;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;Lcom/squareup/crm/RolodexContactLoader;Lcom/squareup/crm/ConversationLoader;Lcom/squareup/crm/MergeProposalLoader;Lcom/squareup/crm/RolodexGroupLoader;Lcom/squareup/ui/crm/applet/CustomersApplet;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/ui/crm/applet/CustomerConversationTokenHolder;Lcom/squareup/util/Device;Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/ui/crm/v2/RightPaneDataRenderer;)Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;
    .locals 21

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    .line 143
    new-instance v20, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;

    move-object/from16 v0, v20

    invoke-direct/range {v0 .. v19}, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;-><init>(Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/payment/Transaction;Lcom/squareup/payment/TransactionDiscountAdapter;Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;Lcom/squareup/ui/crm/flow/ResolveDuplicatesFlow;Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;Lcom/squareup/ui/crm/flow/SelectCustomersFlow;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;Lcom/squareup/crm/RolodexContactLoader;Lcom/squareup/crm/ConversationLoader;Lcom/squareup/crm/MergeProposalLoader;Lcom/squareup/crm/RolodexGroupLoader;Lcom/squareup/ui/crm/applet/CustomersApplet;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/ui/crm/applet/CustomerConversationTokenHolder;Lcom/squareup/util/Device;Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/ui/crm/v2/RightPaneDataRenderer;)V

    return-object v20
.end method


# virtual methods
.method public get()Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;
    .locals 21

    move-object/from16 v0, p0

    .line 111
    iget-object v1, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lflow/Flow;

    iget-object v1, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/util/Res;

    iget-object v1, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/payment/Transaction;

    iget-object v1, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner_Factory;->transactionDiscountAdapterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/payment/TransactionDiscountAdapter;

    iget-object v1, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner_Factory;->updateGroup2FlowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;

    iget-object v1, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner_Factory;->resolveDuplicatesFlowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/ui/crm/flow/ResolveDuplicatesFlow;

    iget-object v1, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner_Factory;->chooseFiltersFlowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;

    iget-object v1, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner_Factory;->selectCustomersFlowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;

    iget-object v1, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner_Factory;->updateCustomerFlowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;

    iget-object v1, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner_Factory;->contactLoaderProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/crm/RolodexContactLoader;

    iget-object v1, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner_Factory;->conversationLoaderProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/crm/ConversationLoader;

    iget-object v1, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner_Factory;->mergeProposalLoaderProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/crm/MergeProposalLoader;

    iget-object v1, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner_Factory;->groupLoaderProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/crm/RolodexGroupLoader;

    iget-object v1, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner_Factory;->customersAppletProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/ui/crm/applet/CustomersApplet;

    iget-object v1, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner_Factory;->x2ScreenRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    iget-object v1, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner_Factory;->customerConversationTokenHolderProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/ui/crm/applet/CustomerConversationTokenHolder;

    iget-object v1, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lcom/squareup/util/Device;

    iget-object v1, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner_Factory;->rolodexProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v19, v1

    check-cast v19, Lcom/squareup/crm/RolodexServiceHelper;

    iget-object v1, v0, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner_Factory;->rightPaneDataRendererProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v20, v1

    check-cast v20, Lcom/squareup/ui/crm/v2/RightPaneDataRenderer;

    invoke-static/range {v2 .. v20}, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner_Factory;->newInstance(Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/payment/Transaction;Lcom/squareup/payment/TransactionDiscountAdapter;Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;Lcom/squareup/ui/crm/flow/ResolveDuplicatesFlow;Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;Lcom/squareup/ui/crm/flow/SelectCustomersFlow;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;Lcom/squareup/crm/RolodexContactLoader;Lcom/squareup/crm/ConversationLoader;Lcom/squareup/crm/MergeProposalLoader;Lcom/squareup/crm/RolodexGroupLoader;Lcom/squareup/ui/crm/applet/CustomersApplet;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/ui/crm/applet/CustomerConversationTokenHolder;Lcom/squareup/util/Device;Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/ui/crm/v2/RightPaneDataRenderer;)Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner_Factory;->get()Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;

    move-result-object v0

    return-object v0
.end method
