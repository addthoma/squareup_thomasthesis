.class public Lcom/squareup/ui/crm/v2/profile/ContactTestHelper;
.super Ljava/lang/Object;
.source "ContactTestHelper.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static newContact(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Contact;
    .locals 1

    .line 31
    new-instance v0, Lcom/squareup/protos/client/rolodex/Contact$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;-><init>()V

    invoke-virtual {v0, p0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->display_name(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->build()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p0

    return-object p0
.end method

.method public static newContactBuilder(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Contact$Builder;
    .locals 1

    .line 14
    new-instance v0, Lcom/squareup/protos/client/rolodex/Contact$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;-><init>()V

    .line 15
    invoke-virtual {v0, p0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->display_name(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object v0

    .line 16
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->contact_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object p1

    .line 17
    invoke-virtual {p1, p4}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->creation_source(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object p1

    .line 19
    new-instance p4, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    invoke-direct {p4}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;-><init>()V

    invoke-virtual {p4, p0}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->given_name(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    move-result-object p0

    if-eqz p2, :cond_0

    .line 21
    invoke-virtual {p0, p2}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->email_address(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    :cond_0
    if-eqz p3, :cond_1

    .line 24
    invoke-virtual {p0, p3}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->phone_number(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    .line 27
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->build()Lcom/squareup/protos/client/rolodex/CustomerProfile;

    move-result-object p0

    invoke-virtual {p1, p0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->profile(Lcom/squareup/protos/client/rolodex/CustomerProfile;)Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object p0

    return-object p0
.end method

.method public static newContactWithCustomerProfile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Contact;
    .locals 2

    .line 36
    new-instance v0, Lcom/squareup/protos/client/rolodex/Contact$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;-><init>()V

    new-instance v1, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;-><init>()V

    .line 38
    invoke-virtual {v1, p0}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->given_name(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    move-result-object p0

    .line 39
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->surname(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    move-result-object p0

    .line 40
    invoke-virtual {p0, p2}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->email_address(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    move-result-object p0

    .line 41
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->build()Lcom/squareup/protos/client/rolodex/CustomerProfile;

    move-result-object p0

    .line 37
    invoke-virtual {v0, p0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->profile(Lcom/squareup/protos/client/rolodex/CustomerProfile;)Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object p0

    .line 42
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->build()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p0

    return-object p0
.end method
