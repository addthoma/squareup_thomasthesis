.class public final Lcom/squareup/ui/buyer/BuyerActionContainer;
.super Landroid/widget/RelativeLayout;
.source "BuyerActionContainer.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/buyer/BuyerActionContainer$Config;,
        Lcom/squareup/ui/buyer/BuyerActionContainer$TitleSize;,
        Lcom/squareup/ui/buyer/BuyerActionContainer$ButtonInfo;,
        Lcom/squareup/ui/buyer/BuyerActionContainer$PrimaryButtonInfo;,
        Lcom/squareup/ui/buyer/BuyerActionContainer$SecondaryButtonInfo;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBuyerActionContainer.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BuyerActionContainer.kt\ncom/squareup/ui/buyer/BuyerActionContainer\n+ 2 MarketSpan.kt\ncom/squareup/marketfont/MarketSpanKt\n+ 3 Views.kt\ncom/squareup/util/Views\n*L\n1#1,210:1\n17#2,2:211\n1261#3:213\n*E\n*S KotlinDebug\n*F\n+ 1 BuyerActionContainer.kt\ncom/squareup/ui/buyer/BuyerActionContainer\n*L\n159#1,2:211\n42#1:213\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0007\u0018\u00002\u00020\u0001:\u000556789B#\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\'\u001a\u00020(2\u0006\u0010\u0011\u001a\u00020\u0010H\u0002J\u0018\u0010)\u001a\u00020(2\u0006\u0010*\u001a\u00020+2\u0006\u0010,\u001a\u00020\u001cH\u0002J \u0010-\u001a\u00020(2\u0006\u0010*\u001a\u00020.2\u0006\u0010,\u001a\u00020\u001c2\u0006\u0010/\u001a\u00020 H\u0002J\u0006\u00100\u001a\u00020(J\u000e\u00101\u001a\u00020(2\u0006\u00102\u001a\u000203J\u000e\u00104\u001a\u00020(2\u0006\u00102\u001a\u000203R\u001b\u0010\t\u001a\u00020\n8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\r\u0010\u000e\u001a\u0004\u0008\u000b\u0010\u000cR$\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u000f\u001a\u00020\u0010@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013\"\u0004\u0008\u0014\u0010\u0015R\u001b\u0010\u0016\u001a\u00020\u00178BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u001a\u0010\u000e\u001a\u0004\u0008\u0018\u0010\u0019R\u001b\u0010\u001b\u001a\u00020\u001c8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u001e\u0010\u000e\u001a\u0004\u0008\u001b\u0010\u001dR\u001b\u0010\u001f\u001a\u00020 8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008#\u0010\u000e\u001a\u0004\u0008!\u0010\"R\u001b\u0010$\u001a\u00020 8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008&\u0010\u000e\u001a\u0004\u0008%\u0010\"\u00a8\u0006:"
    }
    d2 = {
        "Lcom/squareup/ui/buyer/BuyerActionContainer;",
        "Landroid/widget/RelativeLayout;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "defStyleAttr",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "callToAction",
        "Lcom/squareup/widgets/MessageView;",
        "getCallToAction",
        "()Lcom/squareup/widgets/MessageView;",
        "callToAction$delegate",
        "Lkotlin/Lazy;",
        "value",
        "Lcom/squareup/ui/buyer/BuyerActionContainer$Config;",
        "config",
        "getConfig",
        "()Lcom/squareup/ui/buyer/BuyerActionContainer$Config;",
        "setConfig",
        "(Lcom/squareup/ui/buyer/BuyerActionContainer$Config;)V",
        "helperText",
        "Landroid/widget/TextView;",
        "getHelperText",
        "()Landroid/widget/TextView;",
        "helperText$delegate",
        "isTablet",
        "",
        "()Z",
        "isTablet$delegate",
        "primaryActionContainer",
        "Landroid/widget/LinearLayout;",
        "getPrimaryActionContainer",
        "()Landroid/widget/LinearLayout;",
        "primaryActionContainer$delegate",
        "secondaryActionContainer",
        "getSecondaryActionContainer",
        "secondaryActionContainer$delegate",
        "applyConfig",
        "",
        "createPrimaryButton",
        "info",
        "Lcom/squareup/ui/buyer/BuyerActionContainer$PrimaryButtonInfo;",
        "isFirstElement",
        "createSecondaryButton",
        "Lcom/squareup/ui/buyer/BuyerActionContainer$ButtonInfo;",
        "container",
        "hideHelperText",
        "setCallToAction",
        "text",
        "Lcom/squareup/util/ViewString;",
        "setHelperText",
        "ButtonInfo",
        "Config",
        "PrimaryButtonInfo",
        "SecondaryButtonInfo",
        "TitleSize",
        "checkout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final callToAction$delegate:Lkotlin/Lazy;

.field private config:Lcom/squareup/ui/buyer/BuyerActionContainer$Config;

.field private final helperText$delegate:Lkotlin/Lazy;

.field private final isTablet$delegate:Lkotlin/Lazy;

.field private final primaryActionContainer$delegate:Lkotlin/Lazy;

.field private final secondaryActionContainer$delegate:Lkotlin/Lazy;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/buyer/BuyerActionContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 213
    sget-object v0, Lcom/squareup/util/VectorFriendly;->INSTANCE:Lcom/squareup/util/VectorFriendly;

    invoke-virtual {v0, p1}, Lcom/squareup/util/VectorFriendly;->ensureContext(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    .line 42
    invoke-direct {p0, v0, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    new-instance p2, Lcom/squareup/ui/buyer/BuyerActionContainer$isTablet$2;

    invoke-direct {p2, p0}, Lcom/squareup/ui/buyer/BuyerActionContainer$isTablet$2;-><init>(Lcom/squareup/ui/buyer/BuyerActionContainer;)V

    check-cast p2, Lkotlin/jvm/functions/Function0;

    invoke-static {p2}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/buyer/BuyerActionContainer;->isTablet$delegate:Lkotlin/Lazy;

    .line 49
    new-instance p2, Lcom/squareup/ui/buyer/BuyerActionContainer$primaryActionContainer$2;

    invoke-direct {p2, p0}, Lcom/squareup/ui/buyer/BuyerActionContainer$primaryActionContainer$2;-><init>(Lcom/squareup/ui/buyer/BuyerActionContainer;)V

    check-cast p2, Lkotlin/jvm/functions/Function0;

    invoke-static {p2}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/buyer/BuyerActionContainer;->primaryActionContainer$delegate:Lkotlin/Lazy;

    .line 54
    new-instance p2, Lcom/squareup/ui/buyer/BuyerActionContainer$secondaryActionContainer$2;

    invoke-direct {p2, p0}, Lcom/squareup/ui/buyer/BuyerActionContainer$secondaryActionContainer$2;-><init>(Lcom/squareup/ui/buyer/BuyerActionContainer;)V

    check-cast p2, Lkotlin/jvm/functions/Function0;

    invoke-static {p2}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/buyer/BuyerActionContainer;->secondaryActionContainer$delegate:Lkotlin/Lazy;

    .line 59
    new-instance p2, Lcom/squareup/ui/buyer/BuyerActionContainer$callToAction$2;

    invoke-direct {p2, p0}, Lcom/squareup/ui/buyer/BuyerActionContainer$callToAction$2;-><init>(Lcom/squareup/ui/buyer/BuyerActionContainer;)V

    check-cast p2, Lkotlin/jvm/functions/Function0;

    invoke-static {p2}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/buyer/BuyerActionContainer;->callToAction$delegate:Lkotlin/Lazy;

    .line 64
    new-instance p2, Lcom/squareup/ui/buyer/BuyerActionContainer$helperText$2;

    invoke-direct {p2, p0}, Lcom/squareup/ui/buyer/BuyerActionContainer$helperText$2;-><init>(Lcom/squareup/ui/buyer/BuyerActionContainer;)V

    check-cast p2, Lkotlin/jvm/functions/Function0;

    invoke-static {p2}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/buyer/BuyerActionContainer;->helperText$delegate:Lkotlin/Lazy;

    .line 66
    new-instance p2, Lcom/squareup/ui/buyer/BuyerActionContainer$Config;

    const/4 p3, 0x0

    const/4 v0, 0x3

    invoke-direct {p2, p3, p3, v0, p3}, Lcom/squareup/ui/buyer/BuyerActionContainer$Config;-><init>(Ljava/util/List;Ljava/util/List;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p2, p0, Lcom/squareup/ui/buyer/BuyerActionContainer;->config:Lcom/squareup/ui/buyer/BuyerActionContainer$Config;

    .line 73
    sget p2, Lcom/squareup/checkout/R$layout;->buyer_action_container:I

    move-object p3, p0

    check-cast p3, Landroid/view/ViewGroup;

    invoke-static {p1, p2, p3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 75
    invoke-direct {p0}, Lcom/squareup/ui/buyer/BuyerActionContainer;->isTablet()Z

    move-result p1

    if-nez p1, :cond_0

    .line 76
    invoke-direct {p0}, Lcom/squareup/ui/buyer/BuyerActionContainer;->getPrimaryActionContainer()Landroid/widget/LinearLayout;

    move-result-object p1

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    :cond_0
    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    .line 41
    sget p3, Lcom/squareup/noho/R$attr;->nohoActionBarStyle:I

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/buyer/BuyerActionContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private final applyConfig(Lcom/squareup/ui/buyer/BuyerActionContainer$Config;)V
    .locals 7

    .line 95
    invoke-direct {p0}, Lcom/squareup/ui/buyer/BuyerActionContainer;->getPrimaryActionContainer()Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 96
    invoke-direct {p0}, Lcom/squareup/ui/buyer/BuyerActionContainer;->getSecondaryActionContainer()Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 98
    invoke-direct {p0}, Lcom/squareup/ui/buyer/BuyerActionContainer;->getPrimaryActionContainer()Landroid/widget/LinearLayout;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/BuyerActionContainer$Config;->getPrimaryActions()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    const/4 v2, 0x1

    xor-int/2addr v1, v2

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 99
    invoke-direct {p0}, Lcom/squareup/ui/buyer/BuyerActionContainer;->isTablet()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 100
    invoke-virtual {p1}, Lcom/squareup/ui/buyer/BuyerActionContainer$Config;->getPrimaryActions()Ljava/util/List;

    move-result-object v0

    .line 101
    check-cast v0, Ljava/lang/Iterable;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v3, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/ui/buyer/BuyerActionContainer$PrimaryButtonInfo;

    if-nez v3, :cond_0

    const/4 v5, 0x1

    goto :goto_1

    :cond_0
    const/4 v5, 0x0

    .line 102
    :goto_1
    invoke-direct {p0, v4, v5}, Lcom/squareup/ui/buyer/BuyerActionContainer;->createPrimaryButton(Lcom/squareup/ui/buyer/BuyerActionContainer$PrimaryButtonInfo;Z)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 106
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/ui/buyer/BuyerActionContainer$Config;->getPrimaryActions()Ljava/util/List;

    move-result-object v0

    .line 107
    check-cast v0, Ljava/lang/Iterable;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v3, 0x0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/ui/buyer/BuyerActionContainer$PrimaryButtonInfo;

    .line 108
    check-cast v4, Lcom/squareup/ui/buyer/BuyerActionContainer$ButtonInfo;

    if-nez v3, :cond_2

    const/4 v5, 0x1

    goto :goto_3

    :cond_2
    const/4 v5, 0x0

    :goto_3
    invoke-direct {p0}, Lcom/squareup/ui/buyer/BuyerActionContainer;->getPrimaryActionContainer()Landroid/widget/LinearLayout;

    move-result-object v6

    invoke-direct {p0, v4, v5, v6}, Lcom/squareup/ui/buyer/BuyerActionContainer;->createSecondaryButton(Lcom/squareup/ui/buyer/BuyerActionContainer$ButtonInfo;ZLandroid/widget/LinearLayout;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 113
    :cond_3
    invoke-direct {p0}, Lcom/squareup/ui/buyer/BuyerActionContainer;->getSecondaryActionContainer()Landroid/widget/LinearLayout;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/BuyerActionContainer$Config;->getSecondaryActions()Ljava/util/List;

    move-result-object v3

    check-cast v3, Ljava/util/Collection;

    invoke-interface {v3}, Ljava/util/Collection;->isEmpty()Z

    move-result v3

    xor-int/2addr v3, v2

    invoke-static {v0, v3}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 114
    invoke-virtual {p1}, Lcom/squareup/ui/buyer/BuyerActionContainer$Config;->getSecondaryActions()Ljava/util/List;

    move-result-object p1

    .line 115
    check-cast p1, Ljava/lang/Iterable;

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v0, 0x0

    :goto_4
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/ui/buyer/BuyerActionContainer$SecondaryButtonInfo;

    .line 116
    check-cast v3, Lcom/squareup/ui/buyer/BuyerActionContainer$ButtonInfo;

    if-nez v0, :cond_4

    const/4 v4, 0x1

    goto :goto_5

    :cond_4
    const/4 v4, 0x0

    :goto_5
    invoke-direct {p0}, Lcom/squareup/ui/buyer/BuyerActionContainer;->getSecondaryActionContainer()Landroid/widget/LinearLayout;

    move-result-object v5

    invoke-direct {p0, v3, v4, v5}, Lcom/squareup/ui/buyer/BuyerActionContainer;->createSecondaryButton(Lcom/squareup/ui/buyer/BuyerActionContainer$ButtonInfo;ZLandroid/widget/LinearLayout;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    return-void
.end method

.method private final createPrimaryButton(Lcom/squareup/ui/buyer/BuyerActionContainer$PrimaryButtonInfo;Z)V
    .locals 9

    .line 125
    invoke-virtual {p1}, Lcom/squareup/ui/buyer/BuyerActionContainer$PrimaryButtonInfo;->getTitleSize()Lcom/squareup/ui/buyer/BuyerActionContainer$TitleSize;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 126
    :cond_0
    sget-object v1, Lcom/squareup/ui/buyer/BuyerActionContainer$TitleSize$Small;->INSTANCE:Lcom/squareup/ui/buyer/BuyerActionContainer$TitleSize$Small;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget v0, Lcom/squareup/checkout/R$style;->Theme_BuyerFacing_SmallTitle:I

    goto :goto_0

    .line 127
    :cond_1
    sget-object v1, Lcom/squareup/ui/buyer/BuyerActionContainer$TitleSize$Large;->INSTANCE:Lcom/squareup/ui/buyer/BuyerActionContainer$TitleSize$Large;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    sget v0, Lcom/squareup/checkout/R$style;->Theme_BuyerFacing_LargeTitle:I

    .line 129
    :goto_0
    invoke-direct {p0}, Lcom/squareup/ui/buyer/BuyerActionContainer;->getPrimaryActionContainer()Landroid/widget/LinearLayout;

    move-result-object v1

    new-instance v8, Lcom/squareup/noho/NohoButton;

    new-instance v2, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lcom/squareup/ui/buyer/BuyerActionContainer;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    move-object v3, v2

    check-cast v3, Landroid/content/Context;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v2, v8

    invoke-direct/range {v2 .. v7}, Lcom/squareup/noho/NohoButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 130
    sget-object v0, Lcom/squareup/noho/NohoButtonType;->PRIMARY:Lcom/squareup/noho/NohoButtonType;

    invoke-virtual {v8, v0}, Lcom/squareup/noho/NohoButton;->apply(Lcom/squareup/noho/NohoButtonType;)V

    .line 131
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 132
    invoke-virtual {v8}, Lcom/squareup/noho/NohoButton;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "context"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v4, Lcom/squareup/noho/R$dimen;->noho_gap_0:I

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    const/4 v4, -0x2

    const/high16 v5, 0x3f800000    # 1.0f

    .line 131
    invoke-direct {v0, v2, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    const/16 v2, 0x11

    .line 136
    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    if-eqz p2, :cond_2

    const/4 v2, 0x0

    goto :goto_1

    .line 140
    :cond_2
    invoke-virtual {v8}, Lcom/squareup/noho/NohoButton;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 141
    sget v3, Lcom/squareup/noho/R$dimen;->noho_gap_big_not_converted_from_marin:I

    .line 140
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    .line 137
    :goto_1
    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 135
    check-cast v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v8, v0}, Lcom/squareup/noho/NohoButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 145
    invoke-virtual {p1}, Lcom/squareup/ui/buyer/BuyerActionContainer$PrimaryButtonInfo;->getTitle()Lcom/squareup/util/ViewString;

    move-result-object v0

    invoke-virtual {v8}, Lcom/squareup/noho/NohoButton;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const-string v3, "resources"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lcom/squareup/util/ViewString;->getString(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcom/squareup/noho/NohoButton;->setText(Ljava/lang/CharSequence;)V

    .line 146
    invoke-virtual {p1}, Lcom/squareup/ui/buyer/BuyerActionContainer$PrimaryButtonInfo;->getSubtitle()Lcom/squareup/util/ViewString;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 147
    invoke-virtual {p1}, Lcom/squareup/ui/buyer/BuyerActionContainer$PrimaryButtonInfo;->getSubtitle()Lcom/squareup/util/ViewString;

    move-result-object v0

    invoke-virtual {v8}, Lcom/squareup/noho/NohoButton;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lcom/squareup/util/ViewString;->getString(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcom/squareup/noho/NohoButton;->setSubText(Ljava/lang/CharSequence;)V

    .line 149
    :cond_3
    check-cast v8, Landroid/view/View;

    invoke-static {v8}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object v0

    new-instance v2, Lcom/squareup/ui/buyer/BuyerActionContainer$createPrimaryButton$$inlined$apply$lambda$1;

    invoke-direct {v2, p2, p1}, Lcom/squareup/ui/buyer/BuyerActionContainer$createPrimaryButton$$inlined$apply$lambda$1;-><init>(ZLcom/squareup/ui/buyer/BuyerActionContainer$PrimaryButtonInfo;)V

    check-cast v2, Lrx/functions/Action1;

    invoke-virtual {v0, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    .line 129
    invoke-virtual {v1, v8}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-void

    .line 127
    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final createSecondaryButton(Lcom/squareup/ui/buyer/BuyerActionContainer$ButtonInfo;ZLandroid/widget/LinearLayout;)V
    .locals 8

    .line 159
    new-instance v6, Lcom/squareup/noho/NohoButton;

    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lcom/squareup/ui/buyer/BuyerActionContainer;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/squareup/checkout/R$style;->Theme_BuyerFacing:I

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    move-object v1, v0

    check-cast v1, Landroid/content/Context;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/noho/NohoButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 160
    sget-object v0, Lcom/squareup/noho/NohoButtonType;->SECONDARY:Lcom/squareup/noho/NohoButtonType;

    invoke-virtual {v6, v0}, Lcom/squareup/noho/NohoButton;->apply(Lcom/squareup/noho/NohoButtonType;)V

    .line 161
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/4 v1, 0x0

    const-string v2, "context"

    if-eqz p2, :cond_0

    const/4 v3, 0x0

    goto :goto_0

    .line 165
    :cond_0
    invoke-virtual {v6}, Lcom/squareup/noho/NohoButton;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/squareup/checkout/R$dimen;->buyer_facing_bar_button_margin:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    .line 162
    :goto_0
    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 161
    check-cast v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v6, v0}, Lcom/squareup/noho/NohoButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 168
    invoke-virtual {p1}, Lcom/squareup/ui/buyer/BuyerActionContainer$ButtonInfo;->getSubtitle()Lcom/squareup/util/ViewString;

    move-result-object v0

    const-string v3, "resources"

    if-nez v0, :cond_1

    .line 169
    invoke-virtual {p1}, Lcom/squareup/ui/buyer/BuyerActionContainer$ButtonInfo;->getTitle()Lcom/squareup/util/ViewString;

    move-result-object v0

    invoke-virtual {v6}, Lcom/squareup/noho/NohoButton;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/squareup/util/ViewString;->getString(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_1

    .line 171
    :cond_1
    new-instance v0, Landroid/text/SpannableString;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/BuyerActionContainer$ButtonInfo;->getSubtitle()Lcom/squareup/util/ViewString;

    move-result-object v5

    invoke-virtual {v6}, Lcom/squareup/noho/NohoButton;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-static {v7, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v5, v7}, Lcom/squareup/util/ViewString;->getString(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const/16 v5, 0x29

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-direct {v0, v4}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v0, Landroid/text/Spannable;

    .line 172
    invoke-virtual {v6}, Lcom/squareup/noho/NohoButton;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v2, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    .line 212
    new-instance v5, Lcom/squareup/fonts/FontSpan;

    invoke-static {v2, v1}, Lcom/squareup/marketfont/MarketFont$Weight;->resourceIdFor(Lcom/squareup/marketfont/MarketFont$Weight;I)I

    move-result v1

    invoke-direct {v5, v4, v1}, Lcom/squareup/fonts/FontSpan;-><init>(Landroid/content/Context;I)V

    check-cast v5, Landroid/text/style/CharacterStyle;

    .line 171
    invoke-static {v0, v5}, Lcom/squareup/text/Spannables;->span(Landroid/text/Spannable;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object v0

    .line 174
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/BuyerActionContainer$ButtonInfo;->getTitle()Lcom/squareup/util/ViewString;

    move-result-object v2

    invoke-virtual {v6}, Lcom/squareup/noho/NohoButton;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {v4, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Lcom/squareup/util/ViewString;->getString(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    .line 175
    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    .line 168
    :goto_1
    invoke-virtual {v6, v0}, Lcom/squareup/noho/NohoButton;->setText(Ljava/lang/CharSequence;)V

    .line 177
    check-cast v6, Landroid/view/View;

    invoke-static {v6}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/buyer/BuyerActionContainer$createSecondaryButton$$inlined$apply$lambda$1;

    invoke-direct {v1, p2, p1}, Lcom/squareup/ui/buyer/BuyerActionContainer$createSecondaryButton$$inlined$apply$lambda$1;-><init>(ZLcom/squareup/ui/buyer/BuyerActionContainer$ButtonInfo;)V

    check-cast v1, Lrx/functions/Action1;

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    .line 158
    invoke-virtual {p3, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-void
.end method

.method private final getCallToAction()Lcom/squareup/widgets/MessageView;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerActionContainer;->callToAction$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    return-object v0
.end method

.method private final getHelperText()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerActionContainer;->helperText$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getPrimaryActionContainer()Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerActionContainer;->primaryActionContainer$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    return-object v0
.end method

.method private final getSecondaryActionContainer()Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerActionContainer;->secondaryActionContainer$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    return-object v0
.end method

.method private final isTablet()Z
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerActionContainer;->isTablet$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final getConfig()Lcom/squareup/ui/buyer/BuyerActionContainer$Config;
    .locals 1

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerActionContainer;->config:Lcom/squareup/ui/buyer/BuyerActionContainer$Config;

    return-object v0
.end method

.method public final hideHelperText()V
    .locals 2

    .line 91
    invoke-direct {p0}, Lcom/squareup/ui/buyer/BuyerActionContainer;->getHelperText()Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method public final setCallToAction(Lcom/squareup/util/ViewString;)V
    .locals 3

    const-string/jumbo v0, "text"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    invoke-direct {p0}, Lcom/squareup/ui/buyer/BuyerActionContainer;->getCallToAction()Lcom/squareup/widgets/MessageView;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/ui/buyer/BuyerActionContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "resources"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Lcom/squareup/util/ViewString;->getString(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    invoke-direct {p0}, Lcom/squareup/ui/buyer/BuyerActionContainer;->getCallToAction()Lcom/squareup/widgets/MessageView;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p0}, Lcom/squareup/ui/buyer/BuyerActionContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Lcom/squareup/util/ViewString;->getString(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public final setConfig(Lcom/squareup/ui/buyer/BuyerActionContainer$Config;)V
    .locals 1

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    iput-object p1, p0, Lcom/squareup/ui/buyer/BuyerActionContainer;->config:Lcom/squareup/ui/buyer/BuyerActionContainer$Config;

    .line 69
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/BuyerActionContainer;->applyConfig(Lcom/squareup/ui/buyer/BuyerActionContainer$Config;)V

    return-void
.end method

.method public final setHelperText(Lcom/squareup/util/ViewString;)V
    .locals 3

    const-string/jumbo v0, "text"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    invoke-direct {p0}, Lcom/squareup/ui/buyer/BuyerActionContainer;->getHelperText()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/ui/buyer/BuyerActionContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "resources"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Lcom/squareup/util/ViewString;->getString(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 87
    invoke-direct {p0}, Lcom/squareup/ui/buyer/BuyerActionContainer;->getHelperText()Landroid/widget/TextView;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p0}, Lcom/squareup/ui/buyer/BuyerActionContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Lcom/squareup/util/ViewString;->getString(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method
