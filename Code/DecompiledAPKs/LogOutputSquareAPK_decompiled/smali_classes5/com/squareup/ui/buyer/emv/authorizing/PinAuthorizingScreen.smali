.class public final Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen;
.super Lcom/squareup/ui/buyer/emv/InEmvScope;
.source "PinAuthorizingScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/ui/buyer/PaymentExempt;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen$Component;,
        Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen$Module;,
        Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final listenForNfcAuthBytes:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 113
    sget-object v0, Lcom/squareup/ui/buyer/emv/authorizing/-$$Lambda$PinAuthorizingScreen$3gV4_2S9YWtgc70a_YnpJAeN-jY;->INSTANCE:Lcom/squareup/ui/buyer/emv/authorizing/-$$Lambda$PinAuthorizingScreen$3gV4_2S9YWtgc70a_YnpJAeN-jY;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/buyer/emv/EmvScope;Z)V
    .locals 0

    .line 36
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/emv/InEmvScope;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope;)V

    .line 37
    iput-boolean p2, p0, Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen;->listenForNfcAuthBytes:Z

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen;)Z
    .locals 0

    .line 30
    iget-boolean p0, p0, Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen;->listenForNfcAuthBytes:Z

    return p0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen;
    .locals 2

    .line 114
    const-class v0, Lcom/squareup/ui/buyer/emv/EmvScope;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/emv/EmvScope;

    .line 115
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result p0

    const/4 v1, 0x1

    if-ne p0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 116
    :goto_0
    new-instance p0, Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen;

    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope;Z)V

    return-object p0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 108
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/buyer/emv/InEmvScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 109
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen;->emvPath:Lcom/squareup/ui/buyer/emv/EmvScope;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 110
    iget-boolean p2, p0, Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen;->listenForNfcAuthBytes:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method

.method public screenLayout()I
    .locals 1

    .line 120
    sget v0, Lcom/squareup/ui/buyerflow/R$layout;->emv_progress_view:I

    return v0
.end method
