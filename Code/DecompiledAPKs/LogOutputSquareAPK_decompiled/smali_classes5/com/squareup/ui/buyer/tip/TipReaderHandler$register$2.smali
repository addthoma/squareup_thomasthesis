.class public final Lcom/squareup/ui/buyer/tip/TipReaderHandler$register$2;
.super Ljava/lang/Object;
.source "TipReaderHandler.kt"

# interfaces
.implements Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/buyer/tip/TipReaderHandler;->register()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0019\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016J\u0010\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0007"
    }
    d2 = {
        "com/squareup/ui/buyer/tip/TipReaderHandler$register$2",
        "Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;",
        "onCardReaderAdded",
        "",
        "cardReader",
        "Lcom/squareup/cardreader/CardReader;",
        "onCardReaderRemoved",
        "buyer-flow_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/buyer/tip/TipReaderHandler;


# direct methods
.method constructor <init>(Lcom/squareup/ui/buyer/tip/TipReaderHandler;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 59
    iput-object p1, p0, Lcom/squareup/ui/buyer/tip/TipReaderHandler$register$2;->this$0:Lcom/squareup/ui/buyer/tip/TipReaderHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCardReaderAdded(Lcom/squareup/cardreader/CardReader;)V
    .locals 1

    const-string v0, "cardReader"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public onCardReaderRemoved(Lcom/squareup/cardreader/CardReader;)V
    .locals 4

    const-string v0, "cardReader"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/buyer/tip/TipReaderHandler$register$2;->this$0:Lcom/squareup/ui/buyer/tip/TipReaderHandler;

    invoke-static {v0}, Lcom/squareup/ui/buyer/tip/TipReaderHandler;->access$getActiveCardReader$p(Lcom/squareup/ui/buyer/tip/TipReaderHandler;)Lcom/squareup/cardreader/dipper/ActiveCardReader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->hasActiveCardReader()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/buyer/tip/TipReaderHandler$register$2;->this$0:Lcom/squareup/ui/buyer/tip/TipReaderHandler;

    invoke-static {v0}, Lcom/squareup/ui/buyer/tip/TipReaderHandler;->access$getActiveCardReader$p(Lcom/squareup/ui/buyer/tip/TipReaderHandler;)Lcom/squareup/cardreader/dipper/ActiveCardReader;

    move-result-object v0

    .line 68
    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    .line 67
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->unsetActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 76
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/tip/TipReaderHandler$register$2;->this$0:Lcom/squareup/ui/buyer/tip/TipReaderHandler;

    invoke-static {v0}, Lcom/squareup/ui/buyer/tip/TipReaderHandler;->access$getTransaction$p(Lcom/squareup/ui/buyer/tip/TipReaderHandler;)Lcom/squareup/payment/Transaction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasBillPayment()Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/buyer/tip/TipReaderHandler$register$2;->this$0:Lcom/squareup/ui/buyer/tip/TipReaderHandler;

    invoke-static {v0}, Lcom/squareup/ui/buyer/tip/TipReaderHandler;->access$getTransaction$p(Lcom/squareup/ui/buyer/tip/TipReaderHandler;)Lcom/squareup/payment/Transaction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->hasMagStripeTenderInFlight()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 78
    :goto_0
    iget-object v3, p0, Lcom/squareup/ui/buyer/tip/TipReaderHandler$register$2;->this$0:Lcom/squareup/ui/buyer/tip/TipReaderHandler;

    invoke-static {v3}, Lcom/squareup/ui/buyer/tip/TipReaderHandler;->access$getTenderInEdit$p(Lcom/squareup/ui/buyer/tip/TipReaderHandler;)Lcom/squareup/payment/TenderInEdit;

    move-result-object v3

    invoke-interface {v3}, Lcom/squareup/payment/TenderInEdit;->isEditingTender()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/squareup/ui/buyer/tip/TipReaderHandler$register$2;->this$0:Lcom/squareup/ui/buyer/tip/TipReaderHandler;

    invoke-static {v3}, Lcom/squareup/ui/buyer/tip/TipReaderHandler;->access$getTenderInEdit$p(Lcom/squareup/ui/buyer/tip/TipReaderHandler;)Lcom/squareup/payment/TenderInEdit;

    move-result-object v3

    invoke-interface {v3}, Lcom/squareup/payment/TenderInEdit;->isMagStripeTender()Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    if-nez v0, :cond_5

    if-eqz v1, :cond_3

    goto :goto_3

    .line 84
    :cond_3
    iget-object v0, p0, Lcom/squareup/ui/buyer/tip/TipReaderHandler$register$2;->this$0:Lcom/squareup/ui/buyer/tip/TipReaderHandler;

    invoke-static {v0}, Lcom/squareup/ui/buyer/tip/TipReaderHandler;->access$getTransaction$p(Lcom/squareup/ui/buyer/tip/TipReaderHandler;)Lcom/squareup/payment/Transaction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasBillPayment()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/buyer/tip/TipReaderHandler$register$2;->this$0:Lcom/squareup/ui/buyer/tip/TipReaderHandler;

    invoke-static {v0}, Lcom/squareup/ui/buyer/tip/TipReaderHandler;->access$getTransaction$p(Lcom/squareup/ui/buyer/tip/TipReaderHandler;)Lcom/squareup/payment/Transaction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->hasSmartCardTenderWithCaptureArgs()Z

    move-result v0

    if-nez v0, :cond_5

    .line 88
    iget-object v0, p0, Lcom/squareup/ui/buyer/tip/TipReaderHandler$register$2;->this$0:Lcom/squareup/ui/buyer/tip/TipReaderHandler;

    invoke-static {v0}, Lcom/squareup/ui/buyer/tip/TipReaderHandler;->access$getTransaction$p(Lcom/squareup/ui/buyer/tip/TipReaderHandler;)Lcom/squareup/payment/Transaction;

    move-result-object v0

    sget-object v1, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_HUMAN_INITIATED:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    invoke-virtual {v0, v2, v1}, Lcom/squareup/payment/Transaction;->dropPaymentOrTender(ZLcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    .line 89
    new-instance v0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;-><init>()V

    .line 90
    sget-object v1, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->GENERIC_ERROR:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->warningType(Lcom/squareup/cardreader/ui/api/ReaderWarningType;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object v0

    .line 92
    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object p1

    const-string v1, "cardReader.cardReaderInfo"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->isWireless()Z

    move-result p1

    if-eqz p1, :cond_4

    .line 93
    sget p1, Lcom/squareup/cardreader/R$string;->contactless_reader_disconnected_title:I

    goto :goto_2

    .line 95
    :cond_4
    sget p1, Lcom/squareup/checkout/R$string;->emv_reader_disconnected_title:I

    .line 91
    :goto_2
    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->titleId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 97
    sget v0, Lcom/squareup/checkout/R$string;->emv_reader_disconnected_msg:I

    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->messageId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 98
    invoke-virtual {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->build()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object p1

    .line 99
    iget-object v0, p0, Lcom/squareup/ui/buyer/tip/TipReaderHandler$register$2;->this$0:Lcom/squareup/ui/buyer/tip/TipReaderHandler;

    invoke-static {v0}, Lcom/squareup/ui/buyer/tip/TipReaderHandler;->access$getReaderIssueScreenRequestSink$p(Lcom/squareup/ui/buyer/tip/TipReaderHandler;)Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowWarningScreen;

    const-string v2, "params"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, p1}, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowWarningScreen;-><init>(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;)V

    check-cast v1, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;->requestReaderIssueScreen(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;)V

    :cond_5
    :goto_3
    return-void
.end method
