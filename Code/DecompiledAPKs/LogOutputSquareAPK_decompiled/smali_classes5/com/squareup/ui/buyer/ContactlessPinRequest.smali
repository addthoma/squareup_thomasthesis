.class public Lcom/squareup/ui/buyer/ContactlessPinRequest;
.super Ljava/lang/Object;
.source "ContactlessPinRequest.java"


# static fields
.field public static final NONE:Lcom/squareup/ui/buyer/ContactlessPinRequest;


# instance fields
.field public final canSkip:Z

.field public final cardInfo:Lcom/squareup/cardreader/CardInfo;

.field public final isFinal:Z

.field public final pinRequested:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 29
    new-instance v0, Lcom/squareup/ui/buyer/ContactlessPinRequest;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, v1, v1, v1, v2}, Lcom/squareup/ui/buyer/ContactlessPinRequest;-><init>(ZZZLcom/squareup/cardreader/CardInfo;)V

    sput-object v0, Lcom/squareup/ui/buyer/ContactlessPinRequest;->NONE:Lcom/squareup/ui/buyer/ContactlessPinRequest;

    return-void
.end method

.method constructor <init>(ZZZLcom/squareup/cardreader/CardInfo;)V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-boolean p1, p0, Lcom/squareup/ui/buyer/ContactlessPinRequest;->canSkip:Z

    .line 19
    iput-boolean p2, p0, Lcom/squareup/ui/buyer/ContactlessPinRequest;->isFinal:Z

    .line 20
    iput-boolean p3, p0, Lcom/squareup/ui/buyer/ContactlessPinRequest;->pinRequested:Z

    .line 21
    iput-object p4, p0, Lcom/squareup/ui/buyer/ContactlessPinRequest;->cardInfo:Lcom/squareup/cardreader/CardInfo;

    return-void
.end method

.method public static create(Lcom/squareup/cardreader/PinRequestData;)Lcom/squareup/ui/buyer/ContactlessPinRequest;
    .locals 4

    .line 25
    new-instance v0, Lcom/squareup/ui/buyer/ContactlessPinRequest;

    invoke-virtual {p0}, Lcom/squareup/cardreader/PinRequestData;->getCanSkip()Z

    move-result v1

    .line 26
    invoke-virtual {p0}, Lcom/squareup/cardreader/PinRequestData;->getFinalPinAttempt()Z

    move-result v2

    invoke-virtual {p0}, Lcom/squareup/cardreader/PinRequestData;->getCardInfo()Lcom/squareup/cardreader/CardInfo;

    move-result-object p0

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3, p0}, Lcom/squareup/ui/buyer/ContactlessPinRequest;-><init>(ZZZLcom/squareup/cardreader/CardInfo;)V

    return-object v0
.end method
