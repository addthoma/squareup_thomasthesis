.class public Lcom/squareup/ui/buyer/actionbar/BuyerActionBarScrollLayout;
.super Landroid/widget/ScrollView;
.source "BuyerActionBarScrollLayout.java"

# interfaces
.implements Lcom/squareup/ui/buyer/actionbar/BuyerActionBarHost;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private backPressedHandler:Lrx/functions/Action0;

.field protected buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 31
    invoke-direct {p0, p1, p2}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p1, 0x0

    .line 28
    iput-object p1, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBarScrollLayout;->backPressedHandler:Lrx/functions/Action0;

    const/4 p1, 0x1

    .line 32
    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/actionbar/BuyerActionBarScrollLayout;->setFillViewport(Z)V

    return-void
.end method


# virtual methods
.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 45
    sget-object p1, Lcom/squareup/ui/buyer/BuyerSpots;->BUYER_RIGHT:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public hideUpButton()V
    .locals 1

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBarScrollLayout;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->hideUpButton()V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBarScrollLayout;->backPressedHandler:Lrx/functions/Action0;

    if-eqz v0, :cond_0

    .line 78
    invoke-interface {v0}, Lrx/functions/Action0;->call()V

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 40
    invoke-super {p0}, Landroid/widget/ScrollView;->onFinishInflate()V

    .line 41
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->buyer_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;

    iput-object v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBarScrollLayout;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;

    return-void
.end method

.method public setBackPressedHandler(Lrx/functions/Action0;)V
    .locals 0

    .line 36
    iput-object p1, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBarScrollLayout;->backPressedHandler:Lrx/functions/Action0;

    return-void
.end method

.method public setCallToAction(Ljava/lang/CharSequence;)V
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBarScrollLayout;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->setCallToAction(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setOnUpClicked(Lcom/squareup/debounce/DebouncedOnClickListener;)V
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBarScrollLayout;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->setOnUpGlyphClicked(Lcom/squareup/debounce/DebouncedOnClickListener;)V

    return-void
.end method

.method public setSubtitle(Ljava/lang/CharSequence;)V
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBarScrollLayout;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->setSubtitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setTicketName(Ljava/lang/String;)V
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBarScrollLayout;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->setTicketName(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setTotal(Ljava/lang/CharSequence;)V
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBarScrollLayout;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->setTotal(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public showUpAsBack()V
    .locals 1

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBarScrollLayout;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->showBackArrow()V

    return-void
.end method
