.class public final synthetic Lcom/squareup/ui/buyer/emv/ReaderInPaymentWarningScreen$Component$-CC;
.super Ljava/lang/Object;
.source "ReaderInPaymentWarningScreen.java"


# direct methods
.method public static synthetic $default$dipRequiredFallback(Lcom/squareup/ui/buyer/emv/ReaderInPaymentWarningScreen$Component;)Lcom/squareup/ui/main/errors/ReaderWarningScreenHandler;
    .locals 1
    .param p0, "_this"    # Lcom/squareup/ui/buyer/emv/ReaderInPaymentWarningScreen$Component;

    .line 54
    invoke-interface {p0}, Lcom/squareup/ui/buyer/emv/ReaderInPaymentWarningScreen$Component;->dipRequiredFallback()Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$DipRequiredFallbackHandler;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic $default$emvSchemeFallback(Lcom/squareup/ui/buyer/emv/ReaderInPaymentWarningScreen$Component;)Lcom/squareup/ui/main/errors/ReaderWarningScreenHandler;
    .locals 1
    .param p0, "_this"    # Lcom/squareup/ui/buyer/emv/ReaderInPaymentWarningScreen$Component;

    .line 54
    invoke-interface {p0}, Lcom/squareup/ui/buyer/emv/ReaderInPaymentWarningScreen$Component;->emvSchemeFallback()Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$EmvSchemeFallbackScreenHandler;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic $default$emvTechnicalFallback(Lcom/squareup/ui/buyer/emv/ReaderInPaymentWarningScreen$Component;)Lcom/squareup/ui/main/errors/ReaderWarningScreenHandler;
    .locals 1
    .param p0, "_this"    # Lcom/squareup/ui/buyer/emv/ReaderInPaymentWarningScreen$Component;

    .line 54
    invoke-interface {p0}, Lcom/squareup/ui/buyer/emv/ReaderInPaymentWarningScreen$Component;->emvTechnicalFallback()Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$EmvTechnicalFallbackScreenHandler;

    move-result-object v0

    return-object v0
.end method
