.class public Lcom/squareup/ui/buyer/CancelNonEmvPaymentScreen$Factory;
.super Ljava/lang/Object;
.source "CancelNonEmvPaymentScreen.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/CancelNonEmvPaymentScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 38
    const-class v0, Lcom/squareup/ui/buyer/BuyerScopeComponent;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/BuyerScopeComponent;

    .line 39
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/buyer/CancelNonEmvPaymentScreen;

    .line 40
    invoke-interface {v0}, Lcom/squareup/ui/buyer/BuyerScopeComponent;->scopeRunner()Lcom/squareup/ui/buyer/BuyerScopeRunner;

    move-result-object v2

    .line 41
    invoke-interface {v0}, Lcom/squareup/ui/buyer/BuyerScopeComponent;->permissionPasscodeGatekeeper()Lcom/squareup/permissions/PermissionGatekeeper;

    move-result-object v0

    .line 43
    new-instance v3, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v3, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget p1, Lcom/squareup/cancelsplit/R$string;->discard_payment_prompt_confirm:I

    new-instance v4, Lcom/squareup/ui/buyer/-$$Lambda$CancelNonEmvPaymentScreen$Factory$Mu9GO3jjKVIwNsJsOAqsyz0ihKE;

    invoke-direct {v4, p0, v0, v2, v1}, Lcom/squareup/ui/buyer/-$$Lambda$CancelNonEmvPaymentScreen$Factory$Mu9GO3jjKVIwNsJsOAqsyz0ihKE;-><init>(Lcom/squareup/ui/buyer/CancelNonEmvPaymentScreen$Factory;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/ui/buyer/CancelNonEmvPaymentScreen;)V

    .line 44
    invoke-virtual {v3, p1, v4}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/cancelsplit/R$string;->discard_payment_prompt_cancel:I

    .line 52
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/ui/buyerflow/R$string;->discard_payment_prompt_message:I

    .line 53
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/cancelsplit/R$string;->discard_payment_prompt_title:I

    .line 54
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 55
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    const/4 v0, 0x0

    .line 56
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 58
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$create$0$CancelNonEmvPaymentScreen$Factory(Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/ui/buyer/CancelNonEmvPaymentScreen;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 46
    sget-object p4, Lcom/squareup/permissions/Permission;->CANCEL_BUYER_FLOW:Lcom/squareup/permissions/Permission;

    new-instance p5, Lcom/squareup/ui/buyer/CancelNonEmvPaymentScreen$Factory$1;

    invoke-direct {p5, p0, p2, p3}, Lcom/squareup/ui/buyer/CancelNonEmvPaymentScreen$Factory$1;-><init>(Lcom/squareup/ui/buyer/CancelNonEmvPaymentScreen$Factory;Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/ui/buyer/CancelNonEmvPaymentScreen;)V

    invoke-virtual {p1, p4, p5}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessExplicitlyGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    return-void
.end method
