.class public abstract Lcom/squareup/ui/buyer/emv/InEmvScope;
.super Lcom/squareup/ui/main/InBuyerScope;
.source "InEmvScope.java"


# instance fields
.field public final emvPath:Lcom/squareup/ui/buyer/emv/EmvScope;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/buyer/emv/EmvScope;)V
    .locals 1

    .line 9
    iget-object v0, p1, Lcom/squareup/ui/buyer/emv/EmvScope;->buyerPath:Lcom/squareup/ui/buyer/BuyerScope;

    invoke-direct {p0, v0}, Lcom/squareup/ui/main/InBuyerScope;-><init>(Lcom/squareup/ui/buyer/BuyerScope;)V

    .line 10
    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/InEmvScope;->emvPath:Lcom/squareup/ui/buyer/emv/EmvScope;

    return-void
.end method


# virtual methods
.method public final getParentKey()Ljava/lang/Object;
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/InEmvScope;->emvPath:Lcom/squareup/ui/buyer/emv/EmvScope;

    return-object v0
.end method
