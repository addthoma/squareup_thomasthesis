.class public final Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableLayoutRunner;
.super Ljava/lang/Object;
.source "StoreAndForwardQuickEnableLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableLayoutRunner$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableScreen;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u000eB\u0017\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0018\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u00022\u0006\u0010\u000c\u001a\u00020\rH\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableScreen;",
        "view",
        "Landroid/view/View;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Landroid/view/View;Lcom/squareup/util/Res;)V",
        "Lcom/squareup/marin/widgets/DetailConfirmationView;",
        "showRendering",
        "",
        "rendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "Factory",
        "buyer-flow_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final res:Lcom/squareup/util/Res;

.field private final view:Lcom/squareup/marin/widgets/DetailConfirmationView;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/squareup/util/Res;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableLayoutRunner;->res:Lcom/squareup/util/Res;

    .line 27
    check-cast p1, Lcom/squareup/marin/widgets/DetailConfirmationView;

    iput-object p1, p0, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableLayoutRunner;->view:Lcom/squareup/marin/widgets/DetailConfirmationView;

    return-void
.end method


# virtual methods
.method public showRendering(Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 3

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    iget-object p2, p0, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableLayoutRunner;->view:Lcom/squareup/marin/widgets/DetailConfirmationView;

    .line 34
    new-instance v0, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-virtual {p2}, Lcom/squareup/marin/widgets/DetailConfirmationView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    .line 35
    sget v1, Lcom/squareup/ui/buyerflow/R$string;->offline_mode_for_more_information_quick_enable:I

    const-string v2, "support_center"

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    .line 36
    sget v1, Lcom/squareup/billhistoryui/R$string;->offline_mode_url:I

    invoke-virtual {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    .line 37
    sget v1, Lcom/squareup/checkout/R$string;->support_center:I

    invoke-virtual {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    .line 38
    invoke-virtual {v0}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object v0

    .line 33
    invoke-virtual {p2, v0}, Lcom/squareup/marin/widgets/DetailConfirmationView;->setHelperText(Ljava/lang/CharSequence;)V

    .line 41
    iget-object p2, p0, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableLayoutRunner;->view:Lcom/squareup/marin/widgets/DetailConfirmationView;

    .line 43
    invoke-virtual {p2}, Lcom/squareup/marin/widgets/DetailConfirmationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/common/bootstrap/R$string;->offline_mode_warning:I

    .line 44
    sget v2, Lcom/squareup/common/bootstrap/R$dimen;->message_new_line_spacing:I

    .line 42
    invoke-static {v0, v1, v2}, Lcom/squareup/text/Fonts;->addSectionBreaks(Landroid/content/res/Resources;II)Ljava/lang/CharSequence;

    move-result-object v0

    .line 41
    invoke-virtual {p2, v0}, Lcom/squareup/marin/widgets/DetailConfirmationView;->setMessage(Ljava/lang/CharSequence;)V

    .line 48
    iget-object p2, p0, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableLayoutRunner;->view:Lcom/squareup/marin/widgets/DetailConfirmationView;

    new-instance v0, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableLayoutRunner$showRendering$1;

    invoke-direct {v0, p1}, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableLayoutRunner$showRendering$1;-><init>(Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableScreen;)V

    check-cast v0, Lcom/squareup/debounce/DebouncedOnClickListener;

    invoke-virtual {p2, v0}, Lcom/squareup/marin/widgets/DetailConfirmationView;->setOnConfirmListener(Lcom/squareup/debounce/DebouncedOnClickListener;)V

    .line 54
    iget-object p2, p0, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableLayoutRunner;->view:Lcom/squareup/marin/widgets/DetailConfirmationView;

    check-cast p2, Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableScreen;->getOnCancelPayment()Lkotlin/jvm/functions/Function0;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 56
    new-instance p2, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {p2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 57
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v1, p0, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableLayoutRunner;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/ui/buyerflow/R$string;->enable_offline_mode:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {p2, v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p2

    .line 58
    invoke-virtual {p1}, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableScreen;->getOnCancelPayment()Lkotlin/jvm/functions/Function0;

    move-result-object p1

    if-eqz p1, :cond_0

    new-instance v0, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableLayoutRunner$sam$java_lang_Runnable$0;

    invoke-direct {v0, p1}, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableLayoutRunner$sam$java_lang_Runnable$0;-><init>(Lkotlin/jvm/functions/Function0;)V

    move-object p1, v0

    :cond_0
    check-cast p1, Ljava/lang/Runnable;

    invoke-virtual {p2, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 59
    iget-object p2, p0, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableLayoutRunner;->view:Lcom/squareup/marin/widgets/DetailConfirmationView;

    check-cast p2, Landroid/view/View;

    invoke-static {p2}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object p2

    const-string v0, "ActionBarView.findIn(view)"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 18
    check-cast p1, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableScreen;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableLayoutRunner;->showRendering(Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableScreen;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
