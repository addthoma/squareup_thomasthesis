.class final Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow$getPaymentTypeInfo$$inlined$let$lambda$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealBillReceiptWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow;->getPaymentTypeInfo(Lcom/squareup/payment/PaymentReceipt;)Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/util/Res;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005\u00a8\u0006\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "kotlin.jvm.PlatformType",
        "res",
        "Lcom/squareup/util/Res;",
        "invoke",
        "com/squareup/ui/buyer/receipt/RealBillReceiptWorkflow$getPaymentTypeInfo$2$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $receipt$inlined:Lcom/squareup/payment/PaymentReceipt;


# direct methods
.method constructor <init>(Lcom/squareup/payment/PaymentReceipt;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow$getPaymentTypeInfo$$inlined$let$lambda$1;->$receipt$inlined:Lcom/squareup/payment/PaymentReceipt;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 43
    check-cast p1, Lcom/squareup/util/Res;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow$getPaymentTypeInfo$$inlined$let$lambda$1;->invoke(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 1

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 206
    iget-object v0, p0, Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow$getPaymentTypeInfo$$inlined$let$lambda$1;->$receipt$inlined:Lcom/squareup/payment/PaymentReceipt;

    invoke-virtual {v0, p1}, Lcom/squareup/payment/PaymentReceipt;->getRemainingBalanceText(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "receipt.getRemainingBalanceText(res)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
