.class final Lcom/squareup/ui/buyer/BuyerActionContainer$primaryActionContainer$2;
.super Lkotlin/jvm/internal/Lambda;
.source "BuyerActionContainer.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/buyer/BuyerActionContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Landroid/widget/LinearLayout;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "Landroid/widget/LinearLayout;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/buyer/BuyerActionContainer;


# direct methods
.method constructor <init>(Lcom/squareup/ui/buyer/BuyerActionContainer;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/buyer/BuyerActionContainer$primaryActionContainer$2;->this$0:Lcom/squareup/ui/buyer/BuyerActionContainer;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()Landroid/widget/LinearLayout;
    .locals 2

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerActionContainer$primaryActionContainer$2;->this$0:Lcom/squareup/ui/buyer/BuyerActionContainer;

    .line 51
    sget v1, Lcom/squareup/checkout/R$id;->primary_action_container:I

    .line 50
    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 37
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/BuyerActionContainer$primaryActionContainer$2;->invoke()Landroid/widget/LinearLayout;

    move-result-object v0

    return-object v0
.end method
