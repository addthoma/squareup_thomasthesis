.class Lcom/squareup/ui/buyer/auth/EmvApprovedScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "EmvApprovedScreen.java"

# interfaces
.implements Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/auth/EmvApprovedScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/buyer/auth/EmvApprovedView;",
        ">;",
        "Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;"
    }
.end annotation


# instance fields
.field private final buyerAmountTextProvider:Lcom/squareup/ui/buyer/BuyerAmountTextProvider;

.field private final buyerFlowStarter:Lcom/squareup/ui/buyer/BuyerFlowStarter;

.field private final buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

.field private final buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

.field private final clock:Lcom/squareup/util/Clock;

.field private final emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private proceedAfterDelay:Ljava/lang/Runnable;

.field private showingApprovedIndicatorSince:J

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method protected constructor <init>(Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Clock;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;Lcom/squareup/ui/buyer/BuyerFlowStarter;Lcom/squareup/ui/buyer/BuyerAmountTextProvider;Lcom/squareup/buyer/language/BuyerLocaleOverride;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 81
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    const-wide/16 v0, -0x1

    .line 75
    iput-wide v0, p0, Lcom/squareup/ui/buyer/auth/EmvApprovedScreen$Presenter;->showingApprovedIndicatorSince:J

    .line 82
    iput-object p1, p0, Lcom/squareup/ui/buyer/auth/EmvApprovedScreen$Presenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    .line 83
    iput-object p2, p0, Lcom/squareup/ui/buyer/auth/EmvApprovedScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 84
    iput-object p3, p0, Lcom/squareup/ui/buyer/auth/EmvApprovedScreen$Presenter;->clock:Lcom/squareup/util/Clock;

    .line 85
    iput-object p4, p0, Lcom/squareup/ui/buyer/auth/EmvApprovedScreen$Presenter;->mainThread:Lcom/squareup/thread/executor/MainThread;

    .line 86
    iput-object p5, p0, Lcom/squareup/ui/buyer/auth/EmvApprovedScreen$Presenter;->emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    .line 87
    iput-object p6, p0, Lcom/squareup/ui/buyer/auth/EmvApprovedScreen$Presenter;->buyerFlowStarter:Lcom/squareup/ui/buyer/BuyerFlowStarter;

    .line 88
    iput-object p7, p0, Lcom/squareup/ui/buyer/auth/EmvApprovedScreen$Presenter;->buyerAmountTextProvider:Lcom/squareup/ui/buyer/BuyerAmountTextProvider;

    .line 89
    iput-object p8, p0, Lcom/squareup/ui/buyer/auth/EmvApprovedScreen$Presenter;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    return-void
.end method

.method private updateView()V
    .locals 2

    .line 156
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/auth/EmvApprovedScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/auth/EmvApprovedView;

    .line 157
    iget-object v1, p0, Lcom/squareup/ui/buyer/auth/EmvApprovedScreen$Presenter;->buyerAmountTextProvider:Lcom/squareup/ui/buyer/BuyerAmountTextProvider;

    invoke-virtual {v1}, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;->getBuyerFormattedTotalAmount()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/auth/EmvApprovedView;->setTotal(Ljava/lang/CharSequence;)V

    .line 158
    iget-object v1, p0, Lcom/squareup/ui/buyer/auth/EmvApprovedScreen$Presenter;->buyerAmountTextProvider:Lcom/squareup/ui/buyer/BuyerAmountTextProvider;

    invoke-virtual {v1}, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;->getBuyerFormattedAmountDueAutoGratuityAndTip()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/auth/EmvApprovedView;->setSubtitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private waitSomeMoreAfterSuccess()V
    .locals 5

    .line 113
    iget-object v0, p0, Lcom/squareup/ui/buyer/auth/EmvApprovedScreen$Presenter;->proceedAfterDelay:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 114
    iget-object v1, p0, Lcom/squareup/ui/buyer/auth/EmvApprovedScreen$Presenter;->mainThread:Lcom/squareup/thread/executor/MainThread;

    invoke-interface {v1, v0}, Lcom/squareup/thread/executor/MainThread;->cancel(Ljava/lang/Runnable;)V

    .line 117
    :cond_0
    new-instance v0, Lcom/squareup/ui/buyer/auth/-$$Lambda$EmvApprovedScreen$Presenter$whvQRLEYYqsYKWcRVhDE0nZBgME;

    invoke-direct {v0, p0}, Lcom/squareup/ui/buyer/auth/-$$Lambda$EmvApprovedScreen$Presenter$whvQRLEYYqsYKWcRVhDE0nZBgME;-><init>(Lcom/squareup/ui/buyer/auth/EmvApprovedScreen$Presenter;)V

    iput-object v0, p0, Lcom/squareup/ui/buyer/auth/EmvApprovedScreen$Presenter;->proceedAfterDelay:Ljava/lang/Runnable;

    .line 141
    iget-wide v0, p0, Lcom/squareup/ui/buyer/auth/EmvApprovedScreen$Presenter;->showingApprovedIndicatorSince:J

    const-wide/16 v2, -0x1

    cmp-long v4, v0, v2

    if-nez v4, :cond_1

    .line 142
    iget-object v0, p0, Lcom/squareup/ui/buyer/auth/EmvApprovedScreen$Presenter;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getElapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/squareup/ui/buyer/auth/EmvApprovedScreen$Presenter;->showingApprovedIndicatorSince:J

    .line 145
    :cond_1
    iget-wide v0, p0, Lcom/squareup/ui/buyer/auth/EmvApprovedScreen$Presenter;->showingApprovedIndicatorSince:J

    sget-object v2, Lcom/squareup/register/widgets/GlassSpinner;->MIN_SPINNER_TIME_UNIT:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v3, 0x320

    .line 146
    invoke-virtual {v2, v3, v4}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    add-long/2addr v0, v2

    iget-object v2, p0, Lcom/squareup/ui/buyer/auth/EmvApprovedScreen$Presenter;->clock:Lcom/squareup/util/Clock;

    .line 147
    invoke-interface {v2}, Lcom/squareup/util/Clock;->getElapsedRealtime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-lez v4, :cond_2

    .line 149
    iget-object v2, p0, Lcom/squareup/ui/buyer/auth/EmvApprovedScreen$Presenter;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v3, p0, Lcom/squareup/ui/buyer/auth/EmvApprovedScreen$Presenter;->proceedAfterDelay:Ljava/lang/Runnable;

    invoke-interface {v2, v3, v0, v1}, Lcom/squareup/thread/executor/MainThread;->executeDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 151
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/buyer/auth/EmvApprovedScreen$Presenter;->proceedAfterDelay:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    :goto_0
    return-void
.end method


# virtual methods
.method public synthetic lambda$null$0$EmvApprovedScreen$Presenter(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;)V
    .locals 2

    .line 129
    iget-object v0, p0, Lcom/squareup/ui/buyer/auth/EmvApprovedScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireReceiptForLastPayment()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v0

    .line 130
    invoke-virtual {v0}, Lcom/squareup/payment/PaymentReceipt;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->matchesOtherTenderIdPair(Lcom/squareup/protos/client/IdPair;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 132
    invoke-virtual {v0}, Lcom/squareup/payment/PaymentReceipt;->paymentComplete()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 133
    iget-object v1, p0, Lcom/squareup/ui/buyer/auth/EmvApprovedScreen$Presenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    invoke-virtual {v1, v0, p1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->goToLoyaltyScreen(Lcom/squareup/payment/PaymentReceipt;Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;)V

    :cond_0
    return-void
.end method

.method public synthetic lambda$waitSomeMoreAfterSuccess$1$EmvApprovedScreen$Presenter()V
    .locals 2

    const/4 v0, 0x0

    .line 118
    iput-object v0, p0, Lcom/squareup/ui/buyer/auth/EmvApprovedScreen$Presenter;->proceedAfterDelay:Ljava/lang/Runnable;

    .line 120
    iget-object v0, p0, Lcom/squareup/ui/buyer/auth/EmvApprovedScreen$Presenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->goToFirstScreenAfterAuthSpinner()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 123
    iget-object v0, p0, Lcom/squareup/ui/buyer/auth/EmvApprovedScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasReceiptForLastPayment()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/buyer/auth/EmvApprovedScreen$Presenter;->buyerFlowStarter:Lcom/squareup/ui/buyer/BuyerFlowStarter;

    .line 124
    invoke-interface {v0}, Lcom/squareup/ui/buyer/BuyerFlowStarter;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/squareup/ui/buyer/auth/EmvApprovedScreen$Presenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->loyaltyEvent()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/buyer/auth/-$$Lambda$EmvApprovedScreen$Presenter$sVq8O8OuVyD3LAXF5NLFWI_lw2s;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/auth/-$$Lambda$EmvApprovedScreen$Presenter$sVq8O8OuVyD3LAXF5NLFWI_lw2s;-><init>(Lcom/squareup/ui/buyer/auth/EmvApprovedScreen$Presenter;)V

    .line 128
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    :cond_0
    return-void

    .line 121
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "EmvApproved screen must proceed. Payment is already authorized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method onBackPressed()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 93
    iget-object v0, p0, Lcom/squareup/ui/buyer/auth/EmvApprovedScreen$Presenter;->emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    invoke-interface {v0, p1, p0}, Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;->registerEmvCardInsertRemoveProcessor(Lmortar/MortarScope;Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 2

    .line 99
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/auth/EmvApprovedScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/buyer/auth/EmvApprovedView;

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/auth/EmvApprovedView;->transitionToCheck()V

    .line 100
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/auth/EmvApprovedScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/buyer/auth/EmvApprovedView;

    iget-object v0, p0, Lcom/squareup/ui/buyer/auth/EmvApprovedScreen$Presenter;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    invoke-interface {v0}, Lcom/squareup/buyer/language/BuyerLocaleOverride;->getBuyerRes()Lcom/squareup/util/Res;

    move-result-object v0

    sget v1, Lcom/squareup/checkout/R$string;->buyer_approved:I

    .line 101
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    .line 100
    invoke-virtual {p1, v0, v1}, Lcom/squareup/ui/buyer/auth/EmvApprovedView;->setText(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    invoke-direct {p0}, Lcom/squareup/ui/buyer/auth/EmvApprovedScreen$Presenter;->waitSomeMoreAfterSuccess()V

    .line 103
    invoke-direct {p0}, Lcom/squareup/ui/buyer/auth/EmvApprovedScreen$Presenter;->updateView()V

    return-void
.end method

.method public processEmvCardInserted(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    return-void
.end method

.method public processEmvCardRemoved(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    return-void
.end method
