.class public final Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningScreen;
.super Lcom/squareup/ui/main/InBuyerScope;
.source "PartialAuthWarningScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningScreen$Component;,
        Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningScreen$Module;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 41
    sget-object v0, Lcom/squareup/ui/buyer/partialauth/-$$Lambda$PartialAuthWarningScreen$P4qiqBut1KzTcvQUXuDjDM933J0;->INSTANCE:Lcom/squareup/ui/buyer/partialauth/-$$Lambda$PartialAuthWarningScreen$P4qiqBut1KzTcvQUXuDjDM933J0;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/buyer/BuyerScope;)V
    .locals 0

    .line 20
    invoke-direct {p0, p1}, Lcom/squareup/ui/main/InBuyerScope;-><init>(Lcom/squareup/ui/buyer/BuyerScope;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningScreen;
    .locals 1

    .line 42
    const-class v0, Lcom/squareup/ui/buyer/BuyerScope;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/buyer/BuyerScope;

    .line 43
    new-instance v0, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningScreen;-><init>(Lcom/squareup/ui/buyer/BuyerScope;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 37
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/main/InBuyerScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 38
    iget-object v0, p0, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningScreen;->buyerPath:Lcom/squareup/ui/buyer/BuyerScope;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public screenLayout()I
    .locals 1

    .line 47
    sget v0, Lcom/squareup/ui/buyerflow/R$layout;->partial_auth_warning_view:I

    return v0
.end method
