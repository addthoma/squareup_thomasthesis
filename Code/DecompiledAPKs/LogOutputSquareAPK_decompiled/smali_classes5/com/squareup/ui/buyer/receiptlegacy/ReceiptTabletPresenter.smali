.class Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;
.super Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;
.source "ReceiptTabletPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter<",
        "Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private autoFinishing:Z

.field public autocompletePossible:Z

.field private glyphTitleId:I

.field private noReceiptSelected:Z


# direct methods
.method constructor <init>(Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/settings/server/AccountStatusSettings;Ljavax/inject/Provider;Lcom/squareup/ui/main/TransactionMetrics;Lcom/squareup/receipt/ReceiptSender;Lcom/squareup/register/tutorial/TutorialPresenter;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/print/PrinterStations;Lcom/squareup/merchantimages/CuratedImage;Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseProvider;Lcom/squareup/settings/server/Features;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/ui/main/TopScreenChecker;Lcom/squareup/payment/Transaction;Lcom/squareup/analytics/Analytics;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/util/Res;Lcom/squareup/print/PrintSettings;Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;)V
    .locals 32
    .param p17    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p18    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/buyer/BuyerScopeRunner;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;",
            "Lcom/squareup/ui/main/TransactionMetrics;",
            "Lcom/squareup/receipt/ReceiptSender;",
            "Lcom/squareup/register/tutorial/TutorialPresenter;",
            "Lcom/squareup/thread/executor/MainThread;",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            "Lcom/squareup/log/CheckoutInformationEventLogger;",
            "Lcom/squareup/cardreader/CardReaderHub;",
            "Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;",
            "Lcom/squareup/text/PhoneNumberHelper;",
            "Lcom/squareup/print/PrinterStations;",
            "Lcom/squareup/merchantimages/CuratedImage;",
            "Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseProvider;",
            "Lcom/squareup/settings/server/Features;",
            "Lio/reactivex/Scheduler;",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            "Lcom/squareup/ui/main/TopScreenChecker;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            "Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/print/PrintSettings;",
            "Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v15, p7

    move-object/from16 v8, p8

    move-object/from16 v7, p9

    move-object/from16 v9, p10

    move-object/from16 v10, p11

    move-object/from16 v11, p12

    move-object/from16 v12, p13

    move-object/from16 v13, p14

    move-object/from16 v14, p15

    move-object/from16 v22, p16

    move-object/from16 v17, p17

    move-object/from16 v18, p18

    move-object/from16 v16, p19

    move-object/from16 v19, p20

    move-object/from16 v20, p21

    move-object/from16 v21, p22

    move-object/from16 v23, p23

    move-object/from16 v24, p24

    move-object/from16 v25, p25

    move-object/from16 v26, p26

    move-object/from16 v27, p27

    move-object/from16 v28, p28

    move-object/from16 v29, p29

    move-object/from16 v30, p30

    move-object/from16 v31, p31

    .line 95
    invoke-direct/range {v0 .. v31}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;-><init>(Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/settings/server/AccountStatusSettings;Ljavax/inject/Provider;Lcom/squareup/ui/main/TransactionMetrics;Lcom/squareup/receipt/ReceiptSender;Lcom/squareup/register/tutorial/TutorialPresenter;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/print/PrinterStations;Lcom/squareup/merchantimages/CuratedImage;Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseProvider;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/payment/OfflineModeMonitor;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/ui/main/TopScreenChecker;Lcom/squareup/payment/Transaction;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/util/Res;Lcom/squareup/print/PrintSettings;Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;)V

    const/4 v0, -0x1

    move-object/from16 v1, p0

    .line 75
    iput v0, v1, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->glyphTitleId:I

    return-void
.end method

.method private onReceiptOptionSelected(Lcom/squareup/glyph/GlyphTypeface$Glyph;IZLcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;)V
    .locals 0

    .line 306
    invoke-direct/range {p0 .. p5}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->selectReceiptOptionAndAnimate(Lcom/squareup/glyph/GlyphTypeface$Glyph;IZLcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;)V

    .line 307
    invoke-virtual {p0, p4}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->updateMessages(Lcom/squareup/locale/LocaleOverrideFactory;)V

    .line 308
    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

    invoke-interface {p1}, Lcom/squareup/log/CheckoutInformationEventLogger;->updateTentativeEndTime()V

    .line 309
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->maybeGoToLoyaltyOrEmailCollectionScreen()V

    return-void
.end method

.method private printReceipt(Lcom/squareup/print/payload/ReceiptPayload$RenderType;Lcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 7

    .line 241
    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->tryPrintReceipt(Lcom/squareup/print/payload/ReceiptPayload$RenderType;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 242
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->receiptAutoCloseProvider:Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseProvider;

    invoke-interface {v0}, Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseProvider;->hasReceiptAutoCloseOverride()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 243
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->doScheduleFinish()V

    .line 245
    :cond_0
    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_RECEIPT:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v3, Lcom/squareup/billhistoryui/R$string;->buyer_send_receipt_printed:I

    const/4 v4, 0x0

    sget-object v0, Lcom/squareup/print/payload/ReceiptPayload$RenderType;->FORMAL_RECEIPT:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    if-ne p1, v0, :cond_1

    sget-object p1, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;->FORMAL_PAPER:Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;

    goto :goto_0

    :cond_1
    sget-object p1, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;->PAPER:Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;

    :goto_0
    move-object v6, p1

    move-object v1, p0

    move-object v5, p2

    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->onReceiptOptionSelected(Lcom/squareup/glyph/GlyphTypeface$Glyph;IZLcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;)V

    :cond_2
    return-void
.end method

.method private selectReceiptOptionAndAnimate(Lcom/squareup/glyph/GlyphTypeface$Glyph;IZLcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;)V
    .locals 1

    .line 315
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    invoke-virtual {v0, p5}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->setReceiptSelectionMade(Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;)V

    .line 316
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->endTransaction()V

    .line 318
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object p5

    invoke-virtual {p5}, Lcom/squareup/payment/PaymentReceipt;->isReceiptDeferred()Z

    move-result p5

    if-eqz p5, :cond_0

    const/4 p2, -0x1

    .line 319
    :cond_0
    iput p2, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->glyphTitleId:I

    .line 320
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->getView()Ljava/lang/Object;

    move-result-object p5

    check-cast p5, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;

    invoke-virtual {p5, p1, p2, p3, p4}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->animateToGlyphState(Lcom/squareup/glyph/GlyphTypeface$Glyph;IZLcom/squareup/locale/LocaleOverrideFactory;)V

    .line 322
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->maybeEnableClickAnywhereToFinish()V

    .line 324
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->onShowingThanks()V

    return-void
.end method

.method private setSubtitle(ILcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 1

    .line 279
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;

    if-nez v0, :cond_0

    return-void

    .line 282
    :cond_0
    invoke-virtual {p2}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->setSubtitle(ILcom/squareup/util/Res;)V

    return-void
.end method

.method private updateSubtitle(ILcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 1

    .line 328
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->shouldRemoveCard()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 329
    sget p1, Lcom/squareup/billhistoryui/R$string;->please_remove_card:I

    goto :goto_0

    .line 330
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->receiptSelectionMade()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 331
    sget p1, Lcom/squareup/ui/buyerflow/R$string;->buyer_send_receipt_all_done:I

    .line 333
    :cond_1
    :goto_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->setSubtitle(ILcom/squareup/locale/LocaleOverrideFactory;)V

    return-void
.end method


# virtual methods
.method noReceiptClicked(Lcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 8

    .line 253
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->tryDeclineReceipt()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 254
    iput-boolean v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->noReceiptSelected:Z

    .line 255
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->receiptAutoCloseProvider:Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseProvider;

    invoke-interface {v0}, Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseProvider;->hasReceiptAutoCloseOverride()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 256
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->scheduleAutoFinish()V

    .line 258
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->setDigitalReceiptHint(Ljava/lang/CharSequence;)V

    .line 259
    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CHECK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v4, -0x1

    const/4 v5, 0x0

    sget-object v7, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;->NO_RECEIPT:Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;

    move-object v2, p0

    move-object v6, p1

    invoke-direct/range {v2 .. v7}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->onReceiptOptionSelected(Lcom/squareup/glyph/GlyphTypeface$Glyph;IZLcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;)V

    :cond_1
    return-void
.end method

.method onClickAnywhere()V
    .locals 0

    .line 286
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->cancelAutoFinish()V

    .line 287
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->endTransaction()V

    .line 288
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->finish()V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    .line 106
    invoke-super {p0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 107
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptScreenLegacy;

    .line 108
    iget-boolean p1, p1, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptScreenLegacy;->allowAutocomplete:Z

    iput-boolean p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->autocompletePossible:Z

    return-void
.end method

.method printFormalReceiptClicked(Lcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 1

    .line 231
    sget-object v0, Lcom/squareup/print/payload/ReceiptPayload$RenderType;->FORMAL_RECEIPT:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    invoke-direct {p0, v0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->printReceipt(Lcom/squareup/print/payload/ReceiptPayload$RenderType;Lcom/squareup/locale/LocaleOverrideFactory;)V

    .line 232
    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterTapName;->PRINT_FORMAL_RECEIPT:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    return-void
.end method

.method printReceiptClicked(Lcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 1

    .line 236
    sget-object v0, Lcom/squareup/print/payload/ReceiptPayload$RenderType;->RECEIPT:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    invoke-direct {p0, v0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->printReceipt(Lcom/squareup/print/payload/ReceiptPayload$RenderType;Lcom/squareup/locale/LocaleOverrideFactory;)V

    .line 237
    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterTapName;->PRINT_RECEIPT:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    return-void
.end method

.method public scheduleAutoFinish()V
    .locals 1

    .line 292
    iget-boolean v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->autocompletePossible:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->noReceiptSelected:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->shouldSkipReceipt()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 293
    iput-boolean v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->autoFinishing:Z

    .line 294
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->doScheduleFinish()V

    :cond_1
    return-void
.end method

.method sendEmailReceiptClicked(Lcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 8

    .line 200
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->CHECKOUT_EMAIL_RECEIPT:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 201
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->trySendEmailReceipt()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 202
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->logEmailReceiptSent()V

    .line 203
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->receiptAutoCloseProvider:Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseProvider;

    invoke-interface {v0}, Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseProvider;->hasReceiptAutoCloseOverride()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 204
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->doScheduleFinish()V

    .line 206
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->useValidEmailAddress()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 207
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->getValidEmail()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->setValidEmailAddress(Ljava/lang/String;)V

    .line 209
    :cond_1
    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_ENVELOPE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v4, Lcom/squareup/billhistoryui/R$string;->buyer_send_receipt_email:I

    const/4 v5, 0x0

    sget-object v7, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;->DIGITAL:Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;

    move-object v2, p0

    move-object v6, p1

    invoke-direct/range {v2 .. v7}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->onReceiptOptionSelected(Lcom/squareup/glyph/GlyphTypeface$Glyph;IZLcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;)V

    :cond_2
    return-void
.end method

.method sendSmsReceiptClicked(Lcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 8

    .line 216
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->CHECKOUT_SMS_RECEIPT:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 217
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->trySendSmsReceipt()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 218
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->receiptAutoCloseProvider:Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseProvider;

    invoke-interface {v0}, Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseProvider;->hasReceiptAutoCloseOverride()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 219
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->doScheduleFinish()V

    .line 221
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->useValidSmsNumber()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 222
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->getValidSmsNumber()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->setValidSmsNumber(Ljava/lang/String;)V

    .line 224
    :cond_1
    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_SMS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v4, Lcom/squareup/billhistoryui/R$string;->buyer_send_receipt_text:I

    const/4 v5, 0x0

    sget-object v7, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;->DIGITAL:Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;

    move-object v2, p0

    move-object v6, p1

    invoke-direct/range {v2 .. v7}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->onReceiptOptionSelected(Lcom/squareup/glyph/GlyphTypeface$Glyph;IZLcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;)V

    :cond_2
    return-void
.end method

.method showAllDone(Lcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 7

    .line 112
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;

    .line 113
    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getMoneyFormatter()Lcom/squareup/text/Formatter;

    move-result-object v1

    .line 114
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/payment/Payment;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->setTitle(Ljava/lang/CharSequence;)V

    .line 115
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->shouldSkipReceipt()Z

    move-result v1

    const/4 v6, 0x0

    if-eqz v1, :cond_1

    .line 118
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->shouldShowEmailSent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 119
    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_ENVELOPE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v2, Lcom/squareup/billhistoryui/R$string;->buyer_send_receipt_email:I

    const/4 v3, 0x1

    sget-object v5, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;->DIGITAL:Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;

    move-object v0, p0

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->selectReceiptOptionAndAnimate(Lcom/squareup/glyph/GlyphTypeface$Glyph;IZLcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;)V

    goto :goto_0

    .line 123
    :cond_0
    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CHECK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v2, -0x1

    const/4 v3, 0x1

    sget-object v5, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;->NO_RECEIPT:Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;

    move-object v0, p0

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->selectReceiptOptionAndAnimate(Lcom/squareup/glyph/GlyphTypeface$Glyph;IZLcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;)V

    .line 125
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;

    invoke-virtual {v0, v6}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->setDigitalReceiptHint(Ljava/lang/CharSequence;)V

    .line 127
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->scheduleAutoFinish()V

    .line 128
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->onShowingThanks()V

    goto :goto_1

    .line 130
    :cond_1
    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    sget-object v2, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;->NO_RECEIPT:Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;

    invoke-virtual {v1, v2}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->setReceiptSelectionMade(Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;)V

    .line 131
    iget v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->glyphTitleId:I

    invoke-virtual {v0, v1, p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->setPostReceiptVisible(ILcom/squareup/locale/LocaleOverrideFactory;)V

    .line 132
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->maybeEnableClickAnywhereToFinish()V

    .line 134
    iget-boolean v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->autoFinishing:Z

    if-eqz v0, :cond_2

    .line 135
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->scheduleAutoFinish()V

    .line 137
    :cond_2
    iget-boolean v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->noReceiptSelected:Z

    if-eqz v0, :cond_3

    .line 138
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;

    invoke-virtual {v0, v6}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->setDigitalReceiptHint(Ljava/lang/CharSequence;)V

    .line 141
    :cond_3
    :goto_1
    sget v0, Lcom/squareup/ui/buyerflow/R$string;->buyer_send_receipt_all_done:I

    invoke-direct {p0, v0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->updateSubtitle(ILcom/squareup/locale/LocaleOverrideFactory;)V

    .line 142
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->maybeGoToLoyaltyOrEmailCollectionScreen()V

    return-void
.end method

.method showReceiptSection()V
    .locals 1

    .line 146
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->setInputControlPadding()V

    return-void
.end method

.method showTenderAmountAndTip(Lcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 7

    .line 265
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    .line 266
    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->getTip()Lcom/squareup/protos/common/Money;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 267
    iget-object v2, v1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v6, v2, v4

    if-lez v6, :cond_0

    .line 269
    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getMoneyFormatter()Lcom/squareup/text/Formatter;

    move-result-object v2

    .line 270
    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object p1

    .line 271
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->getView()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;

    sget v4, Lcom/squareup/checkout/R$string;->buyer_payment_note_tip:I

    invoke-interface {p1, v4}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 272
    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->getTenderAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-interface {v2, v0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    const-string v4, "amount"

    invoke-virtual {p1, v4, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 273
    invoke-interface {v2, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    const-string/jumbo v1, "tip"

    invoke-virtual {p1, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 274
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 271
    invoke-virtual {v3, p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->setAmountSubtitle(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method updateMessages(Lcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 12

    .line 154
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;

    .line 155
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/payment/PaymentReceipt;->asReturnsChange()Lcom/squareup/payment/tender/BaseTender$ReturnsChange;

    move-result-object v1

    .line 156
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/payment/PaymentReceipt;->getRemainingAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v2

    .line 157
    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getMoneyFormatter()Lcom/squareup/text/Formatter;

    move-result-object v3

    .line 158
    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object v4

    if-eqz v1, :cond_2

    .line 161
    invoke-interface {v1}, Lcom/squareup/payment/tender/BaseTender$ReturnsChange;->getChange()Lcom/squareup/protos/common/Money;

    move-result-object v5

    iget-object v5, v5, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    const-string v7, "amount"

    const-wide/16 v8, 0x0

    cmp-long v10, v5, v8

    if-eqz v10, :cond_0

    .line 162
    sget v5, Lcom/squareup/checkout/R$string;->buyer_send_receipt_title_cash_change_only:I

    .line 163
    invoke-interface {v4, v5}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v5

    .line 164
    invoke-interface {v1}, Lcom/squareup/payment/tender/BaseTender$ReturnsChange;->getChange()Lcom/squareup/protos/common/Money;

    move-result-object v6

    invoke-interface {v3, v6}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v7, v6}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v5

    .line 165
    invoke-virtual {v5}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v5

    .line 162
    invoke-virtual {v0, v5}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 167
    :cond_0
    sget v5, Lcom/squareup/checkout/R$string;->buyer_send_receipt_title_no_change:I

    .line 168
    invoke-interface {v4, v5}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 167
    invoke-virtual {v0, v5}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->setTitle(Ljava/lang/CharSequence;)V

    :goto_0
    const-string/jumbo v5, "total"

    if-eqz v2, :cond_1

    .line 170
    iget-object v6, v2, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    cmp-long v6, v10, v8

    if-lez v6, :cond_1

    .line 171
    sget v6, Lcom/squareup/checkout/R$string;->buyer_send_receipt_title_no_change_sub_with_remaining_balance:I

    .line 172
    invoke-interface {v4, v6}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v6

    .line 175
    invoke-interface {v3, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v6, v7, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    .line 176
    invoke-interface {v1}, Lcom/squareup/payment/tender/BaseTender$ReturnsChange;->getTendered()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-interface {v3, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v2, v5, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 177
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 171
    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->setAmountSubtitle(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 178
    :cond_1
    invoke-interface {v1}, Lcom/squareup/payment/tender/BaseTender$ReturnsChange;->getTendered()Lcom/squareup/protos/common/Money;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v2, v6, v8

    if-eqz v2, :cond_3

    .line 179
    sget v2, Lcom/squareup/checkout/R$string;->buyer_send_receipt_title_no_change_sub:I

    .line 180
    invoke-interface {v4, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    .line 181
    invoke-interface {v1}, Lcom/squareup/payment/tender/BaseTender$ReturnsChange;->getTendered()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-interface {v3, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v2, v5, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 182
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 179
    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->setAmountSubtitle(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 185
    :cond_2
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/payment/Payment;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-interface {v3, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->setTitle(Ljava/lang/CharSequence;)V

    .line 188
    :cond_3
    :goto_1
    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->countryCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    sget-object v2, Lcom/squareup/CountryCode;->US:Lcom/squareup/CountryCode;

    if-ne v1, v2, :cond_4

    const/4 v1, 0x1

    goto :goto_2

    :cond_4
    const/4 v1, 0x0

    :goto_2
    if-eqz v1, :cond_5

    .line 189
    sget v1, Lcom/squareup/ui/buyerflow/R$string;->buyer_send_receipt_digital_subtitle:I

    goto :goto_3

    :cond_5
    sget v1, Lcom/squareup/checkout/R$string;->buyer_send_receipt_subtitle:I

    :goto_3
    invoke-direct {p0, v1, p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->updateSubtitle(ILcom/squareup/locale/LocaleOverrideFactory;)V

    .line 194
    invoke-virtual {v0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->isPrintReceiptButtonVisible()Z

    move-result p1

    if-eqz p1, :cond_6

    .line 195
    sget p1, Lcom/squareup/activity/R$string;->receipt_paper:I

    invoke-interface {v4, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->setPrintText(Ljava/lang/String;)V

    :cond_6
    return-void
.end method

.method updateStrings(Lcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 0

    .line 150
    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->updateMessages(Lcom/squareup/locale/LocaleOverrideFactory;)V

    return-void
.end method

.method public visibilityChanged(Z)V
    .locals 0

    if-nez p1, :cond_0

    .line 300
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->cancelAutoFinish()V

    :cond_0
    return-void
.end method
