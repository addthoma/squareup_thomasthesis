.class Lcom/squareup/ui/buyer/crm/EmailCollectionScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "EmailCollectionScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/crm/EmailCollectionScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/buyer/crm/EmailCollectionView;",
        ">;"
    }
.end annotation


# static fields
.field static final SCREEN_TIMEOUT_SECONDS:J = 0x1eL


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final autoClose:Lcom/squareup/util/RxWatchdog;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/util/RxWatchdog<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

.field private final checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

.field private final res:Lcom/squareup/util/Res;

.field private final retrofitQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

.field private screen:Lcom/squareup/ui/buyer/crm/EmailCollectionScreen;

.field private final topScreenChecker:Lcom/squareup/ui/main/TopScreenChecker;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method constructor <init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/util/Res;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/payment/Transaction;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/ui/main/TopScreenChecker;)V
    .locals 0
    .param p7    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p8    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 98
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 99
    iput-object p1, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 100
    iput-object p3, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionScreen$Presenter;->checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

    .line 101
    iput-object p2, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionScreen$Presenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    .line 102
    iput-object p4, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 103
    iput-object p5, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionScreen$Presenter;->retrofitQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    .line 104
    iput-object p6, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 105
    new-instance p1, Lcom/squareup/util/RxWatchdog;

    invoke-direct {p1, p7, p8}, Lcom/squareup/util/RxWatchdog;-><init>(Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    iput-object p1, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionScreen$Presenter;->autoClose:Lcom/squareup/util/RxWatchdog;

    .line 106
    iput-object p9, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionScreen$Presenter;->topScreenChecker:Lcom/squareup/ui/main/TopScreenChecker;

    return-void
.end method

.method private finish()V
    .locals 4

    .line 174
    iget-object v0, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireReceiptForLastPayment()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    .line 175
    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->isComplete()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 176
    iget-object v1, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionScreen$Presenter;->checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->getTenderTypeForLogging()Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    move-result-object v2

    .line 177
    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->getBillId()Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    const/4 v3, 0x0

    .line 176
    invoke-interface {v1, v2, v0, v3}, Lcom/squareup/log/CheckoutInformationEventLogger;->finishCheckout(Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;Ljava/lang/String;Z)V

    .line 179
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionScreen$Presenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->startSeparatedPrintoutsOrCompleteBuyerFlow()V

    return-void
.end method

.method static synthetic lambda$null$2(Lkotlin/Unit;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    return-object p1
.end method

.method static synthetic lambda$null$3(Ljava/lang/Boolean;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 129
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$null$6(Lkotlin/Unit;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    return-object p1
.end method

.method static synthetic lambda$onLoad$11(Lcom/squareup/ui/buyer/crm/EmailCollectionView;)Lrx/Subscription;
    .locals 2

    .line 164
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/crm/EmailCollectionView;->emailText()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/buyer/crm/-$$Lambda$fJkbvbzrcAf0-2PSI7GNXvjnOjc;->INSTANCE:Lcom/squareup/ui/buyer/crm/-$$Lambda$fJkbvbzrcAf0-2PSI7GNXvjnOjc;

    .line 165
    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/buyer/crm/-$$Lambda$b8rVjkBtXYbT1h_6hxujTy8oTdg;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/crm/-$$Lambda$b8rVjkBtXYbT1h_6hxujTy8oTdg;-><init>(Lcom/squareup/ui/buyer/crm/EmailCollectionView;)V

    .line 166
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p0

    return-object p0
.end method

.method private onNewSaleClicked()V
    .locals 3

    .line 183
    iget-object v0, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/crm/events/CrmEvent;

    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->CRM_EMAIL_COLLECTION_SCREEN_NEW_SALE:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {v1, v2}, Lcom/squareup/crm/events/CrmEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    .line 184
    invoke-direct {p0}, Lcom/squareup/ui/buyer/crm/EmailCollectionScreen$Presenter;->finish()V

    return-void
.end method


# virtual methods
.method public synthetic lambda$null$0$EmailCollectionScreen$Presenter(Lkotlin/Unit;)V
    .locals 0

    .line 122
    invoke-direct {p0}, Lcom/squareup/ui/buyer/crm/EmailCollectionScreen$Presenter;->onNewSaleClicked()V

    return-void
.end method

.method public synthetic lambda$null$4$EmailCollectionScreen$Presenter(Lcom/squareup/ui/buyer/crm/EmailCollectionView;Ljava/lang/Boolean;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 131
    invoke-direct {p0}, Lcom/squareup/ui/buyer/crm/EmailCollectionScreen$Presenter;->onNewSaleClicked()V

    .line 132
    iget-object p2, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v0, Lcom/squareup/crm/events/CrmScreenTimeoutEvent;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/crm/events/CrmScreenTimeoutEvent;-><init>(Ljava/lang/String;)V

    invoke-interface {p2, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public synthetic lambda$null$7$EmailCollectionScreen$Presenter(Lcom/squareup/ui/buyer/crm/EmailCollectionView;Ljava/lang/String;)V
    .locals 3

    .line 141
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 142
    iget-object v0, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/crm/events/CrmEvent;

    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->CRM_EMAIL_COLLECTION_SCREEN_SUBMIT:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {v1, v2}, Lcom/squareup/crm/events/CrmEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    .line 143
    iget-object v0, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionScreen$Presenter;->retrofitQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    new-instance v1, Lcom/squareup/queue/crm/EmailCollectionTask$Builder;

    invoke-direct {v1}, Lcom/squareup/queue/crm/EmailCollectionTask$Builder;-><init>()V

    iget-object v2, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 144
    invoke-virtual {v2}, Lcom/squareup/payment/Transaction;->requireReceiptForLastPayment()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v2

    .line 145
    invoke-virtual {v2}, Lcom/squareup/payment/PaymentReceipt;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    .line 144
    invoke-virtual {v1, v2}, Lcom/squareup/queue/crm/EmailCollectionTask$Builder;->paymentToken(Ljava/lang/String;)Lcom/squareup/queue/crm/EmailCollectionTask$Builder;

    move-result-object v1

    .line 146
    invoke-virtual {v1, p2}, Lcom/squareup/queue/crm/EmailCollectionTask$Builder;->emailAddress(Ljava/lang/String;)Lcom/squareup/queue/crm/EmailCollectionTask$Builder;

    move-result-object p2

    iget-object v1, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 147
    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->requireReceiptForLastPayment()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v1

    .line 148
    invoke-virtual {v1}, Lcom/squareup/payment/Payment;->getCustomerContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v1

    .line 147
    invoke-virtual {p2, v1}, Lcom/squareup/queue/crm/EmailCollectionTask$Builder;->contact(Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/queue/crm/EmailCollectionTask$Builder;

    move-result-object p2

    .line 149
    invoke-virtual {p2}, Lcom/squareup/queue/crm/EmailCollectionTask$Builder;->build()Lcom/squareup/queue/crm/EmailCollectionTask;

    move-result-object p2

    .line 143
    invoke-interface {v0, p2}, Lcom/squareup/queue/retrofit/RetrofitQueue;->add(Ljava/lang/Object;)V

    .line 150
    iget-object p2, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/ui/buyerflow/R$string;->crm_email_collection_on_the_list:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/buyer/crm/EmailCollectionView;->showAllDone(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public synthetic lambda$null$9$EmailCollectionScreen$Presenter(Lcom/squareup/ui/buyer/crm/EmailCollectionView;Lkotlin/Unit;)V
    .locals 2

    .line 157
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 158
    iget-object p2, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v0, Lcom/squareup/crm/events/CrmEvent;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_EMAIL_COLLECTION_SCREEN_NO_THANKS:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {v0, v1}, Lcom/squareup/crm/events/CrmEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;)V

    invoke-interface {p2, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    .line 159
    iget-object p2, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/ui/buyerflow/R$string;->crm_email_collection_done:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/buyer/crm/EmailCollectionView;->showAllDone(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public synthetic lambda$onLoad$1$EmailCollectionScreen$Presenter(Lcom/squareup/ui/buyer/crm/EmailCollectionView;)Lrx/Subscription;
    .locals 1

    .line 121
    invoke-virtual {p1}, Lcom/squareup/ui/buyer/crm/EmailCollectionView;->onNewSaleButtonClicked()Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/buyer/crm/-$$Lambda$EmailCollectionScreen$Presenter$Ka5LLGCUAwnI7ghPaFxF602EAb8;

    invoke-direct {v0, p0}, Lcom/squareup/ui/buyer/crm/-$$Lambda$EmailCollectionScreen$Presenter$Ka5LLGCUAwnI7ghPaFxF602EAb8;-><init>(Lcom/squareup/ui/buyer/crm/EmailCollectionScreen$Presenter;)V

    .line 122
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$10$EmailCollectionScreen$Presenter(Lcom/squareup/ui/buyer/crm/EmailCollectionView;)Lrx/Subscription;
    .locals 2

    .line 155
    invoke-virtual {p1}, Lcom/squareup/ui/buyer/crm/EmailCollectionView;->onNoThanksButtonClicked()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/buyer/crm/-$$Lambda$EmailCollectionScreen$Presenter$fynha2zqwcTBpGpy_cofmJgmGiA;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/buyer/crm/-$$Lambda$EmailCollectionScreen$Presenter$fynha2zqwcTBpGpy_cofmJgmGiA;-><init>(Lcom/squareup/ui/buyer/crm/EmailCollectionScreen$Presenter;Lcom/squareup/ui/buyer/crm/EmailCollectionView;)V

    .line 156
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$5$EmailCollectionScreen$Presenter(Lcom/squareup/ui/buyer/crm/EmailCollectionView;)Lio/reactivex/disposables/Disposable;
    .locals 3

    .line 126
    iget-object v0, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionScreen$Presenter;->autoClose:Lcom/squareup/util/RxWatchdog;

    invoke-virtual {v0}, Lcom/squareup/util/RxWatchdog;->timeout()Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionScreen$Presenter;->topScreenChecker:Lcom/squareup/ui/main/TopScreenChecker;

    iget-object v2, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionScreen$Presenter;->screen:Lcom/squareup/ui/buyer/crm/EmailCollectionScreen;

    .line 127
    invoke-virtual {v1, v2}, Lcom/squareup/ui/main/TopScreenChecker;->unobscured(Lcom/squareup/container/ContainerTreeKey;)Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/buyer/crm/-$$Lambda$EmailCollectionScreen$Presenter$zbtR9CZME1MyCMVxPhfCTUus-Ts;->INSTANCE:Lcom/squareup/ui/buyer/crm/-$$Lambda$EmailCollectionScreen$Presenter$zbtR9CZME1MyCMVxPhfCTUus-Ts;

    .line 126
    invoke-static {v0, v1, v2}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/buyer/crm/-$$Lambda$EmailCollectionScreen$Presenter$aAiOpKT4Fo0vZz_MqWKBm6-45MY;->INSTANCE:Lcom/squareup/ui/buyer/crm/-$$Lambda$EmailCollectionScreen$Presenter$aAiOpKT4Fo0vZz_MqWKBm6-45MY;

    .line 129
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/buyer/crm/-$$Lambda$EmailCollectionScreen$Presenter$KUq0cvAZgDCmTHXBHuhoApjyrJk;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/buyer/crm/-$$Lambda$EmailCollectionScreen$Presenter$KUq0cvAZgDCmTHXBHuhoApjyrJk;-><init>(Lcom/squareup/ui/buyer/crm/EmailCollectionScreen$Presenter;Lcom/squareup/ui/buyer/crm/EmailCollectionView;)V

    .line 130
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$8$EmailCollectionScreen$Presenter(Lcom/squareup/ui/buyer/crm/EmailCollectionView;)Lrx/Subscription;
    .locals 3

    .line 137
    invoke-virtual {p1}, Lcom/squareup/ui/buyer/crm/EmailCollectionView;->onSubmitButtonClicked()Lrx/Observable;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/crm/EmailCollectionView;->onKeyboardSendPressed()Lrx/Observable;

    move-result-object v1

    invoke-static {v0, v1}, Lrx/Observable;->merge(Lrx/Observable;Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    .line 138
    invoke-virtual {p1}, Lcom/squareup/ui/buyer/crm/EmailCollectionView;->emailText()Lrx/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/buyer/crm/-$$Lambda$EmailCollectionScreen$Presenter$Spf5467CuL7QV-9Hr3QGdm688II;->INSTANCE:Lcom/squareup/ui/buyer/crm/-$$Lambda$EmailCollectionScreen$Presenter$Spf5467CuL7QV-9Hr3QGdm688II;

    invoke-virtual {v0, v1, v2}, Lrx/Observable;->withLatestFrom(Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/buyer/crm/-$$Lambda$fJkbvbzrcAf0-2PSI7GNXvjnOjc;->INSTANCE:Lcom/squareup/ui/buyer/crm/-$$Lambda$fJkbvbzrcAf0-2PSI7GNXvjnOjc;

    .line 139
    invoke-virtual {v0, v1}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/buyer/crm/-$$Lambda$EmailCollectionScreen$Presenter$BBemcUdH5_mVjiSDfD11SEbGWas;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/buyer/crm/-$$Lambda$EmailCollectionScreen$Presenter$BBemcUdH5_mVjiSDfD11SEbGWas;-><init>(Lcom/squareup/ui/buyer/crm/EmailCollectionScreen$Presenter;Lcom/squareup/ui/buyer/crm/EmailCollectionView;)V

    .line 140
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method onBackPressed()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    .line 110
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 111
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/buyer/crm/EmailCollectionScreen;

    iput-object p1, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionScreen$Presenter;->screen:Lcom/squareup/ui/buyer/crm/EmailCollectionScreen;

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 6

    .line 115
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 117
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/crm/EmailCollectionScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/crm/EmailCollectionView;

    .line 120
    new-instance v1, Lcom/squareup/ui/buyer/crm/-$$Lambda$EmailCollectionScreen$Presenter$WjLuMtxyd1SSLC3_GDQqg48QG5A;

    invoke-direct {v1, p0, v0}, Lcom/squareup/ui/buyer/crm/-$$Lambda$EmailCollectionScreen$Presenter$WjLuMtxyd1SSLC3_GDQqg48QG5A;-><init>(Lcom/squareup/ui/buyer/crm/EmailCollectionScreen$Presenter;Lcom/squareup/ui/buyer/crm/EmailCollectionView;)V

    invoke-static {v0, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 124
    iget-object v1, p0, Lcom/squareup/ui/buyer/crm/EmailCollectionScreen$Presenter;->autoClose:Lcom/squareup/util/RxWatchdog;

    sget-object v2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x1e

    invoke-virtual {v1, v2, v4, v5, v3}, Lcom/squareup/util/RxWatchdog;->restart(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)V

    .line 125
    new-instance v1, Lcom/squareup/ui/buyer/crm/-$$Lambda$EmailCollectionScreen$Presenter$WWAYaY2XFIIMeFZ19Wwq7V6nlX0;

    invoke-direct {v1, p0, v0}, Lcom/squareup/ui/buyer/crm/-$$Lambda$EmailCollectionScreen$Presenter$WWAYaY2XFIIMeFZ19Wwq7V6nlX0;-><init>(Lcom/squareup/ui/buyer/crm/EmailCollectionScreen$Presenter;Lcom/squareup/ui/buyer/crm/EmailCollectionView;)V

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 136
    new-instance v1, Lcom/squareup/ui/buyer/crm/-$$Lambda$EmailCollectionScreen$Presenter$wsM3GHL7jnwfjLmY4P42TkCUcjU;

    invoke-direct {v1, p0, v0}, Lcom/squareup/ui/buyer/crm/-$$Lambda$EmailCollectionScreen$Presenter$wsM3GHL7jnwfjLmY4P42TkCUcjU;-><init>(Lcom/squareup/ui/buyer/crm/EmailCollectionScreen$Presenter;Lcom/squareup/ui/buyer/crm/EmailCollectionView;)V

    invoke-static {v0, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 154
    new-instance v1, Lcom/squareup/ui/buyer/crm/-$$Lambda$EmailCollectionScreen$Presenter$nIaLpW5F3mC9ASmRFnGptuXJkB8;

    invoke-direct {v1, p0, v0}, Lcom/squareup/ui/buyer/crm/-$$Lambda$EmailCollectionScreen$Presenter$nIaLpW5F3mC9ASmRFnGptuXJkB8;-><init>(Lcom/squareup/ui/buyer/crm/EmailCollectionScreen$Presenter;Lcom/squareup/ui/buyer/crm/EmailCollectionView;)V

    invoke-static {v0, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 163
    new-instance v1, Lcom/squareup/ui/buyer/crm/-$$Lambda$EmailCollectionScreen$Presenter$FphxUvIoC9FK_tQMb2nYq80n33o;

    invoke-direct {v1, v0}, Lcom/squareup/ui/buyer/crm/-$$Lambda$EmailCollectionScreen$Presenter$FphxUvIoC9FK_tQMb2nYq80n33o;-><init>(Lcom/squareup/ui/buyer/crm/EmailCollectionView;)V

    invoke-static {v0, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    if-nez p1, :cond_0

    .line 169
    invoke-virtual {v0}, Lcom/squareup/ui/buyer/crm/EmailCollectionView;->requestInitialFocus()V

    :cond_0
    return-void
.end method
