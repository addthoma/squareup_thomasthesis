.class public final Lcom/squareup/ui/buyer/tip/TipReaderHandler$register$1;
.super Ljava/lang/Object;
.source "TipReaderHandler.kt"

# interfaces
.implements Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/buyer/tip/TipReaderHandler;->register()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0019\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016J\u0010\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0007"
    }
    d2 = {
        "com/squareup/ui/buyer/tip/TipReaderHandler$register$1",
        "Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;",
        "processEmvCardInserted",
        "",
        "cardReaderInfo",
        "Lcom/squareup/cardreader/CardReaderInfo;",
        "processEmvCardRemoved",
        "buyer-flow_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/buyer/tip/TipReaderHandler;


# direct methods
.method constructor <init>(Lcom/squareup/ui/buyer/tip/TipReaderHandler;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 48
    iput-object p1, p0, Lcom/squareup/ui/buyer/tip/TipReaderHandler$register$1;->this$0:Lcom/squareup/ui/buyer/tip/TipReaderHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public processEmvCardInserted(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 1

    const-string v0, "cardReaderInfo"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public processEmvCardRemoved(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 1

    const-string v0, "cardReaderInfo"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/buyer/tip/TipReaderHandler$register$1;->this$0:Lcom/squareup/ui/buyer/tip/TipReaderHandler;

    invoke-static {v0}, Lcom/squareup/ui/buyer/tip/TipReaderHandler;->access$getDefaultEmvCardInsertRemoveProcessor$p(Lcom/squareup/ui/buyer/tip/TipReaderHandler;)Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;->processEmvCardRemoved(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void
.end method
