.class public final Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper_Factory;
.super Ljava/lang/Object;
.source "ReceiptEmailAndLoyaltyHelper_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;",
        ">;"
    }
.end annotation


# instance fields
.field private final emailCollectionSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/EmailCollectionSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final loyaltyEventPublisherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltyEventPublisher;",
            ">;"
        }
    .end annotation
.end field

.field private final loyaltySettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadEnforcerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;"
        }
    .end annotation
.end field

.field private final offlineModeMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final rolodexServiceHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final skipReceiptScreenSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final taskEnqueuerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/loyalty/MissedLoyaltyEnqueuer;",
            ">;"
        }
    .end annotation
.end field

.field private final topScreenCheckerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/TopScreenChecker;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/TopScreenChecker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/EmailCollectionSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/loyalty/MissedLoyaltyEnqueuer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltyEventPublisher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ">;)V"
        }
    .end annotation

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper_Factory;->transactionProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p2, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper_Factory;->topScreenCheckerProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p3, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper_Factory;->skipReceiptScreenSettingsProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p4, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper_Factory;->emailCollectionSettingsProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p5, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper_Factory;->rolodexServiceHelperProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p6, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper_Factory;->offlineModeMonitorProvider:Ljavax/inject/Provider;

    .line 66
    iput-object p7, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 67
    iput-object p8, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper_Factory;->mainThreadEnforcerProvider:Ljavax/inject/Provider;

    .line 68
    iput-object p9, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper_Factory;->taskEnqueuerProvider:Ljavax/inject/Provider;

    .line 69
    iput-object p10, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper_Factory;->loyaltyEventPublisherProvider:Ljavax/inject/Provider;

    .line 70
    iput-object p11, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper_Factory;->loyaltySettingsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper_Factory;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/TopScreenChecker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/EmailCollectionSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/loyalty/MissedLoyaltyEnqueuer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltyEventPublisher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ">;)",
            "Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper_Factory;"
        }
    .end annotation

    .line 90
    new-instance v12, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper_Factory;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v12
.end method

.method public static newInstance(Lcom/squareup/payment/Transaction;Lcom/squareup/ui/main/TopScreenChecker;Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;Lcom/squareup/crm/EmailCollectionSettings;Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/payment/OfflineModeMonitor;Lrx/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/queue/loyalty/MissedLoyaltyEnqueuer;Lcom/squareup/loyalty/LoyaltyEventPublisher;Lcom/squareup/loyalty/LoyaltySettings;)Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;
    .locals 13

    .line 99
    new-instance v12, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;-><init>(Lcom/squareup/payment/Transaction;Lcom/squareup/ui/main/TopScreenChecker;Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;Lcom/squareup/crm/EmailCollectionSettings;Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/payment/OfflineModeMonitor;Lrx/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/queue/loyalty/MissedLoyaltyEnqueuer;Lcom/squareup/loyalty/LoyaltyEventPublisher;Lcom/squareup/loyalty/LoyaltySettings;)V

    return-object v12
.end method


# virtual methods
.method public get()Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;
    .locals 12

    .line 75
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/payment/Transaction;

    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper_Factory;->topScreenCheckerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/main/TopScreenChecker;

    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper_Factory;->skipReceiptScreenSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;

    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper_Factory;->emailCollectionSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/crm/EmailCollectionSettings;

    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper_Factory;->rolodexServiceHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/crm/RolodexServiceHelper;

    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper_Factory;->offlineModeMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/payment/OfflineModeMonitor;

    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lrx/Scheduler;

    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper_Factory;->mainThreadEnforcerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/thread/enforcer/ThreadEnforcer;

    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper_Factory;->taskEnqueuerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/queue/loyalty/MissedLoyaltyEnqueuer;

    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper_Factory;->loyaltyEventPublisherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/loyalty/LoyaltyEventPublisher;

    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper_Factory;->loyaltySettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/loyalty/LoyaltySettings;

    invoke-static/range {v1 .. v11}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper_Factory;->newInstance(Lcom/squareup/payment/Transaction;Lcom/squareup/ui/main/TopScreenChecker;Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;Lcom/squareup/crm/EmailCollectionSettings;Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/payment/OfflineModeMonitor;Lrx/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/queue/loyalty/MissedLoyaltyEnqueuer;Lcom/squareup/loyalty/LoyaltyEventPublisher;Lcom/squareup/loyalty/LoyaltySettings;)Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 18
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper_Factory;->get()Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;

    move-result-object v0

    return-object v0
.end method
