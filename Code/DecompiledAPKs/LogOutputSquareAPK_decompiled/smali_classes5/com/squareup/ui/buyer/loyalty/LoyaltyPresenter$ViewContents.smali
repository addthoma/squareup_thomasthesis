.class final enum Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;
.super Ljava/lang/Enum;
.source "LoyaltyPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "ViewContents"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;

.field public static final enum ALL_DONE_CONTENTS:Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;

.field public static final enum ENROLLMENT_CONTENTS:Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;

.field public static final enum NON_QUALIFYING_VIEW_TIERS:Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;

.field public static final enum REWARD_STATUS_CONTENTS:Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 127
    new-instance v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;

    const/4 v1, 0x0

    const-string v2, "ALL_DONE_CONTENTS"

    invoke-direct {v0, v2, v1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;->ALL_DONE_CONTENTS:Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;

    .line 128
    new-instance v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;

    const/4 v2, 0x1

    const-string v3, "NON_QUALIFYING_VIEW_TIERS"

    invoke-direct {v0, v3, v2}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;->NON_QUALIFYING_VIEW_TIERS:Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;

    .line 129
    new-instance v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;

    const/4 v3, 0x2

    const-string v4, "ENROLLMENT_CONTENTS"

    invoke-direct {v0, v4, v3}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;->ENROLLMENT_CONTENTS:Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;

    .line 130
    new-instance v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;

    const/4 v4, 0x3

    const-string v5, "REWARD_STATUS_CONTENTS"

    invoke-direct {v0, v5, v4}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;->REWARD_STATUS_CONTENTS:Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;

    .line 126
    sget-object v5, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;->ALL_DONE_CONTENTS:Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;->NON_QUALIFYING_VIEW_TIERS:Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;->ENROLLMENT_CONTENTS:Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;->REWARD_STATUS_CONTENTS:Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;->$VALUES:[Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 126
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;
    .locals 1

    .line 126
    const-class v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;
    .locals 1

    .line 126
    sget-object v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;->$VALUES:[Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;

    invoke-virtual {v0}, [Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$ViewContents;

    return-object v0
.end method
