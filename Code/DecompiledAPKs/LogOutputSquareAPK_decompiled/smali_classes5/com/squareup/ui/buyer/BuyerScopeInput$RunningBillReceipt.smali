.class public final Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;
.super Lcom/squareup/ui/buyer/BuyerScopeInput;
.source "BuyerScopeInput.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/BuyerScopeInput;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RunningBillReceipt"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0002\u0008%\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001Bo\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0012\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00020\u00030\u0007\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0008\u0010\r\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\u000e\u001a\u0004\u0018\u00010\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u000c\u0012\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u000c\u0012\u0006\u0010\u0014\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0015J\t\u0010)\u001a\u00020\u0003H\u00c6\u0003J\t\u0010*\u001a\u00020\u000cH\u00c6\u0003J\t\u0010+\u001a\u00020\u0003H\u00c6\u0003J\t\u0010,\u001a\u00020\u0005H\u00c6\u0003J\u0015\u0010-\u001a\u000e\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00020\u00030\u0007H\u00c6\u0003J\t\u0010.\u001a\u00020\nH\u00c6\u0003J\t\u0010/\u001a\u00020\u000cH\u00c6\u0003J\u000b\u00100\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u00101\u001a\u0004\u0018\u00010\u000fH\u00c6\u0003J\t\u00102\u001a\u00020\u000cH\u00c6\u0003J\u0010\u00103\u001a\u0004\u0018\u00010\u0012H\u00c6\u0003\u00a2\u0006\u0002\u0010\u0017J\u008e\u0001\u00104\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0014\u0008\u0002\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00020\u00030\u00072\u0008\u0008\u0002\u0010\t\u001a\u00020\n2\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u000c2\n\u0008\u0002\u0010\r\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u000f2\u0008\u0008\u0002\u0010\u0010\u001a\u00020\u000c2\n\u0008\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u00122\u0008\u0008\u0002\u0010\u0013\u001a\u00020\u000c2\u0008\u0008\u0002\u0010\u0014\u001a\u00020\u0003H\u00c6\u0001\u00a2\u0006\u0002\u00105J\u0013\u00106\u001a\u00020\u000c2\u0008\u00107\u001a\u0004\u0018\u000108H\u00d6\u0003J\t\u00109\u001a\u00020:H\u00d6\u0001J\t\u0010;\u001a\u00020\u0003H\u00d6\u0001R\u0015\u0010\u0011\u001a\u0004\u0018\u00010\u0012\u00a2\u0006\n\n\u0002\u0010\u0018\u001a\u0004\u0008\u0016\u0010\u0017R\u0011\u0010\u0014\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u001aR\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u001cR\u0013\u0010\r\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001d\u0010\u001aR\u001d\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00020\u00030\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001e\u0010\u001fR\u0013\u0010\u000e\u001a\u0004\u0018\u00010\u000f\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008 \u0010!R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\"\u0010#R\u0011\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008$\u0010%R\u0011\u0010\u0013\u001a\u00020\u000c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008&\u0010%R\u0011\u0010\u0010\u001a\u00020\u000c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\'\u0010%R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008(\u0010\u001a\u00a8\u0006<"
    }
    d2 = {
        "Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;",
        "Lcom/squareup/ui/buyer/BuyerScopeInput;",
        "uniqueKey",
        "",
        "receipt",
        "Lcom/squareup/payment/PaymentReceipt;",
        "getRemainingBalanceText",
        "Lkotlin/Function1;",
        "Lcom/squareup/util/Res;",
        "countryCode",
        "Lcom/squareup/CountryCode;",
        "showLanguageSelection",
        "",
        "displayName",
        "mostRecentActiveCardReaderId",
        "Lcom/squareup/cardreader/CardReaderId;",
        "supportsSms",
        "autoReceiptCompleteTimeout",
        "",
        "showSmsMarketing",
        "businessName",
        "(Ljava/lang/String;Lcom/squareup/payment/PaymentReceipt;Lkotlin/jvm/functions/Function1;Lcom/squareup/CountryCode;ZLjava/lang/String;Lcom/squareup/cardreader/CardReaderId;ZLjava/lang/Long;ZLjava/lang/String;)V",
        "getAutoReceiptCompleteTimeout",
        "()Ljava/lang/Long;",
        "Ljava/lang/Long;",
        "getBusinessName",
        "()Ljava/lang/String;",
        "getCountryCode",
        "()Lcom/squareup/CountryCode;",
        "getDisplayName",
        "getGetRemainingBalanceText",
        "()Lkotlin/jvm/functions/Function1;",
        "getMostRecentActiveCardReaderId",
        "()Lcom/squareup/cardreader/CardReaderId;",
        "getReceipt",
        "()Lcom/squareup/payment/PaymentReceipt;",
        "getShowLanguageSelection",
        "()Z",
        "getShowSmsMarketing",
        "getSupportsSms",
        "getUniqueKey",
        "component1",
        "component10",
        "component11",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "component9",
        "copy",
        "(Ljava/lang/String;Lcom/squareup/payment/PaymentReceipt;Lkotlin/jvm/functions/Function1;Lcom/squareup/CountryCode;ZLjava/lang/String;Lcom/squareup/cardreader/CardReaderId;ZLjava/lang/Long;ZLjava/lang/String;)Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "buyer-flow_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final autoReceiptCompleteTimeout:Ljava/lang/Long;

.field private final businessName:Ljava/lang/String;

.field private final countryCode:Lcom/squareup/CountryCode;

.field private final displayName:Ljava/lang/String;

.field private final getRemainingBalanceText:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/util/Res;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mostRecentActiveCardReaderId:Lcom/squareup/cardreader/CardReaderId;

.field private final receipt:Lcom/squareup/payment/PaymentReceipt;

.field private final showLanguageSelection:Z

.field private final showSmsMarketing:Z

.field private final supportsSms:Z

.field private final uniqueKey:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/squareup/payment/PaymentReceipt;Lkotlin/jvm/functions/Function1;Lcom/squareup/CountryCode;ZLjava/lang/String;Lcom/squareup/cardreader/CardReaderId;ZLjava/lang/Long;ZLjava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/payment/PaymentReceipt;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/util/Res;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/CountryCode;",
            "Z",
            "Ljava/lang/String;",
            "Lcom/squareup/cardreader/CardReaderId;",
            "Z",
            "Ljava/lang/Long;",
            "Z",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const-string/jumbo v0, "uniqueKey"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "receipt"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "getRemainingBalanceText"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "countryCode"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "businessName"

    invoke-static {p11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 34
    invoke-direct {p0, v0}, Lcom/squareup/ui/buyer/BuyerScopeInput;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->uniqueKey:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->receipt:Lcom/squareup/payment/PaymentReceipt;

    iput-object p3, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->getRemainingBalanceText:Lkotlin/jvm/functions/Function1;

    iput-object p4, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->countryCode:Lcom/squareup/CountryCode;

    iput-boolean p5, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->showLanguageSelection:Z

    iput-object p6, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->displayName:Ljava/lang/String;

    iput-object p7, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->mostRecentActiveCardReaderId:Lcom/squareup/cardreader/CardReaderId;

    iput-boolean p8, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->supportsSms:Z

    iput-object p9, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->autoReceiptCompleteTimeout:Ljava/lang/Long;

    iput-boolean p10, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->showSmsMarketing:Z

    iput-object p11, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->businessName:Ljava/lang/String;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;Ljava/lang/String;Lcom/squareup/payment/PaymentReceipt;Lkotlin/jvm/functions/Function1;Lcom/squareup/CountryCode;ZLjava/lang/String;Lcom/squareup/cardreader/CardReaderId;ZLjava/lang/Long;ZLjava/lang/String;ILjava/lang/Object;)Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;
    .locals 12

    move-object v0, p0

    move/from16 v1, p12

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->uniqueKey:Ljava/lang/String;

    goto :goto_0

    :cond_0
    move-object v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->receipt:Lcom/squareup/payment/PaymentReceipt;

    goto :goto_1

    :cond_1
    move-object v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->getRemainingBalanceText:Lkotlin/jvm/functions/Function1;

    goto :goto_2

    :cond_2
    move-object v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->countryCode:Lcom/squareup/CountryCode;

    goto :goto_3

    :cond_3
    move-object/from16 v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-boolean v6, v0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->showLanguageSelection:Z

    goto :goto_4

    :cond_4
    move/from16 v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-object v7, v0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->displayName:Ljava/lang/String;

    goto :goto_5

    :cond_5
    move-object/from16 v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-object v8, v0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->mostRecentActiveCardReaderId:Lcom/squareup/cardreader/CardReaderId;

    goto :goto_6

    :cond_6
    move-object/from16 v8, p7

    :goto_6
    and-int/lit16 v9, v1, 0x80

    if-eqz v9, :cond_7

    iget-boolean v9, v0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->supportsSms:Z

    goto :goto_7

    :cond_7
    move/from16 v9, p8

    :goto_7
    and-int/lit16 v10, v1, 0x100

    if-eqz v10, :cond_8

    iget-object v10, v0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->autoReceiptCompleteTimeout:Ljava/lang/Long;

    goto :goto_8

    :cond_8
    move-object/from16 v10, p9

    :goto_8
    and-int/lit16 v11, v1, 0x200

    if-eqz v11, :cond_9

    iget-boolean v11, v0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->showSmsMarketing:Z

    goto :goto_9

    :cond_9
    move/from16 v11, p10

    :goto_9
    and-int/lit16 v1, v1, 0x400

    if-eqz v1, :cond_a

    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->businessName:Ljava/lang/String;

    goto :goto_a

    :cond_a
    move-object/from16 v1, p11

    :goto_a
    move-object p1, v2

    move-object p2, v3

    move-object p3, v4

    move-object/from16 p4, v5

    move/from16 p5, v6

    move-object/from16 p6, v7

    move-object/from16 p7, v8

    move/from16 p8, v9

    move-object/from16 p9, v10

    move/from16 p10, v11

    move-object/from16 p11, v1

    invoke-virtual/range {p0 .. p11}, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->copy(Ljava/lang/String;Lcom/squareup/payment/PaymentReceipt;Lkotlin/jvm/functions/Function1;Lcom/squareup/CountryCode;ZLjava/lang/String;Lcom/squareup/cardreader/CardReaderId;ZLjava/lang/Long;ZLjava/lang/String;)Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->uniqueKey:Ljava/lang/String;

    return-object v0
.end method

.method public final component10()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->showSmsMarketing:Z

    return v0
.end method

.method public final component11()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->businessName:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Lcom/squareup/payment/PaymentReceipt;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->receipt:Lcom/squareup/payment/PaymentReceipt;

    return-object v0
.end method

.method public final component3()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/util/Res;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->getRemainingBalanceText:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final component4()Lcom/squareup/CountryCode;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->countryCode:Lcom/squareup/CountryCode;

    return-object v0
.end method

.method public final component5()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->showLanguageSelection:Z

    return v0
.end method

.method public final component6()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->displayName:Ljava/lang/String;

    return-object v0
.end method

.method public final component7()Lcom/squareup/cardreader/CardReaderId;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->mostRecentActiveCardReaderId:Lcom/squareup/cardreader/CardReaderId;

    return-object v0
.end method

.method public final component8()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->supportsSms:Z

    return v0
.end method

.method public final component9()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->autoReceiptCompleteTimeout:Ljava/lang/Long;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;Lcom/squareup/payment/PaymentReceipt;Lkotlin/jvm/functions/Function1;Lcom/squareup/CountryCode;ZLjava/lang/String;Lcom/squareup/cardreader/CardReaderId;ZLjava/lang/Long;ZLjava/lang/String;)Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/payment/PaymentReceipt;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/util/Res;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/CountryCode;",
            "Z",
            "Ljava/lang/String;",
            "Lcom/squareup/cardreader/CardReaderId;",
            "Z",
            "Ljava/lang/Long;",
            "Z",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;"
        }
    .end annotation

    const-string/jumbo v0, "uniqueKey"

    move-object v2, p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "receipt"

    move-object v3, p2

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "getRemainingBalanceText"

    move-object/from16 v4, p3

    invoke-static {v4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "countryCode"

    move-object/from16 v5, p4

    invoke-static {v5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "businessName"

    move-object/from16 v12, p11

    invoke-static {v12, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;

    move-object v1, v0

    move/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move/from16 v9, p8

    move-object/from16 v10, p9

    move/from16 v11, p10

    invoke-direct/range {v1 .. v12}, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;-><init>(Ljava/lang/String;Lcom/squareup/payment/PaymentReceipt;Lkotlin/jvm/functions/Function1;Lcom/squareup/CountryCode;ZLjava/lang/String;Lcom/squareup/cardreader/CardReaderId;ZLjava/lang/Long;ZLjava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;

    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->uniqueKey:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->uniqueKey:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->receipt:Lcom/squareup/payment/PaymentReceipt;

    iget-object v1, p1, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->receipt:Lcom/squareup/payment/PaymentReceipt;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->getRemainingBalanceText:Lkotlin/jvm/functions/Function1;

    iget-object v1, p1, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->getRemainingBalanceText:Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->countryCode:Lcom/squareup/CountryCode;

    iget-object v1, p1, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->countryCode:Lcom/squareup/CountryCode;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->showLanguageSelection:Z

    iget-boolean v1, p1, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->showLanguageSelection:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->displayName:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->displayName:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->mostRecentActiveCardReaderId:Lcom/squareup/cardreader/CardReaderId;

    iget-object v1, p1, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->mostRecentActiveCardReaderId:Lcom/squareup/cardreader/CardReaderId;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->supportsSms:Z

    iget-boolean v1, p1, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->supportsSms:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->autoReceiptCompleteTimeout:Ljava/lang/Long;

    iget-object v1, p1, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->autoReceiptCompleteTimeout:Ljava/lang/Long;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->showSmsMarketing:Z

    iget-boolean v1, p1, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->showSmsMarketing:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->businessName:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->businessName:Ljava/lang/String;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAutoReceiptCompleteTimeout()Ljava/lang/Long;
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->autoReceiptCompleteTimeout:Ljava/lang/Long;

    return-object v0
.end method

.method public final getBusinessName()Ljava/lang/String;
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->businessName:Ljava/lang/String;

    return-object v0
.end method

.method public final getCountryCode()Lcom/squareup/CountryCode;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->countryCode:Lcom/squareup/CountryCode;

    return-object v0
.end method

.method public final getDisplayName()Ljava/lang/String;
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->displayName:Ljava/lang/String;

    return-object v0
.end method

.method public final getGetRemainingBalanceText()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/util/Res;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 25
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->getRemainingBalanceText:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getMostRecentActiveCardReaderId()Lcom/squareup/cardreader/CardReaderId;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->mostRecentActiveCardReaderId:Lcom/squareup/cardreader/CardReaderId;

    return-object v0
.end method

.method public final getReceipt()Lcom/squareup/payment/PaymentReceipt;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->receipt:Lcom/squareup/payment/PaymentReceipt;

    return-object v0
.end method

.method public final getShowLanguageSelection()Z
    .locals 1

    .line 27
    iget-boolean v0, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->showLanguageSelection:Z

    return v0
.end method

.method public final getShowSmsMarketing()Z
    .locals 1

    .line 32
    iget-boolean v0, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->showSmsMarketing:Z

    return v0
.end method

.method public final getSupportsSms()Z
    .locals 1

    .line 30
    iget-boolean v0, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->supportsSms:Z

    return v0
.end method

.method public final getUniqueKey()Ljava/lang/String;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->uniqueKey:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->uniqueKey:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->receipt:Lcom/squareup/payment/PaymentReceipt;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->getRemainingBalanceText:Lkotlin/jvm/functions/Function1;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->countryCode:Lcom/squareup/CountryCode;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->showLanguageSelection:Z

    const/4 v3, 0x1

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :cond_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->displayName:Ljava/lang/String;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_5
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->mostRecentActiveCardReaderId:Lcom/squareup/cardreader/CardReaderId;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_6
    const/4 v2, 0x0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->supportsSms:Z

    if-eqz v2, :cond_7

    const/4 v2, 0x1

    :cond_7
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->autoReceiptCompleteTimeout:Ljava/lang/Long;

    if-eqz v2, :cond_8

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_6

    :cond_8
    const/4 v2, 0x0

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->showSmsMarketing:Z

    if-eqz v2, :cond_9

    const/4 v2, 0x1

    :cond_9
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->businessName:Ljava/lang/String;

    if-eqz v2, :cond_a

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_a
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RunningBillReceipt(uniqueKey="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->uniqueKey:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", receipt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->receipt:Lcom/squareup/payment/PaymentReceipt;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", getRemainingBalanceText="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->getRemainingBalanceText:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", countryCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->countryCode:Lcom/squareup/CountryCode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", showLanguageSelection="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->showLanguageSelection:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", displayName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->displayName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", mostRecentActiveCardReaderId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->mostRecentActiveCardReaderId:Lcom/squareup/cardreader/CardReaderId;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", supportsSms="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->supportsSms:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", autoReceiptCompleteTimeout="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->autoReceiptCompleteTimeout:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", showSmsMarketing="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->showSmsMarketing:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", businessName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->businessName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
