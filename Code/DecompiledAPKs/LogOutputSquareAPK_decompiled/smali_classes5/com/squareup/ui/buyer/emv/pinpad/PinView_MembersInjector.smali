.class public final Lcom/squareup/ui/buyer/emv/pinpad/PinView_MembersInjector;
.super Ljava/lang/Object;
.source "PinView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/buyer/emv/pinpad/PinView;",
        ">;"
    }
.end annotation


# instance fields
.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 21
    iput-object p2, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinView_MembersInjector;->deviceProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/buyer/emv/pinpad/PinView;",
            ">;"
        }
    .end annotation

    .line 26
    new-instance v0, Lcom/squareup/ui/buyer/emv/pinpad/PinView_MembersInjector;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/buyer/emv/pinpad/PinView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectDevice(Lcom/squareup/ui/buyer/emv/pinpad/PinView;Lcom/squareup/util/Device;)V
    .locals 0

    .line 41
    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->device:Lcom/squareup/util/Device;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/ui/buyer/emv/pinpad/PinView;Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;)V
    .locals 0

    .line 36
    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinView;->presenter:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/buyer/emv/pinpad/PinView;)V
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/buyer/emv/pinpad/PinView_MembersInjector;->injectPresenter(Lcom/squareup/ui/buyer/emv/pinpad/PinView;Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter;)V

    .line 31
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinView_MembersInjector;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Device;

    invoke-static {p1, v0}, Lcom/squareup/ui/buyer/emv/pinpad/PinView_MembersInjector;->injectDevice(Lcom/squareup/ui/buyer/emv/pinpad/PinView;Lcom/squareup/util/Device;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 9
    check-cast p1, Lcom/squareup/ui/buyer/emv/pinpad/PinView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/emv/pinpad/PinView_MembersInjector;->injectMembers(Lcom/squareup/ui/buyer/emv/pinpad/PinView;)V

    return-void
.end method
