.class public final Lcom/squareup/ui/buyer/workflow/BuyerScopeViewFactory_Factory;
.super Ljava/lang/Object;
.source "BuyerScopeViewFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/buyer/workflow/BuyerScopeViewFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final separatedPrintoutsViewFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsViewFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final storeAndForwardQuickEnableFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableLayoutRunner$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final viewFactoriesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/squareup/workflow/WorkflowViewFactory;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableLayoutRunner$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/squareup/workflow/WorkflowViewFactory;",
            ">;>;)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/ui/buyer/workflow/BuyerScopeViewFactory_Factory;->storeAndForwardQuickEnableFactoryProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p2, p0, Lcom/squareup/ui/buyer/workflow/BuyerScopeViewFactory_Factory;->separatedPrintoutsViewFactoryProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p3, p0, Lcom/squareup/ui/buyer/workflow/BuyerScopeViewFactory_Factory;->viewFactoriesProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/buyer/workflow/BuyerScopeViewFactory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableLayoutRunner$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/squareup/workflow/WorkflowViewFactory;",
            ">;>;)",
            "Lcom/squareup/ui/buyer/workflow/BuyerScopeViewFactory_Factory;"
        }
    .end annotation

    .line 44
    new-instance v0, Lcom/squareup/ui/buyer/workflow/BuyerScopeViewFactory_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/buyer/workflow/BuyerScopeViewFactory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableLayoutRunner$Factory;Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsViewFactory;Ljava/util/Set;)Lcom/squareup/ui/buyer/workflow/BuyerScopeViewFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableLayoutRunner$Factory;",
            "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsViewFactory;",
            "Ljava/util/Set<",
            "Lcom/squareup/workflow/WorkflowViewFactory;",
            ">;)",
            "Lcom/squareup/ui/buyer/workflow/BuyerScopeViewFactory;"
        }
    .end annotation

    .line 51
    new-instance v0, Lcom/squareup/ui/buyer/workflow/BuyerScopeViewFactory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/buyer/workflow/BuyerScopeViewFactory;-><init>(Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableLayoutRunner$Factory;Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsViewFactory;Ljava/util/Set;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/buyer/workflow/BuyerScopeViewFactory;
    .locals 3

    .line 37
    iget-object v0, p0, Lcom/squareup/ui/buyer/workflow/BuyerScopeViewFactory_Factory;->storeAndForwardQuickEnableFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableLayoutRunner$Factory;

    iget-object v1, p0, Lcom/squareup/ui/buyer/workflow/BuyerScopeViewFactory_Factory;->separatedPrintoutsViewFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsViewFactory;

    iget-object v2, p0, Lcom/squareup/ui/buyer/workflow/BuyerScopeViewFactory_Factory;->viewFactoriesProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Set;

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/buyer/workflow/BuyerScopeViewFactory_Factory;->newInstance(Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableLayoutRunner$Factory;Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsViewFactory;Ljava/util/Set;)Lcom/squareup/ui/buyer/workflow/BuyerScopeViewFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/workflow/BuyerScopeViewFactory_Factory;->get()Lcom/squareup/ui/buyer/workflow/BuyerScopeViewFactory;

    move-result-object v0

    return-object v0
.end method
