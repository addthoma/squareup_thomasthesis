.class public abstract Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;
.super Lcom/squareup/mortar/ContextPresenter;
.source "AbstractReceiptPresenter.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;
.implements Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;
.implements Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter$AbstractReceiptView;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter$AbstractReceiptView;",
        ">",
        "Lcom/squareup/mortar/ContextPresenter<",
        "TT;>;",
        "Lcom/squareup/workflow/ui/HandlesBack;",
        "Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;",
        "Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field static DEFAULT_RECEIPT_AUTOCLOSE_MS:J


# instance fields
.field protected final analytics:Lcom/squareup/analytics/Analytics;

.field private final autoClose:Lcom/squareup/util/RxWatchdog;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/util/RxWatchdog<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final autoFinisher:Ljava/lang/Runnable;

.field protected final buyerFlowReceiptManager:Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;

.field private final buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

.field protected final buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

.field protected final cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

.field protected final checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

.field protected final countryCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;"
        }
    .end annotation
.end field

.field private final curatedImage:Lcom/squareup/merchantimages/CuratedImage;

.field protected final customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

.field private final disposables:Lio/reactivex/disposables/CompositeDisposable;

.field protected final emailAndLoyaltyHelper:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;

.field private final employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

.field protected final emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

.field protected final features:Lcom/squareup/settings/server/Features;

.field private isFinishing:Z

.field private localeOverride:Lcom/squareup/locale/LocaleOverrideFactory;

.field protected final loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field protected final offlineMode:Lcom/squareup/payment/OfflineModeMonitor;

.field private final phoneNumbers:Lcom/squareup/text/PhoneNumberHelper;

.field private final printSettings:Lcom/squareup/print/PrintSettings;

.field private final printerStations:Lcom/squareup/print/PrinterStations;

.field protected final receiptAutoCloseProvider:Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseProvider;

.field protected final receiptSender:Lcom/squareup/receipt/ReceiptSender;

.field private final res:Lcom/squareup/util/Res;

.field private final retrofitQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

.field private screen:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptScreenLegacy;

.field protected final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private smartCardReaderId:Lcom/squareup/cardreader/CardReaderId;

.field private final subscriptions:Lrx/subscriptions/CompositeSubscription;

.field private final topScreenChecker:Lcom/squareup/ui/main/TopScreenChecker;

.field private final transaction:Lcom/squareup/payment/Transaction;

.field private transactionEnded:Z

.field protected final transactionMetrics:Lcom/squareup/ui/main/TransactionMetrics;

.field private final tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

.field private final tutorialPresenter:Lcom/squareup/register/tutorial/TutorialPresenter;

.field protected uniqueKey:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 108
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x2

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->DEFAULT_RECEIPT_AUTOCLOSE_MS:J

    return-void
.end method

.method protected constructor <init>(Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/settings/server/AccountStatusSettings;Ljavax/inject/Provider;Lcom/squareup/ui/main/TransactionMetrics;Lcom/squareup/receipt/ReceiptSender;Lcom/squareup/register/tutorial/TutorialPresenter;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/print/PrinterStations;Lcom/squareup/merchantimages/CuratedImage;Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseProvider;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/payment/OfflineModeMonitor;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/ui/main/TopScreenChecker;Lcom/squareup/payment/Transaction;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/util/Res;Lcom/squareup/print/PrintSettings;Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;)V
    .locals 4
    .param p17    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p18    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/buyer/BuyerScopeRunner;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;",
            "Lcom/squareup/ui/main/TransactionMetrics;",
            "Lcom/squareup/receipt/ReceiptSender;",
            "Lcom/squareup/register/tutorial/TutorialPresenter;",
            "Lcom/squareup/log/CheckoutInformationEventLogger;",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            "Lcom/squareup/cardreader/CardReaderHub;",
            "Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;",
            "Lcom/squareup/text/PhoneNumberHelper;",
            "Lcom/squareup/print/PrinterStations;",
            "Lcom/squareup/merchantimages/CuratedImage;",
            "Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseProvider;",
            "Lcom/squareup/thread/executor/MainThread;",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            "Lio/reactivex/Scheduler;",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            "Lcom/squareup/ui/main/TopScreenChecker;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            "Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/print/PrintSettings;",
            "Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;",
            ")V"
        }
    .end annotation

    move-object v0, p0

    .line 170
    invoke-direct {p0}, Lcom/squareup/mortar/ContextPresenter;-><init>()V

    .line 147
    new-instance v1, Lcom/squareup/ui/buyer/receiptlegacy/-$$Lambda$AbstractReceiptPresenter$uG04zq4-6LNyMa26SXMY3-c9yHc;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/receiptlegacy/-$$Lambda$AbstractReceiptPresenter$uG04zq4-6LNyMa26SXMY3-c9yHc;-><init>(Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;)V

    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->autoFinisher:Ljava/lang/Runnable;

    move-object v1, p1

    .line 171
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    move-object v1, p2

    .line 172
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    move-object v1, p3

    .line 173
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->countryCodeProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 174
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->transactionMetrics:Lcom/squareup/ui/main/TransactionMetrics;

    move-object v1, p5

    .line 175
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->receiptSender:Lcom/squareup/receipt/ReceiptSender;

    move-object v1, p6

    .line 176
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->tutorialPresenter:Lcom/squareup/register/tutorial/TutorialPresenter;

    move-object v1, p7

    .line 177
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

    move-object v1, p8

    .line 178
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

    move-object v1, p11

    .line 179
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->phoneNumbers:Lcom/squareup/text/PhoneNumberHelper;

    move-object/from16 v1, p12

    .line 180
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->printerStations:Lcom/squareup/print/PrinterStations;

    move-object v1, p9

    .line 181
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    move-object v1, p10

    .line 182
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    move-object/from16 v1, p13

    .line 183
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->curatedImage:Lcom/squareup/merchantimages/CuratedImage;

    move-object/from16 v1, p14

    .line 184
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->receiptAutoCloseProvider:Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseProvider;

    move-object/from16 v1, p15

    .line 185
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->mainThread:Lcom/squareup/thread/executor/MainThread;

    move-object/from16 v1, p16

    .line 186
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->offlineMode:Lcom/squareup/payment/OfflineModeMonitor;

    move-object/from16 v1, p19

    .line 187
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->topScreenChecker:Lcom/squareup/ui/main/TopScreenChecker;

    move-object/from16 v1, p20

    .line 188
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 189
    new-instance v1, Lcom/squareup/util/RxWatchdog;

    move-object/from16 v2, p17

    move-object/from16 v3, p18

    invoke-direct {v1, v2, v3}, Lcom/squareup/util/RxWatchdog;-><init>(Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->autoClose:Lcom/squareup/util/RxWatchdog;

    move-object/from16 v1, p21

    .line 190
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    move-object/from16 v1, p22

    .line 191
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->features:Lcom/squareup/settings/server/Features;

    move-object/from16 v1, p23

    .line 192
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    move-object/from16 v1, p24

    .line 193
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    move-object/from16 v1, p25

    .line 194
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->buyerFlowReceiptManager:Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;

    move-object/from16 v1, p26

    .line 195
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    move-object/from16 v1, p27

    .line 196
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->retrofitQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    move-object/from16 v1, p28

    .line 197
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    move-object/from16 v1, p29

    .line 198
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->res:Lcom/squareup/util/Res;

    move-object/from16 v1, p30

    .line 199
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->printSettings:Lcom/squareup/print/PrintSettings;

    move-object/from16 v1, p31

    .line 200
    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->emailAndLoyaltyHelper:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;

    .line 202
    new-instance v1, Lrx/subscriptions/CompositeSubscription;

    invoke-direct {v1}, Lrx/subscriptions/CompositeSubscription;-><init>()V

    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->subscriptions:Lrx/subscriptions/CompositeSubscription;

    .line 203
    new-instance v1, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v1}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object v1, v0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    return-void
.end method

.method private buyerLocaleUpdated(Lcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 9

    .line 332
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter$AbstractReceiptView;

    .line 333
    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->countryCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/CountryCode;

    .line 335
    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object v2

    .line 336
    invoke-static {v1}, Lcom/squareup/address/CountryResources;->smsReceiptDisclaimerId(Lcom/squareup/CountryCode;)I

    move-result v3

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter$AbstractReceiptView;->setSmsReceiptDisclaimer(Ljava/lang/CharSequence;)V

    .line 337
    invoke-static {v1}, Lcom/squareup/address/CountryResources;->emailReceiptDisclaimerId(Lcom/squareup/CountryCode;)I

    move-result v3

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter$AbstractReceiptView;->setEmailReceiptDisclaimer(Ljava/lang/CharSequence;)V

    .line 338
    invoke-static {v1}, Lcom/squareup/address/CountryResources;->generalReceiptDisclaimerId(Lcom/squareup/CountryCode;)I

    move-result v3

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter$AbstractReceiptView;->setDigitalReceiptHint(Ljava/lang/CharSequence;)V

    .line 339
    iget-object v3, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    invoke-virtual {v3}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->shouldShowDisplayNamePerScreen()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 340
    iget-object v3, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    invoke-virtual {v3}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->getDisplayNameText()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter$AbstractReceiptView;->setTicketName(Ljava/lang/CharSequence;)V

    .line 342
    :cond_0
    invoke-static {v1}, Lcom/squareup/address/CountryResources;->noReceiptId(Lcom/squareup/CountryCode;)I

    move-result v1

    invoke-interface {v2, v1}, Lcom/squareup/util/Res;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter$AbstractReceiptView;->setNoReceiptText(Ljava/lang/CharSequence;)V

    .line 344
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/payment/PaymentReceipt;->getRemainingAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 345
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/squareup/payment/PaymentReceipt;->getRemainingBalanceText(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v3

    if-eqz v1, :cond_1

    .line 347
    iget-object v4, v1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v8, v4, v6

    if-lez v8, :cond_1

    .line 349
    sget v3, Lcom/squareup/checkout/R$string;->buyer_remaining_payment_due:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v3

    .line 350
    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getMoneyFormatter()Lcom/squareup/text/Formatter;

    move-result-object v4

    invoke-interface {v4, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    const-string v4, "amount"

    invoke-virtual {v3, v4, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 351
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 349
    invoke-interface {v0, v1}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter$AbstractReceiptView;->setAmountSubtitle(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 352
    :cond_1
    invoke-static {v3}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 354
    invoke-interface {v0, v3}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter$AbstractReceiptView;->setAmountSubtitle(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 356
    :cond_2
    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->showTenderAmountAndTip(Lcom/squareup/locale/LocaleOverrideFactory;)V

    .line 359
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    invoke-virtual {v1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->isReceiptSelectionMade()Z

    move-result v1

    .line 360
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->shouldSkipReceipt()Z

    move-result v3

    if-nez v3, :cond_4

    if-eqz v1, :cond_3

    goto :goto_1

    .line 364
    :cond_3
    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v3, "Shown ReceiptScreenLegacy"

    invoke-interface {v1, v3}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    .line 365
    invoke-virtual {p0, v2}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->showEmailAndSmsRows(Lcom/squareup/util/Res;)V

    .line 366
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->showReceiptSection()V

    goto :goto_2

    .line 361
    :cond_4
    :goto_1
    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    invoke-direct {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->isPaymentComplete()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const-string v3, "Shown Receipt -- All Done"

    invoke-interface {v1, v3, v2}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;Ljava/lang/Object;)V

    .line 362
    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->showAllDone(Lcom/squareup/locale/LocaleOverrideFactory;)V

    .line 369
    :goto_2
    invoke-interface {v0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter$AbstractReceiptView;->resourcesUpdated(Lcom/squareup/locale/LocaleOverrideFactory;)V

    .line 370
    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->updateStrings(Lcom/squareup/locale/LocaleOverrideFactory;)V

    .line 372
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getLocale()Ljava/util/Locale;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/payment/PaymentReceipt;->updateBuyerLanguage(Ljava/util/Locale;)V

    return-void
.end method

.method private checkAndEnqueueMissedOpportunity()V
    .locals 1

    .line 709
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->offlineMode:Lcom/squareup/payment/OfflineModeMonitor;

    invoke-interface {v0}, Lcom/squareup/payment/OfflineModeMonitor;->isInOfflineMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 710
    sget-object v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->OFFLINE_MODE:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    invoke-direct {p0, v0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->enqueueMissedLoyaltyOpportunity(Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;)V

    goto :goto_0

    .line 711
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    invoke-interface {v0}, Lcom/squareup/loyalty/LoyaltySettings;->isLoyaltyScreensEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 712
    sget-object v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->CLIENT_DISABLED_LOYALTY:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    invoke-direct {p0, v0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->enqueueMissedLoyaltyOpportunity(Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;)V

    goto :goto_0

    .line 714
    :cond_1
    sget-object v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;->RETURNED_FROM_RECEIPT_SCREEN:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    invoke-direct {p0, v0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->enqueueMissedLoyaltyOpportunity(Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;)V

    :goto_0
    return-void
.end method

.method private enqueueMissedLoyaltyOpportunity(Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;)V
    .locals 4

    .line 719
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-interface {v0}, Lcom/squareup/permissions/EmployeeManagement;->getCurrentEmployeeToken()Ljava/lang/String;

    move-result-object v0

    .line 720
    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->emailAndLoyaltyHelper:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;

    iget-object v2, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    iget-object v3, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->res:Lcom/squareup/util/Res;

    .line 721
    invoke-static {v0, v2, v3}, Lcom/squareup/activity/refund/CreatorDetailsHelper;->forEmployeeToken(Ljava/lang/String;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/util/Res;)Lcom/squareup/protos/client/CreatorDetails;

    move-result-object v0

    .line 720
    invoke-virtual {v1, p1, v0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->logMissedLoyalty(Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;Lcom/squareup/protos/client/CreatorDetails;)V

    return-void
.end method

.method private getContactEmailAddress()Ljava/lang/String;
    .locals 1

    .line 751
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->getCustomerContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 752
    :cond_0
    invoke-static {v0}, Lcom/squareup/crm/util/RolodexContactHelper;->getEmail(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method private getContactSmsNumber()Ljava/lang/String;
    .locals 1

    .line 746
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->getCustomerContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 747
    :cond_0
    invoke-static {v0}, Lcom/squareup/crm/util/RolodexContactHelper;->getPhone(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method private getDefaultEmail()Ljava/lang/String;
    .locals 1

    .line 565
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/PaymentReceipt;->hasDefaultEmail()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/PaymentReceipt;->getDefaultEmail()Lcom/squareup/payment/Obfuscated;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Obfuscated;->getValue()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method private getDefaultSmsNumber()Ljava/lang/String;
    .locals 1

    .line 569
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/PaymentReceipt;->hasDefaultSms()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/PaymentReceipt;->getDefaultSms()Lcom/squareup/payment/Obfuscated;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Obfuscated;->getValue()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method private getTrimmedEmail()Ljava/lang/String;
    .locals 1

    .line 688
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter$AbstractReceiptView;

    if-nez v0, :cond_0

    const-string v0, ""

    goto :goto_0

    .line 689
    :cond_0
    invoke-interface {v0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter$AbstractReceiptView;->getEmailString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method private getTrimmedSms()Ljava/lang/String;
    .locals 1

    .line 693
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter$AbstractReceiptView;

    if-nez v0, :cond_0

    const-string v0, ""

    goto :goto_0

    .line 694
    :cond_0
    invoke-interface {v0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter$AbstractReceiptView;->getSmsString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method private isPaymentComplete()Z
    .locals 1

    .line 561
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/PaymentReceipt;->paymentComplete()Z

    move-result v0

    return v0
.end method

.method static synthetic lambda$onLoad$5(Lkotlin/Unit;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    return-object p1
.end method

.method static synthetic lambda$onLoad$6(Ljava/lang/Boolean;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 288
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$onLoad$8(Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter$AbstractReceiptView;Lcom/squareup/payment/Payment;Lkotlin/Unit;)V
    .locals 0

    .line 315
    invoke-virtual {p1}, Lcom/squareup/payment/Payment;->hasCustomer()Z

    move-result p1

    invoke-interface {p0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter$AbstractReceiptView;->showCustomerButton(Z)V

    return-void
.end method

.method private restartScreenTimeout()V
    .locals 5

    .line 756
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->autoClose:Lcom/squareup/util/RxWatchdog;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    sget-wide v2, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->DEFAULT_RECEIPT_AUTOCLOSE_MS:J

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/squareup/util/RxWatchdog;->restart(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)V

    return-void
.end method

.method public static shouldShowAddCardButton(Lcom/squareup/payment/Payment;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/crm/CustomerManagementSettings;)Z
    .locals 3

    .line 208
    invoke-interface {p1}, Lcom/squareup/payment/OfflineModeMonitor;->isInOfflineMode()Z

    move-result p1

    const/4 v0, 0x0

    if-nez p1, :cond_4

    instance-of p1, p0, Lcom/squareup/payment/BillPayment;

    if-nez p1, :cond_0

    goto :goto_0

    .line 212
    :cond_0
    move-object p1, p0

    check-cast p1, Lcom/squareup/payment/BillPayment;

    .line 213
    invoke-virtual {p1}, Lcom/squareup/payment/BillPayment;->hasMagStripeTenderInFlight()Z

    move-result v1

    .line 214
    invoke-virtual {p1}, Lcom/squareup/payment/BillPayment;->hasEmvTenderInFlight()Z

    move-result v2

    .line 215
    invoke-interface {p2, v1, v2}, Lcom/squareup/crm/CustomerManagementSettings;->showCardButtonEnabledForTenderType(ZZ)Z

    move-result p2

    if-nez p2, :cond_1

    return v0

    .line 220
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/payment/BillPayment;->hasCustomer()Z

    move-result p2

    if-eqz p2, :cond_3

    .line 221
    invoke-virtual {p1}, Lcom/squareup/payment/BillPayment;->getCard()Lcom/squareup/Card;

    move-result-object p1

    .line 222
    invoke-virtual {p1}, Lcom/squareup/Card;->getPan()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1}, Lcom/squareup/Card;->getPan()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x4

    invoke-virtual {p2, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p2

    .line 224
    invoke-virtual {p0}, Lcom/squareup/payment/Payment;->getCustomerInstrumentDetails()Ljava/util/List;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_2
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;

    .line 225
    iget-object v1, v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;->display_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;

    if-eqz v1, :cond_2

    .line 226
    iget-object v2, v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;->last_four:Ljava/lang/String;

    .line 227
    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 228
    invoke-static {v1}, Lcom/squareup/card/CardBrands;->toCardBrand(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)Lcom/squareup/Card$Brand;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/Card;->getBrand()Lcom/squareup/Card$Brand;

    move-result-object v2

    if-ne v1, v2, :cond_2

    return v0

    :cond_3
    const/4 p0, 0x1

    return p0

    :cond_4
    :goto_0
    return v0
.end method

.method private subscribeToMerchantHeroImage()V
    .locals 3

    .line 698
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->curatedImage:Lcom/squareup/merchantimages/CuratedImage;

    invoke-interface {v1}, Lcom/squareup/merchantimages/CuratedImage;->loadDarkened()Lio/reactivex/Observable;

    move-result-object v1

    .line 699
    invoke-static {}, Lcom/squareup/util/OptionalExtensionsKt;->mapIfPresentObservable()Lio/reactivex/ObservableTransformer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/buyer/receiptlegacy/-$$Lambda$AbstractReceiptPresenter$36UitSZBj7tingwqb2lTCcdq_fc;

    invoke-direct {v2, p0}, Lcom/squareup/ui/buyer/receiptlegacy/-$$Lambda$AbstractReceiptPresenter$36UitSZBj7tingwqb2lTCcdq_fc;-><init>(Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;)V

    .line 700
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 698
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method private updateMessagesForSmartCard(Lcom/squareup/cardreader/CardReaderId;)V
    .locals 1

    .line 422
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->smartCardReaderId:Lcom/squareup/cardreader/CardReaderId;

    invoke-static {v0, p1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->hasView()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 423
    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->localeOverride:Lcom/squareup/locale/LocaleOverrideFactory;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->updateMessages(Lcom/squareup/locale/LocaleOverrideFactory;)V

    :cond_0
    return-void
.end method

.method private useDefaultEmailAddress()Z
    .locals 1

    .line 577
    invoke-direct {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getTrimmedEmail()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getDefaultEmail()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private useDefaultSmsNumber()Z
    .locals 1

    .line 581
    invoke-direct {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getTrimmedSms()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getDefaultSmsNumber()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method


# virtual methods
.method protected cancelAutoFinish()V
    .locals 2

    .line 418
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->autoFinisher:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->cancel(Ljava/lang/Runnable;)V

    return-void
.end method

.method protected doScheduleFinish()V
    .locals 4

    .line 410
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->receiptAutoCloseProvider:Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseProvider;

    invoke-interface {v0}, Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseProvider;->getReceiptAutoCloseOverride()Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseOverride;

    move-result-object v0

    .line 411
    instance-of v1, v0, Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseOverride$HasReceiptAutoCloseOverride;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseOverride$HasReceiptAutoCloseOverride;

    .line 412
    invoke-virtual {v0}, Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseOverride$HasReceiptAutoCloseOverride;->getDurationMs()J

    move-result-wide v0

    goto :goto_0

    :cond_0
    sget-wide v0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->DEFAULT_RECEIPT_AUTOCLOSE_MS:J

    .line 414
    :goto_0
    iget-object v2, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v3, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->autoFinisher:Ljava/lang/Runnable;

    invoke-interface {v2, v3, v0, v1}, Lcom/squareup/thread/executor/MainThread;->executeDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public dropView(Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter$AbstractReceiptView;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .line 376
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_0

    .line 377
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->subscriptions:Lrx/subscriptions/CompositeSubscription;

    invoke-virtual {v0}, Lrx/subscriptions/CompositeSubscription;->clear()V

    .line 378
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    .line 381
    :cond_0
    invoke-super {p0, p1}, Lcom/squareup/mortar/ContextPresenter;->dropView(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic dropView(Ljava/lang/Object;)V
    .locals 0

    .line 103
    check-cast p1, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter$AbstractReceiptView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->dropView(Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter$AbstractReceiptView;)V

    return-void
.end method

.method endTransaction()V
    .locals 2

    .line 507
    iget-boolean v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->transactionEnded:Z

    if-nez v0, :cond_1

    .line 508
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->isPaymentComplete()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 509
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->transactionMetrics:Lcom/squareup/ui/main/TransactionMetrics;

    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->uniqueKey:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/squareup/ui/main/TransactionMetrics;->endTransaction(Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x1

    .line 511
    iput-boolean v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->transactionEnded:Z

    :cond_1
    return-void
.end method

.method finish()V
    .locals 4

    .line 516
    iget-boolean v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->isFinishing:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    .line 517
    iput-boolean v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->isFinishing:Z

    .line 518
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->emailAndLoyaltyHelper:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->setIsFinishing()V

    .line 520
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->isComplete()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 521
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    .line 522
    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->getTenderTypeForLogging()Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    move-result-object v1

    .line 523
    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->getBillId()Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    .line 524
    iget-object v2, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

    const/4 v3, 0x0

    invoke-interface {v2, v1, v0, v3}, Lcom/squareup/log/CheckoutInformationEventLogger;->finishCheckout(Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;Ljava/lang/String;Z)V

    .line 527
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

    invoke-interface {v0}, Lcom/squareup/crm/CustomerManagementSettings;->isAddCustomerToSaleEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 529
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->enqueueAttachContactTask()V

    .line 532
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->completeBuyerFlow(Lcom/squareup/payment/Payment;)V

    :cond_2
    return-void
.end method

.method protected getReceipt()Lcom/squareup/payment/PaymentReceipt;
    .locals 1

    .line 760
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireReceiptForLastPayment()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v0

    return-object v0
.end method

.method protected getValidEmail()Ljava/lang/String;
    .locals 2

    .line 547
    invoke-direct {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getTrimmedEmail()Ljava/lang/String;

    move-result-object v0

    .line 548
    invoke-static {v0}, Lcom/squareup/text/Emails;->isValid(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getValidSmsNumber()Ljava/lang/String;
    .locals 2

    .line 556
    invoke-direct {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getTrimmedSms()Ljava/lang/String;

    move-result-object v0

    .line 557
    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->phoneNumbers:Lcom/squareup/text/PhoneNumberHelper;

    invoke-interface {v1, v0}, Lcom/squareup/text/PhoneNumberHelper;->isValid(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method protected isReceiptPrintingAvailable()Z
    .locals 4

    .line 537
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->printerStations:Lcom/squareup/print/PrinterStations;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/squareup/print/PrinterStation$Role;

    sget-object v2, Lcom/squareup/print/PrinterStation$Role;->RECEIPTS:Lcom/squareup/print/PrinterStation$Role;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-interface {v0, v1}, Lcom/squareup/print/PrinterStations;->hasEnabledStationsForAnyRoleIn([Lcom/squareup/print/PrinterStation$Role;)Z

    move-result v0

    return v0
.end method

.method public synthetic lambda$new$0$AbstractReceiptPresenter()V
    .locals 0

    .line 148
    invoke-direct {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->checkAndEnqueueMissedOpportunity()V

    .line 149
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->endTransaction()V

    .line 150
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->finish()V

    return-void
.end method

.method public synthetic lambda$null$2$AbstractReceiptPresenter(Lcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 273
    iput-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->localeOverride:Lcom/squareup/locale/LocaleOverrideFactory;

    .line 274
    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->localeOverride:Lcom/squareup/locale/LocaleOverrideFactory;

    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->buyerLocaleUpdated(Lcom/squareup/locale/LocaleOverrideFactory;)V

    return-void
.end method

.method public synthetic lambda$onEnterScope$1$AbstractReceiptPresenter(Ljava/lang/Boolean;)V
    .locals 0

    .line 251
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->cancelAutoFinish()V

    return-void
.end method

.method public synthetic lambda$onLoad$3$AbstractReceiptPresenter()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 271
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    invoke-interface {v0}, Lcom/squareup/buyer/language/BuyerLocaleOverride;->localeOverrideFactory()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/buyer/receiptlegacy/-$$Lambda$AbstractReceiptPresenter$Hzysmurz3sKcASeoXIsmC3U4FBY;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/receiptlegacy/-$$Lambda$AbstractReceiptPresenter$Hzysmurz3sKcASeoXIsmC3U4FBY;-><init>(Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onLoad$4$AbstractReceiptPresenter(Lkotlin/Unit;)V
    .locals 0

    .line 279
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 280
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->onNewSaleClicked()V

    return-void
.end method

.method public synthetic lambda$onLoad$7$AbstractReceiptPresenter(Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter$AbstractReceiptView;Ljava/lang/Boolean;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 290
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->onNewSaleClicked()V

    .line 291
    iget-object p2, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v0, Lcom/squareup/crm/events/CrmScreenTimeoutEvent;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/crm/events/CrmScreenTimeoutEvent;-><init>(Ljava/lang/String;)V

    invoke-interface {p2, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public synthetic lambda$onLoad$9$AbstractReceiptPresenter(Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter$AbstractReceiptView;Lcom/squareup/payment/Payment;Lkotlin/Unit;)V
    .locals 1

    .line 322
    iget-object p3, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->offlineMode:Lcom/squareup/payment/OfflineModeMonitor;

    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

    invoke-static {p2, p3, v0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->shouldShowAddCardButton(Lcom/squareup/payment/Payment;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/crm/CustomerManagementSettings;)Z

    move-result p2

    invoke-interface {p1, p2}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter$AbstractReceiptView;->showCustomerAddCardButton(Z)V

    return-void
.end method

.method public synthetic lambda$subscribeToMerchantHeroImage$10$AbstractReceiptPresenter(Lcom/squareup/picasso/RequestCreator;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 701
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getMerchantProfileSettings()Lcom/squareup/settings/server/MerchantProfileSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/MerchantProfileSettings;->shouldUseCuratedImageForReceipt()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 703
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter$AbstractReceiptView;

    invoke-interface {v0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter$AbstractReceiptView;->setBackgroundImage(Lcom/squareup/picasso/RequestCreator;)V

    :cond_0
    return-void
.end method

.method final logEmailReceiptSent()V
    .locals 4

    .line 636
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/payment/PaymentReceipt;->getTenderAmount()Lcom/squareup/protos/common/Money;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    .line 637
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/payment/Payment;->getBillId()Lcom/squareup/protos/client/IdPair;

    move-result-object v3

    iget-object v3, v3, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    .line 636
    invoke-interface {v0, v1, v2, v3}, Lcom/squareup/log/CheckoutInformationEventLogger;->userProvideEmail(JLjava/lang/String;)V

    return-void
.end method

.method protected maybeEnableClickAnywhereToFinish()V
    .locals 1

    .line 768
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

    invoke-interface {v0}, Lcom/squareup/crm/CustomerManagementSettings;->isAfterCheckoutEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    .line 769
    invoke-interface {v0}, Lcom/squareup/loyalty/LoyaltySettings;->isLoyaltyProgramActive()Z

    move-result v0

    if-nez v0, :cond_0

    .line 770
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter$AbstractReceiptView;

    invoke-interface {v0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter$AbstractReceiptView;->enableClickAnywhereToFinish()V

    :cond_0
    return-void
.end method

.method maybeGoToLoyaltyOrEmailCollectionScreen()V
    .locals 7

    .line 725
    invoke-direct {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getContactSmsNumber()Ljava/lang/String;

    move-result-object v0

    .line 726
    invoke-direct {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getContactEmailAddress()Ljava/lang/String;

    move-result-object v1

    .line 727
    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    const/4 v3, 0x1

    xor-int/2addr v2, v3

    .line 730
    iget-object v4, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    invoke-virtual {v4}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->hasValidSmsNumber()Z

    move-result v4

    const/4 v5, 0x0

    if-nez v4, :cond_1

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 731
    iget-object v4, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->phoneNumbers:Lcom/squareup/text/PhoneNumberHelper;

    invoke-interface {v4, v0}, Lcom/squareup/text/PhoneNumberHelper;->isValid(Ljava/lang/String;)Z

    move-result v4

    .line 732
    iget-object v6, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    if-eqz v4, :cond_0

    goto :goto_0

    :cond_0
    move-object v0, v5

    :goto_0
    invoke-virtual {v6, v0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->setValidSmsNumber(Ljava/lang/String;)V

    .line 735
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->hasValidEmailAddress()Z

    move-result v0

    if-nez v0, :cond_3

    if-eqz v2, :cond_3

    .line 736
    invoke-static {v1}, Lcom/squareup/text/Emails;->isValid(Ljava/lang/CharSequence;)Z

    move-result v0

    .line 737
    iget-object v4, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    if-eqz v0, :cond_2

    goto :goto_1

    :cond_2
    move-object v1, v5

    :goto_1
    invoke-virtual {v4, v1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->setValidEmailAddress(Ljava/lang/String;)V

    .line 740
    :cond_3
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->emailAndLoyaltyHelper:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;

    if-nez v2, :cond_5

    .line 741
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->useValidEmailAddress()Z

    move-result v1

    if-nez v1, :cond_5

    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/payment/PaymentReceipt;->hasDefaultEmail()Z

    move-result v1

    if-eqz v1, :cond_4

    goto :goto_2

    :cond_4
    const/4 v3, 0x0

    .line 740
    :cond_5
    :goto_2
    invoke-virtual {v0, v3}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->setHasEmailAddress(Z)V

    .line 742
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->emailAndLoyaltyHelper:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->goToLoyaltyOrEmailCollection()V

    return-void
.end method

.method abstract noReceiptClicked(Lcom/squareup/locale/LocaleOverrideFactory;)V
.end method

.method public onBackPressed()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public onCardReaderAdded(Lcom/squareup/cardreader/CardReader;)V
    .locals 0

    return-void
.end method

.method public onCardReaderRemoved(Lcom/squareup/cardreader/CardReader;)V
    .locals 0

    .line 406
    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->updateMessagesForSmartCard(Lcom/squareup/cardreader/CardReaderId;)V

    return-void
.end method

.method onCustomerAddCardClicked()V
    .locals 2

    .line 667
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->cancelAutoFinish()V

    .line 668
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->goToFirstAddCardScreen(Lcom/squareup/payment/PaymentReceipt;)V

    return-void
.end method

.method onCustomerClicked()V
    .locals 2

    .line 661
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->cancelAutoFinish()V

    .line 662
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->goToFirstCrmScreen(Lcom/squareup/payment/PaymentReceipt;)V

    .line 663
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

    invoke-interface {v0}, Lcom/squareup/log/CheckoutInformationEventLogger;->updateTentativeEndTime()V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 237
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptScreenLegacy;

    iput-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->screen:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptScreenLegacy;

    .line 238
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->screen:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptScreenLegacy;

    iget-object v0, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptScreenLegacy;->uniqueKey:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->uniqueKey:Ljava/lang/String;

    .line 239
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->screen:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptScreenLegacy;

    iget-object v0, v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptScreenLegacy;->smartCardReaderId:Lcom/squareup/cardreader/CardReaderId;

    iput-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->smartCardReaderId:Lcom/squareup/cardreader/CardReaderId;

    .line 241
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

    invoke-interface {v0}, Lcom/squareup/log/CheckoutInformationEventLogger;->updateTentativeEndTime()V

    .line 242
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

    .line 243
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/payment/PaymentReceipt;->hasDefaultEmail()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/squareup/log/CheckoutInformationEventLogger;->setEmailOnFile(Z)V

    .line 244
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    invoke-interface {v0, p1, p0}, Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;->registerEmvCardInsertRemoveProcessor(Lmortar/MortarScope;Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;)V

    .line 245
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {v0, p0}, Lcom/squareup/cardreader/CardReaderHub;->addCardReaderAttachListener(Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;)V

    .line 247
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->emailAndLoyaltyHelper:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->init(Lmortar/MortarScope;)V

    .line 249
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->emailAndLoyaltyHelper:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;

    .line 250
    invoke-virtual {v0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->onIsFinishing()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/buyer/receiptlegacy/-$$Lambda$AbstractReceiptPresenter$Pr0nnheSOu4Tg55-Vor04BnNUU0;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/receiptlegacy/-$$Lambda$AbstractReceiptPresenter$Pr0nnheSOu4Tg55-Vor04BnNUU0;-><init>(Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;)V

    .line 251
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 249
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    return-void
.end method

.method protected onExitScope()V
    .locals 1

    .line 255
    invoke-super {p0}, Lcom/squareup/mortar/ContextPresenter;->onExitScope()V

    .line 256
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {v0, p0}, Lcom/squareup/cardreader/CardReaderHub;->removeCardReaderAttachListener(Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;)V

    .line 257
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->emailAndLoyaltyHelper:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->terminate()V

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 6

    .line 261
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter$AbstractReceiptView;

    .line 262
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v1

    .line 264
    invoke-direct {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->subscribeToMerchantHeroImage()V

    .line 266
    iget-object v2, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->res:Lcom/squareup/util/Res;

    .line 267
    invoke-virtual {v1}, Lcom/squareup/payment/Payment;->isComplete()Z

    move-result v3

    if-eqz v3, :cond_0

    sget v3, Lcom/squareup/checkout/R$string;->new_sale:I

    goto :goto_0

    :cond_0
    sget v3, Lcom/squareup/common/strings/R$string;->continue_label:I

    :goto_0
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 266
    invoke-interface {v0, v2}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter$AbstractReceiptView;->setNewSaleText(Ljava/lang/CharSequence;)V

    .line 270
    invoke-interface {v0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter$AbstractReceiptView;->asView()Landroid/view/View;

    move-result-object v2

    new-instance v3, Lcom/squareup/ui/buyer/receiptlegacy/-$$Lambda$AbstractReceiptPresenter$lAMjrCfRVs472VReyd3eMymTe9k;

    invoke-direct {v3, p0}, Lcom/squareup/ui/buyer/receiptlegacy/-$$Lambda$AbstractReceiptPresenter$lAMjrCfRVs472VReyd3eMymTe9k;-><init>(Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;)V

    invoke-static {v2, v3}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 277
    iget-object v2, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->subscriptions:Lrx/subscriptions/CompositeSubscription;

    invoke-interface {v0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter$AbstractReceiptView;->onNewSaleClicked()Lrx/Observable;

    move-result-object v3

    new-instance v4, Lcom/squareup/ui/buyer/receiptlegacy/-$$Lambda$AbstractReceiptPresenter$G2PaNbm5yDwHfB6RBSdAPC3Y9E0;

    invoke-direct {v4, p0}, Lcom/squareup/ui/buyer/receiptlegacy/-$$Lambda$AbstractReceiptPresenter$G2PaNbm5yDwHfB6RBSdAPC3Y9E0;-><init>(Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;)V

    .line 278
    invoke-virtual {v3, v4}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v3

    .line 277
    invoke-virtual {v2, v3}, Lrx/subscriptions/CompositeSubscription;->add(Lrx/Subscription;)V

    .line 283
    invoke-direct {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->restartScreenTimeout()V

    .line 284
    iget-object v2, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v3, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->autoClose:Lcom/squareup/util/RxWatchdog;

    .line 285
    invoke-virtual {v3}, Lcom/squareup/util/RxWatchdog;->timeout()Lio/reactivex/Observable;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->topScreenChecker:Lcom/squareup/ui/main/TopScreenChecker;

    iget-object v5, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->screen:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptScreenLegacy;

    .line 286
    invoke-virtual {v4, v5}, Lcom/squareup/ui/main/TopScreenChecker;->unobscured(Lcom/squareup/container/ContainerTreeKey;)Lio/reactivex/Observable;

    move-result-object v4

    sget-object v5, Lcom/squareup/ui/buyer/receiptlegacy/-$$Lambda$AbstractReceiptPresenter$rqgy5YydpbtkulFLmF7qRJ68U8k;->INSTANCE:Lcom/squareup/ui/buyer/receiptlegacy/-$$Lambda$AbstractReceiptPresenter$rqgy5YydpbtkulFLmF7qRJ68U8k;

    .line 285
    invoke-static {v3, v4, v5}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v3

    sget-object v4, Lcom/squareup/ui/buyer/receiptlegacy/-$$Lambda$AbstractReceiptPresenter$QVAZkK5rU8mYcmcPILQH9TLixHk;->INSTANCE:Lcom/squareup/ui/buyer/receiptlegacy/-$$Lambda$AbstractReceiptPresenter$QVAZkK5rU8mYcmcPILQH9TLixHk;

    .line 288
    invoke-virtual {v3, v4}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v3

    new-instance v4, Lcom/squareup/ui/buyer/receiptlegacy/-$$Lambda$AbstractReceiptPresenter$Efm8_AyiFBqK8A9cTwn1O_wFFc0;

    invoke-direct {v4, p0, v0}, Lcom/squareup/ui/buyer/receiptlegacy/-$$Lambda$AbstractReceiptPresenter$Efm8_AyiFBqK8A9cTwn1O_wFFc0;-><init>(Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter$AbstractReceiptView;)V

    .line 289
    invoke-virtual {v3, v4}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v3

    .line 284
    invoke-virtual {v2, v3}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    if-nez p1, :cond_1

    .line 299
    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->buyerFlowReceiptManager:Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;

    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v2

    invoke-interface {p1, v2}, Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;->maybeAutoPrintReceipt(Lcom/squareup/payment/PaymentReceipt;)Lio/reactivex/Observable;

    move-result-object p1

    invoke-virtual {p1}, Lio/reactivex/Observable;->subscribe()Lio/reactivex/disposables/Disposable;

    .line 302
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->isReceiptPrintingAvailable()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 303
    invoke-interface {v0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter$AbstractReceiptView;->showPrintReceiptButton()V

    .line 306
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->printerStations:Lcom/squareup/print/PrinterStations;

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/squareup/print/PrinterStation$Role;

    const/4 v3, 0x0

    sget-object v4, Lcom/squareup/print/PrinterStation$Role;->RECEIPTS:Lcom/squareup/print/PrinterStation$Role;

    aput-object v4, v2, v3

    invoke-interface {p1, v2}, Lcom/squareup/print/PrinterStations;->hasEnabledStationsForAnyRoleIn([Lcom/squareup/print/PrinterStation$Role;)Z

    move-result p1

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->printSettings:Lcom/squareup/print/PrintSettings;

    .line 307
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v2

    invoke-interface {p1, v2}, Lcom/squareup/print/PrintSettings;->isFormalReceiptPrintingAvailable(Lcom/squareup/payment/PaymentReceipt;)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 308
    invoke-interface {v0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter$AbstractReceiptView;->showPrintFormalReceiptButton()V

    .line 311
    :cond_3
    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

    invoke-interface {p1}, Lcom/squareup/crm/CustomerManagementSettings;->isAfterCheckoutEnabled()Z

    move-result p1

    if-eqz p1, :cond_4

    .line 312
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object p1

    instance-of p1, p1, Lcom/squareup/payment/PaymentReceipt$NoTenderReceipt;

    if-nez p1, :cond_4

    .line 313
    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->subscriptions:Lrx/subscriptions/CompositeSubscription;

    .line 314
    invoke-virtual {v1}, Lcom/squareup/payment/Payment;->onCustomerChanged()Lrx/Observable;

    move-result-object v2

    new-instance v3, Lcom/squareup/ui/buyer/receiptlegacy/-$$Lambda$AbstractReceiptPresenter$Zt2TamTG6AWynY3BPigPQGjckNA;

    invoke-direct {v3, v0, v1}, Lcom/squareup/ui/buyer/receiptlegacy/-$$Lambda$AbstractReceiptPresenter$Zt2TamTG6AWynY3BPigPQGjckNA;-><init>(Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter$AbstractReceiptView;Lcom/squareup/payment/Payment;)V

    .line 315
    invoke-virtual {v2, v3}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v2

    .line 313
    invoke-virtual {p1, v2}, Lrx/subscriptions/CompositeSubscription;->add(Lrx/Subscription;)V

    .line 318
    :cond_4
    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

    invoke-interface {p1}, Lcom/squareup/crm/CustomerManagementSettings;->isSaveCardPostTransactionEnabled()Z

    move-result p1

    if-eqz p1, :cond_5

    .line 319
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/payment/PaymentReceipt;->isCard()Z

    move-result p1

    if-eqz p1, :cond_5

    .line 320
    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->subscriptions:Lrx/subscriptions/CompositeSubscription;

    .line 321
    invoke-virtual {v1}, Lcom/squareup/payment/Payment;->onCustomerChanged()Lrx/Observable;

    move-result-object v2

    new-instance v3, Lcom/squareup/ui/buyer/receiptlegacy/-$$Lambda$AbstractReceiptPresenter$TeRBMnpwyqbKKcGllwWRjdlSHAQ;

    invoke-direct {v3, p0, v0, v1}, Lcom/squareup/ui/buyer/receiptlegacy/-$$Lambda$AbstractReceiptPresenter$TeRBMnpwyqbKKcGllwWRjdlSHAQ;-><init>(Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter$AbstractReceiptView;Lcom/squareup/payment/Payment;)V

    .line 322
    invoke-virtual {v2, v3}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 320
    invoke-virtual {p1, v0}, Lrx/subscriptions/CompositeSubscription;->add(Lrx/Subscription;)V

    .line 326
    :cond_5
    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v0, Lcom/squareup/settings/server/Features$Feature;->CAN_USE_BUYER_LANGUAGE_SELECTION:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p1, v0}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p1

    if-eqz p1, :cond_6

    .line 327
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter$AbstractReceiptView;

    invoke-interface {p1}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter$AbstractReceiptView;->showBuyerLanguageSelection()V

    :cond_6
    return-void
.end method

.method onNewSaleClicked()V
    .locals 1

    .line 428
    invoke-direct {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->checkAndEnqueueMissedOpportunity()V

    .line 429
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->isReceiptSelectionMade()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/PaymentReceipt;->decline()V

    .line 430
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->endTransaction()V

    .line 431
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->finish()V

    return-void
.end method

.method protected final onSave(Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method protected onShowingThanks()V
    .locals 3

    .line 541
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->tutorialPresenter:Lcom/squareup/register/tutorial/TutorialPresenter;

    invoke-virtual {v0}, Lcom/squareup/register/tutorial/TutorialPresenter;->onShowingThanks()V

    .line 542
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    invoke-direct {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->isPaymentComplete()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const-string v2, "Shown Receipt -- All Done"

    invoke-interface {v0, v2, v1}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method abstract printFormalReceiptClicked(Lcom/squareup/locale/LocaleOverrideFactory;)V
.end method

.method abstract printReceiptClicked(Lcom/squareup/locale/LocaleOverrideFactory;)V
.end method

.method public processEmvCardInserted(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    .line 394
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->updateMessagesForSmartCard(Lcom/squareup/cardreader/CardReaderId;)V

    return-void
.end method

.method public processEmvCardRemoved(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    .line 398
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->updateMessagesForSmartCard(Lcom/squareup/cardreader/CardReaderId;)V

    return-void
.end method

.method protected receiptSelectionMade()Z
    .locals 1

    .line 596
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->isReceiptSelectionMade()Z

    move-result v0

    return v0
.end method

.method abstract sendEmailReceiptClicked(Lcom/squareup/locale/LocaleOverrideFactory;)V
.end method

.method abstract sendSmsReceiptClicked(Lcom/squareup/locale/LocaleOverrideFactory;)V
.end method

.method protected shouldRemoveCard()Z
    .locals 3

    .line 585
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->smartCardReaderId:Lcom/squareup/cardreader/CardReaderId;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 588
    :cond_0
    iget-object v2, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {v2, v0}, Lcom/squareup/cardreader/CardReaderHub;->getCardReader(Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/cardreader/CardReader;

    move-result-object v0

    if-nez v0, :cond_1

    return v1

    .line 592
    :cond_1
    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->isCardPresent()Z

    move-result v0

    return v0
.end method

.method protected shouldShowEmailSent()Z
    .locals 1

    .line 573
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->shouldAutoSendReceipt()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->useDefaultEmailAddress()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method protected shouldSkipReceipt()Z
    .locals 1

    .line 632
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->buyerFlowReceiptManager:Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;

    invoke-interface {v0}, Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;->shouldReceiptScreenSkipSelection()Z

    move-result v0

    return v0
.end method

.method abstract showAllDone(Lcom/squareup/locale/LocaleOverrideFactory;)V
.end method

.method protected showEmailAndSmsRows(Lcom/squareup/util/Res;)V
    .locals 4

    .line 601
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter$AbstractReceiptView;

    .line 602
    invoke-direct {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getDefaultEmail()Ljava/lang/String;

    move-result-object v1

    .line 604
    invoke-direct {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getContactSmsNumber()Ljava/lang/String;

    move-result-object v2

    .line 605
    invoke-direct {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getContactEmailAddress()Ljava/lang/String;

    move-result-object v3

    if-eqz v1, :cond_0

    .line 608
    invoke-interface {v0, v1}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter$AbstractReceiptView;->setEmailInputHint(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    if-eqz v3, :cond_1

    .line 610
    invoke-interface {v0, v3}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter$AbstractReceiptView;->setEmailInputHint(Ljava/lang/String;)V

    goto :goto_0

    .line 612
    :cond_1
    invoke-interface {v0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter$AbstractReceiptView;->updateEmail(Lcom/squareup/util/Res;)V

    .line 614
    :goto_0
    invoke-direct {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getDefaultSmsNumber()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 616
    invoke-interface {v0, v1}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter$AbstractReceiptView;->setSmsInputHint(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    if-eqz v2, :cond_3

    .line 618
    invoke-interface {v0, v2}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter$AbstractReceiptView;->setSmsInputHint(Ljava/lang/String;)V

    goto :goto_1

    .line 620
    :cond_3
    invoke-interface {v0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter$AbstractReceiptView;->updateSms(Lcom/squareup/util/Res;)V

    .line 624
    :goto_1
    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {p1}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/settings/server/PaymentSettings;->supportsSms()Z

    move-result p1

    if-eqz p1, :cond_4

    .line 625
    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->localeOverride:Lcom/squareup/locale/LocaleOverrideFactory;

    invoke-interface {v0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter$AbstractReceiptView;->showSmsInput(Lcom/squareup/locale/LocaleOverrideFactory;)V

    .line 627
    :cond_4
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->updateEmailEnabledStates()V

    .line 628
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->updateSmsEnabledStates()V

    return-void
.end method

.method abstract showReceiptSection()V
.end method

.method abstract showTenderAmountAndTip(Lcom/squareup/locale/LocaleOverrideFactory;)V
.end method

.method public switchLanguageClicked()V
    .locals 1

    .line 764
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->showSelectBuyerLanguageScreen()V

    return-void
.end method

.method tryDeclineReceipt()Z
    .locals 2

    .line 482
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->isReceiptSelectionMade()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 483
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->receiptSender:Lcom/squareup/receipt/ReceiptSender;

    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/receipt/ReceiptSender;->receiptDeclined(Lcom/squareup/payment/PaymentReceipt;)V

    const/4 v0, 0x1

    return v0
.end method

.method tryPrintReceipt(Lcom/squareup/print/payload/ReceiptPayload$RenderType;)Z
    .locals 2

    .line 472
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->isReceiptSelectionMade()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 476
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->buyerFlowReceiptManager:Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;

    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;->maybePrintReceipt(Lcom/squareup/payment/PaymentReceipt;Lcom/squareup/print/payload/ReceiptPayload$RenderType;)Lio/reactivex/Observable;

    move-result-object p1

    invoke-virtual {p1}, Lio/reactivex/Observable;->subscribe()Lio/reactivex/disposables/Disposable;

    const/4 p1, 0x1

    return p1
.end method

.method trySendEmailReceipt()Z
    .locals 5

    .line 436
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->isReceiptSelectionMade()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 439
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->receiptSender:Lcom/squareup/receipt/ReceiptSender;

    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v2

    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getValidEmail()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Lcom/squareup/receipt/ReceiptSender;->trySendEmailReceipt(Lcom/squareup/payment/PaymentReceipt;Ljava/lang/String;)Z

    move-result v0

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    return v2

    .line 442
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->receiptSender:Lcom/squareup/receipt/ReceiptSender;

    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v3

    invoke-direct {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getContactEmailAddress()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Lcom/squareup/receipt/ReceiptSender;->trySendEmailReceipt(Lcom/squareup/payment/PaymentReceipt;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    return v2

    .line 445
    :cond_2
    invoke-direct {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->useDefaultEmailAddress()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 446
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->receiptSender:Lcom/squareup/receipt/ReceiptSender;

    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/receipt/ReceiptSender;->sendEmailReceiptToDefault(Lcom/squareup/payment/PaymentReceipt;)V

    return v2

    :cond_3
    return v1
.end method

.method trySendSmsReceipt()Z
    .locals 5

    .line 454
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->isReceiptSelectionMade()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 457
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->receiptSender:Lcom/squareup/receipt/ReceiptSender;

    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v2

    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getValidSmsNumber()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Lcom/squareup/receipt/ReceiptSender;->trySendSmsReceipt(Lcom/squareup/payment/PaymentReceipt;Ljava/lang/String;)Z

    move-result v0

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    return v2

    .line 460
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->receiptSender:Lcom/squareup/receipt/ReceiptSender;

    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v3

    invoke-direct {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getContactSmsNumber()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Lcom/squareup/receipt/ReceiptSender;->trySendSmsReceipt(Lcom/squareup/payment/PaymentReceipt;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    return v2

    .line 463
    :cond_2
    invoke-direct {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->useDefaultSmsNumber()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 464
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->receiptSender:Lcom/squareup/receipt/ReceiptSender;

    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/receipt/ReceiptSender;->sendSmsReceiptToDefault(Lcom/squareup/payment/PaymentReceipt;)V

    return v2

    :cond_3
    return v1
.end method

.method updateEmailEnabledStates()V
    .locals 2

    .line 488
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter$AbstractReceiptView;

    if-eqz v0, :cond_2

    .line 491
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->useValidEmailAddress()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-direct {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->useDefaultEmailAddress()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->useContactEmailAddress()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    .line 490
    :goto_1
    invoke-interface {v0, v1}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter$AbstractReceiptView;->enableSendEmailButton(Z)V

    .line 493
    :cond_2
    invoke-direct {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->restartScreenTimeout()V

    return-void
.end method

.method abstract updateMessages(Lcom/squareup/locale/LocaleOverrideFactory;)V
.end method

.method updateSmsEnabledStates()V
    .locals 2

    .line 497
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter$AbstractReceiptView;

    if-eqz v0, :cond_2

    .line 500
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->useValidSmsNumber()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-direct {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->useDefaultSmsNumber()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->useContactSmsNumber()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    .line 499
    :goto_1
    invoke-interface {v0, v1}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter$AbstractReceiptView;->enableSendSmsButton(Z)V

    .line 502
    :cond_2
    invoke-direct {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->restartScreenTimeout()V

    return-void
.end method

.method abstract updateStrings(Lcom/squareup/locale/LocaleOverrideFactory;)V
.end method

.method useContactEmailAddress()Z
    .locals 1

    .line 680
    invoke-direct {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getTrimmedEmail()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getContactEmailAddress()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method useContactSmsNumber()Z
    .locals 1

    .line 684
    invoke-direct {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getTrimmedSms()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getContactSmsNumber()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method useValidEmailAddress()Z
    .locals 1

    .line 672
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getValidEmail()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method useValidSmsNumber()Z
    .locals 1

    .line 676
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter;->getValidSmsNumber()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
