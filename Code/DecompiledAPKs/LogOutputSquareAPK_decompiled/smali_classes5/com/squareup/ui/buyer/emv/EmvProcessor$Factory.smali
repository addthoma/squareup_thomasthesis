.class public Lcom/squareup/ui/buyer/emv/EmvProcessor$Factory;
.super Ljava/lang/Object;
.source "EmvProcessor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/emv/EmvProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# instance fields
.field private final activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

.field private final buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

.field private final cardReader:Lcom/squareup/cardreader/CardReader;

.field private final cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

.field private final cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

.field private final cardReaderPowerMonitor:Lcom/squareup/cardreader/CardReaderPowerMonitor;

.field private final dippedCardTracker:Lcom/squareup/cardreader/DippedCardTracker;

.field private final emvPaymentStarter:Lcom/squareup/ui/buyer/emv/EmvPaymentStarter;

.field private final emvSwipePassthroughEnabler:Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;

.field private final hudToaster:Lcom/squareup/hudtoaster/HudToaster;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

.field private final readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

.field private final secureTouchFeature:Lcom/squareup/securetouch/SecureTouchFeature;

.field private final tenderInEdit:Lcom/squareup/payment/TenderInEdit;

.field private final tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method public constructor <init>(Lcom/squareup/log/ReaderEventLogger;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/cardreader/CardReader;Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/cardreader/CardReaderPowerMonitor;Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;Lcom/squareup/payment/Transaction;Lcom/squareup/hudtoaster/HudToaster;Lio/reactivex/Scheduler;Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/ui/buyer/emv/EmvPaymentStarter;Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/securetouch/SecureTouchFeature;)V
    .locals 2
    .param p10    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    .line 947
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 948
    iput-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvProcessor$Factory;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    move-object v1, p2

    .line 949
    iput-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvProcessor$Factory;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    move-object v1, p3

    .line 950
    iput-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvProcessor$Factory;->cardReader:Lcom/squareup/cardreader/CardReader;

    move-object v1, p4

    .line 951
    iput-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvProcessor$Factory;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    move-object v1, p5

    .line 952
    iput-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvProcessor$Factory;->cardReaderPowerMonitor:Lcom/squareup/cardreader/CardReaderPowerMonitor;

    move-object v1, p6

    .line 953
    iput-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvProcessor$Factory;->dippedCardTracker:Lcom/squareup/cardreader/DippedCardTracker;

    move-object v1, p7

    .line 954
    iput-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvProcessor$Factory;->emvSwipePassthroughEnabler:Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;

    move-object v1, p8

    .line 955
    iput-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvProcessor$Factory;->transaction:Lcom/squareup/payment/Transaction;

    move-object v1, p9

    .line 956
    iput-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvProcessor$Factory;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    move-object v1, p10

    .line 957
    iput-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvProcessor$Factory;->mainScheduler:Lio/reactivex/Scheduler;

    move-object v1, p11

    .line 958
    iput-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvProcessor$Factory;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    move-object v1, p12

    .line 959
    iput-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvProcessor$Factory;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    move-object v1, p13

    .line 960
    iput-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvProcessor$Factory;->tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

    move-object/from16 v1, p14

    .line 961
    iput-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvProcessor$Factory;->emvPaymentStarter:Lcom/squareup/ui/buyer/emv/EmvPaymentStarter;

    move-object/from16 v1, p15

    .line 962
    iput-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvProcessor$Factory;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    move-object/from16 v1, p16

    .line 963
    iput-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvProcessor$Factory;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    move-object/from16 v1, p17

    .line 964
    iput-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvProcessor$Factory;->secureTouchFeature:Lcom/squareup/securetouch/SecureTouchFeature;

    return-void
.end method


# virtual methods
.method public create(Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;)Lcom/squareup/ui/buyer/emv/EmvProcessor;
    .locals 22

    move-object/from16 v0, p0

    .line 968
    new-instance v20, Lcom/squareup/ui/buyer/emv/EmvProcessor;

    move-object/from16 v1, v20

    iget-object v2, v0, Lcom/squareup/ui/buyer/emv/EmvProcessor$Factory;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    iget-object v3, v0, Lcom/squareup/ui/buyer/emv/EmvProcessor$Factory;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    iget-object v4, v0, Lcom/squareup/ui/buyer/emv/EmvProcessor$Factory;->cardReader:Lcom/squareup/cardreader/CardReader;

    iget-object v5, v0, Lcom/squareup/ui/buyer/emv/EmvProcessor$Factory;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    iget-object v6, v0, Lcom/squareup/ui/buyer/emv/EmvProcessor$Factory;->cardReaderPowerMonitor:Lcom/squareup/cardreader/CardReaderPowerMonitor;

    iget-object v7, v0, Lcom/squareup/ui/buyer/emv/EmvProcessor$Factory;->dippedCardTracker:Lcom/squareup/cardreader/DippedCardTracker;

    iget-object v8, v0, Lcom/squareup/ui/buyer/emv/EmvProcessor$Factory;->emvSwipePassthroughEnabler:Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;

    iget-object v9, v0, Lcom/squareup/ui/buyer/emv/EmvProcessor$Factory;->transaction:Lcom/squareup/payment/Transaction;

    iget-object v10, v0, Lcom/squareup/ui/buyer/emv/EmvProcessor$Factory;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    iget-object v11, v0, Lcom/squareup/ui/buyer/emv/EmvProcessor$Factory;->mainScheduler:Lio/reactivex/Scheduler;

    const-string v12, "listener"

    move-object/from16 v13, p1

    .line 970
    invoke-static {v13, v12}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;

    iget-object v13, v0, Lcom/squareup/ui/buyer/emv/EmvProcessor$Factory;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    iget-object v14, v0, Lcom/squareup/ui/buyer/emv/EmvProcessor$Factory;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    iget-object v15, v0, Lcom/squareup/ui/buyer/emv/EmvProcessor$Factory;->tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

    move-object/from16 v21, v1

    iget-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvProcessor$Factory;->emvPaymentStarter:Lcom/squareup/ui/buyer/emv/EmvPaymentStarter;

    move-object/from16 v16, v1

    iget-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvProcessor$Factory;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    move-object/from16 v17, v1

    iget-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvProcessor$Factory;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    move-object/from16 v18, v1

    iget-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvProcessor$Factory;->secureTouchFeature:Lcom/squareup/securetouch/SecureTouchFeature;

    move-object/from16 v19, v1

    move-object/from16 v1, v21

    invoke-direct/range {v1 .. v19}, Lcom/squareup/ui/buyer/emv/EmvProcessor;-><init>(Lcom/squareup/log/ReaderEventLogger;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/cardreader/CardReader;Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/cardreader/CardReaderPowerMonitor;Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;Lcom/squareup/payment/Transaction;Lcom/squareup/hudtoaster/HudToaster;Lio/reactivex/Scheduler;Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/ui/buyer/emv/EmvPaymentStarter;Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/securetouch/SecureTouchFeature;)V

    return-object v20
.end method
