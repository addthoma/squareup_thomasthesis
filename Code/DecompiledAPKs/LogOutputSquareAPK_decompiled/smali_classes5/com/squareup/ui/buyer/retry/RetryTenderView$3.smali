.class Lcom/squareup/ui/buyer/retry/RetryTenderView$3;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "RetryTenderView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/buyer/retry/RetryTenderView;->setOfflineButtonToShowQuickEnableCardOnClick()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/buyer/retry/RetryTenderView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/buyer/retry/RetryTenderView;)V
    .locals 0

    .line 146
    iput-object p1, p0, Lcom/squareup/ui/buyer/retry/RetryTenderView$3;->this$0:Lcom/squareup/ui/buyer/retry/RetryTenderView;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 0

    .line 148
    iget-object p1, p0, Lcom/squareup/ui/buyer/retry/RetryTenderView$3;->this$0:Lcom/squareup/ui/buyer/retry/RetryTenderView;

    iget-object p1, p1, Lcom/squareup/ui/buyer/retry/RetryTenderView;->presenter:Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/retry/RetryTenderPresenter;->onShowOfflineEnableCardClicked()V

    return-void
.end method
