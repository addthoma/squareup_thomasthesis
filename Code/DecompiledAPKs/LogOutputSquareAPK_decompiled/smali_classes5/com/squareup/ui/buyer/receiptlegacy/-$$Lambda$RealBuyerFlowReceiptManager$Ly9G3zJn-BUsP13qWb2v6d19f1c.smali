.class public final synthetic Lcom/squareup/ui/buyer/receiptlegacy/-$$Lambda$RealBuyerFlowReceiptManager$Ly9G3zJn-BUsP13qWb2v6d19f1c;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lio/reactivex/functions/Function;


# instance fields
.field private final synthetic f$0:Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager;

.field private final synthetic f$1:Lcom/squareup/print/payload/ReceiptPayload$RenderType;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager;Lcom/squareup/print/payload/ReceiptPayload$RenderType;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/-$$Lambda$RealBuyerFlowReceiptManager$Ly9G3zJn-BUsP13qWb2v6d19f1c;->f$0:Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager;

    iput-object p2, p0, Lcom/squareup/ui/buyer/receiptlegacy/-$$Lambda$RealBuyerFlowReceiptManager$Ly9G3zJn-BUsP13qWb2v6d19f1c;->f$1:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/-$$Lambda$RealBuyerFlowReceiptManager$Ly9G3zJn-BUsP13qWb2v6d19f1c;->f$0:Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager;

    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/-$$Lambda$RealBuyerFlowReceiptManager$Ly9G3zJn-BUsP13qWb2v6d19f1c;->f$1:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    check-cast p1, Lcom/squareup/payment/PaymentReceipt;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager;->lambda$maybePrintReceipt$0$RealBuyerFlowReceiptManager(Lcom/squareup/print/payload/ReceiptPayload$RenderType;Lcom/squareup/payment/PaymentReceipt;)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method
