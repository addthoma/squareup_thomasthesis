.class public Lcom/squareup/ui/cardreader/BatteryLevelToaster;
.super Ljava/lang/Object;
.source "BatteryLevelToaster.java"


# instance fields
.field private final application:Landroid/app/Application;

.field private final hudToaster:Lcom/squareup/hudtoaster/HudToaster;


# direct methods
.method public constructor <init>(Landroid/app/Application;Lcom/squareup/hudtoaster/HudToaster;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/squareup/ui/cardreader/BatteryLevelToaster;->application:Landroid/app/Application;

    .line 35
    iput-object p2, p0, Lcom/squareup/ui/cardreader/BatteryLevelToaster;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    return-void
.end method

.method private spanContent(Landroid/widget/TextView;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 1

    .line 52
    invoke-virtual {p1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Lcom/squareup/ui/cardreader/BatteryLevelToaster;->spanText(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private spanText(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/text/SpannableString;
    .locals 2

    if-eqz p3, :cond_0

    .line 60
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/cardreader/ui/R$string;->battery_level_hud_content:I

    invoke-static {v0, v1}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string/jumbo v1, "title"

    .line 61
    invoke-virtual {v0, v1, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v1, "message"

    .line 62
    invoke-virtual {v0, v1, p3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p3

    .line 63
    invoke-virtual {p3}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p3

    goto :goto_0

    :cond_0
    move-object p3, p2

    .line 67
    :goto_0
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p3}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 68
    sget-object p3, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-static {p1, p3}, Lcom/squareup/marketfont/MarketSpanKt;->marketSpanFor(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;)Lcom/squareup/fonts/FontSpan;

    move-result-object p1

    invoke-static {v0, p2, p1}, Lcom/squareup/text/Spannables;->spanFirst(Landroid/text/Spannable;Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    return-object v0
.end method


# virtual methods
.method public toast(Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)Z
    .locals 3

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/cardreader/BatteryLevelToaster;->application:Landroid/app/Application;

    sget v1, Lcom/squareup/cardreader/ui/R$layout;->reader_battery_hud:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 41
    sget v1, Lcom/squareup/cardreader/ui/R$id;->battery_message:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 42
    invoke-direct {p0, v1, p1, p2}, Lcom/squareup/ui/cardreader/BatteryLevelToaster;->spanContent(Landroid/widget/TextView;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 43
    sget p1, Lcom/squareup/cardreader/ui/R$id;->battery_image:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    .line 45
    invoke-virtual {p1}, Landroid/widget/ImageView;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    .line 46
    invoke-virtual {p1}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    .line 45
    invoke-static {p2, p3, v1}, Landroidx/vectordrawable/graphics/drawable/VectorDrawableCompat;->create(Landroid/content/res/Resources;ILandroid/content/res/Resources$Theme;)Landroidx/vectordrawable/graphics/drawable/VectorDrawableCompat;

    move-result-object p2

    .line 47
    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 48
    iget-object p1, p0, Lcom/squareup/ui/cardreader/BatteryLevelToaster;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    const/4 p2, 0x0

    invoke-interface {p1, v0, p2}, Lcom/squareup/hudtoaster/HudToaster;->doToast(Landroid/view/View;I)Z

    move-result p1

    return p1
.end method
