.class public abstract Lcom/squareup/ui/ReorderableRecyclerViewAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "ReorderableRecyclerViewAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/ReorderableRecyclerViewAdapter$ItemTouchHelperCallback;,
        Lcom/squareup/ui/ReorderableRecyclerViewAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<VH:",
        "Lcom/squareup/ui/ReorderableRecyclerViewAdapter$ViewHolder;",
        "T:",
        "Ljava/lang/Object;",
        ">",
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "TVH;>;"
    }
.end annotation


# instance fields
.field protected draggableItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation
.end field

.field private isDragging:Z

.field private final itemTouchHelper:Landroidx/recyclerview/widget/ItemTouchHelper;

.field private onDragStateChanged:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>()V
    .locals 3

    .line 120
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    .line 121
    invoke-static {}, Lrx/subjects/PublishSubject;->create()Lrx/subjects/PublishSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/ReorderableRecyclerViewAdapter;->onDragStateChanged:Lrx/subjects/PublishSubject;

    .line 122
    iget-object v0, p0, Lcom/squareup/ui/ReorderableRecyclerViewAdapter;->onDragStateChanged:Lrx/subjects/PublishSubject;

    new-instance v1, Lcom/squareup/ui/-$$Lambda$ReorderableRecyclerViewAdapter$8MGlNCjtPQntgoc02jQXT_VxCB0;

    invoke-direct {v1, p0}, Lcom/squareup/ui/-$$Lambda$ReorderableRecyclerViewAdapter$8MGlNCjtPQntgoc02jQXT_VxCB0;-><init>(Lcom/squareup/ui/ReorderableRecyclerViewAdapter;)V

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    .line 123
    new-instance v0, Landroidx/recyclerview/widget/ItemTouchHelper;

    new-instance v1, Lcom/squareup/ui/ReorderableRecyclerViewAdapter$ItemTouchHelperCallback;

    iget-object v2, p0, Lcom/squareup/ui/ReorderableRecyclerViewAdapter;->onDragStateChanged:Lrx/subjects/PublishSubject;

    invoke-direct {v1, v2}, Lcom/squareup/ui/ReorderableRecyclerViewAdapter$ItemTouchHelperCallback;-><init>(Lrx/subjects/PublishSubject;)V

    invoke-direct {v0, v1}, Landroidx/recyclerview/widget/ItemTouchHelper;-><init>(Landroidx/recyclerview/widget/ItemTouchHelper$Callback;)V

    iput-object v0, p0, Lcom/squareup/ui/ReorderableRecyclerViewAdapter;->itemTouchHelper:Landroidx/recyclerview/widget/ItemTouchHelper;

    const/4 v0, 0x1

    .line 124
    invoke-virtual {p0, v0}, Lcom/squareup/ui/ReorderableRecyclerViewAdapter;->setHasStableIds(Z)V

    return-void
.end method

.method constructor <init>(Landroidx/recyclerview/widget/ItemTouchHelper;)V
    .locals 0

    .line 127
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    .line 128
    iput-object p1, p0, Lcom/squareup/ui/ReorderableRecyclerViewAdapter;->itemTouchHelper:Landroidx/recyclerview/widget/ItemTouchHelper;

    const/4 p1, 0x1

    .line 129
    invoke-virtual {p0, p1}, Lcom/squareup/ui/ReorderableRecyclerViewAdapter;->setHasStableIds(Z)V

    return-void
.end method

.method private isStaticRow(I)Z
    .locals 2

    .line 262
    invoke-virtual {p0}, Lcom/squareup/ui/ReorderableRecyclerViewAdapter;->getStaticTopRowsCount()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 263
    invoke-virtual {p0}, Lcom/squareup/ui/ReorderableRecyclerViewAdapter;->getDraggableItemCount()I

    move-result v0

    invoke-virtual {p0}, Lcom/squareup/ui/ReorderableRecyclerViewAdapter;->getStaticTopRowsCount()I

    move-result v1

    add-int/2addr v0, v1

    if-lt p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method


# virtual methods
.method public getDraggableItemAtPosition(I)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .line 227
    invoke-direct {p0, p1}, Lcom/squareup/ui/ReorderableRecyclerViewAdapter;->isStaticRow(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 230
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/ReorderableRecyclerViewAdapter;->draggableItems:Ljava/util/List;

    invoke-virtual {p0}, Lcom/squareup/ui/ReorderableRecyclerViewAdapter;->getStaticTopRowsCount()I

    move-result v1

    sub-int/2addr p1, v1

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public getDraggableItemCount()I
    .locals 1

    .line 223
    iget-object v0, p0, Lcom/squareup/ui/ReorderableRecyclerViewAdapter;->draggableItems:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    return v0
.end method

.method public abstract getDraggableItemId(I)J
.end method

.method public getDraggableItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation

    .line 219
    iget-object v0, p0, Lcom/squareup/ui/ReorderableRecyclerViewAdapter;->draggableItems:Ljava/util/List;

    return-object v0
.end method

.method public getItemCount()I
    .locals 2

    .line 203
    invoke-virtual {p0}, Lcom/squareup/ui/ReorderableRecyclerViewAdapter;->getDraggableItemCount()I

    move-result v0

    invoke-virtual {p0}, Lcom/squareup/ui/ReorderableRecyclerViewAdapter;->getStaticTopRowsCount()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/squareup/ui/ReorderableRecyclerViewAdapter;->getStaticBottomRowsCount()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public getItemId(I)J
    .locals 2

    .line 207
    invoke-direct {p0, p1}, Lcom/squareup/ui/ReorderableRecyclerViewAdapter;->isStaticRow(I)Z

    move-result v0

    if-eqz v0, :cond_0

    int-to-long v0, p1

    return-wide v0

    .line 210
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/ReorderableRecyclerViewAdapter;->getStaticTopRowsCount()I

    move-result v0

    sub-int/2addr p1, v0

    invoke-virtual {p0, p1}, Lcom/squareup/ui/ReorderableRecyclerViewAdapter;->getDraggableItemId(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public abstract getStaticBottomRowsCount()I
.end method

.method public abstract getStaticTopRowsCount()I
.end method

.method public isDragging()Z
    .locals 1

    .line 215
    iget-boolean v0, p0, Lcom/squareup/ui/ReorderableRecyclerViewAdapter;->isDragging:Z

    return v0
.end method

.method public synthetic lambda$new$0$ReorderableRecyclerViewAdapter(Ljava/lang/Boolean;)V
    .locals 0

    .line 122
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/ui/ReorderableRecyclerViewAdapter;->isDragging:Z

    return-void
.end method

.method public onAttachedToRecyclerView(Landroidx/recyclerview/widget/RecyclerView;)V
    .locals 1

    .line 198
    invoke-super {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->onAttachedToRecyclerView(Landroidx/recyclerview/widget/RecyclerView;)V

    .line 199
    iget-object v0, p0, Lcom/squareup/ui/ReorderableRecyclerViewAdapter;->itemTouchHelper:Landroidx/recyclerview/widget/ItemTouchHelper;

    invoke-virtual {v0, p1}, Landroidx/recyclerview/widget/ItemTouchHelper;->attachToRecyclerView(Landroidx/recyclerview/widget/RecyclerView;)V

    return-void
.end method

.method public abstract onCreateReorderableViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/ReorderableRecyclerViewAdapter$ViewHolder;
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    .line 24
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/ReorderableRecyclerViewAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/ReorderableRecyclerViewAdapter$ViewHolder;

    move-result-object p1

    return-object p1
.end method

.method public final onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/ReorderableRecyclerViewAdapter$ViewHolder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "I)TVH;"
        }
    .end annotation

    .line 181
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/ReorderableRecyclerViewAdapter;->onCreateReorderableViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/ReorderableRecyclerViewAdapter$ViewHolder;

    move-result-object p1

    .line 182
    invoke-virtual {p1}, Lcom/squareup/ui/ReorderableRecyclerViewAdapter$ViewHolder;->getDragHandle()Landroid/view/View;

    move-result-object p2

    if-eqz p2, :cond_0

    .line 184
    new-instance v0, Lcom/squareup/ui/ReorderableRecyclerViewAdapter$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/ReorderableRecyclerViewAdapter$1;-><init>(Lcom/squareup/ui/ReorderableRecyclerViewAdapter;Lcom/squareup/ui/ReorderableRecyclerViewAdapter$ViewHolder;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    :cond_0
    return-object p1
.end method

.method public onDragStateChanged()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 148
    iget-object v0, p0, Lcom/squareup/ui/ReorderableRecyclerViewAdapter;->onDragStateChanged:Lrx/subjects/PublishSubject;

    return-object v0
.end method

.method reorderDraggableItems(II)Z
    .locals 4

    .line 239
    invoke-virtual {p0}, Lcom/squareup/ui/ReorderableRecyclerViewAdapter;->getStaticTopRowsCount()I

    move-result v0

    .line 240
    invoke-virtual {p0}, Lcom/squareup/ui/ReorderableRecyclerViewAdapter;->getStaticTopRowsCount()I

    move-result v1

    iget-object v2, p0, Lcom/squareup/ui/ReorderableRecyclerViewAdapter;->draggableItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/2addr v1, v2

    const/4 v2, 0x1

    sub-int/2addr v1, v2

    if-lt p1, v0, :cond_3

    if-lt p2, v0, :cond_3

    if-gt p1, v1, :cond_3

    if-le p2, v1, :cond_0

    goto :goto_2

    :cond_0
    if-ge p1, p2, :cond_1

    sub-int/2addr p1, v0

    :goto_0
    sub-int v1, p2, v0

    if-ge p1, v1, :cond_2

    .line 248
    iget-object v1, p0, Lcom/squareup/ui/ReorderableRecyclerViewAdapter;->draggableItems:Ljava/util/List;

    add-int/lit8 v3, p1, 0x1

    invoke-static {v1, p1, v3}, Ljava/util/Collections;->swap(Ljava/util/List;II)V

    add-int/2addr p1, v0

    add-int v1, v3, v0

    .line 249
    invoke-virtual {p0, p1, v1}, Lcom/squareup/ui/ReorderableRecyclerViewAdapter;->notifyItemMoved(II)V

    move p1, v3

    goto :goto_0

    :cond_1
    sub-int/2addr p1, v0

    :goto_1
    sub-int v1, p2, v0

    if-le p1, v1, :cond_2

    .line 253
    iget-object v1, p0, Lcom/squareup/ui/ReorderableRecyclerViewAdapter;->draggableItems:Ljava/util/List;

    add-int/lit8 v3, p1, -0x1

    invoke-static {v1, p1, v3}, Ljava/util/Collections;->swap(Ljava/util/List;II)V

    add-int v1, p1, v0

    add-int/2addr v3, v0

    .line 254
    invoke-virtual {p0, v1, v3}, Lcom/squareup/ui/ReorderableRecyclerViewAdapter;->notifyItemMoved(II)V

    add-int/lit8 p1, p1, -0x1

    goto :goto_1

    :cond_2
    return v2

    :cond_3
    :goto_2
    const/4 p1, 0x0

    return p1
.end method

.method public setDraggableItems(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "TT;>;)V"
        }
    .end annotation

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 138
    :goto_0
    invoke-static {v0}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    .line 139
    iput-object p1, p0, Lcom/squareup/ui/ReorderableRecyclerViewAdapter;->draggableItems:Ljava/util/List;

    .line 140
    invoke-virtual {p0}, Lcom/squareup/ui/ReorderableRecyclerViewAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method protected startDrag(Lcom/squareup/ui/ReorderableRecyclerViewAdapter$ViewHolder;)V
    .locals 1

    .line 235
    iget-object v0, p0, Lcom/squareup/ui/ReorderableRecyclerViewAdapter;->itemTouchHelper:Landroidx/recyclerview/widget/ItemTouchHelper;

    invoke-virtual {v0, p1}, Landroidx/recyclerview/widget/ItemTouchHelper;->startDrag(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V

    return-void
.end method
