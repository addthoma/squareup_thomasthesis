.class Lcom/squareup/ui/addressbook/AddressBookLoader$AndroidContact;
.super Ljava/lang/Object;
.source "AddressBookLoader.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/addressbook/AddressBookLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "AndroidContact"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable<",
        "Lcom/squareup/ui/addressbook/AddressBookLoader$AndroidContact;",
        ">;"
    }
.end annotation


# instance fields
.field final email:Ljava/lang/String;

.field final id:Ljava/lang/String;

.field final name:Ljava/lang/String;

.field final phone:Ljava/lang/String;

.field final photo:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    iput-object p1, p0, Lcom/squareup/ui/addressbook/AddressBookLoader$AndroidContact;->id:Ljava/lang/String;

    .line 87
    iput-object p2, p0, Lcom/squareup/ui/addressbook/AddressBookLoader$AndroidContact;->name:Ljava/lang/String;

    .line 88
    iput-object p3, p0, Lcom/squareup/ui/addressbook/AddressBookLoader$AndroidContact;->photo:Ljava/lang/String;

    .line 89
    iput-object p4, p0, Lcom/squareup/ui/addressbook/AddressBookLoader$AndroidContact;->email:Ljava/lang/String;

    .line 90
    iput-object p5, p0, Lcom/squareup/ui/addressbook/AddressBookLoader$AndroidContact;->phone:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public compareTo(Lcom/squareup/ui/addressbook/AddressBookLoader$AndroidContact;)I
    .locals 2

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/addressbook/AddressBookLoader$AndroidContact;->name:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/ui/addressbook/AddressBookLoader$AndroidContact;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/addressbook/AddressBookLoader$AndroidContact;->id:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/ui/addressbook/AddressBookLoader$AndroidContact;->id:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    :cond_0
    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 76
    check-cast p1, Lcom/squareup/ui/addressbook/AddressBookLoader$AndroidContact;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/addressbook/AddressBookLoader$AndroidContact;->compareTo(Lcom/squareup/ui/addressbook/AddressBookLoader$AndroidContact;)I

    move-result p1

    return p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .line 99
    instance-of v0, p1, Lcom/squareup/ui/addressbook/AddressBookLoader$AndroidContact;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 100
    check-cast p1, Lcom/squareup/ui/addressbook/AddressBookLoader$AndroidContact;

    .line 101
    iget-object v0, p0, Lcom/squareup/ui/addressbook/AddressBookLoader$AndroidContact;->id:Ljava/lang/String;

    iget-object v2, p1, Lcom/squareup/ui/addressbook/AddressBookLoader$AndroidContact;->id:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/addressbook/AddressBookLoader$AndroidContact;->name:Ljava/lang/String;

    iget-object v2, p1, Lcom/squareup/ui/addressbook/AddressBookLoader$AndroidContact;->name:Ljava/lang/String;

    .line 102
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/addressbook/AddressBookLoader$AndroidContact;->photo:Ljava/lang/String;

    iget-object v2, p1, Lcom/squareup/ui/addressbook/AddressBookLoader$AndroidContact;->photo:Ljava/lang/String;

    .line 103
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/addressbook/AddressBookLoader$AndroidContact;->email:Ljava/lang/String;

    iget-object v2, p1, Lcom/squareup/ui/addressbook/AddressBookLoader$AndroidContact;->email:Ljava/lang/String;

    .line 104
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/addressbook/AddressBookLoader$AndroidContact;->phone:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/ui/addressbook/AddressBookLoader$AndroidContact;->phone:Ljava/lang/String;

    .line 105
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    .line 94
    iget-object v1, p0, Lcom/squareup/ui/addressbook/AddressBookLoader$AndroidContact;->id:Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/addressbook/AddressBookLoader$AndroidContact;->name:Ljava/lang/String;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/addressbook/AddressBookLoader$AndroidContact;->photo:Ljava/lang/String;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/addressbook/AddressBookLoader$AndroidContact;->email:Ljava/lang/String;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/addressbook/AddressBookLoader$AndroidContact;->phone:Ljava/lang/String;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    const-string v1, "Id: %1$s, Name: %2$s, Photo: %3$s, Email: %4$s, Phone: %5$s"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
