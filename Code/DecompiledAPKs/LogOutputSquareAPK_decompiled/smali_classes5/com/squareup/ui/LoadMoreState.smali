.class public final enum Lcom/squareup/ui/LoadMoreState;
.super Ljava/lang/Enum;
.source "LoadMoreState.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/LoadMoreState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/LoadMoreState;

.field public static final enum LOADING:Lcom/squareup/ui/LoadMoreState;

.field public static final enum NO_CONNECTION_RETRY:Lcom/squareup/ui/LoadMoreState;

.field public static final enum NO_MORE:Lcom/squareup/ui/LoadMoreState;


# instance fields
.field public final showsRow:Z


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 9
    new-instance v0, Lcom/squareup/ui/LoadMoreState;

    const/4 v1, 0x0

    const-string v2, "NO_MORE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/ui/LoadMoreState;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/squareup/ui/LoadMoreState;->NO_MORE:Lcom/squareup/ui/LoadMoreState;

    .line 12
    new-instance v0, Lcom/squareup/ui/LoadMoreState;

    const/4 v2, 0x1

    const-string v3, "LOADING"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/ui/LoadMoreState;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/squareup/ui/LoadMoreState;->LOADING:Lcom/squareup/ui/LoadMoreState;

    .line 15
    new-instance v0, Lcom/squareup/ui/LoadMoreState;

    const/4 v3, 0x2

    const-string v4, "NO_CONNECTION_RETRY"

    invoke-direct {v0, v4, v3, v2}, Lcom/squareup/ui/LoadMoreState;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/squareup/ui/LoadMoreState;->NO_CONNECTION_RETRY:Lcom/squareup/ui/LoadMoreState;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/ui/LoadMoreState;

    .line 7
    sget-object v4, Lcom/squareup/ui/LoadMoreState;->NO_MORE:Lcom/squareup/ui/LoadMoreState;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/ui/LoadMoreState;->LOADING:Lcom/squareup/ui/LoadMoreState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/LoadMoreState;->NO_CONNECTION_RETRY:Lcom/squareup/ui/LoadMoreState;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/ui/LoadMoreState;->$VALUES:[Lcom/squareup/ui/LoadMoreState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 20
    iput-boolean p3, p0, Lcom/squareup/ui/LoadMoreState;->showsRow:Z

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/LoadMoreState;
    .locals 1

    .line 7
    const-class v0, Lcom/squareup/ui/LoadMoreState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/LoadMoreState;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/LoadMoreState;
    .locals 1

    .line 7
    sget-object v0, Lcom/squareup/ui/LoadMoreState;->$VALUES:[Lcom/squareup/ui/LoadMoreState;

    invoke-virtual {v0}, [Lcom/squareup/ui/LoadMoreState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/LoadMoreState;

    return-object v0
.end method
