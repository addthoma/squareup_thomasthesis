.class Lcom/squareup/ui/BaseXableEditText$2;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "BaseXableEditText.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/BaseXableEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/BaseXableEditText;


# direct methods
.method constructor <init>(Lcom/squareup/ui/BaseXableEditText;)V
    .locals 0

    .line 159
    iput-object p1, p0, Lcom/squareup/ui/BaseXableEditText$2;->this$0:Lcom/squareup/ui/BaseXableEditText;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    .line 161
    iget-object p1, p0, Lcom/squareup/ui/BaseXableEditText$2;->this$0:Lcom/squareup/ui/BaseXableEditText;

    iget-object p1, p1, Lcom/squareup/ui/BaseXableEditText;->editText:Landroid/widget/EditText;

    const-string v0, ""

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 162
    iget-object p1, p0, Lcom/squareup/ui/BaseXableEditText$2;->this$0:Lcom/squareup/ui/BaseXableEditText;

    invoke-static {p1}, Lcom/squareup/ui/BaseXableEditText;->access$200(Lcom/squareup/ui/BaseXableEditText;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 163
    iget-object p1, p0, Lcom/squareup/ui/BaseXableEditText$2;->this$0:Lcom/squareup/ui/BaseXableEditText;

    iget-object p1, p1, Lcom/squareup/ui/BaseXableEditText;->editText:Landroid/widget/EditText;

    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 164
    iget-object p1, p0, Lcom/squareup/ui/BaseXableEditText$2;->this$0:Lcom/squareup/ui/BaseXableEditText;

    iget-object p1, p1, Lcom/squareup/ui/BaseXableEditText;->editText:Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->clearFocus()V

    :cond_0
    return-void
.end method
