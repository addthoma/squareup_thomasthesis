.class public Lcom/squareup/ui/TokenView;
.super Landroid/widget/LinearLayout;
.source "TokenView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/TokenView$Listener;
    }
.end annotation


# static fields
.field private static final FADE_DURATION_MS:I = 0x64


# instance fields
.field private colorStateListToRestore:Landroid/content/res/ColorStateList;

.field private focusChangeListener:Landroid/view/ViewTreeObserver$OnGlobalFocusChangeListener;

.field private final label:Lcom/squareup/marketfont/MarketTextView;

.field private listener:Lcom/squareup/ui/TokenView$Listener;

.field private final xButton:Lcom/squareup/glyph/SquareGlyphView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .line 38
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    sget-object v0, Lcom/squareup/widgets/pos/R$styleable;->TokenView:[I

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 41
    sget p2, Lcom/squareup/widgets/pos/R$styleable;->TokenView_android_saveEnabled:I

    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p2

    .line 42
    sget v2, Lcom/squareup/widgets/pos/R$styleable;->TokenView_android_textColor:I

    invoke-virtual {p1, v2}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    .line 43
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    .line 45
    new-instance p1, Landroid/animation/LayoutTransition;

    invoke-direct {p1}, Landroid/animation/LayoutTransition;-><init>()V

    const-wide/16 v3, 0x64

    .line 46
    invoke-virtual {p1, v3, v4}, Landroid/animation/LayoutTransition;->setDuration(J)V

    const-wide/16 v3, 0x0

    .line 50
    invoke-virtual {p1, v0, v3, v4}, Landroid/animation/LayoutTransition;->setStartDelay(IJ)V

    .line 51
    invoke-virtual {p0, p1}, Lcom/squareup/ui/TokenView;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 53
    invoke-virtual {p0}, Lcom/squareup/ui/TokenView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    .line 54
    sget v0, Lcom/squareup/marin/R$dimen;->marin_gap_mini:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 57
    new-instance v3, Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p0}, Lcom/squareup/ui/TokenView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/squareup/marketfont/MarketTextView;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/squareup/ui/TokenView;->label:Lcom/squareup/marketfont/MarketTextView;

    .line 58
    iget-object v3, p0, Lcom/squareup/ui/TokenView;->label:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v3, v0, v1, v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setPadding(IIII)V

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/TokenView;->label:Lcom/squareup/marketfont/MarketTextView;

    sget v1, Lcom/squareup/marin/R$drawable;->marin_selector_ultra_light_gray_when_pressed:I

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setBackgroundResource(I)V

    if-eqz v2, :cond_0

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/TokenView;->label:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, v2}, Lcom/squareup/marketfont/MarketTextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 62
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/TokenView;->label:Lcom/squareup/marketfont/MarketTextView;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setGravity(I)V

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/TokenView;->label:Lcom/squareup/marketfont/MarketTextView;

    sget-object v1, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/TokenView;->label:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p2}, Lcom/squareup/marketfont/MarketTextView;->setSaveEnabled(Z)V

    .line 65
    iget-object p2, p0, Lcom/squareup/ui/TokenView;->label:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p0, p2}, Lcom/squareup/ui/TokenView;->addView(Landroid/view/View;)V

    .line 66
    iget-object p2, p0, Lcom/squareup/ui/TokenView;->label:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p2}, Lcom/squareup/marketfont/MarketTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p2

    check-cast p2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v0, -0x2

    .line 67
    iput v0, p2, Landroid/widget/LinearLayout$LayoutParams;->width:I

    const/4 v0, -0x1

    .line 68
    iput v0, p2, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 69
    iget-object v1, p0, Lcom/squareup/ui/TokenView;->label:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v1, p2}, Lcom/squareup/marketfont/MarketTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 70
    iget-object p2, p0, Lcom/squareup/ui/TokenView;->label:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p2}, Lcom/squareup/marketfont/MarketTextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/TokenView;->colorStateListToRestore:Landroid/content/res/ColorStateList;

    .line 73
    iget-object p2, p0, Lcom/squareup/ui/TokenView;->label:Lcom/squareup/marketfont/MarketTextView;

    new-instance v1, Lcom/squareup/ui/TokenView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/TokenView$1;-><init>(Lcom/squareup/ui/TokenView;)V

    invoke-virtual {p2, v1}, Lcom/squareup/marketfont/MarketTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    new-instance p2, Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {p0}, Lcom/squareup/ui/TokenView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-direct {p2, v1, v2}, Lcom/squareup/glyph/SquareGlyphView;-><init>(Landroid/content/Context;Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    iput-object p2, p0, Lcom/squareup/ui/TokenView;->xButton:Lcom/squareup/glyph/SquareGlyphView;

    .line 81
    iget-object p2, p0, Lcom/squareup/ui/TokenView;->xButton:Lcom/squareup/glyph/SquareGlyphView;

    const/16 v1, 0x8

    invoke-virtual {p2, v1}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 82
    iget-object p2, p0, Lcom/squareup/ui/TokenView;->xButton:Lcom/squareup/glyph/SquareGlyphView;

    sget v1, Lcom/squareup/marin/R$color;->marin_white:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p2, v1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyphColor(I)V

    .line 83
    iget-object p2, p0, Lcom/squareup/ui/TokenView;->xButton:Lcom/squareup/glyph/SquareGlyphView;

    sget v1, Lcom/squareup/marin/R$drawable;->marin_selector_dark_gray_when_pressed:I

    invoke-virtual {p2, v1}, Lcom/squareup/glyph/SquareGlyphView;->setBackgroundResource(I)V

    .line 85
    iget-object p2, p0, Lcom/squareup/ui/TokenView;->xButton:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {p0, p2}, Lcom/squareup/ui/TokenView;->addView(Landroid/view/View;)V

    .line 86
    iget-object p2, p0, Lcom/squareup/ui/TokenView;->xButton:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {p2}, Lcom/squareup/glyph/SquareGlyphView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p2

    check-cast p2, Landroid/widget/LinearLayout$LayoutParams;

    .line 87
    sget v1, Lcom/squareup/marin/R$dimen;->marin_inline_element_height:I

    .line 88
    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p2, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 89
    iput v0, p2, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 90
    iget-object p1, p0, Lcom/squareup/ui/TokenView;->xButton:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {p1, p2}, Lcom/squareup/glyph/SquareGlyphView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 91
    iget-object p1, p0, Lcom/squareup/ui/TokenView;->xButton:Lcom/squareup/glyph/SquareGlyphView;

    new-instance p2, Lcom/squareup/ui/TokenView$2;

    invoke-direct {p2, p0}, Lcom/squareup/ui/TokenView$2;-><init>(Lcom/squareup/ui/TokenView;)V

    invoke-virtual {p1, p2}, Lcom/squareup/glyph/SquareGlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/TokenView;)Lcom/squareup/ui/TokenView$Listener;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/squareup/ui/TokenView;->listener:Lcom/squareup/ui/TokenView$Listener;

    return-object p0
.end method


# virtual methods
.method highlight()V
    .locals 3

    .line 122
    iget-object v0, p0, Lcom/squareup/ui/TokenView;->xButton:Lcom/squareup/glyph/SquareGlyphView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 123
    iget-object v0, p0, Lcom/squareup/ui/TokenView;->label:Lcom/squareup/marketfont/MarketTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 124
    sget v0, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    invoke-virtual {p0, v0}, Lcom/squareup/ui/TokenView;->setBackgroundResource(I)V

    .line 125
    iget-object v0, p0, Lcom/squareup/ui/TokenView;->label:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p0}, Lcom/squareup/ui/TokenView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/marin/R$color;->marin_white:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setTextColor(I)V

    return-void
.end method

.method public synthetic lambda$onAttachedToWindow$0$TokenView(Landroid/view/View;Landroid/view/View;)V
    .locals 0

    if-eq p0, p2, :cond_0

    .line 105
    invoke-virtual {p0}, Lcom/squareup/ui/TokenView;->getVisibility()I

    move-result p1

    const/16 p2, 0x8

    if-eq p1, p2, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/TokenView;->unhighlight()V

    :cond_0
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .line 99
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 101
    iget-object v0, p0, Lcom/squareup/ui/TokenView;->focusChangeListener:Landroid/view/ViewTreeObserver$OnGlobalFocusChangeListener;

    if-nez v0, :cond_0

    .line 102
    new-instance v0, Lcom/squareup/ui/-$$Lambda$TokenView$kbx6UVLxScrQ4fwuhGABf3hluyI;

    invoke-direct {v0, p0}, Lcom/squareup/ui/-$$Lambda$TokenView$kbx6UVLxScrQ4fwuhGABf3hluyI;-><init>(Lcom/squareup/ui/TokenView;)V

    iput-object v0, p0, Lcom/squareup/ui/TokenView;->focusChangeListener:Landroid/view/ViewTreeObserver$OnGlobalFocusChangeListener;

    .line 107
    invoke-virtual {p0}, Lcom/squareup/ui/TokenView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/TokenView;->focusChangeListener:Landroid/view/ViewTreeObserver$OnGlobalFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalFocusChangeListener(Landroid/view/ViewTreeObserver$OnGlobalFocusChangeListener;)V

    :cond_0
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .line 112
    invoke-virtual {p0}, Lcom/squareup/ui/TokenView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/TokenView;->focusChangeListener:Landroid/view/ViewTreeObserver$OnGlobalFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeOnGlobalFocusChangeListener(Landroid/view/ViewTreeObserver$OnGlobalFocusChangeListener;)V

    const/4 v0, 0x0

    .line 113
    iput-object v0, p0, Lcom/squareup/ui/TokenView;->focusChangeListener:Landroid/view/ViewTreeObserver$OnGlobalFocusChangeListener;

    .line 114
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method public setListener(Lcom/squareup/ui/TokenView$Listener;)V
    .locals 0

    .line 118
    iput-object p1, p0, Lcom/squareup/ui/TokenView;->listener:Lcom/squareup/ui/TokenView$Listener;

    return-void
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 143
    iget-object v0, p0, Lcom/squareup/ui/TokenView;->label:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setVisibility(I)V
    .locals 0

    if-nez p1, :cond_0

    .line 138
    invoke-virtual {p0}, Lcom/squareup/ui/TokenView;->unhighlight()V

    .line 139
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    return-void
.end method

.method unhighlight()V
    .locals 2

    .line 129
    iget-object v0, p0, Lcom/squareup/ui/TokenView;->xButton:Lcom/squareup/glyph/SquareGlyphView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 130
    iget-object v0, p0, Lcom/squareup/ui/TokenView;->label:Lcom/squareup/marketfont/MarketTextView;

    sget v1, Lcom/squareup/marin/R$drawable;->marin_selector_ultra_light_gray_when_pressed:I

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setBackgroundResource(I)V

    const/4 v0, 0x0

    .line 132
    invoke-virtual {p0, v0}, Lcom/squareup/ui/TokenView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 133
    iget-object v0, p0, Lcom/squareup/ui/TokenView;->label:Lcom/squareup/marketfont/MarketTextView;

    iget-object v1, p0, Lcom/squareup/ui/TokenView;->colorStateListToRestore:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    return-void
.end method
