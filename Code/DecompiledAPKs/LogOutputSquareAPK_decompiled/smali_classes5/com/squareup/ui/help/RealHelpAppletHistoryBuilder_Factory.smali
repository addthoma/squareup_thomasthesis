.class public final Lcom/squareup/ui/help/RealHelpAppletHistoryBuilder_Factory;
.super Ljava/lang/Object;
.source "RealHelpAppletHistoryBuilder_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/help/RealHelpAppletHistoryBuilder;",
        ">;"
    }
.end annotation


# instance fields
.field private final helpAppletProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/HelpApplet;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/HelpApplet;",
            ">;)V"
        }
    .end annotation

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/squareup/ui/help/RealHelpAppletHistoryBuilder_Factory;->helpAppletProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/ui/help/RealHelpAppletHistoryBuilder_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/HelpApplet;",
            ">;)",
            "Lcom/squareup/ui/help/RealHelpAppletHistoryBuilder_Factory;"
        }
    .end annotation

    .line 29
    new-instance v0, Lcom/squareup/ui/help/RealHelpAppletHistoryBuilder_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/ui/help/RealHelpAppletHistoryBuilder_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/help/HelpApplet;)Lcom/squareup/ui/help/RealHelpAppletHistoryBuilder;
    .locals 1

    .line 33
    new-instance v0, Lcom/squareup/ui/help/RealHelpAppletHistoryBuilder;

    invoke-direct {v0, p0}, Lcom/squareup/ui/help/RealHelpAppletHistoryBuilder;-><init>(Lcom/squareup/ui/help/HelpApplet;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/help/RealHelpAppletHistoryBuilder;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/ui/help/RealHelpAppletHistoryBuilder_Factory;->helpAppletProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/help/HelpApplet;

    invoke-static {v0}, Lcom/squareup/ui/help/RealHelpAppletHistoryBuilder_Factory;->newInstance(Lcom/squareup/ui/help/HelpApplet;)Lcom/squareup/ui/help/RealHelpAppletHistoryBuilder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/ui/help/RealHelpAppletHistoryBuilder_Factory;->get()Lcom/squareup/ui/help/RealHelpAppletHistoryBuilder;

    move-result-object v0

    return-object v0
.end method
