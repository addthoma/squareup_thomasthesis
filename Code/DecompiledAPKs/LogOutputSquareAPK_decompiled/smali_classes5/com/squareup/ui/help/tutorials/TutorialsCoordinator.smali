.class public Lcom/squareup/ui/help/tutorials/TutorialsCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "TutorialsCoordinator.java"


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private activationView:Landroid/view/ViewGroup;

.field private final helpAppletScopeRunner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

.field private final tutorialsSections:Lcom/squareup/ui/help/tutorials/TutorialsSections;

.field private tutorialsView:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/help/tutorials/TutorialsSections;Lcom/squareup/ui/help/HelpAppletScopeRunner;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 34
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/squareup/ui/help/tutorials/TutorialsCoordinator;->tutorialsSections:Lcom/squareup/ui/help/tutorials/TutorialsSections;

    .line 36
    iput-object p2, p0, Lcom/squareup/ui/help/tutorials/TutorialsCoordinator;->helpAppletScopeRunner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

    return-void
.end method

.method private bindViews(Landroid/view/View;)V
    .locals 1

    .line 91
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    .line 92
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/help/tutorials/TutorialsCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 94
    sget v0, Lcom/squareup/applet/help/R$id;->activation_content:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/ui/help/tutorials/TutorialsCoordinator;->activationView:Landroid/view/ViewGroup;

    .line 95
    sget v0, Lcom/squareup/applet/help/R$id;->tutorials_content:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    iput-object p1, p0, Lcom/squareup/ui/help/tutorials/TutorialsCoordinator;->tutorialsView:Landroid/view/ViewGroup;

    return-void
.end method

.method private createRow(Landroid/view/ViewGroup;Landroid/content/res/Resources;Lcom/squareup/ui/help/HelpAppletContent;Landroid/view/View$OnClickListener;)Lcom/squareup/ui/account/view/SmartLineRow;
    .locals 1

    .line 70
    sget v0, Lcom/squareup/applet/help/R$layout;->help_applet_content_row:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/account/view/SmartLineRow;

    .line 71
    iget v0, p3, Lcom/squareup/ui/help/HelpAppletContent;->titleStringId:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleText(Ljava/lang/CharSequence;)V

    .line 72
    iget-object p2, p3, Lcom/squareup/ui/help/HelpAppletContent;->subtitle:Ljava/lang/CharSequence;

    if-eqz p2, :cond_0

    .line 73
    iget-object p2, p3, Lcom/squareup/ui/help/HelpAppletContent;->subtitle:Ljava/lang/CharSequence;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleText(Ljava/lang/CharSequence;)V

    const/4 p2, 0x1

    .line 74
    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleVisible(Z)V

    .line 76
    :cond_0
    invoke-static {p4}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object p1
.end method

.method private getActionBarConfig(Landroid/content/res/Resources;)Lcom/squareup/marin/widgets/MarinActionBar$Config;
    .locals 2

    .line 87
    iget-object v0, p0, Lcom/squareup/ui/help/tutorials/TutorialsCoordinator;->helpAppletScopeRunner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

    sget v1, Lcom/squareup/applet/help/R$string;->tutorials:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/help/HelpAppletScopeRunner;->getActionBarConfig(Ljava/lang/String;)Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    return-object p1
.end method

.method private showBadge(Lcom/squareup/ui/account/view/SmartLineRow;Ljava/lang/String;)V
    .locals 1

    .line 81
    sget v0, Lcom/squareup/marin/R$color;->marin_blue:I

    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueColor(I)V

    const/4 v0, 0x1

    .line 82
    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueVisible(Z)V

    .line 83
    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private updateViews(Landroid/view/ViewGroup;Landroid/content/res/Resources;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Landroid/content/res/Resources;",
            "Ljava/util/List<",
            "Lcom/squareup/ui/help/HelpAppletContent;",
            ">;)V"
        }
    .end annotation

    .line 54
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_0
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/help/HelpAppletContent;

    .line 55
    new-instance v1, Lcom/squareup/ui/help/tutorials/-$$Lambda$TutorialsCoordinator$uEIv5b5o5c-Lg0JF5t2o_fOmaRA;

    invoke-direct {v1, p0, v0}, Lcom/squareup/ui/help/tutorials/-$$Lambda$TutorialsCoordinator$uEIv5b5o5c-Lg0JF5t2o_fOmaRA;-><init>(Lcom/squareup/ui/help/tutorials/TutorialsCoordinator;Lcom/squareup/ui/help/HelpAppletContent;)V

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/squareup/ui/help/tutorials/TutorialsCoordinator;->createRow(Landroid/view/ViewGroup;Landroid/content/res/Resources;Lcom/squareup/ui/help/HelpAppletContent;Landroid/view/View$OnClickListener;)Lcom/squareup/ui/account/view/SmartLineRow;

    move-result-object v1

    .line 58
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 59
    new-instance v2, Lcom/squareup/ui/help/tutorials/-$$Lambda$TutorialsCoordinator$OptScLW8y3DpNsh1dSpQUw4knNg;

    invoke-direct {v2, p0, v0, v1, p2}, Lcom/squareup/ui/help/tutorials/-$$Lambda$TutorialsCoordinator$OptScLW8y3DpNsh1dSpQUw4knNg;-><init>(Lcom/squareup/ui/help/tutorials/TutorialsCoordinator;Lcom/squareup/ui/help/HelpAppletContent;Lcom/squareup/ui/account/view/SmartLineRow;Landroid/content/res/Resources;)V

    invoke-static {p1, v2}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    .line 40
    invoke-direct {p0, p1}, Lcom/squareup/ui/help/tutorials/TutorialsCoordinator;->bindViews(Landroid/view/View;)V

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/help/tutorials/TutorialsCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/squareup/ui/help/tutorials/TutorialsCoordinator;->getActionBarConfig(Landroid/content/res/Resources;)Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 44
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 45
    new-instance v1, Lcom/squareup/ui/help/tutorials/-$$Lambda$TutorialsCoordinator$EyQ-Xjbiu0o2w_57qA2FUC-yixw;

    invoke-direct {v1, p0, v0}, Lcom/squareup/ui/help/tutorials/-$$Lambda$TutorialsCoordinator$EyQ-Xjbiu0o2w_57qA2FUC-yixw;-><init>(Lcom/squareup/ui/help/tutorials/TutorialsCoordinator;Landroid/content/res/Resources;)V

    invoke-static {p1, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 48
    new-instance v1, Lcom/squareup/ui/help/tutorials/-$$Lambda$TutorialsCoordinator$wl9ejRrLoWV_hwFdBbu2Wb0JDg0;

    invoke-direct {v1, p0, v0}, Lcom/squareup/ui/help/tutorials/-$$Lambda$TutorialsCoordinator$wl9ejRrLoWV_hwFdBbu2Wb0JDg0;-><init>(Lcom/squareup/ui/help/tutorials/TutorialsCoordinator;Landroid/content/res/Resources;)V

    invoke-static {p1, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public synthetic lambda$attach$1$TutorialsCoordinator(Landroid/content/res/Resources;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/help/tutorials/TutorialsCoordinator;->tutorialsSections:Lcom/squareup/ui/help/tutorials/TutorialsSections;

    invoke-virtual {v0}, Lcom/squareup/ui/help/tutorials/TutorialsSections;->tutorialsScreens()Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/help/tutorials/-$$Lambda$TutorialsCoordinator$sUym-8JLWyWkZIfZ2tozk3WnmJg;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/help/tutorials/-$$Lambda$TutorialsCoordinator$sUym-8JLWyWkZIfZ2tozk3WnmJg;-><init>(Lcom/squareup/ui/help/tutorials/TutorialsCoordinator;Landroid/content/res/Resources;)V

    .line 46
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$attach$3$TutorialsCoordinator(Landroid/content/res/Resources;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/help/tutorials/TutorialsCoordinator;->tutorialsSections:Lcom/squareup/ui/help/tutorials/TutorialsSections;

    invoke-virtual {v0}, Lcom/squareup/ui/help/tutorials/TutorialsSections;->tutorialsActivationScreens()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/help/tutorials/-$$Lambda$TutorialsCoordinator$rOlcYJSD15ppULilHcDXXxnKVwE;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/help/tutorials/-$$Lambda$TutorialsCoordinator$rOlcYJSD15ppULilHcDXXxnKVwE;-><init>(Lcom/squareup/ui/help/tutorials/TutorialsCoordinator;Landroid/content/res/Resources;)V

    .line 49
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$0$TutorialsCoordinator(Landroid/content/res/Resources;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/help/tutorials/TutorialsCoordinator;->tutorialsView:Landroid/view/ViewGroup;

    invoke-direct {p0, v0, p1, p2}, Lcom/squareup/ui/help/tutorials/TutorialsCoordinator;->updateViews(Landroid/view/ViewGroup;Landroid/content/res/Resources;Ljava/util/List;)V

    return-void
.end method

.method public synthetic lambda$null$2$TutorialsCoordinator(Landroid/content/res/Resources;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/help/tutorials/TutorialsCoordinator;->activationView:Landroid/view/ViewGroup;

    invoke-direct {p0, v0, p1, p2}, Lcom/squareup/ui/help/tutorials/TutorialsCoordinator;->updateViews(Landroid/view/ViewGroup;Landroid/content/res/Resources;Ljava/util/List;)V

    return-void
.end method

.method public synthetic lambda$null$5$TutorialsCoordinator(Lcom/squareup/ui/account/view/SmartLineRow;Lcom/squareup/ui/help/HelpAppletContent;Landroid/content/res/Resources;Ljava/lang/Boolean;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 60
    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p4

    invoke-static {p1, p4}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 61
    iget-object p4, p2, Lcom/squareup/ui/help/HelpAppletContent;->badgeStringId:Ljava/lang/Integer;

    if-eqz p4, :cond_0

    .line 62
    iget-object p2, p2, Lcom/squareup/ui/help/HelpAppletContent;->badgeStringId:Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    invoke-virtual {p3, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/help/tutorials/TutorialsCoordinator;->showBadge(Lcom/squareup/ui/account/view/SmartLineRow;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public synthetic lambda$updateViews$4$TutorialsCoordinator(Lcom/squareup/ui/help/HelpAppletContent;Landroid/view/View;)V
    .locals 0

    .line 56
    iget-object p2, p0, Lcom/squareup/ui/help/tutorials/TutorialsCoordinator;->helpAppletScopeRunner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

    invoke-virtual {p2, p1}, Lcom/squareup/ui/help/HelpAppletScopeRunner;->tutorialsContentClicked(Lcom/squareup/ui/help/HelpAppletContent;)V

    return-void
.end method

.method public synthetic lambda$updateViews$6$TutorialsCoordinator(Lcom/squareup/ui/help/HelpAppletContent;Lcom/squareup/ui/account/view/SmartLineRow;Landroid/content/res/Resources;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 59
    invoke-virtual {p1}, Lcom/squareup/ui/help/HelpAppletContent;->shouldDisplayObservable()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/help/tutorials/-$$Lambda$TutorialsCoordinator$b4iwZak__rWPSEsPVrvx6hVVpkw;

    invoke-direct {v1, p0, p2, p1, p3}, Lcom/squareup/ui/help/tutorials/-$$Lambda$TutorialsCoordinator$b4iwZak__rWPSEsPVrvx6hVVpkw;-><init>(Lcom/squareup/ui/help/tutorials/TutorialsCoordinator;Lcom/squareup/ui/account/view/SmartLineRow;Lcom/squareup/ui/help/HelpAppletContent;Landroid/content/res/Resources;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method
