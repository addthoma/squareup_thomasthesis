.class public final Lcom/squareup/ui/help/jedi/JediHelpScopeRunner_Factory;
.super Ljava/lang/Object;
.source "JediHelpScopeRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountStatusSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final browserLauncherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;"
        }
    .end annotation
.end field

.field private final deepLinksProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/DeepLinks;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final jediHelpSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/jedi/JediHelpSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final jediServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/help/JediService;",
            ">;"
        }
    .end annotation
.end field

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final messagingControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/MessagingController;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final resourcesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;"
        }
    .end annotation
.end field

.field private final telephonyManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/telephony/TelephonyManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/help/JediService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/DeepLinks;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/jedi/JediHelpSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/telephony/TelephonyManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/MessagingController;",
            ">;)V"
        }
    .end annotation

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p2, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner_Factory;->jediServiceProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p3, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner_Factory;->deepLinksProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p4, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner_Factory;->browserLauncherProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p5, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner_Factory;->resProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p6, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner_Factory;->jediHelpSettingsProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p7, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner_Factory;->resourcesProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p8, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner_Factory;->localeProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p9, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    .line 66
    iput-object p10, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner_Factory;->telephonyManagerProvider:Ljavax/inject/Provider;

    .line 67
    iput-object p11, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner_Factory;->messagingControllerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/help/jedi/JediHelpScopeRunner_Factory;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/help/JediService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/DeepLinks;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/jedi/JediHelpSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/telephony/TelephonyManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/MessagingController;",
            ">;)",
            "Lcom/squareup/ui/help/jedi/JediHelpScopeRunner_Factory;"
        }
    .end annotation

    .line 83
    new-instance v12, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner_Factory;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v12
.end method

.method public static newInstance(Lflow/Flow;Lcom/squareup/server/help/JediService;Lcom/squareup/ui/main/DeepLinks;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/util/Res;Lcom/squareup/jedi/JediHelpSettings;Landroid/content/res/Resources;Ljava/util/Locale;Lcom/squareup/settings/server/AccountStatusSettings;Landroid/telephony/TelephonyManager;Lcom/squareup/ui/help/MessagingController;)Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;
    .locals 13

    .line 91
    new-instance v12, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;-><init>(Lflow/Flow;Lcom/squareup/server/help/JediService;Lcom/squareup/ui/main/DeepLinks;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/util/Res;Lcom/squareup/jedi/JediHelpSettings;Landroid/content/res/Resources;Ljava/util/Locale;Lcom/squareup/settings/server/AccountStatusSettings;Landroid/telephony/TelephonyManager;Lcom/squareup/ui/help/MessagingController;)V

    return-object v12
.end method


# virtual methods
.method public get()Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;
    .locals 12

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lflow/Flow;

    iget-object v0, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner_Factory;->jediServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/server/help/JediService;

    iget-object v0, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner_Factory;->deepLinksProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/ui/main/DeepLinks;

    iget-object v0, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner_Factory;->browserLauncherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/util/BrowserLauncher;

    iget-object v0, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner_Factory;->jediHelpSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/jedi/JediHelpSettings;

    iget-object v0, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner_Factory;->resourcesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Landroid/content/res/Resources;

    iget-object v0, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner_Factory;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Ljava/util/Locale;

    iget-object v0, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner_Factory;->telephonyManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Landroid/telephony/TelephonyManager;

    iget-object v0, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner_Factory;->messagingControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/ui/help/MessagingController;

    invoke-static/range {v1 .. v11}, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner_Factory;->newInstance(Lflow/Flow;Lcom/squareup/server/help/JediService;Lcom/squareup/ui/main/DeepLinks;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/util/Res;Lcom/squareup/jedi/JediHelpSettings;Landroid/content/res/Resources;Ljava/util/Locale;Lcom/squareup/settings/server/AccountStatusSettings;Landroid/telephony/TelephonyManager;Lcom/squareup/ui/help/MessagingController;)Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 18
    invoke-virtual {p0}, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner_Factory;->get()Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;

    move-result-object v0

    return-object v0
.end method
