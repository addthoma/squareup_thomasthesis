.class public Lcom/squareup/ui/help/orders/OrdersVisibility;
.super Ljava/lang/Object;
.source "OrdersVisibility.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;
    }
.end annotation


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/AccountStatusSettings;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/ui/help/orders/OrdersVisibility;->features:Lcom/squareup/settings/server/Features;

    .line 22
    iput-object p2, p0, Lcom/squareup/ui/help/orders/OrdersVisibility;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    return-void
.end method


# virtual methods
.method public getReaderScreenType()Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;
    .locals 2

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrdersVisibility;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/PaymentSettings;->eligibleForSquareCardProcessing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 50
    sget-object v0, Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;->NONE:Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;

    return-object v0

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrdersVisibility;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CAN_SHOW_ORDER_READER_MENU:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 55
    sget-object v0, Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;->HARDWARE:Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;

    return-object v0

    .line 56
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrdersVisibility;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->REORDER_READER_IN_APP:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 58
    sget-object v0, Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;->MAGSTRIPE:Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;

    return-object v0

    .line 59
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrdersVisibility;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getSupportSettings()Lcom/squareup/settings/server/SupportUrlSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/SupportUrlSettings;->getReorderReaderUrl()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 61
    sget-object v0, Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;->LEGACY:Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;

    return-object v0

    .line 65
    :cond_3
    sget-object v0, Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;->NONE:Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;

    return-object v0
.end method

.method public showOrderHistory()Z
    .locals 2

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrdersVisibility;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->READER_ORDER_HISTORY:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method public showOrderReader()Z
    .locals 1

    .line 34
    invoke-virtual {p0}, Lcom/squareup/ui/help/orders/OrdersVisibility;->getReaderScreenType()Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;->isNative()Z

    move-result v0

    return v0
.end method

.method public showOrderReaderLegacy()Z
    .locals 1

    .line 38
    invoke-virtual {p0}, Lcom/squareup/ui/help/orders/OrdersVisibility;->getReaderScreenType()Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;->isLegacy()Z

    move-result v0

    return v0
.end method

.method public showOrdersSection()Z
    .locals 1

    .line 30
    invoke-virtual {p0}, Lcom/squareup/ui/help/orders/OrdersVisibility;->showOrderReader()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/ui/help/orders/OrdersVisibility;->showOrderReaderLegacy()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/ui/help/orders/OrdersVisibility;->showOrderHistory()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method
