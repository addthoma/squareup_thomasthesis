.class public Lcom/squareup/ui/help/about/LibrariesScreenData;
.super Ljava/lang/Object;
.source "LibrariesScreenData.java"


# instance fields
.field public final canFollowLinks:Z

.field public final libraries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/help/about/AndroidLibrary;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/help/about/AndroidLibrary;",
            ">;Z)V"
        }
    .end annotation

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object p1, p0, Lcom/squareup/ui/help/about/LibrariesScreenData;->libraries:Ljava/util/List;

    .line 11
    iput-boolean p2, p0, Lcom/squareup/ui/help/about/LibrariesScreenData;->canFollowLinks:Z

    return-void
.end method
