.class final Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$searchResponseForString$1$1$1;
.super Ljava/lang/Object;
.source "JediHelpScopeRunner.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$searchResponseForString$1$1;->apply(Ljava/lang/String;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/jedi/JediHelpScreenData;",
        "successOrFailure",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/jedi/service/SearchResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $it:Ljava/lang/String;

.field final synthetic $screenData:Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;

.field final synthetic this$0:Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$searchResponseForString$1$1;


# direct methods
.method constructor <init>(Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$searchResponseForString$1$1;Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$searchResponseForString$1$1$1;->this$0:Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$searchResponseForString$1$1;

    iput-object p2, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$searchResponseForString$1$1$1;->$screenData:Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;

    iput-object p3, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$searchResponseForString$1$1$1;->$it:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/jedi/JediHelpScreenData;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/jedi/service/SearchResponse;",
            ">;)",
            "Lcom/squareup/jedi/JediHelpScreenData;"
        }
    .end annotation

    const-string v0, "successOrFailure"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 238
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$searchResponseForString$1$1$1;->this$0:Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$searchResponseForString$1$1;

    iget-object v0, v0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$searchResponseForString$1$1;->this$0:Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$searchResponseForString$1;

    iget-object v0, v0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$searchResponseForString$1;->this$0:Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;

    iget-object v1, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$searchResponseForString$1$1$1;->$screenData:Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;

    iget-object v2, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$searchResponseForString$1$1$1;->$it:Ljava/lang/String;

    const-string v3, "it"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 239
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/jedi/service/SearchResponse;

    .line 238
    invoke-static {v0, v1, v2, p1}, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->access$screenDataFromSuccessfulResponse(Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;Ljava/lang/String;Lcom/squareup/protos/jedi/service/SearchResponse;)Lcom/squareup/jedi/JediHelpScreenData;

    move-result-object p1

    return-object p1

    .line 240
    :cond_0
    instance-of p1, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz p1, :cond_1

    new-instance p1, Lcom/squareup/ui/help/jedi/JediResponseException;

    const-string v0, "Unable to get search result."

    invoke-direct {p1, v0}, Lcom/squareup/ui/help/jedi/JediResponseException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 76
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$searchResponseForString$1$1$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/jedi/JediHelpScreenData;

    move-result-object p1

    return-object p1
.end method
