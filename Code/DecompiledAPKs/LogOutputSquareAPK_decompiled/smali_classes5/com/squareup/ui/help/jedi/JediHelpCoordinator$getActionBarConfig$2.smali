.class final Lcom/squareup/ui/help/jedi/JediHelpCoordinator$getActionBarConfig$2;
.super Ljava/lang/Object;
.source "JediHelpCoordinator.kt"

# interfaces
.implements Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig$CustomViewFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/help/jedi/JediHelpCoordinator;->getActionBarConfig(Ljava/lang/String;Z)Lcom/squareup/marin/widgets/MarinActionBar$Config;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/jedi/SpeechBubbleImageView;",
        "it",
        "Landroid/content/Context;",
        "kotlin.jvm.PlatformType",
        "buildView"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/help/jedi/JediHelpCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/help/jedi/JediHelpCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$getActionBarConfig$2;->this$0:Lcom/squareup/ui/help/jedi/JediHelpCoordinator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic buildView(Landroid/content/Context;)Landroid/view/View;
    .locals 0

    .line 32
    invoke-virtual {p0, p1}, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$getActionBarConfig$2;->buildView(Landroid/content/Context;)Lcom/squareup/jedi/SpeechBubbleImageView;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    return-object p1
.end method

.method public final buildView(Landroid/content/Context;)Lcom/squareup/jedi/SpeechBubbleImageView;
    .locals 0

    .line 147
    iget-object p1, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$getActionBarConfig$2;->this$0:Lcom/squareup/ui/help/jedi/JediHelpCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/help/jedi/JediHelpCoordinator;->access$getSpeechBubbleImageView$p(Lcom/squareup/ui/help/jedi/JediHelpCoordinator;)Lcom/squareup/jedi/SpeechBubbleImageView;

    move-result-object p1

    return-object p1
.end method
