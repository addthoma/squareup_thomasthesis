.class public Lcom/squareup/ui/help/tutorials/TutorialsScreen;
.super Lcom/squareup/ui/help/InHelpAppletScope;
.source "TutorialsScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/coordinators/CoordinatorProvider;
.implements Lcom/squareup/container/layer/InSection;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/help/tutorials/TutorialsScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/help/tutorials/TutorialsScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    new-instance v0, Lcom/squareup/ui/help/tutorials/TutorialsScreen;

    invoke-direct {v0}, Lcom/squareup/ui/help/tutorials/TutorialsScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/help/tutorials/TutorialsScreen;->INSTANCE:Lcom/squareup/ui/help/tutorials/TutorialsScreen;

    .line 41
    sget-object v0, Lcom/squareup/ui/help/tutorials/TutorialsScreen;->INSTANCE:Lcom/squareup/ui/help/tutorials/TutorialsScreen;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/help/tutorials/TutorialsScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Lcom/squareup/ui/help/InHelpAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 34
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->SUPPORT_TUTORIALS:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 26
    const-class v0, Lcom/squareup/ui/help/tutorials/TutorialsSection;

    return-object v0
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    .line 38
    const-class v0, Lcom/squareup/ui/help/HelpAppletScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/help/HelpAppletScope$Component;

    invoke-interface {p1}, Lcom/squareup/ui/help/HelpAppletScope$Component;->tutorialsCoordinator()Lcom/squareup/ui/help/tutorials/TutorialsCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 30
    sget v0, Lcom/squareup/applet/help/R$layout;->tutorials:I

    return v0
.end method
