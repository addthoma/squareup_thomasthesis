.class public final Lcom/squareup/ui/help/tutorials/content/AdjustPointsLoyaltyTutorial;
.super Lcom/squareup/ui/help/HelpAppletContent;
.source "AdjustPointsLoyaltyTutorial.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\'\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000e\u0008\u0001\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\u0008\u0010\n\u001a\u00020\u0006H\u0016J\u0008\u0010\u000b\u001a\u00020\u000cH\u0016R\u0014\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/ui/help/tutorials/content/AdjustPointsLoyaltyTutorial;",
        "Lcom/squareup/ui/help/HelpAppletContent;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "adjustPointsLoyaltyTutorialViewed",
        "Lcom/squareup/settings/LocalSetting;",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/settings/server/Features;Lcom/squareup/settings/LocalSetting;Lcom/squareup/util/Res;)V",
        "shouldDisplay",
        "updateBadge",
        "",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final adjustPointsLoyaltyTutorialViewed:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final features:Lcom/squareup/settings/server/Features;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/Features;Lcom/squareup/settings/LocalSetting;Lcom/squareup/util/Res;)V
    .locals 9
    .param p2    # Lcom/squareup/settings/LocalSetting;
        .annotation runtime Lcom/squareup/settings/LoyaltyAdjustPointsTutorialViewed;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/util/Res;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "features"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "adjustPointsLoyaltyTutorialViewed"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    sget v2, Lcom/squareup/applet/help/R$string;->loyalty_adjust_points:I

    .line 20
    sget v0, Lcom/squareup/applet/help/R$string;->loyalty_adjust_points_subtext:I

    invoke-interface {p3, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p3

    move-object v3, p3

    check-cast v3, Ljava/lang/CharSequence;

    .line 21
    sget-object v5, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_LOYALTY_ADJUST_POINTS_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x14

    const/4 v8, 0x0

    move-object v1, p0

    .line 18
    invoke-direct/range {v1 .. v8}, Lcom/squareup/ui/help/HelpAppletContent;-><init>(ILjava/lang/CharSequence;Ljava/lang/Integer;Lcom/squareup/analytics/RegisterTapName;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/ui/help/tutorials/content/AdjustPointsLoyaltyTutorial;->features:Lcom/squareup/settings/server/Features;

    iput-object p2, p0, Lcom/squareup/ui/help/tutorials/content/AdjustPointsLoyaltyTutorial;->adjustPointsLoyaltyTutorialViewed:Lcom/squareup/settings/LocalSetting;

    return-void
.end method


# virtual methods
.method public shouldDisplay()Z
    .locals 2

    .line 25
    iget-object v0, p0, Lcom/squareup/ui/help/tutorials/content/AdjustPointsLoyaltyTutorial;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_TUTORIALS_ENROLLMENT:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method public updateBadge()V
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/ui/help/tutorials/content/AdjustPointsLoyaltyTutorial;->adjustPointsLoyaltyTutorialViewed:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v0}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 30
    sget v0, Lcom/squareup/applet/help/R$string;->new_word:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/help/HelpAppletContent;->badgeStringId:Ljava/lang/Integer;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 32
    check-cast v0, Ljava/lang/Integer;

    iput-object v0, p0, Lcom/squareup/ui/help/HelpAppletContent;->badgeStringId:Ljava/lang/Integer;

    :goto_0
    return-void
.end method
