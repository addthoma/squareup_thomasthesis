.class public Lcom/squareup/ui/help/troubleshooting/TroubleShootingCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "TroubleShootingCoordinator.java"


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private emailSupportLedgerRow:Lcom/squareup/widgets/list/NameValueRow;

.field private final helpAppletScopeRunner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

.field private final helpTroubleshootingRunner:Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private sendDiagnosticRow:Lcom/squareup/widgets/list/NameValueRow;

.field private troubleshootingProgressBar:Landroid/widget/ProgressBar;

.field private uploadSupportLedgerRow:Lcom/squareup/widgets/list/NameValueRow;


# direct methods
.method constructor <init>(Lcom/squareup/ui/help/HelpAppletScopeRunner;Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;Lio/reactivex/Scheduler;)V
    .locals 0
    .param p3    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 36
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/squareup/ui/help/troubleshooting/TroubleShootingCoordinator;->helpAppletScopeRunner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

    .line 38
    iput-object p2, p0, Lcom/squareup/ui/help/troubleshooting/TroubleShootingCoordinator;->helpTroubleshootingRunner:Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;

    .line 39
    iput-object p3, p0, Lcom/squareup/ui/help/troubleshooting/TroubleShootingCoordinator;->mainScheduler:Lio/reactivex/Scheduler;

    return-void
.end method

.method private bindViews(Landroid/view/View;)V
    .locals 1

    .line 78
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    .line 79
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/help/troubleshooting/TroubleShootingCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 80
    sget v0, Lcom/squareup/applet/help/R$id;->email_support_ledger_row:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/NameValueRow;

    iput-object v0, p0, Lcom/squareup/ui/help/troubleshooting/TroubleShootingCoordinator;->emailSupportLedgerRow:Lcom/squareup/widgets/list/NameValueRow;

    .line 81
    sget v0, Lcom/squareup/applet/help/R$id;->upload_support_ledger_row:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/NameValueRow;

    iput-object v0, p0, Lcom/squareup/ui/help/troubleshooting/TroubleShootingCoordinator;->uploadSupportLedgerRow:Lcom/squareup/widgets/list/NameValueRow;

    .line 82
    sget v0, Lcom/squareup/applet/help/R$id;->send_diagnostic_row:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/NameValueRow;

    iput-object v0, p0, Lcom/squareup/ui/help/troubleshooting/TroubleShootingCoordinator;->sendDiagnosticRow:Lcom/squareup/widgets/list/NameValueRow;

    .line 83
    sget v0, Lcom/squareup/applet/help/R$id;->troubleshooting_progress_bar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ProgressBar;

    iput-object p1, p0, Lcom/squareup/ui/help/troubleshooting/TroubleShootingCoordinator;->troubleshootingProgressBar:Landroid/widget/ProgressBar;

    return-void
.end method

.method private getActionBarConfig(Landroid/content/res/Resources;)Lcom/squareup/marin/widgets/MarinActionBar$Config;
    .locals 2

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/help/troubleshooting/TroubleShootingCoordinator;->helpAppletScopeRunner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

    sget v1, Lcom/squareup/applet/help/R$string;->troubleshooting:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/help/HelpAppletScopeRunner;->getActionBarConfig(Ljava/lang/String;)Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    return-object p1
.end method

.method private updateLoading(Z)V
    .locals 2

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/help/troubleshooting/TroubleShootingCoordinator;->troubleshootingProgressBar:Landroid/widget/ProgressBar;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/help/troubleshooting/TroubleShootingCoordinator;->emailSupportLedgerRow:Lcom/squareup/widgets/list/NameValueRow;

    xor-int/lit8 v1, p1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/NameValueRow;->setEnabled(Z)V

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/help/troubleshooting/TroubleShootingCoordinator;->uploadSupportLedgerRow:Lcom/squareup/widgets/list/NameValueRow;

    xor-int/lit8 v1, p1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/NameValueRow;->setEnabled(Z)V

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/help/troubleshooting/TroubleShootingCoordinator;->sendDiagnosticRow:Lcom/squareup/widgets/list/NameValueRow;

    xor-int/lit8 p1, p1, 0x1

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/NameValueRow;->setEnabled(Z)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    .line 43
    invoke-direct {p0, p1}, Lcom/squareup/ui/help/troubleshooting/TroubleShootingCoordinator;->bindViews(Landroid/view/View;)V

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/help/troubleshooting/TroubleShootingCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/squareup/ui/help/troubleshooting/TroubleShootingCoordinator;->getActionBarConfig(Landroid/content/res/Resources;)Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/help/troubleshooting/TroubleShootingCoordinator;->emailSupportLedgerRow:Lcom/squareup/widgets/list/NameValueRow;

    new-instance v1, Lcom/squareup/ui/help/troubleshooting/-$$Lambda$TroubleShootingCoordinator$Z_UwIe8fjDe26QwiVl-o6JlIkT4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/help/troubleshooting/-$$Lambda$TroubleShootingCoordinator$Z_UwIe8fjDe26QwiVl-o6JlIkT4;-><init>(Lcom/squareup/ui/help/troubleshooting/TroubleShootingCoordinator;)V

    .line 48
    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v1

    .line 47
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/NameValueRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/help/troubleshooting/TroubleShootingCoordinator;->uploadSupportLedgerRow:Lcom/squareup/widgets/list/NameValueRow;

    new-instance v1, Lcom/squareup/ui/help/troubleshooting/-$$Lambda$TroubleShootingCoordinator$pv1YXfT6AQiMP-qAKq0KEwmpwO8;

    invoke-direct {v1, p0}, Lcom/squareup/ui/help/troubleshooting/-$$Lambda$TroubleShootingCoordinator$pv1YXfT6AQiMP-qAKq0KEwmpwO8;-><init>(Lcom/squareup/ui/help/troubleshooting/TroubleShootingCoordinator;)V

    .line 50
    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v1

    .line 49
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/NameValueRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/help/troubleshooting/TroubleShootingCoordinator;->sendDiagnosticRow:Lcom/squareup/widgets/list/NameValueRow;

    new-instance v1, Lcom/squareup/ui/help/troubleshooting/-$$Lambda$TroubleShootingCoordinator$JbvLrTAdim_3-b4qebRC08kDdaM;

    invoke-direct {v1, p0}, Lcom/squareup/ui/help/troubleshooting/-$$Lambda$TroubleShootingCoordinator$JbvLrTAdim_3-b4qebRC08kDdaM;-><init>(Lcom/squareup/ui/help/troubleshooting/TroubleShootingCoordinator;)V

    .line 52
    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v1

    .line 51
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/NameValueRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 54
    new-instance v0, Lcom/squareup/ui/help/troubleshooting/-$$Lambda$TroubleShootingCoordinator$-olwyfzcYcyz1ahWcgCKCuHKz2A;

    invoke-direct {v0, p0}, Lcom/squareup/ui/help/troubleshooting/-$$Lambda$TroubleShootingCoordinator$-olwyfzcYcyz1ahWcgCKCuHKz2A;-><init>(Lcom/squareup/ui/help/troubleshooting/TroubleShootingCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public synthetic lambda$attach$0$TroubleShootingCoordinator(Landroid/view/View;)V
    .locals 0

    .line 48
    iget-object p1, p0, Lcom/squareup/ui/help/troubleshooting/TroubleShootingCoordinator;->helpTroubleshootingRunner:Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;->emailSupportLedger()V

    return-void
.end method

.method public synthetic lambda$attach$1$TroubleShootingCoordinator(Landroid/view/View;)V
    .locals 0

    .line 50
    iget-object p1, p0, Lcom/squareup/ui/help/troubleshooting/TroubleShootingCoordinator;->helpTroubleshootingRunner:Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;->uploadSupportLedger()V

    return-void
.end method

.method public synthetic lambda$attach$2$TroubleShootingCoordinator(Landroid/view/View;)V
    .locals 0

    .line 52
    iget-object p1, p0, Lcom/squareup/ui/help/troubleshooting/TroubleShootingCoordinator;->helpTroubleshootingRunner:Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;->sendDiagnosticReport()V

    return-void
.end method

.method public synthetic lambda$attach$4$TroubleShootingCoordinator()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/help/troubleshooting/TroubleShootingCoordinator;->helpTroubleshootingRunner:Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;->troubleshootingScreenData()Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/help/troubleshooting/TroubleShootingCoordinator;->mainScheduler:Lio/reactivex/Scheduler;

    .line 55
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/help/troubleshooting/-$$Lambda$TroubleShootingCoordinator$zX7KrafkZg3Y4krlzHRFmQZN7jQ;

    invoke-direct {v1, p0}, Lcom/squareup/ui/help/troubleshooting/-$$Lambda$TroubleShootingCoordinator$zX7KrafkZg3Y4krlzHRFmQZN7jQ;-><init>(Lcom/squareup/ui/help/troubleshooting/TroubleShootingCoordinator;)V

    .line 56
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$null$3$TroubleShootingCoordinator(Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/help/troubleshooting/TroubleShootingCoordinator;->emailSupportLedgerRow:Lcom/squareup/widgets/list/NameValueRow;

    iget-boolean v1, p1, Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData;->showEmailSupportLedger:Z

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/help/troubleshooting/TroubleShootingCoordinator;->uploadSupportLedgerRow:Lcom/squareup/widgets/list/NameValueRow;

    iget-boolean v1, p1, Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData;->showUploadSupportLedger:Z

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/help/troubleshooting/TroubleShootingCoordinator;->sendDiagnosticRow:Lcom/squareup/widgets/list/NameValueRow;

    iget-boolean v1, p1, Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData;->showSendDiagnosticReport:Z

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 61
    iget-boolean p1, p1, Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData;->isSending:Z

    invoke-direct {p0, p1}, Lcom/squareup/ui/help/troubleshooting/TroubleShootingCoordinator;->updateLoading(Z)V

    return-void
.end method
