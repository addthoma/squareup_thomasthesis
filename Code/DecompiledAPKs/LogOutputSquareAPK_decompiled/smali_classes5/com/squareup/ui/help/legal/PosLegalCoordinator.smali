.class public Lcom/squareup/ui/help/legal/PosLegalCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "PosLegalCoordinator.java"


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private final helpAppletScopeRunner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

.field private privacyPolicyRow:Lcom/squareup/ui/account/view/SmartLineRow;

.field private sellerAgreementRow:Lcom/squareup/ui/account/view/SmartLineRow;


# direct methods
.method constructor <init>(Lcom/squareup/ui/help/HelpAppletScopeRunner;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 25
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/ui/help/legal/PosLegalCoordinator;->helpAppletScopeRunner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

    return-void
.end method

.method private bindViews(Landroid/view/View;)V
    .locals 1

    .line 45
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    .line 46
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/help/legal/PosLegalCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 47
    sget v0, Lcom/squareup/applet/help/R$id;->seller_agreement_row:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/account/view/SmartLineRow;

    iput-object v0, p0, Lcom/squareup/ui/help/legal/PosLegalCoordinator;->sellerAgreementRow:Lcom/squareup/ui/account/view/SmartLineRow;

    .line 48
    sget v0, Lcom/squareup/applet/help/R$id;->privacy_policy_row:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/account/view/SmartLineRow;

    iput-object p1, p0, Lcom/squareup/ui/help/legal/PosLegalCoordinator;->privacyPolicyRow:Lcom/squareup/ui/account/view/SmartLineRow;

    return-void
.end method

.method private getActionBarConfig(Landroid/content/res/Resources;)Lcom/squareup/marin/widgets/MarinActionBar$Config;
    .locals 2

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/help/legal/PosLegalCoordinator;->helpAppletScopeRunner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

    sget v1, Lcom/squareup/applet/help/R$string;->legal:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/help/HelpAppletScopeRunner;->getActionBarConfig(Ljava/lang/String;)Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    .line 30
    invoke-direct {p0, p1}, Lcom/squareup/ui/help/legal/PosLegalCoordinator;->bindViews(Landroid/view/View;)V

    .line 32
    iget-object v0, p0, Lcom/squareup/ui/help/legal/PosLegalCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/help/legal/PosLegalCoordinator;->getActionBarConfig(Landroid/content/res/Resources;)Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 34
    iget-object p1, p0, Lcom/squareup/ui/help/legal/PosLegalCoordinator;->sellerAgreementRow:Lcom/squareup/ui/account/view/SmartLineRow;

    iget-object v0, p0, Lcom/squareup/ui/help/legal/PosLegalCoordinator;->helpAppletScopeRunner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/help/legal/-$$Lambda$tQsfYY77aczT2QpBJnGyw4ORkfg;

    invoke-direct {v1, v0}, Lcom/squareup/ui/help/legal/-$$Lambda$tQsfYY77aczT2QpBJnGyw4ORkfg;-><init>(Lcom/squareup/ui/help/HelpAppletScopeRunner;)V

    .line 35
    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounceRunnable(Ljava/lang/Runnable;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v0

    .line 34
    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 36
    iget-object p1, p0, Lcom/squareup/ui/help/legal/PosLegalCoordinator;->privacyPolicyRow:Lcom/squareup/ui/account/view/SmartLineRow;

    iget-object v0, p0, Lcom/squareup/ui/help/legal/PosLegalCoordinator;->helpAppletScopeRunner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/help/legal/-$$Lambda$F0EpqbhWw9HztNzaGifA1bUqI-w;

    invoke-direct {v1, v0}, Lcom/squareup/ui/help/legal/-$$Lambda$F0EpqbhWw9HztNzaGifA1bUqI-w;-><init>(Lcom/squareup/ui/help/HelpAppletScopeRunner;)V

    .line 37
    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounceRunnable(Ljava/lang/Runnable;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v0

    .line 36
    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
