.class public Lcom/squareup/ui/help/tutorials/TutorialsSections;
.super Ljava/lang/Object;
.source "TutorialsSections.java"


# instance fields
.field private final accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;

.field private final helpAppletSettings:Lcom/squareup/ui/help/api/HelpAppletSettings;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/help/api/HelpAppletSettings;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/banklinking/BankAccountSettings;Lcom/squareup/util/Res;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/ui/help/tutorials/TutorialsSections;->helpAppletSettings:Lcom/squareup/ui/help/api/HelpAppletSettings;

    .line 29
    iput-object p2, p0, Lcom/squareup/ui/help/tutorials/TutorialsSections;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 30
    iput-object p3, p0, Lcom/squareup/ui/help/tutorials/TutorialsSections;->bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;

    .line 31
    iput-object p4, p0, Lcom/squareup/ui/help/tutorials/TutorialsSections;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method static synthetic lambda$tutorialsActivationScreens$0(Lkotlin/Unit;Lcom/squareup/banklinking/BankAccountSettings$State;)Ljava/lang/Boolean;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 46
    invoke-virtual {p1}, Lcom/squareup/banklinking/BankAccountSettings$State;->showLinkBankAccount()Z

    move-result p0

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public synthetic lambda$tutorialsActivationScreens$1$TutorialsSections(Lcom/squareup/settings/server/OnboardingSettings;Ljava/lang/Boolean;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 50
    invoke-virtual {p1}, Lcom/squareup/settings/server/OnboardingSettings;->showInAppActivationPostSignup()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 51
    invoke-virtual {p1}, Lcom/squareup/settings/server/OnboardingSettings;->isEligibleForSquareCardPayments()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 52
    new-instance p1, Lcom/squareup/ui/help/tutorials/content/AcceptCreditCards;

    invoke-direct {p1}, Lcom/squareup/ui/help/tutorials/content/AcceptCreditCards;-><init>()V

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 54
    :cond_0
    new-instance p1, Lcom/squareup/ui/help/tutorials/content/FinalizeAccountSetup;

    invoke-direct {p1}, Lcom/squareup/ui/help/tutorials/content/FinalizeAccountSetup;-><init>()V

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 56
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/settings/server/OnboardingSettings;->acceptsCards()Z

    move-result p1

    if-eqz p1, :cond_2

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 57
    new-instance p1, Lcom/squareup/ui/help/tutorials/content/LinkBankAccount;

    iget-object p2, p0, Lcom/squareup/ui/help/tutorials/TutorialsSections;->res:Lcom/squareup/util/Res;

    invoke-direct {p1, p2}, Lcom/squareup/ui/help/tutorials/content/LinkBankAccount;-><init>(Lcom/squareup/util/Res;)V

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    :goto_0
    return-object v0
.end method

.method public tutorialsActivationScreens()Lio/reactivex/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/ui/help/HelpAppletContent;",
            ">;>;"
        }
    .end annotation

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/help/tutorials/TutorialsSections;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getOnboardingSettings()Lcom/squareup/settings/server/OnboardingSettings;

    move-result-object v0

    .line 42
    iget-object v1, p0, Lcom/squareup/ui/help/tutorials/TutorialsSections;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 44
    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->settingsAvailable()Lio/reactivex/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/help/tutorials/TutorialsSections;->bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;

    .line 45
    invoke-interface {v2}, Lcom/squareup/banklinking/BankAccountSettings;->state()Lio/reactivex/Observable;

    move-result-object v2

    sget-object v3, Lcom/squareup/ui/help/tutorials/-$$Lambda$TutorialsSections$RTo4TSVM1keFAXQfMgH4NeM1YE0;->INSTANCE:Lcom/squareup/ui/help/tutorials/-$$Lambda$TutorialsSections$RTo4TSVM1keFAXQfMgH4NeM1YE0;

    .line 43
    invoke-static {v1, v2, v3}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/help/tutorials/-$$Lambda$TutorialsSections$zbon7wAYC3kZeOKPHdNp77tUwKQ;

    invoke-direct {v2, p0, v0}, Lcom/squareup/ui/help/tutorials/-$$Lambda$TutorialsSections$zbon7wAYC3kZeOKPHdNp77tUwKQ;-><init>(Lcom/squareup/ui/help/tutorials/TutorialsSections;Lcom/squareup/settings/server/OnboardingSettings;)V

    .line 48
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public tutorialsScreens()Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Ljava/util/List<",
            "Lcom/squareup/ui/help/HelpAppletContent;",
            ">;>;"
        }
    .end annotation

    .line 69
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/squareup/ui/help/tutorials/TutorialsSections;->helpAppletSettings:Lcom/squareup/ui/help/api/HelpAppletSettings;

    .line 70
    invoke-interface {v1}, Lcom/squareup/ui/help/api/HelpAppletSettings;->helpAppletTutorials()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 71
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/help/HelpAppletContent;

    .line 72
    invoke-virtual {v2}, Lcom/squareup/ui/help/HelpAppletContent;->shouldDisplay()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 73
    invoke-virtual {v2}, Lcom/squareup/ui/help/HelpAppletContent;->updateBadge()V

    goto :goto_0

    .line 76
    :cond_1
    invoke-static {v0}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method
