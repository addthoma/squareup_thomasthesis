.class final Lcom/squareup/ui/help/messages/HelpshiftMessagingController$messagingViewState$1;
.super Ljava/lang/Object;
.source "HelpshiftMessagingController.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/help/messages/HelpshiftMessagingController;->messagingViewState(Z)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/ui/help/MessagingViewState;",
        "messagingEligibilityState",
        "Lcom/squareup/ui/help/messages/MessagingEligibilityState;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $requireInstall:Z

.field final synthetic this$0:Lcom/squareup/ui/help/messages/HelpshiftMessagingController;


# direct methods
.method constructor <init>(Lcom/squareup/ui/help/messages/HelpshiftMessagingController;Z)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/help/messages/HelpshiftMessagingController$messagingViewState$1;->this$0:Lcom/squareup/ui/help/messages/HelpshiftMessagingController;

    iput-boolean p2, p0, Lcom/squareup/ui/help/messages/HelpshiftMessagingController$messagingViewState$1;->$requireInstall:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/ui/help/messages/MessagingEligibilityState;)Lcom/squareup/ui/help/MessagingViewState;
    .locals 3

    const-string v0, "messagingEligibilityState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    instance-of v0, p1, Lcom/squareup/ui/help/messages/MessagingEligibilityState$EligibleToMessage;

    if-eqz v0, :cond_3

    .line 78
    iget-boolean v0, p0, Lcom/squareup/ui/help/messages/HelpshiftMessagingController$messagingViewState$1;->$requireInstall:Z

    if-nez v0, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/squareup/ui/help/messages/MessagingEligibilityState$EligibleToMessage;

    invoke-virtual {v0}, Lcom/squareup/ui/help/messages/MessagingEligibilityState$EligibleToMessage;->getRecentActive()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/help/messages/HelpshiftMessagingController$messagingViewState$1;->this$0:Lcom/squareup/ui/help/messages/HelpshiftMessagingController;

    move-object v1, p1

    check-cast v1, Lcom/squareup/ui/help/messages/MessagingEligibilityState$EligibleToMessage;

    invoke-static {v0, v1}, Lcom/squareup/ui/help/messages/HelpshiftMessagingController;->access$installHelpshift(Lcom/squareup/ui/help/messages/HelpshiftMessagingController;Lcom/squareup/ui/help/messages/MessagingEligibilityState$EligibleToMessage;)Z

    .line 81
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/help/messages/HelpshiftMessagingController$messagingViewState$1;->this$0:Lcom/squareup/ui/help/messages/HelpshiftMessagingController;

    invoke-virtual {v0}, Lcom/squareup/ui/help/messages/HelpshiftMessagingController;->helpshiftSDKInstalled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 83
    new-instance v0, Lcom/squareup/ui/help/MessagingViewState$Ready;

    check-cast p1, Lcom/squareup/ui/help/messages/MessagingEligibilityState$EligibleToMessage;

    invoke-virtual {p1}, Lcom/squareup/ui/help/messages/MessagingEligibilityState$EligibleToMessage;->getCustomIssueFields()Ljava/util/List;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/ui/help/MessagingViewState$Ready;-><init>(Ljava/util/List;)V

    check-cast v0, Lcom/squareup/ui/help/MessagingViewState;

    goto :goto_0

    .line 86
    :cond_2
    sget-object p1, Lcom/squareup/ui/help/MessagingViewState$UnableToLoad;->INSTANCE:Lcom/squareup/ui/help/MessagingViewState$UnableToLoad;

    move-object v0, p1

    check-cast v0, Lcom/squareup/ui/help/MessagingViewState;

    goto :goto_0

    .line 91
    :cond_3
    instance-of v0, p1, Lcom/squareup/ui/help/messages/MessagingEligibilityState$Ineligible;

    if-eqz v0, :cond_4

    new-instance v0, Lcom/squareup/ui/help/MessagingViewState$Disallowed;

    .line 92
    check-cast p1, Lcom/squareup/ui/help/messages/MessagingEligibilityState$Ineligible;

    invoke-virtual {p1}, Lcom/squareup/ui/help/messages/MessagingEligibilityState$Ineligible;->getTitle()Ljava/lang/String;

    move-result-object v1

    .line 93
    invoke-virtual {p1}, Lcom/squareup/ui/help/messages/MessagingEligibilityState$Ineligible;->getHelpTipTitle()Ljava/lang/String;

    move-result-object v2

    .line 94
    invoke-virtual {p1}, Lcom/squareup/ui/help/messages/MessagingEligibilityState$Ineligible;->getHelpTipSubtitle()Ljava/lang/String;

    move-result-object p1

    .line 91
    invoke-direct {v0, v1, v2, p1}, Lcom/squareup/ui/help/MessagingViewState$Disallowed;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/ui/help/MessagingViewState;

    goto :goto_0

    .line 98
    :cond_4
    instance-of p1, p1, Lcom/squareup/ui/help/messages/MessagingEligibilityState$ServerError;

    if-eqz p1, :cond_5

    sget-object p1, Lcom/squareup/ui/help/MessagingViewState$UnableToLoad;->INSTANCE:Lcom/squareup/ui/help/MessagingViewState$UnableToLoad;

    move-object v0, p1

    check-cast v0, Lcom/squareup/ui/help/MessagingViewState;

    :goto_0
    return-object v0

    :cond_5
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 34
    check-cast p1, Lcom/squareup/ui/help/messages/MessagingEligibilityState;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/help/messages/HelpshiftMessagingController$messagingViewState$1;->apply(Lcom/squareup/ui/help/messages/MessagingEligibilityState;)Lcom/squareup/ui/help/MessagingViewState;

    move-result-object p1

    return-object p1
.end method
