.class final Lcom/squareup/ui/help/tutorials/NewTutorialNotificationsSource$notifications$1$1;
.super Ljava/lang/Object;
.source "NewTutorialNotificationsSource.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/help/tutorials/NewTutorialNotificationsSource$notifications$1;->apply(Ljava/lang/Integer;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/notificationcenterdata/NotificationsSource$Result$Success;",
        "state",
        "Lcom/squareup/notificationcenterdata/Notification$State;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $count:Ljava/lang/Integer;

.field final synthetic this$0:Lcom/squareup/ui/help/tutorials/NewTutorialNotificationsSource$notifications$1;


# direct methods
.method constructor <init>(Lcom/squareup/ui/help/tutorials/NewTutorialNotificationsSource$notifications$1;Ljava/lang/Integer;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/help/tutorials/NewTutorialNotificationsSource$notifications$1$1;->this$0:Lcom/squareup/ui/help/tutorials/NewTutorialNotificationsSource$notifications$1;

    iput-object p2, p0, Lcom/squareup/ui/help/tutorials/NewTutorialNotificationsSource$notifications$1$1;->$count:Ljava/lang/Integer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/notificationcenterdata/Notification$State;)Lcom/squareup/notificationcenterdata/NotificationsSource$Result$Success;
    .locals 4

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    new-instance v0, Lcom/squareup/notificationcenterdata/NotificationsSource$Result$Success;

    .line 54
    iget-object v1, p0, Lcom/squareup/ui/help/tutorials/NewTutorialNotificationsSource$notifications$1$1;->$count:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->compare(II)I

    move-result v1

    if-lez v1, :cond_0

    .line 55
    iget-object v1, p0, Lcom/squareup/ui/help/tutorials/NewTutorialNotificationsSource$notifications$1$1;->this$0:Lcom/squareup/ui/help/tutorials/NewTutorialNotificationsSource$notifications$1;

    iget-object v1, v1, Lcom/squareup/ui/help/tutorials/NewTutorialNotificationsSource$notifications$1;->this$0:Lcom/squareup/ui/help/tutorials/NewTutorialNotificationsSource;

    iget-object v2, p0, Lcom/squareup/ui/help/tutorials/NewTutorialNotificationsSource$notifications$1$1;->$count:Ljava/lang/Integer;

    const-string v3, "count"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2, p1}, Lcom/squareup/ui/help/tutorials/NewTutorialNotificationsSource;->access$createNewTutorialNotification(Lcom/squareup/ui/help/tutorials/NewTutorialNotificationsSource;ILcom/squareup/notificationcenterdata/Notification$State;)Lcom/squareup/notificationcenterdata/Notification;

    move-result-object p1

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    goto :goto_0

    .line 57
    :cond_0
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p1

    .line 53
    :goto_0
    invoke-direct {v0, p1}, Lcom/squareup/notificationcenterdata/NotificationsSource$Result$Success;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 29
    check-cast p1, Lcom/squareup/notificationcenterdata/Notification$State;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/help/tutorials/NewTutorialNotificationsSource$notifications$1$1;->apply(Lcom/squareup/notificationcenterdata/Notification$State;)Lcom/squareup/notificationcenterdata/NotificationsSource$Result$Success;

    move-result-object p1

    return-object p1
.end method
