.class public Lcom/squareup/ui/help/announcements/events/LegacyViewMessageListEvent;
.super Lcom/squareup/analytics/event/v1/ViewEvent;
.source "LegacyViewMessageListEvent.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field public final tracker_tokens:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/server/messages/Message;",
            ">;)V"
        }
    .end annotation

    .line 14
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->MESSAGE_CENTER_LIST:Lcom/squareup/analytics/RegisterViewName;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/ViewEvent;-><init>(Lcom/squareup/analytics/EventNamedView;)V

    .line 15
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/squareup/ui/help/announcements/events/LegacyViewMessageListEvent;->tracker_tokens:Ljava/util/List;

    .line 16
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/messages/Message;

    .line 17
    iget-object v1, p0, Lcom/squareup/ui/help/announcements/events/LegacyViewMessageListEvent;->tracker_tokens:Ljava/util/List;

    iget-object v0, v0, Lcom/squareup/server/messages/Message;->tracker_token:Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-void
.end method
