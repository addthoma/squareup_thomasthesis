.class Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$MoreViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
.source "OrderHistoryCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MoreViewHolder"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;Landroid/view/View;)V
    .locals 0

    .line 313
    iput-object p1, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$MoreViewHolder;->this$0:Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;

    .line 314
    invoke-direct {p0, p2}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 315
    sget p1, Lcom/squareup/applet/help/R$id;->order_more:I

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    .line 316
    new-instance p2, Lcom/squareup/ui/help/orders/-$$Lambda$OrderHistoryCoordinator$MoreViewHolder$LexkmGWhB_tHXIlTxdriZeYitxI;

    invoke-direct {p2, p0}, Lcom/squareup/ui/help/orders/-$$Lambda$OrderHistoryCoordinator$MoreViewHolder$LexkmGWhB_tHXIlTxdriZeYitxI;-><init>(Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$MoreViewHolder;)V

    invoke-static {p2}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method public synthetic lambda$new$0$OrderHistoryCoordinator$MoreViewHolder(Landroid/view/View;)V
    .locals 1

    .line 316
    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$MoreViewHolder;->this$0:Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;->access$400(Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;)Lcom/squareup/ui/help/HelpAppletScopeRunner;

    move-result-object v0

    invoke-static {p1}, Lcom/squareup/util/Views;->getActivity(Landroid/view/View;)Landroid/app/Activity;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/help/HelpAppletScopeRunner;->seeMoreOrderHistory(Landroid/app/Activity;)V

    return-void
.end method
