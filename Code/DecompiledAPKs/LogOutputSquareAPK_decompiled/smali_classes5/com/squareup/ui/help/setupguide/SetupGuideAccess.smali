.class public final Lcom/squareup/ui/help/setupguide/SetupGuideAccess;
.super Lcom/squareup/applet/SectionAccess;
.source "SetupGuideAccess.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0005\u001a\u00020\u0006H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/ui/help/setupguide/SetupGuideAccess;",
        "Lcom/squareup/applet/SectionAccess;",
        "setupGuideVisibility",
        "Lcom/squareup/setupguide/SetupGuideVisibility;",
        "(Lcom/squareup/setupguide/SetupGuideVisibility;)V",
        "determineVisibility",
        "",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final setupGuideVisibility:Lcom/squareup/setupguide/SetupGuideVisibility;


# direct methods
.method public constructor <init>(Lcom/squareup/setupguide/SetupGuideVisibility;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "setupGuideVisibility"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Lcom/squareup/applet/SectionAccess;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/help/setupguide/SetupGuideAccess;->setupGuideVisibility:Lcom/squareup/setupguide/SetupGuideVisibility;

    return-void
.end method


# virtual methods
.method public determineVisibility()Z
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/squareup/ui/help/setupguide/SetupGuideAccess;->setupGuideVisibility:Lcom/squareup/setupguide/SetupGuideVisibility;

    invoke-interface {v0}, Lcom/squareup/setupguide/SetupGuideVisibility;->shouldShowSetupGuideInSupport()Z

    move-result v0

    return v0
.end method
