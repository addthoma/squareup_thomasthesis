.class public final Lcom/squareup/ui/help/about/LibrariesListAdapter$LicenseHeaderViewHolder;
.super Lcom/squareup/ui/help/about/LibrariesListAdapter$LibrariesListViewHolder;
.source "LibrariesListAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/help/about/LibrariesListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LicenseHeaderViewHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/help/about/LibrariesListAdapter$LibrariesListViewHolder<",
        "Lcom/squareup/ui/help/about/LibrariesListAdapter$Item$LicenseHeader;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLibrariesListAdapter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LibrariesListAdapter.kt\ncom/squareup/ui/help/about/LibrariesListAdapter$LicenseHeaderViewHolder\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,163:1\n1103#2,7:164\n*E\n*S KotlinDebug\n*F\n+ 1 LibrariesListAdapter.kt\ncom/squareup/ui/help/about/LibrariesListAdapter$LicenseHeaderViewHolder\n*L\n125#1,7:164\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0004\u0008\u0000\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B)\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0012\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\u0008\u00a2\u0006\u0002\u0010\u000bJ\u0010\u0010\u000c\u001a\u00020\n2\u0006\u0010\r\u001a\u00020\u0002H\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/ui/help/about/LibrariesListAdapter$LicenseHeaderViewHolder;",
        "Lcom/squareup/ui/help/about/LibrariesListAdapter$LibrariesListViewHolder;",
        "Lcom/squareup/ui/help/about/LibrariesListAdapter$Item$LicenseHeader;",
        "headerView",
        "Lcom/squareup/marketfont/MarketTextView;",
        "canFollowLinks",
        "",
        "onLicenseClicked",
        "Lkotlin/Function1;",
        "Lcom/squareup/ui/help/about/License;",
        "",
        "(Lcom/squareup/marketfont/MarketTextView;ZLkotlin/jvm/functions/Function1;)V",
        "bindItem",
        "item",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final canFollowLinks:Z

.field private final headerView:Lcom/squareup/marketfont/MarketTextView;

.field private final onLicenseClicked:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/ui/help/about/License;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/marketfont/MarketTextView;ZLkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/marketfont/MarketTextView;",
            "Z",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/ui/help/about/License;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "headerView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onLicenseClicked"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 119
    move-object v0, p1

    check-cast v0, Landroid/view/View;

    .line 118
    invoke-direct {p0, v0}, Lcom/squareup/ui/help/about/LibrariesListAdapter$LibrariesListViewHolder;-><init>(Landroid/view/View;)V

    iput-object p1, p0, Lcom/squareup/ui/help/about/LibrariesListAdapter$LicenseHeaderViewHolder;->headerView:Lcom/squareup/marketfont/MarketTextView;

    iput-boolean p2, p0, Lcom/squareup/ui/help/about/LibrariesListAdapter$LicenseHeaderViewHolder;->canFollowLinks:Z

    iput-object p3, p0, Lcom/squareup/ui/help/about/LibrariesListAdapter$LicenseHeaderViewHolder;->onLicenseClicked:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public static final synthetic access$getOnLicenseClicked$p(Lcom/squareup/ui/help/about/LibrariesListAdapter$LicenseHeaderViewHolder;)Lkotlin/jvm/functions/Function1;
    .locals 0

    .line 114
    iget-object p0, p0, Lcom/squareup/ui/help/about/LibrariesListAdapter$LicenseHeaderViewHolder;->onLicenseClicked:Lkotlin/jvm/functions/Function1;

    return-object p0
.end method


# virtual methods
.method public bindItem(Lcom/squareup/ui/help/about/LibrariesListAdapter$Item$LicenseHeader;)V
    .locals 2

    const-string v0, "item"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 122
    invoke-virtual {p1}, Lcom/squareup/ui/help/about/LibrariesListAdapter$Item$LicenseHeader;->getLicense()Lcom/squareup/ui/help/about/License;

    move-result-object p1

    .line 123
    iget-object v0, p0, Lcom/squareup/ui/help/about/LibrariesListAdapter$LicenseHeaderViewHolder;->headerView:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p1}, Lcom/squareup/ui/help/about/License;->getLicenseName()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 124
    iget-boolean v0, p0, Lcom/squareup/ui/help/about/LibrariesListAdapter$LicenseHeaderViewHolder;->canFollowLinks:Z

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/squareup/ui/help/about/LibrariesListAdapter$LicenseHeaderViewHolder;->headerView:Lcom/squareup/marketfont/MarketTextView;

    check-cast v0, Landroid/view/View;

    .line 164
    new-instance v1, Lcom/squareup/ui/help/about/LibrariesListAdapter$LicenseHeaderViewHolder$bindItem$$inlined$onClickDebounced$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/help/about/LibrariesListAdapter$LicenseHeaderViewHolder$bindItem$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/ui/help/about/LibrariesListAdapter$LicenseHeaderViewHolder;Lcom/squareup/ui/help/about/License;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method

.method public bridge synthetic bindItem(Lcom/squareup/ui/help/about/LibrariesListAdapter$Item;)V
    .locals 0

    .line 114
    check-cast p1, Lcom/squareup/ui/help/about/LibrariesListAdapter$Item$LicenseHeader;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/help/about/LibrariesListAdapter$LicenseHeaderViewHolder;->bindItem(Lcom/squareup/ui/help/about/LibrariesListAdapter$Item$LicenseHeader;)V

    return-void
.end method
