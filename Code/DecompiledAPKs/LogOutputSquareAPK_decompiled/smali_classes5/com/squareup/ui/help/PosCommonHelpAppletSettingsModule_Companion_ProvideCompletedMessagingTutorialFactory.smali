.class public final Lcom/squareup/ui/help/PosCommonHelpAppletSettingsModule_Companion_ProvideCompletedMessagingTutorialFactory;
.super Ljava/lang/Object;
.source "PosCommonHelpAppletSettingsModule_Companion_ProvideCompletedMessagingTutorialFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/f2prateek/rx/preferences2/Preference<",
        "Ljava/lang/Boolean;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final rxPrefsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/ui/help/PosCommonHelpAppletSettingsModule_Companion_ProvideCompletedMessagingTutorialFactory;->rxPrefsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/ui/help/PosCommonHelpAppletSettingsModule_Companion_ProvideCompletedMessagingTutorialFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ">;)",
            "Lcom/squareup/ui/help/PosCommonHelpAppletSettingsModule_Companion_ProvideCompletedMessagingTutorialFactory;"
        }
    .end annotation

    .line 33
    new-instance v0, Lcom/squareup/ui/help/PosCommonHelpAppletSettingsModule_Companion_ProvideCompletedMessagingTutorialFactory;

    invoke-direct {v0, p0}, Lcom/squareup/ui/help/PosCommonHelpAppletSettingsModule_Companion_ProvideCompletedMessagingTutorialFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideCompletedMessagingTutorial(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 37
    sget-object v0, Lcom/squareup/ui/help/PosCommonHelpAppletSettingsModule;->Companion:Lcom/squareup/ui/help/PosCommonHelpAppletSettingsModule$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/help/PosCommonHelpAppletSettingsModule$Companion;->provideCompletedMessagingTutorial(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/f2prateek/rx/preferences2/Preference;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/f2prateek/rx/preferences2/Preference;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 28
    iget-object v0, p0, Lcom/squareup/ui/help/PosCommonHelpAppletSettingsModule_Companion_ProvideCompletedMessagingTutorialFactory;->rxPrefsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;

    invoke-static {v0}, Lcom/squareup/ui/help/PosCommonHelpAppletSettingsModule_Companion_ProvideCompletedMessagingTutorialFactory;->provideCompletedMessagingTutorial(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/ui/help/PosCommonHelpAppletSettingsModule_Companion_ProvideCompletedMessagingTutorialFactory;->get()Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object v0

    return-object v0
.end method
