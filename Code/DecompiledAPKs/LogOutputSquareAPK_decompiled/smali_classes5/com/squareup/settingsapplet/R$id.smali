.class public final Lcom/squareup/settingsapplet/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/settingsapplet/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final account_error_message:I = 0x7f0a0129

.field public static final add_money_instrument:I = 0x7f0a016e

.field public static final add_money_section:I = 0x7f0a016f

.field public static final add_tax_button:I = 0x7f0a0172

.field public static final add_tax_row:I = 0x7f0a0173

.field public static final additional_auth_slip_container:I = 0x7f0a0175

.field public static final aditional_auth_slip:I = 0x7f0a0189

.field public static final all:I = 0x7f0a01ae

.field public static final always_skip_signature:I = 0x7f0a01b5

.field public static final am_pm_picker:I = 0x7f0a01b6

.field public static final automatic_itemized_receipt_switch:I = 0x7f0a01df

.field public static final automatic_ticket_name_hint:I = 0x7f0a01e5

.field public static final automatic_ticket_names:I = 0x7f0a01e6

.field public static final bank_account_settings_view:I = 0x7f0a0216

.field public static final banner:I = 0x7f0a0217

.field public static final barcode_scanners_list:I = 0x7f0a021e

.field public static final business_address_edit_view:I = 0x7f0a0265

.field public static final business_address_help_text:I = 0x7f0a0266

.field public static final business_address_section:I = 0x7f0a026c

.field public static final cancel_verification_button:I = 0x7f0a029d

.field public static final cash_drawer_settings_list:I = 0x7f0a02e9

.field public static final categories_list_layout:I = 0x7f0a02f2

.field public static final categories_progress:I = 0x7f0a02f3

.field public static final change_account_button:I = 0x7f0a0301

.field public static final change_bank_account_button:I = 0x7f0a0302

.field public static final check_compatibility_reader_button:I = 0x7f0a0311

.field public static final check_row_check:I = 0x7f0a0313

.field public static final check_row_title:I = 0x7f0a0314

.field public static final close_of_day_pickers:I = 0x7f0a035d

.field public static final close_of_day_row:I = 0x7f0a035e

.field public static final confirm_button:I = 0x7f0a038b

.field public static final connect_reader_wirelessly:I = 0x7f0a0391

.field public static final contactless_learn_more_view:I = 0x7f0a039c

.field public static final container:I = 0x7f0a03a1

.field public static final content:I = 0x7f0a03a3

.field public static final contents:I = 0x7f0a03a8

.field public static final create_passcode_action_bar:I = 0x7f0a03c8

.field public static final create_passcode_container:I = 0x7f0a03c9

.field public static final create_passcode_description:I = 0x7f0a03ca

.field public static final create_passcode_digit_group:I = 0x7f0a03cb

.field public static final create_passcode_header:I = 0x7f0a03cc

.field public static final create_passcode_pin_pad:I = 0x7f0a03cd

.field public static final create_passcode_progress_bar:I = 0x7f0a03ce

.field public static final create_passcode_progress_bar_spinner:I = 0x7f0a03cf

.field public static final create_passcode_progress_bar_title:I = 0x7f0a03d0

.field public static final create_passcode_star_group:I = 0x7f0a03d1

.field public static final create_passcode_success_action_bar:I = 0x7f0a03d2

.field public static final create_passcode_success_button:I = 0x7f0a03d3

.field public static final create_passcode_success_container:I = 0x7f0a03d4

.field public static final create_passcode_success_description:I = 0x7f0a03d5

.field public static final create_passcode_success_glyph:I = 0x7f0a03d6

.field public static final create_passcode_success_progress_bar:I = 0x7f0a03d7

.field public static final create_passcode_success_progress_bar_spinner:I = 0x7f0a03d8

.field public static final create_passcode_success_progress_bar_title:I = 0x7f0a03d9

.field public static final create_ticket_group_button:I = 0x7f0a03da

.field public static final crm_customer_management_in_cart_toggle:I = 0x7f0a042a

.field public static final crm_customer_management_post_transaction_label:I = 0x7f0a042b

.field public static final crm_customer_management_post_transaction_save_card_label:I = 0x7f0a042c

.field public static final crm_customer_management_post_transaction_save_card_toggle:I = 0x7f0a042d

.field public static final crm_customer_management_post_transaction_toggle:I = 0x7f0a042e

.field public static final crm_customer_management_save_card_divider:I = 0x7f0a042f

.field public static final crm_customer_management_save_card_label:I = 0x7f0a0430

.field public static final crm_customer_management_save_card_post_transaction_container:I = 0x7f0a0431

.field public static final crm_customer_management_save_card_post_transaction_divider:I = 0x7f0a0432

.field public static final crm_customer_management_save_card_toggle:I = 0x7f0a0433

.field public static final crm_email_collection_settings_screen_hint:I = 0x7f0a043a

.field public static final crm_email_collection_settings_screen_toggle:I = 0x7f0a043b

.field public static final custom_tax_enabled_row:I = 0x7f0a0527

.field public static final custom_ticket_name_hint:I = 0x7f0a0529

.field public static final custom_ticket_names:I = 0x7f0a052a

.field public static final custom_tip_row:I = 0x7f0a052f

.field public static final customer_checkout_divider:I = 0x7f0a0535

.field public static final customer_checkout_settings_group:I = 0x7f0a0536

.field public static final day_of_week_picker:I = 0x7f0a0557

.field public static final delete_tax_button:I = 0x7f0a0564

.field public static final delete_ticket_group_button:I = 0x7f0a0565

.field public static final deposit_options_hint:I = 0x7f0a056b

.field public static final deposit_schedule:I = 0x7f0a056c

.field public static final deposit_schedule_automatic:I = 0x7f0a056d

.field public static final deposit_schedule_checkable_group:I = 0x7f0a056e

.field public static final deposit_schedule_error_message:I = 0x7f0a056f

.field public static final deposit_schedule_hint:I = 0x7f0a0570

.field public static final deposit_schedule_manual:I = 0x7f0a0571

.field public static final deposit_schedule_rows:I = 0x7f0a0572

.field public static final deposit_schedule_title:I = 0x7f0a0573

.field public static final deposit_speed_checkable_group:I = 0x7f0a0574

.field public static final deposit_speed_custom:I = 0x7f0a0575

.field public static final deposit_speed_hint:I = 0x7f0a0576

.field public static final deposit_speed_one_to_two_business_days:I = 0x7f0a0578

.field public static final deposit_speed_same_day:I = 0x7f0a0579

.field public static final deposit_speed_title:I = 0x7f0a057a

.field public static final device_name_field:I = 0x7f0a05b2

.field public static final device_name_layout:I = 0x7f0a05b3

.field public static final dismiss_button:I = 0x7f0a05d6

.field public static final editor:I = 0x7f0a0690

.field public static final editor_wrapper:I = 0x7f0a0691

.field public static final egiftcard_design_checkmark:I = 0x7f0a069c

.field public static final egiftcard_design_delete_button:I = 0x7f0a069d

.field public static final egiftcard_design_image:I = 0x7f0a069e

.field public static final egiftcard_design_image_frame:I = 0x7f0a069f

.field public static final egiftcard_design_settings_add:I = 0x7f0a06a0

.field public static final egiftcard_designs_image:I = 0x7f0a06a1

.field public static final egiftcard_designs_image_frame:I = 0x7f0a06a2

.field public static final egiftcard_designs_image_loading:I = 0x7f0a06a3

.field public static final egiftcard_designs_not_valid_image:I = 0x7f0a06a4

.field public static final egiftcard_designs_upload_custom:I = 0x7f0a06a5

.field public static final egiftcard_designs_upload_custom_not_available:I = 0x7f0a06a6

.field public static final egiftcard_settings_design_recyclerview:I = 0x7f0a06ac

.field public static final employee_management_enabled_toggle:I = 0x7f0a06de

.field public static final employee_management_enabled_toggle_description:I = 0x7f0a06df

.field public static final employee_management_learn_more:I = 0x7f0a06e0

.field public static final employee_management_mode_toggle_guest:I = 0x7f0a06e1

.field public static final employee_management_timeout_option_toggle_1m:I = 0x7f0a06e2

.field public static final employee_management_timeout_option_toggle_30s:I = 0x7f0a06e3

.field public static final employee_management_timeout_option_toggle_5m:I = 0x7f0a06e4

.field public static final employee_management_timeout_option_toggle_never:I = 0x7f0a06e5

.field public static final employee_management_track_time_toggle:I = 0x7f0a06e6

.field public static final employee_management_track_time_toggle_description:I = 0x7f0a06e7

.field public static final employee_management_transaction_lock_mode_toggle:I = 0x7f0a06e8

.field public static final empty_view:I = 0x7f0a06f6

.field public static final emv_liability_hint:I = 0x7f0a06f8

.field public static final enable_store_and_forward:I = 0x7f0a0704

.field public static final enable_swipe_chip_cards:I = 0x7f0a0705

.field public static final exclude_row:I = 0x7f0a072f

.field public static final first_bank_account:I = 0x7f0a0759

.field public static final giftcards_settings_custom_policy:I = 0x7f0a07a5

.field public static final giftcards_settings_designs_row:I = 0x7f0a07a6

.field public static final giftcards_settings_egift_max:I = 0x7f0a07a7

.field public static final giftcards_settings_egift_min:I = 0x7f0a07a8

.field public static final giftcards_settings_egiftcard_content:I = 0x7f0a07a9

.field public static final giftcards_settings_enable_sell_egift_in_pos:I = 0x7f0a07aa

.field public static final giftcards_settings_loading:I = 0x7f0a07ab

.field public static final giftcards_settings_plastic_description:I = 0x7f0a07ac

.field public static final giftcards_settings_scroll_view:I = 0x7f0a07ad

.field public static final hardware_printer_list_view:I = 0x7f0a07c5

.field public static final hardware_printer_select:I = 0x7f0a07c6

.field public static final hour_picker:I = 0x7f0a07e6

.field public static final image_tile:I = 0x7f0a0825

.field public static final image_tile_radio:I = 0x7f0a0827

.field public static final include_on_ticket_container:I = 0x7f0a082a

.field public static final include_row:I = 0x7f0a082b

.field public static final instant_transfers_divider:I = 0x7f0a0859

.field public static final instant_transfers_hint:I = 0x7f0a085a

.field public static final instant_transfers_instrument:I = 0x7f0a085b

.field public static final instant_transfers_section:I = 0x7f0a085c

.field public static final instant_transfers_set_up_button:I = 0x7f0a085d

.field public static final instant_transfers_toggle:I = 0x7f0a085e

.field public static final learn_more_magstripe:I = 0x7f0a091c

.field public static final learn_more_r12:I = 0x7f0a091d

.field public static final legacy_signature_available_options:I = 0x7f0a0925

.field public static final linked_bank_account_section:I = 0x7f0a0947

.field public static final loyalty_settings_enable_loyalty:I = 0x7f0a0999

.field public static final loyalty_settings_enabled_content:I = 0x7f0a099a

.field public static final loyalty_settings_front_of_transaction_section:I = 0x7f0a099b

.field public static final loyalty_settings_front_of_transaction_toggle:I = 0x7f0a099c

.field public static final loyalty_settings_show_nonqualifying:I = 0x7f0a099d

.field public static final loyalty_settings_timeout_30:I = 0x7f0a099e

.field public static final loyalty_settings_timeout_60:I = 0x7f0a099f

.field public static final loyalty_settings_timeout_90:I = 0x7f0a09a0

.field public static final magstripe_learn_more_view:I = 0x7f0a09a7

.field public static final merchant_profile_animator:I = 0x7f0a09ca

.field public static final merchant_profile_content:I = 0x7f0a09cb

.field public static final merchant_profile_content_progress:I = 0x7f0a09cc

.field public static final merchant_profile_error:I = 0x7f0a09cd

.field public static final merchant_profile_id_for_state_restoration:I = 0x7f0a09ce

.field public static final merchant_profile_retry:I = 0x7f0a09cf

.field public static final message_button:I = 0x7f0a09d5

.field public static final message_text:I = 0x7f0a09d8

.field public static final mobile_business_help_text:I = 0x7f0a09df

.field public static final name_field:I = 0x7f0a0a0d

.field public static final never_skip_signature:I = 0x7f0a0a1b

.field public static final new_station:I = 0x7f0a0a1f

.field public static final no_signature_tooltip:I = 0x7f0a0a2f

.field public static final none:I = 0x7f0a0a45

.field public static final offline_payments_hint:I = 0x7f0a0a82

.field public static final open_tickets_as_home_screen_toggle:I = 0x7f0a0ab3

.field public static final open_tickets_as_home_screen_toggle_hint:I = 0x7f0a0ab4

.field public static final open_tickets_recycler_view:I = 0x7f0a0ab6

.field public static final open_tickets_toggle:I = 0x7f0a0ab7

.field public static final open_tickets_toggle_hint:I = 0x7f0a0ab8

.field public static final order_contactless_reader_button:I = 0x7f0a0ac7

.field public static final order_magstripe_reader_button:I = 0x7f0a0ad3

.field public static final order_ticket_autonumber_switch:I = 0x7f0a0aed

.field public static final order_ticket_custom_name_switch:I = 0x7f0a0aee

.field public static final orderhub_alert_settings_enabled:I = 0x7f0a0aff

.field public static final orderhub_alert_settings_explanation:I = 0x7f0a0b00

.field public static final orderhub_alert_settings_frequency_1:I = 0x7f0a0b01

.field public static final orderhub_alert_settings_frequency_2:I = 0x7f0a0b02

.field public static final orderhub_alert_settings_frequency_3:I = 0x7f0a0b03

.field public static final orderhub_alert_settings_frequency_4:I = 0x7f0a0b04

.field public static final orderhub_alert_settings_frequency_container:I = 0x7f0a0b05

.field public static final orderhub_printing_settings_enabled:I = 0x7f0a0b68

.field public static final orderhub_printing_settings_explanation:I = 0x7f0a0b69

.field public static final orderhub_quick_actions_settings_enabled:I = 0x7f0a0b6a

.field public static final orderhub_quick_actions_settings_explanation:I = 0x7f0a0b6b

.field public static final paper_signature_available_options:I = 0x7f0a0ba5

.field public static final paper_signature_divider:I = 0x7f0a0ba6

.field public static final paper_signature_group:I = 0x7f0a0ba7

.field public static final paper_signature_receipt_options:I = 0x7f0a0ba8

.field public static final passcode_employee_management_container:I = 0x7f0a0bb2

.field public static final passcode_employee_management_options:I = 0x7f0a0bb3

.field public static final passcode_settings_action_bar:I = 0x7f0a0bb4

.field public static final passcode_settings_scroll_view:I = 0x7f0a0bb5

.field public static final passcode_settings_timeout_action_bar:I = 0x7f0a0bb6

.field public static final passcode_settings_timeout_radio_group:I = 0x7f0a0bb7

.field public static final passcodes_settings_after_each_sale_check:I = 0x7f0a0bbc

.field public static final passcodes_settings_after_logout_check:I = 0x7f0a0bbd

.field public static final passcodes_settings_after_logout_description:I = 0x7f0a0bbe

.field public static final passcodes_settings_after_timeout:I = 0x7f0a0bbf

.field public static final passcodes_settings_back_out_of_sale_check:I = 0x7f0a0bc0

.field public static final passcodes_settings_create_or_edit_passcode:I = 0x7f0a0bc1

.field public static final passcodes_settings_create_or_edit_passcode_success:I = 0x7f0a0bc2

.field public static final passcodes_settings_enable_section:I = 0x7f0a0bc3

.field public static final passcodes_settings_enable_switch:I = 0x7f0a0bc4

.field public static final passcodes_settings_enable_switch_description:I = 0x7f0a0bc5

.field public static final passcodes_settings_require_passcode_section:I = 0x7f0a0bc6

.field public static final passcodes_settings_team_passcode:I = 0x7f0a0bc7

.field public static final passcodes_settings_team_passcode_description:I = 0x7f0a0bc8

.field public static final passcodes_settings_team_passcode_section:I = 0x7f0a0bc9

.field public static final passcodes_settings_team_passcode_switch:I = 0x7f0a0bca

.field public static final passcodes_settings_team_permissions:I = 0x7f0a0bcb

.field public static final passcodes_settings_timeout:I = 0x7f0a0bcc

.field public static final passcodes_settings_timeout_1_minute_radio:I = 0x7f0a0bcd

.field public static final passcodes_settings_timeout_30_seconds_radio:I = 0x7f0a0bce

.field public static final passcodes_settings_timeout_5_minutes_radio:I = 0x7f0a0bcf

.field public static final passcodes_settings_timeout_description:I = 0x7f0a0bd0

.field public static final passcodes_settings_timeout_never_radio:I = 0x7f0a0bd1

.field public static final payment_types_settings_header_title:I = 0x7f0a0c01

.field public static final payment_types_settings_info_message:I = 0x7f0a0c02

.field public static final payment_types_settings_instructions:I = 0x7f0a0c03

.field public static final payment_types_settings_preview:I = 0x7f0a0c04

.field public static final payment_types_settings_recycler_view:I = 0x7f0a0c05

.field public static final payroll_learn_more:I = 0x7f0a0c06

.field public static final percent_character:I = 0x7f0a0c0b

.field public static final predefined_ticket_groups_header:I = 0x7f0a0c44

.field public static final predefined_tickets_recycler_view:I = 0x7f0a0c46

.field public static final predefined_tickets_toggle:I = 0x7f0a0c47

.field public static final predefined_tickets_toggle_hint:I = 0x7f0a0c48

.field public static final print_a_ticket_for_each_item_switch:I = 0x7f0a0c61

.field public static final print_compact_tickets_switch:I = 0x7f0a0c63

.field public static final print_order_ticket_stubs_switch:I = 0x7f0a0c69

.field public static final print_order_ticket_stubs_unavailable:I = 0x7f0a0c6a

.field public static final print_order_tickets_message:I = 0x7f0a0c6b

.field public static final print_order_tickets_no_categories_message:I = 0x7f0a0c6c

.field public static final print_order_tickets_switch:I = 0x7f0a0c6d

.field public static final print_receipts_hint:I = 0x7f0a0c6f

.field public static final print_receipts_switch:I = 0x7f0a0c70

.field public static final print_receipts_unavailable:I = 0x7f0a0c71

.field public static final printer_station_name:I = 0x7f0a0c75

.field public static final printer_station_top_divider:I = 0x7f0a0c76

.field public static final printer_stations_list:I = 0x7f0a0c77

.field public static final public_profile_address:I = 0x7f0a0c86

.field public static final public_profile_address_edit_button:I = 0x7f0a0c87

.field public static final public_profile_address_help_text:I = 0x7f0a0c88

.field public static final public_profile_address_save_button:I = 0x7f0a0c89

.field public static final public_profile_business_name:I = 0x7f0a0c8a

.field public static final public_profile_description:I = 0x7f0a0c8b

.field public static final public_profile_edit_address:I = 0x7f0a0c8c

.field public static final public_profile_edit_mobile_business:I = 0x7f0a0c8d

.field public static final public_profile_email:I = 0x7f0a0c8e

.field public static final public_profile_email_suggestion_box:I = 0x7f0a0c8f

.field public static final public_profile_facebook:I = 0x7f0a0c90

.field public static final public_profile_featured:I = 0x7f0a0c91

.field public static final public_profile_logo_container:I = 0x7f0a0c92

.field public static final public_profile_logo_image:I = 0x7f0a0c93

.field public static final public_profile_logo_message:I = 0x7f0a0c94

.field public static final public_profile_logo_text:I = 0x7f0a0c95

.field public static final public_profile_mobile_business:I = 0x7f0a0c96

.field public static final public_profile_phone:I = 0x7f0a0c97

.field public static final public_profile_photo_on_receipt:I = 0x7f0a0c98

.field public static final public_profile_twitter:I = 0x7f0a0c99

.field public static final public_profile_website:I = 0x7f0a0c9a

.field public static final quick_tip:I = 0x7f0a0cb3

.field public static final quick_tip_hint:I = 0x7f0a0cb4

.field public static final reader_list:I = 0x7f0a0cee

.field public static final remove_printer_station:I = 0x7f0a0d5b

.field public static final resend_email_button:I = 0x7f0a0d6d

.field public static final root:I = 0x7f0a0d94

.field public static final sample_image_tile:I = 0x7f0a0dfe

.field public static final sample_text_tile:I = 0x7f0a0dff

.field public static final scroll_view:I = 0x7f0a0e16

.field public static final second_bank_account:I = 0x7f0a0e29

.field public static final section_list:I = 0x7f0a0e3a

.field public static final settings_applet_section_id_for_state_restoration:I = 0x7f0a0e6a

.field public static final shared_setting_message_view:I = 0x7f0a0e77

.field public static final sign_on_device:I = 0x7f0a0e87

.field public static final sign_on_device_hint:I = 0x7f0a0e88

.field public static final sign_on_printed_receipt:I = 0x7f0a0e89

.field public static final sign_out_button:I = 0x7f0a0e8a

.field public static final sign_skip_under_amount:I = 0x7f0a0e8b

.field public static final sign_skip_under_amount_tooltip:I = 0x7f0a0e8c

.field public static final signature_available_options:I = 0x7f0a0e8e

.field public static final skip_itemized_cart:I = 0x7f0a0e9d

.field public static final skip_itemized_cart_hint:I = 0x7f0a0e9e

.field public static final skip_payment_type_hint:I = 0x7f0a0e9f

.field public static final skip_payment_type_selection:I = 0x7f0a0ea0

.field public static final skip_receipt_divider:I = 0x7f0a0ea1

.field public static final skip_receipt_screen:I = 0x7f0a0ea2

.field public static final skip_receipt_screen_hint:I = 0x7f0a0ea3

.field public static final spinner:I = 0x7f0a0ebc

.field public static final store_and_forward_section_view:I = 0x7f0a0f40

.field public static final tax_applicable_items_row:I = 0x7f0a0f71

.field public static final tax_applicable_list:I = 0x7f0a0f72

.field public static final tax_applicable_progress_bar:I = 0x7f0a0f73

.field public static final tax_applicable_services_row:I = 0x7f0a0f74

.field public static final tax_detail_content:I = 0x7f0a0f77

.field public static final tax_detail_progress_bar:I = 0x7f0a0f78

.field public static final tax_enabled_row:I = 0x7f0a0f79

.field public static final tax_fee_type_container:I = 0x7f0a0f7a

.field public static final tax_item_pricing_row:I = 0x7f0a0f7b

.field public static final tax_name_row:I = 0x7f0a0f7c

.field public static final tax_percentage_row:I = 0x7f0a0f7d

.field public static final tax_type_row:I = 0x7f0a0f7e

.field public static final taxes_list_view:I = 0x7f0a0f81

.field public static final taxes_progress_bar:I = 0x7f0a0f82

.field public static final test_print:I = 0x7f0a0f93

.field public static final text_tile:I = 0x7f0a0fa2

.field public static final text_tile_radio:I = 0x7f0a0fa6

.field public static final ticket_count_row:I = 0x7f0a0fb0

.field public static final ticket_name_method:I = 0x7f0a0fbe

.field public static final ticket_name_options_container:I = 0x7f0a0fbf

.field public static final ticket_template_delete:I = 0x7f0a0fcb

.field public static final ticket_template_drag_handle:I = 0x7f0a0fcc

.field public static final ticket_template_name:I = 0x7f0a0fcd

.field public static final tile_choice:I = 0x7f0a0fdb

.field public static final tile_divider:I = 0x7f0a0fdc

.field public static final time_tracking_settings_action_bar:I = 0x7f0a0fe2

.field public static final time_tracking_settings_enable_switch:I = 0x7f0a0fe3

.field public static final time_tracking_settings_enable_switch_description:I = 0x7f0a0fe4

.field public static final tip_amount_first:I = 0x7f0a1032

.field public static final tip_amount_second:I = 0x7f0a1033

.field public static final tip_amount_third:I = 0x7f0a1034

.field public static final tip_post_taxes_toggle:I = 0x7f0a103a

.field public static final tip_pre_taxes_toggle:I = 0x7f0a103b

.field public static final tip_separate_screen_row:I = 0x7f0a103c

.field public static final tip_smart_row:I = 0x7f0a103d

.field public static final tips_collect_row:I = 0x7f0a103e

.field public static final title:I = 0x7f0a103f

.field public static final traditional_receipt:I = 0x7f0a1063

.field public static final transaction_limit:I = 0x7f0a106b

.field public static final transaction_limit_hint:I = 0x7f0a106c

.field public static final transaction_limit_section:I = 0x7f0a106d

.field public static final tutorial_video_link_r12:I = 0x7f0a10a8

.field public static final under_amount_skip_signature:I = 0x7f0a10ae

.field public static final updated_employee_management_container:I = 0x7f0a10c5

.field public static final updated_employee_management_passcode_description:I = 0x7f0a10c6

.field public static final updated_employee_management_passcode_toggle_always:I = 0x7f0a10c7

.field public static final updated_employee_management_passcode_toggle_never:I = 0x7f0a10c8

.field public static final updated_employee_management_passcode_toggle_restricted:I = 0x7f0a10c9

.field public static final updated_employee_management_timeout_toggle_1m:I = 0x7f0a10ca

.field public static final updated_employee_management_timeout_toggle_30s:I = 0x7f0a10cb

.field public static final updated_employee_management_timeout_toggle_5m:I = 0x7f0a10cc

.field public static final updated_employee_management_timeout_toggle_container:I = 0x7f0a10cd

.field public static final updated_employee_management_timeout_toggle_never:I = 0x7f0a10ce

.field public static final updated_employee_management_transaction_lock_mode_toggle:I = 0x7f0a10cf

.field public static final verification_hint:I = 0x7f0a10e8

.field public static final warning_message:I = 0x7f0a1111

.field public static final weekend_balance_button:I = 0x7f0a1119

.field public static final weekend_balance_hint:I = 0x7f0a111a


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
