.class public final Lcom/squareup/toast/ToastTokenFix;
.super Ljava/lang/Object;
.source "ToastTokenFix.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/toast/ToastTokenFix$ToastTnField;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nToastTokenFix.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ToastTokenFix.kt\ncom/squareup/toast/ToastTokenFix\n+ 2 _Arrays.kt\nkotlin/collections/ArraysKt___ArraysKt\n*L\n1#1,196:1\n1203#2,2:197\n10725#2,2:199\n1060#2,2:201\n*E\n*S KotlinDebug\n*F\n+ 1 ToastTokenFix.kt\ncom/squareup/toast/ToastTokenFix\n*L\n102#1,2:197\n106#1,2:199\n108#1,2:201\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\u0008\u00c7\u0002\u0018\u00002\u00020\u0001:\u0001\u000bB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\n\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0002J\u0008\u0010\u0007\u001a\u00020\u0008H\u0007J\u0010\u0010\t\u001a\u00020\u00082\u0006\u0010\n\u001a\u00020\u0006H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/toast/ToastTokenFix;",
        "",
        "()V",
        "fixApplied",
        "",
        "lookupToastTnFields",
        "Lcom/squareup/toast/ToastTokenFix$ToastTnField;",
        "swallowToastBadTokenExceptions",
        "",
        "swapINotificationManager",
        "fieldWrapper",
        "ToastTnField",
        "android-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/toast/ToastTokenFix;

.field private static fixApplied:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 64
    new-instance v0, Lcom/squareup/toast/ToastTokenFix;

    invoke-direct {v0}, Lcom/squareup/toast/ToastTokenFix;-><init>()V

    sput-object v0, Lcom/squareup/toast/ToastTokenFix;->INSTANCE:Lcom/squareup/toast/ToastTokenFix;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final lookupToastTnFields()Lcom/squareup/toast/ToastTokenFix$ToastTnField;
    .locals 11

    const-string v0, "android.widget.Toast$TN"

    .line 97
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-string v1, "Class.forName(\"android.widget.Toast\\$TN\")"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 98
    invoke-virtual {v0}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v0

    const-string/jumbo v1, "toastTNclass.declaredFields"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 197
    array-length v1, v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    const-string v4, "it"

    const/4 v5, 0x0

    if-ge v3, v1, :cond_1

    aget-object v6, v0, v3

    .line 102
    invoke-static {v6, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, "mShow"

    invoke-static {v7, v8}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    move-object v6, v5

    :goto_1
    const/4 v1, 0x1

    if-eqz v6, :cond_2

    .line 104
    invoke-virtual {v6, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    goto :goto_2

    :cond_2
    move-object v6, v5

    .line 199
    :goto_2
    array-length v3, v0

    const/4 v7, 0x0

    :goto_3
    if-ge v7, v3, :cond_4

    aget-object v8, v0, v7

    .line 106
    invoke-static {v8, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v8

    const-string v9, "SHOW"

    invoke-static {v8, v9}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    const/4 v3, 0x1

    goto :goto_4

    :cond_3
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    :cond_4
    const/4 v3, 0x0

    .line 201
    :goto_4
    array-length v7, v0

    :goto_5
    if-ge v2, v7, :cond_8

    aget-object v8, v0, v2

    .line 108
    invoke-static {v8, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v9

    const-string v10, "mHandler"

    invoke-static {v9, v10}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 109
    invoke-virtual {v8, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    if-eqz v6, :cond_5

    .line 112
    new-instance v0, Lcom/squareup/toast/ToastTokenFix$ToastTnField$TnMShowField;

    invoke-direct {v0, v6}, Lcom/squareup/toast/ToastTokenFix$ToastTnField$TnMShowField;-><init>(Ljava/lang/reflect/Field;)V

    check-cast v0, Lcom/squareup/toast/ToastTokenFix$ToastTnField;

    goto :goto_6

    :cond_5
    if-eqz v8, :cond_6

    if-eqz v3, :cond_6

    .line 114
    new-instance v5, Lcom/squareup/toast/ToastTokenFix$ToastTnField$TnMHandlerfield;

    invoke-direct {v5, v8}, Lcom/squareup/toast/ToastTokenFix$ToastTnField$TnMHandlerfield;-><init>(Ljava/lang/reflect/Field;)V

    .line 113
    :cond_6
    move-object v0, v5

    check-cast v0, Lcom/squareup/toast/ToastTokenFix$ToastTnField;

    :goto_6
    return-object v0

    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 202
    :cond_8
    new-instance v0, Ljava/util/NoSuchElementException;

    const-string v1, "Array contains no element matching the predicate."

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public static final swallowToastBadTokenExceptions()V
    .locals 4
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 71
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x19

    if-eq v0, v1, :cond_0

    return-void

    .line 74
    :cond_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    const-string v2, "Looper.getMainLooper()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    if-nez v0, :cond_3

    .line 79
    sget-boolean v0, Lcom/squareup/toast/ToastTokenFix;->fixApplied:Z

    if-eqz v0, :cond_1

    return-void

    .line 82
    :cond_1
    sput-boolean v1, Lcom/squareup/toast/ToastTokenFix;->fixApplied:Z

    const/4 v0, 0x0

    .line 85
    :try_start_0
    sget-object v1, Lcom/squareup/toast/ToastTokenFix;->INSTANCE:Lcom/squareup/toast/ToastTokenFix;

    invoke-direct {v1}, Lcom/squareup/toast/ToastTokenFix;->lookupToastTnFields()Lcom/squareup/toast/ToastTokenFix$ToastTnField;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 87
    sget-object v2, Lcom/squareup/toast/ToastTokenFix;->INSTANCE:Lcom/squareup/toast/ToastTokenFix;

    invoke-direct {v2, v1}, Lcom/squareup/toast/ToastTokenFix;->swapINotificationManager(Lcom/squareup/toast/ToastTokenFix$ToastTnField;)V

    goto :goto_0

    :cond_2
    const-string v1, "Could not apply patch to fix Toast BadTokenException"

    new-array v2, v0, [Ljava/lang/Object;

    .line 89
    invoke-static {v1, v2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    new-array v0, v0, [Ljava/lang/Object;

    const-string v2, "Exception when trying to apply BadTokenException patch, ignored."

    .line 92
    invoke-static {v1, v2, v0}, Ltimber/log/Timber;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void

    .line 75
    :cond_3
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    .line 76
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Should be called from the main thread, not "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    const-string v3, "Thread.currentThread()"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 75
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method private final swapINotificationManager(Lcom/squareup/toast/ToastTokenFix$ToastTnField;)V
    .locals 9

    .line 124
    const-class v0, Landroid/widget/Toast;

    const/4 v1, 0x0

    new-array v2, v1, [Ljava/lang/Class;

    const-string v3, "getService"

    .line 125
    invoke-virtual {v0, v3, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    const-string/jumbo v3, "toastClass.getDeclaredMethod(\"getService\")"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x1

    .line 126
    invoke-virtual {v2, v3}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    new-array v4, v1, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 127
    invoke-virtual {v2, v5, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    const-string v4, "android.app.INotificationManager"

    .line 129
    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    const-string v6, "Class.forName(\"android.app.INotificationManager\")"

    invoke-static {v4, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v6, "android.app.ITransientNotification"

    .line 130
    invoke-static {v6}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v6

    const-string v7, "Class.forName(\"android.a\u2026.ITransientNotification\")"

    invoke-static {v6, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Class;

    .line 132
    const-class v8, Ljava/lang/String;

    aput-object v8, v7, v1

    aput-object v6, v7, v3

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v8, 0x2

    aput-object v6, v7, v8

    const-string v6, "enqueueToast"

    .line 131
    invoke-virtual {v4, v6, v7}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v6

    const-string v7, "iNotificationManagerClas\u2026ss, Int::class.java\n    )"

    invoke-static {v6, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 136
    invoke-virtual {v4}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v7

    new-array v8, v3, [Ljava/lang/Class;

    aput-object v4, v8, v1

    .line 137
    new-instance v1, Lcom/squareup/toast/ToastTokenFix$swapINotificationManager$proxy$1;

    invoke-direct {v1, v6, p1, v2}, Lcom/squareup/toast/ToastTokenFix$swapINotificationManager$proxy$1;-><init>(Ljava/lang/reflect/Method;Lcom/squareup/toast/ToastTokenFix$ToastTnField;Ljava/lang/Object;)V

    check-cast v1, Ljava/lang/reflect/InvocationHandler;

    .line 135
    invoke-static {v7, v8, v1}, Ljava/lang/reflect/Proxy;->newProxyInstance(Ljava/lang/ClassLoader;[Ljava/lang/Class;Ljava/lang/reflect/InvocationHandler;)Ljava/lang/Object;

    move-result-object p1

    const-string v1, "Proxy.newProxyInstance(\n\u2026ealService)\n      }\n    }"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "sService"

    .line 148
    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    const-string/jumbo v1, "toastClass.getDeclaredField(\"sService\")"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 149
    invoke-virtual {v0, v3}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 150
    invoke-virtual {v0, v5, p1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method
