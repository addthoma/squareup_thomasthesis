.class final Lcom/squareup/signature/Point$Timestamped;
.super Lcom/squareup/signature/Point;
.source "Point.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/signature/Point;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "Timestamped"
.end annotation


# instance fields
.field final time:J


# direct methods
.method constructor <init>(FFJ)V
    .locals 0

    .line 42
    invoke-direct {p0, p1, p2}, Lcom/squareup/signature/Point;-><init>(FF)V

    .line 43
    iput-wide p3, p0, Lcom/squareup/signature/Point$Timestamped;->time:J

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    .line 51
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/signature/Point$Timestamped;->x:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/signature/Point$Timestamped;->y:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ") @ "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/signature/Point$Timestamped;->time:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method velocityFrom(Lcom/squareup/signature/Point$Timestamped;)F
    .locals 5

    .line 47
    invoke-virtual {p0, p1}, Lcom/squareup/signature/Point$Timestamped;->distanceTo(Lcom/squareup/signature/Point;)F

    move-result v0

    iget-wide v1, p0, Lcom/squareup/signature/Point$Timestamped;->time:J

    iget-wide v3, p1, Lcom/squareup/signature/Point$Timestamped;->time:J

    sub-long/2addr v1, v3

    long-to-float p1, v1

    div-float/2addr v0, p1

    return v0
.end method
