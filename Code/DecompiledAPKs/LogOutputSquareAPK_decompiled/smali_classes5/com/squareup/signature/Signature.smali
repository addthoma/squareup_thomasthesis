.class public Lcom/squareup/signature/Signature;
.super Ljava/lang/Object;
.source "Signature.java"

# interfaces
.implements Lcom/squareup/signature/SignatureAsJson;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/signature/Signature$Pickle;,
        Lcom/squareup/signature/Signature$Glyph;,
        Lcom/squareup/signature/Signature$Rerenderer;
    }
.end annotation


# static fields
.field private static final gson:Lcom/google/gson/Gson;


# instance fields
.field public final color:I

.field public final glyphs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/signature/Signature$Glyph;",
            ">;"
        }
    .end annotation
.end field

.field public final height:I

.field public final width:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 64
    new-instance v0, Lcom/google/gson/Gson;

    invoke-direct {v0}, Lcom/google/gson/Gson;-><init>()V

    sput-object v0, Lcom/squareup/signature/Signature;->gson:Lcom/google/gson/Gson;

    return-void
.end method

.method public constructor <init>(III)V
    .locals 0

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    iput p1, p0, Lcom/squareup/signature/Signature;->width:I

    .line 73
    iput p2, p0, Lcom/squareup/signature/Signature;->height:I

    .line 74
    iput p3, p0, Lcom/squareup/signature/Signature;->color:I

    .line 75
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/squareup/signature/Signature;->glyphs:Ljava/util/List;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/signature/Signature;)[[[I
    .locals 0

    .line 16
    invoke-direct {p0}, Lcom/squareup/signature/Signature;->toGlyphArray()[[[I

    move-result-object p0

    return-object p0
.end method

.method public static decode(Ljava/lang/String;Lcom/squareup/signature/Signature$Rerenderer;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Lcom/squareup/signature/Signature$Rerenderer<",
            "TT;>;)TT;"
        }
    .end annotation

    .line 55
    sget-object v0, Lcom/squareup/signature/Signature;->gson:Lcom/google/gson/Gson;

    const-class v1, Lcom/squareup/signature/Signature$Pickle;

    invoke-virtual {v0, p0, v1}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/signature/Signature$Pickle;

    .line 56
    new-instance v0, Lcom/squareup/signature/Signature;

    iget v1, p0, Lcom/squareup/signature/Signature$Pickle;->width:I

    iget v2, p0, Lcom/squareup/signature/Signature$Pickle;->height:I

    iget v3, p0, Lcom/squareup/signature/Signature$Pickle;->color:I

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/signature/Signature;-><init>(III)V

    .line 57
    iget-object p0, p0, Lcom/squareup/signature/Signature$Pickle;->glyphs:[[[I

    invoke-interface {p1, v0, p0}, Lcom/squareup/signature/Signature$Rerenderer;->rerender(Lcom/squareup/signature/Signature;[[[I)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private toGlyphArray()[[[I
    .locals 17

    move-object/from16 v0, p0

    .line 84
    iget-object v1, v0, Lcom/squareup/signature/Signature;->glyphs:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    const-wide/16 v3, 0x0

    goto :goto_0

    .line 85
    :cond_0
    iget-object v3, v0, Lcom/squareup/signature/Signature;->glyphs:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/signature/Signature$Glyph;

    iget-wide v3, v3, Lcom/squareup/signature/Signature$Glyph;->startTime:J

    .line 87
    :goto_0
    new-array v1, v1, [[[I

    .line 89
    iget-object v5, v0, Lcom/squareup/signature/Signature;->glyphs:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    :goto_1
    if-ge v6, v5, :cond_3

    .line 90
    iget-object v8, v0, Lcom/squareup/signature/Signature;->glyphs:Ljava/util/List;

    invoke-interface {v8, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/squareup/signature/Signature$Glyph;

    .line 91
    iget-object v8, v8, Lcom/squareup/signature/Signature$Glyph;->points:Ljava/util/List;

    .line 92
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v9

    .line 93
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10, v9}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v11, 0x0

    :goto_2
    if-ge v11, v9, :cond_1

    .line 95
    invoke-interface {v8, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    invoke-interface {v10, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    .line 97
    :cond_1
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v8

    new-array v8, v8, [[I

    aput-object v8, v1, v7

    .line 99
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v8

    const/4 v9, 0x0

    const/4 v11, 0x0

    :goto_3
    if-ge v9, v8, :cond_2

    .line 100
    invoke-interface {v10, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/squareup/signature/Point$Timestamped;

    .line 101
    aget-object v13, v1, v7

    add-int/lit8 v14, v11, 0x1

    const/4 v15, 0x3

    new-array v15, v15, [I

    iget v0, v12, Lcom/squareup/signature/Point$Timestamped;->x:F

    float-to-int v0, v0

    aput v0, v15, v2

    iget v0, v12, Lcom/squareup/signature/Point$Timestamped;->y:F

    float-to-int v0, v0

    const/16 v16, 0x1

    aput v0, v15, v16

    move-object/from16 v16, v1

    iget-wide v0, v12, Lcom/squareup/signature/Point$Timestamped;->time:J

    sub-long/2addr v0, v3

    long-to-int v1, v0

    const/4 v0, 0x2

    aput v1, v15, v0

    aput-object v15, v13, v11

    add-int/lit8 v9, v9, 0x1

    move-object/from16 v0, p0

    move v11, v14

    move-object/from16 v1, v16

    goto :goto_3

    :cond_2
    move-object/from16 v16, v1

    add-int/lit8 v7, v7, 0x1

    add-int/lit8 v6, v6, 0x1

    move-object/from16 v0, p0

    goto :goto_1

    :cond_3
    move-object/from16 v16, v1

    return-object v16
.end method


# virtual methods
.method public encode()Ljava/lang/String;
    .locals 2

    .line 80
    sget-object v0, Lcom/squareup/signature/Signature;->gson:Lcom/google/gson/Gson;

    new-instance v1, Lcom/squareup/signature/Signature$Pickle;

    invoke-direct {v1, p0}, Lcom/squareup/signature/Signature$Pickle;-><init>(Lcom/squareup/signature/Signature;)V

    invoke-virtual {v0, v1}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
