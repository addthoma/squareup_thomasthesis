.class public Lcom/squareup/signature/SignatureDeterminer$Builder;
.super Ljava/lang/Object;
.source "SignatureDeterminer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/signature/SignatureDeterminer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private cardEntryMethod:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

.field private hasIssuerRequestForSignature:Ljava/lang/Boolean;

.field private isFallbackSwipe:Ljava/lang/Boolean;

.field private isGiftCard:Ljava/lang/Boolean;

.field private merchantAlwaysSkipSignature:Ljava/lang/Boolean;

.field private merchantIsAllowedToNSR:Ljava/lang/Boolean;

.field private merchantOptedInToNSR:Ljava/lang/Boolean;

.field private showSignatureForCardNotPresent:Ljava/lang/Boolean;

.field private showSignatureForCardOnFile:Ljava/lang/Boolean;

.field private signatureRequiredThreshold:Lcom/squareup/protos/common/Money;

.field private smartReaderPresentAndRequestsSignature:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/signature/SignatureDeterminer$Builder;)Ljava/lang/Boolean;
    .locals 0

    .line 80
    iget-object p0, p0, Lcom/squareup/signature/SignatureDeterminer$Builder;->hasIssuerRequestForSignature:Ljava/lang/Boolean;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/signature/SignatureDeterminer$Builder;)Ljava/lang/Boolean;
    .locals 0

    .line 80
    iget-object p0, p0, Lcom/squareup/signature/SignatureDeterminer$Builder;->merchantAlwaysSkipSignature:Ljava/lang/Boolean;

    return-object p0
.end method

.method static synthetic access$1000(Lcom/squareup/signature/SignatureDeterminer$Builder;)Ljava/lang/Boolean;
    .locals 0

    .line 80
    iget-object p0, p0, Lcom/squareup/signature/SignatureDeterminer$Builder;->showSignatureForCardOnFile:Ljava/lang/Boolean;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/signature/SignatureDeterminer$Builder;)Ljava/lang/Boolean;
    .locals 0

    .line 80
    iget-object p0, p0, Lcom/squareup/signature/SignatureDeterminer$Builder;->merchantIsAllowedToNSR:Ljava/lang/Boolean;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/signature/SignatureDeterminer$Builder;)Ljava/lang/Boolean;
    .locals 0

    .line 80
    iget-object p0, p0, Lcom/squareup/signature/SignatureDeterminer$Builder;->merchantOptedInToNSR:Ljava/lang/Boolean;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/signature/SignatureDeterminer$Builder;)Lcom/squareup/protos/common/Money;
    .locals 0

    .line 80
    iget-object p0, p0, Lcom/squareup/signature/SignatureDeterminer$Builder;->signatureRequiredThreshold:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/signature/SignatureDeterminer$Builder;)Ljava/lang/Boolean;
    .locals 0

    .line 80
    iget-object p0, p0, Lcom/squareup/signature/SignatureDeterminer$Builder;->isGiftCard:Ljava/lang/Boolean;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/signature/SignatureDeterminer$Builder;)Ljava/lang/Boolean;
    .locals 0

    .line 80
    iget-object p0, p0, Lcom/squareup/signature/SignatureDeterminer$Builder;->isFallbackSwipe:Ljava/lang/Boolean;

    return-object p0
.end method

.method static synthetic access$700(Lcom/squareup/signature/SignatureDeterminer$Builder;)Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;
    .locals 0

    .line 80
    iget-object p0, p0, Lcom/squareup/signature/SignatureDeterminer$Builder;->cardEntryMethod:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    return-object p0
.end method

.method static synthetic access$800(Lcom/squareup/signature/SignatureDeterminer$Builder;)Ljava/lang/Boolean;
    .locals 0

    .line 80
    iget-object p0, p0, Lcom/squareup/signature/SignatureDeterminer$Builder;->smartReaderPresentAndRequestsSignature:Ljava/lang/Boolean;

    return-object p0
.end method

.method static synthetic access$900(Lcom/squareup/signature/SignatureDeterminer$Builder;)Ljava/lang/Boolean;
    .locals 0

    .line 80
    iget-object p0, p0, Lcom/squareup/signature/SignatureDeterminer$Builder;->showSignatureForCardNotPresent:Ljava/lang/Boolean;

    return-object p0
.end method

.method private checkSignatureDeterminerInvariants()V
    .locals 2

    .line 188
    iget-object v0, p0, Lcom/squareup/signature/SignatureDeterminer$Builder;->hasIssuerRequestForSignature:Ljava/lang/Boolean;

    const-string v1, "hasIssuerRequestForSignature"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 189
    iget-object v0, p0, Lcom/squareup/signature/SignatureDeterminer$Builder;->merchantIsAllowedToNSR:Ljava/lang/Boolean;

    const-string v1, "merchantIsAllowedToNSR"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 190
    iget-object v0, p0, Lcom/squareup/signature/SignatureDeterminer$Builder;->merchantOptedInToNSR:Ljava/lang/Boolean;

    const-string v1, "merchantOptedInToNSR"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 192
    iget-object v0, p0, Lcom/squareup/signature/SignatureDeterminer$Builder;->isGiftCard:Ljava/lang/Boolean;

    const-string v1, "isGiftCard"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 193
    iget-object v0, p0, Lcom/squareup/signature/SignatureDeterminer$Builder;->isFallbackSwipe:Ljava/lang/Boolean;

    const-string v1, "isFallbackSwipe"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 194
    iget-object v0, p0, Lcom/squareup/signature/SignatureDeterminer$Builder;->smartReaderPresentAndRequestsSignature:Ljava/lang/Boolean;

    const-string v1, "smartReaderPresentAndRequestsSignature"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 195
    iget-object v0, p0, Lcom/squareup/signature/SignatureDeterminer$Builder;->showSignatureForCardNotPresent:Ljava/lang/Boolean;

    const-string v1, "showSignatureForCardNotPresent"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 196
    iget-object v0, p0, Lcom/squareup/signature/SignatureDeterminer$Builder;->showSignatureForCardOnFile:Ljava/lang/Boolean;

    const-string v1, "showSignatureForCardOnFile"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 198
    iget-object v0, p0, Lcom/squareup/signature/SignatureDeterminer$Builder;->isGiftCard:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 199
    iget-object v0, p0, Lcom/squareup/signature/SignatureDeterminer$Builder;->cardEntryMethod:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->SWIPED:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/squareup/signature/SignatureDeterminer$Builder;->cardEntryMethod:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->KEYED:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/squareup/signature/SignatureDeterminer$Builder;->cardEntryMethod:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->ON_FILE:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    const-string v1, "Must be swiped, on file, or keyed (CNP) if gift card"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 205
    :cond_2
    iget-object v0, p0, Lcom/squareup/signature/SignatureDeterminer$Builder;->merchantIsAllowedToNSR:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/squareup/signature/SignatureDeterminer$Builder;->merchantOptedInToNSR:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 206
    iget-object v0, p0, Lcom/squareup/signature/SignatureDeterminer$Builder;->signatureRequiredThreshold:Lcom/squareup/protos/common/Money;

    const-string v1, "signatureRequiredThreshold"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    :cond_3
    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/signature/SignatureDeterminer;
    .locals 2

    .line 183
    invoke-direct {p0}, Lcom/squareup/signature/SignatureDeterminer$Builder;->checkSignatureDeterminerInvariants()V

    .line 184
    new-instance v0, Lcom/squareup/signature/SignatureDeterminer;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/signature/SignatureDeterminer;-><init>(Lcom/squareup/signature/SignatureDeterminer$Builder;Lcom/squareup/signature/SignatureDeterminer$1;)V

    return-object v0
.end method

.method public cardEntryMethod(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)Lcom/squareup/signature/SignatureDeterminer$Builder;
    .locals 0

    .line 158
    iput-object p1, p0, Lcom/squareup/signature/SignatureDeterminer$Builder;->cardEntryMethod:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    return-object p0
.end method

.method public hasIssuerRequestForSignature(Z)Lcom/squareup/signature/SignatureDeterminer$Builder;
    .locals 0

    .line 98
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/signature/SignatureDeterminer$Builder;->hasIssuerRequestForSignature:Ljava/lang/Boolean;

    return-object p0
.end method

.method public isFallbackSwipe(Z)Lcom/squareup/signature/SignatureDeterminer$Builder;
    .locals 0

    .line 150
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/signature/SignatureDeterminer$Builder;->isFallbackSwipe:Ljava/lang/Boolean;

    return-object p0
.end method

.method public isGiftCard(Z)Lcom/squareup/signature/SignatureDeterminer$Builder;
    .locals 0

    .line 142
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/signature/SignatureDeterminer$Builder;->isGiftCard:Ljava/lang/Boolean;

    return-object p0
.end method

.method public merchantAlwaysSkipSignature(Z)Lcom/squareup/signature/SignatureDeterminer$Builder;
    .locals 0

    .line 107
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/signature/SignatureDeterminer$Builder;->merchantAlwaysSkipSignature:Ljava/lang/Boolean;

    return-object p0
.end method

.method public merchantIsAllowedToNSR(Z)Lcom/squareup/signature/SignatureDeterminer$Builder;
    .locals 0

    .line 116
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/signature/SignatureDeterminer$Builder;->merchantIsAllowedToNSR:Ljava/lang/Boolean;

    return-object p0
.end method

.method public merchantOptedInToNSR(Z)Lcom/squareup/signature/SignatureDeterminer$Builder;
    .locals 0

    .line 125
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/signature/SignatureDeterminer$Builder;->merchantOptedInToNSR:Ljava/lang/Boolean;

    return-object p0
.end method

.method public showSignatureForCardNotPresent(Z)Lcom/squareup/signature/SignatureDeterminer$Builder;
    .locals 0

    .line 173
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/signature/SignatureDeterminer$Builder;->showSignatureForCardNotPresent:Ljava/lang/Boolean;

    return-object p0
.end method

.method public showSignatureForCardOnFile(Z)Lcom/squareup/signature/SignatureDeterminer$Builder;
    .locals 0

    .line 178
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/signature/SignatureDeterminer$Builder;->showSignatureForCardOnFile:Ljava/lang/Boolean;

    return-object p0
.end method

.method public signatureRequiredThreshold(Lcom/squareup/protos/common/Money;)Lcom/squareup/signature/SignatureDeterminer$Builder;
    .locals 0

    .line 134
    iput-object p1, p0, Lcom/squareup/signature/SignatureDeterminer$Builder;->signatureRequiredThreshold:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public smartReaderPresentAndRequestsSignature(Z)Lcom/squareup/signature/SignatureDeterminer$Builder;
    .locals 0

    .line 168
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/signature/SignatureDeterminer$Builder;->smartReaderPresentAndRequestsSignature:Ljava/lang/Boolean;

    return-object p0
.end method
