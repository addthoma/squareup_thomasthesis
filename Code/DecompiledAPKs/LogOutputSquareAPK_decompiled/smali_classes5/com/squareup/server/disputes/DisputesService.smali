.class public interface abstract Lcom/squareup/server/disputes/DisputesService;
.super Ljava/lang/Object;
.source "DisputesService.java"


# virtual methods
.method public abstract getDisputeSummaryAndListDisputes(Lcom/squareup/protos/client/cbms/ListDisputesRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/cbms/ListDisputesRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/cbms/ListDisputesRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/Headers;
        value = {
            "Accept: application/x-protobuf"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/disputes/get-dispute-summary-and-list-disputes"
    .end annotation
.end method

.method public abstract getForm(Lcom/squareup/protos/client/irf/GetFormRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/irf/GetFormRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/irf/GetFormRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/irf/GetFormResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/Headers;
        value = {
            "Accept: application/x-protobuf"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/irf/get-form"
    .end annotation
.end method

.method public abstract listResolvedDisputes(Lcom/squareup/protos/client/cbms/ListDisputesRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/cbms/ListDisputesRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/cbms/ListDisputesRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/cbms/ListDisputesResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/Headers;
        value = {
            "Accept: application/x-protobuf"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/disputes/list-resolved-disputes"
    .end annotation
.end method

.method public abstract saveForm(Lcom/squareup/protos/client/irf/SaveFormRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/irf/SaveFormRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/irf/SaveFormRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/irf/SaveFormResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/Headers;
        value = {
            "Accept: application/x-protobuf"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/irf/save-form"
    .end annotation
.end method
