.class public interface abstract Lcom/squareup/server/crm/RolodexService;
.super Ljava/lang/Object;
.source "RolodexService.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00ec\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0006H\'J\u0018\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020\tH\'J\u0018\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u000cH\'J\u0018\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u000fH\'J\u0018\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0012H\'J\u0018\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00140\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0015H\'J\u0018\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00170\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0018H\'J\"\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u001b0\u001a2\u0008\u0008\u0001\u0010\u001c\u001a\u00020\u001d2\u0008\u0008\u0001\u0010\u001e\u001a\u00020\u001fH\'J\u0018\u0010 \u001a\u0008\u0012\u0004\u0012\u00020!0\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020\"H\'J\u0018\u0010#\u001a\u0008\u0012\u0004\u0012\u00020$0\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020%H\'J\u0018\u0010&\u001a\u0008\u0012\u0004\u0012\u00020\'0\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020(H\'J\u0018\u0010)\u001a\u0008\u0012\u0004\u0012\u00020*0\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020+H\'J\u0018\u0010,\u001a\u0008\u0012\u0004\u0012\u00020-0\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020.H\'J\u0018\u0010/\u001a\u0008\u0012\u0004\u0012\u0002000\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u000201H\'J\u0018\u00102\u001a\u0008\u0012\u0004\u0012\u0002030\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u000204H\'J\u0018\u00105\u001a\u0008\u0012\u0004\u0012\u0002060\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u000207H\'J\u0018\u00108\u001a\u0008\u0012\u0004\u0012\u0002090\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020:H\'J\u0018\u0010;\u001a\u0008\u0012\u0004\u0012\u00020<0\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020=H\'J\u0018\u0010>\u001a\u0008\u0012\u0004\u0012\u00020?0\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020@H\'J\u0018\u0010A\u001a\u0008\u0012\u0004\u0012\u00020B0\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020CH\'J\u0018\u0010D\u001a\u0008\u0012\u0004\u0012\u00020E0\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020FH\'J\u0018\u0010G\u001a\u0008\u0012\u0004\u0012\u00020H0\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020IH\'J\u0018\u0010J\u001a\u0008\u0012\u0004\u0012\u00020K0\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020LH\'J\u0018\u0010M\u001a\u0008\u0012\u0004\u0012\u00020N0\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020OH\'J\u0018\u0010P\u001a\u0008\u0012\u0004\u0012\u00020Q0\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020RH\'J\u0018\u0010S\u001a\u0008\u0012\u0004\u0012\u00020T0\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020UH\'J6\u0010V\u001a\u0008\u0012\u0004\u0012\u00020\u001b0\u001a2\u0008\u0008\u0001\u0010W\u001a\u00020X2\u0008\u0008\u0001\u0010Y\u001a\u00020X2\u0008\u0008\u0001\u0010Z\u001a\u00020X2\u0008\u0008\u0001\u0010[\u001a\u00020\\H\'J\u0018\u0010]\u001a\u0008\u0012\u0004\u0012\u00020^0\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020_H\'J\u0018\u0010`\u001a\u0008\u0012\u0004\u0012\u00020a0\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020bH\'J\u0018\u0010c\u001a\u0008\u0012\u0004\u0012\u00020d0\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020eH\'J\u0018\u0010f\u001a\u0008\u0012\u0004\u0012\u00020g0\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020hH\'J\u0018\u0010i\u001a\u0008\u0012\u0004\u0012\u00020j0\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020kH\'J\u0018\u0010l\u001a\u0008\u0012\u0004\u0012\u00020m0\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020nH\'\u00a8\u0006o"
    }
    d2 = {
        "Lcom/squareup/server/crm/RolodexService;",
        "",
        "addContactsToGroups",
        "Lcom/squareup/server/StatusResponse;",
        "Lcom/squareup/protos/client/rolodex/AddContactsToGroupsResponse;",
        "request",
        "Lcom/squareup/protos/client/rolodex/AddContactsToGroupsRequest;",
        "deleteAttachment",
        "Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse;",
        "Lcom/squareup/protos/client/rolodex/DeleteAttachmentsRequest;",
        "deleteContact",
        "Lcom/squareup/protos/client/rolodex/DeleteContactResponse;",
        "Lcom/squareup/protos/client/rolodex/DeleteContactRequest;",
        "deleteContacts",
        "Lcom/squareup/protos/client/rolodex/DeleteContactsResponse;",
        "Lcom/squareup/protos/client/rolodex/DeleteContactsRequest;",
        "deleteGroup",
        "Lcom/squareup/protos/client/rolodex/DeleteGroupResponse;",
        "Lcom/squareup/protos/client/rolodex/DeleteGroupRequest;",
        "deleteNote",
        "Lcom/squareup/protos/client/rolodex/DeleteNoteResponse;",
        "Lcom/squareup/protos/client/rolodex/DeleteNoteRequest;",
        "dismissMergeProposal",
        "Lcom/squareup/protos/client/rolodex/DismissMergeProposalResponse;",
        "Lcom/squareup/protos/client/rolodex/DismissMergeProposalRequest;",
        "downloadAttachment",
        "Lcom/squareup/server/AcceptedResponse;",
        "Lokhttp3/ResponseBody;",
        "tokens",
        "",
        "isPreview",
        "",
        "getContact",
        "Lcom/squareup/protos/client/rolodex/GetContactResponse;",
        "Lcom/squareup/protos/client/rolodex/GetContactRequest;",
        "getManualMergeProposal",
        "Lcom/squareup/protos/client/rolodex/ListManualMergeProposalResponse;",
        "Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest;",
        "getMerchant",
        "Lcom/squareup/protos/client/rolodex/GetMerchantResponse;",
        "Lcom/squareup/protos/client/rolodex/GetMerchantRequest;",
        "getMergeAllStatus",
        "Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse;",
        "Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest;",
        "getMergeProposalStatus",
        "Lcom/squareup/protos/client/rolodex/GetMergeProposalStatusResponse;",
        "Lcom/squareup/protos/client/rolodex/GetMergeProposalStatusRequest;",
        "listContacts",
        "Lcom/squareup/protos/client/rolodex/ListContactsResponse;",
        "Lcom/squareup/protos/client/rolodex/ListContactsRequest;",
        "listEventsForContact",
        "Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse;",
        "Lcom/squareup/protos/client/rolodex/ListEventsForContactRequest;",
        "listFilterTemplates",
        "Lcom/squareup/protos/client/rolodex/ListFilterTemplatesResponse;",
        "Lcom/squareup/protos/client/rolodex/ListFilterTemplatesRequest;",
        "listGroups",
        "Lcom/squareup/protos/client/rolodex/ListGroupsResponse;",
        "Lcom/squareup/protos/client/rolodex/ListGroupsRequest;",
        "listMergeProposals",
        "Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;",
        "Lcom/squareup/protos/client/rolodex/ListMergeProposalRequest;",
        "manualMergeContacts",
        "Lcom/squareup/protos/client/rolodex/ManualMergeContactsResponse;",
        "Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;",
        "setContact",
        "Lcom/squareup/protos/client/rolodex/SetContactForPaymentResponse;",
        "Lcom/squareup/protos/client/rolodex/SetContactForPaymentRequest;",
        "shouldShowEmailCollection",
        "Lcom/squareup/protos/client/rolodex/ShouldShowEmailCollectionResponse;",
        "Lcom/squareup/protos/client/rolodex/ShouldShowEmailCollectionRequest;",
        "triggerMergeAll",
        "Lcom/squareup/protos/client/rolodex/TriggerMergeAllResponse;",
        "Lcom/squareup/protos/client/rolodex/TriggerMergeAllRequest;",
        "triggerMergeProposal",
        "Lcom/squareup/protos/client/rolodex/TriggerMergeProposalResponse;",
        "Lcom/squareup/protos/client/rolodex/TriggerMergeProposalRequest;",
        "unlinkInstrumentOnFile",
        "Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileResponse;",
        "Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest;",
        "updateAttachment",
        "Lcom/squareup/protos/client/rolodex/UpdateAttachmentResponse;",
        "Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest;",
        "updateAttributeSchema",
        "Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaResponse;",
        "Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest;",
        "uploadAttachment",
        "contactToken",
        "Lokhttp3/RequestBody;",
        "attachmentToken",
        "simplePayload",
        "content",
        "Lokhttp3/MultipartBody$Part;",
        "upsertContact",
        "Lcom/squareup/protos/client/rolodex/UpsertContactResponse;",
        "Lcom/squareup/protos/client/rolodex/UpsertContactRequest;",
        "upsertContactForEmailCollection",
        "Lcom/squareup/protos/client/rolodex/UpsertContactForEmailCollectionResponse;",
        "Lcom/squareup/protos/client/rolodex/UpsertContactForEmailCollectionRequest;",
        "upsertGroup",
        "Lcom/squareup/protos/client/rolodex/UpsertGroupResponse;",
        "Lcom/squareup/protos/client/rolodex/UpsertGroupRequest;",
        "upsertGroupV2",
        "Lcom/squareup/protos/client/rolodex/UpsertGroupV2Response;",
        "Lcom/squareup/protos/client/rolodex/UpsertGroupV2Request;",
        "upsertNote",
        "Lcom/squareup/protos/client/rolodex/UpsertNoteResponse;",
        "Lcom/squareup/protos/client/rolodex/UpsertNoteRequest;",
        "verifyAndLinkInstrument",
        "Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentResponse;",
        "Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract addContactsToGroups(Lcom/squareup/protos/client/rolodex/AddContactsToGroupsRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/rolodex/AddContactsToGroupsRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/AddContactsToGroupsRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/rolodex/AddContactsToGroupsResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/rolodex/v2/add-contacts-to-groups"
    .end annotation
.end method

.method public abstract deleteAttachment(Lcom/squareup/protos/client/rolodex/DeleteAttachmentsRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/rolodex/DeleteAttachmentsRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/DeleteAttachmentsRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/rolodex/v2/delete-attachments"
    .end annotation
.end method

.method public abstract deleteContact(Lcom/squareup/protos/client/rolodex/DeleteContactRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/rolodex/DeleteContactRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/DeleteContactRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/rolodex/DeleteContactResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/rolodex/v2/delete-contact"
    .end annotation
.end method

.method public abstract deleteContacts(Lcom/squareup/protos/client/rolodex/DeleteContactsRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/rolodex/DeleteContactsRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/DeleteContactsRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/rolodex/DeleteContactsResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/rolodex/v2/delete-contacts"
    .end annotation
.end method

.method public abstract deleteGroup(Lcom/squareup/protos/client/rolodex/DeleteGroupRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/rolodex/DeleteGroupRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/DeleteGroupRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/rolodex/DeleteGroupResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/rolodex/v2/delete-group"
    .end annotation
.end method

.method public abstract deleteNote(Lcom/squareup/protos/client/rolodex/DeleteNoteRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/rolodex/DeleteNoteRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/DeleteNoteRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/rolodex/DeleteNoteResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/rolodex/v2/delete-note"
    .end annotation
.end method

.method public abstract dismissMergeProposal(Lcom/squareup/protos/client/rolodex/DismissMergeProposalRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/rolodex/DismissMergeProposalRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/DismissMergeProposalRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/rolodex/DismissMergeProposalResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/rolodex/v2/dismiss-merge-proposal"
    .end annotation
.end method

.method public abstract downloadAttachment(Ljava/lang/String;Z)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Query;
            value = "attachment_tokens"
        .end annotation
    .end param
    .param p2    # Z
        .annotation runtime Lretrofit2/http/Query;
            value = "preview"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lokhttp3/ResponseBody;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/GET;
        value = "/1.0/rolodex/download-attachments"
    .end annotation
.end method

.method public abstract getContact(Lcom/squareup/protos/client/rolodex/GetContactRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/rolodex/GetContactRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/GetContactRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/rolodex/GetContactResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/rolodex/v2/get-contact"
    .end annotation
.end method

.method public abstract getManualMergeProposal(Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/ListManualMergeProposalRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/rolodex/ListManualMergeProposalResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/rolodex/v2/list-manual-merge-proposal"
    .end annotation
.end method

.method public abstract getMerchant(Lcom/squareup/protos/client/rolodex/GetMerchantRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/rolodex/GetMerchantRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/GetMerchantRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/rolodex/GetMerchantResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/rolodex/v2/get-merchant"
    .end annotation
.end method

.method public abstract getMergeAllStatus(Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/GetMergeAllStatusRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/rolodex/v2/get-merge-all-status"
    .end annotation
.end method

.method public abstract getMergeProposalStatus(Lcom/squareup/protos/client/rolodex/GetMergeProposalStatusRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/rolodex/GetMergeProposalStatusRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/GetMergeProposalStatusRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/rolodex/GetMergeProposalStatusResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/rolodex/v2/get-merge-proposal-status"
    .end annotation
.end method

.method public abstract listContacts(Lcom/squareup/protos/client/rolodex/ListContactsRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/rolodex/ListContactsRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/ListContactsRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/rolodex/ListContactsResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/rolodex/v2/list-contacts"
    .end annotation
.end method

.method public abstract listEventsForContact(Lcom/squareup/protos/client/rolodex/ListEventsForContactRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/rolodex/ListEventsForContactRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/ListEventsForContactRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/rolodex/ListEventsForContactResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/rolodex/v2/list-events-for-contact"
    .end annotation
.end method

.method public abstract listFilterTemplates(Lcom/squareup/protos/client/rolodex/ListFilterTemplatesRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/rolodex/ListFilterTemplatesRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/ListFilterTemplatesRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/rolodex/ListFilterTemplatesResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/rolodex/v2/list-filter-templates"
    .end annotation
.end method

.method public abstract listGroups(Lcom/squareup/protos/client/rolodex/ListGroupsRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/rolodex/ListGroupsRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/ListGroupsRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/rolodex/ListGroupsResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/rolodex/v2/list-groups"
    .end annotation
.end method

.method public abstract listMergeProposals(Lcom/squareup/protos/client/rolodex/ListMergeProposalRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/rolodex/ListMergeProposalRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/ListMergeProposalRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/rolodex/v2/list-merge-proposal"
    .end annotation
.end method

.method public abstract manualMergeContacts(Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/ManualMergeContactsRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/rolodex/ManualMergeContactsResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/rolodex/v2/manual-merge"
    .end annotation
.end method

.method public abstract setContact(Lcom/squareup/protos/client/rolodex/SetContactForPaymentRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/rolodex/SetContactForPaymentRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/SetContactForPaymentRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/rolodex/SetContactForPaymentResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/rolodex/v2/set-contact-for-payment"
    .end annotation
.end method

.method public abstract shouldShowEmailCollection(Lcom/squareup/protos/client/rolodex/ShouldShowEmailCollectionRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/rolodex/ShouldShowEmailCollectionRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/ShouldShowEmailCollectionRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/rolodex/ShouldShowEmailCollectionResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/rolodex/v2/should-show-email-collection"
    .end annotation
.end method

.method public abstract triggerMergeAll(Lcom/squareup/protos/client/rolodex/TriggerMergeAllRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/rolodex/TriggerMergeAllRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/TriggerMergeAllRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/rolodex/TriggerMergeAllResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/rolodex/v2/trigger-merge-all"
    .end annotation
.end method

.method public abstract triggerMergeProposal(Lcom/squareup/protos/client/rolodex/TriggerMergeProposalRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/rolodex/TriggerMergeProposalRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/TriggerMergeProposalRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/rolodex/TriggerMergeProposalResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/rolodex/v2/trigger-merge-proposal"
    .end annotation
.end method

.method public abstract unlinkInstrumentOnFile(Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/rolodex/v2/unlink-instrument"
    .end annotation
.end method

.method public abstract updateAttachment(Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/rolodex/UpdateAttachmentResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/rolodex/v2/update-attachment"
    .end annotation
.end method

.method public abstract updateAttributeSchema(Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/rolodex/v2/update-attribute-schema"
    .end annotation
.end method

.method public abstract uploadAttachment(Lokhttp3/RequestBody;Lokhttp3/RequestBody;Lokhttp3/RequestBody;Lokhttp3/MultipartBody$Part;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Lokhttp3/RequestBody;
        .annotation runtime Lretrofit2/http/Part;
            value = "contact_token"
        .end annotation
    .end param
    .param p2    # Lokhttp3/RequestBody;
        .annotation runtime Lretrofit2/http/Part;
            value = "attachment_token"
        .end annotation
    .end param
    .param p3    # Lokhttp3/RequestBody;
        .annotation runtime Lretrofit2/http/Part;
            value = "simple_payload"
        .end annotation
    .end param
    .param p4    # Lokhttp3/MultipartBody$Part;
        .annotation runtime Lretrofit2/http/Part;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lokhttp3/RequestBody;",
            "Lokhttp3/RequestBody;",
            "Lokhttp3/RequestBody;",
            "Lokhttp3/MultipartBody$Part;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lokhttp3/ResponseBody;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/Multipart;
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/rolodex/upload-attachments"
    .end annotation
.end method

.method public abstract upsertContact(Lcom/squareup/protos/client/rolodex/UpsertContactRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/rolodex/UpsertContactRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/UpsertContactRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/rolodex/UpsertContactResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/rolodex/v2/upsert-contact"
    .end annotation
.end method

.method public abstract upsertContactForEmailCollection(Lcom/squareup/protos/client/rolodex/UpsertContactForEmailCollectionRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/rolodex/UpsertContactForEmailCollectionRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/UpsertContactForEmailCollectionRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/rolodex/UpsertContactForEmailCollectionResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/rolodex/v2/upsert-contact-for-email-collection"
    .end annotation
.end method

.method public abstract upsertGroup(Lcom/squareup/protos/client/rolodex/UpsertGroupRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/rolodex/UpsertGroupRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/UpsertGroupRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/rolodex/UpsertGroupResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/rolodex/v2/upsert-group"
    .end annotation
.end method

.method public abstract upsertGroupV2(Lcom/squareup/protos/client/rolodex/UpsertGroupV2Request;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/rolodex/UpsertGroupV2Request;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/UpsertGroupV2Request;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/rolodex/UpsertGroupV2Response;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/rolodex/v2/upsert-group-v2"
    .end annotation
.end method

.method public abstract upsertNote(Lcom/squareup/protos/client/rolodex/UpsertNoteRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/rolodex/UpsertNoteRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/UpsertNoteRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/rolodex/UpsertNoteResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/rolodex/v2/upsert-note"
    .end annotation
.end method

.method public abstract verifyAndLinkInstrument(Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/instruments/verify-and-link-instrument"
    .end annotation
.end method
