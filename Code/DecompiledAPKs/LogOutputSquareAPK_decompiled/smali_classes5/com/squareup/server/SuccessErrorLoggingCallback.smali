.class public abstract Lcom/squareup/server/SuccessErrorLoggingCallback;
.super Lcom/squareup/server/ErrorLoggingCallback;
.source "SuccessErrorLoggingCallback.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/squareup/server/ErrorLoggingCallback<",
        "TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 7
    invoke-direct {p0, p1}, Lcom/squareup/server/ErrorLoggingCallback;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .line 15
    invoke-virtual {p0, p1}, Lcom/squareup/server/SuccessErrorLoggingCallback;->onSuccess(Ljava/lang/Object;)V

    return-void
.end method

.method public clientError(Ljava/lang/Object;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;I)V"
        }
    .end annotation

    .line 29
    invoke-super {p0, p1, p2}, Lcom/squareup/server/ErrorLoggingCallback;->clientError(Ljava/lang/Object;I)V

    .line 30
    invoke-virtual {p0}, Lcom/squareup/server/SuccessErrorLoggingCallback;->onError()V

    return-void
.end method

.method public networkError()V
    .locals 0

    .line 24
    invoke-super {p0}, Lcom/squareup/server/ErrorLoggingCallback;->networkError()V

    .line 25
    invoke-virtual {p0}, Lcom/squareup/server/SuccessErrorLoggingCallback;->onError()V

    return-void
.end method

.method public abstract onError()V
.end method

.method public abstract onSuccess(Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation
.end method

.method public serverError(I)V
    .locals 0

    .line 34
    invoke-super {p0, p1}, Lcom/squareup/server/ErrorLoggingCallback;->serverError(I)V

    .line 35
    invoke-virtual {p0}, Lcom/squareup/server/SuccessErrorLoggingCallback;->onError()V

    return-void
.end method

.method public sessionExpired()V
    .locals 0

    .line 19
    invoke-super {p0}, Lcom/squareup/server/ErrorLoggingCallback;->sessionExpired()V

    .line 20
    invoke-virtual {p0}, Lcom/squareup/server/SuccessErrorLoggingCallback;->onError()V

    return-void
.end method
