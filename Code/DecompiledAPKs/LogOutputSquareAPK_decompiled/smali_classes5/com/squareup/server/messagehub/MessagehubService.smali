.class public interface abstract Lcom/squareup/server/messagehub/MessagehubService;
.super Ljava/lang/Object;
.source "MessagehubService.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0006H\'J\u0018\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020\tH\'\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/server/messagehub/MessagehubService;",
        "",
        "allowConversation",
        "Lcom/squareup/server/AcceptedResponse;",
        "Lcom/squareup/protos/messagehub/service/AllowConversationResponse;",
        "request",
        "Lcom/squareup/protos/messagehub/service/AllowConversationRequest;",
        "getMessagingToken",
        "Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;",
        "Lcom/squareup/protos/messagehub/service/GetMessagingTokenRequest;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract allowConversation(Lcom/squareup/protos/messagehub/service/AllowConversationRequest;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Lcom/squareup/protos/messagehub/service/AllowConversationRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/messagehub/service/AllowConversationRequest;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/protos/messagehub/service/AllowConversationResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/Headers;
        value = {
            "Accept: application/x-protobuf"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/messagehub/service/allow-conversation"
    .end annotation
.end method

.method public abstract getMessagingToken(Lcom/squareup/protos/messagehub/service/GetMessagingTokenRequest;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Lcom/squareup/protos/messagehub/service/GetMessagingTokenRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/messagehub/service/GetMessagingTokenRequest;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/Headers;
        value = {
            "Accept: application/x-protobuf"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/messagehub/service/messaging-token"
    .end annotation
.end method
