.class public Lcom/squareup/server/ExperimentsResponse$ExperimentConfigWrapper;
.super Ljava/lang/Object;
.source "ExperimentsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/ExperimentsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ExperimentConfigWrapper"
.end annotation


# instance fields
.field public final experiment:Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;


# direct methods
.method public constructor <init>(Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;)V
    .locals 0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfigWrapper;->experiment:Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_4

    .line 43
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_1

    .line 45
    :cond_1
    check-cast p1, Lcom/squareup/server/ExperimentsResponse$ExperimentConfigWrapper;

    .line 47
    iget-object v2, p0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfigWrapper;->experiment:Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;

    iget-object p1, p1, Lcom/squareup/server/ExperimentsResponse$ExperimentConfigWrapper;->experiment:Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;

    if-eqz v2, :cond_2

    invoke-virtual {v2, p1}, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_3

    goto :goto_0

    :cond_2
    if-eqz p1, :cond_3

    :goto_0
    return v1

    :cond_3
    return v0

    :cond_4
    :goto_1
    return v1
.end method

.method public hashCode()I
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfigWrapper;->experiment:Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
