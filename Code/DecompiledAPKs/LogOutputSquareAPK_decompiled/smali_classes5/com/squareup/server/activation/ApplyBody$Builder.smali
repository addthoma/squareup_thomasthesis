.class public Lcom/squareup/server/activation/ApplyBody$Builder;
.super Ljava/lang/Object;
.source "ApplyBody.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/activation/ApplyBody;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private businessType:Ljava/lang/String;

.field private business_name:Ljava/lang/String;

.field private circa:Ljava/lang/String;

.field private city:Ljava/lang/String;

.field private deviceIdMd5:Ljava/lang/String;

.field private deviceIdSha1:Ljava/lang/String;

.field private ein:Ljava/lang/String;

.field private estimated_revenue:Ljava/lang/String;

.field private firstName:Ljava/lang/String;

.field private installationIdMd5:Ljava/lang/String;

.field private installationIdSha1:Ljava/lang/String;

.field private lastName:Ljava/lang/String;

.field private phoneNumber:Ljava/lang/String;

.field private postalCode:Ljava/lang/String;

.field private ssnLastFour:Ljava/lang/String;

.field private state:Ljava/lang/String;

.field private street1:Ljava/lang/String;

.field private street2:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/activation/ApplyBody;
    .locals 23

    move-object/from16 v0, p0

    .line 180
    new-instance v21, Lcom/squareup/server/activation/ApplyBody;

    move-object/from16 v1, v21

    iget-object v2, v0, Lcom/squareup/server/activation/ApplyBody$Builder;->firstName:Ljava/lang/String;

    iget-object v3, v0, Lcom/squareup/server/activation/ApplyBody$Builder;->lastName:Ljava/lang/String;

    iget-object v4, v0, Lcom/squareup/server/activation/ApplyBody$Builder;->phoneNumber:Ljava/lang/String;

    iget-object v5, v0, Lcom/squareup/server/activation/ApplyBody$Builder;->street1:Ljava/lang/String;

    iget-object v6, v0, Lcom/squareup/server/activation/ApplyBody$Builder;->street2:Ljava/lang/String;

    iget-object v7, v0, Lcom/squareup/server/activation/ApplyBody$Builder;->city:Ljava/lang/String;

    iget-object v8, v0, Lcom/squareup/server/activation/ApplyBody$Builder;->state:Ljava/lang/String;

    iget-object v9, v0, Lcom/squareup/server/activation/ApplyBody$Builder;->postalCode:Ljava/lang/String;

    iget-object v10, v0, Lcom/squareup/server/activation/ApplyBody$Builder;->circa:Ljava/lang/String;

    iget-object v11, v0, Lcom/squareup/server/activation/ApplyBody$Builder;->ssnLastFour:Ljava/lang/String;

    iget-object v12, v0, Lcom/squareup/server/activation/ApplyBody$Builder;->businessType:Ljava/lang/String;

    iget-object v13, v0, Lcom/squareup/server/activation/ApplyBody$Builder;->installationIdSha1:Ljava/lang/String;

    iget-object v14, v0, Lcom/squareup/server/activation/ApplyBody$Builder;->installationIdMd5:Ljava/lang/String;

    iget-object v15, v0, Lcom/squareup/server/activation/ApplyBody$Builder;->deviceIdSha1:Ljava/lang/String;

    move-object/from16 v22, v1

    iget-object v1, v0, Lcom/squareup/server/activation/ApplyBody$Builder;->deviceIdMd5:Ljava/lang/String;

    move-object/from16 v16, v1

    iget-object v1, v0, Lcom/squareup/server/activation/ApplyBody$Builder;->business_name:Ljava/lang/String;

    move-object/from16 v17, v1

    iget-object v1, v0, Lcom/squareup/server/activation/ApplyBody$Builder;->estimated_revenue:Ljava/lang/String;

    move-object/from16 v18, v1

    iget-object v1, v0, Lcom/squareup/server/activation/ApplyBody$Builder;->ein:Ljava/lang/String;

    move-object/from16 v19, v1

    const/16 v20, 0x0

    move-object/from16 v1, v22

    invoke-direct/range {v1 .. v20}, Lcom/squareup/server/activation/ApplyBody;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/server/activation/ApplyBody$1;)V

    return-object v21
.end method

.method public setBusinessName(Ljava/lang/String;)Lcom/squareup/server/activation/ApplyBody$Builder;
    .locals 0

    .line 165
    iput-object p1, p0, Lcom/squareup/server/activation/ApplyBody$Builder;->business_name:Ljava/lang/String;

    return-object p0
.end method

.method public setBusinessType(Ljava/lang/String;)Lcom/squareup/server/activation/ApplyBody$Builder;
    .locals 0

    .line 148
    iput-object p1, p0, Lcom/squareup/server/activation/ApplyBody$Builder;->businessType:Ljava/lang/String;

    return-object p0
.end method

.method public setCirca(Ljava/lang/String;)Lcom/squareup/server/activation/ApplyBody$Builder;
    .locals 0

    .line 138
    iput-object p1, p0, Lcom/squareup/server/activation/ApplyBody$Builder;->circa:Ljava/lang/String;

    return-object p0
.end method

.method public setCity(Ljava/lang/String;)Lcom/squareup/server/activation/ApplyBody$Builder;
    .locals 0

    .line 123
    iput-object p1, p0, Lcom/squareup/server/activation/ApplyBody$Builder;->city:Ljava/lang/String;

    return-object p0
.end method

.method public setDevice(Ljava/lang/String;)Lcom/squareup/server/activation/ApplyBody$Builder;
    .locals 1

    .line 159
    invoke-static {p1}, Lcom/squareup/util/Strings;->createSHA1Hash(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/server/activation/ApplyBody$Builder;->deviceIdSha1:Ljava/lang/String;

    .line 160
    invoke-static {p1}, Lcom/squareup/util/Strings;->createMD5Hash(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/server/activation/ApplyBody$Builder;->deviceIdMd5:Ljava/lang/String;

    return-object p0
.end method

.method public setEin(Ljava/lang/String;)Lcom/squareup/server/activation/ApplyBody$Builder;
    .locals 0

    .line 175
    iput-object p1, p0, Lcom/squareup/server/activation/ApplyBody$Builder;->ein:Ljava/lang/String;

    return-object p0
.end method

.method public setEstimatedRevenue(Ljava/lang/String;)Lcom/squareup/server/activation/ApplyBody$Builder;
    .locals 0

    .line 170
    iput-object p1, p0, Lcom/squareup/server/activation/ApplyBody$Builder;->estimated_revenue:Ljava/lang/String;

    return-object p0
.end method

.method public setFirstName(Ljava/lang/String;)Lcom/squareup/server/activation/ApplyBody$Builder;
    .locals 0

    .line 98
    iput-object p1, p0, Lcom/squareup/server/activation/ApplyBody$Builder;->firstName:Ljava/lang/String;

    return-object p0
.end method

.method public setInstallation(Ljava/lang/String;)Lcom/squareup/server/activation/ApplyBody$Builder;
    .locals 1

    .line 153
    invoke-static {p1}, Lcom/squareup/util/Strings;->createSHA1Hash(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/server/activation/ApplyBody$Builder;->installationIdSha1:Ljava/lang/String;

    .line 154
    invoke-static {p1}, Lcom/squareup/util/Strings;->createMD5Hash(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/server/activation/ApplyBody$Builder;->installationIdMd5:Ljava/lang/String;

    return-object p0
.end method

.method public setLastName(Ljava/lang/String;)Lcom/squareup/server/activation/ApplyBody$Builder;
    .locals 0

    .line 103
    iput-object p1, p0, Lcom/squareup/server/activation/ApplyBody$Builder;->lastName:Ljava/lang/String;

    return-object p0
.end method

.method public setPhoneNumber(Ljava/lang/String;)Lcom/squareup/server/activation/ApplyBody$Builder;
    .locals 0

    .line 108
    iput-object p1, p0, Lcom/squareup/server/activation/ApplyBody$Builder;->phoneNumber:Ljava/lang/String;

    return-object p0
.end method

.method public setPostalCode(Ljava/lang/String;)Lcom/squareup/server/activation/ApplyBody$Builder;
    .locals 0

    .line 133
    iput-object p1, p0, Lcom/squareup/server/activation/ApplyBody$Builder;->postalCode:Ljava/lang/String;

    return-object p0
.end method

.method public setSsnLastFour(Ljava/lang/String;)Lcom/squareup/server/activation/ApplyBody$Builder;
    .locals 0

    .line 143
    iput-object p1, p0, Lcom/squareup/server/activation/ApplyBody$Builder;->ssnLastFour:Ljava/lang/String;

    return-object p0
.end method

.method public setState(Ljava/lang/String;)Lcom/squareup/server/activation/ApplyBody$Builder;
    .locals 0

    .line 128
    iput-object p1, p0, Lcom/squareup/server/activation/ApplyBody$Builder;->state:Ljava/lang/String;

    return-object p0
.end method

.method public setStreet1(Ljava/lang/String;)Lcom/squareup/server/activation/ApplyBody$Builder;
    .locals 0

    .line 113
    iput-object p1, p0, Lcom/squareup/server/activation/ApplyBody$Builder;->street1:Ljava/lang/String;

    return-object p0
.end method

.method public setStreet2(Ljava/lang/String;)Lcom/squareup/server/activation/ApplyBody$Builder;
    .locals 0

    .line 118
    iput-object p1, p0, Lcom/squareup/server/activation/ApplyBody$Builder;->street2:Ljava/lang/String;

    return-object p0
.end method
