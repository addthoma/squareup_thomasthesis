.class public Lcom/squareup/server/bills/ApiClientId;
.super Ljava/lang/Object;
.source "ApiClientId.java"


# static fields
.field public static final NO_CLIENT_ID:Ljava/lang/String; = "NO_CLIENT_ID"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static clientIdOrNull(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    :cond_0
    const-string v1, "NO_CLIENT_ID"

    .line 18
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    move-object p0, v0

    :cond_1
    return-object p0
.end method
