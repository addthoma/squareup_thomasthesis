.class public Lcom/squareup/server/RestServiceCreator;
.super Ljava/lang/Object;
.source "RestServiceCreator.java"

# interfaces
.implements Lcom/squareup/api/ServiceCreator;


# instance fields
.field private final restAdapter:Lretrofit/RestAdapter;


# direct methods
.method public constructor <init>(Lretrofit/RestAdapter;)V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-object p1, p0, Lcom/squareup/server/RestServiceCreator;->restAdapter:Lretrofit/RestAdapter;

    return-void
.end method


# virtual methods
.method public create(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    .line 15
    iget-object v0, p0, Lcom/squareup/server/RestServiceCreator;->restAdapter:Lretrofit/RestAdapter;

    invoke-virtual {v0, p1}, Lretrofit/RestAdapter;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
