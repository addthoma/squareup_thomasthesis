.class public interface abstract Lcom/squareup/server/PublicApiService;
.super Ljava/lang/Object;
.source "PublicApiService.java"


# virtual methods
.method public abstract decryptEmail(Lcom/squareup/protos/client/multipass/DecryptEmailRequest;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Lcom/squareup/protos/client/multipass/DecryptEmailRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/multipass/DecryptEmailRequest;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/protos/client/multipass/DecryptEmailResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/multipass/decrypt-email-from-token"
    .end annotation
.end method

.method public abstract retrieveExperiments(Lcom/squareup/server/ExperimentsRequest;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Lcom/squareup/server/ExperimentsRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/ExperimentsRequest;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/server/ExperimentsResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/get-experiments"
    .end annotation
.end method
