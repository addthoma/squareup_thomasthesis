.class public interface abstract Lcom/squareup/server/api/ConnectService;
.super Ljava/lang/Object;
.source "ConnectService.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0006H\'\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/server/api/ConnectService;",
        "",
        "getClientSettings",
        "Lcom/squareup/server/AcceptedResponse;",
        "Lcom/squareup/server/api/ClientSettings;",
        "clientId",
        "",
        "devs-api_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getClientSettings(Ljava/lang/String;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Path;
            value = "client_id"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/server/api/ClientSettings;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/GET;
        value = "/oauth2/clients/{client_id}"
    .end annotation
.end method
