.class public final Lcom/squareup/server/onboard/Image;
.super Lcom/squareup/server/onboard/ComponentBuilder;
.source "PanelComponents.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/onboard/Image$Size;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPanelComponents.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PanelComponents.kt\ncom/squareup/server/onboard/Image\n+ 2 Panels.kt\ncom/squareup/server/onboard/PanelsKt\n*L\n1#1,166:1\n347#2,5:167\n*E\n*S KotlinDebug\n*F\n+ 1 PanelComponents.kt\ncom/squareup/server/onboard/Image\n*L\n117#1,5:167\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0010\u000e\n\u0002\u0008\u0008\u0018\u00002\u00020\u0001:\u0001\u0013B\u0007\u0008\u0000\u00a2\u0006\u0002\u0010\u0002R/\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0008\u0010\u0003\u001a\u0004\u0018\u00010\u00048F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\u0008\n\u0010\u000b\u001a\u0004\u0008\u0006\u0010\u0007\"\u0004\u0008\u0008\u0010\tR/\u0010\r\u001a\u0004\u0018\u00010\u000c2\u0008\u0010\u0003\u001a\u0004\u0018\u00010\u000c8F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\u0008\u0012\u0010\u000b\u001a\u0004\u0008\u000e\u0010\u000f\"\u0004\u0008\u0010\u0010\u0011\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/server/onboard/Image;",
        "Lcom/squareup/server/onboard/ComponentBuilder;",
        "()V",
        "<set-?>",
        "Lcom/squareup/server/onboard/Image$Size;",
        "size",
        "getSize",
        "()Lcom/squareup/server/onboard/Image$Size;",
        "setSize",
        "(Lcom/squareup/server/onboard/Image$Size;)V",
        "size$delegate",
        "Lcom/squareup/server/onboard/PropertyEntryDelegate;",
        "",
        "url",
        "getUrl",
        "()Ljava/lang/String;",
        "setUrl",
        "(Ljava/lang/String;)V",
        "url$delegate",
        "Size",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private final size$delegate:Lcom/squareup/server/onboard/PropertyEntryDelegate;

.field private final url$delegate:Lcom/squareup/server/onboard/PropertyEntryDelegate;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-class v0, Lcom/squareup/server/onboard/Image;

    const/4 v1, 0x2

    new-array v1, v1, [Lkotlin/reflect/KProperty;

    new-instance v2, Lkotlin/jvm/internal/MutablePropertyReference1Impl;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    const-string/jumbo v4, "url"

    const-string v5, "getUrl()Ljava/lang/String;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/MutablePropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->mutableProperty1(Lkotlin/jvm/internal/MutablePropertyReference1;)Lkotlin/reflect/KMutableProperty1;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/MutablePropertyReference1Impl;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    const-string v3, "size"

    const-string v4, "getSize()Lcom/squareup/server/onboard/Image$Size;"

    invoke-direct {v2, v0, v3, v4}, Lkotlin/jvm/internal/MutablePropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->mutableProperty1(Lkotlin/jvm/internal/MutablePropertyReference1;)Lkotlin/reflect/KMutableProperty1;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aput-object v0, v1, v2

    sput-object v1, Lcom/squareup/server/onboard/Image;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .line 115
    sget-object v0, Lcom/squareup/protos/client/onboard/ComponentType;->IMAGE:Lcom/squareup/protos/client/onboard/ComponentType;

    invoke-direct {p0, v0}, Lcom/squareup/server/onboard/ComponentBuilder;-><init>(Lcom/squareup/protos/client/onboard/ComponentType;)V

    const-string v0, "image_url"

    .line 116
    invoke-static {v0}, Lcom/squareup/server/onboard/PanelsKt;->stringPropertyEntry(Ljava/lang/String;)Lcom/squareup/server/onboard/PropertyEntryDelegate;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/server/onboard/Image;->url$delegate:Lcom/squareup/server/onboard/PropertyEntryDelegate;

    const/4 v0, 0x0

    .line 167
    check-cast v0, Ljava/lang/String;

    .line 169
    new-instance v1, Lcom/squareup/server/onboard/PropertyEntryDelegate;

    .line 170
    sget-object v2, Lcom/squareup/server/onboard/Image$$special$$inlined$enumPropertyEntry$1;->INSTANCE:Lcom/squareup/server/onboard/Image$$special$$inlined$enumPropertyEntry$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 171
    sget-object v3, Lcom/squareup/server/onboard/PanelsKt$enumPropertyEntry$2;->INSTANCE:Lcom/squareup/server/onboard/PanelsKt$enumPropertyEntry$2;

    check-cast v3, Lkotlin/jvm/functions/Function2;

    .line 169
    invoke-direct {v1, v0, v2, v3}, Lcom/squareup/server/onboard/PropertyEntryDelegate;-><init>(Ljava/lang/String;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function2;)V

    iput-object v1, p0, Lcom/squareup/server/onboard/Image;->size$delegate:Lcom/squareup/server/onboard/PropertyEntryDelegate;

    return-void
.end method


# virtual methods
.method public final getSize()Lcom/squareup/server/onboard/Image$Size;
    .locals 4

    iget-object v0, p0, Lcom/squareup/server/onboard/Image;->size$delegate:Lcom/squareup/server/onboard/PropertyEntryDelegate;

    move-object v1, p0

    check-cast v1, Lcom/squareup/server/onboard/ComponentBuilder;

    sget-object v2, Lcom/squareup/server/onboard/Image;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/squareup/server/onboard/PropertyEntryDelegate;->getValue(Lcom/squareup/server/onboard/ComponentBuilder;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/onboard/Image$Size;

    return-object v0
.end method

.method public final getUrl()Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Lcom/squareup/server/onboard/Image;->url$delegate:Lcom/squareup/server/onboard/PropertyEntryDelegate;

    move-object v1, p0

    check-cast v1, Lcom/squareup/server/onboard/ComponentBuilder;

    sget-object v2, Lcom/squareup/server/onboard/Image;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/squareup/server/onboard/PropertyEntryDelegate;->getValue(Lcom/squareup/server/onboard/ComponentBuilder;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final setSize(Lcom/squareup/server/onboard/Image$Size;)V
    .locals 4

    iget-object v0, p0, Lcom/squareup/server/onboard/Image;->size$delegate:Lcom/squareup/server/onboard/PropertyEntryDelegate;

    move-object v1, p0

    check-cast v1, Lcom/squareup/server/onboard/ComponentBuilder;

    sget-object v2, Lcom/squareup/server/onboard/Image;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-virtual {v0, v1, v2, p1}, Lcom/squareup/server/onboard/PropertyEntryDelegate;->setValue(Lcom/squareup/server/onboard/ComponentBuilder;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    return-void
.end method

.method public final setUrl(Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lcom/squareup/server/onboard/Image;->url$delegate:Lcom/squareup/server/onboard/PropertyEntryDelegate;

    move-object v1, p0

    check-cast v1, Lcom/squareup/server/onboard/ComponentBuilder;

    sget-object v2, Lcom/squareup/server/onboard/Image;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-virtual {v0, v1, v2, p1}, Lcom/squareup/server/onboard/PropertyEntryDelegate;->setValue(Lcom/squareup/server/onboard/ComponentBuilder;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    return-void
.end method
