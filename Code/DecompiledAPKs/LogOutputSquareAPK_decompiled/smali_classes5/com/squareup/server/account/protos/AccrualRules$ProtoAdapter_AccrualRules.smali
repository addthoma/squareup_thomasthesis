.class final Lcom/squareup/server/account/protos/AccrualRules$ProtoAdapter_AccrualRules;
.super Lcom/squareup/wire/ProtoAdapter;
.source "AccrualRules.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/AccrualRules;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_AccrualRules"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/server/account/protos/AccrualRules;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1467
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/server/account/protos/AccrualRules;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/AccrualRules;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1490
    new-instance v0, Lcom/squareup/server/account/protos/AccrualRules$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/AccrualRules$Builder;-><init>()V

    .line 1491
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 1492
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_2

    const/4 v4, 0x3

    if-eq v3, v4, :cond_1

    const/4 v4, 0x4

    if-eq v3, v4, :cond_0

    .line 1499
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 1497
    :cond_0
    sget-object v3, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/AccrualRules$Builder;->accrual_rules_options(Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;)Lcom/squareup/server/account/protos/AccrualRules$Builder;

    goto :goto_0

    .line 1496
    :cond_1
    sget-object v3, Lcom/squareup/server/account/protos/AccrualRules$ItemRules;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/AccrualRules$ItemRules;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/AccrualRules$Builder;->item_rules(Lcom/squareup/server/account/protos/AccrualRules$ItemRules;)Lcom/squareup/server/account/protos/AccrualRules$Builder;

    goto :goto_0

    .line 1495
    :cond_2
    sget-object v3, Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/AccrualRules$Builder;->spend_rate_rules(Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;)Lcom/squareup/server/account/protos/AccrualRules$Builder;

    goto :goto_0

    .line 1494
    :cond_3
    sget-object v3, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/AccrualRules$Builder;->visit_rules(Lcom/squareup/server/account/protos/AccrualRules$VisitRules;)Lcom/squareup/server/account/protos/AccrualRules$Builder;

    goto :goto_0

    .line 1503
    :cond_4
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/AccrualRules$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 1504
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/AccrualRules$Builder;->build()Lcom/squareup/server/account/protos/AccrualRules;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1465
    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/AccrualRules$ProtoAdapter_AccrualRules;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/AccrualRules;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/AccrualRules;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1481
    sget-object v0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/AccrualRules;->accrual_rules_options:Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1482
    sget-object v0, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/AccrualRules;->visit_rules:Lcom/squareup/server/account/protos/AccrualRules$VisitRules;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1483
    sget-object v0, Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/AccrualRules;->spend_rate_rules:Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1484
    sget-object v0, Lcom/squareup/server/account/protos/AccrualRules$ItemRules;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/AccrualRules;->item_rules:Lcom/squareup/server/account/protos/AccrualRules$ItemRules;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1485
    invoke-virtual {p2}, Lcom/squareup/server/account/protos/AccrualRules;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1465
    check-cast p2, Lcom/squareup/server/account/protos/AccrualRules;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/server/account/protos/AccrualRules$ProtoAdapter_AccrualRules;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/AccrualRules;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/server/account/protos/AccrualRules;)I
    .locals 4

    .line 1472
    sget-object v0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccrualRules;->accrual_rules_options:Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;

    const/4 v2, 0x4

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/AccrualRules;->visit_rules:Lcom/squareup/server/account/protos/AccrualRules$VisitRules;

    const/4 v3, 0x1

    .line 1473
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/AccrualRules;->spend_rate_rules:Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;

    const/4 v3, 0x2

    .line 1474
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/AccrualRules$ItemRules;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/AccrualRules;->item_rules:Lcom/squareup/server/account/protos/AccrualRules$ItemRules;

    const/4 v3, 0x3

    .line 1475
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1476
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/AccrualRules;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 1465
    check-cast p1, Lcom/squareup/server/account/protos/AccrualRules;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/AccrualRules$ProtoAdapter_AccrualRules;->encodedSize(Lcom/squareup/server/account/protos/AccrualRules;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/server/account/protos/AccrualRules;)Lcom/squareup/server/account/protos/AccrualRules;
    .locals 2

    .line 1509
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/AccrualRules;->newBuilder()Lcom/squareup/server/account/protos/AccrualRules$Builder;

    move-result-object p1

    .line 1510
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccrualRules$Builder;->accrual_rules_options:Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccrualRules$Builder;->accrual_rules_options:Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;

    iput-object v0, p1, Lcom/squareup/server/account/protos/AccrualRules$Builder;->accrual_rules_options:Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;

    .line 1511
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccrualRules$Builder;->visit_rules:Lcom/squareup/server/account/protos/AccrualRules$VisitRules;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccrualRules$Builder;->visit_rules:Lcom/squareup/server/account/protos/AccrualRules$VisitRules;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;

    iput-object v0, p1, Lcom/squareup/server/account/protos/AccrualRules$Builder;->visit_rules:Lcom/squareup/server/account/protos/AccrualRules$VisitRules;

    .line 1512
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccrualRules$Builder;->spend_rate_rules:Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccrualRules$Builder;->spend_rate_rules:Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;

    iput-object v0, p1, Lcom/squareup/server/account/protos/AccrualRules$Builder;->spend_rate_rules:Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;

    .line 1513
    :cond_2
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccrualRules$Builder;->item_rules:Lcom/squareup/server/account/protos/AccrualRules$ItemRules;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/server/account/protos/AccrualRules$ItemRules;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccrualRules$Builder;->item_rules:Lcom/squareup/server/account/protos/AccrualRules$ItemRules;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/AccrualRules$ItemRules;

    iput-object v0, p1, Lcom/squareup/server/account/protos/AccrualRules$Builder;->item_rules:Lcom/squareup/server/account/protos/AccrualRules$ItemRules;

    .line 1514
    :cond_3
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/AccrualRules$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 1515
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/AccrualRules$Builder;->build()Lcom/squareup/server/account/protos/AccrualRules;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1465
    check-cast p1, Lcom/squareup/server/account/protos/AccrualRules;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/AccrualRules$ProtoAdapter_AccrualRules;->redact(Lcom/squareup/server/account/protos/AccrualRules;)Lcom/squareup/server/account/protos/AccrualRules;

    move-result-object p1

    return-object p1
.end method
