.class public final Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;
.super Lcom/squareup/wire/AndroidMessage;
.source "AccrualRules.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/AccrualRules;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AccrualRulesOptions"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$ProtoAdapter_AccrualRulesOptions;,
        Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;",
        "Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_EXCLUDE_COMP_ITEMS:Ljava/lang/Boolean;

.field public static final DEFAULT_EXCLUDE_DISCOUNTED_ITEMS:Ljava/lang/Boolean;

.field public static final DEFAULT_EXCLUDE_REWARD_ITEMS:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final exclude_comp_items:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field

.field public final exclude_discounted_items:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field

.field public final exclude_reward_items:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field

.field public final excluded_catalog_objects:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.AccrualRules$CatalogObject#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x4
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/AccrualRules$CatalogObject;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1201
    new-instance v0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$ProtoAdapter_AccrualRulesOptions;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$ProtoAdapter_AccrualRulesOptions;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 1203
    sget-object v0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->CREATOR:Landroid/os/Parcelable$Creator;

    const/4 v0, 0x0

    .line 1207
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->DEFAULT_EXCLUDE_REWARD_ITEMS:Ljava/lang/Boolean;

    .line 1209
    sput-object v0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->DEFAULT_EXCLUDE_DISCOUNTED_ITEMS:Ljava/lang/Boolean;

    .line 1211
    sput-object v0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->DEFAULT_EXCLUDE_COMP_ITEMS:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/AccrualRules$CatalogObject;",
            ">;)V"
        }
    .end annotation

    .line 1265
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/AccrualRules$CatalogObject;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 1271
    sget-object v0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 1272
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->exclude_reward_items:Ljava/lang/Boolean;

    .line 1273
    iput-object p2, p0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->exclude_discounted_items:Ljava/lang/Boolean;

    .line 1274
    iput-object p3, p0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->exclude_comp_items:Ljava/lang/Boolean;

    const-string p1, "excluded_catalog_objects"

    .line 1275
    invoke-static {p1, p4}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->excluded_catalog_objects:Ljava/util/List;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;)Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 1349
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->newBuilder()Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 1292
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 1293
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;

    .line 1294
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->exclude_reward_items:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->exclude_reward_items:Ljava/lang/Boolean;

    .line 1295
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->exclude_discounted_items:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->exclude_discounted_items:Ljava/lang/Boolean;

    .line 1296
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->exclude_comp_items:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->exclude_comp_items:Ljava/lang/Boolean;

    .line 1297
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->excluded_catalog_objects:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->excluded_catalog_objects:Ljava/util/List;

    .line 1298
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 1303
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_3

    .line 1305
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 1306
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->exclude_reward_items:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1307
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->exclude_discounted_items:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1308
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->exclude_comp_items:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 1309
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->excluded_catalog_objects:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1310
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;
    .locals 2

    .line 1280
    new-instance v0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;-><init>()V

    .line 1281
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->exclude_reward_items:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;->exclude_reward_items:Ljava/lang/Boolean;

    .line 1282
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->exclude_discounted_items:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;->exclude_discounted_items:Ljava/lang/Boolean;

    .line 1283
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->exclude_comp_items:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;->exclude_comp_items:Ljava/lang/Boolean;

    .line 1284
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->excluded_catalog_objects:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;->excluded_catalog_objects:Ljava/util/List;

    .line 1285
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 1200
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->newBuilder()Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;)Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;
    .locals 2

    .line 1341
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->exclude_reward_items:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->requireBuilder(Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;)Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->exclude_reward_items:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;->exclude_reward_items(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;

    move-result-object v1

    .line 1342
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->exclude_discounted_items:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->requireBuilder(Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;)Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->exclude_discounted_items:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;->exclude_discounted_items(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;

    move-result-object v1

    .line 1343
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->exclude_comp_items:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->requireBuilder(Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;)Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->exclude_comp_items:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;->exclude_comp_items(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;

    move-result-object v1

    .line 1344
    :cond_2
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->excluded_catalog_objects:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->requireBuilder(Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;)Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->excluded_catalog_objects:Ljava/util/List;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;->excluded_catalog_objects(Ljava/util/List;)Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;

    move-result-object v1

    :cond_3
    if-nez v1, :cond_4

    move-object p1, p0

    goto :goto_0

    .line 1345
    :cond_4
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;->build()Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 1200
    check-cast p1, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->overlay(Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;)Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;
    .locals 3

    .line 1328
    iget-object v0, p0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->exclude_reward_items:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->requireBuilder(Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;)Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->DEFAULT_EXCLUDE_REWARD_ITEMS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;->exclude_reward_items(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;

    move-result-object v1

    .line 1329
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->exclude_discounted_items:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->requireBuilder(Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;)Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->DEFAULT_EXCLUDE_DISCOUNTED_ITEMS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;->exclude_discounted_items(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;

    move-result-object v1

    .line 1330
    :cond_1
    iget-object v0, p0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->exclude_comp_items:Ljava/lang/Boolean;

    if-nez v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->requireBuilder(Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;)Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->DEFAULT_EXCLUDE_COMP_ITEMS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;->exclude_comp_items(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;

    move-result-object v1

    .line 1331
    :cond_2
    iget-object v0, p0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->excluded_catalog_objects:Ljava/util/List;

    if-eqz v0, :cond_3

    .line 1332
    invoke-static {v0}, Lcom/squareup/wired/Wired;->populateDefaults(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 1333
    iget-object v2, p0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->excluded_catalog_objects:Ljava/util/List;

    if-eq v0, v2, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->requireBuilder(Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;)Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;->excluded_catalog_objects(Ljava/util/List;)Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;

    move-result-object v1

    :cond_3
    if-nez v1, :cond_4

    move-object v0, p0

    goto :goto_0

    .line 1335
    :cond_4
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;->build()Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 1200
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->populateDefaults()Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 1317
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1318
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->exclude_reward_items:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", exclude_reward_items="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->exclude_reward_items:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1319
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->exclude_discounted_items:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", exclude_discounted_items="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->exclude_discounted_items:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1320
    :cond_1
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->exclude_comp_items:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", exclude_comp_items="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->exclude_comp_items:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1321
    :cond_2
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->excluded_catalog_objects:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, ", excluded_catalog_objects="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->excluded_catalog_objects:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "AccrualRulesOptions{"

    .line 1322
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
