.class public final Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "User.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/User$ReceiptAddress;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/User$ReceiptAddress;",
        "Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public address_line_3:Ljava/lang/String;

.field public city:Ljava/lang/String;

.field public country_code:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public postal_code:Ljava/lang/String;

.field public state:Ljava/lang/String;

.field public street1:Ljava/lang/String;

.field public street2:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 918
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public address_line_3(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;
    .locals 0

    .line 932
    iput-object p1, p0, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->address_line_3:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/server/account/protos/User$ReceiptAddress;
    .locals 11

    .line 963
    new-instance v10, Lcom/squareup/server/account/protos/User$ReceiptAddress;

    iget-object v1, p0, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->street1:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->street2:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->address_line_3:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->city:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->state:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->postal_code:Ljava/lang/String;

    iget-object v7, p0, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->country_code:Ljava/lang/String;

    iget-object v8, p0, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->name:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v9

    move-object v0, v10

    invoke-direct/range {v0 .. v9}, Lcom/squareup/server/account/protos/User$ReceiptAddress;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v10
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 901
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->build()Lcom/squareup/server/account/protos/User$ReceiptAddress;

    move-result-object v0

    return-object v0
.end method

.method public city(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;
    .locals 0

    .line 937
    iput-object p1, p0, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->city:Ljava/lang/String;

    return-object p0
.end method

.method public country_code(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;
    .locals 0

    .line 952
    iput-object p1, p0, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->country_code:Ljava/lang/String;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;
    .locals 0

    .line 957
    iput-object p1, p0, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public postal_code(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;
    .locals 0

    .line 947
    iput-object p1, p0, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->postal_code:Ljava/lang/String;

    return-object p0
.end method

.method public state(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;
    .locals 0

    .line 942
    iput-object p1, p0, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->state:Ljava/lang/String;

    return-object p0
.end method

.method public street1(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;
    .locals 0

    .line 922
    iput-object p1, p0, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->street1:Ljava/lang/String;

    return-object p0
.end method

.method public street2(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;
    .locals 0

    .line 927
    iput-object p1, p0, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->street2:Ljava/lang/String;

    return-object p0
.end method
