.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;
.super Lcom/squareup/wire/AndroidMessage;
.source "FlagsAndPermissions.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Payments"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$ProtoAdapter_Payments;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_QUICK_AMOUNTS_V2:Ljava/lang/Boolean;

.field public static final DEFAULT_REQUEST_BUSINESS_ADDRESS:Ljava/lang/Boolean;

.field public static final DEFAULT_REQUEST_BUSINESS_ADDRESS_BADGE:Ljava/lang/Boolean;

.field public static final DEFAULT_REQUEST_BUSINESS_ADDRESS_POBOX_VALIDATION:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final quick_amounts_v2:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation
.end field

.field public final request_business_address:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field

.field public final request_business_address_badge:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field

.field public final request_business_address_pobox_validation:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 18465
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$ProtoAdapter_Payments;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$ProtoAdapter_Payments;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 18467
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->CREATOR:Landroid/os/Parcelable$Creator;

    const/4 v0, 0x0

    .line 18471
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->DEFAULT_REQUEST_BUSINESS_ADDRESS:Ljava/lang/Boolean;

    .line 18473
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->DEFAULT_REQUEST_BUSINESS_ADDRESS_BADGE:Ljava/lang/Boolean;

    .line 18475
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->DEFAULT_REQUEST_BUSINESS_ADDRESS_POBOX_VALIDATION:Ljava/lang/Boolean;

    .line 18477
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->DEFAULT_QUICK_AMOUNTS_V2:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 6

    .line 18523
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 18530
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 18531
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->request_business_address:Ljava/lang/Boolean;

    .line 18532
    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->request_business_address_badge:Ljava/lang/Boolean;

    .line 18533
    iput-object p3, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->request_business_address_pobox_validation:Ljava/lang/Boolean;

    .line 18534
    iput-object p4, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->quick_amounts_v2:Ljava/lang/Boolean;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 18605
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 18551
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 18552
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;

    .line 18553
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->request_business_address:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->request_business_address:Ljava/lang/Boolean;

    .line 18554
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->request_business_address_badge:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->request_business_address_badge:Ljava/lang/Boolean;

    .line 18555
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->request_business_address_pobox_validation:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->request_business_address_pobox_validation:Ljava/lang/Boolean;

    .line 18556
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->quick_amounts_v2:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->quick_amounts_v2:Ljava/lang/Boolean;

    .line 18557
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 18562
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_4

    .line 18564
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 18565
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->request_business_address:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 18566
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->request_business_address_badge:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 18567
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->request_business_address_pobox_validation:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 18568
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->quick_amounts_v2:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 18569
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;
    .locals 2

    .line 18539
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;-><init>()V

    .line 18540
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->request_business_address:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;->request_business_address:Ljava/lang/Boolean;

    .line 18541
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->request_business_address_badge:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;->request_business_address_badge:Ljava/lang/Boolean;

    .line 18542
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->request_business_address_pobox_validation:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;->request_business_address_pobox_validation:Ljava/lang/Boolean;

    .line 18543
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->quick_amounts_v2:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;->quick_amounts_v2:Ljava/lang/Boolean;

    .line 18544
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 18464
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;
    .locals 2

    .line 18597
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->request_business_address:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->request_business_address:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;->request_business_address(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;

    move-result-object v1

    .line 18598
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->request_business_address_badge:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->request_business_address_badge:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;->request_business_address_badge(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;

    move-result-object v1

    .line 18599
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->request_business_address_pobox_validation:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->request_business_address_pobox_validation:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;->request_business_address_pobox_validation(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;

    move-result-object v1

    .line 18600
    :cond_2
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->quick_amounts_v2:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->quick_amounts_v2:Ljava/lang/Boolean;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;->quick_amounts_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;

    move-result-object v1

    :cond_3
    if-nez v1, :cond_4

    move-object p1, p0

    goto :goto_0

    .line 18601
    :cond_4
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 18464
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;
    .locals 2

    .line 18587
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->request_business_address:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->DEFAULT_REQUEST_BUSINESS_ADDRESS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;->request_business_address(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;

    move-result-object v1

    .line 18588
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->request_business_address_badge:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->DEFAULT_REQUEST_BUSINESS_ADDRESS_BADGE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;->request_business_address_badge(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;

    move-result-object v1

    .line 18589
    :cond_1
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->request_business_address_pobox_validation:Ljava/lang/Boolean;

    if-nez v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->DEFAULT_REQUEST_BUSINESS_ADDRESS_POBOX_VALIDATION:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;->request_business_address_pobox_validation(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;

    move-result-object v1

    .line 18590
    :cond_2
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->quick_amounts_v2:Ljava/lang/Boolean;

    if-nez v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->DEFAULT_QUICK_AMOUNTS_V2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;->quick_amounts_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;

    move-result-object v1

    :cond_3
    if-nez v1, :cond_4

    move-object v0, p0

    goto :goto_0

    .line 18591
    :cond_4
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 18464
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 18576
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 18577
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->request_business_address:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", request_business_address="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->request_business_address:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 18578
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->request_business_address_badge:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", request_business_address_badge="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->request_business_address_badge:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 18579
    :cond_1
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->request_business_address_pobox_validation:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", request_business_address_pobox_validation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->request_business_address_pobox_validation:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 18580
    :cond_2
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->quick_amounts_v2:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", quick_amounts_v2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;->quick_amounts_v2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Payments{"

    .line 18581
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
