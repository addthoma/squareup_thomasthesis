.class public final Lcom/squareup/server/account/protos/FeeType;
.super Lcom/squareup/wire/AndroidMessage;
.source "FeeType.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/FeeType$ProtoAdapter_FeeType;,
        Lcom/squareup/server/account/protos/FeeType$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/FeeType;",
        "Lcom/squareup/server/account/protos/FeeType$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/FeeType;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/FeeType;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/FeeType;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/FeeType;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CALCULATION_PHASE:Ljava/lang/Integer;

.field public static final DEFAULT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_INCLUSION_TYPE:Lcom/squareup/api/items/Fee$InclusionType;

.field public static final DEFAULT_IS_DEFAULT:Ljava/lang/Boolean;

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_PERCENTAGE:Ljava/lang/Double;

.field private static final serialVersionUID:J


# instance fields
.field public final calculation_phase:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x3
    .end annotation
.end field

.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.Fee$InclusionType#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final is_default:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final percentage:Ljava/lang/Double;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#DOUBLE"
        tag = 0x5
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 32
    new-instance v0, Lcom/squareup/server/account/protos/FeeType$ProtoAdapter_FeeType;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FeeType$ProtoAdapter_FeeType;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/FeeType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 34
    sget-object v0, Lcom/squareup/server/account/protos/FeeType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FeeType;->CREATOR:Landroid/os/Parcelable$Creator;

    const/4 v0, 0x0

    .line 42
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sput-object v1, Lcom/squareup/server/account/protos/FeeType;->DEFAULT_CALCULATION_PHASE:Ljava/lang/Integer;

    .line 44
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FeeType;->DEFAULT_IS_DEFAULT:Ljava/lang/Boolean;

    const-wide/16 v0, 0x0

    .line 46
    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FeeType;->DEFAULT_PERCENTAGE:Ljava/lang/Double;

    .line 48
    sget-object v0, Lcom/squareup/api/items/Fee$InclusionType;->ADDITIVE:Lcom/squareup/api/items/Fee$InclusionType;

    sput-object v0, Lcom/squareup/server/account/protos/FeeType;->DEFAULT_INCLUSION_TYPE:Lcom/squareup/api/items/Fee$InclusionType;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Double;Lcom/squareup/api/items/Fee$InclusionType;)V
    .locals 8

    .line 95
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/server/account/protos/FeeType;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Double;Lcom/squareup/api/items/Fee$InclusionType;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Double;Lcom/squareup/api/items/Fee$InclusionType;Lokio/ByteString;)V
    .locals 1

    .line 101
    sget-object v0, Lcom/squareup/server/account/protos/FeeType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 102
    iput-object p1, p0, Lcom/squareup/server/account/protos/FeeType;->id:Ljava/lang/String;

    .line 103
    iput-object p2, p0, Lcom/squareup/server/account/protos/FeeType;->name:Ljava/lang/String;

    .line 104
    iput-object p3, p0, Lcom/squareup/server/account/protos/FeeType;->calculation_phase:Ljava/lang/Integer;

    .line 105
    iput-object p4, p0, Lcom/squareup/server/account/protos/FeeType;->is_default:Ljava/lang/Boolean;

    .line 106
    iput-object p5, p0, Lcom/squareup/server/account/protos/FeeType;->percentage:Ljava/lang/Double;

    .line 107
    iput-object p6, p0, Lcom/squareup/server/account/protos/FeeType;->inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/FeeType$Builder;)Lcom/squareup/server/account/protos/FeeType$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 187
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FeeType;->newBuilder()Lcom/squareup/server/account/protos/FeeType$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 126
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/FeeType;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 127
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/FeeType;

    .line 128
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FeeType;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/FeeType;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FeeType;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FeeType;->id:Ljava/lang/String;

    .line 129
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FeeType;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FeeType;->name:Ljava/lang/String;

    .line 130
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FeeType;->calculation_phase:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FeeType;->calculation_phase:Ljava/lang/Integer;

    .line 131
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FeeType;->is_default:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FeeType;->is_default:Ljava/lang/Boolean;

    .line 132
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FeeType;->percentage:Ljava/lang/Double;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FeeType;->percentage:Ljava/lang/Double;

    .line 133
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FeeType;->inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;

    iget-object p1, p1, Lcom/squareup/server/account/protos/FeeType;->inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;

    .line 134
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 139
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_6

    .line 141
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FeeType;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 142
    iget-object v1, p0, Lcom/squareup/server/account/protos/FeeType;->id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 143
    iget-object v1, p0, Lcom/squareup/server/account/protos/FeeType;->name:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 144
    iget-object v1, p0, Lcom/squareup/server/account/protos/FeeType;->calculation_phase:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 145
    iget-object v1, p0, Lcom/squareup/server/account/protos/FeeType;->is_default:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 146
    iget-object v1, p0, Lcom/squareup/server/account/protos/FeeType;->percentage:Ljava/lang/Double;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Double;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 147
    iget-object v1, p0, Lcom/squareup/server/account/protos/FeeType;->inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/api/items/Fee$InclusionType;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    .line 148
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_6
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/FeeType$Builder;
    .locals 2

    .line 112
    new-instance v0, Lcom/squareup/server/account/protos/FeeType$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FeeType$Builder;-><init>()V

    .line 113
    iget-object v1, p0, Lcom/squareup/server/account/protos/FeeType;->id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FeeType$Builder;->id:Ljava/lang/String;

    .line 114
    iget-object v1, p0, Lcom/squareup/server/account/protos/FeeType;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FeeType$Builder;->name:Ljava/lang/String;

    .line 115
    iget-object v1, p0, Lcom/squareup/server/account/protos/FeeType;->calculation_phase:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FeeType$Builder;->calculation_phase:Ljava/lang/Integer;

    .line 116
    iget-object v1, p0, Lcom/squareup/server/account/protos/FeeType;->is_default:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FeeType$Builder;->is_default:Ljava/lang/Boolean;

    .line 117
    iget-object v1, p0, Lcom/squareup/server/account/protos/FeeType;->percentage:Ljava/lang/Double;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FeeType$Builder;->percentage:Ljava/lang/Double;

    .line 118
    iget-object v1, p0, Lcom/squareup/server/account/protos/FeeType;->inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FeeType$Builder;->inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;

    .line 119
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FeeType;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FeeType$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 31
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FeeType;->newBuilder()Lcom/squareup/server/account/protos/FeeType$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/FeeType;)Lcom/squareup/server/account/protos/FeeType;
    .locals 2

    .line 177
    iget-object v0, p1, Lcom/squareup/server/account/protos/FeeType;->id:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FeeType;->requireBuilder(Lcom/squareup/server/account/protos/FeeType$Builder;)Lcom/squareup/server/account/protos/FeeType$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FeeType;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FeeType$Builder;->id(Ljava/lang/String;)Lcom/squareup/server/account/protos/FeeType$Builder;

    move-result-object v1

    .line 178
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/FeeType;->name:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FeeType;->requireBuilder(Lcom/squareup/server/account/protos/FeeType$Builder;)Lcom/squareup/server/account/protos/FeeType$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FeeType;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FeeType$Builder;->name(Ljava/lang/String;)Lcom/squareup/server/account/protos/FeeType$Builder;

    move-result-object v1

    .line 179
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/FeeType;->calculation_phase:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FeeType;->requireBuilder(Lcom/squareup/server/account/protos/FeeType$Builder;)Lcom/squareup/server/account/protos/FeeType$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FeeType;->calculation_phase:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FeeType$Builder;->calculation_phase(Ljava/lang/Integer;)Lcom/squareup/server/account/protos/FeeType$Builder;

    move-result-object v1

    .line 180
    :cond_2
    iget-object v0, p1, Lcom/squareup/server/account/protos/FeeType;->is_default:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FeeType;->requireBuilder(Lcom/squareup/server/account/protos/FeeType$Builder;)Lcom/squareup/server/account/protos/FeeType$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FeeType;->is_default:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FeeType$Builder;->is_default(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FeeType$Builder;

    move-result-object v1

    .line 181
    :cond_3
    iget-object v0, p1, Lcom/squareup/server/account/protos/FeeType;->percentage:Ljava/lang/Double;

    if-eqz v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FeeType;->requireBuilder(Lcom/squareup/server/account/protos/FeeType$Builder;)Lcom/squareup/server/account/protos/FeeType$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FeeType;->percentage:Ljava/lang/Double;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FeeType$Builder;->percentage(Ljava/lang/Double;)Lcom/squareup/server/account/protos/FeeType$Builder;

    move-result-object v1

    .line 182
    :cond_4
    iget-object v0, p1, Lcom/squareup/server/account/protos/FeeType;->inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;

    if-eqz v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FeeType;->requireBuilder(Lcom/squareup/server/account/protos/FeeType$Builder;)Lcom/squareup/server/account/protos/FeeType$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/FeeType;->inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/FeeType$Builder;->inclusion_type(Lcom/squareup/api/items/Fee$InclusionType;)Lcom/squareup/server/account/protos/FeeType$Builder;

    move-result-object v1

    :cond_5
    if-nez v1, :cond_6

    move-object p1, p0

    goto :goto_0

    .line 183
    :cond_6
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FeeType$Builder;->build()Lcom/squareup/server/account/protos/FeeType;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 31
    check-cast p1, Lcom/squareup/server/account/protos/FeeType;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/FeeType;->overlay(Lcom/squareup/server/account/protos/FeeType;)Lcom/squareup/server/account/protos/FeeType;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/FeeType;
    .locals 2

    .line 168
    iget-object v0, p0, Lcom/squareup/server/account/protos/FeeType;->calculation_phase:Ljava/lang/Integer;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FeeType;->requireBuilder(Lcom/squareup/server/account/protos/FeeType$Builder;)Lcom/squareup/server/account/protos/FeeType$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FeeType;->DEFAULT_CALCULATION_PHASE:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FeeType$Builder;->calculation_phase(Ljava/lang/Integer;)Lcom/squareup/server/account/protos/FeeType$Builder;

    move-result-object v1

    .line 169
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/protos/FeeType;->is_default:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FeeType;->requireBuilder(Lcom/squareup/server/account/protos/FeeType$Builder;)Lcom/squareup/server/account/protos/FeeType$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FeeType;->DEFAULT_IS_DEFAULT:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FeeType$Builder;->is_default(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FeeType$Builder;

    move-result-object v1

    .line 170
    :cond_1
    iget-object v0, p0, Lcom/squareup/server/account/protos/FeeType;->percentage:Ljava/lang/Double;

    if-nez v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FeeType;->requireBuilder(Lcom/squareup/server/account/protos/FeeType$Builder;)Lcom/squareup/server/account/protos/FeeType$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FeeType;->DEFAULT_PERCENTAGE:Ljava/lang/Double;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FeeType$Builder;->percentage(Ljava/lang/Double;)Lcom/squareup/server/account/protos/FeeType$Builder;

    move-result-object v1

    :cond_2
    if-nez v1, :cond_3

    move-object v0, p0

    goto :goto_0

    .line 171
    :cond_3
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FeeType$Builder;->build()Lcom/squareup/server/account/protos/FeeType;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 31
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FeeType;->populateDefaults()Lcom/squareup/server/account/protos/FeeType;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 155
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 156
    iget-object v1, p0, Lcom/squareup/server/account/protos/FeeType;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FeeType;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 157
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/FeeType;->name:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FeeType;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 158
    :cond_1
    iget-object v1, p0, Lcom/squareup/server/account/protos/FeeType;->calculation_phase:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    const-string v1, ", calculation_phase="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FeeType;->calculation_phase:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 159
    :cond_2
    iget-object v1, p0, Lcom/squareup/server/account/protos/FeeType;->is_default:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", is_default="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FeeType;->is_default:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 160
    :cond_3
    iget-object v1, p0, Lcom/squareup/server/account/protos/FeeType;->percentage:Ljava/lang/Double;

    if-eqz v1, :cond_4

    const-string v1, ", percentage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FeeType;->percentage:Ljava/lang/Double;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 161
    :cond_4
    iget-object v1, p0, Lcom/squareup/server/account/protos/FeeType;->inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;

    if-eqz v1, :cond_5

    const-string v1, ", inclusion_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FeeType;->inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "FeeType{"

    .line 162
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
