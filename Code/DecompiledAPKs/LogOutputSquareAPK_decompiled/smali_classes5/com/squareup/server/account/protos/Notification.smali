.class public final Lcom/squareup/server/account/protos/Notification;
.super Lcom/squareup/wire/AndroidMessage;
.source "Notification.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/Notification$ProtoAdapter_Notification;,
        Lcom/squareup/server/account/protos/Notification$Button;,
        Lcom/squareup/server/account/protos/Notification$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/Notification;",
        "Lcom/squareup/server/account/protos/Notification$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/Notification;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/Notification;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/Notification;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/Notification;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_DELIVERY_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_LOCK_OUT:Ljava/lang/Boolean;

.field public static final DEFAULT_MESSAGE:Ljava/lang/String; = ""

.field public static final DEFAULT_TITLE:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final button:Lcom/squareup/server/account/protos/Notification$Button;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.Notification$Button#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final buttons:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.Notification$Button#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x5
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/Notification$Button;",
            ">;"
        }
    .end annotation
.end field

.field public final delivery_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final lock_out:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x6
    .end annotation
.end field

.field public final message:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final title:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 33
    new-instance v0, Lcom/squareup/server/account/protos/Notification$ProtoAdapter_Notification;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/Notification$ProtoAdapter_Notification;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/Notification;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 35
    sget-object v0, Lcom/squareup/server/account/protos/Notification;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/Notification;->CREATOR:Landroid/os/Parcelable$Creator;

    const/4 v0, 0x0

    .line 45
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/Notification;->DEFAULT_LOCK_OUT:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/server/account/protos/Notification$Button;Ljava/util/List;Ljava/lang/Boolean;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/server/account/protos/Notification$Button;",
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/Notification$Button;",
            ">;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .line 92
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/server/account/protos/Notification;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/server/account/protos/Notification$Button;Ljava/util/List;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/server/account/protos/Notification$Button;Ljava/util/List;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/server/account/protos/Notification$Button;",
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/Notification$Button;",
            ">;",
            "Ljava/lang/Boolean;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 98
    sget-object v0, Lcom/squareup/server/account/protos/Notification;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 99
    iput-object p1, p0, Lcom/squareup/server/account/protos/Notification;->delivery_id:Ljava/lang/String;

    .line 100
    iput-object p2, p0, Lcom/squareup/server/account/protos/Notification;->title:Ljava/lang/String;

    .line 101
    iput-object p3, p0, Lcom/squareup/server/account/protos/Notification;->message:Ljava/lang/String;

    .line 102
    iput-object p4, p0, Lcom/squareup/server/account/protos/Notification;->button:Lcom/squareup/server/account/protos/Notification$Button;

    const-string p1, "buttons"

    .line 103
    invoke-static {p1, p5}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/server/account/protos/Notification;->buttons:Ljava/util/List;

    .line 104
    iput-object p6, p0, Lcom/squareup/server/account/protos/Notification;->lock_out:Ljava/lang/Boolean;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/Notification$Builder;)Lcom/squareup/server/account/protos/Notification$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 190
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Notification;->newBuilder()Lcom/squareup/server/account/protos/Notification$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 123
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/Notification;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 124
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/Notification;

    .line 125
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Notification;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/Notification;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Notification;->delivery_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Notification;->delivery_id:Ljava/lang/String;

    .line 126
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Notification;->title:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Notification;->title:Ljava/lang/String;

    .line 127
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Notification;->message:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Notification;->message:Ljava/lang/String;

    .line 128
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Notification;->button:Lcom/squareup/server/account/protos/Notification$Button;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Notification;->button:Lcom/squareup/server/account/protos/Notification$Button;

    .line 129
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Notification;->buttons:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Notification;->buttons:Ljava/util/List;

    .line 130
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Notification;->lock_out:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/server/account/protos/Notification;->lock_out:Ljava/lang/Boolean;

    .line 131
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 136
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_5

    .line 138
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Notification;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 139
    iget-object v1, p0, Lcom/squareup/server/account/protos/Notification;->delivery_id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 140
    iget-object v1, p0, Lcom/squareup/server/account/protos/Notification;->title:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 141
    iget-object v1, p0, Lcom/squareup/server/account/protos/Notification;->message:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 142
    iget-object v1, p0, Lcom/squareup/server/account/protos/Notification;->button:Lcom/squareup/server/account/protos/Notification$Button;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/Notification$Button;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 143
    iget-object v1, p0, Lcom/squareup/server/account/protos/Notification;->buttons:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 144
    iget-object v1, p0, Lcom/squareup/server/account/protos/Notification;->lock_out:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    .line 145
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_5
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/Notification$Builder;
    .locals 2

    .line 109
    new-instance v0, Lcom/squareup/server/account/protos/Notification$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/Notification$Builder;-><init>()V

    .line 110
    iget-object v1, p0, Lcom/squareup/server/account/protos/Notification;->delivery_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Notification$Builder;->delivery_id:Ljava/lang/String;

    .line 111
    iget-object v1, p0, Lcom/squareup/server/account/protos/Notification;->title:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Notification$Builder;->title:Ljava/lang/String;

    .line 112
    iget-object v1, p0, Lcom/squareup/server/account/protos/Notification;->message:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Notification$Builder;->message:Ljava/lang/String;

    .line 113
    iget-object v1, p0, Lcom/squareup/server/account/protos/Notification;->button:Lcom/squareup/server/account/protos/Notification$Button;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Notification$Builder;->button:Lcom/squareup/server/account/protos/Notification$Button;

    .line 114
    iget-object v1, p0, Lcom/squareup/server/account/protos/Notification;->buttons:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/server/account/protos/Notification$Builder;->buttons:Ljava/util/List;

    .line 115
    iget-object v1, p0, Lcom/squareup/server/account/protos/Notification;->lock_out:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Notification$Builder;->lock_out:Ljava/lang/Boolean;

    .line 116
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Notification;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Notification$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 32
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Notification;->newBuilder()Lcom/squareup/server/account/protos/Notification$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/Notification;)Lcom/squareup/server/account/protos/Notification;
    .locals 2

    .line 180
    iget-object v0, p1, Lcom/squareup/server/account/protos/Notification;->delivery_id:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Notification;->requireBuilder(Lcom/squareup/server/account/protos/Notification$Builder;)Lcom/squareup/server/account/protos/Notification$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Notification;->delivery_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Notification$Builder;->delivery_id(Ljava/lang/String;)Lcom/squareup/server/account/protos/Notification$Builder;

    move-result-object v1

    .line 181
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/Notification;->title:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Notification;->requireBuilder(Lcom/squareup/server/account/protos/Notification$Builder;)Lcom/squareup/server/account/protos/Notification$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Notification;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Notification$Builder;->title(Ljava/lang/String;)Lcom/squareup/server/account/protos/Notification$Builder;

    move-result-object v1

    .line 182
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/Notification;->message:Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Notification;->requireBuilder(Lcom/squareup/server/account/protos/Notification$Builder;)Lcom/squareup/server/account/protos/Notification$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Notification;->message:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Notification$Builder;->message(Ljava/lang/String;)Lcom/squareup/server/account/protos/Notification$Builder;

    move-result-object v1

    .line 183
    :cond_2
    iget-object v0, p1, Lcom/squareup/server/account/protos/Notification;->button:Lcom/squareup/server/account/protos/Notification$Button;

    if-eqz v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Notification;->requireBuilder(Lcom/squareup/server/account/protos/Notification$Builder;)Lcom/squareup/server/account/protos/Notification$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Notification;->button:Lcom/squareup/server/account/protos/Notification$Button;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Notification$Builder;->button(Lcom/squareup/server/account/protos/Notification$Button;)Lcom/squareup/server/account/protos/Notification$Builder;

    move-result-object v1

    .line 184
    :cond_3
    iget-object v0, p1, Lcom/squareup/server/account/protos/Notification;->buttons:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Notification;->requireBuilder(Lcom/squareup/server/account/protos/Notification$Builder;)Lcom/squareup/server/account/protos/Notification$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Notification;->buttons:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Notification$Builder;->buttons(Ljava/util/List;)Lcom/squareup/server/account/protos/Notification$Builder;

    move-result-object v1

    .line 185
    :cond_4
    iget-object v0, p1, Lcom/squareup/server/account/protos/Notification;->lock_out:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Notification;->requireBuilder(Lcom/squareup/server/account/protos/Notification$Builder;)Lcom/squareup/server/account/protos/Notification$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/Notification;->lock_out:Ljava/lang/Boolean;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/Notification$Builder;->lock_out(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Notification$Builder;

    move-result-object v1

    :cond_5
    if-nez v1, :cond_6

    move-object p1, p0

    goto :goto_0

    .line 186
    :cond_6
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/Notification$Builder;->build()Lcom/squareup/server/account/protos/Notification;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 32
    check-cast p1, Lcom/squareup/server/account/protos/Notification;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/Notification;->overlay(Lcom/squareup/server/account/protos/Notification;)Lcom/squareup/server/account/protos/Notification;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/Notification;
    .locals 3

    .line 165
    iget-object v0, p0, Lcom/squareup/server/account/protos/Notification;->button:Lcom/squareup/server/account/protos/Notification$Button;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 166
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/Notification$Button;->populateDefaults()Lcom/squareup/server/account/protos/Notification$Button;

    move-result-object v0

    .line 167
    iget-object v2, p0, Lcom/squareup/server/account/protos/Notification;->button:Lcom/squareup/server/account/protos/Notification$Button;

    if-eq v0, v2, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Notification;->requireBuilder(Lcom/squareup/server/account/protos/Notification$Builder;)Lcom/squareup/server/account/protos/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/Notification$Builder;->button(Lcom/squareup/server/account/protos/Notification$Button;)Lcom/squareup/server/account/protos/Notification$Builder;

    move-result-object v1

    .line 169
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/protos/Notification;->buttons:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 170
    invoke-static {v0}, Lcom/squareup/wired/Wired;->populateDefaults(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 171
    iget-object v2, p0, Lcom/squareup/server/account/protos/Notification;->buttons:Ljava/util/List;

    if-eq v0, v2, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Notification;->requireBuilder(Lcom/squareup/server/account/protos/Notification$Builder;)Lcom/squareup/server/account/protos/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/Notification$Builder;->buttons(Ljava/util/List;)Lcom/squareup/server/account/protos/Notification$Builder;

    move-result-object v1

    .line 173
    :cond_1
    iget-object v0, p0, Lcom/squareup/server/account/protos/Notification;->lock_out:Ljava/lang/Boolean;

    if-nez v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Notification;->requireBuilder(Lcom/squareup/server/account/protos/Notification$Builder;)Lcom/squareup/server/account/protos/Notification$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/Notification;->DEFAULT_LOCK_OUT:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Notification$Builder;->lock_out(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Notification$Builder;

    move-result-object v1

    :cond_2
    if-nez v1, :cond_3

    move-object v0, p0

    goto :goto_0

    .line 174
    :cond_3
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/Notification$Builder;->build()Lcom/squareup/server/account/protos/Notification;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 32
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Notification;->populateDefaults()Lcom/squareup/server/account/protos/Notification;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 152
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 153
    iget-object v1, p0, Lcom/squareup/server/account/protos/Notification;->delivery_id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", delivery_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Notification;->delivery_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 154
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/Notification;->title:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Notification;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    :cond_1
    iget-object v1, p0, Lcom/squareup/server/account/protos/Notification;->message:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", message="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Notification;->message:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 156
    :cond_2
    iget-object v1, p0, Lcom/squareup/server/account/protos/Notification;->button:Lcom/squareup/server/account/protos/Notification$Button;

    if-eqz v1, :cond_3

    const-string v1, ", button="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Notification;->button:Lcom/squareup/server/account/protos/Notification$Button;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 157
    :cond_3
    iget-object v1, p0, Lcom/squareup/server/account/protos/Notification;->buttons:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, ", buttons="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Notification;->buttons:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 158
    :cond_4
    iget-object v1, p0, Lcom/squareup/server/account/protos/Notification;->lock_out:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    const-string v1, ", lock_out="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Notification;->lock_out:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Notification{"

    .line 159
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
