.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FlagsAndPermissions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public can_create_edit_services:Ljava/lang/Boolean;

.field public has_extra_create_edit_service_options:Ljava/lang/Boolean;

.field public library_list_show_services:Ljava/lang/Boolean;

.field public should_show_display_price:Ljava/lang/Boolean;

.field public show_phase_three_fields:Ljava/lang/Boolean;

.field public support_appointment_items_v2:Ljava/lang/Boolean;

.field public use_itemized_payments_tutorial:Ljava/lang/Boolean;

.field public use_items_tutorial:Ljava/lang/Boolean;

.field public use_pos_intent_payments_tutorial:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15010
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;
    .locals 12

    .line 15069
    new-instance v11, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;->use_items_tutorial:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;->library_list_show_services:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;->support_appointment_items_v2:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;->can_create_edit_services:Ljava/lang/Boolean;

    iget-object v5, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;->has_extra_create_edit_service_options:Ljava/lang/Boolean;

    iget-object v6, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;->should_show_display_price:Ljava/lang/Boolean;

    iget-object v7, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;->show_phase_three_fields:Ljava/lang/Boolean;

    iget-object v8, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;->use_itemized_payments_tutorial:Ljava/lang/Boolean;

    iget-object v9, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;->use_pos_intent_payments_tutorial:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v10

    move-object v0, v11

    invoke-direct/range {v0 .. v10}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v11
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 14991
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Items;

    move-result-object v0

    return-object v0
.end method

.method public can_create_edit_services(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 15034
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;->can_create_edit_services:Ljava/lang/Boolean;

    return-object p0
.end method

.method public has_extra_create_edit_service_options(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 15041
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;->has_extra_create_edit_service_options:Ljava/lang/Boolean;

    return-object p0
.end method

.method public library_list_show_services(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;
    .locals 0

    .line 15022
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;->library_list_show_services:Ljava/lang/Boolean;

    return-object p0
.end method

.method public should_show_display_price(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 15047
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;->should_show_display_price:Ljava/lang/Boolean;

    return-object p0
.end method

.method public show_phase_three_fields(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 15053
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;->show_phase_three_fields:Ljava/lang/Boolean;

    return-object p0
.end method

.method public support_appointment_items_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 15028
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;->support_appointment_items_v2:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_itemized_payments_tutorial(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;
    .locals 0

    .line 15058
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;->use_itemized_payments_tutorial:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_items_tutorial(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;
    .locals 0

    .line 15017
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;->use_items_tutorial:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_pos_intent_payments_tutorial(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;
    .locals 0

    .line 15063
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Items$Builder;->use_pos_intent_payments_tutorial:Ljava/lang/Boolean;

    return-object p0
.end method
