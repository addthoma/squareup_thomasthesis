.class public final enum Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;
.super Ljava/lang/Enum;
.source "LoyaltyPointsExpirationPolicy.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type$ProtoAdapter_Type;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum TIME_ISSUED:Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;

.field public static final enum UNKNOWN:Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 98
    new-instance v0, Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;->UNKNOWN:Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;

    .line 100
    new-instance v0, Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;

    const/4 v2, 0x1

    const-string v3, "TIME_ISSUED"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;->TIME_ISSUED:Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;

    .line 97
    sget-object v3, Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;->UNKNOWN:Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;->TIME_ISSUED:Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;->$VALUES:[Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;

    .line 102
    new-instance v0, Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type$ProtoAdapter_Type;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type$ProtoAdapter_Type;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 106
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 107
    iput p3, p0, Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;
    .locals 1

    if-eqz p0, :cond_1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 116
    :cond_0
    sget-object p0, Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;->TIME_ISSUED:Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;

    return-object p0

    .line 115
    :cond_1
    sget-object p0, Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;->UNKNOWN:Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;
    .locals 1

    .line 97
    const-class v0, Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;

    return-object p0
.end method

.method public static values()[Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;
    .locals 1

    .line 97
    sget-object v0, Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;->$VALUES:[Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;

    invoke-virtual {v0}, [Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 123
    iget v0, p0, Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;->value:I

    return v0
.end method
