.class public final Lcom/squareup/server/account/protos/Notification$Button$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Notification.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/Notification$Button;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/Notification$Button;",
        "Lcom/squareup/server/account/protos/Notification$Button$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public label:Ljava/lang/String;

.field public url:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 344
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/Notification$Button;
    .locals 4

    .line 359
    new-instance v0, Lcom/squareup/server/account/protos/Notification$Button;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Notification$Button$Builder;->url:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/server/account/protos/Notification$Button$Builder;->label:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/server/account/protos/Notification$Button;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 339
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Notification$Button$Builder;->build()Lcom/squareup/server/account/protos/Notification$Button;

    move-result-object v0

    return-object v0
.end method

.method public label(Ljava/lang/String;)Lcom/squareup/server/account/protos/Notification$Button$Builder;
    .locals 0

    .line 353
    iput-object p1, p0, Lcom/squareup/server/account/protos/Notification$Button$Builder;->label:Ljava/lang/String;

    return-object p0
.end method

.method public url(Ljava/lang/String;)Lcom/squareup/server/account/protos/Notification$Button$Builder;
    .locals 0

    .line 348
    iput-object p1, p0, Lcom/squareup/server/account/protos/Notification$Button$Builder;->url:Ljava/lang/String;

    return-object p0
.end method
