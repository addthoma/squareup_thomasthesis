.class public final Lcom/squareup/server/account/protos/Limits;
.super Lcom/squareup/wire/AndroidMessage;
.source "Limits.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/Limits$ProtoAdapter_Limits;,
        Lcom/squareup/server/account/protos/Limits$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/Limits;",
        "Lcom/squareup/server/account/protos/Limits$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/Limits;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/Limits;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/Limits;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/Limits;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_EMONEY_TRANSACTION_MIN_CENTS:Ljava/lang/Long;

.field public static final DEFAULT_GIFT_CARD_MAX_ACTIVATION_AMOUNT_CENTS:Ljava/lang/Long;

.field public static final DEFAULT_GIFT_CARD_MAX_TOTAL_PURCHASE_AMOUNT_CENTS:Ljava/lang/Long;

.field public static final DEFAULT_SKIP_RECEIPT_MAX_CENTS:Ljava/lang/Long;

.field public static final DEFAULT_SUICA_TRANSACTION_MAX_CENTS:Ljava/lang/Long;

.field public static final DEFAULT_TRANSACTION_MAX_CENTS:Ljava/lang/Long;

.field public static final DEFAULT_TRANSACTION_MIN_CENTS:Ljava/lang/Long;

.field private static final serialVersionUID:J


# instance fields
.field public final emoney_transaction_min_cents:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x6
    .end annotation
.end field

.field public final gift_card_max_activation_amount_cents:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x4
    .end annotation
.end field

.field public final gift_card_max_total_purchase_amount_cents:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x5
    .end annotation
.end field

.field public final skip_receipt_max_cents:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x3
    .end annotation
.end field

.field public final suica_transaction_max_cents:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x7
    .end annotation
.end field

.field public final transaction_max_cents:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x2
    .end annotation
.end field

.field public final transaction_min_cents:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 32
    new-instance v0, Lcom/squareup/server/account/protos/Limits$ProtoAdapter_Limits;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/Limits$ProtoAdapter_Limits;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/Limits;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 34
    sget-object v0, Lcom/squareup/server/account/protos/Limits;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/Limits;->CREATOR:Landroid/os/Parcelable$Creator;

    const-wide/16 v0, 0x0

    .line 38
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/Limits;->DEFAULT_TRANSACTION_MIN_CENTS:Ljava/lang/Long;

    .line 40
    sput-object v0, Lcom/squareup/server/account/protos/Limits;->DEFAULT_TRANSACTION_MAX_CENTS:Ljava/lang/Long;

    .line 42
    sput-object v0, Lcom/squareup/server/account/protos/Limits;->DEFAULT_SKIP_RECEIPT_MAX_CENTS:Ljava/lang/Long;

    .line 44
    sput-object v0, Lcom/squareup/server/account/protos/Limits;->DEFAULT_GIFT_CARD_MAX_ACTIVATION_AMOUNT_CENTS:Ljava/lang/Long;

    .line 46
    sput-object v0, Lcom/squareup/server/account/protos/Limits;->DEFAULT_GIFT_CARD_MAX_TOTAL_PURCHASE_AMOUNT_CENTS:Ljava/lang/Long;

    .line 48
    sput-object v0, Lcom/squareup/server/account/protos/Limits;->DEFAULT_EMONEY_TRANSACTION_MIN_CENTS:Ljava/lang/Long;

    .line 50
    sput-object v0, Lcom/squareup/server/account/protos/Limits;->DEFAULT_SUICA_TRANSACTION_MAX_CENTS:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;)V
    .locals 9

    .line 114
    sget-object v8, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/server/account/protos/Limits;-><init>(Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Lokio/ByteString;)V
    .locals 1

    .line 122
    sget-object v0, Lcom/squareup/server/account/protos/Limits;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p8}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 123
    iput-object p1, p0, Lcom/squareup/server/account/protos/Limits;->transaction_min_cents:Ljava/lang/Long;

    .line 124
    iput-object p2, p0, Lcom/squareup/server/account/protos/Limits;->transaction_max_cents:Ljava/lang/Long;

    .line 125
    iput-object p3, p0, Lcom/squareup/server/account/protos/Limits;->skip_receipt_max_cents:Ljava/lang/Long;

    .line 126
    iput-object p4, p0, Lcom/squareup/server/account/protos/Limits;->gift_card_max_activation_amount_cents:Ljava/lang/Long;

    .line 127
    iput-object p5, p0, Lcom/squareup/server/account/protos/Limits;->gift_card_max_total_purchase_amount_cents:Ljava/lang/Long;

    .line 128
    iput-object p6, p0, Lcom/squareup/server/account/protos/Limits;->emoney_transaction_min_cents:Ljava/lang/Long;

    .line 129
    iput-object p7, p0, Lcom/squareup/server/account/protos/Limits;->suica_transaction_max_cents:Ljava/lang/Long;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/Limits$Builder;)Lcom/squareup/server/account/protos/Limits$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 218
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Limits;->newBuilder()Lcom/squareup/server/account/protos/Limits$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 149
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/Limits;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 150
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/Limits;

    .line 151
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Limits;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/Limits;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Limits;->transaction_min_cents:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Limits;->transaction_min_cents:Ljava/lang/Long;

    .line 152
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Limits;->transaction_max_cents:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Limits;->transaction_max_cents:Ljava/lang/Long;

    .line 153
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Limits;->skip_receipt_max_cents:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Limits;->skip_receipt_max_cents:Ljava/lang/Long;

    .line 154
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Limits;->gift_card_max_activation_amount_cents:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Limits;->gift_card_max_activation_amount_cents:Ljava/lang/Long;

    .line 155
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Limits;->gift_card_max_total_purchase_amount_cents:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Limits;->gift_card_max_total_purchase_amount_cents:Ljava/lang/Long;

    .line 156
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Limits;->emoney_transaction_min_cents:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Limits;->emoney_transaction_min_cents:Ljava/lang/Long;

    .line 157
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Limits;->suica_transaction_max_cents:Ljava/lang/Long;

    iget-object p1, p1, Lcom/squareup/server/account/protos/Limits;->suica_transaction_max_cents:Ljava/lang/Long;

    .line 158
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 163
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_7

    .line 165
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Limits;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 166
    iget-object v1, p0, Lcom/squareup/server/account/protos/Limits;->transaction_min_cents:Ljava/lang/Long;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 167
    iget-object v1, p0, Lcom/squareup/server/account/protos/Limits;->transaction_max_cents:Ljava/lang/Long;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 168
    iget-object v1, p0, Lcom/squareup/server/account/protos/Limits;->skip_receipt_max_cents:Ljava/lang/Long;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 169
    iget-object v1, p0, Lcom/squareup/server/account/protos/Limits;->gift_card_max_activation_amount_cents:Ljava/lang/Long;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 170
    iget-object v1, p0, Lcom/squareup/server/account/protos/Limits;->gift_card_max_total_purchase_amount_cents:Ljava/lang/Long;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 171
    iget-object v1, p0, Lcom/squareup/server/account/protos/Limits;->emoney_transaction_min_cents:Ljava/lang/Long;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 172
    iget-object v1, p0, Lcom/squareup/server/account/protos/Limits;->suica_transaction_max_cents:Ljava/lang/Long;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v2

    :cond_6
    add-int/2addr v0, v2

    .line 173
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_7
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/Limits$Builder;
    .locals 2

    .line 134
    new-instance v0, Lcom/squareup/server/account/protos/Limits$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/Limits$Builder;-><init>()V

    .line 135
    iget-object v1, p0, Lcom/squareup/server/account/protos/Limits;->transaction_min_cents:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Limits$Builder;->transaction_min_cents:Ljava/lang/Long;

    .line 136
    iget-object v1, p0, Lcom/squareup/server/account/protos/Limits;->transaction_max_cents:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Limits$Builder;->transaction_max_cents:Ljava/lang/Long;

    .line 137
    iget-object v1, p0, Lcom/squareup/server/account/protos/Limits;->skip_receipt_max_cents:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Limits$Builder;->skip_receipt_max_cents:Ljava/lang/Long;

    .line 138
    iget-object v1, p0, Lcom/squareup/server/account/protos/Limits;->gift_card_max_activation_amount_cents:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Limits$Builder;->gift_card_max_activation_amount_cents:Ljava/lang/Long;

    .line 139
    iget-object v1, p0, Lcom/squareup/server/account/protos/Limits;->gift_card_max_total_purchase_amount_cents:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Limits$Builder;->gift_card_max_total_purchase_amount_cents:Ljava/lang/Long;

    .line 140
    iget-object v1, p0, Lcom/squareup/server/account/protos/Limits;->emoney_transaction_min_cents:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Limits$Builder;->emoney_transaction_min_cents:Ljava/lang/Long;

    .line 141
    iget-object v1, p0, Lcom/squareup/server/account/protos/Limits;->suica_transaction_max_cents:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Limits$Builder;->suica_transaction_max_cents:Ljava/lang/Long;

    .line 142
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Limits;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Limits$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 31
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Limits;->newBuilder()Lcom/squareup/server/account/protos/Limits$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/Limits;)Lcom/squareup/server/account/protos/Limits;
    .locals 2

    .line 207
    iget-object v0, p1, Lcom/squareup/server/account/protos/Limits;->transaction_min_cents:Ljava/lang/Long;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Limits;->requireBuilder(Lcom/squareup/server/account/protos/Limits$Builder;)Lcom/squareup/server/account/protos/Limits$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Limits;->transaction_min_cents:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Limits$Builder;->transaction_min_cents(Ljava/lang/Long;)Lcom/squareup/server/account/protos/Limits$Builder;

    move-result-object v1

    .line 208
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/Limits;->transaction_max_cents:Ljava/lang/Long;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Limits;->requireBuilder(Lcom/squareup/server/account/protos/Limits$Builder;)Lcom/squareup/server/account/protos/Limits$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Limits;->transaction_max_cents:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Limits$Builder;->transaction_max_cents(Ljava/lang/Long;)Lcom/squareup/server/account/protos/Limits$Builder;

    move-result-object v1

    .line 209
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/Limits;->skip_receipt_max_cents:Ljava/lang/Long;

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Limits;->requireBuilder(Lcom/squareup/server/account/protos/Limits$Builder;)Lcom/squareup/server/account/protos/Limits$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Limits;->skip_receipt_max_cents:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Limits$Builder;->skip_receipt_max_cents(Ljava/lang/Long;)Lcom/squareup/server/account/protos/Limits$Builder;

    move-result-object v1

    .line 210
    :cond_2
    iget-object v0, p1, Lcom/squareup/server/account/protos/Limits;->gift_card_max_activation_amount_cents:Ljava/lang/Long;

    if-eqz v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Limits;->requireBuilder(Lcom/squareup/server/account/protos/Limits$Builder;)Lcom/squareup/server/account/protos/Limits$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Limits;->gift_card_max_activation_amount_cents:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Limits$Builder;->gift_card_max_activation_amount_cents(Ljava/lang/Long;)Lcom/squareup/server/account/protos/Limits$Builder;

    move-result-object v1

    .line 211
    :cond_3
    iget-object v0, p1, Lcom/squareup/server/account/protos/Limits;->gift_card_max_total_purchase_amount_cents:Ljava/lang/Long;

    if-eqz v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Limits;->requireBuilder(Lcom/squareup/server/account/protos/Limits$Builder;)Lcom/squareup/server/account/protos/Limits$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Limits;->gift_card_max_total_purchase_amount_cents:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Limits$Builder;->gift_card_max_total_purchase_amount_cents(Ljava/lang/Long;)Lcom/squareup/server/account/protos/Limits$Builder;

    move-result-object v1

    .line 212
    :cond_4
    iget-object v0, p1, Lcom/squareup/server/account/protos/Limits;->emoney_transaction_min_cents:Ljava/lang/Long;

    if-eqz v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Limits;->requireBuilder(Lcom/squareup/server/account/protos/Limits$Builder;)Lcom/squareup/server/account/protos/Limits$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Limits;->emoney_transaction_min_cents:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Limits$Builder;->emoney_transaction_min_cents(Ljava/lang/Long;)Lcom/squareup/server/account/protos/Limits$Builder;

    move-result-object v1

    .line 213
    :cond_5
    iget-object v0, p1, Lcom/squareup/server/account/protos/Limits;->suica_transaction_max_cents:Ljava/lang/Long;

    if-eqz v0, :cond_6

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Limits;->requireBuilder(Lcom/squareup/server/account/protos/Limits$Builder;)Lcom/squareup/server/account/protos/Limits$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/Limits;->suica_transaction_max_cents:Ljava/lang/Long;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/Limits$Builder;->suica_transaction_max_cents(Ljava/lang/Long;)Lcom/squareup/server/account/protos/Limits$Builder;

    move-result-object v1

    :cond_6
    if-nez v1, :cond_7

    move-object p1, p0

    goto :goto_0

    .line 214
    :cond_7
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/Limits$Builder;->build()Lcom/squareup/server/account/protos/Limits;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 31
    check-cast p1, Lcom/squareup/server/account/protos/Limits;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/Limits;->overlay(Lcom/squareup/server/account/protos/Limits;)Lcom/squareup/server/account/protos/Limits;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/Limits;
    .locals 2

    .line 194
    iget-object v0, p0, Lcom/squareup/server/account/protos/Limits;->transaction_min_cents:Ljava/lang/Long;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Limits;->requireBuilder(Lcom/squareup/server/account/protos/Limits$Builder;)Lcom/squareup/server/account/protos/Limits$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/Limits;->DEFAULT_TRANSACTION_MIN_CENTS:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Limits$Builder;->transaction_min_cents(Ljava/lang/Long;)Lcom/squareup/server/account/protos/Limits$Builder;

    move-result-object v1

    .line 195
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/protos/Limits;->transaction_max_cents:Ljava/lang/Long;

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Limits;->requireBuilder(Lcom/squareup/server/account/protos/Limits$Builder;)Lcom/squareup/server/account/protos/Limits$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/Limits;->DEFAULT_TRANSACTION_MAX_CENTS:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Limits$Builder;->transaction_max_cents(Ljava/lang/Long;)Lcom/squareup/server/account/protos/Limits$Builder;

    move-result-object v1

    .line 196
    :cond_1
    iget-object v0, p0, Lcom/squareup/server/account/protos/Limits;->skip_receipt_max_cents:Ljava/lang/Long;

    if-nez v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Limits;->requireBuilder(Lcom/squareup/server/account/protos/Limits$Builder;)Lcom/squareup/server/account/protos/Limits$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/Limits;->DEFAULT_SKIP_RECEIPT_MAX_CENTS:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Limits$Builder;->skip_receipt_max_cents(Ljava/lang/Long;)Lcom/squareup/server/account/protos/Limits$Builder;

    move-result-object v1

    .line 197
    :cond_2
    iget-object v0, p0, Lcom/squareup/server/account/protos/Limits;->gift_card_max_activation_amount_cents:Ljava/lang/Long;

    if-nez v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Limits;->requireBuilder(Lcom/squareup/server/account/protos/Limits$Builder;)Lcom/squareup/server/account/protos/Limits$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/Limits;->DEFAULT_GIFT_CARD_MAX_ACTIVATION_AMOUNT_CENTS:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Limits$Builder;->gift_card_max_activation_amount_cents(Ljava/lang/Long;)Lcom/squareup/server/account/protos/Limits$Builder;

    move-result-object v1

    .line 198
    :cond_3
    iget-object v0, p0, Lcom/squareup/server/account/protos/Limits;->gift_card_max_total_purchase_amount_cents:Ljava/lang/Long;

    if-nez v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Limits;->requireBuilder(Lcom/squareup/server/account/protos/Limits$Builder;)Lcom/squareup/server/account/protos/Limits$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/Limits;->DEFAULT_GIFT_CARD_MAX_TOTAL_PURCHASE_AMOUNT_CENTS:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Limits$Builder;->gift_card_max_total_purchase_amount_cents(Ljava/lang/Long;)Lcom/squareup/server/account/protos/Limits$Builder;

    move-result-object v1

    .line 199
    :cond_4
    iget-object v0, p0, Lcom/squareup/server/account/protos/Limits;->emoney_transaction_min_cents:Ljava/lang/Long;

    if-nez v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Limits;->requireBuilder(Lcom/squareup/server/account/protos/Limits$Builder;)Lcom/squareup/server/account/protos/Limits$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/Limits;->DEFAULT_EMONEY_TRANSACTION_MIN_CENTS:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Limits$Builder;->emoney_transaction_min_cents(Ljava/lang/Long;)Lcom/squareup/server/account/protos/Limits$Builder;

    move-result-object v1

    .line 200
    :cond_5
    iget-object v0, p0, Lcom/squareup/server/account/protos/Limits;->suica_transaction_max_cents:Ljava/lang/Long;

    if-nez v0, :cond_6

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Limits;->requireBuilder(Lcom/squareup/server/account/protos/Limits$Builder;)Lcom/squareup/server/account/protos/Limits$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/Limits;->DEFAULT_SUICA_TRANSACTION_MAX_CENTS:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Limits$Builder;->suica_transaction_max_cents(Ljava/lang/Long;)Lcom/squareup/server/account/protos/Limits$Builder;

    move-result-object v1

    :cond_6
    if-nez v1, :cond_7

    move-object v0, p0

    goto :goto_0

    .line 201
    :cond_7
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/Limits$Builder;->build()Lcom/squareup/server/account/protos/Limits;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 31
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Limits;->populateDefaults()Lcom/squareup/server/account/protos/Limits;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 180
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 181
    iget-object v1, p0, Lcom/squareup/server/account/protos/Limits;->transaction_min_cents:Ljava/lang/Long;

    if-eqz v1, :cond_0

    const-string v1, ", transaction_min_cents="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Limits;->transaction_min_cents:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 182
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/Limits;->transaction_max_cents:Ljava/lang/Long;

    if-eqz v1, :cond_1

    const-string v1, ", transaction_max_cents="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Limits;->transaction_max_cents:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 183
    :cond_1
    iget-object v1, p0, Lcom/squareup/server/account/protos/Limits;->skip_receipt_max_cents:Ljava/lang/Long;

    if-eqz v1, :cond_2

    const-string v1, ", skip_receipt_max_cents="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Limits;->skip_receipt_max_cents:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 184
    :cond_2
    iget-object v1, p0, Lcom/squareup/server/account/protos/Limits;->gift_card_max_activation_amount_cents:Ljava/lang/Long;

    if-eqz v1, :cond_3

    const-string v1, ", gift_card_max_activation_amount_cents="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Limits;->gift_card_max_activation_amount_cents:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 185
    :cond_3
    iget-object v1, p0, Lcom/squareup/server/account/protos/Limits;->gift_card_max_total_purchase_amount_cents:Ljava/lang/Long;

    if-eqz v1, :cond_4

    const-string v1, ", gift_card_max_total_purchase_amount_cents="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Limits;->gift_card_max_total_purchase_amount_cents:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 186
    :cond_4
    iget-object v1, p0, Lcom/squareup/server/account/protos/Limits;->emoney_transaction_min_cents:Ljava/lang/Long;

    if-eqz v1, :cond_5

    const-string v1, ", emoney_transaction_min_cents="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Limits;->emoney_transaction_min_cents:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 187
    :cond_5
    iget-object v1, p0, Lcom/squareup/server/account/protos/Limits;->suica_transaction_max_cents:Ljava/lang/Long;

    if-eqz v1, :cond_6

    const-string v1, ", suica_transaction_max_cents="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Limits;->suica_transaction_max_cents:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_6
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Limits{"

    .line 188
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
