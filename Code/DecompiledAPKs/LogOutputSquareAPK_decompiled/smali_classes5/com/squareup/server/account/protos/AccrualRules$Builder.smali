.class public final Lcom/squareup/server/account/protos/AccrualRules$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AccrualRules.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/AccrualRules;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/AccrualRules;",
        "Lcom/squareup/server/account/protos/AccrualRules$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public accrual_rules_options:Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;

.field public item_rules:Lcom/squareup/server/account/protos/AccrualRules$ItemRules;

.field public spend_rate_rules:Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;

.field public visit_rules:Lcom/squareup/server/account/protos/AccrualRules$VisitRules;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 176
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public accrual_rules_options(Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;)Lcom/squareup/server/account/protos/AccrualRules$Builder;
    .locals 0

    .line 180
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccrualRules$Builder;->accrual_rules_options:Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;

    return-object p0
.end method

.method public build()Lcom/squareup/server/account/protos/AccrualRules;
    .locals 7

    .line 207
    new-instance v6, Lcom/squareup/server/account/protos/AccrualRules;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules$Builder;->accrual_rules_options:Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;

    iget-object v2, p0, Lcom/squareup/server/account/protos/AccrualRules$Builder;->visit_rules:Lcom/squareup/server/account/protos/AccrualRules$VisitRules;

    iget-object v3, p0, Lcom/squareup/server/account/protos/AccrualRules$Builder;->spend_rate_rules:Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;

    iget-object v4, p0, Lcom/squareup/server/account/protos/AccrualRules$Builder;->item_rules:Lcom/squareup/server/account/protos/AccrualRules$ItemRules;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/server/account/protos/AccrualRules;-><init>(Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;Lcom/squareup/server/account/protos/AccrualRules$VisitRules;Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;Lcom/squareup/server/account/protos/AccrualRules$ItemRules;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 167
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/AccrualRules$Builder;->build()Lcom/squareup/server/account/protos/AccrualRules;

    move-result-object v0

    return-object v0
.end method

.method public item_rules(Lcom/squareup/server/account/protos/AccrualRules$ItemRules;)Lcom/squareup/server/account/protos/AccrualRules$Builder;
    .locals 0

    .line 199
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccrualRules$Builder;->item_rules:Lcom/squareup/server/account/protos/AccrualRules$ItemRules;

    const/4 p1, 0x0

    .line 200
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccrualRules$Builder;->visit_rules:Lcom/squareup/server/account/protos/AccrualRules$VisitRules;

    .line 201
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccrualRules$Builder;->spend_rate_rules:Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;

    return-object p0
.end method

.method public spend_rate_rules(Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;)Lcom/squareup/server/account/protos/AccrualRules$Builder;
    .locals 0

    .line 192
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccrualRules$Builder;->spend_rate_rules:Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;

    const/4 p1, 0x0

    .line 193
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccrualRules$Builder;->visit_rules:Lcom/squareup/server/account/protos/AccrualRules$VisitRules;

    .line 194
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccrualRules$Builder;->item_rules:Lcom/squareup/server/account/protos/AccrualRules$ItemRules;

    return-object p0
.end method

.method public visit_rules(Lcom/squareup/server/account/protos/AccrualRules$VisitRules;)Lcom/squareup/server/account/protos/AccrualRules$Builder;
    .locals 0

    .line 185
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccrualRules$Builder;->visit_rules:Lcom/squareup/server/account/protos/AccrualRules$VisitRules;

    const/4 p1, 0x0

    .line 186
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccrualRules$Builder;->spend_rate_rules:Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;

    .line 187
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccrualRules$Builder;->item_rules:Lcom/squareup/server/account/protos/AccrualRules$ItemRules;

    return-object p0
.end method
