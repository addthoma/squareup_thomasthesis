.class public final Lcom/squareup/server/account/protos/PreferencesRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "PreferencesRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/PreferencesRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/PreferencesRequest;",
        "Lcom/squareup/server/account/protos/PreferencesRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public allow_instant_deposit:Ljava/lang/Boolean;

.field public cash_management_default_starting_amount:Lcom/squareup/protos/common/Money;

.field public cash_management_email_report:Ljava/lang/Boolean;

.field public cash_management_enabled:Ljava/lang/Boolean;

.field public cash_management_print_report:Ljava/lang/Boolean;

.field public cash_management_report_recipient_email:Ljava/lang/String;

.field public custom_tender_options_proto:Ljava/lang/String;

.field public customer_management_collect_after_checkout:Ljava/lang/Boolean;

.field public customer_management_collect_before_checkout:Ljava/lang/Boolean;

.field public customer_management_show_email_collection_screen:Ljava/lang/Boolean;

.field public customer_management_show_save_card_button_after_checkout:Ljava/lang/Boolean;

.field public customer_management_use_card_on_file:Ljava/lang/Boolean;

.field public employee_management_enabled_for_account:Ljava/lang/Boolean;

.field public employee_management_enabled_for_device:Ljava/lang/Boolean;

.field public employee_management_inactivity_timeout:Ljava/lang/String;

.field public employee_management_tracking_level:Ljava/lang/String;

.field public for_paper_signature_always_print_customer_copy:Ljava/lang/Boolean;

.field public for_paper_signature_print_additional_auth_slip:Ljava/lang/Boolean;

.field public for_paper_signature_use_quick_tip_receipt:Ljava/lang/Boolean;

.field public open_tickets_enabled:Ljava/lang/Boolean;

.field public requires_employee_logout_after_completed_transaction:Ljava/lang/Boolean;

.field public skip_signature:Ljava/lang/Boolean;

.field public skip_signature_always:Ljava/lang/Boolean;

.field public store_and_forward_enabled:Ljava/lang/Boolean;

.field public store_and_forward_single_transaction_limit:Lcom/squareup/protos/common/Money;

.field public terminal_connected_mode_enabled:Ljava/lang/Boolean;

.field public tipping_calculation_phase:Ljava/lang/String;

.field public tipping_custom_percentages:Ljava/lang/String;

.field public tipping_enabled:Ljava/lang/Boolean;

.field public tipping_use_custom_percentages:Ljava/lang/Boolean;

.field public tipping_use_manual_tip_entry:Ljava/lang/Boolean;

.field public use_allow_swipe_for_chip_cards:Ljava/lang/Boolean;

.field public use_curated_image_for_receipt:Ljava/lang/Boolean;

.field public use_open_tickets_menu_as_home_screen:Ljava/lang/Boolean;

.field public use_paper_signature:Ljava/lang/Boolean;

.field public use_predefined_tickets:Ljava/lang/Boolean;

.field public use_separate_tipping_screen:Ljava/lang/Boolean;

.field public using_time_tracking:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 737
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public allow_instant_deposit(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;
    .locals 0

    .line 935
    iput-object p1, p0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->allow_instant_deposit:Ljava/lang/Boolean;

    return-object p0
.end method

.method public build()Lcom/squareup/server/account/protos/PreferencesRequest;
    .locals 2

    .line 952
    new-instance v0, Lcom/squareup/server/account/protos/PreferencesRequest;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest;-><init>(Lcom/squareup/server/account/protos/PreferencesRequest$Builder;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 660
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->build()Lcom/squareup/server/account/protos/PreferencesRequest;

    move-result-object v0

    return-object v0
.end method

.method public cash_management_default_starting_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;
    .locals 0

    .line 894
    iput-object p1, p0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->cash_management_default_starting_amount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public cash_management_email_report(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;
    .locals 0

    .line 883
    iput-object p1, p0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->cash_management_email_report:Ljava/lang/Boolean;

    return-object p0
.end method

.method public cash_management_enabled(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;
    .locals 0

    .line 878
    iput-object p1, p0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->cash_management_enabled:Ljava/lang/Boolean;

    return-object p0
.end method

.method public cash_management_print_report(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;
    .locals 0

    .line 888
    iput-object p1, p0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->cash_management_print_report:Ljava/lang/Boolean;

    return-object p0
.end method

.method public cash_management_report_recipient_email(Ljava/lang/String;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;
    .locals 0

    .line 900
    iput-object p1, p0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->cash_management_report_recipient_email:Ljava/lang/String;

    return-object p0
.end method

.method public custom_tender_options_proto(Ljava/lang/String;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;
    .locals 0

    .line 857
    iput-object p1, p0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->custom_tender_options_proto:Ljava/lang/String;

    return-object p0
.end method

.method public customer_management_collect_after_checkout(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;
    .locals 0

    .line 912
    iput-object p1, p0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->customer_management_collect_after_checkout:Ljava/lang/Boolean;

    return-object p0
.end method

.method public customer_management_collect_before_checkout(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;
    .locals 0

    .line 906
    iput-object p1, p0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->customer_management_collect_before_checkout:Ljava/lang/Boolean;

    return-object p0
.end method

.method public customer_management_show_email_collection_screen(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;
    .locals 0

    .line 930
    iput-object p1, p0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->customer_management_show_email_collection_screen:Ljava/lang/Boolean;

    return-object p0
.end method

.method public customer_management_show_save_card_button_after_checkout(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;
    .locals 0

    .line 924
    iput-object p1, p0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->customer_management_show_save_card_button_after_checkout:Ljava/lang/Boolean;

    return-object p0
.end method

.method public customer_management_use_card_on_file(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;
    .locals 0

    .line 918
    iput-object p1, p0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->customer_management_use_card_on_file:Ljava/lang/Boolean;

    return-object p0
.end method

.method public employee_management_enabled_for_account(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;
    .locals 0

    .line 820
    iput-object p1, p0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->employee_management_enabled_for_account:Ljava/lang/Boolean;

    return-object p0
.end method

.method public employee_management_enabled_for_device(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;
    .locals 0

    .line 826
    iput-object p1, p0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->employee_management_enabled_for_device:Ljava/lang/Boolean;

    return-object p0
.end method

.method public employee_management_inactivity_timeout(Ljava/lang/String;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;
    .locals 0

    .line 832
    iput-object p1, p0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->employee_management_inactivity_timeout:Ljava/lang/String;

    return-object p0
.end method

.method public employee_management_tracking_level(Ljava/lang/String;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;
    .locals 0

    .line 837
    iput-object p1, p0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->employee_management_tracking_level:Ljava/lang/String;

    return-object p0
.end method

.method public for_paper_signature_always_print_customer_copy(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;
    .locals 0

    .line 802
    iput-object p1, p0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->for_paper_signature_always_print_customer_copy:Ljava/lang/Boolean;

    return-object p0
.end method

.method public for_paper_signature_print_additional_auth_slip(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;
    .locals 0

    .line 814
    iput-object p1, p0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->for_paper_signature_print_additional_auth_slip:Ljava/lang/Boolean;

    return-object p0
.end method

.method public for_paper_signature_use_quick_tip_receipt(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;
    .locals 0

    .line 808
    iput-object p1, p0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->for_paper_signature_use_quick_tip_receipt:Ljava/lang/Boolean;

    return-object p0
.end method

.method public open_tickets_enabled(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;
    .locals 0

    .line 791
    iput-object p1, p0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->open_tickets_enabled:Ljava/lang/Boolean;

    return-object p0
.end method

.method public requires_employee_logout_after_completed_transaction(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;
    .locals 0

    .line 946
    iput-object p1, p0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->requires_employee_logout_after_completed_transaction:Ljava/lang/Boolean;

    return-object p0
.end method

.method public skip_signature(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;
    .locals 0

    .line 765
    iput-object p1, p0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->skip_signature:Ljava/lang/Boolean;

    return-object p0
.end method

.method public skip_signature_always(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;
    .locals 0

    .line 862
    iput-object p1, p0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->skip_signature_always:Ljava/lang/Boolean;

    return-object p0
.end method

.method public store_and_forward_enabled(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;
    .locals 0

    .line 770
    iput-object p1, p0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->store_and_forward_enabled:Ljava/lang/Boolean;

    return-object p0
.end method

.method public store_and_forward_single_transaction_limit(Lcom/squareup/protos/common/Money;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;
    .locals 0

    .line 776
    iput-object p1, p0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->store_and_forward_single_transaction_limit:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public terminal_connected_mode_enabled(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;
    .locals 0

    .line 940
    iput-object p1, p0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->terminal_connected_mode_enabled:Ljava/lang/Boolean;

    return-object p0
.end method

.method public tipping_calculation_phase(Ljava/lang/String;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;
    .locals 0

    .line 847
    iput-object p1, p0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->tipping_calculation_phase:Ljava/lang/String;

    return-object p0
.end method

.method public tipping_custom_percentages(Ljava/lang/String;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;
    .locals 0

    .line 760
    iput-object p1, p0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->tipping_custom_percentages:Ljava/lang/String;

    return-object p0
.end method

.method public tipping_enabled(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;
    .locals 0

    .line 741
    iput-object p1, p0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->tipping_enabled:Ljava/lang/Boolean;

    return-object p0
.end method

.method public tipping_use_custom_percentages(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;
    .locals 0

    .line 746
    iput-object p1, p0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->tipping_use_custom_percentages:Ljava/lang/Boolean;

    return-object p0
.end method

.method public tipping_use_manual_tip_entry(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;
    .locals 0

    .line 751
    iput-object p1, p0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->tipping_use_manual_tip_entry:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_allow_swipe_for_chip_cards(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;
    .locals 0

    .line 842
    iput-object p1, p0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->use_allow_swipe_for_chip_cards:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_curated_image_for_receipt(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;
    .locals 0

    .line 781
    iput-object p1, p0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->use_curated_image_for_receipt:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_open_tickets_menu_as_home_screen(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;
    .locals 0

    .line 868
    iput-object p1, p0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->use_open_tickets_menu_as_home_screen:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_paper_signature(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;
    .locals 0

    .line 796
    iput-object p1, p0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->use_paper_signature:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_predefined_tickets(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;
    .locals 0

    .line 873
    iput-object p1, p0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->use_predefined_tickets:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_separate_tipping_screen(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;
    .locals 0

    .line 786
    iput-object p1, p0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->use_separate_tipping_screen:Ljava/lang/Boolean;

    return-object p0
.end method

.method public using_time_tracking(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;
    .locals 0

    .line 852
    iput-object p1, p0, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->using_time_tracking:Ljava/lang/Boolean;

    return-object p0
.end method
