.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FlagsAndPermissions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public account_type_selection:Ljava/lang/Boolean;

.field public common_debit_support:Ljava/lang/Boolean;

.field public felica_notification:Ljava/lang/Boolean;

.field public pinblock_format_v2:Ljava/lang/Boolean;

.field public quickchip_fw209030:Ljava/lang/Boolean;

.field public sonic_branding:Ljava/lang/Boolean;

.field public spoc_prng_seed:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14207
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public account_type_selection(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;
    .locals 0

    .line 14221
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;->account_type_selection:Ljava/lang/Boolean;

    return-object p0
.end method

.method public build()Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;
    .locals 10

    .line 14247
    new-instance v9, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;->quickchip_fw209030:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;->pinblock_format_v2:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;->account_type_selection:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;->spoc_prng_seed:Ljava/lang/Boolean;

    iget-object v5, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;->common_debit_support:Ljava/lang/Boolean;

    iget-object v6, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;->sonic_branding:Ljava/lang/Boolean;

    iget-object v7, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;->felica_notification:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v8

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v9
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 14192
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;

    move-result-object v0

    return-object v0
.end method

.method public common_debit_support(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;
    .locals 0

    .line 14231
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;->common_debit_support:Ljava/lang/Boolean;

    return-object p0
.end method

.method public felica_notification(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;
    .locals 0

    .line 14241
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;->felica_notification:Ljava/lang/Boolean;

    return-object p0
.end method

.method public pinblock_format_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;
    .locals 0

    .line 14216
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;->pinblock_format_v2:Ljava/lang/Boolean;

    return-object p0
.end method

.method public quickchip_fw209030(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;
    .locals 0

    .line 14211
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;->quickchip_fw209030:Ljava/lang/Boolean;

    return-object p0
.end method

.method public sonic_branding(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;
    .locals 0

    .line 14236
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;->sonic_branding:Ljava/lang/Boolean;

    return-object p0
.end method

.method public spoc_prng_seed(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;
    .locals 0

    .line 14226
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;->spoc_prng_seed:Ljava/lang/Boolean;

    return-object p0
.end method
