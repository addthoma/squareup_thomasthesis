.class public final Lcom/squareup/server/account/protos/Cardreaders;
.super Lcom/squareup/wire/AndroidMessage;
.source "Cardreaders.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/Cardreaders$ProtoAdapter_Cardreaders;,
        Lcom/squareup/server/account/protos/Cardreaders$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/Cardreaders;",
        "Lcom/squareup/server/account/protos/Cardreaders$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/Cardreaders;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/Cardreaders;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/Cardreaders;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/Cardreaders;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_DISABLE_MAGSTRIPE_PROCESSING:Ljava/lang/Boolean;

.field public static final DEFAULT_ENABLE_EMONEY_SPEEDTEST:Ljava/lang/Boolean;

.field public static final DEFAULT_R12_EARLY_K400_POWERUP:Ljava/lang/Boolean;

.field public static final DEFAULT_RECORD_TMN_TIMINGS:Ljava/lang/Boolean;

.field public static final DEFAULT_REORDER_IN_APP:Ljava/lang/Boolean;

.field public static final DEFAULT_REORDER_IN_APP_EMAIL_CONFIRM:Ljava/lang/Boolean;

.field public static final DEFAULT_SHOW_ORDER_READER_STATUS_IN_APP:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_V2_BLE_STATE_MACHINE:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_V2_CARDREADERS:Ljava/lang/Boolean;

.field public static final DEFAULT_VERBOSE_TMN_TIMINGS:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final disable_magstripe_processing:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x8
    .end annotation
.end field

.field public final enable_emoney_speedtest:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xb
    .end annotation
.end field

.field public final r12_early_k400_powerup:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field

.field public final record_tmn_timings:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x9
    .end annotation
.end field

.field public final reorder_in_app:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field

.field public final reorder_in_app_email_confirm:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5
    .end annotation
.end field

.field public final show_order_reader_status_in_app:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x6
    .end annotation
.end field

.field public final use_v2_ble_state_machine:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x7
    .end annotation
.end field

.field public final use_v2_cardreaders:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xc
    .end annotation
.end field

.field public final verbose_tmn_timings:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xa
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 29
    new-instance v0, Lcom/squareup/server/account/protos/Cardreaders$ProtoAdapter_Cardreaders;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/Cardreaders$ProtoAdapter_Cardreaders;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/Cardreaders;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 31
    sget-object v0, Lcom/squareup/server/account/protos/Cardreaders;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/Cardreaders;->CREATOR:Landroid/os/Parcelable$Creator;

    const/4 v0, 0x0

    .line 35
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/Cardreaders;->DEFAULT_R12_EARLY_K400_POWERUP:Ljava/lang/Boolean;

    .line 37
    sput-object v0, Lcom/squareup/server/account/protos/Cardreaders;->DEFAULT_REORDER_IN_APP:Ljava/lang/Boolean;

    .line 39
    sput-object v0, Lcom/squareup/server/account/protos/Cardreaders;->DEFAULT_REORDER_IN_APP_EMAIL_CONFIRM:Ljava/lang/Boolean;

    .line 41
    sput-object v0, Lcom/squareup/server/account/protos/Cardreaders;->DEFAULT_SHOW_ORDER_READER_STATUS_IN_APP:Ljava/lang/Boolean;

    .line 43
    sput-object v0, Lcom/squareup/server/account/protos/Cardreaders;->DEFAULT_USE_V2_BLE_STATE_MACHINE:Ljava/lang/Boolean;

    .line 45
    sput-object v0, Lcom/squareup/server/account/protos/Cardreaders;->DEFAULT_DISABLE_MAGSTRIPE_PROCESSING:Ljava/lang/Boolean;

    .line 47
    sput-object v0, Lcom/squareup/server/account/protos/Cardreaders;->DEFAULT_RECORD_TMN_TIMINGS:Ljava/lang/Boolean;

    .line 49
    sput-object v0, Lcom/squareup/server/account/protos/Cardreaders;->DEFAULT_VERBOSE_TMN_TIMINGS:Ljava/lang/Boolean;

    .line 51
    sput-object v0, Lcom/squareup/server/account/protos/Cardreaders;->DEFAULT_ENABLE_EMONEY_SPEEDTEST:Ljava/lang/Boolean;

    .line 53
    sput-object v0, Lcom/squareup/server/account/protos/Cardreaders;->DEFAULT_USE_V2_CARDREADERS:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 12

    .line 131
    sget-object v11, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/server/account/protos/Cardreaders;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 140
    sget-object v0, Lcom/squareup/server/account/protos/Cardreaders;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p11}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 141
    iput-object p1, p0, Lcom/squareup/server/account/protos/Cardreaders;->r12_early_k400_powerup:Ljava/lang/Boolean;

    .line 142
    iput-object p2, p0, Lcom/squareup/server/account/protos/Cardreaders;->reorder_in_app:Ljava/lang/Boolean;

    .line 143
    iput-object p3, p0, Lcom/squareup/server/account/protos/Cardreaders;->reorder_in_app_email_confirm:Ljava/lang/Boolean;

    .line 144
    iput-object p4, p0, Lcom/squareup/server/account/protos/Cardreaders;->show_order_reader_status_in_app:Ljava/lang/Boolean;

    .line 145
    iput-object p5, p0, Lcom/squareup/server/account/protos/Cardreaders;->use_v2_ble_state_machine:Ljava/lang/Boolean;

    .line 146
    iput-object p6, p0, Lcom/squareup/server/account/protos/Cardreaders;->disable_magstripe_processing:Ljava/lang/Boolean;

    .line 147
    iput-object p7, p0, Lcom/squareup/server/account/protos/Cardreaders;->record_tmn_timings:Ljava/lang/Boolean;

    .line 148
    iput-object p8, p0, Lcom/squareup/server/account/protos/Cardreaders;->verbose_tmn_timings:Ljava/lang/Boolean;

    .line 149
    iput-object p9, p0, Lcom/squareup/server/account/protos/Cardreaders;->enable_emoney_speedtest:Ljava/lang/Boolean;

    .line 150
    iput-object p10, p0, Lcom/squareup/server/account/protos/Cardreaders;->use_v2_cardreaders:Ljava/lang/Boolean;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/Cardreaders$Builder;)Lcom/squareup/server/account/protos/Cardreaders$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 257
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Cardreaders;->newBuilder()Lcom/squareup/server/account/protos/Cardreaders$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 173
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/Cardreaders;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 174
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/Cardreaders;

    .line 175
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Cardreaders;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/Cardreaders;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Cardreaders;->r12_early_k400_powerup:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Cardreaders;->r12_early_k400_powerup:Ljava/lang/Boolean;

    .line 176
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Cardreaders;->reorder_in_app:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Cardreaders;->reorder_in_app:Ljava/lang/Boolean;

    .line 177
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Cardreaders;->reorder_in_app_email_confirm:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Cardreaders;->reorder_in_app_email_confirm:Ljava/lang/Boolean;

    .line 178
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Cardreaders;->show_order_reader_status_in_app:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Cardreaders;->show_order_reader_status_in_app:Ljava/lang/Boolean;

    .line 179
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Cardreaders;->use_v2_ble_state_machine:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Cardreaders;->use_v2_ble_state_machine:Ljava/lang/Boolean;

    .line 180
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Cardreaders;->disable_magstripe_processing:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Cardreaders;->disable_magstripe_processing:Ljava/lang/Boolean;

    .line 181
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Cardreaders;->record_tmn_timings:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Cardreaders;->record_tmn_timings:Ljava/lang/Boolean;

    .line 182
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Cardreaders;->verbose_tmn_timings:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Cardreaders;->verbose_tmn_timings:Ljava/lang/Boolean;

    .line 183
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Cardreaders;->enable_emoney_speedtest:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Cardreaders;->enable_emoney_speedtest:Ljava/lang/Boolean;

    .line 184
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Cardreaders;->use_v2_cardreaders:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/server/account/protos/Cardreaders;->use_v2_cardreaders:Ljava/lang/Boolean;

    .line 185
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 190
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_a

    .line 192
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Cardreaders;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 193
    iget-object v1, p0, Lcom/squareup/server/account/protos/Cardreaders;->r12_early_k400_powerup:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 194
    iget-object v1, p0, Lcom/squareup/server/account/protos/Cardreaders;->reorder_in_app:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 195
    iget-object v1, p0, Lcom/squareup/server/account/protos/Cardreaders;->reorder_in_app_email_confirm:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 196
    iget-object v1, p0, Lcom/squareup/server/account/protos/Cardreaders;->show_order_reader_status_in_app:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 197
    iget-object v1, p0, Lcom/squareup/server/account/protos/Cardreaders;->use_v2_ble_state_machine:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 198
    iget-object v1, p0, Lcom/squareup/server/account/protos/Cardreaders;->disable_magstripe_processing:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 199
    iget-object v1, p0, Lcom/squareup/server/account/protos/Cardreaders;->record_tmn_timings:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 200
    iget-object v1, p0, Lcom/squareup/server/account/protos/Cardreaders;->verbose_tmn_timings:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 201
    iget-object v1, p0, Lcom/squareup/server/account/protos/Cardreaders;->enable_emoney_speedtest:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 202
    iget-object v1, p0, Lcom/squareup/server/account/protos/Cardreaders;->use_v2_cardreaders:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_9
    add-int/2addr v0, v2

    .line 203
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_a
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/Cardreaders$Builder;
    .locals 2

    .line 155
    new-instance v0, Lcom/squareup/server/account/protos/Cardreaders$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/Cardreaders$Builder;-><init>()V

    .line 156
    iget-object v1, p0, Lcom/squareup/server/account/protos/Cardreaders;->r12_early_k400_powerup:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Cardreaders$Builder;->r12_early_k400_powerup:Ljava/lang/Boolean;

    .line 157
    iget-object v1, p0, Lcom/squareup/server/account/protos/Cardreaders;->reorder_in_app:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Cardreaders$Builder;->reorder_in_app:Ljava/lang/Boolean;

    .line 158
    iget-object v1, p0, Lcom/squareup/server/account/protos/Cardreaders;->reorder_in_app_email_confirm:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Cardreaders$Builder;->reorder_in_app_email_confirm:Ljava/lang/Boolean;

    .line 159
    iget-object v1, p0, Lcom/squareup/server/account/protos/Cardreaders;->show_order_reader_status_in_app:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Cardreaders$Builder;->show_order_reader_status_in_app:Ljava/lang/Boolean;

    .line 160
    iget-object v1, p0, Lcom/squareup/server/account/protos/Cardreaders;->use_v2_ble_state_machine:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Cardreaders$Builder;->use_v2_ble_state_machine:Ljava/lang/Boolean;

    .line 161
    iget-object v1, p0, Lcom/squareup/server/account/protos/Cardreaders;->disable_magstripe_processing:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Cardreaders$Builder;->disable_magstripe_processing:Ljava/lang/Boolean;

    .line 162
    iget-object v1, p0, Lcom/squareup/server/account/protos/Cardreaders;->record_tmn_timings:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Cardreaders$Builder;->record_tmn_timings:Ljava/lang/Boolean;

    .line 163
    iget-object v1, p0, Lcom/squareup/server/account/protos/Cardreaders;->verbose_tmn_timings:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Cardreaders$Builder;->verbose_tmn_timings:Ljava/lang/Boolean;

    .line 164
    iget-object v1, p0, Lcom/squareup/server/account/protos/Cardreaders;->enable_emoney_speedtest:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Cardreaders$Builder;->enable_emoney_speedtest:Ljava/lang/Boolean;

    .line 165
    iget-object v1, p0, Lcom/squareup/server/account/protos/Cardreaders;->use_v2_cardreaders:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Cardreaders$Builder;->use_v2_cardreaders:Ljava/lang/Boolean;

    .line 166
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Cardreaders;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Cardreaders$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 28
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Cardreaders;->newBuilder()Lcom/squareup/server/account/protos/Cardreaders$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/Cardreaders;)Lcom/squareup/server/account/protos/Cardreaders;
    .locals 2

    .line 243
    iget-object v0, p1, Lcom/squareup/server/account/protos/Cardreaders;->r12_early_k400_powerup:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Cardreaders;->requireBuilder(Lcom/squareup/server/account/protos/Cardreaders$Builder;)Lcom/squareup/server/account/protos/Cardreaders$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Cardreaders;->r12_early_k400_powerup:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Cardreaders$Builder;->r12_early_k400_powerup(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Cardreaders$Builder;

    move-result-object v1

    .line 244
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/Cardreaders;->reorder_in_app:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Cardreaders;->requireBuilder(Lcom/squareup/server/account/protos/Cardreaders$Builder;)Lcom/squareup/server/account/protos/Cardreaders$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Cardreaders;->reorder_in_app:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Cardreaders$Builder;->reorder_in_app(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Cardreaders$Builder;

    move-result-object v1

    .line 245
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/Cardreaders;->reorder_in_app_email_confirm:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Cardreaders;->requireBuilder(Lcom/squareup/server/account/protos/Cardreaders$Builder;)Lcom/squareup/server/account/protos/Cardreaders$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Cardreaders;->reorder_in_app_email_confirm:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Cardreaders$Builder;->reorder_in_app_email_confirm(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Cardreaders$Builder;

    move-result-object v1

    .line 246
    :cond_2
    iget-object v0, p1, Lcom/squareup/server/account/protos/Cardreaders;->show_order_reader_status_in_app:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Cardreaders;->requireBuilder(Lcom/squareup/server/account/protos/Cardreaders$Builder;)Lcom/squareup/server/account/protos/Cardreaders$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Cardreaders;->show_order_reader_status_in_app:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Cardreaders$Builder;->show_order_reader_status_in_app(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Cardreaders$Builder;

    move-result-object v1

    .line 247
    :cond_3
    iget-object v0, p1, Lcom/squareup/server/account/protos/Cardreaders;->use_v2_ble_state_machine:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Cardreaders;->requireBuilder(Lcom/squareup/server/account/protos/Cardreaders$Builder;)Lcom/squareup/server/account/protos/Cardreaders$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Cardreaders;->use_v2_ble_state_machine:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Cardreaders$Builder;->use_v2_ble_state_machine(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Cardreaders$Builder;

    move-result-object v1

    .line 248
    :cond_4
    iget-object v0, p1, Lcom/squareup/server/account/protos/Cardreaders;->disable_magstripe_processing:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Cardreaders;->requireBuilder(Lcom/squareup/server/account/protos/Cardreaders$Builder;)Lcom/squareup/server/account/protos/Cardreaders$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Cardreaders;->disable_magstripe_processing:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Cardreaders$Builder;->disable_magstripe_processing(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Cardreaders$Builder;

    move-result-object v1

    .line 249
    :cond_5
    iget-object v0, p1, Lcom/squareup/server/account/protos/Cardreaders;->record_tmn_timings:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Cardreaders;->requireBuilder(Lcom/squareup/server/account/protos/Cardreaders$Builder;)Lcom/squareup/server/account/protos/Cardreaders$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Cardreaders;->record_tmn_timings:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Cardreaders$Builder;->record_tmn_timings(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Cardreaders$Builder;

    move-result-object v1

    .line 250
    :cond_6
    iget-object v0, p1, Lcom/squareup/server/account/protos/Cardreaders;->verbose_tmn_timings:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Cardreaders;->requireBuilder(Lcom/squareup/server/account/protos/Cardreaders$Builder;)Lcom/squareup/server/account/protos/Cardreaders$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Cardreaders;->verbose_tmn_timings:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Cardreaders$Builder;->verbose_tmn_timings(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Cardreaders$Builder;

    move-result-object v1

    .line 251
    :cond_7
    iget-object v0, p1, Lcom/squareup/server/account/protos/Cardreaders;->enable_emoney_speedtest:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Cardreaders;->requireBuilder(Lcom/squareup/server/account/protos/Cardreaders$Builder;)Lcom/squareup/server/account/protos/Cardreaders$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Cardreaders;->enable_emoney_speedtest:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Cardreaders$Builder;->enable_emoney_speedtest(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Cardreaders$Builder;

    move-result-object v1

    .line 252
    :cond_8
    iget-object v0, p1, Lcom/squareup/server/account/protos/Cardreaders;->use_v2_cardreaders:Ljava/lang/Boolean;

    if-eqz v0, :cond_9

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Cardreaders;->requireBuilder(Lcom/squareup/server/account/protos/Cardreaders$Builder;)Lcom/squareup/server/account/protos/Cardreaders$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/Cardreaders;->use_v2_cardreaders:Ljava/lang/Boolean;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/Cardreaders$Builder;->use_v2_cardreaders(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Cardreaders$Builder;

    move-result-object v1

    :cond_9
    if-nez v1, :cond_a

    move-object p1, p0

    goto :goto_0

    .line 253
    :cond_a
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/Cardreaders$Builder;->build()Lcom/squareup/server/account/protos/Cardreaders;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 28
    check-cast p1, Lcom/squareup/server/account/protos/Cardreaders;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/Cardreaders;->overlay(Lcom/squareup/server/account/protos/Cardreaders;)Lcom/squareup/server/account/protos/Cardreaders;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/Cardreaders;
    .locals 2

    .line 227
    iget-object v0, p0, Lcom/squareup/server/account/protos/Cardreaders;->r12_early_k400_powerup:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Cardreaders;->requireBuilder(Lcom/squareup/server/account/protos/Cardreaders$Builder;)Lcom/squareup/server/account/protos/Cardreaders$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/Cardreaders;->DEFAULT_R12_EARLY_K400_POWERUP:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Cardreaders$Builder;->r12_early_k400_powerup(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Cardreaders$Builder;

    move-result-object v1

    .line 228
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/protos/Cardreaders;->reorder_in_app:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Cardreaders;->requireBuilder(Lcom/squareup/server/account/protos/Cardreaders$Builder;)Lcom/squareup/server/account/protos/Cardreaders$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/Cardreaders;->DEFAULT_REORDER_IN_APP:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Cardreaders$Builder;->reorder_in_app(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Cardreaders$Builder;

    move-result-object v1

    .line 229
    :cond_1
    iget-object v0, p0, Lcom/squareup/server/account/protos/Cardreaders;->reorder_in_app_email_confirm:Ljava/lang/Boolean;

    if-nez v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Cardreaders;->requireBuilder(Lcom/squareup/server/account/protos/Cardreaders$Builder;)Lcom/squareup/server/account/protos/Cardreaders$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/Cardreaders;->DEFAULT_REORDER_IN_APP_EMAIL_CONFIRM:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Cardreaders$Builder;->reorder_in_app_email_confirm(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Cardreaders$Builder;

    move-result-object v1

    .line 230
    :cond_2
    iget-object v0, p0, Lcom/squareup/server/account/protos/Cardreaders;->show_order_reader_status_in_app:Ljava/lang/Boolean;

    if-nez v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Cardreaders;->requireBuilder(Lcom/squareup/server/account/protos/Cardreaders$Builder;)Lcom/squareup/server/account/protos/Cardreaders$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/Cardreaders;->DEFAULT_SHOW_ORDER_READER_STATUS_IN_APP:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Cardreaders$Builder;->show_order_reader_status_in_app(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Cardreaders$Builder;

    move-result-object v1

    .line 231
    :cond_3
    iget-object v0, p0, Lcom/squareup/server/account/protos/Cardreaders;->use_v2_ble_state_machine:Ljava/lang/Boolean;

    if-nez v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Cardreaders;->requireBuilder(Lcom/squareup/server/account/protos/Cardreaders$Builder;)Lcom/squareup/server/account/protos/Cardreaders$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/Cardreaders;->DEFAULT_USE_V2_BLE_STATE_MACHINE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Cardreaders$Builder;->use_v2_ble_state_machine(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Cardreaders$Builder;

    move-result-object v1

    .line 232
    :cond_4
    iget-object v0, p0, Lcom/squareup/server/account/protos/Cardreaders;->disable_magstripe_processing:Ljava/lang/Boolean;

    if-nez v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Cardreaders;->requireBuilder(Lcom/squareup/server/account/protos/Cardreaders$Builder;)Lcom/squareup/server/account/protos/Cardreaders$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/Cardreaders;->DEFAULT_DISABLE_MAGSTRIPE_PROCESSING:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Cardreaders$Builder;->disable_magstripe_processing(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Cardreaders$Builder;

    move-result-object v1

    .line 233
    :cond_5
    iget-object v0, p0, Lcom/squareup/server/account/protos/Cardreaders;->record_tmn_timings:Ljava/lang/Boolean;

    if-nez v0, :cond_6

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Cardreaders;->requireBuilder(Lcom/squareup/server/account/protos/Cardreaders$Builder;)Lcom/squareup/server/account/protos/Cardreaders$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/Cardreaders;->DEFAULT_RECORD_TMN_TIMINGS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Cardreaders$Builder;->record_tmn_timings(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Cardreaders$Builder;

    move-result-object v1

    .line 234
    :cond_6
    iget-object v0, p0, Lcom/squareup/server/account/protos/Cardreaders;->verbose_tmn_timings:Ljava/lang/Boolean;

    if-nez v0, :cond_7

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Cardreaders;->requireBuilder(Lcom/squareup/server/account/protos/Cardreaders$Builder;)Lcom/squareup/server/account/protos/Cardreaders$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/Cardreaders;->DEFAULT_VERBOSE_TMN_TIMINGS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Cardreaders$Builder;->verbose_tmn_timings(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Cardreaders$Builder;

    move-result-object v1

    .line 235
    :cond_7
    iget-object v0, p0, Lcom/squareup/server/account/protos/Cardreaders;->enable_emoney_speedtest:Ljava/lang/Boolean;

    if-nez v0, :cond_8

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Cardreaders;->requireBuilder(Lcom/squareup/server/account/protos/Cardreaders$Builder;)Lcom/squareup/server/account/protos/Cardreaders$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/Cardreaders;->DEFAULT_ENABLE_EMONEY_SPEEDTEST:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Cardreaders$Builder;->enable_emoney_speedtest(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Cardreaders$Builder;

    move-result-object v1

    .line 236
    :cond_8
    iget-object v0, p0, Lcom/squareup/server/account/protos/Cardreaders;->use_v2_cardreaders:Ljava/lang/Boolean;

    if-nez v0, :cond_9

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Cardreaders;->requireBuilder(Lcom/squareup/server/account/protos/Cardreaders$Builder;)Lcom/squareup/server/account/protos/Cardreaders$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/Cardreaders;->DEFAULT_USE_V2_CARDREADERS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Cardreaders$Builder;->use_v2_cardreaders(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Cardreaders$Builder;

    move-result-object v1

    :cond_9
    if-nez v1, :cond_a

    move-object v0, p0

    goto :goto_0

    .line 237
    :cond_a
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/Cardreaders$Builder;->build()Lcom/squareup/server/account/protos/Cardreaders;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 28
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Cardreaders;->populateDefaults()Lcom/squareup/server/account/protos/Cardreaders;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 210
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 211
    iget-object v1, p0, Lcom/squareup/server/account/protos/Cardreaders;->r12_early_k400_powerup:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", r12_early_k400_powerup="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Cardreaders;->r12_early_k400_powerup:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 212
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/Cardreaders;->reorder_in_app:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", reorder_in_app="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Cardreaders;->reorder_in_app:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 213
    :cond_1
    iget-object v1, p0, Lcom/squareup/server/account/protos/Cardreaders;->reorder_in_app_email_confirm:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", reorder_in_app_email_confirm="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Cardreaders;->reorder_in_app_email_confirm:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 214
    :cond_2
    iget-object v1, p0, Lcom/squareup/server/account/protos/Cardreaders;->show_order_reader_status_in_app:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", show_order_reader_status_in_app="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Cardreaders;->show_order_reader_status_in_app:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 215
    :cond_3
    iget-object v1, p0, Lcom/squareup/server/account/protos/Cardreaders;->use_v2_ble_state_machine:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    const-string v1, ", use_v2_ble_state_machine="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Cardreaders;->use_v2_ble_state_machine:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 216
    :cond_4
    iget-object v1, p0, Lcom/squareup/server/account/protos/Cardreaders;->disable_magstripe_processing:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    const-string v1, ", disable_magstripe_processing="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Cardreaders;->disable_magstripe_processing:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 217
    :cond_5
    iget-object v1, p0, Lcom/squareup/server/account/protos/Cardreaders;->record_tmn_timings:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    const-string v1, ", record_tmn_timings="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Cardreaders;->record_tmn_timings:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 218
    :cond_6
    iget-object v1, p0, Lcom/squareup/server/account/protos/Cardreaders;->verbose_tmn_timings:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    const-string v1, ", verbose_tmn_timings="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Cardreaders;->verbose_tmn_timings:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 219
    :cond_7
    iget-object v1, p0, Lcom/squareup/server/account/protos/Cardreaders;->enable_emoney_speedtest:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    const-string v1, ", enable_emoney_speedtest="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Cardreaders;->enable_emoney_speedtest:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 220
    :cond_8
    iget-object v1, p0, Lcom/squareup/server/account/protos/Cardreaders;->use_v2_cardreaders:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    const-string v1, ", use_v2_cardreaders="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Cardreaders;->use_v2_cardreaders:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_9
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Cardreaders{"

    .line 221
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
