.class public final Lcom/squareup/server/account/protos/AccrualRules$CatalogObject$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AccrualRules.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/AccrualRules$CatalogObject;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/AccrualRules$CatalogObject;",
        "Lcom/squareup/server/account/protos/AccrualRules$CatalogObject$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public catalog_object_type:Lcom/squareup/server/account/protos/AccrualRules$CatalogObject$CatalogObjectType;

.field public token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 313
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/AccrualRules$CatalogObject;
    .locals 4

    .line 328
    new-instance v0, Lcom/squareup/server/account/protos/AccrualRules$CatalogObject;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules$CatalogObject$Builder;->token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/server/account/protos/AccrualRules$CatalogObject$Builder;->catalog_object_type:Lcom/squareup/server/account/protos/AccrualRules$CatalogObject$CatalogObjectType;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/server/account/protos/AccrualRules$CatalogObject;-><init>(Ljava/lang/String;Lcom/squareup/server/account/protos/AccrualRules$CatalogObject$CatalogObjectType;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 308
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/AccrualRules$CatalogObject$Builder;->build()Lcom/squareup/server/account/protos/AccrualRules$CatalogObject;

    move-result-object v0

    return-object v0
.end method

.method public catalog_object_type(Lcom/squareup/server/account/protos/AccrualRules$CatalogObject$CatalogObjectType;)Lcom/squareup/server/account/protos/AccrualRules$CatalogObject$Builder;
    .locals 0

    .line 322
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccrualRules$CatalogObject$Builder;->catalog_object_type:Lcom/squareup/server/account/protos/AccrualRules$CatalogObject$CatalogObjectType;

    return-object p0
.end method

.method public token(Ljava/lang/String;)Lcom/squareup/server/account/protos/AccrualRules$CatalogObject$Builder;
    .locals 0

    .line 317
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccrualRules$CatalogObject$Builder;->token:Ljava/lang/String;

    return-object p0
.end method
