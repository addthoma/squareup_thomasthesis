.class final Lcom/squareup/server/account/protos/LoyaltyProgram$ProtoAdapter_LoyaltyProgram;
.super Lcom/squareup/wire/ProtoAdapter;
.source "LoyaltyProgram.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/LoyaltyProgram;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_LoyaltyProgram"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/server/account/protos/LoyaltyProgram;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 605
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/server/account/protos/LoyaltyProgram;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/LoyaltyProgram;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 634
    new-instance v0, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;-><init>()V

    .line 635
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 636
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 660
    :pswitch_0
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 658
    :pswitch_1
    sget-object v3, Lcom/squareup/server/account/protos/ExpirationPolicy;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/ExpirationPolicy;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->expiration_policy(Lcom/squareup/server/account/protos/ExpirationPolicy;)Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;

    goto :goto_0

    .line 657
    :pswitch_2
    iget-object v3, v0, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->reward_tiers:Ljava/util/List;

    sget-object v4, Lcom/squareup/server/account/protos/RewardTier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 656
    :pswitch_3
    sget-object v3, Lcom/squareup/server/account/protos/AccrualRules;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/AccrualRules;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->accrual_rules(Lcom/squareup/server/account/protos/AccrualRules;)Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;

    goto :goto_0

    .line 655
    :pswitch_4
    sget-object v3, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->points_terminology(Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;)Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;

    goto :goto_0

    .line 649
    :pswitch_5
    :try_start_0
    sget-object v4, Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;

    invoke-virtual {v0, v4}, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->program_type(Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;)Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 651
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 646
    :pswitch_6
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->qualifying_purchase_description(Ljava/lang/String;)Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;

    goto :goto_0

    .line 640
    :pswitch_7
    :try_start_1
    sget-object v4, Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

    invoke-virtual {v0, v4}, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->program_status(Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;)Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;
    :try_end_1
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v4

    .line 642
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 664
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 665
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->build()Lcom/squareup/server/account/protos/LoyaltyProgram;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_0
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 603
    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/LoyaltyProgram$ProtoAdapter_LoyaltyProgram;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/LoyaltyProgram;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/LoyaltyProgram;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 622
    sget-object v0, Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/LoyaltyProgram;->program_status:Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 623
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/LoyaltyProgram;->qualifying_purchase_description:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 624
    sget-object v0, Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/LoyaltyProgram;->program_type:Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 625
    sget-object v0, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/LoyaltyProgram;->points_terminology:Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 626
    sget-object v0, Lcom/squareup/server/account/protos/AccrualRules;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/LoyaltyProgram;->accrual_rules:Lcom/squareup/server/account/protos/AccrualRules;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 627
    sget-object v0, Lcom/squareup/server/account/protos/RewardTier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/server/account/protos/LoyaltyProgram;->reward_tiers:Ljava/util/List;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 628
    sget-object v0, Lcom/squareup/server/account/protos/ExpirationPolicy;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/LoyaltyProgram;->expiration_policy:Lcom/squareup/server/account/protos/ExpirationPolicy;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 629
    invoke-virtual {p2}, Lcom/squareup/server/account/protos/LoyaltyProgram;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 603
    check-cast p2, Lcom/squareup/server/account/protos/LoyaltyProgram;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/server/account/protos/LoyaltyProgram$ProtoAdapter_LoyaltyProgram;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/LoyaltyProgram;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/server/account/protos/LoyaltyProgram;)I
    .locals 4

    .line 610
    sget-object v0, Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/LoyaltyProgram;->program_status:Lcom/squareup/server/account/protos/LoyaltyProgram$ProgramStatus;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/LoyaltyProgram;->qualifying_purchase_description:Ljava/lang/String;

    const/4 v3, 0x2

    .line 611
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/LoyaltyProgram;->program_type:Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;

    const/4 v3, 0x4

    .line 612
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/LoyaltyProgram;->points_terminology:Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;

    const/4 v3, 0x5

    .line 613
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/AccrualRules;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/LoyaltyProgram;->accrual_rules:Lcom/squareup/server/account/protos/AccrualRules;

    const/4 v3, 0x6

    .line 614
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/RewardTier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 615
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/server/account/protos/LoyaltyProgram;->reward_tiers:Ljava/util/List;

    const/4 v3, 0x7

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/ExpirationPolicy;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/LoyaltyProgram;->expiration_policy:Lcom/squareup/server/account/protos/ExpirationPolicy;

    const/16 v3, 0x8

    .line 616
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 617
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/LoyaltyProgram;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 603
    check-cast p1, Lcom/squareup/server/account/protos/LoyaltyProgram;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/LoyaltyProgram$ProtoAdapter_LoyaltyProgram;->encodedSize(Lcom/squareup/server/account/protos/LoyaltyProgram;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/server/account/protos/LoyaltyProgram;)Lcom/squareup/server/account/protos/LoyaltyProgram;
    .locals 2

    .line 670
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/LoyaltyProgram;->newBuilder()Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;

    move-result-object p1

    .line 671
    iget-object v0, p1, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->points_terminology:Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->points_terminology:Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;

    iput-object v0, p1, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->points_terminology:Lcom/squareup/server/account/protos/LoyaltyProgram$PointsTerminology;

    .line 672
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->accrual_rules:Lcom/squareup/server/account/protos/AccrualRules;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/server/account/protos/AccrualRules;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->accrual_rules:Lcom/squareup/server/account/protos/AccrualRules;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/AccrualRules;

    iput-object v0, p1, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->accrual_rules:Lcom/squareup/server/account/protos/AccrualRules;

    .line 673
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->reward_tiers:Ljava/util/List;

    sget-object v1, Lcom/squareup/server/account/protos/RewardTier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 674
    iget-object v0, p1, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->expiration_policy:Lcom/squareup/server/account/protos/ExpirationPolicy;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/server/account/protos/ExpirationPolicy;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->expiration_policy:Lcom/squareup/server/account/protos/ExpirationPolicy;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/ExpirationPolicy;

    iput-object v0, p1, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->expiration_policy:Lcom/squareup/server/account/protos/ExpirationPolicy;

    .line 675
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 676
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/LoyaltyProgram$Builder;->build()Lcom/squareup/server/account/protos/LoyaltyProgram;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 603
    check-cast p1, Lcom/squareup/server/account/protos/LoyaltyProgram;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/LoyaltyProgram$ProtoAdapter_LoyaltyProgram;->redact(Lcom/squareup/server/account/protos/LoyaltyProgram;)Lcom/squareup/server/account/protos/LoyaltyProgram;

    move-result-object p1

    return-object p1
.end method
