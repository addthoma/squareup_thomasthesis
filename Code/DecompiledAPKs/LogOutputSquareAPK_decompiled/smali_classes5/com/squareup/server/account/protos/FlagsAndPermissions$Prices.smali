.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;
.super Lcom/squareup/wire/AndroidMessage;
.source "FlagsAndPermissions.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Prices"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices$ProtoAdapter_Prices;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ENABLE_TAX_BASIS:Ljava/lang/Boolean;

.field public static final DEFAULT_ENABLE_WHOLE_PURCHASE_DISCOUNTS:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_PRICING_ENGINE_HEURISTIC:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final enable_tax_basis:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field

.field public final enable_whole_purchase_discounts:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field

.field public final use_pricing_engine_heuristic:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 17872
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices$ProtoAdapter_Prices;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices$ProtoAdapter_Prices;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 17874
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->CREATOR:Landroid/os/Parcelable$Creator;

    const/4 v0, 0x0

    .line 17878
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->DEFAULT_USE_PRICING_ENGINE_HEURISTIC:Ljava/lang/Boolean;

    .line 17880
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->DEFAULT_ENABLE_TAX_BASIS:Ljava/lang/Boolean;

    .line 17882
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->DEFAULT_ENABLE_WHOLE_PURCHASE_DISCOUNTS:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 1

    .line 17917
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 17923
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 17924
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->use_pricing_engine_heuristic:Ljava/lang/Boolean;

    .line 17925
    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->enable_tax_basis:Ljava/lang/Boolean;

    .line 17926
    iput-object p3, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->enable_whole_purchase_discounts:Ljava/lang/Boolean;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 17991
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 17942
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 17943
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;

    .line 17944
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->use_pricing_engine_heuristic:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->use_pricing_engine_heuristic:Ljava/lang/Boolean;

    .line 17945
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->enable_tax_basis:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->enable_tax_basis:Ljava/lang/Boolean;

    .line 17946
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->enable_whole_purchase_discounts:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->enable_whole_purchase_discounts:Ljava/lang/Boolean;

    .line 17947
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 17952
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_3

    .line 17954
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 17955
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->use_pricing_engine_heuristic:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 17956
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->enable_tax_basis:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 17957
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->enable_whole_purchase_discounts:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 17958
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices$Builder;
    .locals 2

    .line 17931
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices$Builder;-><init>()V

    .line 17932
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->use_pricing_engine_heuristic:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices$Builder;->use_pricing_engine_heuristic:Ljava/lang/Boolean;

    .line 17933
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->enable_tax_basis:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices$Builder;->enable_tax_basis:Ljava/lang/Boolean;

    .line 17934
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->enable_whole_purchase_discounts:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices$Builder;->enable_whole_purchase_discounts:Ljava/lang/Boolean;

    .line 17935
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 17871
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;
    .locals 2

    .line 17984
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->use_pricing_engine_heuristic:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->use_pricing_engine_heuristic:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices$Builder;->use_pricing_engine_heuristic(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices$Builder;

    move-result-object v1

    .line 17985
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->enable_tax_basis:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->enable_tax_basis:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices$Builder;->enable_tax_basis(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices$Builder;

    move-result-object v1

    .line 17986
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->enable_whole_purchase_discounts:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->enable_whole_purchase_discounts:Ljava/lang/Boolean;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices$Builder;->enable_whole_purchase_discounts(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices$Builder;

    move-result-object v1

    :cond_2
    if-nez v1, :cond_3

    move-object p1, p0

    goto :goto_0

    .line 17987
    :cond_3
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 17871
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;
    .locals 2

    .line 17975
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->use_pricing_engine_heuristic:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->DEFAULT_USE_PRICING_ENGINE_HEURISTIC:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices$Builder;->use_pricing_engine_heuristic(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices$Builder;

    move-result-object v1

    .line 17976
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->enable_tax_basis:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->DEFAULT_ENABLE_TAX_BASIS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices$Builder;->enable_tax_basis(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices$Builder;

    move-result-object v1

    .line 17977
    :cond_1
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->enable_whole_purchase_discounts:Ljava/lang/Boolean;

    if-nez v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->DEFAULT_ENABLE_WHOLE_PURCHASE_DISCOUNTS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices$Builder;->enable_whole_purchase_discounts(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices$Builder;

    move-result-object v1

    :cond_2
    if-nez v1, :cond_3

    move-object v0, p0

    goto :goto_0

    .line 17978
    :cond_3
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 17871
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 17965
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 17966
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->use_pricing_engine_heuristic:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", use_pricing_engine_heuristic="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->use_pricing_engine_heuristic:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 17967
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->enable_tax_basis:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", enable_tax_basis="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->enable_tax_basis:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 17968
    :cond_1
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->enable_whole_purchase_discounts:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", enable_whole_purchase_discounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;->enable_whole_purchase_discounts:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Prices{"

    .line 17969
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
