.class final Lcom/squareup/server/account/protos/ProcessingFees$ProtoAdapter_ProcessingFees;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ProcessingFees.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/ProcessingFees;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ProcessingFees"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/server/account/protos/ProcessingFees;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 249
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/server/account/protos/ProcessingFees;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/ProcessingFees;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 276
    new-instance v0, Lcom/squareup/server/account/protos/ProcessingFees$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/ProcessingFees$Builder;-><init>()V

    .line 277
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 278
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 287
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 285
    :pswitch_0
    sget-object v3, Lcom/squareup/server/account/protos/ProcessingFee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/ProcessingFee;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->jcb_cp(Lcom/squareup/server/account/protos/ProcessingFee;)Lcom/squareup/server/account/protos/ProcessingFees$Builder;

    goto :goto_0

    .line 284
    :pswitch_1
    sget-object v3, Lcom/squareup/server/account/protos/ProcessingFee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/ProcessingFee;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->interac(Lcom/squareup/server/account/protos/ProcessingFee;)Lcom/squareup/server/account/protos/ProcessingFees$Builder;

    goto :goto_0

    .line 283
    :pswitch_2
    sget-object v3, Lcom/squareup/server/account/protos/ProcessingFee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/ProcessingFee;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->cof(Lcom/squareup/server/account/protos/ProcessingFee;)Lcom/squareup/server/account/protos/ProcessingFees$Builder;

    goto :goto_0

    .line 282
    :pswitch_3
    sget-object v3, Lcom/squareup/server/account/protos/ProcessingFee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/ProcessingFee;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->invoice_web_form(Lcom/squareup/server/account/protos/ProcessingFee;)Lcom/squareup/server/account/protos/ProcessingFees$Builder;

    goto :goto_0

    .line 281
    :pswitch_4
    sget-object v3, Lcom/squareup/server/account/protos/ProcessingFee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/ProcessingFee;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->cp(Lcom/squareup/server/account/protos/ProcessingFee;)Lcom/squareup/server/account/protos/ProcessingFees$Builder;

    goto :goto_0

    .line 280
    :pswitch_5
    sget-object v3, Lcom/squareup/server/account/protos/ProcessingFee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/ProcessingFee;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->cnp(Lcom/squareup/server/account/protos/ProcessingFee;)Lcom/squareup/server/account/protos/ProcessingFees$Builder;

    goto :goto_0

    .line 291
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 292
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->build()Lcom/squareup/server/account/protos/ProcessingFees;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 247
    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/ProcessingFees$ProtoAdapter_ProcessingFees;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/ProcessingFees;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/ProcessingFees;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 265
    sget-object v0, Lcom/squareup/server/account/protos/ProcessingFee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/ProcessingFees;->cnp:Lcom/squareup/server/account/protos/ProcessingFee;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 266
    sget-object v0, Lcom/squareup/server/account/protos/ProcessingFee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/ProcessingFees;->cp:Lcom/squareup/server/account/protos/ProcessingFee;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 267
    sget-object v0, Lcom/squareup/server/account/protos/ProcessingFee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/ProcessingFees;->invoice_web_form:Lcom/squareup/server/account/protos/ProcessingFee;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 268
    sget-object v0, Lcom/squareup/server/account/protos/ProcessingFee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/ProcessingFees;->cof:Lcom/squareup/server/account/protos/ProcessingFee;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 269
    sget-object v0, Lcom/squareup/server/account/protos/ProcessingFee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/ProcessingFees;->interac:Lcom/squareup/server/account/protos/ProcessingFee;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 270
    sget-object v0, Lcom/squareup/server/account/protos/ProcessingFee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/ProcessingFees;->jcb_cp:Lcom/squareup/server/account/protos/ProcessingFee;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 271
    invoke-virtual {p2}, Lcom/squareup/server/account/protos/ProcessingFees;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 247
    check-cast p2, Lcom/squareup/server/account/protos/ProcessingFees;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/server/account/protos/ProcessingFees$ProtoAdapter_ProcessingFees;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/ProcessingFees;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/server/account/protos/ProcessingFees;)I
    .locals 4

    .line 254
    sget-object v0, Lcom/squareup/server/account/protos/ProcessingFee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/ProcessingFees;->cnp:Lcom/squareup/server/account/protos/ProcessingFee;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/server/account/protos/ProcessingFee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/ProcessingFees;->cp:Lcom/squareup/server/account/protos/ProcessingFee;

    const/4 v3, 0x2

    .line 255
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/ProcessingFee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/ProcessingFees;->invoice_web_form:Lcom/squareup/server/account/protos/ProcessingFee;

    const/4 v3, 0x3

    .line 256
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/ProcessingFee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/ProcessingFees;->cof:Lcom/squareup/server/account/protos/ProcessingFee;

    const/4 v3, 0x4

    .line 257
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/ProcessingFee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/ProcessingFees;->interac:Lcom/squareup/server/account/protos/ProcessingFee;

    const/4 v3, 0x5

    .line 258
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/ProcessingFee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/ProcessingFees;->jcb_cp:Lcom/squareup/server/account/protos/ProcessingFee;

    const/4 v3, 0x6

    .line 259
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 260
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/ProcessingFees;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 247
    check-cast p1, Lcom/squareup/server/account/protos/ProcessingFees;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/ProcessingFees$ProtoAdapter_ProcessingFees;->encodedSize(Lcom/squareup/server/account/protos/ProcessingFees;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/server/account/protos/ProcessingFees;)Lcom/squareup/server/account/protos/ProcessingFees;
    .locals 2

    .line 297
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/ProcessingFees;->newBuilder()Lcom/squareup/server/account/protos/ProcessingFees$Builder;

    move-result-object p1

    .line 298
    iget-object v0, p1, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->cnp:Lcom/squareup/server/account/protos/ProcessingFee;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/server/account/protos/ProcessingFee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->cnp:Lcom/squareup/server/account/protos/ProcessingFee;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/ProcessingFee;

    iput-object v0, p1, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->cnp:Lcom/squareup/server/account/protos/ProcessingFee;

    .line 299
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->cp:Lcom/squareup/server/account/protos/ProcessingFee;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/server/account/protos/ProcessingFee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->cp:Lcom/squareup/server/account/protos/ProcessingFee;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/ProcessingFee;

    iput-object v0, p1, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->cp:Lcom/squareup/server/account/protos/ProcessingFee;

    .line 300
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->invoice_web_form:Lcom/squareup/server/account/protos/ProcessingFee;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/server/account/protos/ProcessingFee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->invoice_web_form:Lcom/squareup/server/account/protos/ProcessingFee;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/ProcessingFee;

    iput-object v0, p1, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->invoice_web_form:Lcom/squareup/server/account/protos/ProcessingFee;

    .line 301
    :cond_2
    iget-object v0, p1, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->cof:Lcom/squareup/server/account/protos/ProcessingFee;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/server/account/protos/ProcessingFee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->cof:Lcom/squareup/server/account/protos/ProcessingFee;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/ProcessingFee;

    iput-object v0, p1, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->cof:Lcom/squareup/server/account/protos/ProcessingFee;

    .line 302
    :cond_3
    iget-object v0, p1, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->interac:Lcom/squareup/server/account/protos/ProcessingFee;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/server/account/protos/ProcessingFee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->interac:Lcom/squareup/server/account/protos/ProcessingFee;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/ProcessingFee;

    iput-object v0, p1, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->interac:Lcom/squareup/server/account/protos/ProcessingFee;

    .line 303
    :cond_4
    iget-object v0, p1, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->jcb_cp:Lcom/squareup/server/account/protos/ProcessingFee;

    if-eqz v0, :cond_5

    sget-object v0, Lcom/squareup/server/account/protos/ProcessingFee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->jcb_cp:Lcom/squareup/server/account/protos/ProcessingFee;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/ProcessingFee;

    iput-object v0, p1, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->jcb_cp:Lcom/squareup/server/account/protos/ProcessingFee;

    .line 304
    :cond_5
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 305
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->build()Lcom/squareup/server/account/protos/ProcessingFees;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 247
    check-cast p1, Lcom/squareup/server/account/protos/ProcessingFees;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/ProcessingFees$ProtoAdapter_ProcessingFees;->redact(Lcom/squareup/server/account/protos/ProcessingFees;)Lcom/squareup/server/account/protos/ProcessingFees;

    move-result-object p1

    return-object p1
.end method
